/******************************************************************************
 *
 * @file     led.c
 * @brief    Support for DIP switches on SRU Base board
 * @version  V1.00
 * @date     23, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include <sru_e.h>
#include <sru_e_private.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static const struct st_port_pin_pair gastPortPinPairs_LEDs[ SRU_NUM_LEDS ] =
{
#if ENABLE_SR3031F
   {0, 25},  // enSRU_LED_Heartbeat
   {0, 26},  // enSRU_LED_Fault
   {0, 24},  // enSRU_LED_Extra
#else
   {0, 25},  // enSRU_LED_Heartbeat
   {0, 26},  // enSRU_LED_Fault
#endif
};

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SRU_Write_LED( enum en_sru_leds enSRU_LED, uint32_t bValue )
{
   if ( enSRU_LED < SRU_NUM_LEDS )
   {
      uint32_t uiPort = gastPortPinPairs_LEDs[ enSRU_LED ].ucPort;
      uint32_t uiPin = gastPortPinPairs_LEDs[ enSRU_LED ].ucPin;

      // The port pins that control the LEDs must be pulled low to turn on the
      // LED so we need to invert bValue to get the desired behavior.
      Sys_MCU_Pin_Out( uiPort, uiPin, !bValue );
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void LED_Init( void )
{
   //set direction
   for(uint8_t j = 0; j < SRU_NUM_LEDS;j++)
   {
      Sys_MCU_Pin_SetDir(gastPortPinPairs_LEDs[j].ucPort,
                     gastPortPinPairs_LEDs[j].ucPin);
   }
   // Turn off all LEDs.
   int i;
   for ( i = 0; i < SRU_NUM_LEDS; ++i )
   {
      SRU_Write_LED( i, 0 );
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
