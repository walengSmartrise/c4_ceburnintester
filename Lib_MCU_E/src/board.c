/*
 * @brief Embedded Artists LPC1788 and LPC4088 Development Kit board file
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2012
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#include "board.h"



/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/



/* System oscillator rate and RTC oscillator rate */


// X macro the following
typedef enum
{
   Alarm,
   Fault,
   Heartbeat,

   Num_LEDS
}LED_Names;

typedef struct{
   uint8_t ledPort;
   uint8_t ledPin;
} LED;

static const LED leds[] = {
        //SDA REVIEW: why is this here? are we eben using it
// TODO: Fill in
};

/*****************************************************************************
 * Private functions
 ****************************************************************************/


/*****************************************************************************
 * Public functions
 ****************************************************************************/

/* Initialize UART pins */
void Board_UART_Init(LPC_USART_T *pUART)
{
    //SDA REVIEW: if this function is not used then get rif of it
}

/* Sets the state of a board LED to on or off */
void Board_LED_Set(LED_Names LEDNumber, bool On)
{
   if (LEDNumber < Num_LEDS) {
      /* Set state, low is on, high is off */
      Chip_GPIO_SetPinState(LPC_GPIO, leds[LEDNumber].ledPort, leds[LEDNumber].ledPin, !On);
   }
}

/* Returns the current state of a board LED */
bool Board_LED_Test(uint8_t LEDNumber)
{
   bool state = false;

   if (LEDNumber < Num_LEDS) {
      state = Chip_GPIO_GetPinState(LPC_GPIO, leds[LEDNumber].ledPort, leds[LEDNumber].ledPin);
   }

   /* These LEDs are reverse logic. */
   return !state;
}

/* Toggles the current state of a board LED */
void Board_LED_Toggle(uint8_t LEDNumber)
{
   if (LEDNumber < Num_LEDS) {
      Chip_GPIO_SetPinToggle(LPC_GPIO, leds[LEDNumber].ledPort, leds[LEDNumber].ledPin);
   }
}

//SDA REVIEW: if this function is not used then get rid of it
/* Initialize pin muxing for SSP interface */
void Board_SSP_Init(LPC_SSP_T *pSSP)
{

}

/* Set up and initialize all required blocks and functions related to the
   board hardware */
void Board_Init(void)
{
    //SDA REVIEW: commented code. remove it and commit to bitbucket
   /* Initializes GPIO */
   //Chip_GPIO_Init(LPC_GPIO);
   //Chip_IOCON_Init(LPC_IOCON);

   //Chip_CAN_Init(LPC_CAN1);
   //Chip_CAN_Init(LPC_CAN1);


   //Board_GPIO_Init();


   /* Initialize LEDs */
   //Board_LED_Init();

   /* Initialize the LCD Display */
   //Board_LCD_Init();

   /* Initialize the Pushbuttons */
   //Board_Pushbutton_Init();
   //Board_DIP_Init();
   //Board_MCU_ID_Init();
   //Board_617_618_Init();
}

//SDA REVIEW: is this function used? if not get rid of it
/* Sets up board specific CAN interface */
void Board_CAN_Init(LPC_CAN_T *pCAN)
{
   
}


