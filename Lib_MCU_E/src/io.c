/******************************************************************************
 *
 * @file     io.c
 * @brief    I/O support for MCU A on SRU Base board
 * @version  V1.00
 * @date     21, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include <sru_e.h>
#include <sru_e_private.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
#define INVERT_IO_LAYOUT 0

// TODO: this should be in a different file
static const struct st_port_pin_pair gastPortPinPairs_Inputs[ SRU_NUM_INPUTS ] =
{
        //SDA REVIEW: is invert layout something that we will change? lets decide on a format and stick to it
#if INVERT_IO_LAYOUT == 1
   {1, 15},  // enSRU_Input_501
   {1, 14},  // enSRU_Input_502
   {1, 10},  // enSRU_Input_503
   {1, 9},   // enSRU_Input_504
   {1, 8},   // enSRU_Input_505
   {1, 4},   // enSRU_Input_506
   {1, 1},   // enSRU_Input_507
   {1, 0},   // enSRU_Input_508
#else
   {1, 0},   // enSRU_Input_501
   {1, 1},   // enSRU_Input_502
   {1, 4},   // enSRU_Input_503
   {1, 8},   // enSRU_Input_504
   {1, 9},   // enSRU_Input_505
   {1, 10},  // enSRU_Input_506
   {1, 14},  // enSRU_Input_507
   {1, 15},  // enSRU_Input_508
#endif
};

//SDA REVIEW: is this todo still valid. if not remove it and commit to bitbucket
// TODO: this should be in a different file
static const struct st_port_pin_pair gastPortPinPairs_Outputs[ SRU_NUM_OUTPUTS ] =
{
        //SDA REVIEW: lets choose to invert or nott and remove what we dont need
#if INVERT_IO_LAYOUT == 1
   {1, 26},  // enSRU_Output_601
   {1, 25},  // enSRU_Output_602
   {1, 24},  // enSRU_Output_603
   {1, 23},  // enSRU_Output_604
   {1, 22},  // enSRU_Output_605
   {1, 20},  // enSRU_Output_606
   {1, 19},  // enSRU_Output_607
   {1, 18},  // enSRU_Output_608
#else
   {1, 18},  // enSRU_Output_601
   {1, 19},  // enSRU_Output_602
   {1, 20},  // enSRU_Output_603
   {1, 22},  // enSRU_Output_604
   {1, 23},  // enSRU_Output_605
   {1, 24},  // enSRU_Output_606
   {1, 25},  // enSRU_Output_607
   {1, 26},  // enSRU_Output_608
#endif
};

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_Input( enum en_sru_inputs enSRU_Input )
{
   uint8_t bValue = 0;

   if ( enSRU_Input < SRU_NUM_INPUTS )
   {
      uint32_t uiPort = gastPortPinPairs_Inputs[ enSRU_Input ].ucPort;
      uint32_t uiPin = gastPortPinPairs_Inputs[ enSRU_Input ].ucPin;

      // Invert the value returned by Sys_MCU_Pin_In() because when an input is
      // powered, the port pin is low and vice versa.
      bValue = !Sys_MCU_Pin_In( uiPort, uiPin );
   }

   return bValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SRU_Write_Output( enum en_sru_outputs enSRU_Output, uint32_t bValue )
{
   if ( enSRU_Output < SRU_NUM_OUTPUTS )
   {
      uint32_t uiPort = gastPortPinPairs_Outputs[ enSRU_Output ].ucPort;
      uint32_t uiPin = gastPortPinPairs_Outputs[ enSRU_Output ].ucPin;

      Sys_MCU_Pin_Out( uiPort, uiPin, bValue );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
//SDA REVIEW: if this is not used the get rid of it
void ConfigureGPIO_Direction(void)
{

}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void IO_Init( void )
{
   for(uint8_t i = 0; i < SRU_NUM_OUTPUTS; i++)
   {
      Chip_GPIO_SetDir(LPC_GPIO, gastPortPinPairs_Outputs[i].ucPort, gastPortPinPairs_Outputs[i].ucPin,true);
   }
   int i;

   // Turn off all outputs.
   for ( i = 0; i < SRU_NUM_OUTPUTS; ++i )
   {
      SRU_Write_Output( i, 0 );
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
