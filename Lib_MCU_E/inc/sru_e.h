/******************************************************************************
 *
 * @file     sru_a.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef SRU_E_H
#define SRU_E_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "sru.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define ENABLE_SR3031F     (1)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

enum en_sru_inputs
{
   enSRU_Input_501,
   enSRU_Input_502,
   enSRU_Input_503,
   enSRU_Input_504,
   enSRU_Input_505,
   enSRU_Input_506,
   enSRU_Input_507,
   enSRU_Input_508,

   SRU_NUM_INPUTS
};

enum en_sru_outputs
{
   enSRU_Output_601,
   enSRU_Output_602,
   enSRU_Output_603,
   enSRU_Output_604,
   enSRU_Output_605,
   enSRU_Output_606,
   enSRU_Output_607,
   enSRU_Output_608,

   SRU_NUM_OUTPUTS
};

enum en_sru_leds
{
#if ENABLE_SR3031F
   enSRU_LED_Heartbeat, // For the new boards this is LED B
   enSRU_LED_Fault,
   enSRU_LED_Extra, // For the new boards this is LED A
#else
   enSRU_LED_Heartbeat,
   enSRU_LED_Fault,
#endif
   SRU_NUM_LEDS
};

enum en_sru_dip_switches
{
   enSRU_DIP_B1,
   enSRU_DIP_B2,
   enSRU_DIP_B3,
   enSRU_DIP_B4,
   enSRU_DIP_B5,
   enSRU_DIP_B6,
   enSRU_DIP_B7,
   enSRU_DIP_B8,

   SRU_NUM_DIP_SWITCHES
};

enum en_sru_dip_banks
{
   enSRU_DIP_BANK_B,

   enSRU_NUM_DIP_BANKS,
};

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

//extern const struct st_port_pin_pair gastPortPinPairs_Inputs[ SRU_NUM_INPUTS ];

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

void MCU_E_Init( void );

uint8_t SRU_Read_Input( enum en_sru_inputs enSRU_Input );

void SRU_Write_Output( enum en_sru_outputs enSRU_Output, uint32_t bValue );

void SRU_Write_LED( enum en_sru_leds enSRU_LED, uint32_t bValue );

uint8_t SRU_Read_DIP_Switch( enum en_sru_dip_switches enSRU_DIP_Switch );

uint8_t SRU_Read_DIP_Bank( enum en_sru_dip_banks enSRU_DIP_Bank );

void SetSRU_Deployment( void );
en_sru_deployments GetSRU_Deployment( void );

void CAN_Init( void );
Status CAN1_UnloadFromRB(CAN_MSG_T *pstRxMsg);
Status CAN1_LoadToRB(CAN_MSG_T *pstRxMsg);
Status CAN2_UnloadFromRB(CAN_MSG_T *pstRxMsg);
Status CAN2_LoadToRB(CAN_MSG_T *pstRxMsg);
Status CAN1_FillHardwareBuffer();
Status CAN2_FillHardwareBuffer();

uint8_t GetSRU_TestMode();
uint8_t SRU_CheckIfWatchdogDisabled();
#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
