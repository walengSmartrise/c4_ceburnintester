#include <mcu_inputBoard.h>
#include <string.h>
#define EXP_CAR_NET      (LPC_CAN1)
#define EXP_CAR_NET_BAUD (enCAN_BAUD_125k)
#define EXP_SUB_NET      (LPC_CAN2)
#define EXP_SUB_NET_BAUD (enCAN_BAUD_125k)

#define RIS_GRP_NET        (LPC_CAN1)
#define RIS_GRP_NET_BAUD   (enCAN_BAUD_125k)
#define RIS_HALL_NET      (LPC_CAN2)
#define RIS_HALL_NET_BAUD (enCAN_BAUD_50k)

//--------------------------------------------------------
// Ring buffer of can messages
// This is accessed outside of this module.
extern RINGBUFF_T RxRingBuffer_CAN1;
extern RINGBUFF_T RxRingBuffer_CAN2;
extern RINGBUFF_T TxRingBuffer_CAN1;
extern RINGBUFF_T TxRingBuffer_CAN2;

#define RING_BUFFER_SIZE (32)
static CAN_MSG_T RxBuffer_CAN1[RING_BUFFER_SIZE];
static CAN_MSG_T RxBuffer_CAN2[RING_BUFFER_SIZE];
static CAN_MSG_T TxBuffer_CAN1[RING_BUFFER_SIZE];
static CAN_MSG_T TxBuffer_CAN2[RING_BUFFER_SIZE];
//--------------------------------------------------------
static void Init_CAN_RingBuffers( void )
{
    // Init the Rx Ring buffer.
    RingBuffer_Init(&RxRingBuffer_CAN1, RxBuffer_CAN1, sizeof(RxBuffer_CAN1[0]), RING_BUFFER_SIZE);
    RingBuffer_Init(&RxRingBuffer_CAN2, RxBuffer_CAN2, sizeof(RxBuffer_CAN2[0]), RING_BUFFER_SIZE);

    // Init the Tx Ring buffer.
    RingBuffer_Init(&TxRingBuffer_CAN1, TxBuffer_CAN1, sizeof(TxBuffer_CAN1[0]), RING_BUFFER_SIZE);
    RingBuffer_Init(&TxRingBuffer_CAN2, TxBuffer_CAN2, sizeof(TxBuffer_CAN2[0]), RING_BUFFER_SIZE);
}
//--------------------------------------------------------
void CAN_Init( void )
{
   // This is the only place the can periphs are init.
   switch(GetSRU_Deployment())
   {
       case enSRU_DEPLOYMENT__CN:
           // Car Net
           // TODO: Network abstraction layer
           Chip_CAN_Init      (EXP_CAR_NET, LPC_CANAF, LPC_CANAF_RAM);
           Chip_CAN_SetBitRate(EXP_CAR_NET, EXP_CAR_NET_BAUD);
           Chip_CAN_EnableInt (EXP_CAR_NET, CAN_IER_RIE);
           Chip_CAN_SetAFMode (LPC_CANAF,CAN_AF_BYBASS_MODE);

           // sub expansion board net
           // TODO: Network abstraction layer
           Chip_CAN_Init      (EXP_SUB_NET, LPC_CANAF, LPC_CANAF_RAM);
           Chip_CAN_SetBitRate(EXP_SUB_NET, EXP_SUB_NET_BAUD);
           Chip_CAN_EnableInt (EXP_SUB_NET, CAN_IER_RIE);
           Chip_CAN_SetAFMode (LPC_CANAF,CAN_AF_BYBASS_MODE);

           NVIC_EnableIRQ(CAN_IRQn);

           break;
       case enSRU_DEPLOYMENT__RIS:
           // Car Net
           Chip_CAN_Init      (RIS_GRP_NET, LPC_CANAF, LPC_CANAF_RAM);
           Chip_CAN_SetBitRate(RIS_GRP_NET, RIS_GRP_NET_BAUD);
           Chip_CAN_EnableInt (RIS_GRP_NET, CAN_IER_BITMASK);
           Chip_CAN_SetAFMode (LPC_CANAF,CAN_AF_BYBASS_MODE);

           // CEDES
           Chip_CAN_Init      (RIS_HALL_NET, LPC_CANAF, LPC_CANAF_RAM);
           Chip_CAN_SetBitRate(RIS_HALL_NET, RIS_HALL_NET_BAUD);
           Chip_CAN_EnableInt (RIS_HALL_NET, CAN_IER_BITMASK);
           Chip_CAN_SetAFMode (LPC_CANAF,CAN_AF_BYBASS_MODE);

           NVIC_EnableIRQ(CAN_IRQn);
           break;
       default:

           break;
   }
   Init_CAN_RingBuffers();
}

Status CAN1_UnloadFromRB(CAN_MSG_T *pstRxMsg)
{
   Status bReturn = ERROR;
   if(!RingBuffer_IsEmpty( &RxRingBuffer_CAN1 ) )
   {
      RingBuffer_Pop(&RxRingBuffer_CAN1, pstRxMsg);
      bReturn = SUCCESS;
   }
   return bReturn;
}

Status CAN1_LoadToRB(CAN_MSG_T *pstRxMsg)
{
   Status bReturn = ERROR;
   if(RingBuffer_Insert(&TxRingBuffer_CAN1, pstRxMsg))
   {
      bReturn = SUCCESS;
   }
   return bReturn;
}

Status CAN2_UnloadFromRB(CAN_MSG_T *pstRxMsg)
{
   Status bReturn = ERROR;
   if(!RingBuffer_IsEmpty( &RxRingBuffer_CAN2 ) )
   {
      RingBuffer_Pop(&RxRingBuffer_CAN2, pstRxMsg);
      bReturn = SUCCESS;
   }
   return bReturn;
}

Status CAN2_LoadToRB(CAN_MSG_T *pstRxMsg)
{
   Status bReturn = ERROR;
   if(RingBuffer_Insert(&TxRingBuffer_CAN2, pstRxMsg))
   {
      bReturn = SUCCESS;
   }
   return bReturn;
}
/*----------------------------------------------------------------------------
   If theres is data in the TxRingBuffer, it will be loaded into any hardware buffers
 *----------------------------------------------------------------------------*/
Status CAN1_FillHardwareBuffer()
{
   Status bReturn = ERROR;
   for(uint8_t i = 0; i < 2; i++)
   {
      CAN_BUFFER_ID_T TxBuf = CAN_GetFreeTxBuf_Pr1(LPC_CAN1);
      if(TxBuf >= CAN_BUFFER_LAST)
      {
         i = 3;
         break;
      }
      else
      {
         CAN_MSG_T stTxMsg;
         if(!RingBuffer_IsEmpty( &TxRingBuffer_CAN1 ) )
         {
            RingBuffer_Pop(&TxRingBuffer_CAN1, &stTxMsg);
            bReturn = Chip_CAN_Send(LPC_CAN1, TxBuf, &stTxMsg);
         }
      }
   }
   return bReturn;
}
Status CAN2_FillHardwareBuffer()
{
   Status bReturn = ERROR;
   for(uint8_t i = 0; i < 2; i++)
   {
      CAN_BUFFER_ID_T TxBuf = CAN_GetFreeTxBuf_Pr1(LPC_CAN2);
      if(TxBuf >= CAN_BUFFER_LAST)
      {
         i = 3;
         break;
      }
      else
      {
         CAN_MSG_T stTxMsg;
         if(!RingBuffer_IsEmpty( &TxRingBuffer_CAN2 ) )
         {
            RingBuffer_Pop(&TxRingBuffer_CAN2, &stTxMsg);
            bReturn = Chip_CAN_Send(LPC_CAN2, TxBuf, &stTxMsg);
         }
      }
   }
   return bReturn;
}


