/******************************************************************************
 *
 * @file     mcu_f.c
 * @brief    Support for the 24-Input board
 * @version  V1.00
 * @date     21, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include <mcu_inputBoard.h>
#include <mcu_inputBoard_private.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

   Type "D" I/O pins. See NXP User Manual Tables 83/84.
   Reset value 00 0 1 10 000 = 0x00000030

 -----------------------------------------------------------------------------*/
static void IOCON_D_Init( void )
{
   // The SRU board has pull-up resistors on the MCU input signals so I
   // am disabling the on-chip pull-ups and pull-downs. I am leaving
   // Hysteresis enabled as that is the default.
   uint32_t uiInput = (IOCON_FUNC0 | IOCON_HYS_EN);
   uint32_t uiOutput = IOCON_FUNC0;

   // The SRU board has pull-down resistors on the MCU output signals so I
   // am disabling the on-chip pull-ups and pull-downs. I am leaving
   // Hysteresis enabled as that is the default.

   LPC_IOCON->p[0][0] = (IOCON_FUNC1 | IOCON_HYS_EN | IOCON_MODE_PULLUP );  // CAN1_RD
   LPC_IOCON->p[0][1] = (IOCON_FUNC1 | IOCON_HYS_EN | IOCON_MODE_PULLUP );  // CAN1_TD

   LPC_IOCON->p[0][4] = (IOCON_FUNC2 | IOCON_HYS_EN | IOCON_MODE_PULLUP );  // CAN2_RD
   LPC_IOCON->p[0][5] = (IOCON_FUNC2 | IOCON_HYS_EN | IOCON_MODE_PULLUP );  // CAN2_TD

   LPC_IOCON->p[1][0]  = uiInput;  // Input 501
   LPC_IOCON->p[1][1] = uiInput;  // Input 502
   LPC_IOCON->p[1][4]  = uiInput;  // Input 503
   LPC_IOCON->p[1][8]  = uiInput;  // Input 504
   LPC_IOCON->p[1][9]  = uiInput;  // Input 505
   LPC_IOCON->p[1][10]  = uiInput;  // Input 506
   LPC_IOCON->p[1][15]  = uiInput;  // Input 508

   LPC_IOCON->p[0][3] = uiInput;  // Input 509
   LPC_IOCON->p[0][2] = uiInput;  // Input 510
   LPC_IOCON->p[4][0] = uiInput;  // Input 511
   LPC_IOCON->p[0][14] = uiInput;  // Input 512
   LPC_IOCON->p[3][24] = uiInput;  // Input 513
   LPC_IOCON->p[3][25] = uiInput;  // Input 514
   LPC_IOCON->p[3][23] = uiInput;  // Input 515
   LPC_IOCON->p[1][19] = uiInput;  // Input 516

   LPC_IOCON->p[1][18]  = uiInput;  // Input 517
   LPC_IOCON->p[3][26] = uiInput;  // Input 518
   LPC_IOCON->p[1][20] = uiInput;  // Input 519
   LPC_IOCON->p[1][22] = uiInput;  // Input 520
   LPC_IOCON->p[2][5] = uiInput;  // Input 522
   LPC_IOCON->p[2][12] = uiInput;  // Input 523
   LPC_IOCON->p[4][13] = uiInput;  // Input 524

   LPC_IOCON->p[0][10] = uiInput;  // Board ID 1
   LPC_IOCON->p[0][11] = uiInput;  // Board ID 2
   LPC_IOCON->p[0][17] = uiInput;  // Board ID 3
   LPC_IOCON->p[0][18] = uiInput;  // Board ID 4

   // The SRU board has pull-up resistors on the DIP switches so they can
   // be treated the same as the inputs.
   LPC_IOCON->p[0][6] = uiInput;  // DIP B0
   LPC_IOCON->p[2][0]  = uiInput;  // DIP B4
   LPC_IOCON->p[2][1]  = uiInput;  // DIP B5
   LPC_IOCON->p[2][2]  = uiInput;  // DIP B6
   LPC_IOCON->p[2][3]  = uiInput;  // DIP B7

   LPC_IOCON->p[2][11]  = uiInput; // Watchdog enable

   LPC_IOCON->p[0][29] = uiOutput; //Output 601
   LPC_IOCON->p[0][30] = uiOutput;
   LPC_IOCON->p[0][31] = uiOutput;
   LPC_IOCON->p[0][27] = uiOutput;
   LPC_IOCON->p[1][23] = uiOutput;
   LPC_IOCON->p[1][24] = uiOutput;
   LPC_IOCON->p[1][25] = uiOutput;
   LPC_IOCON->p[1][26] = uiOutput; //Output 608

   LPC_IOCON->p[4][12] = uiOutput; //Output 609
   LPC_IOCON->p[4][11] = uiOutput;
   LPC_IOCON->p[2][7] = uiOutput;
   LPC_IOCON->p[4][10] = uiOutput;
   LPC_IOCON->p[2][8] = uiOutput;
   LPC_IOCON->p[4][9] = uiOutput;
   LPC_IOCON->p[4][8] = uiOutput;
   LPC_IOCON->p[0][19] = uiOutput; //Output 616

   LPC_IOCON->p[4][7] = uiOutput; //Ouput 617
   LPC_IOCON->p[0][20] = uiOutput;
   LPC_IOCON->p[4][6] = uiOutput;
   LPC_IOCON->p[4][5] = uiOutput;
   LPC_IOCON->p[3][2] = uiOutput;
   LPC_IOCON->p[3][1] = uiOutput;
   LPC_IOCON->p[3][0] = uiOutput;
   LPC_IOCON->p[4][31] = uiOutput; //Output 624

   LPC_IOCON->p[1][16] = uiOutput; //Output Enable
}

/*-----------------------------------------------------------------------------

   Type "A" I/O pins. See NXP User Manual Tables 85/86.
   Reset value 0 1 1 0 0 10 000 = 0x00000190

 -----------------------------------------------------------------------------*/
static void IOCON_A_Init( void )
{
   uint32_t uiLED = (IOCON_FUNC0 | IOCON_MODE_PULLUP | IOCON_HYS_EN | IOCON_DIGMODE_EN);//0x30;
   uint32_t uiInput = (IOCON_FUNC0 | IOCON_HYS_EN | IOCON_DIGMODE_EN);
   // The three LEDs that MCU A controls are pulled up to 3.3V on the anode
   // side. The cathode side connects directly to the MCU port pins. I am
   // enabling the on-chip pull-ups so that the LED will be off by default
   // until the pin is driven.

   LPC_IOCON->p[0][25]= uiLED;  // Heartbeat LED
   LPC_IOCON->p[0][26]= uiLED;  // Fault LED
   LPC_IOCON->p[0][24]= uiLED;  // Extra LED

   LPC_IOCON->p[1][30] = uiInput;  // Input 521
}

/*-----------------------------------------------------------------------------

   Type "U" I/O pins. See NXP User Manual Tables 87/88.
   Reset value = 0

 -----------------------------------------------------------------------------*/
static void IOCON_U_Init( void )
{
}

/*-----------------------------------------------------------------------------

   Type "I" I/O pins. See NXP User Manual Tables 89/90.
   Reset value = 0

 -----------------------------------------------------------------------------*/
static void IOCON_I_Init( void )
{
}

/*-----------------------------------------------------------------------------

   Type "W" I/O pins. See NXP User Manual Tables 91/92.

   Reset value
      P0[7-9] = 0 1 0 1 00 000 = 0x0a0
      P1[5-7, 14, 16-17] = 0 1 0 1 10 000 = 0x0b0

 -----------------------------------------------------------------------------*/
static void IOCON_W_Init( void )
{
   uint32_t uiInput = (IOCON_FUNC0 | IOCON_DIGMODE_EN | IOCON_HYS_EN);//0x0A0;

   LPC_IOCON->p[1][14]  = uiInput;  // Input 507

   LPC_IOCON->p[0][7] = uiInput;  // DIP B1
   LPC_IOCON->p[0][8]  = uiInput;  // DIP B2
   LPC_IOCON->p[0][9]  = uiInput;  // DIP B3
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void IOCON_Init( void )
{
   IOCON_D_Init();
   IOCON_A_Init();
   IOCON_U_Init();
   IOCON_I_Init();
   IOCON_W_Init();
}


/*-----------------------------------------------------------------------------
   Verify the 4 ID pins connected to the MCU identify this MCU as part of the
   SRU Base board (MCU A). If not, loop forever flashing all LEDs.
 -----------------------------------------------------------------------------*/
void VerifyBoardID( void )
{
   enum_sys_mcu_ids enMCU_ID = enMCU_Unknown;

   if(SRU_Read_MCU_ID() == enMCU_Input)
   {
      enMCU_ID = enMCUF_Input;
   }

   if ( enMCU_ID != enMCUF_Input )
   {
      while ( 1 )
      {
         static uint32_t uiCounter, bState;

         if( uiCounter >= 0xfffff )
         {
            uiCounter = 0;

            bState = ( bState )? 0: 1;

            SRU_Write_LED( enSRU_LED_Heartbeat, bState );
            SRU_Write_LED( enSRU_LED_Fault,     bState );
         }
         else
         {
            uiCounter++;
         }
      }
   }
}
/*-----------------------------------------------------------------------------
If all dips are set, board will be in test mode.
 * In Test mode:
 *    Watchdog is disabled.
 *    HB LED toggles with RX message on CAN1
 *    FLT LED toggles with RX message on CAN2
 *    Outputs will mirror inputs
 *    Board will otherwise act like an IO EXP board
 -----------------------------------------------------------------------------*/
static uint8_t bBoardTestFlag;
uint8_t GetSRU_TestMode( void )
{
   return bBoardTestFlag;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static en_sru_deployments eSRU_Deployment;
inline en_sru_deployments GetSRU_Deployment( void )
{
     return eSRU_Deployment;
}

inline void SetSRU_Deployment( void )
{
   if(SRU_Read_MCU_ID() == enMCU_Input)
   {
      eSRU_Deployment = enSRU_DEPLOYMENT__CN;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_CheckIfWatchdogDisabled()
{
   uint8_t bDisabled = 0;
   // When WD_EN is high (jumper is off), watchdog is on.
   bDisabled = !Sys_MCU_Pin_In( 2, 11 );

   return bDisabled;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void MCU_InputBoard_Init( void )
{
   VerifyBoardID();

   IOCON_Init();

   IO_Init();

   LED_Init();

   bBoardTestFlag = ( SRU_Read_DIP_Bank(enSRU_DIP_BANK_B) == 0xFF )? 1:0;

   SetSRU_Deployment();

   CAN_Init();
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
