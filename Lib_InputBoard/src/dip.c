/******************************************************************************
 *
 * @file     dip.c
 * @brief    Support for DIP switches on SRU Base board
 * @version  V1.00
 * @date     23, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include <mcu_inputBoard.h>
#include <mcu_inputBoard_private.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_DIP_Switch( enum en_sru_dip_switches enSRU_DIP_Switch )
{
   uint8_t bValue = 0;

   //SDA REVIEW:is this todo still valid? can it be removed
   // TODO: Relient on the hardware

   switch (enSRU_DIP_Switch)
   {
       case enSRU_DIP_B1:
           bValue = !Chip_GPIO_GetPinState(LPC_GPIO,0,6);
           break;
       case enSRU_DIP_B2:
           bValue = !Chip_GPIO_GetPinState(LPC_GPIO,0,7);
           break;
       case enSRU_DIP_B3:
           bValue = !Chip_GPIO_GetPinState(LPC_GPIO,0,8);
           break;
       case enSRU_DIP_B4:
           bValue = !Chip_GPIO_GetPinState(LPC_GPIO,0,9);
           break;
       case enSRU_DIP_B5:
           bValue = !Chip_GPIO_GetPinState(LPC_GPIO,2,0);
           break;
       case enSRU_DIP_B6:
           bValue = !Chip_GPIO_GetPinState(LPC_GPIO,2,1);
           break;
       case enSRU_DIP_B7:
           bValue = !Chip_GPIO_GetPinState(LPC_GPIO,2,2);
           break;
       case enSRU_DIP_B8:
           bValue = !Chip_GPIO_GetPinState(LPC_GPIO,2,3);
           break;
       default:
           bValue = 0;
           break;
   }
   //SDA REVIEW: commented code. remove it and commit to bitbucket
/*
   if ( enSRU_DIP_Switch < SRU_NUM_DIP_SWITCHES )
   {
      // Invert the value returned by Sys_MCU_Pin_In() because when a DIP switch is
      // in the ON position, the port pin is low and vice versa.
      bValue = !Sys_MCU_Pin_In( 4, enSRU_DIP_Switch );
   }
*/
   return bValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_DIP_Bank( enum en_sru_dip_banks enSRU_DIP_Bank )
{
    // TODO: relient on the hardware. Need to fix this
   uint8_t ucValue = 0;

   // SRU_A only has access to one bank of DIP switches.
   if ( enSRU_DIP_Bank == enSRU_DIP_BANK_B )
   {
      // The DIP switches come in on the lower 8 bits of port 4. The port pin
      // values must be inverted so that a switch in the on position reports a
      // value of 1 in the corresponding bit.
      //ucValue = (~Sys_MCU_Port_In( 4 )) & 0x0ff;

      ucValue  = Chip_GPIO_GetPinState(LPC_GPIO,0,6) ;
      ucValue |= Chip_GPIO_GetPinState(LPC_GPIO,0,7) << 1;
      ucValue |= Chip_GPIO_GetPinState(LPC_GPIO,0,8) << 2;
      ucValue |= Chip_GPIO_GetPinState(LPC_GPIO,0,9)  << 3;
      ucValue |= Chip_GPIO_GetPinState(LPC_GPIO,2,0)  << 4;
      ucValue |= Chip_GPIO_GetPinState(LPC_GPIO,2,1)  << 5;
      ucValue |= Chip_GPIO_GetPinState(LPC_GPIO,2,2)  << 6;
      ucValue |= Chip_GPIO_GetPinState(LPC_GPIO,2,3)  << 7;
      ucValue = (~ucValue) & 0x0ff;
   }

   return ucValue;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
