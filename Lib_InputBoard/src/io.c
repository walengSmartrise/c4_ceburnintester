/******************************************************************************
 *
 * @file     io.c
 * @brief    I/O support for MCU A on SRU Base board
 * @version  V1.00
 * @date     21, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include <mcu_inputBoard.h>
#include <mcu_inputBoard_private.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static const PortAndPin PortAndPin_InputsID[SRU_NUM_ID_PINS] =
{
   {0,  18 },
   {0,  17 },
   {0,  11 },
   {0,  10 },
};

static const struct st_port_pin_pair gastPortPinPairs_Inputs[ SRU_NUM_INPUTS ] =
{
   {1, 0},   // enSRU_Input_501
   {1, 1},   // enSRU_Input_502
   {1, 4},   // enSRU_Input_503
   {1, 8},   // enSRU_Input_504
   {1, 9},   // enSRU_Input_505
   {1, 10},  // enSRU_Input_506
   {1, 14},  // enSRU_Input_507
   {1, 15},  // enSRU_Input_508
   {0, 3},   // enSRU_Input_509
   {0, 2},   // enSRU_Input_510
   {4, 0},   // enSRU_Input_511
   {0, 14},  // enSRU_Input_512
   {3, 24},  // enSRU_Input_513
   {3, 25},  // enSRU_Input_514
   {3, 23},  // enSRU_Input_515
   {1, 19},  // enSRU_Input_516
   {1, 18},  // enSRU_Input_517
   {3, 26},  // enSRU_Input_518
   {1, 20},  // enSRU_Input_519
   {1, 22},  // enSRU_Input_520
   {1, 30},  // enSRU_Input_521
   {2, 5},   // enSRU_Input_522
   {2, 12},  // enSRU_Input_523
   {4, 13},  // enSRU_Input_524

   {4, 25},  //DRVRFLT
};

static const struct st_port_pin_pair gastPortPinPairs_Outputs[ SRU_NUM_OUTPUTS ] =
{
   {0, 29},   // enSRU_Output_601
   {0, 30},   // enSRU_Output_602
   {0, 31},   // enSRU_Output_603
   {0, 27},   // enSRU_Output_604
   {1, 23},   // enSRU_Output_605
   {1, 24},  // enSRU_Output_606
   {1, 25},  // enSRU_Output_607
   {1, 26},  // enSRU_Output_608
   {4, 12},   // enSRU_Output_609
   {4, 11},   // enSRU_Output_610
   {2, 7},   // enSRU_Output_611
   {4, 10},  // enSRU_Output_612
   {2, 8},  // enSRU_Output_613
   {4, 9},  // enSRU_Output_614
   {4, 8},  // enSRU_Output_615
   {0, 19},  // enSRU_Output_616
   {4, 7},  // enSRU_Output_617
   {0, 20},  // enSRU_Output_618
   {4, 6},  // enSRU_Output_619
   {4, 5},  // enSRU_Output_620
   {3, 2},  // enSRU_Output_621
   {3, 1},   // enSRU_Output_622
   {3, 0},  // enSRU_Output_623
   {4, 31},  // enSRU_Output_624

   {1, 16}, //enSRU_Output_OE
};

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Returns 4 bit hardcoded MCU ID
-----------------------------------------------------------------------------*/
uint8_t SRU_Read_MCU_ID()
{
  uint8_t ucValue = 0;
  for(uint8_t i = 0; i < SRU_NUM_ID_PINS; i++)
  {
     uint8_t uiPort = PortAndPin_InputsID[i].port;
     uint8_t uiPin  = PortAndPin_InputsID[i].pin;
     uint8_t bValue = Chip_GPIO_GetPinState(LPC_GPIO, uiPort, uiPin);
     ucValue |= (bValue << i);
  }
  return ucValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_Input( enum en_sru_inputs enSRU_Input )
{
   uint8_t bValue = 0;

   if ( enSRU_Input < SRU_NUM_INPUTS )
   {
      uint32_t uiPort = gastPortPinPairs_Inputs[ enSRU_Input ].ucPort;
      uint32_t uiPin = gastPortPinPairs_Inputs[ enSRU_Input ].ucPin;

      // Invert the value returned by Sys_MCU_Pin_In() because when an input is
      // powered, the port pin is low and vice versa.
      bValue = !Sys_MCU_Pin_In( uiPort, uiPin );
   }

   return bValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SRU_Write_Output( enum en_sru_outputs enSRU_Output, uint32_t bValue )
{
   if ( enSRU_Output < SRU_NUM_OUTPUTS )
   {
      uint32_t uiPort = gastPortPinPairs_Outputs[ enSRU_Output ].ucPort;
      uint32_t uiPin = gastPortPinPairs_Outputs[ enSRU_Output ].ucPin;

      Sys_MCU_Pin_Out( uiPort, uiPin, bValue );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void IO_Init( void )
{
   for(uint8_t i = 0; i < SRU_NUM_OUTPUTS; i++)
   {
      Chip_GPIO_SetDir(LPC_GPIO, gastPortPinPairs_Outputs[i].ucPort, gastPortPinPairs_Outputs[i].ucPin,true);
   }
   int i;

   // Turn off all outputs.
   for ( i = 0; i < SRU_NUM_OUTPUTS; ++i )
   {
      SRU_Write_Output( i, 0 );
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
