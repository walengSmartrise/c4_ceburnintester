#include "board.h"
/* The System initialization code is called prior to the application and
   initializes the board for run-time operation. Board initialization
   includes clock setup and default pin muxing configuration. */

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/* Pin muxing configuration */
STATIC const PINMUX_GRP_T pinmuxing[] = {

};

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
/* Sets up system pin muxing */
void Board_SetupMuxing(void)
{
   // Reset the IOCON register
   Chip_SYSCTL_PeriphReset(SYSCTL_RESET_IOCON);

   /* Setup system level pin muxing */
   Chip_IOCON_SetPinMuxing(LPC_IOCON, pinmuxing, sizeof(pinmuxing) / sizeof(PINMUX_GRP_T));
}

/* Setup system clocking */
void Board_SetupClocking(void)
{
   // Use the internal system clock
   //Chip_SetupIrcClocking();
   Chip_SetupXtalClocking();
}


/* Set up and initialize hardware prior to call to main */
void Board_SystemInit(void)
{

   Board_SetupMuxing();
   Board_SetupClocking();
}

