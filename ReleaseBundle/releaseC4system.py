
import os
import shutil
import zipfile
import glob
import subprocess

# Boolean flag to drive generating downloadable files for C4-GUI
bGenerateRDLbundles = True

# Define boolean flags for temporary on/off components
bIncludeBrake = False
bIncludeDDM   = False

# For each processor to download, define the following
#   'appname'   used for the folder name and binary filenames (.bin/.srec)
#   'build'     used to decide debug vs. release, also for the subdir
#   'rdl'       to convert binary for remote download (.sbf for C4-GUI)
#   'gitfolder' optional if binaries at different location

dict_key_appname = 'appname'
dict_key_build   = 'build'
dict_key_gitdir  = 'gitfolder'

dictBootloader = { dict_key_appname : 'Bootloader', dict_key_build : 'Debug'   }
dictMRA        = { dict_key_appname : 'App_MRA',    dict_key_build : 'Release' }
dictMRB        = { dict_key_appname : 'App_MRB',    dict_key_build : 'Release' }
dictSRUA       = { dict_key_appname : 'App_SRU_A',  dict_key_build : 'Release' }
dictSRUB       = { dict_key_appname : 'App_SRU_B',  dict_key_build : 'Release' }
dictRiser      = { dict_key_appname : 'App_MCU_E',  dict_key_build : 'Release' }

# Optional, DDM images, if bIncludeDDM
dictInputBoard = { dict_key_appname : 'App_InputBoard', dict_key_build : 'Debug'   }
dictDDMA       = { dict_key_appname : 'App_DDM_A',      dict_key_build : 'Release' }
dictDDMB       = { dict_key_appname : 'App_DDM_B',      dict_key_build : 'Release' }

# Optional, Brake images, if bIncludeBrake
dictBrakeBoard = { 
	               dict_key_appname : 'BrakeBoard', 
				   dict_key_build   : 'Release', 
				   dict_key_gitdir  : r'c:\git\c4_brakeboard',
				 }

# Master list of all applications
listApplications = [ 
	                 dictBootloader,
					 dictMRA,  dictMRB,
					 dictSRUA, dictSRUB,
					 dictRiser,
				   ]

# List of lists, all apps in the same list will be bundled in the same .sbf file
if bGenerateRDLbundles:
	listRDLbundle = [ 
					  [ dictMRA, dictMRB, dictSRUA, dictSRUB ],
					  [ dictRiser ],
					]

# Add DDM binaries to Master list, only if bIncludeDDM is set
if bIncludeDDM:

	listApplications +=  [ dictInputBoard, dictDDMA, dictDDMB ]
	if bGenerateRDLbundles:
		listRDLbundle    += [[ dictInputBoard, dictDDMA, dictDDMB ]]

# Add Brake Board binaries to Master list, only if bIncludeBrake is set
if bIncludeBrake:
	listApplications += [ dictBrakeBoard ]

c4_system_version_h = r'Lib_System\inc\version.h'

# All additional files to complement the release
listExtraFiles = [ 
	               r'CPLD_Software\*.pof',
				   r'Lib_System\Support\defaults.txt',
				   r'Lib_System\Support\IO_Definitions_ReadOnly.xlsx',
				   r'Lib_System\Support\SystemParameters.xlsx',
				   r'Lib_System\Support\Alarms\SystemAlarms.xlsx',
				   r'Lib_System\Support\Faults\SystemFaults.xlsx',
				   c4_system_version_h,
				 ]

# General list of file types to be released
listWildcardExtensions = [ '.axf', '.bin', '.srec', '.sbf', '.pof',
					       '.xlsx', '.h',  '.txt', ]

# List of files matching listWildcardExtensions but not to be included 
listExceptions = [ 
				   'release_notes.txt',
				 ]

# ------------------------------------------------------------------------
# Removes older files from the working directory

def cleanup_directory( lstExtensions, listExceptions, working_dst_directory = '.' ):
	for wildcard_name in listWildcardExtensions:
		for each_file in glob.glob( working_dst_directory + r'\*' + wildcard_name ):

			## skip files in the exception list
			if os.path.basename(each_file) in listExceptions:
				continue

			try:
				os.remove( each_file )
			except:
				print( 'Error while deleting ', each_file )

# ------------------------------------------------------------------------
# Pulls the version number from version.h file 

def fGetVersionNumber( filepath_version_h ):
    
    ret_version = 'x.y.z'
    versionFile = open( filepath_version_h, 'r' )

    for next_line in versionFile:
        if 'Vers. ' in next_line:
            ret_version = next_line.replace( '#define SOFTWARE_RELEASE_VERSION (\"Vers. ', '' )
            ret_version = ret_version.replace( '.', '_' )
            for word in ( ' ', '\"', ')', '\n' ):
                ret_version = ret_version.replace(word, '' )
            break

    return ret_version

# ------------------------------------------------------------------------
# Define release notes name based on the software version

def fReleaseNotesFileName( version_string, target_dir = '.' ):
   return ( os.path.join( target_dir, 'release_notes_' + version_string + '.txt' ) )

# ------------------------------------------------------------------------
# Define output file name based on the software version

def fBundleFilename( version_string, target_dir = '.' ):
   return ( os.path.join( target_dir, 'ReleaseV' + version_string + '.zip' ) )

# ------------------------------------------------------------------------
# Build a list of files to bundle in the release

def get_list_of_all_files( lstExtensions, listExceptions, working_src_directory = '.' ):

    # initializing empty file paths list
    ret_lst_of_files = []

    # crawling through directory and subdirectories
    for root, directories, all_files in os.walk( working_src_directory ):
        for each_file in all_files:

            ## skip files in the exception list
            if os.path.basename(each_file) in listExceptions:
                continue

            # If the file extension is on the list # add the full path to the list
            if any( each_file.endswith( file_ext ) for file_ext in lstExtensions ):
                ret_lst_of_files.append( os.path.join( root, each_file ) )

    # returning all file paths
    return ret_lst_of_files

# ------------------------------------------------------------------------
# - 
# ------------------------------------------------------------------------
# - 
# ------------------------------------------------------------------------

if __name__ == '__main__':
	print ( 'Running from ', os.getcwd() )

	bEverythingOK = True

	# It points to the <GIT>\c4_system root directory
	# This value should not change since it's part of the c4_system project
	gRootGIT_c4_system = '..'

	path_sbf_tool = os.path.join( gRootGIT_c4_system, r'Bootloader\scripts\ToSBF.py' )

	target_release_dir = '.'

	cleanup_directory( listWildcardExtensions, listExceptions, target_release_dir )

	t_version_string = fGetVersionNumber( os.path.join(gRootGIT_c4_system,c4_system_version_h ) )
	print( 'Preparing bundle release for version', t_version_string )

	for app_profile in listApplications:
		if dict_key_gitdir in app_profile.keys():
			app_dir = app_profile[ dict_key_gitdir ]
		else:
			app_dir = gRootGIT_c4_system

		app_filename = os.path.join( app_dir,
									 app_profile[dict_key_appname],
							         app_profile[dict_key_build],
									 app_profile[dict_key_appname] )
		
		for each_ext in [ '.bin', '.srec' ]:
			app_binary_file = app_filename + each_ext
			if os.path.exists( app_binary_file ):
				shutil.copy( app_binary_file, target_release_dir )
			else:
				print( 'WARNING: missing', app_binary_file )
				bEverythingOK = False

	if bEverythingOK:
		for extra_file in listExtraFiles:
			# Use different APIs if copying files via wildcard
			if '*' not in extra_file:
				if os.path.exists( os.path.join( gRootGIT_c4_system, extra_file ) ):
					shutil.copy( os.path.join( gRootGIT_c4_system, extra_file ), target_release_dir )
				else:
					print( 'WARNING: missing', extra_file )
					bEverythingOK = False
			else:
				for each_file in glob.glob( os.path.join( gRootGIT_c4_system, extra_file ) ):
					if os.path.exists( each_file ):
						shutil.copy( each_file, target_release_dir )
					else:
						print( 'WARNING: missing', each_file )
						bEverythingOK = False

	if bEverythingOK and bGenerateRDLbundles:
		for each_rdl_bundle in listRDLbundle:
			list_srec_files = '.srec '.join(  app[dict_key_appname] for app in each_rdl_bundle ) + '.srec '
			print( 'Bundle', list_srec_files )
			subprocess.getoutput( ' '.join( [ 'python', path_sbf_tool, list_srec_files ] ) )

	if bEverythingOK:
		if os.path.exists( 'release_notes.txt' ):
			tReleaseNotes = fReleaseNotesFileName( t_version_string )
			shutil.copy( 'release_notes.txt', tReleaseNotes )

	if bEverythingOK:
		# Get a list of all files to bundle
		lstFiles2Release = get_list_of_all_files( listWildcardExtensions, listExceptions, target_release_dir )

		# Bundle all files, adding files one by one
		zf = zipfile.ZipFile( fBundleFilename( t_version_string ), 'w', zipfile.ZIP_DEFLATED )
		for each_file in lstFiles2Release:
			zf.write( each_file )

		zf.close()
		print('All files zipped successfully!')

	if not bEverythingOK:
		cleanup_directory( listWildcardExtensions, listExceptions, target_release_dir )
