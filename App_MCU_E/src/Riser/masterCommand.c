
/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief
 * @version  V1.00
 * @date     27, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>

#include "sru_e.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "masterCommand.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UnloadDatagram_MasterCommand( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   en_master_cmd eCMD = punDatagram->auc8[0];
   uint8_t ucLanding = punDatagram->auc8[1];
   en_hc_dir eDir = punDatagram->auc8[2];
   uint32_t uiMask = punDatagram->aui32[1];
   if( ( ucLanding < MAX_NUM_FLOORS )
    && ( eDir <= HC_DIR__UP ) )
   {
      if( eCMD == MASTER_CMD__SetHallCall )
      {
         SetRawLatchedHallCalls(ucLanding, eDir, uiMask);
      }
      else if( eCMD == MASTER_CMD__ClrHallCall )
      {
         ClrRawLatchedHallCalls(ucLanding, eDir, uiMask);
      }
   }
}


/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
