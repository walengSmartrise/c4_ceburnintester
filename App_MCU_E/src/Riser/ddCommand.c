/******************************************************************************
 *
 * @file     ddcommand.c
 * @brief
 * @version  V1.00
 * @date     13, June 2018
 * @author   Keith Soneda
 *
 * @note     Manages commands used for CC/HC assignments to cars.
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>

#include "sru_e.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "ddCommand.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
      punDatagram->auc8[0] = astHCRequest[ucIndex].ucCarID;
      punDatagram->auc8[1] = astHCRequest[ucIndex].ucLanding;
      punDatagram->auc8[2] = astHCRequest[ucIndex].eDir;
      punDatagram->auc8[3] = astHCRequest[ucIndex].bADA;
      if(astHCRequest[ucIndex].bRear)
      {
         punDatagram->aui32[1] = CarData_GetUniqueHallMask_Rear(astHCRequest[ucIndex].ucCarID);
      }
      else
      {
         punDatagram->aui32[1] = CarData_GetUniqueHallMask_Front(astHCRequest[ucIndex].ucCarID);
      }
 *----------------------------------------------------------------------------*/
void DDC_UnloadDatagram_LatchHC(un_sdata_datagram *punDatagram)
{
   uint8_t ucCarID = punDatagram->auc8[0];
   uint8_t ucLanding = punDatagram->auc8[1];
   en_hc_dir eDir = punDatagram->auc8[2];
//   uint8_t bADA = punDatagram->auc8[3];
   uint32_t uiMask = punDatagram->aui32[1];

   if( ( ucCarID < MAX_GROUP_CARS )
    && ( ucLanding < MAX_NUM_FLOORS ) )
   {
      SetRawLatchedHallCalls(ucLanding, eDir, uiMask);
   }
}


/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
