/******************************************************************************
 * @author   Keith Soneda
 * @version  V1.00
 * @date     1, August 2016
 *
 * @note    Monitors hall boards & their data
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "sru_e.h"
#include "sru.h"
#include "sys.h"
#include <string.h>
#include "carData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_HallMonitor =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define HALLBOARD_INACTIVE_OFFLINE_COUNT        (255)
#define TIMEOUT_HALLBOARD_OFFLINE_MS            (20000)
#define MIN_RESEND_RATE_LATCHED_CALLS_5MS       (100)
#define MIN_RESEND_RATE_CLEARED_CALLS_5MS       (2000)
#define HALLBOARD_STATUS_MIN_RESEND_RATE_MS     (5000)
#define HALLBOARD_STATUS_REQUEST_RESEND_RATE_MS (250)

#define MIN_RESEND_RATE_SECURITY_CONTACTS_5MS   (100)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
/* Defines the limits for interpreting aucOfflineCounter values as NUM_HB_COM_STATE */
static const uint16_t aucComStatesCountLimits[NUM_HB_COM_STATE] =
{
(HALLBOARD_INACTIVE_OFFLINE_COUNT),//HB_COM_STATE__INACTIVE,
(TIMEOUT_HALLBOARD_OFFLINE_MS/MOD_RUN_PERIOD_HALL_MONITOR_1MS),//HB_COM_STATE__0,
(0.9*TIMEOUT_HALLBOARD_OFFLINE_MS/MOD_RUN_PERIOD_HALL_MONITOR_1MS),//HB_COM_STATE__25,
(0.8*TIMEOUT_HALLBOARD_OFFLINE_MS/MOD_RUN_PERIOD_HALL_MONITOR_1MS),//HB_COM_STATE__50,
(0.7*TIMEOUT_HALLBOARD_OFFLINE_MS/MOD_RUN_PERIOD_HALL_MONITOR_1MS),//HB_COM_STATE__75,
(0.6*TIMEOUT_HALLBOARD_OFFLINE_MS/MOD_RUN_PERIOD_HALL_MONITOR_1MS),//HB_COM_STATE__100,
};
static st_hallboard_data stHallBoardData;
static st_hallcall_data stHallCallData;

static uint16_t auwOfflineCounter_ms[MAX_NUM_RISER_BOARDS];

/* Status bitmap for hall security contacts. These arrays are used by RISER3 only.
 * The front contacts are the UB inputs, the rear contacts are the DB inputs */
static uint32_t auiSecurityContacts_F[BITMAP32_SIZE(NEW_HALL_BOARD_MAX_FLOOR_LIMIT)];
static uint32_t auiSecurityContacts_R[BITMAP32_SIZE(NEW_HALL_BOARD_MAX_FLOOR_LIMIT)];
static uint8_t ucSecurityContactDirtyBits_F;
static uint8_t ucSecurityContactDirtyBits_R;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Returns 1 if either call button on an EMS medical recall board is actively ON.
 -----------------------------------------------------------------------------*/
uint8_t HallBoard_CheckIfMedicalRecallHeldActive( uint8_t ucLanding, uint8_t ucMedicalMask )
{
   uint8_t bActive = 0;

   for(uint8_t ucRiserIndex = 0; ucRiserIndex < NUM_BITS_PER_BITMAP8; ucRiserIndex++)
   {
      if( Sys_Bit_Get(&ucMedicalMask, ucRiserIndex) )
      {
         uint16_t uwDIP = ucRiserIndex*GetHallBoard_MaxNumberOfFloors() + ucLanding;
         uint8_t bActiveButton_Up = ( stHallBoardData.stBoardStatuses[uwDIP].ucBF_RawIO >> HB_IO_SHIFT__BTN_UP ) & 1;
         uint8_t bActiveButton_Down = ( stHallBoardData.stBoardStatuses[uwDIP].ucBF_RawIO >> HB_IO_SHIFT__BTN_DN ) & 1;
         if( bActiveButton_Up || bActiveButton_Down )
         {
            bActive = 1;
            break;
         }
      }
   }

   return bActive;
}
/*-----------------------------------------------------------------------------
   Riser online tracking functions
 -----------------------------------------------------------------------------*/
void RiserBoard_UpdateOfflineCounters( uint16_t uwRunPeriod_ms )
{
   for(uint8_t i = 0; i < MAX_NUM_RISER_BOARDS; i++)
   {
      if( i != GetSRU_RiserBoardIndex() )
      {
         if( auwOfflineCounter_ms[i] < RISER_OFFLINE_COUNT_MS__FAULTED )
         {
            auwOfflineCounter_ms[i] += uwRunPeriod_ms;

            /* Clear latched calls from other riser boards if communication is lost */
            if(!RiserBoard_GetOnlineFlag(i))
            {
               for(uint8_t j = 0; j < GetHallBoard_MaxNumberOfFloors(); j++)
               {
                  stHallCallData.auiBF_HallCalls_Up[i][j] = 0;
                  stHallCallData.auiBF_HallCalls_Down[i][j] = 0;
               }
            }
         }
      }
   }
}
void RiserBoard_ClearOfflineCounter(uint8_t ucRiserIndex)
{
   auwOfflineCounter_ms[ucRiserIndex] = 0;
}
uint8_t RiserBoard_GetOnlineFlag(uint8_t ucRiserIndex)
{
   return ( auwOfflineCounter_ms[ucRiserIndex] < RISER_OFFLINE_COUNT_MS__FAULTED );
}
/*-----------------------------------------------------------------------------
   ucMapIndex is the index into 32bit blocks of bitflags
 -----------------------------------------------------------------------------*/
uint32_t GetActiveHallLamps( uint8_t ucMapIndex, en_hc_dir bUpDirection )
{
   uint32_t uiHallLamps = 0;
   if( ucMapIndex < BITMAP32_SIZE(GetHallBoard_MaxNumberOfBoards()) )
   {
      if(bUpDirection)
      {
         uiHallLamps = stHallBoardData.auiBF_ActiveLamp_Up[ucMapIndex];
      }
      else
      {
         uiHallLamps = stHallBoardData.auiBF_ActiveLamp_Down[ucMapIndex];
      }
   }
   return uiHallLamps;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint32_t GetLatchedCalls( uint8_t ucLandingIndex , en_hc_dir bUpDirection )
{
   uint32_t uiHallMap;
   if ( bUpDirection )
   {
      uiHallMap = stHallCallData.auiBF_HallCalls_Up[GetSRU_RiserBoardIndex()][ucLandingIndex];
   }
   else
   {
      uiHallMap = stHallCallData.auiBF_HallCalls_Down[GetSRU_RiserBoardIndex()][ucLandingIndex];
   }
   return uiHallMap;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetRawLatchedHallCalls ( uint8_t ucLandingIndex, en_hc_dir bUpCall, uint32_t uiVirtualRiserMask)
{
   //This is the original mask used to check if its changed since the new mask was added
   uint32_t uiMasked;
   if (bUpCall)
   {
      uiMasked = stHallCallData.auiBF_HallCalls_Up[GetSRU_RiserBoardIndex()][ucLandingIndex];
      stHallCallData.auiBF_HallCalls_Up[GetSRU_RiserBoardIndex()][ucLandingIndex] |= uiVirtualRiserMask;
      if ( stHallCallData.auiBF_HallCalls_Up[GetSRU_RiserBoardIndex()][ucLandingIndex] != uiMasked )
      {
         Sys_Bit_Set(&stHallCallData.aucBF_DirtyBits_Up[0], ucLandingIndex, 1);
      }
   }
   else
   {
      uiMasked = stHallCallData.auiBF_HallCalls_Down[GetSRU_RiserBoardIndex()][ucLandingIndex];
      stHallCallData.auiBF_HallCalls_Down[GetSRU_RiserBoardIndex()][ucLandingIndex] |= uiVirtualRiserMask;
      if ( stHallCallData.auiBF_HallCalls_Down[GetSRU_RiserBoardIndex()][ucLandingIndex] != uiMasked )
      {
         Sys_Bit_Set(&stHallCallData.aucBF_DirtyBits_Down[0], ucLandingIndex, 1);
      }
   }
}
void ClrRawLatchedHallCalls( uint8_t ucLandingIndex, en_hc_dir bUpCall, uint32_t uiVirtualRiserMask)
{
   uint32_t uiMasked;
   if (bUpCall)
   {
      uiMasked = stHallCallData.auiBF_HallCalls_Up[GetSRU_RiserBoardIndex()][ucLandingIndex] & ~uiVirtualRiserMask;
      if ( stHallCallData.auiBF_HallCalls_Up[GetSRU_RiserBoardIndex()][ucLandingIndex] != uiMasked )
      {
         stHallCallData.auiBF_HallCalls_Up[GetSRU_RiserBoardIndex()][ucLandingIndex] = uiMasked;
         Sys_Bit_Set(&stHallCallData.aucBF_DirtyBits_Up[0], ucLandingIndex, 1);
      }
      for(uint8_t i = 0; i < MAX_NUM_RISER_BOARDS; i++)
      {
         if(i != GetSRU_RiserBoardIndex())
         {
            stHallCallData.auiBF_HallCalls_Up[i][ucLandingIndex] = stHallCallData.auiBF_HallCalls_Up[i][ucLandingIndex] & ~uiVirtualRiserMask;
         }
      }
   }
   else
   {
      uiMasked = stHallCallData.auiBF_HallCalls_Down[GetSRU_RiserBoardIndex()][ucLandingIndex] & ~uiVirtualRiserMask;
      if ( stHallCallData.auiBF_HallCalls_Down[GetSRU_RiserBoardIndex()][ucLandingIndex] != uiMasked )
      {
         stHallCallData.auiBF_HallCalls_Down[GetSRU_RiserBoardIndex()][ucLandingIndex] = uiMasked;
         Sys_Bit_Set(&stHallCallData.aucBF_DirtyBits_Down[0], ucLandingIndex, 1);
      }
      for(uint8_t i = 0; i < MAX_NUM_RISER_BOARDS; i++)
      {
         if(i != GetSRU_RiserBoardIndex())
         {
            stHallCallData.auiBF_HallCalls_Down[i][ucLandingIndex] = stHallCallData.auiBF_HallCalls_Down[i][ucLandingIndex] & ~uiVirtualRiserMask;
         }
      }
   }
}

/*-----------------------------------------------------------------------------
   The security riser (GROUP_NET__RIS_3) sends the status of security contacts
   over the latch DG_GroupNet_RIS__LATCHED_HALL_CALLS_UP and DG_GroupNet_RIS__LATCHED_HALL_CALLS_DN
   datagrams.
   Front contacts are sent as UP calls.
   Rear contacts are sent as DN calls.
 -----------------------------------------------------------------------------*/
void LoadDatagram_LatchedCalls_Up_SecurityContactsF(void)
{
   static uint8_t ucLastIndex;
   static uint16_t auwMinResendTimer_5ms[BITMAP32_SIZE(NEW_HALL_BOARD_MAX_FLOOR_LIMIT)];
   uint8_t ucIndexToSend_Plus1 = 0;

   uint16_t uwResendLimit = MIN_RESEND_RATE_SECURITY_CONTACTS_5MS;
   if(CarData_GetFastResend())
   {
      uwResendLimit /= 2;
   }

   /* Check if a set of contacts has not been updated recently, if so mark it for sending */
   for(uint8_t i = 0; i < BITMAP32_SIZE(NEW_HALL_BOARD_MAX_FLOOR_LIMIT); i++)
   {
      if( auwMinResendTimer_5ms[i] >= uwResendLimit )
      {
         Sys_Bit_Set(&ucSecurityContactDirtyBits_F, i, 1);
      }
      else
      {
         auwMinResendTimer_5ms[i]++;
      }
   }

   if ( !SDATA_DirtyBit_Get( gpstSData_GroupNet_RIS, DG_GroupNet_RIS__LATCHED_HALL_CALLS_UP) )
   {
      for(uint8_t i = 0; i < BITMAP32_SIZE(NEW_HALL_BOARD_MAX_FLOOR_LIMIT); i++)
      {
         uint8_t ucIndex = (ucLastIndex+i+1) % BITMAP32_SIZE(NEW_HALL_BOARD_MAX_FLOOR_LIMIT);
         if( Sys_Bit_Get(&ucSecurityContactDirtyBits_F, ucIndex) )
         {
            ucIndexToSend_Plus1 = ucIndex+1;
            break;
         }
      }

      if( ucIndexToSend_Plus1 )
      {
         un_sdata_datagram unDatagram;
         ucLastIndex = ucIndexToSend_Plus1-1;
         unDatagram.aui32[0] = 0;
         unDatagram.auc8[0] = ucLastIndex;
         unDatagram.aui32[1] = auiSecurityContacts_F[ucLastIndex];
         SDATA_WriteDatagram( gpstSData_GroupNet_RIS,
                              DG_GroupNet_RIS__LATCHED_HALL_CALLS_UP,
                              &unDatagram
                             );
         SDATA_DirtyBit_Set(gpstSData_GroupNet_RIS, DG_GroupNet_RIS__LATCHED_HALL_CALLS_UP);
         Sys_Bit_Set(&ucSecurityContactDirtyBits_F, ucLastIndex, 0);
         auwMinResendTimer_5ms[ucLastIndex] = 0;
      }
   }
}
void LoadDatagram_LatchedCalls_Down_SecurityContactsR(void)
{
   static uint8_t ucLastIndex;
   static uint16_t auwMinResendTimer_5ms[BITMAP32_SIZE(NEW_HALL_BOARD_MAX_FLOOR_LIMIT)];
   uint8_t ucIndexToSend_Plus1 = 0;

   uint16_t uwResendLimit = MIN_RESEND_RATE_SECURITY_CONTACTS_5MS;
   if(CarData_GetFastResend())
   {
      uwResendLimit /= 2;
   }

   /* Check if a set of contacts has not been updated recently, if so mark it for sending */
   for(uint8_t i = 0; i < BITMAP32_SIZE(NEW_HALL_BOARD_MAX_FLOOR_LIMIT); i++)
   {
      if( auwMinResendTimer_5ms[i] >= uwResendLimit )
      {
         Sys_Bit_Set(&ucSecurityContactDirtyBits_R, i, 1);
      }
      else
      {
         auwMinResendTimer_5ms[i]++;
      }
   }

   if ( !SDATA_DirtyBit_Get( gpstSData_GroupNet_RIS, DG_GroupNet_RIS__LATCHED_HALL_CALLS_DN) )
   {
      for(uint8_t i = 0; i < BITMAP32_SIZE(NEW_HALL_BOARD_MAX_FLOOR_LIMIT); i++)
      {
         uint8_t ucIndex = (ucLastIndex+i+1) % BITMAP32_SIZE(NEW_HALL_BOARD_MAX_FLOOR_LIMIT);
         if( Sys_Bit_Get(&ucSecurityContactDirtyBits_R, ucIndex) )
         {
            ucIndexToSend_Plus1 = ucIndex+1;
            break;
         }
      }

      if( ucIndexToSend_Plus1 )
      {
         un_sdata_datagram unDatagram;
         ucLastIndex = ucIndexToSend_Plus1-1;
         unDatagram.aui32[0] = 0;
         unDatagram.auc8[0] = ucLastIndex;
         unDatagram.aui32[1] = auiSecurityContacts_R[ucLastIndex];
         SDATA_WriteDatagram( gpstSData_GroupNet_RIS,
                              DG_GroupNet_RIS__LATCHED_HALL_CALLS_DN,
                              &unDatagram
                             );
         SDATA_DirtyBit_Set(gpstSData_GroupNet_RIS, DG_GroupNet_RIS__LATCHED_HALL_CALLS_DN);
         Sys_Bit_Set(&ucSecurityContactDirtyBits_R, ucLastIndex, 0);
         auwMinResendTimer_5ms[ucLastIndex] = 0;
      }
   }
}
/*-----------------------------------------------------------------------------
   Selects new per floor hall calls to transmit on group net

 -----------------------------------------------------------------------------*/
void LoadDatagram_LatchedCalls_Up()
{
   static uint8_t ucLastLanding;
   static uint16_t auwMinResendTimer_5ms[HALL_BOARDS_MAX_NUM_FLOORS];
   uint8_t ucLandingToSend_Plus1 = 0;

   uint32_t uiResendLimit = MIN_RESEND_RATE_LATCHED_CALLS_5MS;
   if(CarData_GetFastResend())
   {
      uiResendLimit /= 2;
   }

   /* Check if a floor has not been updated recently, if so mark it for sending */
   uint8_t ucHighestLanding = CarData_GetHighestActiveLanding();
   for( uint8_t i = 0; i <= ucHighestLanding; i++ )
   {
      /* If call is latched, send it more frequently than if it is not */
      if( ( GetLatchedCalls(i, HC_DIR__UP) )
       && ( auwMinResendTimer_5ms[i] > uiResendLimit ) )
      {
         Sys_Bit_Set(&stHallCallData.aucBF_DirtyBits_Up[0], i, 1);
      }
      else if( auwMinResendTimer_5ms[i] > MIN_RESEND_RATE_CLEARED_CALLS_5MS )
      {
         Sys_Bit_Set(&stHallCallData.aucBF_DirtyBits_Up[0], i, 1);
      }
      else
      {
         auwMinResendTimer_5ms[i]++;
      }
   }

   if ( !SDATA_DirtyBit_Get( gpstSData_GroupNet_RIS, DG_GroupNet_RIS__LATCHED_HALL_CALLS_UP) )
   {
      for( uint8_t i = 0; i <= ucHighestLanding; i++ )
      {
         uint8_t ucLanding = ( ucLastLanding+i+1 ) % (ucHighestLanding+1);
         /* Round robin packtes that need to be sent and select the first one */
         if( Sys_Bit_Get(&stHallCallData.aucBF_DirtyBits_Up[0], ucLanding) )
         {
            ucLandingToSend_Plus1 = ucLanding+1;
            break;
         }
      }

      /* If new floor to send was found */
      if( ucLandingToSend_Plus1 )
      {
         un_sdata_datagram unDatagram;
         ucLastLanding = ucLandingToSend_Plus1-1;
         unDatagram.aui32[0] = 0;
         unDatagram.auc8[0] = ucLastLanding;
         unDatagram.aui32[1] = GetLatchedCalls( ucLastLanding, HC_DIR__UP );
         SDATA_WriteDatagram( gpstSData_GroupNet_RIS,
                              DG_GroupNet_RIS__LATCHED_HALL_CALLS_UP,
                              &unDatagram
                             );
         SDATA_DirtyBit_Set(gpstSData_GroupNet_RIS, DG_GroupNet_RIS__LATCHED_HALL_CALLS_UP);
         Sys_Bit_Set(&stHallCallData.aucBF_DirtyBits_Up[0], ucLastLanding, 0);
         auwMinResendTimer_5ms[ucLastLanding] = 0;
      }
   }
}
void LoadDatagram_LatchedCalls_Down()
{
   static uint8_t ucLastLanding;
   static uint16_t auwMinResendTimer_5ms[HALL_BOARDS_MAX_NUM_FLOORS];
   uint8_t ucLandingToSend_Plus1 = 0;

   uint32_t uiResendLimit = MIN_RESEND_RATE_LATCHED_CALLS_5MS;
   if(CarData_GetFastResend())
   {
      uiResendLimit /= 2;
   }

   /* Check if a floor has not been updated recently, if so mark it for sending */
   uint8_t ucHighestLanding = CarData_GetHighestActiveLanding();
   for( uint8_t i = 0; i <= ucHighestLanding; i++ )
   {
      /* If call is latched, send it more frequently than if it is not */
      if( ( GetLatchedCalls(i, HC_DIR__DOWN) )
       && ( auwMinResendTimer_5ms[i] > uiResendLimit ) )
      {
         Sys_Bit_Set(&stHallCallData.aucBF_DirtyBits_Down[0], i, 1);
      }
      else if( auwMinResendTimer_5ms[i] > MIN_RESEND_RATE_CLEARED_CALLS_5MS )
      {
         Sys_Bit_Set(&stHallCallData.aucBF_DirtyBits_Down[0], i, 1);
      }
      else
      {
         auwMinResendTimer_5ms[i]++;
      }
   }

   if ( !SDATA_DirtyBit_Get( gpstSData_GroupNet_RIS, DG_GroupNet_RIS__LATCHED_HALL_CALLS_DN) )
   {
      /* Round robin packtes that need to be sent and select the first one */
      for( uint8_t i = 0; i <= ucHighestLanding; i++ )
      {
         uint8_t ucLanding = ( ucLastLanding+i+1 ) % (ucHighestLanding+1);
         if( Sys_Bit_Get(&stHallCallData.aucBF_DirtyBits_Down[0], ucLanding) )
         {
            ucLandingToSend_Plus1 = ucLanding+1;
            break;
         }
      }

      /* If new floor to send was found */
      if( ucLandingToSend_Plus1 )
      {
         un_sdata_datagram unDatagram;
         ucLastLanding = ucLandingToSend_Plus1-1;
         unDatagram.aui32[0] = 0;
         unDatagram.auc8[0] = ucLastLanding;
         unDatagram.aui32[1] = GetLatchedCalls( ucLastLanding, HC_DIR__DOWN );
         SDATA_WriteDatagram( gpstSData_GroupNet_RIS,
                              DG_GroupNet_RIS__LATCHED_HALL_CALLS_DN,
                              &unDatagram
                             );
         SDATA_DirtyBit_Set(gpstSData_GroupNet_RIS, DG_GroupNet_RIS__LATCHED_HALL_CALLS_DN);
         Sys_Bit_Set(&stHallCallData.aucBF_DirtyBits_Down[0], ucLastLanding, 0);
         auwMinResendTimer_5ms[ucLastLanding] = 0;
      }
   }
}
/*-----------------------------------------------------------------------------
   Extract data received from hall network CAN datagrams
 -----------------------------------------------------------------------------*/
void UnloadDatagram_HallboardStatus_HN( uint16_t uwDIPs, uint8_t *pucData )
{
   uint8_t ucRiserIndex = uwDIPs / GetHallBoard_MaxNumberOfFloors();
   uint8_t ucLandingIndex = uwDIPs % GetHallBoard_MaxNumberOfFloors();

   /* Mark data for send if changed  */
   uint8_t ucNewIO = pucData[0] & 0x0F;
   en_hallboard_error eNewError = pucData[1];
   if( ( stHallBoardData.stBoardStatuses[uwDIPs].ucBF_RawIO != ucNewIO )
    || ( stHallBoardData.stBoardStatuses[uwDIPs].eError != eNewError ) )
   {
      Sys_Bit_Set(&stHallBoardData.aucBF_DirtyBits[0], uwDIPs, 1);
      stHallBoardData.stBoardStatuses[uwDIPs].ucBF_RawIO = ucNewIO;
      stHallBoardData.stBoardStatuses[uwDIPs].eError = eNewError;
   }

   uint8_t bActiveButton_Up = ( stHallBoardData.stBoardStatuses[uwDIPs].ucBF_RawIO >> HB_IO_SHIFT__BTN_UP ) & 1;
   uint8_t bActiveButton_Down = ( stHallBoardData.stBoardStatuses[uwDIPs].ucBF_RawIO >> HB_IO_SHIFT__BTN_DN ) & 1;

   if( GetSRU_RiserBoardID() != GROUP_NET__RIS_3 )
   {
      Sys_Bit_Set(&stHallBoardData.auiBF_ActiveButton_Up[0], uwDIPs, bActiveButton_Up);
      Sys_Bit_Set(&stHallBoardData.auiBF_ActiveButton_Down[0], uwDIPs, bActiveButton_Down);

      /* Check that this virtual riser is being serviced */
      uint32_t uiGroupMask = CarData_GetGroupHallCallMask(ucLandingIndex, DOOR_ANY) | CarData_GetMedicalHallCallMask(ucLandingIndex, DOOR_ANY);
      if( uiGroupMask & ( 1 << ucRiserIndex ) )
      {
         /* On button press rising edge, latch call */
         if ( !Sys_Bit_Get(&stHallBoardData.auiBF_LastButton_Up[0], uwDIPs)
           && Sys_Bit_Get(&stHallBoardData.auiBF_ActiveButton_Up[0], uwDIPs) )
         {
            SetRawLatchedHallCalls ( ucLandingIndex, HC_DIR__UP, (1 << ucRiserIndex) );
         }
         if ( !Sys_Bit_Get(&stHallBoardData.auiBF_LastButton_Down[0], uwDIPs)
           && Sys_Bit_Get(&stHallBoardData.auiBF_ActiveButton_Down[0], uwDIPs) )
         {
            SetRawLatchedHallCalls ( ucLandingIndex, HC_DIR__DOWN, (1 << ucRiserIndex) );
         }

         /* For EMS medical recall inputs, holding the input on should allow the mode to remain active.
          * Keep the call latched as long as a button to enable this feature */
         if( Sys_Bit_Get(&stHallBoardData.auiBF_ActiveButton_Up[0], uwDIPs)
          || Sys_Bit_Get(&stHallBoardData.auiBF_ActiveButton_Down[0], uwDIPs) )
         {
            uint32_t uiButtonMask = 1 << ucRiserIndex;
            uint32_t uiMedicalMask = CarData_GetMedicalHallCallMask(ucLandingIndex, DOOR_ANY);
            if( uiButtonMask & uiMedicalMask )
            {
               SetRawLatchedHallCalls ( ucLandingIndex, HC_DIR__UP  , uiButtonMask );
               SetRawLatchedHallCalls ( ucLandingIndex, HC_DIR__DOWN, uiButtonMask );
            }
         }
      }
      /* Added for latching only rising edge of button press */
      Sys_Bit_Set(&stHallBoardData.auiBF_LastButton_Up[0], uwDIPs, bActiveButton_Up);
      Sys_Bit_Set(&stHallBoardData.auiBF_LastButton_Down[0], uwDIPs, bActiveButton_Down);
   }
   /* Riser 3 is fixed as the security riser. Its UB is front security, its DB is rear security */
   else if( uwDIPs < NEW_HALL_BOARD_MAX_FLOOR_LIMIT )
   {
      uint8_t bLast_F = Sys_Bit_Get(&auiSecurityContacts_F[0], uwDIPs);
      uint8_t bLast_R = Sys_Bit_Get(&auiSecurityContacts_R[0], uwDIPs);
      uint8_t ucMapIndex = uwDIPs/NUM_BITS_PER_BITMAP32;
      /* Mark packet for resend if the inputs have changed. */
      if( bLast_F != bActiveButton_Up )
      {
         Sys_Bit_Set(&ucSecurityContactDirtyBits_F, ucMapIndex, 1);
      }
      if( bLast_R != bActiveButton_Down )
      {
         Sys_Bit_Set(&ucSecurityContactDirtyBits_R, ucMapIndex, 1);
      }
      Sys_Bit_Set(&auiSecurityContacts_F[0], uwDIPs, bActiveButton_Up);
      Sys_Bit_Set(&auiSecurityContacts_R[0], uwDIPs, bActiveButton_Down);
   }

   /* Clear timeout counter */
   stHallBoardData.aucOfflineCounter[uwDIPs] = 0;
}
/*-----------------------------------------------------------------------------
   Selects new per hallboard status to transmit on group net

 -----------------------------------------------------------------------------*/
static void LoadDatagram_HallboardStatus_GN( struct st_module *pstThisModule )
{
   static uint16_t uwLastAddress;
   static uint16_t uwMinResendDelay_ms;
   if( !SDATA_DirtyBit_Get(gpstSData_GroupNet_RIS, DG_GroupNet_RIS__HALLBOARD_STATUS) )
   {
      /* Check if a board status is being requested. */
      en_car_request eRequest = GetCarRequest_AnyCar();
      if( ( GetSRU_RiserBoardID() != GROUP_NET__RIS_3 )
       && ( eRequest >= CAR_REQUEST__HALL_BOARD__START )
       && ( eRequest <= CAR_REQUEST__HALL_BOARD__END ) )
      {
         uint16_t uwAddress = eRequest - CAR_REQUEST__HALL_BOARD__START;
         if( ( uwAddress < MAX_NUM_HALLBOARDS )
          && ( stHallBoardData.stBoardStatuses[uwAddress].eCommState != HB_COM_STATE__INACTIVE ) )
         {
            if(uwMinResendDelay_ms < HALLBOARD_STATUS_REQUEST_RESEND_RATE_MS)
            {
               uwMinResendDelay_ms += pstThisModule->uwRunPeriod_1ms;
            }
            else
            {
               un_sdata_datagram unDatagram;
               unDatagram.aui32[1] = 0;
               unDatagram.auw16[0] = uwAddress;
               unDatagram.auc8[2] = stHallBoardData.stBoardStatuses[uwAddress].eCommState;
               unDatagram.auc8[3] = stHallBoardData.stBoardStatuses[uwAddress].ucBF_RawIO;
               unDatagram.auc8[4] = stHallBoardData.stBoardStatuses[uwAddress].eError;
               SDATA_WriteDatagram(gpstSData_GroupNet_RIS, DG_GroupNet_RIS__HALLBOARD_STATUS, &unDatagram);
               SDATA_DirtyBit_Set(gpstSData_GroupNet_RIS, DG_GroupNet_RIS__HALLBOARD_STATUS);
               uwLastAddress = uwAddress;
               uwMinResendDelay_ms = 0;
            }
         }
      }
      else if( ( GetSRU_RiserBoardID() == GROUP_NET__RIS_3 )
            && ( eRequest >= CAR_REQUEST__HALL_SECURITY__START )
            && ( eRequest <=  CAR_REQUEST__HALL_SECURITY__END ) )
      {
         uint16_t uwAddress = eRequest - CAR_REQUEST__HALL_SECURITY__START;
         if(uwMinResendDelay_ms < HALLBOARD_STATUS_REQUEST_RESEND_RATE_MS)
         {
            uwMinResendDelay_ms += pstThisModule->uwRunPeriod_1ms;
         }
         else
         {
            un_sdata_datagram unDatagram;
            unDatagram.aui32[1] = 0;
            unDatagram.auw16[0] = uwAddress;
            unDatagram.auc8[2] = stHallBoardData.stBoardStatuses[uwAddress].eCommState;
            unDatagram.auc8[3] = stHallBoardData.stBoardStatuses[uwAddress].ucBF_RawIO;
            unDatagram.auc8[4] = stHallBoardData.stBoardStatuses[uwAddress].eError;
            SDATA_WriteDatagram(gpstSData_GroupNet_RIS, DG_GroupNet_RIS__HALLBOARD_STATUS, &unDatagram);
            SDATA_DirtyBit_Set(gpstSData_GroupNet_RIS, DG_GroupNet_RIS__HALLBOARD_STATUS);
            uwLastAddress = uwAddress;
            uwMinResendDelay_ms = 0;
         }
      }
      /* To maintain backwards compatibility,
       * continue regularly sending hall board
       * statuses unprompted */
      else
      {
         /* Check if a board has been marked for transmit */
         uint16_t uwCurrentAddress = uwLastAddress;
         for( uint16_t i = 0; i < GetHallBoard_MaxNumberOfBoards(); i++ )
         {
            uint16_t uwAddress = ( uwLastAddress+i+1 ) % GetHallBoard_MaxNumberOfBoards();
            if( stHallBoardData.stBoardStatuses[uwAddress].eCommState != HB_COM_STATE__INACTIVE )
            {
               /* Check if changed */
               if( Sys_Bit_Get(&stHallBoardData.aucBF_DirtyBits[0], uwAddress) )
               {
                  Sys_Bit_Set(&stHallBoardData.aucBF_DirtyBits[0], uwAddress, 0);
                  uwLastAddress = uwCurrentAddress-1; // Force a send
                  uwCurrentAddress = uwAddress;
                  break;
               }
            }
         }
         /* Load datagram for transmit if new */
         if( uwLastAddress != uwCurrentAddress )
         {
            un_sdata_datagram unDatagram;
            unDatagram.aui32[1] = 0;
            unDatagram.auw16[0] = uwCurrentAddress;
            unDatagram.auc8[2] = stHallBoardData.stBoardStatuses[uwCurrentAddress].eCommState;
            unDatagram.auc8[3] = stHallBoardData.stBoardStatuses[uwCurrentAddress].ucBF_RawIO;
            unDatagram.auc8[4] = stHallBoardData.stBoardStatuses[uwCurrentAddress].eError;
            SDATA_WriteDatagram(gpstSData_GroupNet_RIS, DG_GroupNet_RIS__HALLBOARD_STATUS, &unDatagram);
            SDATA_DirtyBit_Set(gpstSData_GroupNet_RIS, DG_GroupNet_RIS__HALLBOARD_STATUS);
            uwLastAddress = uwCurrentAddress;
            uwMinResendDelay_ms = 0;
         }
         else
         {
            /* If no updates sent within specified period, select another index to send. */
            if(uwMinResendDelay_ms < HALLBOARD_STATUS_MIN_RESEND_RATE_MS)
            {
               uwMinResendDelay_ms += pstThisModule->uwRunPeriod_1ms;
            }
            else
            {
               for( uint16_t i = 0; i < GetHallBoard_MaxNumberOfBoards(); i++ )
               {
                  uint16_t uwAddress = ( uwLastAddress+i+1 ) % GetHallBoard_MaxNumberOfBoards();
                  if( stHallBoardData.stBoardStatuses[uwAddress].eCommState != HB_COM_STATE__INACTIVE )
                  {
                     Sys_Bit_Set(&stHallBoardData.aucBF_DirtyBits[0], uwAddress, 1);
                     break;
                  }
               }
            }
         }
      }
   }
}
/*-----------------------------------------------------------------------------
   Unload hall calls latched by other riser boards.
 -----------------------------------------------------------------------------*/
void UnloadDatagram_RiserLatchedHallCalls( uint8_t ucRiserIndex, en_hc_dir eDir, un_sdata_datagram *punDatagram )
{
   uint8_t ucLanding = punDatagram->auc8[0];
   uint8_t ucSecurityRiserIndex = SECURITY_RISER_INDEX;
   uint8_t ucLocalRiserIndex = GetSRU_RiserBoardIndex();
   /* Ignore latched hall call packets if the security riser or from the security riser */
   if( ( ucRiserIndex != ucSecurityRiserIndex )
    && ( ucLocalRiserIndex != ucSecurityRiserIndex )
    && ( ucLanding < GetHallBoard_MaxNumberOfFloors() ) )
   {
      if( eDir == HC_DIR__UP )
      {
         stHallCallData.auiBF_HallCalls_Up[ucRiserIndex][ucLanding] = punDatagram->aui32[1];
      }
      else
      {
         stHallCallData.auiBF_HallCalls_Down[ucRiserIndex][ucLanding] = punDatagram->aui32[1];
      }
   }
}
/*-----------------------------------------------------------------------------
   Extract hall board status data received from other riser boards
 -----------------------------------------------------------------------------*/
void UnloadDatagram_RiserHallboardStatus_GN( uint8_t ucRiserIndex, un_sdata_datagram *punDatagram )
{
   uint16_t uwHallboardAddress = punDatagram->auw16[0];
   if( uwHallboardAddress < GetHallBoard_MaxNumberOfBoards() )
   {
      uint8_t ucLocalRiserIndex = GetSRU_RiserBoardID() - GROUP_NET__RIS_1;
      uint8_t ucSecurityRiserIndex = SECURITY_RISER_INDEX;
      if( ( ucRiserIndex != ucLocalRiserIndex )
       && ( ucRiserIndex != ucSecurityRiserIndex ) /* Ignore hall boards from the security riser */
       && ( ucLocalRiserIndex != ucSecurityRiserIndex ) /* Ignore hall boards as the security riser */
       && ( stHallBoardData.stBoardStatuses[uwHallboardAddress].ucSourceNode != ( ucLocalRiserIndex+1 ) ) )
      {
         stHallBoardData.stBoardStatuses[uwHallboardAddress].ucBF_RawIO = punDatagram->auc8[3];
         stHallBoardData.stBoardStatuses[uwHallboardAddress].eError = punDatagram->auc8[4];
         stHallBoardData.stBoardStatuses[uwHallboardAddress].ucSourceNode = ucRiserIndex+1;

         uint8_t ucRiser = uwHallboardAddress / GetHallBoard_MaxNumberOfFloors();
         uint8_t ucLandingIndex = uwHallboardAddress % GetHallBoard_MaxNumberOfFloors();

         uint8_t bActiveButton_Up = ( stHallBoardData.stBoardStatuses[uwHallboardAddress].ucBF_RawIO >> HB_IO_SHIFT__BTN_UP ) & 1;
         uint8_t bActiveButton_Down = ( stHallBoardData.stBoardStatuses[uwHallboardAddress].ucBF_RawIO >> HB_IO_SHIFT__BTN_DN ) & 1;

         Sys_Bit_Set(&stHallBoardData.auiBF_ActiveButton_Up[0], uwHallboardAddress, bActiveButton_Up);
         Sys_Bit_Set(&stHallBoardData.auiBF_ActiveButton_Down[0], uwHallboardAddress, bActiveButton_Down);

         /* Calls from hall board connected to other riser board detected here */
         /* Check that this virtual riser is being serviced */
         uint32_t uiGroupMask = CarData_GetGroupHallCallMask(ucLandingIndex, DOOR_ANY) | CarData_GetMedicalHallCallMask(ucLandingIndex, DOOR_ANY);
         if( uiGroupMask & ( 1 << ucRiser ) )
         {
            /* On button press rising edge, latch call */
            if ( !Sys_Bit_Get(&stHallBoardData.auiBF_LastButton_Up[0], uwHallboardAddress)
              && Sys_Bit_Get(&stHallBoardData.auiBF_ActiveButton_Up[0], uwHallboardAddress) )
            {
               SetRawLatchedHallCalls ( ucLandingIndex, HC_DIR__UP, (1 << ucRiser) );
            }
            if ( !Sys_Bit_Get(&stHallBoardData.auiBF_LastButton_Down[0], uwHallboardAddress)
              && Sys_Bit_Get(&stHallBoardData.auiBF_ActiveButton_Down[0], uwHallboardAddress) )
            {
               SetRawLatchedHallCalls ( ucLandingIndex, HC_DIR__DOWN, (1 << ucRiser) );
            }
         }
         /* Added for latching only rising edge of button press */
         Sys_Bit_Set(&stHallBoardData.auiBF_LastButton_Up[0], uwHallboardAddress, bActiveButton_Up);
         Sys_Bit_Set(&stHallBoardData.auiBF_LastButton_Down[0], uwHallboardAddress, bActiveButton_Down);
      }
   }
}
/*-----------------------------------------------------------------------------
   Updates the per hallboard offline counters and sets each hallboard's COM state based
   on the current value
 -----------------------------------------------------------------------------*/
static void UpdateHallboardCommState()
{
   stHallBoardData.bHallBoardOffline = 0;
   for( uint16_t i = 0; i < GetHallBoard_MaxNumberOfBoards(); i++ )
   {
      en_hb_com_state eLastState = stHallBoardData.stBoardStatuses[i].eCommState;
      if( stHallBoardData.aucOfflineCounter[i] == aucComStatesCountLimits[HB_COM_STATE__INACTIVE] )
      {
         stHallBoardData.stBoardStatuses[i].eCommState = HB_COM_STATE__INACTIVE;
      }
      else if( stHallBoardData.aucOfflineCounter[i] >= aucComStatesCountLimits[HB_COM_STATE__0] )
      {
         stHallBoardData.stBoardStatuses[i].eCommState = HB_COM_STATE__0;
         stHallBoardData.bHallBoardOffline = 1;
      }
      else
      {
         stHallBoardData.aucOfflineCounter[i]++;
         if( stHallBoardData.aucOfflineCounter[i] < aucComStatesCountLimits[HB_COM_STATE__100] )
         {
            stHallBoardData.stBoardStatuses[i].eCommState = HB_COM_STATE__100;
         }
         else if( stHallBoardData.aucOfflineCounter[i] < aucComStatesCountLimits[HB_COM_STATE__75] )
         {
            stHallBoardData.stBoardStatuses[i].eCommState = HB_COM_STATE__75;
         }
         else if( stHallBoardData.aucOfflineCounter[i] < aucComStatesCountLimits[HB_COM_STATE__50] )
         {
            stHallBoardData.stBoardStatuses[i].eCommState = HB_COM_STATE__50;
         }
         else if( stHallBoardData.aucOfflineCounter[i] < aucComStatesCountLimits[HB_COM_STATE__25] )
         {
            stHallBoardData.stBoardStatuses[i].eCommState = HB_COM_STATE__25;
         }

         uint8_t ucLocalRiserIndex = GetSRU_RiserBoardID() - GROUP_NET__RIS_1;
         stHallBoardData.stBoardStatuses[i].ucSourceNode = ucLocalRiserIndex + 1;
      }

      if( eLastState != stHallBoardData.stBoardStatuses[i].eCommState )
      {
         Sys_Bit_Set(&stHallBoardData.aucBF_DirtyBits[0], i, 1);
      }

      /* Clear hall board IO if offline */
      if(stHallBoardData.stBoardStatuses[i].eCommState == HB_COM_STATE__0)
      {
         stHallBoardData.stBoardStatuses[i].ucBF_RawIO = 0;
         /* Update security contacts if this is the security riser */
         if( ( GetSRU_RiserBoardID() == GROUP_NET__RIS_3 )
          && ( i < NEW_HALL_BOARD_MAX_FLOOR_LIMIT ) )
         {
            uint8_t bLastButton_F = Sys_Bit_Get(&auiSecurityContacts_F[0], i);
            uint8_t bLastButton_R = Sys_Bit_Get(&auiSecurityContacts_R[0], i);
            uint8_t ucDirtyBitIndex = i/NUM_BITS_PER_BITMAP32;
            // Flag packet for resend if a security contact has changed
            if( bLastButton_F )
            {
               Sys_Bit_Set(&ucSecurityContactDirtyBits_F, ucDirtyBitIndex, 1);
            }
            if( bLastButton_R )
            {
               Sys_Bit_Set(&ucSecurityContactDirtyBits_R, ucDirtyBitIndex, 1);
            }
            Sys_Bit_Set(&auiSecurityContacts_F[0], i, 0);
            Sys_Bit_Set(&auiSecurityContacts_R[0], i, 0);
         }
      }
   }
}

/*-----------------------------------------------------------------------------
   Updates hall lamp bitmaps
 -----------------------------------------------------------------------------*/
static void UpdateHallLamps()
{
   /* Clear all hall board lamps */
   memset(&stHallBoardData.auiBF_ActiveLamp_Up[0], 0, sizeof(stHallBoardData.auiBF_ActiveLamp_Up));
   memset(&stHallBoardData.auiBF_ActiveLamp_Down[0], 0, sizeof(stHallBoardData.auiBF_ActiveLamp_Down));
   st_cardata *pstCarData = GetCarDataStructure(GROUP_NET__CAR_8);
   /* Get the paired hall masks on the master car */
   for( uint8_t ucCar = 0; ucCar < MAX_GROUP_CARS-1; ucCar++ )
   {
      if( GetCarOfflineTimer_ByCar(ucCar) < GROUP_CAR_OFFLINE_TIMER_MS )
      {
         pstCarData = GetCarDataStructure(ucCar);
         break;
      }
   }

   uint8_t ucHighestLanding = CarData_GetHighestActiveLanding();
   for( uint8_t ucLandingIndex = 0; ucLandingIndex <= ucHighestLanding; ucLandingIndex++ )
   {
      for(uint8_t ucFunctionIndex = 0; ucFunctionIndex < GetHallBoard_MaxNumberOfBoards()/GetHallBoard_MaxNumberOfFloors(); ucFunctionIndex++ )
      {
         uint8_t bLamp_Up = 0;
         uint8_t bLamp_Down = 0;
         uint16_t uwHallboardIndex = ( ucFunctionIndex * GetHallBoard_MaxNumberOfFloors() ) + ucLandingIndex;
         if( uwHallboardIndex < GetHallBoard_MaxNumberOfBoards() )
         {
            /* Set lamp output if button is currently pressed, or call is latched */
            uint32_t uiHallCalls_Up = 0;
            uint32_t uiHallCalls_Down = 0;
            for( uint8_t ucRiserBoardIndex = 0; ucRiserBoardIndex < MAX_NUM_RISER_BOARDS; ucRiserBoardIndex++ )
            {
               uiHallCalls_Up |= stHallCallData.auiBF_HallCalls_Up[ucRiserBoardIndex][ucLandingIndex];
               uiHallCalls_Down |= stHallCallData.auiBF_HallCalls_Down[ucRiserBoardIndex][ucLandingIndex];
            }
            uint8_t bLatched_Up = Sys_Bit_Get(&uiHallCalls_Up, ucFunctionIndex);
            uint8_t bCurrentPress_Up = Sys_Bit_Get(&stHallBoardData.auiBF_ActiveButton_Up[0], uwHallboardIndex);
            if( bLatched_Up
             || bCurrentPress_Up )
            {
               Sys_Bit_Set(&stHallBoardData.auiBF_ActiveLamp_Up[0], uwHallboardIndex, 1);
               bLamp_Up = 1;
            }

            if( Sys_Bit_Get(&uiHallCalls_Down, ucFunctionIndex)
             || Sys_Bit_Get(&stHallBoardData.auiBF_ActiveButton_Down[0], uwHallboardIndex) )
            {
               Sys_Bit_Set(&stHallBoardData.auiBF_ActiveLamp_Down[0], uwHallboardIndex, 1);
               bLamp_Down = 1;
            }
         }

         /* Also set any paired hall boards */
         uint16_t uwFunctionMask = 1 << ucFunctionIndex;
         for( uint8_t i = 0; i < NUM_PAIRED_HALLBOARD_MASKS; i++ )
         {
            uint8_t ucPairedHBMask = pstCarData->aucPairedHBMask[i];
            if( ucPairedHBMask & uwFunctionMask )
            {
               for( uint8_t j = 0; j < NUM_BITS_PER_BITMAP8-1; j++ )
               {
                  uint8_t ucPairedRiser = ( ucFunctionIndex + j + 1 ) % (GetHallBoard_MaxNumberOfBoards()/GetHallBoard_MaxNumberOfFloors());

                  if( Sys_Bit_Get(&ucPairedHBMask, ucPairedRiser) )
                  {
                     uwHallboardIndex = ( ucPairedRiser * GetHallBoard_MaxNumberOfFloors() ) + ucLandingIndex;
                     if( uwHallboardIndex < GetHallBoard_MaxNumberOfBoards() )
                     {
                        if( bLamp_Up )
                        {
                           Sys_Bit_Set(&stHallBoardData.auiBF_ActiveLamp_Up[0], uwHallboardIndex, 1);
                        }

                        if( bLamp_Down )
                        {
                           Sys_Bit_Set(&stHallBoardData.auiBF_ActiveLamp_Down[0], uwHallboardIndex, 1);
                        }
                     }
                  }
               }
            }
         }
      }
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 100;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_HALL_MONITOR_1MS;


   for( uint16_t i = 0; i < MAX_NUM_HALLBOARDS; i++ )
   {
      stHallBoardData.aucOfflineCounter[i] = HALLBOARD_INACTIVE_OFFLINE_COUNT;
   }

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   UpdateHallboardCommState();
   LoadDatagram_HallboardStatus_GN(pstThisModule);
   UpdateHallLamps();

   if( stHallBoardData.bHallBoardOffline )
   {
      SetFault(ERROR_RIS_EXP__HALL_BOARD_OFFLINE);
   }
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
