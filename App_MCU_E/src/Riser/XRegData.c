
/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief
 * @version  V1.00
 * @date     27, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>

#include "sru_e.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "XRegData.h"
#include "carData.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static st_xreg_car_data astXRegCarData[MAX_GROUP_CARS];
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Initialize XREGData
   ucCarIndex < 8
 -----------------------------------------------------------------------------*/
void XRegData_Init(uint8_t ucCarIndex)
{
   memset(&astXRegCarData[ucCarIndex], 0, sizeof(st_xreg_car_data));
   astXRegCarData[ucCarIndex].uwOfflineCounter_ms = XREG_CAR_OFFLINE_INACTIVE_MS;
}
/*-----------------------------------------------------------------------------
   Updates XREG car's bCarOnline flag.
   uiModRunPeriod_ms = run period of the module calling this function
   ucCarIndex < 8
 -----------------------------------------------------------------------------*/
void XRegData_UpdateCarOnlineFlag(uint8_t ucCarIndex, uint16_t uwModRunPeriod_ms)
{
   if(astXRegCarData[ucCarIndex].uwOfflineCounter_ms < XREG_CAR_OFFLINE_LIMIT_MS)
   {
      astXRegCarData[ucCarIndex].uwOfflineCounter_ms += uwModRunPeriod_ms;
      astXRegCarData[ucCarIndex].bCarOnline = 1;
   }
   else
   {
      astXRegCarData[ucCarIndex].bCarOnline = 0;
   }
}

/*-----------------------------------------------------------------------------
   Access variables
   ucCarIndex < 8
 -----------------------------------------------------------------------------*/
uint8_t XRegData_GetCarActive(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].bCarOnline
       && astXRegCarData[ucCarIndex].bInGroup;
}
enum en_classop XRegData_GetClassOp(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].eClassOp;
}
enum en_mode_auto XRegData_GetAutoMode(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].eAutoMode;
}
uint8_t XRegData_GetCurrentLanding(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].ucLandingIndex;
}
enum direction_enum XRegData_GetMotionDirection(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].cMotionDir;
}
enum direction_enum XRegData_GetArrivalDirection(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].cArrivalDir;
}
uint8_t XRegData_GetOpening_Front(uint8_t ucCarIndex, uint8_t ucLanding)
{
   uint8_t bActive = Sys_Bit_Get(&astXRegCarData[ucCarIndex].aucBF_FrontMap[0], ucLanding);
   return bActive;
}
uint8_t XRegData_GetOpening_Rear(uint8_t ucCarIndex, uint8_t ucLanding)
{
   uint8_t bActive = Sys_Bit_Get(&astXRegCarData[ucCarIndex].aucBF_RearMap[0], ucLanding);
   return bActive;
}
uint8_t XRegData_GetFirstLanding(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].ucFirstLanding;
}
uint8_t XRegData_GetLastLanding(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].ucLastLanding;
}
en_hc_dir XRegData_GetPriority(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].enPriority;
}
uint32_t XRegData_GetHallMask_F(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].uiHallMask_F;
}
uint32_t XRegData_GetHallMask_R(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].uiHallMask_R;
}
uint8_t XRegData_GetCarOnlineFlag(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].bCarOnline;
}

/*-----------------------------------------------------------------------------
   Get latchable hall mask for XReg boards
 -----------------------------------------------------------------------------*/
uint16_t XRegData_GetGroupHallCallMask()
{
   uint16_t uwMask = 0;
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      if( XRegData_GetCarActive(i) )
      {
         uwMask |= XRegData_GetHallMask_F(i) | XRegData_GetHallMask_R(i);
      }
   }
   return uwMask;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_CarStatus( un_sdata_datagram *punDatagram )
{
   uint8_t ucCarID = punDatagram->auc8[1];
   enum en_classop eClassOp = punDatagram->auc8[2];
   enum en_mode_auto eAutoMode = punDatagram->auc8[3];
//   uint8_t bGSW_F = (punDatagram->auc8[5] & 0x01) ? 1:0;
//   uint8_t bGSW_R = (punDatagram->auc8[5] & 0x02) ? 1:0;
   uint8_t bTrvUp = (punDatagram->auc8[5] & 0x04) ? 1:0;
   uint8_t bTrvDown = (punDatagram->auc8[5] & 0x08) ? 1:0;
   uint8_t bArvUp = (punDatagram->auc8[5] & 0x10) ? 1:0;
   uint8_t bArvDown = (punDatagram->auc8[5] & 0x20) ? 1:0;
   uint8_t bInGroup = (punDatagram->auc8[5] & 0x40) ? 1:0;
   uint8_t ucFloor_Plus1 = punDatagram->auc8[4];
   if( ( ucCarID < MAX_GROUP_CARS )
    && ( ucFloor_Plus1 ) )
   {
      astXRegCarData[ucCarID].eClassOp = eClassOp;
      astXRegCarData[ucCarID].eAutoMode = eAutoMode;
      astXRegCarData[ucCarID].ucLandingIndex = ucFloor_Plus1-1;

      if(bTrvUp)
      {
         astXRegCarData[ucCarID].cMotionDir = DIR__UP;
      }
      else if(bTrvDown)
      {
         astXRegCarData[ucCarID].cMotionDir = DIR__DN;
      }
      else
      {
         astXRegCarData[ucCarID].cMotionDir = DIR__NONE;
      }

      if(bArvUp)
      {
         astXRegCarData[ucCarID].cArrivalDir = DIR__UP;
      }
      else if(bArvDown)
      {
         astXRegCarData[ucCarID].cArrivalDir = DIR__DN;
      }
      else
      {
         astXRegCarData[ucCarID].cArrivalDir = DIR__NONE;
      }

      astXRegCarData[ucCarID].bInGroup = bInGroup;

      // TODO receive from XREG
      // If even, car prioritize down calls, if odd prioritize up calls
      astXRegCarData[ucCarID].enPriority = ucCarID % 2;

      astXRegCarData[ucCarID].uwOfflineCounter_ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_ClearHallCall( un_sdata_datagram *punDatagram )
{
   uint8_t ucCarID = punDatagram->auc8[1];
   enum en_doors eDoor = punDatagram->auc8[2];
   en_hc_dir eCallDir = punDatagram->auc8[3];
   uint8_t ucLanding = INVALID_LANDING;

   if( ( punDatagram->auc8[4] ) // ucFloor_Plus1
    && ( ucCarID < MAX_GROUP_CARS ) )
   {
      ucLanding = punDatagram->auc8[4]-1;
      if(eDoor == DOOR_FRONT)
      {
         ClrRawLatchedHallCalls(ucLanding, eCallDir, XRegData_GetHallMask_F(ucCarID));
      }
      else
      {
         ClrRawLatchedHallCalls(ucLanding, eCallDir, XRegData_GetHallMask_R(ucCarID));
      }

      astXRegCarData[ucCarID].uwOfflineCounter_ms = 0;
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_FloorDoorMap( un_sdata_datagram *punDatagram )
{
   uint8_t ucCarID = punDatagram->auc8[1];
   uint8_t ucFloor_Plus1 = punDatagram->auc8[2];
   uint8_t ucFloorMap = punDatagram->auc8[3];
   uint8_t ucFrontMap = punDatagram->auc8[4];
   uint8_t ucRearMap = punDatagram->auc8[5];
   uint8_t ucBitmapIndex = (ucFloor_Plus1-1) / 8;
   if( ( ucCarID < MAX_GROUP_CARS )
    && ( ucFloor_Plus1 )
    && ( ucBitmapIndex < BITMAP8_SIZE(MAX_NUM_FLOORS) ) )
   {
      uint8_t ucBitmapIndex = (ucFloor_Plus1-1) / 8;
      astXRegCarData[ucCarID].aucBF_LandingMap[ucBitmapIndex] = ucFloorMap;
      astXRegCarData[ucCarID].aucBF_FrontMap[ucBitmapIndex] = ucFrontMap;
      astXRegCarData[ucCarID].aucBF_RearMap[ucBitmapIndex] = ucRearMap;

      astXRegCarData[ucCarID].uwOfflineCounter_ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_HallCallMask( un_sdata_datagram *punDatagram )
{
   uint16_t uwHallMask_F = (punDatagram->auc8[1])
                         | (punDatagram->auc8[2] << 8);
   uint16_t uwHallMask_R = (punDatagram->auc8[3])
                         | (punDatagram->auc8[4] << 8);

   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      astXRegCarData[i].uiHallMask_F = uwHallMask_F;
      astXRegCarData[i].uiHallMask_R = uwHallMask_R;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UnloadDatagram_XRegStatus( un_sdata_datagram *punDatagram )
{
   en_xreg_stat eStatus = punDatagram->auc8[0];
   switch( eStatus )
   {
      case XREG_STAT__CAR_STATUS:
         Unload_CarStatus(punDatagram);
         break;
      case XREG_STAT__FIRE_EP:
         break;
      case XREG_STAT__LATCHED_HC:
         break;
      case XREG_STAT__CLEAR_HC:
         Unload_ClearHallCall(punDatagram);
         break;
      case XREG_STAT__FLOOR_DOOR_MAP:
         Unload_FloorDoorMap(punDatagram);
         break;
      case XREG_STAT__HALL_CALL_MASK:
         Unload_HallCallMask(punDatagram);
         break;

      default:
         break;

   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
