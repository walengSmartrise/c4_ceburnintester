/******************************************************************************
 *
 * @file     mod_group_master.c
 * @brief    Monitors car data for HC clearing events
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "sru_e.h"
#include "sys.h"
#include "carData.h"
#include "groupnet_datagrams.h"
#include "groupnet.h"
#include "carData.h"
#include "XRegData.h"
#include "hallSecurity.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_GroupMaster =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define CALL_SERVICED_TIMEOUT_50MS        (40)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
   Update local outputs
 -----------------------------------------------------------------------------*/
static void UpdateLocalOutputs()
{
   uint8_t ucOutputs = 0;
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      // Selects set of outputs encounters and ignores the rest
      if(GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS)
      {
         uint8_t ucRiserIndex = GetSRU_RiserBoardID() - GROUP_NET__RIS_1;
         GroupNet_DatagramID eGroupDatagramID = GroupNet_LocalCarIDToGlobalID(i, DG_GroupNet_MRB__RiserOutputs);
         un_sdata_datagram *punDatagram = GetGroupDatagram(eGroupDatagramID);
         ucOutputs |= punDatagram->auc8[ucRiserIndex];
      }
   }
   SetLocalOutputs_ThisNode(ucOutputs);
}
/*-----------------------------------------------------------------------------
   Clear any hall calls latched in inactive masks
 -----------------------------------------------------------------------------*/
static void ClearInvalidHallCalls(void)
{
   uint32_t uiXRegHallMask = XRegData_GetGroupHallCallMask();
   uint8_t ucHighestLanding = CarData_GetHighestActiveLanding();
   for (uint8_t i = 0; i <= ucHighestLanding; i++)
   {
      uint32_t uiMask = CarData_GetGroupHallCallMask(i, DOOR_ANY) | uiXRegHallMask | CarData_GetMedicalHallCallMask(i, DOOR_ANY);
      ClrRawLatchedHallCalls( i, HC_DIR__DOWN, ~uiMask );
      ClrRawLatchedHallCalls( i, HC_DIR__UP,   ~uiMask );
   }
}
/*-----------------------------------------------------------------------------
   Clears hall calls that have been cleared by a car
 -----------------------------------------------------------------------------*/
static void ClearServicedHallCalls(void)
{
   /* Delay hall call clearing to allow time for the call to reach the car,
    * otherwise arrival lantern assertion will be suppressed if doors are opened */
   static uint8_t aucCallServicedTimer_F_50ms[MAX_GROUP_CARS];
   static uint8_t aucCallServicedTimer_R_50ms[MAX_GROUP_CARS];
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      if(GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS)
      {
         st_cardata *pstCarData = GetCarDataStructure(i);
         uint8_t ucDestinationLanding = pstCarData->ucDestinationLanding;
         uint8_t ucCurrentLanding = pstCarData->ucCurrentLanding;
         en_hc_dir enPriority = pstCarData->enPriority;
         if( ( ucDestinationLanding < MAX_NUM_FLOORS )
          && ( ucDestinationLanding == ucCurrentLanding ) )
         {
            if( CarData_GetDoorOpen_Front(i) )
            {
               if( aucCallServicedTimer_F_50ms[i] >= CALL_SERVICED_TIMEOUT_50MS )
               {
                  aucCallServicedTimer_F_50ms[i] = 0;
                  /* EMS recall clearing doesn't depend on car direction priority */
                  if( pstCarData->eAutoMode == MODE_A__EMS1 )
                  {
                     /* Add condition to allow EMS1 to stay active as long as the keyswitch is kept on. */
                     if( !HallBoard_CheckIfMedicalRecallHeldActive(ucDestinationLanding, pstCarData->ucMedicalMask) )
                     {
                        ClrRawLatchedHallCalls(ucDestinationLanding, HC_DIR__UP  , pstCarData->ucMedicalMask);
                        ClrRawLatchedHallCalls(ucDestinationLanding, HC_DIR__DOWN, pstCarData->ucMedicalMask);
                     }
                  }
                  else if( enPriority == HC_DIR__UP )
                  {
                     ClrRawLatchedHallCalls(ucDestinationLanding, HC_DIR__UP, pstCarData->uiHallMask_F);
                  }
                  else if( enPriority == HC_DIR__DOWN )
                  {
                     ClrRawLatchedHallCalls(ucDestinationLanding, HC_DIR__DOWN, pstCarData->uiHallMask_F);
                  }
               }
               else
               {
                  aucCallServicedTimer_F_50ms[i]++;
               }
            }
            else
            {
               aucCallServicedTimer_F_50ms[i] = 0;
            }

            if( CarData_GetDoorOpen_Rear(i) )
            {
               if( aucCallServicedTimer_R_50ms[i] >= CALL_SERVICED_TIMEOUT_50MS )
               {
                  aucCallServicedTimer_R_50ms[i] = 0;
                  /* EMS recall clearing doesn't depend on car direction priority */
                  if( pstCarData->eAutoMode == MODE_A__EMS1 )
                  {
                     /* Add condition to allow EMS1 to stay active as long as the keyswitch is kept on. */
                     if( !HallBoard_CheckIfMedicalRecallHeldActive(ucDestinationLanding, pstCarData->ucMedicalMask) )
                     {
                        ClrRawLatchedHallCalls(ucDestinationLanding, HC_DIR__UP  , pstCarData->ucMedicalMask);
                        ClrRawLatchedHallCalls(ucDestinationLanding, HC_DIR__DOWN, pstCarData->ucMedicalMask);
                     }
                  }
                  else if( enPriority == HC_DIR__UP )
                  {
                     ClrRawLatchedHallCalls(ucDestinationLanding, HC_DIR__UP, pstCarData->uiHallMask_R);
                  }
                  else if( enPriority == HC_DIR__DOWN )
                  {
                     ClrRawLatchedHallCalls(ucDestinationLanding, HC_DIR__DOWN, pstCarData->uiHallMask_R);
                  }
               }
               else
               {
                  aucCallServicedTimer_R_50ms[i]++;
               }
            }
            else
            {
               aucCallServicedTimer_R_50ms[i] = 0;
            }
         }
      }
      else
      {
         aucCallServicedTimer_F_50ms[i] = 0;
         aucCallServicedTimer_R_50ms[i] = 0;
      }
   }
}
/*-----------------------------------------------------------------------------
   Clear any hall calls latched for secured floors
 -----------------------------------------------------------------------------*/
static void ClearSecuredHallCalls(void)
{
   static uint8_t ucLanding;
   uint8_t ucHighestLanding = CarData_GetHighestActiveLanding();

   ClearSecuredHallCalls_ByLanding(ucLanding);

   ucLanding = (ucLanding+1) % (ucHighestLanding+1);
}
/*-----------------------------------------------------------------------------
Update data for cross registration cars
 -----------------------------------------------------------------------------*/
static void UpdateXRegData( struct st_module *pstThisModule )
{
   static uint8_t abLastXRegOnline[MAX_GROUP_CARS];
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      XRegData_UpdateCarOnlineFlag(i, pstThisModule->uwRunPeriod_1ms);

      if( abLastXRegOnline[i]
      && !XRegData_GetCarOnlineFlag(i) )
      {
         XRegData_Init(i);
      }

      abLastXRegOnline[i] = XRegData_GetCarOnlineFlag(i);
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckForCarOffline( struct st_module *pstThisModule )
{
   static uint8_t abLastOnline[MAX_GROUP_CARS];
   IncrementAllCarOfflineTimers(pstThisModule);
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      uint8_t bOnline = 0;
      if( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
      {
         bOnline = 1;
      }
      else if( abLastOnline[i] )
      {
         Init_CarDataStructure_ByCar(i);
      }

      abLastOnline[i] = bOnline;
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 500;
   pstThisModule->uwRunPeriod_1ms = 50;

   Init_CarDataStructure();

   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      XRegData_Init(i);
   }
   return 0;
}

/*-----------------------------------------------------------------------------
 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   CheckForCarOffline(pstThisModule);

   UpdateLocalOutputs();
   ClearServicedHallCalls();
   ClearInvalidHallCalls();
   ClearSecuredHallCalls();
   UpdateXRegData(pstThisModule);

   return 0;
}



/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
