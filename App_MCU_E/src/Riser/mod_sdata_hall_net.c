/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     26, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sru_e.h"
#include "sys.h"
#include "sdata.h"
#include "GlobalData.h"
#include "mod_hallmonitor.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init(struct st_module *pstThisModule);
static uint32_t Run(struct st_module *pstThisModule);

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_SData_HallNet =
{ .pfnInit = Init, .pfnRun = Run, };

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
/* Below are all the nodes in Hall Network */
static struct st_sdata_control gstSData_HallNet_HB; /* TODO: How to Handle Multiple Instances? */
static struct st_sdata_control gstSData_HallNet_RIS; /* TODO: No RIS nodes if MRB is present */
static struct st_sdata_control gstSData_HallNet_MRB; /* TODO: No MRB nodes if RIS is present */

static CAN_MSG_T gstCAN_MSG_Tx;
//static CAN_MSG_T gstCAN_MSG_Rx;

struct st_sdata_control * gastHallNetSData_Nodes[ NUM_HALL_NET_NODES ] =
{
         &gstSData_HallNet_RIS,
         &gstSData_HallNet_MRB,
         &gstSData_HallNet_HB,// Configure at runtime w/ param fetch?
};

uint8_t gaucHallNetPerNodeNumDatagrams[ NUM_HALL_NET_NODES ] =
{
   NUM_HallNet_RIS_DATAGRAMS,
   1,
   NUM_HallNet_HB_DATAGRAMS,
};

struct st_sdata_local gstHallNetSData_Config;

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SharedData_HallNet_Init()
{
   int iError;
   int i;

      gstHallNetSData_Config.ucLocalNodeID = HALL_NET__RIS;

      gstHallNetSData_Config.ucNetworkID = SDATA_NET__HALL;

      gstHallNetSData_Config.ucNumNodes = NUM_HALL_NET_NODES;

      gstHallNetSData_Config.ucDestNodeID = 0x1F;

      for ( i = 0; i < gstHallNetSData_Config.ucNumNodes; ++i )
      {
         iError = SDATA_CreateDatagrams( gastHallNetSData_Nodes[ i ],
                  gaucHallNetPerNodeNumDatagrams[ i ] );

         if ( iError )
         {
            while ( 1 )
            {
               // TODO: unable to allocate shared data
               SRU_Write_LED( enSRU_LED_Fault, 1 );
            }
         }
      }

      gstCAN_MSG_Tx.DLC = 8;

}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Receive(void)
{
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Transmit(void)
{
   un_sdata_datagram unDatagram;
   int iError = 0;
   uint8_t ucNodeID;
   uint8_t ucDatagramID;

   ucNodeID = gstHallNetSData_Config.ucLocalNodeID;

   ucDatagramID = SDATA_GetNextDatagramToSendIndex_Plus1(
            gastHallNetSData_Nodes[ ucNodeID ] );

   if ( ucDatagramID && ucDatagramID <= gaucHallNetPerNodeNumDatagrams[ ucNodeID ] )
   {
      ucDatagramID--;
      SDATA_ReadDatagram( gastHallNetSData_Nodes[ ucNodeID ], ucDatagramID,
               &unDatagram );

      memcpy( gstCAN_MSG_Tx.Data, unDatagram.auc8,
               sizeof( unDatagram.auc8 ) );

      gstCAN_MSG_Tx.ID = CAN_EXTEND_ID_USAGE
                      | ( SRU_Read_DIP_Bank( enSRU_DIP_BANK_B ) << HALLNET_CAN_ID_SHIFT__DIP )
                      | ( ucDatagramID << HALLNET_CAN_ID_SHIFT__DG )
                      | ( ( gstHallNetSData_Config.ucDestNodeID & 0xF ) << HALLNET_CAN_ID_SHIFT__DEST )
                      | ( gstHallNetSData_Config.ucLocalNodeID << HALLNET_CAN_ID_SHIFT__SRC )
                      | ( gstHallNetSData_Config.ucNetworkID << HALLNET_CAN_ID_SHIFT__NET );

      CAN_BUFFER_ID_T   TxBuf = Chip_CAN_GetFreeTxBuf(LPC_CAN2);
      iError =  !Chip_CAN_Send( LPC_CAN2, TxBuf, &gstCAN_MSG_Tx);

      if ( iError )
      {
         SDATA_DirtyBit_Set( gastHallNetSData_Nodes[ ucNodeID ], ucDatagramID );
         if ( gastHallNetSData_Nodes[ ucNodeID ]->uwLastSentIndex )
         {
            --gastHallNetSData_Nodes[ ucNodeID ]->uwLastSentIndex;
         }
         else
         {
            gastHallNetSData_Nodes[ ucNodeID ]->uwLastSentIndex =
                     gastHallNetSData_Nodes[ ucNodeID ]->uiNumDatagrams - 1;
         }
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadData(void)
{
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData(void)
{
   un_sdata_datagram unDatagram;
   for ( uint8_t i = DG_HallNet_RIS__HallLamps_0; i < GetHallBoard_MaxNumberOfBoards()/NUM_BITS_PER_BITMAP32; i++ )
   {
      unDatagram.aui32[1] = GetActiveHallLamps( i, HC_DIR__UP );
      unDatagram.aui32[0] = GetActiveHallLamps( i, HC_DIR__DOWN );

      SDATA_WriteDatagram( &gstSData_HallNet_RIS,
                           i,
                           &unDatagram );
   }
}

/*-----------------------------------------------------------------------------

 Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init(struct st_module *pstThisModule)
{

   //------------------------------------------------
   pstThisModule->uwInitialDelay_1ms = 5;
   pstThisModule->uwRunPeriod_1ms = 5;
   for( uint8_t i = DG_HallNet_RIS__HallLamps_0; i < NUM_HallNet_RIS_DATAGRAMS; i++ )
   {
      gstSData_HallNet_RIS.paiResendRate_1ms[i] = 5000;
   }

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run(struct st_module *pstThisModule)
{
   Receive();
   UnloadData();

   LoadData();
   Transmit();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
