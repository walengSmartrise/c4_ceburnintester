/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     26, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "sru.h"
#include "sru_e.h"
#include "sys.h"
#include "sdata.h"
#include <string.h>
#include "carData.h"
#include "groupnet_datagrams.h"
#include "groupnet.h"
#include "param_edit_protocol.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_SData_GroupNet =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/* Below are all the nodes in Group Network */
static struct st_sdata_control gstSData_GroupNet_RIS;

struct st_sdata_control * const gpstSData_GroupNet_RIS = &gstSData_GroupNet_RIS;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static const uint8_t ucNumDatagrams_GroupNet = NUM_GroupNet_RIS_DATAGRAMS;

struct st_sdata_local gstGroupNetSData_Config;

static CAN_MSG_T gstCAN_MSG_Tx;

static char *pasVersion;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define CAR_REQUEST_MAX_RESEND_INTERVAL_5MS        (20)
static void LoadDatagram_CarRequest(void)
{
   static uint8_t ucResendCounter_5ms;
   if ( !SDATA_DirtyBit_Get( gpstSData_GroupNet_RIS, DG_GroupNet_RIS__CAR_REQUEST) )
   {
      if(ucResendCounter_5ms < CAR_REQUEST_MAX_RESEND_INTERVAL_5MS)
      {
         ucResendCounter_5ms++;
      }
      else
      {
         un_sdata_datagram unDatagram;
         en_car_request eRequest = GetCarRequest_AnyCar();
         uint8_t bValidRequest = 0;
         switch(eRequest)
         {
            case CAR_REQUEST__RIS1_CAN1 ... CAR_REQUEST__RIS4_CAN1:
               {
                  uint8_t ucRequestedRiser = eRequest - CAR_REQUEST__RIS1_CAN1;
                  uint8_t ucLocalRiser = GetSRU_RiserBoardIndex();
                  if( ucLocalRiser == ucRequestedRiser )
                  {
                     bValidRequest = 1;
                     unDatagram.aui32[0] = eRequest;
                     unDatagram.aui32[1] = GetDebugBusOfflineCounter_CAN1();
                  }
               }
               break;
            case CAR_REQUEST__RIS1_CAN2 ... CAR_REQUEST__RIS4_CAN2:
               {
                  uint8_t ucRequestedRiser = eRequest - CAR_REQUEST__RIS1_CAN2;
                  uint8_t ucLocalRiser = GetSRU_RiserBoardIndex();
                  if( ucLocalRiser == ucRequestedRiser )
                  {
                     bValidRequest = 1;
                     unDatagram.aui32[0] = eRequest;
                     unDatagram.aui32[1] = GetDebugBusOfflineCounter_CAN2();
                  }
               }
               break;
            default: break;
         }

         if(bValidRequest)
         {
            SDATA_WriteDatagram( gpstSData_GroupNet_RIS,
                                 DG_GroupNet_RIS__CAR_REQUEST,
                                 &unDatagram
                                );
            SDATA_DirtyBit_Set(gpstSData_GroupNet_RIS, DG_GroupNet_RIS__CAR_REQUEST);
            ucResendCounter_5ms = 0;
         }
      }
   }
   else
   {
      ucResendCounter_5ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SharedData_GroupNet_Init()
{
   int iError = 0;
   gstGroupNetSData_Config.ucLocalNodeID = GetSRU_RiserBoardID();

   gstGroupNetSData_Config.ucNetworkID = SDATA_NET__GROUP;

   gstGroupNetSData_Config.ucNumNodes = NUM_GROUP_NET_NODES;

   gstGroupNetSData_Config.ucDestNodeID = 0x1F; /* All */

   iError = SDATA_CreateDatagrams( &gstSData_GroupNet_RIS, ucNumDatagrams_GroupNet );
   if ( iError )
   {
      while ( 1 )
      {
         // TODO: unable to allocate shared data
         SRU_Write_LED( enSRU_LED_Fault, 1 );
      }
   }

   gstCAN_MSG_Tx.DLC = 8;

}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Receive( void )
{
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Transmit( void )
{
   un_sdata_datagram unDatagram;
   int iError = 0;
   uint8_t ucDatagramID;

   uint16_t uwDatagram_Plus1 = SDATA_GetNextDatagramToSendIndex_Plus1(&gstSData_GroupNet_RIS );
   if( uwDatagram_Plus1
  && ( uwDatagram_Plus1 <= ucNumDatagrams_GroupNet ) )
   {
      ucDatagramID = uwDatagram_Plus1-1;
      SDATA_ReadDatagram( &gstSData_GroupNet_RIS,
                          ucDatagramID,
                          &unDatagram
                        );

      memcpy( gstCAN_MSG_Tx.Data, unDatagram.auc8, sizeof( unDatagram.auc8 ) );

      enum en_sdata_networks eNet = SDATA_NET__GROUP;
      enum en_group_net_nodes eLocalNode = GetSRU_RiserBoardID();
      GroupNet_DatagramID eID = GroupNet_LocalRiserIDToGlobalID( eLocalNode-GROUP_NET__RIS_1, ucDatagramID );
      uint16_t uwDest = GROUP_DEST__ALL;
      gstCAN_MSG_Tx.ID = GroupNet_EncodeCANMessageID(eID, eNet, eLocalNode, uwDest);

      CAN_BUFFER_ID_T   TxBuf = Chip_CAN_GetFreeTxBuf(LPC_CAN1);

      // Chip_can_send returns 0 for error and 1 for success. We need to invert this.
      iError = !Chip_CAN_Send(LPC_CAN1, TxBuf, &gstCAN_MSG_Tx );

      if ( iError )
      {
         if ( gstSData_GroupNet_RIS.uwLastSentIndex )
         {
            --gstSData_GroupNet_RIS.uwLastSentIndex;
         }
         else
         {
            gstSData_GroupNet_RIS.uwLastSentIndex = gstSData_GroupNet_RIS.uiNumDatagrams - 1;
         }
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadData( void )
{

}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData( void )
{
   un_sdata_datagram unDatagram;
   memset( &unDatagram, 0, sizeof(un_sdata_datagram));
   unDatagram.auc8[0] = GetLocalInputs_ThisNode();
   unDatagram.auc8[1] = GetFault();
   unDatagram.auc8[2] = GetSRU_RiserBoardID();// Unused
   unDatagram.auc8[3] = 0; //UNUSED
   unDatagram.auc8[4] = *(pasVersion+0);
   unDatagram.auc8[5] = *(pasVersion+1);
   unDatagram.auc8[6] = *(pasVersion+2);
   unDatagram.auc8[7] = *(pasVersion+3);
   SDATA_WriteDatagram( &gstSData_GroupNet_RIS,
                   (DG_GroupNet_RIS__INPUT_STATE),
                   &unDatagram
                    );

   /* For the security riser, the latched call packets are repurposed for security contacts.
    * The UP calls are the front opening contacts.
    * The DOWN calls are the rear opening contacts. */
   if( GetSRU_RiserBoardID() == GROUP_NET__RIS_3 )
   {
      LoadDatagram_LatchedCalls_Up_SecurityContactsF();
      LoadDatagram_LatchedCalls_Down_SecurityContactsR();
   }
   else
   {
      LoadDatagram_LatchedCalls_Up();
      LoadDatagram_LatchedCalls_Down();
   }

   LoadDatagram_CarRequest();
}

/*-----------------------------------------------------------------------------
Gets the last segment of the software version string for transmit on the group net
 -----------------------------------------------------------------------------*/
static void UpdateVersionString(void)
{
   uint8_t ucSize = sizeof(SOFTWARE_RELEASE_VERSION);
   uint8_t ucFirstIndex = 0;
   for(int8_t i = ucSize-1; i >= 0; i--)
   {
      char *ps = SOFTWARE_RELEASE_VERSION+i;
      if(*ps == '.')
      {
         ucFirstIndex = i+1;
         break;
      }
   }

   pasVersion = SOFTWARE_RELEASE_VERSION+ucFirstIndex;
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   //------------------------------------------------
   pstThisModule->uwInitialDelay_1ms = 5;
   pstThisModule->uwRunPeriod_1ms = 5;

   gstSData_GroupNet_RIS.bDisableHeartbeat = 1;

   /* Riser 3 is fixed as the security riser. Its UB is front security, its DB is rear security */
   if(GetSRU_RiserBoardID() == GROUP_NET__RIS_3)
   {
      gstSData_GroupNet_RIS.paiResendRate_1ms[DG_GroupNet_RIS__INPUT_STATE] = 1000;
      gstSData_GroupNet_RIS.paiResendRate_1ms[DG_GroupNet_RIS__LATCHED_HALL_CALLS_UP] = DISABLED_DATAGRAM_RESEND_RATE_MS;
      gstSData_GroupNet_RIS.paiResendRate_1ms[DG_GroupNet_RIS__LATCHED_HALL_CALLS_DN] = DISABLED_DATAGRAM_RESEND_RATE_MS;
      gstSData_GroupNet_RIS.paiResendRate_1ms[DG_GroupNet_RIS__HALLBOARD_STATUS] = DISABLED_DATAGRAM_RESEND_RATE_MS;
      gstSData_GroupNet_RIS.paiResendRate_1ms[DG_GroupNet_RIS__CAR_REQUEST] = DISABLED_DATAGRAM_RESEND_RATE_MS;
      gstSData_GroupNet_RIS.paiResendRate_1ms[DG_GroupNet_RIS__DispatchPanel] = DISABLED_DATAGRAM_RESEND_RATE_MS;
   }
   else
   {
      gstSData_GroupNet_RIS.paiResendRate_1ms[DG_GroupNet_RIS__INPUT_STATE] = 1000;
      gstSData_GroupNet_RIS.paiResendRate_1ms[DG_GroupNet_RIS__LATCHED_HALL_CALLS_UP] = DISABLED_DATAGRAM_RESEND_RATE_MS;
      gstSData_GroupNet_RIS.paiResendRate_1ms[DG_GroupNet_RIS__LATCHED_HALL_CALLS_DN] = DISABLED_DATAGRAM_RESEND_RATE_MS;
      gstSData_GroupNet_RIS.paiResendRate_1ms[DG_GroupNet_RIS__HALLBOARD_STATUS] = DISABLED_DATAGRAM_RESEND_RATE_MS;
      gstSData_GroupNet_RIS.paiResendRate_1ms[DG_GroupNet_RIS__CAR_REQUEST] = DISABLED_DATAGRAM_RESEND_RATE_MS;
      gstSData_GroupNet_RIS.paiResendRate_1ms[DG_GroupNet_RIS__DispatchPanel] = DISABLED_DATAGRAM_RESEND_RATE_MS;
   }

   UpdateVersionString();

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   //------------------------------------------------
   // Check for receive data every time.
   Receive();
   UnloadData();
   //------------------------------------------------
   // Send data.
   LoadData();
   Transmit();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
