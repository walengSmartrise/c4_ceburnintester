/******************************************************************************
 *
 * @file
 * @brief
 * @version  V1.00
 * @date
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "sru_e.h"
#include "sys.h"
#include <string.h>
#include "ring_buffer.h"

#include "GlobalData.h"
#include "sdata.h"

#include "carData.h"
#include "groupnet_datagrams.h"
#include "groupnet.h"
#include "shieldData.h"
#include "masterCommand.h"
#include "param_edit_protocol.h"
#include "XRegData.h"
#include "ddCommand.h"
#include "expData.h"
#include "hallSecurity.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_CAN =
{
   .pfnInit = Init,
   .pfnRun = Run,
};
RINGBUFF_T RxPositionRingBuffer;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

#define MAX_RING_BUFFER_READ           (4)
#define LIMIT_UNLOAD_CAN_CYCLES       (16)
#define EXTRACT_NET_ID(x)      (( x >> 25 ) & 0x0F )
#define EXTRACT_SRC_ID(x)      (( x >> 20 ) & 0x1F )
#define EXTRACT_DEST_ID(x)     (( x >> 15 ) & 0x1F )
#define EXTRACT_DATAGRAM_ID(x) (( x >> 10 ) & 0x1F )

#define OFFLINE_TIMEOUT_5MS         (1000U)

/* Since there won't always be a device communicating on CAN2,
 * initialize the offline timer with a starter value and suppress
 * communication errors for the bus if not communication was ever detected */
#define OFFLINE_TIMER_COM_NOT_DETECTED          (0xFFFF)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *-----------------------------------------------------------------------------*/
// Cleared if valid packet received on CAN1 for an IO Board
static uint16_t uwOfflineTimeout_CAN1_5ms;
static uint16_t uwOfflineTimeout_CAN2_5ms = OFFLINE_TIMER_COM_NOT_DETECTED;

static uint16_t uwBusErrorCounter_CAN1;
static uint16_t uwBusErrorCounter_CAN2;

static uint8_t bNewPacket_CAN1;
static uint8_t bNewPacket_CAN2;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Returns bus error counter
 -----------------------------------------------------------------------------*/
uint16_t GetDebugBusOfflineCounter_CAN1()
{
   return uwBusErrorCounter_CAN1;
}
uint16_t GetDebugBusOfflineCounter_CAN2()
{
   return uwBusErrorCounter_CAN2;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ReceiveGroupCarDatagram(enum en_group_net_nodes eCarID,
                                    en_group_net_mrb_datagrams eLocalDatagramID,
                                    un_sdata_datagram *punDatagram)
{
   ClrCarOfflineTimer_ByCar(eCarID);
   switch(eLocalDatagramID)
   {
      case DG_GroupNet_MRB__MappedInputs1:
         UnloadDatagram_InputMap1(eCarID, punDatagram);
         break;
      case DG_GroupNet_MRB__MappedInputs2:
         UnloadDatagram_InputMap2(eCarID, punDatagram);
         break;
      case DG_GroupNet_MRB__MappedInputs3:
         UnloadDatagram_InputMap3(eCarID, punDatagram);
         break;
      case DG_GroupNet_MRB__MappedOutputs1:
         UnloadDatagram_OutputMap1(eCarID, punDatagram);
         break;
      case DG_GroupNet_MRB__MappedOutputs2:
         UnloadDatagram_OutputMap2(eCarID, punDatagram);
         break;
      case DG_GroupNet_MRB__CAR_STATUS:
         UnloadDatagram_CarStatus(eCarID, punDatagram);
         break;
      case DG_GroupNet_MRB__CarData:
         UnloadDatagram_CarData(eCarID, punDatagram);
         break;
      case DG_GroupNet_MRB__CarOperation:
         UnloadDatagram_CarOperation(eCarID, punDatagram);
         break;
      case DG_GroupNet_MRB__MasterCommand:
         if( GetSRU_RiserBoardID() != GROUP_NET__RIS_3 )
         {
            UnloadDatagram_MasterCommand(eCarID, punDatagram);
         }
         break;
      default:
         break;
   }
}
/*
 * Riser board commands
 */
static void RecieveRiserCommand( uint8_t ucRiser, uint8_t ucDatagramID, un_sdata_datagram *unDatagram )
{
   if( ucRiser < MAX_NUM_RISER_BOARDS )
   {
      switch (ucDatagramID) {
         case  DG_GroupNet_RIS__LATCHED_HALL_CALLS_UP:
            if( ucRiser == SECURITY_RISER_INDEX )
            {
               UnloadDatagram_RiserSecurityContacts_F(unDatagram);
            }
            else
            {
               UnloadDatagram_RiserLatchedHallCalls( ucRiser, HC_DIR__UP, unDatagram );
            }
            RiserBoard_ClearOfflineCounter(ucRiser);
            break;
         case  DG_GroupNet_RIS__LATCHED_HALL_CALLS_DN:
            if( ucRiser == SECURITY_RISER_INDEX )
            {
               UnloadDatagram_RiserSecurityContacts_R(unDatagram);
            }
            else
            {
               UnloadDatagram_RiserLatchedHallCalls( ucRiser, HC_DIR__DOWN, unDatagram );
            }
            RiserBoard_ClearOfflineCounter(ucRiser);
            break;
         case  DG_GroupNet_RIS__INPUT_STATE:
            CheckFor_AddressFault_Riser( ucRiser, unDatagram );
            RiserBoard_ClearOfflineCounter(ucRiser);
            break;
         case  DG_GroupNet_RIS__HALLBOARD_STATUS:
            UnloadDatagram_RiserHallboardStatus_GN( ucRiser, unDatagram );
            RiserBoard_ClearOfflineCounter(ucRiser);
            break;
         default:
            break;
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ReceiveShieldDatagram( en_group_net_shield_datagrams eLocalDatagramID,
                                    un_sdata_datagram *punDatagram)
{
   uint8_t bSuccess = 1;
   switch(eLocalDatagramID)
   {
      case DG_GroupNet_SHIELD__SyncTime:
//         UnloadDatagram_SyncTime(punDatagram);
         break;

      case DG_GroupNet_SHIELD__RemoteCommand:
//         UnloadDatagram_SaveParameter(punDatagram);
         break;

      case DG_GroupNet_SHIELD__LatchCarCall:
//         UnloadDatagram_LatchCarCall(punDatagram);
         break;

      case DG_GroupNet_SHIELD__LatchHallCall:
         if( GetSRU_RiserBoardID() != GROUP_NET__RIS_3 )
         {
            UnloadDatagram_LatchHallCall(punDatagram);
         }
         break;

      case DG_GroupNet_SHIELD__ClearHallCall:
         if( GetSRU_RiserBoardID() != GROUP_NET__RIS_3 )
         {
            UnloadDatagram_ClearHallCall(punDatagram);
         }
         break;

      default:
         bSuccess = 0;
         break;
   }
   if(bSuccess)
   {
      ShieldData_ClrTimeoutCounter();
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ReceiveDDMDatagram( en_group_net_ddm_datagrams eLocalDatagramID,
                                un_sdata_datagram *punDatagram)
{
   uint8_t bSuccess = 1;
   switch(eLocalDatagramID)
   {
      case DG_GroupNet_DDM__LATCH_HC:
         if( GetSRU_RiserBoardID() != GROUP_NET__RIS_3 )
         {
            DDC_UnloadDatagram_LatchHC(punDatagram);
         }
         break;
      case DG_GroupNet_DDM__LATCH_CC:
//         DDC_UnloadDatagram_LatchCC(punDatagram);
         break;
      case DG_GroupNet_DDM__HEARTBEAT:
         break;
      default:
         bSuccess = 0;
         break;
   }
   if(bSuccess)
   {
      //TODO clear timeout
   }
}
/*-----------------------------------------------------------------------------
   Check for CAN Bus offline and reset if offline
 -----------------------------------------------------------------------------*/
static void CheckFor_CANBusOffline()
{
   if( Chip_CAN_GetGlobalStatus( LPC_CAN1 ) & CAN_GSR_BS ) // Bus offline if 1
   {
      Chip_CAN_SetMode( LPC_CAN1, CAN_RESET_MODE, ENABLE );
      __NOP();
      __NOP();
      __NOP();
      Chip_CAN_SetMode( LPC_CAN1, CAN_RESET_MODE, DISABLE );
      SetFault( ERROR_RIS_EXP__BUS_RESET_CAN1 );
   }

   if( Chip_CAN_GetGlobalStatus( LPC_CAN2 ) & CAN_GSR_BS ) // Bus offline if 1
   {
      Chip_CAN_SetMode( LPC_CAN2, CAN_RESET_MODE, ENABLE );
      __NOP();
      __NOP();
      __NOP();
      Chip_CAN_SetMode( LPC_CAN2, CAN_RESET_MODE, DISABLE );
      SetFault( ERROR_RIS_EXP__BUS_RESET_CAN2 );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckFor_CommunicationLoss_EXP()
{
   if( GetSRU_ExpansionMasterID() )
   {
      if( uwOfflineTimeout_CAN1_5ms > ( FAULT_TIMEOUT_CAR_COM_MS/gstMod_CAN.uwRunPeriod_1ms ) )
      {
         SetFault( ERROR_RIS_EXP__COM_CAR );
      }
      else
      {
         uwOfflineTimeout_CAN1_5ms++;
      }

      if( uwOfflineTimeout_CAN2_5ms > ( FAULT_TIMEOUT_SLAVE_COM_MS/gstMod_CAN.uwRunPeriod_1ms ) )
      {
         /* Suppress the offline error for CAN2 if no communication has ever been detected on the bus.
          * There is not always a device on this bus. */
         if( uwOfflineTimeout_CAN2_5ms != OFFLINE_TIMER_COM_NOT_DETECTED )
         {
            SetFault( ERROR_RIS_EXP__COM_SLAVE );
         }
      }
      else
      {
         uwOfflineTimeout_CAN2_5ms++;
      }
   }
   else
   {
      if( uwOfflineTimeout_CAN1_5ms > ( FAULT_TIMEOUT_MASTER_COM_MS/gstMod_CAN.uwRunPeriod_1ms ) )
      {
         SetFault( ERROR_RIS_EXP__COM_MASTER );
      }
      else
      {
         uwOfflineTimeout_CAN1_5ms++;
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckFor_CommunicationLoss_RIS()
{
   if( uwOfflineTimeout_CAN1_5ms > ( FAULT_TIMEOUT_GROUP_COM_MS/gstMod_CAN.uwRunPeriod_1ms ) )
   {
      SetFault( ERROR_RIS_EXP__COM_GROUP );
   }
   else
   {
      uwOfflineTimeout_CAN1_5ms++;
   }

   if( uwOfflineTimeout_CAN2_5ms > ( FAULT_TIMEOUT_HALL_COM_MS/gstMod_CAN.uwRunPeriod_1ms ) )
   {
      /* Suppress the offline error for CAN2 if no communication has ever been detected on the bus.
       * There is not always a device on this bus. */
      if( uwOfflineTimeout_CAN2_5ms != OFFLINE_TIMER_COM_NOT_DETECTED )
      {
         SetFault( ERROR_RIS_EXP__COM_HALL );
      }
   }
   else
   {
      uwOfflineTimeout_CAN2_5ms++;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Receive_Packet(CAN_MSG_T *pstRxMsg)
{
   un_sdata_datagram unDatagram;
   uint8_t ucNetworkDatagramID = ExtractFromCAN_DatagramID(pstRxMsg->ID);

   memcpy(&unDatagram, &pstRxMsg->Data, sizeof(un_sdata_datagram) );
   InsertSharedDatagram_ByDatagramID( &unDatagram, ucNetworkDatagramID);
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = 5;

   ExpData_Initialize();

   return 0;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
/* Check for expansion boards with overlapping input address space */
static uint8_t CheckForDuplicateSlaveAddress(un_sdata_datagram *punDatagram)
{
   uint8_t bDuplicate = 0;
   uint8_t ucLocalSlaveID = SRU_Read_DIP_Bank(enSRU_DIP_BANK_B) & 0x07;
   uint8_t ucRxSlaveID = punDatagram->auc8[1] & 0x07;
   en_io_board_type eType = punDatagram->auc8[4];
   if(eType == IO_BOARD_TYPE__Input)
   {
      /* This is a 24-input board meaning it will take
       *  the space of 3 standard exp IO boards */
      for(uint8_t i = 0; i < 3; i++)
      {
         uint8_t ucOccupiedID = ucRxSlaveID+i;
         if(ucLocalSlaveID == ucOccupiedID)
         {
            bDuplicate = 1;
            break;
         }
      }
   }
   else
   {
      if(ucRxSlaveID == ucLocalSlaveID)
      {
         bDuplicate = 1;
      }
   }

   return bDuplicate;
}
static void UnloadCAN_CNA_Master( void )
{
    CAN_MSG_T tmp;
    // Car Net
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN1_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
          uwOfflineTimeout_CAN1_5ms = 0;
          un_sdata_datagram unDatagram;
          memcpy( unDatagram.auc8, tmp.Data, sizeof ( tmp.Data ) );

          // Unload or forward packet
          Receive_Packet(&tmp);
          bNewPacket_CAN1 = 1;

          uint8_t ucDatagramID = ExtractFromCAN_DatagramID(tmp.ID) ;
          CheckFor_AddressFault_Master( &unDatagram, ucDatagramID );
       }
    }

    // IO Net
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN2_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
          uwOfflineTimeout_CAN2_5ms = 0;
          un_sdata_datagram unDatagram;
          uint8_t ucDatagramID = (tmp.ID >> 21) & 0xFF;
          uint8_t ucNetworkID = (tmp.ID >> 18) & 0x7;
          uint8_t ucNodeID = (tmp.ID >> 14) & 0xF;
          uint16_t uwDestinationBitmap = tmp.ID & 0xFFFF;
          if( ( ucNetworkID == SDATA_NET__IO )
           && ( ucNodeID == SYS_NODE__EXP_S )
           && ( ucDatagramID < NUM_IONet_Slave_DATAGRAMS )
           && ( ( uwDestinationBitmap & NODE__EXP ) == NODE__EXP ) )
          {
              memcpy( unDatagram.auc8, tmp.Data, sizeof ( tmp.Data ) );

              SDATA_WriteDatagram( gastIONetSData_Nodes[IO_NET__SLAVE],
                                   ucDatagramID,
                                   &unDatagram );
              ExpData_UnloadSlaveMessage(&unDatagram, ucDatagramID);
              if(CheckForDuplicateSlaveAddress(&unDatagram))
              {
                 SetFault(ERROR_RIS_EXP__ADDRESS);
              }
          }
          bNewPacket_CAN2 = 1;
       }
    }
}
static void UnloadCAN_CNA_Slave( void )
{
    CAN_MSG_T tmp;
    // parent Net
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN1_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
          uwOfflineTimeout_CAN1_5ms = 0;
          un_sdata_datagram unDatagram;
          uint8_t ucDatagramID = (tmp.ID >> 21) & 0xFF;
          uint8_t ucNetworkID = (tmp.ID >> 18) & 0x7;
          uint8_t ucNodeID = (tmp.ID >> 14) & 0xF;
          uint16_t uwDestinationBitmap = tmp.ID & 0xFFFF;
          if( ( ucNetworkID == SDATA_NET__IO )
           && ( ucNodeID == SYS_NODE__EXP )
           && ( ucDatagramID < NUM_IONet_Master_DATAGRAMS )
           && ( ( uwDestinationBitmap & NODE__EXP_S ) == NODE__EXP_S ) )
          {
              memcpy( unDatagram.auc8, tmp.Data, sizeof ( tmp.Data ) );

              SDATA_WriteDatagram( gastIONetSData_Nodes[IO_NET__MASTER],
                                   ucDatagramID,
                                   &unDatagram );
          }
          else if( ( ucNetworkID == SDATA_NET__IO )
                && ( ucNodeID == SYS_NODE__EXP_S )
                && ( ucDatagramID < NUM_IONet_Slave_DATAGRAMS )
                && ( ( uwDestinationBitmap & NODE__EXP ) == NODE__EXP ) )
          {
             memcpy( unDatagram.auc8, tmp.Data, sizeof ( tmp.Data ) );

             if(CheckForDuplicateSlaveAddress(&unDatagram))
             {
                SetFault(ERROR_RIS_EXP__ADDRESS);
             }
          }
       }
    }
}
static void UnloadCAN_CNA( void )
{
   if(GetSRU_ExpansionMasterID())
   {
      UnloadCAN_CNA_Master();
   }
   else
   {
      UnloadCAN_CNA_Slave();
   }
}
static void UnloadCAN_RIS( void )
{
    CAN_MSG_T tmp;
    // Read data from ring buffer till it is empty
    // Devices on CAN1: RIS[1:4], MRB[CAR1:CAR8], BBShield
    // Group Net
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN1_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
         // Unload or forward packet
          uwOfflineTimeout_CAN1_5ms = 0;
          un_sdata_datagram unDatagram;
          enum en_group_net_nodes ucSourceID      = ExtractFromBusID_SourceID(tmp.ID);
          GroupNet_DatagramID ucDatagramID    = ExtractFromBusID_DatagramID(tmp.ID);
          uint16_t uwDestinationBitmap = ExtractFromBusID_DestinationBitmap(tmp.ID);
          // Map the old system datagram ID to the new system datagram ID and insert into the data table
          memcpy( unDatagram.auc8, tmp.Data, sizeof( tmp.Data ) );
          // Check for valid destination
          if( uwDestinationBitmap & GROUP_DEST__ALL_RIS )
          {
             if( ucSourceID <= GROUP_NET__CAR_8 )
             {
                 // Mapping a car data gram to the new system. The new system ID is calculated as follows.
                 // First extract the CAR ID and multiply by the number of datagrams a car can produce,
                 // hard coded to 25. Then add the old system datagram ID to the above. This results in a
                 // unique number for converting from the old system to the new system.
                 InsertGroupNetDatagram_ByDatagramID(&unDatagram ,ucDatagramID);

                 // Unload some DG on receive to prevent missing multiplexed data
                 en_group_net_mrb_datagrams eMRBDatagramID = GroupNet_GlobalIDToLocalNodeID(ucDatagramID);
                 ReceiveGroupCarDatagram( ucSourceID, eMRBDatagramID, &unDatagram );
             }
             //Risers
             else if (ucSourceID <= GROUP_NET__RIS_4)
             {
                InsertGroupNetDatagram_ByDatagramID(&unDatagram ,ucDatagramID);
                en_group_net_ris_datagrams eLocalDatagramID = GroupNet_GlobalIDToLocalNodeID( ucDatagramID );
                if( eLocalDatagramID < NUM_GroupNet_RIS_DATAGRAMS )
                {
                   uint8_t ucRiserIndex = ucSourceID - GROUP_NET__RIS_1;
                   RecieveRiserCommand( ucRiserIndex, eLocalDatagramID, GetRiserDatagram(ucDatagramID));
                }
             }
             else if(ucSourceID == GROUP_NET__SHIELD)
             {
                InsertGroupNetDatagram_ByDatagramID(&unDatagram ,ucDatagramID);
                en_group_net_shield_datagrams eShieldDatagramID = GroupNet_GlobalIDToLocalNodeID(ucDatagramID);
                ReceiveShieldDatagram( eShieldDatagramID, &unDatagram );
             }
             else if( ucSourceID == GROUP_NET__XREG )
             {
                InsertGroupNetDatagram_ByDatagramID(&unDatagram ,ucDatagramID);

                /* Currently only one datagram ID expected from the XREG */
                UnloadDatagram_XRegStatus(&unDatagram);
             }
             else if( ucSourceID == GROUP_NET__DDM )
             {
                InsertGroupNetDatagram_ByDatagramID(&unDatagram ,ucDatagramID);
                en_group_net_ddm_datagrams eLocalDatagramID = GroupNet_GlobalIDToLocalNodeID(ucDatagramID);
                if (eLocalDatagramID < NUM_GroupNet_DDM_DATAGRAMS)
                {
                   ReceiveDDMDatagram( eLocalDatagramID, &unDatagram );
                }
             }
          }
       }
    }
    //Devices on CAN2: Hall Boards
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN2_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
          uwOfflineTimeout_CAN2_5ms = 0;

          uint8_t ucNetworkID = EXTRACT_HALLNET_NET_ID( tmp.ID );
          uint8_t ucDatagramID = EXTRACT_HALLNET_DATAGRAM_ID( tmp.ID );
          uint8_t ucSourceID = EXTRACT_HALLNET_SRC_ID( tmp.ID );
          uint16_t uwDIPs = EXTRACT_HALLNET_DIP_ID( tmp.ID );
          uint8_t bExtendedHallBoardType = (tmp.Data[0] >> 7) & 1;

          if( bExtendedHallBoardType )
          {
             uint8_t ucExtraDIPs = EXTRACT_HALLNET_DEST_ID(tmp.ID);
             uwDIPs |= ( ucExtraDIPs & 1 ) << 10;
             SetNewHallBoardTypeFlag();
          }
          if ( ( ucNetworkID == SDATA_NET__HALL )
            && ( ucSourceID == HALL_NET__HB )
            && ( ucDatagramID == DG_HallNet_HB__Status )
            && ( uwDIPs < GetHallBoard_MaxNumberOfBoards() ) )
          {
             UnloadDatagram_HallboardStatus_HN( uwDIPs, &tmp.Data[0] );
          }
       }
    }

}
/*-----------------------------------------------------------------------------
   Updates the CAN bus error counters viewed via View Debug Data menus
 -----------------------------------------------------------------------------*/
static void UpdateDebugBusErrorCounter()
{
   static uint8_t ucLastCountRx_CAN1;
   static uint8_t ucLastCountTx_CAN1;
   static uint8_t ucLastCountRx_CAN2;
   static uint8_t ucLastCountTx_CAN2;
   uint32_t uiStatus = Chip_CAN_GetGlobalStatus( LPC_CAN1 );
   uint8_t ucCountRx = (uiStatus >> 16) & 0xFF;
   uint8_t ucCountTx = (uiStatus >> 24) & 0xFF;
   if( ( ucCountRx > ucLastCountRx_CAN1 )
    || ( ucCountTx > ucLastCountTx_CAN1 ))
   {
       uwBusErrorCounter_CAN1++;
   }
   ucLastCountRx_CAN1 = ucCountRx;
   ucLastCountTx_CAN1 = ucCountTx;

   uiStatus = Chip_CAN_GetGlobalStatus( LPC_CAN2 );
   ucCountRx = (uiStatus >> 16) & 0xFF;
   ucCountTx = (uiStatus >> 24) & 0xFF;
   if( ( ucCountRx > ucLastCountRx_CAN2 )
    || ( ucCountTx > ucLastCountTx_CAN2 ))
   {
       uwBusErrorCounter_CAN2++;
   }
   ucLastCountRx_CAN2 = ucCountRx;
   ucLastCountTx_CAN2 = ucCountTx;
}
/*-----------------------------------------------------------------------------


 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
    // This is the only place the can periphs are init.
    switch(GetSRU_Deployment())
    {
        case enSRU_DEPLOYMENT__CN:
            CheckFor_CommunicationLoss_EXP();
            UnloadCAN_CNA();
            ExpData_UpdateLocalData();
            ExpData_UpdateOfflineTimers(pstThisModule->uwRunPeriod_1ms);
            break;
        case enSRU_DEPLOYMENT__RIS:
            CheckFor_CommunicationLoss_RIS();
            UnloadCAN_RIS();
            RiserBoard_UpdateOfflineCounters(pstThisModule->uwRunPeriod_1ms);
            break;
        default:
            break;
    }
    UpdateDebugBusErrorCounter();
    CheckFor_CANBusOffline();

    /* Toggle LEDs when new packet received if in test mode */
    if( GetSRU_TestMode() )
    {
       static uint8_t bFaultLED;
       static uint8_t bHeartbeatLED;
       if(bNewPacket_CAN1)
       {
          bNewPacket_CAN1 = 0;
          bHeartbeatLED ^= 1;
       }
       if(bNewPacket_CAN2)
       {
          bNewPacket_CAN2 = 0;
          bFaultLED ^= 1;
       }
#if ENABLE_SR3031F
       SRU_Write_LED( enSRU_LED_Extra, bHeartbeatLED );// LED A
       SRU_Write_LED( enSRU_LED_Heartbeat, bFaultLED );// LED B
#else
       SRU_Write_LED( enSRU_LED_Heartbeat, bHeartbeatLED );
       SRU_Write_LED( enSRU_LED_Fault, bFaultLED );
#endif
    }

    return 0;
}
