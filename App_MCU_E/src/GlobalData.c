
#include <sru_e.h>
#include "sru.h"
#include "GlobalData.h"
#include <stdint.h>
#include "sys.h"
#include "carData.h"

static uint8_t gucIOExt_Master;
/*----------------------------------------------------------------------------
   If no slave DIPS are set DIP[1,3], then this is a slave EXP board. Returns 0.
   Else, this is a master IO board with ID set by DIP[4,7] + 1
 *----------------------------------------------------------------------------*/
inline void SetSRU_ExpansionMasterID( void )
{
   uint8_t ucDIP_Switches = SRU_Read_DIP_Bank( enSRU_DIP_BANK_B );
   uint8_t ucSlaveBits = ucDIP_Switches & 0x7;
   uint8_t ucMasterBits = ( ucDIP_Switches >> 3 ) & 0xF;
   if( !ucSlaveBits || SRU_Read_DIP_Bank( enSRU_DIP_BANK_B )== 0xFF )
   {
      gucIOExt_Master = ucMasterBits+1;
   }
   else
   {
      gucIOExt_Master = 0;
   }
}
/*----------------------------------------------------------------------------
   Returns IO board Master ID
 *----------------------------------------------------------------------------*/
inline uint8_t GetSRU_ExpansionMasterID( void )
{
     return gucIOExt_Master;
}
/*----------------------------------------------------------------------------
   Returns IO board slave ID - DIP numbers [1,3]
 *----------------------------------------------------------------------------*/
inline uint8_t GetSRU_ExpansionSlaveID()
{
   uint8_t ucDIP_Switches = SRU_Read_DIP_Bank( enSRU_DIP_BANK_B ) & 0x7;
   if(ucDIP_Switches)
   {
      ucDIP_Switches -=1;
   }
   else if(gucIOExt_Master)
   {
      ucDIP_Switches = 0;
   }

   return ucDIP_Switches;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static enum en_group_net_nodes eRiserID = GROUP_NET__RIS_1;
inline void SetSRU_RiserBoardID()
{
   eRiserID = ( SRU_Read_DIP_Bank( enSRU_DIP_BANK_B ) & 0x03 ) + GROUP_NET__RIS_1;
}
inline enum en_group_net_nodes GetSRU_RiserBoardID()
{
   return eRiserID;
}
/* Returns the riser board's index, valid 0 to 3. */
inline uint8_t GetSRU_RiserBoardIndex()
{
   return ( eRiserID-GROUP_NET__RIS_1 ) & 0x03;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static en_io_board_type eIOBoardType;
inline en_io_board_type GetIOBoardType()
{
   return eIOBoardType;
}
inline void SetIOBoardType(en_io_board_type eBoardType)
{
   eIOBoardType = eBoardType;
}

/*----------------------------------------------------------------------------
   Flag signaling that a packet from an new style hall board has been received. Pre SR1060G

   This means the backwards compatible support code should be bypassed and the
   increased floor and hall board address limits should be used.
 *----------------------------------------------------------------------------*/
static uint8_t bNewStyleHallBoard;
inline uint8_t GetNewHallBoardTypeFlag(void)
{
   return bNewStyleHallBoard;
}
inline void SetNewHallBoardTypeFlag(void)
{
   bNewStyleHallBoard = 1;
}
inline uint8_t GetHallBoard_MaxNumberOfFloors(void)
{
   if(bNewStyleHallBoard)
   {
      return NEW_HALL_BOARD_MAX_FLOOR_LIMIT;
   }
   else if( CarData_GetExtFloorLimitFlag_AnyCar() )
   {
      return MAX_NUM_FLOORS;
   }
   else
   {
      return MAX_NUM_FLOORS_REDUCED;
   }
}
inline uint16_t GetHallBoard_MaxNumberOfBoards(void)
{
   if(bNewStyleHallBoard)
   {
      return MAX_NUM_HALLBOARDS;
   }
   else
   {
      return MAX_NUM_HALLBOARDS_OLD;
   }
}
