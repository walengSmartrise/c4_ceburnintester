/******************************************************************************
 *
 * @file     mod_sdata_io_net.c
 * @brief    Keith Soneda
 * @version  V1.00
 * @date     Sept 15 2016
 *
 * @note   Network between daisy chained IO nodes where
 *         this device plays one of the following rolls
 *         depending on its DIP configuration.
 *
 *          Roles:
 *             Slave:
 *                - Receives outputs forwarded from the Car Net.
 *                - Sends inputs to master node, originating from
 *                  slave IO boards in its chain.
 *             Master:
 *                - Receives inputs from slave IO boards in its chain.
 *                - Sends outputs to slave nodes,
 *                  originating from the car net.
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sru_e.h"
#include "sys.h"
#include "sdata.h"
#include "GlobalData.h"
#include "expData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_SData_IONet =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/* Below are all the nodes in IO Network */
static struct st_sdata_control gstSData_IONet_Master;
static struct st_sdata_control gstSData_IONet_Slave;

struct st_sdata_control * gastIONetSData_Nodes[ NUM_IO_NET_NODES ] =
{
   &gstSData_IONet_Master,
   &gstSData_IONet_Slave,
};

uint8_t gaucIONetPerNodeNumDatagrams[ NUM_IO_NET_NODES ] =
{
   NUM_IONet_Master_DATAGRAMS,
   NUM_IONet_Slave_DATAGRAMS,
};

struct st_sdata_local gstIONetSData_Config;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define SLAVE_RESEND_RATE_5MS       (200)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static CAN_MSG_T gstCAN_MSG_Tx;
static un_sdata_datagram unTxDatagram;
static uint8_t ucDatagramID;
static uint8_t bResendDatagram; // Transmit
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Checks if datagram has changed or timer has elapsed and sets bit to transmit
 -----------------------------------------------------------------------------*/
static void CheckFor_ResendDatagram()
{
   static un_sdata_datagram unLastDatagram;
   static uint16_t uwResendTimer_5ms;

   uint8_t bDiff = (unTxDatagram.aui32[0] != unLastDatagram.aui32[0])
                || (unTxDatagram.aui32[1] != unLastDatagram.aui32[1]);
   if(bDiff)
   {
      uwResendTimer_5ms = 0;
      memcpy( &unLastDatagram, &unTxDatagram, sizeof(un_sdata_datagram));
      bResendDatagram = 1;
   }
   else if( ++uwResendTimer_5ms >= SLAVE_RESEND_RATE_5MS )
   {
      uwResendTimer_5ms = 0;
      bResendDatagram = 1;
   }
}

/*-----------------------------------------------------------------------------

   Initialize shared data used by MCU_A.

 -----------------------------------------------------------------------------*/
void SharedData_IONet_Init()
{
   int iError;
   int i;

   //--------------------------------------------------------
   gstIONetSData_Config.ucNetworkID = SDATA_NET__IO;

   gstIONetSData_Config.ucNumNodes = NUM_IO_NET_NODES;

   if(GetSRU_ExpansionMasterID())
   {
      gstIONetSData_Config.ucLocalNodeID = IO_NET__MASTER;
      gstIONetSData_Config.ucDestNodeID = IO_NET__SLAVE;
   }
   else
   {
      gstIONetSData_Config.ucLocalNodeID = IO_NET__SLAVE;
      gstIONetSData_Config.ucDestNodeID = IO_NET__MASTER;
   }

   //--------------------------------------------------------
   for ( i = 0; i < gstIONetSData_Config.ucNumNodes; ++i )
   {
      iError = SDATA_CreateDatagrams( gastIONetSData_Nodes[ i ],
                                      gaucIONetPerNodeNumDatagrams[ i ]
                                    );

      if ( iError )
      {
         while ( 1 )
         {
            // TODO: unable to allocate shared data
            SRU_Write_LED( enSRU_LED_Fault, 1 );
         }
      }
   }

   gstCAN_MSG_Tx.DLC = 8;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Receive( void )
{}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Transmit( void )
{
   int iError = 1;
   //If Master, then auto forward from CAR net
   if(GetSRU_ExpansionMasterID())
   {
      if(bResendDatagram)
      {
         ucDatagramID = 0;
         Sys_Nodes eLocalNodeID = GetSystemNodeID();
         uint16_t uwDestinationBitmap = NODE__EXP_S;

         memcpy( gstCAN_MSG_Tx.Data, unTxDatagram.auc8, sizeof( unTxDatagram.auc8 ) );
         gstCAN_MSG_Tx.DLC = 8;
         gstCAN_MSG_Tx.Type = 0;
         gstCAN_MSG_Tx.ID = 1 << 30;
         gstCAN_MSG_Tx.ID |= ucDatagramID << 21;
         gstCAN_MSG_Tx.ID |= (gstIONetSData_Config.ucNetworkID & 0x07) << 18;
         gstCAN_MSG_Tx.ID |= (eLocalNodeID & 0x0F) << 14;
         gstCAN_MSG_Tx.ID |= uwDestinationBitmap;

         // Return of 0 means error. Return of not 0 means success. This is opposite of our logic.
         iError = !CAN2_LoadToRB(&gstCAN_MSG_Tx);

         if(!iError)
         {
            bResendDatagram = 0;
         }
      }

      // Polls ring buffer for data and loads into CAN hardware buffer
      CAN2_FillHardwareBuffer();
   }
   //If slave, then determine datagramID for local and transmit
   else
   {
      if(bResendDatagram)
      {
         ucDatagramID = GetSRU_ExpansionSlaveID();
         Sys_Nodes eLocalNodeID = GetSystemNodeID();
         uint16_t uwDestinationBitmap = NODE__EXP;

         memcpy( gstCAN_MSG_Tx.Data, unTxDatagram.auc8, sizeof( unTxDatagram.auc8 ) );
         gstCAN_MSG_Tx.DLC = 8;
         gstCAN_MSG_Tx.Type = 0;
         gstCAN_MSG_Tx.ID = 1 << 30;
         gstCAN_MSG_Tx.ID |= ucDatagramID << 21;
         gstCAN_MSG_Tx.ID |= (gstIONetSData_Config.ucNetworkID & 0x07) << 18;
         gstCAN_MSG_Tx.ID |= (eLocalNodeID & 0x0F) << 14;
         gstCAN_MSG_Tx.ID |= uwDestinationBitmap;

         // Return of 0 means error. Return of not 0 means success. This is opposite of our logic.
         iError = !CAN1_LoadToRB(&gstCAN_MSG_Tx);

         if(!iError)
         {
            bResendDatagram = 0;
         }
      }
      // Polls ring buffer for data and loads into CAN hardware buffer
      CAN1_FillHardwareBuffer();
   }

}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData( void )
{
   //If master, then auto forward
   if(GetSRU_ExpansionMasterID())
   {
      memset( &unTxDatagram, 0, sizeof( un_sdata_datagram ) );
      unTxDatagram.auc8[0] = GetLocalOutputs_ThisNode();
      for( uint8_t i = 0; i < NUM_IONet_Slave_DATAGRAMS; i++ )
      {
         unTxDatagram.auc8[i+1] = GetSlaveOutputs( i );
      }
   }
   //If slave, then load local inputs
   else
   {
      memset( &unTxDatagram, 0, sizeof( un_sdata_datagram ) );
      unTxDatagram.auc8[0] = GetLocalInputs_ThisNode();
      unTxDatagram.auc8[1] = SRU_Read_DIP_Bank( enSRU_DIP_BANK_B );
      unTxDatagram.auc8[2] = 0; // Unused to maintain backwards compatibility with v1.02.54
      unTxDatagram.auc8[3] = 0; // Unused to maintain backwards compatibility with v1.02.54
      unTxDatagram.auc8[4] = GetIOBoardType();
      unTxDatagram.auc8[5] = 0; // Unused (Used for 24 input boards)
      unTxDatagram.auc8[6] = 0; // Unused (Used for 24 input boards)
      unTxDatagram.auc8[7] = GetFault();
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadData( void )
{
   un_sdata_datagram unDatagram;
   if(GetSRU_ExpansionMasterID())
   {
      // Moved to mod_can.c
   }
   // If slave, unload local outputs
   else
   {
      uint8_t ucSlaveID = GetSRU_ExpansionSlaveID();//Limited up to 7 slaves
      SDATA_ReadDatagram( &gstSData_IONet_Master,
                           DG_IONet_Master_Outputs,
                           &unDatagram
                         );
      SetLocalOutputs_ThisNode(unDatagram.auc8[ ucSlaveID + 1 ]);
   }

}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{

   //------------------------------------------------
   pstThisModule->uwInitialDelay_1ms = 5;
   pstThisModule->uwRunPeriod_1ms = 5;

   return 0;
}

/*-----------------------------------------------------------------------------

                  MSB   x
                        x     4 bits for Network ID (global enum)
                        x
                        x
                      -----
                        x
                        x
                        x    5 bits for Source Node ID (static enum to use less bits)
  29-bit CAN ID:        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Destination Node ID (0x1F = Broadcast to all)
                        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Datagram ID (global enum)
                        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Version Number (major)
                        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Version Number (minor)
                        x
                  LSB   x

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   Receive();
   UnloadData();
   LoadData();
   CheckFor_ResendDatagram();
   Transmit();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

