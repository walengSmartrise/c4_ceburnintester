/******************************************************************************
 *
 * @file     mod_local_inputs.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "sru.h"
#include "sru_e.h"
#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_LocalInputs =
{
   .pfnInit = Init,
   .pfnRun = Run,
};


/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define INPUT_DEBOUNCE_10MS         (10)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static uint8_t ucLocalInputs_ThisNode;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
uint8_t GetLocalInputs_ThisNode()
{
   return ucLocalInputs_ThisNode;
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   //------------------------
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = 10;

   return 0;
}

/*-----------------------------------------------------------------------------

   This module is used on any MCUA. It simply collects all 16 inputs bitmapped
   in one variable, and forwards it to MRA for input mapping.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   enum en_sru_inputs enInput;
   static uint8_t aucDebounceTimer_10ms[SRU_NUM_INPUTS];
   for ( enInput = enSRU_Input_501; enInput < SRU_NUM_INPUTS; ++enInput )
   {
      uint_fast8_t bValue = SRU_Read_Input( enInput );
      if( bValue != Sys_Bit_Get(&ucLocalInputs_ThisNode, enInput) )
      {
         if( ++aucDebounceTimer_10ms[enInput] >= INPUT_DEBOUNCE_10MS )
         {
            aucDebounceTimer_10ms[enInput] = 0;
            Sys_Bit_Set( &ucLocalInputs_ThisNode, enInput, bValue );
         }
      }
      else
      {
         aucDebounceTimer_10ms[enInput] = 0;
      }
   }
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
