
/******************************************************************************
 * @author   Keith Soneda
 * @version  V1.00
 * @date     1, August 2016
 *
 * @note   These are technically alarms since they have no affect on device performance.
 *         They are status notifications declared serially and by LED
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "sru_e.h"
#include "sru.h"
#include "sys.h"

#include <stdint.h>
#include <string.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Fault =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static en_ris_exp_error eActiveError = ERROR_RIS_EXP__UNKNOWN;
static uint32_t uiErrorCountdown_ms;

/* The lower the value the higher the priority */
static const uint8_t aucFaultPriority[NUM_RIS_EXP_ERROR] =
{
   255,//ERROR_RIS_EXP__UNKNOWN
   255,//ERROR_RIS_EXP__NONE
   0,//ERROR_RIS_EXP__RESET_POR
   0,//ERROR_RIS_EXP__RESET_WDT
   0,//ERROR_RIS_EXP__RESET_BOD
   1,//ERROR_RIS_EXP__COM_GROUP
   3,//ERROR_RIS_EXP__COM_HALL
   1,//ERROR_RIS_EXP__COM_CAR
   3,//ERROR_RIS_EXP__COM_MASTER
   2,//ERROR_RIS_EXP__DRIVER_FLT
   2,//ERROR_RIS_EXP__ADDRESS
   255,//ERROR_RIS_EXP__BUS_RESET_CAN1
   255,//ERROR_RIS_EXP__BUS_RESET_CAN2
   4,//ERROR_RIS_EXP__COM_SLAVE
   5,//ERROR_RIS_EXP__HALL_BOARD_OFFLINE
};
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Sets active board fault if none is active, or if req fault priority is lower.
 -----------------------------------------------------------------------------*/
#define ADDRESS_FAULT_EXTENDED_LATCH_PERIOD_MS        (30000)
void SetFault( en_ris_exp_error eFault )
{
   if( eFault < NUM_RIS_EXP_ERROR )
   {
      if( ( eActiveError == ERROR_RIS_EXP__NONE )
       || ( eActiveError == ERROR_RIS_EXP__UNKNOWN )
       || ( eFault == eActiveError ) )
      {
         eActiveError = eFault;
         uiErrorCountdown_ms = ( eFault == ERROR_RIS_EXP__ADDRESS )
                             ? ADDRESS_FAULT_EXTENDED_LATCH_PERIOD_MS
                             : FAULT_LATCH_PERIOD_1MS;
      }
      /* Allow only lower priority faults to be overwritten  */
      else if( aucFaultPriority[eFault] < aucFaultPriority[eActiveError] )
      {
         eActiveError = eFault;
         uiErrorCountdown_ms = ( eFault == ERROR_RIS_EXP__ADDRESS )
                             ? ADDRESS_FAULT_EXTENDED_LATCH_PERIOD_MS
                             : FAULT_LATCH_PERIOD_1MS;
      }
   }
}

/*-----------------------------------------------------------------------------
   Returns current board fault
 -----------------------------------------------------------------------------*/
en_ris_exp_error GetFault()
{
   return eActiveError;
}
/*-----------------------------------------------------------------------------
   DG_GroupNet_RIS__INPUT_STATE
 -----------------------------------------------------------------------------*/
void CheckFor_AddressFault_Riser( uint8_t ucRiserIndex, un_sdata_datagram *punDatagram )
{
   enum en_group_net_nodes eNode = punDatagram->auc8[2];
   enum en_group_net_nodes eLocalNode = GetSRU_RiserBoardID();
   enum en_group_net_nodes ePacketNode = ucRiserIndex + GROUP_NET__RIS_1;
   if( ( eNode == eLocalNode )
    && ( ePacketNode == eLocalNode ) )
   {
      SetFault(ERROR_RIS_EXP__ADDRESS);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void CheckFor_AddressFault_Master( un_sdata_datagram *punDatagram, uint8_t ucDatagramID )
{
   uint8_t ucLocalDatagramID = GetExpDipComNetworkDatagramID();
   if( ucDatagramID == ucLocalDatagramID )
   {
      SetFault(ERROR_RIS_EXP__ADDRESS);
   }
}
/*-----------------------------------------------------------------------------
   Checks reset source and asserts appropriate reset fault
 -----------------------------------------------------------------------------*/
static void UpdateResetFault()
{
   static uint32_t uiResetSrc;
   if(!uiResetSrc)
   {
      uiResetSrc = Chip_SYSCTL_GetSystemRSTStatus();
      Chip_SYSCTL_ClearSystemRSTStatus(uiResetSrc);
   }

   if( uiResetSrc & SYSCTL_RST_BOD )
   {
      SetFault( ERROR_RIS_EXP__RESET_BOD );
   }
   else if( uiResetSrc & SYSCTL_RST_WDT )
   {
      SetFault( ERROR_RIS_EXP__RESET_WDT );
   }
   else//assume SYSCTL_RST_POR
   {
      SetFault( ERROR_RIS_EXP__RESET_POR );
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_FaultLED()
{
   static uint8_t ucTimer_1ms;
   static uint8_t bLastLED;

   if( ( GetFault() != ERROR_RIS_EXP__NONE )
    && ( GetFault() != ERROR_RIS_EXP__COM_HALL )
    && ( GetFault() != ERROR_RIS_EXP__DRIVER_FLT ) )
   {
      if ( ucTimer_1ms > 100/MOD_RUN_PERIOD_FAULT_1MS )
      {
         SRU_Write_LED( enSRU_LED_Fault, bLastLED );

         ucTimer_1ms = 0;

         bLastLED = ( bLastLED )? 0 : 1;
      }
      else
      {
         ucTimer_1ms++;
      }
   }
   else
   {
      SRU_Write_LED( enSRU_LED_Fault, 0 );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateActiveFault()
{
   if( GetFault() != ERROR_RIS_EXP__NONE )
   {
      if( uiErrorCountdown_ms >= MOD_RUN_PERIOD_FAULT_1MS )
      {
         uiErrorCountdown_ms -= MOD_RUN_PERIOD_FAULT_1MS;
      }
      else // Clear fault after count down elapses
      {
         eActiveError = ERROR_RIS_EXP__NONE;
         uiErrorCountdown_ms = 0;
      }
   }
   else
   {
      uiErrorCountdown_ms = 0;
   }
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 100;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_FAULT_1MS;

   return 0;
}

/*-----------------------------------------------------------------------------


 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static uint32_t uiStartupCounter = BOARD_RESET_FAULT_TIMER_MS/MOD_RUN_PERIOD_FAULT_1MS;
   if( uiStartupCounter )
   {
      UpdateResetFault();
      uiStartupCounter--;
   }
   else
   {
      UpdateActiveFault();
   }
   //-----------------------------------------
   if( !GetSRU_TestMode() )
   {
      Update_FaultLED();
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
