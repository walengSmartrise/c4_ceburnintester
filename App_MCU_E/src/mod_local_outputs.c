/******************************************************************************
 *
 * @file     mod_local_outputs.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include <stdint.h>
#include "sru.h"
#include "sru_e.h"
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_LocalOutputs =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static uint8_t ucLocalOutputs_ThisNode;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetLocalOutputs_ThisNode( uint8_t ucOutputs )
{
   ucLocalOutputs_ThisNode = ucOutputs;
}
uint8_t GetLocalOutputs_ThisNode()
{
   return ucLocalOutputs_ThisNode;
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{

   //------------------------
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = 10;

   return 0;
}

/*-----------------------------------------------------------------------------

   This module runs on all MCUAs. It receives a 16-bit variable on the car
   network wich contains 16 bitmapped outputs that get written to HW from here

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   enum en_sru_outputs enOutput;

   /* Mirror inputs if in test mode */
   if( GetSRU_TestMode() )
   {
      ucLocalOutputs_ThisNode = GetLocalInputs_ThisNode();
   }

   for ( enOutput = enSRU_Output_601; enOutput <= enSRU_Output_608; ++enOutput )
   {
      uint_fast8_t bValue = Sys_Bit_Get( &ucLocalOutputs_ThisNode, enOutput );

      SRU_Write_Output( enOutput, bValue );
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
