/******************************************************************************
 *
 * @file     mod.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef _MOD_HALL_MONITOR_H_
#define _MOD_HALL_MONITOR_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_HALL_MONITOR_1MS       ( 100 )

#define ENABLE_REV1_HALLBOARD_SUPPORT         (0)

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

typedef struct
{
      uint32_t auiBF_ActiveButton_Up[BITMAP32_SIZE(MAX_NUM_HALLBOARDS)];
      uint32_t auiBF_ActiveButton_Down[BITMAP32_SIZE(MAX_NUM_HALLBOARDS)];
      // For ignoring stuck buttons
      uint32_t auiBF_LastButton_Up[BITMAP32_SIZE(MAX_NUM_HALLBOARDS)];
      uint32_t auiBF_LastButton_Down[BITMAP32_SIZE(MAX_NUM_HALLBOARDS)];

      uint32_t auiBF_ActiveLamp_Up[BITMAP32_SIZE(MAX_NUM_HALLBOARDS)];
      uint32_t auiBF_ActiveLamp_Down[BITMAP32_SIZE(MAX_NUM_HALLBOARDS)];

//      en_hallboard_error aeError[MAX_NUM_HALLBOARDS];

      /* Below is locally processed per hall board data */
      uint8_t aucOfflineCounter[MAX_NUM_HALLBOARDS]; /* Initialized as 255 to mark hallboard as inactive */
//      en_hb_com_state aeCommState[MAX_NUM_HALLBOARDS];
//      uint8_t aucBF_RawIO[MAX_NUM_HALLBOARDS];
      st_hallboard_status stBoardStatuses[MAX_NUM_HALLBOARDS];
      uint32_t aucBF_DirtyBits[BITMAP32_SIZE(MAX_NUM_HALLBOARDS)];/* Flag to resend hall board status */

      uint8_t bHallBoardOffline; // This flag signals if communication with a hall board previously detected was lost.
} st_hallboard_data; /* Hallboard data organized by DIP address */

typedef struct
{
      /* Latched hall calls - Per floor bitmaps with calls sorted into virtual risers */
      uint32_t auiBF_HallCalls_Up[MAX_NUM_RISER_BOARDS][HALL_BOARDS_MAX_NUM_FLOORS];
      uint32_t auiBF_HallCalls_Down[MAX_NUM_RISER_BOARDS][HALL_BOARDS_MAX_NUM_FLOORS];

      uint32_t aucBF_DirtyBits_Up[BITMAP32_SIZE(HALL_BOARDS_MAX_NUM_FLOORS)];/* Flag to resend active calls */
      uint32_t aucBF_DirtyBits_Down[BITMAP32_SIZE(HALL_BOARDS_MAX_NUM_FLOORS)];/* Flag to resend active calls */
} st_hallcall_data; /* Processed hall call data for transmit */

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_module gstMod_HallMonitor;
extern struct st_sdata_control * const gpstSData_GroupNet_RIS;
 /*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
uint8_t HallBoard_CheckIfMedicalRecallHeldActive( uint8_t ucLanding, uint8_t ucMedicalMask );

void UnloadDatagram_HallboardStatus_HN( uint16_t uwDIPs, uint8_t *pucData );

void UnloadDatagram_RiserHallboardStatus_GN( uint8_t ucRiserIndex, un_sdata_datagram *punDatagram );
void UnloadDatagram_RiserLatchedHallCalls( uint8_t ucRiserIndex, en_hc_dir eDir, un_sdata_datagram *punDatagram );

void RiserBoard_UpdateOfflineCounters( uint16_t uwRunPeriod_ms );
void RiserBoard_ClearOfflineCounter( uint8_t ucRiserIndex );
uint8_t RiserBoard_GetOnlineFlag( uint8_t ucRiserIndex );

void ClrRawLatchedHallCalls( uint8_t ucLanding, en_hc_dir eDir, uint32_t uiClrMask );
void SetRawLatchedHallCalls( uint8_t ucLanding, en_hc_dir eDir, uint32_t uiSetMask );
#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

