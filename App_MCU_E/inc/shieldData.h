/******************************************************************************
 *
 * @file     mod_run_log.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef _SHIELD_DATA_H_
#define _SHIELD_DATA_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_e.h"
#include "sys.h"
#include "remoteCommands.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define SHIELD_TIMEOUT_COUNT_MS     (15000)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void ShieldData_IncMessageCounter( void );
uint16_t ShieldData_GetMessageCounter( void );
void ShieldData_IncTimeoutCounter( uint16_t uwInterval_ms );
void ShieldData_ClrTimeoutCounter();
uint8_t ShieldData_CheckTimeoutCounter();
uint16_t ShieldData_GetTimeoutCounter();

void LoadDatagram_SyncTime( uint32_t uiTime );
void LoadDatagram_ParamSync_GUI(uint8_t* aucData);

void UnloadDatagram_SyncTime( un_sdata_datagram *punDatagram );
void UnloadDatagram_RemoteCommand( un_sdata_datagram *punDatagram );
void UnloadDatagram_LatchCarCall( un_sdata_datagram *punDatagram );
void UnloadDatagram_LatchHallCall( un_sdata_datagram *punDatagram );
void UnloadDatagram_ClearHallCall( un_sdata_datagram *punDatagram );
void UnloadDatagram_ParkingFloors( un_sdata_datagram *punDatagram );
void UnloadDatagram_VirtualCCEnables( un_sdata_datagram *punDatagram );
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
