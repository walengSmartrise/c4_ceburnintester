/******************************************************************************
 *
 * @file     remoteCommands.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     13 August 2019
 *
 * @note
 *
 ******************************************************************************/

#ifndef _REMOTE_COMMANDS_H_
#define _REMOTE_COMMANDS_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_e.h"
#include "sys.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   REMOTE_CMD__SET_DISPATCH,
   REMOTE_CMD__SET_PARKING,
   REMOTE_CMD__SET_GROUP_PARAMETER,
   REMOTE_CMD__SET_VIRTUAL_INPUTS,

   NUM_REMOTE_CMD
} en_remote_cmd;

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void SetVirtualCarCallSecurity(enum en_doors enDoorIndex, uint8_t ucMapIndex, uint32_t uiValue);

void RemoteCommand_SetDispatchMode(en_dispatch_mode enDispatchMode);
void RemoteCommand_SetParkingFloor(uint8_t ucFloor);
void RemoteCommand_SetGroupParameter(enum en_param_block_types eParamType, uint16_t uwParamIndex, uint32_t uiParamValue);
void RemoteCommand_SetVirtualInputs(un_sdata_datagram *punDatagram);

en_dispatch_mode RemoteCommand_GetDispatchMode(void);
uint32_t GetVirtualCarCallSecurity(enum en_doors enDoorIndex, uint8_t ucMapIndex);

uint8_t LoadDatagram_RemoteCommands(un_sdata_datagram* punDatagram);

void Reset_Virtual_Inputs(void);
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
