/******************************************************************************
 *
 * @file     mod.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef MOD_H
#define MOD_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "sys.h"
#include "sdata.h"
#include "shared_data.h"
#include "sru_e.h"
#include "datagrams.h"
#include "GlobalData.h"
#include "mod_fault.h"
#include "mod_hallmonitor.h"
#include "groupnet.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define CAR_OFFLINE_TIMEOUT (100)
#define HALLBOARD_OFFLINE_TIMEOUT (250)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

extern struct st_module_control *gpstModuleControl_ThisDeployment;

extern struct st_module gstMod_SData_CarNet;
extern struct st_module gstMod_SData_IONet;
extern struct st_module gstMod_SData_IOChildNet;

extern struct st_module gstMod_SData_GroupNet;
extern struct st_module gstMod_SData_HallNet;

extern struct st_module gstMod_LocalInputs;
extern struct st_module gstMod_LocalOutputs;
extern struct st_module gstMod_Heartbeat;
extern struct st_module gstMod_GroupMaster;

extern struct st_module gstMod_Watchdog;
extern struct st_module gstMod_CAN;

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

void GetChildInputs(un_sdata_datagram *punChildNodeInputs);
void SetChildOutputs(un_sdata_datagram *punChildNodeOutputs);

void SetInputsToSend_C2P( un_sdata_datagram *punInputsReceived );
void SetOutputsToSend_P2C( un_sdata_datagram *punOutputsReceived );
void SetInputsToSend_CarNet( un_sdata_datagram *punInputsReceived );

/* Riser Functions*/
void UpdateGroupCarData( uint8_t ucCarIndex, un_sdata_datagram *unDatagram);
void UpdateGroupCarMode( uint8_t ucCarIndex, un_sdata_datagram *unDatagram);
void UpdateGroupOutputs ( uint8_t ucCarIndex, un_sdata_datagram *unDatagram);

uint8_t GetNextGroupHallCalls ( un_sdata_datagram *unDatagram);
uint8_t GetHallBoardStatus ( un_sdata_datagram *unDatagram );
uint32_t GetActiveHallLamps( uint8_t ucMapIndex, en_hc_dir bUpDirection );
uint32_t GetLatchedCalls( uint8_t ucLandingIndex , en_hc_dir bUpDirection );

Datagram_ID GetExpNetworkDatagramID();
Datagram_ID GetExpDipComNetworkDatagramID();

void ResetOfflineCounter_MRA();
void ResetOfflineCounter_MRB();

void LoadDatagram_LatchedCalls_Up();
void LoadDatagram_LatchedCalls_Down();

uint8_t GetLocalInputs_ThisNode();
void SetLocalOutputs_ThisNode( uint8_t ucOutputs );
uint8_t GetLocalOutputs_ThisNode();

uint16_t GetDebugBusOfflineCounter_CAN1();
uint16_t GetDebugBusOfflineCounter_CAN2();

void LoadDatagram_LatchedCalls_Up_SecurityContactsF(void);
void LoadDatagram_LatchedCalls_Down_SecurityContactsR(void);
#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
