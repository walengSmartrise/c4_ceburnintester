/******************************************************************************
 * @author   Keith Soneda
 * @version  V1.00
 * @date     6, Sept 2018
 *
 * @note    Access functions for master expansion board's data
 *
 ******************************************************************************/

#ifndef _EXP_DATA_H_
#define _EXP_DATA_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include "sys.h"
#include "riser.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

 /*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void ExpData_LoadInputStatusMessage( un_sdata_datagram *punDatagram );
void ExpData_LoadMasterStatusMessage( un_sdata_datagram *punDatagram );
void ExpData_UnloadSlaveMessage( un_sdata_datagram *punDatagram, uint8_t ucSlaveIndex );

void ExpData_UpdateLocalData(void);
void ExpData_Initialize(void);
uint8_t ExpData_GetOnlineBitmap(void);
void ExpData_ClrOfflineTimer( uint8_t ucBoardIndex );
void ExpData_UpdateOfflineTimers( uint16_t uwRunPeriod_ms );
void ExpData_SetInputs( uint8_t ucBoardIndex, uint8_t ucInputs );
uint8_t ExpData_GetInputs( uint8_t ucBoardIndex );
void ExpData_SetOutputs( uint8_t ucBoardIndex, uint8_t ucOutputs );
uint8_t ExpData_GetOutputs( uint8_t ucBoardIndex );
void ExpData_SetError( uint8_t ucBoardIndex, en_ris_exp_error eError );
en_ris_exp_error ExpData_GetError( uint8_t ucBoardIndex );
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
