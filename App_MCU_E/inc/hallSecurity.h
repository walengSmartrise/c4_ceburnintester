/******************************************************************************
 *
 * @file     hallSecurity.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef _HALL_SECURITY_H_
#define _HALL_SECURITY_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_e.h"
#include "sys.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Unload status of hall call security contacts
 -----------------------------------------------------------------------------*/
void ClearSecuredHallCalls_ByLanding( uint8_t ucLanding );
void UnloadDatagram_RiserSecurityContacts_F( un_sdata_datagram *punDatagram );
void UnloadDatagram_RiserSecurityContacts_R( un_sdata_datagram *punDatagram );
uint8_t GetHallCallSecurity(enum en_group_net_nodes eCarID, uint8_t ucLanding, enum en_doors eDoor, uint32_t uiMask);
void ResetHallSecurity(void);
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
