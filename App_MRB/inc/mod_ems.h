/******************************************************************************
 *
 * @file     mod_ems.h
 * @brief    Manages EMS emergency medical recall requests and their assignments to cars
 * @version  V1.00
 * @date     7, August 2019
 *
 * @note
 *
 ******************************************************************************/

#ifndef _MOD_EMS_H_
#define _MOD_EMS_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_EMS_1MS                     (500U)
#define EMS_CAR_MOVE_TIMEOUT_500MS                 (60U) // 30sec
#define EMS_CAR_OFFLINE_COUNTDOWN_500MS            (20U) // 10sec

#define EMS_ASSIGNMENT_MIN_RESEND_LATCHED_500MS    (4U)
#define EMS_ASSIGNMENT_MIN_RESEND_CLEARED_500MS    (20U)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_module gstMod_EMS;

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
uint8_t EMS_GetRecallLanding_Plus1( enum en_group_net_nodes eCarID );
void EMS_SetRecallLanding_Plus1( enum en_group_net_nodes eCarID, uint8_t ucRecallLanding_Plus1 );

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
