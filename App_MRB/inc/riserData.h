/******************************************************************************
 *
 * @file     mod_run_log.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef _RISER_DATA_H_
#define _RISER_DATA_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include <stdint.h>
#include "riser.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void Initialize_RiserData();
void ClrRiserOfflineCounter( uint8_t ucRiserIndex );

uint8_t RiserData_GetInputs( uint8_t ucRiserIndex );
uint8_t RiserData_GetOutputs( uint8_t ucRiserIndex );
en_ris_exp_error RiserData_GetError( uint8_t ucRiserIndex );
uint16_t RiserData_GetOfflineCounter( uint8_t ucRiserIndex );
char * RiserData_GetVersionString( uint8_t ucRiserIndex );

void UnloadDatagram_RiserOutputs();
void LoadDatagram_RiserOutputs(un_sdata_datagram *punDatagram );
void LoadDatagram_RiserInputs(  un_sdata_datagram *punDatagram );
void UnloadDatagram_RiserStatus( uint8_t ucRiserIndex,  un_sdata_datagram *punDatagram );
void CheckFor_RiserBoardFault( struct st_module *pstThisModule );
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
