/******************************************************************************
 *
 * @file     fault_app.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef _ALARM_APP_H_
#define _ALARM_APP_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_ALARM_1MS             (5U)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_module gstMod_AlarmApp;
extern st_alarm gstAlarm;
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

uint8_t GetAlarm_ByRange( en_alarms eFirstAlarmNum,  en_alarms eLastAlarmNum );
uint8_t GetAlarm_ByNumber( en_alarms eAlarmNum );
en_alarms GetAlarm_ByNode( en_alarm_nodes eAlarmNode );

void UnloadDatagram_ActiveAlarms_MR( st_alarm_data *pastActiveAlarms );
void LoadDatagram_ActiveAlarms_MR( st_alarm_data *pastActiveAlarms );

void UnloadDatagram_ActiveAlarms_CT( st_alarm_data *pastActiveAlarms );
void LoadDatagram_ActiveAlarms_CT( st_alarm_data *pastActiveAlarms );

void UnloadDatagram_ActiveAlarms_COP( st_alarm_data *pastActiveAlarms );
void LoadDatagram_ActiveAlarms_COP( st_alarm_data *pastActiveAlarms );

/* Alarm access functions */
void SetAlarm( en_alarms eAlarmNum );
en_alarm_nodes AlarmApp_GetNode( void );

void SetLocalActiveAlarm( st_alarm_data *pstAlarmData );
void GetActiveAlarmData( st_alarm_data *pstAlarmData, en_alarm_nodes eAlarmNode );

#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
