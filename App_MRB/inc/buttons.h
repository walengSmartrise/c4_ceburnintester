/******************************************************************************
 *
 * @file     buttons.h
 * @brief    Button Header File
 * @version  V1.00
 * @date     23, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef BUTTONS_H
#define BUTTONS_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

enum en_keypresses
{
   enKEYPRESS_NONE,

   enKEYPRESS_UP,
   enKEYPRESS_DOWN,
   enKEYPRESS_LEFT,
   enKEYPRESS_RIGHT,
   enKEYPRESS_ENTER,

   NUM_KEYPRESS_TYPES
};

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

enum en_keypresses Button_GetKeypress( void );

void Button_PutKeypress( enum en_sru_buttons enSRU_Button );

#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
