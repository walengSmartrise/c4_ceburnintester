/******************************************************************************
 *
 * @file     ddCommand.h
 * @brief
 * @version  V1.00
 * @date     13, June 2018
 * @author   Keith Soneda
 *
 * @note     Manages commands used for CC/HC assignments to cars.
 *
 ******************************************************************************/
#ifndef _DD_COMMAND_H_
#define _DD_COMMAND_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include "DDM_Alarms.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define DDM_INACTIVE_TIMER__MS            (65535)
#define DDM_OFFLINE_TIMER__MS             (30000)
// Note: gstSData_GroupNet_DDM.paiResendRate_1ms[DG_GroupNet_DDM__HEARTBEAT] = 10000;
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
en_ddm_alarms DDC_GetAlarm(void);

uint8_t DDC_GetOnlineFlag(void);
void DDC_ClrOfflineTimer(void);
void DDC_UpdateOfflineTimer( struct st_module *pstThisModule );

void DDC_UnloadDatagram_LatchHC(un_sdata_datagram *punDatagram);
void DDC_UnloadDatagram_LatchCC(un_sdata_datagram *punDatagram);
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
