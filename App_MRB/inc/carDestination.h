/******************************************************************************
 *
 * @file     mod_run_log.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef _CAR_DESTINATION_H_
#define _CAR_DESTINATION_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define ENABLE_REV2_DEST_DISPATCHING     (1)

#define INVALID_LANDING    (255)

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

typedef struct
{
      uint8_t ucLanding;
      uint32_t uiMask; // Active hall mask for clearing the call
      enum en_doors eDoor; //Destination opening
      enum direction_enum eCallDir; // Call direction
      uint8_t bDirty;
} st_carDestination;


/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

void Init_CarDestinationStructure();

void ClrCarDestination_ByCar(enum en_group_net_nodes eCarID);

st_carDestination * GetCarDestinationStructure(enum en_group_net_nodes eCarID);
uint8_t CarDestination_GetLanding(enum en_group_net_nodes eCarID);
uint32_t CarDestination_GetMask(enum en_group_net_nodes eCarID);
enum en_doors CarDestination_GetDoor(enum en_group_net_nodes eCarID);
enum direction_enum CarDestination_GetDirection(enum en_group_net_nodes eCarID);

uint8_t GetLatchedHallCallUp_Front_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding );
uint8_t GetLatchedHallCallUp_Rear_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding );
uint8_t GetLatchedHallCallDown_Front_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding );
uint8_t GetLatchedHallCallDown_Rear_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding );

void UpdateDestination_ByCar( enum en_group_net_nodes eCarID );

uint8_t LoadDatagram_MasterDispatcher( un_sdata_datagram *punDatagram, struct st_module *pstThisModule );
void UnloadDatagram_MasterDispatcher( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram );


void UpdateGroupCarDestinations();
void Vip_Dispatching( void );
void VIP_UnloadDatagram_MasterDispatcher( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram );
void VIP_LoadDatagram_MasterDispatcher( struct st_module *pstThisModule );
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
