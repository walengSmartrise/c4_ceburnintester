/******************************************************************************
 *
 * @file     mod_run_log.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef _CAR_DATA_H_
#define _CAR_DATA_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include <stdint.h>
#include "motion.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define GROUP_CAR_OFFLINE_TIMER_MS     (10000)
#define GROUP_CAR_INVALID_TIMER_MS     (0xFFFF)

/* Defines a unique hall mask for each car, combined with the car's parameter controlled mask */
#define UNIQUE_HALL_MASK_F__CAR1      (0x01000000)
#define UNIQUE_HALL_MASK_F__CAR2      (0x02000000)
#define UNIQUE_HALL_MASK_F__CAR3      (0x04000000)
#define UNIQUE_HALL_MASK_F__CAR4      (0x08000000)
#define UNIQUE_HALL_MASK_F__CAR5      (0x10000000)
#define UNIQUE_HALL_MASK_F__CAR6      (0x20000000)
#define UNIQUE_HALL_MASK_F__CAR7      (0x40000000)
#define UNIQUE_HALL_MASK_F__CAR8      (0x80000000)

#define UNIQUE_HALL_MASK_R__CAR1      (0x00010000)
#define UNIQUE_HALL_MASK_R__CAR2      (0x00020000)
#define UNIQUE_HALL_MASK_R__CAR3      (0x00040000)
#define UNIQUE_HALL_MASK_R__CAR4      (0x00080000)
#define UNIQUE_HALL_MASK_R__CAR5      (0x00100000)
#define UNIQUE_HALL_MASK_R__CAR6      (0x00200000)
#define UNIQUE_HALL_MASK_R__CAR7      (0x00400000)
#define UNIQUE_HALL_MASK_R__CAR8      (0x00800000)

#define NUM_PAIRED_HALLBOARD_MASKS    (4)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/* Requests for data sent by each group car out onto the group network */
typedef enum
{
   CAR_REQUEST__NONE,
   CAR_REQUEST__HALL_BOARD__START,
   CAR_REQUEST__HALL_BOARD__END = CAR_REQUEST__HALL_BOARD__START + MAX_NUM_HALLBOARDS - 1,
   CAR_REQUEST__HALL_SECURITY__START,
   CAR_REQUEST__HALL_SECURITY__END = CAR_REQUEST__HALL_SECURITY__START + NEW_HALL_BOARD_MAX_FLOOR_LIMIT - 1,

   CAR_REQUEST__RIS1_CAN1,
   CAR_REQUEST__RIS2_CAN1,
   CAR_REQUEST__RIS3_CAN1,
   CAR_REQUEST__RIS4_CAN1,

   CAR_REQUEST__RIS1_CAN2,
   CAR_REQUEST__RIS2_CAN2,
   CAR_REQUEST__RIS3_CAN2,
   CAR_REQUEST__RIS4_CAN2,

   NUM_CAR_REQUESTS
} en_car_request;

typedef struct
{
      /* Car Parameters updated regularly */
      uint32_t auiBF_CarCalls_F[BITMAP32_SIZE(MAX_NUM_FLOORS)]; // Mask by landing index, not floor index
      uint32_t auiBF_CarCalls_R[BITMAP32_SIZE(MAX_NUM_FLOORS)]; // Mask by landing index, not floor index
      uint8_t ucCurrentLanding;
      uint8_t ucDestinationLanding;
      uint8_t ucReachableLanding;
      enum en_door_states eDoor_F;
      enum en_door_states eDoor_R;
      enum direction_enum cMotionDir;
      en_hc_dir enPriority;

      // This option originates from car parameter 01-125 and doubles the minimum resend rate of dispatching packets
      uint8_t bFastGroupResend;

      // This flag signals when the car is stopped and able to change direction.
      uint8_t bIdleDirection;

      // This flag signals when reopen at the current floor is disabled. Used to lockout dispatching to a floor for a specific car.
      uint8_t bSuppressReopen;

      // This flag signals when the car is in slowdown. Used to lockout direction change.
      uint8_t bInSlowdown;

      // This flag signals when the car has hall call security enabled.
      uint8_t bEnableHallCallSecurity;

      /* Car Operation Data */
      enum en_classop eClassOp;
      enum en_mocmd eMotionCmd;
      enum en_mode_learn eLearnMode;
      enum en_mode_auto eAutoMode;
      enum en_mode_manual eManualMode;
      enum en_recall_states eRecallState;
      enum en_automatic_state eAutoState;
      uint8_t ucFireService;
      enum en_fire_2_states eFire2State;
      enum en_Capture_Mode eCaptureMode;

      uint8_t bEP_Disabled; /* Disable EPower */
      uint8_t bEP_CarRecalledFlag; /* Flag signaling that the car is at the recall floor with its doors open */
      enum enEmergencyPowerCommands eEP_CommandFeedback; /* Feedback of the EP command seen by the car */

      /* Car Parameters updated rarely */
      uint32_t auiBF_OpeningMap_F[BITMAP32_SIZE(MAX_NUM_FLOORS)]; // Mask by landing index, not floor index
      uint32_t auiBF_OpeningMap_R[BITMAP32_SIZE(MAX_NUM_FLOORS)]; // Mask by landing index, not floor index
      uint8_t ucFirstLanding;
      uint8_t ucLastLanding;
      uint32_t uiHallMask_F; // Valid destination calls based on this mask
      uint32_t uiHallMask_R; // Valid destination calls based on this mask

      uint8_t uiVIPMask_F; // Valid destination calls based on this mask
      uint8_t uiVIPMask_R; // Valid destination calls based on this mask

      uint32_t uiLatchableHallMask_F; // Latchable calls based on is mask
      uint32_t uiLatchableHallMask_R; // Latchable calls based on is mask
      uint8_t aucPairedHBMask[NUM_PAIRED_HALLBOARD_MASKS];
      uint32_t auiBF_SecurityMap_F[BITMAP32_SIZE(MAX_NUM_FLOORS)]; // Mask by landing index, not floor index
      uint32_t auiBF_SecurityMap_R[BITMAP32_SIZE(MAX_NUM_FLOORS)]; // Mask by landing index, not floor index
      uint8_t ucMaxFloorToFloorTime_sec; // Expected time in motion for an average floor to floor run
      uint8_t ucDoorDwellTime_sec; // Car call dwell time
      uint8_t ucDoorDwellHallTime_sec; // Hall call dwell time

      uint32_t auiBF_HallSecurityMap[BITMAP32_SIZE(MAX_NUM_FLOORS)]; // Mask by landing index, not floor index
      uint8_t ucHallSecurityMask_F; // Marks front hall calls function groups effected by hall security
      uint8_t ucHallSecurityMask_R; // Marks rear hall calls function groups effected by hall security

      en_car_request eCarRequest; // Requests for for data (i.e. HC status)

      /* For EMS Recall */
      uint8_t ucMedicalMask;
      uint8_t ucMedicalLanding_Plus1;

      /* Car I/O */
      uint32_t auiBF_InputMap[BITMAP32_SIZE(MAX_INPUT_FUNCTIONS)];
      uint32_t auiBF_OutputMap[BITMAP32_SIZE(MAX_OUTPUT_FUNCTIONS)];

      en_dispatch_mode enDispatchMode;

      uint8_t bMasterDispatcher;
      uint16_t uwOfflineCounter_ms;
      uint8_t bDynamicParking;   // dynamic parking enabled.
      uint8_t bParking;         // idle and ready to start parking

      uint8_t bExtFloorLimit; // Flag marking if the car's maximum floor limit is 96 floors instead of the usual 64. Only has an effect if the 8 or 10 DIP hall boards are used instead of the 12 DIP.

      uint8_t bVIP;  //VIP enabled
      uint8_t ucbClosestCar; // VIP closest car
      uint8_t bCarReady; // feedback from MRA for car able to take call
      uint8_t bCarCapture; // feedback from MRA for car being captured

      uint8_t bFaulted;  // 1 if the car has an active fault

      uint8_t ucVIPFloor_Plus1; // current VIP HC floor assignment from VIP dispatcher
      uint8_t bDirty; // Call data has changed and destination should be reevaluated
} st_cardata;

typedef enum
{
   CAR_DATA_PACKET__OPENING_F_1,
   CAR_DATA_PACKET__OPENING_F_2,
   CAR_DATA_PACKET__OPENING_F_3,
   CAR_DATA_PACKET__OPENING_R_1,
   CAR_DATA_PACKET__OPENING_R_2,
   CAR_DATA_PACKET__OPENING_R_3,
   CAR_DATA_PACKET__HALLMASK_F,
   CAR_DATA_PACKET__HALLMASK_R,
   CAR_DATA_PACKET__LATCHABLE_HALLMASK_F,
   CAR_DATA_PACKET__PAIRED_HB_MASK,

   CAR_DATA_PACKET__SECURITY_F_1,
   CAR_DATA_PACKET__SECURITY_F_2,
   CAR_DATA_PACKET__SECURITY_F_3,
   CAR_DATA_PACKET__SECURITY_R_1,
   CAR_DATA_PACKET__SECURITY_R_2,
   CAR_DATA_PACKET__SECURITY_R_3,

   CAR_DATA_PACKET__HALL_SEC_1,// Which landings are secured
   CAR_DATA_PACKET__HALL_SEC_2,
   CAR_DATA_PACKET__HALL_SEC_3,

   CAR_DATA_PACKET__REQUEST,
   CAR_DATA_PACKET__HALL_SEC_MASK,// Which sets of hall boards are secured
   CAR_DATA_PACKET__LATCHABLE_HALLMASK_R,
   CAR_DATA_PACKET__HALL_MEDICAL,

   CAR_DATA_PACKET__TIME,
   CAR_DATA_PACKET__FLAGS,

   CAR_DATA_PACKET__VIPMASK,

   NUM_CAR_DATA_PACKETS
} en_car_data_packets;
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void Init_CarDataStructure();
void Init_CarDataStructure_ByCar(enum en_group_net_nodes eCarID);

void UpdateLocalGroupCarData();

st_cardata * GetCarDataStructure(enum en_group_net_nodes eCarID);
uint8_t CarData_GetMasterDispatcherFlag(enum en_group_net_nodes eCarID);
uint8_t CarData_GetDoorOpen_Front( enum en_group_net_nodes eCarID );
uint8_t CarData_GetDoorOpen_Rear( enum en_group_net_nodes eCarID );
uint8_t CarData_CheckOpening( enum en_group_net_nodes eCarID, uint8_t ucLanding, enum en_doors eDoor );
uint32_t CarData_GetHallMask( enum en_group_net_nodes eCarID, enum en_doors eDoor );
uint32_t CarData_VIP_GetHallMask( enum en_group_net_nodes eCarID, enum en_doors eDoor );
uint32_t CarData_GetGroupHallCallMask( uint8_t ucLanding, enum en_doors eDoor );
uint8_t CarData_GetMedicalHallCallMask( uint8_t ucLanding, enum en_doors eDoor );

uint8_t CarData_GetFastResend(void);

uint8_t GetInputValue_ByCar( enum en_group_net_nodes eCarID, enum en_input_functions enInput );
uint8_t GetOutputValue_ByCar( enum en_group_net_nodes eCarID, enum en_output_functions enOutput );

void IncrementAllCarOfflineTimers( struct st_module *pstThisModule );
void ClrCarOfflineTimer_ByCar( enum en_group_net_nodes eCarID );
uint16_t GetCarOfflineTimer_ByCar( enum en_group_net_nodes eCarID );
uint8_t GetCarOnlineFlag_ByCar( enum en_group_net_nodes eCarID );
en_car_request GetCarRequest_ByCar( enum en_group_net_nodes eCarID );
en_car_request GetCarRequest_AnyCar(void);

uint8_t CarData_GetExtFloorLimitFlag( enum en_group_net_nodes eCarID );
uint8_t CarData_GetExtFloorLimitFlag_AnyCar(void);

uint8_t GetMasterDispatcherFlag();
void UpdateMasterDispatcherFlag();
uint8_t GetMasterDispatcherFlag_ByCar( enum en_group_net_nodes eCarID );

uint32_t CarData_GetUniqueHallMask_Front( enum en_group_net_nodes eCarID );
uint32_t CarData_GetUniqueHallMask_Rear( enum en_group_net_nodes eCarID );

uint8_t CarData_GetHighestActiveLanding();

uint8_t GetLatchedCarCall_Front_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding );
uint8_t GetLatchedCarCall_Rear_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding );

void LoadDatagram_CarStatus( un_sdata_datagram *punDatagram );
uint8_t LoadDatagram_CarData( un_sdata_datagram *punDatagram );
void LoadDatagram_CarOperation( un_sdata_datagram *punDatagram );

void UnloadDatagram_CarStatus( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram );
void UnloadDatagram_CarData( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram );
void UnloadDatagram_CarOperation( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram );
void UnloadDatagram_CarCalls1( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram );
void UnloadDatagram_CarCalls2( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram );
void UnloadDatagram_CarCalls3( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram );

void UnloadDatagram_InputMap1( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram );
void UnloadDatagram_InputMap2( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram );
void UnloadDatagram_InputMap3( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram );

void UnloadDatagram_OutputMap1( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram );
void UnloadDatagram_OutputMap2( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram );

en_dispatch_mode GetDispatchMode_ByCar(enum en_group_net_nodes eCarID);
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
