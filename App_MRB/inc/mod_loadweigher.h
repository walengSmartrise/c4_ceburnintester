/******************************************************************************
 *
 * @file
 * @brief
 * @version  V1.00
 * @date     28, Nov 2018
 *
 * @note    For smartrise load weigher device
 *
 ******************************************************************************/

#ifndef _LOAD_WEIGHER_H_
#define _LOAD_WEIGHER_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_LOAD_WEIGHER_1MS      (50)

#define CAN_TIMEOUT_COUNT_MS                 (10000U)
#define LWD_OFFLINE_TIMER_UNK_MS             (0xFFFF)
#define MIN_TORQUE_RESEND_DELAY_MS           (250U)
#define MIN_WIFI_RESEND_DELAY_MS             (60000U)
#define MAX_UI_REQ_RESEND_DELAY_MS           (200U)
#define NUM_PACKET_RESEND                    (2U)
#define NUM_LW_DATA_BYTES                    (9)
#define NUM_LW_TORQUE_SAMPLES                (4)
#define LW_UI_REQUEST_LOCAL_CLEAR_COUNTDOWN_50MS   (60)
#define LW_UI_REQUEST_REMOTE_CLEAR_COUNTDOWN_50MS  (60)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   LW_CMD__READ,     // Command for LWD to read specified value
   LW_CMD__WRITE,    // Command for LWD to store value from C4
   LW_CMD__GENERATE, // Command for LWD to run specified operation

   NUM_LW_CMD
} en_lw_cmd;

/* Messages from C4 System to LW */
typedef enum
{
   LW_REQ__TORQUE,
   LW_REQ__ZERO,
   LW_REQ__SPAN,
   LW_REQ__COMP_ROPE,
   LW_REQ__MAX_CAP,
   LW_REQ__CW_PERCENT,
   LW_REQ__ROPE_LB_PER_FT,
   LW_REQ__POS_BOT,
   LW_REQ__POS_TOP,
   LW_REQ__WIFI,
   LW_REQ__LIGHT_LOAD,
   LW_REQ__FULL_LOAD,
   LW_REQ__OVER_LOAD,
   LW_REQ__ROPE_DIAM,
   LW_REQ__ROPE_COUNT,
   LW_REQ__EMPTY_CAR_WEIGHT,
   LW_REQ__WEIGHT_IN_CAR,
   LW_REQ__READ_LOAD_CELL,
   LW_REQ__CAR_SIDE_ROPE_WEIGHT,
   LW_REQ__CAR_SIDE_COMP_ROPE_WEIGHT,
   LW_REQ__CW_SIDE_ROPE_WEIGHT,
   LW_REQ__CW_SIDE_COMP_ROPE_WEIGHT,
   LW_REQ__TORQUE_OFFSET,
   LW_REQ__TORQUE_SCALING,

   NUM_LW_REQ
} en_lw_req;

/* Messages from LW to C4 System */
typedef enum
{
   LW_RESP__TORQUE,
   LW_RESP__ZERO,
   LW_RESP__SPAN,
   LW_RESP__COMP_ROPE,
   LW_RESP__MAX_CAP,
   LW_RESP__CW_PERCENT,
   LW_RESP__ROPE_LB_PER_FT,
   LW_RESP__POS_BOT,
   LW_RESP__POS_TOP,
   LW_RESP__WIFI,
   LW_RESP__LIGHT_LOAD,
   LW_RESP__FULL_LOAD,
   LW_RESP__OVER_LOAD,
   LW_RESP__ROPE_DIAM,
   LW_RESP__ROPE_COUNT,
   LW_RESP__EMPTY_CAR_WEIGHT,
   LW_RESP__WEIGHT_IN_CAR,
   LW_RESP__READ_LOAD_CELL,
   LW_RESP__CAR_SIDE_ROPE_WEIGHT,
   LW_RESP__CAR_SIDE_COMP_ROPE_WEIGHT,
   LW_RESP__CW_SIDE_ROPE_WEIGHT,
   LW_RESP__CW_SIDE_COMP_ROPE_WEIGHT,
   LW_RESP__TORQUE_OFFSET,
   LW_RESP__TORQUE_SCALING,

   NUM_LW_RESP
} en_lw_resp;

/* Errors taken from LoadWeigher project fault.h */
typedef enum
{
   LWD_ERROR__UNK,
   LWD_ERROR__NONE,
   LWD_ERROR__RESET_POR,
   LWD_ERROR__RESET_WDT,
   LWD_ERROR__RESET_BOD,
   LWD_ERROR__COM_SYS, // com loss with c4 system
   LWD_ERROR__COM_LOAD, // com loss with load cell monitor
   LWD_ERROR__CAN_BUS_RST,
   LWD_ERROR__WD_DISABLED,

   NUM_LWD_ERROR
} en_lwd_error;

typedef struct
{
      en_lw_req eRequest;
      en_lw_cmd eCommand;
      float fWriteData;
      uint8_t ucSendCountdown; // Times to transmit the request from C4 to LWD
      uint8_t ucClearCountdown_50ms; // Time to assert a UI request before clearing
}st_lwd_ui_request;

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_module gstMod_LoadWeigher;

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void LoadWeigher_SetUIRequestReadData(float fValue);
float LoadWeigher_GetUIRequestReadData(void);

void LoadWeigher_GetUIRequest(st_lwd_ui_request *pstUIRequest);
void LoadWeigher_SetUIRequest(st_lwd_ui_request *pstUIRequest, uint8_t bRemote);

en_lwd_calib_state LoadWeigher_GetCalibrationState(void);

uint8_t LoadWeigher_GetLoadFlag(en_load_flag_bits eBit);

uint8_t LoadWeigher_GetInCarWeight_50lb(void);

en_lwd_error LoadWeigher_GetError(void);

uint16_t LoadWeigher_GetReceiveCounter(void);

uint8_t LoadWeigher_GetOfflineStatus(void);
void LoadWeigher_UpdateOfflineCounter(uint16_t uwRunPeriod_ms);

int8_t LoadWeigher_GetRawTorquePercent(void);
int8_t LoadWeigher_GetAverageTorquePercent(void);

void LoadWeigher_UnloadDatagram(un_sdata_datagram *punDatagram);
#endif
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
