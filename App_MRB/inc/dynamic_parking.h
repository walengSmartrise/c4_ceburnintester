/******************************************************************************
 *
 * @file     dynamic_parking.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef _DYNAMIC_PARKING_H_
#define _DYNAMIC_PARKING_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

uint8_t DynamicParking_GetFloorAssignment(void);
uint8_t DynamicParking_GetLandingAssignment(uint8_t car);

void DynamicParking_UnloadPredictiveParkingDatagram( un_sdata_datagram *punDatagram );
void DynamicParking_UnloadDynamicParkingDatagram( un_sdata_datagram *punDatagram );
void UnloadDatagram_DynamicParking(enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram);
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
