/******************************************************************************
 *
 * @file     emergency_power.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef _EMERGENCY_POWER_H_
#define _EMERGENCY_POWER_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define EMERGENCY_POWER_RECALL_TIMEOUT_MS       ( 30000 )
#define EMERGENCY_POWER_RECALL_DEBOUNCE_MS      ( 3000 ) // Time car must be seen as recalled before the next car can be selected.
#define EMERGENCY_POWER_CAR_UPDATE_TIMEOUT_MS   ( 1000 )

#define EMERGENCY_POWER_STARTUP_DELAY_50MS      ( 200 )
#define EMERGENCY_POWER_RECALLED_DELAY_50MS     ( 60 )
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
enum enEmergencyPowerCommands GetEPowerCommand ( enum en_group_net_nodes eNode );
void SetEPowerCommand(enum enEmergencyPowerCommands eCommand, enum en_group_net_nodes eNode );

void UnloadDatagram_EmergencyPower(enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram);
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
