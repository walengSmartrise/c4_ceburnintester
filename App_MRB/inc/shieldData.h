/******************************************************************************
 *
 * @file     mod_run_log.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef _SHIELD_DATA_H_
#define _SHIELD_DATA_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include "remoteCommands.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define SHIELD_TIMEOUT_COUNT_MS     (15000)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   SHIELD_ERROR__UNKNOWN, // Value 0 - For transmission purposes this signals that no communication has been seen from the shield
   SHIELD_ERROR__NONE, // Value 1 - No active errors
   SHIELD_ERROR__POR_RESET, // Value 2 - Shield is starting up after being reset or power cycled. Should be held 10 seconds after startup.
   SHIELD_ERROR__BOD_RESET, // Value 3 - Shield is starting up after resetting due a dip in its power supply rail. Should be held 10 seconds after startup.
   SHIELD_ERROR__WDT_RESET, // Value 4 - Shield is starting up after watchdog triggered a reset. Should be held 10 seconds after startup.
   SHIELD_ERROR__COM_GROUP, // Value 5 - Shield has not recieved any communication packets from the group network in 10 seconds.
   SHIELD_ERROR__COM_RPI, // Value 6 - Shield has not recieved any communication packets from the RPi in 10 seconds.
   SHIELD_ERROR__FAILED_RTC, // Value 7 - Shield RTC is not working.
   SHIELD_ERROR__UART_OVF_TX, // Value 8 - Shield UART transmit buffer overflowed.
   SHIELD_ERROR__UART_OVF_RX, // Value 9 - Shield UART receive buffer overflowed.
   SHIELD_ERROR__CAN_OVF_TX, // Value 10 - Shield CAN transmit buffer overflowed.
   SHIELD_ERROR__CAN_OVF_RX, // Value 11 - Shield CAN receive buffer overflowed.
   SHIELD_ERROR__CAN_BUS_RST, // Value 12 - Shield CAN bus reset
   NUM_SHIELD_ERRORS,
} en_shield_error; // Note each error should be held a minimum of 3 seconds. Any of these errors should cause the on board fault LED to blink.

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void ShieldData_ResetData(void);
en_shield_error ShieldData_GetError(void);
uint8_t ShieldData_GetVersion_Major(void);
uint8_t ShieldData_GetVersion_Minor(void);

void ShieldData_IncMessageCounter( void );
uint16_t ShieldData_GetMessageCounter( void );
void ShieldData_IncTimeoutCounter( uint16_t uwInterval_ms );
void ShieldData_ClrTimeoutCounter();
uint8_t ShieldData_CheckTimeoutCounter();
uint16_t ShieldData_GetTimeoutCounter();

void LoadDatagram_SyncTime( uint32_t uiTime );
void LoadDatagram_ParamSync_GUI(uint8_t* aucData);

void UnloadDatagram_SyncTime( un_sdata_datagram *punDatagram );
void UnloadDatagram_RemoteCommand( un_sdata_datagram *punDatagram );
void UnloadDatagram_LatchCarCall( un_sdata_datagram *punDatagram );
void UnloadDatagram_LatchHallCall( un_sdata_datagram *punDatagram );
void UnloadDatagram_ClearHallCall( un_sdata_datagram *punDatagram );
void UnloadDatagram_VirtualCCEnables( un_sdata_datagram *punDatagram );
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
