/***
 *       _____                          __         _
 *      / ___/ ____ ___   ____ _ _____ / /_ _____ (_)_____ ___
 *      \__ \ / __ `__ \ / __ `// ___// __// ___// // ___// _ \
 *     ___/ // / / / / // /_/ // /   / /_ / /   / /(__  )/  __/
 *    /____//_/ /_/ /_/ \__,_//_/    \__//_/   /_//____/ \___/
 *        ______               _                           _
 *       / ____/____   ____ _ (_)____   ___   ___   _____ (_)____   ____ _
 *      / __/  / __ \ / __ `// // __ \ / _ \ / _ \ / ___// // __ \ / __ `/
 *     / /___ / / / // /_/ // // / / //  __//  __// /   / // / / // /_/ /
 *    /_____//_/ /_/ \__, //_//_/ /_/ \___/ \___//_/   /_//_/ /_/ \__, /
 *                  /____/                                       /____/
 */
#ifndef _GROUP_NET_DATA_H_
#define _GROUP_NET_DATA_H_

#include <stdint.h>


uint8_t Dispatch_GetEmergencyDispatchFlag(void);

uint8_t GetCarRemovedFromGroupFlag(void);
uint8_t GetFaultedCarRemovedFromGroupFlag(void);
/*-----------------------------------------------------------------------------
   Swing Calls
 -----------------------------------------------------------------------------*/
void UpdateSwingCall();
uint8_t GetAnySwingCallActive( void );
void SetSwingCall  ( void );
void ClearSwingCall( void );

/*-----------------------------------------------------------------------------
   Group Landing Index <--> Car Floor Index
 -----------------------------------------------------------------------------*/
uint8_t GetGroupMapping ( uint8_t ucFloorIndex );;
uint8_t GetCarMapping ( uint8_t ucLandingIndex );

#endif // _GROUP_NET_DATA_H_
