/******************************************************************************
 *
 * @file     mod.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef MOD_H
#define MOD_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
#include "fault_app.h"
#include "alarm_app.h"
#include "shared_data.h"
#include "datagrams.h"
#include "sru_b.h"
#include "sdata_AB.h"
#include "operation.h"
#include "position.h"
#include "motion.h"

#include "mod_loadweigher.h"
#include "mod_run_log.h"
#include "riserData.h"
#include "hallData.h"
#include "groupnet.h"
#include "mod_lanternMonitor.h"
#include "mod_ems.h"
#include "mod_shield.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_UART_1MS              (5U)
#define MOD_RUN_PERIOD_CAN_1MS               (5U)
#define MOD_RUN_PERIOD_AB_NET_1MS            (5U)
#define MOD_RUN_PERIOD_FAULT_APP_1MS         (5U)
#define MOD_RUN_PERIOD_FAULT_1MS             (5U)
#define MOD_RUN_PERIOD_UI_1MS                (30U)
#define MOD_RUN_PERIOD_POSITION_1MS          (5U)
#define MOD_RUN_PERIOD_CE_BOARD_1MS          (5U)
#define FAULT_LOG_REQ_UPDATE_RATE_1MS        ( 300U )
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/



struct st_hallcalls
{
   uint8_t bRecentlyPressed_Down: 1; //flag for recently Pressed Down from the hall board
   uint8_t bRecentlyPressed_Up: 1;   //flag for recently pressed Up from the hall board
   uint8_t bNowPressed_Down: 1; //flag for now Pressed Down from the hall board
   uint8_t bNowPressed_Up: 1;   //flag for now pressed Up from the hall board
   uint8_t bNowLamp_Down: 1; //flag for lamp now on Down from the hall board
   uint8_t bNowLamp_Up: 1;   //flag for lamp now on from the hall board

   uint8_t bCommand_Down: 1; //command down for the down hall call lamp
   uint8_t bCommand_Up: 1; //command up will be used for the hall call lamp
   uint8_t bLatched_Down: 1; //latched down for the down hall call lamp
   uint8_t bLatched_Up: 1; //latched up will be used for the hall call lamp

   uint8_t bFlashLamp: 1;
   uint8_t bSecured: 1;
};

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

extern struct st_module_control *gpstModuleControl_ThisDeployment;

extern struct st_sdata_control gstSData_GroupNet_MRB;

extern struct st_sdata_control gstSData_LocalData;

extern struct st_sdata_control * gpastSData_Nodes_AuxNet[ NUM_MR_AUX_NET_NODES ];

extern struct st_module gstMod_UART;
extern struct st_module gstMod_SData_AuxNet;
extern struct st_module gstMod_SData_GroupNet;
extern struct st_module gstMod_ParamApp;
extern struct st_module gstMod_ParamMRB;

extern struct st_module gstMod_UI;
extern struct st_module gstMod_Buttons;
extern struct st_module gstMod_LCD;

extern struct st_module gstMod_Heartbeat;
extern struct st_module gstMod_CEboards;
extern struct st_module gstMod_CAN;

extern struct st_module gstMod_Watchdog;
extern struct st_module gstMod_FaultApp;
extern struct st_module gstMod_Fault;
extern struct st_module gstMod_SData;

extern struct st_module gstMod_Position;
extern struct st_module gstMod_LocalOutputs;
extern struct st_module gstMod_Dispatching;

extern struct st_module gstMod_EmergencyPowerMaster;

extern struct st_module gstMod_XReg;

extern struct st_module gstMod_DyPark;

extern st_fault gstFault;

extern uint8_t gaucLatchedCallsIndex_Plus1[MAX_VISIBLE_CALLS];

extern char *pasVersion;
 /*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
char * Get_PI_Label( uint8_t ucFloor );

void SharedData_AB_Init( );
void SharedData_GroupNet_Init();
void SharedData_AuxNet_Init();

extern uint8_t NTS_GetState();
extern uint8_t NTS_GetAlarm();
extern uint8_t NTS_GetFault();
extern uint32_t NTS_Get_Pos( uint8_t ucIndex, uint8_t bUp );

extern uint8_t ParamApp_GetFault(void);

uint8_t SaveAndVerifyParameter( struct st_param_and_value *pstPV );

void Update_CC_LatchRequest( void );

/*
 * Hall Call Controller
 */
uint8_t GetGroupCurrentFloor( void );
void SetHallCallData (un_sdata_datagram *unDatagram);
uint8_t RecieveRiserCommand( uint8_t ucRiser, uint8_t ucDatagramID, un_sdata_datagram *unDatagram );

Status UART_LoadCANMessage(CAN_MSG_T *pstTxMsg);
uint16_t UART_GetRxErrorCount(void);
uint8_t CheckIf_SendOnABNet(uint16_t uwDestinationBitmap);
void SharedData_Init();

void SetLocalOutputs_ThisNode(uint8_t uwOutputs);

void Update_UIRequest_DefaultCommand( void );
uint16_t GetDebugBusOfflineCounter_CAN1();
uint16_t GetDebugBusOfflineCounter_CAN2();

void SetUIRequest_ViewDebugData(en_view_debug_data eVDD);
void ClrUIRequest_ViewDebugData(void);
en_view_debug_data GetUIRequest_ViewDebugData(void);
#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

