/******************************************************************************
 *
 * @file     mod.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note    Monitors hall lantern boards & their data. Revised from mod_hallMonitor.h
 *          from App_MCU_E
 *
 ******************************************************************************/

#ifndef _MOD_LANTERN_MONITOR_H_
#define _MOD_LANTERN_MONITOR_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_LANTERN_MONITOR_1MS       ( 100 )

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_module gstMod_LanternMonitor;
 /*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

uint8_t GetHallLanternStatus_IO(void);
en_hallboard_error GetHallLanternStatus_Error(void);
en_hb_com_state GetHallLanternStatus_Comm(void);

void LoadDatagram_HallLanterns( struct st_sdata_control *pstSData_AuxNet );
void UnloadDatagram_HallLanternStatus( uint16_t uwDIPs, uint8_t *pucData );

#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

