/******************************************************************************
 *
 * @file     mod_run_log.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef _MASTER_COMMAND_H_
#define _MASTER_COMMAND_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   MASTER_CMD__None,
   MASTER_CMD__SetHallCall,
   MASTER_CMD__ClrHallCall,
   MASTER_CMD__SetCarCall,
   MASTER_CMD__EMS_Assignment_Car1,
   MASTER_CMD__EMS_Assignment_Car2,
   MASTER_CMD__EMS_Assignment_Car3,
   MASTER_CMD__EMS_Assignment_Car4,
   MASTER_CMD__EMS_Assignment_Car5,
   MASTER_CMD__EMS_Assignment_Car6,
   MASTER_CMD__EMS_Assignment_Car7,
   MASTER_CMD__EMS_Assignment_Car8,
   NUM_MASTER_CMD
} en_master_cmd;

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
uint8_t MasterCommand_GetDirtyBit( en_master_cmd eCMD );
void MasterCommand_SetDirtyBit( en_master_cmd eCMD );
void MasterCommand_ResetCommandBuffer( en_master_cmd eCMD );
void MasterCommand_SetHallCall( uint8_t ucLanding, en_hc_dir eDir, uint32_t uiMask );
void MasterCommand_ClrHallCall( uint8_t ucLanding, en_hc_dir eDir, uint32_t uiMask );
void MasterCommand_SetCarCall( enum en_group_net_nodes eCarID, uint8_t ucLanding, enum en_doors eDoor );
void MasterCommand_SetEMSAssignment( enum en_group_net_nodes eCarID, uint8_t ucLanding_Plus1 );

uint8_t LoadDatagram_MasterCommand( un_sdata_datagram *punDatagram );
void UnloadDatagram_MasterCommand( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram );
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
