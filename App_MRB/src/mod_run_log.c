/******************************************************************************
 *
 * @file     mod_run_log.c
 * @brief
 * @version  V1.00
 * @date     11, April 2017
 *
 * @note    This module stores the run data for the most recent run.
 *          On MRB: The module unloads and buffers latest run log data from MRA
 *          and loads it for transmit to BBB
 *
 *          Timestamped:
 *             Drive HW Enable
 *             B2 Cont. Pick
 *             M Cont. Pick
 *             E Brake Pick
 *             Command Zero Speed
 *             Brake Pick
 *             Command Nonzero Speed
 *
 *             End of Accel
 *             End of Cruising
 *             Start of Leveling
 *
 *          Interval (100 ms):
 *             Car Speed
 *             Commanded Speed
 *             Position
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "sys.h"
#include "pattern.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_RunLog =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *s
 *----------------------------------------------------------------------------*/

static int32_t aiTransitionTimestamp_ms[NUM_TPs];
//#define MAX_RUN_LOG_SIZE      (MAX_PATTERN_SIZE_BYTES/(MOD_RUN_PERIOD_RUN_LOG_1MS/MOD_RUN_PERIOD_MOTION_1MS))
#define MAX_RUN_LOG_SIZE      (180)//Fix run log size to increase run log module run rate
static Run_Log_Item stAccelRunLog[MAX_RUN_LOG_SIZE];
static Run_Log_Item stDecelRunLog[MAX_RUN_LOG_SIZE];

static Run_Log_States eState;
//static Transition_Points eTransitionPointIndex;

static uint32_t uiExpectedSize_TP;
static uint32_t uiExpectedSize_Accel;
static uint32_t uiExpectedSize_Decel;

/*  */
static uint8_t bNewLog; //Signals new run in available to transmit
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
      State 0: Idle
      State 1: Send Transition points
      State 2: Send Accel points
      State 3: Send Decel points
 -----------------------------------------------------------------------------*/
static void UnloadRunLog()
{
   static uint8_t ucState;
   static uint32_t uiIndex;
   static uint8_t ucLastAlignment;

   if( GetDatagramDirtyBit( DATAGRAM_ID_190 ) )
   {
      ClrDatagramDirtyBit( DATAGRAM_ID_190 );
      /* Only accept new log if last has been transmitted to BBB */
      if( !bNewLog )
      {
         if( GetRunLog_StartCommand() == 0x544F53 )// 'SOT'
         {
            uiExpectedSize_TP = GetRunLog_ExpectedSize();
            uiIndex = 0;
            memset(&aiTransitionTimestamp_ms[0], 0, sizeof(aiTransitionTimestamp_ms));
            memset(&stAccelRunLog[0], 0, sizeof(stAccelRunLog));
            memset(&stDecelRunLog[0], 0, sizeof(stDecelRunLog));
            ucState = 1;
         }
         else if( GetRunLog_StartCommand() == 0x414F53 )// 'SOA'
         {
            uiExpectedSize_Accel = GetRunLog_ExpectedSize();
            uiIndex = 0;
            ucState = 2;
         }
         else if( GetRunLog_StartCommand() == 0x444F53 )// 'SOD'
         {
            uiExpectedSize_Decel = GetRunLog_ExpectedSize();
            uiIndex = 0;
            ucState = 3;
         }
         else if( GetRunLog_StartCommand() == 0x444F45 )// 'EOD'
         {
            bNewLog = 1;// Set flag for complete log received
            uiIndex = 0;
            ucState = 0;
         }
      }

      if( !ucState)
      {
         //Hold until command characters received
      }
      else if(ucState == 1)//log TP
      {
         if( GetRunLog_StartCommand() != 0x544F53 )// 'SOT'
         {
            uiIndex = GetRunLog_TimestampIndex();
            if( GetRunLog_TimestampIndex() < NUM_TPs )
            {
               aiTransitionTimestamp_ms[ uiIndex ] = GetRunLog_Timestamp();
            }
         }
      }
      else if(ucState == 2)//log accel
      {
         if( GetRunLog_StartCommand() != 0x414F53 )// 'SOA'
         {
            ucLastAlignment = GetRunLog_AlignmentByte();
            if( ( ucLastAlignment < MAX_PATTERN_SIZE_BYTES/(MOD_RUN_PERIOD_RUN_LOG_1MS/MOD_RUN_PERIOD_MOTION_1MS ) )
             && ( ucLastAlignment < uiExpectedSize_Accel ) )
            {
               stAccelRunLog[ucLastAlignment].uiCarPosition = GetRunLog_CarPosition();
               stAccelRunLog[ucLastAlignment].wCarSpeed = GetRunLog_CarSpeed();
               stAccelRunLog[ucLastAlignment].wCmdSpeed = GetRunLog_CommandSpeed();
            }
         }
      }
      else if(ucState == 3)//log decel
      {
         if( GetRunLog_StartCommand() != 0x444F53 )// 'SOD'
         {
            ucLastAlignment = GetRunLog_AlignmentByte();
            if( ( ucLastAlignment < MAX_PATTERN_SIZE_BYTES/(MOD_RUN_PERIOD_RUN_LOG_1MS/MOD_RUN_PERIOD_MOTION_1MS ) )
             && ( ucLastAlignment < uiExpectedSize_Decel ) )
            {
               stDecelRunLog[ucLastAlignment].uiCarPosition = GetRunLog_CarPosition();
               stDecelRunLog[ucLastAlignment].wCarSpeed = GetRunLog_CarSpeed();
               stDecelRunLog[ucLastAlignment].wCmdSpeed = GetRunLog_CommandSpeed();
               uiIndex++;
            }
         }
      }
      else
      {
         ucState = 0;
      }
   }
}

/*-----------------------------------------------------------------------------
   On request, start sending run log packet
      State 0: Idle
      State 1: Send Transition points
      State 2: Send Accel points
      State 3: Send Decel points
      State 4: End of decel

 -----------------------------------------------------------------------------*/
//Delay between log transmissions to allow data to accumulate on MRB and then transmit to BBB
#define RUN_LOG_TRANSMISSION_DELAY_5MS          (2000)
void LoadDatagram_RunLog_MRB()
{
   static uint32_t uiIndex;
   static uint32_t uiTransmitDelayCountdown_5ms;
   static uint8_t ucState;
   static uint8_t ucLastState;
   static uint8_t ucResendCounter;

   uint8_t bFirst = 0;
   static un_sdata_datagram unDatagram;
   Run_Log_Item stRunLogItem;

   if( ( Param_ReadValue_1Bit( enPARAM1__DEBUG_TransmitRunLog ) )
    && ( GetOperation_ClassOfOp() == CLASSOP__AUTO ) )
   {
      if( !SDATA_DirtyBit_Get( &gstSData_GroupNet_MRB, DG_GroupNet_MRB__RunLog ) )
      {
         if(++ucResendCounter >= Param_ReadValue_8Bit(enPARAM8__NumResendRunLog))
         {
            ucResendCounter = 0;
            /* If state has changed since last cycle, run different code set */
            if( ucLastState != ucState )
            {
               ucLastState = ucState;
               uiIndex = 0;
               bFirst = 1;
            }

            memset( &unDatagram, 0, sizeof( un_sdata_datagram ) );
            switch( ucState )
            {
               case 0:
                  if( uiTransmitDelayCountdown_5ms )
                  {
                     uiTransmitDelayCountdown_5ms--;
                  }
                  /* If new log is available, then transmit to MRB */
                  if( bNewLog
                   && !uiTransmitDelayCountdown_5ms )
                  {
                     uiTransmitDelayCountdown_5ms = RUN_LOG_TRANSMISSION_DELAY_5MS;
                     ucState = 1;
                  }
                  break;

               case 1:
                  if( bFirst )
                  {
                     unDatagram.auc8[0] = 'S'; //Start of frame command
                     unDatagram.auc8[1] = 'O';
                     unDatagram.auc8[2] = 'T';
                     unDatagram.aui32[1] = NUM_TPs;
                     unDatagram.auc8[7] = Param_ReadValue_8Bit( enPARAM8__Debug_RunLogScaling );
                  }
                  else
                  {
                     unDatagram.auc8[0] = uiIndex & 0xFF;
                     unDatagram.aui32[1] = GetRunLog_TransitionPointTimestamp( uiIndex );
                     if( ++uiIndex >= NUM_TPs )
                     {
                        ucState = 2;
                     }
                  }
                  break;

               case 2:
                  if( bFirst )
                  {
                     unDatagram.auc8[0] = 'S'; //Start of Accel
                     unDatagram.auc8[1] = 'O';
                     unDatagram.auc8[2] = 'A';
                     unDatagram.aui32[1] = GetRunLog_AccelLogSize();
                  }
                  else
                  {
                     if( GetRunLog_AccelLogItem( &stRunLogItem, uiIndex ) )
                     {
                        unDatagram.auw16[0] = stRunLogItem.wCmdSpeed;
                        unDatagram.auw16[1] = stRunLogItem.wCarSpeed;
                        unDatagram.aui32[1] = stRunLogItem.uiCarPosition;
                        unDatagram.auc8[7] = uiIndex & 0xFF;
                     }

                     if( ++uiIndex >= GetRunLog_AccelLogSize() )
                     {
                        ucState = 3;
                     }
                  }
                  break;

               case 3:
                  if( bFirst )
                  {
                     unDatagram.auc8[0] = 'S'; //Start of Decel
                     unDatagram.auc8[1] = 'O';
                     unDatagram.auc8[2] = 'D';
                     unDatagram.aui32[1] = GetRunLog_DecelLogSize();
                  }
                  else
                  {
                     if( GetRunLog_DecelLogItem( &stRunLogItem, uiIndex ) )
                     {
                        unDatagram.auw16[0] = stRunLogItem.wCmdSpeed;
                        unDatagram.auw16[1] = stRunLogItem.wCarSpeed;
                        unDatagram.aui32[1] = stRunLogItem.uiCarPosition;
                        unDatagram.auc8[7] = uiIndex & 0xFF;
                     }

                     if( ++uiIndex >= GetRunLog_DecelLogSize() )
                     {
                        ucState = 4;
                     }
                  }
                  break;

               case 4:
                  if( bFirst )
                  {
                     unDatagram.auc8[0] = 'E'; //End of Decel
                     unDatagram.auc8[1] = 'O';
                     unDatagram.auc8[2] = 'D';
                     unDatagram.aui32[1] = 0;
                  }
                  else
                  {
                     if(!bNewLog)
                     {
                        ucState = 0;
                     }
                     /* Set flag to allow receiving of new run log */
                     bNewLog = 0;
                  }
                  break;

               default:
                  ucState = 0;
                  break;
            }
         }
         SDATA_WriteDatagram( &gstSData_GroupNet_MRB, DG_GroupNet_MRB__RunLog, &unDatagram );
         if( ucState )
         {
            SDATA_DirtyBit_Set( &gstSData_GroupNet_MRB, DG_GroupNet_MRB__RunLog );
         }
      }
   }
   else
   {
      unDatagram.aui32[0] = 0;
      unDatagram.aui32[1] = 0;
      SDATA_WriteDatagram( &gstSData_GroupNet_MRB, DG_GroupNet_MRB__RunLog, &unDatagram );
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
Run_Log_States GetRunLog_State()
{
   return eState;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint32_t GetRunLog_AccelLogSize()
{
   return uiExpectedSize_Accel;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint32_t GetRunLog_DecelLogSize()
{
   return uiExpectedSize_Decel;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t GetRunLog_AccelLogItem( Run_Log_Item *pstRunLogItem, uint32_t uiIndex )
{
   uint8_t bSuccess = 0;
   if( uiIndex < uiExpectedSize_Accel )
   {
      bSuccess = 1;
      memcpy( pstRunLogItem, &stAccelRunLog[uiIndex], sizeof( Run_Log_Item ) );
   }
   return bSuccess;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t GetRunLog_DecelLogItem( Run_Log_Item *pstRunLogItem, uint32_t uiIndex )
{
   uint8_t bSuccess = 0;
   if( uiIndex < uiExpectedSize_Decel )
   {
      bSuccess = 1;
      memcpy( pstRunLogItem, &stDecelRunLog[uiIndex], sizeof( Run_Log_Item ) );
   }
   return bSuccess;
}
/*-----------------------------------------------------------------------------
   Returns the timestamp of each transition point of the last run
 -----------------------------------------------------------------------------*/
int32_t GetRunLog_TransitionPointTimestamp( Transition_Points eTransitionPoint )
{
   int32_t iReturn = 0;
   if( eTransitionPoint < NUM_TPs )
   {
      iReturn = aiTransitionTimestamp_ms[eTransitionPoint];
   }
   return iReturn;
}



/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 1000;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_RUN_LOG_1MS;

   return 0;
}

/*-----------------------------------------------------------------------------


 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   if( GetOperation_ClassOfOp() == CLASSOP__AUTO )
   {
      UnloadRunLog();
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
