/******************************************************************************
 *
 * @file     mod_ems.c
 * @brief    Manages EMS emergency medical recall requests and their assignments to cars
 * @version  V1.00
 * @date     7, August 2019
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "carData.h"
#include "mod_ems.h"
#include "masterCommand.h"
#include "group_net_data.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init(struct st_module * pstThisModule);
static uint32_t Run(struct st_module * pstThisModule);

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_EMS =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t aucRecallLanding_Plus1[MAX_GROUP_CARS];

static uint16_t auwCarMoveTimeout_500ms[MAX_GROUP_CARS];
static uint16_t auwCarOfflineCountdown_500ms[MAX_GROUP_CARS];
/*---------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint8_t EMS_GetRecallLanding_Plus1( enum en_group_net_nodes eCarID )
{
   return aucRecallLanding_Plus1[eCarID];
}
void EMS_SetRecallLanding_Plus1( enum en_group_net_nodes eCarID, uint8_t ucRecallLanding_Plus1 )
{
   aucRecallLanding_Plus1[eCarID] = ucRecallLanding_Plus1;
}

/*-----------------------------------------------------------------------------
   Checks & removes current recall assignment if:
      If a car's recall assignment has timed out without car response
      If a car's mode no longer supports medical calls
      If two cars are assigned the same landing.
 TODO
-----------------------------------------------------------------------------*/
static void EMS_CheckForInvalidRecall(void)
{
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      if( auwCarOfflineCountdown_500ms[i] )
      {
         auwCarOfflineCountdown_500ms[i]--;
      }
   }

   if( Param_ReadValue_8Bit(enPARAM8__DispatchTimeout_1s)
    && Param_ReadValue_8Bit(enPARAM8__DispatchOffline_1s) )
   {
      uint16_t uwMoveTimeout_500ms =  Param_ReadValue_8Bit(enPARAM8__DispatchTimeout_1s) * 2;
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         if( aucRecallLanding_Plus1[i] )
         {
            st_cardata *pstCarData = GetCarDataStructure(i);
            if( ( pstCarData->eAutoMode == MODE_A__EMS1 )
             && ( ( GetMotion_RunFlag() ) || ( pstCarData->eRecallState == RECALL__RECALL_FINISHED ) ) )
            {
               auwCarMoveTimeout_500ms[i] = 0;
            }
            else if( auwCarMoveTimeout_500ms[i] < uwMoveTimeout_500ms )
            {
               auwCarMoveTimeout_500ms[i]++;
            }
            else
            {
               auwCarOfflineCountdown_500ms[i] = Param_ReadValue_8Bit(enPARAM8__DispatchOffline_1s) * 2;
               en_alarms eAlarm = ALM__DISPATCH_TIMEOUT_C1 + i;
               if( eAlarm > ALM__DISPATCH_TIMEOUT_C8 )
               {
                  eAlarm = ALM__DISPATCH_TIMEOUT_C8;
               }
               SetAlarm( eAlarm );
               auwCarMoveTimeout_500ms[i] = 0;
            }
         }
         else
         {
            auwCarMoveTimeout_500ms[i] = 0;
         }
      }
   }
   else
   {
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         auwCarMoveTimeout_500ms[i] = 0;
         auwCarOfflineCountdown_500ms[i] = 0;
      }
   }
}
/*-----------------------------------------------------------------------------
   Returns 1 if the currently latched call is still available
-----------------------------------------------------------------------------*/
static uint8_t EMS_GetCurrentCallStatus( enum en_group_net_nodes eCarID )
{
   uint8_t bReturn = 0;
   st_cardata *pstCarData = GetCarDataStructure(eCarID);
   if( aucRecallLanding_Plus1[eCarID] )
   {
      uint8_t ucLanding = aucRecallLanding_Plus1[eCarID]-1;
      uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__UP ) | GetRawLatchedHallCalls( ucLanding, HC_DIR__DOWN );
      uint32_t uiValidCalls = uiLatchedCalls & pstCarData->ucMedicalMask;
      if( uiValidCalls )
      {
         bReturn = 1;
      }
   }
   return bReturn;
}
/*-----------------------------------------------------------------------------
   Returns the landing of the nearest medical call
-----------------------------------------------------------------------------*/
static uint8_t EMS_GetNearestMedicalCall_Above( enum en_group_net_nodes eCarID )
{
   uint8_t ucNearestLanding = INVALID_FLOOR;
   st_cardata *pstCarData = GetCarDataStructure(eCarID);
   for(int16_t wLanding = pstCarData->ucCurrentLanding; wLanding <= pstCarData->ucLastLanding; wLanding++)
   {
      uint8_t bValidOpening = CarData_CheckOpening( eCarID, wLanding, DOOR_ANY );
      if( bValidOpening )
      {
         uint32_t uiLatchedCalls = GetRawLatchedHallCalls( wLanding, HC_DIR__UP ) | GetRawLatchedHallCalls( wLanding, HC_DIR__DOWN );
         uint32_t uiValidCalls = uiLatchedCalls & pstCarData->ucMedicalMask;
         if( uiValidCalls )
         {
            uint32_t uiReservedMask = 0;
            for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
            {
               st_cardata *pstCarData2 = GetCarDataStructure(i);
               if( ( GetCarOnlineFlag_ByCar(i) )
                && ( wLanding == ( aucRecallLanding_Plus1[i]-1 ) ) )
               {
                  uiReservedMask |= pstCarData2->ucMedicalMask;
               }
            }
            uint32_t uiRemainderMask = uiValidCalls & ~uiReservedMask;
            if( uiRemainderMask )
            {
               ucNearestLanding = wLanding;
               break;
            }
         }
      }
   }

   return ucNearestLanding;
}
static uint8_t EMS_GetNearestMedicalCall_Below( enum en_group_net_nodes eCarID )
{
   uint8_t ucNearestLanding = INVALID_FLOOR;
   st_cardata *pstCarData = GetCarDataStructure(eCarID);
   for(int16_t wLanding = pstCarData->ucCurrentLanding; wLanding >= pstCarData->ucFirstLanding; wLanding--)
   {
      uint8_t bValidOpening = CarData_CheckOpening( eCarID, wLanding, DOOR_ANY );
      if( bValidOpening )
      {
         uint32_t uiLatchedCalls = GetRawLatchedHallCalls( wLanding, HC_DIR__UP ) | GetRawLatchedHallCalls( wLanding, HC_DIR__DOWN );
         uint32_t uiValidCalls = uiLatchedCalls & pstCarData->ucMedicalMask;
         if( uiValidCalls )
         {
            uint32_t uiReservedMask = 0;
            for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
            {
               if( ( GetCarOnlineFlag_ByCar(i) )
                && ( wLanding == ( aucRecallLanding_Plus1[i]-1 ) ) )
               {
                  st_cardata *pstCarData2 = GetCarDataStructure(i);
                  uiReservedMask |= pstCarData2->ucMedicalMask;
               }
            }
            uint32_t uiRemainderMask = uiValidCalls & ~uiReservedMask;
            if( uiRemainderMask )
            {
               ucNearestLanding = wLanding;
               break;
            }
         }
      }
   }

   return ucNearestLanding;
}
/*-----------------------------------------------------------------------------
   Check for latched EMS recall requests and assigns the nearest available car
-----------------------------------------------------------------------------*/
static void EMS_UpdateMedicalRecallDestinations(void)
{
   uint8_t aucProposedLandings_Plus1[MAX_GROUP_CARS];
   uint8_t aucProposedDistance[MAX_GROUP_CARS];
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      aucProposedLandings_Plus1[i] = 0;
      aucProposedDistance[i] = INVALID_FLOOR;
      st_cardata *pstCarData = GetCarDataStructure(i);
      if( GetCarOnlineFlag_ByCar(i)
       && pstCarData->ucMedicalMask
       && !auwCarOfflineCountdown_500ms[i] )
      {
         /* Check available cars for new recall destinations */
         if( !aucRecallLanding_Plus1[i] )
         {
            /* Find the nearest EMS call in the up and down direction */
            uint8_t ucNearestCall_UP = EMS_GetNearestMedicalCall_Above(i);
            uint8_t ucNearestCall_DN = EMS_GetNearestMedicalCall_Below(i);
            uint8_t ucDistance_UP = INVALID_FLOOR;
            uint8_t ucDistance_DN = INVALID_FLOOR;
            if( ucNearestCall_UP <= pstCarData->ucLastLanding )
            {
               ucDistance_UP = ucNearestCall_UP - pstCarData->ucCurrentLanding;
               aucProposedLandings_Plus1[i] = ucNearestCall_UP+1;
               aucProposedDistance[i] = ucDistance_UP;
            }
            if( ucNearestCall_DN <= pstCarData->ucLastLanding )
            {
               ucDistance_DN = pstCarData->ucCurrentLanding - ucNearestCall_DN;
               if( ucDistance_DN < aucProposedDistance[i] )
               {
                  aucProposedLandings_Plus1[i] = ucNearestCall_DN+1;
                  aucProposedDistance[i] = ucDistance_DN;
               }
            }
         }
         /* If a car has an assignment check if its recall assignment is still valid */
         else
         {
            uint8_t bCurrentCallStatus = EMS_GetCurrentCallStatus(i);
            if( ( pstCarData->eAutoMode == MODE_A__EMS1 )
             && ( bCurrentCallStatus ) )
            {
               aucProposedLandings_Plus1[i] = aucRecallLanding_Plus1[i];
            }
            else
            {
               aucProposedLandings_Plus1[i] = 0;
               aucRecallLanding_Plus1[i] = 0;
            }
         }
      }
      else
      {
         aucProposedLandings_Plus1[i] = 0;
         if( aucRecallLanding_Plus1[i] )
         {
            aucRecallLanding_Plus1[i] = 0;
         }
      }
   }

   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      if( !aucRecallLanding_Plus1[i] && aucProposedLandings_Plus1[i] )
      {
         /* Compare proposed destination with other car's
          * proposals if two cars propose for the same
          * landing, clear out the one further away */
         for(uint8_t j = 0; j < MAX_GROUP_CARS; j++)
         {
            if( ( i != j )
             && ( aucProposedLandings_Plus1[i] == aucProposedLandings_Plus1[j] ) )
            {
               if( aucProposedDistance[i] <= aucProposedDistance[j] )
               {
                  aucProposedLandings_Plus1[j] = 0;
               }
               else
               {
                  aucProposedLandings_Plus1[i] = 0;
               }
            }
         }

         if( aucProposedLandings_Plus1[i] )
         {
            aucRecallLanding_Plus1[i] = aucProposedLandings_Plus1[i];
         }
      }
   }
}
/*-----------------------------------------------------------------------------
   Load medical recall assignment packets for transmission
-----------------------------------------------------------------------------*/
static void EMS_LoadAssignmentPackets(void)
{
   static uint8_t aucMinResendTimer_500ms[MAX_GROUP_CARS];
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      if( i != GetFP_GroupCarIndex() )
      {
         MasterCommand_SetEMSAssignment(i, aucRecallLanding_Plus1[i]);
         en_master_cmd eCommand = MASTER_CMD__EMS_Assignment_Car1+i;
         if( MasterCommand_GetDirtyBit(eCommand) )
         {
            aucMinResendTimer_500ms[i] = 0;
         }
         else
         {
            if( aucRecallLanding_Plus1[i] )
            {
               if( aucMinResendTimer_500ms[i] >= EMS_ASSIGNMENT_MIN_RESEND_LATCHED_500MS )
               {
                  aucMinResendTimer_500ms[i] = 0;
                  MasterCommand_SetDirtyBit(eCommand);
               }
               else
               {
                  aucMinResendTimer_500ms[i]++;
               }
            }
            else
            {
               if( aucMinResendTimer_500ms[i] >= EMS_ASSIGNMENT_MIN_RESEND_CLEARED_500MS )
               {
                  aucMinResendTimer_500ms[i] = 0;
                  MasterCommand_SetDirtyBit(eCommand);
               }
               else
               {
                  aucMinResendTimer_500ms[i]++;
               }
            }
         }
      }
   }


}
/*-----------------------------------------------------------------------------

Initialize anything required prior to running for the first time

-----------------------------------------------------------------------------*/
static uint32_t Init(struct st_module * pstThisModule)
{
   pstThisModule->uwInitialDelay_1ms = 500;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_EMS_1MS;

   return 0;
}

/*-----------------------------------------------------------------------------
 *
-----------------------------------------------------------------------------*/
static uint32_t Run(struct st_module * pstThisModule)
{
   if( GetMasterDispatcherFlag() )
   {
      EMS_CheckForInvalidRecall();
      EMS_UpdateMedicalRecallDestinations();
      EMS_LoadAssignmentPackets();
   }
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
*----------------------------------------------------------------------------*/
