
/******************************************************************************
 * @author   Keith Soneda
 * @version  V1.00
 * @date     1, August 2016
 *
 * @note    Monitors hall lantern boards & their data. Revised from mod_hallMonitor.c
 *          from App_MCU_E
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <mod_lanternMonitor.h>
#include "mod.h"
#include "sys.h"
#include <string.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_LanternMonitor =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define HALLBOARD_INACTIVE_OFFLINE_COUNT        (255)
#define TIMEOUT_HALLBOARD_OFFLINE_MS            (20000)
#define HALLBOARD_STATUS_MIN_RESEND_RATE_MS     (5000)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
/* Defines the limits for interpreting auwOfflineCounter values as NUM_HB_COM_STATE */
static const uint16_t auwComStatesCountLimits[NUM_HB_COM_STATE] =
{
(HALLBOARD_INACTIVE_OFFLINE_COUNT),//HB_COM_STATE__INACTIVE,
(TIMEOUT_HALLBOARD_OFFLINE_MS/MOD_RUN_PERIOD_LANTERN_MONITOR_1MS),//HB_COM_STATE__0,
(0.9*TIMEOUT_HALLBOARD_OFFLINE_MS/MOD_RUN_PERIOD_LANTERN_MONITOR_1MS),//HB_COM_STATE__25,
(0.8*TIMEOUT_HALLBOARD_OFFLINE_MS/MOD_RUN_PERIOD_LANTERN_MONITOR_1MS),//HB_COM_STATE__50,
(0.7*TIMEOUT_HALLBOARD_OFFLINE_MS/MOD_RUN_PERIOD_LANTERN_MONITOR_1MS),//HB_COM_STATE__75,
(0.6*TIMEOUT_HALLBOARD_OFFLINE_MS/MOD_RUN_PERIOD_LANTERN_MONITOR_1MS),//HB_COM_STATE__100,
};

static en_hallboard_error eError;
static en_hb_com_state eCommState;
static uint8_t ucBF_RawIO; // see en_hb_io_shifts

static uint16_t auwOfflineCounter[MAX_NUM_HALL_LANTERNS];

static uint16_t uwLastUnloadedAddress_Plus1;

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
uint8_t GetHallLanternStatus_IO(void)
{
   uint16_t uwAddress_Plus1 = GetUIRequest_HallStatusIndex_Plus1() - HALL_UI_REQ__HALL_LANTERN__START + 1;
   if( uwAddress_Plus1 == uwLastUnloadedAddress_Plus1 )
   {
      return ucBF_RawIO;
   }
   else
   {
      return 0;
   }
}
en_hallboard_error GetHallLanternStatus_Error(void)
{
   uint16_t uwAddress_Plus1 = GetUIRequest_HallStatusIndex_Plus1() - HALL_UI_REQ__HALL_LANTERN__START + 1;
   if( uwAddress_Plus1 == uwLastUnloadedAddress_Plus1 )
   {
      return eError;
   }
   else
   {
      return 0;
   }
}
en_hb_com_state GetHallLanternStatus_Comm(void)
{
   uint16_t uwAddress_Plus1 = GetUIRequest_HallStatusIndex_Plus1() - HALL_UI_REQ__HALL_LANTERN__START + 1;
   if( uwAddress_Plus1 == uwLastUnloadedAddress_Plus1 )
   {
      return eCommState;
   }
   else
   {
      return 0;
   }
}
/*-----------------------------------------------------------------------------
Update and load hall lantern datagrams
 -----------------------------------------------------------------------------*/
void LoadDatagram_HallLanterns( struct st_sdata_control *pstSData_AuxNet )
{
   /* If lanterns are enabled */
   if(Param_ReadValue_8Bit(enPARAM8__HallLanternMask))
   {
      /* Update hall lanterns */
      uint32_t auiBF_HallLanterns_Up[DG_AuxNet_MRB__HallLamps_15-DG_AuxNet_MRB__HallLamps_0+1];
      uint32_t auiBF_HallLanterns_Down[DG_AuxNet_MRB__HallLamps_15-DG_AuxNet_MRB__HallLamps_0+1];
      memset(&auiBF_HallLanterns_Up[0], 0, sizeof(auiBF_HallLanterns_Up));
      memset(&auiBF_HallLanterns_Down[0], 0, sizeof(auiBF_HallLanterns_Down));

      uint16_t uwLanternIndex;
      uint8_t ucDestinationLanding = GetOperation_DestinationFloor() + Param_ReadValue_8Bit(enPARAM8__GroupLandingOffset);
      uint8_t ucCurrentLanding = GetOperation_CurrentFloor() + Param_ReadValue_8Bit(enPARAM8__GroupLandingOffset);
      uint32_t uiLanternMask_F = Param_ReadValue_8Bit(enPARAM8__HallLanternMask) & ~Param_ReadValue_8Bit(enPARAM8__HallLanternRearMask);
      uint32_t uiLanternMask_R = Param_ReadValue_8Bit(enPARAM8__HallLanternMask) & Param_ReadValue_8Bit(enPARAM8__HallLanternRearMask);
      if( ( ucCurrentLanding == ucDestinationLanding )
       && ( ucDestinationLanding < MAX_NUM_FLOORS ) )
      {
         uint8_t ucNumHallBoardBlocks = MAX_NUM_HALL_LANTERNS/GetHallBoard_MaxNumberOfFloors();
         /* Front */
         if( GetOutputValue(enOUT_ARV_UP_F) || GetOutputValue(enOUT_ARV_DN_F) )
         {
            for(uint8_t i = 0; i < ucNumHallBoardBlocks; i++)
            {
               /* Find active hall lantern groups */
               if( Sys_Bit_Get(&uiLanternMask_F, i) )
               {
                  uwLanternIndex = i*GetHallBoard_MaxNumberOfFloors() + ucDestinationLanding;
                  if(GetOutputValue(enOUT_ARV_UP_F))
                  {
                     Sys_Bit_Set( &auiBF_HallLanterns_Up[0], uwLanternIndex, 1 );
                  }
                  else if(GetOutputValue(enOUT_ARV_DN_F))
                  {
                     Sys_Bit_Set( &auiBF_HallLanterns_Down[0], uwLanternIndex, 1 );
                  }
               }
            }
         }

         /* Rear */
         if( Param_ReadValue_8Bit(enPARAM8__HallLanternRearMask)
        && ( GetOutputValue(enOUT_ARV_UP_R) || GetOutputValue(enOUT_ARV_DN_R) ) )
         {
            for(uint8_t i = 0; i < ucNumHallBoardBlocks; i++)
            {
               /* Find active hall lantern groups */
               if( Sys_Bit_Get(&uiLanternMask_R, i) )
               {
                  uwLanternIndex = i*GetHallBoard_MaxNumberOfFloors() + ucDestinationLanding;
                  if(GetOutputValue(enOUT_ARV_UP_R))
                  {
                     Sys_Bit_Set( &auiBF_HallLanterns_Up[0], uwLanternIndex, 1 );
                  }
                  else if(GetOutputValue(enOUT_ARV_DN_R))
                  {
                     Sys_Bit_Set( &auiBF_HallLanterns_Down[0], uwLanternIndex, 1 );
                  }
               }
            }
         }
      }

      /* Load lantern updates */
      for( uint8_t i = DG_AuxNet_MRB__HallLamps_0; i <= DG_AuxNet_MRB__HallLamps_15; i++ )
      {
         un_sdata_datagram unDatagram;
         unDatagram.aui32[0] = auiBF_HallLanterns_Down[i-DG_AuxNet_MRB__HallLamps_0];
         unDatagram.aui32[1] = auiBF_HallLanterns_Up[i-DG_AuxNet_MRB__HallLamps_0];
         SDATA_WriteDatagram(pstSData_AuxNet, i, &unDatagram);
      }
   }
}
/*-----------------------------------------------------------------------------
   Extract data received from HB CAN datagrams
 -----------------------------------------------------------------------------*/
void UnloadDatagram_HallLanternStatus( uint16_t uwDIPs, uint8_t *pucData )
{
   if( uwDIPs < MAX_NUM_HALL_LANTERNS )
   {
      auwOfflineCounter[uwDIPs] = 0; /* Clear timeout counter */

      uint16_t uwRequest_Plus1 = GetUIRequest_HallStatusIndex_Plus1() - HALL_UI_REQ__HALL_LANTERN__START + 1;
      if( uwRequest_Plus1 <= MAX_NUM_HALL_LANTERNS )
      {
         uint16_t uwAddress_Plus1 = uwDIPs+1;
         if( uwAddress_Plus1 == uwRequest_Plus1 )
         {
            uwLastUnloadedAddress_Plus1 = uwAddress_Plus1;
            ucBF_RawIO = pucData[0];
            eError = pucData[1];
         }
      }
   }
}

/*-----------------------------------------------------------------------------
   Updates the per hallboard offline counters and sets each hallboard's COM state based
   on the current value
 -----------------------------------------------------------------------------*/
static void UpdateHallboardCommState()
{
   /* Update all hall lantern timeout counters */
   for( uint16_t i = 0; i < MAX_NUM_HALL_LANTERNS; i++ )
   {
      if( ( auwOfflineCounter[i] != auwComStatesCountLimits[HB_COM_STATE__INACTIVE] )
       && ( auwOfflineCounter[i] <  auwComStatesCountLimits[HB_COM_STATE__0] ) )
      {
         auwOfflineCounter[i]++;
      }
   }

   /* Check the offline counter for the currently requested hall lantern
    * and update its communication state variable */
   uint16_t uwAddress = GetUIRequest_HallStatusIndex_Plus1() - HALL_UI_REQ__HALL_LANTERN__START;
   if( uwAddress < MAX_NUM_HALL_LANTERNS )
   {
      if( auwOfflineCounter[uwAddress] == auwComStatesCountLimits[HB_COM_STATE__INACTIVE] )
      {
         eCommState = HB_COM_STATE__INACTIVE;
         ucBF_RawIO = 0;
      }
      else if( auwOfflineCounter[uwAddress] >= auwComStatesCountLimits[HB_COM_STATE__0] )
      {
         eCommState = HB_COM_STATE__0;
         ucBF_RawIO = 0;
      }
      else
      {
         if( auwOfflineCounter[uwAddress] < auwComStatesCountLimits[HB_COM_STATE__100] )
         {
            eCommState = HB_COM_STATE__100;
         }
         else if( auwOfflineCounter[uwAddress] < auwComStatesCountLimits[HB_COM_STATE__75] )
         {
            eCommState = HB_COM_STATE__75;
         }
         else if( auwOfflineCounter[uwAddress] < auwComStatesCountLimits[HB_COM_STATE__50] )
         {
            eCommState = HB_COM_STATE__50;
         }
         else if( auwOfflineCounter[uwAddress] < auwComStatesCountLimits[HB_COM_STATE__25] )
         {
            eCommState = HB_COM_STATE__25;
         }
      }
   }
}
/*-----------------------------------------------------------------------------
Checks for need to reset due to change of the hall lantern mask after startup
 -----------------------------------------------------------------------------*/
static void CheckForNeedToResetAlarm_HallLanternMask(void)
{
   static uint8_t bFirst = 1;
   static uint8_t ucLastHallLanternMask;
   /* These setting need to be set at board initializations.
    * If they aren't, some settings may be initialized incorrectly */
   if(bFirst)
   {
      bFirst = 0;

      ucLastHallLanternMask = Param_ReadValue_8Bit(enPARAM8__HallLanternMask);
   }
   else
   {
      uint8_t ucHallLanternMask = Param_ReadValue_8Bit(enPARAM8__HallLanternMask);
      if( ucHallLanternMask != ucLastHallLanternMask )
      {
         SetAlarm(ALM__NEED_TO_RESET_MR);
      }
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 100;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_LANTERN_MONITOR_1MS;


   for( uint16_t i = 0; i < MAX_NUM_HALL_LANTERNS; i++ )
   {
      auwOfflineCounter[i] = HALLBOARD_INACTIVE_OFFLINE_COUNT;
   }

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   /* If lanterns are enabled */
   if(Param_ReadValue_8Bit(enPARAM8__HallLanternMask))
   {
      UpdateHallboardCommState();
   }

   CheckForNeedToResetAlarm_HallLanternMask();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
