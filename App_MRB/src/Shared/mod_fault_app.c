/******************************************************************************
 * @file     mod_fault_app.c
 * @author   Keith Soneda
 * @version  V1.00
 * @date     1, August 2016
 *
 * @note   Each fault node (TRC, MRA, MRB, CTA, CTB, COPA, COPB) will assert their own fault.
 *         Nodes will forward their local active fault to eachother.
 *         Nodes will auto-clear their active fault after 2 seconds.
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "sru.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "sys.h"
#include "position.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_module gstMod_FaultApp =
{
   .pfnInit = Init,
   .pfnRun = Run,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
static enum en_fault_nodes eFaultNodeID;

static st_fault_data astBoardActiveFaults[NUM_FAULT_NODES];
static uint16_t uwFaultLatchCountdown = FAULT_LATCH_PERIOD_1MS/MOD_RUN_PERIOD_FAULT_APP_1MS;

static const en_faultentry_oos aeFaultOOSType[NUM_FAULTS] =
{
        FAULT_TABLE(EXPAND_FAULT_TABLE_AS_OOS_ARRAY)
};
static const en_faultentry_con aeFaultConstType[NUM_FAULTS] =
{
        FAULT_TABLE(EXPAND_FAULT_TABLE_AS_CONSTRUCTION_ARRAY)
};
static const en_faultentry_cleartype aeFaultClearType[NUM_FAULTS] =
{
        FAULT_TABLE(EXPAND_FAULT_TABLE_AS_RESET_ARRAY)
};
static const en_faultentry_priority aeFaultPriority[NUM_FAULTS] =
{
        FAULT_TABLE(EXPAND_FAULT_TABLE_AS_PRIORITY_ARRAY)
};
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Returns the oos type of requested fault.
   FT_OOS_OFF
   FT_OOS__ON - If ON, the fault will contribute to the OOS limit.
 -----------------------------------------------------------------------------*/
en_faultentry_oos GetFault_OOSType( en_faults eFault )
{
   return aeFaultOOSType[eFault];
}
/*-----------------------------------------------------------------------------
   Returns the construction type of requested fault.
   FT_CON_OFF
   FT_CON__ON - If ON, the fault will be checked during construction operation.
 -----------------------------------------------------------------------------*/
en_faultentry_con GetFault_ConstructionType( en_faults eFault )
{
   return aeFaultConstType[eFault];
}
/*-----------------------------------------------------------------------------
   Returns the reset type of requested fault.
   FT_CLR_NORM - Self clears after 3 seconds,
   FT_CLR_LTCH - Latches & persists after reset,
   FT_CLR_RSET - Requires reset to clear,
   FT_CLR_DIP1 - Requires DIP A1 and reset to clear,
 -----------------------------------------------------------------------------*/
en_faultentry_cleartype GetFault_ClearType( en_faults eFault )
{
   return aeFaultClearType[eFault];
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
en_faultentry_priority GetFault_Priority( en_faults eFault )
{
   return aeFaultPriority[eFault];
}
/*-----------------------------------------------------------------------------
   Returns 1 if requested fault should override current active fault
 -----------------------------------------------------------------------------*/
static uint8_t CheckIf_OverrideCurrentFault( en_faults eCurrentFault, en_faults eReqFault )
{
   uint8_t bReturn = 0;
   /* Latching faults will override others */
   if( ( GetFault_ClearType( eCurrentFault ) != FT_CLR_LTCH )
    && ( GetFault_ClearType( eReqFault ) == FT_CLR_LTCH ) )
   {
      bReturn = 1;
   }
   else if( GetFault_Priority(eReqFault) < GetFault_Priority(eCurrentFault) )
   {
      bReturn = 1;
   }
   return bReturn;
}
/*-----------------------------------------------------------------------------
   Accepts new fault only if no fault has been latched locally for 3 seconds.
   Asserting a fault that is locally latched will reset the fault's latch count
 -----------------------------------------------------------------------------*/
void SetLocalActiveFault( st_fault_data *pstActiveFault )
{
   uint16_t ucLatchedFaultNum = astBoardActiveFaults[eFaultNodeID].eFaultNum;
   uint16_t ucReqFaultNum = pstActiveFault->eFaultNum;

   if( ucLatchedFaultNum == FLT__NONE )
   {
      //pstActiveFault->ulTimestamp = RTC_GetFullTime_Unix();
      pstActiveFault->ulTimestamp = RTC_GetLocalTime();
      pstActiveFault->ulPosition = GetPosition_PositionCount();
      pstActiveFault->wSpeed = GetPosition_Velocity();

      /* Store additional log data */
      pstActiveFault->ucCurrentFloor = GetOperation_CurrentFloor();
      pstActiveFault->ucDestinationFloor = GetOperation_DestinationFloor();
//      pstActiveFault->wEncoderSpeed = GetDriveSpeedFeedback(); // Captured on MRA
      pstActiveFault->wCommandSpeed = GetMotion_SpeedCommand();

      memcpy(&astBoardActiveFaults[eFaultNodeID],
            pstActiveFault,
            sizeof(st_fault_data));
      uwFaultLatchCountdown = FAULT_LATCH_PERIOD_1MS/MOD_RUN_PERIOD_FAULT_APP_1MS;
   }
   else if( CheckIf_OverrideCurrentFault( ucLatchedFaultNum, ucReqFaultNum ) )
   {
      //pstActiveFault->ulTimestamp = RTC_GetFullTime_Unix();
      pstActiveFault->ulTimestamp = RTC_GetLocalTime();
      pstActiveFault->ulPosition = GetPosition_PositionCount();
      pstActiveFault->wSpeed = GetPosition_Velocity();

      /* Store additional log data */
      pstActiveFault->ucCurrentFloor = GetOperation_CurrentFloor();
      pstActiveFault->ucDestinationFloor = GetOperation_DestinationFloor();
//      pstActiveFault->wEncoderSpeed = GetDriveSpeedFeedback(); // Captured on MRA
      pstActiveFault->wCommandSpeed = GetMotion_SpeedCommand();

      memcpy(&astBoardActiveFaults[eFaultNodeID],
            pstActiveFault,
            sizeof(st_fault_data));
      uwFaultLatchCountdown = FAULT_LATCH_PERIOD_1MS/MOD_RUN_PERIOD_FAULT_APP_1MS;
   }
   else if( ucLatchedFaultNum == ucReqFaultNum )
   {
      uwFaultLatchCountdown = FAULT_LATCH_PERIOD_1MS/MOD_RUN_PERIOD_FAULT_APP_1MS;
   }
}
/*-----------------------------------------------------------------------------
   Returns structure of active fault for specified fault node
 -----------------------------------------------------------------------------*/
void GetBoardActiveFault( st_fault_data *pstActiveFault, enum en_fault_nodes eFaultNode )
{
   if( eFaultNode < NUM_FAULT_NODES )
   {
      memcpy( pstActiveFault,
            &astBoardActiveFaults[eFaultNode],
            sizeof(st_fault_data) );
   }
   else
   {
      memset( pstActiveFault,
            0,
            sizeof(st_fault_data) );
   }
}

/*-----------------------------------------------------------------------------
   Returns 1 if the specified fault
   is active from any source node
 -----------------------------------------------------------------------------*/
uint8_t GetFault_ByNumber( en_faults eFaultNum )
{
   uint8_t bActive = 0;
   for(uint8_t i = 0; i < NUM_FAULT_NODES; i++)
   {
      if( astBoardActiveFaults[i].eFaultNum == eFaultNum )
      {
         bActive = 1;
         break;
      }
   }
   return bActive;
}
/*-----------------------------------------------------------------------------
   Returns 1 if any fault in the specified range (inclusive)
   is active from any source node

   eFirstFaultNum must be <= eLastFaultNum
 -----------------------------------------------------------------------------*/
uint8_t GetFault_ByRange( en_faults eFirstFaultNum, en_faults eLastFaultNum )
{
   uint8_t bActive = 0;
   if(eFirstFaultNum <= eLastFaultNum)
   {
      for(uint8_t i = 0; i < NUM_FAULT_NODES; i++)
      {
         if( ( astBoardActiveFaults[i].eFaultNum >= eFirstFaultNum )
          && ( astBoardActiveFaults[i].eFaultNum <= eLastFaultNum ) )
         {
            bActive = 1;
            break;
         }
      }
   }
   return bActive;
}
/*-----------------------------------------------------------------------------
   Returns the latched fault for the specified fault node
 -----------------------------------------------------------------------------*/
en_faults GetFault_ByNode( enum en_fault_nodes eFaultNode )
{
   if( eFaultNode < NUM_FAULT_NODES)
   {
        return astBoardActiveFaults[eFaultNode].eFaultNum;
   }
   return 0;
}
/*-----------------------------------------------------------------------------
Unload active faults from other boards
 -----------------------------------------------------------------------------*/
static void UnloadDatagram_ActiveFaults()
{
   if( eFaultNodeID == enFAULT_NODE__MRB )
   {
      UnloadDatagram_ActiveFaults_MR( &astBoardActiveFaults[0] );
   }
   else
   {
      //err
   }
}
/*-----------------------------------------------------------------------------
Load active faults from local & other boards
 -----------------------------------------------------------------------------*/
static void LoadDatagram_ActiveFaults()
{
   if( eFaultNodeID == enFAULT_NODE__MRB )
   {
      LoadDatagram_ActiveFaults_MR( &astBoardActiveFaults[0] );
   }
   else
   {
      //err
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckFor_LatchedFaultTimeout(void)
{
   en_faults eFaultNum = astBoardActiveFaults[eFaultNodeID].eFaultNum;
   if( eFaultNum != FLT__NONE )
   {
      if( ( GetFault_ClearType( eFaultNum ) == FT_CLR_NORM )
       || ( GetFault_ClearType( eFaultNum ) == FT_CLR_LTCH ) )
      {
         if( uwFaultLatchCountdown )
         {
            uwFaultLatchCountdown--;
         }
         else
         {
            memset(&astBoardActiveFaults[eFaultNodeID], 0, sizeof(st_fault_data));
         }
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

enum en_fault_nodes FaultApp_GetNode( void )
{
   return eFaultNodeID;
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 200;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_FAULT_APP_1MS;
   if( GetSRU_Deployment() == enSRU_DEPLOYMENT__MR )
   {
      eFaultNodeID = enFAULT_NODE__MRB;
   }
   else
   {
      eFaultNodeID = enFAULT_NODE__UNK;
   }

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   CheckFor_LatchedFaultTimeout();

   UnloadDatagram_ActiveFaults();
   LoadDatagram_ActiveFaults();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
