/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     26, March 2016
 *
 * @note    Loads locally created datagrams and loads into ring buffers for transmit
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sru_b.h"
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
#include "expData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_SData =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

struct st_sdata_control gstSData_LocalData;

char *pasVersion;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
// node, number of datagrams
// network id
static Sys_Nodes eSysNodeID;
static uint8_t ucNumberOfDatagrams;
static CAN_MSG_T gstCAN_MSG_Tx;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Gets the last segment of the software version string for transmit on the group net
 -----------------------------------------------------------------------------*/
static void UpdateVersionString(void)
{
   uint8_t ucSize = sizeof(SOFTWARE_RELEASE_VERSION);
   uint8_t ucFirstIndex = 0;
   for(int8_t i = ucSize-1; i >= 0; i--)
   {
      char *ps = SOFTWARE_RELEASE_VERSION+i;
      if(*ps == '.')
      {
         ucFirstIndex = i+1;
         break;
      }
   }

   pasVersion = SOFTWARE_RELEASE_VERSION+ucFirstIndex;
}

/*-----------------------------------------------------------------------------
   Creates datagrams produced by this node
 -----------------------------------------------------------------------------*/
void SharedData_Init()
{
   int iError = 1;
   eSysNodeID = GetSystemNodeID();

   switch(eSysNodeID)
   {
      case SYS_NODE__MRB:
         ucNumberOfDatagrams = NUM_MRB_DATAGRAMS;
         break;

      case SYS_NODE__CTB:
         ucNumberOfDatagrams = NUM_CTB_DATAGRAMS;
         break;

      case SYS_NODE__COPB:
         ucNumberOfDatagrams = NUM_COPB_DATAGRAMS;
         break;
      default:
         break;

   }
   if(ucNumberOfDatagrams)
   {
      iError = SDATA_CreateDatagrams( &gstSData_LocalData,
                                      ucNumberOfDatagrams
                                     );
   }


   if ( iError )
   {
      while ( 1 )
      {
         ;  // TODO: unable to allocate shared data
      }
   }
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData( void )
{
   LoadData_MR();
}

/*-----------------------------------------------------------------------------
   If destination is A processor of the board, forward it
 -----------------------------------------------------------------------------*/
uint8_t CheckIf_SendOnABNet(uint16_t uwDestinationBitmap)
{
   uint16_t uwForwardingBitmap = NODE_ALL_CORE;
   uint8_t bReturn = 0;
   if(uwForwardingBitmap & uwDestinationBitmap)
   {
      bReturn = 1;
   }
   return bReturn;
}
/*-----------------------------------------------------------------------------
 Loads to Car Net & AB Net
 -----------------------------------------------------------------------------*/
static void Transmit( void )
{
   un_sdata_datagram unDatagram;
   int iError = 0;
   uint8_t ucLocalDatagramID;
   uint8_t ucNetworkID = SDATA_NET__CAR;
   uint16_t uwDatagramToSend_Plus1;
   uwDatagramToSend_Plus1 = SDATA_GetNextDatagramToSendIndex_Plus1( &gstSData_LocalData );
   if(uwDatagramToSend_Plus1)
   {
      ucLocalDatagramID = uwDatagramToSend_Plus1 - 1;

      if ( ucLocalDatagramID < ucNumberOfDatagrams )
      {
         SDATA_ReadDatagram( &gstSData_LocalData,
                              ucLocalDatagramID,
                             &unDatagram
                           );

         Sys_Nodes eLocalNodeID = GetSystemNodeID();
         Datagram_ID eDatagramID = GetNetworkDatagramID_AnyBoard(ucLocalDatagramID);
         uint16_t uwDestinationBitmap = GetDatagramDestinationBitmap(eDatagramID);

         memcpy( gstCAN_MSG_Tx.Data, unDatagram.auc8, sizeof( unDatagram.auc8 ) );
         gstCAN_MSG_Tx.ID = 1 << 30;
         gstCAN_MSG_Tx.ID |= eDatagramID << 21;
         gstCAN_MSG_Tx.ID |= (ucNetworkID & 0x07) << 18;
         gstCAN_MSG_Tx.ID |= (eLocalNodeID & 0x0F) << 14;
         gstCAN_MSG_Tx.ID |= uwDestinationBitmap;


         if(CheckIf_SendOnABNet(uwDestinationBitmap))
         {
            iError = !UART_LoadCANMessage(&gstCAN_MSG_Tx);
         }
         else if(Param_ReadValue_1Bit(enPARAM1__DEBUG_IncreaseMRBSendRate))
         {
            iError = !UART_LoadCANMessage(&gstCAN_MSG_Tx);
         }

         if ( iError )
         {
            SDATA_DirtyBit_Set( &gstSData_LocalData, ucLocalDatagramID );

            if ( gstSData_LocalData.uwLastSentIndex )
            {
               --gstSData_LocalData.uwLastSentIndex;
            }
            else
            {
               gstSData_LocalData.uwLastSentIndex = gstSData_LocalData.uiNumDatagrams - 1;
            }
         }
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadData( void )
{
   UnloadData_MR();
}

/*-----------------------------------------------------------------------------
   Set datagram resend rates
 -----------------------------------------------------------------------------*/
static void Initialize_DatagramSendRates()
{
   uint8_t ucOfflineDelay_5ms = Param_ReadValue_8Bit(enPARAM8__OfflineCtrlTimer_5ms);
   if(ucOfflineDelay_5ms < MIN_OFFLINE_DELAY_5MS)
   {
      ucOfflineDelay_5ms = MIN_OFFLINE_DELAY_5MS;
   }

   uint16_t uwHeartbeatInterval_1ms = (ucOfflineDelay_5ms*5)/3;

   if(Param_ReadValue_1Bit(enPARAM1__DEBUG_IncreaseMRBSendRate))
   {
      gstSData_LocalData.uwHeartbeatInterval_1ms = uwHeartbeatInterval_1ms/2;
   }
   else
   {
      gstSData_LocalData.uwHeartbeatInterval_1ms = uwHeartbeatInterval_1ms;
   }

   for( uint8_t i = 0; i < ucNumberOfDatagrams; i++ )
   {
      Datagram_ID eID = GetNetworkDatagramID_AnyBoard( i );
      uint16_t uwLifetime_ms = GetDatagramLifetime_ms( eID );
      if( uwLifetime_ms != IGNORED_DATAGRAM )
      {
         gstSData_LocalData.paiResendRate_1ms[ i ] = uwLifetime_ms/2;
      }
   }
   gstSData_LocalData.paiResendRate_1ms[DG_MRB__SyncTime] = DISABLED_DATAGRAM_RESEND_RATE_MS;
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   //------------------------------------------------
   pstThisModule->uwInitialDelay_1ms = 5;
   pstThisModule->uwRunPeriod_1ms = 5;

   Initialize_DatagramSendRates();

   UpdateVersionString();

   return 0;
}

/*-----------------------------------------------------------------------------

                  MSB   x
                        x     4 bits for Network ID (global enum)
                        x
                        x
                      -----
                        x
                        x
                        x    5 bits for Source Node ID (static enum to use less bits)
  29-bit CAN ID:        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Destination Node ID (0x1F = Broadcast to all)
                        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Datagram ID (global enum)
                        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Version Number (major)
                        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Version Number (minor)
                        x
                  LSB   x

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
    LoadData();
    Transmit();

    UnloadData();

    ExpData_UpdateMasterOfflineTimers(pstThisModule->uwRunPeriod_1ms);

    return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
