/******************************************************************************
 * @file     mod_alarm_app.c
 * @author   Keith Soneda
 * @version  V1.00
 * @date     1, August 2016
 *
 * @note   Each alarm node (TRC, MRA, MRB, CTA, CTB) will assert their own alarm.
 *         Nodes will forward their local active alarm to eachother.
 *         Nodes will auto-clear their active alarm after 3 seconds.
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "sru.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "sys.h"
#include "position.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_module gstMod_AlarmApp =
{
   .pfnInit = Init,
   .pfnRun = Run,
};
st_alarm gstAlarm;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
static en_alarm_nodes eAlarmNodeID;

static st_alarm_data astActiveAlarms[NUM_ALARM_NODES];
static uint16_t uwAlarmLatchCountdown = ALARM_LATCH_PERIOD_1MS/MOD_RUN_PERIOD_ALARM_1MS;

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
   Set all subalarms with SetAlarm(eAlarmNum, 0xFF)

   Use to set a single subalarm bit for a alarm.
   Otherwise, Use SetAlarm_ByMask if multiple bits should be cleared.

   Will only send a SetAlarm request if the alarm is not already set or requested
 -----------------------------------------------------------------------------*/
void SetAlarm( en_alarms eAlarmNum )
{
   if( eAlarmNum < NUM_ALARMS )
   {
      st_alarm_data stAlarmData;
      stAlarmData.eAlarmNum = eAlarmNum;
      SetLocalActiveAlarm(&stAlarmData);
   }
}

/*-----------------------------------------------------------------------------
   Update active alarm structure
 -----------------------------------------------------------------------------*/
static void UpdateActiveAlarms( void )
{
   gstAlarm.bActiveAlarm = 0;
   gstAlarm.eAlarmNumber = ALM__NONE;
   gstAlarm.eActiveNode_Plus1 = 0;
   for( uint8_t i = 0; i < NUM_ALARM_NODES; i++ )
   {
      st_alarm_data stAlarmData;
      GetActiveAlarmData( &stAlarmData, i );
      en_alarms eAlarmNum = stAlarmData.eAlarmNum;

      gstAlarm.aeActiveAlarmNum[ i ] = eAlarmNum;

      if( !gstAlarm.bActiveAlarm
       && ( eAlarmNum != ALM__NONE ) )
      {
            gstAlarm.bActiveAlarm = 1;
            gstAlarm.eAlarmNumber = eAlarmNum;
            gstAlarm.eActiveNode_Plus1 = i + 1;
      }
   }
}
/*-----------------------------------------------------------------------------
   Accepts new alarm only if no alarm has been latched locally for 3 seconds.
   Asserting a alarm that is locally latched will reset the alarm's latch count
 -----------------------------------------------------------------------------*/
void SetLocalActiveAlarm( st_alarm_data *pstAlarmData )
{
   en_alarms eActiveAlarmNum = astActiveAlarms[eAlarmNodeID].eAlarmNum;
   en_alarms eReqAlarmNum = pstAlarmData->eAlarmNum;
   if( eActiveAlarmNum == ALM__NONE )
   {
      pstAlarmData->ulTimestamp = RTC_GetLocalTime();
      pstAlarmData->ulPosition = GetPosition_PositionCount();
      pstAlarmData->wSpeed = GetPosition_Velocity();
      memcpy(&astActiveAlarms[eAlarmNodeID],
            pstAlarmData,
            sizeof(st_alarm_data));
      uwAlarmLatchCountdown = ALARM_LATCH_PERIOD_1MS/MOD_RUN_PERIOD_ALARM_1MS;
   }
   else if( eActiveAlarmNum == eReqAlarmNum )
   {
      uwAlarmLatchCountdown = ALARM_LATCH_PERIOD_1MS/MOD_RUN_PERIOD_ALARM_1MS;
   }
}
/*-----------------------------------------------------------------------------
   Returns structure with data for active alarm for specified node
 -----------------------------------------------------------------------------*/
void GetActiveAlarmData( st_alarm_data *pstAlarmData, en_alarm_nodes eAlarmNode )
{
   if( eAlarmNode < NUM_ALARM_NODES )
   {
      memcpy( pstAlarmData,
            &astActiveAlarms[eAlarmNode],
            sizeof(st_alarm_data) );
   }
   else
   {
      memset( pstAlarmData,
            0,
            sizeof(st_alarm_data) );
   }
}
/*-----------------------------------------------------------------------------
   Returns 1 if any alarm in a range is active. Inclusive.
   Must be eFirstAlarmNum < eLastAlarmNum
 -----------------------------------------------------------------------------*/
uint8_t GetAlarm_ByRange( en_alarms eFirstAlarmNum,  en_alarms eLastAlarmNum )
{
   uint8_t bActive = 0;
   if(eFirstAlarmNum <= eLastAlarmNum)
   {
      for(uint8_t i = 0; i < NUM_ALARM_NODES; i++)
      {
         if( ( astActiveAlarms[i].eAlarmNum >= eFirstAlarmNum )
          && ( astActiveAlarms[i].eAlarmNum <= eLastAlarmNum ) )
         {
            bActive = 1;
            break;
         }
      }
   }
   return bActive;
}
/*-----------------------------------------------------------------------------
   Returns 1 if a specific fault is active
 -----------------------------------------------------------------------------*/
uint8_t GetAlarm_ByNumber( en_alarms eAlarmNum )
{
   uint8_t bActive = 0;
   for(uint8_t i = 0; i < NUM_ALARM_NODES; i++)
   {
      if( astActiveAlarms[i].eAlarmNum == eAlarmNum)
      {
         bActive = 1;
         break;
      }
   }
   return bActive;
}

/*-----------------------------------------------------------------------------
   Returns the active alarm for the specified alarm node
 -----------------------------------------------------------------------------*/
en_alarms GetAlarm_ByNode( en_alarm_nodes eAlarmNode )
{
   if( eAlarmNode < NUM_ALARM_NODES)
   {
      return astActiveAlarms[eAlarmNode].eAlarmNum;
   }
   return 0;
}
/*-----------------------------------------------------------------------------
Unload active alarms from other boards
 -----------------------------------------------------------------------------*/
static void UnloadDatagram_ActiveAlarms()
{
   if( eAlarmNodeID == enALARM_NODE__MRB )
   {
      UnloadDatagram_ActiveAlarms_MR( &astActiveAlarms[0] );
   }
   else
   {
      //err
   }
}
/*-----------------------------------------------------------------------------
Load active alarms from local & other boards
 -----------------------------------------------------------------------------*/
static void LoadDatagram_ActiveAlarms()
{
   if( eAlarmNodeID == enALARM_NODE__MRB )
   {
      LoadDatagram_ActiveAlarms_MR( &astActiveAlarms[0] );
   }
   else
   {
      //err
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckFor_LatchedAlarmTimeout(void)
{
   en_alarms eAlarmNum = astActiveAlarms[eAlarmNodeID].eAlarmNum;
   if( eAlarmNum != ALM__NONE )
   {
      if( uwAlarmLatchCountdown )
      {
         uwAlarmLatchCountdown--;
      }
      else
      {
         memset(&astActiveAlarms[eAlarmNodeID], 0, sizeof(st_alarm_data));
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_AlarmLED()
{
   static uint8_t ucTimer_1ms;
   static uint8_t bLastLED;

   if ( gstAlarm.bActiveAlarm )
   {
      if ( ucTimer_1ms > 100/MOD_RUN_PERIOD_ALARM_1MS )
      {
         SRU_Write_LED( enSRU_LED_Alarm, bLastLED );

         ucTimer_1ms = 0;

         bLastLED = ( bLastLED )? 0 : 1;
      }
      else
      {
         ucTimer_1ms++;
      }
   }
   else
   {
      SRU_Write_LED( enSRU_LED_Alarm, 0 );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

en_alarm_nodes AlarmApp_GetNode( void )
{
   return eAlarmNodeID;
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 200;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_ALARM_1MS;

   if( GetSRU_Deployment() == enSRU_DEPLOYMENT__MR )
   {
      eAlarmNodeID = enALARM_NODE__MRB;
   }
   else if( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT )
   {
      eAlarmNodeID = enALARM_NODE__CTB;
   }
   else if( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP )
   {
      eAlarmNodeID = enALARM_NODE__COPB;
   }
   else
   {
      eAlarmNodeID = enALARM_NODE__UNK;
   }

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{

    CheckFor_LatchedAlarmTimeout();

    UnloadDatagram_ActiveAlarms();
    LoadDatagram_ActiveAlarms();

    UpdateActiveAlarms();

    Update_AlarmLED();

    return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
