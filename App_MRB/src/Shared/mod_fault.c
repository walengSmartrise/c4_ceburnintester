
/******************************************************************************
 * @author   Keith Soneda
 * @version  V1.00
 * @date     1, August 2016
 *
 * @note   This file works with mod_fault_app.c for fault management between boards in enum en_fault_nodes
 *          ClrFault/SetFault(ucFaultNum)
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "sru_b.h"
#include "sru.h"
#include "sys.h"

#include <stdint.h>
#include <string.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Fault =
{
   .pfnInit = Init,
   .pfnRun = Run,
};
st_fault gstFault;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define LIMIT_CONSECUTIVE_FAULTS    (3)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Checks reset source and asserts appropriate reset fault
 -----------------------------------------------------------------------------*/
static void UpdateResetFault()
{
   static uint32_t uiResetSrc;
   if(!uiResetSrc)
   {
      uiResetSrc = Chip_SYSCTL_GetSystemRSTStatus();
      Chip_SYSCTL_ClearSystemRSTStatus(uiResetSrc);
   }

   if( uiResetSrc & SYSCTL_RST_BOD )
   {
      en_faults eFaultNum = FLT__BOD_RESET_MRA + FaultApp_GetNode()-1;
      if(eFaultNum > FLT__BOD_RESET_COPB)
      {
         eFaultNum = FLT__BOD_RESET_COPB;
      }
      SetFault( eFaultNum );
      gstFault.bActiveFault = 1;
      gstFault.eFaultNumber = eFaultNum;
      gstFault.eActiveNode_Plus1 = FaultApp_GetNode() + 1;
      gstFault.aeActiveFaultNum[FaultApp_GetNode()] = eFaultNum;
   }
   else if( uiResetSrc & SYSCTL_RST_WDT )
   {
      en_faults eFaultNum = FLT__WDT_RESET_MRA + FaultApp_GetNode()-1;
      if(eFaultNum > FLT__WDT_RESET_COPB)
      {
         eFaultNum = FLT__WDT_RESET_COPB;
      }
      SetFault( eFaultNum );
      gstFault.bActiveFault = 1;
      gstFault.eFaultNumber = eFaultNum;
      gstFault.eActiveNode_Plus1 = FaultApp_GetNode() + 1;
      gstFault.aeActiveFaultNum[FaultApp_GetNode()] = eFaultNum;
   }
   else//assume SYSCTL_RST_POR
   {
      en_faults eFaultNum = FLT__BOARD_RESET_MRA + FaultApp_GetNode()-1;
      if(eFaultNum > FLT__BOARD_RESET_COPB)
      {
         eFaultNum = FLT__BOARD_RESET_COPB;
      }
      SetFault( eFaultNum );
      gstFault.bActiveFault = 1;
      gstFault.eFaultNumber = eFaultNum;
      gstFault.eActiveNode_Plus1 = FaultApp_GetNode() + 1;
      gstFault.aeActiveFaultNum[FaultApp_GetNode()] = eFaultNum;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetFault( en_faults eFaultNum )
{
   if( ( GetOperation_ManualMode() != MODE_M__CONSTRUCTION )
    || ( GetFault_ConstructionType(eFaultNum) == FT_CON__ON ) )
   {
      if( eFaultNum < NUM_FAULTS )
      {
         st_fault_data stFaultData;
         stFaultData.eFaultNum = eFaultNum;
         SetLocalActiveFault(&stFaultData);
      }
   }
}

/*-----------------------------------------------------------------------------
   Update active fault and per node fault arrays
 -----------------------------------------------------------------------------*/
void UpdateActiveFaults( void )
{
    gstFault.bActiveFault = 0;
    gstFault.eFaultNumber = FLT__NONE;
    gstFault.eActiveNode_Plus1 = 0;
    for( uint8_t i = 0; i < NUM_FAULT_NODES; i++ )
    {
        st_fault_data stActiveFault;
        GetBoardActiveFault( &stActiveFault, i );
        en_faults eFaultNum = stActiveFault.eFaultNum;

        gstFault.aeActiveFaultNum[ i ] = eFaultNum;

        if( !gstFault.bActiveFault
       && ( eFaultNum != FLT__NONE ) )
        {
            gstFault.bActiveFault = 1;
            gstFault.eFaultNumber = eFaultNum;
            gstFault.eActiveNode_Plus1 = i + 1;
        }
    }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_FaultLED()
{
   static uint8_t ucTimer_1ms;
   static uint8_t bLastLED;
   if ( gstFault.eFaultNumber )
   {
      if ( ucTimer_1ms > 100/MOD_RUN_PERIOD_FAULT_1MS )
      {
         SRU_Write_LED( enSRU_LED_Fault, bLastLED );

         ucTimer_1ms = 0;

         bLastLED = ( bLastLED )? 0 : 1;
      }
      else
      {
         ucTimer_1ms++;
      }
   }
   else
   {
      SRU_Write_LED( enSRU_LED_Fault, 0 );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckFor_InvalidHallMaskFault()
{
   uint32_t uiHallCallMask = Param_ReadValue_8Bit(enPARAM8__HallCallMask);
   uint32_t uiHallMedicalMask = Param_ReadValue_8Bit(enPARAM8__HallMedicalMask);
   uint32_t uiHallSwingMask = Param_ReadValue_8Bit(enPARAM8__SwingCallMask);
   uint32_t uiInvalidMask = (uiHallCallMask&uiHallMedicalMask)
                          | (uiHallCallMask&uiHallSwingMask)
                          | (uiHallMedicalMask&uiHallSwingMask);
   if( !GetMotion_RunFlag() && uiInvalidMask )
   {
      SetFault( FLT__INVALID_HALLMASK );
   }
}

/*-----------------------------------------------------------------------------
   Check for payment passcode fault
 -----------------------------------------------------------------------------*/
// Should be in the default all header file.
#define NUM_HEADER_BYTES   ((uint32_t)6)
#define FLASH_BASE_ADDRESS ( ((uint32_t)0x9000) + NUM_HEADER_BYTES )
static void CheckFor_PaymentPasscodeFault()
{
   if( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
    && ( GetOperation_AutoMode() == MODE_A__NORMAL )
    && ( GetOperation3_CaptureMode_MR() != CAPTURE_IDLE ) )
   {
      uint8_t bInvalidPasscode = 1;
      uint32_t uiEnteredPasscode = Param_ReadValue_24Bit(enPARAM24__PaymentPasscode);
      uint32_t uiAddressOffset = ( (NUM_32BIT_PARAMS*4) + (NUM_24BIT_PARAMS*3) + (NUM_16BIT_PARAMS*2) + (NUM_8BIT_PARAMS*1) + (NUM_1BIT_PARAMS*1));
      uiAddressOffset += FLASH_BASE_ADDRESS;
      const char *psJobName = ((char *)(uiAddressOffset));
      uint32_t uiJobID = Param_ReadValue_24Bit(enPARAM24__JobID);
      uint8_t ucCarID = Param_ReadValue_8Bit(enPARAM8__GroupCarIndex);
      if( ucCarID >= MAX_GROUP_CARS )
      {
         ucCarID = MAX_GROUP_CARS-1;
      }

      uint32_t uiPasscode = psJobName[0] << 8; // Shift in the first character of the job name
      uiPasscode |= 0x2000;  // Convert any upper case chars to lower case
      uiPasscode += uiJobID; // Add in the job ID
      uiPasscode = ( uiPasscode << ucCarID )
                 | ( uiPasscode >> ( 16 - ucCarID ) );

      if( ( uiEnteredPasscode == uiPasscode )
       || ( uiEnteredPasscode == MASTER_PAYMENT_PASSCODE ) )
      {
         bInvalidPasscode = 0;
      }

      if( bInvalidPasscode )
      {
         SetFault( FLT__PAYMENT_PASSCODE );
      }
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 50;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_FAULT_1MS;

   return 0;
}

/*-----------------------------------------------------------------------------


 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static uint32_t uiStartupCounter_1ms = BOARD_RESET_FAULT_TIMER_MS/MOD_RUN_PERIOD_FAULT_1MS;

   if( uiStartupCounter_1ms )
   {
      UpdateResetFault();
      uiStartupCounter_1ms--;
   }
   else if( SRU_Read_DIP_Switch( enSRU_DIP_A1 ) )
   {
      en_faults eFaultNum = FLT__CPU_STOP_SW_MRA + FaultApp_GetNode()-1;
      if(eFaultNum > FLT__CPU_STOP_SW_COPB)
      {
         eFaultNum = FLT__CPU_STOP_SW_COPB;
      }
      SetFault( eFaultNum );
      gstFault.bActiveFault = 1;
      gstFault.eFaultNumber = eFaultNum;
      gstFault.eActiveNode_Plus1 = FaultApp_GetNode() + 1;
      gstFault.aeActiveFaultNum[FaultApp_GetNode()] = eFaultNum;
   }
   else
   {
      UpdateActiveFaults();
      CheckFor_InvalidHallMaskFault();
      CheckFor_PaymentPasscodeFault();
   }
   Update_FaultLED();
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
