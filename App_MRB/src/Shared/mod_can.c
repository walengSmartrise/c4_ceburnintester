/******************************************************************************
 *
 * @file
 * @brief
 * @version  V1.00
 * @date
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "sru_b.h"
#include "sys.h"

#include <string.h>
#include "GlobalData.h"
#include "ring_buffer.h"
#include "groupnet.h"
#include "carData.h"
#include "carDestination.h"
#include "shieldData.h"
#include "masterCommand.h"
#include "emergency_power.h"
#include "param_edit_protocol.h"
#include "XRegData.h"
#include "ddCommand.h"
#include "dynamic_parking.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_CAN =
{
        .pfnInit = Init,
        .pfnRun = Run,
};

static uint16_t uwBusErrorCounter_CAN1;
static uint16_t uwBusErrorCounter_CAN2;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define LIMIT_UNLOAD_CAN_CYCLES       (16)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *-----------------------------------------------------------------------------*/

st_position_frame channel1_0x11;
st_position_frame channel1_0x12;

#define DESTINATION_ID_ALL     (0x1f)
#define EXTRACT_NET_ID(x)      (( x >> 25 ) & 0x0F )
#define EXTRACT_SRC_ID(x)      (( x >> 20 ) & 0x1F )
#define EXTRACT_DEST_ID(x)     (( x >> 15 ) & 0x1F )
#define EXTRACT_DATAGRAM_ID(x) (( x >> 10 ) & 0x1F )
#define SECONDS (200)

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Returns bus error counter
 -----------------------------------------------------------------------------*/
uint16_t GetDebugBusOfflineCounter_CAN1()
{
    return uwBusErrorCounter_CAN1;
}
uint16_t GetDebugBusOfflineCounter_CAN2()
{
    return uwBusErrorCounter_CAN2;
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
    pstThisModule->uwInitialDelay_1ms = 10;
    pstThisModule->uwRunPeriod_1ms    = 5; // Should be as fast as the fastest producer.

    // A check to make sure that datagrams that are added are the maximum supported (ie 255). If there are more
    // than this number of datagrams specified the below static assert will trigger which results in the build
    // failing and issuing an error.
    _Static_assert( GROUP_NET_NUM_DATAGRAMS <= MAX_GROUP_NET_DATAGRAMS, "Unsupported number of group net datagrams" );




    return 0;
}
/*-----------------------------------------------------------------------------
   Unload set RTC time request sent from another car
 -----------------------------------------------------------------------------*/
static void UnloadDatagram_SyncTime_MRB(enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram)
{
   if( CarData_GetMasterDispatcherFlag(eCarID) && !GetMasterDispatcherFlag() )
   {
      if(Param_ReadValue_1Bit(enPARAM1__EnableBoardRTC))
      {
         RTC_WritePeripheralTime_Unix(punDatagram->aui32[0]);
      }
      RTC_SetSyncTime(punDatagram->aui32[0]);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ReceiveGroupCarDatagram(enum en_group_net_nodes eCarID,
                                    en_group_net_mrb_datagrams eLocalDatagramID,
                                    un_sdata_datagram *punDatagram)
{
   if( !GetMotion_RunFlag() && ( eCarID == GetFP_GroupCarIndex() ) )
   {
      SetFault(FLT__DUPLICATE_GROUP_ID);
   }
   else
   {
      ClrCarOfflineTimer_ByCar(eCarID);
      switch(eLocalDatagramID)
      {
         case DG_GroupNet_MRB__MappedInputs1:
            UnloadDatagram_InputMap1(eCarID, punDatagram);
            break;
         case DG_GroupNet_MRB__MappedInputs2:
            UnloadDatagram_InputMap2(eCarID, punDatagram);
            break;
         case DG_GroupNet_MRB__MappedInputs3:
            UnloadDatagram_InputMap3(eCarID, punDatagram);
            break;
         case DG_GroupNet_MRB__MappedOutputs1:
            UnloadDatagram_OutputMap1(eCarID, punDatagram);
            break;
         case DG_GroupNet_MRB__MappedOutputs2:
            UnloadDatagram_OutputMap2(eCarID, punDatagram);
            break;
         case DG_GroupNet_MRB__CAR_STATUS:
            UnloadDatagram_CarStatus(eCarID, punDatagram);
            break;
         case DG_GroupNet_MRB__CarData:
            UnloadDatagram_CarData(eCarID, punDatagram);
            break;
         case DG_GroupNet_MRB__VIPMasterDispatcher:
            VIP_UnloadDatagram_MasterDispatcher(eCarID, punDatagram);
            break;
         case DG_GroupNet_MRB__CarOperation:
            UnloadDatagram_CarOperation(eCarID, punDatagram);
            break;
         case DG_GroupNet_MRB__MasterDispatcher:
            UnloadDatagram_MasterDispatcher(eCarID, punDatagram);
            break;
         case DG_GroupNet_MRB__MasterCommand:
            UnloadDatagram_MasterCommand(eCarID, punDatagram);
            break;
         case DG_GroupNet_MRB__EmergencyPower:
            UnloadDatagram_EmergencyPower(eCarID, punDatagram);
            break;
         case DG_GroupNet_MRB__DynamicParking:
            UnloadDatagram_DynamicParking(eCarID, punDatagram);
            break;
         case DG_GroupNet_MRB__SyncClock:
            UnloadDatagram_SyncTime_MRB(eCarID, punDatagram);
            break;
         default:
            break;
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ReceiveShieldDatagram( en_group_net_shield_datagrams eLocalDatagramID,
      un_sdata_datagram *punDatagram)
{
   uint8_t bSuccess = 1;
   switch(eLocalDatagramID)
   {
      case DG_GroupNet_SHIELD__SyncTime:
         UnloadDatagram_SyncTime(punDatagram);
         ShieldData_ClrTimeoutCounter();
         break;

      case DG_GroupNet_SHIELD__RemoteCommand:
         UnloadDatagram_RemoteCommand(punDatagram);
         break;

      case DG_GroupNet_SHIELD__LatchCarCall:
         UnloadDatagram_LatchCarCall(punDatagram);
         break;

      case DG_GroupNet_SHIELD__LatchHallCall:
         UnloadDatagram_LatchHallCall(punDatagram);
         break;

      case DG_GroupNet_SHIELD__ClearHallCall:
         UnloadDatagram_ClearHallCall(punDatagram);
         break;

      case DG_GroupNet_SHIELD__ParamEditRequest:
      {
         uint8_t ucProtocol_Plus1 = UnloadDatagram_ParamEditRequest(punDatagram, SRU_Read_DIP_Switch(enSRU_DIP_A4));
         if( ucProtocol_Plus1 )
         {
            en_alarms eAlarm = ALM__REMOTE_PU_1BIT + ucProtocol_Plus1 - 1;
            if(eAlarm > ALM__REMOTE_PU_KEB)
            {
               eAlarm = ALM__REMOTE_PU_KEB;
            }
            SetAlarm(eAlarm);
         }
      }
      break;

      case DG_GroupNet_SHIELD__ParkingFloors:
         DynamicParking_UnloadPredictiveParkingDatagram(punDatagram);
         break;
      case DG_GroupNet_SHIELD__Virtual_CCEnables:
         UnloadDatagram_VirtualCCEnables(punDatagram);
         break;
      case DG_GroupNet_SHIELD__DynamicParking:
         DynamicParking_UnloadDynamicParkingDatagram(punDatagram);
         break;
         
      default:
         bSuccess = 0;
         break;
   }
   if(bSuccess)
   {
      ShieldData_IncMessageCounter();
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ReceiveDDMDatagram( en_group_net_ddm_datagrams eLocalDatagramID,
                                un_sdata_datagram *punDatagram)
{
   uint8_t bSuccess = 1;
   switch(eLocalDatagramID)
   {
      case DG_GroupNet_DDM__LATCH_HC:
         DDC_UnloadDatagram_LatchHC(punDatagram);
         break;
      case DG_GroupNet_DDM__LATCH_CC:
         DDC_UnloadDatagram_LatchCC(punDatagram);
         break;
      case DG_GroupNet_DDM__HEARTBEAT:
         break;
      default:
         bSuccess = 0;
         break;
   }
   if(bSuccess)
   {
      DDC_ClrOfflineTimer();
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ProcessAuxNetMessages( void )
{
    CAN_MSG_T tmp;
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
        // Read a CAN message from the buffer
        if(!CAN1_UnloadFromRB(&tmp)) //if empty
        {
            i = LIMIT_UNLOAD_CAN_CYCLES;
            break;
        }
        else
        {
            un_sdata_datagram unDatagram;
            memcpy( unDatagram.auc8, tmp.Data, sizeof( tmp.Data ) );

            enum en_sdata_networks eNet2 = EXTRACT_NET_ID(tmp.ID);
            uint8_t ucSource2 = EXTRACT_SRC_ID(tmp.ID);
            uint8_t ucDest2 = EXTRACT_DEST_ID(tmp.ID);
            uint8_t ucDatagramID2 = EXTRACT_DATAGRAM_ID(tmp.ID);
            if( ( eNet2 == SDATA_NET__AUX )
             && ( ucDest2 == 0x1F ) // All nodes
             && ( ucSource2 == MR_AUX_NET__LW )
             && ( ucDatagramID2 == DG_AuxNet_LW__Response ) )
            {
               LoadWeigher_UnloadDatagram(&unDatagram);
            }

            /* Unloading status updates from hall lantern boards. */
            uint8_t ucNetworkID = EXTRACT_HALLNET_NET_ID( tmp.ID );
            uint8_t ucDatagramID = EXTRACT_HALLNET_DATAGRAM_ID( tmp.ID );
            uint8_t ucSourceID = EXTRACT_HALLNET_SRC_ID( tmp.ID );
            uint16_t uwDIPs = EXTRACT_HALLNET_DIP_ID( tmp.ID );
            uint8_t bExtendedHallBoardType = (tmp.Data[0] >> 7) & 1;
            if( bExtendedHallBoardType )
            {
               uint8_t ucExtraDIPs = EXTRACT_HALLNET_DEST_ID(tmp.ID);
               uwDIPs |= ( ucExtraDIPs & 1 ) << 10;
            }
            if ( ( ucNetworkID == SDATA_NET__HALL )
              && ( ucSourceID == HALL_NET__HB )
              && ( ucDatagramID == DG_HallNet_HB__Status )
              && ( uwDIPs < GetHallBoard_MaxNumberOfBoards() ) )
            {
               UnloadDatagram_HallLanternStatus( uwDIPs, &tmp.Data[0] );
            }
        }
    }
}


static void ProcessGroupNetMessages( void )
{
    CAN_MSG_T tmp;
    // Read data from ring buffer till it is empty
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
        // Read a CAN message from the buffer
        if(!CAN2_UnloadFromRB(&tmp)) //if empty
        {
            i = LIMIT_UNLOAD_CAN_CYCLES;
            break;
        }
        else
        {
            un_sdata_datagram unDatagram;
            uint8_t ucNetID = ExtractFromBusID_NetworkID(tmp.ID);
            enum en_group_net_nodes ucSourceID      = ExtractFromBusID_SourceID(tmp.ID);
            GroupNet_DatagramID ucDatagramID    = ExtractFromBusID_DatagramID(tmp.ID);
            uint16_t uwDestinationBitmap = ExtractFromBusID_DestinationBitmap(tmp.ID);
//            uint32_t uiNewID = GroupNet_EncodeCANMessageID(ucDatagramID, ucNetID, ucSourceID, uwDestinationBitmap);//TODO rm test
            // Map the old system datagram ID to the new system datagram ID and insert into the data table
            // TODO: The below can be removed when all other baords on the groupnet are set up send and
            // receive using the same standard. But right now the old system and the new system are not
            // directly compatible.
            if( ucNetID != SDATA_NET__GROUP )
            {
               //todo set alarm
            }
            // Check for valid destination
            else if( uwDestinationBitmap & GroupNet_GetDestinationBitmap(GetFP_GroupCarIndex()) )
            {
               if( ucSourceID <= GROUP_NET__CAR_8 )
               {
                   // Mapping a car data gram to the new system. The new system ID is calculated as follows.
                   // First extract the CAR ID and multiply by the number of datagrams a car can produce,
                   // hard coded to 25. Then add the old system datagram ID to the above. This results in a
                   // unique number for converting from the old system to the new system.
                   memcpy( unDatagram.auc8, tmp.Data, sizeof( tmp.Data ) );
                   InsertGroupNetDatagram_ByDatagramID(&unDatagram ,ucDatagramID);

                   // Unload some DG on receive to prevent missing multiplexed data
                   en_group_net_mrb_datagrams eLocalDatagramID = GroupNet_GlobalIDToLocalNodeID(ucDatagramID);
                   ReceiveGroupCarDatagram( ucSourceID, eLocalDatagramID, &unDatagram );
               }
               else if( ucSourceID == GROUP_NET__SHIELD )
               {
                   // For converting these datagrams ID to fit into the table, simply add the
                   // value for the first shield datagram to the datagram value extracted from the
                   // CAN ID.
                   memcpy( unDatagram.auc8, tmp.Data, sizeof( tmp.Data ) );
                   InsertGroupNetDatagram_ByDatagramID(&unDatagram ,ucDatagramID);
                   en_group_net_shield_datagrams eLocalDatagramID = GroupNet_GlobalIDToLocalNodeID(ucDatagramID);
                   if (eLocalDatagramID < NUM_GroupNet_SHIELD_DATAGRAMS)
                   {
                      ReceiveShieldDatagram( eLocalDatagramID, &unDatagram );
                   }
               }
               else if( (ucSourceID >= GROUP_NET__RIS_1)
                     && (ucSourceID <= GROUP_NET__RIS_4) )
               {
                   // The riser address is calculated as follows.
                   // Add the offset from 0 to the first riser board datagram, GROUP_NET_DATAGRAM_RISER0_DATA0.
                   // Then calculate the riser board offset which is calculated by subtracting the riser board value from the
                   // extracted source id. Now add the datagram offset which we gather from the extracted can id. This now gives
                   // you the mapping into the new system.
                   memcpy( unDatagram.auc8, tmp.Data, sizeof( tmp.Data ) );
                   InsertGroupNetDatagram_ByDatagramID(&unDatagram ,ucDatagramID);
                   en_group_net_ris_datagrams eLocalDatagramID = GroupNet_GlobalIDToLocalNodeID(ucDatagramID);
                   if (eLocalDatagramID < NUM_GroupNet_RIS_DATAGRAMS)
                   {
                       uint8_t ucRiserIndex = ucSourceID - GROUP_NET__RIS_1;
                       RecieveRiserCommand( ucRiserIndex, eLocalDatagramID, GetRiserDatagram(ucDatagramID));
                   }
               }
               else if( ucSourceID == GROUP_NET__XREG )
               {
                  memcpy( unDatagram.auc8, tmp.Data, sizeof( tmp.Data ) );
                  InsertGroupNetDatagram_ByDatagramID(&unDatagram ,ucDatagramID);

                  /* Currently only one datagram ID expected from the XREG */
                  UnloadDatagram_XRegStatus(&unDatagram);
               }
               else if( ucSourceID == GROUP_NET__DDM )
               {
                  memcpy( unDatagram.auc8, tmp.Data, sizeof( tmp.Data ) );
                  InsertGroupNetDatagram_ByDatagramID(&unDatagram ,ucDatagramID);
                  en_group_net_ddm_datagrams eLocalDatagramID = GroupNet_GlobalIDToLocalNodeID(ucDatagramID);
                  if (eLocalDatagramID < NUM_GroupNet_DDM_DATAGRAMS)
                  {
                     ReceiveDDMDatagram( eLocalDatagramID, &unDatagram );
                  }
               }

               else
               {
                  static uint8_t ucTest = 0;
                  ucTest++;
                   //SEGGER_RTT_printf(0, "Error");
                  //TODO add fault
               }
            }
            else
            {

            }
        }
    }
}

static void UnloadCAN_MRB( void )
{
    // CAN1
    ProcessAuxNetMessages();

    // CAN2
    ProcessGroupNetMessages();

}

/*-----------------------------------------------------------------------------
   Check for CAN Bus offline and reset if offline
 -----------------------------------------------------------------------------*/
static void CheckFor_CANBusOffline()
{
    if( Chip_CAN_GetGlobalStatus( LPC_CAN1 ) & CAN_GSR_BS ) // Bus offline if 1
    {
         Chip_CAN_SetMode( LPC_CAN1, CAN_RESET_MODE, ENABLE );
         __NOP();
         __NOP();
         __NOP();
         Chip_CAN_SetMode( LPC_CAN1, CAN_RESET_MODE, DISABLE );
         SetAlarm(ALM__MR_CAN_RESET_3);
    }

    if( Chip_CAN_GetGlobalStatus( LPC_CAN2 ) & CAN_GSR_BS ) // Bus offline if 1
    {
         Chip_CAN_SetMode( LPC_CAN2, CAN_RESET_MODE, ENABLE );
         __NOP();
         __NOP();
         __NOP();
         Chip_CAN_SetMode( LPC_CAN2, CAN_RESET_MODE, DISABLE );
         SetAlarm(ALM__MR_CAN_RESET_4);
    }
}
/*-----------------------------------------------------------------------------
   Updates the CAN bus error counters viewed via View Debug Data menus
 -----------------------------------------------------------------------------*/
static void UpdateDebugBusErrorCounter()
{
   static uint8_t ucLastCountRx_CAN1;
   static uint8_t ucLastCountTx_CAN1;
   static uint8_t ucLastCountRx_CAN2;
   static uint8_t ucLastCountTx_CAN2;
   uint32_t uiStatus = Chip_CAN_GetGlobalStatus( LPC_CAN1 );
   uint8_t ucCountRx = (uiStatus >> 16) & 0xFF;
   uint8_t ucCountTx = (uiStatus >> 24) & 0xFF;
   if( ( ucCountRx > ucLastCountRx_CAN1 )
    || ( ucCountTx > ucLastCountTx_CAN1 ))
   {
       uwBusErrorCounter_CAN1++;
   }
   ucLastCountRx_CAN1 = ucCountRx;
   ucLastCountTx_CAN1 = ucCountTx;

   uiStatus = Chip_CAN_GetGlobalStatus( LPC_CAN2 );
   ucCountRx = (uiStatus >> 16) & 0xFF;
   ucCountTx = (uiStatus >> 24) & 0xFF;
   if( ( ucCountRx > ucLastCountRx_CAN2 )
    || ( ucCountTx > ucLastCountTx_CAN2 ))
   {
       uwBusErrorCounter_CAN2++;
   }
   ucLastCountRx_CAN2 = ucCountRx;
   ucLastCountTx_CAN2 = ucCountTx;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   UnloadCAN_MRB();
   CheckFor_RiserBoardFault(pstThisModule);

   UpdateDebugBusErrorCounter();
   CheckFor_CANBusOffline();
   DDC_UpdateOfflineTimer(pstThisModule);

   return 0;
}

