
/******************************************************************************
 *
 * @file     ui_menu_io.c
 * @brief    I/O Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "GlobalData.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_EBrakeStatus( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
//---------------------------------------------------------------

static struct st_ui_screen__freeform gstFFS_EBrakeStatus =
{
   .pfnDraw = UI_FFS_EBrakeStatus,
};


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_EBrakeStatus =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_EBrakeStatus,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

enum en_brake_status_data
{
   BRAKE_DATA__STATE,
   BRAKE_DATA__ERROR,
   BRAKE_DATA__VOLT_FB,
   BRAKE_DATA__BPS,

   BRAKE_DATA__BRAKE_VAC,
   NUM_BRAKE_DATA
};
static char const * const pasDataStrings[NUM_BRAKE_DATA] =
{
      "State: ",
      "Error: ",
      "Volt FB: ",
      "BPS: ",
      "VAC: ",
};

static char const * const pasVACStrings[2] =
{
      "120 VAC",
      "240 VAC",
};
static char const * const pasState[NUM_BRAKE_STATES] =
{
      "Unknown",
      "Idle",
      "Generate",
      "RampToPick",
      "Pick",
      "RampToHold",
      "Hold",
      "RampToStop",
      "Error",
};
static char const * const pasError[NUM_BRAKE_ERRORS] =
{
"Unknown",//BRAKE_ERROR__UNK,
"None",//BRAKE_ERROR__NONE,
"POR Reset",//BRAKE_ERROR__RESET_POR,
"WDT Reset",//BRAKE_ERROR__RESET_WDT,
"Comm.",//BRAKE_ERROR__COM,   // com loss with C4 system
"Gate Flt",//BRAKE_ERROR__GATE_FAULT,
"MOS Failure",//BRAKE_ERROR__MOSFET_FAILURE, // Ripple check has failed
"CAN Bus Offline",//BRAKE_ERROR__CAN_BUS_OFFLINE,
"Duplicate",//BRAKE_ERROR__DUPLICATE_ADDR,//Multiple brakeboards on network with same DIP addressing
"BOD Reset",//BRAKE_ERROR__RESET_BOD,
"AC Loss",//BRAKE_ERROR__AC_LOSS,  // Loss of 120AC source (only for 20A brake)
"Overheat",//BRAKE_ERROR__OVERHEAT,  // Loss of 120AC source (only for 20A brake)
};
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
static void Print_Screen(uint8_t ucCursor)
{
   uint8_t ucIndex;

   for(ucIndex = 0; ucIndex < 3; ucIndex++)//num lines
   {
      uint8_t ucLine = ucCursor + ucIndex;
      if(ucLine < NUM_BRAKE_DATA)//max bitmap size
      {
         LCD_Char_GotoXY( 0, 1+ucIndex );

         const char *ps = pasDataStrings[ucLine];
         LCD_Char_WriteString(ps);

         switch(ucLine)
         {
            case BRAKE_DATA__STATE:
               if(GetEBrakeData_State() < NUM_BRAKE_STATES) {
                  LCD_Char_WriteString(pasState[GetEBrakeData_State()]);
               }
               break;
            case BRAKE_DATA__ERROR:
               if(GetEBrakeData_Error() < NUM_BRAKE_ERRORS) {
                  LCD_Char_WriteString(pasError[GetEBrakeData_Error()]);
               }
               break;
            case BRAKE_DATA__VOLT_FB:
               LCD_Char_WriteInteger_MinLength(GetEBrakeData_VoltageFeedback(),1);
               break;
            case BRAKE_DATA__BPS:
               LCD_Char_WriteInteger_MinLength(GetEBrakeData_BPS(),1);
               break;
            case BRAKE_DATA__BRAKE_VAC:
               LCD_Char_WriteString(pasVACStrings[GetEBrakeData_BrakeVoltage()]);
               break;
            default:
               break;
         }
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_EBrakeStatus( void )
{
   static uint8_t ucCursorY = 0;

   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("EBrake Status");

   Print_Screen(ucCursorY);

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      uint8_t ucLimit = (NUM_BRAKE_DATA > 3) ? NUM_BRAKE_DATA-3:0;

      if(ucCursorY < ucLimit)//Minus cursor scroll+1
      {
         ucCursorY++;
      }
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

