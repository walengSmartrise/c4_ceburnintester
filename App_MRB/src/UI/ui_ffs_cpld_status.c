/******************************************************************************
 *
 * @file     ui_ffs_cpld_status.c
 * @brief    Pages for viewing CPLD status information
 * @version  V1.00
 * @date     14, Aug 2019
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "GlobalData.h"
#include <math.h>
#include "fpga_api.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_CPLD_Status_MR( void );
static void UI_FFS_CPLD_Status_CT( void );
static void UI_FFS_CPLD_Status_COP( void );
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define NUM_LCD_DISPLAY_LINES_Y        (4)
#define INVALID_INPUT                  (255)
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static en_fpga_locations eLoc;
static uint8_t ucCursorY;

static char const * const pasLocations[NUM_FPGA_LOC] =
{
      "MR",
      "CT",
      "COP",
};
static char const * const pasPreflightStatus[NUM_PREFLIGHT_STATUS] =
{
"INACTIVE",//PREFLIGHT_STATUS__INACTIVE,
"ACTIVE",//PREFLIGHT_STATUS__ACTIVE,
"PASS",//PREFLIGHT_STATUS__PASS,
"FAIL",//PREFLIGHT_STATUS__FAIL,
};
static char const * const pasPreflightCommand[NUM_PREFLIGHT_CMD] =
{
"INACTIVE",//PREFLIGHT_CMD__INACTIVE,
"NA",//PREFLIGHT_CMD__UNUSED1,
"NA",//PREFLIGHT_CMD__UNUSED2,
"NA",//PREFLIGHT_CMD__UNUSED3,
"PICK BYP",//PREFLIGHT_CMD__PICK_BYPASS,
"DROP RG",//PREFLIGHT_CMD__DROP_GRIPPER,
"PICK RG",//PREFLIGHT_CMD__PICK_GRIPPER,
"DROP BYP",//PREFLIGHT_CMD__DROP_BYPASS,
};
//------------------------------------------------------
static char const * const pasInputs_MR[END_OF_INPUTS__FPGA_BITS_MR_V2] =
{
   "RELAY C SFP",//FPGA_BITS_MR_V2__RELAY_C_SFP,
   "RELAY M SFP",//FPGA_BITS_MR_V2__RELAY_M_SFP,
   "RELAY C SFM",//FPGA_BITS_MR_V2__RELAY_C_SFM,
   "RELAY M SFM",//FPGA_BITS_MR_V2__RELAY_M_SFM,
   "RELAY C EB1",//FPGA_BITS_MR_V2__RELAY_C_EB1,
   "RELAY M EB1",//FPGA_BITS_MR_V2__RELAY_M_EB1,

   "RELAY C EB2",//FPGA_BITS_MR_V2__RELAY_C_EB2,
   "RELAY M EB2",//FPGA_BITS_MR_V2__RELAY_M_EB2,
   "RELAY C EB3",//FPGA_BITS_MR_V2__RELAY_C_EB3,
   "RELAY M EB3",//FPGA_BITS_MR_V2__RELAY_M_EB3,
   "RELAY C EB4",//FPGA_BITS_MR_V2__RELAY_C_EB4,
   "RELAY M EB4",//FPGA_BITS_MR_V2__RELAY_M_EB4,
   //
   "PIT INSP",//FPGA_BITS_MR_V2__INSP_PIT,
   "LND INSP",//FPGA_BITS_MR_V2__INSP_LAND,
   "MR INSP",//FPGA_BITS_MR_V2__INSP_MR,
   "ATU",//FPGA_BITS_MR_V2__ATU,
   "ATD",//FPGA_BITS_MR_V2__ATD,
   "ABU",//FPGA_BITS_MR_V2__ABU,

   "ABD",//FPGA_BITS_MR_V2__ABD,
   "MM",//FPGA_BITS_MR_V2__MM,
   "BYP H",//FPGA_BITS_MR_V2__BYP_H,
   "BYP C",//FPGA_BITS_MR_V2__BYP_C,
   "LRT",//FPGA_BITS_MR_V2__LRT,
   "LRM",//FPGA_BITS_MR_V2__LRM,
   //
   "LRB",//FPGA_BITS_MR_V2__LRB,
   "LFT",//FPGA_BITS_MR_V2__LFT,
   "LFM",//FPGA_BITS_MR_V2__LFM,
   "LFB",//FPGA_BITS_MR_V2__LFB,
   "120VAC",//FPGA_BITS_MR_V2__120VAC,
   "GOV",//FPGA_BITS_MR_V2__GOV,
   //
   "PIT",//FPGA_BITS_MR_V2__PIT,
   "BUF",//FPGA_BITS_MR_V2__BUF,
   "TFL",//FPGA_BITS_MR_V2__TFL,
   "BFL",//FPGA_BITS_MR_V2__BFL,
   "SFH",//FPGA_BITS_MR_V2__SFH,
   "SFM",//FPGA_BITS_MR_V2__SFM,
   //
   "DIP B1",//FPGA_BITS_MR_V2__DIP_B1,
   "DIP B2",//FPGA_BITS_MR_V2__DIP_B2,
   "DIP B3",//FPGA_BITS_MR_V2__DIP_B3,
   "DIP B4",//FPGA_BITS_MR_V2__DIP_B4,
   "DIP B5",//FPGA_BITS_MR_V2__DIP_B5,
   "DIP B6",//FPGA_BITS_MR_V2__DIP_B6,

   "DIP B7",//FPGA_BITS_MR_V2__DIP_B7,
   "DIP B8",//FPGA_BITS_MR_V2__DIP_B8,
   "NTS",//FPGA_BITS_MR_V2__NTS_STATUS,
};
static char const * const pasInputs_CT[END_OF_INPUTS__FPGA_BITS_CT] =
{
   "CT SW",//FPGA_BITS_CT__CT_SW,
   "ESC HATCH",//FPGA_BITS_CT__ESC_HATCH,
   "CAR SAFE",//FPGA_BITS_CT__CAR_SAFE,
   "CT INSP",//FPGA_BITS_CT__CT_INSP,
   "GSWF",//FPGA_BITS_CT__GSWF,
   "GSWR",//FPGA_BITS_CT__GSWR,

   "DZF",//FPGA_BITS_CT__DZF,
   "DZR",//FPGA_BITS_CT__DZR,
   "DIP B1",//FPGA_BITS_CT__DIP_B1,
   "DIP B2",//FPGA_BITS_CT__DIP_B2,
   "DIP B3",//FPGA_BITS_CT__DIP_B3,
   "DIP B4",//FPGA_BITS_CT__DIP_B4,

   "DIP B5",//FPGA_BITS_CT__DIP_B5,
   "DIP B6",//FPGA_BITS_CT__DIP_B6,
   "DIP B7",//FPGA_BITS_CT__DIP_B7,
   "DIP B8",//FPGA_BITS_CT__DIP_B8,
};
static char const * const pasInputs_COP[END_OF_INPUTS__FPGA_BITS_COP] =
{
   "HA INSP",//FPGA_BITS_COP__HA_INSP,
   "IC ST",//FPGA_BITS_COP__IC_ST,
   "FSS",//FPGA_BITS_COP__FSS,
   "IC INSP",//FPGA_BITS_COP__IC_INSP,
   "DIP B1",//FPGA_BITS_COP__DIP_B1,
   "DIP B2",//FPGA_BITS_COP__DIP_B2,

   "DIP B3",//FPGA_BITS_COP__DIP_B3,
   "DIP B4",//FPGA_BITS_COP__DIP_B4,
   "DIP B5",//FPGA_BITS_COP__DIP_B5,
   "DIP B6",//FPGA_BITS_COP__DIP_B6,
   "DIP B7",//FPGA_BITS_COP__DIP_B7,
   "DIP B8",//FPGA_BITS_COP__DIP_B8,
};
//------------------------------------------------------
static char const * const pasPreflightFaults_MR[NUM_FPGA_FAULTS_MR_V2] =
{
   "NONE",  //FPGA_Fault_MR__NONE,
   "STARTUP MR",//FPGA_Fault_MR__STARTUP_MR,
   "STARTUP CT",//FPGA_Fault_MR__STARTUP_CT,
   "STARTUP COP",//FPGA_Fault_MR__STARTUP_COP,
   "UNINT MOV",    //FPGA_Fault_MR__UNINTENDED_MOV,
   "COMM LOSS CT",//FPGA_Fault_MR__COMM_LOSS_CT,
   "COMM LOSS COP",//FPGA_Fault_MR__COMM_LOSS_COP,
   "120VAC LOSS",    //FPGA_Fault_MR__120VAC_LOSS,
   "GOV LOSS",  //FPGA_Fault_MR__GOVERNOR_LOSS,
   "CAR BYP SW",  //FPGA_Fault_MR__CAR_BYPASS_SW,
   "HALL BYP SW",    //FPGA_Fault_MR__HALL_BYPASS_SW,
   "SFM LOSS",    //FPGA_Fault_MR__SFM_LOSS,
   "SFH LOSS",    //FPGA_Fault_MR__SFH_LOSS,
   "PIT LOSS",    //FPGA_Fault_MR__PIT_LOSS,
   "BUF LOSS",    //FPGA_Fault_MR__BUF_LOSS,
   "TFL LOSS",    //FPGA_Fault_MR__TFL_LOSS,
   "BFL LOSS",    //FPGA_Fault_MR__BFL_LOSS,
   "CT SW LOSS",  //FPGA_Fault_MR__CT_SW_LOSS,
   "ESC HATCH LOSS",    //FPGA_Fault_MR__ESC_HATCH_LOSS,
   "CAR SAFE LOSS",  //FPGA_Fault_MR__CAR_SAFE_LOSS,
   "IC STOP SW",  //FPGA_Fault_MR__IC_STOP_SW,
   "FIRE STOP SW",   //FPGA_Fault_MR__FIRE_STOP_SW,
   "INSP",  //FPGA_Fault_MR__INSPECTION,
   "ACCESS",   //FPGA_Fault_MR__ACCESS,
   "LFT",    //FPGA_Fault_MR__LOCK_FRONT_TOP,
   "LFM",    //FPGA_Fault_MR__LOCK_FRONT_MID,
   "LFB",    //FPGA_Fault_MR__LOCK_FRONT_BOT,
   "LRT",  //FPGA_Fault_MR__LOCK_REAR_TOP,
   "LRM",  //FPGA_Fault_MR__LOCK_REAR_MID,
   "LRB",  //FPGA_Fault_MR__LOCK_REAR_BOT,
   "GSWF",  //FPGA_Fault_MR__GATESWITCH_FRONT,
   "GSWR",   //FPGA_Fault_MR__GATESWITCH_REAR,
   "PF PIT INSP",    //FPGA_Fault_MR__PF_PIT_INSP,
   "PF LND INSP",    //FPGA_Fault_MR__PF_LND_INSP,
   "PF BFL",   //FPGA_Fault_MR__PF_BFL,
   "PF TFL",   //FPGA_Fault_MR__PF_TFL,
   "PF BUF",   //FPGA_Fault_MR__PF_BUF,
   "PF PIT",   //FPGA_Fault_MR__PF_PIT,
   "PF GOV",   //FPGA_Fault_MR__PF_GOV,
   "PF SFH",   //FPGA_Fault_MR__PF_SFH,
   "PF SFM",   //FPGA_Fault_MR__PF_SFM,
   "PF LFT",   //FPGA_Fault_MR__PF_LFT,
   "PF LFM",   //FPGA_Fault_MR__PF_LFM,
   "PF LFB",   //FPGA_Fault_MR__PF_LFB,
   "PF LRT",   //FPGA_Fault_MR__PF_LRT,
   "PF LRM",   //FPGA_Fault_MR__PF_LRM,
   "PF LRB",   //FPGA_Fault_MR__PF_LRB,
   "PF BYP H",    //FPGA_Fault_MR__PF_BYP_H,
   "PF BYP C",    //FPGA_Fault_MR__PF_BYP_C,
   "PF MR_INSP",  //FPGA_Fault_MR__PF_MR_INSP,
   "PF CPLD PICK BYP",  //FPGA_Fault_MR__PF_CPLD_PICK_BYPASS,
   "PF MCU PICK BYP",   //FPGA_Fault_MR__PF_MCU_PICK_BYPASS,
   "PF MCU DROP GRIP",  //FPGA_Fault_MR__PF_MCU_DROP_GRIPPER,
   "PF CPLD DROP GRIP",    //FPGA_Fault_MR__PF_CPLD_DROP_GRIPPER,
   "PF CPLD PICK GRIP",    //FPGA_Fault_MR__PF_CPLD_PICK_GRIPPER,
   "PF MCU PICK GRIP",  //FPGA_Fault_MR__PF_MCU_PICK_GRIPPER,
   "PF MCU DROP BYP",   //FPGA_Fault_MR__PF_MCU_DROP_BYPASS,
   "PF CPLD DROP BYP",  //FPGA_Fault_MR__PF_CPLD_DROP_BYPASS,
};
static char const * const pasPreflightFaults_CT[NUM_FPGA_FAULTS_CT] =
{
   "NONE",  //FPGA_FAULT_CT__NONE,
   "STARTUP",//FPGA_FAULT_CT__STARTUP,
   "PF CT SW",//FPGA_FAULT_CT__PF_CT_SW,
   "PF ESC HATCH",//FPGA_FAULT_CT__PF_ESC_HATCH,
   "PF CAR SAFE",//FPGA_FAULT_CT__PF_CAR_SAFE,
   "PF CT INSP",//FPGA_FAULT_CT__PF_CT_INSP,
   "PF GSWF",//FPGA_FAULT_CT__PF_GSWF,
   "PF GSWR",//FPGA_FAULT_CT__PF_GSWR,
   "PF DZF",//FPGA_FAULT_CT__PF_DZF,
   "PF DZR",//FPGA_FAULT_CT__PF_DZR,
};
static char const * const pasPreflightFaults_COP[NUM_FPGA_FAULTS_COP] =
{
   "NONE",  //FPGA_FAULT_COP__NONE,
   "STARTUP",//FPGA_FAULT_COP__STARTUP,
   "PF HA INSP",//FPGA_FAULT_COP__PF_HA_INSP,
   "PF IC ST",//FPGA_FAULT_COP__PF_IC_ST,
   "PF FSS",//FPGA_FAULT_COP__PF_FSS,
   "PF IC INSP",//FPGA_FAULT_COP__PF_IC_INSP,
};
//------------------------------------------------------
static uint8_t aucMCU_Inputs_MR[END_OF_INPUTS__FPGA_BITS_MR_V2] =
{
      enIN_CSFP,//FPGA_BITS_MR_V2__RELAY_C_SFP,
      enIN_MSFP,//FPGA_BITS_MR_V2__RELAY_M_SFP,
      INVALID_INPUT,//FPGA_BITS_MR_V2__RELAY_C_SFM,
      enIN_MSFM,//FPGA_BITS_MR_V2__RELAY_M_SFM,
      enIN_CRGP,//FPGA_BITS_MR_V2__RELAY_C_EB1,
      enIN_MRGP,//FPGA_BITS_MR_V2__RELAY_M_EB1,
//
      INVALID_INPUT,//FPGA_BITS_MR_V2__RELAY_C_EB2,
      enIN_MRGM,//FPGA_BITS_MR_V2__RELAY_M_EB2,
      enIN_CRGB,//FPGA_BITS_MR_V2__RELAY_C_EB3,
      enIN_MRGBP,//FPGA_BITS_MR_V2__RELAY_M_EB3,
      INVALID_INPUT,//FPGA_BITS_MR_V2__RELAY_C_EB4,
      enIN_MRGBM,//FPGA_BITS_MR_V2__RELAY_M_EB4,
//
      enIN_IPTR,//FPGA_BITS_MR_V2__INSP_PIT,
      enIN_ILTR,//FPGA_BITS_MR_V2__INSP_LAND,
      enIN_MRTR,//FPGA_BITS_MR_V2__INSP_MR,
      enIN_ATU,//FPGA_BITS_MR_V2__ATU,
      enIN_ATD,//FPGA_BITS_MR_V2__ATD,
      enIN_ABU,//FPGA_BITS_MR_V2__ABU,
//
      enIN_ABD,//FPGA_BITS_MR_V2__ABD,
      enIN_MM,//FPGA_BITS_MR_V2__MM,
      enIN_BYPH,//FPGA_BITS_MR_V2__BYP_H,
      enIN_BYPC,//FPGA_BITS_MR_V2__BYP_C,
      enIN_LTR,//FPGA_BITS_MR_V2__LRT,
      enIN_LMR,//FPGA_BITS_MR_V2__LRM,
//
      enIN_LBR,//FPGA_BITS_MR_V2__LRB,
      enIN_LTF,//FPGA_BITS_MR_V2__LFT,
      enIN_LMF,//FPGA_BITS_MR_V2__LFM,
      enIN_LBF,//FPGA_BITS_MR_V2__LFB,
      enIN_M120VAC,//FPGA_BITS_MR_V2__120VAC,
      enIN_GOV,//FPGA_BITS_MR_V2__GOV,
//
      enIN_PIT,//FPGA_BITS_MR_V2__PIT,
      enIN_BUF,//FPGA_BITS_MR_V2__BUF,
      enIN_TFL,//FPGA_BITS_MR_V2__TFL,
      enIN_BFL,//FPGA_BITS_MR_V2__BFL,
      enIN_ZHIN,//FPGA_BITS_MR_V2__SFH,
      enIN_ZMIN,//FPGA_BITS_MR_V2__SFM,
//
      INVALID_INPUT,//FPGA_BITS_MR_V2__DIP_B1,
      INVALID_INPUT,//FPGA_BITS_MR_V2__DIP_B2,
      INVALID_INPUT,//FPGA_BITS_MR_V2__DIP_B3,
      INVALID_INPUT,//FPGA_BITS_MR_V2__DIP_B4,
      INVALID_INPUT,//FPGA_BITS_MR_V2__DIP_B5,
      INVALID_INPUT,//FPGA_BITS_MR_V2__DIP_B6,
//
      INVALID_INPUT,//FPGA_BITS_MR_V2__DIP_B7,
      INVALID_INPUT,//FPGA_BITS_MR_V2__DIP_B8,
      INVALID_INPUT,//FPGA_BITS_MR_V2__NTS_STATUS,
};
static uint8_t aucMCU_Inputs_CT[END_OF_INPUTS__FPGA_BITS_CT] =
{
      enIN_CT_SW,//FPGA_BITS_CT__CT_SW,
      enIN_ESC_HATCH,//FPGA_BITS_CT__ESC_HATCH,
      enIN_CAR_SAFE,//FPGA_BITS_CT__CAR_SAFE,
      enIN_CTTR,//FPGA_BITS_CT__CT_INSP,
      enIN_GSWF,//FPGA_BITS_CT__GSWF,
      enIN_GSWR,//FPGA_BITS_CT__GSWR,
      enIN_DZ_F,//FPGA_BITS_CT__DZF,
      enIN_DZ_R,//FPGA_BITS_CT__DZR,
      INVALID_INPUT,//FPGA_BITS_CT__DIP_B1,
      INVALID_INPUT,//FPGA_BITS_CT__DIP_B2,
      INVALID_INPUT,//FPGA_BITS_CT__DIP_B3,
      INVALID_INPUT,//FPGA_BITS_CT__DIP_B4,
      INVALID_INPUT,//FPGA_BITS_CT__DIP_B5,
      INVALID_INPUT,//FPGA_BITS_CT__DIP_B6,
      INVALID_INPUT,//FPGA_BITS_CT__DIP_B7,
      INVALID_INPUT,//FPGA_BITS_CT__DIP_B8,
};
static uint8_t aucMCU_Inputs_COP[END_OF_INPUTS__FPGA_BITS_COP] =
{
      enIN_HATR,//FPGA_BITS_COP__HA_INSP,
      enIN_ICST,//FPGA_BITS_COP__IC_ST,
      enIN_FIRE_STOP_SW,//FPGA_BITS_COP__FSS,
      enIN_ICTR,//FPGA_BITS_COP__IC_INSP,
      INVALID_INPUT,//FPGA_BITS_COP__DIP_B1,
      INVALID_INPUT,//FPGA_BITS_COP__DIP_B2,
      INVALID_INPUT,//FPGA_BITS_COP__DIP_B3,
      INVALID_INPUT,//FPGA_BITS_COP__DIP_B4,
      INVALID_INPUT,//FPGA_BITS_COP__DIP_B5,
      INVALID_INPUT,//FPGA_BITS_COP__DIP_B6,
      INVALID_INPUT,//FPGA_BITS_COP__DIP_B7,
      INVALID_INPUT,//FPGA_BITS_COP__DIP_B8,
};

//------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_CPLD_Status_MR =
{
   .pfnDraw = UI_FFS_CPLD_Status_MR,
};
static struct st_ui_screen__freeform gstFFS_CPLD_Status_CT =
{
   .pfnDraw = UI_FFS_CPLD_Status_CT,
};
static struct st_ui_screen__freeform gstFFS_CPLD_Status_COP =
{
   .pfnDraw = UI_FFS_CPLD_Status_COP,
};

static struct st_ui_menu_item gstMI_CPLD_Status_MR =
{
   .psTitle = "MR CPLD",
   .pstUGS_Next = &gstUGS_CPLD_Status_MR,
};
static struct st_ui_menu_item gstMI_CPLD_Status_CT =
{
   .psTitle = "CT CPLD",
   .pstUGS_Next = &gstUGS_CPLD_Status_CT,
};
static struct st_ui_menu_item gstMI_CPLD_Status_COP =
{
   .psTitle = "COP CPLD",
   .pstUGS_Next = &gstUGS_CPLD_Status_COP,
};
static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_CPLD_Status_MR,
   &gstMI_CPLD_Status_CT,
   &gstMI_CPLD_Status_COP,
};
static struct st_ui_screen__menu gstMenu =
{
   .psTitle = "CPLD Status",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_CPLD_Status_MR =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_CPLD_Status_MR,
};
struct st_ui_generic_screen gstUGS_CPLD_Status_CT =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_CPLD_Status_CT,
};
struct st_ui_generic_screen gstUGS_CPLD_Status_COP =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_CPLD_Status_COP,
};

struct st_ui_generic_screen gstUGS_Menu_CPLD_Status =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu,
};
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   CPLD_TOP_UI_LINE__TITLE,
   CPLD_TOP_UI_LINE__VERSION,
   CPLD_TOP_UI_LINE__FAULT,
   CPLD_TOP_UI_LINE__PF_STATUS,
   CPLD_TOP_UI_LINE__PF_COMMAND,
   CPLD_TOP_UI_LINE__PF_ERROR,
   CPLD_TOP_UI_LINE__INPUTS,

   NUM_CPLD_TOP_UI_LINES
} en_cpld_top_ui_lines; // The first ines shared by all CPLD status pages
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

|01234567890123456789|
|MR CPLD             | //Line 0
|VERSION: 01         | //Line 1
|FAULT: 01           | //Line 2
|PF STAT: INACTIVE   | //Line 3


|01234567890123456789| // Access inputs by scrolling down
|PF CMD:  PICK GRIP  | //Line 4
|INPUTS:             | //Line 5
| RELAY C SFP  [M][S]| //Line 6
| RELAY M SFP  [ ][ ]| //Line 7

-----------------------------------------------------------------------------*/
static void Print_Screen(void)
{
   LCD_Char_Clear();

   for(uint8_t i = 0; i < NUM_LCD_DISPLAY_LINES_Y; i++)//num lines
   {
      uint8_t ucLine = ucCursorY + i;
      LCD_Char_GotoXY( 0, i );
      switch(ucLine)
      {
         case CPLD_TOP_UI_LINE__TITLE:
            LCD_Char_WriteString(pasLocations[eLoc]);
            LCD_Char_WriteString(" CPLD");
            break;
         case CPLD_TOP_UI_LINE__VERSION:
         {
            uint8_t ucVersion = FPGA_GetVersion(eLoc);
            uint8_t ucMajor = ucVersion >> 4;
            uint8_t ucMinor = ucVersion & 0x0F;
            LCD_Char_WriteString("VERSION: ");
            LCD_Char_WriteInteger_ExactLength(ucMajor, 1);
            LCD_Char_WriteChar('.');
            LCD_Char_WriteInteger_ExactLength(ucMinor, 1);
         }
            break;
         case CPLD_TOP_UI_LINE__FAULT:
         {
            uint8_t ucFault = FPGA_GetFault(eLoc);
            LCD_Char_WriteString("FLT: ");
            LCD_Char_WriteInteger_ExactLength(ucFault, 2);
            LCD_Char_WriteString(" - ");
            if( eLoc == FPGA_LOC__MRA )
            {
               if( ucFault < NUM_FPGA_FAULTS_MR_V2 )
               {
                  LCD_Char_WriteString(pasPreflightFaults_MR[ucFault]);
               }
               else
               {
                  LCD_Char_WriteString("UNK");
               }
            }
            else if( eLoc == FPGA_LOC__CTA )
            {
               if( ucFault < NUM_FPGA_FAULTS_CT )
               {
                  LCD_Char_WriteString(pasPreflightFaults_CT[ucFault]);
               }
               else
               {
                  LCD_Char_WriteString("UNK");
               }
            }
            else if( eLoc == FPGA_LOC__COPA )
            {
               if( ucFault < NUM_FPGA_FAULTS_COP )
               {
                  LCD_Char_WriteString(pasPreflightFaults_COP[ucFault]);
               }
               else
               {
                  LCD_Char_WriteString("UNK");
               }
            }
         }
            break;
         case CPLD_TOP_UI_LINE__PF_STATUS:
         {
            PreflightStatus eStatus = FPGA_GetPreflightStatus(eLoc);
            LCD_Char_WriteString("PF STAT: ");
            if( eStatus < NUM_PREFLIGHT_STATUS )
            {
               LCD_Char_WriteString(pasPreflightStatus[eStatus]);
            }
            else
            {
               LCD_Char_WriteString("UNK");
            }
         }
            break;
         case CPLD_TOP_UI_LINE__PF_COMMAND:
         {
            PreflightMCU_Command eCommand = FPGA_GetPreflightCommand(eLoc);
            LCD_Char_WriteString("PF CMD:  ");
            LCD_Char_WriteString(pasPreflightCommand[eCommand]);
         }
            break;
         case CPLD_TOP_UI_LINE__PF_ERROR:
         {
            uint8_t ucFault = FPGA_GetPreflightErrors(eLoc);
            LCD_Char_WriteString("PFE: ");
            LCD_Char_WriteInteger_ExactLength(ucFault, 2);
         }
            break;
         case CPLD_TOP_UI_LINE__INPUTS:
            LCD_Char_WriteString("INPUTS:");
            break;
         default:// Inputs
         {
            uint8_t ucInputBitIndex = ucLine-NUM_CPLD_TOP_UI_LINES;
            if(eLoc == FPGA_LOC__MRA)
            {
               uint8_t bCPLD = FPGA_GetDataBit(eLoc, ucInputBitIndex);
               uint8_t bMCU = 0;
               uint8_t ucInputFunction = aucMCU_Inputs_MR[ucInputBitIndex];
               if( ucInputFunction < NUM_INPUT_FUNCTIONS )
               {
                  bMCU = GetInputValue(ucInputFunction);
               }
               else if( ( ucInputBitIndex >= FPGA_BITS_MR_V2__DIP_B1 )
                     && ( ucInputBitIndex <= FPGA_BITS_MR_V2__DIP_B8 )
                     && ( GetSRU_Deployment() == enSRU_DEPLOYMENT__MR ) )
               {
                  bMCU = SRU_Read_DIP_Switch(ucInputBitIndex-FPGA_BITS_MR_V2__DIP_B1+enSRU_DIP_B1);
               }
               else if( ucInputBitIndex == FPGA_BITS_MR_V2__NTS_STATUS )
               {
                  bMCU = GetNTS_StopFlag_CTB();
               }
               /* We dont't have access to some of the outputs here so mirror the CPLD */
               else if( ( ucInputBitIndex == FPGA_BITS_MR_V2__RELAY_C_SFM )
                     || ( ucInputBitIndex == FPGA_BITS_MR_V2__RELAY_C_EB2 )
                     || ( ucInputBitIndex == FPGA_BITS_MR_V2__RELAY_C_EB4 ) )
               {
                  bMCU = bCPLD;
               }

               LCD_Char_GotoXY( 1, i );
               LCD_Char_WriteString(pasInputs_MR[ucInputBitIndex]);
               LCD_Char_GotoXY( 14, i );
               if(bCPLD)
               {
                  LCD_Char_WriteString("[S]");
               }
               else
               {
                  LCD_Char_WriteString("[ ]");
               }
               if(bMCU)
               {
                  LCD_Char_WriteString("[M]");
               }
               else
               {
                  LCD_Char_WriteString("[ ]");
               }
            }
            else if(eLoc == FPGA_LOC__CTA)
            {
               uint8_t bCPLD = FPGA_GetDataBit(eLoc, ucInputBitIndex);
               uint8_t bMCU = 0;
               uint8_t ucInputFunction = aucMCU_Inputs_CT[ucInputBitIndex];
               if( ucInputFunction < NUM_INPUT_FUNCTIONS )
               {
                  bMCU = GetInputValue(ucInputFunction);
               }
               else if( ( ucInputBitIndex >= FPGA_BITS_CT__DIP_B1 )
                     && ( ucInputBitIndex <= FPGA_BITS_CT__DIP_B8 )
                     && ( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT ) )
               {
                  bMCU = SRU_Read_DIP_Switch(ucInputBitIndex-FPGA_BITS_CT__DIP_B1+enSRU_DIP_B1);
               }

               LCD_Char_GotoXY( 1, i );
               LCD_Char_WriteString(pasInputs_CT[ucInputBitIndex]);
               LCD_Char_GotoXY( 14, i );
               if(bCPLD)
               {
                  LCD_Char_WriteString("[S]");
               }
               else
               {
                  LCD_Char_WriteString("[ ]");
               }
               if(bMCU)
               {
                  LCD_Char_WriteString("[M]");
               }
               else
               {
                  LCD_Char_WriteString("[ ]");
               }
            }
            else //FPGA_LOC__COPA
            {
               uint8_t bCPLD = FPGA_GetDataBit(eLoc, ucInputBitIndex);
               uint8_t bMCU = 0;
               uint8_t ucInputFunction = aucMCU_Inputs_COP[ucInputBitIndex];
               if( ucInputFunction < NUM_INPUT_FUNCTIONS )
               {
                  bMCU = GetInputValue(ucInputFunction);
               }
               else if( ( ucInputBitIndex >= FPGA_BITS_COP__DIP_B1 )
                     && ( ucInputBitIndex <= FPGA_BITS_COP__DIP_B8 )
                     && ( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP ) )
               {
                  bMCU = SRU_Read_DIP_Switch(ucInputBitIndex-FPGA_BITS_COP__DIP_B1+enSRU_DIP_B1);
               }

               LCD_Char_GotoXY( 1, i );
               LCD_Char_WriteString(pasInputs_COP[ucInputBitIndex]);
               LCD_Char_GotoXY( 14, i );
               if(bCPLD)
               {
                  LCD_Char_WriteString("[S]");
               }
               else
               {
                  LCD_Char_WriteString("[ ]");
               }
               if(bMCU)
               {
                  LCD_Char_WriteString("[M]");
               }
               else
               {
                  LCD_Char_WriteString("[ ]");
               }
            }
         }
            break;
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_CPLD_Status_MR( void )
{
   uint8_t ucLimitY = END_OF_INPUTS__FPGA_BITS_MR_V2+NUM_CPLD_TOP_UI_LINES-NUM_LCD_DISPLAY_LINES_Y;
   eLoc = FPGA_LOC__MRA;

   enum en_keypresses enKeypress = Button_GetKeypress();
   if( ( enKeypress == enKEYPRESS_LEFT ) || ( !( Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V2) || Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) ) ) )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN )
   {
      ucCursorY++;
   }

   if( ucCursorY > ucLimitY )
   {
      ucCursorY = ucLimitY;
   }

   SetUIRequest_ViewDebugData(VDD__CPLD_MR);
   Print_Screen();
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_CPLD_Status_CT( void )
{
   uint8_t ucLimitY = END_OF_INPUTS__FPGA_BITS_CT+NUM_CPLD_TOP_UI_LINES-NUM_LCD_DISPLAY_LINES_Y;
   eLoc = FPGA_LOC__CTA;

   enum en_keypresses enKeypress = Button_GetKeypress();
   if( ( enKeypress == enKEYPRESS_LEFT ) || ( !( Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V2) || Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) ) ) )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN )
   {
      ucCursorY++;
   }

   if( ucCursorY > ucLimitY )
   {
      ucCursorY = ucLimitY;
   }

   SetUIRequest_ViewDebugData(VDD__CPLD_CT);
   Print_Screen();
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_CPLD_Status_COP( void )
{
   uint8_t ucLimitY = END_OF_INPUTS__FPGA_BITS_COP+NUM_CPLD_TOP_UI_LINES-NUM_LCD_DISPLAY_LINES_Y;
   eLoc = FPGA_LOC__COPA;

   enum en_keypresses enKeypress = Button_GetKeypress();
   if( ( enKeypress == enKEYPRESS_LEFT ) || ( !( Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V2) || Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) ) ) )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN )
   {
      ucCursorY++;
   }

   if( ucCursorY > ucLimitY )
   {
      ucCursorY = ucLimitY;
   }

   SetUIRequest_ViewDebugData(VDD__CPLD_COP);
   Print_Screen();
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
