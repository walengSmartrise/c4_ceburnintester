/******************************************************************************
 *
 * @file     ui_menu_faults.c
 * @brief    Faults Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_ui_menu_item gstMI_Active =
{
   .psTitle = "Active",
   .pstUGS_Next = &gstUGS_ActiveFaults,
};
static struct st_ui_menu_item gstMI_Logged =
{
   .psTitle = "Logged",
   .pstUGS_Next = &gstUGS_LoggedFaults,
};
static struct st_ui_menu_item gstMI_ClearLog =
{
   .psTitle = "Clear Log",
   .pstUGS_Next = &gstUGS_Faults_ClearLog,
};

static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_Active,
   &gstMI_Logged,
   &gstMI_ClearLog,
};

static struct st_ui_screen__menu gstMenu =
{
   .psTitle = "Faults",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_Menu_Faults =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
