/******************************************************************************
 *
 * @file     ui_ffs_about.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <stdint.h>
#include <string.h>
#include <time.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_Active_Faults( void );
static void UI_FFS_PopUp_Faults( void );
static void UI_FFS_Logged_Faults( void );
static void UI_FFS_Faults_ClearLog( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_ui_screen__freeform gstFreeFormScreen_ActiveFaults =
{
   .pfnDraw = UI_FFS_Active_Faults,
};
static struct st_ui_screen__freeform gstFreeFormScreen_PopUpFaults =
{
   .pfnDraw = UI_FFS_PopUp_Faults,
};
static struct st_ui_screen__freeform gstFreeFormScreen_LoggedFaults =
{
   .pfnDraw = UI_FFS_Logged_Faults,
};
static struct st_ui_screen__freeform gstFFS_Faults_ClearLog =
{
   .pfnDraw = UI_FFS_Faults_ClearLog,
};
static char const * const pasBoardStrings[NUM_FAULT_NODES] =
{
      "CPLD: ",
      "MRA: ",
      "MRB: ",
      "CTA: ",
      "CTB: ",
      "COPA: ",
      "COPB: ",
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_ActiveFaults =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFreeFormScreen_ActiveFaults,
};
struct st_ui_generic_screen gstUGS_PopUpFaults =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFreeFormScreen_PopUpFaults,
   .pstUGS_Previous = &gstUGS_HomeScreen,
};

struct st_ui_generic_screen gstUGS_LoggedFaults =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFreeFormScreen_LoggedFaults,
};
struct st_ui_generic_screen gstUGS_Faults_ClearLog =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Faults_ClearLog,
};
//----------------------------------------------------------------------------


/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MAX_SCROLL_X_POS      7
#define MAX_DETAILS_SCROLLY   (5)

#define UI_REQUEST_CLEAR_LOG     (0xFF)
#define TIME_TO_CLEAR_LOG_1MS    (5000)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void FetchFaultLog()
{
   static uint8_t ucIndex;
   static uint32_t uiDelay_1ms;
   if( uiDelay_1ms > FAULT_LOG_REQ_UPDATE_RATE_1MS )
   {
      uiDelay_1ms = 0;
      if(++ucIndex >= NUM_FAULTLOG_ITEMS)
      {
         ucIndex = 0;
      }
   }
   else
   {
      uiDelay_1ms += MOD_RUN_PERIOD_UI_1MS;
   }
   SetUIRequest_FaultLog(ucIndex+1, GetSRU_Deployment());
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Print_FaultDetails( st_fault_data *pstFaultData, uint8_t ucScrollY )
{
   char *psTime = ctime(&pstFaultData->ulTimestamp);
   char *psPI = Get_PI_Label(pstFaultData->ucCurrentFloor);

   LCD_Char_Clear();
   LCD_Char_GotoXY(0,0);
   for(uint8_t ucRow = 0; ucRow < 4; ucRow++)
   {
      uint8_t ucIndex = ucRow + ucScrollY;
      switch( ucIndex )
      {
         case 0:
            PrintFaultString(pstFaultData->eFaultNum);
            break;

         case 1:
            LCD_Char_WriteString(" NUM: ");
            LCD_Char_WriteInteger_MinLength(pstFaultData->eFaultNum, 1);
            break;

         case 2:
            LCD_Char_WriteString(" TIME:");
            PrintMovingText(psTime);
            break;

         case 3:
            LCD_Char_WriteString(" SPD: ");
            LCD_Char_WriteSignedInteger(pstFaultData->wSpeed);
            break;

         case 4:
            LCD_Char_WriteString(" POS: ");
            LCD_WritePositionInFeet_Shortened(pstFaultData->ulPosition);
            break;

         case 5:
            LCD_Char_WriteString(" CMD: ");
            LCD_Char_WriteSignedInteger(pstFaultData->wCommandSpeed);
            break;

         case 6:
            LCD_Char_WriteString(" ENC: ");
            LCD_Char_WriteSignedInteger(pstFaultData->wEncoderSpeed);
            break;

         case 7:
            LCD_Char_WriteString(" FLR: ");
            //ignore first char for two PI displays
            if( ( Param_ReadValue_1Bit(enPARAM1__Enable3DigitPI) )
             && ( *psPI != ' ' ) ) // Drop the third char if its blank to save space
            {
               LCD_Char_WriteChar(*(psPI));
            }
            LCD_Char_WriteChar(*(psPI+1));
            LCD_Char_WriteChar(*(psPI+2));
            break;

         case 8:
            LCD_Char_WriteString(" DEST:");
            psPI = Get_PI_Label(pstFaultData->ucDestinationFloor);
            //ignore first char for two PI displays
            if( ( Param_ReadValue_1Bit(enPARAM1__Enable3DigitPI) )
             && ( *psPI != ' ' ) ) // Drop the third char if its blank to save space
            {
               LCD_Char_WriteChar(*(psPI));
            }
            LCD_Char_WriteChar(*(psPI+1));
            LCD_Char_WriteChar(*(psPI+2));
            break;

         default:
            break;
      }
      LCD_Char_GoToStartOfNextRow();
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_PopUp_Faults( void )
{
   uint16_t ucFault = 0;
   uint8_t ucNode_Plus1 = 0;
   for(uint8_t i = 0; i < NUM_FAULT_NODES; i++)
   {
      uint8_t ucNode = ( enFAULT_NODE__MRA+i ) % NUM_FAULT_NODES;
      ucFault = GetFault_ByNode( ucNode );
      if(ucFault)
      {
         ucNode_Plus1 = ucNode+1;
         break;
      }
   }

   if( !ucNode_Plus1 )
   {
      PrevScreen();
   }
   else if(ucFault)
   {
       // Special printing of CPLD faults
      st_fault_data stFaultData;
      GetBoardActiveFault( &stFaultData, ucNode_Plus1 - 1 );
      Print_FaultDetails(&stFaultData, 0);
   }
   else
   {
      LCD_Char_Clear();
      LCD_Char_GotoXY( 0, 0 );
   }

   enum en_keypresses enKeypress = Button_GetKeypress();
   if ( enKeypress != enKEYPRESS_NONE )
   {
      PrevScreen();
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Active_Faults( void )
{
   static uint8_t ucScrollY_Details; // Fault Details Y position
   static uint8_t ucScrollY;// Fault List Y position
   static uint8_t ucCursorY;
   static uint8_t ucScrollX;
   uint16_t gaucActiveFaults[NUM_FAULT_NODES] = {0};
   uint8_t ucNumActive = NUM_FAULT_NODES;

   /* Check for active faults*/
   for( uint8_t i = 0; i < NUM_FAULT_NODES; i++ )
   {
         gaucActiveFaults[i] = GetFault_ByNode(i);
   }

   if(ucScrollY+3 >= ucNumActive)
   {
      ucScrollY = (ucNumActive < 3) ? 0:ucNumActive - 3;
   }
   if(ucCursorY >= ucNumActive)
   {
      ucCursorY = ucNumActive - 1;
   }

   LCD_Char_Clear();
   LCD_Char_GotoXY(0,0);
   LCD_Char_WriteString("Active Faults ");

   st_fault_data stFaultData;
   if(ucScrollX)//Details screen
   {
      enum en_fault_nodes eFaultNode;
      eFaultNode = ucScrollY + ucCursorY;
      GetBoardActiveFault( &stFaultData, eFaultNode );
      Print_FaultDetails(&stFaultData, ucScrollY_Details);
   }
   else//Enterance screen
   {
      for( uint8_t i = 0; i < 3; i++ )
      {
         if(ucScrollY < ucNumActive)
         {
            LCD_Char_GotoXY( 1, 1+i );
            LCD_Char_WriteString( pasBoardStrings[i+ucScrollY] );
            PrintFaultString(gaucActiveFaults[i+ucScrollY]);
         }
      }
      LCD_Char_GotoXY(0,1+ucCursorY);
      LCD_Char_WriteString("*");
   }

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      if(ucScrollX)
      {
         ucScrollX--;
      }
      else
      {
         ucScrollY = 0;
         ucScrollY_Details = 0;
         PrevScreen();
      }
   }
   else if( enKeypress == enKEYPRESS_RIGHT )
   {
      if(ucScrollX < 1 && ucNumActive)
      {
         ucScrollX++;
      }
   }
   else if( enKeypress == enKEYPRESS_DOWN )
   {
      if(!ucScrollX)
      {
         if( ucCursorY == 2)
         {
            if(ucScrollY < ucNumActive - 3)
            {
               ucScrollY++;
            }
         }
         else
         {
            ucCursorY++;
         }
      }
      else
      {
         if(ucScrollY_Details < MAX_DETAILS_SCROLLY)
         {
            ucScrollY_Details++;
         }
      }
   }
   else if( enKeypress == enKEYPRESS_UP )
   {
      if(!ucScrollX)
      {
         if( !ucCursorY )
         {
            if(ucScrollY)
            {
               ucScrollY--;
            }
         }
         else
         {
            ucCursorY--;
         }
      }
      else
      {
         if(ucScrollY_Details)
         {
            ucScrollY_Details--;
         }
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Logged_Faults( void )
{
   static uint8_t ucScrollX;//details screen vs list
   static uint8_t ucScrollY;//up/dn on list
   static uint8_t ucScrollY_Details;//up/down on details screen
   static uint8_t ucCursorY;//ui pos indicator

   FetchFaultLog();
   //--------------------------------
   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Fault Log");
   if(ucScrollX)//Details screen
   {
      uint8_t ucLogIndex = ucScrollY + ucCursorY;
      st_fault_data stFault;
      GetFaultLogElement(&stFault, ucLogIndex+1);
      Print_FaultDetails(&stFault, ucScrollY_Details);
   }
   else//Entrance screen
   {
      for(uint8_t i = 0; i < 3; i++)
      {
         uint8_t ucLogIndex = ucScrollY + i;
         uint16_t ucFault = GetFaultLog_FaultNumber( ucLogIndex + 1 );

         LCD_Char_GotoXY( 1, i+1 );
         LCD_Char_WriteInteger_MinLength(ucLogIndex+1,1);
         LCD_Char_WriteString(".");
         PrintFaultString(ucFault);
      }
      LCD_Char_GotoXY(0,1+ucCursorY);
      LCD_Char_WriteString("*");
   }

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      if(ucScrollX)
      {
         ucScrollX--;
      }
      else
      {
         ucScrollY = 0;
         ucScrollY_Details = 0;
         SetUIRequest_FaultLog(0, GetSRU_Deployment());
         PrevScreen();
      }
   }
   else if( enKeypress == enKEYPRESS_RIGHT )
   {
      if(ucScrollX < 1)
      {
         ucScrollX++;
      }
   }
   else if( enKeypress == enKEYPRESS_DOWN )
   {
      if(!ucScrollX)
      {
         if( ucCursorY == 2)
         {
            if(ucScrollY < NUM_FAULTLOG_ITEMS - 3)
            {
               ucScrollY++;
            }
         }
         else
         {
            ucCursorY++;
         }
      }
      else
      {
         if(ucScrollY_Details < MAX_DETAILS_SCROLLY)
         {
            ucScrollY_Details++;
         }
      }
   }
   else if( enKeypress == enKEYPRESS_UP )
   {
      if(!ucScrollX)
      {
         if( !ucCursorY )
         {
            if(ucScrollY)
            {
               ucScrollY--;
            }
         }
         else
         {
            ucCursorY--;
         }
      }
      else
      {
         if(ucScrollY_Details)
         {
            ucScrollY_Details--;
         }
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Faults_ClearLog( void )
{
   static uint16_t uwClearingCounter;
   static uint8_t ucCursor = 0;
   static uint8_t bFinished = 1;
   const uint8_t ucFields = 2;

   enum en_keypresses enKeypress = Button_GetKeypress();
   switch(enKeypress)
   {
      case enKEYPRESS_RIGHT:
         if(ucCursor < ucFields-1)
         {
            ucCursor++;
         }
         break;
      case enKEYPRESS_LEFT:
         if( ucCursor )
         {
            ucCursor--;
         }
         else
         {
            SetUIRequest_FaultLog(0, GetSRU_Deployment());
            PrevScreen();
         }
         break;
      case enKEYPRESS_ENTER:
         if(ucCursor)
         {
            bFinished = 0;
            uwClearingCounter = 0;
         }
         else
         {
            SetUIRequest_FaultLog(0, GetSRU_Deployment());
            PrevScreen();
         }
         break;
      default:
         break;
   }

   if(bFinished)
   {
      LCD_Char_Clear();
      LCD_Char_GotoXY( 0, 0 );
      LCD_Char_WriteStringUpper("Clear Fault Log?");
      LCD_Char_GotoXY(4,2);
      LCD_Char_WriteStringUpper("NO");
      LCD_Char_GotoXY(12,2);
      LCD_Char_WriteStringUpper("YES");
      if(!ucCursor)
      {
         LCD_Char_GotoXY(5,3);
      }
      else
      {
         LCD_Char_GotoXY(12,3);
      }
      LCD_Char_WriteString("*");
   }
   else
   {
      LCD_Char_Clear();
      LCD_Char_GotoXY(4,2);

      LCD_Char_WriteStringUpper("Clearing");

      if(uwClearingCounter < TIME_TO_CLEAR_LOG_1MS)
      {
         SetUIRequest_FaultLog(UI_REQUEST_CLEAR_LOG, GetSRU_Deployment());
         uwClearingCounter += MOD_RUN_PERIOD_UI_1MS;
      }
      else
      {
         uwClearingCounter = 0;
         bFinished = 1;
         ucCursor = 0;
         SetUIRequest_FaultLog(0, GetSRU_Deployment());
         PrevScreen();
      }
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
