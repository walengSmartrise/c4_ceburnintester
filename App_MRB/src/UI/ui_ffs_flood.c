/******************************************************************************
 *
 * @file     ui_ffs_earthquake.c
 * @brief    Earthquake UI pages
 * @version  V1.00
 * @date     13, Feb 2018
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Flood_NumFloors( void );
static void UI_FFS_Flood_OkayToRun( void );
static void UI_FFS_Flood_OverrideFire( void );
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_Flood_NumFloors =
{
   .pfnDraw = UI_FFS_Flood_NumFloors,
};
static struct st_ui_screen__freeform gstFFS_Flood_OkayToRun =
{
   .pfnDraw = UI_FFS_Flood_OkayToRun,
};
static struct st_ui_screen__freeform gstFFS_Flood_OverrideFire =
{
   .pfnDraw = UI_FFS_Flood_OverrideFire,
};
//----------------------------------------------------------
static struct st_ui_menu_item gstMI_NumFloors =
{
   .psTitle = "Number of Floors",
   .pstUGS_Next = &gstUGS_Flood_NumFloors,
};
static struct st_ui_menu_item gstMI_OkayToRun =
{
   .psTitle = "Okay To Run",
   .pstUGS_Next = &gstUGS_Flood_OkayToRun,
};
static struct st_ui_menu_item gstMI_OverrideFire =
{
   .psTitle = "Override Fire",
   .pstUGS_Next = &gstUGS_Flood_OverrideFire,
};
static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_NumFloors,
   &gstMI_OkayToRun,
   &gstMI_OverrideFire,
};
static struct st_ui_screen__menu gstMenu_Setup_Flood =
{
   .psTitle = "Flood",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Flood_NumFloors =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Flood_NumFloors,
};
struct st_ui_generic_screen gstUGS_Flood_OkayToRun =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Flood_OkayToRun,
};
struct st_ui_generic_screen gstUGS_Flood_OverrideFire =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Flood_OverrideFire,
};
//------------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_Setup_Flood =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Flood,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MAX_CURSOR_X_POS (1)
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_Flood_NumFloors( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__FLOOD_NumFloors_Plus1;
   stParamEdit.uwParamIndex_End = enPARAM8__FLOOD_NumFloors_Plus1;
   stParamEdit.psTitle = "Num of Flood Floors";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_Flood_OkayToRun( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__FLOOD_OkayToRun;
   stParamEdit.uwParamIndex_End = enPARAM1__FLOOD_OkayToRun;
   stParamEdit.psTitle = "Okay To Run";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_Flood_OverrideFire( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__FLOOD_OverrideFire;
   stParamEdit.uwParamIndex_End = enPARAM1__FLOOD_OverrideFire;
   stParamEdit.psTitle = "Override Fire";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
