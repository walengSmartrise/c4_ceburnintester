/******************************************************************************
 *
 * @file     ui_ffs_earthquake.c
 * @brief    Earthquake UI pages
 * @version  V1.00
 * @date     13, Feb 2018
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_DebugRuns_DwellTime( void );
static void UI_FFS_DebugRuns_T2T( void );
static void UI_FFS_DebugRuns_F2F( void );
static void UI_FFS_DebugRuns_Random( void );
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_DebugRuns_DwellTime =
{
   .pfnDraw = UI_FFS_DebugRuns_DwellTime,
};
static struct st_ui_screen__freeform gstFFS_DebugRuns_T2T =
{
   .pfnDraw = UI_FFS_DebugRuns_T2T,
};
static struct st_ui_screen__freeform gstFFS_DebugRuns_F2F =
{
   .pfnDraw = UI_FFS_DebugRuns_F2F,
};
static struct st_ui_screen__freeform gstFFS_DebugRuns_Random =
{
   .pfnDraw = UI_FFS_DebugRuns_Random,
};
//----------------------------------------------------------
static struct st_ui_menu_item gstMI_DwellTime =
{
   .psTitle = "Dwell Time",
   .pstUGS_Next = &gstUGS_DebugRuns_DwellTime,
};
static struct st_ui_menu_item gstMI_T2T =
{
   .psTitle = "Terminal to Terminal",
   .pstUGS_Next = &gstUGS_DebugRuns_T2T,
};
static struct st_ui_menu_item gstMI_F2F =
{
   .psTitle = "Floor To Floor",
   .pstUGS_Next = &gstUGS_DebugRuns_F2F,
};
static struct st_ui_menu_item gstMI_Random =
{
   .psTitle = "Random",
   .pstUGS_Next = &gstUGS_DebugRuns_Random,
};
static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_DwellTime,
   &gstMI_T2T,
   &gstMI_F2F,
   &gstMI_Random,
};
static struct st_ui_screen__menu gstMenu_DebugRuns =
{
   .psTitle = "DebugRuns",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_DebugRuns_DwellTime =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DebugRuns_DwellTime,
};
struct st_ui_generic_screen gstUGS_DebugRuns_T2T =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DebugRuns_T2T,
};
struct st_ui_generic_screen gstUGS_DebugRuns_F2F =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DebugRuns_F2F,
};
struct st_ui_generic_screen gstUGS_DebugRuns_Random =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DebugRuns_Random,
};
//------------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_DebugRuns =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_DebugRuns,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_DebugRuns_DwellTime( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__DEBUG_TestRunsDwellTime_s;
   stParamEdit.uwParamIndex_End = enPARAM8__DEBUG_TestRunsDwellTime_s;
   stParamEdit.psTitle = "Test Run Dwell Time";
   stParamEdit.psUnit = " sec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_DebugRuns_T2T( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__DEBUG_RunTerminalToTerminal;
   stParamEdit.uwParamIndex_End = enPARAM1__DEBUG_RunTerminalToTerminal;
   stParamEdit.psTitle = "Terminal to Terminal";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_DebugRuns_F2F( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__DEBUG_RunFloorToFloor;
   stParamEdit.uwParamIndex_End = enPARAM1__DEBUG_RunFloorToFloor;
   stParamEdit.psTitle = "Floor to Floor";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_DebugRuns_Random( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__EnableRandomCarCallRuns;
   stParamEdit.uwParamIndex_End = enPARAM1__EnableRandomCarCallRuns;
   stParamEdit.psTitle = "Random Runs";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
