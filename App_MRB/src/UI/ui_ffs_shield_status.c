/*
 * ui_ffs_shield_status.c
 *
 *  Created on: Jan 14, 2019
 *      Author: SangH
 */

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "GlobalData.h"
#include "shieldData.h"
#include <math.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_ShieldStatus( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
//---------------------------------------------------------------

static struct st_ui_screen__freeform gstFFS_ShieldStatus =
{
   .pfnDraw = UI_FFS_ShieldStatus,
};


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_ShieldStatus =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ShieldStatus,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
static char const * const pasShieldErrors[NUM_SHIELD_ERRORS] =
{
   "Unknown", //SHIELD_ERROR__UNKNOWN, // Value 0 - For transmission purposes this signals that no communication has been seen from the shield
   "None", //SHIELD_ERROR__NONE, // Value 1 - No active errors
   "POR RST", //SHIELD_ERROR__POR_RESET, // Value 2 - Shield is starting up after being reset or power cycled. Should be held 10 seconds after startup.
   "BOD RST", //SHIELD_ERROR__BOD_RESET, // Value 3 - Shield is starting up after resetting due a dip in its power supply rail. Should be held 10 seconds after startup.
   "WDT RST", //SHIELD_ERROR__WDT_RESET, // Value 4 - Shield is starting up after watchdog triggered a reset. Should be held 10 seconds after startup.
   "Comm. Group", //SHIELD_ERROR__COM_GROUP, // Value 5 - Shield has not recieved any communication packets from the group network in 10 seconds.
   "Comm. RPi", //SHIELD_ERROR__COM_RPI, // Value 6 - Shield has not recieved any communication packets from the RPi in 10 seconds.
   "RTC", //SHIELD_ERROR__FAILED_RTC, // Value 7 - Shield RTC is not working.
   "UART TX OVF", //SHIELD_ERROR__UART_OVF_TX, // Value 8 - Shield UART transmit buffer overflowed.
   "UART RX OVF", //SHIELD_ERROR__UART_OVF_RX, // Value 9 - Shield UART receive buffer overflowed.
   "CAN OVF TX", //SHIELD_ERROR__CAN_OVF_TX, // Value 10 - Shield CAN transmit buffer overflowed.
   "CAN OVF RX", //SHIELD_ERROR__CAN_OVF_RX, // Value 11 - Shield CAN receive buffer overflowed.
   "CAN Bus RST", //SHIELD_ERROR__CAN_BUS_RST, // Value 12 - Shield CAN bus reset
};
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

|01234567890123456789|
|DAD STATUS - OFFLINE|
|Error:   CAN BUS RST|
|Version: 1.2        |
|RX Count:111111     |

-----------------------------------------------------------------------------*/
static void Print_Screen()
{
   LCD_Char_Clear();

   /* Line 1 */
   LCD_Char_GotoXY(0, 0);
   LCD_Char_WriteString("DAD STATUS - ");
   if( ShieldData_CheckTimeoutCounter() )
   {
      LCD_Char_WriteString("OFFLINE");
   }
   else
   {
      LCD_Char_WriteString("ONLINE");
   }

   /* Line 2 */
   LCD_Char_GotoXY(0, 1);
   LCD_Char_WriteString("Error:   ");
   en_shield_error eError = ShieldData_GetError();
   if(eError < NUM_SHIELD_ERRORS)
   {
      LCD_Char_WriteString(pasShieldErrors[eError]);
   }

   /* Line 3 */
   LCD_Char_GotoXY(0, 2);
   LCD_Char_WriteString("Version: ");
   LCD_Char_WriteInteger_MinLength(ShieldData_GetVersion_Major(), 1);
   LCD_Char_WriteChar('.');
   LCD_Char_WriteInteger_MinLength(ShieldData_GetVersion_Minor(), 1);

   /* Line 4 */
   LCD_Char_GotoXY(0, 3);
   LCD_Char_WriteString("RX Count:");
   LCD_Char_WriteInteger_MinLength(ShieldData_GetMessageCounter(), 5);
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
#define MAX_CURSOR_X_POS      (2)
static void UI_FFS_ShieldStatus( void )
{
   static uint8_t ucCursorX = 0;

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      if(!ucCursorX)
      {
         PrevScreen();
      }
      else
      {
         ucCursorX--;
      }
   }

   Print_Screen();
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/


