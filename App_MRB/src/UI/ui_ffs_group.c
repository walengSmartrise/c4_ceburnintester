/******************************************************************************
 *
 * @file     ui_ffs_doors.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Group_CarIndex( void );
static void UI_FFS_Group_LandingOffset( void );
static void UI_FFS_Group_DispatchTimeout( void );
static void UI_FFS_Group_DispatchOffline( void );
static void UI_FFS_Group_NumXRegCars( void );
static void UI_FFS_Group_XREG_DestOfflineTimer( void );
static void UI_FFS_Group_XREG_DestTimeout( void );

static void UI_FFS_Group_HallSecurityMask( void );
static void UI_FFS_Group_HallSecurityMap( void );
static void UI_FFS_Group_HallCallMask( void );
static void UI_FFS_Group_HallMedicalMask( void );
static void UI_FFS_Group_HallRearDoorMask( void );
static void UI_FFS_Group_SwingCallMask( void );
static void UI_FFS_Group_LinkedHallMask_1( void );
static void UI_FFS_Group_LinkedHallMask_2( void );
static void UI_FFS_Group_LinkedHallMask_3( void );
static void UI_FFS_Group_LinkedHallMask_4( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
//---------------------------------------------------------------------

static struct st_ui_screen__freeform gstFFS_Group_CarIndex =
{
   .pfnDraw = UI_FFS_Group_CarIndex,
};
static struct st_ui_screen__freeform gstFFS_Group_LandingOffset =
{
   .pfnDraw = UI_FFS_Group_LandingOffset,
};
static struct st_ui_screen__freeform gstFFS_Group_DispatchTimeout =
{
   .pfnDraw = UI_FFS_Group_DispatchTimeout,
};
static struct st_ui_screen__freeform gstFFS_Group_DispatchOffline =
{
   .pfnDraw = UI_FFS_Group_DispatchOffline,
};
static struct st_ui_screen__freeform gstFFS_Group_NumXRegCars =
{
   .pfnDraw = UI_FFS_Group_NumXRegCars,
};
static struct st_ui_screen__freeform gstFFS_Group_XREG_DestOfflineTimer =
{
   .pfnDraw = UI_FFS_Group_XREG_DestOfflineTimer,
};
static struct st_ui_screen__freeform gstFFS_Group_XREG_DestTimeout =
{
   .pfnDraw = UI_FFS_Group_XREG_DestTimeout,
};

static struct st_ui_screen__freeform gstFFS_Group_HallSecurityMask =
{
   .pfnDraw = UI_FFS_Group_HallSecurityMask,
};
static struct st_ui_screen__freeform gstFFS_Group_HallSecurityMap =
{
   .pfnDraw = UI_FFS_Group_HallSecurityMap,
};
static struct st_ui_screen__freeform gstFFS_Group_HallCallMask =
{
   .pfnDraw = UI_FFS_Group_HallCallMask,
};
static struct st_ui_screen__freeform gstFFS_Group_HallMedicalMask =
{
   .pfnDraw = UI_FFS_Group_HallMedicalMask,
};
static struct st_ui_screen__freeform gstFFS_Group_HallRearDoorMask =
{
   .pfnDraw = UI_FFS_Group_HallRearDoorMask,
};
static struct st_ui_screen__freeform gstFFS_Group_SwingCallMask =
{
   .pfnDraw = UI_FFS_Group_SwingCallMask,
};
static struct st_ui_screen__freeform gstFFS_Group_LinkedHallMask_1 =
{
   .pfnDraw = UI_FFS_Group_LinkedHallMask_1,
};
static struct st_ui_screen__freeform gstFFS_Group_LinkedHallMask_2 =
{
   .pfnDraw = UI_FFS_Group_LinkedHallMask_2,
};
static struct st_ui_screen__freeform gstFFS_Group_LinkedHallMask_3 =
{
   .pfnDraw = UI_FFS_Group_LinkedHallMask_3,
};
static struct st_ui_screen__freeform gstFFS_Group_LinkedHallMask_4 =
{
   .pfnDraw = UI_FFS_Group_LinkedHallMask_4,
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Group_CarIndex =
{
   .psTitle = "Group Car Index",
   .pstUGS_Next = &gstUGS_Group_CarIndex,
};
static struct st_ui_menu_item gstMI_Group_LandingOffset =
{
   .psTitle = "Group Landing Offset",
   .pstUGS_Next = &gstUGS_Group_LandingOffset,
};
static struct st_ui_menu_item gstMI_Group_DispatchTimeout =
{
   .psTitle = "Dispatch Timeout",
   .pstUGS_Next = &gstUGS_Group_DispatchTimeout,
};
static struct st_ui_menu_item gstMI_Group_DispatchOffline =
{
   .psTitle = "Dispatch Offline Timer",
   .pstUGS_Next = &gstUGS_Group_DispatchOffline,
};
static struct st_ui_menu_item gstMI_Group_NumXRegCars =
{
   .psTitle = "XReg Cars",
   .pstUGS_Next = &gstUGS_Group_NumXRegCars,
};
static struct st_ui_menu_item gstMI_Group_XREG_DestTimeout =
{
   .psTitle = "XReg Dest Timeout",
   .pstUGS_Next = &gstUGS_Group_XREG_DestTimeout,
};
static struct st_ui_menu_item gstMI_Group_XREG_DestOfflineTimer =
{
   .psTitle = "XReg Dest Offline Timer",
   .pstUGS_Next = &gstUGS_Group_XREG_DestOfflineTimer,
};
static struct st_ui_menu_item gstMI_Group_HallSecurityMask =
{
   .psTitle = "Hall Security Mask",
   .pstUGS_Next = &gstUGS_Group_HallSecurityMask,
};
static struct st_ui_menu_item gstMI_Group_HallSecurityMap =
{
   .psTitle = "Hall Security Map",
   .pstUGS_Next = &gstUGS_Group_HallSecurityMap,
};
static struct st_ui_menu_item gstMI_Group_HallCallMask =
{
   .psTitle = "Hall Call Mask",
   .pstUGS_Next = &gstUGS_Group_HallCallMask,
};
static struct st_ui_menu_item gstMI_Group_HallMedicalMask =
{
   .psTitle = "Hall Medical Mask",
   .pstUGS_Next = &gstUGS_Group_HallMedicalMask,
};
static struct st_ui_menu_item gstMI_Group_HallRearDoorMask =
{
   .psTitle = "Hall Rear Door Mask",
   .pstUGS_Next = &gstUGS_Group_HallRearDoorMask,
};
static struct st_ui_menu_item gstMI_Group_SwingCallMask =
{
   .psTitle = "Swing Call Mask",
   .pstUGS_Next = &gstUGS_Group_SwingCallMask,
};
static struct st_ui_menu_item gstMI_Group_LinkedHallMask_1 =
{
   .psTitle = "Linked Hall Mask 1",
   .pstUGS_Next = &gstUGS_Group_LinkedHallMask_1,
};
static struct st_ui_menu_item gstMI_Group_LinkedHallMask_2 =
{
   .psTitle = "Linked Hall Mask 2",
   .pstUGS_Next = &gstUGS_Group_LinkedHallMask_2,
};
static struct st_ui_menu_item gstMI_Group_LinkedHallMask_3 =
{
   .psTitle = "Linked Hall Mask 3",
   .pstUGS_Next = &gstUGS_Group_LinkedHallMask_3,
};
static struct st_ui_menu_item gstMI_Group_LinkedHallMask_4 =
{
   .psTitle = "Linked Hall Mask 4",
   .pstUGS_Next = &gstUGS_Group_LinkedHallMask_4,
};
static struct st_ui_menu_item * gastMenuItems_Group[] =
{
   &gstMI_Group_CarIndex,
   &gstMI_Group_LandingOffset,
   &gstMI_Group_DispatchTimeout,
   &gstMI_Group_DispatchOffline,
   &gstMI_Group_NumXRegCars,
   &gstMI_Group_XREG_DestTimeout,
   &gstMI_Group_XREG_DestOfflineTimer,
   &gstMI_Group_HallSecurityMask,
   &gstMI_Group_HallSecurityMap,
   &gstMI_Group_HallCallMask,
   &gstMI_Group_SwingCallMask,
   &gstMI_Group_HallMedicalMask,
   &gstMI_Group_HallRearDoorMask,
   &gstMI_Group_LinkedHallMask_1,
   &gstMI_Group_LinkedHallMask_2,
   &gstMI_Group_LinkedHallMask_3,
   &gstMI_Group_LinkedHallMask_4,
};
static struct st_ui_screen__menu gstMenu_Group =
{
   .psTitle = "Group Setup",
   .pastMenuItems = &gastMenuItems_Group,
   .ucNumItems = sizeof(gastMenuItems_Group) / sizeof(gastMenuItems_Group[ 0 ]),
};

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Group_CarIndex =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Group_CarIndex,
};
struct st_ui_generic_screen gstUGS_Group_LandingOffset =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Group_LandingOffset,
};
struct st_ui_generic_screen gstUGS_Group_DispatchTimeout =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Group_DispatchTimeout,
};
struct st_ui_generic_screen gstUGS_Group_DispatchOffline =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Group_DispatchOffline,
};
struct st_ui_generic_screen gstUGS_Group_NumXRegCars =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Group_NumXRegCars,
};
struct st_ui_generic_screen gstUGS_Group_XREG_DestTimeout =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Group_XREG_DestTimeout,
};
struct st_ui_generic_screen gstUGS_Group_XREG_DestOfflineTimer =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Group_XREG_DestOfflineTimer,
};
struct st_ui_generic_screen gstUGS_Group_HallSecurityMask =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Group_HallSecurityMask,
};
struct st_ui_generic_screen gstUGS_Group_HallSecurityMap =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Group_HallSecurityMap,
};

struct st_ui_generic_screen gstUGS_Group_HallCallMask =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Group_HallCallMask,
};
struct st_ui_generic_screen gstUGS_Group_HallMedicalMask =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Group_HallMedicalMask,
};
struct st_ui_generic_screen gstUGS_Group_SwingCallMask =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Group_SwingCallMask,
};
struct st_ui_generic_screen gstUGS_Group_HallRearDoorMask =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Group_HallRearDoorMask,
};
struct st_ui_generic_screen gstUGS_Group_LinkedHallMask_1 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Group_LinkedHallMask_1,
};
struct st_ui_generic_screen gstUGS_Group_LinkedHallMask_2 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Group_LinkedHallMask_2,
};
struct st_ui_generic_screen gstUGS_Group_LinkedHallMask_3 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Group_LinkedHallMask_3,
};
struct st_ui_generic_screen gstUGS_Group_LinkedHallMask_4 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Group_LinkedHallMask_4,
};

//-----------------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_Setup_Group =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Group,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Group_CarIndex( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__GroupCarIndex;
   stParamEdit.uwParamIndex_End = enPARAM8__GroupCarIndex;
   stParamEdit.ulValue_Max = 7;
   stParamEdit.psTitle = "Group Car Index";
   stParamEdit.psUnit = "";
   stParamEdit.ucValueOffset = 1;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Group_LandingOffset( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__GroupLandingOffset;
   stParamEdit.uwParamIndex_End = enPARAM8__GroupLandingOffset;
   stParamEdit.psTitle = "Group Landing Offset";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Group_DispatchTimeout( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__DispatchTimeout_1s;
   stParamEdit.uwParamIndex_End = enPARAM8__DispatchTimeout_1s;
   stParamEdit.psTitle = "Dispatching Timeout";
   stParamEdit.psUnit = " sec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Group_DispatchOffline( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__DispatchOffline_1s;
   stParamEdit.uwParamIndex_End = enPARAM8__DispatchOffline_1s;
   stParamEdit.psTitle = "Dispatching Offline Timer";
   stParamEdit.psUnit = " sec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Group_NumXRegCars( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__NumXRegCars;
   stParamEdit.uwParamIndex_End = enPARAM8__NumXRegCars;
   stParamEdit.psTitle = "Number of XReg Cars";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Group_XREG_DestTimeout( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__XREG_DestinationTimeout_10sec;
   stParamEdit.uwParamIndex_End = enPARAM8__XREG_DestinationTimeout_10sec;
   stParamEdit.psTitle = "XREG Dest. Timeout";
   stParamEdit.psUnit = "0 sec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Group_XREG_DestOfflineTimer( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__XREG_DestinationOffline_10sec;
   stParamEdit.uwParamIndex_End = enPARAM8__XREG_DestinationOffline_10sec;
   stParamEdit.psTitle = "XREG Dest. Offline Timer";
   stParamEdit.psUnit = "0 sec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}

/*-----------------------------------------------------------------------------
   Sets which ranges of hall board addresses have hall security
 -----------------------------------------------------------------------------*/
static void UI_FFS_Group_HallSecurityMask( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__HallSecurityMask;
   stParamEdit.uwParamIndex_End = enPARAM8__HallSecurityMask;
   stParamEdit.psTitle = "Hall Security Mask";
   stParamEdit.ucFieldSize_Value = 1;
   stParamEdit.ucFieldSize_Index = 2;
   UI_ParamEditScreenTemplate_EditByBit_PerHallMaskFunction(&stParamEdit);
}

/*-----------------------------------------------------------------------------
   Sets which landings have hall security
 -----------------------------------------------------------------------------*/
static void UI_FFS_Group_HallSecurityMap( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__HallSecureMap_0;
   stParamEdit.uwParamIndex_End = enPARAM16__HallSecureMap_5;
   stParamEdit.psTitle = "Hall Secure Landings";
   stParamEdit.ucFieldSize_Value = 1;
   stParamEdit.ucFieldSize_Index = 2;
   UI_ParamEditScreenTemplate_EditByBit_PerLanding(&stParamEdit);
}

/*-----------------------------------------------------------------------------
   Sets which ranges of hall board addresses are seen as regular hall calls
 -----------------------------------------------------------------------------*/
static void UI_FFS_Group_HallCallMask( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__HallCallMask;
   stParamEdit.uwParamIndex_End = enPARAM8__HallCallMask;
   stParamEdit.psTitle = "Hall Call Mask";
   stParamEdit.ucFieldSize_Value = 1;
   stParamEdit.ucFieldSize_Index = 2;
   UI_ParamEditScreenTemplate_EditByBit_PerHallMaskFunction(&stParamEdit);
}
/*-----------------------------------------------------------------------------
   Sets which ranges of hall board addresses are seen as medical hall calls
 -----------------------------------------------------------------------------*/
static void UI_FFS_Group_HallMedicalMask( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__HallMedicalMask;
   stParamEdit.uwParamIndex_End = enPARAM8__HallMedicalMask;
   stParamEdit.psTitle = "Hall Medical Mask";
   stParamEdit.ucFieldSize_Value = 1;
   stParamEdit.ucFieldSize_Index = 2;
   UI_ParamEditScreenTemplate_EditByBit_PerHallMaskFunction(&stParamEdit);
}
/*-----------------------------------------------------------------------------
   Sets which ranges of hall board addresses are seen as swing hall calls
 -----------------------------------------------------------------------------*/
static void UI_FFS_Group_SwingCallMask( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__SwingCallMask;
   stParamEdit.uwParamIndex_End = enPARAM8__SwingCallMask;
   stParamEdit.psTitle = "Hall Swing Mask";
   stParamEdit.ucFieldSize_Value = 1;
   stParamEdit.ucFieldSize_Index = 2;
   UI_ParamEditScreenTemplate_EditByBit_PerHallMaskFunction(&stParamEdit);
}
/*-----------------------------------------------------------------------------
   Sets which ranges of hall board addresses are seen as rear hall calls
 -----------------------------------------------------------------------------*/
static void UI_FFS_Group_HallRearDoorMask( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__HallRearDoorMask;
   stParamEdit.uwParamIndex_End = enPARAM8__HallRearDoorMask;
   stParamEdit.psTitle = "Hall Rear Door Mask";
   stParamEdit.ucFieldSize_Value = 1;
   stParamEdit.ucFieldSize_Index = 2;
   UI_ParamEditScreenTemplate_EditByBit_PerHallMaskFunction(&stParamEdit);
}
/*-----------------------------------------------------------------------------
   Sets which hall board functions will light up together
 -----------------------------------------------------------------------------*/
static void UI_FFS_Group_LinkedHallMask_1( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__LinkedHallMask_1;
   stParamEdit.uwParamIndex_End = enPARAM8__LinkedHallMask_1;
   stParamEdit.psTitle = "Linked Hall Mask 1";
   stParamEdit.ucFieldSize_Value = 1;
   stParamEdit.ucFieldSize_Index = 2;
   UI_ParamEditScreenTemplate_EditByBit_PerHallMaskFunction(&stParamEdit);
}
/*-----------------------------------------------------------------------------
   Sets which hall board functions will light up together
 -----------------------------------------------------------------------------*/
static void UI_FFS_Group_LinkedHallMask_2( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__LinkedHallMask_2;
   stParamEdit.uwParamIndex_End = enPARAM8__LinkedHallMask_2;
   stParamEdit.psTitle = "Linked Hall Mask 2";
   stParamEdit.ucFieldSize_Value = 1;
   stParamEdit.ucFieldSize_Index = 2;
   UI_ParamEditScreenTemplate_EditByBit_PerHallMaskFunction(&stParamEdit);
}
/*-----------------------------------------------------------------------------
   Sets which hall board functions will light up together
 -----------------------------------------------------------------------------*/
static void UI_FFS_Group_LinkedHallMask_3( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__LinkedHallMask_3;
   stParamEdit.uwParamIndex_End = enPARAM8__LinkedHallMask_3;
   stParamEdit.psTitle = "Linked Hall Mask 3";
   stParamEdit.ucFieldSize_Value = 1;
   stParamEdit.ucFieldSize_Index = 2;
   UI_ParamEditScreenTemplate_EditByBit_PerHallMaskFunction(&stParamEdit);
}
/*-----------------------------------------------------------------------------
   Sets which hall board functions will light up together
 -----------------------------------------------------------------------------*/
static void UI_FFS_Group_LinkedHallMask_4( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__LinkedHallMask_4;
   stParamEdit.uwParamIndex_End = enPARAM8__LinkedHallMask_4;
   stParamEdit.psTitle = "Linked Hall Mask 4";
   stParamEdit.ucFieldSize_Value = 1;
   stParamEdit.ucFieldSize_Index = 2;
   UI_ParamEditScreenTemplate_EditByBit_PerHallMaskFunction(&stParamEdit);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
