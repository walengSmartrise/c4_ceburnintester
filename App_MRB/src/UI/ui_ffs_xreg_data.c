
/******************************************************************************
 *
 * @file     ui_ffs_xreg_data.c
 * @brief    XReg Data Page
 * @version  V1.00
 * @date     15, Oct 2018
 *
 * @note     Page for viewing the data from cross registration (XREG) cars
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "XRegData.h"
#include "XRegDestination.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_XRegData( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_XRegData =
{
   .pfnDraw = UI_FFS_XRegData,
};
//---------------------------------------------------------------
static char const* const gpasAutoMode[NUM_MODE_AUTO] =
{
      "UNK",//MODE_A__UNKNOWN,
      "NON",//MODE_A__NONE,
      "NORM",//MODE_A__NORMAL,
      "FIR1",//MODE_A__FIRE1,
      "FIR2",//MODE_A__FIRE2,
      "EMS1",//MODE_A__EMS1,
      "EMS2",//MODE_A__EMS2,
      "ATTD",//MODE_A__ATTENDANT,
      "INDP",//MODE_A__INDP_SRV,
      "SEIS",//MODE_A__SEISMC,
      "CWDR",//MODE_A__CW_DRAIL,
      "SABB",//MODE_A__SABBATH,
      "EPWR",//MODE_A__EPOWER,
      "EVAC",//MODE_A__EVAC,
      "OOS",//MODE_A__OOS,
      "BATL",//MODE_A__BATT_LOW,
      "BATR",//MODE_A__BATT_RESQ,
      "PRS1",//MODE_A__PRSN_TRSPT1,
      "PRS2",//MODE_A__PRSN_TRSPT2,
      "INV",//MODE_A__UNUSED19,
      "WG",//MODE_A__WANDERGUARD,
      "HUGS",//MODE_A__HUGS,
      "CSW",//MODE_A__CAR_SW,
      "TEST",//MODE_A__TEST,
      "WIND",//MODE_A__WIND_OP,
      "FLD",//MODE_A__FLOOD_OP,
      "SWING",//MODE_A__SWING,
      "CUST",//MODE_A__CUSTOM,
      "MARS",//MODE_A__MARSHAL_MODE
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_XRegData =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_XRegData,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   CD_PAGE__STATUS,
   CD_PAGE__HALLMASK,
   CD_PAGE__OPENING_F,
   CD_PAGE__OPENING_R,

   NUM_CD_PAGE
}en_cardata_page;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
static void PrintScreen_Status( enum en_group_net_nodes eCarID )
{
   enum en_classop eClassOp = XRegData_GetClassOp(eCarID);
   enum en_mode_auto eAutoMode = XRegData_GetAutoMode(eCarID);

   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   if(eClassOp == CLASSOP__UNKNOWN)
   {
      LCD_Char_WriteString("U-UNK");
   }
   else if(eClassOp == CLASSOP__MANUAL)
   {
      LCD_Char_WriteString("M-UNK");
   }
   else if(eClassOp == CLASSOP__SEMI_AUTO)
   {
      LCD_Char_WriteString("L-UNK");
   }
   else if(eClassOp == CLASSOP__AUTO)
   {
      LCD_Char_WriteString("A-");
      LCD_Char_WriteString(gpasAutoMode[eAutoMode]);
   }

   LCD_Char_WriteString(" - ");
   if(XRegData_GetCarActive(eCarID))
   {
      LCD_Char_WriteString("IN");
   }
   else
   {
      LCD_Char_WriteString("OUT");
   }
   LCD_Char_WriteString(" GRP");

   /* L3 */
   LCD_Char_GotoXY( 0, 2 );

   LCD_Char_WriteString("C-");
   if( XRegData_GetCurrentLanding(eCarID) == INVALID_FLOOR )
   {
      LCD_Char_WriteString("NA");
   }
   else
   {
      LCD_Char_WriteInteger_ExactLength(XRegData_GetCurrentLanding(eCarID)+1, 2);
   }

   LCD_Char_WriteString(" D-");
   if( XRegDestination_GetLanding(eCarID) == INVALID_FLOOR )
   {
      LCD_Char_WriteString("NA");
   }
   else
   {
      LCD_Char_WriteInteger_ExactLength(XRegDestination_GetLanding(eCarID)+1, 2);
   }

   LCD_Char_WriteString(" M-");
   if( XRegData_GetMotionDirection(eCarID) == DIR__UP )
   {
      LCD_Char_WriteString("UP");
   }
   else if( XRegData_GetMotionDirection(eCarID) == DIR__DN )
   {
      LCD_Char_WriteString("DN");
   }
   else
   {
      LCD_Char_WriteString("ST");
   }

   /* L4 */
   LCD_Char_GotoXY( 0, 3 );
   if( XRegData_GetGSW_F(eCarID) )
   {
      LCD_Char_WriteString("[ | ]");
   }
   else
   {
      LCD_Char_WriteString("[   ]");
   }
   LCD_Char_AdvanceX(1);
   if( XRegData_GetGSW_R(eCarID) )
   {
      LCD_Char_WriteString("[ | ]");
   }
   else
   {
      LCD_Char_WriteString("[   ]");
   }

   LCD_Char_GotoXY( 15, 3 );
   LCD_Char_WriteString("P-");
   if(XRegData_GetPriority(eCarID) == HC_DIR__DOWN)
   {
      LCD_Char_WriteString("DN");
   }
   else
   {
      LCD_Char_WriteString("UP");
   }
}
static void PrintScreen_HallMask( enum en_group_net_nodes eCarID )
{
   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("HMF: 0x");
   LCD_Char_WriteHex_ExactLength(XRegData_GetHallMask_F(eCarID), 8);

   /* L3 */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("HMR: 0x");
   LCD_Char_WriteHex_ExactLength(XRegData_GetHallMask_R(eCarID), 8);
}
static void PrintScreen_OpeningF( enum en_group_net_nodes eCarID )
{
   uint32_t uiBF_OpeningMap = ( XRegData_GetOpeningMap_F(eCarID, 0) )
                            | ( XRegData_GetOpeningMap_F(eCarID, 1) << 8 )
                            | ( XRegData_GetOpeningMap_F(eCarID, 2) << 16 )
                            | ( XRegData_GetOpeningMap_F(eCarID, 3) << 24 );
   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("OMF1: 0x");
   LCD_Char_WriteHex_ExactLength(uiBF_OpeningMap, 8);

   uiBF_OpeningMap = ( XRegData_GetOpeningMap_F(eCarID, 4) )
                   | ( XRegData_GetOpeningMap_F(eCarID, 5) << 8 )
                   | ( XRegData_GetOpeningMap_F(eCarID, 6) << 16 )
                   | ( XRegData_GetOpeningMap_F(eCarID, 7) << 24 );
   /* L3 */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("OMF2: 0x");
   LCD_Char_WriteHex_ExactLength(uiBF_OpeningMap, 8);

   uiBF_OpeningMap = ( XRegData_GetOpeningMap_F(eCarID, 8) )
                   | ( XRegData_GetOpeningMap_F(eCarID, 9) << 8 )
                   | ( XRegData_GetOpeningMap_F(eCarID, 10) << 16 )
                   | ( XRegData_GetOpeningMap_F(eCarID, 11) << 24 );
   /* L4 */
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("OMF3: 0x");
   LCD_Char_WriteHex_ExactLength(uiBF_OpeningMap, 8);
}
static void PrintScreen_OpeningR( enum en_group_net_nodes eCarID )
{
   uint32_t uiBF_OpeningMap = ( XRegData_GetOpeningMap_R(eCarID, 0) )
                            | ( XRegData_GetOpeningMap_R(eCarID, 1) << 8 )
                            | ( XRegData_GetOpeningMap_R(eCarID, 2) << 16 )
                            | ( XRegData_GetOpeningMap_R(eCarID, 3) << 24 );
   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("OMR1: 0x");
   LCD_Char_WriteHex_ExactLength(uiBF_OpeningMap, 8);

   uiBF_OpeningMap = ( XRegData_GetOpeningMap_R(eCarID, 4) )
                   | ( XRegData_GetOpeningMap_R(eCarID, 5) << 8 )
                   | ( XRegData_GetOpeningMap_R(eCarID, 6) << 16 )
                   | ( XRegData_GetOpeningMap_R(eCarID, 7) << 24 );
   /* L3 */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("OMR2: 0x");
   LCD_Char_WriteHex_ExactLength(uiBF_OpeningMap, 8);

   uiBF_OpeningMap = ( XRegData_GetOpeningMap_R(eCarID, 8) )
                   | ( XRegData_GetOpeningMap_R(eCarID, 9) << 8 )
                   | ( XRegData_GetOpeningMap_R(eCarID, 10) << 16 )
                   | ( XRegData_GetOpeningMap_R(eCarID, 11) << 24 );
   /* L4 */
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("OMR3: 0x");
   LCD_Char_WriteHex_ExactLength(uiBF_OpeningMap, 8);
}

/*----------------------------------------------------------------------------
|01234567890123456789| |01234567890123456789| |01234567890123456789| |01234567890123456789|

|CAR1 - ON          >| |CAR1 - ON         <>| |CAR1 - ON         <>| |CAR1 - ON         < |
|A-Normal - IN GRP   | |HMF: 0x00000001     | |OMF1: 0x00000001    | |OMR1: 0x00000001    |
|C-10 D-12      M-UP | |HMR: 0x00000002     | |OMF2: 0x00000001    | |OMR2: 0x00000001    |
|               P-DN | |HML: 0x00000003     | |OMF3: 0x00000001    | |OMR3: 0x00000001    |

 ----------------------------------------------------------------------------*/
static void Print_Screen( uint8_t ucCursorX, uint8_t ucCursorY )
{
   LCD_Char_Clear();

   /* Print Row 1 */
   LCD_Char_GotoXY( 0, 0 );

   LCD_Char_WriteString("CAR");
   LCD_Char_WriteInteger_ExactLength(ucCursorY+1, 1);

   if( XRegData_GetCarOnlineFlag(ucCursorY) )
   {
      LCD_Char_WriteString(" - ON ");
   }
   else
   {
      LCD_Char_WriteString(" - OFF ");
   }

   if( ucCursorX )
   {
      LCD_Char_GotoXY( 18, 0 );
      LCD_Char_WriteString("<");
   }
   if( ucCursorX < (NUM_CD_PAGE-1) )
   {
      LCD_Char_GotoXY( 19, 0 );
      LCD_Char_WriteString(">");
   }

   /* Print Rows 2-4 */
   switch(ucCursorX)
   {
      case CD_PAGE__STATUS:
         PrintScreen_Status(ucCursorY);
         break;
      case CD_PAGE__HALLMASK:
         PrintScreen_HallMask(ucCursorY);
         break;
      case CD_PAGE__OPENING_F:
         PrintScreen_OpeningF(ucCursorY);
         break;
      case CD_PAGE__OPENING_R:
         PrintScreen_OpeningR(ucCursorY);
         break;
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_XRegData( void )
{
   static int8_t cCursorX, cCursorY;

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      if(!cCursorX)
      {
         PrevScreen();
      }
      else
      {
         cCursorX--;
      }
   }
   else if ( enKeypress == enKEYPRESS_RIGHT )
   {
      cCursorX++;
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      cCursorY--;
   }
   else if ( enKeypress == enKEYPRESS_DOWN )
   {
      cCursorY++;
   }

   /* Bound values */
   if( cCursorY >= MAX_GROUP_CARS )
   {
      cCursorY = MAX_GROUP_CARS-1;
   }
   else if( cCursorY < 0 )
   {
      cCursorY = 0;
   }
   if( cCursorX >= NUM_CD_PAGE )
   {
      cCursorX = NUM_CD_PAGE-1;
   }
   else if( cCursorX < 0 )
   {
      cCursorX = 0;
   }

   Print_Screen( cCursorX, cCursorY );
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/


