/******************************************************************************
 *
 * @file     ui_menu_io.c
 * @brief    I/O Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "GlobalData.h"
#include "carData.h"
#include "carDestination.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_HallCallStatus_Up( void );
static void UI_FFS_HallCallStatus_Down( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_HallCallStatus_Up =
{
   .pfnDraw = UI_FFS_HallCallStatus_Up,
};
static struct st_ui_screen__freeform gstFFS_HallCallStatus_Down =
{
   .pfnDraw = UI_FFS_HallCallStatus_Down,
};
//---------------------------------------
static struct st_ui_menu_item gstMI_HallCallStatus_Up =
{
   .psTitle = "Up Calls",
   .pstUGS_Next = &gstUGS_HallCallStatus_Up,
};
static struct st_ui_menu_item gstMI_HallCallStatus_Down =
{
   .psTitle = "Down Calls",
   .pstUGS_Next = &gstUGS_HallCallStatus_Down,
};
static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_HallCallStatus_Up,
   &gstMI_HallCallStatus_Down,
};
static struct st_ui_screen__menu gstMenu =
{
   .psTitle = "Hall Call Status",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};
//---------------------------------------
static en_hc_dir eHallCallDir;
static uint8_t ucCarIndex;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_HallCallStatus_Up =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_HallCallStatus_Up,
};
struct st_ui_generic_screen gstUGS_HallCallStatus_Down =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_HallCallStatus_Down,
};
struct st_ui_generic_screen gstUGS_Menu_HallCallStatus =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

#define MAX_DISPLAYED_HCS             (15)
#define DISPLAYED_HCS_PER_LINE        (5)
#define SPACING_X_PER_HC              (4)
#define HC_START_POS_Y                (1)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void Print_Screen(void)
{
   LCD_Char_Clear();

   /* L1 Title */
   LCD_Char_GotoXY( 0, 0 );
   if( eHallCallDir == HC_DIR__UP )
   {
      LCD_Char_WriteString("UP CALLS");
   }
   else
   {
      LCD_Char_WriteString("DOWN CALLS");
   }
   LCD_Char_WriteString(" - CAR");
   LCD_Char_WriteInteger_ExactLength(ucCarIndex+1, 1);


   /* L2-4 Hall Calls */
   uint8_t ucNumHallCalls = 0;
   uint8_t ucLastLanding = CarData_GetHighestActiveLanding();
   for(uint8_t ucLanding = 0; ucLanding < ucLastLanding; ucLanding++)
   {
      if(eHallCallDir == HC_DIR__UP)
      {
         if( GetLatchedHallCallUp_Front_ByCar(ucCarIndex, ucLanding) )
         {
            uint8_t ucPosX = ( ucNumHallCalls % DISPLAYED_HCS_PER_LINE ) * SPACING_X_PER_HC;
            uint8_t ucPosY = ( ucNumHallCalls / DISPLAYED_HCS_PER_LINE ) + HC_START_POS_Y;
            LCD_Char_GotoXY( ucPosX, ucPosY );
            LCD_Char_WriteInteger_ExactLength(ucLanding+1, 2);
            LCD_Char_WriteChar('F');
            ucNumHallCalls++;
         }
         if( GetLatchedHallCallUp_Rear_ByCar(ucCarIndex, ucLanding) )
         {
            uint8_t ucPosX = ( ucNumHallCalls % DISPLAYED_HCS_PER_LINE ) * SPACING_X_PER_HC;
            uint8_t ucPosY = ( ucNumHallCalls / DISPLAYED_HCS_PER_LINE ) + HC_START_POS_Y;
            LCD_Char_GotoXY( ucPosX, ucPosY );
            LCD_Char_WriteInteger_ExactLength(ucLanding+1, 2);
            LCD_Char_WriteChar('R');
            ucNumHallCalls++;
         }
      }
      else
      {
         if( GetLatchedHallCallDown_Front_ByCar(ucCarIndex, ucLanding) )
         {
            uint8_t ucPosX = ( ucNumHallCalls % DISPLAYED_HCS_PER_LINE ) * SPACING_X_PER_HC;
            uint8_t ucPosY = ( ucNumHallCalls / DISPLAYED_HCS_PER_LINE ) + HC_START_POS_Y;
            LCD_Char_GotoXY( ucPosX, ucPosY );
            LCD_Char_WriteInteger_ExactLength(ucLanding+1, 2);
            LCD_Char_WriteChar('F');
            ucNumHallCalls++;
         }
         if( GetLatchedHallCallDown_Rear_ByCar(ucCarIndex, ucLanding) )
         {
            uint8_t ucPosX = ( ucNumHallCalls % DISPLAYED_HCS_PER_LINE ) * SPACING_X_PER_HC;
            uint8_t ucPosY = ( ucNumHallCalls / DISPLAYED_HCS_PER_LINE ) + HC_START_POS_Y;
            LCD_Char_GotoXY( ucPosX, ucPosY );
            LCD_Char_WriteInteger_ExactLength(ucLanding+1, 2);
            LCD_Char_WriteChar('R');
            ucNumHallCalls++;
         }
      }
      if(ucNumHallCalls >= MAX_DISPLAYED_HCS)
      {
         break;
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_HallCallStatus_Up( void )
{
   eHallCallDir = HC_DIR__UP;
   int8_t cScrollY = ucCarIndex;
   enum en_keypresses enKeypress = Button_GetKeypress();
   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      cScrollY++;
   }
   else if ( enKeypress == enKEYPRESS_DOWN )
   {
      cScrollY--;
   }
   if( cScrollY >= MAX_GROUP_CARS )
   {
      cScrollY = MAX_GROUP_CARS-1;
   }
   else if( cScrollY < 0 )
   {
      cScrollY = 0;
   }

   ucCarIndex = cScrollY;

   Print_Screen();
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_HallCallStatus_Down( void )
{
   eHallCallDir = HC_DIR__DOWN;
   int8_t cScrollY = ucCarIndex;
   enum en_keypresses enKeypress = Button_GetKeypress();
   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      cScrollY++;
   }
   else if ( enKeypress == enKEYPRESS_DOWN )
   {
      cScrollY--;
   }
   if( cScrollY >= MAX_GROUP_CARS )
   {
      cScrollY = MAX_GROUP_CARS-1;
   }
   else if( cScrollY < 0 )
   {
      cScrollY = 0;
   }

   ucCarIndex = cScrollY;

   Print_Screen();
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
