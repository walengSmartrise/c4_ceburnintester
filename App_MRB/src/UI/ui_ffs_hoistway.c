/******************************************************************************
 *
 * @file     ui_ffs_hoistway.c
 * @brief    Hoistway access UI pages
 * @version  V1.00
 * @date     13, Feb 2018
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_HoistWay_Distance_Top( void );
static void UI_FFS_HoistWay_Distance_Bottom( void );
static void UI_FFS_Misc_HoistWay_TopFloor( void );
static void UI_FFS_Misc_HoistWay_BottomFloor( void );
static void UI_FFS_HoistWay_TopOpening(void);
static void UI_FFS_HoistWay_BottomOpening(void);
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_HoistWay_Distance_Top =
{
   .pfnDraw = UI_FFS_HoistWay_Distance_Top,
};
static struct st_ui_screen__freeform gstFFS_HoistWay_Distance_Bottom =
{
   .pfnDraw = UI_FFS_HoistWay_Distance_Bottom,
};
static struct st_ui_screen__freeform gstFFS_HoistWay_TopFloor =
{
   .pfnDraw = UI_FFS_Misc_HoistWay_TopFloor,
};
static struct st_ui_screen__freeform gstFFS_HoistWay_BottomFloor =
{
   .pfnDraw = UI_FFS_Misc_HoistWay_BottomFloor,
};
static struct st_ui_screen__freeform gstFFS_HoistWay_TopOpening =
{
   .pfnDraw = UI_FFS_HoistWay_TopOpening ,
};
static struct st_ui_screen__freeform gstFFS_HoistWay_BottomOpening =
{
   .pfnDraw = UI_FFS_HoistWay_BottomOpening,
};
//---------------------------------------------------------------

static struct st_ui_menu_item gstMI_Hoistway_Distance_Top =
{
   .psTitle = "Allowed Dist. Top",
   .pstUGS_Next = &gstUGS_HoistWay_Distance_Top,
};
static struct st_ui_menu_item gstMI_Hoistway_Distance_Bottom =
{
   .psTitle = "Allowed Dist. Bot",
   .pstUGS_Next = &gstUGS_HoistWay_Distance_Bottom,
};
static struct st_ui_menu_item gstMI_Hoistway_TopFloor =
{
   .psTitle = "Top Floor",
   .pstUGS_Next = &gstUGS_HoistWay_TopFloor,
};
static struct st_ui_menu_item gstMI_Hoistway_BottomFloor =
{
   .psTitle = "Bottom Floor",
   .pstUGS_Next = &gstUGS_HoistWay_BottomFloor,
};
static struct st_ui_menu_item gstMI_Hoistway_TopOpening =
{
   .psTitle = "Top Opening",
   .pstUGS_Next = &gstUGS_HoistWay_TopOpening,
};
static struct st_ui_menu_item gstMI_Hoistway_BottomOpening =
{
   .psTitle = "Bottom Opening",
   .pstUGS_Next = &gstUGS_HoistWay_BottomOpening,
};

static struct st_ui_menu_item * gastMenuItems_Hoistway[] =
{
   &gstMI_Hoistway_Distance_Top,
   &gstMI_Hoistway_Distance_Bottom,
   &gstMI_Hoistway_TopFloor,
   &gstMI_Hoistway_BottomFloor,
   &gstMI_Hoistway_TopOpening,
   &gstMI_Hoistway_BottomOpening,

};
static struct st_ui_screen__menu gstMenu_Setup_Hoistway =
{
   .psTitle = "Hoistway Access",
   .pastMenuItems = &gastMenuItems_Hoistway,
   .ucNumItems = sizeof(gastMenuItems_Hoistway) / sizeof(gastMenuItems_Hoistway[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_HoistWay_Distance_Top =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_HoistWay_Distance_Top,
};
struct st_ui_generic_screen gstUGS_HoistWay_Distance_Bottom =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_HoistWay_Distance_Bottom,
};
struct st_ui_generic_screen gstUGS_HoistWay_TopFloor =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_HoistWay_TopFloor,
};
struct st_ui_generic_screen gstUGS_HoistWay_BottomFloor =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_HoistWay_BottomFloor,
};
struct st_ui_generic_screen gstUGS_HoistWay_TopOpening =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_HoistWay_TopOpening,
};
struct st_ui_generic_screen gstUGS_HoistWay_BottomOpening =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_HoistWay_BottomOpening,
};
//---------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_Setup_Hoistway =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Hoistway,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_HoistWay_Distance_Top( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__HA_AllowedDist_Top_FT;
   stParamEdit.uwParamIndex_End = enPARAM8__HA_AllowedDist_Top_FT;
   stParamEdit.psTitle = "Allowed Dist. Top";
   stParamEdit.psUnit = " ft.";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_HoistWay_Distance_Bottom( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__HA_AllowedDist_Bot_FT;
   stParamEdit.uwParamIndex_End = enPARAM8__HA_AllowedDist_Bot_FT;
   stParamEdit.psTitle = "Allowed Dist. Bot";
   stParamEdit.psUnit = " ft.";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_HoistWay_TopFloor( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__HA_TopFloor;
   stParamEdit.uwParamIndex_End = enPARAM8__HA_TopFloor;
   stParamEdit.psTitle = "Top Floor";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_HoistWay_BottomFloor( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__HA_BottomFloor;
   stParamEdit.uwParamIndex_End = enPARAM8__HA_BottomFloor;
   stParamEdit.psTitle = "Bottom Floor";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static char *(DoorArray[]) =
{
      "Front",
      "Rear",
};
static void UI_FFS_HoistWay_TopOpening( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__HA_TopOpening;
   stParamEdit.uwParamIndex_End = enPARAM8__HA_TopOpening;
   stParamEdit.psTitle = "Top Opening";
   // stParamEdit.psUnit = "";
   stParamEdit.ucaPTRChoiceList = &DoorArray;
   stParamEdit.ulValue_Max = (sizeof(DoorArray)/sizeof(*DoorArray)) - 1;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_HoistWay_BottomOpening( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__HA_BottomOpening;
   stParamEdit.uwParamIndex_End = enPARAM8__HA_BottomOpening;
   stParamEdit.psTitle = "Bottom Opening";
//   stParamEdit.psUnit = "";
   stParamEdit.ucaPTRChoiceList = &DoorArray;
   stParamEdit.ulValue_Max = (sizeof(DoorArray)/sizeof(*DoorArray)) - 1;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
