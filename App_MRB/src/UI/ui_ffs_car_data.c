
/******************************************************************************
 *
 * @file     ui_menu_io.c
 * @brief    I/O Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "carData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_CarData( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_CarData =
{
   .pfnDraw = UI_FFS_CarData,
};
//---------------------------------------------------------------
static char const* const gpasAutoMode[NUM_MODE_AUTO] =
{
      "UNK",//MODE_A__UNKNOWN,
      "NON",//MODE_A__NONE,
      "NORM",//MODE_A__NORMAL,
      "FIR1",//MODE_A__FIRE1,
      "FIR2",//MODE_A__FIRE2,
      "EMS1",//MODE_A__EMS1,
      "EMS2",//MODE_A__EMS2,
      "ATTD",//MODE_A__ATTENDANT,
      "INDP",//MODE_A__INDP_SRV,
      "SEIS",//MODE_A__SEISMC,
      "CWDR",//MODE_A__CW_DRAIL,
      "SABB",//MODE_A__SABBATH,
      "EPWR",//MODE_A__EPOWER,
      "EVAC",//MODE_A__EVAC,
      "OOS",//MODE_A__OOS,
      "BATL",//MODE_A__BATT_LOW,
      "BATR",//MODE_A__BATT_RESQ,
      "PRS1",//MODE_A__PRSN_TRSPT1,
      "PRS2",//MODE_A__PRSN_TRSPT2,
      "INV",//MODE_A__UNUSED19,
      "WG",//MODE_A__WANDERGUARD,
      "HUGS",//MODE_A__HUGS,
      "CSW",//MODE_A__CAR_SW,
      "TEST",//MODE_A__TEST,
      "WIND",//MODE_A__WIND_OP,
      "FLD",//MODE_A__FLOOD_OP,
      "SWING",//MODE_A__SWING,
      "CUST",//MODE_A__CUSTOM,
      "SHOO",//MODE_A__ACTIVE_SHOOTER_MODE
      "MARS",//MODE_A__MARSHAL_MODE
      "VIP",//MODE_A__VIP_MODE
};
static char const* const gpasLearnMode[NUM_MODE_LEARN] =
{
      "UNK",//MODE_L__UNKNOWN,
      "INV",//MODE_L__INVALID,
      "NON",//MODE_L__NONE,
      "G2T",//MODE_L__GOTO_TERM_HA,
      "RB1",//MODE_L__READY_BRK,
      "RB2",//MODE_L__READY_BRK2,
      "BHA",//MODE_L__BYP_TERM_LIMIT_HA,
      "RHA",//MODE_L__READY_HA,
      "LB1",//MODE_L__LEARNING_BRK,
      "LB2",//MODE_L__LEARNING_BRK2,
      "LHU",//MODE_L__LEARNING_UHA,
      "LHD",//MODE_L__LEARNING_DHA,
      "EBP",//MODE_L__ENABLE_BPS,
      "INV",//MODE_L__UNUSED_LEARNING_DNTS,
      "CMP",//MODE_L__COMPLETE,
};
static char const* const gpasManualMode[NUM_MODE_MANUAL] =
{
      "UNK",//MODE_M__UNKNOWN,
      "INV",//MODE_M__INVALID,
      "NON",//MODE_M__NONE,
      "CT",//MODE_M__INSP_CT,
      "IC",//MODE_M__INSP_IC,
      "HA",//MODE_M__INSP_HA,
      "MR",//MODE_M__INSP_MR,
      "PIT",//MODE_M__INSP_PIT,
      "LND",//MODE_M__INSP_LND,
      "CON",//MODE_M__CONSTRUCTION,
      "HAT",//MODE_M__INSP_HA_TOP,
      "HAB",//MODE_M__INSP_HA_BOTTOM,
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_CarData =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_CarData,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   CD_PAGE__STATUS,
   CD_PAGE__HALLMASK,
   CD_PAGE__OPENING_F,
   CD_PAGE__OPENING_R,
   CD_PAGE__SECURITY_F,
   CD_PAGE__SECURITY_R,
   CD_PAGE__LINKEDMASK,
   CD_PAGE__HALL_SECURITY_MAP,
   CD_PAGE__HALL_SECURITY_MASK,
   CD_PAGE__DD_TIMES,
   CD_PAGE__VIPFlags,
   CD_PAGE__VIPMasks,

   NUM_CD_PAGE
}en_cardata_page;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
static void PrintScreen_Status( enum en_group_net_nodes eCarID )
{
   st_cardata *pstCarData = GetCarDataStructure(eCarID);

   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   if(pstCarData->eClassOp == CLASSOP__UNKNOWN)
   {
      LCD_Char_WriteString("U-UNK");
   }
   else if(pstCarData->eClassOp == CLASSOP__MANUAL)
   {
      LCD_Char_WriteString("M-");
      LCD_Char_WriteString(gpasManualMode[pstCarData->eManualMode]);
   }
   else if(pstCarData->eClassOp == CLASSOP__SEMI_AUTO)
   {
      LCD_Char_WriteString("L-");
      LCD_Char_WriteString(gpasLearnMode[pstCarData->eLearnMode]);
   }
   else if(pstCarData->eClassOp == CLASSOP__AUTO)
   {
      LCD_Char_WriteString("A-");
      LCD_Char_WriteString(gpasAutoMode[pstCarData->eAutoMode]);
   }

   LCD_Char_WriteString(" - ");
   if(pstCarData->uiHallMask_F || pstCarData->uiHallMask_R)
   {
      LCD_Char_WriteString("IN");
   }
   else
   {
      LCD_Char_WriteString("OUT");
   }
   LCD_Char_WriteString(" GRP");

   /* Dispatching flags */
   LCD_Char_GotoXY(17, 1);
   if( pstCarData->bIdleDirection )
   {
      LCD_Char_WriteString("I");
   }
   else
   {
      LCD_Char_WriteString(".");
   }
   if( pstCarData->bInSlowdown )
   {
      LCD_Char_WriteString("S");
   }
   else
   {
      LCD_Char_WriteString(".");
   }
   if( pstCarData->bSuppressReopen )
   {
      LCD_Char_WriteString("R");
   }
   else
   {
      LCD_Char_WriteString(".");
   }

   /* L3 */
   LCD_Char_GotoXY( 0, 2 );

   LCD_Char_WriteString("C-");
   LCD_Char_WriteInteger_ExactLength(pstCarData->ucCurrentLanding+1, 2);

   LCD_Char_WriteString(" D-");
   LCD_Char_WriteInteger_ExactLength(pstCarData->ucDestinationLanding+1, 2);

   LCD_Char_WriteString(" R-");
   LCD_Char_WriteInteger_ExactLength(pstCarData->ucReachableLanding+1, 2);

   LCD_Char_WriteString(" M-");
   if( pstCarData->cMotionDir == DIR__UP )
   {
      LCD_Char_WriteString("UP");
   }
   else if( pstCarData->cMotionDir == DIR__DN )
   {
      LCD_Char_WriteString("DN");
   }
   else
   {
      LCD_Char_WriteString("ST");
   }
   uint8_t bIN_PHE_F = ( GetPHE2InputProgrammed_F2() ) ? ( GetInputValue_ByCar(eCarID, enIN_PHE_F) & GetInputValue_ByCar(eCarID, enIN_PHE_F2) ):GetInputValue_ByCar(eCarID, enIN_PHE_F);
   uint8_t bIN_PHE_R = ( GetPHE2InputProgrammed_R2() ) ? ( GetInputValue_ByCar(eCarID, enIN_PHE_R) & GetInputValue_ByCar(eCarID, enIN_PHE_R2) ):GetInputValue_ByCar(eCarID, enIN_PHE_R);

   /* L4 */
   LCD_Char_GotoXY( 0, 3 );
   UI_PrintDoorSymbol( pstCarData->eDoor_F,
                       bIN_PHE_F,
                       GetInputValue_ByCar(eCarID, enIN_GSWF),
                       GetOutputValue_ByCar(eCarID, enOUT_DO_F),
                       GetOutputValue_ByCar(eCarID, enOUT_DC_F) );
   LCD_Char_AdvanceX(1);
   UI_PrintDoorSymbol( pstCarData->eDoor_R,
                       bIN_PHE_R,
                       GetInputValue_ByCar(eCarID, enIN_GSWR),
                       GetOutputValue_ByCar(eCarID, enOUT_DO_R),
                       GetOutputValue_ByCar(eCarID, enOUT_DC_R) );

   LCD_Char_GotoXY( 15, 3 );
   LCD_Char_WriteString("P-");
   if(pstCarData->enPriority == HC_DIR__DOWN)
   {
      LCD_Char_WriteString("DN");
   }
   else
   {
      LCD_Char_WriteString("UP");
   }
}
static void PrintScreen_HallMask( enum en_group_net_nodes eCarID )
{
   st_cardata *pstCarData = GetCarDataStructure(eCarID);

   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("HMF: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->uiHallMask_F, 8);

   /* L3 */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("HMR: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->uiHallMask_R, 8);

   /* L4 */
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("HML: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->uiLatchableHallMask_F | pstCarData->uiLatchableHallMask_R, 8);
}
static void PrintScreen_OpeningF( enum en_group_net_nodes eCarID )
{
   st_cardata *pstCarData = GetCarDataStructure(eCarID);

   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("OMF1: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->auiBF_OpeningMap_F[0], 8);

   /* L3 */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("OMF2: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->auiBF_OpeningMap_F[1], 8);

   /* L4 */
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("OMF3: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->auiBF_OpeningMap_F[2], 8);
}
static void PrintScreen_OpeningR( enum en_group_net_nodes eCarID )
{
   st_cardata *pstCarData = GetCarDataStructure(eCarID);

   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("OMR1: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->auiBF_OpeningMap_R[0], 8);

   /* L3 */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("OMR2: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->auiBF_OpeningMap_R[1], 8);

   /* L4 */
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("OMR3: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->auiBF_OpeningMap_R[2], 8);
}
static void PrintScreen_SecurityF( enum en_group_net_nodes eCarID )
{
   st_cardata *pstCarData = GetCarDataStructure(eCarID);

   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("SMF1: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->auiBF_SecurityMap_F[0], 8);

   /* L3 */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("SMF2: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->auiBF_SecurityMap_F[1], 8);

   /* L4 */
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("SMF3: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->auiBF_SecurityMap_F[2], 8);
}
static void PrintScreen_SecurityR( enum en_group_net_nodes eCarID )
{
   st_cardata *pstCarData = GetCarDataStructure(eCarID);

   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("SMR1: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->auiBF_SecurityMap_R[0], 8);

   /* L3 */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("SMR2: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->auiBF_SecurityMap_R[1], 8);

   /* L4 */
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("SMR3: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->auiBF_SecurityMap_R[2], 8);
}
static void PrintScreen_LinkedMask( enum en_group_net_nodes eCarID )
{
   st_cardata *pstCarData = GetCarDataStructure(eCarID);

   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("LM1: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->aucPairedHBMask[0], 2);
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteString("LM4: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->aucPairedHBMask[3], 2);

   /* L3 */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("LM2: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->aucPairedHBMask[1], 2);

   /* L4 */
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("LM3: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->aucPairedHBMask[2], 2);
}
static void PrintScreen_HallSecurityMap( enum en_group_net_nodes eCarID )
{
   st_cardata *pstCarData = GetCarDataStructure(eCarID);

   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("HSO1: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->auiBF_HallSecurityMap[0], 8);

   /* L3 */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("HSO2: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->auiBF_HallSecurityMap[1], 8);

   /* L4 */
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("HSO3: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->auiBF_HallSecurityMap[2], 8);
}
static void PrintScreen_HallSecurityMask( enum en_group_net_nodes eCarID )
{
   st_cardata *pstCarData = GetCarDataStructure(eCarID);

   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("HSMF: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->ucHallSecurityMask_F, 2);

   /* L3 */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("HSMR: 0x");
   LCD_Char_WriteHex_ExactLength(pstCarData->ucHallSecurityMask_R, 2);

   /* L4 */
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("BYP:  ");
   if(pstCarData->bEnableHallCallSecurity)
   {
      LCD_Char_WriteString("OFF");
   }
   else
   {
      LCD_Char_WriteString("ON");
   }
}
static void PrintScreen_DDTimes( enum en_group_net_nodes eCarID )
{
   st_cardata *pstCarData = GetCarDataStructure(eCarID);

   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("F2F: ");
   LCD_Char_WriteInteger_ExactLength(pstCarData->ucMaxFloorToFloorTime_sec, 3);

   /* L3 */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("CCD: ");
   LCD_Char_WriteInteger_ExactLength(pstCarData->ucDoorDwellTime_sec, 3);

   /* L4 */
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("HCD: ");
   LCD_Char_WriteInteger_ExactLength(pstCarData->ucDoorDwellHallTime_sec, 3);
}
static void PrintScreen_VIPFlags( enum en_group_net_nodes eCarID )
{
   st_cardata *pstCarData = GetCarDataStructure(eCarID);

   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("bVIP: ");
   LCD_Char_WriteInteger_ExactLength(pstCarData->bVIP, 3);

   /* L3 */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("bCarCapture: ");
   LCD_Char_WriteInteger_ExactLength(pstCarData->bCarCapture, 3);

   /* L4 */
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("bCarReady: ");
   LCD_Char_WriteInteger_ExactLength(pstCarData->bCarReady, 3);
}
static void PrintScreen_VIPMasks( enum en_group_net_nodes eCarID )
{
   st_cardata *pstCarData = GetCarDataStructure(eCarID);

   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("F Mask: ");
   LCD_Char_WriteInteger_ExactLength(pstCarData->uiVIPMask_F, 3);

   /* L3 */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("R Mask: ");
   LCD_Char_WriteInteger_ExactLength(pstCarData->uiVIPMask_F, 3);
}
/*----------------------------------------------------------------------------
|01234567890123456789| |01234567890123456789| |01234567890123456789| |01234567890123456789| |01234567890123456789| |01234567890123456789| |01234567890123456789| |01234567890123456789| |01234567890123456789|

|CAR1 - ON [M]      >| |CAR1 - ON [M]     <>| |CAR1 - ON [M]     <>| |CAR1 - ON [M]     <>| |CAR1 - ON [M]     <>| |CAR1 - ON [M]     <>| |CAR1 - ON [M]     <>| |CAR1 - ON [M]     <>| |CAR1 - ON [M]     <>|
|A-NORM - IN GRP  ISR| |HMF: 0x00000001     | |OMF1: 0x00000001    | |OMR1: 0x00000001    | |SMF1: 0x00000001    | |SMR1: 0x00000001    | |LM1: 0x01 LM4: 0x01 | |HSO1: 0x00000001    | |HSMF: 0x00000001    |
|C-10 D-12 R-11 M-UP | |HMR: 0x00000002     | |OMF2: 0x00000001    | |OMR2: 0x00000001    | |SMF2: 0x00000001    | |SMR2: 0x00000001    | |LM2: 0x01           | |HSO2: 0x00000001    | |HSMR: 0x00000002    |
|[>|<] [>|<]    P-DN | |HML: 0x00000003     | |OMF3: 0x00000001    | |OMR3: 0x00000001    | |SMF3: 0x00000001    | |SMR3: 0x00000001    | |LM3: 0x01           | |HSO3: 0x00000001    | |BYP:  ON            |

 ----------------------------------------------------------------------------*/
static void Print_Screen( uint8_t ucCursorX, uint8_t ucCursorY )
{
   LCD_Char_Clear();

   /* Print Row 1 */
   LCD_Char_GotoXY( 0, 0 );

   LCD_Char_WriteString("CAR");
   LCD_Char_WriteInteger_ExactLength(ucCursorY+1, 1);

   if( GetCarOnlineFlag_ByCar(ucCursorY) )
   {
      LCD_Char_WriteString(" - ON ");
   }
   else
   {
      LCD_Char_WriteString(" - OFF ");
   }

   if( GetMasterDispatcherFlag_ByCar(ucCursorY) )
   {
      LCD_Char_WriteString("[M]");
   }

   if( ucCursorX )
   {
      LCD_Char_GotoXY( 18, 0 );
      LCD_Char_WriteString("<");
   }
   if( ucCursorX < (NUM_CD_PAGE-1) )
   {
      LCD_Char_GotoXY( 19, 0 );
      LCD_Char_WriteString(">");
   }

   /* Print Rows 2-4 */
   switch(ucCursorX)
   {
      case CD_PAGE__STATUS:
         PrintScreen_Status(ucCursorY);
         break;
      case CD_PAGE__HALLMASK:
         PrintScreen_HallMask(ucCursorY);
         break;
      case CD_PAGE__OPENING_F:
         PrintScreen_OpeningF(ucCursorY);
         break;
      case CD_PAGE__OPENING_R:
         PrintScreen_OpeningR(ucCursorY);
         break;
      case CD_PAGE__SECURITY_F:
         PrintScreen_SecurityF(ucCursorY);
         break;
      case CD_PAGE__SECURITY_R:
         PrintScreen_SecurityR(ucCursorY);
         break;
      case CD_PAGE__LINKEDMASK:
         PrintScreen_LinkedMask(ucCursorY);
         break;
      case CD_PAGE__HALL_SECURITY_MAP:
         PrintScreen_HallSecurityMap(ucCursorY);
         break;
      case CD_PAGE__HALL_SECURITY_MASK:
         PrintScreen_HallSecurityMask(ucCursorY);
         break;
      case CD_PAGE__DD_TIMES:
         PrintScreen_DDTimes(ucCursorY);
         break;
      case CD_PAGE__VIPFlags:
         PrintScreen_VIPFlags(ucCursorY);
         break;
      case CD_PAGE__VIPMasks:
         PrintScreen_VIPMasks(ucCursorY);
         break;
      default: break;
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_CarData( void )
{
   static int8_t cCursorX, cCursorY;

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      if(!cCursorX)
      {
         PrevScreen();
      }
      else
      {
         cCursorX--;
      }
   }
   else if ( enKeypress == enKEYPRESS_RIGHT )
   {
      cCursorX++;
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      cCursorY--;
   }
   else if ( enKeypress == enKEYPRESS_DOWN )
   {
      cCursorY++;
   }

   /* Bound values */
   if( cCursorY >= MAX_GROUP_CARS )
   {
      cCursorY = MAX_GROUP_CARS-1;
   }
   else if( cCursorY < 0 )
   {
      cCursorY = 0;
   }
   if( cCursorX >= NUM_CD_PAGE )
   {
      cCursorX = NUM_CD_PAGE-1;
   }
   else if( cCursorX < 0 )
   {
      cCursorX = 0;
   }

   Print_Screen( cCursorX, cCursorY );
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/


