/******************************************************************************
 *
 * @file     ui_ffs_sabbath.c
 * @brief    Sabbath UI pages
 * @version  V1.00
 * @date     13, March 2018
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Sabbath_KeyEnable( void );
static void UI_FFS_Sabbath_TimerEnable ( void );
static void UI_FFS_Sabbath_KeyOrTimer_Enable ( void );
static void UI_FFS_Sabbath_Start_Time( void );
static void UI_FFS_Sabbath_End_Time( void );
static void UI_FSS_Sabbath_Door_Dwell_Timer( void );
static void UI_FFS_Sabbath_FloorsOpening_F( void );
static void UI_FFS_Sabbath_FloorsOpening_R( void );
static void UI_FFS_Sabbath_Destinations_Up( void );
static void UI_FFS_Sabbath_Destinations_Down( void );
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_Sabbath_KeyEnable =
{
   .pfnDraw = UI_FFS_Sabbath_KeyEnable,
};
static struct st_ui_screen__freeform gstFFS_Sabbath_TimerEnable =
{
   .pfnDraw = UI_FFS_Sabbath_TimerEnable,
};
static struct st_ui_screen__freeform gstFFS_Sabbath_KeyOrTimer_Enable =
{
   .pfnDraw = UI_FFS_Sabbath_KeyOrTimer_Enable,
};
static struct st_ui_screen__freeform gstFFS_Sabbath_Start_Time =
{
   .pfnDraw = UI_FFS_Sabbath_Start_Time,
};
static struct st_ui_screen__freeform gstFFS_Sabbath_End_Time =
{
   .pfnDraw = UI_FFS_Sabbath_End_Time,
};
static struct st_ui_screen__freeform gstFFS_Sabbath_DoorDwell_Timer =
{
   .pfnDraw = UI_FSS_Sabbath_Door_Dwell_Timer,
};
static struct st_ui_screen__freeform gstFFS_Sabbath_FloorsOpening_F =
{
   .pfnDraw = UI_FFS_Sabbath_FloorsOpening_F,
};
static struct st_ui_screen__freeform gstFFS_Sabbath_FloorsOpening_R =
{
   .pfnDraw = UI_FFS_Sabbath_FloorsOpening_R,
};
static struct st_ui_screen__freeform gstFFS_Sabbath_Destinations_Up =
{
   .pfnDraw = UI_FFS_Sabbath_Destinations_Up,
};
static struct st_ui_screen__freeform gstFFS_Sabbath_Destinations_Down =
{
   .pfnDraw = UI_FFS_Sabbath_Destinations_Down,
};
//----------------------------------------------------------
static struct st_ui_menu_item gstMI_Key_Enable =
{
   .psTitle = "Key Enable Only",
   .pstUGS_Next = &gstUGS_Sabbath_KeyEnable,
};
static struct st_ui_menu_item gstMI_Timer_Enable =
{
   .psTitle = "Timer Enable Only",
   .pstUGS_Next = &gstUGS_Sabbath_TimerEnable,
};
static struct st_ui_menu_item gstMI_KeyOrTimer_Enable =
{
   .psTitle = "Key or Timer Enable",
   .pstUGS_Next = &gstUGS_Sabbath_KeyOrTimer_Enable,
};
static struct st_ui_menu_item gstMI_Start_Time =
{
   .psTitle = "Friday Start Time",
   .pstUGS_Next = &gstUGS_Sabbath_Start_Time,
};
static struct st_ui_menu_item gstMI_End_Time =
{
   .psTitle = "Saturday End Time",
   .pstUGS_Next = &gstUGS_Sabbath_End_Time,
};
static struct  st_ui_menu_item gstMI_Door_Dwell_Timer =
{
   .psTitle = "Door Dwell Timer",
   .pstUGS_Next = &gstUGS_Sabbath_Door_Dwell_Timer,
};
static struct st_ui_menu_item gstMI_Floors_Opening_F =
{
   .psTitle = "Floors Opening (F)",
   .pstUGS_Next = &gstUGS_Sabbath_FloorsOpening_F,
};
static struct st_ui_menu_item gstMI_Floors_Opening_R =
{
   .psTitle = "Floors Opening (R)",
   .pstUGS_Next = &gstUGS_Sabbath_FloorsOpening_R,
};
static struct st_ui_menu_item gstMI_Destinations_Up =
{
   .psTitle = "Destinations Up",
   .pstUGS_Next = &gstUGS_Sabbath_Destinations_Up,
};
static struct st_ui_menu_item gstMI_Destinations_Down =
{
   .psTitle = "Destinations Down",
   .pstUGS_Next = &gstUGS_Sabbath_Destinations_Down,
};
static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_Key_Enable,
   &gstMI_Timer_Enable,
   &gstMI_KeyOrTimer_Enable,
   &gstMI_Start_Time,
   &gstMI_End_Time,
   &gstMI_Door_Dwell_Timer,
   &gstMI_Floors_Opening_F,
   &gstMI_Floors_Opening_R,
   &gstMI_Destinations_Up,
   &gstMI_Destinations_Down,
};
static struct st_ui_screen__menu gstMenu_Setup_Sabbath =
{
   .psTitle = "Sabbath",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Sabbath_KeyEnable =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Sabbath_KeyEnable,
};
struct st_ui_generic_screen gstUGS_Sabbath_TimerEnable =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Sabbath_TimerEnable,
};
struct st_ui_generic_screen gstUGS_Sabbath_KeyOrTimer_Enable =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Sabbath_KeyOrTimer_Enable,
};
struct st_ui_generic_screen gstUGS_Sabbath_Start_Time =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Sabbath_Start_Time,
};
struct st_ui_generic_screen gstUGS_Sabbath_End_Time =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Sabbath_End_Time,
};
struct st_ui_generic_screen gstUGS_Sabbath_Door_Dwell_Timer =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Sabbath_DoorDwell_Timer,
};
struct st_ui_generic_screen gstUGS_Sabbath_FloorsOpening_F =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Sabbath_FloorsOpening_F,
};
struct st_ui_generic_screen gstUGS_Sabbath_FloorsOpening_R =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Sabbath_FloorsOpening_R,
};
struct st_ui_generic_screen gstUGS_Sabbath_Destinations_Up =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Sabbath_Destinations_Up,
};
struct st_ui_generic_screen gstUGS_Sabbath_Destinations_Down =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Sabbath_Destinations_Down,
};
//------------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_Setup_Sabbath =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Sabbath,
};

static uint8_t ucCurrentFloorIndex;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MAX_CURSOR_X_POS (1)
#define START_CURSOR_POS_VALUE (7U)
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void Screen_Bitmap(struct st_param_edit_menu * pstParamEditMenu)
{
   LCD_Char_Clear();

   LCD_Char_GotoXY( 0, 0 );

   //------------------------------------------------------------------
   if(pstParamEditMenu->bSaving)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Saving");
   }
   else if(pstParamEditMenu->bInvalid)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Invalid");
   }
   else
   {
     LCD_Char_GotoXY( 0, 0 );
     LCD_Char_WriteStringUpper( pstParamEditMenu->psTitle);
     LCD_Char_GotoXY( 16, 0 );
     char *psPI = Get_PI_Label(ucCurrentFloorIndex);
     LCD_Char_WriteStringUpper( "[" );
     if(Param_ReadValue_1Bit(enPARAM1__Enable3DigitPI))
     {
        LCD_Char_WriteStringUpper( psPI );
     }
     else
     {
        LCD_Char_WriteStringUpper( psPI+1 );
     }
     LCD_Char_WriteStringUpper( "]" );
      //------------------------------------------------------------------
      LCD_Char_GotoXY(1,2);
      LCD_Char_WriteInteger_ExactLength( ucCurrentFloorIndex + 1, 2 );
      LCD_Char_WriteStringUpper(" = ");

      LCD_Char_GotoXY(START_CURSOR_POS_VALUE, 2);

      uint8_t ucBitMapIndex = ucCurrentFloorIndex%32;// Size of each 32 bit parameter
      uint8_t bEditedValue = Sys_Bit_Get(&pstParamEditMenu->ulValue_Edited, ucBitMapIndex);

      if(bEditedValue)
      {
         LCD_Char_WriteString("On");
      }
      else
      {
         LCD_Char_WriteString("Off");
      }

      LCD_Char_GotoXY( pstParamEditMenu->ucCursorColumn, 3 );

      LCD_Char_WriteChar( '*' );

      //------------------------------------------------------------------
      if ( pstParamEditMenu->ulValue_Edited != pstParamEditMenu->ulValue_Saved )
      {
         LCD_Char_GotoXY( 16, 1 );
         LCD_Char_WriteString( "Save" );

         LCD_Char_GotoXY( 19, 2 );
         LCD_Char_WriteChar( '|' );
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ValidateParameter(struct st_param_edit_menu * pstParamEditMenu)
{
   switch(pstParamEditMenu->enParamType)
   {
      case enPARAM_BLOCK_TYPE__BIT:
         pstParamEditMenu->ulValue_Max = ( !pstParamEditMenu->ulValue_Max )
                                     ? 1 : pstParamEditMenu->ulValue_Max;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_1BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_1Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT8:
         pstParamEditMenu->ulValue_Max = ( !pstParamEditMenu->ulValue_Max )
                                       ? 0xFF : pstParamEditMenu->ulValue_Max;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_8BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_8Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT16:
         pstParamEditMenu->ulValue_Max = ( !pstParamEditMenu->ulValue_Max )
                                       ? 0xFFFF : pstParamEditMenu->ulValue_Max;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_16BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_16Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT24:
         pstParamEditMenu->ulValue_Max = ( !pstParamEditMenu->ulValue_Max )
                                       ? 0xFFFFFF : pstParamEditMenu->ulValue_Max;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_24BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_24Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT32:
         pstParamEditMenu->ulValue_Max = ( !pstParamEditMenu->ulValue_Max )
                                       ? 0xFFFFFFFF : pstParamEditMenu->ulValue_Max;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_32BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_32Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      default:
         pstParamEditMenu->bInvalid = 1;
         break;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Floor( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_param_edit_menu * pstParamEditMenu )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex = 0;
   int32_t lParamIndex_Edited;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = pstParamEditMenu->ucFieldSize_Index-1;
      }
   }
   else  // enACTION__EDIT
   {

      lParamIndex_Edited = ucCurrentFloorIndex;

      pstParamEditMenu->ucCursorColumn = 1+ucCursorIndex;
      //------------------------------------------------------------------
      int32_t uiDigitWeight = (pstParamEditMenu->ucFieldSize_Index - 1) - ucCursorIndex;

      uiDigitWeight = pow( 10, uiDigitWeight );  // 10 to the power of uiDigitWeight
      //------------------------------------------------------------------
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            lParamIndex_Edited += uiDigitWeight;
            break;
         case enKEYPRESS_DOWN:
            lParamIndex_Edited -= uiDigitWeight;
            break;
         case enKEYPRESS_LEFT:
            if ( ucCursorIndex )
            {
               --ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_LEFT;
            }
            break;
         case enKEYPRESS_RIGHT:
            if ( ucCursorIndex < pstParamEditMenu->ucFieldSize_Index - 1 )
            {
               ++ucCursorIndex;
            }
            else
            {
               pstParamEditMenu->ulValue_Edited = pstParamEditMenu->ulValue_Saved;
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;
         default:
            break;
      }
      //------------------------------------------------------------------
      if(lParamIndex_Edited >= GetFP_NumFloors()-1)
      {
         lParamIndex_Edited = GetFP_NumFloors()-1;
      }
      else if(lParamIndex_Edited <= 0)
      {
         lParamIndex_Edited = 0;
      }

      ucCurrentFloorIndex = (uint8_t) lParamIndex_Edited;

      pstParamEditMenu->ulValue_Edited = pstParamEditMenu->ulValue_Saved;

   }

   return enReturnValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Value_ByBit( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_param_edit_menu * pstParamEditMenu )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex;
   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = pstParamEditMenu->ucFieldSize_Value - 1;
      }
   }
   else  // enACTION__EDIT
   {
      uint8_t ucBitMapIndex = ucCurrentFloorIndex%32;// Size of each 32 bit parameter
      //------------------------------------------------------------------
      uint8_t bEditedValue = Sys_Bit_Get(&pstParamEditMenu->ulValue_Edited, ucBitMapIndex);
      //------------------------------------------------------------------
      pstParamEditMenu->ucCursorColumn = START_CURSOR_POS_VALUE + ucCursorIndex;
      //------------------------------------------------------------------
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            bEditedValue = 1;
            break;

         case enKEYPRESS_DOWN:
            bEditedValue = 0;
            break;

         case enKEYPRESS_LEFT:
            if ( ucCursorIndex )
            {
               --ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_LEFT;
            }
            break;

         case enKEYPRESS_RIGHT:
            if ( ucCursorIndex < (pstParamEditMenu->ucFieldSize_Value - 1) )
            {
               ++ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;

         default:
            break;
      }
      Sys_Bit_Set(&pstParamEditMenu->ulValue_Edited, ucBitMapIndex, bEditedValue);
   }

   return enReturnValue;
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void SaveParameter( struct st_param_edit_menu * pstParamEditMenu )
{
   uint16_t ucIndex = pstParamEditMenu->uwParamIndex_Current;
   switch(pstParamEditMenu->enParamType)
   {
      case enPARAM_BLOCK_TYPE__UINT32:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_32Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_32Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT24:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_24Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_24Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT16:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_16Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_16Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT8:

         pstParamEditMenu->ulValue_Saved = Param_ReadValue_8Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
          Param_WriteValue_8Bit( ucIndex,pstParamEditMenu->ulValue_Edited);
         }
         break;
      default:
      case enPARAM_BLOCK_TYPE__BIT:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_1Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_1Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Save( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_param_edit_menu * pstParamEditMenu )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static enum en_save_state enSaveState = SAVE_STATE__SAVED;

   pstParamEditMenu->ucCursorColumn = 19;
   //------------------------------------------
   if ( enFieldAction == enACTION__EDIT )
   {
      if ( enKeypress == enKEYPRESS_ENTER )
      {
         enSaveState = SAVE_STATE__SAVING;
      }
      else if ( enKeypress == enKEYPRESS_LEFT )
      {
         enSaveState = SAVE_STATE__SAVED;
         enReturnValue = enACTION__EXIT_TO_LEFT;
      }
   }

   //--------------------------------
   switch(enSaveState)
   {
      case SAVE_STATE__CLEARING:
         pstParamEditMenu->bSaving = 1;
         if(!pstParamEditMenu->ulValue_Saved)
         {
            enSaveState = SAVE_STATE__SAVING;
         }
         break;
      case SAVE_STATE__SAVING:
         SaveParameter(pstParamEditMenu);
         pstParamEditMenu->bSaving = 1;
         if(pstParamEditMenu->ulValue_Saved == pstParamEditMenu->ulValue_Edited)
         {
            enReturnValue = enACTION__EXIT_TO_LEFT;
            pstParamEditMenu->bSaving = 0;
            enSaveState = SAVE_STATE__SAVED;
         }
         break;
      case SAVE_STATE__SAVED:
         pstParamEditMenu->bSaving = 0;
         break;
      default:
         enSaveState = SAVE_STATE__SAVED;
         break;
   }
   return enReturnValue;
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void EditParameterRange_ByBit(struct st_param_edit_menu * pstParamEditMenu)
{
   /* Modification for editing by bit */
   pstParamEditMenu->uwParamIndex_Current = pstParamEditMenu->uwParamIndex_Start + ( ucCurrentFloorIndex / 32 );

   ValidateParameter(pstParamEditMenu);
   if ( pstParamEditMenu->ucFieldIndex == enFIELD__NONE )
   {
      pstParamEditMenu->ulValue_Edited = pstParamEditMenu->ulValue_Saved;
      pstParamEditMenu->bSaving = 0;
      pstParamEditMenu->bInvalid = 0;
      pstParamEditMenu->ucFieldIndex = enFIELD__FLOOR;

      Edit_Floor( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );
   }
   else
   {
      enum en_field_action enFieldAction;

      enum en_keypresses enKeypress = Button_GetKeypress();
      //------------------------------------------------------------------
      switch ( pstParamEditMenu->ucFieldIndex )
      {
         case enFIELD__FLOOR:
            enFieldAction = Edit_Floor( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               PrevScreen();
               pstParamEditMenu->ucFieldIndex = enFIELD__NONE;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Value_ByBit( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__VALUE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__VALUE: //function

            enFieldAction = Edit_Value_ByBit( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Floor( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT, pstParamEditMenu);

               pstParamEditMenu->ucFieldIndex = enFIELD__FLOOR;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Save( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__SAVE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__SAVE:

            enFieldAction = Edit_Save( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Value_ByBit( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__VALUE;
            }
            break;

         default:
            break;
      }
      Screen_Bitmap(pstParamEditMenu);
   }
}

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_Sabbath_KeyEnable( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__Sabbath_KeyEnable_Only;
   stParamEdit.uwParamIndex_End = stParamEdit.uwParamIndex_Start;
   stParamEdit.psTitle = "Key Enable Only";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_Sabbath_TimerEnable( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__Sabbath_TimerEnable_Only;
   stParamEdit.uwParamIndex_End = stParamEdit.uwParamIndex_Start;
   stParamEdit.psTitle = "Timer Enable Only";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_Sabbath_KeyOrTimer_Enable( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__Sabbath_KeyOrTimer_Enable;
   stParamEdit.uwParamIndex_End = stParamEdit.uwParamIndex_Start;
   stParamEdit.psTitle = "Key or Timer Enable";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_Sabbath_Start_Time( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT24;
   stParamEdit.uwParamIndex_Start = enPARAM24__Sabbath_Start_Time;
   stParamEdit.uwParamIndex_End = stParamEdit.uwParamIndex_Start;
   stParamEdit.psTitle = "Friday Start Time";
   stParamEdit.psUnit = "";
   stParamEdit.ulValue_Max = 2359;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_Sabbath_End_Time( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT24;
   stParamEdit.uwParamIndex_Start = enPARAM24__Sabbath_End_Time;
   stParamEdit.uwParamIndex_End = stParamEdit.uwParamIndex_Start;
   stParamEdit.psTitle = "Saturday End Time";
   stParamEdit.psUnit = "";
   stParamEdit.ulValue_Max = 2359;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FSS_Sabbath_Door_Dwell_Timer( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__DoorDwellSabbathTimer_1s;
   stParamEdit.uwParamIndex_End = stParamEdit.uwParamIndex_Start;
   stParamEdit.psTitle = "Door Dwell Timer";
   stParamEdit.psUnit = "sec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Sabbath_FloorsOpening_F( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT32;
   stParamEdit.uwParamIndex_Start = enPARAM32__SabbathOpeningF_0;
   stParamEdit.uwParamIndex_End = enPARAM32__SabbathOpeningF_1;
   stParamEdit.psTitle = "Floor Openings";
   stParamEdit.ucFieldSize_Value = 1;
   stParamEdit.ucFieldSize_Index = 2;
   EditParameterRange_ByBit(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Sabbath_FloorsOpening_R( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT32;
   stParamEdit.uwParamIndex_Start = enPARAM32__SabbathOpeningR_0;
   stParamEdit.uwParamIndex_End = enPARAM32__SabbathOpeningR_1;
   stParamEdit.psTitle = "Floor Openings";
   stParamEdit.ucFieldSize_Value = 1;
   stParamEdit.ucFieldSize_Index = 2;
   EditParameterRange_ByBit(&stParamEdit);
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Sabbath_Destinations_Up( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT32;
   stParamEdit.uwParamIndex_Start = enPARAM32__SabbathDestinationUp_0;
   stParamEdit.uwParamIndex_End = enPARAM32__SabbathDestinationUp_1;
   stParamEdit.psTitle = "Up Destinations";
   stParamEdit.ucFieldSize_Value = 1;
   stParamEdit.ucFieldSize_Index = 2;
   EditParameterRange_ByBit(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Sabbath_Destinations_Down( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT32;
   stParamEdit.uwParamIndex_Start = enPARAM32__SabbathDestinationDown_0;
   stParamEdit.uwParamIndex_End = enPARAM32__SabbathDestinationDown_1;
   stParamEdit.psTitle = "DN Destinations";
   stParamEdit.ucFieldSize_Value = 1;
   stParamEdit.ucFieldSize_Index = 2;
   EditParameterRange_ByBit(&stParamEdit);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
