/******************************************************************************
 *
 * @file     ui_ffs_fire.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>

 /*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_Edit_Fire_ParamBit( uint32_t ulParamNumber, char* psTitle );

static void UI_FFS_Fire_MainRecall_Floor( void );
static void UI_FFS_Fire_MainRecall_Opening( void );

static void UI_FFS_Fire_AltRecall_Floor( void );
static void UI_FFS_Fire_AltRecall_Opening( void );
//---------------------------------------------------------------
// MAin Smoke
static void UI_FFS_Fire_MainSmoke_MainOrAlt( void );
static void UI_FFS_Fire_MainSmoke_FlashFireHat( void );
static void UI_FFS_Fire_MainSmoke_ShuntTrip( void );
//---------------------------------------------------------------
// Alt Smoke
static void UI_FFS_Fire_AltSmoke_MainOrAlt( void );
static void UI_FFS_Fire_AltSmoke_FlashFireHat( void );
static void UI_FFS_Fire_AltSmoke_ShuntTrip( void );
//---------------------------------------------------------------
// Hoistway Smoke
static void UI_FFS_Fire_HoistwaySmoke_MainOrAlt( void );
static void UI_FFS_Fire_HoistwaySmoke_FlashFireHat( void );
static void UI_FFS_Fire_HoistwaySmoke_ShuntTrip( void );
//---------------------------------------------------------------
// MR Smoke
static void UI_FFS_Fire_MRSmoke_MainOrAlt( void );
static void UI_FFS_Fire_MRSmoke_FlashFireHat( void );
static void UI_FFS_Fire_MRSmoke_ShuntTrip( void );

//---------------------------------------------------------------
// PIT Smoke
static void UI_FFS_Fire_PITSmoke_MainOrAlt( void );
static void UI_FFS_Fire_PITSmoke_FlashFireHat( void );
static void UI_FFS_Fire_PITSmoke_ShuntTrip( void );

static void UI_FFS_Fire_SmokeConfig( void );

//---------------------------------------------------------------
// Hoistway 2 Smoke
static void UI_FFS_Fire_EnableAltMachineRoom( void );
static void UI_FFS_Fire_Hoistway2Smoke_MainOrAlt( void );
static void UI_FFS_Fire_Hoistway2Smoke_FlashFireHat( void );
static void UI_FFS_Fire_Hoistway2Smoke_ShuntTrip( void );
//---------------------------------------------------------------
// MR 2 Smoke
static void UI_FFS_Fire_MR2Smoke_MainOrAlt( void );
static void UI_FFS_Fire_MR2Smoke_FlashFireHat( void );
static void UI_FFS_Fire_MR2Smoke_ShuntTrip( void );

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_Fire_MainRecall_Floor =
{
   .pfnDraw = UI_FFS_Fire_MainRecall_Floor ,
};
static struct st_ui_screen__freeform gstFFS_Fire_MainRecall_Opening =
{
   .pfnDraw = UI_FFS_Fire_MainRecall_Opening ,
};
static struct st_ui_screen__freeform gstFFS_Fire_AltRecall_Floor =
{
   .pfnDraw = UI_FFS_Fire_AltRecall_Floor,
};
static struct st_ui_screen__freeform gstFFS_Fire_AltRecall_Opening =
{
   .pfnDraw = UI_FFS_Fire_AltRecall_Opening,
};

static struct st_ui_screen__freeform gstFFS_Fire_MainSmoke_MainOrAlt =
{
   .pfnDraw = UI_FFS_Fire_MainSmoke_MainOrAlt,
};
static struct st_ui_screen__freeform gstFFS_Fire_MainSmoke_FlashFireHat =
{
   .pfnDraw = UI_FFS_Fire_MainSmoke_FlashFireHat,
};
static struct st_ui_screen__freeform gstFFS_Fire_MainSmoke_ShuntTrip =
{
   .pfnDraw = UI_FFS_Fire_MainSmoke_ShuntTrip ,
};


static struct st_ui_screen__freeform gstFFS_Fire_AltSmoke_MainOrAlt =
{
   .pfnDraw = UI_FFS_Fire_AltSmoke_MainOrAlt,
};
static struct st_ui_screen__freeform gstFFS_Fire_AltSmoke_FlashFireHat =
{
   .pfnDraw = UI_FFS_Fire_AltSmoke_FlashFireHat ,
};
static struct st_ui_screen__freeform gstFFS_Fire_AltSmoke_ShuntTrip =
{
   .pfnDraw = UI_FFS_Fire_AltSmoke_ShuntTrip,
};

static struct st_ui_screen__freeform gstFFS_Fire_HoistwaySmoke_MainOrAlt =
{
   .pfnDraw = UI_FFS_Fire_HoistwaySmoke_MainOrAlt ,
};
static struct st_ui_screen__freeform gstFFS_Fire_HoistwaySmoke_FlashFireHat =
{
   .pfnDraw = UI_FFS_Fire_HoistwaySmoke_FlashFireHat ,
};
static struct st_ui_screen__freeform gstFFS_Fire_HoistwaySmoke_ShuntTrip =
{
   .pfnDraw = UI_FFS_Fire_HoistwaySmoke_ShuntTrip,
};


static struct st_ui_screen__freeform gstFFS_Fire_MRSmoke_MainOrAlt =
{
   .pfnDraw = UI_FFS_Fire_MRSmoke_MainOrAlt,
};
static struct st_ui_screen__freeform gstFFS_Fire_MRSmoke_FlashFireHat =
{
   .pfnDraw = UI_FFS_Fire_MRSmoke_FlashFireHat ,
};
static struct st_ui_screen__freeform gstFFS_Fire_MRSmoke_ShuntTrip =
{
   .pfnDraw = UI_FFS_Fire_MRSmoke_ShuntTrip,
};


static struct st_ui_screen__freeform gstFFS_Fire_PITSmoke_MainOrAlt =
{
   .pfnDraw = UI_FFS_Fire_PITSmoke_MainOrAlt,
};
static struct st_ui_screen__freeform gstFFS_Fire_PITSmoke_FlashFireHat =
{
   .pfnDraw = UI_FFS_Fire_PITSmoke_FlashFireHat ,
};
static struct st_ui_screen__freeform gstFFS_Fire_PITSmoke_ShuntTrip =
{
   .pfnDraw = UI_FFS_Fire_PITSmoke_ShuntTrip,
};


static struct st_ui_screen__freeform gstFFS_Fire_SmokeConfig =
{
   .pfnDraw = UI_FFS_Fire_SmokeConfig,
};

static struct st_ui_screen__freeform gstFFS_Fire_EnableAltMachineRoom =
{
   .pfnDraw = UI_FFS_Fire_EnableAltMachineRoom,
};
static struct st_ui_screen__freeform gstFFS_Fire_Hoistway2Smoke_MainOrAlt =
{
   .pfnDraw = UI_FFS_Fire_Hoistway2Smoke_MainOrAlt ,
};
static struct st_ui_screen__freeform gstFFS_Fire_Hoistway2Smoke_FlashFireHat =
{
   .pfnDraw = UI_FFS_Fire_Hoistway2Smoke_FlashFireHat ,
};
static struct st_ui_screen__freeform gstFFS_Fire_Hoistway2Smoke_ShuntTrip =
{
   .pfnDraw = UI_FFS_Fire_Hoistway2Smoke_ShuntTrip,
};


static struct st_ui_screen__freeform gstFFS_Fire_MR2Smoke_MainOrAlt =
{
   .pfnDraw = UI_FFS_Fire_MR2Smoke_MainOrAlt,
};
static struct st_ui_screen__freeform gstFFS_Fire_MR2Smoke_FlashFireHat =
{
   .pfnDraw = UI_FFS_Fire_MR2Smoke_FlashFireHat ,
};
static struct st_ui_screen__freeform gstFFS_Fire_MR2Smoke_ShuntTrip =
{
   .pfnDraw = UI_FFS_Fire_MR2Smoke_ShuntTrip,
};


//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Fire_MainRecall =
{
   .psTitle = "Main Recall",
   .pstUGS_Next = &gstUGS_Fire_MainRecall,
};
static struct st_ui_menu_item gstMI_Fire_AltRecall =
{
   .psTitle = "Alt Recall",
   .pstUGS_Next = &gstUGS_Fire_AltRecall,
};
static struct st_ui_menu_item gstMI_Fire_MainSmoke =
{
   .psTitle = "Main Smoke",
   .pstUGS_Next = &gstUGS_Fire_MainSmoke,
};
static struct st_ui_menu_item gstMI_Fire_AltSmoke =
{
   .psTitle = "Alt Smoke",
   .pstUGS_Next = &gstUGS_Fire_AltSmoke,
};
static struct st_ui_menu_item gstMI_Fire_HoistwaySmoke =
{
   .psTitle = "Hoistway Smoke",
   .pstUGS_Next = &gstUGS_Fire_HoistwaySmoke,
};
static struct st_ui_menu_item gstMI_Fire_MRSmoke =
{
   .psTitle = "MR Smoke",
   .pstUGS_Next = &gstUGS_Fire_MRSmoke,
};
static struct st_ui_menu_item gstMI_Fire_PITSmoke =
{
   .psTitle = "PIT Smoke",
   .pstUGS_Next = &gstUGS_Fire_PITSmoke,
};
static struct st_ui_menu_item gstMI_Fire_SmokeConfig =
{
   .psTitle = "Advance Configs",
   .pstUGS_Next = &gstUGS_Fire_SmokeConfig,
};
static struct st_ui_menu_item gstMI_Fire_AltMachineRoom =
{
   .psTitle = "Alt Machine Room",
   .pstUGS_Next = &gstUGS_Fire_AltMachineRoom,
};

static struct st_ui_menu_item * gastMenuItems_Fire[] =
{
   &gstMI_Fire_MainRecall,
   &gstMI_Fire_AltRecall,
   &gstMI_Fire_MainSmoke,
   &gstMI_Fire_AltSmoke,
   &gstMI_Fire_HoistwaySmoke,
   &gstMI_Fire_MRSmoke,
   &gstMI_Fire_PITSmoke,
   &gstMI_Fire_AltMachineRoom,
   &gstMI_Fire_SmokeConfig,
};
static struct st_ui_screen__menu gstMenu_Setup_Fire =
{
   .psTitle = "Fire Service",
   .pastMenuItems = &gastMenuItems_Fire,
   .ucNumItems = sizeof(gastMenuItems_Fire) / sizeof(gastMenuItems_Fire[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Fire_MainRecall_Floor =
{
   .psTitle = "Floor ",
   .pstUGS_Next = &gstUGS_Fire_MainRecall_Floor,
};
static struct st_ui_menu_item gstMI_Fire_MainRecall_Opening =
{
   .psTitle = "Opening ",
   .pstUGS_Next = &gstUGS_Fire_MainRecall_Opening,
};
static struct st_ui_menu_item * gastMenuItems_Fire_MainRecall[] =
{

   &gstMI_Fire_MainRecall_Floor,
   &gstMI_Fire_MainRecall_Opening,
};
static struct st_ui_screen__menu gstMenu_Setup_Fire_MainRecall =
{
   .psTitle = "Main Recall",
   .pastMenuItems = &gastMenuItems_Fire_MainRecall,
   .ucNumItems = sizeof(gastMenuItems_Fire_MainRecall) / sizeof(gastMenuItems_Fire_MainRecall[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Fire__AltRecall_Floor =
{
   .psTitle = "Floor ",
   .pstUGS_Next = &gstUGS_Fire_AltRecall_Floor,
};
static struct st_ui_menu_item gstMI_Fire__AltRecall_Opening =
{
   .psTitle = "Opening ",
   .pstUGS_Next = &gstUGS_Fire_AltRecall_Opening,
};
static struct st_ui_menu_item * gastMenuItems_Fire_AltRecall[] =
{

   &gstMI_Fire__AltRecall_Floor,
   &gstMI_Fire__AltRecall_Opening,
};
static struct st_ui_screen__menu gstMenu_Setup_Fire_AltRecall =
{
   .psTitle = "Alt Recall",
   .pastMenuItems = &gastMenuItems_Fire_AltRecall,
   .ucNumItems = sizeof(gastMenuItems_Fire_AltRecall) / sizeof(gastMenuItems_Fire_AltRecall[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Fire_MainSmoke_MainOrAlt =
{
      .psTitle = "Main or Alt",
      .pstUGS_Next = &gstUGS_Fire_MainSmoke_MainOrAlt,
};
static struct st_ui_menu_item gstMI_Fire_MainSmoke_FlashFireHat =
{
      .psTitle = "Flash Fire Hat",
      .pstUGS_Next = &gstUGS_Fire_MainSmoke_FlashFireHat,
};
static struct st_ui_menu_item gstMI_Fire_MainSmoke_ShuntTrip =
{
      .psTitle = "Shunt Trip",
      .pstUGS_Next = &gstUGS_Fire_MainSmoke_ShuntTrip,
};
static struct st_ui_menu_item * gastMenuItems_Fire_MainSmoke[] =
{

   &gstMI_Fire_MainSmoke_MainOrAlt,
   &gstMI_Fire_MainSmoke_FlashFireHat,
   &gstMI_Fire_MainSmoke_ShuntTrip,
};
static struct st_ui_screen__menu gstMenu_Setup_Fire_MainSmoke =
{
   .psTitle = "Main Smoke",
   .pastMenuItems = &gastMenuItems_Fire_MainSmoke,
   .ucNumItems = sizeof(gastMenuItems_Fire_MainSmoke) / sizeof(gastMenuItems_Fire_MainSmoke[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Fire_AltSmoke_MainOrAlt =
{
      .psTitle = "Main or Alt",
      .pstUGS_Next = &gstUGS_Fire_AltSmoke_MainOrAlt,
};
static struct st_ui_menu_item gstMI_Fire_AltSmoke_FlashFireHat =
{
      .psTitle = "Flash Fire Hat",
      .pstUGS_Next = &gstUGS_Fire_AltSmoke_FlashFireHat,
};
static struct st_ui_menu_item gstMI_Fire_AltSmoke_ShuntTrip =
{
      .psTitle = "Shunt Trip",
      .pstUGS_Next = &gstUGS_Fire_AltSmoke_ShuntTrip,
};
static struct st_ui_menu_item * gastMenuItems_Fire_AltSmoke[] =
{

   &gstMI_Fire_AltSmoke_MainOrAlt,
   &gstMI_Fire_AltSmoke_FlashFireHat,
   &gstMI_Fire_AltSmoke_ShuntTrip,
};
static struct st_ui_screen__menu gstMenu_Setup_Fire_AltSmoke =
{
   .psTitle = "Alt Smoke",
   .pastMenuItems = &gastMenuItems_Fire_AltSmoke,
   .ucNumItems = sizeof(gastMenuItems_Fire_AltSmoke) / sizeof(gastMenuItems_Fire_AltSmoke[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Fire_HoistwaySmoke_MainOrAlt =
{
      .psTitle = "Main or Alt",
      .pstUGS_Next = &gstUGS_Fire_HoistwaySmoke_MainOrAlt,
};
static struct st_ui_menu_item gstMI_Fire_HoistwaySmoke_FlashFireHat =
{
      .psTitle = "Flash Fire Hat",
      .pstUGS_Next = &gstUGS_Fire_HoistwaySmoke_FlashFireHat,
};
static struct st_ui_menu_item gstMI_Fire_HoistwaySmoke_ShuntTrip =
{
      .psTitle = "Shunt Trip",
      .pstUGS_Next = &gstUGS_Fire_HoistwaySmoke_ShuntTrip,
};
static struct st_ui_menu_item * gastMenuItems_Fire_HoistwaySmoke[] =
{

   &gstMI_Fire_HoistwaySmoke_MainOrAlt,
   &gstMI_Fire_HoistwaySmoke_FlashFireHat,
   &gstMI_Fire_HoistwaySmoke_ShuntTrip,
};
static struct st_ui_screen__menu gstMenu_Setup_Fire_HoistwaySmoke =
{
   .psTitle = "Hoistway Smoke",
   .pastMenuItems = &gastMenuItems_Fire_HoistwaySmoke,
   .ucNumItems = sizeof(gastMenuItems_Fire_HoistwaySmoke) / sizeof(gastMenuItems_Fire_HoistwaySmoke[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Fire_MRSmoke_MainOrAlt =
{
      .psTitle = "Main or Alt",
      .pstUGS_Next = &gstUGS_Fire_MRSmoke_MainOrAlt,
};
static struct st_ui_menu_item gstMI_Fire_MRSmoke_FlashFireHat =
{
      .psTitle = "Flash Fire Hat",
      .pstUGS_Next = &gstUGS_Fire_MRSmoke_FlashFireHat,
};
static struct st_ui_menu_item gstMI_Fire_MRSmoke_ShuntTrip =
{
      .psTitle = "Shunt Trip",
      .pstUGS_Next = &gstUGS_Fire_MRSmoke_ShuntTrip,
};
static struct st_ui_menu_item * gastMenuItems_Fire_MRSmoke[] =
{

   &gstMI_Fire_MRSmoke_MainOrAlt,
   &gstMI_Fire_MRSmoke_FlashFireHat,
   &gstMI_Fire_MRSmoke_ShuntTrip,
};
static struct st_ui_screen__menu gstMenu_Setup_Fire_MRSmoke =
{
   .psTitle = "MR Smoke",
   .pastMenuItems = &gastMenuItems_Fire_MRSmoke,
   .ucNumItems = sizeof(gastMenuItems_Fire_MRSmoke) / sizeof(gastMenuItems_Fire_MRSmoke[ 0 ]),
};

//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Fire_PITSmoke_MainOrAlt =
{
      .psTitle = "Main Or Alt",
      .pstUGS_Next = &gstUGS_Fire_PITSmoke_MainOrAlt,
};
static struct st_ui_menu_item gstMI_Fire_PITSmoke_FlashFireHat =
{
      .psTitle = "Flash Fire Hat",
      .pstUGS_Next = &gstUGS_Fire_PITSmoke_FlashFireHat,
};
static struct st_ui_menu_item gstMI_Fire_PITSmoke_ShuntTrip =
{
      .psTitle = "Shunt Trip",
      .pstUGS_Next = &gstUGS_Fire_PITSmoke_ShuntTrip,
};
static struct st_ui_menu_item * gastMenuItems_Fire_PITSmoke[] =
{
   &gstMI_Fire_PITSmoke_MainOrAlt,
   &gstMI_Fire_PITSmoke_FlashFireHat,
   &gstMI_Fire_PITSmoke_ShuntTrip,
};
static struct st_ui_screen__menu gstMenu_Setup_Fire_PITSmoke =
{
   .psTitle = "PIT Smoke",
   .pastMenuItems = &gastMenuItems_Fire_PITSmoke,
   .ucNumItems = sizeof(gastMenuItems_Fire_PITSmoke) / sizeof(gastMenuItems_Fire_PITSmoke[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Fire_Hoistway2Smoke_MainOrAlt =
{
      .psTitle = "Main or Alt",
      .pstUGS_Next = &gstUGS_Fire_Hoistway2Smoke_MainOrAlt,
};
static struct st_ui_menu_item gstMI_Fire_Hoistway2Smoke_FlashFireHat =
{
      .psTitle = "Flash Fire Hat",
      .pstUGS_Next = &gstUGS_Fire_Hoistway2Smoke_FlashFireHat,
};
static struct st_ui_menu_item gstMI_Fire_Hoistway2Smoke_ShuntTrip =
{
      .psTitle = "Shunt Trip",
      .pstUGS_Next = &gstUGS_Fire_Hoistway2Smoke_ShuntTrip,
};
static struct st_ui_menu_item * gastMenuItems_Fire_Hoistway2Smoke[] =
{

   &gstMI_Fire_Hoistway2Smoke_MainOrAlt,
   &gstMI_Fire_Hoistway2Smoke_FlashFireHat,
   &gstMI_Fire_Hoistway2Smoke_ShuntTrip,
};
static struct st_ui_screen__menu gstMenu_Setup_Fire_Hoistway2Smoke =
{
   .psTitle = "Hoistway 2 Smoke",
   .pastMenuItems = &gastMenuItems_Fire_Hoistway2Smoke,
   .ucNumItems = sizeof(gastMenuItems_Fire_Hoistway2Smoke) / sizeof(gastMenuItems_Fire_Hoistway2Smoke[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Fire_MR2Smoke_MainOrAlt =
{
      .psTitle = "Main or Alt",
      .pstUGS_Next = &gstUGS_Fire_MR2Smoke_MainOrAlt,
};
static struct st_ui_menu_item gstMI_Fire_MR2Smoke_FlashFireHat =
{
      .psTitle = "Flash Fire Hat",
      .pstUGS_Next = &gstUGS_Fire_MR2Smoke_FlashFireHat,
};
static struct st_ui_menu_item gstMI_Fire_MR2Smoke_ShuntTrip =
{
      .psTitle = "Shunt Trip",
      .pstUGS_Next = &gstUGS_Fire_MR2Smoke_ShuntTrip,
};
static struct st_ui_menu_item * gastMenuItems_Fire_MR2Smoke[] =
{
   &gstMI_Fire_MR2Smoke_MainOrAlt,
   &gstMI_Fire_MR2Smoke_FlashFireHat,
   &gstMI_Fire_MR2Smoke_ShuntTrip,
};
static struct st_ui_screen__menu gstMenu_Setup_Fire_MR2Smoke =
{
   .psTitle = "MR 2 Smoke",
   .pastMenuItems = &gastMenuItems_Fire_MR2Smoke,
   .ucNumItems = sizeof(gastMenuItems_Fire_MR2Smoke) / sizeof(gastMenuItems_Fire_MR2Smoke[ 0 ]),
};

//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Fire_EnableAltMachineRoom =
{
      .psTitle = "Enable Alt. MR",
      .pstUGS_Next = &gstUGS_Fire_EnableAltMachineRoom,
};
static struct st_ui_menu_item gstMI_Fire_Hoistway2Smoke =
{
      .psTitle = "HW 2 Smoke",
      .pstUGS_Next = &gstUGS_Fire_Hoistway2Smoke,
};
static struct st_ui_menu_item gstMI_Fire_MR2Smoke =
{
      .psTitle = "MR 2 Smoke",
      .pstUGS_Next = &gstUGS_Fire_MR2Smoke,
};

static struct st_ui_menu_item * gastMenuItems_Fire_AltMachineRoom[] =
{

   &gstMI_Fire_EnableAltMachineRoom,
   &gstMI_Fire_Hoistway2Smoke,
   &gstMI_Fire_MR2Smoke,
};
static struct st_ui_screen__menu gstMenu_Setup_Fire_AltMachineRoom =
{
   .psTitle = "Alt Machine Room",
   .pastMenuItems = &gastMenuItems_Fire_AltMachineRoom,
   .ucNumItems = sizeof(gastMenuItems_Fire_AltMachineRoom) / sizeof(gastMenuItems_Fire_AltMachineRoom[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Fire_MainRecall_Floor =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_MainRecall_Floor,
};
struct st_ui_generic_screen gstUGS_Fire_MainRecall_Opening =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_MainRecall_Opening,
};

struct st_ui_generic_screen gstUGS_Fire_AltRecall_Floor =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_AltRecall_Floor,
};
struct st_ui_generic_screen gstUGS_Fire_AltRecall_Opening =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_AltRecall_Opening,
};

//---------------------------------------------------------------
// Main Smoke
struct st_ui_generic_screen gstUGS_Fire_MainSmoke_MainOrAlt =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_MainSmoke_MainOrAlt,
};
struct st_ui_generic_screen gstUGS_Fire_MainSmoke_FlashFireHat =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_MainSmoke_FlashFireHat,
};
struct st_ui_generic_screen gstUGS_Fire_MainSmoke_ShuntTrip =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_MainSmoke_ShuntTrip,
};

//---------------------------------------------------------------
// Alt Smoke
struct st_ui_generic_screen gstUGS_Fire_AltSmoke_MainOrAlt =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_AltSmoke_MainOrAlt ,
};
struct st_ui_generic_screen gstUGS_Fire_AltSmoke_FlashFireHat =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_AltSmoke_FlashFireHat,
};
struct st_ui_generic_screen gstUGS_Fire_AltSmoke_ShuntTrip =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_AltSmoke_ShuntTrip,
};

//---------------------------------------------------------------
// Hoistway Smoke
struct st_ui_generic_screen gstUGS_Fire_HoistwaySmoke_MainOrAlt =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_HoistwaySmoke_MainOrAlt,
};
struct st_ui_generic_screen gstUGS_Fire_HoistwaySmoke_FlashFireHat =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_HoistwaySmoke_FlashFireHat,
};
struct st_ui_generic_screen gstUGS_Fire_HoistwaySmoke_ShuntTrip =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_HoistwaySmoke_ShuntTrip,
};

//---------------------------------------------------------------
// MR Smoke
struct st_ui_generic_screen gstUGS_Fire_MRSmoke_MainOrAlt =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_MRSmoke_MainOrAlt,
};
struct st_ui_generic_screen gstUGS_Fire_MRSmoke_FlashFireHat =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_MRSmoke_FlashFireHat,
};
struct st_ui_generic_screen gstUGS_Fire_MRSmoke_ShuntTrip =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_MRSmoke_ShuntTrip ,
};

//---------------------------------------------------------------
// PIT Smoke
struct st_ui_generic_screen gstUGS_Fire_PITSmoke_MainOrAlt =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_PITSmoke_MainOrAlt,
};
struct st_ui_generic_screen gstUGS_Fire_PITSmoke_FlashFireHat =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_PITSmoke_FlashFireHat,
};
struct st_ui_generic_screen gstUGS_Fire_PITSmoke_ShuntTrip =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_PITSmoke_ShuntTrip ,
};

struct st_ui_generic_screen gstUGS_Fire_SmokeConfig =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_SmokeConfig,
};

struct st_ui_generic_screen gstUGS_Fire_EnableAltMachineRoom =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_EnableAltMachineRoom,
};
//---------------------------------------------------------------
// Hoistway Smoke
struct st_ui_generic_screen gstUGS_Fire_Hoistway2Smoke_MainOrAlt =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_Hoistway2Smoke_MainOrAlt,
};
struct st_ui_generic_screen gstUGS_Fire_Hoistway2Smoke_FlashFireHat =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_Hoistway2Smoke_FlashFireHat,
};
struct st_ui_generic_screen gstUGS_Fire_Hoistway2Smoke_ShuntTrip =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_Hoistway2Smoke_ShuntTrip,
};

//---------------------------------------------------------------
// MR Smoke
struct st_ui_generic_screen gstUGS_Fire_MR2Smoke_MainOrAlt =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_MR2Smoke_MainOrAlt,
};
struct st_ui_generic_screen gstUGS_Fire_MR2Smoke_FlashFireHat =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_MR2Smoke_FlashFireHat,
};
struct st_ui_generic_screen gstUGS_Fire_MR2Smoke_ShuntTrip =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Fire_MR2Smoke_ShuntTrip ,
};

struct st_ui_generic_screen gstUGS_Fire_MainRecall =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Fire_MainRecall,
};
struct st_ui_generic_screen gstUGS_Fire_AltRecall =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Fire_AltRecall,
};
struct st_ui_generic_screen gstUGS_Fire_MainSmoke =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Fire_MainSmoke,
};
struct st_ui_generic_screen gstUGS_Fire_AltSmoke =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Fire_AltSmoke,
};
struct st_ui_generic_screen gstUGS_Fire_HoistwaySmoke =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Fire_HoistwaySmoke,
};
struct st_ui_generic_screen gstUGS_Fire_MRSmoke =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Fire_MRSmoke,
};
struct st_ui_generic_screen gstUGS_Fire_PITSmoke =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Fire_PITSmoke,
};
struct st_ui_generic_screen gstUGS_Fire_Hoistway2Smoke =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Fire_Hoistway2Smoke,
};
struct st_ui_generic_screen gstUGS_Fire_MR2Smoke =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Fire_MR2Smoke,
};
struct st_ui_generic_screen gstUGS_Menu_Setup_Fire =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Fire,
};
struct st_ui_generic_screen gstUGS_Fire_AltMachineRoom =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Fire_AltMachineRoom,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_Edit_Fire_ParamBit( uint32_t ulParamNumber, char * psTitle )
{
   static struct st_param_edit_menu stParamEdit;

   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = ulParamNumber;
   stParamEdit.uwParamIndex_End   = stParamEdit.uwParamIndex_Start;
   stParamEdit.psTitle = psTitle;

   UI_ParamEditSceenTemplate(&stParamEdit);
}

static void UI_FFS_Fire_MainRecall_Floor( void )
{
   static struct st_param_edit_menu stParamEdit;

   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__FireRecallFloorM;
   stParamEdit.uwParamIndex_End = enPARAM8__FireRecallFloorM;
   stParamEdit.psTitle = "Main Recall Floor";
   stParamEdit.psUnit = "";
   
   UI_ParamEditSceenTemplate(&stParamEdit);
}
static void UI_FFS_Fire_MainRecall_Opening( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__FireRecallDoorM, "Main: Use Rear Doors" );
}

static void UI_FFS_Fire_AltRecall_Floor( void )
{
	static struct st_param_edit_menu stParamEdit;
	stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
	stParamEdit.uwParamIndex_Start = enPARAM8__FireRecallFloorA;
	stParamEdit.uwParamIndex_End = enPARAM8__FireRecallFloorA;
	stParamEdit.psTitle = "Alt Recall Floor";
	stParamEdit.psUnit = "";
	UI_ParamEditSceenTemplate(&stParamEdit);
}

static void UI_FFS_Fire_AltRecall_Opening( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__FireRecallDoorA, "Alt: Use Rear Doors" );
}

//---------------------------------------------------------------
// MAin Smoke
static void UI_FFS_Fire_MainSmoke_MainOrAlt( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_Main_UseAltFloor, "Main: Use Alt Floor" );
}

static void UI_FFS_Fire_MainSmoke_FlashFireHat( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_Main_FlashFireHat, "Main: Flash Fire Hat" );
}

static void UI_FFS_Fire_MainSmoke_ShuntTrip( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_Main_ShuntOnRecall, "Main: Shunt On Recall" );
}

//---------------------------------------------------------------
// Alt Smoke
static void UI_FFS_Fire_AltSmoke_MainOrAlt( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_Alt_UseAltFloor, "Alt: Use Alt Floor" );
}

static void UI_FFS_Fire_AltSmoke_FlashFireHat( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_Alt_FlashFireHat, "Alt: Flash Fire Hat" );
}

static void UI_FFS_Fire_AltSmoke_ShuntTrip( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_Alt_ShuntOnRecall, "Alt: Shunt On Recall" );
}

//---------------------------------------------------------------
// Hoistway Smoke
static void UI_FFS_Fire_HoistwaySmoke_MainOrAlt( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_Hoistway_UseAltFloor, "H: Use Alt Floor" );
}

static void UI_FFS_Fire_HoistwaySmoke_FlashFireHat( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_Hoistway_FlashFireHat, "H: Flash Fire Hat" );
}

static void UI_FFS_Fire_HoistwaySmoke_ShuntTrip( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_Hoistway_ShuntOnRecall, "H: Shunt On Recall" );
}

//---------------------------------------------------------------
// MR Smoke
static void UI_FFS_Fire_MRSmoke_MainOrAlt( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_MachineRoom_UseAltFloor, "MR: Use Alt Floor" );
}

static void UI_FFS_Fire_MRSmoke_FlashFireHat( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_MachineRoom_FlashFireHat, "MR: Flash Fire Hat" );
}

static void UI_FFS_Fire_MRSmoke_ShuntTrip( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_MachineRoom_ShuntOnRecall, "MR: Shunt On Recall" );
}

//---------------------------------------------------------------
static void UI_FFS_Fire_PITSmoke_MainOrAlt( void )
{
   UI_FFS_Edit_Fire_ParamBit(enPARAM1__Fire__Smoke_Pit_UseAltFloor, "PIT: Use Alt Floor" );
}

static void UI_FFS_Fire_PITSmoke_FlashFireHat( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_Pit_FlashFireHat, "PIT: Flash Fire Hat" );
}

static void UI_FFS_Fire_PITSmoke_ShuntTrip( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_Pit_ShuntOnRecall, "PIT: Shunt On Recall" );
}


static char *(SmokeConfigArray[]) =
{
		"Reset To Exit Phase 1",
		"Dis Rstctr On Phase 2",
		"Phase 2 Swing Reopen Disabled",
		"Group 3 Type Hold Switch",
		"Ignore Locks Jumped On Phase 2",
		"Fire Stop Switch Kills Door Operator",
		"DOL to Exit Phase 2",
		"Exit Phase 1 At Main Recall Only",
		"OK To Stop Outside Door Zone",
		"Allow Reset with Active Smoke",
		"Hat Flash Ignore Order",
		"Momemntary Door Close Button",
		"Flash Lobby Fire Lamp",
		"Remote and Main To Override Smoke",
		"Enable PHE on Ph2",
		"Door Open On Hold",
};

static void UI_FFS_Fire_SmokeConfig( void )
{
	// Add param stuff
	static struct st_param_edit_menu stParamEdit;
	stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
	stParamEdit.uwParamIndex_Start = enPARAM1__Fire__ResetToExitPhase1;
	stParamEdit.uwParamIndex_End = enPARAM1__Fire__DoorOpenOnHold;
	stParamEdit.psTitle = "Smoke Configuration";
	stParamEdit.psUnit = "";
	stParamEdit.ucaPTRChoiceList = &SmokeConfigArray;
	stParamEdit.ulValue_Max = (sizeof(SmokeConfigArray)/sizeof(*SmokeConfigArray)) - 1;
	UI_ParamEditSceenTemplate(&stParamEdit);

}

static void UI_FFS_Fire_EnableAltMachineRoom( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__EnableAltMachineRoom, "Enable Alt MR" );
}

//---------------------------------------------------------------
// Hoistway 2 Smoke
static void UI_FFS_Fire_Hoistway2Smoke_MainOrAlt( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_Hoistway2_UseAltFloor, "Use Alt Floor" );
}

static void UI_FFS_Fire_Hoistway2Smoke_FlashFireHat( void )
{
   UI_FFS_Edit_Fire_ParamBit(enPARAM1__Fire__Smoke_Hoistway2_FlashFireHat, "Flash Fire Hat" );
}

static void UI_FFS_Fire_Hoistway2Smoke_ShuntTrip( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_Hoistway2_ShuntOnRecall, "Shunt On Recall" );
}

//---------------------------------------------------------------
// MR 2 Smoke
static void UI_FFS_Fire_MR2Smoke_MainOrAlt( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_MachineRoom2_UseAltFloor, "Use Alt Floor" );
}

static void UI_FFS_Fire_MR2Smoke_FlashFireHat( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_MachineRoom2_FlashFireHat, "Flash Fire Hat" );
}

static void UI_FFS_Fire_MR2Smoke_ShuntTrip( void )
{
   UI_FFS_Edit_Fire_ParamBit( enPARAM1__Fire__Smoke_MachineRoom2_ShuntOnRecall, "Shunt On Recall" );
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
