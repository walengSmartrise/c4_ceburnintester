/******************************************************************************
 *
 * @file     ui_menu_status.c
 * @brief    Status Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_ui_menu_item gstMI_Inputs =
{
   .psTitle = "Inputs",
   .pstUGS_Next = &gstUGS_Status_Inputs_Functions,
};

static struct st_ui_menu_item gstMI_Outputs =
{
   .psTitle = "Outputs",
   .pstUGS_Next = &gstUGS_Status_Outputs_Functions,
};
static struct st_ui_menu_item gstMI_ExpansionStatus =
{
   .psTitle = "Expansion Status",
   .pstUGS_Next = &gstUGS_Menu_ExpansionStatus,
};
static struct st_ui_menu_item gstMI_HallBoardStatus =
{
   .psTitle = "Hall Board Status",
   .pstUGS_Next = &gstUGS_HallBoardStatus,
};
static struct st_ui_menu_item gstMI_HallLanternStatus =
{
   .psTitle = "Hall Lantern Status",
   .pstUGS_Next = &gstUGS_HallLanternStatus,
};
static struct st_ui_menu_item gstMI_HallSecurityStatus =
{
   .psTitle = "Hall Security Status",
   .pstUGS_Next = &gstUGS_HallSecurityStatus,
};

static struct st_ui_menu_item gstMI_ClockStatus =
{
   .psTitle = "Clock",
   .pstUGS_Next = &gstUGS_ClockStatus,
};
static struct st_ui_menu_item gstMI_CPLD_Status =
{
   .psTitle = "CPLD Status",
   .pstUGS_Next = &gstUGS_Menu_CPLD_Status,
};
static struct st_ui_menu_item gstMI_BrakeStatus =
{
   .psTitle = "Brake Status",
   .pstUGS_Next = &gstUGS_BrakeStatus,
};
static struct st_ui_menu_item gstMI_EBrakeStatus =
{
   .psTitle = "EBrake Status",
   .pstUGS_Next = &gstUGS_EBrakeStatus,
};
static struct st_ui_menu_item gstMI_LoadWeigherStatus =
{
   .psTitle = "Load Weigher Status",
   .pstUGS_Next = &gstUGS_LoadWeigher,
};
static struct st_ui_menu_item gstMI_ShieldStatus =
{
   .psTitle = "DAD Status",
   .pstUGS_Next = &gstUGS_ShieldStatus,
};
static struct st_ui_menu_item gstMI_HallCallStatus =
{
   .psTitle = "Hall Call Status",
   .pstUGS_Next = &gstUGS_Menu_HallCallStatus,
};
static struct st_ui_menu_item gstMI_RiserStatus =
{
   .psTitle = "Riser Board Status",
   .pstUGS_Next = &gstUGS_RiserStatus,
};
static struct st_ui_menu_item gstMI_EPowerStatus =
{
   .psTitle = "E-Power Status",
   .pstUGS_Next = &gstUGS_EPowerStatus,
};
static struct st_ui_menu_item gstMI_EMSStatus =
{
   .psTitle = "EMS Status",
   .pstUGS_Next = &gstUGS_EMSStatus,
};
static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_Inputs,
   &gstMI_Outputs,
   &gstMI_BrakeStatus,
   &gstMI_EBrakeStatus,
   &gstMI_ExpansionStatus,
   &gstMI_RiserStatus,
   &gstMI_HallBoardStatus,
   &gstMI_HallLanternStatus,
   &gstMI_HallSecurityStatus,
   &gstMI_HallCallStatus,
   &gstMI_ShieldStatus,
   &gstMI_ClockStatus,
   &gstMI_CPLD_Status,
   &gstMI_LoadWeigherStatus,
   &gstMI_EPowerStatus,
   &gstMI_EMSStatus,
};

static struct st_ui_screen__menu gstMenu =
{
   .psTitle = "Status",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_Menu_Status =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
