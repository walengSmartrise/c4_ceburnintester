/******************************************************************************
 *
 * @file     ui_ffs_Shield_CarPark.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include "GlobalData.h"
#include "operation.h"
#include "masterCommand.h"
#include "hallData.h"
#include "carData.h"
#include "carDestination.h"
#include "group_net_data.h"
#include "dynamic_parking.h"
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_SH_CarPk( void );
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_SH_CarPk =
{
   .pfnDraw = UI_FFS_SH_CarPk,
};

static uint8_t ucCursorX = 0;

static uint8_t ucCar = 0;
static un_sdata_datagram uDatagram;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_SH_CarPk =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SH_CarPk,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define DOOR_OPEN_FLAG                    (0x80)
#define LANDING_MASK                      (0x7f)

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void DataGramInit()
{

   static uint8_t bFirst = 1;

   if(bFirst)
   {
      bFirst = 0;

      memset( &uDatagram, 0, sizeof(un_sdata_datagram));
   }
}
/*-----------------------------------------------------------------------------

|01234567890123456789|

|Car Landing Door DG |
|01     00    DC Send|
|*                   |
|GD:00000000 00000000|

-----------------------------------------------------------------------------*/
static void UpdateScreen( void )
{
   LCD_Char_Clear();
   /* Line 1 */
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteString( "Car Landing Door DG" );

   /* Line 2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteInteger_ExactLength(ucCar+1, 2);
   LCD_Char_GotoXY( 7, 1 );
   LCD_Char_WriteInteger_ExactLength(uDatagram.auc8[ucCar] & LANDING_MASK, 2);
   LCD_Char_GotoXY( 13, 1 );
   if( uDatagram.auc8[ucCar] & DOOR_OPEN_FLAG )
   {
      LCD_Char_WriteString("DO");
   }
   else
   {
      LCD_Char_WriteString("DC");
   }
   
   LCD_Char_WriteString( " Send");

   /* Line 3 */
   switch( ucCursorX )
   {
      case 0:
         LCD_Char_GotoXY( 1, 2 );
         break;
      case 1:
         LCD_Char_GotoXY( 7, 2 );
         break;
      case 2:
         LCD_Char_GotoXY( 8, 2 );
         break;
      case 3:
         LCD_Char_GotoXY( 13, 2 );
         break;
      case 4:
         LCD_Char_GotoXY( 17, 2 );
         break;
      default:
         break;
   }
   LCD_Char_WriteString("*");

   /* Line 4 */
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("DG:");

   LCD_Char_GotoXY( 3, 3 );
   LCD_Char_WriteHex_ExactLength(uDatagram.ai32[1], 8);
   LCD_Char_WriteString(" ");
   LCD_Char_WriteHex_ExactLength(uDatagram.ai32[0], 8);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ControlScreen( void )
{
   enum en_keypresses eKeypress = Button_GetKeypress();
   int8_t cEditLanding = uDatagram.auc8[ucCar] & LANDING_MASK;
   int8_t bDoorOpen = uDatagram.auc8[ucCar] & DOOR_OPEN_FLAG;
   uint8_t oldCar = ucCar;

   switch(eKeypress)
   {
      case enKEYPRESS_LEFT:
         if(ucCursorX)
         {
            ucCursorX--;
         }
         else
         {
            PrevScreen();
         }
         break;

      case enKEYPRESS_RIGHT:
         if(ucCursorX < 4)
         {
            ucCursorX++;
         }
         break;

      case enKEYPRESS_UP:
         switch( ucCursorX )
         {
            case 0:
               ucCar += 1;
               break;
            case 1:
               cEditLanding += 10;
               break;
            case 2:
               cEditLanding += 1;
               break;
            case 3:
               if( bDoorOpen )
               {
                  bDoorOpen = 0;
               }
               else
               {
                  bDoorOpen = DOOR_OPEN_FLAG;
               }
               break;
            default:
               break;
         }
         break;

      case enKEYPRESS_DOWN:
         switch( ucCursorX )
         {
            case 0:
               if(ucCar)
               {
                  ucCar -= 1;
               }
               break;
            case 1:
               cEditLanding -= 10;
               break;
            case 2:
               cEditLanding -= 1;
               break;
            case 3:
               if( bDoorOpen )
               {
                  bDoorOpen = 0;
               }
               else
               {
                  bDoorOpen = DOOR_OPEN_FLAG;
               }
               break;
            default:
               break;
         }
         break;

      case enKEYPRESS_ENTER:
         if( ucCursorX == 4)
         {
            DynamicParking_UnloadDynamicParkingDatagram(&uDatagram);
            ucCursorX--;
         }
         break;

      default:
         break;
   }

   /* Bound values */
   if( cEditLanding > MAX_NUM_FLOORS )
   {
      cEditLanding = MAX_NUM_FLOORS;
   }
   else if( cEditLanding < 0 )
   {
      cEditLanding = 0;
   }

   if(cEditLanding == 0)
   {
      bDoorOpen = 0;
   }

   if(ucCar >= MAX_GROUP_CARS)
   {
      ucCar = MAX_GROUP_CARS - 1;
   }

   // if car changed load landing parameter from new offset
   if ( oldCar != ucCar)
   {
      cEditLanding = uDatagram.auc8[ucCar] & LANDING_MASK;
      bDoorOpen = uDatagram.auc8[ucCar] & DOOR_OPEN_FLAG;
   }

   uDatagram.auc8[ucCar] = cEditLanding | ((bDoorOpen) ? DOOR_OPEN_FLAG : 0);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SH_CarPk( void )
{
   DataGramInit();
   ControlScreen();
   UpdateScreen();
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
