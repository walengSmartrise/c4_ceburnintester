/******************************************************************************
 *
 * @file     ui_menu_io.c
 * @brief    I/O Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note     Menus for displaying expansion board status
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "GlobalData.h"
#include "expData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_ExpansionStatus1( void );
static void UI_FFS_ExpansionStatus2( void );
static void UI_FFS_ExpansionStatus3( void );
static void UI_FFS_ExpansionStatus4( void );
static void UI_FFS_ExpansionStatus5( void );
static void UI_FFS_ExpansionStatus6( void );
static void UI_FFS_ExpansionStatus7( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_ExpansionStatus1 =
{
   .pfnDraw = UI_FFS_ExpansionStatus1,
};
static struct st_ui_screen__freeform gstFFS_ExpansionStatus2 =
{
   .pfnDraw = UI_FFS_ExpansionStatus2,
};
static struct st_ui_screen__freeform gstFFS_ExpansionStatus3 =
{
   .pfnDraw = UI_FFS_ExpansionStatus3,
};
static struct st_ui_screen__freeform gstFFS_ExpansionStatus4 =
{
   .pfnDraw = UI_FFS_ExpansionStatus4,
};
static struct st_ui_screen__freeform gstFFS_ExpansionStatus5 =
{
   .pfnDraw = UI_FFS_ExpansionStatus5,
};
static struct st_ui_screen__freeform gstFFS_ExpansionStatus6 =
{
   .pfnDraw = UI_FFS_ExpansionStatus6,
};
static struct st_ui_screen__freeform gstFFS_ExpansionStatus7 =
{
   .pfnDraw = UI_FFS_ExpansionStatus7,
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Expansion1 =
{
   .psTitle = "Expansion 1-8",
   .pstUGS_Next = &gstUGS_ExpansionStatus1,
};
static struct st_ui_menu_item gstMI_Expansion2 =
{
   .psTitle = "Expansion 9-16",
   .pstUGS_Next = &gstUGS_ExpansionStatus2,
};
static struct st_ui_menu_item gstMI_Expansion3 =
{
   .psTitle = "Expansion 17-24",
   .pstUGS_Next = &gstUGS_ExpansionStatus3,
};
static struct st_ui_menu_item gstMI_Expansion4 =
{
   .psTitle = "Expansion 25-32",
   .pstUGS_Next = &gstUGS_ExpansionStatus4,
};
static struct st_ui_menu_item gstMI_Expansion5 =
{
   .psTitle = "Expansion 33-40",
   .pstUGS_Next = &gstUGS_ExpansionStatus5,
};
static struct st_ui_menu_item gstMI_Expansion6 =
{
   .psTitle = "Expansion 41-48",
   .pstUGS_Next = &gstUGS_ExpansionStatus6,
};
static struct st_ui_menu_item gstMI_Expansion7 =
{
   .psTitle = "Expansion 49-56",
   .pstUGS_Next = &gstUGS_ExpansionStatus7,
};
static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_Expansion1,
   &gstMI_Expansion2,
   &gstMI_Expansion3,
   &gstMI_Expansion4,
   &gstMI_Expansion5,
   &gstMI_Expansion6,
   &gstMI_Expansion7,
};
static struct st_ui_screen__menu gstMenu =
{
   .psTitle = "Expansion Status",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};

static uint8_t ucMasterIndex = 0;
static int8_t cCursorY = 0;
static char const* const pasErrors[NUM_RIS_EXP_ERROR] =
{
      "Unknown",//ERROR_RIS_EXP__UNKNOWN,
      "None",//ERROR_RIS_EXP__NONE,
      "POR Rst",//ERROR_RIS_EXP__RESET_POR,
      "WDT Rst",//ERROR_RIS_EXP__RESET_WDT,
      "BOD Rst",//ERROR_RIS_EXP__RESET_BOD,
      "Comm. Group",//ERROR_RIS_EXP__COM_GROUP,
      "Comm. Hall",//ERROR_RIS_EXP__COM_HALL,
      "Comm. Car",//ERROR_RIS_EXP__COM_CAR,
      "Comm. Master",//ERROR_RIS_EXP__COM_MASTER,
      "Driver",//ERROR_RIS_EXP__DRIVER_FLT,
      "Address",//ERROR_RIS_EXP__ADDRESS, // Two boards with same DIP address
      "CAN1 Rst",//ERROR_RIS_EXP__BUS_RESET_CAN1,
      "CAN2 Rst",//ERROR_RIS_EXP__BUS_RESET_CAN2,
      "Comm. Slave",//ERROR_RIS_EXP__COM_SLAVE,
      "HB Offline",//ERROR_RIS_EXP__HALL_BOARD_OFFLINE,
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_ExpansionStatus1 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ExpansionStatus1,
};
struct st_ui_generic_screen gstUGS_ExpansionStatus2 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ExpansionStatus2,
};
struct st_ui_generic_screen gstUGS_ExpansionStatus3 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ExpansionStatus3,
};
struct st_ui_generic_screen gstUGS_ExpansionStatus4 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ExpansionStatus4,
};
struct st_ui_generic_screen gstUGS_ExpansionStatus5 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ExpansionStatus5,
};
struct st_ui_generic_screen gstUGS_ExpansionStatus6 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ExpansionStatus6,
};
struct st_ui_generic_screen gstUGS_ExpansionStatus7 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ExpansionStatus7,
};
//-----------------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_ExpansionStatus =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
|01234567890123456789|

|EXP01 - ONLINE      |
|IN:  0x01           |
|OUT: 0xFF           |
|ERR: None           |
 ----------------------------------------------------------------------------*/
static void Print_Screen(void)
{
   LCD_Char_Clear();

   /* Title */
   uint8_t ucExpansionNumber = ucMasterIndex*8 + cCursorY + 1;
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteString("EXP");
   LCD_Char_WriteInteger_MinLength(ucExpansionNumber, 2);

   if( ExpData_GetOnlineFlag(ucMasterIndex, cCursorY) )
   {
      LCD_Char_WriteString(" - ONLINE");
   }
   else
   {
      LCD_Char_WriteString(" - OFFLINE");
   }

   /* Inputs */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("IN: ");
   uint8_t ucInputs = GetExpansionInputs(ucMasterIndex, cCursorY);
   for(uint8_t i = 0; i < 8; i++)
   {
      if(Sys_Bit_Get(&ucInputs, i))
      {
         LCD_Char_WriteInteger_ExactLength(i+1, 1);
      }
      else
      {
         LCD_Char_WriteString(".");
      }
   }

   /* Outputs */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("OUT: ");
   uint8_t ucOutputs = GetExpansionOutputs(ucMasterIndex, cCursorY);
   for(uint8_t i = 0; i < 8; i++)
   {
      if(Sys_Bit_Get(&ucOutputs, i))
      {
         LCD_Char_WriteInteger_ExactLength(i+1, 1);
      }
      else
      {
         LCD_Char_WriteString(".");
      }
   }

   /* Error */
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("ERR: ");
   LCD_Char_WriteString(pasErrors[GetExpansionError(ucMasterIndex, cCursorY)]);
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
#define LIMIT_CURSOR_Y_VALUE (8)
static void ControlScreen(void)
{
   enum en_keypresses enKeypress = Button_GetKeypress();
   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      cCursorY++;
   }
   else if ( enKeypress == enKEYPRESS_DOWN )
   {
      cCursorY--;
   }

   /* Bound value */
   if( cCursorY >= LIMIT_CURSOR_Y_VALUE )
   {
      cCursorY = LIMIT_CURSOR_Y_VALUE-1;
   }
   else if( cCursorY < 0 )
   {
      cCursorY = 0;
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_ExpansionStatus1( void )
{
   ucMasterIndex = 0;
   ControlScreen();
   Print_Screen();
}
static void UI_FFS_ExpansionStatus2( void )
{
   ucMasterIndex = 1;
   ControlScreen();
   Print_Screen();
}
static void UI_FFS_ExpansionStatus3( void )
{
   ucMasterIndex = 2;
   ControlScreen();
   Print_Screen();
}
static void UI_FFS_ExpansionStatus4( void )
{
   ucMasterIndex = 3;
   ControlScreen();
   Print_Screen();
}
static void UI_FFS_ExpansionStatus5( void )
{
   ucMasterIndex = 4;
   ControlScreen();
   Print_Screen();
}
static void UI_FFS_ExpansionStatus6( void )
{
   ucMasterIndex = 5;
   ControlScreen();
   Print_Screen();
}
static void UI_FFS_ExpansionStatus7( void )
{
   ucMasterIndex = 6;
   ControlScreen();
   Print_Screen();
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
