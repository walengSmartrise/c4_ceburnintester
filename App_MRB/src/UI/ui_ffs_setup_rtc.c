/******************************************************************************
 *
 * @file     ui_ffs_setup_rtc.c
 * @brief    RTC Setup UI page
 * @version  V1.00
 * @date     1, October 2018
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "carData.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include "sru_b.h"
#include "shieldData.h"
#include <math.h>
#include <time.h>
#include <string.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Setup_RTC_Time( void );
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_time_edit_menu stNewMenu;

static struct st_ui_screen__freeform gstFFS_Setup_RTC_Time =
{
      .pfnDraw = UI_FFS_Setup_RTC_Time,
};

static uint16_t uwTimeoutCounter;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Menu_Setup_RTC =
{
      .ucType = enUI_STYPE__FREEFORM,
      .pvReference = &gstFFS_Setup_RTC_Time,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MAX_CURSOR_X_POS (1)
#define START_CURSOR_POS_VALUE (7U)

#define CLOCK_UPDATE_TIMEOUT_1MS (1000)

#define SECONDS_IN_MINUTE (60)
#define SECONDS_IN_HOUR (3600)
#define SECONDS_IN_DAY (86400)
#define SECONDS_IN_YEAR (3155760)
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
static void PrintScreen(struct st_time_edit_menu* stMenu, uint8_t ucCursorIndex)
{
   if(GetMasterDispatcherFlag())
   {
      if(stMenu->bSaving)
      {
         LCD_Char_Clear();
         LCD_Char_GotoXY(0,1);
         LCD_Char_WriteString_Centered("Saving");
      }
      else
      {
         LCD_Char_Clear();
         LCD_Char_GotoXY(0,0);
         LCD_Char_WriteString("MM/DD/YY HH:MM");
         LCD_Char_GotoXY(0,1);
         LCD_Char_WriteInteger_ExactLength(stMenu->ucMonth,2);
         LCD_Char_WriteChar('/');
         LCD_Char_WriteInteger_ExactLength(stMenu->ucDay,2);
         LCD_Char_WriteChar('/');
         LCD_Char_WriteInteger_ExactLength(stMenu->ucYear,2);
         LCD_Char_WriteChar(' ');
         LCD_Char_WriteInteger_ExactLength(stMenu->ucHour,2);
         LCD_Char_WriteChar(':');
         LCD_Char_WriteInteger_ExactLength(stMenu->ucMinutes,2);
         LCD_Char_GotoXY(ucCursorIndex,2);
         LCD_Char_WriteChar('*');

         if( stMenu->ucMonth || stMenu->ucDay || stMenu->ucYear ||
               stMenu->ucHour || stMenu->ucMinutes)
         {
            LCD_Char_GotoXY(16,1);
            LCD_Char_WriteString("Save");
         }
      }
   }
   else
   {
      LCD_Char_Clear();
      LCD_Char_GotoXY(1,1);
      LCD_Char_WriteString("Move to master car");
      LCD_Char_GotoXY(5,2);
      LCD_Char_WriteString("to set RTC");
   }

}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static uint32_t ConvertToUTC( struct st_time_edit_menu* stMenu )
{
   struct tm t;
   time_t t_date;

   t.tm_year = (2000 + stMenu->ucYear) - 1900;
   t.tm_mon = stMenu->ucMonth - 1;
   t.tm_mday = stMenu->ucDay;

   t.tm_hour = stMenu->ucHour;
   t.tm_min = stMenu->ucMinutes;

   t.tm_isdst = 0;

   t_date = mktime(&t);
   stMenu->time_date = t_date;

   return (uint32_t) t_date;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
#define MIN_X_CURSOR (0)
#define MAX_X_CURSOR (13)
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_TimeEdit( struct st_time_edit_menu* stMenu )
{
   static uint8_t ucCursorIndex = 0;
   static uint8_t ucFieldIndex = 0;

   PrintScreen(stMenu, ucCursorIndex);

   enum en_keypresses enKeypress = Button_GetKeypress();
   switch(enKeypress)
   {
      case enKEYPRESS_RIGHT:
         ucCursorIndex = (ucCursorIndex < MAX_X_CURSOR ) ? ucCursorIndex + 1 : 16;
         ucFieldIndex = (ucFieldIndex < 15) ? ucFieldIndex + 1 : ucFieldIndex;
         break;
      case enKEYPRESS_LEFT:
         if( (ucCursorIndex > MIN_X_CURSOR) && (ucCursorIndex <= MAX_X_CURSOR) )
         {
            ucCursorIndex--;
            ucFieldIndex--;
         }
         else if( ucCursorIndex > MAX_X_CURSOR )
         {
            ucCursorIndex = MAX_X_CURSOR;
            ucFieldIndex--;
         }
         else
         {
            ucCursorIndex = 0;
            ucFieldIndex = 0;
            memcpy(stMenu, &stNewMenu, sizeof(struct st_time_edit_menu));
            PrevScreen();
         }
         break;
      case enKEYPRESS_UP:
         switch(ucFieldIndex)
         {
            case 0:
               stMenu->ucMonth += 10;
               if( stMenu->ucMonth > 12)
               {
                  stMenu->ucMonth = 0;
               }
               break;
            case 1:
               stMenu->ucMonth += 1;
               if( stMenu->ucMonth > 12)
               {
                  stMenu->ucMonth = 0;
               }
               break;
            case 3:
               stMenu->ucDay += 10;
               if( stMenu->ucDay > 31)
               {
                  stMenu->ucDay = 0;
               }
               break;
            case 4:
               stMenu->ucDay += 1;
               if( stMenu->ucDay > 31)
               {
                  stMenu->ucDay = 0;
               }
               break;
            case 6:
               stMenu->ucYear += 10;
               if( stMenu->ucYear > 99)
               {
                  stMenu->ucYear = 0;
               }
               break;
            case 7:
               stMenu->ucYear += 1;
               if( stMenu->ucYear > 99)
               {
                  stMenu->ucYear = 0;
               }
               break;
            case 9:
               stMenu->ucHour += 10;
               if( stMenu->ucHour > 23)
               {
                  stMenu->ucHour = 0;
               }
               break;
            case 10:
               stMenu->ucHour += 1;
               if( stMenu->ucHour > 23)
               {
                  stMenu->ucHour = 0;
               }
               break;
            case 12:
               stMenu->ucMinutes += 10;
               if( stMenu->ucMinutes > 59)
               {
                  stMenu->ucMinutes = 0;
               }
               break;
            case 13:
               stMenu->ucMinutes += 1;
               if( stMenu->ucMinutes > 59)
               {
                  stMenu->ucMinutes = 0;
               }
               break;
            case 14: //save
            default:
               break;
         }
         break;
            case enKEYPRESS_DOWN:
               switch(ucFieldIndex)
               {
                  case 0:
                     stMenu->ucMonth -= 10;
                     if( stMenu->ucMonth > 12)
                     {
                        stMenu->ucMonth = 12;
                     }
                     break;
                  case 1:
                     stMenu->ucMonth -= 1;
                     if( stMenu->ucMonth > 12)
                     {
                        stMenu->ucMonth = 12;
                     }
                     break;
                  case 3:
                     stMenu->ucDay -= 10;
                     if( stMenu->ucDay > 31)
                     {
                        stMenu->ucDay = 31;
                     }
                     break;
                  case 4:
                     stMenu->ucDay -= 1;
                     if( stMenu->ucDay > 31)
                     {
                        stMenu->ucDay = 31;
                     }
                     break;
                  case 6:
                     stMenu->ucYear -= 10;
                     if( stMenu->ucYear > 99)
                     {
                        stMenu->ucYear = 99;
                     }
                     break;
                  case 7:
                     stMenu->ucYear -= 1;
                     if( stMenu->ucYear > 99)
                     {
                        stMenu->ucYear = 99;
                     }
                     break;
                  case 9:
                     stMenu->ucHour -= 10;
                     if( stMenu->ucHour > 23)
                     {
                        stMenu->ucHour = 23;
                     }
                     break;
                  case 10:
                     stMenu->ucHour -= 1;
                     if( stMenu->ucHour > 23)
                     {
                        stMenu->ucHour = 23;
                     }
                     break;
                  case 12:
                     stMenu->ucMinutes -= 10;
                     if( stMenu->ucMinutes > 59)
                     {
                        stMenu->ucMinutes = 59;
                     }
                     break;
                  case 13:
                     stMenu->ucMinutes -= 1;
                     if( stMenu->ucMinutes > 59)
                     {
                        stMenu->ucMinutes = 59;
                     }
                     break;
                  case 14: //save
                  default:
                     break;
               }
               break;
                  case enKEYPRESS_ENTER:
                     if(ucCursorIndex == 16)
                     {
                        time_t ulSysTime = ConvertToUTC( stMenu );
                        LoadDatagram_SyncTime( ulSysTime );
                        RTC_SetSyncTime(ulSysTime);
                        if(Param_ReadValue_1Bit(enPARAM1__EnableBoardRTC))
                        {
                           RTC_WritePeripheralTime_Unix(ulSysTime);
                        }
                        stMenu->bSaving = 1;
                        ucCursorIndex = 0;
                        ucFieldIndex = 0;
                     }

                     break;
                  default:
                     break;
   }
}

static void SetNewMenu(struct st_time_edit_menu* stNewMenu)
{
   //time_t time_date = RTC_GetFullTime_Unix();
   time_t time_date = RTC_GetLocalTime();
   struct tm* t = gmtime(&time_date);

   stNewMenu->ucMonth = t->tm_mon + 1;
   stNewMenu->ucDay = t->tm_mday;
   stNewMenu->ucYear = t->tm_year - 100;
   stNewMenu->ucHour = t->tm_hour;
   stNewMenu->ucMinutes = t->tm_min;
}

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_Setup_RTC_Time( void )
{
   static struct st_time_edit_menu stTimeEdit;
   if(!stTimeEdit.bOld)
   {
      SetNewMenu(&stTimeEdit);
      stTimeEdit.bOld = 1;
   }
   UI_TimeEdit(&stTimeEdit);

   if( uwTimeoutCounter > CLOCK_UPDATE_TIMEOUT_1MS )
   {
      memcpy(&stTimeEdit, &stNewMenu, sizeof(struct st_time_edit_menu));
      uwTimeoutCounter = 0;
   }
   else if (stTimeEdit.bSaving)
   {
      uwTimeoutCounter += MOD_RUN_PERIOD_UI_1MS;
   }

}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
