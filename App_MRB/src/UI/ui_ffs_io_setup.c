/******************************************************************************
 *
 * @file     ui_ffs_iosetup.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Inputs_MR( void );
static void UI_FFS_Setup_Inputs_CT( void );
static void UI_FFS_Setup_Inputs_COP( void );
static void UI_FFS_Setup_Inputs_RIS( void );
static void UI_FFS_Setup_Inputs_EXP1( void );
static void UI_FFS_Setup_Inputs_EXP2( void );
static void UI_FFS_Setup_Inputs_EXP3( void );
static void UI_FFS_Setup_Inputs_EXP4( void );
static void UI_FFS_Setup_Inputs_EXP5( void );
static void UI_FFS_Setup_Outputs_MR( void );
static void UI_FFS_Setup_Outputs_CT( void );
static void UI_FFS_Setup_Outputs_COP( void );
static void UI_FFS_Setup_Outputs_RIS( void );
static void UI_FFS_Setup_Outputs_EXP1( void );
static void UI_FFS_Setup_Outputs_EXP2( void );
static void UI_FFS_Setup_Outputs_EXP3( void );
static void UI_FFS_Setup_Outputs_EXP4( void );
static void UI_FFS_Setup_Outputs_EXP5( void );
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Inputs_MR =
{
   .psTitle = "Machine Room",
   .pstUGS_Next = &gstUGS_Setup_Inputs_MR,
};
static struct st_ui_menu_item gstMI_Inputs_CT =
{
   .psTitle = "Car Top",
   .pstUGS_Next = &gstUGS_Setup_Inputs_CT,
};
static struct st_ui_menu_item gstMI_Inputs_COP =
{
   .psTitle = "Car Operating Panel",
   .pstUGS_Next = &gstUGS_Setup_Inputs_COP,
};
static struct st_ui_menu_item gstMI_Inputs_RIS =
{
   .psTitle = "Riser",
   .pstUGS_Next = &gstUGS_Setup_Inputs_RIS,
};
static struct st_ui_menu_item gstMI_Inputs_EXP1 =
{
   .psTitle = "Expansion 1-8",
   .pstUGS_Next = &gstUGS_Setup_Inputs_EXP1,
};
static struct st_ui_menu_item gstMI_Inputs_EXP2 =
{
   .psTitle = "Expansion 9-16",
   .pstUGS_Next = &gstUGS_Setup_Inputs_EXP2,
};
static struct st_ui_menu_item gstMI_Inputs_EXP3 =
{
   .psTitle = "Expansion 17-24",
   .pstUGS_Next = &gstUGS_Setup_Inputs_EXP3,
};
static struct st_ui_menu_item gstMI_Inputs_EXP4 =
{
   .psTitle = "Expansion 25-32",
   .pstUGS_Next = &gstUGS_Setup_Inputs_EXP4,
};
static struct st_ui_menu_item gstMI_Inputs_EXP5 =
{
   .psTitle = "Expansion 33-40",
   .pstUGS_Next = &gstUGS_Setup_Inputs_EXP5,
};
static struct st_ui_menu_item * gastMenuItems_Inputs_Boards[] =
{
   &gstMI_Inputs_MR,
   &gstMI_Inputs_CT,
   &gstMI_Inputs_COP,
   &gstMI_Inputs_RIS,
   &gstMI_Inputs_EXP1,
   &gstMI_Inputs_EXP2,
   &gstMI_Inputs_EXP3,
   &gstMI_Inputs_EXP4,
   &gstMI_Inputs_EXP5,
};
static struct st_ui_screen__menu gstMenu_Inputs_Boards =
{
   .psTitle = "Select Board",
   .pastMenuItems = &gastMenuItems_Inputs_Boards,
   .ucNumItems = sizeof(gastMenuItems_Inputs_Boards) / sizeof(gastMenuItems_Inputs_Boards[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Outputs_MR =
{
   .psTitle = "Machine Room",
   .pstUGS_Next = &gstUGS_Setup_Outputs_MR,
};
static struct st_ui_menu_item gstMI_Outputs_CT =
{
   .psTitle = "Car Top",
   .pstUGS_Next = &gstUGS_Setup_Outputs_CT,
};
static struct st_ui_menu_item gstMI_Outputs_COP =
{
   .psTitle = "Car Operating Panel",
   .pstUGS_Next = &gstUGS_Setup_Outputs_COP,
};
static struct st_ui_menu_item gstMI_Outputs_RIS =
{
   .psTitle = "Riser",
   .pstUGS_Next = &gstUGS_Setup_Outputs_RIS,
};
static struct st_ui_menu_item gstMI_Outputs_EXP1 =
{
   .psTitle = "Expansion 1-8",
   .pstUGS_Next = &gstUGS_Setup_Outputs_EXP1,
};
static struct st_ui_menu_item gstMI_Outputs_EXP2 =
{
   .psTitle = "Expansion 9-16",
   .pstUGS_Next = &gstUGS_Setup_Outputs_EXP2,
};
static struct st_ui_menu_item gstMI_Outputs_EXP3 =
{
   .psTitle = "Expansion 17-24",
   .pstUGS_Next = &gstUGS_Setup_Outputs_EXP3,
};
static struct st_ui_menu_item gstMI_Outputs_EXP4 =
{
   .psTitle = "Expansion 25-32",
   .pstUGS_Next = &gstUGS_Setup_Outputs_EXP4,
};
static struct st_ui_menu_item gstMI_Outputs_EXP5 =
{
   .psTitle = "Expansion 33-40",
   .pstUGS_Next = &gstUGS_Setup_Outputs_EXP5,
};
static struct st_ui_menu_item * gastMenuItems_Outputs_Boards[] =
{
   &gstMI_Outputs_MR,
   &gstMI_Outputs_CT,
   &gstMI_Outputs_COP,
   &gstMI_Outputs_RIS,
   &gstMI_Outputs_EXP1,
   &gstMI_Outputs_EXP2,
   &gstMI_Outputs_EXP3,
   &gstMI_Outputs_EXP4,
   &gstMI_Outputs_EXP5,
};
static struct st_ui_screen__menu gstMenu_Outputs_Boards =
{
   .psTitle = "Select Board",
   .pastMenuItems = &gastMenuItems_Outputs_Boards,
   .ucNumItems = sizeof(gastMenuItems_Outputs_Boards) / sizeof(gastMenuItems_Outputs_Boards[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Setup_Inputs_MR =
{
   .pfnDraw = UI_FFS_Setup_Inputs_MR,
};
static struct st_ui_screen__freeform gstFFS_Setup_Inputs_CT =
{
   .pfnDraw = UI_FFS_Setup_Inputs_CT,
};
static struct st_ui_screen__freeform gstFFS_Setup_Inputs_COP =
{
   .pfnDraw = UI_FFS_Setup_Inputs_COP,
};
static struct st_ui_screen__freeform gstFFS_Setup_Inputs_RIS =
{
   .pfnDraw = UI_FFS_Setup_Inputs_RIS,
};
static struct st_ui_screen__freeform gstFFS_Setup_Inputs_EXP1 =
{
   .pfnDraw = UI_FFS_Setup_Inputs_EXP1,
};
static struct st_ui_screen__freeform gstFFS_Setup_Inputs_EXP2 =
{
   .pfnDraw = UI_FFS_Setup_Inputs_EXP2,
};
static struct st_ui_screen__freeform gstFFS_Setup_Inputs_EXP3 =
{
   .pfnDraw = UI_FFS_Setup_Inputs_EXP3,
};
static struct st_ui_screen__freeform gstFFS_Setup_Inputs_EXP4 =
{
   .pfnDraw = UI_FFS_Setup_Inputs_EXP4,
};
static struct st_ui_screen__freeform gstFFS_Setup_Inputs_EXP5 =
{
   .pfnDraw = UI_FFS_Setup_Inputs_EXP5,
};
//---------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Setup_Outputs_MR =
{
   .pfnDraw = UI_FFS_Setup_Outputs_MR,
};
static struct st_ui_screen__freeform gstFFS_Setup_Outputs_CT =
{
   .pfnDraw = UI_FFS_Setup_Outputs_CT,
};
static struct st_ui_screen__freeform gstFFS_Setup_Outputs_COP =
{
   .pfnDraw = UI_FFS_Setup_Outputs_COP,
};
static struct st_ui_screen__freeform gstFFS_Setup_Outputs_RIS =
{
   .pfnDraw = UI_FFS_Setup_Outputs_RIS,
};
static struct st_ui_screen__freeform gstFFS_Setup_Outputs_EXP1 =
{
   .pfnDraw = UI_FFS_Setup_Outputs_EXP1,
};
static struct st_ui_screen__freeform gstFFS_Setup_Outputs_EXP2 =
{
   .pfnDraw = UI_FFS_Setup_Outputs_EXP2,
};
static struct st_ui_screen__freeform gstFFS_Setup_Outputs_EXP3 =
{
   .pfnDraw = UI_FFS_Setup_Outputs_EXP3,
};
static struct st_ui_screen__freeform gstFFS_Setup_Outputs_EXP4 =
{
   .pfnDraw = UI_FFS_Setup_Outputs_EXP4,
};
static struct st_ui_screen__freeform gstFFS_Setup_Outputs_EXP5 =
{
   .pfnDraw = UI_FFS_Setup_Outputs_EXP5,
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_InvertInputs =
{
   .psTitle = "Invert Inputs",
   .pstUGS_Next = &gstUGS_Setup_InvertInputs_Boards,
};
static struct st_ui_menu_item gstMI_SetupInputs =
{
   .psTitle = "Setup Inputs",
   .pstUGS_Next = &gstUGS_Setup_Inputs_Boards,
};
static struct st_ui_menu_item gstMI_SetupOutputs =
{
   .psTitle = "Setup Outputs",
   .pstUGS_Next = &gstUGS_Setup_Outputs_Boards,
};
static struct st_ui_menu_item * gastMenuItems_SetupIO[] =
{
   &gstMI_InvertInputs,
   &gstMI_SetupInputs,
   &gstMI_SetupOutputs,
};
static struct st_ui_screen__menu gstMenu_SetupIO =
{
   .psTitle = "Setup I/O",
   .pastMenuItems = &gastMenuItems_SetupIO,
   .ucNumItems = sizeof(gastMenuItems_SetupIO) / sizeof(gastMenuItems_SetupIO[ 0 ]),
};
//---------------------------------------------------------------
static enum en_parm_edit_fields eFieldIndex; // Field selected for edit on the ui

static uint16_t uwParamIndex_Current;
static uint16_t uwParamIndex_Start;
static uint16_t uwParamIndex_End;

static uint16_t uwParamValue_Edited;
static uint16_t uwParamValue_Saved;

static uint8_t bEditingOutputs; //Select inputs or outputs

static uint8_t bSavingInProgress;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Menu_SetupIO =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_SetupIO,
};
//---------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Setup_Inputs_Boards =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Inputs_Boards,
};
struct st_ui_generic_screen gstUGS_Setup_Outputs_Boards =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Outputs_Boards,
};
//---------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Setup_Inputs_MR =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Inputs_MR,
};
struct st_ui_generic_screen gstUGS_Setup_Inputs_CT =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Inputs_CT,
};
struct st_ui_generic_screen gstUGS_Setup_Inputs_COP =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Inputs_COP,
};
struct st_ui_generic_screen gstUGS_Setup_Inputs_RIS =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Inputs_RIS,
};
struct st_ui_generic_screen gstUGS_Setup_Inputs_EXP1 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Inputs_EXP1,
};
struct st_ui_generic_screen gstUGS_Setup_Inputs_EXP2 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Inputs_EXP2,
};
struct st_ui_generic_screen gstUGS_Setup_Inputs_EXP3 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Inputs_EXP3,
};
struct st_ui_generic_screen gstUGS_Setup_Inputs_EXP4 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Inputs_EXP4,
};
struct st_ui_generic_screen gstUGS_Setup_Inputs_EXP5 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Inputs_EXP5,
};
//---------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Setup_Outputs_MR =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Outputs_MR,
};
struct st_ui_generic_screen gstUGS_Setup_Outputs_CT =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Outputs_CT,
};
struct st_ui_generic_screen gstUGS_Setup_Outputs_COP =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Outputs_COP,
};
struct st_ui_generic_screen gstUGS_Setup_Outputs_RIS =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Outputs_RIS,
};
struct st_ui_generic_screen gstUGS_Setup_Outputs_EXP1 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Outputs_EXP1,
};
struct st_ui_generic_screen gstUGS_Setup_Outputs_EXP2 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Outputs_EXP2,
};
struct st_ui_generic_screen gstUGS_Setup_Outputs_EXP3 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Outputs_EXP3,
};
struct st_ui_generic_screen gstUGS_Setup_Outputs_EXP4 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Outputs_EXP4,
};
struct st_ui_generic_screen gstUGS_Setup_Outputs_EXP5 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Outputs_EXP5,
};

 /*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void PrintBoardAndTerminal( uint16_t uwTerminalIndex )
{
   uint16_t uwLocalIndex;
   uint8_t ucBoardIndex;
   uint8_t ucMaxTerminals = MIN_TERMINALS_PER_IO_BOARD;
   char *psNumber = (bEditingOutputs) ? "6":"5";
   if( uwTerminalIndex < TERMINAL_INDEX_START__CTA )
   {
      uwLocalIndex = uwTerminalIndex + 1;
      LCD_Char_WriteString("MR ");
      LCD_Char_WriteString(psNumber);
      LCD_Char_WriteInteger_MinLength( uwLocalIndex, 2);
   }
   else if( uwTerminalIndex < TERMINAL_INDEX_START__COP )
   {
      uwLocalIndex = uwTerminalIndex - TERMINAL_INDEX_START__CTA + 1;
      ucMaxTerminals = 2*MIN_TERMINALS_PER_IO_BOARD;
      LCD_Char_WriteString("CT ");
      LCD_Char_WriteString(psNumber);
      LCD_Char_WriteInteger_MinLength( uwLocalIndex, 2);
   }
   else if( uwTerminalIndex < TERMINAL_INDEX_START__RIS )
   {
      uwLocalIndex = uwTerminalIndex - TERMINAL_INDEX_START__COP + 1;
      ucMaxTerminals = 2*MIN_TERMINALS_PER_IO_BOARD;
      LCD_Char_WriteString("COP ");
      LCD_Char_WriteString(psNumber);
      LCD_Char_WriteInteger_MinLength( uwLocalIndex, 2);
   }
   else if( uwTerminalIndex < TERMINAL_INDEX_START__EXP )
   {
      uwLocalIndex = ( uwTerminalIndex - TERMINAL_INDEX_START__RIS )%MIN_TERMINALS_PER_IO_BOARD + 1;
      ucBoardIndex = ( uwTerminalIndex - TERMINAL_INDEX_START__RIS )/MIN_TERMINALS_PER_IO_BOARD + 1;
      LCD_Char_WriteString("RIS");
      LCD_Char_WriteInteger_MinLength(ucBoardIndex, 1);
      LCD_Char_WriteString(" ");
      LCD_Char_WriteString(psNumber);
      LCD_Char_WriteInteger_MinLength( uwLocalIndex, 2);
   }
   else
   {
      uwLocalIndex = ( uwTerminalIndex - TERMINAL_INDEX_START__EXP )%MIN_TERMINALS_PER_IO_BOARD + 1;
      ucBoardIndex = ( uwTerminalIndex - TERMINAL_INDEX_START__EXP )/MIN_TERMINALS_PER_IO_BOARD + 1;
      LCD_Char_WriteString("EXP");
      LCD_Char_WriteInteger_MinLength(ucBoardIndex, 1);
      LCD_Char_WriteString(" ");
      LCD_Char_WriteString(psNumber);
      LCD_Char_WriteInteger_MinLength( uwLocalIndex, 2);
   }
   LCD_Char_WriteString("/");
   LCD_Char_WriteString(psNumber);
   LCD_Char_WriteInteger_ExactLength( ucMaxTerminals, 2 );
}
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void PrintFunction( uint8_t ucGroup, uint8_t ucFunction )
{
   if(!bEditingOutputs)
   {
      if( ( ucGroup < INPUT_GROUP__CCB_F )
       || ( ucGroup == INPUT_GROUP__EPWR ) )
      {
         LCD_Char_WriteString( SysIO_GetInputGroupString_ByFunctionIndex(ucGroup, ucFunction) );
      }
      else
      {
         switch(ucGroup)
         {
            case INPUT_GROUP__CCB_F:
            case INPUT_GROUP__CCB_R:
               LCD_Char_WriteString("Button ");
               LCD_Char_WriteInteger_MinLength( ucFunction+1, 1 );
               break;
            case INPUT_GROUP__CC_ENABLE_F:
            case INPUT_GROUP__CC_ENABLE_R:
               LCD_Char_WriteString("Key ");
               LCD_Char_WriteInteger_MinLength( ucFunction+1, 1 );
               break;
            default:
               LCD_Char_WriteString("<invalid>");
               break;
         }
      }
   }
   else
   {
      if( ( ucGroup < OUTPUT_GROUP__CCL_F )
       || ( ucGroup == OUTPUT_GROUP__EPWR ) )
      {
         LCD_Char_WriteString( SysIO_GetOutputGroupString_ByFunctionIndex( ucGroup, ucFunction ) );
      }
      else
      {
         switch(ucGroup)
         {
            case OUTPUT_GROUP__CCL_F:
            case OUTPUT_GROUP__CCL_R:
               LCD_Char_WriteString("Lamp ");
               LCD_Char_WriteInteger_MinLength( ucFunction+1, 1 );
               break;
            default:
               LCD_Char_WriteString("<invalid>");
               break;
         }
      }
   }
}

/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void Update_Screen(void)
{
   LCD_Char_Clear();

   if(!bEditingOutputs)
   {
      /* L1 */
      LCD_Char_GotoXY( 0, 0 );
      LCD_Char_WriteString("In ");
      PrintBoardAndTerminal( uwParamIndex_Current - enPARAM16__MR_Inputs_501 );

      /* L2 */
      uint8_t ucGroup = ( uwParamValue_Edited >> 8 ) & 0xFF;
      LCD_Char_GotoXY( 0, 1 );
      LCD_Char_WriteString("| ");
      LCD_Char_WriteString( SysIO_GetInputGroupString(ucGroup) );

      /* L3 */
      uint8_t ucFunction = ( uwParamValue_Edited ) & 0x7F;
      LCD_Char_GotoXY( 0, 2 );
      LCD_Char_WriteString("| | ");
      PrintFunction( ucGroup, ucFunction );
   }
   else
   {
      /* L1 */
      LCD_Char_GotoXY( 0, 0 );
      LCD_Char_WriteString("Out ");
      PrintBoardAndTerminal( uwParamIndex_Current - enPARAM16__MR_Outputs_601 );

      /* L2 */
      uint8_t ucGroup = ( uwParamValue_Edited >> 8 ) & 0xFF;
      LCD_Char_GotoXY( 0, 1 );
      LCD_Char_WriteString("| ");
      LCD_Char_WriteString( SysIO_GetOutputGroupString(ucGroup) );

      /* L3 */
      uint8_t ucFunction = ( uwParamValue_Edited ) & 0x7F;
      LCD_Char_GotoXY( 0, 2 );
      LCD_Char_WriteString("| | ");
      PrintFunction( ucGroup, ucFunction );
   }

   /* L4 */
   if( !bSavingInProgress ) /* Display cursor */
   {
      uint8_t ucCursorPos = 0;
      if( eFieldIndex == enFIELD__IO_GROUP )
      {
         ucCursorPos = 2;
      }
      else if( eFieldIndex == enFIELD__IO_FUNCT )
      {
         ucCursorPos = 4;
      }
      else if( eFieldIndex == enFIELD__SAVE )
      {
         ucCursorPos = 19;
      }
      LCD_Char_GotoXY( ucCursorPos, 3 );
      LCD_Char_WriteString("*");
   }
   else/* Or display saving */
   {
      LCD_Char_GotoXY( 0, 3 );
      LCD_Char_WriteString("Saving value...");
   }

   /* Only show save option if values differ  */
   uint8_t bValueEdited = uwParamValue_Edited != uwParamValue_Saved;
   if( bValueEdited )
   {
      LCD_Char_GotoXY( 16, 0 );
      LCD_Char_WriteString("Save");
      LCD_Char_GotoXY( 19, 1 );
      LCD_Char_WriteString("|");
      LCD_Char_GotoXY( 19, 2 );
      LCD_Char_WriteString("|");
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Index( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   enum en_field_action enReturnValue = enACTION__NONE;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         uwParamIndex_Current = uwParamIndex_Start;
      }
   }
   else  // enACTION__EDIT
   {
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            if( uwParamIndex_Current < uwParamIndex_End )
            {
               uwParamIndex_Current++;
            }
            else if( uwParamIndex_Current > uwParamIndex_End )
            {
               uwParamIndex_Current = uwParamIndex_End;
            }
            break;
         case enKEYPRESS_DOWN:
            if( uwParamIndex_Current > uwParamIndex_Start )
            {
               uwParamIndex_Current--;
            }
            else if( uwParamIndex_Current < uwParamIndex_Start )
            {
               uwParamIndex_Current = uwParamIndex_Start;
            }
            break;
         case enKEYPRESS_LEFT:
            enReturnValue = enACTION__EXIT_TO_LEFT;
            break;
         case enKEYPRESS_RIGHT:
            enReturnValue = enACTION__EXIT_TO_RIGHT;
            break;
         default:
            break;
      }
      uwParamValue_Saved = Param_ReadValue_16Bit( uwParamIndex_Current );
      uwParamValue_Edited = uwParamValue_Saved;
   }

   return enReturnValue;
}
/*-----------------------------------------------------------------------------
   Hold to a given group value if terminal is fixed
 -----------------------------------------------------------------------------*/
static void GetMaxAndMinGroup( uint16_t uwTerminalIndex, uint8_t *pucMaxGroup, uint8_t *pucMinGroup )
{
   if(!bEditingOutputs)
   {
      *pucMaxGroup = NUM_INPUT_GROUPS-1;
      *pucMinGroup = INPUT_GROUP__NONE;
      for( uint8_t ucIG = 0; ucIG < INPUT_GROUP__CCB_F; ucIG++ )
      {
         for( uint8_t ucFunct = 0; ucFunct < SysIO_GetSizeOfCIG(ucIG); ucFunct++ )
         {
            uint16_t uwTerminal_Plus1 = SysIO_GetFixedTerminalFromCIG_Plus1(ucIG, ucFunct) & ~MASK_INVERT_FIXED_TERMINAL_PLUS1;
            if( uwTerminal_Plus1
           && ( (uwTerminal_Plus1-1) == uwTerminalIndex ) )
            {
               *pucMaxGroup = ucIG;
               *pucMinGroup = ucIG;
               ucFunct = MAX_NUM_FLOORS;
               ucIG = NUM_INPUT_GROUPS;
               break;
            }
         }
      }
   }
   else
   {
      *pucMaxGroup = NUM_OUTPUT_GROUPS-1;
      *pucMinGroup = OUTPUT_GROUP__NONE;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Group( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   static uint8_t ucMaxGroup;
   static uint8_t ucMinGroup;
   enum en_field_action enReturnValue = enACTION__NONE;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         uint16_t uwTerminal = (bEditingOutputs) ? ( uwParamIndex_Current - enPARAM16__MR_Outputs_601 )
                                                 : ( uwParamIndex_Current - enPARAM16__MR_Inputs_501 );
         GetMaxAndMinGroup( uwTerminal, &ucMaxGroup, &ucMinGroup );
         uwParamValue_Saved = Param_ReadValue_16Bit( uwParamIndex_Current );
         uwParamValue_Edited = uwParamValue_Saved;
      }
   }
   else  // enACTION__EDIT
   {
      uint8_t ucGroup_Current = ( uwParamValue_Edited >> 8 ) & 0xFF;

      uwParamValue_Saved = Param_ReadValue_16Bit( uwParamIndex_Current );
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            if( ucGroup_Current < ucMaxGroup )
            {
               ucGroup_Current++;
            }
            break;

         case enKEYPRESS_DOWN:
            if( ucGroup_Current > ucMinGroup )
            {
               ucGroup_Current--;
            }
            break;

         case enKEYPRESS_LEFT:
            enReturnValue = enACTION__EXIT_TO_LEFT;
            break;

         case enKEYPRESS_RIGHT:
            enReturnValue = enACTION__EXIT_TO_RIGHT;
            break;

         default:
            break;
      }

      if( ucGroup_Current > ucMaxGroup )
      {
         ucGroup_Current = ucMaxGroup;
      }
      else if( ucGroup_Current < ucMinGroup )
      {
         ucGroup_Current = ucMinGroup;
      }

      uwParamValue_Edited &= ~(0xFF << 8);
      uwParamValue_Edited |= ucGroup_Current << 8;
   }

   return enReturnValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t GetMaxFunctionValue( uint8_t ucGroup )
{
   uint8_t ucMaxFunction = 0;
   if(!bEditingOutputs)
   {
      if( ( ucGroup < INPUT_GROUP__CCB_F )
       || ( ucGroup == INPUT_GROUP__EPWR ) )
      {
         ucMaxFunction = SysIO_GetSizeOfCIG(ucGroup)-1;
      }
      else
      {
         ucMaxFunction = GetFP_NumFloors()-1;
      }
   }
   else
   {
      if( ( ucGroup < OUTPUT_GROUP__CCL_F )
       || ( ucGroup == OUTPUT_GROUP__EPWR ) )
      {
         ucMaxFunction = SysIO_GetSizeOfCOG(ucGroup)-1;
      }
      else
      {
         ucMaxFunction = GetFP_NumFloors()-1;
      }
   }
   return ucMaxFunction;
}
/*-----------------------------------------------------------------------------
   Hold to a given group value if terminal is fixed
 -----------------------------------------------------------------------------*/
static void GetMaxAndMinFunction( uint16_t uwTerminalIndex, uint8_t *pucMaxFunct, uint8_t *pucMinFunct )
{
   uint8_t ucGroup = ( uwParamValue_Edited >> 8 ) & 0xFF;
   *pucMaxFunct = GetMaxFunctionValue(ucGroup);
   *pucMinFunct = 0;
   if(!bEditingOutputs)
   {
      for( uint8_t ucIG = 0; ucIG < INPUT_GROUP__CCB_F; ucIG++ )
      {
         for( uint8_t ucFunct = 0; ucFunct < SysIO_GetSizeOfCIG(ucIG); ucFunct++ )
         {
            uint16_t uwTerminal_Plus1 = SysIO_GetFixedTerminalFromCIG_Plus1(ucIG, ucFunct) & ~MASK_INVERT_FIXED_TERMINAL_PLUS1;
            if( uwTerminal_Plus1
           && ( (uwTerminal_Plus1-1) == uwTerminalIndex ) )
            {
               *pucMaxFunct = ucFunct;
               *pucMaxFunct = ucFunct;
               ucFunct = MAX_NUM_FLOORS;
               ucIG = NUM_INPUT_GROUPS;
               break;
            }
         }
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Function( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   static uint8_t ucMaxFunct, ucMinFunct;
   enum en_field_action enReturnValue = enACTION__NONE;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         uint16_t uwTerminal = (bEditingOutputs) ? ( uwParamIndex_Current - enPARAM16__MR_Outputs_601 )
                                                 : ( uwParamIndex_Current - enPARAM16__MR_Inputs_501 );
         GetMaxAndMinFunction( uwTerminal, &ucMaxFunct, &ucMinFunct );
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         bSavingInProgress = 0;
      }
   }
   else  // enACTION__EDIT
   {
      uint8_t ucFunction_Current = uwParamValue_Edited & 0x7F;
      uint8_t ucGroup_Current = ( uwParamValue_Edited >> 8 ) & 0xFF;
      uwParamValue_Saved = Param_ReadValue_16Bit( uwParamIndex_Current );

      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            if( ucFunction_Current < ucMaxFunct )
            {
               ucFunction_Current++;
            }
            break;

         case enKEYPRESS_DOWN:
            if( ucFunction_Current > ucMinFunct )
            {
               ucFunction_Current--;
            }
            break;

         case enKEYPRESS_LEFT:
            enReturnValue = enACTION__EXIT_TO_LEFT;
            break;

         case enKEYPRESS_RIGHT:
            if( uwParamValue_Edited != uwParamValue_Saved )
            {
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;

         default:
            break;
      }

      uint16_t uwTerminal_Plus1 = SysIO_GetFixedTerminalFromCIG_Plus1( ucGroup_Current, ucFunction_Current ) & ~MASK_INVERT_FIXED_TERMINAL_PLUS1;
      uint8_t bFunctionIsFixed = ( !bEditingOutputs && uwTerminal_Plus1 );
      uint8_t bCurrentTerminalIsFixed = bFunctionIsFixed && ( ( uwTerminal_Plus1-1 ) == uwParamIndex_Current );
      /* If a function is fixed, but not at the current terminal, skip to the next function. */
      if( bFunctionIsFixed && !bCurrentTerminalIsFixed )
      {
         if( ucFunction_Current < ucMaxFunct )
         {
            ucFunction_Current += 1;
         }
         else
         {
            ucFunction_Current -= 1;
         }
      }

      if( ucFunction_Current > ucMaxFunct )
      {
         ucFunction_Current = ucMaxFunct;
      }
      else if( ucFunction_Current < ucMinFunct )
      {
         ucFunction_Current = ucMinFunct;
      }

      uwParamValue_Edited &= ~0x007F;
      uwParamValue_Edited |= ucFunction_Current & 0x7F;
   }

   return enReturnValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t SavingParameter( void )
{
   uint8_t bSaving = 0;
   uwParamValue_Saved = Param_ReadValue_16Bit( uwParamIndex_Current );
   if( uwParamValue_Saved != uwParamValue_Edited )
   {
      bSaving = 1;
      Param_WriteValue_16Bit( uwParamIndex_Current, uwParamValue_Edited );
   }
   return bSaving;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Save( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static enum en_save_state enSaveState = SAVE_STATE__SAVED;
   bSavingInProgress = 0;
   if ( enFieldAction == enACTION__EDIT )
   {
      if ( enKeypress == enKEYPRESS_ENTER )
      {
         enSaveState = SAVE_STATE__SAVING;
      }
      else if ( enKeypress == enKEYPRESS_LEFT )
      {
         enSaveState = SAVE_STATE__SAVED;

         enReturnValue = enACTION__EXIT_TO_LEFT;
      }
   }
   switch(enSaveState)
   {
      case SAVE_STATE__SAVING:
         if(SavingParameter())
         {
            bSavingInProgress = 1;
         }
         else
         {
            enSaveState = SAVE_STATE__SAVED;
            enReturnValue = enACTION__EXIT_TO_LEFT;
         }
         break;
      default:
         enSaveState = SAVE_STATE__SAVED;
         break;
   }
   return enReturnValue;
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void Edit_IO(void)
{
   if ( eFieldIndex == enFIELD__NONE )
   {
      uwParamIndex_Current = uwParamIndex_Start;
      uwParamValue_Saved = Param_ReadValue_16Bit( uwParamIndex_Current );
      uwParamValue_Edited = uwParamValue_Saved;
      Edit_Index( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT );
      eFieldIndex = enFIELD__INDEX;
   }
   else
   {
      enum en_field_action enFieldAction;
      enum en_keypresses enKeypress = Button_GetKeypress();
      //------------------------------------------------------------------
      switch ( eFieldIndex )
      {
         case enFIELD__INDEX:
            enFieldAction = Edit_Index( enKeypress, enACTION__EDIT );
            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               PrevScreen();
               eFieldIndex = enFIELD__NONE;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Group( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT );
               eFieldIndex = enFIELD__IO_GROUP;
            }
            break;
         //------------------------------------------------------------------
         case enFIELD__IO_GROUP: //function
            enFieldAction = Edit_Group( enKeypress, enACTION__EDIT );
            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Index( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT );
               eFieldIndex = enFIELD__INDEX;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Function( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT );

               eFieldIndex = enFIELD__IO_FUNCT;
            }
            break;
         //------------------------------------------------------------------
         case enFIELD__IO_FUNCT: //function
            enFieldAction = Edit_Function( enKeypress, enACTION__EDIT );
            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Group( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT );
               eFieldIndex = enFIELD__IO_GROUP;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Save( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT );
               eFieldIndex = enFIELD__SAVE;
            }
            break;
         case enFIELD__SAVE:
            enFieldAction = Edit_Save( enKeypress, enACTION__EDIT );
            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Function( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT );

               eFieldIndex = enFIELD__IO_FUNCT;
            }
            break;
         default:
            eFieldIndex = enFIELD__NONE;
            break;
      }
      Update_Screen();
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Inputs_MR( void )
{
   bEditingOutputs = 0;
   uwParamIndex_Start = enPARAM16__MR_Inputs_501;
   uwParamIndex_End = enPARAM16__MR_Inputs_508;
   Edit_IO();
}
static void UI_FFS_Setup_Inputs_CT( void )
{
   bEditingOutputs = 0;
   uwParamIndex_Start = enPARAM16__CT_Inputs_501;
   uwParamIndex_End = enPARAM16__CT_Inputs_516;
   Edit_IO();
}
static void UI_FFS_Setup_Inputs_COP( void )
{
   bEditingOutputs = 0;
   uwParamIndex_Start = enPARAM16__COP_Inputs_501;
   uwParamIndex_End = enPARAM16__COP_Inputs_516;
   Edit_IO();
}
static void UI_FFS_Setup_Inputs_RIS( void )
{
   bEditingOutputs = 0;
   uwParamIndex_Start = enPARAM16__RIS1_Inputs_501;
   uwParamIndex_End = enPARAM16__RIS4_Inputs_508;
   Edit_IO();
}
static void UI_FFS_Setup_Inputs_EXP1( void )
{
   bEditingOutputs = 0;
   uwParamIndex_Start = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_START__EXP1;
   uwParamIndex_End = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_START__EXP2 - 1;
   Edit_IO();
}
static void UI_FFS_Setup_Inputs_EXP2( void )
{
   bEditingOutputs = 0;
   uwParamIndex_Start = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_START__EXP2;
   uwParamIndex_End = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_START__EXP3 - 1;
   Edit_IO();
}
static void UI_FFS_Setup_Inputs_EXP3( void )
{
   bEditingOutputs = 0;
   uwParamIndex_Start = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_START__EXP3;
   uwParamIndex_End = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_START__EXP4 - 1;
   Edit_IO();
}
static void UI_FFS_Setup_Inputs_EXP4( void )
{
   bEditingOutputs = 0;
   uwParamIndex_Start = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_START__EXP4;
   uwParamIndex_End = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_START__EXP5 - 1;
   Edit_IO();
}
static void UI_FFS_Setup_Inputs_EXP5( void )
{
   bEditingOutputs = 0;
   uwParamIndex_Start = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_START__EXP5;
   uwParamIndex_End = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_END__EXP;
   Edit_IO();
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Outputs_MR( void )
{
   bEditingOutputs = 1;
   uwParamIndex_Start = enPARAM16__MR_Outputs_601;
   uwParamIndex_End = enPARAM16__MR_Outputs_608;
   Edit_IO();
}
static void UI_FFS_Setup_Outputs_CT( void )
{
   bEditingOutputs = 1;
   uwParamIndex_Start = enPARAM16__CT_Outputs_601;
   uwParamIndex_End = enPARAM16__CT_Outputs_616;
   Edit_IO();
}
static void UI_FFS_Setup_Outputs_COP( void )
{
   bEditingOutputs = 1;
   uwParamIndex_Start = enPARAM16__COP_Outputs_601;
   uwParamIndex_End = enPARAM16__COP_Outputs_616;
   Edit_IO();
}
static void UI_FFS_Setup_Outputs_RIS( void )
{
   bEditingOutputs = 1;
   uwParamIndex_Start = enPARAM16__RIS1_Outputs_601;
   uwParamIndex_End = enPARAM16__RIS4_Outputs_608;
   Edit_IO();
}
static void UI_FFS_Setup_Outputs_EXP1( void )
{
   bEditingOutputs = 1;
   uwParamIndex_Start = enPARAM16__MR_Outputs_601 + TERMINAL_INDEX_START__EXP1;
   uwParamIndex_End = enPARAM16__MR_Outputs_608 + TERMINAL_INDEX_START__EXP2 - 1;
   Edit_IO();
}
static void UI_FFS_Setup_Outputs_EXP2( void )
{
   bEditingOutputs = 1;
   uwParamIndex_Start = enPARAM16__MR_Outputs_601 + TERMINAL_INDEX_START__EXP2;
   uwParamIndex_End = enPARAM16__MR_Outputs_608 + TERMINAL_INDEX_START__EXP3 - 1;
   Edit_IO();
}
static void UI_FFS_Setup_Outputs_EXP3( void )
{
   bEditingOutputs = 1;
   uwParamIndex_Start = enPARAM16__MR_Outputs_601 + TERMINAL_INDEX_START__EXP3;
   uwParamIndex_End = enPARAM16__MR_Outputs_608 + TERMINAL_INDEX_START__EXP4 - 1;
   Edit_IO();
}
static void UI_FFS_Setup_Outputs_EXP4( void )
{
   bEditingOutputs = 1;
   uwParamIndex_Start = enPARAM16__MR_Outputs_601 + TERMINAL_INDEX_START__EXP4;
   uwParamIndex_End = enPARAM16__MR_Outputs_608 + TERMINAL_INDEX_START__EXP5 - 1;
   Edit_IO();
}
static void UI_FFS_Setup_Outputs_EXP5( void )
{
   bEditingOutputs = 1;
   uwParamIndex_Start = enPARAM16__MR_Outputs_601 + TERMINAL_INDEX_START__EXP5;
   uwParamIndex_End = enPARAM16__MR_Outputs_608 + TERMINAL_INDEX_END__EXP;
   Edit_IO();
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
