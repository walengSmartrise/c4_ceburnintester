/******************************************************************************
 *
 * @file     ui_strings_faults.c
 * @brief    Fault string access
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "ui.h"

#include "mod.h"
#include <stdint.h>
#include <string.h>
#include "lcd.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MAX_SUBFAULT_STRINGS     (16)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/


static char const * const pasFaultStrings[ NUM_FAULTS + 1 ] =
{
   FAULT_TABLE(EXPAND_FAULT_TABLE_AS_STRING_ARRAY)
};

static char const * const psInvalidFaultNumString = "Unknown Fault";
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void PrintFaultString( en_faults eFaultNum )
{
   if(eFaultNum < NUM_FAULTS)
   {
      LCD_Char_WriteString(pasFaultStrings[ eFaultNum ]);
   }
   else
   {
      LCD_Char_WriteString( psInvalidFaultNumString );
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

