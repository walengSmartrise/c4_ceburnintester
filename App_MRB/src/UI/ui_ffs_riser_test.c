
/******************************************************************************
 *
 * @file     ui_menu_io.c
 * @brief    I/O Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sru.h"
#include "sys.h"
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "GlobalData.h"
#include "riserData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_Riser_Test( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
//---------------------------------------------------------------

static struct st_ui_screen__freeform gstFFS_Riser_Test =
{
   .pfnDraw = UI_FFS_Riser_Test,
};


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_Riser_Test =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Riser_Test,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define RISER_ADDR 0
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
static void Print_Screen( void)
{
   uint8_t bValid = 0;
   uint8_t bInputs = 0;

   uint8_t DIP_Index = 1;


	LCD_Char_Clear();

	LCD_Char_GotoXY( 0, 0 );
	LCD_Char_WriteString("RISER BOARD");

	LCD_Char_GotoXY( 0, 1 );
	LCD_Char_WriteString("Status:");

	LCD_Char_GotoXY( 0, 2);
   LCD_Char_WriteString("In");

   LCD_Char_GotoXY( 9, 2);
   LCD_Char_WriteString("Out");

   char inputs[] = "........";
   char outputs[] = "........";

   LCD_Char_GotoXY( 9, 3);
   
   uint8_t ucOutputs = RiserData_GetOutputs( RISER_ADDR );

   for(int i = 1; i <= 8; i++)
   {
      if((ucOutputs & DIP_Index) == DIP_Index)
         outputs[i - 1] = i + '0';

      DIP_Index = DIP_Index << 1;
   }

   LCD_Char_WriteString(outputs);

   DIP_Index = 1;

   LCD_Char_GotoXY( 0, 3);
   uint8_t ucInputs = RiserData_GetInputs( RISER_ADDR );

   for(int i = 1; i <= 8; i++)
   {
      if((ucInputs & DIP_Index) == DIP_Index){
         inputs[i - 1] = i + '0';
         bInputs = 1;
      }

      DIP_Index = DIP_Index << 1;
   }

   LCD_Char_WriteString(inputs);


   for( int i = 0; i < 8; i++){

     bValid = 1;

     if(inputs[DIP_Index] != outputs[DIP_Index])
        bValid = 0;

     DIP_Index++;
   }

   if((bValid == 1) && (bInputs == 1))
   {
     LCD_Char_GotoXY( 8, 1);
     LCD_Char_WriteString("PASS");
   }

   else if(( bValid == 0 ) && (bInputs == 1))
   {
     LCD_Char_GotoXY( 8, 1);
     LCD_Char_WriteString("FAIL");
   }

}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_Riser_Test( void )
{
   enum en_keypresses enKeypress = Button_GetKeypress();

   Print_Screen();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {

   }
   else if ( enKeypress == enKEYPRESS_DOWN )
   {

   }


   Print_Screen();
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/


