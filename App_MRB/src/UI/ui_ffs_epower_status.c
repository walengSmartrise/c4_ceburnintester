/******************************************************************************
 *
 * @file     ui_ffs_epower_status.c
 * @brief
 * @version  V1.00
 * @date     18, December 2017
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "GlobalData.h"
#include "emergency_power.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_EPowerStatus( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_EPowerStatus =
{
   .pfnDraw = UI_FFS_EPowerStatus,
};

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_EPowerStatus =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_EPowerStatus,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
static char const * const pasEPowerCommands[EPC__NUM_OF_COMMANDS] =
{
      "OFF",
      "RECALL",
      "AUTO",
      "OOS",
      "PRETRANSFER",
};
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
static void Print_Screen(uint8_t ucCursor)
{
   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("E-Power Command");

   uint8_t ucIndex;
   for(ucIndex = 0; ucIndex < 3; ucIndex++)//num lines
   {
      uint8_t ucLine = ucCursor + ucIndex;
      if(ucLine < MAX_GROUP_CARS)
      {
         enum enEmergencyPowerCommands eCommand = GetEPowerCommand( ucLine );
         const char *ps = pasEPowerCommands[eCommand];
         LCD_Char_GotoXY( 0, 1+ucIndex );
         LCD_Char_WriteString("CAR");
         LCD_Char_WriteInteger_ExactLength(ucLine+1, 1);
         LCD_Char_WriteString(": ");
         LCD_Char_WriteString(ps);
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_EPowerStatus( void )
{
   static uint8_t ucCursorY = 0;

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      uint8_t ucLimit = (MAX_GROUP_CARS > 3) ? MAX_GROUP_CARS-3:0;

      if(ucCursorY < ucLimit)//Minus cursor scroll+1
      {
         ucCursorY++;
      }
   }

   Print_Screen(ucCursorY);
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

