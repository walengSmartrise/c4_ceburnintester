/******************************************************************************
 *
 * @file     ui_ffs_hallsecurity_status.c
 * @brief    Page for viewing hall board status information
 * @version  V1.00
 * @date     18, Sept 2018
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "GlobalData.h"
#include <math.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_HallSecurityStatus( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
//---------------------------------------------------------------

static struct st_ui_screen__freeform gstFFS_HallSecurityStatus =
{
   .pfnDraw = UI_FFS_HallSecurityStatus,
};


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_HallSecurityStatus =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_HallSecurityStatus,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

static char const * const pasCommStateStrings[NUM_HB_COM_STATE] =
{
      "N/A ",
      "0%  ",
      "25% ",
      "50% ",
      "75% ",
      "100%",
};

static char const * const pasFunctionStrings[NUM_OF_HALLBOARD_BLOCKS] =
{
      "F1",
      "F2",
      "F3",
      "F4",
      "F5",
      "F6",
      "F7",
      "F8",
};

static char const * const pasError[NUM_HALLBOARD_ERROR] =
{
      "UNK",
      "NONE",
      "POR",
      "WDT",
      "BOR",
      "COM",
      "DIP",
      "BUS",
};
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

|01234567890123456789|
|LND01 - F1 - RIS1   |  <- Landing - FunctionGroup - ConnectedRiser
|COM:N/A  ERR:NONE   |  <- Connection / Error
|001   SW: ......... |  <- Hall Board Number / DIP Switches
|  *     .. .. .. .. |  <- Button/Lamp Status

|01234567890123456789|
|LND64 - F1 - RIS1   |  <- Landing - FunctionGroup - ConnectedRiser
|COM:N/A  ERR:NONE   |  <- Connection / Error
|064   SW: 123456... |  <- Hall Board Number / DIP Switches
|  *     UL DL UB DB |  <- Button/Lamp Status

-----------------------------------------------------------------------------*/
static void Print_Screen( uint16_t uwDIPs, uint8_t ucCursorX )
{
   LCD_Char_Clear();

   /* Floor & Type */
   LCD_Char_GotoXY( 0, 0 );
   uint8_t ucLanding = uwDIPs % GetHallBoard_MaxNumberOfFloors();
   uint8_t ucType = uwDIPs / GetHallBoard_MaxNumberOfFloors();
   LCD_Char_WriteString("LND");
   LCD_Char_WriteInteger_MinLength(ucLanding+1, 2);
   LCD_Char_WriteString(" - ");
   LCD_Char_WriteString(pasFunctionStrings[ucType]);
   if(GetHallStatusResponse_Comm() <= HB_COM_STATE__0 )
   {
      LCD_Char_WriteString(" - N/A");
   }
   else
   {
      LCD_Char_WriteString(" - RIS");
      LCD_Char_WriteInteger_ExactLength(GetHallStatusResponse_Source(), 1);
   }

   /* COM */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("COM:");
   LCD_Char_WriteString(pasCommStateStrings[GetHallStatusResponse_Comm()]);
   /* ERR */
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteString("ERR:");
   LCD_Char_WriteString(pasError[GetHallStatusResponse_Error()]);

   /* Lamps/Buttons */
   LCD_Char_GotoXY( 8, 3 );
   uint8_t ucIO = GetHallStatusResponse_IO();
   uint8_t bLampUp = ( ucIO >> HB_IO_SHIFT__LMP_UP ) & 1;
   if( bLampUp )
   {
      LCD_Char_WriteString("UL");
   }
   else
   {
      LCD_Char_WriteString("..");
   }
   LCD_Char_AdvanceX(1);
   uint8_t bLampDown = ( ucIO >> HB_IO_SHIFT__LMP_DN ) & 1;
   if( bLampDown )
   {
      LCD_Char_WriteString("DL");
   }
   else
   {
      LCD_Char_WriteString("..");
   }
   LCD_Char_AdvanceX(1);
   uint8_t bButtonUp = ( ucIO >> HB_IO_SHIFT__BTN_UP ) & 1;
   if( bButtonUp )
   {
      LCD_Char_WriteString("UB");
   }
   else
   {
      LCD_Char_WriteString("..");
   }
   LCD_Char_AdvanceX(1);
   uint8_t bButtonDown = ( ucIO >> HB_IO_SHIFT__BTN_DN ) & 1;
   if( bButtonDown )
   {
      LCD_Char_WriteString("DB");
   }
   else
   {
      LCD_Char_WriteString("..");
   }

   /* Hall Board Number */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteInteger_ExactLength(uwDIPs+1, 4);

   /* DIP Switches */
   LCD_Char_GotoXY( 5, 2 );
   LCD_Char_WriteString("SW: ");
   for(uint8_t i = 0; i < GetHallBoard_NumberOfDIPs(); i++)
   {
      if(Sys_Bit_Get(&uwDIPs, i))
      {
         LCD_Char_WriteInteger_ExactLength( ( i+1 )%10, 1);
      }
      else
      {
         LCD_Char_WriteString(".");
      }
   }

   /* Cursor */
   LCD_Char_GotoXY( ucCursorX, 3 );
   LCD_Char_WriteString("*");
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
#define MAX_CURSOR_X_POS      (3)
static void UI_FFS_HallSecurityStatus( void )
{
   static uint8_t ucCursorX = 0;
   static uint16_t uwAddress;

   enum en_keypresses enKeypress = Button_GetKeypress();
   int32_t iEditAddress = uwAddress;
   uint16_t uwMagnitude = pow(10, MAX_CURSOR_X_POS-ucCursorX);

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      if(!ucCursorX)
      {
         PrevScreen();
      }
      else
      {
         ucCursorX--;
      }
   }
   else if ( enKeypress == enKEYPRESS_RIGHT )
   {
      if( ucCursorX < MAX_CURSOR_X_POS )
      {
         ucCursorX++;
      }
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      iEditAddress += uwMagnitude;
   }
   else if ( enKeypress == enKEYPRESS_DOWN )
   {
      iEditAddress -= uwMagnitude;
   }

   if( iEditAddress < 0 )
   {
      iEditAddress = 0;
   }
   else if( iEditAddress >= NEW_HALL_BOARD_MAX_FLOOR_LIMIT )
   {
      iEditAddress = NEW_HALL_BOARD_MAX_FLOOR_LIMIT-1;
   }
   uwAddress = iEditAddress;

   SetUIRequest_HallStatusIndex_Plus1(uwAddress+HALL_UI_REQ__HALL_SECURITY__START);

   Print_Screen( uwAddress, ucCursorX );
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
