/******************************************************************************
 *
 * @file     ui_ffs_doors.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include <stdint.h>
#include <string.h>
#include "sys.h"
#include "pattern.h"

 /*----------------------------------------------------------------------------
  *
  * Place typedefs, structs, unions, and enums that will be used only
  * by this source file here.
  *
  *----------------------------------------------------------------------------*/

typedef enum
{
   EDIT_SCURVE_ACCEL,
   EDIT_SCURVE_DECEL,
   EDIT_SCURVE_JERK_IN_ACCEL,
   EDIT_SCURVE_JERK_OUT_ACCEL,
   EDIT_SCURVE_JERK_IN_DECEL,
   EDIT_SCURVE_JERK_OUT_DECEL,
   EDIT_SCURVE_QUICK_STOP,
   EDIT_SCURVE_LVLING_DECEL,
   NUM_EDIT_SCURVE_PARAMS_SPEED
} en_edit_scurve_param_speed;

typedef enum
{
   EDIT_SCURVE_LVLING_DISTANCE,
   EDIT_SCURVE_DEST_OFFSET_UP,
   EDIT_SCURVE_DEST_OFFSET_DOWN,
   EDIT_SCURVE_RELVL_OFFSET_UP,
   EDIT_SCURVE_RELVL_OFFSET_DOWN,
   NUM_EDIT_SCURVE_PARAMS_DISTANCE
} en_edit_scurve_param_distance;

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
//---------------------------------------------------------------------

static void UI_FFS_Edit_SCurve_Speed( en_edit_scurve_param_speed, uint32_t );
static void UI_FFS_Edit_SCurve_Distance( en_edit_scurve_param_distance, uint32_t );

static void UI_FFS_SCurve_P1_Accel( void );
static void UI_FFS_SCurve_P1_Decel( void );
static void UI_FFS_SCurve_P1_AccelJerkIn( void );
static void UI_FFS_SCurve_P1_AccelJerkOut( void );
static void UI_FFS_SCurve_P1_DecelJerkIn( void );
static void UI_FFS_SCurve_P1_DecelJerkOut( void );
static void UI_FFS_SCurve_P1_LevelingDistance( void );
//---------------------------------------------------------------------
static void UI_FFS_SCurve_P2_Accel( void );
static void UI_FFS_SCurve_P2_Decel( void );
static void UI_FFS_SCurve_P2_AccelJerkIn( void );
static void UI_FFS_SCurve_P2_AccelJerkOut( void );
//---------------------------------------------------------------------
static void UI_FFS_SCurve_P3_Accel( void );
static void UI_FFS_SCurve_P3_Decel( void );
static void UI_FFS_SCurve_P3_AccelJerkIn( void );
static void UI_FFS_SCurve_P3_AccelJerkOut( void );
static void UI_FFS_SCurve_P3_DecelJerkIn( void );
static void UI_FFS_SCurve_P3_DecelJerkOut( void );
static void UI_FFS_SCurve_P3_LevelingDistance( void );
//---------------------------------------------------------------------
static void UI_FFS_SCurve_P4_Accel( void );
static void UI_FFS_SCurve_P4_Decel( void );
static void UI_FFS_SCurve_P4_AccelJerkIn( void );
static void UI_FFS_SCurve_P4_AccelJerkOut( void );
static void UI_FFS_SCurve_P4_DecelJerkIn( void );
static void UI_FFS_SCurve_P4_DecelJerkOut( void );
static void UI_FFS_SCurve_P4_LevelingDistance( void );

//---------------------------------------------------------------------
static void UI_FFS_SCurve_QuickStopDecel( void );
static void UI_FFS_SCurve_LevelingDecel( void );
static void UI_FFS_SCurve_InspectionTerminalSpeed( void );
static void UI_FFS_SCurve_SoftLimitDistUp( void );
static void UI_FFS_SCurve_SoftLimitDistDown( void );
static void UI_FFS_SCurve_ShortRunMinDist( void );

static void UI_FFS_SCurve_DestOffsetUp( void );
static void UI_FFS_SCurve_DestOffsetDown( void );
static void UI_FFS_SCurve_RelevelOffsetUp( void );
static void UI_FFS_SCurve_RelevelOffsetDown( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
//---------------------------------------------------------------------

static struct st_ui_screen__freeform gstFFS_SCurve_P1_Accel =
{
   .pfnDraw = UI_FFS_SCurve_P1_Accel,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P1_Decel =
{
   .pfnDraw = UI_FFS_SCurve_P1_Decel,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P1_AccelJerkIn =
{
   .pfnDraw = UI_FFS_SCurve_P1_AccelJerkIn,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P1_AccelJerkOut =
{
   .pfnDraw = UI_FFS_SCurve_P1_AccelJerkOut,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P1_DecelJerkIn =
{
   .pfnDraw = UI_FFS_SCurve_P1_DecelJerkIn,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P1_DecelJerkOut =
{
   .pfnDraw = UI_FFS_SCurve_P1_DecelJerkOut,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P1_LevelingDistance =
{
   .pfnDraw = UI_FFS_SCurve_P1_LevelingDistance,
};
//---------------------------------------------------------------------

static struct st_ui_screen__freeform gstFFS_SCurve_P2_Accel =
{
   .pfnDraw = UI_FFS_SCurve_P2_Accel,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P2_Decel =
{
   .pfnDraw = UI_FFS_SCurve_P2_Decel,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P2_AccelJerkIn =
{
   .pfnDraw = UI_FFS_SCurve_P2_AccelJerkIn,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P2_AccelJerkOut =
{
   .pfnDraw = UI_FFS_SCurve_P2_AccelJerkOut,
};

//---------------------------------------------------------------------

static struct st_ui_screen__freeform gstFFS_SCurve_P3_Accel =
{
   .pfnDraw = UI_FFS_SCurve_P3_Accel,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P3_Decel =
{
   .pfnDraw = UI_FFS_SCurve_P3_Decel,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P3_AccelJerkIn =
{
   .pfnDraw = UI_FFS_SCurve_P3_AccelJerkIn,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P3_AccelJerkOut =
{
   .pfnDraw = UI_FFS_SCurve_P3_AccelJerkOut,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P3_DecelJerkIn =
{
   .pfnDraw = UI_FFS_SCurve_P3_DecelJerkIn,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P3_DecelJerkOut =
{
   .pfnDraw = UI_FFS_SCurve_P3_DecelJerkOut,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P3_LevelingDistance =
{
   .pfnDraw = UI_FFS_SCurve_P3_LevelingDistance,
};

//---------------------------------------------------------------------

static struct st_ui_screen__freeform gstFFS_SCurve_P4_Accel =
{
   .pfnDraw = UI_FFS_SCurve_P4_Accel,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P4_Decel =
{
   .pfnDraw = UI_FFS_SCurve_P4_Decel,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P4_AccelJerkIn =
{
   .pfnDraw = UI_FFS_SCurve_P4_AccelJerkIn,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P4_AccelJerkOut =
{
   .pfnDraw = UI_FFS_SCurve_P4_AccelJerkOut,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P4_DecelJerkIn =
{
   .pfnDraw = UI_FFS_SCurve_P4_DecelJerkIn,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P4_DecelJerkOut =
{
   .pfnDraw = UI_FFS_SCurve_P4_DecelJerkOut,
};
static struct st_ui_screen__freeform gstFFS_SCurve_P4_LevelingDistance =
{
   .pfnDraw = UI_FFS_SCurve_P4_LevelingDistance,
};
//---------------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_SCurve_QuickStopDecel =
{
   .pfnDraw = UI_FFS_SCurve_QuickStopDecel,
};
static struct st_ui_screen__freeform gstFFS_SCurve_LevelingDecel =
{
   .pfnDraw = UI_FFS_SCurve_LevelingDecel,
};
static struct st_ui_screen__freeform gstFFS_SCurve_InspectionTerminalSpeed =
{
   .pfnDraw = UI_FFS_SCurve_InspectionTerminalSpeed,
};
static struct st_ui_screen__freeform gstFFS_SCurve_SoftLimitDistUp =
{
   .pfnDraw = UI_FFS_SCurve_SoftLimitDistUp,
};
static struct st_ui_screen__freeform gstFFS_SCurve_SoftLimitDistDown =
{
   .pfnDraw = UI_FFS_SCurve_SoftLimitDistDown,
};
static struct st_ui_screen__freeform gstFFS_SCurve_ShortRunMinDist =
{
   .pfnDraw = UI_FFS_SCurve_ShortRunMinDist,
};

static struct st_ui_screen__freeform gstFFS_SCurve_DestOffsetUp =
{
   .pfnDraw = UI_FFS_SCurve_DestOffsetUp,
};
static struct st_ui_screen__freeform gstFFS_SCurve_DestOffsetDown =
{
   .pfnDraw = UI_FFS_SCurve_DestOffsetDown,
};
static struct st_ui_screen__freeform gstFFS_SCurve_RelevelOffsetUp =
{
   .pfnDraw = UI_FFS_SCurve_RelevelOffsetUp,
};
static struct st_ui_screen__freeform gstFFS_SCurve_RelevelOffsetDown =
{
   .pfnDraw = UI_FFS_SCurve_RelevelOffsetDown,
};

//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_SCurve_P1_Accel =
{
   .psTitle = "Acceleration",
   .pstUGS_Next = &gstUGS_SCurve_P1_Accel,
};
static struct st_ui_menu_item gstMI_SCurve_P1_Decel =
{
   .psTitle = "Deceleration",
   .pstUGS_Next = &gstUGS_SCurve_P1_Decel,
};
static struct st_ui_menu_item gstMI_SCurve_P1_AccelJerkIn =
{
   .psTitle = "Accel Jerk In",
   .pstUGS_Next = &gstUGS_SCurve_P1_AccelJerkIn,
};
static struct st_ui_menu_item gstMI_SCurve_P1_AccelJerkOut =
{
   .psTitle = "Accel Jerk Out",
   .pstUGS_Next = &gstUGS_SCurve_P1_AccelJerkOut,
};
static struct st_ui_menu_item gstMI_SCurve_P1_DecelJerkIn =
{
   .psTitle = "Decel Jerk In",
   .pstUGS_Next = &gstUGS_SCurve_P1_DecelJerkIn,
};
static struct st_ui_menu_item gstMI_SCurve_P1_DecelJerkOut =
{
   .psTitle = "Decel Jerk Out",
   .pstUGS_Next = &gstUGS_SCurve_P1_DecelJerkOut,
};
static struct st_ui_menu_item gstMI_SCurve_P1_LevelingDistance =
{
   .psTitle = "Leveling Distance",
   .pstUGS_Next = &gstUGS_SCurve_P1_LevelingDistance,
};

static struct st_ui_menu_item * gastMenuItems_Scurve_P1[] =
{
  &gstMI_SCurve_P1_Accel,
  &gstMI_SCurve_P1_AccelJerkIn,
  &gstMI_SCurve_P1_AccelJerkOut,

  &gstMI_SCurve_P1_Decel,
  &gstMI_SCurve_P1_DecelJerkIn,
  &gstMI_SCurve_P1_DecelJerkOut,

  &gstMI_SCurve_P1_LevelingDistance,
};
static struct st_ui_screen__menu gstMenu_Setup_Scurve_P1 =
{
   .psTitle = "Normal Profile",
   .pastMenuItems = &gastMenuItems_Scurve_P1,
   .ucNumItems = sizeof(gastMenuItems_Scurve_P1) / sizeof(gastMenuItems_Scurve_P1[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_SCurve_P2_Accel =
{
   .psTitle = "Acceleration",
   .pstUGS_Next = &gstUGS_SCurve_P2_Accel,
};
static struct st_ui_menu_item gstMI_SCurve_P2_Decel =
{
   .psTitle = "Deceleration",
   .pstUGS_Next = &gstUGS_SCurve_P2_Decel,
};
static struct st_ui_menu_item gstMI_SCurve_P2_AccelJerkIn =
{
   .psTitle = "Accel Jerk In",
   .pstUGS_Next = &gstUGS_SCurve_P2_AccelJerkIn,
};
static struct st_ui_menu_item gstMI_SCurve_P2_AccelJerkOut =
{
   .psTitle = "Accel Jerk Out",
   .pstUGS_Next = &gstUGS_SCurve_P2_AccelJerkOut,
};
static struct st_ui_menu_item * gastMenuItems_Scurve_P2[] =
{
  &gstMI_SCurve_P2_Accel,
  &gstMI_SCurve_P2_AccelJerkIn,
  &gstMI_SCurve_P2_AccelJerkOut,

  &gstMI_SCurve_P2_Decel,
};
static struct st_ui_screen__menu gstMenu_Setup_Scurve_P2 =
{
   .psTitle = "Inspection Profile",
   .pastMenuItems = &gastMenuItems_Scurve_P2,
   .ucNumItems = sizeof(gastMenuItems_Scurve_P2) / sizeof(gastMenuItems_Scurve_P2[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_SCurve_P3_Accel =
{
   .psTitle = "Acceleration",
   .pstUGS_Next = &gstUGS_SCurve_P3_Accel,
};
static struct st_ui_menu_item gstMI_SCurve_P3_Decel =
{
   .psTitle = "Deceleration",
   .pstUGS_Next = &gstUGS_SCurve_P3_Decel,
};
static struct st_ui_menu_item gstMI_SCurve_P3_AccelJerkIn =
{
   .psTitle = "Accel Jerk In",
   .pstUGS_Next = &gstUGS_SCurve_P3_AccelJerkIn,
};
static struct st_ui_menu_item gstMI_SCurve_P3_AccelJerkOut =
{
   .psTitle = "Accel Jerk Out",
   .pstUGS_Next = &gstUGS_SCurve_P3_AccelJerkOut,
};
static struct st_ui_menu_item gstMI_SCurve_P3_DecelJerkIn =
{
   .psTitle = "Decel Jerk In",
   .pstUGS_Next = &gstUGS_SCurve_P3_DecelJerkIn,
};
static struct st_ui_menu_item gstMI_SCurve_P3_DecelJerkOut =
{
   .psTitle = "Decel Jerk Out",
   .pstUGS_Next = &gstUGS_SCurve_P3_DecelJerkOut,
};
static struct st_ui_menu_item gstMI_SCurve_P3_LevelingDistance =
{
   .psTitle = "Leveling Distance",
   .pstUGS_Next = &gstUGS_SCurve_P3_LevelingDistance,
};

static struct st_ui_menu_item * gastMenuItems_Scurve_P3[] =
{
  &gstMI_SCurve_P3_Accel,
  &gstMI_SCurve_P3_AccelJerkIn,
  &gstMI_SCurve_P3_AccelJerkOut,

  &gstMI_SCurve_P3_Decel,
  &gstMI_SCurve_P3_DecelJerkIn,
  &gstMI_SCurve_P3_DecelJerkOut,

  &gstMI_SCurve_P3_LevelingDistance,
};
static struct st_ui_screen__menu gstMenu_Setup_Scurve_P3 =
{
   .psTitle = "E-Power Profile",
   .pastMenuItems = &gastMenuItems_Scurve_P3,
   .ucNumItems = sizeof(gastMenuItems_Scurve_P3) / sizeof(gastMenuItems_Scurve_P3[ 0 ]),
};

//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_SCurve_P4_Accel =
{
   .psTitle = "Acceleration",
   .pstUGS_Next = &gstUGS_SCurve_P4_Accel,
};
static struct st_ui_menu_item gstMI_SCurve_P4_Decel =
{
   .psTitle = "Deceleration",
   .pstUGS_Next = &gstUGS_SCurve_P4_Decel,
};
static struct st_ui_menu_item gstMI_SCurve_P4_AccelJerkIn =
{
   .psTitle = "Accel Jerk In",
   .pstUGS_Next = &gstUGS_SCurve_P4_AccelJerkIn,
};
static struct st_ui_menu_item gstMI_SCurve_P4_AccelJerkOut =
{
   .psTitle = "Accel Jerk Out",
   .pstUGS_Next = &gstUGS_SCurve_P4_AccelJerkOut,
};
static struct st_ui_menu_item gstMI_SCurve_P4_DecelJerkIn =
{
   .psTitle = "Decel Jerk In",
   .pstUGS_Next = &gstUGS_SCurve_P4_DecelJerkIn,
};
static struct st_ui_menu_item gstMI_SCurve_P4_DecelJerkOut =
{
   .psTitle = "Decel Jerk Out",
   .pstUGS_Next = &gstUGS_SCurve_P4_DecelJerkOut,
};
static struct st_ui_menu_item gstMI_SCurve_P4_LevelingDistance =
{
   .psTitle = "Leveling Distance",
   .pstUGS_Next = &gstUGS_SCurve_P4_LevelingDistance,
};

static struct st_ui_menu_item * gastMenuItems_Scurve_P4[] =
{
  &gstMI_SCurve_P4_Accel,
  &gstMI_SCurve_P4_AccelJerkIn,
  &gstMI_SCurve_P4_AccelJerkOut,

  &gstMI_SCurve_P4_Decel,
  &gstMI_SCurve_P4_DecelJerkIn,
  &gstMI_SCurve_P4_DecelJerkOut,

  &gstMI_SCurve_P4_LevelingDistance,
};
static struct st_ui_screen__menu gstMenu_Setup_Scurve_P4 =
{
   .psTitle = "Short Profile",
   .pastMenuItems = &gastMenuItems_Scurve_P4,
   .ucNumItems = sizeof(gastMenuItems_Scurve_P4) / sizeof(gastMenuItems_Scurve_P4[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_SCurve_P1 =
{
   .psTitle = "Normal Profile",
   .pstUGS_Next = &gstUGS_Menu_Scurve_P1,
};
static struct st_ui_menu_item gstMI_SCurve_P2 =
{
   .psTitle = "Inspection Profile",
   .pstUGS_Next = &gstUGS_Menu_Scurve_P2,
};
static struct st_ui_menu_item gstMI_SCurve_P3 =
{
   .psTitle = "E-Power Profile",
   .pstUGS_Next = &gstUGS_Menu_Scurve_P3,
};
static struct st_ui_menu_item gstMI_SCurve_P4 =
{
   .psTitle = "Short Profile",
   .pstUGS_Next = &gstUGS_Menu_Scurve_P4,
};
static struct st_ui_menu_item gstMI_SCurve_QuickStopDecel =
{
   .psTitle = "Quick Stop Decel",
   .pstUGS_Next = &gstUGS_SCurve_QuickStopDecel,
};
static struct st_ui_menu_item gstMI_SCurve_LevelingDecel =
{
   .psTitle = "Leveling Decel",
   .pstUGS_Next = &gstUGS_SCurve_LevelingDecel,
};
static struct st_ui_menu_item gstMI_SCurve_SoftLimitDistUp =
{
   .psTitle = "Soft Limit Dist Up",
   .pstUGS_Next = &gstUGS_SCurve_SoftLimitDistUp,
};
static struct st_ui_menu_item gstMI_SCurve_SoftLimitDistDown =
{
   .psTitle = "Soft Limit Dist Down",
   .pstUGS_Next = &gstUGS_SCurve_SoftLimitDistDown,
};
static struct st_ui_menu_item gstMI_SCurve_ShortRunMinDist =
{
   .psTitle = "Short Run Min Dist",
   .pstUGS_Next = &gstUGS_SCurve_ShortRunMinDist,
};
static struct st_ui_menu_item gstMI_SCurve_DestOffsetUp =
{
   .psTitle = "Dest. Offset Up",
   .pstUGS_Next = &gstUGS_SCurve_DestOffsetUp,
};
static struct st_ui_menu_item gstMI_SCurve_DestOffsetDown =
{
   .psTitle = "Dest. Offset Down",
   .pstUGS_Next = &gstUGS_SCurve_DestOffsetDown,
};
static struct st_ui_menu_item gstMI_SCurve_RelevelOffsetUp =
{
   .psTitle = "Relevel Offset Up",
   .pstUGS_Next = &gstUGS_SCurve_RelevelOffsetUp,
};
static struct st_ui_menu_item gstMI_SCurve_RelevelOffsetDown =
{
   .psTitle = "Relevel Offset Down",
   .pstUGS_Next = &gstUGS_SCurve_RelevelOffsetDown,
};
static struct st_ui_menu_item * gastMenuItems_Scurve[] =
{
   &gstMI_SCurve_P1,
   &gstMI_SCurve_P2,
   &gstMI_SCurve_P3,
   &gstMI_SCurve_P4,
   &gstMI_SCurve_QuickStopDecel,
   &gstMI_SCurve_LevelingDecel,
   &gstMI_SCurve_SoftLimitDistUp,
   &gstMI_SCurve_SoftLimitDistDown,
   &gstMI_SCurve_ShortRunMinDist,
   &gstMI_SCurve_DestOffsetUp,
   &gstMI_SCurve_DestOffsetDown,
   &gstMI_SCurve_RelevelOffsetUp,
   &gstMI_SCurve_RelevelOffsetDown,
};
static struct st_ui_screen__menu gstMenu_Setup_Scurve =
{
   .psTitle = "S-Curve",
   .pastMenuItems = &gastMenuItems_Scurve,
   .ucNumItems = sizeof(gastMenuItems_Scurve) / sizeof(gastMenuItems_Scurve[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_SCurve_P1_Accel =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P1_Accel,
};
struct st_ui_generic_screen gstUGS_SCurve_P1_Decel =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P1_Decel,
};
struct st_ui_generic_screen gstUGS_SCurve_P1_AccelJerkIn =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P1_AccelJerkIn,
};
struct st_ui_generic_screen gstUGS_SCurve_P1_AccelJerkOut =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P1_AccelJerkOut,
};
struct st_ui_generic_screen gstUGS_SCurve_P1_DecelJerkIn =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P1_DecelJerkIn,
};
struct st_ui_generic_screen gstUGS_SCurve_P1_DecelJerkOut =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P1_DecelJerkOut,
};
struct st_ui_generic_screen gstUGS_SCurve_P1_LevelingDistance =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P1_LevelingDistance,
};

//----------------------------------------------------
struct st_ui_generic_screen gstUGS_SCurve_P2_Accel =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P2_Accel,
};
struct st_ui_generic_screen gstUGS_SCurve_P2_Decel =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P2_Decel,
};
struct st_ui_generic_screen gstUGS_SCurve_P2_AccelJerkIn =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P2_AccelJerkIn,
};
struct st_ui_generic_screen gstUGS_SCurve_P2_AccelJerkOut =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P2_AccelJerkOut,
};

//----------------------------------------------------
struct st_ui_generic_screen gstUGS_SCurve_P3_Accel =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P3_Accel,
};
struct st_ui_generic_screen gstUGS_SCurve_P3_Decel =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P3_Decel,
};
struct st_ui_generic_screen gstUGS_SCurve_P3_AccelJerkIn =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P3_AccelJerkIn,
};
struct st_ui_generic_screen gstUGS_SCurve_P3_AccelJerkOut =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P3_AccelJerkOut,
};
struct st_ui_generic_screen gstUGS_SCurve_P3_DecelJerkIn =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P3_DecelJerkIn,
};
struct st_ui_generic_screen gstUGS_SCurve_P3_DecelJerkOut =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P3_DecelJerkOut,
};
struct st_ui_generic_screen gstUGS_SCurve_P3_LevelingDistance =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P3_LevelingDistance,
};
//----------------------------------------------------
struct st_ui_generic_screen gstUGS_SCurve_P4_Accel =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P4_Accel,
};
struct st_ui_generic_screen gstUGS_SCurve_P4_Decel =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P4_Decel,
};
struct st_ui_generic_screen gstUGS_SCurve_P4_AccelJerkIn =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P4_AccelJerkIn,
};
struct st_ui_generic_screen gstUGS_SCurve_P4_AccelJerkOut =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P4_AccelJerkOut,
};
struct st_ui_generic_screen gstUGS_SCurve_P4_DecelJerkIn =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P4_DecelJerkIn,
};
struct st_ui_generic_screen gstUGS_SCurve_P4_DecelJerkOut =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P4_DecelJerkOut,
};
struct st_ui_generic_screen gstUGS_SCurve_P4_LevelingDistance =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_P4_LevelingDistance,
};
//----------------------------------------------------
struct st_ui_generic_screen gstUGS_SCurve_QuickStopDecel =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_QuickStopDecel,
};
struct st_ui_generic_screen gstUGS_SCurve_LevelingDecel =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_LevelingDecel,
};
struct st_ui_generic_screen gstUGS_SCurve_InspectionTerminalSpeed =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_InspectionTerminalSpeed,
};
struct st_ui_generic_screen gstUGS_SCurve_SoftLimitDistUp =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_SoftLimitDistUp,
};
struct st_ui_generic_screen gstUGS_SCurve_SoftLimitDistDown =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_SoftLimitDistDown,
};
struct st_ui_generic_screen gstUGS_SCurve_ShortRunMinDist =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_ShortRunMinDist,
};
struct st_ui_generic_screen gstUGS_SCurve_DestOffsetUp =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_DestOffsetUp,
};
struct st_ui_generic_screen gstUGS_SCurve_DestOffsetDown =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_DestOffsetDown,
};
struct st_ui_generic_screen gstUGS_SCurve_RelevelOffsetUp =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_RelevelOffsetUp,
};
struct st_ui_generic_screen gstUGS_SCurve_RelevelOffsetDown =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SCurve_RelevelOffsetDown,
};

//----------------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_Scurve_P1 =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Scurve_P1,
};
struct st_ui_generic_screen gstUGS_Menu_Scurve_P2 =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Scurve_P2,
};
struct st_ui_generic_screen gstUGS_Menu_Scurve_P3 =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Scurve_P3,
};
struct st_ui_generic_screen gstUGS_Menu_Scurve_P4 =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Scurve_P4,
};
struct st_ui_generic_screen gstUGS_Menu_Scurve =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Scurve,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

static void UI_FFS_Edit_SCurve_Speed( en_edit_scurve_param_speed enParam, uint32_t ulParamNumber )
{
   static struct st_param_edit_menu stParamEdit;

   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.enDecimalFormat    = enNumberDecimalPoints_One;
   stParamEdit.uwParamIndex_Start = ulParamNumber;
   stParamEdit.uwParamIndex_End   = stParamEdit.uwParamIndex_Start;

   switch ( enParam )
   {
      case EDIT_SCURVE_ACCEL:           stParamEdit.psTitle = "Accel";             break;
      case EDIT_SCURVE_DECEL:           stParamEdit.psTitle = "Decel";             break;
      case EDIT_SCURVE_JERK_IN_ACCEL:   stParamEdit.psTitle = "Jerk In Accel";     break;
      case EDIT_SCURVE_JERK_OUT_ACCEL:  stParamEdit.psTitle = "Jerk Out Accel";    break;
      case EDIT_SCURVE_JERK_IN_DECEL:   stParamEdit.psTitle = "Jerk In Decel";     break;
      case EDIT_SCURVE_JERK_OUT_DECEL:  stParamEdit.psTitle = "Jerk Out Decel";    break;
      case EDIT_SCURVE_QUICK_STOP:      stParamEdit.psTitle = "Quick Stop Decel";  break;
      case EDIT_SCURVE_LVLING_DECEL:    stParamEdit.psTitle = "Leveling Decel";    break;

      default:
         break;
   }

   switch ( enParam )
   {
      case EDIT_SCURVE_ACCEL:
      case EDIT_SCURVE_DECEL:
         stParamEdit.psUnit = " ft/s^2";
         stParamEdit.ulValue_Max = MAX_ACCEL_X10;
         stParamEdit.ulValue_Min = MIN_ACCEL_X10;
         break;

      case EDIT_SCURVE_QUICK_STOP:
      case EDIT_SCURVE_LVLING_DECEL:
         stParamEdit.psUnit = " ft/s^2";
         stParamEdit.ulValue_Max = 0;
         stParamEdit.ulValue_Min = 0;
         break;

      case EDIT_SCURVE_JERK_IN_ACCEL:
      case EDIT_SCURVE_JERK_OUT_ACCEL:
      case EDIT_SCURVE_JERK_IN_DECEL:
      case EDIT_SCURVE_JERK_OUT_DECEL:
         stParamEdit.psUnit = " ft/s^3";
         stParamEdit.ulValue_Max = MAX_JERK_X10;
         stParamEdit.ulValue_Min = MIN_JERK_X10;
         break;

      default:
         break;
   }

   UI_ParamEditSceenTemplate(&stParamEdit);
}

static void UI_FFS_Edit_SCurve_Distance(en_edit_scurve_param_distance enParam, uint32_t ulParamNumber)
{
   static struct st_param_edit_menu stParamEdit;

   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = ulParamNumber;
   stParamEdit.uwParamIndex_End = stParamEdit.uwParamIndex_Start;

   switch (enParam)
   {
      case EDIT_SCURVE_LVLING_DISTANCE:   stParamEdit.psTitle = "Leveling Distance";   break;
      case EDIT_SCURVE_DEST_OFFSET_UP:    stParamEdit.psTitle = "Dest. Offset Up";     break;
      case EDIT_SCURVE_DEST_OFFSET_DOWN:  stParamEdit.psTitle = "Dest. Offset Down";   break;
      case EDIT_SCURVE_RELVL_OFFSET_UP:   stParamEdit.psTitle = "Relevel Offset Up";   break;
      case EDIT_SCURVE_RELVL_OFFSET_DOWN: stParamEdit.psTitle = "Relevel Offset Down"; break;

      default:
         break;
   }


   switch (enParam)
   {
      case EDIT_SCURVE_LVLING_DISTANCE:
         stParamEdit.psUnit = " inches";
         stParamEdit.ulValue_Max = MAX_LEVELING_DISTANCE_5MM;
         stParamEdit.ulValue_Min = 0;
         break;

      case EDIT_SCURVE_DEST_OFFSET_UP:
      case EDIT_SCURVE_DEST_OFFSET_DOWN:
      case EDIT_SCURVE_RELVL_OFFSET_UP:
      case EDIT_SCURVE_RELVL_OFFSET_DOWN:
         stParamEdit.psUnit = "/0.5mm";
         stParamEdit.ulValue_Max = 0;
         stParamEdit.ulValue_Min = 0;
         break;

      default:
         break;
   }

   UI_ParamEditSceenTemplate(&stParamEdit);
}


static void UI_FFS_SCurve_P1_Accel( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_ACCEL, enPARAM8__P1_Accel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P1_Decel( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_DECEL, enPARAM8__P1_Decel_x10);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P1_AccelJerkIn( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_JERK_IN_ACCEL, enPARAM8__P1_JerkInAccel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P1_AccelJerkOut( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_JERK_OUT_ACCEL, enPARAM8__P1_JerkOutAccel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P1_DecelJerkIn( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_JERK_IN_DECEL, enPARAM8__P1_JerkInDecel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P1_DecelJerkOut( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_JERK_OUT_DECEL, enPARAM8__P1_JerkOutDecel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P1_LevelingDistance( void )
{
   UI_FFS_Edit_SCurve_Distance( EDIT_SCURVE_LVLING_DISTANCE, enPARAM8__P1_LevelingDistance_5mm );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P2_Accel( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_ACCEL, enPARAM8__P2_Accel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P2_Decel( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_DECEL, enPARAM8__P2_Decel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P2_AccelJerkIn( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_JERK_IN_ACCEL, enPARAM8__P2_JerkInAccel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P2_AccelJerkOut( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_JERK_OUT_ACCEL, enPARAM8__P2_JerkOutAccel_x10 );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P3_Accel( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_ACCEL, enPARAM8__P3_Accel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P3_Decel( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_DECEL, enPARAM8__P3_Decel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P3_AccelJerkIn( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_JERK_IN_ACCEL, enPARAM8__P3_JerkInAccel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P3_AccelJerkOut( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_JERK_OUT_ACCEL, enPARAM8__P3_JerkOutAccel_x10 );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P3_DecelJerkIn( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_JERK_IN_DECEL, enPARAM8__P3_JerkInDecel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P3_DecelJerkOut( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_JERK_OUT_DECEL, enPARAM8__P3_JerkOutDecel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P3_LevelingDistance( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_LVLING_DISTANCE, enPARAM8__P3_LevelingDistance_5mm );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P4_Accel( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_ACCEL, enPARAM8__P4_Accel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P4_Decel( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_DECEL, enPARAM8__P4_Decel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P4_AccelJerkIn( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_JERK_IN_ACCEL, enPARAM8__P4_JerkInAccel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P4_AccelJerkOut( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_JERK_OUT_ACCEL, enPARAM8__P4_JerkOutAccel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P4_DecelJerkIn( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_JERK_IN_DECEL, enPARAM8__P4_JerkInDecel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P4_DecelJerkOut( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_JERK_OUT_DECEL, enPARAM8__P4_JerkOutDecel_x10 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_P4_LevelingDistance( void )
{
   UI_FFS_Edit_SCurve_Speed( EDIT_SCURVE_LVLING_DISTANCE, enPARAM8__P4_LevelingDistance_5mm );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_QuickStopDecel( void )
{
   UI_FFS_Edit_SCurve_Speed(EDIT_SCURVE_QUICK_STOP, enPARAM8__QuickStopDecel_x10);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_LevelingDecel( void )
{
   UI_FFS_Edit_SCurve_Speed(EDIT_SCURVE_LVLING_DECEL, enPARAM8__LevelingDecel_01fps);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_InspectionTerminalSpeed( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__InspectionTerminalSpeed;
   stParamEdit.uwParamIndex_End = enPARAM16__InspectionTerminalSpeed;
   stParamEdit.psTitle = "Terminal Speed on Inspection";
   stParamEdit.psUnit = "fpm";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_SoftLimitDistUp( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__SoftLimitDistance_UP;
   stParamEdit.uwParamIndex_End = enPARAM16__SoftLimitDistance_UP;
   stParamEdit.psTitle = "Soft Limit Dist Up";
   stParamEdit.psUnit = " ft.";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_SoftLimitDistDown( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__SoftLimitDistance_DN;
   stParamEdit.uwParamIndex_End = enPARAM16__SoftLimitDistance_DN;
   stParamEdit.psTitle = "Soft Limit Dist Down";
   stParamEdit.psUnit = " ft.";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_ShortRunMinDist( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__ShortProfileMinDist_ft;
   stParamEdit.uwParamIndex_End = enPARAM8__ShortProfileMinDist_ft;
   stParamEdit.psTitle = "Short Run Min Dist";
   stParamEdit.psUnit = " ft.";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_DestOffsetUp( void )
{
   UI_FFS_Edit_SCurve_Distance(EDIT_SCURVE_DEST_OFFSET_UP, enPARAM8__DestOffsetUp_05mm);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_DestOffsetDown( void )
{
   UI_FFS_Edit_SCurve_Distance(EDIT_SCURVE_DEST_OFFSET_DOWN, enPARAM8__DestOffsetDown_05mm);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_RelevelOffsetUp( void )
{
   UI_FFS_Edit_SCurve_Distance(EDIT_SCURVE_RELVL_OFFSET_UP, enPARAM8__RelevelOffsetUp_05mm);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SCurve_RelevelOffsetDown( void )
{
   UI_FFS_Edit_SCurve_Distance(EDIT_SCURVE_RELVL_OFFSET_DOWN, enPARAM8__RelevelOffsetDown_05mm);
}


/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
