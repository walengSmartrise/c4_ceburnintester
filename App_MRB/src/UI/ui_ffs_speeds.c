/******************************************************************************
 *
 * @file     ui_ffs_speeds.c
 * @brief    Speeds UI pages
 * @version  V1.00
 * @date     13, Feb 2018
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Speeds_Contract( void );
static void UI_FFS_Speeds_Inspection( void );
static void UI_FFS_Speeds_Learn( void );
static void UI_FFS_Speeds_Terminal( void );
static void UI_FFS_Speeds_Leveling( void );
static void UI_FFS_Speeds_MinAccel(void );
static void UI_FFS_Speeds_NTSD(void);
static void UI_FFS_Speeds_Test_AscDescSpeed(void);
static void UI_FFS_Speeds_Test_BufferSpeed(void);
static void UI_FFS_Speeds_MinRelevel(void );
static void UI_FFS_Speeds_EPowerSpeed(void );
static void UI_FFS_Speeds_AccessSpeed(void );
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_Speeds_Contract =
{
   .pfnDraw = UI_FFS_Speeds_Contract,
};

static struct st_ui_screen__freeform gstFFS_Speeds_Inspection =
{
   .pfnDraw = UI_FFS_Speeds_Inspection,
};

static struct st_ui_screen__freeform gstFFS_Speeds_Learn =
{
   .pfnDraw = UI_FFS_Speeds_Learn,
};
static struct st_ui_screen__freeform gstFFS_Speeds_Terminal =
{
   .pfnDraw = UI_FFS_Speeds_Terminal,
};
static struct st_ui_screen__freeform gstFFS_Speeds_Leveling =
{
   .pfnDraw = UI_FFS_Speeds_Leveling,
};
static struct st_ui_screen__freeform gstFFS_Speeds_MinAccel =
{
   .pfnDraw = UI_FFS_Speeds_MinAccel,
};
static struct st_ui_screen__freeform gstFFS_Speeds_NTSD =
{
   .pfnDraw = UI_FFS_Speeds_NTSD,
};
static struct st_ui_screen__freeform gstFFS_Speeds_Test_AscDescSpeed =
{
   .pfnDraw = UI_FFS_Speeds_Test_AscDescSpeed,
};
static struct st_ui_screen__freeform gstFFS_Speeds_Test_BufferSpeed =
{
   .pfnDraw = UI_FFS_Speeds_Test_BufferSpeed,
};
static struct st_ui_screen__freeform gstFFS_Speeds_MinRelevel =
{
   .pfnDraw = UI_FFS_Speeds_MinRelevel,
};
static struct st_ui_screen__freeform gstFFS_Speeds_EPowerSpeed =
{
   .pfnDraw = UI_FFS_Speeds_EPowerSpeed,
};
static struct st_ui_screen__freeform gstFFS_Speeds_AccessSpeed =
{
   .pfnDraw = UI_FFS_Speeds_AccessSpeed,
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Speeds_Contract =
{
   .psTitle = "Contract Speed",
   .pstUGS_Next = &gstUGS_Speeds_Contract,
};

static struct st_ui_menu_item gstMI_Speeds_Inspection =
{
   .psTitle = "Inspection Speed",
   .pstUGS_Next = &gstUGS_Speeds_Inspection,
};

static struct st_ui_menu_item gstMI_Speeds_Learn =
{
   .psTitle = "Learn Speed",
   .pstUGS_Next = &gstUGS_Speeds_Learn,
};
static struct st_ui_menu_item gstMI_Speeds_Terminal =
{
   .psTitle = "Terminal Speed",
   .pstUGS_Next = &gstUGS_Speeds_Terminal,
};
static struct st_ui_menu_item gstMI_Speeds_Leveling =
{
   .psTitle = "Leveling Speed",
   .pstUGS_Next = &gstUGS_Speeds_Leveling,
};
static struct st_ui_menu_item gstMI_Speeds_MinAccel =
{
   .psTitle = "Min Accel Speed",
   .pstUGS_Next = &gstUGS_Speeds_MinAccel,
};
static struct st_ui_menu_item gstMI_Speeds_NTSD =
{
   .psTitle = "NTSD Speed",
   .pstUGS_Next = &gstUGS_Speeds_NTSD,
};
static struct st_ui_menu_item gstMI_Speeds_Test_AscDescSpeed =
{
   .psTitle = "Test A/D Speed",
   .pstUGS_Next = &gstUGS_Speeds_Test_AscDescSpeed,
};
static struct st_ui_menu_item gstMI_Speeds_Test_BufferSpeed =
{
   .psTitle = "Test Buffer Speed",
   .pstUGS_Next = &gstUGS_Speeds_Test_BufferSpeed,
};
static struct st_ui_menu_item gstMI_Speeds_MinRelevel =
{
   .psTitle = "Min Relevel Speed",
   .pstUGS_Next = &gstUGS_Speeds_MinRelevel,
};
static struct st_ui_menu_item gstMI_Speeds_EPowerSpeed =
{
   .psTitle = "EPower Speed",
   .pstUGS_Next = &gstUGS_Speeds_EPowerSpeed,
};
static struct st_ui_menu_item gstMI_Speeds_AccessSpeed =
{
   .psTitle = "Access Speed",
   .pstUGS_Next = &gstUGS_Speeds_AccessSpeed,
};
static struct st_ui_menu_item * gastMenuItems_Speeds[] =
{
   &gstMI_Speeds_Contract,
   &gstMI_Speeds_Inspection,
   &gstMI_Speeds_AccessSpeed,
   &gstMI_Speeds_Learn,
   &gstMI_Speeds_EPowerSpeed,
   &gstMI_Speeds_Terminal,
   &gstMI_Speeds_Leveling,
   &gstMI_Speeds_NTSD,
   &gstMI_Speeds_MinAccel,
   &gstMI_Speeds_MinRelevel,
   &gstMI_Speeds_Test_AscDescSpeed,
   &gstMI_Speeds_Test_BufferSpeed,
};
static struct st_ui_screen__menu gstMenu_Speeds =
{
   .psTitle = "Speeds",
   .pastMenuItems = &gastMenuItems_Speeds,
   .ucNumItems = sizeof(gastMenuItems_Speeds) / sizeof(gastMenuItems_Speeds[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_Speeds_Contract =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Speeds_Contract,
};
struct st_ui_generic_screen gstUGS_Speeds_Inspection =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Speeds_Inspection,
};
struct st_ui_generic_screen gstUGS_Speeds_Learn =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Speeds_Learn,
};
struct st_ui_generic_screen gstUGS_Speeds_Terminal =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Speeds_Terminal,
};
struct st_ui_generic_screen gstUGS_Speeds_Leveling=
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Speeds_Leveling,
};
struct st_ui_generic_screen gstUGS_Speeds_MinAccel=
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Speeds_MinAccel,
};
struct st_ui_generic_screen gstUGS_Speeds_NTSD=
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Speeds_NTSD,
};
struct st_ui_generic_screen gstUGS_Speeds_Test_AscDescSpeed =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Speeds_Test_AscDescSpeed,
};
struct st_ui_generic_screen gstUGS_Speeds_Test_BufferSpeed =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Speeds_Test_BufferSpeed,
};
struct st_ui_generic_screen gstUGS_Speeds_MinRelevel =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Speeds_MinRelevel,
};
struct st_ui_generic_screen gstUGS_Speeds_EPowerSpeed =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Speeds_EPowerSpeed,
};
struct st_ui_generic_screen gstUGS_Speeds_AccessSpeed =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Speeds_AccessSpeed,
};
//-----------------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_Setup_Speeds =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Speeds,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Speeds_Contract( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__ContractSpeed;
   stParamEdit.uwParamIndex_End = enPARAM16__ContractSpeed;
   stParamEdit.psTitle = "Contract Speed";
   stParamEdit.psUnit = " fpm";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Speeds_Inspection( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__InspectionSpeed;
   stParamEdit.uwParamIndex_End = enPARAM16__InspectionSpeed;
   stParamEdit.psTitle = "Inspection Speed";
   stParamEdit.psUnit = " fpm";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Speeds_Learn( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__LearnSpeed;
   stParamEdit.uwParamIndex_End = enPARAM16__LearnSpeed;
   stParamEdit.psTitle = "Learn Speed";
   stParamEdit.psUnit = " fpm";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Speeds_Terminal( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__InspectionTerminalSpeed;
   stParamEdit.uwParamIndex_End = enPARAM16__InspectionTerminalSpeed;
   stParamEdit.psTitle = "Terminal Speed";
   stParamEdit.psUnit = " fpm";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Speeds_Leveling( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__LevelingSpeed;
   stParamEdit.uwParamIndex_End = enPARAM16__LevelingSpeed;
   stParamEdit.psTitle = "Leveling Speed";
   stParamEdit.psUnit = " fpm";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Speeds_MinAccel( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__MinAccelSpeed;
   stParamEdit.uwParamIndex_End = enPARAM16__MinAccelSpeed;
   stParamEdit.psTitle = "Min Accel Speed";
   stParamEdit.psUnit = " fpm";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Speeds_NTSD( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__NTSD_Speed;
   stParamEdit.uwParamIndex_End = enPARAM8__NTSD_Speed;
   stParamEdit.psTitle = "NTSD Speed";
   stParamEdit.psUnit = " fpm";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Speeds_Test_AscDescSpeed( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__Acceptance_AscOrDescSpeed;
   stParamEdit.uwParamIndex_End = enPARAM16__Acceptance_AscOrDescSpeed;
   stParamEdit.psTitle = "Asc/Desc Speed";
   stParamEdit.psUnit = " fpm";
   UI_ParamEditSceenTemplate(&stParamEdit);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Speeds_Test_BufferSpeed( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__Acceptance_BufferSpeed;
   stParamEdit.uwParamIndex_End = enPARAM16__Acceptance_BufferSpeed;
   stParamEdit.psTitle = "Buffer Speed";
   stParamEdit.psUnit = " fpm";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Speeds_MinRelevel( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__MinRelevelSpeed;
   stParamEdit.uwParamIndex_End = enPARAM8__MinRelevelSpeed;
   stParamEdit.psTitle = "Min Relevel Speed";
   stParamEdit.psUnit = " fpm";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Speeds_EPowerSpeed( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__EPowerSpeed_fpm;
   stParamEdit.uwParamIndex_End = enPARAM16__EPowerSpeed_fpm;
   stParamEdit.psTitle = "EPower Speed";
   stParamEdit.psUnit = " fpm";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Speeds_AccessSpeed( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__AccessSpeed_fpm;
   stParamEdit.uwParamIndex_End = enPARAM8__AccessSpeed_fpm;
   stParamEdit.psTitle = "Access Speed";
   stParamEdit.psUnit = " fpm";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
