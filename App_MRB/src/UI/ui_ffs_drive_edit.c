/******************************************************************************
 *
 * @file     ui_menu_setup.c
 * @brief    Setup Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "mod.h"
#include "sru.h"
#include <string.h>
#include <math.h>
#include "lcd.h"
#include "sys_drive.h"
#include "param_edit_protocol.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Drive_EditParameter();
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t ucCursorX;
st_drive_edit_cmd stEditCmd;
//---------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Drive_EditParameter =
{
   .pfnDraw = UI_FFS_Drive_EditParameter,
};

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Drive_EditParameter =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Drive_EditParameter,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define DRIVE_WRITE_COUNTDOWN_30MS     (10)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
|01234567890123456789|

|ID     Value    Save|
|65535 +2147483647 | |
| *                  |
|Saved:-2147483648   |
 -----------------------------------------------------------------------------*/
static void UpdateEditParameterScreen( void )
{
   LCD_Char_Clear();

   /* Line 1 */
   LCD_Char_GotoXY(0, 0);
   LCD_Char_WriteString("ID     Value    ");

   /* Disable save option if currently writing */
   if( stEditCmd.eCommand != DRIVE_CMD__WRITE )
   {
      LCD_Char_WriteString("Save");
   }

   /* Line 2 */
   LCD_Char_GotoXY(0, 1);
   LCD_Char_WriteInteger_ExactLength(stEditCmd.uwID, 5);

   LCD_Char_GotoXY(6, 1);
   if(stEditCmd.iValue >= 0)
   {
      LCD_Char_WriteString("+");
      LCD_Char_WriteInteger_ExactLength(stEditCmd.iValue, 10);
   }
   else
   {
      LCD_Char_WriteString("-");
      LCD_Char_WriteInteger_ExactLength(-1*stEditCmd.iValue, 10);
   }

   /* Disable save option if currently writing */
   if( stEditCmd.eCommand != DRIVE_CMD__WRITE )
   {
      LCD_Char_GotoXY(19, 1);
      LCD_Char_WriteString("|");
   }

   /* Line 3 */
   switch( ucCursorX )
   {
      case 0 ... 4:
      {
         LCD_Char_GotoXY(ucCursorX, 2);
         break;
      }
      case 5 ... 14:
      {
         LCD_Char_GotoXY(ucCursorX+2, 2);
         break;
      }
      default:
         LCD_Char_GotoXY(19, 2);
         break;
   }
   LCD_Char_WriteString("*");

   /* Line 4 */
   LCD_Char_GotoXY(0, 3);
   LCD_Char_WriteString("Saved:");
   uint16_t uwReadParamID = GetDriveData_ParameterID();
   int32_t iSavedValue = GetDriveData_ParameterValue();

   /* EXCEPTION FOR MAG DRIVES. RETURNED VALUES ARE 24BIT, BUT REPRESENT SIGNED INT16_T VALUES.
    * SIGN EXTENSION REQUIRED */
   if(( Param_ReadValue_8Bit(enPARAM8__DriveSelect) == enDriveType__HPV )
   && ( iSavedValue & 0x00008000 )
  && (( iSavedValue & 0xFFFF0000 ) == 0 ))
   {
      iSavedValue |=0xFFFF0000;
   }

   if(uwReadParamID == stEditCmd.uwID)
   {
      if(iSavedValue >= 0)
      {
         LCD_Char_WriteString("+");
         LCD_Char_WriteInteger_ExactLength(iSavedValue, 10);
      }
      else
      {
         LCD_Char_WriteString("-");
         LCD_Char_WriteInteger_ExactLength(-1*iSavedValue, 10);
      }
   }
   else
   {
      LCD_Char_WriteString("(FETCHING)");
   }
}
static void UI_FFS_Drive_EditParameter( void )
{
   if( GetOperation_ClassOfOp() != CLASSOP__MANUAL )
   {
      LCD_Char_Clear();
      LCD_Char_GotoXY(2, 1);
      LCD_Char_WriteString("MOVE TO INSPECTION");
      enum en_keypresses eKeypress = Button_GetKeypress();
      if(eKeypress != enKEYPRESS_NONE)
      {
         PrevScreen();
      }
   }
   else
   {
      GetEditDriveParameterCommand(&stEditCmd);
      /* Control */
      uint8_t bSave = 0;
      int32_t iEditID = stEditCmd.uwID;
      int32_t iEditValue = stEditCmd.iValue;
      enum en_keypresses eKeypress = Button_GetKeypress();
      switch(eKeypress)
       {
          case enKEYPRESS_LEFT:
             if(ucCursorX)
             {
                ucCursorX--;
             }
             else
             {
                PrevScreen();
             }
             break;

          case enKEYPRESS_RIGHT:
             if(ucCursorX < 15)
             {
                ucCursorX++;
             }
             break;

          case enKEYPRESS_UP:
             switch( ucCursorX )
             {
                case 0 ... 4:
                {
                   int32_t iMagnitude = pow(10, (4-ucCursorX));
                   iEditID += iMagnitude;
                   break;
                }
                case 5 ... 14:
                {
                   int32_t iMagnitude = pow(10, (14-ucCursorX));
                   iEditValue += iMagnitude;
                   break;
                }
                default: break;
             }
             break;

          case enKEYPRESS_DOWN:
             switch( ucCursorX )
             {
                case 0 ... 4:
                {
                   int32_t iMagnitude = pow(10, (4-ucCursorX));
                   iEditID -= iMagnitude;
                   break;
                }
                case 5 ... 14:
                {
                   int32_t iMagnitude = pow(10, (14-ucCursorX));
                   iEditValue -= iMagnitude;
                   break;
                }
                default: break;
             }
             break;

          case enKEYPRESS_ENTER:
             if(ucCursorX == 15)  /* save column */
             {
                bSave = 1;
             }
             break;

          default:
             break;
       }

      /* Bound values */
      if( iEditID > 65535 )
      {
         iEditID = 65535;
      }
      else if( iEditID < 0 )
      {
         iEditID = 0;
      }

      /* Disable save option if currently writing */
      if( ( stEditCmd.eCommand == DRIVE_CMD__WRITE )
       && ( ucCursorX == 15 ) )
      {
         ucCursorX = 14;
      }

      stEditCmd.uwID = iEditID;
      stEditCmd.iValue = iEditValue;

      if( bSave )
      {
         stEditCmd.eCommand = DRIVE_CMD__WRITE;
      }
      else if(!stEditCmd.uwCountdown_ms)
      {
         stEditCmd.eCommand = DRIVE_CMD__READ;
      }

      SetEditDriveParameterCommand(&stEditCmd, !bSave);

      /* Display */
      UpdateEditParameterScreen();
   }

}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
