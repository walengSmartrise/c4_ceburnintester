/******************************************************************************
 *
 * @file     ui_ffs_earthquake.c
 * @brief    Earthquake UI pages
 * @version  V1.00
 * @date     13, Feb 2018
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Swing_CallsEnable( void );
static void UI_FFS_Swing_StayInGroup( void );
static void UI_FFS_Swing_IdleTimer( void );
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_Swing_CallsEnable =
{
   .pfnDraw = UI_FFS_Swing_CallsEnable,
};
static struct st_ui_screen__freeform gstFFS_Swing_StayInGroup =
{
   .pfnDraw = UI_FFS_Swing_StayInGroup,
};
static struct st_ui_screen__freeform gstFFS_Swing_IdleTimer =
{
   .pfnDraw = UI_FFS_Swing_IdleTimer,
};
//----------------------------------------------------------
static struct st_ui_menu_item gstMI_Swing_CallsEnable =
{
   .psTitle = "Calls Enable Swing",
   .pstUGS_Next = &gstUGS_Swing_CallsEnable,
};
static struct st_ui_menu_item gstMI_Swing_StayInGroup =
{
   .psTitle = "Stay In Group",
   .pstUGS_Next = &gstUGS_Swing_StayInGroup,
};
static struct st_ui_menu_item gstMI_Swing_IdleTimer =
{
   .psTitle = "Idle Timer",
   .pstUGS_Next = &gstUGS_Swing_IdleTimer,
};
static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_Swing_CallsEnable,
   &gstMI_Swing_StayInGroup,
   &gstMI_Swing_IdleTimer,
};
static struct st_ui_screen__menu gstMenu_Setup_Swing =
{
   .psTitle = "Swing",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Swing_CallsEnable =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Swing_CallsEnable,
};
struct st_ui_generic_screen gstUGS_Swing_StayInGroup =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Swing_StayInGroup,
};
struct st_ui_generic_screen gstUGS_Swing_IdleTimer =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Swing_IdleTimer,
};
//-------------------------
struct st_ui_generic_screen gstUGS_Menu_Setup_Swing =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Swing,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_Swing_CallsEnable( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__Swing_CallsEnable;
   stParamEdit.uwParamIndex_End = enPARAM1__Swing_CallsEnable;
   stParamEdit.psTitle = "Calls Enable";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_Swing_StayInGroup( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__Swing_StayInGroup;
   stParamEdit.uwParamIndex_End = enPARAM1__Swing_StayInGroup;
   stParamEdit.psTitle = "Stay In Group";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_Swing_IdleTimer( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__Swing_IdleTime_1s;
   stParamEdit.uwParamIndex_End = enPARAM8__Swing_IdleTime_1s;
   stParamEdit.psTitle = "Idle Timer";
   stParamEdit.psUnit = " sec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
