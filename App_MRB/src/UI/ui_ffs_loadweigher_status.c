/******************************************************************************
 *
 * @file     ui_menu_io.c
 * @brief    I/O Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "GlobalData.h"
#include "mod_loadweigher.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_LoadWeigher( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_LoadWeigher =
{
   .pfnDraw = UI_FFS_LoadWeigher,
};

static char const * const pasError[NUM_LWD_ERROR] =
{
      "UNK",
      "NONE",
      "POR",
      "WDT",
      "BOR",
      "COM SYS",
      "COM LOAD",
      "BUS RST",
      "WD DIS",
};
static char const * const pasCalibrate[NUM_LWD_CALIB_STATES] =
{
"NON",//LWD_CALIB_STATE__NONE,
"CPT",//LWD_CALIB_STATE__CAPTURE,
"M2B",//LWD_CALIB_STATE__MOVE_TO_BOTTOM,
"LRN",//LWD_CALIB_STATE__LEARN_FLOOR,
"M2N",//LWD_CALIB_STATE__MOVE_TO_NEXT,
"CMP",//LWD_CALIB_STATE__COMPLETE,
};

static uint8_t ucScrollIndex;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_LoadWeigher =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_LoadWeigher,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MAX_SCROLL_INDEX      (3)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void Print_Screen(void)
{
   uint8_t bOffline = LoadWeigher_GetOfflineStatus();
   int8_t cRawTorque = LoadWeigher_GetRawTorquePercent();
   int8_t cAvgTorque = LoadWeigher_GetAverageTorquePercent();
   uint8_t ucRcvCounter = LoadWeigher_GetReceiveCounter() & 0xFF;
   en_lwd_error eError = LoadWeigher_GetError();
   int16_t wInCarWeight_lb = LoadWeigher_GetInCarWeight_50lb()*50;
   uint8_t bLightLoad = LoadWeigher_GetLoadFlag(LOAD_FLAG__LIGHT_LOAD);
   uint8_t bFullLoad = LoadWeigher_GetLoadFlag(LOAD_FLAG__FULL_LOAD);
   uint8_t bOverLoad = LoadWeigher_GetLoadFlag(LOAD_FLAG__OVER_LOAD);
   en_lwd_calib_state eCalib = ( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__CT )
                             ? ( GetLW_GetCalibrationState_CTB() )
                             : ( GetLW_GetCalibrationState_MRB() );
   if( ( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__CT )
    && ( GetSRU_Deployment() != enSRU_DEPLOYMENT__CT ) )
   {
      uint8_t ucFlags = GetLW_GetLoadFlag_CTB();
      bOffline = GetLW_OfflineStatus_CTB();
      cRawTorque = GetLW_RawTorquePercent_CTB();
      cAvgTorque = GetLW_AverageTorquePercent_CTB();
      ucRcvCounter = GetLW_ReceiveCounter_CTB();
      eError = GetLW_GetError_CTB();
      wInCarWeight_lb = GetLW_GetInCarWeight_CTB()*50;
      bLightLoad = Sys_Bit_Get(&ucFlags, LOAD_FLAG__LIGHT_LOAD);
      bFullLoad = Sys_Bit_Get(&ucFlags, LOAD_FLAG__FULL_LOAD);
      bOverLoad = Sys_Bit_Get(&ucFlags, LOAD_FLAG__OVER_LOAD);
   }
   else if( ( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__MR )
         && ( GetSRU_Deployment() != enSRU_DEPLOYMENT__MR ) )
   {
      uint8_t ucFlags = GetLW_GetLoadFlag_MRB();
      bOffline = GetLW_OfflineStatus_MRB();
      cRawTorque = GetLW_RawTorquePercent_MRB();
      cAvgTorque = GetLW_AverageTorquePercent_MRB();
      ucRcvCounter = GetLW_ReceiveCounter_MRB();
      eError = GetLW_GetError_MRB();
      wInCarWeight_lb = GetLW_GetInCarWeight_MRB()*50;
      bLightLoad = Sys_Bit_Get(&ucFlags, LOAD_FLAG__LIGHT_LOAD);
      bFullLoad = Sys_Bit_Get(&ucFlags, LOAD_FLAG__FULL_LOAD);
      bOverLoad = Sys_Bit_Get(&ucFlags, LOAD_FLAG__OVER_LOAD);
   }
   else if( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__DISCRETE )
   {
      bOffline = 0;
   }

   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Load Weigher - ");
   if(bOffline)
   {
      LCD_Char_WriteStringUpper("OFF");
   }
   else
   {
      LCD_Char_WriteStringUpper("ON");
   }

   for(uint8_t ucLine = 0; ucLine < 3; ucLine++)
   {
      uint8_t ucSelect = ucLine + ucScrollIndex;
      switch(ucSelect)
      {
         case 0:
            LCD_Char_GotoXY( 0, ucLine+1 );
            LCD_Char_WriteString("Torque: ");
            LCD_Char_WriteSignedInteger(cAvgTorque);
            LCD_Char_WriteString("%");
            LCD_Char_GotoXY( 13, ucLine+1 );
            LCD_Char_WriteString(" (");
            LCD_Char_WriteSignedInteger(cRawTorque);
            LCD_Char_WriteString("%)");
            break;

         case 1:
            LCD_Char_GotoXY( 0, ucLine+1 );
            LCD_Char_WriteString("Weight: ");
            LCD_Char_WriteSignedInteger(wInCarWeight_lb);
            break;

         case 2:
            LCD_Char_GotoXY( 0, ucLine+1 );
            LCD_Char_WriteString("Flags: ");
            if(bLightLoad)
            {
               LCD_Char_WriteString("LL");
            }
            else
            {
               LCD_Char_WriteString("..");
            }
            LCD_Char_AdvanceX(1);

            if(bFullLoad)
            {
               LCD_Char_WriteString("FL");
            }
            else
            {
               LCD_Char_WriteString("..");
            }
            LCD_Char_AdvanceX(1);

            if(bOverLoad)
            {
               LCD_Char_WriteString("OL");
            }
            else
            {
               LCD_Char_WriteString("..");
            }
            break;

         case 3:
            LCD_Char_GotoXY( 0, ucLine+1 );
            LCD_Char_WriteString("Err: ");
            LCD_Char_WriteString(pasError[eError]);
            break;

         case 4:
            LCD_Char_GotoXY( 0, ucLine+1 );
            LCD_Char_WriteString("RX: ");
            LCD_Char_WriteInteger_ExactLength(ucRcvCounter, 3);
            break;

         case 5:
            LCD_Char_GotoXY( 0, ucLine+1 );
            LCD_Char_WriteString("CAL: ");
            LCD_Char_WriteString(pasCalibrate[eCalib]);
            break;

         default: break;
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_LoadWeigher( void )
{
   Print_Screen();

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucScrollIndex)
      {
         ucScrollIndex--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN )
   {
      if(ucScrollIndex < MAX_SCROLL_INDEX)
      {
         ucScrollIndex++;
      }
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
