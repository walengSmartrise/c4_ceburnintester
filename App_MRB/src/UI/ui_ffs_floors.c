/******************************************************************************
 *
 * @file     ui_ffs_floors.c
 * @brief    Setup floors UI pages
 * @version  V1.00
 * @date     13, Feb 2018
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Floors_NumberOfFloors( void );
static void UI_FFS_Setup_Floors_TooHighTooLow( void );
static void UI_FFS_Floors_RelevelZoneSize( void );
static void UI_FFS_Floors_EnableRelevel( void );
static void UI_FFS_Floors_RelevelDelay( void );

static void UI_FFS_Setup_Feature_per_Floors( uint32_t, uint32_t, char* );

static void UI_FFS_Setup_Floors_Security_F( void );
static void UI_FFS_Setup_Floors_Security_R( void );
static void UI_FFS_Setup_Floors_Opening_F( void );
static void UI_FFS_Setup_Floors_Opening_R( void );
static void UI_FFS_Setup_Floors_Sabbath_Opening_F( void );
static void UI_FFS_Setup_Floors_Sabbath_Opening_R( void );
static void UI_FFS_Setup_Floors_StoreFloorLevel( void );
static void UI_FFS_Setup_Floors_ShortFloorOpening( void );

static void UI_FFS_Setup_Floors_Weekday_Time( uint32_t ulParamNumber, char* psTitle );

static void UI_FFS_Setup_Floors_TimedCCSecurity_Start_MF( void );
static void UI_FFS_Setup_Floors_TimedCCSecurity_Stop_MF( void );
static void UI_FFS_Setup_Floors_TimedCCSecurity_Start_SS( void );
static void UI_FFS_Setup_Floors_TimedCCSecurity_Stop_SS( void );
static void UI_FFS_Setup_Floors_TimedCCSecurity_EnablePerFloor_F( void );
static void UI_FFS_Setup_Floors_TimedCCSecurity_EnablePerFloor_R( void );
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_Floors_NumberOfFloors =
{
   .pfnDraw = UI_FFS_Floors_NumberOfFloors,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_TooHighTooLow =
{
   .pfnDraw = UI_FFS_Setup_Floors_TooHighTooLow,
};
static struct st_ui_screen__freeform gstFFS_Floors_RelevelZoneSize =
{
   .pfnDraw = UI_FFS_Floors_RelevelZoneSize,
};
static struct st_ui_screen__freeform gstFFS_Floors_EnableRelevel =
{
   .pfnDraw = UI_FFS_Floors_EnableRelevel,
};
static struct st_ui_screen__freeform gstFFS_Floors_RelevelDelay =
{
   .pfnDraw = UI_FFS_Floors_RelevelDelay,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Security_F =
{
   .pfnDraw = UI_FFS_Setup_Floors_Security_F,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Security_R =
{
   .pfnDraw = UI_FFS_Setup_Floors_Security_R,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Opening_F =
{
   .pfnDraw = UI_FFS_Setup_Floors_Opening_F,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Opening_R =
{
   .pfnDraw = UI_FFS_Setup_Floors_Opening_R,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Sabbath_Opening_F =
{
   .pfnDraw = UI_FFS_Setup_Floors_Sabbath_Opening_F,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Sabbath_Opening_R =
{
   .pfnDraw = UI_FFS_Setup_Floors_Sabbath_Opening_R,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_StoreFloorLevel =
{
   .pfnDraw = UI_FFS_Setup_Floors_StoreFloorLevel,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_ShortFloorOpening =
{
   .pfnDraw = UI_FFS_Setup_Floors_ShortFloorOpening,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_TimedCCSecurity_Start_MF =
{
   .pfnDraw = UI_FFS_Setup_Floors_TimedCCSecurity_Start_MF,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_TimedCCSecurity_Stop_MF =
{
   .pfnDraw = UI_FFS_Setup_Floors_TimedCCSecurity_Stop_MF,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_TimedCCSecurity_Start_SS =
{
   .pfnDraw = UI_FFS_Setup_Floors_TimedCCSecurity_Start_SS,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_TimedCCSecurity_Stop_SS =
{
   .pfnDraw = UI_FFS_Setup_Floors_TimedCCSecurity_Stop_SS,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_TimedCCSecurity_EnablePerFloor_F =
{
   .pfnDraw = UI_FFS_Setup_Floors_TimedCCSecurity_EnablePerFloor_F,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_TimedCCSecurity_EnablePerFloor_R =
{
   .pfnDraw = UI_FFS_Setup_Floors_TimedCCSecurity_EnablePerFloor_R,
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Floors_TooHigh =
{
   .psTitle = "Too High/ Too Low",
   .pstUGS_Next = &gstUGS_Setup_Floors_TooHighTooLow,
};
static struct st_ui_menu_item gstMI_Floors_NumFloors =
{
   .psTitle = "Number Of Floors",
   .pstUGS_Next = &gstUGS_Floor_NumberOfFloors,
};
static struct st_ui_menu_item gstMI_Floors_RelevelZone =
{
   .psTitle = "Relevel Zone Size",
   .pstUGS_Next = &gstUGS_Floors_RelevelZoneSize,
};
static struct st_ui_menu_item gstMI_Floors_Releveling =
{
   .psTitle = "Enable Releveling",
   .pstUGS_Next = &gstUGS_Floors_EnableRelevel,
};
static struct st_ui_menu_item gstMI_Floors_RelevelingDelay =
{
   .psTitle = "Releveling Delay",
   .pstUGS_Next = &gstUGS_Floors_RelevelDelay,
};
static struct st_ui_menu_item gstMI_Floors_Security_F =
{
   .psTitle = "Security (F)",
   .pstUGS_Next = &gstUGS_Setup_Floors_Security_F,
};
static struct st_ui_menu_item gstMI_Floors_Security_R =
{
   .psTitle = "Security (R)",
   .pstUGS_Next = &gstUGS_Setup_Floors_Security_R,
};
static struct st_ui_menu_item gstMI_Floors_Opening_F =
{
   .psTitle = "Openings (F)",
   .pstUGS_Next = &gstUGS_Setup_Floors_Opening_F,
};
static struct st_ui_menu_item gstMI_Floors_Opening_R =
{
   .psTitle = "Openings (R)",
   .pstUGS_Next = &gstUGS_Setup_Floors_Opening_R,
};
static struct st_ui_menu_item gstMI_Floors_Sabbath_Opening_F =
{
   .psTitle = "Sabbath (F)",
   .pstUGS_Next = &gstUGS_Setup_Floors_Sabbath_Opening_F,
};
static struct st_ui_menu_item gstMI_Floors_Sabbath_Opening_R =
{
   .psTitle = "Sabbath (R)",
   .pstUGS_Next = &gstUGS_Setup_Floors_Sabbath_Opening_R,
};
static struct st_ui_menu_item gstMI_Floors_StoreFloorLevel =
{
   .psTitle = "Store Floor Level",
   .pstUGS_Next = &gstUGS_Setup_Floors_StoreFloorLevel,
};
static struct st_ui_menu_item gstMI_Floors_ShortFloorOpening =
{
   .psTitle = "Short Floor Openings",
   .pstUGS_Next = &gstUGS_Setup_Floors_ShortFloorOpening,
};
static struct st_ui_menu_item gstMI_Floors_TimedCCSecurity =
{
   .psTitle = "Timed CC Security",
   .pstUGS_Next = &gstUGS_Setup_Floors_TimedCCSecurity,
};
static struct st_ui_menu_item * gastMenuItems_Floors[] =
{
      &gstMI_Floors_NumFloors,
      &gstMI_Floors_TooHigh,
      &gstMI_Floors_Releveling,
      &gstMI_Floors_RelevelZone,
      &gstMI_Floors_RelevelingDelay,
      &gstMI_Floors_Opening_F,
      &gstMI_Floors_Opening_R,
      &gstMI_Floors_Security_F,
      &gstMI_Floors_Security_R,
      &gstMI_Floors_Sabbath_Opening_F,
      &gstMI_Floors_Sabbath_Opening_R,
      &gstMI_Floors_StoreFloorLevel,
      &gstMI_Floors_ShortFloorOpening,
      &gstMI_Floors_TimedCCSecurity,
};
static struct st_ui_screen__menu gstMenu_Setup_Floors =
{
   .psTitle = "Floors",
   .pastMenuItems = &gastMenuItems_Floors,
   .ucNumItems = sizeof(gastMenuItems_Floors) / sizeof(gastMenuItems_Floors[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Floors_TimedCCSecurity_Start_MF =
{
   .psTitle = "Start (M-F)",
   .pstUGS_Next = &gstUGS_Setup_Floors_TimedCCSecurity_Start_MF,
};
static struct st_ui_menu_item gstMI_Floors_TimedCCSecurity_Stop_MF =
{
   .psTitle = "Stop (M-F)",
   .pstUGS_Next = &gstUGS_Setup_Floors_TimedCCSecurity_Stop_MF,
};
static struct st_ui_menu_item gstMI_Floors_TimedCCSecurity_Start_SS =
{
   .psTitle = "Start (S-S)",
   .pstUGS_Next = &gstUGS_Setup_Floors_TimedCCSecurity_Start_SS,
};
static struct st_ui_menu_item gstMI_Floors_TimedCCSecurity_Stop_SS =
{
   .psTitle = "Stop (S-S)",
   .pstUGS_Next = &gstUGS_Setup_Floors_TimedCCSecurity_Stop_SS,
};
static struct st_ui_menu_item gstMI_Floors_TimedCCSecurity_EnablePerFloor_F =
{
   .psTitle = "Enable Floor (F)",
   .pstUGS_Next = &gstUGS_Setup_Floors_TimedCCSecurity_EnablePerFloor_F,
};
static struct st_ui_menu_item gstMI_Floors_TimedCCSecurity_EnablePerFloor_R =
{
   .psTitle = "Enable Floor (R)",
   .pstUGS_Next = &gstUGS_Setup_Floors_TimedCCSecurity_EnablePerFloor_R,
};
static struct st_ui_menu_item * gastMenuItems_TimedCCSecurity[] =
{
   &gstMI_Floors_TimedCCSecurity_EnablePerFloor_F,
   &gstMI_Floors_TimedCCSecurity_EnablePerFloor_R,
   &gstMI_Floors_TimedCCSecurity_Start_MF,
   &gstMI_Floors_TimedCCSecurity_Stop_MF,
   &gstMI_Floors_TimedCCSecurity_Start_SS,
   &gstMI_Floors_TimedCCSecurity_Stop_SS,
};
static struct st_ui_screen__menu gstMenu_Setup_TimedCCSecurity =
{
   .psTitle = "Timed CC Security",
   .pastMenuItems = &gastMenuItems_TimedCCSecurity,
   .ucNumItems = sizeof(gastMenuItems_TimedCCSecurity) / sizeof(gastMenuItems_TimedCCSecurity[ 0 ]),
};

static int8_t cCursorX, cIndex, ucSaveCountdown_30ms;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Floor_NumberOfFloors =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Floors_NumberOfFloors,
};
struct st_ui_generic_screen gstUGS_Setup_Floors_TooHighTooLow =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_TooHighTooLow,
};
struct st_ui_generic_screen gstUGS_Floors_RelevelZoneSize =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Floors_RelevelZoneSize,
};
struct st_ui_generic_screen gstUGS_Floors_EnableRelevel =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Floors_EnableRelevel,
};
struct st_ui_generic_screen gstUGS_Floors_RelevelDelay =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Floors_RelevelDelay,
};
struct st_ui_generic_screen gstUGS_Setup_Floors_Security_F =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Security_F,
};
struct st_ui_generic_screen gstUGS_Setup_Floors_Security_R =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Security_R,
};
struct st_ui_generic_screen gstUGS_Setup_Floors_Opening_F =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Opening_F,
};
struct st_ui_generic_screen gstUGS_Setup_Floors_Opening_R =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Opening_R,
};
struct st_ui_generic_screen gstUGS_Setup_Floors_Sabbath_Opening_F =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Sabbath_Opening_F,
};
struct st_ui_generic_screen gstUGS_Setup_Floors_Sabbath_Opening_R =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Sabbath_Opening_R,
};
struct st_ui_generic_screen gstUGS_Setup_Floors_StoreFloorLevel =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_StoreFloorLevel,
};
struct st_ui_generic_screen gstUGS_Setup_Floors_ShortFloorOpening =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_ShortFloorOpening,
};
struct st_ui_generic_screen gstUGS_Setup_Floors_TimedCCSecurity_Start_MF =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_TimedCCSecurity_Start_MF,
};
struct st_ui_generic_screen gstUGS_Setup_Floors_TimedCCSecurity_Stop_MF =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_TimedCCSecurity_Stop_MF,
};
struct st_ui_generic_screen gstUGS_Setup_Floors_TimedCCSecurity_Start_SS =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_TimedCCSecurity_Start_SS,
};
struct st_ui_generic_screen gstUGS_Setup_Floors_TimedCCSecurity_Stop_SS =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_TimedCCSecurity_Stop_SS,
};
struct st_ui_generic_screen gstUGS_Setup_Floors_TimedCCSecurity_EnablePerFloor_F =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_TimedCCSecurity_EnablePerFloor_F,
};
struct st_ui_generic_screen gstUGS_Setup_Floors_TimedCCSecurity_EnablePerFloor_R=
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_TimedCCSecurity_EnablePerFloor_R,
};
//---------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_Setup_Floors =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Floors,
};
struct st_ui_generic_screen gstUGS_Setup_Floors_TimedCCSecurity =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_TimedCCSecurity,
};
//---------------------------------------------------------------
static uint8_t ucCurrentFloorIndex;

uint8_t gbResetSaveState;

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define START_CURSOR_POS_VALUE (7U)
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Floors_NumberOfFloors( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__NumFloors;
   stParamEdit.uwParamIndex_End = enPARAM8__NumFloors;
   stParamEdit.ulValue_Max = ( Param_ReadValue_1Bit(enPARAM1__EnableExtFloorLimit) ) ? MAX_NUM_FLOORS : MAX_NUM_FLOORS_REDUCED;
   stParamEdit.psTitle = "Number of Floors";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_TooHighTooLow( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT24;
   stParamEdit.uwParamIndex_Start = enPARAM24__LearnedFloor_0;
   stParamEdit.uwParamIndex_End = enPARAM24__LearnedFloor_0 + GetFP_NumFloors()-1;
   stParamEdit.psTitle = "Adjust Floors";

   if( ( UI_GetNewPageFlag() )
    && ( GetOperation_CurrentFloor() < GetFP_NumFloors() ) )
   {
      stParamEdit.uwParamIndex_Current = enPARAM24__LearnedFloor_0 + GetOperation_CurrentFloor();
      stParamEdit.bInvalid = 0;
     stParamEdit.bSaving = 0;
     stParamEdit.ucCursorColumn = 0;
     stParamEdit.ucFieldIndex = enFIELD__NONE;
     gbResetSaveState = 1;
   }

   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Floors_RelevelZoneSize( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__RelevelingDistance_05mm;
   stParamEdit.uwParamIndex_End = enPARAM8__RelevelingDistance_05mm;
   stParamEdit.psTitle = "Relevel Zone Size";
   stParamEdit.psUnit = " ft";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Floors_EnableRelevel( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__RelevelEnabled;
   stParamEdit.uwParamIndex_End = enPARAM1__RelevelEnabled;
   stParamEdit.psTitle = "Enable Releveling";
   stParamEdit.psUnit = " ";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Floors_RelevelDelay( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__RelevelingDelay_50ms;
   stParamEdit.uwParamIndex_End = enPARAM8__RelevelingDelay_50ms;
   stParamEdit.psTitle = "Releveling Delay";
   stParamEdit.psUnit = " x50 msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void Screen_Bitmap(struct st_param_edit_menu * pstParamEditMenu)
{
   LCD_Char_Clear();

   LCD_Char_GotoXY( 0, 0 );

   //------------------------------------------------------------------
   if(pstParamEditMenu->bSaving)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Saving");
   }
   else if(pstParamEditMenu->bInvalid)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Invalid");
   }
   else
   {
     LCD_Char_GotoXY( 0, 0 );
     LCD_Char_WriteStringUpper( pstParamEditMenu->psTitle);
     LCD_Char_GotoXY( 16, 0 );
     char *psPI = Get_PI_Label(ucCurrentFloorIndex);
     LCD_Char_WriteStringUpper( "[" );
     if(Param_ReadValue_1Bit(enPARAM1__Enable3DigitPI))
     {
        LCD_Char_WriteStringUpper( psPI );
     }
     else
     {
        LCD_Char_WriteStringUpper( psPI+1 );
     }
     LCD_Char_WriteStringUpper( "]" );
      //------------------------------------------------------------------
      LCD_Char_GotoXY(1,2);
      LCD_Char_WriteInteger_ExactLength( ucCurrentFloorIndex + 1, 2 );
      LCD_Char_WriteStringUpper(" = ");

      LCD_Char_GotoXY(START_CURSOR_POS_VALUE, 2);

      uint8_t ucBitMapIndex = ucCurrentFloorIndex%32;// Size of each 32 bit parameter
      uint8_t bEditedValue = Sys_Bit_Get(&pstParamEditMenu->ulValue_Edited, ucBitMapIndex);

      if(bEditedValue)
      {
         LCD_Char_WriteString("On");
      }
      else
      {
         LCD_Char_WriteString("Off");
      }

      LCD_Char_GotoXY( pstParamEditMenu->ucCursorColumn, 3 );

      LCD_Char_WriteChar( '*' );

      //------------------------------------------------------------------
      if ( pstParamEditMenu->ulValue_Edited != pstParamEditMenu->ulValue_Saved )
      {
         LCD_Char_GotoXY( 16, 1 );
         LCD_Char_WriteString( "Save" );

         LCD_Char_GotoXY( 19, 2 );
         LCD_Char_WriteChar( '|' );
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ValidateParameter(struct st_param_edit_menu * pstParamEditMenu)
{
   switch(pstParamEditMenu->enParamType)
   {

      case enPARAM_BLOCK_TYPE__BIT:
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_1BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_1Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT8:
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_8BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_8Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT16:
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_16BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_16Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT24:
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_24BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_24Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT32:
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_32BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_32Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      default:
         pstParamEditMenu->bInvalid = 1;
         break;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Floor( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_param_edit_menu * pstParamEditMenu )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex = 0;
   int32_t lParamIndex_Edited;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = pstParamEditMenu->ucFieldSize_Index-1;
      }
   }
   else  // enACTION__EDIT
   {

      lParamIndex_Edited = ucCurrentFloorIndex;

      pstParamEditMenu->ucCursorColumn = 1+ucCursorIndex;
      //------------------------------------------------------------------
      int32_t uiDigitWeight = (pstParamEditMenu->ucFieldSize_Index - 1) - ucCursorIndex;

      uiDigitWeight = pow( 10, uiDigitWeight );  // 10 to the power of uiDigitWeight
      //------------------------------------------------------------------
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            lParamIndex_Edited += uiDigitWeight;
            break;
         case enKEYPRESS_DOWN:
            lParamIndex_Edited -= uiDigitWeight;
            break;
         case enKEYPRESS_LEFT:
            if ( ucCursorIndex )
            {
               --ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_LEFT;
            }
            break;
         case enKEYPRESS_RIGHT:
            if ( ucCursorIndex < pstParamEditMenu->ucFieldSize_Index - 1 )
            {
               ++ucCursorIndex;
            }
            else
            {
               pstParamEditMenu->ulValue_Edited = pstParamEditMenu->ulValue_Saved;
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;
         default:
            break;
      }
      //------------------------------------------------------------------
      if(lParamIndex_Edited >= GetFP_NumFloors()-1)
      {
         lParamIndex_Edited = GetFP_NumFloors()-1;
      }
      else if(lParamIndex_Edited <= 0)
      {
         lParamIndex_Edited = 0;
      }

      ucCurrentFloorIndex = (uint8_t) lParamIndex_Edited;

      pstParamEditMenu->ulValue_Edited = pstParamEditMenu->ulValue_Saved;

   }

   return enReturnValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Value_ByBit( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_param_edit_menu * pstParamEditMenu )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex;
   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = pstParamEditMenu->ucFieldSize_Value - 1;
      }
   }
   else  // enACTION__EDIT
   {
      uint8_t ucBitMapIndex = ucCurrentFloorIndex%32;// Size of each 32 bit parameter
      //------------------------------------------------------------------
      uint8_t bEditedValue = Sys_Bit_Get(&pstParamEditMenu->ulValue_Edited, ucBitMapIndex);
      //------------------------------------------------------------------
      pstParamEditMenu->ucCursorColumn = START_CURSOR_POS_VALUE + ucCursorIndex;
      //------------------------------------------------------------------
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            bEditedValue = 1;
            break;

         case enKEYPRESS_DOWN:
            bEditedValue = 0;
            break;

         case enKEYPRESS_LEFT:
            if ( ucCursorIndex )
            {
               --ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_LEFT;
            }
            break;

         case enKEYPRESS_RIGHT:
            if ( ucCursorIndex < (pstParamEditMenu->ucFieldSize_Value - 1) )
            {
               ++ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;

         default:
            break;
      }
      Sys_Bit_Set(&pstParamEditMenu->ulValue_Edited, ucBitMapIndex, bEditedValue);
   }

   return enReturnValue;
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void SaveParameter( struct st_param_edit_menu * pstParamEditMenu )
{
   uint16_t ucIndex = pstParamEditMenu->uwParamIndex_Current;
   switch(pstParamEditMenu->enParamType)
   {
      case enPARAM_BLOCK_TYPE__UINT32:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_32Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_32Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT24:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_24Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_24Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT16:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_16Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_16Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT8:

         pstParamEditMenu->ulValue_Saved = Param_ReadValue_8Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
          Param_WriteValue_8Bit( ucIndex,pstParamEditMenu->ulValue_Edited);
         }
         break;
      default:
      case enPARAM_BLOCK_TYPE__BIT:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_1Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_1Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Save( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_param_edit_menu * pstParamEditMenu )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static enum en_save_state enSaveState = SAVE_STATE__SAVED;

   pstParamEditMenu->ucCursorColumn = 19;
   //------------------------------------------
   if ( enFieldAction == enACTION__EDIT )
   {
      if ( enKeypress == enKEYPRESS_ENTER )
      {
         enSaveState = SAVE_STATE__SAVING;
      }
      else if ( enKeypress == enKEYPRESS_LEFT )
      {
         enSaveState = SAVE_STATE__SAVED;
         enReturnValue = enACTION__EXIT_TO_LEFT;
      }
   }

   //--------------------------------
   switch(enSaveState)
   {
      case SAVE_STATE__CLEARING:
         pstParamEditMenu->bSaving = 1;
         if(!pstParamEditMenu->ulValue_Saved)
         {
            enSaveState = SAVE_STATE__SAVING;
         }
         break;
      case SAVE_STATE__SAVING:
         SaveParameter(pstParamEditMenu);
         pstParamEditMenu->bSaving = 1;
         if(pstParamEditMenu->ulValue_Saved == pstParamEditMenu->ulValue_Edited)
         {
            enReturnValue = enACTION__EXIT_TO_LEFT;
            pstParamEditMenu->bSaving = 0;
            enSaveState = SAVE_STATE__SAVED;
         }
         break;
      case SAVE_STATE__SAVED:
         pstParamEditMenu->bSaving = 0;
         break;
      default:
         enSaveState = SAVE_STATE__SAVED;
         break;
   }
   return enReturnValue;
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void EditParameterRange_ByBit(struct st_param_edit_menu * pstParamEditMenu)
{
   /* Modification for editing by bit */
   pstParamEditMenu->uwParamIndex_Current = pstParamEditMenu->uwParamIndex_Start + ( ucCurrentFloorIndex / 32 );

   ValidateParameter(pstParamEditMenu);
   if ( pstParamEditMenu->ucFieldIndex == enFIELD__NONE )
   {
      pstParamEditMenu->ulValue_Edited = pstParamEditMenu->ulValue_Saved;
      pstParamEditMenu->bSaving = 0;
      pstParamEditMenu->bInvalid = 0;
      pstParamEditMenu->ucFieldIndex = enFIELD__FLOOR;

      Edit_Floor( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );
   }
   else
   {
      enum en_field_action enFieldAction;

      enum en_keypresses enKeypress = Button_GetKeypress();
      //------------------------------------------------------------------
      switch ( pstParamEditMenu->ucFieldIndex )
      {
         case enFIELD__FLOOR:
            enFieldAction = Edit_Floor( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               PrevScreen();
               pstParamEditMenu->ucFieldIndex = enFIELD__NONE;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Value_ByBit( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__VALUE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__VALUE: //function

            enFieldAction = Edit_Value_ByBit( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Floor( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT, pstParamEditMenu);

               pstParamEditMenu->ucFieldIndex = enFIELD__FLOOR;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Save( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__SAVE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__SAVE:

            enFieldAction = Edit_Save( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Value_ByBit( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__VALUE;
            }
            break;

         default:
            break;
      }
      Screen_Bitmap(pstParamEditMenu);
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Feature_per_Floors( uint32_t ulParamIdxStart, 
                                             uint32_t ulParamIdxEnd, 
                                             char* psTitle )
{
   static struct st_param_edit_menu stParamEdit;

   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT32;
   stParamEdit.uwParamIndex_Start = ulParamIdxStart;
   stParamEdit.uwParamIndex_End   = ulParamIdxEnd;
   stParamEdit.psTitle = psTitle;
   stParamEdit.ucFieldSize_Value = 1;
   stParamEdit.ucFieldSize_Index = 2;

   EditParameterRange_ByBit( &stParamEdit );
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_Security_F( void )
{
   UI_FFS_Setup_Feature_per_Floors( enPARAM32__SecureKeyedBitmapF_0,
                                    enPARAM32__SecureKeyedBitmapF_2,
                                    "Security Floors (F)" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_Security_R( void )
{
   UI_FFS_Setup_Feature_per_Floors( enPARAM32__SecureKeyedBitmapR_0,
                                    enPARAM32__SecureKeyedBitmapR_2,
                                    "Security Floors (R)" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_Opening_F( void )
{
   UI_FFS_Setup_Feature_per_Floors( enPARAM32__OpeningBitmapF_0,
                                    enPARAM32__OpeningBitmapF_2,
                                    "Floor Openings (F)" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_Opening_R( void )
{
   UI_FFS_Setup_Feature_per_Floors( enPARAM32__OpeningBitmapR_0,
                                    enPARAM32__OpeningBitmapR_2,
                                    "Floor Openings (R)" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_Sabbath_Opening_F( void )
{
   UI_FFS_Setup_Feature_per_Floors( enPARAM32__SabbathOpeningF_0,
                                    enPARAM32__SabbathOpeningF_1,
                                    "Floor Openings (F)" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_Sabbath_Opening_R( void )
{
   UI_FFS_Setup_Feature_per_Floors( enPARAM32__SabbathOpeningR_0,
                                    enPARAM32__SabbathOpeningR_1,
                                    "Floor Openings (R)" );
}

/*-----------------------------------------------------------------------------
|01234567890123456789|

|FLOOR 12        SAVE|
|      *             | // Cursor lane
|CURR:  123' 11.000" | // Current position
|SAVE:  123' 11.000" | // Stored position
 -----------------------------------------------------------------------------*/
#define MAX_STORE_FLOOR_LEVEL_CURSOR_POS_X            (2)
#define STORE_FLOOR_LEVEL_CURSOR_LCD_POS_X_SAVE       (17)
#define STORE_FLOOR_LEVEL_CURSOR_LCD_POS_X_FLOOR      (6)
#define STORE_FLOOR_LEVEL_SAVE_COUNTDOWN_30MS         (40)
static void UI_FFS_Setup_Floors_StoreFloorLevel( void )
{
   /* Control */
   enum en_keypresses enKeypress = Button_GetKeypress();
   if( enKeypress == enKEYPRESS_LEFT )
   {
      if(!cCursorX)
      {
         PrevScreen();
         ucSaveCountdown_30ms = 0;
      }
      else
      {
         cCursorX--;
      }
   }
   else if( enKeypress == enKEYPRESS_RIGHT )
   {
      cCursorX++;
   }
   else if( enKeypress == enKEYPRESS_UP )
   {
      if( cCursorX == 1 )
      {
         cIndex += 1;
      }
      else if( cCursorX == 0 )
      {
         cIndex += 10;
      }
   }
   else if( enKeypress == enKEYPRESS_DOWN )
   {
      if( cCursorX == 1 )
      {
         cIndex -= 1;
      }
      else if( cCursorX == 0 )
      {
         cIndex -= 10;
      }
   }

   if(cCursorX < 0)
   {
      cCursorX = 0;
   }
   else if(cCursorX > MAX_STORE_FLOOR_LEVEL_CURSOR_POS_X)
   {
      cCursorX = MAX_STORE_FLOOR_LEVEL_CURSOR_POS_X;
   }
   if(cIndex < 0)
   {
      cIndex = 0;
   }
   else if(cIndex >= GetFP_NumFloors())
   {
      cIndex = GetFP_NumFloors()-1;
   }

   /* The GetPosition_PositionCount() will fluctuate. Limit the frequency of
    * parameter write requests to prevent spamming of the parameter master. */
   if( ucSaveCountdown_30ms )
   {
      ucSaveCountdown_30ms--;
   }
   else if( enKeypress == enKEYPRESS_ENTER )
   {
      if(cCursorX == MAX_STORE_FLOOR_LEVEL_CURSOR_POS_X)
      {
         Param_WriteValue_24Bit(enPARAM24__LearnedFloor_0+cIndex, GetPosition_PositionCount());
         ucSaveCountdown_30ms = STORE_FLOOR_LEVEL_SAVE_COUNTDOWN_30MS;
      }
   }


   /* Display */
   LCD_Char_Clear();
   /* Print Row 1 */
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteString("FLOOR ");
   LCD_Char_WriteInteger_ExactLength(cIndex+1, 2);
   LCD_Char_GotoXY( 9, 0 );
   LCD_Char_WriteChar('[');
   LCD_Char_WriteString(Get_PI_Label(cIndex));
   LCD_Char_WriteChar(']');

   LCD_Char_GotoXY( 16, 0 );
   LCD_Char_WriteString("SAVE");

   /* Print Row 2 */
   if(cCursorX == MAX_STORE_FLOOR_LEVEL_CURSOR_POS_X)
   {
      LCD_Char_GotoXY( STORE_FLOOR_LEVEL_CURSOR_LCD_POS_X_SAVE, 1 );
   }
   else
   {
      LCD_Char_GotoXY( STORE_FLOOR_LEVEL_CURSOR_LCD_POS_X_FLOOR+cCursorX, 1 );
   }
   LCD_Char_WriteString("*");

   /* Print Row 3 */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("CURR:");
   LCD_WritePositionInFeet( GetPosition_PositionCount() );

   /* Print Row 3 */
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("SAVE:");
   LCD_WritePositionInFeet( Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0+cIndex) );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_ShortFloorOpening( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__ShortFloorOpening_0;
   stParamEdit.uwParamIndex_End = enPARAM16__ShortFloorOpening_5;
   stParamEdit.psTitle = "Short Floor Openings";
   stParamEdit.ucFieldSize_Value = 1;
   stParamEdit.ucFieldSize_Index = 2;
   EditParameterRange_ByBit(&stParamEdit);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_Weekday_Time( uint32_t ulParamNumber, char* psTitle )
{
   static struct st_param_edit_menu stParamEdit;

   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = ulParamNumber;
   stParamEdit.uwParamIndex_End   = stParamEdit.uwParamIndex_Start;
   stParamEdit.psTitle = psTitle;
   stParamEdit.ulValue_Max = 2359;

   UI_ParamEditSceenTemplate( &stParamEdit );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_TimedCCSecurity_Start_MF( void )
{
   UI_FFS_Setup_Floors_Weekday_Time( enPARAM16__Weekday_StartTime, "Weekday Start Time" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_TimedCCSecurity_Stop_MF( void )
{
   UI_FFS_Setup_Floors_Weekday_Time( enPARAM16__Weekday_EndTime, "Weekday Stop Time" );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_TimedCCSecurity_Start_SS( void )
{
   UI_FFS_Setup_Floors_Weekday_Time( enPARAM16__Weekend_StartTime, "Weekend Start Time" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_TimedCCSecurity_Stop_SS( void )
{
   UI_FFS_Setup_Floors_Weekday_Time( enPARAM16__Weekend_EndTime, "Weekend Stop Time" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_TimedCCSecurity_EnablePerFloor_F( void )
{
   UI_FFS_Setup_Feature_per_Floors( enPARAM32__SecureTimedBitmapF_0,
                                    enPARAM32__SecureTimedBitmapF_2,
                                    "Security Floors (F)" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_TimedCCSecurity_EnablePerFloor_R( void )
{
   UI_FFS_Setup_Feature_per_Floors( enPARAM32__SecureTimedBitmapR_0,
                                    enPARAM32__SecureTimedBitmapR_2,
                                    "Security Floors (R)" );
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
