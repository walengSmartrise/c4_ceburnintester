/******************************************************************************
 *
 * @file     ui_ffs_iosetup.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Invert_Inputs_MR( void );
static void UI_FFS_Invert_Inputs_CT( void );
static void UI_FFS_Invert_Inputs_COP( void );
static void UI_FFS_Invert_Inputs_RIS( void );
static void UI_FFS_Invert_Inputs_EXP1( void );
static void UI_FFS_Invert_Inputs_EXP2( void );
static void UI_FFS_Invert_Inputs_EXP3( void );
static void UI_FFS_Invert_Inputs_EXP4( void );
static void UI_FFS_Invert_Inputs_EXP5( void );

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Inputs_MR =
{
   .psTitle = "Machine Room",
   .pstUGS_Next = &gstUGS_Setup_InvertInputs_MR,
};
static struct st_ui_menu_item gstMI_Inputs_CT =
{
   .psTitle = "Car Top",
   .pstUGS_Next = &gstUGS_Setup_InvertInputs_CT,
};
static struct st_ui_menu_item gstMI_Inputs_COP =
{
   .psTitle = "Car Operating Panel",
   .pstUGS_Next = &gstUGS_Setup_InvertInputs_COP,
};
static struct st_ui_menu_item gstMI_Inputs_RIS =
{
   .psTitle = "Riser",
   .pstUGS_Next = &gstUGS_Setup_InvertInputs_RIS,
};
static struct st_ui_menu_item gstMI_Inputs_EXP1 =
{
   .psTitle = "Expansion 1-8",
   .pstUGS_Next = &gstUGS_Setup_InvertInputs_EXP1,
};
static struct st_ui_menu_item gstMI_Inputs_EXP2 =
{
   .psTitle = "Expansion 9-16",
   .pstUGS_Next = &gstUGS_Setup_InvertInputs_EXP2,
};
static struct st_ui_menu_item gstMI_Inputs_EXP3 =
{
   .psTitle = "Expansion 17-24",
   .pstUGS_Next = &gstUGS_Setup_InvertInputs_EXP3,
};
static struct st_ui_menu_item gstMI_Inputs_EXP4 =
{
   .psTitle = "Expansion 25-32",
   .pstUGS_Next = &gstUGS_Setup_InvertInputs_EXP4,
};
static struct st_ui_menu_item gstMI_Inputs_EXP5 =
{
   .psTitle = "Expansion 33-40",
   .pstUGS_Next = &gstUGS_Setup_InvertInputs_EXP5,
};
static struct st_ui_menu_item * gastMenuItems_Inputs_Boards[] =
{
   &gstMI_Inputs_MR,
   &gstMI_Inputs_CT,
   &gstMI_Inputs_COP,
   &gstMI_Inputs_RIS,
   &gstMI_Inputs_EXP1,
   &gstMI_Inputs_EXP2,
   &gstMI_Inputs_EXP3,
   &gstMI_Inputs_EXP4,
   &gstMI_Inputs_EXP5,
};
static struct st_ui_screen__menu gstMenu_Inputs_Boards =
{
   .psTitle = "Select Board",
   .pastMenuItems = &gastMenuItems_Inputs_Boards,
   .ucNumItems = sizeof(gastMenuItems_Inputs_Boards) / sizeof(gastMenuItems_Inputs_Boards[ 0 ]),
};

//---------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Invert_Inputs_MR =
{
   .pfnDraw = UI_FFS_Invert_Inputs_MR,
};
static struct st_ui_screen__freeform gstFFS_Invert_Inputs_CT =
{
   .pfnDraw = UI_FFS_Invert_Inputs_CT,
};
static struct st_ui_screen__freeform gstFFS_Invert_Inputs_COP =
{
   .pfnDraw = UI_FFS_Invert_Inputs_COP,
};
static struct st_ui_screen__freeform gstFFS_Invert_Inputs_RIS =
{
   .pfnDraw = UI_FFS_Invert_Inputs_RIS,
};
static struct st_ui_screen__freeform gstFFS_Invert_Inputs_EXP1 =
{
   .pfnDraw = UI_FFS_Invert_Inputs_EXP1,
};
static struct st_ui_screen__freeform gstFFS_Invert_Inputs_EXP2 =
{
   .pfnDraw = UI_FFS_Invert_Inputs_EXP2,
};
static struct st_ui_screen__freeform gstFFS_Invert_Inputs_EXP3 =
{
   .pfnDraw = UI_FFS_Invert_Inputs_EXP3,
};
static struct st_ui_screen__freeform gstFFS_Invert_Inputs_EXP4 =
{
   .pfnDraw = UI_FFS_Invert_Inputs_EXP4,
};
static struct st_ui_screen__freeform gstFFS_Invert_Inputs_EXP5 =
{
   .pfnDraw = UI_FFS_Invert_Inputs_EXP5,
};
//---------------------------------------------------------------
static enum en_parm_edit_fields eFieldIndex; // Field selected for edit on the ui

static uint16_t uwParamIndex_Current;
static uint16_t uwParamIndex_Start;
static uint16_t uwParamIndex_End;

static uint16_t uwParamValue_Edited;
static uint16_t uwParamValue_Saved;

static uint8_t bSavingInProgress;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Setup_InvertInputs_Boards =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Inputs_Boards,
};

//---------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Setup_InvertInputs_MR =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Invert_Inputs_MR,
};
struct st_ui_generic_screen gstUGS_Setup_InvertInputs_CT =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Invert_Inputs_CT,
};
struct st_ui_generic_screen gstUGS_Setup_InvertInputs_COP =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Invert_Inputs_COP,
};
struct st_ui_generic_screen gstUGS_Setup_InvertInputs_RIS =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Invert_Inputs_RIS,
};
struct st_ui_generic_screen gstUGS_Setup_InvertInputs_EXP1 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Invert_Inputs_EXP1,
};
struct st_ui_generic_screen gstUGS_Setup_InvertInputs_EXP2 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Invert_Inputs_EXP2,
};
struct st_ui_generic_screen gstUGS_Setup_InvertInputs_EXP3 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Invert_Inputs_EXP3,
};
struct st_ui_generic_screen gstUGS_Setup_InvertInputs_EXP4 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Invert_Inputs_EXP4,
};
struct st_ui_generic_screen gstUGS_Setup_InvertInputs_EXP5 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Invert_Inputs_EXP5,
};
//---------------------------------------------------------------


 /*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void PrintBoardAndTerminal( uint16_t uwTerminalIndex )
{
   uint16_t uwLocalIndex;
   uint8_t ucBoardIndex;
   uint8_t ucMaxTerminals = MIN_TERMINALS_PER_IO_BOARD;
   char *psNumber = "5";
   if( uwTerminalIndex < TERMINAL_INDEX_START__CTA )
   {
      uwLocalIndex = uwTerminalIndex + 1;
      LCD_Char_WriteString("MR ");
      LCD_Char_WriteString(psNumber);
      LCD_Char_WriteInteger_MinLength( uwLocalIndex, 2);
   }
   else if( uwTerminalIndex < TERMINAL_INDEX_START__COP )
   {
      uwLocalIndex = uwTerminalIndex - TERMINAL_INDEX_START__CTA + 1;
      ucMaxTerminals = 2*MIN_TERMINALS_PER_IO_BOARD;
      LCD_Char_WriteString("CT ");
      LCD_Char_WriteString(psNumber);
      LCD_Char_WriteInteger_MinLength( uwLocalIndex, 2);
   }
   else if( uwTerminalIndex < TERMINAL_INDEX_START__RIS )
   {
      uwLocalIndex = uwTerminalIndex - TERMINAL_INDEX_START__COP + 1;
      ucMaxTerminals = 2*MIN_TERMINALS_PER_IO_BOARD;
      LCD_Char_WriteString("COP ");
      LCD_Char_WriteString(psNumber);
      LCD_Char_WriteInteger_MinLength( uwLocalIndex, 2);
   }
   else if( uwTerminalIndex < TERMINAL_INDEX_START__EXP )
   {
      uwLocalIndex = ( uwTerminalIndex - TERMINAL_INDEX_START__RIS )%MIN_TERMINALS_PER_IO_BOARD + 1;
      ucBoardIndex = ( uwTerminalIndex - TERMINAL_INDEX_START__RIS )/MIN_TERMINALS_PER_IO_BOARD + 1;
      LCD_Char_WriteString("RIS");
      LCD_Char_WriteInteger_MinLength(ucBoardIndex, 1);
      LCD_Char_WriteString(" ");
      LCD_Char_WriteString(psNumber);
      LCD_Char_WriteInteger_MinLength( uwLocalIndex, 2);
   }
   else
   {
      uwLocalIndex = ( uwTerminalIndex - TERMINAL_INDEX_START__EXP )%MIN_TERMINALS_PER_IO_BOARD + 1;
      ucBoardIndex = ( uwTerminalIndex - TERMINAL_INDEX_START__EXP )/MIN_TERMINALS_PER_IO_BOARD + 1;
      LCD_Char_WriteString("EXP");
      LCD_Char_WriteInteger_MinLength(ucBoardIndex, 1);
      LCD_Char_WriteString(" ");
      LCD_Char_WriteString(psNumber);
      LCD_Char_WriteInteger_MinLength( uwLocalIndex, 2);
   }
   LCD_Char_WriteString("/");
   LCD_Char_WriteString(psNumber);
   LCD_Char_WriteInteger_ExactLength( ucMaxTerminals, 2 );
}

/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void PrintFunction( uint8_t ucGroup, uint8_t ucFunction )
{
   if( ( ucGroup < INPUT_GROUP__CCB_F )
    || ( ucGroup == INPUT_GROUP__EPWR ) )
   {
      LCD_Char_WriteString( SysIO_GetInputGroupString_ByFunctionIndex(ucGroup, ucFunction) );
   }
   else
   {
      switch(ucGroup)
      {
         case INPUT_GROUP__CCB_F:
         case INPUT_GROUP__CCB_R:
            LCD_Char_WriteString("Button ");
            LCD_Char_WriteInteger_MinLength( ucFunction+1, 1 );
            break;
         case INPUT_GROUP__CC_ENABLE_F:
         case INPUT_GROUP__CC_ENABLE_R:
            LCD_Char_WriteString("Key ");
            LCD_Char_WriteInteger_MinLength( ucFunction+1, 1 );
            break;
         default:
            LCD_Char_WriteString("<invalid>");
            break;
      }
   }
}

/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void Update_Screen(void)
{
   LCD_Char_Clear();

   /* L1 */
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteString("In ");
   PrintBoardAndTerminal( uwParamIndex_Current - enPARAM16__MR_Inputs_501 );

   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   uint8_t ucGroup = ( uwParamValue_Edited >> 8 ) & 0xFF;
   uint8_t ucFunction = ( uwParamValue_Edited ) & 0x7F;
   LCD_Char_WriteString("| ");
   PrintFunction( ucGroup, ucFunction );

   /* L3 */
   LCD_Char_GotoXY( 12, 2 );
   uint8_t bInvertBit = (uwParamValue_Edited & MASK_IO_PARAM_TERMINAL_INVERSION) ? 1:0;
   if(bInvertBit)
   {
      LCD_Char_WriteString("On");
   }
   else
   {
      LCD_Char_WriteString("Off");
   }

   /* L4 */
   if( !bSavingInProgress ) /* Display cursor */
   {
      uint8_t ucCursorPos = 0;
      if( eFieldIndex == enFIELD__VALUE )
      {
         ucCursorPos = 12;
      }
      else if( eFieldIndex == enFIELD__SAVE )
      {
         ucCursorPos = 19;
      }
      LCD_Char_GotoXY( ucCursorPos, 3 );
      LCD_Char_WriteString("*");
   }
   else/* Or display saving */
   {
      LCD_Char_GotoXY( 0, 3 );
      LCD_Char_WriteString("Saving value...");
   }

   /* Only show save option if values differ  */
   uint8_t bValueEdited = uwParamValue_Edited != uwParamValue_Saved;
   if( bValueEdited )
   {
      LCD_Char_GotoXY( 16, 0 );
      LCD_Char_WriteString("Save");
      LCD_Char_GotoXY( 19, 1 );
      LCD_Char_WriteString("|");
      LCD_Char_GotoXY( 19, 2 );
      LCD_Char_WriteString("|");
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Index( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   enum en_field_action enReturnValue = enACTION__NONE;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         uwParamIndex_Current = uwParamIndex_Start;
      }
   }
   else  // enACTION__EDIT
   {
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            if( uwParamIndex_Current < uwParamIndex_End )
            {
               uwParamIndex_Current++;
            }
            else if( uwParamIndex_Current > uwParamIndex_End )
            {
               uwParamIndex_Current = uwParamIndex_End;
            }
            break;
         case enKEYPRESS_DOWN:
            if( uwParamIndex_Current > uwParamIndex_Start )
            {
               uwParamIndex_Current--;
            }
            else if( uwParamIndex_Current < uwParamIndex_Start )
            {
               uwParamIndex_Current = uwParamIndex_Start;
            }
            break;
         case enKEYPRESS_LEFT:
            enReturnValue = enACTION__EXIT_TO_LEFT;
            break;
         case enKEYPRESS_RIGHT:
            enReturnValue = enACTION__EXIT_TO_RIGHT;
            break;
         default:
            break;
      }
      uwParamValue_Saved = Param_ReadValue_16Bit( uwParamIndex_Current );
      uwParamValue_Edited = uwParamValue_Saved;
   }

   return enReturnValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Value( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   enum en_field_action enReturnValue = enACTION__NONE;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         uwParamValue_Saved = Param_ReadValue_16Bit( uwParamIndex_Current );
         uwParamValue_Edited = uwParamValue_Saved;
      }
   }
   else  // enACTION__EDIT
   {
      uint8_t bInvertBit = (uwParamValue_Edited & MASK_IO_PARAM_TERMINAL_INVERSION) ? 1:0;
      uwParamValue_Saved = Param_ReadValue_16Bit( uwParamIndex_Current );
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            bInvertBit = 1;
            break;

         case enKEYPRESS_DOWN:
            bInvertBit = 0;
            break;

         case enKEYPRESS_LEFT:
            enReturnValue = enACTION__EXIT_TO_LEFT;
            break;

         case enKEYPRESS_RIGHT:
            enReturnValue = enACTION__EXIT_TO_RIGHT;
            break;

         default:
            break;
      }

      /* This page edits the most significant bit of the terminal parameter */
      if(bInvertBit)
      {
         uwParamValue_Edited |= MASK_IO_PARAM_TERMINAL_INVERSION;
      }
      else
      {
         uwParamValue_Edited &= ~MASK_IO_PARAM_TERMINAL_INVERSION;
      }
   }

   return enReturnValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t SavingParameter( void )
{
   uint8_t bSaving = 0;
   uwParamValue_Saved = Param_ReadValue_16Bit( uwParamIndex_Current );
   if( uwParamValue_Saved != uwParamValue_Edited )
   {
      bSaving = 1;
      Param_WriteValue_16Bit( uwParamIndex_Current, uwParamValue_Edited );
   }
   return bSaving;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Save( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static enum en_save_state enSaveState = SAVE_STATE__SAVED;
   bSavingInProgress = 0;
   if ( enFieldAction == enACTION__EDIT )
   {
      if ( enKeypress == enKEYPRESS_ENTER )
      {
         enSaveState = SAVE_STATE__SAVING;
      }
      else if ( enKeypress == enKEYPRESS_LEFT )
      {
         enSaveState = SAVE_STATE__SAVED;

         enReturnValue = enACTION__EXIT_TO_LEFT;
      }
   }
   switch(enSaveState)
   {
      case SAVE_STATE__SAVING:
         if(SavingParameter())
         {
            bSavingInProgress = 1;
         }
         else
         {
            enSaveState = SAVE_STATE__SAVED;
            enReturnValue = enACTION__EXIT_TO_LEFT;
         }
         break;
      default:
         enSaveState = SAVE_STATE__SAVED;
         break;
   }
   return enReturnValue;
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void Edit_InputInversion(void)
{
   if ( eFieldIndex == enFIELD__NONE )
   {
      uwParamIndex_Current = uwParamIndex_Start;
      uwParamValue_Saved = Param_ReadValue_16Bit( uwParamIndex_Current );
      uwParamValue_Edited = uwParamValue_Saved;
      Edit_Index( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT );
      eFieldIndex = enFIELD__INDEX;
   }
   else
   {
      enum en_field_action enFieldAction;
      enum en_keypresses enKeypress = Button_GetKeypress();
      //------------------------------------------------------------------
      switch ( eFieldIndex )
      {
         case enFIELD__INDEX:
            enFieldAction = Edit_Index( enKeypress, enACTION__EDIT );
            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               PrevScreen();
               eFieldIndex = enFIELD__NONE;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT );
               eFieldIndex = enFIELD__VALUE;
            }
            break;
         //------------------------------------------------------------------
         case enFIELD__VALUE: //function
            enFieldAction = Edit_Value( enKeypress, enACTION__EDIT );
            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Index( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT );
               eFieldIndex = enFIELD__INDEX;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Save( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT );
               eFieldIndex = enFIELD__SAVE;
            }
            break;
         case enFIELD__SAVE:
            enFieldAction = Edit_Save( enKeypress, enACTION__EDIT );
            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT );

               eFieldIndex = enFIELD__VALUE;
            }
            break;
         default:
            eFieldIndex = enFIELD__INDEX;
            break;
      }
      Update_Screen();
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Invert_Inputs_MR( void )
{
   uwParamIndex_Start = enPARAM16__MR_Inputs_501;
   uwParamIndex_End = enPARAM16__MR_Inputs_508;
   Edit_InputInversion();
}
static void UI_FFS_Invert_Inputs_CT( void )
{
   uwParamIndex_Start = enPARAM16__CT_Inputs_501;
   uwParamIndex_End = enPARAM16__CT_Inputs_516;
   Edit_InputInversion();
}
static void UI_FFS_Invert_Inputs_COP( void )
{
   uwParamIndex_Start = enPARAM16__COP_Inputs_501;
   uwParamIndex_End = enPARAM16__COP_Inputs_516;
   Edit_InputInversion();
}
static void UI_FFS_Invert_Inputs_RIS( void )
{
   uwParamIndex_Start = enPARAM16__RIS1_Inputs_501;
   uwParamIndex_End = enPARAM16__RIS4_Inputs_508;
   Edit_InputInversion();
}
static void UI_FFS_Invert_Inputs_EXP1( void )
{
   uwParamIndex_Start = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_START__EXP1;
   uwParamIndex_End = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_START__EXP2 - 1;
   Edit_InputInversion();
}
static void UI_FFS_Invert_Inputs_EXP2( void )
{
   uwParamIndex_Start = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_START__EXP2;
   uwParamIndex_End = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_START__EXP3 - 1;
   Edit_InputInversion();
}
static void UI_FFS_Invert_Inputs_EXP3( void )
{
   uwParamIndex_Start = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_START__EXP3;
   uwParamIndex_End = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_START__EXP4 - 1;
   Edit_InputInversion();
}
static void UI_FFS_Invert_Inputs_EXP4( void )
{
   uwParamIndex_Start = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_START__EXP4;
   uwParamIndex_End = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_START__EXP5 - 1;
   Edit_InputInversion();
}
static void UI_FFS_Invert_Inputs_EXP5( void )
{
   uwParamIndex_Start = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_START__EXP5;
   uwParamIndex_End = enPARAM16__MR_Inputs_501 + TERMINAL_INDEX_END__EXP;
   Edit_InputInversion();
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
