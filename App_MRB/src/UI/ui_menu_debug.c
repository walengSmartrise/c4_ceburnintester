/******************************************************************************
 *
 * @file     ui_menu_setup.c
 * @brief    Setup Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "mod.h"
#include "sru.h"
#include <string.h>
#include <math.h>
#include "lcd.h"
#include "groupnet.h"
#include "groupnet_datagrams.h"
#include "hallboard.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Debug_ViewNetworkPackets( void );
static void UI_FFS_Debug_ViewGroupPackets();
static void UI_FFS_Debug_RunCounter( void );
static void UI_FFS_Debug_ViewDebugData( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_menu_item gstMI_CarCalls_Latch =
{
   .psTitle = "Enter Car Calls",
   .pstUGS_Next = &gstUGS_Menu_Setup_CarCalls,
};
static struct st_ui_menu_item gstMI_Manual_Doors =
{
   .psTitle = "Enter Door Command",
   .pstUGS_Next = &gstUGS_Doors_Control,
};
static struct st_ui_menu_item gstMI_ViewNetworkPackets =
{
   .psTitle = "View Network Packets",
   .pstUGS_Next = &gstUGS_Debug_ViewNetworkPackets,
};
static struct st_ui_menu_item gstMI_ViewGroupPackets =
{
   .psTitle = "View Group Packets",
   .pstUGS_Next = &gstUGS_Debug_ViewGroupPackets,
};
static struct st_ui_menu_item gstMI_ParamEdit =
{
   .psTitle = "Edit Parameters",
   .pstUGS_Next = &gstUGS_Menu_ParamEdit,
};

static struct st_ui_menu_item gstMI_ViewDebugData =
{
   .psTitle = "View Debug Data",
   .pstUGS_Next = &gstUGS_Debug_ViewDebugData,
};

static struct st_ui_menu_item gstMI_AcceptanceTest =
{
   .psTitle = "Acceptance Test",
   .pstUGS_Next = &gstUGS_Debug_AcceptanceTest,
};
static struct st_ui_menu_item gstMI_EmergencyBitmap =
{
   .psTitle = "EmergencyBitmap",
   .pstUGS_Next = &gstUGS_EmergencyBitmap,
};
static struct st_ui_menu_item gstMI_ModuleStatuses =
{
   .psTitle = "Module Statuses",
   .pstUGS_Next = &gstUGS_Menu_ModuleStatus,
};
static struct st_ui_menu_item gstMI_DriveSetup =
{
   .psTitle = "Drive Setup",
   .pstUGS_Next = &gstUGS_Menu_DriveSetup,
};
static struct st_ui_menu_item gstMI_CarDestinations =
{
   .psTitle = "Car Destinations",
   .pstUGS_Next = &gstUGS_CarDestination,
};
static struct st_ui_menu_item gstMI_LatchHallCalls =
{
   .psTitle = "Enter Hall Calls",
   .pstUGS_Next = &gstUGS_HC_Latch,
};
static struct st_ui_menu_item gstMI_RunCounter =
{
   .psTitle = "Run Counter",
   .pstUGS_Next = &gstUGS_Debug_RunCounter,
};
static struct st_ui_menu_item gstMI_DebugRuns =
{
   .psTitle = "DebugRuns",
   .pstUGS_Next = &gstUGS_Menu_DebugRuns,
};
static struct st_ui_menu_item gstMI_XRegDestination =
{
   .psTitle = "XReg Destination",
   .pstUGS_Next = &gstUGS_XRegDestination,
};
static struct st_ui_menu_item gstMI_CarData =
{
   .psTitle = "Car Data",
   .pstUGS_Next = &gstUGS_CarData,
};
static struct st_ui_menu_item gstMI_XRegData =
{
   .psTitle = "XReg Data",
   .pstUGS_Next = &gstUGS_XRegData,
};
static struct st_ui_menu_item gstMI_SH_CarPk =
{
   .psTitle = "SH Dynam Car Parking",
   .pstUGS_Next = &gstUGS_SH_CarPk,
};
static struct st_ui_menu_item gstMI_SH_PredictPk =
{
   .psTitle = "SH Predict Car Park",
   .pstUGS_Next = &gstUGS_SH_PredictPk,
};

static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_CarCalls_Latch,
   &gstMI_LatchHallCalls,
   &gstMI_Manual_Doors,
   &gstMI_ParamEdit,
   &gstMI_ViewNetworkPackets,
   &gstMI_ViewGroupPackets,
   &gstMI_ViewDebugData,
   &gstMI_AcceptanceTest,
   &gstMI_EmergencyBitmap,
   &gstMI_ModuleStatuses,
   &gstMI_DriveSetup,
   &gstMI_CarData,
   &gstMI_CarDestinations,
   &gstMI_RunCounter,
   &gstMI_DebugRuns,
   &gstMI_XRegDestination,
   &gstMI_XRegData,
   &gstMI_SH_CarPk,
   &gstMI_SH_PredictPk,
};
static struct st_ui_screen__menu gstMenu =
{
   .psTitle = "Debug",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};

//---------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Debug_ViewNetworkPackets =
{
   .pfnDraw = UI_FFS_Debug_ViewNetworkPackets,
};
//---------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Debug_ViewGroupPackets =
{
   .pfnDraw = UI_FFS_Debug_ViewGroupPackets,
};
//---------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Debug_RunCounter =
{
   .pfnDraw = UI_FFS_Debug_RunCounter,
};
//---------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Debug_ViewDebugData =
{
   .pfnDraw = UI_FFS_Debug_ViewDebugData,
};

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_Menu_Debug =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu,
};

//----------------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Debug_ViewNetworkPackets =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Debug_ViewNetworkPackets,
};
//----------------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Debug_ViewGroupPackets =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Debug_ViewGroupPackets,
};
//----------------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Debug_ViewDebugData =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Debug_ViewDebugData,
};
//----------------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Debug_RunCounter =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Debug_RunCounter,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
static en_view_debug_data eViewDebugDataReq;
void SetUIRequest_ViewDebugData(en_view_debug_data eVDD)
{
   eViewDebugDataReq = eVDD;
}
void ClrUIRequest_ViewDebugData(void)
{
   eViewDebugDataReq = VDD__NONE;
}
en_view_debug_data GetUIRequest_ViewDebugData(void)
{
   return eViewDebugDataReq;
}
/*-----------------------------------------------------------------------------
For viewing debugging data such as network error counters

|01234567890123456789|

|View Debug Data     |
|000 (COP CAN4)      |
|*                   |
|Err:65535 Util:100% | <-- Error Count, CAN Utilization
 -----------------------------------------------------------------------------*/
#define VIEW_DEBUG_DATA_MAX_SCROLL_X      (3)
static void PrintScreen_ViewDebugData( uint8_t ucScrollX, en_view_debug_data eCommand )
{
   static char const* const pasDebugDataStr[NUM_VDD] =
   {
      "None",//VDD__NONE,
      "MR CAN1",//VDD__MR_CAN1,
      "MR CAN2",//VDD__MR_CAN2,
      "MR CAN3",//VDD__MR_CAN3,
      "MR CAN4",//VDD__MR_CAN4,
      "MR A NET",//VDD__MR_A_NET,
      "MR B NET",//VDD__MR_B_NET,
      "MR RS485",//VDD__MR_RS485,
      "CT CAN1",//VDD__CT_CAN1,
      "CT CAN2",//VDD__CT_CAN2,
      "CT CAN3",//VDD__CT_CAN3,
      "CT CAN4",//VDD__CT_CAN4,
      "CT A NET",//VDD__CT_A_NET,
      "CT B NET",//VDD__CT_B_NET,
      "CT RS485",//VDD__CT_RS485,
      "COP CAN1",//VDD__COP_CAN1,
      "COP CAN2",//VDD__COP_CAN2,
      "COP CAN3",//VDD__COP_CAN3,
      "COP CAN4",//VDD__COP_CAN4,
      "COP A NET",//VDD__COP_A_NET,
      "COP B NET",//VDD__COP_B_NET,
      "COP RS485",//VDD__COP_RS485,
      "Run Signals",//VDD__RUN_SIGNALS,
      "Last Stop Pos",//VDD__LAST_STOP_POS,
      "MRA Vers.",//VDD__MRA_VERSION,
      "MRB Vers.",//VDD__MRB_VERSION,
      "CTA Vers.",//VDD__CTA_VERSION,
      "CTB Vers.",//VDD__CTB_VERSION,
      "COPA Vers.",//VDD__COPA_VERSION,
      "COPB Vers.",//VDD__COPB_VERSION,
      "Dir. Change Count",//VDD__DIR_CHANGE_COUNT,
      "RIS1 CAN1",//VDD__RIS1_CAN1,
      "RIS2 CAN1",//VDD__RIS2_CAN1,
      "RIS3 CAN1",//VDD__RIS3_CAN1,
      "RIS4 CAN1",//VDD__RIS4_CAN1,
      "RIS1 CAN2",//VDD__RIS1_CAN2,
      "RIS2 CAN2",//VDD__RIS2_CAN2,
      "RIS3 CAN2",//VDD__RIS3_CAN2,
      "RIS4 CAN2",//VDD__RIS4_CAN2,
      "DEST CURRENT",//VDD__DESTINATION_1,
      "DEST NEXT",//VDD__DESITNATION_2,
      "IDLE TIME",//VDD__IDLE_TIME,
      "DRV SPD",//VDD__DRIVE_SPD_FB,
      "DOOR DATA F",//VDD__DOOR_DATA_F,
      "DOOR DATA R",//VDD__DOOR_DATA_R,
      "N/A",//VDD__CPLD_MR,
      "N/A",//VDD__CPLD_CT,
      "N/A",//VDD__CPLD_COP,
   };

   LCD_Char_Clear();

   /* L1 */
   LCD_Char_GotoXY(0,0);
   LCD_Char_WriteString("VIEW DEBUG DATA");

   /* L2 */
   LCD_Char_GotoXY(0,1);
   LCD_Char_WriteInteger_ExactLength(eCommand, 3);
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteString("(");
   LCD_Char_WriteString(pasDebugDataStr[eCommand]);
   LCD_Char_WriteString(")");

   /* L3 */
   uint8_t ucCursorPos = ucScrollX;
   LCD_Char_GotoXY(ucCursorPos,2);
   LCD_Char_WriteString("*");

   /* L4 - Data */
   LCD_Char_GotoXY(0,3);
   switch(eCommand)
   {
      case VDD__MR_CAN1:
      case VDD__MR_CAN2:
      case VDD__MR_RS485:
      case VDD__MR_A_NET:
         LCD_Char_WriteString("Err:");
         LCD_Char_WriteInteger_ExactLength(GetDebugData_MRA_NetworkError(), 5);
         if( ( eCommand == VDD__MR_CAN1 ) || ( eCommand == VDD__MR_CAN2 ) )
         {
            LCD_Char_AdvanceX(1);
            LCD_Char_WriteString("Util:");
            LCD_Char_WriteInteger_MinLength(GetDebugData_MRA_CAN_Utilization(), 1);
            LCD_Char_WriteString("%");
         }
         break;
      case VDD__MR_CAN3:
      case VDD__MR_CAN4:
      case VDD__MR_B_NET:
         LCD_Char_WriteString("Err:");
         LCD_Char_WriteInteger_ExactLength(GetDebugData_MRB_NetworkError(), 5);
         if( ( eCommand == VDD__MR_CAN3 ) || ( eCommand == VDD__MR_CAN4 ) )
         {
            LCD_Char_AdvanceX(1);
            LCD_Char_WriteString("Util:");
            LCD_Char_WriteInteger_MinLength(GetDebugData_MRB_CAN_Utilization(), 1);
            LCD_Char_WriteString("%");
         }
         break;
      case VDD__CT_CAN1:
      case VDD__CT_CAN2:
      case VDD__CT_RS485:
      case VDD__CT_A_NET:
         LCD_Char_WriteString("Err:");
         LCD_Char_WriteInteger_ExactLength(GetDebugData_CTA_NetworkError(), 5);
         if( ( eCommand == VDD__CT_CAN1 ) || ( eCommand == VDD__CT_CAN2 ) )
         {
            LCD_Char_AdvanceX(1);
            LCD_Char_WriteString("Util:");
            LCD_Char_WriteInteger_MinLength(GetDebugData_CTA_CAN_Utilization(), 1);
            LCD_Char_WriteString("%");
         }
         break;
      case VDD__CT_CAN3:
      case VDD__CT_CAN4:
      case VDD__CT_B_NET:
         LCD_Char_WriteString("Err:");
         LCD_Char_WriteInteger_ExactLength(GetDebugData_CTB_NetworkError(), 5);
         if( ( eCommand == VDD__CT_CAN3 ) || ( eCommand == VDD__CT_CAN4 ) )
         {
            LCD_Char_AdvanceX(1);
            LCD_Char_WriteString("Util:");
            LCD_Char_WriteInteger_MinLength(GetDebugData_CTB_CAN_Utilization(), 1);
            LCD_Char_WriteString("%");
         }
         break;
      case VDD__COP_CAN1:
      case VDD__COP_CAN2:
      case VDD__COP_RS485:
      case VDD__COP_A_NET:
         LCD_Char_WriteString("Err:");
         LCD_Char_WriteInteger_ExactLength(GetDebugData_COPA_NetworkError(), 5);
         if( ( eCommand == VDD__COP_CAN1 ) || ( eCommand == VDD__COP_CAN2 ) )
         {
            LCD_Char_AdvanceX(1);
            LCD_Char_WriteString("Util:");
            LCD_Char_WriteInteger_MinLength(GetDebugData_COPA_CAN_Utilization(), 1);
            LCD_Char_WriteString("%");
         }
         break;
      case VDD__COP_CAN3:
      case VDD__COP_CAN4:
      case VDD__COP_B_NET:
         LCD_Char_WriteString("Err:");
         LCD_Char_WriteInteger_ExactLength(GetDebugData_COPB_NetworkError(), 5);
         if( ( eCommand == VDD__COP_CAN3 ) || ( eCommand == VDD__COP_CAN4 ) )
         {
            LCD_Char_AdvanceX(1);
            LCD_Char_WriteString("Util:");
            LCD_Char_WriteInteger_MinLength(GetDebugData_COPB_CAN_Utilization(), 1);
            LCD_Char_WriteString("%");
         }
         break;
      case VDD__RUN_SIGNALS:
         LCD_Char_WriteBin_16(GetDebugData_MRA_NetworkError());
         break;
      case VDD__LAST_STOP_POS:
         LCD_Char_WriteInteger_MinLength(GetDebugData_MRA_Data(), 1);
         break;
      case VDD__MRA_VERSION:
         LCD_Char_WriteString("v.");
         for(uint8_t i = 0; i < 4; i++)
         {
            uint8_t ucChar = GET_BYTE_U32( GetDebugData_MRA_Data(), i );
            if( !( ( ( ucChar >= '0' ) && ( ucChar <= '9' ) )
                || ( ( ucChar >= 'a' ) && ( ucChar <= 'z' ) )
                || ( ( ucChar >= 'A' ) && ( ucChar <= 'Z' ) ) ) )
            {
               ucChar = ' ';
            }
            LCD_Char_WriteChar(ucChar);
         }
         break;
      case VDD__MRB_VERSION:
         LCD_Char_WriteString("v.");
         for(uint8_t i = 0; i < 4; i++)
         {
            uint8_t ucChar = GET_BYTE_U32( GetDebugData_MRB_Data(), i );
            if( !( ( ( ucChar >= '0' ) && ( ucChar <= '9' ) )
                || ( ( ucChar >= 'a' ) && ( ucChar <= 'z' ) )
                || ( ( ucChar >= 'A' ) && ( ucChar <= 'Z' ) ) ) )
            {
               ucChar = ' ';
            }
            LCD_Char_WriteChar(ucChar);
         }
         break;
      case VDD__CTA_VERSION:
         LCD_Char_WriteString("v.");
         for(uint8_t i = 0; i < 4; i++)
         {
            uint8_t ucChar = GET_BYTE_U32( GetDebugData_CTA_Data(), i );
            if( !( ( ( ucChar >= '0' ) && ( ucChar <= '9' ) )
                || ( ( ucChar >= 'a' ) && ( ucChar <= 'z' ) )
                || ( ( ucChar >= 'A' ) && ( ucChar <= 'Z' ) ) ) )
            {
               ucChar = ' ';
            }
            LCD_Char_WriteChar(ucChar);
         }
         break;
      case VDD__CTB_VERSION:
         LCD_Char_WriteString("v.");
         for(uint8_t i = 0; i < 4; i++)
         {
            uint8_t ucChar = GET_BYTE_U32( GetDebugData_CTB_Data(), i );
            if( !( ( ( ucChar >= '0' ) && ( ucChar <= '9' ) )
                || ( ( ucChar >= 'a' ) && ( ucChar <= 'z' ) )
                || ( ( ucChar >= 'A' ) && ( ucChar <= 'Z' ) ) ) )
            {
               ucChar = ' ';
            }
            LCD_Char_WriteChar(ucChar);
         }
         break;
      case VDD__COPA_VERSION:
         LCD_Char_WriteString("v.");
         for(uint8_t i = 0; i < 4; i++)
         {
            uint8_t ucChar = GET_BYTE_U32( GetDebugData_COPA_Data(), i );
            if( !( ( ( ucChar >= '0' ) && ( ucChar <= '9' ) )
                || ( ( ucChar >= 'a' ) && ( ucChar <= 'z' ) )
                || ( ( ucChar >= 'A' ) && ( ucChar <= 'Z' ) ) ) )
            {
               ucChar = ' ';
            }
            LCD_Char_WriteChar(ucChar);
         }
         break;
      case VDD__COPB_VERSION:
         LCD_Char_WriteString("v.");
         for(uint8_t i = 0; i < 4; i++)
         {
            uint8_t ucChar = GET_BYTE_U32( GetDebugData_COPB_Data(), i );
            if( !( ( ( ucChar >= '0' ) && ( ucChar <= '9' ) )
                || ( ( ucChar >= 'a' ) && ( ucChar <= 'z' ) )
                || ( ( ucChar >= 'A' ) && ( ucChar <= 'Z' ) ) ) )
            {
               ucChar = ' ';
            }
            LCD_Char_WriteChar(ucChar);
         }
         break;
      case VDD__DIR_CHANGE_COUNT:
         LCD_Char_WriteInteger_MinLength(GetDebugData_MRA_Data(), 1);
         break;
      case VDD__RIS1_CAN1 ... VDD__RIS4_CAN2:
         LCD_Char_WriteString("Err:");
         LCD_Char_WriteInteger_ExactLength(GetDebugData_MRB_Data(), 5);
         break;
      case VDD__DESTINATION_1:
      {
         uint32_t uiData = GetDebugData_MRA_Data();
         uint8_t ucFloor = uiData & 0xFF;
         en_hc_dir_plus1 eArrivalDir = ( uiData >> 8 ) & 0x03;
         en_hc_dir_plus1 eCallDir = ( uiData >> 10 ) & 0x03;
         uint8_t bDirectionChange_HC = ( uiData >> 12 ) & 0x01;
         LCD_Char_GotoXY(6, 2);
         LCD_Char_WriteString("C:");
         LCD_Char_WriteInteger_ExactLength(ucFloor, 3);
         LCD_Char_AdvanceX(1);
         if( eArrivalDir == HC_DIR_PLUS1__DN )
         {
            LCD_Char_WriteString("DN");
         }
         else if( eArrivalDir == HC_DIR_PLUS1__UP )
         {
            LCD_Char_WriteString("UP");
         }
         else
         {
            LCD_Char_WriteString("CC");
         }
         LCD_Char_AdvanceX(1);
         if( eCallDir == HC_DIR_PLUS1__DN )
         {
            LCD_Char_WriteString("DN");
         }
         else if( eCallDir == HC_DIR_PLUS1__UP )
         {
            LCD_Char_WriteString("UP");
         }
         else
         {
            LCD_Char_WriteString("CC");
         }
         LCD_Char_AdvanceX(1);
         LCD_Char_WriteInteger_ExactLength(bDirectionChange_HC, 1);

         // Next Dest Same Dir
         uiData = uiData >> 16;
         ucFloor = uiData & 0xFF;
         eArrivalDir = ( uiData >> 8 ) & 0x03;
         eCallDir = ( uiData >> 10 ) & 0x03;
         bDirectionChange_HC = ( uiData >> 12 ) & 0x01;
         LCD_Char_GotoXY(6, 3);
         LCD_Char_WriteString("N:");
         LCD_Char_WriteInteger_ExactLength(ucFloor, 3);
         LCD_Char_AdvanceX(1);
         if( eArrivalDir == HC_DIR_PLUS1__DN )
         {
            LCD_Char_WriteString("DN");
         }
         else if( eArrivalDir == HC_DIR_PLUS1__UP )
         {
            LCD_Char_WriteString("UP");
         }
         else
         {
            LCD_Char_WriteString("CC");
         }
         LCD_Char_AdvanceX(1);
         if( eCallDir == HC_DIR_PLUS1__DN )
         {
            LCD_Char_WriteString("DN");
         }
         else if( eCallDir == HC_DIR_PLUS1__UP )
         {
            LCD_Char_WriteString("UP");
         }
         else
         {
            LCD_Char_WriteString("CC");
         }
         LCD_Char_AdvanceX(1);
         LCD_Char_WriteInteger_ExactLength(bDirectionChange_HC, 1);
      }
         break;
      case VDD__DESTINATION_2:
      {
         uint32_t uiData = GetDebugData_MRA_Data();
         uint8_t ucFloor = uiData & 0xFF;
         en_hc_dir_plus1 eArrivalDir = ( uiData >> 8 ) & 0x03;
         en_hc_dir_plus1 eCallDir = ( uiData >> 10 ) & 0x03;
         uint8_t bDirectionChange_HC = ( uiData >> 12 ) & 0x01;
         LCD_Char_WriteString("N:");
         LCD_Char_WriteInteger_ExactLength(ucFloor, 3);
         LCD_Char_AdvanceX(1);
         if( eArrivalDir == HC_DIR_PLUS1__DN )
         {
            LCD_Char_WriteString("DN");
         }
         else if( eArrivalDir == HC_DIR_PLUS1__UP )
         {
            LCD_Char_WriteString("UP");
         }
         else
         {
            LCD_Char_WriteString("CC");
         }
         LCD_Char_AdvanceX(1);
         if( eCallDir == HC_DIR_PLUS1__DN )
         {
            LCD_Char_WriteString("DN");
         }
         else if( eCallDir == HC_DIR_PLUS1__UP )
         {
            LCD_Char_WriteString("UP");
         }
         else
         {
            LCD_Char_WriteString("CC");
         }
         LCD_Char_AdvanceX(1);
         LCD_Char_WriteInteger_ExactLength(bDirectionChange_HC, 1);

         // In Dest DZ
         LCD_Char_AdvanceX(1);
         LCD_Char_WriteString("D-DZ:");
         LCD_Char_WriteInteger_ExactLength(( uiData >> 16 ) & 1, 1);
      }
         break;
      case VDD__IDLE_TIME:
      {
         uint16_t uwIdleTime_s = GetDebugData_MRA_Data() & 0xFFF;
         uint16_t uwDestChangeTime_s = ( GetDebugData_MRA_Data() >> 12 ) & 0xFFF;
         uint8_t ucParkingFloor_Plus1 = ( GetDebugData_MRA_Data() >> 24) & 0x7F;
         uint8_t bParkingWithDoorOpen = ( GetDebugData_MRA_Data() >> 31 ) & 1;
         LCD_Char_WriteString("I:");
         LCD_Char_WriteInteger_ExactLength(uwIdleTime_s, 5);
         LCD_Char_WriteString(" D:");
         LCD_Char_WriteInteger_ExactLength(uwDestChangeTime_s, 5);

         LCD_Char_GotoXY(10, 2);
         LCD_Char_WriteString("P:");
         LCD_Char_WriteInteger_ExactLength(ucParkingFloor_Plus1, 3);
         LCD_Char_AdvanceX(1);
         LCD_Char_WriteString("DO:");
         LCD_Char_WriteInteger_ExactLength(bParkingWithDoorOpen, 1);
      }
         break;
      case VDD__DRIVE_SPD_FB:
         LCD_Char_WriteString("DRV:");
         LCD_Char_WriteSignedInteger(GetDebug_DriveSpeedFeedback_MRA());

         LCD_Char_GotoXY(11, 3);
         LCD_Char_WriteString("CMD:");
         LCD_Char_WriteSignedInteger(GetMotion_SpeedCommand());

         LCD_Char_GotoXY(11, 2);
         LCD_Char_WriteString("FPM:");
         LCD_Char_WriteSignedInteger(GetPosition_Velocity());
         break;
      case VDD__DOOR_DATA_F:
      {
         uint32_t uiData = GetDebugData_MRA_Data();
         enum en_door_commands eCurrentDoorCommand = uiData & 0x1F;
         enum en_door_commands eLastDoorCommand = ( uiData >> 5 ) & 0x1F;
         uint16_t uwCurrentTimer_200ms = ( uiData >> 10 ) & 0x7FF;
         uint16_t uwTimerLimit_200ms = ( uiData >> 21 ) & 0x7FF;
         LCD_Char_WriteInteger_ExactLength(eCurrentDoorCommand, 2);
         LCD_Char_AdvanceX(1);
         LCD_Char_WriteInteger_ExactLength(eLastDoorCommand, 2);
         LCD_Char_AdvanceX(1);
         LCD_Char_WriteInteger_MinLength(uwCurrentTimer_200ms, 5);
         LCD_Char_AdvanceX(1);
         LCD_Char_WriteInteger_MinLength(uwTimerLimit_200ms, 5);

         LCD_Char_GotoXY(15, 2);
         uint8_t bIN_PHE_F = ( GetPHE2InputProgrammed_F2() ) ? ( GetInputValue(enIN_PHE_F) & GetInputValue(enIN_PHE_F2) ):GetInputValue(enIN_PHE_F);

         UI_PrintDoorSymbol( GetDoorState_Front(),
                             bIN_PHE_F,
                             GetInputValue(enIN_GSWF),
                             GetOutputValue(enOUT_DO_F),
                             GetOutputValue(enOUT_DC_F) );
      }
         break;
      case VDD__DOOR_DATA_R:
      {
         uint32_t uiData = GetDebugData_MRA_Data();
         enum en_door_commands eCurrentDoorCommand = uiData & 0x1F;
         enum en_door_commands eLastDoorCommand = ( uiData >> 5 ) & 0x1F;
         uint16_t uwCurrentTimer_200ms = ( uiData >> 10 ) & 0x7FF;
         uint16_t uwTimerLimit_200ms = ( uiData >> 21 ) & 0x7FF;
         LCD_Char_WriteInteger_ExactLength(eCurrentDoorCommand, 2);
         LCD_Char_AdvanceX(1);
         LCD_Char_WriteInteger_ExactLength(eLastDoorCommand, 2);
         LCD_Char_AdvanceX(1);
         LCD_Char_WriteInteger_MinLength(uwCurrentTimer_200ms, 5);
         LCD_Char_AdvanceX(1);
         LCD_Char_WriteInteger_MinLength(uwTimerLimit_200ms, 5);

         LCD_Char_GotoXY(15, 2);
         uint8_t bIN_PHE_R = ( GetPHE2InputProgrammed_R2() ) ? ( GetInputValue(enIN_PHE_R) & GetInputValue(enIN_PHE_R2) ):GetInputValue(enIN_PHE_R);

         UI_PrintDoorSymbol( GetDoorState_Rear(),
                             bIN_PHE_R,
                             GetInputValue(enIN_GSWR),
                             GetOutputValue(enOUT_DO_R),
                             GetOutputValue(enOUT_DC_R) );
      }
         break;
      default:
         LCD_Char_WriteString("N/A");
         break;
   }
}
static void UI_FFS_Debug_ViewDebugData( void )
{
   static uint8_t ucScrollX;
   static en_view_debug_data eLocalCommand;
   int16_t wEditedValue = eLocalCommand;

   //-----------------------------------------
   /* Screen control */
   int8_t cMagnitude = 0;
   if( ucScrollX == 0 )
   {
      cMagnitude = 100;
   }
   else if( ucScrollX == 1 )
   {
      cMagnitude = 10;
   }
   else if( ucScrollX == 2 )
   {
      cMagnitude = 1;
   }

   enum en_keypresses enKeypress = Button_GetKeypress();
   switch(enKeypress)
   {
      case enKEYPRESS_LEFT:
         if(!ucScrollX)
         {
            PrevScreen();
         }
         else
         {
            ucScrollX--;
         }
         break;
      case enKEYPRESS_RIGHT:
         ucScrollX++;
         break;
      case enKEYPRESS_UP:
         wEditedValue += cMagnitude;
         break;
      case enKEYPRESS_DOWN:
         wEditedValue -= cMagnitude;
         break;
      default: break;
   }
   /* Bound Values */
   if( wEditedValue < 0 )
   {
      wEditedValue = 0;
   }
   else if( wEditedValue >= NUM_VDD )
   {
      wEditedValue = NUM_VDD-1;
   }

   if(ucScrollX >= VIEW_DEBUG_DATA_MAX_SCROLL_X)
   {
      ucScrollX = VIEW_DEBUG_DATA_MAX_SCROLL_X-1;
   }

   eViewDebugDataReq = (en_view_debug_data) wEditedValue;
   eLocalCommand = (en_view_debug_data) wEditedValue;
   PrintScreen_ViewDebugData(ucScrollX, GetUIRequest_ViewDebugDataCommand());
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void PrintScreen_ViewNetworkPackets(uint8_t ucCursor, Datagram_ID eID)
{
   LCD_Char_Clear();

   /* Marker */
   LCD_Char_GotoXY(3,0);
   LCD_Char_WriteString("(LSB)");

   /* Datagram Data */
   un_sdata_datagram unDatagram;
   GetSharedDatagram_ByDatagramID(&unDatagram, eID);
   LCD_Char_GotoXY(9,0);
   LCD_Char_WriteHex_8( unDatagram.auc8[0] );
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteHex_8( unDatagram.auc8[1] );
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteHex_8( unDatagram.auc8[2] );
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteHex_8( unDatagram.auc8[3] );

   LCD_Char_GotoXY(9,1);
   LCD_Char_WriteHex_8( unDatagram.auc8[4] );
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteHex_8( unDatagram.auc8[5] );
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteHex_8( unDatagram.auc8[6] );
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteHex_8( unDatagram.auc8[7] );

   /* Datagram Num */
   LCD_Char_GotoXY(0,2);
   LCD_Char_WriteString("PACKET ");
   LCD_Char_WriteInteger_ExactLength( eID, 3 );


   /* Receive Count */
   LCD_Char_GotoXY(11,3);
   LCD_Char_WriteString("RX:");
   LCD_Char_WriteInteger_ExactLength( (uint16_t) GetDatagramReceivedCount(eID), 5 );

   /* Cursor */
   LCD_Char_GotoXY(7+ucCursor,3);
   LCD_Char_WriteString("*");
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Debug_ViewNetworkPackets( void )
{
   static const uint8_t ucMaxCursorPos = 2;
   static uint8_t ucCursorPos;
   static Datagram_ID ePacketIndex;
   /* Screen Navigation */
   int32_t iNewPacketIndex = ePacketIndex;
   int32_t iPosMagnitude = powf(10, ucMaxCursorPos-ucCursorPos);
   enum en_keypresses enKeypress = Button_GetKeypress();
   if ( enKeypress == enKEYPRESS_LEFT )
   {
      if( ucCursorPos )
      {
         ucCursorPos--;
      }
      else
      {
         PrevScreen();
      }
   }
   else if ( enKeypress == enKEYPRESS_RIGHT )
   {
      if( ucCursorPos < ucMaxCursorPos )
      {
         ucCursorPos++;
      }
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      iNewPacketIndex += iPosMagnitude;
   }
   else if ( enKeypress == enKEYPRESS_DOWN )
   {
      iNewPacketIndex -= iPosMagnitude;
   }

   if( iNewPacketIndex < 0 )
   {
      iNewPacketIndex = 0;
   }
   else if( iNewPacketIndex > NUM_DATAGRAM_IDs )
   {
      iNewPacketIndex = NUM_DATAGRAM_IDs;
   }
   ePacketIndex = iNewPacketIndex;

   /* Screen Update */
   PrintScreen_ViewNetworkPackets(ucCursorPos, ePacketIndex);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void PrintScreen_ViewGroupPackets(uint8_t ucCursor, GroupNet_DatagramID eID)
{
   LCD_Char_Clear();

   /* Marker */
   LCD_Char_GotoXY(3,0);
   LCD_Char_WriteString("(LSB)");

   /* Datagram Data */
   un_sdata_datagram *punDatagram = GetGroupDatagram( eID );
   LCD_Char_GotoXY(9,0);
   LCD_Char_WriteHex_8( punDatagram->auc8[0] );
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteHex_8( punDatagram->auc8[1] );
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteHex_8( punDatagram->auc8[2] );
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteHex_8( punDatagram->auc8[3] );

   LCD_Char_GotoXY(9,1);
   LCD_Char_WriteHex_8( punDatagram->auc8[4] );
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteHex_8( punDatagram->auc8[5] );
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteHex_8( punDatagram->auc8[6] );
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteHex_8( punDatagram->auc8[7] );

   /* Datagram Num */
   LCD_Char_GotoXY(0,2);
   LCD_Char_WriteString("PACKET ");
   LCD_Char_WriteInteger_ExactLength( eID, 3 );


   /* Receive Count */
   LCD_Char_GotoXY(11,3);
   LCD_Char_WriteString("RX:");
   LCD_Char_WriteInteger_ExactLength( (uint16_t) GroupNet_GetDatagramRXCounter(eID), 5 );

   /* Cursor */
   LCD_Char_GotoXY(7+ucCursor,3);
   LCD_Char_WriteString("*");
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Debug_ViewGroupPackets( void )
{
   static const uint8_t ucMaxCursorPos = 2;
   static uint8_t ucCursorPos;
   static Datagram_ID ePacketIndex;
   /* Screen Navigation */
   int32_t iNewPacketIndex = ePacketIndex;
   int32_t iPosMagnitude = powf(10, ucMaxCursorPos-ucCursorPos);
   enum en_keypresses enKeypress = Button_GetKeypress();
   if ( enKeypress == enKEYPRESS_LEFT )
   {
      if( ucCursorPos )
      {
         ucCursorPos--;
      }
      else
      {
         PrevScreen();
      }
   }
   else if ( enKeypress == enKEYPRESS_RIGHT )
   {
      if( ucCursorPos < ucMaxCursorPos )
      {
         ucCursorPos++;
      }
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      iNewPacketIndex += iPosMagnitude;
   }
   else if ( enKeypress == enKEYPRESS_DOWN )
   {
      iNewPacketIndex -= iPosMagnitude;
   }

   if( iNewPacketIndex < 0 )
   {
      iNewPacketIndex = 0;
   }
   else if( iNewPacketIndex > GROUP_NET_NUM_DATAGRAMS )
   {
      iNewPacketIndex = GROUP_NET_NUM_DATAGRAMS;
   }
   ePacketIndex = iNewPacketIndex;

   /* Screen Update */
   PrintScreen_ViewGroupPackets(ucCursorPos, ePacketIndex);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Debug_RunCounter( void )
{
   enum en_keypresses enKeypress = Button_GetKeypress();
   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }

   /* Screen Update */
   LCD_Char_Clear();
   LCD_Char_GotoXY(0, 0);
   LCD_Char_WriteString("RUN COUNTER");

   LCD_Char_GotoXY(5, 2);
   LCD_Char_WriteInteger(GetOperation4_RunCounter());
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
