/******************************************************************************
 *
 * @file     ui_menu_main.c
 * @brief    Main Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_ui_menu_item gstMI_Status =
{
   .psTitle = "Status",
   .pstUGS_Next = &gstUGS_Menu_Status,
};

static struct st_ui_menu_item gstMI_Faults =
{
   .psTitle = "Faults",
   .pstUGS_Next = &gstUGS_Menu_Faults,
};
static struct st_ui_menu_item gstMI_Alarms =
{
   .psTitle = "Alarms",
   .pstUGS_Next = &gstUGS_Menu_Alarms,
};
static struct st_ui_menu_item gstMI_Setup =
{
   .psTitle = "Setup",
   .pstUGS_Next = &gstUGS_Menu_Setup,
};

static struct st_ui_menu_item gstMI_Debug =
{
   .psTitle = "Debug",
   .pstUGS_Next = &gstUGS_Menu_Debug,
};
#if 0
static struct st_ui_menu_item gstMI_Test =
{
   .psTitle = "Test",
};
#endif
static struct st_ui_menu_item gstMI_About =
{
   .psTitle = "About",
   .pstUGS_Next = &gstUGS_AboutScreen,
};

static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_Status,
   &gstMI_Faults,
   &gstMI_Alarms,
   &gstMI_Setup,
   &gstMI_Debug,
//   &gstMI_Test,
   &gstMI_About,
};

static struct st_ui_screen__menu gstMenu =
{
   .psTitle = "Main Menu",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

//-----------------------------------------------------------------------------
struct st_ui_generic_screen gstUGS_MainMenu =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
