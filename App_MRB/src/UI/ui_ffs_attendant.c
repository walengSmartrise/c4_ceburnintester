/******************************************************************************
 *
 * @file     ui_ffs_attendant.c
 * @brief    Attendant operation UI pages
 * @version  V1.00
 * @date     13, Feb 2018
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
//static void UI_FFS_ATTD_DirWithCCB( void );
static void UI_FFS_ATTD_DispatchTimeout( void );
static void UI_FFS_ATTD_BuzzerTime( void );
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
//static struct st_ui_screen__freeform gstFFS_ATTD_DirWithCCB =
//{
//   .pfnDraw = UI_FFS_ATTD_DirWithCCB,
//};
static struct st_ui_screen__freeform gstFFS_ATTD_DispatchTimeout =
{
   .pfnDraw = UI_FFS_ATTD_DispatchTimeout,
};
static struct st_ui_screen__freeform gstFFS_ATTD_BuzzerTime =
{
   .pfnDraw = UI_FFS_ATTD_BuzzerTime,
};
//----------------------------------------------------------
//static struct st_ui_menu_item gstMI_ATTD_DirWithCCB =
//{
//   .psTitle = "Dir With CCB",
//   .pstUGS_Next = &gstUGS_ATTD_DirWithCCB,
//};
static struct st_ui_menu_item gstMI_ATTD_DispatchTimeout =
{
   .psTitle = "Dispatch Timeout",
   .pstUGS_Next = &gstUGS_ATTD_DispatchTimeout,
};
static struct st_ui_menu_item gstMI_ATTD_BuzzerTime =
{
   .psTitle = "Buzzer Time",
   .pstUGS_Next = &gstUGS_ATTD_BuzzerTime,
};
static struct st_ui_menu_item * gastMenuItems[] =
{
//   &gstMI_ATTD_DirWithCCB,
   &gstMI_ATTD_DispatchTimeout,
   &gstMI_ATTD_BuzzerTime,
};
static struct st_ui_screen__menu gstMenu_Setup_ATTD =
{
   .psTitle = "Attendant",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
//struct st_ui_generic_screen gstUGS_ATTD_DirWithCCB =
//{
//   .ucType = enUI_STYPE__FREEFORM,
//   .pvReference = &gstFFS_ATTD_DirWithCCB,
//};
struct st_ui_generic_screen gstUGS_ATTD_DispatchTimeout =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ATTD_DispatchTimeout,
};
struct st_ui_generic_screen gstUGS_ATTD_BuzzerTime =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ATTD_BuzzerTime,
};
//---------------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_Setup_ATTD =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_ATTD,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
//static void UI_FFS_ATTD_DirWithCCB( void )
//{
//   static struct st_param_edit_menu stParamEdit;
//   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
//   stParamEdit.uwParamIndex_Start = enPARAM1__ATTD_DirWithCCB;
//   stParamEdit.uwParamIndex_End = enPARAM1__ATTD_DirWithCCB;
//   stParamEdit.psTitle = "Dir With CCB";
//   stParamEdit.psUnit = "";
//   UI_ParamEditSceenTemplate(&stParamEdit);
//}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_ATTD_DispatchTimeout( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__ATTD_DispatchTimeout_1s;
   stParamEdit.uwParamIndex_End = enPARAM8__ATTD_DispatchTimeout_1s;
   stParamEdit.psTitle = "Dispatch Timeout";
   stParamEdit.psUnit = " sec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_ATTD_BuzzerTime( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__ATTD_BuzzerTime_100ms;
   stParamEdit.uwParamIndex_End = enPARAM8__ATTD_BuzzerTime_100ms;
   stParamEdit.psTitle = "Buzzer Time";
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_One;
   stParamEdit.psUnit = " sec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
