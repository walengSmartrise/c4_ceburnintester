/******************************************************************************
 *
 * @file     ui_ffs_nts.c
 * @brief    nts UI pages
 * @version  V1.00
 * @date     13, Feb 2018
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_NTS_Vel1(void);
static void UI_FFS_NTS_Vel3(void);
static void UI_FFS_NTS_Vel4(void);
static void UI_FFS_NTS_Pos1(void);
static void UI_FFS_NTS_Pos3(void);
static void UI_FFS_NTS_Pos4(void);
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_NTS_Vel1 =
{
   .pfnDraw = UI_FFS_NTS_Vel1,
};
static struct st_ui_screen__freeform gstFFS_NTS_Vel3 =
{
   .pfnDraw = UI_FFS_NTS_Vel3,
};
static struct st_ui_screen__freeform gstFFS_NTS_Vel4 =
{
   .pfnDraw = UI_FFS_NTS_Vel4,
};
static struct st_ui_screen__freeform gstFFS_NTS_Pos1 =
{
   .pfnDraw = UI_FFS_NTS_Pos1,
};
static struct st_ui_screen__freeform gstFFS_NTS_Pos3 =
{
   .pfnDraw = UI_FFS_NTS_Pos3,
};
static struct st_ui_screen__freeform gstFFS_NTS_Pos4 =
{
   .pfnDraw = UI_FFS_NTS_Pos4,
};

//--------------------------------------------------
static struct st_ui_menu_item gstMI_NTS_Pos1 =
{
   .psTitle = "NTS Pos P1",
   .pstUGS_Next = &gstUGS_NTS_Pos1,
};
static struct st_ui_menu_item gstMI_NTS_Pos3 =
{
   .psTitle = "NTS Pos P3",
   .pstUGS_Next = &gstUGS_NTS_Pos3,
};
static struct st_ui_menu_item gstMI_NTS_Pos4 =
{
   .psTitle = "NTS Pos P4",
   .pstUGS_Next = &gstUGS_NTS_Pos4,
};

static struct st_ui_menu_item * gastMenuItems_NTS_Pos[] =
{
   &gstMI_NTS_Pos1,
   &gstMI_NTS_Pos3,
   &gstMI_NTS_Pos4,
};
static struct st_ui_screen__menu gstMenu_NTS_Pos =
{
   .psTitle = "NTS Positions ",
   .pastMenuItems = &gastMenuItems_NTS_Pos,
   .ucNumItems = sizeof(gastMenuItems_NTS_Pos) / sizeof(gastMenuItems_NTS_Pos[ 0 ]),
};
//--------------------------------------------------
static struct st_ui_menu_item gstMI_NTS_Vel1 =
{
   .psTitle = "NTS Vel P1",
   .pstUGS_Next = &gstUGS_NTS_Vel1,
};
static struct st_ui_menu_item gstMI_NTS_Vel3 =
{
   .psTitle = "NTS Vel P3",
   .pstUGS_Next = &gstUGS_NTS_Vel3,
};
static struct st_ui_menu_item gstMI_NTS_Vel4 =
{
   .psTitle = "NTS Vel P4",
   .pstUGS_Next = &gstUGS_NTS_Vel4,
};

static struct st_ui_menu_item * gastMenuItems_NTS_Vel[] =
{
   &gstMI_NTS_Vel1,
   &gstMI_NTS_Vel3,
   &gstMI_NTS_Vel4,
};
static struct st_ui_screen__menu gstMenu_NTS_Vel =
{
   .psTitle = "NTS Velocity ",
   .pastMenuItems = &gastMenuItems_NTS_Vel,
   .ucNumItems = sizeof(gastMenuItems_NTS_Vel) / sizeof(gastMenuItems_NTS_Vel[ 0 ]),
};
//--------------------------------------------------
static struct st_ui_menu_item gstMI_NTS_Pos =
{
   .psTitle = "NTS Pos",
   .pstUGS_Next = &gstUGS_NTS_Pos,
};
static struct st_ui_menu_item gstMI_NTS_Vel =
{
   .psTitle = "NTS Vel",
   .pstUGS_Next = &gstUGS_NTS_Vel,
};
static struct st_ui_menu_item * gastMenuItems_NTS[] =
{
   &gstMI_NTS_Pos,
   &gstMI_NTS_Vel,
};
static struct st_ui_screen__menu gstMenu_NTS =
{
   .psTitle = "NTS",
   .pastMenuItems = &gastMenuItems_NTS,
   .ucNumItems = sizeof(gastMenuItems_NTS) / sizeof(gastMenuItems_NTS[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_NTS_Vel1 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_NTS_Vel1,
};
struct st_ui_generic_screen gstUGS_NTS_Vel3 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_NTS_Vel3,
};
struct st_ui_generic_screen gstUGS_NTS_Vel4 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_NTS_Vel4,
};
struct st_ui_generic_screen gstUGS_NTS_Pos1 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_NTS_Pos1,
};
struct st_ui_generic_screen gstUGS_NTS_Pos3 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_NTS_Pos3,
};
struct st_ui_generic_screen gstUGS_NTS_Pos4 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_NTS_Pos4,
};
//--------------------------------------------------
struct st_ui_generic_screen gstUGS_Setup_NTS =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_NTS,
};
struct st_ui_generic_screen gstUGS_NTS_Pos =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_NTS_Pos ,
};
struct st_ui_generic_screen gstUGS_NTS_Vel =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_NTS_Vel ,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
//Needs to be edited
static void UI_FFS_NTS_Pos1( void )
{
   static struct st_param_edit_menu stParamEdit;
    stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
    stParamEdit.uwParamIndex_Start = enPARAM16__NTS_POS_P1_0;
    stParamEdit.uwParamIndex_End = enPARAM16__NTS_POS_P1_7;
    stParamEdit.psTitle = "NTS Pos P1";
    stParamEdit.psUnit = " x 5mm";
    UI_ParamEditSceenTemplate(&stParamEdit);
}
static void UI_FFS_NTS_Pos3( void )
{
   static struct st_param_edit_menu stParamEdit;
    stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
    stParamEdit.uwParamIndex_Start = enPARAM16__NTS_POS_P3_0;
    stParamEdit.uwParamIndex_End = enPARAM16__NTS_POS_P3_7;
    stParamEdit.psTitle = "NTS Pos P3";
    stParamEdit.psUnit = " x 5mm";
    UI_ParamEditSceenTemplate(&stParamEdit);
}
static void UI_FFS_NTS_Pos4( void )
{
   static struct st_param_edit_menu stParamEdit;
    stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
    stParamEdit.uwParamIndex_Start = enPARAM16__NTS_POS_P4_0;
    stParamEdit.uwParamIndex_End = enPARAM16__NTS_POS_P4_7;
    stParamEdit.psTitle = "NTS Pos P4";
    stParamEdit.psUnit = " x 5mm";
    UI_ParamEditSceenTemplate(&stParamEdit);
}
static void UI_FFS_NTS_Vel1( void )
{
   static struct st_param_edit_menu stParamEdit;
    stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
    stParamEdit.uwParamIndex_Start = enPARAM16__NTS_VEL_P1_0;
    stParamEdit.uwParamIndex_End = enPARAM16__NTS_VEL_P1_7;
    stParamEdit.psTitle = "NTS Vel P1";
    stParamEdit.psUnit = " fpm";
    UI_ParamEditSceenTemplate(&stParamEdit);
}
static void UI_FFS_NTS_Vel3( void )
{
   static struct st_param_edit_menu stParamEdit;
    stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
    stParamEdit.uwParamIndex_Start = enPARAM16__NTS_VEL_P3_0;
    stParamEdit.uwParamIndex_End = enPARAM16__NTS_VEL_P3_7;
    stParamEdit.psTitle = "NTS Vel P3";
    stParamEdit.psUnit = " fpm";
    UI_ParamEditSceenTemplate(&stParamEdit);
}
static void UI_FFS_NTS_Vel4( void )
{
   static struct st_param_edit_menu stParamEdit;
    stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
    stParamEdit.uwParamIndex_Start = enPARAM16__NTS_VEL_P4_0;
    stParamEdit.uwParamIndex_End = enPARAM16__NTS_VEL_P4_7;
    stParamEdit.psTitle = "NTS Vel P4";
    stParamEdit.psUnit = " fpm";
    UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
