/******************************************************************************
 *
 * @file     ui_ffs_carcalls.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include "GlobalData.h"
#include "operation.h"
#include "masterCommand.h"
#include "hallData.h"
#include "carData.h"
#include "carDestination.h"
#include "group_net_data.h"
#include <stdlib.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_HC_Latch( void );
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_HC_Latch =
{
   .pfnDraw = UI_FFS_HC_Latch,
};

static uint8_t ucCursorX;

static uint8_t ucLanding;
static en_hc_dir eDir;
static uint32_t uiMask;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_HC_Latch =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_HC_Latch,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void TEST_EnterFloorToFloor()
{
   static uint8_t bFirst = 1;
   static uint16_t uwDelay = 0;
   uint32_t uiRandomRun;

   if(bFirst)
   {
      bFirst = 0;
      /* call srand() prior to rand() to seed the random number generator.
       * This ensures that the randomized numbers won't repeat every device reset */
      srand(Sys_GetTickCount(enTickCount_100ms));
   }

   if( ( GetSRU_Deployment() == enSRU_DEPLOYMENT__MR )
    && ( Param_ReadValue_1Bit( enPARAM1__EnableRandomCarCallRuns ) ) )
   {
      if(uwDelay >= (Param_ReadValue_8Bit(enPARAM8__DEBUG_TestRunsDwellTime_s)*33.3)) // Scale for 30ms runtime
      {
         /* Generate a semi random number */
         uiRandomRun = rand();

         uint8_t ucLanding = uiRandomRun % GetFP_NumFloors();
         en_hc_dir eDir = ( uiRandomRun / GetFP_NumFloors() ) % 2;
         if(GetFloorOpening_Front(GetGroupMapping(ucLanding)))
         {
            uwDelay = 0;
            MasterCommand_SetHallCall( ucLanding, eDir, uiMask);
         }
         else if(GetFloorOpening_Rear(GetGroupMapping(ucLanding)))
         {
            uwDelay = 0;
            MasterCommand_SetHallCall( ucLanding, eDir, uiMask);
         }
      }
      else
      {
         uwDelay++;
      }
   }
}
/*-----------------------------------------------------------------------------

|01234567890123456789|

|Land   Dir Mask     |
|96     UP xFFFFFFFF |
| *                  |
|Latched:  xFFFFFFFF |

-----------------------------------------------------------------------------*/
static void UpdateScreen( void )
{
   LCD_Char_Clear();
   /* Line 1 */
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteString( "Land   Dir Mask     " );

   /* Line 2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteInteger_ExactLength(ucLanding+1, 2);
   LCD_Char_GotoXY( 7, 1 );
   if( eDir == HC_DIR__UP )
   {
      LCD_Char_WriteString("UP");
   }
   else
   {
      LCD_Char_WriteString("DN");
   }
   LCD_Char_GotoXY( 10, 1 );
   LCD_Char_WriteString("x");
   LCD_Char_WriteHex_ExactLength(uiMask, 8);

   /* Line 3 */
   switch( ucCursorX )
   {
      case 0:
      case 1:
         LCD_Char_GotoXY( ucCursorX, 2 );
         break;
      case 2:
         LCD_Char_GotoXY( 7, 2 );
         break;
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
         LCD_Char_GotoXY( (ucCursorX-3)+11, 2 );
         break;
      default:
         break;
   }
   LCD_Char_WriteString("*");

   /* Line 4 */
   uint32_t uiLatchedMask = GetRawLatchedHallCalls( ucLanding, eDir );
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("Latched:");

   LCD_Char_GotoXY( 10, 3 );
   LCD_Char_WriteString("x");
   LCD_Char_WriteHex_ExactLength(uiLatchedMask, 8);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ControlScreen( void )
{
   uint8_t bSave = 0;
   enum en_keypresses eKeypress = Button_GetKeypress();
   int8_t cEditLanding = ucLanding;
   int8_t cEditDir = eDir;
   int64_t iEditMask = uiMask;

   switch(eKeypress)
   {
      case enKEYPRESS_LEFT:
         if(ucCursorX)
         {
            ucCursorX--;
         }
         else
         {
            PrevScreen();
         }
         break;

      case enKEYPRESS_RIGHT:
         if(ucCursorX < 10)
         {
            ucCursorX++;
         }
         break;

      case enKEYPRESS_UP:
         switch( ucCursorX )
         {
            case 0:
               cEditLanding += 10;
               break;
            case 1:
               cEditLanding += 1;
               break;
            case 2:
               cEditDir = HC_DIR__DOWN;
               break;
            case 3:
               iEditMask += (1 << 28);
               break;
            case 4:
               iEditMask += (1 << 24);
               break;
            case 5:
               iEditMask += (1 << 20);
               break;
            case 6:
               iEditMask += (1 << 16);
               break;
            case 7:
               iEditMask += (1 << 12);
               break;
            case 8:
               iEditMask += (1 << 8);
               break;
            case 9:
               iEditMask += (1 << 4);
               break;
            case 10:
               iEditMask += (1 << 0);
               break;
            default:
               break;
         }
         break;

      case enKEYPRESS_DOWN:
         switch( ucCursorX )
         {
            case 0:
               cEditLanding -= 10;
               break;
            case 1:
               cEditLanding -= 1;
               break;
            case 2:
               cEditDir = HC_DIR__UP;
               break;
            case 3:
               iEditMask -= (1 << 28);
               break;
            case 4:
               iEditMask -= (1 << 24);
               break;
            case 5:
               iEditMask -= (1 << 20);
               break;
            case 6:
               iEditMask -= (1 << 16);
               break;
            case 7:
               iEditMask -= (1 << 12);
               break;
            case 8:
               iEditMask -= (1 << 8);
               break;
            case 9:
               iEditMask -= (1 << 4);
               break;
            case 10:
               iEditMask -= (1 << 0);
               break;
            default:
               break;
         }
         break;

      case enKEYPRESS_ENTER:
         bSave = 1;
         break;

      default:
         break;
   }

   /* Bound values */
   if( iEditMask > 0xFFFFFFFF )
   {
      iEditMask = 0xFFFFFFFF;
   }
   else if( iEditMask < 0 )
   {
      iEditMask = 0;
   }
   uiMask = iEditMask;

   if( cEditLanding > MAX_NUM_FLOORS )
   {
      cEditLanding = MAX_NUM_FLOORS;
   }
   else if( cEditLanding < 0 )
   {
      cEditLanding = 0;
   }
   ucLanding = cEditLanding;

   if( cEditDir > HC_DIR__UP )
   {
      cEditDir = HC_DIR__UP;
   }
   else if( cEditDir < HC_DIR__DOWN )
   {
      cEditDir = HC_DIR__DOWN;
   }
   eDir = cEditDir;

   if(bSave)
   {
      MasterCommand_SetHallCall(ucLanding, eDir, uiMask);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_HC_Latch( void )
{
   ControlScreen();
   UpdateScreen();
   TEST_EnterFloorToFloor();
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
