/******************************************************************************
 *
 * @file     ui_strings_faults.c
 * @brief    Fault string access
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "ui.h"
#include "lcd.h"
#include "mod.h"
#include <stdint.h>
#include <string.h>

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/


static char const * const pasAlarmStrings[ NUM_ALARMS + 1 ] =
{
   ALARM_TABLE(EXPAND_ALARM_TABLE_AS_STRING_ARRAY)
};

static char const * const psInvalidAlarmNumString = "Unknown Alarm";
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void PrintAlarmString( en_alarms eAlarmNum )
{
   if(eAlarmNum < NUM_ALARMS)
   {
      LCD_Char_WriteString( pasAlarmStrings[ eAlarmNum ]);
   }
   else
   {
      LCD_Char_WriteString( psInvalidAlarmNumString );
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

