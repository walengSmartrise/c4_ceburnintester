
/******************************************************************************
 *
 * @file     ui_menu_io.c
 * @brief    I/O Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "GlobalData.h"
#include "carDestination.h"
#include "carData.h"
#include "XRegData.h"
#include "XRegDestination.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_XRegDestination( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_XRegDestination =
{
   .pfnDraw = UI_FFS_XRegDestination,
};


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_XRegDestination =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_XRegDestination,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void Print_Screen( uint8_t ucAddress )
{
   LCD_Char_Clear();

   /* Car */
   LCD_Char_GotoXY( 0, 0 );
   uint8_t ucCarNumber = ucAddress+1;
   LCD_Char_WriteString("DESTINATION CAR");
   LCD_Char_WriteInteger_ExactLength(ucCarNumber, 1);

   /* Mark destinations that have been accepted */
   LCD_Char_WriteString(" [");
   if(XRegDestination_GetCallAccepted(ucAddress))
   {
      LCD_Char_WriteString("X]");
   }
   else
   {
      LCD_Char_WriteString(" ]");
   }

   /* Landing */
   LCD_Char_GotoXY( 1, 1 );
   uint8_t ucLanding = XRegDestination_GetLanding(ucAddress)+1;
   LCD_Char_WriteString("Landing: ");
   if(!ucLanding)
   {
      LCD_Char_WriteString("NONE");
   }
   else
   {
      LCD_Char_WriteInteger_MinLength(ucLanding, 1);
   }

   /* Call Type */
   LCD_Char_GotoXY( 1, 2 );
   enum en_doors eDoor = XRegDestination_GetDoor(ucAddress);
   enum direction_enum eDirection = XRegDestination_GetDirection(ucAddress);
   LCD_Char_WriteString("Type:    ");
   if(ucLanding)
   {
         if( eDirection == DIR__UP )
         {
            LCD_Char_WriteString("DIR UP - ");
         }
         else if( eDirection == DIR__DN )
         {
            LCD_Char_WriteString("DIR DN - ");
         }
         else
         {
            LCD_Char_WriteString("CC - ");
         }
         if( eDoor  == DOOR_FRONT )
         {
            LCD_Char_WriteString("FRONT");
         }
         else
         {
            LCD_Char_WriteString("REAR");
         }
   }
   else
   {
      LCD_Char_WriteString("N/A");
   }



   /* Mask */
   LCD_Char_GotoXY( 1, 3 );
   uint32_t uiMask = XRegDestination_GetMask(ucAddress);
   LCD_Char_WriteString("Mask:    0x");
   LCD_Char_WriteHex_ExactLength(uiMask, 8);
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_XRegDestination( void )
{
   static int8_t cAddress;

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      cAddress++;
   }
   else if ( enKeypress == enKEYPRESS_DOWN )
   {
      cAddress--;
   }

   /* Bound address */
   if( cAddress >= MAX_GROUP_CARS )
   {
      cAddress = MAX_GROUP_CARS-1;
   }
   else if( cAddress < 0 )
   {
      cAddress = 0;
   }

   Print_Screen( cAddress );
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/


