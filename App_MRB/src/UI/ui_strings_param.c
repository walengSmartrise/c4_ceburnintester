/******************************************************************************
 *
 * @file     ui.c
 * @brief    User Interface
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "lcd.h"
#include "mod.h"
#include <stdint.h>
#include <string.h>

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static char const* const gpasStrings_1Bit[NUM_1BIT_PARAMS]=
{
   PARAM_STRINGS_1BIT(EXPAND_PARAM_STRING_TABLE_1BIT_AS_ARRAY)
};

static char const * const gpasStrings_8Bit[NUM_8BIT_PARAMS] =
{
   PARAM_STRINGS_8BIT(EXPAND_PARAM_STRING_TABLE_8BIT_AS_ARRAY)
};

static char const * const gpasStrings_16Bit[NUM_16BIT_PARAMS] =
{
   PARAM_STRINGS_16BIT(EXPAND_PARAM_STRING_TABLE_16BIT_AS_ARRAY)
};

static char const * const gpasStrings_24Bit[NUM_24BIT_PARAMS] =
{
   PARAM_STRINGS_24BIT(EXPAND_PARAM_STRING_TABLE_24BIT_AS_ARRAY)
};

static char const * const gpasStrings_32Bit[NUM_32BIT_PARAMS] =
{
   PARAM_STRINGS_32BIT(EXPAND_PARAM_STRING_TABLE_32BIT_AS_ARRAY)
};
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint8_t Param_PrintParamString_1Bit( enum en_1bit_params enParamNum )
{
   uint8_t bReturn = 0;
   if( enParamNum < NUM_1BIT_PARAMS )
   {
      PrintMovingText(gpasStrings_1Bit[enParamNum]);
      bReturn = 1;
   }
   else
   {
      LCD_Char_WriteString("(NA)");
   }

   return bReturn;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint8_t Param_PrintParamString_8Bit( enum en_8bit_params enParamNum )
{
   uint8_t bReturn = 0;
   if( enParamNum < NUM_8BIT_PARAMS )
   {
      PrintMovingText(gpasStrings_8Bit[enParamNum]);
      bReturn = 1;
   }
   else
   {
      LCD_Char_WriteString("(NA)");
   }

   return bReturn;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint8_t Param_PrintParamString_16Bit( enum en_16bit_params enParamNum )
{
   uint8_t bReturn = 0;
   if( enParamNum < NUM_16BIT_PARAMS )
   {
      PrintMovingText(gpasStrings_16Bit[enParamNum]);
      bReturn = 1;
   }
   else
   {
      LCD_Char_WriteString("(NA)");
   }

   return bReturn;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint8_t Param_PrintParamString_24Bit( enum en_24bit_params enParamNum )
{
   uint8_t bReturn = 0;
   if( enParamNum < NUM_24BIT_PARAMS )
   {
      PrintMovingText(gpasStrings_24Bit[enParamNum]);
      bReturn = 1;
   }
   else
   {
      LCD_Char_WriteString("(NA)");
   }

   return bReturn;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint8_t Param_PrintParamString_32Bit( enum en_32bit_params enParamNum )
{
    uint8_t bReturn = 0;
    if( enParamNum < NUM_32BIT_PARAMS )
    {
       PrintMovingText(gpasStrings_32Bit[enParamNum]);
       bReturn = 1;
    }
    else
    {
       LCD_Char_WriteString("(NA)");
    }

    return bReturn;
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/


