/******************************************************************************
 *
 * @file     ui_menu_setup.c
 * @brief    Setup Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "mod.h"
#include "sru.h"
#include <string.h>
#include <math.h>
#include "lcd.h"
#include "acceptance.h"
#include "operation.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_Debug_AcceptanceTest( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_ui_screen__freeform gstFFS_Debug_AcceptanceTest =
{
   .pfnDraw = UI_FFS_Debug_AcceptanceTest,
};


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

//----------------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Debug_AcceptanceTest =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Debug_AcceptanceTest,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define START_CURSOR_POS_VALUE (2U)
#define START_CURSOR_POS_SAVE  (19U)

#define TEST_RESET_DEBOUNCE (256)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

static char const * const psAccptStrings[NUM_ACCEPTANCE_TEST] =
{
   "Inactive",//NO_ACTIVE_ACCEPTANCE_TEST
   "NTS",//ACCEPTANCE_TEST_NTS
   "ETS",//ACCEPTANCE_TEST_ETS
   "ETSL",//ACCEPTANCE_TEST_ETSL
   "TOP LIMIT",//ACCEPTANCE_TEST_TOP_LIMIT

   "BOTTOM LIMIT",//ACCEPTANCE_TEST_BOTTOM_LIMIT
   "REDUNDANCY CPLD",//ACCEPTANCE_TEST_REDUNDANCY_CPLD
   "REDUNDANCY MCU",//ACCEPTANCE_TEST_REDUNDANCY_MCU
   "ASC/DESC OVERSPEED",//ACCEPTANCE_TEST_ASC_OR_DESC_OVERSPEED
   "INSPECTION SPEED",//ACCEPTANCE_TEST_INSPECTION_SPEED

   "LEVELING SPEED",//ACCEPTANCE_TEST_LEVELING_SPEED
   "BOTTOM FINAL",//ACCEPTANCE_TEST_BOTTOM_FINAL_LIMIT
   "TOP FINAL",//ACCEPTANCE_TEST_TOP_FINAL_LIMIT
   "COUNTER BUFFER",//ACCEPTANCE_TEST_COUNTERWEIGHT_BUFFER
   "CAR BUFFER",//ACCEPTANCE_TEST_CAR_BUFFER

   "UNINT MOVT F",//ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_FRONT
   "UNINT MOVT R",//ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_REAR
   "BRK BRD FEEDBACK",//ACCEPTANCE_TEST_BRAKEBOARD_FEEDBACK
   "BRK SLIDE DIST",//ACCEPTANCE_TEST_SLIDE_DISTANCE
   "EBRK SLIDE DIST"//ACCEPTANCE_TEST_EBRAKE_SLIDE
};
static char const * const gpasAcceptanceStatus[NUM_ACCEPTANCE_TEST_STATUS] =
{
    "Idle"   ,//ACCEPTANCE_TEST_NOT_RUN,
    "Complete"   ,//ACCEPTANCE_TEST_PASS,
    "Incomplete"   ,//ACCEPTANCE_TEST_FAIL,
    "Running",//ACCEPTANCE_TEST_IN_PROGRESS
};
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
void SetAcceptanceTestCompleted()
{
   SetAcceptanceTest_Plus1(NO_ACTIVE_ACCEPTANCE_TEST);
}



static void PrintAcceptanceState_NTS( void )
{
    switch (GetDebugStates_AcceptanceTestState_MRA())
    {
        case ACCEPTANCE_NTS_IDLE:
            LCD_Char_WriteString("Idle");
            break;

        case ACCEPTANCE_NTS_IN_DZ:
            LCD_Char_WriteString("Checking if in DZ");
            break;

        case ACCEPTANCE_NTS_SELECT_TYPE:
            PrintMovingText("Press UP for UNTS, DN for DNTS");
            break;

        case ACCEPTANCE_NTS_PARAMETER_SETUP_UNTS:
            LCD_Char_WriteString("Setting up Parameters");
            break;

        case ACCEPTANCE_NTS_PARAMETER_SETUP_DNTS:
            LCD_Char_WriteString("Setting Up Parameters");
            break;

        case ACCEPTANCE_NTS_TEST_RUNNING_UNTS:
            LCD_Char_WriteString("Performing Test");
            break;

        case ACCEPTANCE_NTS_TEST_RUNNING_DNTS:
            LCD_Char_WriteString("Performing Test");
            break;
        case ACCEPTANCE_NTS_MAYBE_PASS_UNTS:
        case ACCEPTANCE_NTS_MAYBE_PASS_DNTS:
            LCD_Char_WriteString("Performing Test");
            break;

        case ACCEPTANCE_NTS_PASS:
            LCD_Char_WriteString("Complete");
            break;

        case ACCEPTANCE_NTS_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;

        case ACCEPTANCE_NTS_PARAMETER_RESTORE:
            LCD_Char_WriteString("Restoring Params");
            break;
        default:
            //Print nothing
            break;
    }
}

void PrintAcceptanceState_UnintendedMovementFront( void )
{
    switch (GetDebugStates_AcceptanceTestState_MRA())
    {
        case ACCEPTANCE_UNINTENDED_MOVEMENT_FRONT_IN_DZ:
            LCD_Char_WriteString("Checking Front DZ");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_FRONT_PARAMETER_SETUP:
            LCD_Char_WriteString("Parameter Setup");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_FRONT_ENABLE_DIP_SWITCH:
            LCD_Char_WriteString("Turn on Dip B8");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_FRONT_OPEN_DOORS:
            LCD_Char_WriteString("Opening Doors");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_FRONT_USER_PRESS_UP:
            LCD_Char_WriteString("Press Up");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_FRONT_PICK_B2:
        case ACCEPTANCE_UNINTENDED_MOVEMENT_FRONT_PICK_SECONDARY:
            LCD_Char_WriteString("Picking 2nd");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_FRONT_PICK_M:
        case ACCEPTANCE_UNINTENDED_MOVEMENT_FRONT_PICK_PRIMARY:
            LCD_Char_WriteString("Pick Primary");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_FRONT_TEST_RUNNING:
            LCD_Char_WriteString("Test Running");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_FRONT_PASS:
            LCD_Char_WriteString("Complete");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_FRONT_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_FRONT_PARAMETER_RESTORE:
            LCD_Char_WriteString("Parameter Restore");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_FRONT_DISABLE_DIP_SWITCH:
            LCD_Char_WriteString("Turn off Dip B8");
            break;
        default:
            // Print nothing
            break;
    }
}
void PrintAcceptanceState_UnintendedMovementRear( void )
{
    switch (GetDebugStates_AcceptanceTestState_MRA())
    {
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_IN_DZ:
            LCD_Char_WriteString("Checking Rear DZ");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PARAMETER_SETUP:
            LCD_Char_WriteString("Parameter Setup");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_ENABLE_DIP_SWITCH:
            LCD_Char_WriteString("Turn on Dip B8");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_OPEN_DOORS:
            LCD_Char_WriteString("Opening Doors");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_USER_PRESS_UP:
            LCD_Char_WriteString("Press Up");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PICK_B2:
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PICK_SECONDARY:
            LCD_Char_WriteString("Picking 2nd");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PICK_M:
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PICK_PRIMARY:
            LCD_Char_WriteString("Pick Primary");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_TEST_RUNNING:
            LCD_Char_WriteString("Test Running");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PASS:
            LCD_Char_WriteString("Complete");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PARAMETER_RESTORE:
            LCD_Char_WriteString("Parameter Restore");
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_DISABLE_DIP_SWITCH:
            LCD_Char_WriteString("Turn off Dip B8");
            break;
        default:
            // Print nothing
            break;
    }
}
void PrintAcceptanceState_TopLimit( void )
{
    switch (GetDebugStates_AcceptanceTestState_MRA())
    {
        case ACCEPTANCE_TOP_LIMIT_MOVE_TO_TOP:
            LCD_Char_WriteString("Moving To Top");
            break;
        case ACCEPTANCE_TOP_LIMIT_PARAMETER_SETUP:
            LCD_Char_WriteString("Parameter Setup");
            break;
        case ACCEPTANCE_TOP_LIMIT_USER_PRESS_UP:
            LCD_Char_WriteString("Press & Hold Up");
            break;
        case ACCEPTANCE_TOP_LIMIT_TEST_RUNNING:
            LCD_Char_WriteString("Performing Test");
            break;
        case ACCEPTANCE_TOP_LIMIT_PASS:
            LCD_Char_WriteString("Complete");
            break;
        case ACCEPTANCE_TOP_LIMIT_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;
        case ACCEPTANCE_TOP_LIMIT_PARAMETER_RESTORE:
            LCD_Char_WriteString("Parameter Restore");
            break;
        default:
            // Print nothing
            break;
    }
}

void PrintAcceptanceState_TopFinalLimit( void )
{
    switch (GetDebugStates_AcceptanceTestState_MRA())
    {
        case ACCEPTANCE_TOP_FINAL_LIMIT_MOVE_TO_TOP:
            LCD_Char_WriteString("Moving To Top");
            break;
        case ACCEPTANCE_TOP_FINAL_LIMIT_PARAMETER_SETUP:
            LCD_Char_WriteString("Bypassing Term Limits");
            break;
        case ACCEPTANCE_TOP_FINAL_LIMIT_USER_PRESS_UP:
            LCD_Char_WriteString("Press & Hold Up");
            break;
        case ACCEPTANCE_TOP_FINAL_LIMIT_TEST_RUNNING:
            LCD_Char_WriteString("Performing Test");
            break;
        case ACCEPTANCE_TOP_FINAL_LIMIT_PASS:
            LCD_Char_WriteString("Complete");
            break;
        case ACCEPTANCE_TOP_FINAL_LIMIT_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;
        case ACCEPTANCE_TOP_FINAL_LIMIT_PARAMETER_RESTORE:
            LCD_Char_WriteString("Parameter Restore");
            break;
        default:
            // Print nothing
            break;
    }
}

void PrintAcceptanceState_RedundancyMCU( void )
{
    switch (GetDebugStates_AcceptanceTestState_MRA())
    {
        case ACCEPTANCE_REDUNDANCY_MCU_TEST_RUNNING:
            LCD_Char_WriteString("Performing Test");
            break;
        case ACCEPTANCE_REDUNDANCY_MCU_PASS:
            LCD_Char_WriteString("Complete");
            break;
        case ACCEPTANCE_REDUNDANCY_MCU_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;
        default:
            // Print nothing
            break;
    }
}

void PrintAcceptanceState_RedundancyCPLD( void )
{
    switch (GetDebugStates_AcceptanceTestState_MRA())
    {
        case ACCEPTANCE_REDUNDANCY_CPLD_TEST_RUNNING:
            LCD_Char_WriteString("Performing Test");
            break;
        case ACCEPTANCE_REDUNDANCY_CPLD_PASS:
            LCD_Char_WriteString("Complete");
            break;
        case ACCEPTANCE_REDUNDANCY_CPLD_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;
        default:
            // Print nothing
            break;
    }
}

void PrintAcceptanceState_LevelingSpeed( void )
{
    switch (GetDebugStates_AcceptanceTestState_MRA())
    {
        case ACCEPTANCE_LEVELING_SPEED_PARAMETER_SETUP:
            LCD_Char_WriteString("Setting Speed 150%");
            break;
        case ACCEPTANCE_LEVELING_SPEED_TEST_RUNNING:
            LCD_Char_WriteString("Performing Test");
            break;
        case ACCEPTANCE_LEVELING_SPEED_PASS:
            LCD_Char_WriteString("Complete");
            break;
        case ACCEPTANCE_LEVELING_SPEED_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;
        case ACCEPTANCE_LEVELING_SPEED_PARAMETER_RESTORE:
            LCD_Char_WriteString("Restoring Speed");
            break;
        default:
            // Print nothing
            break;
    }
}

void PrintAcceptanceState_InspectionSpeed( void )
{
    switch (GetDebugStates_AcceptanceTestState_MRA())
    {
        case ACCEPTANCE_INSPECTION_SPEED_PARAMETER_SETUP:
            LCD_Char_WriteString("Setting Speed 150%");
            break;
        case ACCEPTANCE_INSPECTION_SPEED_TEST_RUNNING:
            LCD_Char_WriteString("Performing Test");
            break;
        case ACCEPTANCE_INSPECTION_SPEED_PASS:
            LCD_Char_WriteString("Complete");
            break;
        case ACCEPTANCE_INSPECTION_SPEED_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;
        case ACCEPTANCE_INSPECTION_SPEED_PARAMETER_RESTORE:
            LCD_Char_WriteString("Restoring Speed");
            break;
        default:
            // Print nothing
            break;
    }
}

void PrintAcceptanceState_AscOrDescOverspeed( void )
{
    switch (GetDebugStates_AcceptanceTestState_MRA())
    {
        case ACCEPTANCE_ASC_OR_DESC_OVERSPEED_IN_DZ:
            LCD_Char_WriteString("In Door Zone Check");
            break;
        case     ACCEPTANCE_ASC_OR_DESC_OVERSPEED_PARAMETER_SETUP:
            LCD_Char_WriteString("Parameter Setup");
            break;
        case ACCEPTANCE_ASC_OR_DESC_OVERSPEED_WAIT_FOR_USER:
            PrintMovingText("Adjust Drive. Press & Hold Up Or Dn");
            break;
        case ACCEPTANCE_ASC_OR_DESC_OVERSPEED_TEST_RUNNING:
            LCD_Char_WriteString("Performing Test");
            break;
        case ACCEPTANCE_ASC_OR_DESC_OVERSPEED_PASS:
            LCD_Char_WriteString("Complete");
            break;
        case ACCEPTANCE_ASC_OR_DESC_OVERSPEED_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;
        case ACCEPTANCE_ASC_OR_DESC_OVERSPEED_PARAMETER_RESTORE:
            LCD_Char_WriteString("Parameter Restore");
            break;
        default:
            // Print nothing
            break;
    }
}


void PrintAcceptanceState_ETS( void )
{
    switch (GetDebugStates_AcceptanceTestState_MRA())
    {

        case ACCEPTANCE_ETS_IN_DZ:
            LCD_Char_WriteString("In Door Zone Check");
            break;
        case ACCEPTANCE_ETS_SELECT_TYPE:
            PrintMovingText("Press UP for UETS, DN for DETS");
            break;
        case ACCEPTANCE_ETS_PARAMETER_SETUP_UETS:
        case ACCEPTANCE_ETS_PARAMETER_SETUP_DETS:
            LCD_Char_WriteString("Parameter Setup");
            break;
        case ACCEPTANCE_ETS_WAIT_FOR_USER_UETS:
            LCD_Char_WriteString("Press & Hold Up");
            break;
        case ACCEPTANCE_ETS_WAIT_FOR_USER_DETS:
            LCD_Char_WriteString("Press & Hold Down");
            break;
        case ACCEPTANCE_ETS_TEST_RUNNING_UETS:
        case ACCEPTANCE_ETS_TEST_RUNNING_DETS:
            LCD_Char_WriteString("Performing Test");
            break;
        case ACCEPTANCE_ETS_PASS:
            LCD_Char_WriteString("Complete");
            break;
        case ACCEPTANCE_ETS_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;
        case ACCEPTANCE_ETS_PARAMETER_RESTORE:
            LCD_Char_WriteString("Parameter Restore");
            break;
        default:
            // Print nothing
            break;
    }
}

void PrintAcceptanceState_ETSL( void )
{
    switch (GetDebugStates_AcceptanceTestState_MRA())
    {

        case ACCEPTANCE_ETSL_IN_DZ:
            LCD_Char_WriteString("In Door Zone Check");
            break;
        case ACCEPTANCE_ETSL_SELECT_TYPE:
            PrintMovingText("Press UP for UETSL or DN for DETSL Test");
            break;
        case ACCEPTANCE_ETSL_PARAMETER_SETUP_UETSL:
        case ACCEPTANCE_ETSL_PARAMETER_SETUP_DETSL:
            LCD_Char_WriteString("Parameter Setup");
            break;
        case ACCEPTANCE_ETSL_WAIT_FOR_USER_UETSL:
        case ACCEPTANCE_ETSL_WAIT_FOR_USER_DETSL:
            PrintMovingText("Press & Hold Enable + Up/Down");
            break;
        case ACCEPTANCE_ETSL_TEST_RUNNING_UETSL:
        case ACCEPTANCE_ETSL_TEST_RUNNING_DETSL:
            LCD_Char_WriteString("Performing Test");
            break;
        case ACCEPTANCE_ETSL_PASS:
            LCD_Char_WriteString("Complete");
            break;
        case ACCEPTANCE_ETSL_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;
        case ACCEPTANCE_ETSL_PARAMETER_RESTORE:
            LCD_Char_WriteString("Parameter Restore");
            break;
        default:
            // Print nothing
            break;
    }
}

void PrintAcceptanceState_CounterweightBuffer( void )
{
    switch (GetDebugStates_AcceptanceTestState_MRA())
    {
        case ACCEPTANCE_COUNTERWEIGHT_BUFFER_IN_DZ:
            LCD_Char_WriteString("In Door Zone Check");
            break;
        case ACCEPTANCE_COUNTERWEIGHT_BUFFER_PARAMETER_SETUP:
            LCD_Char_WriteString("Parameter Setup");
            break;
        case ACCEPTANCE_COUNTERWEIGHT_BUFFER_USER_PRESS_UP:
            LCD_Char_WriteString("Press & Hold Up");
            break;
        case ACCEPTANCE_COUNTERWEIGHT_BUFFER_TEST_RUNNING:
            LCD_Char_WriteString("Performing Test");
            break;
        case ACCEPTANCE_COUNTERWEIGHT_BUFFER_PASS:
            LCD_Char_WriteString("Complete");
            break;
        case ACCEPTANCE_COUNTERWEIGHT_BUFFER_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;
        case ACCEPTANCE_COUNTERWEIGHT_BUFFER_PARAMETER_RESTORE:
            LCD_Char_WriteString("Parameter restore");
            break;
        default:
            // Print nothing
            break;
    }
}

void PrintAcceptanceState_CarBuffer( void )
{
    switch (GetDebugStates_AcceptanceTestState_MRA())
    {
        case ACCEPTANCE_CAR_BUFFER_IN_DZ:
            LCD_Char_WriteString("In Door Zone Check");
            break;
        case ACCEPTANCE_CAR_BUFFER_PARAMETER_SETUP:
            LCD_Char_WriteString("Parameter Setup");
            break;
        case ACCEPTANCE_CAR_BUFFER_USER_PRESS_DOWN:
            LCD_Char_WriteString("Press & Hold Down");
            LCD_Char_GotoXY(1,3);
            PrintMovingText("Ensure car is not at bottom");

            break;
        case ACCEPTANCE_CAR_BUFFER_TEST_RUNNING:
            LCD_Char_WriteString("Performing Test");
            break;
        case ACCEPTANCE_CAR_BUFFER_PASS:
            LCD_Char_WriteString("Complete");
            break;
        case ACCEPTANCE_CAR_BUFFER_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;
        case ACCEPTANCE_CAR_BUFFER_PARAMETER_RESTORE:
            LCD_Char_WriteString("Parameter Restore");
            break;
        default:
            // Print nothing
            break;
    }
}

void PrintAcceptanceState_BottomLimit( void )
{
    switch (GetDebugStates_AcceptanceTestState_MRA())
    {
        case ACCEPTANCE_BOTTOM_LIMIT_MOVE_TO_BOTTOM:
            LCD_Char_WriteString("Moving To Bottom");
            break;
        case ACCEPTANCE_BOTTOM_LIMIT_PARAMETER_SETUP:
            LCD_Char_WriteString("Parameter Setup");
            break;
        case ACCEPTANCE_BOTTOM_LIMIT_USER_PRESS_DN:
            LCD_Char_WriteString("Press & Hold Down");
            break;
        case ACCEPTANCE_BOTTOM_LIMIT_TEST_RUNNING:
            LCD_Char_WriteString("Performing Test");
            break;
        case ACCEPTANCE_BOTTOM_LIMIT_PASS:
            LCD_Char_WriteString("Complete");
            break;
        case ACCEPTANCE_BOTTOM_LIMIT_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;
        case ACCEPTANCE_BOTTOM_LIMIT_PARAMETER_RESTORE:
            LCD_Char_WriteString("Parameter Restore");
            break;
        default:
            // Print nothing
            break;
    }
}

void PrintAcceptanceState_BottomFinalLimit( void )
{
    switch (GetDebugStates_AcceptanceTestState_MRA())
    {
        case ACCEPTANCE_BOTTOM_FINAL_LIMIT_MOVE_TO_BOTTOM:
            LCD_Char_WriteString("Moving To Bottom");
            break;
        case ACCEPTANCE_BOTTOM_FINAL_LIMIT_PARAMETER_SETUP:
            LCD_Char_WriteString("Bypassing Term Limits");
            break;
        case ACCEPTANCE_BOTTOM_FINAL_LIMIT_USER_PRESS_DOWN:
            LCD_Char_WriteString("Press & Hold Down");
            break;
        case ACCEPTANCE_BOTTOM_FINAL_LIMIT_TEST_RUNNING:
            LCD_Char_WriteString("Performing Test");
            break;
        case ACCEPTANCE_BOTTOM_FINAL_LIMIT_PASS:
            LCD_Char_WriteString("Complete");
            break;
        case ACCEPTANCE_BOTTOM_FINAL_LIMIT_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;
        case ACCEPTANCE_BOTTOM_LIMIT_PARAMETER_RESTORE:
            LCD_Char_WriteString("Parameter Restore");
            break;
        default:
            // Print nothing
            break;
    }
}

void PrintAcceptanceState_BrakeBoardFeedback( void )
{
    switch( GetDebugStates_AcceptanceTestState_MRA() )
    {
        case ACCEPTANCE_BRAKEBOARD_WAIT_FOR_USER:
            LCD_Char_WriteString("Press & Hold Up");
            break;
        case ACCEPTANCE_BRAKEBOARD_PRESS_CONTACTOR:
            LCD_Char_WriteString("Hold B Contactor");
            break;
        case ACCEPTANCE_BRAKEBOARD_PICK_BRAKE:
            LCD_Char_WriteString("Picking Brakes");
            break;
        case ACCEPTANCE_BRAKEBOARD_HOLD_MAIN_BRAKE:
            LCD_Char_WriteString("Checking Brake Voltage");
            LCD_Char_GotoXY(8,3);
            LCD_Char_WriteInteger(GetBrakeData_VoltageFeedback());
            LCD_Char_WriteString(" VDC");
            break;
        case ACCEPTANCE_BRAKEBOARD_HOLD_SEC_BRAKE:
            LCD_Char_WriteString("Checking Brake Voltage");
            LCD_Char_GotoXY(8,3);
            LCD_Char_WriteInteger(GetEBrakeData_VoltageFeedback());
            LCD_Char_WriteString(" VDC");
            break;
        case ACCEPTANCE_BRAKEBOARD_PASS:
            LCD_Char_WriteString("Pass");
            break;
        case ACCEPTANCE_BRAKEBOARD_COMPLETE:
            LCD_Char_WriteString("Complete");
            break;
        case ACCEPTANCE_BRAKEBOARD_INCOMPLETE:
        case ACCEPTANCE_BRAKEBOARD_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;
        default:
            break;
    }

}

void PrintAcceptanceState_SlideDistance( void )
{
    switch( GetDebugStates_AcceptanceTestState_MRA() )
    {
       case ACCEPTANCE_SLIDE_CHOOSE_DIR:
            PrintMovingText("UP for Slide Up; DN for Slide Down");
            break;
       case ACCEPTANCE_SLIDE_MOVE_TO_TOP:
            LCD_Char_WriteString("Moving to Top");
            break;
        case ACCEPTANCE_SLIDE_MOVE_TO_BOTTOM:
            LCD_Char_WriteString("Moving to Bot");
            break;
        case ACCEPTANCE_SLIDE_WAIT_FOR_USER:
            PrintMovingText("Press & Hold Enable+Direction");
            break;
        case ACCEPTANCE_SLIDE_GET_TO_SPEED:
            LCD_Char_WriteString("Accel to Contract Speed");
            break;
        case ACCEPTANCE_SLIDE_TRIP_EBRAKE:
            LCD_Char_WriteString("Trigger ESTOP");
            break;
        case ACCEPTANCE_SLIDE_MEASURE_SLIDE:
            LCD_Char_WriteString("Measuring");
            break;
        case ACCEPTANCE_SLIDE_PASS:
        case ACCEPTANCE_SLIDE_COMPLETE:
            LCD_Char_WriteString("Slide ");
            float flDistance = (float) Param_ReadValue_16Bit(enPARAM16__Acceptance_SlideDistance) / 50.8;

            uint32_t uiFeet  = (uint32_t)( flDistance / 12 );

          flDistance -= 12 * uiFeet;

          uint32_t uiInches = (uint32_t)( flDistance );

          flDistance -= uiInches;

            LCD_Char_WriteInteger(uiFeet);
            LCD_Char_WriteString("'");
            LCD_Char_WriteInteger(uiInches);
            LCD_Char_WriteString("\"");
            break;
        case ACCEPTANCE_SLIDE_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;
        default:
            break;
    }
}

void PrintAcceptanceState_EBrkSlide( void )
{
    switch( GetDebugStates_AcceptanceTestState_MRA() )
    {
        case ACCEPTANCE_ESLIDE_CHOOSE_DIR:
            PrintMovingText("UP for Slide Up; DN for Slide Down");
            break;
        case ACCEPTANCE_ESLIDE_MOVE_TO_TOP:
            LCD_Char_WriteString("Moving to Top");
            break;
        case ACCEPTANCE_ESLIDE_MOVE_TO_BOTTOM:
            LCD_Char_WriteString("Moving to Bottom");
            break;
        case ACCEPTANCE_ESLIDE_WAIT_FOR_USER:
           PrintMovingText("Press & Hold Enable+Direction");
            break;
        case ACCEPTANCE_ESLIDE_GET_TO_SPEED:
            LCD_Char_WriteString("Accel to Contract Speed");
            break;
        case ACCEPTANCE_ESLIDE_TRIP_EBRAKE:
            LCD_Char_WriteString("Trigger ESTOP");
            break;
        case ACCEPTANCE_ESLIDE_MEASURE_SLIDE:
            LCD_Char_WriteString("Measuring");
            break;
        case ACCEPTANCE_ESLIDE_PASS:
        case ACCEPTANCE_ESLIDE_COMPLETE:
            LCD_Char_WriteString("Slide ");
            float flDistance = (float) Param_ReadValue_16Bit(enPARAM16__Acceptance_EBrk_SlideDistance) / 50.8;

            uint32_t uiFeet  = (uint32_t)( flDistance / 12 );

          flDistance -= 12 * uiFeet;

          uint32_t uiInches = (uint32_t)( flDistance );

          flDistance -= uiInches;

            LCD_Char_WriteInteger(uiFeet);
            LCD_Char_WriteString("'");
            LCD_Char_WriteInteger(uiInches);
            LCD_Char_WriteString("\"");
            break;
        case ACCEPTANCE_ESLIDE_FAIL:
            LCD_Char_WriteString("Incomplete");
            break;
        default:
            break;
    }
}
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void Screen_Default(struct st_runtime_signal_edit_menu * pstEditMenu)
{
   LCD_Char_Clear();

   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteString(pstEditMenu->psTitle);

   //------------------------------------------------------------------
   if(pstEditMenu->bSaving)
   {
      LCD_Char_GotoXY(START_CURSOR_POS_VALUE,2);
      LCD_Char_WriteStringUpper("Saving");
   }
   else if(pstEditMenu->bInvalid)
   {
      LCD_Char_GotoXY(START_CURSOR_POS_VALUE,2);
      LCD_Char_WriteStringUpper("Invalid");
   }
   // Test is not active
   else if(GetDebugStates_AcceptanceTestStatus_MRA() != ACCEPTANCE_TEST_IN_PROGRESS)
   {
       // If test is not active in progress then display what its doing.
       // Other wise display the status of the test.
       LCD_Char_GotoXY(1, 1);
       LCD_Char_WriteString(gpasAcceptanceStatus[GetDebugStates_AcceptanceTestStatus_MRA()]);

       //------------------------------------------------------------------
       LCD_Char_GotoXY(2, 2);
       if(pstEditMenu->ulValue_Edited <= pstEditMenu->ulValue_Limit)
       {
           LCD_Char_WriteString(psAccptStrings[pstEditMenu->ulValue_Edited]);
       }

       //------------------------------------------------------------------
       LCD_Char_GotoXY(pstEditMenu->ucCursorColumn, 3);
       LCD_Char_WriteString("*");
       //------------------------------------------------------------------
       if (pstEditMenu->ulValue_Edited != pstEditMenu->ulValue_Saved)
       {
           LCD_Char_GotoXY( 16, 1 );
           LCD_Char_WriteString( "Save" );

           LCD_Char_GotoXY( 19, 2 );
           LCD_Char_WriteChar( '|' );
       }
   }
   // Test is active
   else
   {
       // If test is not active in progress then display what its doing.
       // Other wise display the status of the test.
       LCD_Char_GotoXY(1, 1);
       // The current selected acceptance test
       switch (pstEditMenu->ulValue_Edited)
       {
           case ACCEPTANCE_TEST_NTS:
               PrintAcceptanceState_NTS();
               break;
           case ACCEPTANCE_TEST_ETS:
               PrintAcceptanceState_ETS();
               break;
           case ACCEPTANCE_TEST_ETSL:
              PrintAcceptanceState_ETSL();
              break;
           case ACCEPTANCE_TEST_TOP_LIMIT:
               PrintAcceptanceState_TopLimit();
               break;
           case ACCEPTANCE_TEST_BOTTOM_LIMIT:
               PrintAcceptanceState_BottomLimit();
               break;
           case ACCEPTANCE_TEST_REDUNDANCY_CPLD:
               PrintAcceptanceState_RedundancyCPLD();
               break;
           case ACCEPTANCE_TEST_REDUNDANCY_MCU:
               PrintAcceptanceState_RedundancyMCU();
               break;
           case ACCEPTANCE_TEST_ASC_OR_DESC_OVERSPEED:
               PrintAcceptanceState_AscOrDescOverspeed();
               break;
           case ACCEPTANCE_TEST_INSPECTION_SPEED:
               PrintAcceptanceState_InspectionSpeed();
               break;
           case ACCEPTANCE_TEST_LEVELING_SPEED:
               PrintAcceptanceState_LevelingSpeed();
               break;
           case ACCEPTANCE_TEST_BOTTOM_FINAL_LIMIT:
               PrintAcceptanceState_BottomFinalLimit();
               break;
           case ACCEPTANCE_TEST_TOP_FINAL_LIMIT:
               PrintAcceptanceState_TopFinalLimit();
               break;
           case ACCEPTANCE_TEST_COUNTERWEIGHT_BUFFER:
               PrintAcceptanceState_CounterweightBuffer();
               break;
           case ACCEPTANCE_TEST_CAR_BUFFER:
               PrintAcceptanceState_CarBuffer();
               break;
           case ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_FRONT:
               PrintAcceptanceState_UnintendedMovementFront();
               break;
           case ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_REAR:
               PrintAcceptanceState_UnintendedMovementRear();
               break;
            case ACCEPTANCE_TEST_BRAKEBOARD_FEEDBACK:
                PrintAcceptanceState_BrakeBoardFeedback();
                break;
            case ACCEPTANCE_TEST_SLIDE_DISTANCE:
                PrintAcceptanceState_SlideDistance();
                break;
            case ACCEPTANCE_TEST_EBRAKE_SLIDE:
                PrintAcceptanceState_EBrkSlide();
                break;
           default:
               LCD_Char_WriteString(gpasAcceptanceStatus[GetDebugStates_AcceptanceTestStatus_MRA()]);
               break;
       }

       //------------------------------------------------------------------
       LCD_Char_GotoXY(2, 2);
       if(pstEditMenu->ulValue_Edited <= pstEditMenu->ulValue_Limit)
       {
           LCD_Char_WriteString(psAccptStrings[pstEditMenu->ulValue_Edited]);
       }

   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Value( enum en_keypresses enKeypress,
      enum en_field_action enFieldAction,
      struct st_runtime_signal_edit_menu * pstEditMenu )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex;
   int64_t llCurrentParam_EditedValue;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = pstEditMenu->ucFieldSize_Value - 1;
      }
   }
   else  // enACTION__EDIT
   {
      llCurrentParam_EditedValue = pstEditMenu->ulValue_Edited;
      //------------------------------------------------------------------
      int32_t uiDigitWeight = (pstEditMenu->ucFieldSize_Value - 1) - ucCursorIndex;

      uiDigitWeight = pow( 10, uiDigitWeight );  // 10 to the power of uiDigitWeight

      //------------------------------------------------------------------
      pstEditMenu->ucCursorColumn = START_CURSOR_POS_VALUE + ucCursorIndex;
      //------------------------------------------------------------------
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            llCurrentParam_EditedValue += uiDigitWeight;
            break;
         case enKEYPRESS_DOWN:
          llCurrentParam_EditedValue -= uiDigitWeight;
          break;
         case enKEYPRESS_LEFT:
            if ( ucCursorIndex )
            {
               --ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_LEFT;
            }
            break;

         case enKEYPRESS_RIGHT:
            if ( ucCursorIndex < (pstEditMenu->ucFieldSize_Value - 1) )
            {
               ++ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;

         default:
            break;
      }

      if(llCurrentParam_EditedValue > pstEditMenu->ulValue_Limit)
      {
         llCurrentParam_EditedValue = pstEditMenu->ulValue_Limit;
      }
      else if(llCurrentParam_EditedValue < 0)
      {
         llCurrentParam_EditedValue = pstEditMenu->ulValue_Limit;
      }

     pstEditMenu->ulValue_Edited = (uint32_t) llCurrentParam_EditedValue;
   }

   return enReturnValue;
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void SaveValue( struct st_runtime_signal_edit_menu * pstEditMenu )
{
    pstEditMenu->pfnSetSignal(pstEditMenu->ulValue_Edited);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Save( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_runtime_signal_edit_menu * pstEditMenu )
{
    enum en_field_action enReturnValue = enACTION__NONE;
    static enum en_save_state enSaveState = SAVE_STATE__SAVED;

    pstEditMenu->ucCursorColumn = START_CURSOR_POS_SAVE;
    //------------------------------------------
    if ( enFieldAction == enACTION__EDIT )
    {
        if ( enKeypress == enKEYPRESS_ENTER )
        {
            enSaveState = SAVE_STATE__SAVING;
        }
        else if ( enKeypress == enKEYPRESS_LEFT )
        {
            enSaveState = SAVE_STATE__SAVED;
            enReturnValue = enACTION__EXIT_TO_LEFT;
        }
    }

    //--------------------------------
    switch ( enSaveState )
    {
        case SAVE_STATE__CLEARING:
            pstEditMenu->bSaving = 1;
            if ( !pstEditMenu->ulValue_Saved )
            {
                enSaveState = SAVE_STATE__SAVING;
            }
            break;
        case SAVE_STATE__SAVING:
            SaveValue( pstEditMenu );
            pstEditMenu->bSaving = 1;
            if ( pstEditMenu->ulValue_Saved == pstEditMenu->ulValue_Edited )
            {
                enReturnValue = enACTION__EXIT_TO_LEFT;
                pstEditMenu->bSaving = 0;
                enSaveState = SAVE_STATE__SAVED;
            }
            break;
        case SAVE_STATE__SAVED:

            pstEditMenu->bSaving = 0;
            break;
        default:


            enSaveState = SAVE_STATE__SAVED;
            break;
    }
    return enReturnValue;
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void Edit_RuntimeSignal(struct st_runtime_signal_edit_menu * pstEditMenu)
{
   pstEditMenu->ulValue_Saved = pstEditMenu->pfnGetSignal();
   if ( pstEditMenu->ucFieldIndex == enFIELD__NONE )
   {
      pstEditMenu->ulValue_Edited = pstEditMenu->ulValue_Saved;
      pstEditMenu->bSaving = 0;
      pstEditMenu->bInvalid = 0;
      pstEditMenu->ucFieldIndex = enFIELD__VALUE;
      Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstEditMenu );
   }
   else
   {
      enum en_field_action enFieldAction;

      enum en_keypresses enKeypress = Button_GetKeypress();
      //------------------------------------------------------------------
      switch ( pstEditMenu->ucFieldIndex )
      {
         case enFIELD__VALUE:

            enFieldAction = Edit_Value( enKeypress, enACTION__EDIT, pstEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               PrevScreen();
               pstEditMenu->ucFieldIndex = enFIELD__NONE;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Save( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstEditMenu );

               pstEditMenu->ucFieldIndex = enFIELD__SAVE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__SAVE:

            enFieldAction = Edit_Save( enKeypress, enACTION__EDIT, pstEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT, pstEditMenu );

               pstEditMenu->ucFieldIndex = enFIELD__VALUE;
            }
            break;

         default:
            break;
      }
      Screen_Default(pstEditMenu);
   }
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void Enter_AcceptanceMenu()
{
   static struct st_runtime_signal_edit_menu stMenuEdit;

   stMenuEdit.ulValue_Limit = NUM_ACCEPTANCE_TEST - 1;
   stMenuEdit.psTitle = "Select Acceptance Test";
   stMenuEdit.ucFieldSize_Value = 1;
   stMenuEdit.pfnGetSignal = GetAcceptanceTest_Plus1;
   stMenuEdit.pfnSetSignal = SetAcceptanceTest_Plus1;

   Edit_RuntimeSignal(&stMenuEdit);

   SetUIRequest_AcceptanceTestStatus(stMenuEdit.ulValue_Edited);
}
/*----------------------------------------------------------------------------
   Access only on MRB when in Auto - Acceptance Mode
 *----------------------------------------------------------------------------*/
static void UI_FFS_Debug_AcceptanceTest( void )
{
    //SDA REVIEW: This should check for accpt_tst not _test as the mode of op. Test is not the same thing as accpt test
    if(     ( GetSRU_Deployment()     == enSRU_DEPLOYMENT__MR )
            && ( GetOperation_ClassOfOp() == CLASSOP__AUTO        )
            && ( GetOperation_AutoMode()  == MODE_A__TEST    )
    )
    {
        Enter_AcceptanceMenu();
    }
   else
   {
      LCD_Char_Clear();
      if(GetSRU_Deployment() != enSRU_DEPLOYMENT__MR)
      {
         LCD_Char_GotoXY(1,1);
         LCD_Char_WriteString("Access Denied:");
         LCD_Char_GotoXY(2,2);
         LCD_Char_WriteString("Move To MR SRU");
      }
      else if(   GetOperation_ClassOfOp() == CLASSOP__MANUAL
            && GetOperation_ManualMode() == MODE_M__CONSTRUCTION)
      {
      LCD_Char_GotoXY(1,1);
      LCD_Char_WriteString("Resetting Tests");
      LCD_Char_GotoXY(1,2);
      LCD_Char_WriteString("Return to Test Mode");
      LCD_Char_GotoXY(1,3);
      LCD_Char_WriteString("To Resume Tests");
      }
      else
      {
         LCD_Char_GotoXY(1,1);
         LCD_Char_WriteString("Access Denied:");
         LCD_Char_GotoXY(2,2);
         LCD_Char_WriteString("On MR Board,");
         LCD_Char_GotoXY(2,3);
         LCD_Char_WriteString("Jump MM Input");
      }
      enum en_keypresses enKeypress = Button_GetKeypress();
      if(enKeypress == enKEYPRESS_LEFT)
      {
         PrevScreen();
      }
   }
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
