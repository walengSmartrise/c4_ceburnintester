/******************************************************************************
 *
 * @file     ui_ffs_lwd_req.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     20, March 2019
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include "GlobalData.h"
#include "operation.h"
#include "mod_loadweigher.h"
#include <math.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_LWD_Req( void );
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_LWD_Req =
{
   .pfnDraw = UI_FFS_LWD_Req,
};
//--------------------------------------------
static const char *pasRequests[NUM_LW_REQ] =
{
"NO REQUEST",//LW_REQ__TORQUE,
"ZERO",//LW_REQ__ZERO,
"SPAN",//LW_REQ__SPAN,
"COMP ROPE",//LW_REQ__COMP_ROPE,
"MAX CAPACITY",//LW_REQ__MAX_CAP,
"CW PERCENT",//LW_REQ__CW_PERCENT,
"ROPE LB PER FT",//LW_REQ__ROPE_LB_PER_FT,
"BOTTOM POSITION",//LW_REQ__POS_BOT,
"TOP POSITION",//LW_REQ__POS_TOP,
"ENABLE WIFI",//LW_REQ__WIFI,
"LIGHT LOAD LB",//LW_REQ__LIGHT_LOAD,
"FULL LOAD LB",//LW_REQ__FULL_LOAD,
"OVERLOAD LB",//LW_REQ__OVER_LOAD,
"ROPE DIAMETER",//LW_REQ__ROPE_DIAM,
"ROPE COUNT",//LW_REQ__ROPE_COUNT,
"EMPTY CAR WEIGHT",//LW_REQ__EMPTY_CAR_WEIGHT,
"WEIGHT IN CAR",//LW_REQ__WEIGHT_IN_CAR,
"LOAD CELL",//LW_REQ__READ_LOAD_CELL,
"CAR SIDE ROPE",//LW_REQ__CAR_SIDE_ROPE_WEIGHT,
"CAR SIDE CMP ROPE",//LW_REQ__CAR_SIDE_COMP_ROPE_WEIGHT,
"CW SIDE ROPE",//LW_REQ__CW_SIDE_ROPE_WEIGHT,
"CW SIDE CMP ROPE",//LW_REQ__CW_SIDE_COMP_ROPE_WEIGHT,
"TORQUE OFFSET",//LW_REQ__TORQUE_OFFSET,
"TORQUE SCALING",//LW_REQ__TORQUE_SCALING,
};

static const char *pasCMD[NUM_LW_CMD] =
{
"RD",//LW_CMD__READ,
"WR",//LW_CMD__WRITE,
"GN",//LW_CMD__GENERATE,
};
//--------------------------------------------
static uint8_t ucCursorX;

static uint8_t ucRequestIndex;
static uint8_t ucCommandIndex;
static float fWriteValue;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_LWD_Req =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_LWD_Req,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MAX_CURSOR_X                      (15)

#define START_CURSOR_X_SIGN               (2)
#define START_CURSOR_X_VALUE              (3)
#define START_CURSOR_X_DECIMAL_PT         (12)

#define START_SCREEN_POS_X__CMD           (2)
#define START_SCREEN_POS_X__VALUE         (5)
#define START_SCREEN_POS_X__DECIMAL_PT    (16)
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

|01234567890123456789|

|CAR SIDE CMP ROPE   |
|  WR +1000000000.000|
|*                   |
|     +1000000000.000|

|0 1  23456789012 345|// Cursor POS
-----------------------------------------------------------------------------*/
static void UpdateScreen( void )
{
   LCD_Char_Clear();
   /* Line 1 */
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteString(pasRequests[ucRequestIndex]);

   /* Line 2 */
   LCD_Char_GotoXY( START_SCREEN_POS_X__CMD, 1 );
   LCD_Char_WriteString(pasCMD[ucCommandIndex]);

   LCD_Char_GotoXY( START_SCREEN_POS_X__VALUE, 1 );
   LCD_Char_WriteFloat_ExactLength(fWriteValue, 10, 3);

   /* Line 3 */
   switch( ucCursorX )
   {
      case 0:
         LCD_Char_GotoXY( 0, 2 );
         break;
      case 1:
         LCD_Char_GotoXY( 2, 2 );
         break;
      case START_CURSOR_X_SIGN ... START_CURSOR_X_DECIMAL_PT:
         LCD_Char_GotoXY( START_SCREEN_POS_X__VALUE+ucCursorX-START_CURSOR_X_SIGN, 2 );
         break;
      case START_CURSOR_X_DECIMAL_PT+1 ... MAX_CURSOR_X:
         LCD_Char_GotoXY( START_SCREEN_POS_X__DECIMAL_PT+ucCursorX-START_CURSOR_X_DECIMAL_PT, 2 );
         break;
      default:
         break;
   }
   LCD_Char_WriteString("*");

   /* Line 4 */
   float fReadData = LoadWeigher_GetUIRequestReadData();
   LCD_Char_GotoXY( START_SCREEN_POS_X__VALUE, 3 );
   LCD_Char_WriteFloat_ExactLength(fReadData, 10, 3);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ControlScreen( void )
{
   uint8_t bSave = 0;
   enum en_keypresses eKeypress = Button_GetKeypress();

   int8_t cEditRequestIndex = ucRequestIndex;
   int8_t cEditCommandIndex = ucCommandIndex;
   float fEditValue = fWriteValue;

   switch(eKeypress)
   {
      case enKEYPRESS_LEFT:
         if(ucCursorX)
         {
            ucCursorX--;
         }
         else
         {
            PrevScreen();
            cEditRequestIndex = 0;
            cEditCommandIndex = 0;
            fEditValue = 0;
         }
         break;

      case enKEYPRESS_RIGHT:
         if(ucCursorX < MAX_CURSOR_X)
         {
            ucCursorX++;
         }
         break;

      case enKEYPRESS_UP:
         switch( ucCursorX )
         {
            case 0:
               cEditRequestIndex += 1;
               break;
            case 1:
               cEditCommandIndex += 1;
               break;
            case 2:
               fEditValue *= -1;
               break;
            case START_CURSOR_X_VALUE ... MAX_CURSOR_X:
               {
                  float fOffset = powf(10.0f, (START_CURSOR_X_DECIMAL_PT-ucCursorX));
                  fEditValue += fOffset;
               }
               break;
            default: break;
         }
         break;

      case enKEYPRESS_DOWN:
         switch( ucCursorX )
         {
            case 0:
               cEditRequestIndex -= 1;
               break;
            case 1:
               cEditCommandIndex -= 1;
               break;
            case 2:
               fEditValue *= -1;
               break;
            case START_CURSOR_X_VALUE ... MAX_CURSOR_X:
               {
                  float fOffset = powf(10.0f, (START_CURSOR_X_DECIMAL_PT-ucCursorX));
                  fEditValue -= fOffset;
               }
               break;
            default: break;
         }
         break;

      case enKEYPRESS_ENTER:
         bSave = 1;
         break;

      default:
         break;
   }

   /* Bound values */
   if( cEditRequestIndex >= NUM_LW_REQ )
   {
      cEditRequestIndex = NUM_LW_REQ-1;
   }
   else if( cEditRequestIndex < 0 )
   {
      cEditRequestIndex = 0;
   }
   ucRequestIndex = cEditRequestIndex;

   if( cEditCommandIndex >= NUM_LW_CMD )
   {
      cEditCommandIndex = NUM_LW_CMD-1;
   }
   else if( cEditCommandIndex < 0 )
   {
      cEditCommandIndex = 0;
   }
   ucCommandIndex = cEditCommandIndex;

   fWriteValue = fEditValue;

   if(bSave)
   {
      st_lwd_ui_request stUIRequest;
      stUIRequest.eRequest = ucRequestIndex;
      stUIRequest.eCommand = ucCommandIndex;
      stUIRequest.fWriteData = fWriteValue;
      LoadWeigher_SetUIRequest(&stUIRequest, 0);
   }
   else // Constantly query latest read data unless issuing a write or generate command
   {
      st_lwd_ui_request stUIRequest;
      stUIRequest.eRequest = ucRequestIndex;
      stUIRequest.eCommand = LW_CMD__READ;
      stUIRequest.fWriteData = fWriteValue;
      LoadWeigher_SetUIRequest(&stUIRequest, 0);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_LWD_Req( void )
{
   if( ( GetSRU_Deployment() == enSRU_DEPLOYMENT__MR )
    || ( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT ) )
   {
      ControlScreen();
      UpdateScreen();
   }
   else
   {
      PrevScreen();
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
