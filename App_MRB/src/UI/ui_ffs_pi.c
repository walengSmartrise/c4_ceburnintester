/******************************************************************************
 *
 * @file     ui_ffs_pi.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_Setup_PI( void );

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/


static struct st_ui_screen__freeform gstFFS_Setup_PI =
{
   .pfnDraw = UI_FFS_Setup_PI,
};


static struct st_ui_parameter_edit gstUIParamEdit;

static struct st_param_and_value gstPV;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_Setup_PI =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_PI,
};



/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/

static void Update_Screen(void)
{
   LCD_Char_Clear();

   LCD_Char_GotoXY( 0, 0 );

   //------------------------------------------------------------------


   if(gstUIParamEdit.bSaving)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Saving");


   }
   else if(gstUIParamEdit.bInvalid)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Invalid");
   }
   else
   {
      LCD_Char_WriteStringUpper( "Set PI Label" );
      LCD_Char_GotoXY(0,2);
      LCD_Char_WriteStringUpper("Floor ");
      LCD_Char_WriteInteger_MinLength( gstUIParamEdit.uwParamIndex_Current - gstUIParamEdit.uwParamIndex_Start + 1, 3 );
      LCD_Char_WriteStringUpper(" = \"");

      LCD_Char_GotoXY(13,2);
      char ucChar3 = gstUIParamEdit.ulValue_Edited >> 16 & 0xFF;
      char ucChar2 = gstUIParamEdit.ulValue_Edited >> 8 & 0xFF;
      char ucChar1 = gstUIParamEdit.ulValue_Edited & 0xFF;
      LCD_Char_WriteChar(ucChar3);
      LCD_Char_WriteChar(ucChar2);
      LCD_Char_WriteChar(ucChar1);
      LCD_Char_WriteString("\"");
      //------------------------------------------------------------------

      LCD_Char_GotoXY( gstUIParamEdit.ucCursorColumn, 3 );

      LCD_Char_WriteChar( '*' );

      //------------------------------------------------------------------
      if ( gstUIParamEdit.ulValue_Edited != gstUIParamEdit.ulValue_Saved )
      {
         LCD_Char_GotoXY( 16, 1 );
         LCD_Char_WriteString( "Save" );

         LCD_Char_GotoXY( 19, 2 );
         LCD_Char_WriteChar( '|' );
      }
   }

}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ValidateParameter(void)
{
   switch(gstUIParamEdit.enParamType)
   {
      case enPARAM_BLOCK_TYPE__BIT:
         gstUIParamEdit.ulMask = 0x01;
         if(gstPV.uwParamIndex >= NUM_1BIT_PARAMS)
         {
            gstUIParamEdit.bInvalid = 1;
         }
         else
         {
            gstUIParamEdit.ulValue_Saved = Param_ReadValue_1Bit(gstUIParamEdit.uwParamIndex_Current) & gstUIParamEdit.ulMask;
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT8:
         gstUIParamEdit.ulMask = 0xFF;
         if(gstPV.uwParamIndex >= NUM_8BIT_PARAMS)
         {
            gstUIParamEdit.bInvalid = 1;
         }
         else
         {
            gstUIParamEdit.ulValue_Saved = Param_ReadValue_8Bit(gstUIParamEdit.uwParamIndex_Current) & gstUIParamEdit.ulMask;
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT16:
         gstUIParamEdit.ulMask = 0xFFFF;
         if(gstPV.uwParamIndex >= NUM_16BIT_PARAMS)
         {
            gstUIParamEdit.bInvalid = 1;
         }
         else
         {
            gstUIParamEdit.ulValue_Saved = Param_ReadValue_16Bit(gstUIParamEdit.uwParamIndex_Current) & gstUIParamEdit.ulMask;
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT24:
         gstUIParamEdit.ulMask = 0xFFFFFF;
         if(gstPV.uwParamIndex >= NUM_24BIT_PARAMS)
         {
            gstUIParamEdit.bInvalid = 1;
         }
         else
         {
            gstUIParamEdit.ulValue_Saved = Param_ReadValue_24Bit(gstUIParamEdit.uwParamIndex_Current) & gstUIParamEdit.ulMask;
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT32:
         gstUIParamEdit.ulMask = 0xFFFFFFFF;
         if(gstUIParamEdit.uwParamIndex_Current >= NUM_32BIT_PARAMS)
         {
            gstUIParamEdit.bInvalid = 1;
         }
         else
         {
            gstUIParamEdit.ulValue_Saved = Param_ReadValue_32Bit(gstUIParamEdit.uwParamIndex_Current) & gstUIParamEdit.ulMask;
         }
         break;
      default:
         gstUIParamEdit.bInvalid = 1;
         break;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Index( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex = 0;
   int32_t lParamIndex_Edited;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = gstUIParamEdit.ucFieldSize_Index-1;
      }
   }
   else  // enACTION__EDIT
   {

      lParamIndex_Edited = gstUIParamEdit.uwParamIndex_Current;

      gstUIParamEdit.ucCursorColumn = 6+ucCursorIndex;
      //------------------------------------------------------------------
      int32_t uiDigitWeight = (gstUIParamEdit.ucFieldSize_Index - 1) - ucCursorIndex;

      uiDigitWeight = pow( 10, uiDigitWeight );  // 10 to the power of uiDigitWeight
      //------------------------------------------------------------------
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            lParamIndex_Edited += uiDigitWeight;
            break;
         case enKEYPRESS_DOWN:
            lParamIndex_Edited -= uiDigitWeight;
            break;
         case enKEYPRESS_LEFT:
            if ( ucCursorIndex )
            {
               --ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_LEFT;
            }
            break;
         case enKEYPRESS_RIGHT:
            if ( ucCursorIndex < gstUIParamEdit.ucFieldSize_Index - 1 )
            {
               ++ucCursorIndex;
            }
            else
            {
               gstUIParamEdit.ulValue_Edited = gstUIParamEdit.ulValue_Saved;
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;
         default:
            break;
      }
      //------------------------------------------------------------------
      if(lParamIndex_Edited > gstUIParamEdit.uwParamIndex_End)
      {
         lParamIndex_Edited = gstUIParamEdit.uwParamIndex_End;
      }
      else if(lParamIndex_Edited < gstUIParamEdit.uwParamIndex_Start)
      {
         lParamIndex_Edited = gstUIParamEdit.uwParamIndex_Start;
      }

      gstUIParamEdit.uwParamIndex_Current = (uint16_t) lParamIndex_Edited;

      gstUIParamEdit.ulValue_Edited = gstUIParamEdit.ulValue_Saved;

   }

   return enReturnValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Value( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex;
   int64_t llCurrentParam_EditedValue;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = gstUIParamEdit.ucFieldSize_Value - 1;
      }
   }
   else  // enACTION__EDIT
   {
      llCurrentParam_EditedValue = gstUIParamEdit.ulValue_Edited;
      //------------------------------------------------------------------
      int32_t uiDigitWeight = 8*((gstUIParamEdit.ucFieldSize_Value - 1) - ucCursorIndex);

      uiDigitWeight = pow( 2, uiDigitWeight );  // 10 to the power of uiDigitWeight

      //------------------------------------------------------------------
      gstUIParamEdit.ucCursorColumn = 13 + ucCursorIndex;
      //------------------------------------------------------------------
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            llCurrentParam_EditedValue += uiDigitWeight;
            break;

         case enKEYPRESS_DOWN:
            llCurrentParam_EditedValue -= uiDigitWeight;
            break;

         case enKEYPRESS_LEFT:
            if ( ucCursorIndex )
            {
               --ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_LEFT;
            }
            break;

         case enKEYPRESS_RIGHT:
            if ( ucCursorIndex < (gstUIParamEdit.ucFieldSize_Value - 1) )
            {
               ++ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;

         default:
            break;
      }

      uint8_t ucChar3 = llCurrentParam_EditedValue >> 16 & 0xFF;
      uint8_t ucChar2 = llCurrentParam_EditedValue >> 8 & 0xFF;
      uint8_t ucChar1 = llCurrentParam_EditedValue & 0xFF;

      if(ucChar3 < 0x20)
      {
         llCurrentParam_EditedValue &= ~(0xFF << 16);
         llCurrentParam_EditedValue |= (0x20 << 16);
      }
      else if(ucChar3 > 0x5A)
      {
         llCurrentParam_EditedValue &= ~(0xFF << 16);
         llCurrentParam_EditedValue |= (0x5A << 16);
      }

      if(ucChar2 < 0x20)
      {
         llCurrentParam_EditedValue &= ~(0xFF << 8);
         llCurrentParam_EditedValue |= (0x20 << 8);
      }
      else if(ucChar2 > 0x5A)
      {
         llCurrentParam_EditedValue &= ~(0xFF << 8);
         llCurrentParam_EditedValue |= (0x5A << 8);
      }

      if(ucChar1 < 0x20)
      {
         llCurrentParam_EditedValue &= ~(0xFF);
         llCurrentParam_EditedValue |= (0x20);
      }
      else if(ucChar1 > 0x5A)
      {
         llCurrentParam_EditedValue &= ~(0xFF);
         llCurrentParam_EditedValue |= (0x5A);
      }

      gstUIParamEdit.ulValue_Edited = (uint32_t) llCurrentParam_EditedValue;
   }

   return enReturnValue;
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void SaveParameter( void )
{
   uint16_t uwParamIndex = gstUIParamEdit.uwParamIndex_Current - gstUIParamEdit.uwParamIndex_Start;
   gstUIParamEdit.ulValue_Saved = Param_ReadValue_24Bit(uwParamIndex);

   if(gstUIParamEdit.ulValue_Saved != gstUIParamEdit.ulValue_Edited)
   {
      Param_WriteValue_24Bit(uwParamIndex, gstUIParamEdit.ulValue_Edited );
   }

}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Save( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static enum en_save_state enSaveState = SAVE_STATE__SAVED;

   gstUIParamEdit.ucCursorColumn = 19;
   //------------------------------------------
   if ( enFieldAction == enACTION__EDIT )
   {
      if ( enKeypress == enKEYPRESS_ENTER )
      {
         enSaveState = SAVE_STATE__SAVING;
      }
      else if ( enKeypress == enKEYPRESS_LEFT )
      {
         enSaveState = SAVE_STATE__SAVED;
         enReturnValue = enACTION__EXIT_TO_LEFT;
      }
   }

   //--------------------------------
   switch(enSaveState)
   {
      case SAVE_STATE__CLEARING:
         gstUIParamEdit.bSaving = 1;
         if(!gstUIParamEdit.ulValue_Saved)
         {
            enSaveState = SAVE_STATE__SAVING;
         }
         break;
      case SAVE_STATE__SAVING:
         SaveParameter();
         gstUIParamEdit.bSaving = 1;
         if(gstUIParamEdit.ulValue_Saved == gstUIParamEdit.ulValue_Edited)
         {
            enReturnValue = enACTION__EXIT_TO_LEFT;
            gstUIParamEdit.bSaving = 0;
            enSaveState = SAVE_STATE__SAVED;
         }
         break;
      case SAVE_STATE__SAVED:
         gstUIParamEdit.bSaving = 0;
         break;
      default:
         enSaveState = SAVE_STATE__SAVED;
         break;
   }
   return enReturnValue;
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void Edit_PI(void)
{
   ValidateParameter();
   if ( gstUIParamEdit.ucFieldIndex == enFIELD__NONE )
   {
      gstUIParamEdit.ulValue_Edited = gstUIParamEdit.ulValue_Saved;
      gstUIParamEdit.bSaving = 0;
      gstUIParamEdit.bInvalid = 0;
      gstUIParamEdit.ucFieldIndex = enFIELD__INDEX;

      Edit_Index( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT );

   }
   else
   {

      enum en_field_action enFieldAction;

      enum en_keypresses enKeypress = Button_GetKeypress();
      //------------------------------------------------------------------
      switch ( gstUIParamEdit.ucFieldIndex )
      {
         case enFIELD__INDEX:
            enFieldAction = Edit_Index( enKeypress, enACTION__EDIT );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               PrevScreen();
               gstUIParamEdit.ucFieldIndex = enFIELD__NONE;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT );

               gstUIParamEdit.ucFieldIndex = enFIELD__VALUE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__VALUE: //function

            enFieldAction = Edit_Value( enKeypress, enACTION__EDIT );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Index( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT );

               gstUIParamEdit.ucFieldIndex = enFIELD__INDEX;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Save( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT );

               gstUIParamEdit.ucFieldIndex = enFIELD__SAVE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__SAVE:

            enFieldAction = Edit_Save( enKeypress, enACTION__EDIT );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT );

               gstUIParamEdit.ucFieldIndex = enFIELD__VALUE;
            }
            break;

         default:
            break;
      }
      Update_Screen();
   }
}



/*-----------------------------------------------------------------------------
 With 0x422020 saved, the following will be displayed:
 Three PI Displays: "B  "
 Two PI Displays: "B "
 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_PI( void )
{
   static uint8_t bFirst = 1;
   if(GetFP_FlashParamsReady())
   {
      if(bFirst)
      {
         gstUIParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT24;
         gstUIParamEdit.uwParamIndex_Start = enPARAM24__PI_0;
         gstUIParamEdit.uwParamIndex_End = gstUIParamEdit.uwParamIndex_Start + GetFP_NumFloors() - 1;
         gstUIParamEdit.uwParamIndex_Current = gstUIParamEdit.uwParamIndex_Start;
         gstUIParamEdit.ucFieldSize_Value = 3;
         gstUIParamEdit.ucFieldSize_Index = 3;
         gstUIParamEdit.bInvalid = 0;
         bFirst = 0;
      }
      else
      {
         Edit_PI();
      }
   }
   else
   {
      PrevScreen();
   }
}


/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
