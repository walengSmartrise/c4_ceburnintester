/******************************************************************************
 *
 * @file     ui.c
 * @brief    User Interface
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include <stdint.h>
#include <string.h>

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

//-----------------------------------------------------------------------------
struct st_ui_control gstUI_Control =
{
   .pstUGS_Current = &gstUGS_HomeScreen,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t ucMovingTextIndex;
static uint8_t ucMovingTextShiftDelay_30ms;
static uint8_t bNewPageFlag;
static char const* const gpasDoor_Strings[ NUM_UI_DOOR_STATES ] =
{
   "[ ? ]",//UI_DOOR__UNKNOWN,

   "[ | ]",//UI_DOOR__CLOSED,
   " >|< ",//UI_DOOR__CLOSED_WITH_DC,

   "[< >]",//UI_DOOR__OPENING,
   "[<|>]",//UI_DOOR__OPENING_WITH_GSW,
   "[<*>]",//UI_DOOR__OPENING_WITH_PHE,

   "[   ]",//UI_DOOR__OPEN,
   "<   >",//UI_DOOR__OPEN_WITH_DO,
   "[ * ]",//UI_DOOR__OPEN_WITH_PHE,
   "< * >",//UI_DOOR__OPEN_WITH_PHE_DO,

   "[| |]",//UI_DOOR__PARTIALLY_OPEN,
   "[|*|]",//UI_DOOR__PARTIALLY_OPEN_WITH_PHE,

   "[> <]",//UI_DOOR__CLOSING,
   "[>|<]",//UI_DOOR__CLOSING_WITH_GSW,
   "[>*<]",//UI_DOOR__CLOSING_WITH_PHE,

   "[>!<]",//UI_DOOR__NUDGING,
};
/*----------------------------------------------------------------------------
   Update the bNewPageFlag. This flag is 1 only for one module cycle, after a page change.

   Always place before UI_UpdateMovingTextVariables
 *----------------------------------------------------------------------------*/
void UI_UpdateNewPageFlag(struct st_ui_control *pstUI_Control)
{
   static struct st_ui_generic_screen *pstUGS_Last;
   if(pstUGS_Last != pstUI_Control->pstUGS_Current)
   {
      bNewPageFlag = 1;
   }
   else
   {
      bNewPageFlag = 0;
   }
   pstUGS_Last = pstUI_Control->pstUGS_Current;
}
uint8_t UI_GetNewPageFlag()
{
   return bNewPageFlag;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void UI_UpdateMovingTextVariables(struct st_ui_control *pstUI_Control)
{
   if( UI_GetNewPageFlag() )
   {
      ucMovingTextIndex = 0;
      ucMovingTextShiftDelay_30ms = 0;
   }
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void PrintMovingText( char const * const psString )
{
   static uint8_t ucMovingTextIndex;
   static uint8_t ucMovingTextShiftDelay_30ms;
   uint8_t ucLength = strlen(psString);
   if(ucLength > 20)
   {
      if(++ucMovingTextShiftDelay_30ms >= 20)
      {
         ucMovingTextShiftDelay_30ms = 0;
         if(++ucMovingTextIndex >= (ucLength - 20 + 2))
         {
            ucMovingTextIndex = 0;
         }
      }
   }
   else
   {
      ucMovingTextIndex = 0;
   }
   LCD_Char_WriteString( psString + ucMovingTextIndex );
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void Convert_Param_To_Feet( uint32_t ulCount_500UM )
{
   uint16_t uwFeet;
   uint16_t uwInch;
   uint16_t ucMIL;

   int64_t llPosition = ulCount_500UM - GetPositionReference();

   llPosition *= 100000L;//scale by 10000 -> 5 x 10^-8 counts
   llPosition /= 2L; // nanoinch
   llPosition /= 254L;// inches
   if(llPosition < 0L)
   {
      llPosition *= -1;
      LCD_Char_WriteChar( '-' );
   }

   llPosition /= 100L;
   ucMIL = llPosition % 100L;

   llPosition /= 100L;
   uwInch = llPosition % 12L;

   llPosition /= 12L;

   uwFeet = llPosition;
   LCD_Char_WriteInteger_MinLength(uwFeet, 4);

   LCD_Char_WriteChar('\'');

   LCD_Char_WriteInteger_MinLength(uwInch, 2);
   LCD_Char_WriteChar('.');

   LCD_Char_WriteInteger_MinLength(ucMIL, 3);

   LCD_Char_WriteChar('\"');
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void LCD_WritePositionInFeet( uint32_t ulCount_500UM )
{
   uint16_t uwFeet;
   uint16_t uwInch;
   uint16_t uwMIL;

   int32_t iPositionDifference = ulCount_500UM - GetPositionReference();
   int64_t llPosition_mil = ( iPositionDifference * ( 1000 / (2*25.4) ) ) ; // inches to mils, half mm to mm, mm to inches

   if(iPositionDifference < 0L)
   {
      llPosition_mil *= -1;
      LCD_Char_WriteChar( '-' );
   }

   uwMIL = llPosition_mil % 1000;

   llPosition_mil /= 1000;
   uwInch = llPosition_mil % 12;

   llPosition_mil /= 12;

   uwFeet = llPosition_mil;
   LCD_Char_WriteInteger_MinLength(uwFeet, 4);

   LCD_Char_WriteChar('\'');

   LCD_Char_WriteInteger_MinLength(uwInch, 2);
   LCD_Char_WriteChar('.');

   LCD_Char_WriteInteger_MinLength(uwMIL, 3);

   LCD_Char_WriteChar('\"');
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void LCD_WritePositionInFeet_Shortened( uint32_t ulCount_500UM )
{
   uint16_t uwFeet;
   uint16_t uwInch;
   uint16_t uwMIL;

   int32_t iPositionDifference = ulCount_500UM - GetPositionReference();
   int64_t llPosition_mil = ( iPositionDifference * ( 1000 / (2*25.4) ) ) ; // inches to mils, half mm to mm, mm to inches

   if(iPositionDifference < 0L)
   {
      llPosition_mil *= -1;
      LCD_Char_WriteChar( '-' );
   }

   uwMIL = llPosition_mil % 1000;

   llPosition_mil /= 1000;
   uwInch = llPosition_mil % 12;

   llPosition_mil /= 12;

   uwFeet = llPosition_mil;
   LCD_Char_WriteInteger_MinLength(uwFeet, 1);

   LCD_Char_WriteChar('\'');

   LCD_Char_WriteInteger_MinLength(uwInch, 2);
   LCD_Char_WriteChar('.');

   LCD_Char_WriteInteger_MinLength(uwMIL, 3);

   LCD_Char_WriteChar('\"');
}
/*----------------------------------------------------------------------------
  Prints a distance in 0.5mm counts in inches or inches/feet depending on bInchesOnly

  Inches limited to 999
 *----------------------------------------------------------------------------*/
void LCD_WriteDistanceInInches( uint32_t ulCount_500UM, uint8_t bInchesOnly )
{
   uint16_t uwFeet;
   uint16_t uwInch;
   uint16_t uwMIL;

   int32_t iPositionDifference = ulCount_500UM;
   int64_t llPosition_mil = ( iPositionDifference * ( 1000 / (2*25.4) ) ) ; // inches to mils, half mm to mm, mm to inches

   if(iPositionDifference < 0L)
   {
      llPosition_mil *= -1;
      LCD_Char_WriteChar( '-' );
   }
   else
   {
      LCD_Char_WriteChar( '+' );
   }

   if( bInchesOnly )
   {
      uwMIL = llPosition_mil % 1000;

      llPosition_mil /= 1000;
      uwInch = llPosition_mil;

      LCD_Char_WriteInteger_MinLength(uwInch, 3);
      LCD_Char_WriteChar('.');

      LCD_Char_WriteInteger_MinLength(uwMIL, 3);

      LCD_Char_WriteChar('\"');
   }
   else
   {
      uwMIL = llPosition_mil % 1000;

      llPosition_mil /= 1000;
      uwInch = llPosition_mil % 12;

      llPosition_mil /= 12;

      uwFeet = llPosition_mil;
      LCD_Char_WriteInteger_MinLength(uwFeet, 4);

      LCD_Char_WriteChar('\'');

      LCD_Char_WriteInteger_MinLength(uwInch, 2);
      LCD_Char_WriteChar('.');

      LCD_Char_WriteInteger_MinLength(uwMIL, 3);

      LCD_Char_WriteChar('\"');
   }
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void UI_PrintDoorSymbol( enum en_door_states eState, uint8_t bPHE, uint8_t bGSW, uint8_t bDO, uint8_t bDC )
{
   en_ui_door_states eDoorState = UI_DOOR__UNKNOWN;
   switch( eState )
   {
      case DOOR__CLOSED:
         if( bDC )
         {
            eDoorState = UI_DOOR__CLOSED_WITH_DC;
         }
         else
         {
            eDoorState = UI_DOOR__CLOSED;
         }
         break;
      case DOOR__OPENING:
         if( bGSW )
         {
            eDoorState = UI_DOOR__OPENING_WITH_GSW;
         }
         else if( !bPHE )
         {
            eDoorState = UI_DOOR__OPENING_WITH_PHE;
         }
         else
         {
            eDoorState = UI_DOOR__OPENING;
         }
         break;
      case DOOR__OPEN:
         if( bDO )
         {
            if( !bPHE )
            {
               eDoorState = UI_DOOR__OPEN_WITH_PHE_DO;
            }
            else
            {
               eDoorState = UI_DOOR__OPEN_WITH_DO;
            }
         }
         else if( !bPHE )
         {
            eDoorState = UI_DOOR__OPEN_WITH_PHE;
         }
         else
         {
            eDoorState = UI_DOOR__OPEN;
         }
         break;
      case DOOR__PARTIALLY_OPEN:
         if( !bPHE )
         {
            eDoorState = UI_DOOR__PARTIALLY_OPEN_WITH_PHE;
         }
         else
         {
            eDoorState = UI_DOOR__PARTIALLY_OPEN;
         }
         break;
      case DOOR__CLOSING:
         if( bGSW )
         {
            eDoorState = UI_DOOR__CLOSING_WITH_GSW;
         }
         else if( !bPHE )
         {
            eDoorState = UI_DOOR__CLOSING_WITH_PHE;
         }
         else
         {
            eDoorState = UI_DOOR__CLOSING;
         }
         break;
      case DOOR__NUDGING:
         eDoorState = UI_DOOR__NUDGING;
         break;
      default: break;
   }

   LCD_Char_WriteString(gpasDoor_Strings[eDoorState]);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void PrevScreen( void )
{
   struct st_ui_control *pstUI_Control;
   pstUI_Control = &gstUI_Control;

   if ( pstUI_Control->pstUGS_Current )
   {
      struct st_ui_generic_screen *pstUGS_Previous;
      pstUGS_Previous = pstUI_Control->pstUGS_Current->pstUGS_Previous;

      if ( pstUGS_Previous )
      {
         pstUI_Control->pstUGS_Current = pstUGS_Previous;
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void NextScreen( struct st_ui_generic_screen *pstUGS_Next )
{
   if ( pstUGS_Next )
   {
      struct st_ui_control *pstUI_Control;
      pstUI_Control = &gstUI_Control;

      struct st_ui_generic_screen *pstUGS_Current;
      pstUGS_Current = pstUI_Control->pstUGS_Current;

      pstUI_Control->pstUGS_Current = pstUGS_Next;

      pstUGS_Next->pstUGS_Previous = pstUGS_Current;
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void Draw_Menu( struct st_ui_screen__menu * pstMenuScreen )
{
   if ( pstMenuScreen )
   {
      struct st_ui_menu_item * (* pastMenuItems)[] = pstMenuScreen->pastMenuItems;

      LCD_Char_Clear();

      LCD_Char_GotoXY( 0, 0 );
      LCD_Char_WriteStringUpper( pstMenuScreen->psTitle );

      //SDA REVIEW: should be fixed width type
      int iLCD_Row = 1;

      //SDA REVIEW: should use fixed width type
      for ( int i = pstMenuScreen->ucTopItem; i < pstMenuScreen->ucNumItems; ++i )
      {
         LCD_Char_GotoXY( 0, iLCD_Row );

         char c;

         if ( i == pstMenuScreen->ucCurrentItem )
         {
            c = '*';
         }
         else
         {
            c = ' ';
         }

         LCD_Char_WriteChar( c );

         struct st_ui_menu_item *pstCurrentMenuItem = (*pastMenuItems)[ i ];

         LCD_Char_WriteString( pstCurrentMenuItem->psTitle );

         if ( ++iLCD_Row >= LCD_CHAR_NUM_ROWS )
         {
            break;
         }
      }

      enum en_keypresses enKeypress = Button_GetKeypress();

      if ( enKeypress == enKEYPRESS_LEFT )
      {
         PrevScreen();
      }
      else if ( enKeypress == enKEYPRESS_DOWN )
      {
         if (    pstMenuScreen->ucNumItems
              && (pstMenuScreen->ucCurrentItem < (pstMenuScreen->ucNumItems - 1))
            )
         {
            ++pstMenuScreen->ucCurrentItem;

            int iRowsFromTop = pstMenuScreen->ucCurrentItem - pstMenuScreen->ucTopItem;

            if ( iRowsFromTop > (LCD_CHAR_NUM_ROWS - 2) )
            {
               ++pstMenuScreen->ucTopItem;
            }
         }
      }
      else if ( enKeypress == enKEYPRESS_UP )
      {
         if ( pstMenuScreen->ucCurrentItem )
         {
            if ( pstMenuScreen->ucTopItem == pstMenuScreen->ucCurrentItem )
            {
               --pstMenuScreen->ucTopItem;
            }

            --pstMenuScreen->ucCurrentItem;
         }
      }
      else if ( (enKeypress == enKEYPRESS_ENTER) || (enKeypress == enKEYPRESS_RIGHT) )
      {
         struct st_ui_menu_item *pstCurrentMenuItem = (*pastMenuItems)[ pstMenuScreen->ucCurrentItem ];

         if ( pstCurrentMenuItem->pstUGS_Next )
         {
            NextScreen( pstCurrentMenuItem->pstUGS_Next );
         }
      }
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

