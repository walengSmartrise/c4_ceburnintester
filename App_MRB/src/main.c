
#include "mod.h"
#include "board.h"
#include <stdio.h>
#include "sru.h"
#include "sru_b.h"
#include "sys.h"
#include "GlobalData.h"


extern void main_MR(void);

void Deployment_Init( void )
{

    uint8_t iError = 1;  // assume error


    switch(GetSRU_Deployment())
    {
        case enSRU_DEPLOYMENT__MR:
            SetSystemNodeID(SYS_NODE__MRB);
            SharedData_Init();
            SharedData_GroupNet_Init();
            SharedData_AuxNet_Init();

            iError = 0;

            break;

        default:
            iError = 1;

            break;
    }


    if ( iError )
    {
        while ( 1 )
        {
            static uint32_t uiCounter, bState;

            //SDA REVIEW: why the hard coded value. If you need the max value use stdint limit constants eg UINT32_MAX
            if( uiCounter >= 0xfffff )
            {
                uiCounter = 0;

                bState = ( bState )? 0: 1;

                SRU_Write_LED( enSRU_LED_Heartbeat, bState );
                SRU_Write_LED( enSRU_LED_Alarm,     bState );
                SRU_Write_LED( enSRU_LED_Fault,     bState );
            }
            else
            {
                uiCounter++;
            }
        }
    }
}

/**
 * @brief   main routine for systick example
 * @return   Function should not exit.
 */
int main(void)
{
    /* Generic Initialization
     *
     */
    SystemCoreClockUpdate();

    Sys_Init();

    MCU_B_Init();

    Deployment_Init();

    InitCarCallQue ();

    main_MR();

    return 0;
}


