
/******************************************************************************
 *
 * @file     XRegDestination.c
 * @brief
 * @version  V1.00
 * @date     15, May 2018
 *
 * @note    Functions for assigning XReg destinations
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>

#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "carData.h"
#include "carDestination.h"
#include "XRegData.h"
#include "XRegDestination.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void ClrDestination_ByCar(uint8_t ucCarIndex);
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static st_XRegDestination astXRegCarDestination[MAX_GROUP_CARS];

static const st_XRegDestination stClearedCarDestination =
{
  .ucSendCountdown = 0,
  .eCallDir = DIR__NONE,
  .eDoor = DOOR_FRONT,
  .ucLanding = INVALID_LANDING,
  .bCallAccepted = 0,
};
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Initialize car data
 -----------------------------------------------------------------------------*/
void Init_XRegDestinationStructure()
{
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      ClrDestination_ByCar(i);
      astXRegCarDestination[i].ucSendCountdown = 0;
      astXRegCarDestination[i].uiTimeout_ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ClrDestination_ByCar( uint8_t ucCarIndex )
{
   if( (astXRegCarDestination[ucCarIndex].eCallDir != stClearedCarDestination.eCallDir)
    || (astXRegCarDestination[ucCarIndex].eDoor != stClearedCarDestination.eDoor)
    || (astXRegCarDestination[ucCarIndex].ucLanding != stClearedCarDestination.ucLanding) )
   {
      astXRegCarDestination[ucCarIndex].eCallDir = stClearedCarDestination.eCallDir;
      astXRegCarDestination[ucCarIndex].eDoor = stClearedCarDestination.eDoor;
      astXRegCarDestination[ucCarIndex].ucLanding = stClearedCarDestination.ucLanding;
      astXRegCarDestination[ucCarIndex].ucSendCountdown = XREG_DESTINATION_RESEND_COUNT;
   }
   astXRegCarDestination[ucCarIndex].uiTimeout_ms = 0;
}

/*-----------------------------------------------------------------------------
   Access functions
 -----------------------------------------------------------------------------*/
st_XRegDestination * GetXRegDestinationStructure( uint8_t ucCarIndex )
{
   return &astXRegCarDestination[ucCarIndex];
}
uint8_t XRegDestination_GetLanding( uint8_t ucCarIndex )
{
   return astXRegCarDestination[ucCarIndex].ucLanding;
}
enum en_doors XRegDestination_GetDoor( uint8_t ucCarIndex )
{
   return astXRegCarDestination[ucCarIndex].eDoor;
}
enum direction_enum XRegDestination_GetDirection( uint8_t ucCarIndex )
{
   return astXRegCarDestination[ucCarIndex].eCallDir;
}
uint32_t XRegDestination_GetMask( uint8_t ucCarIndex )
{
   return astXRegCarDestination[ucCarIndex].uiMask;
}
uint8_t XRegDestination_GetCallAccepted( uint8_t ucCarIndex )
{
   return astXRegCarDestination[ucCarIndex].bCallAccepted;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void XRegDestination_ClrSendCountdown( uint8_t ucCarIndex )
{
   astXRegCarDestination[ucCarIndex].ucSendCountdown = 0;
}
void XRegDestination_SetCallAccepted( uint8_t ucCarIndex )
{
   astXRegCarDestination[ucCarIndex].bCallAccepted = 1;
}

uint32_t XRegDestination_GetTimeout( uint8_t ucCarIndex )
{
   return astXRegCarDestination[ucCarIndex].uiTimeout_ms;
}
void XRegDestination_IncrementTimeout( uint8_t ucCarIndex, uint16_t uwRunPeriod_ms )
{
   astXRegCarDestination[ucCarIndex].uiTimeout_ms += uwRunPeriod_ms;
}
void XRegDestination_ClrTimeout( uint8_t ucCarIndex )
{
   astXRegCarDestination[ucCarIndex].uiTimeout_ms = 0;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define MASTER_DISPATCHER_MIN_RESEND_MS      (10000)
uint8_t LoadDatagram_XRegDispatcher( un_sdata_datagram *punDatagram, struct st_module *pstThisModule )
{
   static uint8_t ucLastCarID;
   static uint16_t auwMinResendCounter_ms[MAX_GROUP_CARS];
   uint8_t bSend = 0;

   if(GetMasterDispatcherFlag())
   {
      uint8_t ucNumXRegCars = Param_ReadValue_8Bit(enPARAM8__NumXRegCars);
      /* Maintain a minimum resend rate for destination packets */
      for( uint8_t i = 0; i < ucNumXRegCars; i++ )
      {
         if( XRegData_GetCarActive(i) )
         {
            /* If call was already accepted dont resend */
            if( astXRegCarDestination[i].bCallAccepted )
            {
               auwMinResendCounter_ms[i] = 0;
            }
            else if( astXRegCarDestination[i].ucSendCountdown )
            {
               auwMinResendCounter_ms[i] = 0;
            }
            else
            {
               if(auwMinResendCounter_ms[i] >= MASTER_DISPATCHER_MIN_RESEND_MS)
               {
                  astXRegCarDestination[i].ucSendCountdown = 1;
                  auwMinResendCounter_ms[i] = 0;
               }
               else
               {
                  auwMinResendCounter_ms[i] += pstThisModule->uwRunPeriod_1ms;
               }
            }
         }
         else
         {
            astXRegCarDestination[i].ucSendCountdown = 0;
            auwMinResendCounter_ms[i] = 0;
         }
      }

      /* Round robin through cars to send destination commands */
      for( uint8_t i = 0; i < ucNumXRegCars; i++ )
      {
         uint8_t ucCarIndex = (ucLastCarID+i+1) % ucNumXRegCars;
         if( ( XRegData_GetCarActive(ucCarIndex) )
          && ( astXRegCarDestination[ucCarIndex].ucSendCountdown ) )
         {
            punDatagram->auc8[0] = XREG_CMD__LATCH_HC;
            punDatagram->auc8[1] = ucCarIndex;
            punDatagram->auc8[2] = astXRegCarDestination[ucCarIndex].eDoor;
            if(astXRegCarDestination[ucCarIndex].eCallDir == DIR__UP)
            {
               punDatagram->auc8[3] = HC_DIR__UP;
            }
            else
            {
               punDatagram->auc8[3] = HC_DIR__DOWN;
            }
            punDatagram->auc8[4] = astXRegCarDestination[ucCarIndex].ucLanding+1;

            ucLastCarID = ucCarIndex;

            astXRegCarDestination[ucCarIndex].ucSendCountdown--;

            bSend = 1;
            break;
         }
      }
   }

   return bSend;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void SetXRegCarDestination( uint8_t ucCarIndex, st_XRegDestination *pstDestination )
{
   /* Load new destination and mark for send if changed */
   if( ( astXRegCarDestination[ucCarIndex].eCallDir != pstDestination->eCallDir )
    || ( astXRegCarDestination[ucCarIndex].eDoor != pstDestination->eDoor )
    || ( astXRegCarDestination[ucCarIndex].ucLanding != pstDestination->ucLanding )
    || ( astXRegCarDestination[ucCarIndex].uiMask != pstDestination->uiMask ) )
   {
      astXRegCarDestination[ucCarIndex].ucSendCountdown = XREG_DESTINATION_RESEND_COUNT;
      astXRegCarDestination[ucCarIndex].eCallDir = pstDestination->eCallDir;
      astXRegCarDestination[ucCarIndex].eDoor = pstDestination->eDoor;
      astXRegCarDestination[ucCarIndex].ucLanding = pstDestination->ucLanding;
      astXRegCarDestination[ucCarIndex].uiMask = pstDestination->uiMask;
      astXRegCarDestination[ucCarIndex].bCallAccepted = 0;
      astXRegCarDestination[ucCarIndex].uiTimeout_ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void GetNextDestinationAbove_ByCar( uint8_t ucCarIndex, uint8_t ucStartLanding, st_XRegDestination * stDestination)
{
    uint8_t bFullCallFound = 0;
    uint8_t ucTopLanding = XRegData_GetLastLanding( ucCarIndex );
    uint8_t ucBottomLanding = XRegData_GetFirstLanding( ucCarIndex );

    stDestination->ucLanding = INVALID_LANDING;
    stDestination->eCallDir = DIR__NONE;

    //quick sanity check
    if ( (ucTopLanding < MAX_NUM_FLOORS)
      && (ucStartLanding <= ucTopLanding) )
    {
        //scan above for calls
        for (int8_t fl = ucStartLanding; fl <= ucTopLanding; fl++) {
            //if hall call,  if floor is set, set direction and exit, else set floor and direction
           uint8_t bLatchedUpCall = GetLatchedHallCallUp_Front_ByXRegCar(ucCarIndex, fl) || GetLatchedHallCallUp_Rear_ByXRegCar(ucCarIndex, fl);
           uint8_t bUpCallAllowed = (uint8_t) fl != ucTopLanding; // Don't check up call at top terminal
           if ( bUpCallAllowed && bLatchedUpCall ) {
                if (stDestination->ucLanding == INVALID_LANDING)
                {
                    stDestination->ucLanding = (uint8_t) fl;
                    stDestination->eCallDir = DIR__UP;
                }
                else if (stDestination->eCallDir == DIR__NONE)
                {
                    stDestination->eCallDir = DIR__UP;
                }
                bFullCallFound = 1;
                break;
            }
        }
        //if only a car call is found in the direction, look for anything on the way back up to set direction.
        //if coming back up the floor is the same floor as the car call above, set direction to turn around.
        if (!bFullCallFound) {
            for (int8_t fl = ucTopLanding; fl >= ucStartLanding; fl--) {
               uint8_t bLatchedDownCall = GetLatchedHallCallDown_Front_ByXRegCar(ucCarIndex, fl) || GetLatchedHallCallDown_Rear_ByXRegCar(ucCarIndex, fl);
               uint8_t bDownCallAllowed = (uint8_t) fl != ucBottomLanding; // Don't check up call at bottom terminal
               if ( bDownCallAllowed && bLatchedDownCall ) {
                    //if hall call,  if floor is not set set direction and floor
                    if (stDestination->ucLanding == INVALID_LANDING)
                    {
                        stDestination->ucLanding = (uint8_t) fl;
                        stDestination->eCallDir = DIR__DN;
                    }
                    //set direction and exit, else set floor and direction
                    else if (fl > stDestination->ucLanding)
                    {
                        stDestination->eCallDir = DIR__UP;
                    }
                    else if (fl == stDestination->ucLanding)
                    {
                        stDestination->eCallDir = DIR__DN;
                    }
                    break;
                }
            }
        }
    }
}
static void GetNextDestinationBelow_ByCar(  uint8_t ucCarIndex, uint8_t ucStartLanding, st_XRegDestination * stDestination)
{
    uint8_t bFullCallFound = 0;
    uint8_t ucTopLanding = XRegData_GetLastLanding( ucCarIndex );
    uint8_t ucBottomLanding = XRegData_GetFirstLanding( ucCarIndex );

    stDestination->ucLanding = INVALID_LANDING;
    stDestination->eCallDir  = DIR__NONE;

    //sanity check
    if (ucStartLanding < MAX_NUM_FLOORS
     && ucStartLanding <= ucTopLanding ){
        //scan below for calls
          for (int8_t fl = ucStartLanding; fl >= ucBottomLanding; fl--)
          {
            //if hall call,  if floor is set, set direction and exit, else set floor and direction
            uint8_t bLatchedDownCall = GetLatchedHallCallDown_Front_ByXRegCar(ucCarIndex, fl) || GetLatchedHallCallDown_Rear_ByXRegCar(ucCarIndex, fl);
            uint8_t bDownCallAllowed = (uint8_t) fl != ucBottomLanding; // Don't check up call at bottom terminal
            if ( bDownCallAllowed && bLatchedDownCall ) {
                if (stDestination->ucLanding == INVALID_LANDING)
                {
                    stDestination->ucLanding = (uint8_t) fl;
                    stDestination->eCallDir = DIR__DN;
                }
                else if (stDestination->eCallDir == DIR__NONE)
                {
                    stDestination->eCallDir = DIR__DN;
                }
                bFullCallFound = 1;
                break;
            }
        }
        //if only a car call is found in the direction, look for anything on the way back up to set direction.
        //if coming back up the floor is the same floor as the car call above, set direction to turn around.
        if (!bFullCallFound) {
            for (int8_t fl = ucBottomLanding; fl <= ucStartLanding; fl++) {

               uint8_t bLatchedUpCall = GetLatchedHallCallUp_Front_ByXRegCar(ucCarIndex, fl) || GetLatchedHallCallUp_Rear_ByXRegCar(ucCarIndex, fl);
               uint8_t bUpCallAllowed = (uint8_t) fl != ucTopLanding; // Don't check up call at top terminal
               if ( bUpCallAllowed && bLatchedUpCall ) {
                    //if hall call,  if floor is not set set direction and floor
                    if (stDestination->ucLanding == INVALID_LANDING)
                    {
                        stDestination->ucLanding = (uint8_t) fl;
                        stDestination->eCallDir = DIR__UP;
                    }
                    //set direction and exit, else set floor and direction
                    else if (fl < stDestination->ucLanding)
                    {
                        stDestination->eCallDir = DIR__DN;
                    }
                    else if (fl == stDestination->ucLanding)
                    {
                        stDestination->eCallDir = DIR__UP;
                    }
                    break;
                }
            }
        }
    }
}

/*----------------------------------------------------------------------------
    Update per car destination on:
     - Hall call change
     - Car destination change
 *----------------------------------------------------------------------------*/
static void UpdateXRegDestination_ByCar( uint8_t ucCarIndex )
{
   // If car is online & in correct mode.
   st_XRegDestination stDestination;
   stDestination.eCallDir = DIR__NONE;
   stDestination.ucLanding = INVALID_LANDING;
   stDestination.eDoor = DOOR_FRONT;
   stDestination.uiMask = 0;

   uint8_t ucCurrentLanding = XRegData_GetCurrentLanding( ucCarIndex );
   enum direction_enum eMotionDir = XRegData_GetMotionDirection( ucCarIndex );
   uint8_t ucLastLanding = XRegData_GetLastLanding( ucCarIndex );
   en_hc_dir ePriority = XRegData_GetPriority( ucCarIndex );

   if( ( eMotionDir == DIR__NONE )
    || ( Param_ReadValue_1Bit(enPARAM1__XREG_EnableInMotionAssignment) ) )
   {
      // If stopped, and serving UP calls
      if( ePriority == HC_DIR__UP )
      {
         GetNextDestinationAbove_ByCar( ucCarIndex, ucCurrentLanding, &stDestination);
         if (stDestination.ucLanding == INVALID_LANDING) {
            GetNextDestinationBelow_ByCar( ucCarIndex, ucCurrentLanding, &stDestination);
         }
      }
      // If stopped, and serving DOWN calls
      else if( ePriority == HC_DIR__DOWN )
      {
         GetNextDestinationBelow_ByCar( ucCarIndex, ucCurrentLanding, &stDestination);
         if (stDestination.ucLanding == INVALID_LANDING) {
            GetNextDestinationAbove_ByCar( ucCarIndex, ucCurrentLanding, &stDestination);
         }
      }
   }

   /* If destination is valid, update the structure */
   if( stDestination.ucLanding <= ucLastLanding )
   {
      // Answering HC UP
      if( stDestination.eCallDir == DIR__UP )
      {
         if(GetLatchedHallCallUp_Front_ByXRegCar(ucCarIndex, stDestination.ucLanding))
         {
            stDestination.eDoor = DOOR_FRONT;
            stDestination.uiMask = XRegData_GetHallMask_F(ucCarIndex);
         }
         else
         {
            stDestination.eDoor = DOOR_REAR;
            stDestination.uiMask = XRegData_GetHallMask_R(ucCarIndex);
         }
      }
      // Answering HC DN
      else if( stDestination.eCallDir == DIR__DN )
      {
         if(GetLatchedHallCallDown_Front_ByXRegCar(ucCarIndex, stDestination.ucLanding))
         {
            stDestination.eDoor = DOOR_FRONT;
            stDestination.uiMask = XRegData_GetHallMask_F(ucCarIndex);
         }
         else
         {
            stDestination.eDoor = DOOR_REAR;
            stDestination.uiMask = XRegData_GetHallMask_R(ucCarIndex);
         }
      }
   }
   SetXRegCarDestination(ucCarIndex, &stDestination);
}

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void UpdateXRegCarDestinations(void)
{
   for( uint8_t i = 0; i < MAX_GROUP_CARS; i++ )
   {
      if( XRegData_GetCarActive(i) )
      {
         if( XRegDestination_GetLanding(i) < MAX_NUM_FLOORS )
         {
            /* If a car has a current destination, check if it is still valid */
            st_XRegDestination *pstDest = GetXRegDestinationStructure(i);
            uint8_t bValid = 0;
            if( pstDest->eDoor == DOOR_REAR )
            {
               if( pstDest->eCallDir == DIR__UP )
               {
                  bValid = GetLatchedHallCallUp_Rear_ByXRegCar(i, pstDest->ucLanding);
               }
               else if( pstDest->eCallDir == DIR__DN )
               {
                  bValid = GetLatchedHallCallDown_Rear_ByXRegCar(i, pstDest->ucLanding);
               }
            }
            else if( pstDest->eDoor == DOOR_FRONT )
            {
               if( pstDest->eCallDir == DIR__UP )
               {
                  bValid = GetLatchedHallCallUp_Front_ByXRegCar(i, pstDest->ucLanding);
               }
               else if( pstDest->eCallDir == DIR__DN )
               {
                  bValid = GetLatchedHallCallDown_Front_ByXRegCar(i, pstDest->ucLanding);
               }
            }

            if(!bValid)
            {
               UpdateXRegDestination_ByCar(i);
            }
         }
         else
         {
            UpdateXRegDestination_ByCar(i);
         }
      }
      /* Clear out destination for any cars that have gone offline */
      else
      {
         ClrDestination_ByCar(i);
      }
   }
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
