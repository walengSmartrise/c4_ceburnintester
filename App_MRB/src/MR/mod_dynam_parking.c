/***
 *       _____                          __         _
 *      / ___/ ____ ___   ____ _ _____ / /_ _____ (_)_____ ___
 *      \__ \ / __ `__ \ / __ `// ___// __// ___// // ___// _ \
 *     ___/ // / / / / // /_/ // /   / /_ / /   / /(__  )/  __/
 *    /____//_/ /_/ /_/ \__,_//_/    \__//_/   /_//____/ \___/
 *        ______               _                           _
 *       / ____/____   ____ _ (_)____   ___   ___   _____ (_)____   ____ _
 *      / __/  / __ \ / __ `// // __ \ / _ \ / _ \ / ___// // __ \ / __ `/
 *     / /___ / / / // /_/ // // / / //  __//  __// /   / // / / // /_/ /
 *    /_____//_/ /_/ \__, //_//_/ /_/ \___/ \___//_/   /_//_/ /_/ \__, /
 *                  /____/                                       /____/
 */
/******************************************************************************
 *
 * @file
 * @brief    Dynamic Parking Module
 * @version
 * @date
 *
 * @note
 *
 ******************************************************************************/




/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sru_b.h"
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "shared_data.h"

#include "carData.h"
#include "carDestination.h"
#include "groupnet.h"
#include "groupnet_datagrams.h"
#include "group_net_data.h"
#include "XRegData.h"
#include "hallSecurity.h"
#include "masterCommand.h"
#include "dynamic_parking.h"
#include "shieldData.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
//#define SHIELD_DEBUG_PARK_TIMEOUT   // define for (debug) shield parking datagram timers

#ifdef SHIELD_DEBUG_PARK_TIMEOUT
#define DYNAMIC_PARKING_AGING_PERIOD      (60000)     // 1 minute
#endif

#define DYNAMIC_PARKING_UPDATE_PERIOD     (10000)
#define DOOR_OPEN_FLAG                    (0x80)
#define LANDING_MASK                      (0x7f)

#define NO_PRIORITY                       (0)
#define PREDICT_PRIORITY_BASE             (MAX_GROUP_CARS)
#define DYNAMIC_PRIORITY_LOCAL_BASE       (PREDICT_PRIORITY_BASE + MAX_GROUP_CARS)
#define DYNAMIC_GUI_CAR_PRIORITY          (DYNAMIC_PRIORITY_LOCAL_BASE + 1)

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_DyPark =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
#ifdef SHIELD_DEBUG_PARK_TIMEOUT
static   uint32_t ulAgePredictiveTimer;
static   uint32_t ulAgeDynamCarTimer;
#endif

static   uint8_t  aucLandings_Plus1[MAX_GROUP_CARS];    // current parking landings
static   uint8_t  st_LandingPriority[MAX_GROUP_CARS];   // Priority of parked cars

static uint8_t aucPredictiveParkingLandings_Plus1[MAX_GROUP_CARS];
static uint8_t aucDynamicParkingLandings_Plus1[MAX_GROUP_CARS];
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
void DynamicParking_UnloadPredictiveParkingDatagram( un_sdata_datagram *punDatagram )
{
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      aucPredictiveParkingLandings_Plus1[i] = punDatagram->auc8[i];
   }

#ifdef SHIELD_DEBUG_PARK_TIMEOUT
   // reset aging timer
   ulAgePredictiveTimer = 0;
#endif
}
void DynamicParking_UnloadDynamicParkingDatagram( un_sdata_datagram *punDatagram )
{
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      aucDynamicParkingLandings_Plus1[i] = punDatagram->auc8[i];
   }
#ifdef SHIELD_DEBUG_PARK_TIMEOUT
   // reset aging timer
   ulAgeDynamCarTimer = 0;
#endif
}
/*-----------------------------------------------------------------------------
   Function to get current parking assignments
 -----------------------------------------------------------------------------*/
uint8_t DynamicParking_GetLandingAssignment(uint8_t car)
{
   return aucLandings_Plus1[car];
}

/*-----------------------------------------------------------------------------
   Return floor based parking assignments for a specific car
 -----------------------------------------------------------------------------*/
uint8_t DynamicParking_GetFloorAssignment(void)
{
   uint8_t land2Floor;

   land2Floor = (aucLandings_Plus1[GetFP_GroupCarIndex()] & LANDING_MASK) - 1;
   land2Floor = GetCarMapping(land2Floor);
   return ((land2Floor + 1) | (aucLandings_Plus1[GetFP_GroupCarIndex()] & DOOR_OPEN_FLAG));
}
/*-----------------------------------------------------------------------------
   Update Car Dynamic Parking assignments
 -----------------------------------------------------------------------------*/
static void UpdateDynamicParking( void )
{
   st_cardata *pstCarData;

   if( GetMasterDispatcherFlag() )
   {
      uint8_t  car;
      uint8_t  dPark;
      uint8_t  currPark;
      uint8_t  landing;
      uint8_t  bDupPrior;
      uint8_t closeCar = MAX_GROUP_CARS;
      uint8_t lowPrioCar = MAX_GROUP_CARS;
      uint8_t minLandAway = INVALID_FLOOR;
      uint8_t minPriority = DYNAMIC_GUI_CAR_PRIORITY;

      // scan dynamic parking floors from gui
      for(car = 0; car < MAX_GROUP_CARS; car++)
      {
         pstCarData = GetCarDataStructure(car);

         /*** HOUSE KEEPING ***/
         // if car is parked on a different floor or offline
         /* If car is no longer parked, or has gone offline, clear its parking assignment */
         if (((aucLandings_Plus1[car] != 0) &&
             ((aucLandings_Plus1[car] & LANDING_MASK) - 1) != pstCarData->ucCurrentLanding) ||
             !GetCarOnlineFlag_ByCar(car) ||
             !pstCarData->bDynamicParking ||
             !pstCarData->bParking)
         {
            // clear parking record
            aucLandings_Plus1[car] = 0;
            st_LandingPriority[car] = NO_PRIORITY;
         }

         // get dynamic car parking location
         currPark = aucDynamicParkingLandings_Plus1[car];

         // if car park parameter is cleared
         if ((currPark == 0) && (st_LandingPriority[car] == DYNAMIC_GUI_CAR_PRIORITY))
         {
            // clear parking record
            aucLandings_Plus1[car] = 0;
            st_LandingPriority[car] = NO_PRIORITY;
         }

         // if invalid landing skip to next car
         if ((currPark & LANDING_MASK) == 0)
         {
            continue;
         }

         landing = (currPark & LANDING_MASK) - 1;

         // check if car is online
         if (GetCarOnlineFlag_ByCar(car))
         {
            // check if the remote car has Dynamic_Parking enabled, is idle
            // and has a door opening on the landing
            if ((pstCarData->bDynamicParking != 0) && 
                (pstCarData->bParking != 0) &&
                (CarData_CheckOpening(car, landing, DOOR_ANY) != 0))
            {
               st_LandingPriority[car] = DYNAMIC_GUI_CAR_PRIORITY;

               // save GUI Dynamic parking assignment for the car.
               aucLandings_Plus1[car] = currPark; 
            }
         }
      }

      // scan the dynamic park assignments
      for(dPark = 0; dPark < MAX_GROUP_CARS; dPark++)
      {
         // get dynamic car parking location
         currPark = Param_ReadValue_8Bit(enPARAM8__DynamicParkingLanding_1_Plus1 + dPark);

         // if invalid landing clear any old assignment and skip to next designation
         if ((currPark & LANDING_MASK) == 0)
         {
            // if any car parked with this priotity
            for(car = 0; car < MAX_GROUP_CARS; car++)
            {
               if (st_LandingPriority[car] == (DYNAMIC_PRIORITY_LOCAL_BASE - dPark))
               {
                  // clear parking record
                  aucLandings_Plus1[car] = 0;
                  st_LandingPriority[car] = NO_PRIORITY;
               }
            }

            // go to next
            continue;
         }

         // landing - 1
         landing = (currPark & LANDING_MASK) - 1;

         // check if this priority is already assigned
         for(car = 0, bDupPrior = 0; car < MAX_GROUP_CARS; car++)
         {
            if (st_LandingPriority[car] == (DYNAMIC_PRIORITY_LOCAL_BASE - dPark))
            {
               // landing for this priority changed - reassign this car
               if (landing != ((aucLandings_Plus1[car] & LANDING_MASK) - 1))
               {
                  // clear parking record
                  aucLandings_Plus1[car] = 0;
                  st_LandingPriority[car] = NO_PRIORITY;
               }

               else
               {
                  bDupPrior = 1;
               }
            }
         }

         // if so go to next priority
         if (bDupPrior)
         {
            continue;
         }

         // reset for closest car
         minLandAway = INVALID_FLOOR;
         minPriority = DYNAMIC_GUI_CAR_PRIORITY;
         closeCar = MAX_GROUP_CARS;
         lowPrioCar = MAX_GROUP_CARS;

         // find closest car ready for parking
         for(car = 0; car < MAX_GROUP_CARS; car++)
         {
            // check if car is online
            if (GetCarOnlineFlag_ByCar(car))
            {
               pstCarData = GetCarDataStructure(car);
            
               // check if the remote car has Dynamic_Parking enabled is idle
               // and has a door opening on the landing
               if ((pstCarData->bDynamicParking != 0) && 
                   (pstCarData->bParking != 0) &&
                   (CarData_CheckOpening(car, landing, DOOR_ANY) != 0))
               {
                  // only check distance for cars not assigned
                  if (aucLandings_Plus1[car] == 0)
                  {
                     if(landing > pstCarData->ucCurrentLanding)
                     {
                        if (minLandAway > (landing - pstCarData->ucCurrentLanding))
                        {
                           minLandAway = landing - pstCarData->ucCurrentLanding;
                           closeCar = car;
                        }
                     }
                     else
                     {
                        if (minLandAway > (pstCarData->ucCurrentLanding - landing))
                        {
                           minLandAway = pstCarData->ucCurrentLanding - landing;
                           closeCar = car;
                        }
                     }
                  }

                  // find parked car with lowest priority
                  if (minPriority > st_LandingPriority[car])
                  {
                     minPriority = st_LandingPriority[car];
                     lowPrioCar = car;
                  }
               }
            }
         }

         // we did not find an online, unparked car
         if (closeCar == MAX_GROUP_CARS)
         {
            // No parked car with lower priority
            if (lowPrioCar != MAX_GROUP_CARS)
            {
               // look at car with lowest priority
               closeCar = lowPrioCar;
            }

            // cannot park a car
            else
            {
               continue;
            }
         }

         // if higher priority parking is assigned to this car go to next park parameter
         if ( st_LandingPriority[closeCar] > (DYNAMIC_PRIORITY_LOCAL_BASE - dPark))
         {
            continue;
         }

         // we found a car to park not assigned or low priority
         if ((closeCar != MAX_GROUP_CARS) &&
             ( st_LandingPriority[closeCar] < (DYNAMIC_PRIORITY_LOCAL_BASE - dPark)))
         {
            st_LandingPriority[closeCar] = DYNAMIC_PRIORITY_LOCAL_BASE - dPark;

            // or in door open flag
            currPark |= Param_ReadValue_1Bit(enPARAM1__DynamicParkingDO_1 + dPark) ? DOOR_OPEN_FLAG : 0;

            // change landing to Plus1 and save.
            aucLandings_Plus1[closeCar] = currPark; 
         }
      }

      // Scan through predictive parking floors from gui
      for(dPark = 0; dPark < MAX_GROUP_CARS; dPark++)
      {
         // get dynamic car parking location
         currPark = aucPredictiveParkingLandings_Plus1[dPark];

         // if invalid landing clear any old assignment and skip to next designation
         if ((currPark & LANDING_MASK) == 0)
         {
            // if any car parked with this priotity
            for(car = 0; car < MAX_GROUP_CARS; car++)
            {
               if (st_LandingPriority[car] == (PREDICT_PRIORITY_BASE - dPark))
               {
                  // clear parking record
                  aucLandings_Plus1[car] = 0;
                  st_LandingPriority[car] = NO_PRIORITY;
               }
            }

            // go to next
            continue;
         }

         // landing - 1
         landing = (currPark & LANDING_MASK) - 1;

         // check if this priority is already assigned
         for(car = 0, bDupPrior = 0; car < MAX_GROUP_CARS; car++)
         {
            if (st_LandingPriority[car] == (PREDICT_PRIORITY_BASE - dPark))
            {
               // landing for this priority changed - reassign this car
               if (landing != ((aucLandings_Plus1[car] & LANDING_MASK) - 1))
               {
                  // clear parking record
                  aucLandings_Plus1[car] = 0;
                  st_LandingPriority[car] = NO_PRIORITY;
               }

               else
               {
                  bDupPrior = 1;
               }
            }
         }

         // if so go to next priority
         if (bDupPrior)
         {
            continue;
         }

         // reset for closest car
         minLandAway = INVALID_FLOOR;
         minPriority = DYNAMIC_PRIORITY_LOCAL_BASE;
         closeCar = MAX_GROUP_CARS;
         lowPrioCar = MAX_GROUP_CARS;

         // find closest car ready for parking
         for(car = 0; car < MAX_GROUP_CARS; car++)
         {
            // check if car is online
            if (GetCarOnlineFlag_ByCar(car))
            {
               pstCarData = GetCarDataStructure(car);
            
               // check if the remote car has Dynamic_Parking enabled is idle
               // and has a door opening on the landing
               if ((pstCarData->bDynamicParking != 0) && 
                   (pstCarData->bParking != 0) &&
                   (CarData_CheckOpening(car, landing, DOOR_ANY) != 0))
               {
                  // only check distance for cars not assigned
                  if (aucLandings_Plus1[car] == 0)
                  {
                     if(landing > pstCarData->ucCurrentLanding)
                     {
                        if (minLandAway > (landing - pstCarData->ucCurrentLanding))
                        {
                           minLandAway = landing - pstCarData->ucCurrentLanding;
                           closeCar = car;
                        }
                     }
                     else
                     {
                        if (minLandAway > (pstCarData->ucCurrentLanding - landing))
                        {
                           minLandAway = pstCarData->ucCurrentLanding - landing;
                           closeCar = car;
                        }
                     }
                  }

                  // find parked car with lowest priority
                  if (minPriority >= st_LandingPriority[car])
                  {
                     minPriority = st_LandingPriority[car];
                     lowPrioCar = car;
                  }
               }
            }
         }

         // we did not find an online, unparked car
         if (closeCar == MAX_GROUP_CARS)
         {
            // No parked car with lower priority
            if (lowPrioCar != MAX_GROUP_CARS)
            {
               // look at car with lowest priority
               closeCar = lowPrioCar;
            }

            // cannot park a car
            else
            {
               continue;
            }
         }

         // if higher priority parking is assigned to this car go to next park parameter
         if ( st_LandingPriority[closeCar] > (PREDICT_PRIORITY_BASE - dPark))
         {
            continue;
         }

        // we found a car to park not assigned or low priority
         if ((closeCar != MAX_GROUP_CARS) &&
             ( st_LandingPriority[closeCar] < (PREDICT_PRIORITY_BASE - dPark)))
         {
            st_LandingPriority[closeCar] = PREDICT_PRIORITY_BASE - dPark;

            // change landing to Plus1 and save.
            aucLandings_Plus1[closeCar] = aucPredictiveParkingLandings_Plus1[dPark]; 
         }
      }
   }
}
/*-----------------------------------------------------------------------------
   Set Car Dynamic Parking assignments
 -----------------------------------------------------------------------------*/
static void LoadDynamicParking( void )
{
   static uint16_t uwUpdateDelay_ms;
   un_sdata_datagram unDatagram;

   if( GetMasterDispatcherFlag() )
   {
      // scan the cars
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         // clear assignment before checking car status
         unDatagram.auc8[i] = aucLandings_Plus1[i];
      }

      // at least one car is ready for dispatch
      SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__DynamicParking, &unDatagram);

      /* Added a minimum update rate for unchanged packet */
      if(!SDATA_DirtyBit_Get(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__DynamicParking))
      {
         if( uwUpdateDelay_ms < DYNAMIC_PARKING_UPDATE_PERIOD )
         {
            uwUpdateDelay_ms += gstMod_DyPark.uwRunPeriod_1ms;
         }
         else
         {
            uwUpdateDelay_ms = 0;
            SDATA_DirtyBit_Set(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__DynamicParking);
         }
      }
      else
      {
         uwUpdateDelay_ms = 0;
      }
   }
   else
   {
      SDATA_DirtyBit_Clr(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__DynamicParking);
   }
}

/*-----------------------------------------------------------------------------
   Age out dynamic parking Datagrams
 -----------------------------------------------------------------------------*/
static void AgeDynamicParking( void )
{
   // check if shield is offline
   if (ShieldData_CheckTimeoutCounter())
   {
#ifdef SHIELD_DEBUG_PARK_TIMEOUT
      // check timer for predictive datagram
      if (ulAgePredictiveTimer < DYNAMIC_PARKING_AGING_PERIOD)
      {
         // age this timer
         ulAgePredictiveTimer += gstMod_DyPark.uwRunPeriod_1ms;
      }
      else
      {
         // clear the datagram
         memset(aucPredictiveParkingLandings_Plus1, 0, MAX_GROUP_CARS);
      }
      
      // check timer for car park datagram
      if (ulAgeDynamCarTimer < DYNAMIC_PARKING_AGING_PERIOD)
      {
         // age this timer
         ulAgeDynamCarTimer += gstMod_DyPark.uwRunPeriod_1ms;
      }
      else
      {
         // clear the datagram
         memset(aucDynamicParkingLandings_Plus1, 0, MAX_GROUP_CARS);
      }
#else
      // if shield offline clear datagram buffers
      memset(aucPredictiveParkingLandings_Plus1, 0, MAX_GROUP_CARS);
      memset(aucDynamicParkingLandings_Plus1, 0, MAX_GROUP_CARS);
#endif
   }
}
/*-----------------------------------------------------------------------------
   
 -----------------------------------------------------------------------------*/
void UnloadDatagram_DynamicParking(enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram)
{
   if( CarData_GetMasterDispatcherFlag(eCarID) && !GetMasterDispatcherFlag())
   {
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         aucLandings_Plus1[i] = punDatagram->auc8[i];
      }
   }
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   //------------------------------------------------

   pstThisModule->uwInitialDelay_1ms = 10000;
   pstThisModule->uwRunPeriod_1ms = 50;

   memset(&aucLandings_Plus1, 0, MAX_GROUP_CARS);
   memset(&st_LandingPriority, 0, MAX_GROUP_CARS);
   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{

   UpdateDynamicParking();

   LoadDynamicParking();

   AgeDynamicParking();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

