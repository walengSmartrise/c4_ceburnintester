
/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief
 * @version  V1.00
 * @date     27, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>

#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "hallSecurity.h"
#include "group_net_data.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
static st_riser_board_data stRiserData;
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void Initialize_RiserData()
{
   for( uint8_t i = 0; i < MAX_NUM_RISER_BOARDS; i++ )
   {
      stRiserData.auwOfflineCounter_ms[i] = RISER_OFFLINE_COUNT_MS__UNUSED;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void ClrRiserOfflineCounter( uint8_t ucRiserIndex )
{
   stRiserData.auwOfflineCounter_ms[ucRiserIndex] = 0;
}

/*-----------------------------------------------------------------------------
Access functions
 -----------------------------------------------------------------------------*/
uint8_t RiserData_GetInputs( uint8_t ucRiserIndex )
{
   return stRiserData.aucInputs[ucRiserIndex];
}
uint8_t RiserData_GetOutputs( uint8_t ucRiserIndex )
{
   return stRiserData.aucOutputs[ucRiserIndex];
}
en_ris_exp_error RiserData_GetError( uint8_t ucRiserIndex )
{
   return stRiserData.aeError[ucRiserIndex];
}
uint16_t RiserData_GetOfflineCounter( uint8_t ucRiserIndex )
{
   return stRiserData.auwOfflineCounter_ms[ucRiserIndex];
}
char * RiserData_GetVersionString( uint8_t ucRiserIndex )
{
   return &stRiserData.aaucVersion[ucRiserIndex][0];
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UnloadDatagram_RiserOutputs()
{
   stRiserData.aucOutputs[0] = GetRiserBoardOutputs_Output1_MRA();
   stRiserData.aucOutputs[1] = GetRiserBoardOutputs_Output2_MRA();
   stRiserData.aucOutputs[2] = GetRiserBoardOutputs_Output3_MRA();
   stRiserData.aucOutputs[3] = GetRiserBoardOutputs_Output4_MRA();
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void LoadDatagram_RiserOutputs( un_sdata_datagram *punDatagram )
{
   memset(punDatagram, 0, sizeof(un_sdata_datagram) );
   for( uint8_t i = 0; i < MAX_NUM_RISER_BOARDS; i++ )
   {
      punDatagram->auc8[i] = stRiserData.aucOutputs[i];
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void LoadDatagram_RiserInputs( un_sdata_datagram *punDatagram )
{
   memset(punDatagram, 0, sizeof(un_sdata_datagram) );
   for( uint8_t i = 0; i < MAX_NUM_RISER_BOARDS; i++ )
   {
      punDatagram->auc8[i] = stRiserData.aucInputs[i];
   }
   punDatagram->auc8[4] = Dispatch_GetEmergencyDispatchFlag() & 1;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UnloadDatagram_RiserStatus( uint8_t ucRiserIndex, un_sdata_datagram *punDatagram )
{
   stRiserData.aucInputs[ucRiserIndex] = punDatagram->auc8[0];
   stRiserData.aeError[ucRiserIndex] = punDatagram->auc8[1];
   stRiserData.auwOfflineCounter_ms[ucRiserIndex] = 0;

   stRiserData.aaucVersion[ucRiserIndex][0] = punDatagram->auc8[4];
   stRiserData.aaucVersion[ucRiserIndex][1] = punDatagram->auc8[5];
   stRiserData.aaucVersion[ucRiserIndex][2] = punDatagram->auc8[6];
   stRiserData.aaucVersion[ucRiserIndex][3] = punDatagram->auc8[7];
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void CheckFor_RiserBoardFault( struct st_module *pstThisModule )
{
   for( uint8_t i = 0; i < MAX_NUM_RISER_BOARDS; i++ )
   {
      if( stRiserData.auwOfflineCounter_ms[i] != RISER_OFFLINE_COUNT_MS__UNUSED )
      {
         en_alarms eAlarm;
         en_alarms eAlarmLimit;
         switch(i)
         {
            case 0:
               eAlarm = ALM__RIS1_OFFLINE;
               eAlarmLimit = ALM__RIS1_BUS_INV_MSG_2;
               break;
            case 1:
               eAlarm = ALM__RIS2_OFFLINE;
               eAlarmLimit = ALM__RIS2_BUS_INV_MSG_2;
               break;
            case 2:
               eAlarm = ALM__RIS3_OFFLINE;
               eAlarmLimit = ALM__RIS3_BUS_INV_MSG_2;
               break;
            case 3:
               eAlarm = ALM__RIS4_OFFLINE;
               eAlarmLimit = ALM__RIS4_BUS_INV_MSG_2;
               break;
            default:
               break;
         }

         if( stRiserData.auwOfflineCounter_ms[i] >= RISER_OFFLINE_COUNT_MS__FAULTED )
         {
            /* The first time this riser has been flagged as offline... */
            if( stRiserData.aeError[i] != ERROR_RIS_EXP__UNKNOWN )
            {
               /* GROUP_NET__RIS_3 is the security riser.
                * HC security for front and rear openings
                * are encoded in UB and DB inputs respectively. */
               uint8_t ucSecurityRiserIndex = SECURITY_RISER_INDEX;
               if(i == ucSecurityRiserIndex)
               {
                  ResetHallSecurity();
               }
            }

            /* Clear inputs if offline */
            stRiserData.aucInputs[i] = 0;
            stRiserData.aeError[i] = ERROR_RIS_EXP__UNKNOWN;

            SetAlarm(eAlarm);
         }
         else
         {
            stRiserData.auwOfflineCounter_ms[i] += pstThisModule->uwRunPeriod_1ms;
            if( ( Param_ReadValue_1Bit(enPARAM1__EnableRiserAlarms) )
             && ( stRiserData.aeError[i] != ERROR_RIS_EXP__NONE ) )
            {
               if( stRiserData.aeError[i] == ERROR_RIS_EXP__UNKNOWN )
               {
                  eAlarm += 1;
                  if( eAlarm > eAlarmLimit )
                  {
                     eAlarm = eAlarmLimit;
                  }
               }
               else if( stRiserData.aeError[i] == ERROR_RIS_EXP__HALL_BOARD_OFFLINE )
               {
                  eAlarm = ALM__RIS1_HB_OFFLINE+i;
               }
               else
               {
                  eAlarm += stRiserData.aeError[i] - ERROR_RIS_EXP__NONE + 1;
                  if( eAlarm > eAlarmLimit )
                  {
                     eAlarm = eAlarmLimit;
                  }
               }

               SetAlarm(eAlarm);
            }
         }
      }
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
