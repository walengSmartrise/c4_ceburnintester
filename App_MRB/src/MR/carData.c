/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief
 * @version  V1.00
 * @date     27, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>

#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "carData.h"
#include "carDestination.h"
#include "group_net_data.h"
#include "dispatching.h"
#include "remoteCommands.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static st_cardata astCarData[MAX_GROUP_CARS];

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Initialize car data
 -----------------------------------------------------------------------------*/
void Init_CarDataStructure()
{
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      astCarData[i].uwOfflineCounter_ms = GROUP_CAR_INVALID_TIMER_MS;
   }
}
void Init_CarDataStructure_ByCar(enum en_group_net_nodes eCarID)
{
   memset(&astCarData[eCarID], 0, sizeof(st_cardata));
   astCarData[eCarID].uwOfflineCounter_ms = GROUP_CAR_INVALID_TIMER_MS;
}

/*-----------------------------------------------------------------------------
   Access functions
 -----------------------------------------------------------------------------*/
st_cardata * GetCarDataStructure(enum en_group_net_nodes eCarID)
{
   return &astCarData[eCarID];
}
uint8_t CarData_GetMasterDispatcherFlag(enum en_group_net_nodes eCarID)
{
   return astCarData[eCarID].bMasterDispatcher;
}
uint8_t CarData_GetDoorOpen_Front( enum en_group_net_nodes eCarID )
{
   uint8_t bOpen = 0;
   if( ( astCarData[eCarID].eDoor_F == DOOR__OPENING )
    || ( astCarData[eCarID].eDoor_F == DOOR__OPEN ) )
   {
      bOpen = 1;
   }
   return bOpen;
}
uint8_t CarData_GetDoorOpen_Rear( enum en_group_net_nodes eCarID )
{
   uint8_t bOpen = 0;
   if( ( astCarData[eCarID].eDoor_R == DOOR__OPENING )
    || ( astCarData[eCarID].eDoor_R == DOOR__OPEN ) )
   {
      bOpen = 1;
   }
   return bOpen;
}
//Returns 1 if valid opening exists
uint8_t CarData_CheckOpening( enum en_group_net_nodes eCarID, uint8_t ucLanding, enum en_doors eDoor )
{
   uint8_t bValid = 0;
   //Assumes valid door and car id
   if( ucLanding <= astCarData[eCarID].ucLastLanding )
   {
      if( eDoor == DOOR_FRONT )
      {
         bValid = Sys_Bit_Get(&astCarData[eCarID].auiBF_OpeningMap_F[0], ucLanding);
      }
      else if( eDoor == DOOR_REAR )
      {
         bValid = Sys_Bit_Get(&astCarData[eCarID].auiBF_OpeningMap_R[0], ucLanding);
      }
      else
      {
         bValid = Sys_Bit_Get(&astCarData[eCarID].auiBF_OpeningMap_F[0], ucLanding)
                | Sys_Bit_Get(&astCarData[eCarID].auiBF_OpeningMap_R[0], ucLanding);
      }
   }
   return bValid;
}

uint32_t CarData_GetHallMask( enum en_group_net_nodes eCarID, enum en_doors eDoor )
{
   uint32_t uiMask = 0;
   if( eDoor == DOOR_FRONT )
   {
      uiMask = astCarData[eCarID].uiHallMask_F;
   }
   else if( eDoor == DOOR_REAR )
   {
      uiMask = astCarData[eCarID].uiHallMask_R;
   }
   else
   {
      uiMask = astCarData[eCarID].uiHallMask_F;
      uiMask |= astCarData[eCarID].uiHallMask_R;
   }

   return uiMask;
}

uint32_t CarData_VIP_GetHallMask( enum en_group_net_nodes eCarID, enum en_doors eDoor )
{
   uint32_t uiMask = 0;
   if( eDoor == DOOR_FRONT )
   {
      uiMask = astCarData[eCarID].uiVIPMask_F;
   }
   else if( eDoor == DOOR_REAR )
   {
      uiMask = astCarData[eCarID].uiVIPMask_R;
   }
   else
   {
      uiMask = astCarData[eCarID].uiVIPMask_F;
      uiMask |= astCarData[eCarID].uiVIPMask_R;
   }

   return uiMask;
}


uint8_t CarData_GetFastResend(void)
{
   uint8_t bReturn = 0;
   bReturn |= astCarData[GetFP_GroupCarIndex()].bFastGroupResend;
   return bReturn;
}

uint8_t GetInputValue_ByCar( enum en_group_net_nodes eCarID, enum en_input_functions enInput )
{
   uint8_t bReturn = 0;
   if( enInput < NUM_INPUT_FUNCTIONS )
   {
      bReturn = Sys_Bit_Get(&astCarData[eCarID].auiBF_InputMap[0], enInput);
   }
   return bReturn;
}
uint8_t GetOutputValue_ByCar( enum en_group_net_nodes eCarID, enum en_output_functions enOutput )
{
   uint8_t bReturn = 0;
   if( enOutput < NUM_OUTPUT_FUNCTIONS )
   {
      bReturn = Sys_Bit_Get(&astCarData[eCarID].auiBF_OutputMap[0], enOutput);
   }
   return bReturn;
}

/* Flag marking if a car has a 96 floor limit instead of the usual 64 limit (enPARAM1__EnableExtFloorLimit).
 * This option is not needed if 12-DIP hall boards are used and is only necessary for jobs with
 * greater than 64 floors and 8 or 10 DIP style hall boards (!enPARAM1__EnableExtHallBoards). */
uint8_t CarData_GetExtFloorLimitFlag( enum en_group_net_nodes eCarID )
{
   st_cardata *pstCarData = GetCarDataStructure(eCarID);
   return pstCarData->bExtFloorLimit;
}
uint8_t CarData_GetExtFloorLimitFlag_AnyCar(void)
{
   uint8_t bExtFloorLimit = 0;
   for( uint8_t i = 0; i < MAX_GROUP_CARS; i++ )
   {
      if( CarData_GetExtFloorLimitFlag(i) )
      {
         bExtFloorLimit = 1; break;
      }
   }
   return bExtFloorLimit;
}
/*----------------------------------------------------------------------------
   Returns a single mask with the active hall mask for each group car.
   If ucLanding is INVALID_LANDING, then the mask is not floor specific
 *----------------------------------------------------------------------------*/
uint32_t CarData_GetGroupHallCallMask( uint8_t ucLanding, enum en_doors eDoor )
{
   uint32_t uiMask = 0;
   if( ucLanding == INVALID_LANDING )
   {
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         if(GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS)
         {
            st_cardata *pstCarData = GetCarDataStructure(i);
            if( eDoor == DOOR_ANY )
            {
               uiMask |= ( pstCarData->uiLatchableHallMask_F | pstCarData->uiLatchableHallMask_R );
            }
            else if( eDoor == DOOR_FRONT )
            {
               uiMask |= ( pstCarData->uiLatchableHallMask_F );
            }
            else
            {
               uiMask |= ( pstCarData->uiLatchableHallMask_R );
            }
         }
      }
   }
   else
   {
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         if(GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS)
         {
            if(CarData_CheckOpening(i, ucLanding, eDoor))
            {
               st_cardata *pstCarData = GetCarDataStructure(i);
               if( eDoor == DOOR_ANY )
               {
                  uiMask |= ( pstCarData->uiLatchableHallMask_F | pstCarData->uiLatchableHallMask_R );
               }
               else if( eDoor == DOOR_FRONT )
               {
                  uiMask |= ( pstCarData->uiLatchableHallMask_F );
               }
               else
               {
                  uiMask |= ( pstCarData->uiLatchableHallMask_R );
               }
            }
         }
      }
   }

   return uiMask;
}
/*----------------------------------------------------------------------------
   Returns a single mask with the active hall mask for each group car.
   If ucLanding is INVALID_LANDING, then the mask is not floor specific
   Returns the mask for latchable EMS emergency medical recall calls.
 *----------------------------------------------------------------------------*/
uint8_t CarData_GetMedicalHallCallMask( uint8_t ucLanding, enum en_doors eDoor )
{
   uint8_t ucMask = 0;
   if( ucLanding == INVALID_LANDING )
   {
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         if(GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS)
         {
            st_cardata *pstCarData = GetCarDataStructure(i);
            if( !pstCarData->ucMedicalLanding_Plus1 ) /* If no medical landing is specified, the mask applies to any landing. */
            {
               ucMask |= pstCarData->ucMedicalMask;
            }
         }
      }
   }
   else
   {
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         if(GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS)
         {
            if(CarData_CheckOpening(i, ucLanding, eDoor))
            {
               st_cardata *pstCarData = GetCarDataStructure(i);
               /* EMS medical emergency recall support */
               if( pstCarData->ucMedicalLanding_Plus1 )
               {
                  uint8_t ucMedicalLanding = pstCarData->ucMedicalLanding_Plus1-1;
                  if( ucMedicalLanding == ucLanding )
                  {
                     ucMask |= pstCarData->ucMedicalMask;
                  }
               }
               else /* If no medical landing is specified, the mask applies to any landing. */
               {
                  ucMask |= pstCarData->ucMedicalMask;
               }
            }
         }
      }
   }

   return ucMask;
}
/*-----------------------------------------------------------------------------
   Update car offline timer
 -----------------------------------------------------------------------------*/
void IncrementAllCarOfflineTimers( struct st_module *pstThisModule )
{
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      if( astCarData[i].uwOfflineCounter_ms < GROUP_CAR_OFFLINE_TIMER_MS )
      {
         astCarData[i].uwOfflineCounter_ms += pstThisModule->uwRunPeriod_1ms;
      }
   }
}
void ClrCarOfflineTimer_ByCar( enum en_group_net_nodes eCarID )
{
   astCarData[eCarID].uwOfflineCounter_ms = 0;
}
uint16_t GetCarOfflineTimer_ByCar( enum en_group_net_nodes eCarID )
{
   return astCarData[eCarID].uwOfflineCounter_ms;
}
uint8_t GetCarOnlineFlag_ByCar( enum en_group_net_nodes eCarID )
{
   return ( astCarData[eCarID].uwOfflineCounter_ms < GROUP_CAR_OFFLINE_TIMER_MS );
}
en_car_request GetCarRequest_ByCar( enum en_group_net_nodes eCarID )
{
   return astCarData[eCarID].eCarRequest;
}
en_car_request GetCarRequest_AnyCar(void)
{
   en_car_request eRequest = CAR_REQUEST__NONE;
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      if(astCarData[i].eCarRequest)
      {
         eRequest = astCarData[i].eCarRequest;
         break;
      }
   }
   return eRequest;
}

/*----------------------------------------------------------------------------
   Returns 1 if car is master dispatcher
 *----------------------------------------------------------------------------*/
uint8_t GetMasterDispatcherFlag()
{
   enum en_group_net_nodes eCarID = GetFP_GroupCarIndex();
   return astCarData[eCarID].bMasterDispatcher;
}
void UpdateMasterDispatcherFlag()
{
   enum en_group_net_nodes eCarID = GetFP_GroupCarIndex();
   astCarData[eCarID].bMasterDispatcher = 1;
   /* If no cars with lower ID are online, this car is the master dispatcher */
   for(uint8_t i = 0; i < eCarID; i++)
   {
      if(GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS)
      {
         astCarData[eCarID].bMasterDispatcher = 0;
      }
   }
}
uint8_t GetMasterDispatcherFlag_ByCar( enum en_group_net_nodes eCarID )
{
   return astCarData[eCarID].bMasterDispatcher;
}
/*----------------------------------------------------------------------------
   Defines a unique hall mask for each car, combined with the car's parameter controlled mask
   Used for assigning DD panel calls to specific cars
 *----------------------------------------------------------------------------*/
uint32_t CarData_GetUniqueHallMask_Front( enum en_group_net_nodes eCarID )
{
   uint32_t uiMask = 0;
   switch(eCarID)
   {
      case 0:
         uiMask = UNIQUE_HALL_MASK_F__CAR1;
         break;
      case 1:
         uiMask = UNIQUE_HALL_MASK_F__CAR2;
         break;
      case 2:
         uiMask = UNIQUE_HALL_MASK_F__CAR3;
         break;
      case 3:
         uiMask = UNIQUE_HALL_MASK_F__CAR4;
         break;
      case 4:
         uiMask = UNIQUE_HALL_MASK_F__CAR5;
         break;
      case 5:
         uiMask = UNIQUE_HALL_MASK_F__CAR6;
         break;
      case 6:
         uiMask = UNIQUE_HALL_MASK_F__CAR7;
         break;
      case 7:
         uiMask = UNIQUE_HALL_MASK_F__CAR8;
         break;
      default: break;
   }
   return uiMask;
}
uint32_t CarData_GetUniqueHallMask_Rear( enum en_group_net_nodes eCarID )
{
   uint32_t uiMask = 0;
   if(GetFP_RearDoors())
   {
      switch(eCarID)
      {
         case 0:
            uiMask = UNIQUE_HALL_MASK_R__CAR1;
            break;
         case 1:
            uiMask = UNIQUE_HALL_MASK_R__CAR2;
            break;
         case 2:
            uiMask = UNIQUE_HALL_MASK_R__CAR3;
            break;
         case 3:
            uiMask = UNIQUE_HALL_MASK_R__CAR4;
            break;
         case 4:
            uiMask = UNIQUE_HALL_MASK_R__CAR5;
            break;
         case 5:
            uiMask = UNIQUE_HALL_MASK_R__CAR6;
            break;
         case 6:
            uiMask = UNIQUE_HALL_MASK_R__CAR7;
            break;
         case 7:
            uiMask = UNIQUE_HALL_MASK_R__CAR8;
            break;
         default: break;
      }
   }
   return uiMask;
}
/*-----------------------------------------------------------------------------
   Returns the highest active landing
 -----------------------------------------------------------------------------*/
uint8_t CarData_GetHighestActiveLanding()
{
   uint8_t ucLanding = 0;
   for(uint8_t ucCar = 0; ucCar < MAX_GROUP_CARS; ucCar++)
   {
      if( ucLanding < astCarData[ucCar].ucLastLanding )
      {
         ucLanding = astCarData[ucCar].ucLastLanding;
      }
   }
   return ucLanding;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void LoadDatagram_CarStatus( un_sdata_datagram *punDatagram )
{
   enum en_group_net_nodes eCarID = GetFP_GroupCarIndex();
   punDatagram->auc8[0] = astCarData[eCarID].ucCurrentLanding;
   punDatagram->auc8[1] = astCarData[eCarID].ucDestinationLanding;
   punDatagram->auc8[2] = astCarData[eCarID].ucReachableLanding;
   punDatagram->auc8[3] = astCarData[eCarID].eDoor_F;
   punDatagram->auc8[4] = astCarData[eCarID].eDoor_R;
   punDatagram->auc8[5] = astCarData[eCarID].cMotionDir;
   punDatagram->auc8[6] = astCarData[eCarID].enPriority;
   punDatagram->auc8[7] = ( astCarData[eCarID].bFastGroupResend )
                        | ( astCarData[eCarID].bIdleDirection << 1 )
                        | ( astCarData[eCarID].bInSlowdown << 2 )
                        | ( astCarData[eCarID].bSuppressReopen << 3 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define CAR_DATA_HALL_MASK_RESEND_TIMER_5MS        (400)
#define CAR_DATA_DEFAULT_RESEND_TIMER_5MS          (2000)
#define CAR_DATA_INVALID_RESEND_TIMER_5MS          (0xFFFF)
uint8_t LoadDatagram_CarData( un_sdata_datagram *punDatagram )
{
   uint8_t bSend = 0;

   static uint32_t auiLastData[NUM_CAR_DATA_PACKETS];
   static uint16_t auwResendTimer_5ms[NUM_CAR_DATA_PACKETS];
   static uint8_t aucDirtyBits[BITMAP8_SIZE(NUM_CAR_DATA_PACKETS)];
   static en_car_data_packets enLastPacket;
   enum en_group_net_nodes eCarID = GetFP_GroupCarIndex();

   /* Flag packets for resend if they have changed.
    * Update minimum resend timers. */
   uint32_t uiData;
   for( uint8_t i = 0; i < NUM_CAR_DATA_PACKETS; i++ )
   {
      uint16_t uwTimerLimit_5ms = CAR_DATA_INVALID_RESEND_TIMER_5MS;
      switch(i)
      {
         case CAR_DATA_PACKET__OPENING_F_1:
            uiData = astCarData[eCarID].auiBF_OpeningMap_F[0];
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__OPENING_F_2:
            uiData = astCarData[eCarID].auiBF_OpeningMap_F[1];
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__OPENING_F_3:
            uiData = astCarData[eCarID].auiBF_OpeningMap_F[2];
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__OPENING_R_1:
            uiData = astCarData[eCarID].auiBF_OpeningMap_R[0];
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__OPENING_R_2:
            uiData = astCarData[eCarID].auiBF_OpeningMap_R[1];
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__OPENING_R_3:
            uiData = astCarData[eCarID].auiBF_OpeningMap_R[2];
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__HALLMASK_F:
            uiData = astCarData[eCarID].uiHallMask_F;
            uwTimerLimit_5ms = CAR_DATA_HALL_MASK_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__HALLMASK_R:
            uiData = astCarData[eCarID].uiHallMask_R;
            uwTimerLimit_5ms = CAR_DATA_HALL_MASK_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__LATCHABLE_HALLMASK_F:
            uiData = astCarData[eCarID].uiLatchableHallMask_F;
            uwTimerLimit_5ms = CAR_DATA_HALL_MASK_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__PAIRED_HB_MASK:
            uiData = astCarData[eCarID].aucPairedHBMask[0];
            uiData |= astCarData[eCarID].aucPairedHBMask[1] << 8;
            uiData |= astCarData[eCarID].aucPairedHBMask[2] << 16;
            uiData |= astCarData[eCarID].aucPairedHBMask[3] << 24;
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__SECURITY_F_1:
            uiData = astCarData[eCarID].auiBF_SecurityMap_F[0];
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__SECURITY_F_2:
            uiData = astCarData[eCarID].auiBF_SecurityMap_F[1];
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__SECURITY_F_3:
            uiData = astCarData[eCarID].auiBF_SecurityMap_F[2];
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__SECURITY_R_1:
            uiData = astCarData[eCarID].auiBF_SecurityMap_R[0];
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__SECURITY_R_2:
            uiData = astCarData[eCarID].auiBF_SecurityMap_R[1];
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__SECURITY_R_3:
            uiData = astCarData[eCarID].auiBF_SecurityMap_R[2];
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__TIME:
            uiData = astCarData[eCarID].ucMaxFloorToFloorTime_sec;
            uiData |= astCarData[eCarID].ucDoorDwellTime_sec << 8;
            uiData |= astCarData[eCarID].ucDoorDwellHallTime_sec << 16;
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__HALL_SEC_1:
            uiData = astCarData[eCarID].auiBF_HallSecurityMap[0];
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__HALL_SEC_2:
            uiData = astCarData[eCarID].auiBF_HallSecurityMap[1];
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__HALL_SEC_3:
            uiData = astCarData[eCarID].auiBF_HallSecurityMap[2];
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__REQUEST:
            uiData = astCarData[eCarID].eCarRequest;
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__HALL_SEC_MASK:
            uiData = astCarData[eCarID].ucHallSecurityMask_F;
            uiData |= astCarData[eCarID].ucHallSecurityMask_R << 8;
            uiData |= astCarData[eCarID].bEnableHallCallSecurity << 31;
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__LATCHABLE_HALLMASK_R:
            uiData = astCarData[eCarID].uiLatchableHallMask_R;
            uwTimerLimit_5ms = CAR_DATA_HALL_MASK_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__HALL_MEDICAL:
            uiData = astCarData[eCarID].ucMedicalMask;
            uiData |= astCarData[eCarID].ucMedicalLanding_Plus1 << 8;
            // 2 Byte unused
            uwTimerLimit_5ms = CAR_DATA_HALL_MASK_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__FLAGS:
            uiData = ( astCarData[eCarID].bDynamicParking )
                   | ( astCarData[eCarID].bParking << 1 )
                   | ( astCarData[eCarID].bExtFloorLimit << 2 );
            uiData |= astCarData[eCarID].bVIP << 3;
            uiData |= astCarData[eCarID].ucbClosestCar << 4;
            uiData |= astCarData[eCarID].bCarReady << 5;
            uiData |= astCarData[eCarID].bCarCapture << 6;
            uiData |= astCarData[eCarID].bFaulted << 7;
            uwTimerLimit_5ms = CAR_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case CAR_DATA_PACKET__VIPMASK:
            uiData = astCarData[eCarID].uiVIPMask_F;
            uiData |= astCarData[eCarID].uiVIPMask_R << 8;;
            uwTimerLimit_5ms = CAR_DATA_HALL_MASK_RESEND_TIMER_5MS;
            break;
         default:
            break;
      }

      if( uwTimerLimit_5ms == CAR_DATA_INVALID_RESEND_TIMER_5MS )
      {
         Sys_Bit_Set(&aucDirtyBits[0], i, 0);
      }
      else if( uiData != auiLastData[i] )
      {
         auiLastData[i] = uiData;
         Sys_Bit_Set(&aucDirtyBits[0], i, 1);
      }

      if( ( uwTimerLimit_5ms != CAR_DATA_INVALID_RESEND_TIMER_5MS )
       && ( auwResendTimer_5ms[i] > uwTimerLimit_5ms ) )
      {
         Sys_Bit_Set(&aucDirtyBits[0], i, 1);
      }
      else
      {
         auwResendTimer_5ms[i]++;
      }
   }

   /* Fetch next packet to send */
   en_car_data_packets enPacket = NUM_CAR_DATA_PACKETS;
   for(uint8_t i = 0; i < NUM_CAR_DATA_PACKETS; i++)
   {
      en_car_data_packets ePacketToCheck = (enLastPacket+i+1) % NUM_CAR_DATA_PACKETS;
      if(Sys_Bit_Get(&aucDirtyBits[0], ePacketToCheck))
      {
         enPacket = ePacketToCheck;
         break;
      }
   }

   if( enPacket < NUM_CAR_DATA_PACKETS )
   {
      bSend = 1;
      punDatagram->auc8[0] = enPacket;
      punDatagram->auc8[1] = astCarData[eCarID].ucFirstLanding;
      punDatagram->auc8[2] = astCarData[eCarID].ucLastLanding;
      punDatagram->auc8[3] = GetMasterDispatcherFlag();

      switch(enPacket)
      {
         case CAR_DATA_PACKET__OPENING_F_1:
            punDatagram->aui32[1] = astCarData[eCarID].auiBF_OpeningMap_F[0];
            break;
         case CAR_DATA_PACKET__OPENING_F_2:
            punDatagram->aui32[1] = astCarData[eCarID].auiBF_OpeningMap_F[1];
            break;
         case CAR_DATA_PACKET__OPENING_F_3:
            punDatagram->aui32[1] = astCarData[eCarID].auiBF_OpeningMap_F[2];
            break;
         case CAR_DATA_PACKET__OPENING_R_1:
            punDatagram->aui32[1] = astCarData[eCarID].auiBF_OpeningMap_R[0];
            break;
         case CAR_DATA_PACKET__OPENING_R_2:
            punDatagram->aui32[1] = astCarData[eCarID].auiBF_OpeningMap_R[1];
            break;
         case CAR_DATA_PACKET__OPENING_R_3:
            punDatagram->aui32[1] = astCarData[eCarID].auiBF_OpeningMap_R[2];
            break;
         case CAR_DATA_PACKET__HALLMASK_F:
            punDatagram->aui32[1] = astCarData[eCarID].uiHallMask_F;
            break;
         case CAR_DATA_PACKET__HALLMASK_R:
            punDatagram->aui32[1] = astCarData[eCarID].uiHallMask_R;
            break;
         case CAR_DATA_PACKET__LATCHABLE_HALLMASK_F:
            punDatagram->aui32[1] = astCarData[eCarID].uiLatchableHallMask_F;
            break;
         case CAR_DATA_PACKET__PAIRED_HB_MASK:
            punDatagram->aui32[1] = astCarData[eCarID].aucPairedHBMask[0];
            punDatagram->aui32[1] |= astCarData[eCarID].aucPairedHBMask[1] << 8;
            punDatagram->aui32[1] |= astCarData[eCarID].aucPairedHBMask[2] << 16;
            punDatagram->aui32[1] |= astCarData[eCarID].aucPairedHBMask[3] << 24;
            break;
         case CAR_DATA_PACKET__SECURITY_F_1:
            punDatagram->aui32[1] = astCarData[eCarID].auiBF_SecurityMap_F[0];
            break;
         case CAR_DATA_PACKET__SECURITY_F_2:
            punDatagram->aui32[1] = astCarData[eCarID].auiBF_SecurityMap_F[1];
            break;
         case CAR_DATA_PACKET__SECURITY_F_3:
            punDatagram->aui32[1] = astCarData[eCarID].auiBF_SecurityMap_F[2];
            break;
         case CAR_DATA_PACKET__SECURITY_R_1:
            punDatagram->aui32[1] = astCarData[eCarID].auiBF_SecurityMap_R[0];
            break;
         case CAR_DATA_PACKET__SECURITY_R_2:
            punDatagram->aui32[1] = astCarData[eCarID].auiBF_SecurityMap_R[1];
            break;
         case CAR_DATA_PACKET__SECURITY_R_3:
            punDatagram->aui32[1] = astCarData[eCarID].auiBF_SecurityMap_R[2];
            break;
         case CAR_DATA_PACKET__TIME:
            punDatagram->aui32[1] = astCarData[eCarID].ucMaxFloorToFloorTime_sec;
            punDatagram->aui32[1] |= astCarData[eCarID].ucDoorDwellTime_sec << 8;
            punDatagram->aui32[1] |= astCarData[eCarID].ucDoorDwellHallTime_sec << 16;
            break;
         case CAR_DATA_PACKET__HALL_SEC_1:
            punDatagram->aui32[1] = astCarData[eCarID].auiBF_HallSecurityMap[0];
            break;
         case CAR_DATA_PACKET__HALL_SEC_2:
            punDatagram->aui32[1] = astCarData[eCarID].auiBF_HallSecurityMap[1];
            break;
         case CAR_DATA_PACKET__HALL_SEC_3:
            punDatagram->aui32[1] = astCarData[eCarID].auiBF_HallSecurityMap[2];
            break;
         case CAR_DATA_PACKET__REQUEST:
            punDatagram->aui32[1] = astCarData[eCarID].eCarRequest;
            break;
         case CAR_DATA_PACKET__HALL_SEC_MASK:
            punDatagram->aui32[1] = astCarData[eCarID].ucHallSecurityMask_F;
            punDatagram->aui32[1] |= astCarData[eCarID].ucHallSecurityMask_R << 8;
            punDatagram->aui32[1] |= astCarData[eCarID].bEnableHallCallSecurity << 31;
            break;
         case CAR_DATA_PACKET__LATCHABLE_HALLMASK_R:
            punDatagram->aui32[1] = astCarData[eCarID].uiLatchableHallMask_R;
            break;
         case CAR_DATA_PACKET__HALL_MEDICAL:
            punDatagram->aui32[1] = astCarData[eCarID].ucMedicalMask;
            punDatagram->aui32[1] |= astCarData[eCarID].ucMedicalLanding_Plus1 << 8;
            // 2 Byte unused
            break;
         case CAR_DATA_PACKET__FLAGS:
            punDatagram->aui32[1] = ( astCarData[eCarID].bDynamicParking )
                                  | ( astCarData[eCarID].bParking << 1 )
                                  | ( astCarData[eCarID].bExtFloorLimit << 2 );
            punDatagram->ai32[1] |= astCarData[eCarID].bVIP << 3;
            punDatagram->ai32[1] |= astCarData[eCarID].ucbClosestCar << 4;
            punDatagram->ai32[1] |= astCarData[eCarID].bCarReady << 5;
            punDatagram->ai32[1] |= astCarData[eCarID].bCarCapture << 6;
            punDatagram->ai32[1] |= astCarData[eCarID].bFaulted << 7;
            break;
         case CAR_DATA_PACKET__VIPMASK:
            punDatagram->aui32[1] = astCarData[eCarID].uiVIPMask_F;
            punDatagram->aui32[1] |= astCarData[eCarID].uiVIPMask_R << 8;
            break;
         default:
            bSend = 0;
            break;
      }
   }

   if(bSend)
   {
      enLastPacket = enPacket;
      Sys_Bit_Set(&aucDirtyBits[0], enPacket, 0);
      auwResendTimer_5ms[enPacket] = 0;
   }

   return bSend;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void LoadDatagram_CarOperation( un_sdata_datagram *punDatagram )
{
   enum en_group_net_nodes eCarID = GetFP_GroupCarIndex();
   punDatagram->auc8[0] = astCarData[eCarID].eClassOp;
   switch( astCarData[eCarID].eClassOp )
   {
      case CLASSOP__AUTO:
         punDatagram->auc8[1] = astCarData[eCarID].eAutoMode;
         break;

      case CLASSOP__MANUAL:
         punDatagram->auc8[1] = astCarData[eCarID].eManualMode;
         break;

      case CLASSOP__SEMI_AUTO:
         punDatagram->auc8[1] = astCarData[eCarID].eLearnMode;
         break;

      default:
         break;
   }
   punDatagram->auc8[2] = astCarData[eCarID].eAutoState;
   punDatagram->auc8[3] = astCarData[eCarID].eRecallState;
   punDatagram->auc8[4] = astCarData[eCarID].eCaptureMode;
   punDatagram->auc8[5] = astCarData[eCarID].ucFireService;
   punDatagram->auc8[6] = astCarData[eCarID].eFire2State;
   punDatagram->auc8[7] = astCarData[eCarID].eEP_CommandFeedback & 0x07;
   punDatagram->auc8[7] |= (astCarData[eCarID].bEP_Disabled & 0x01) << 6;
   punDatagram->auc8[7] |= (astCarData[eCarID].bEP_CarRecalledFlag & 0x01) << 7;
}
/*-----------------------------------------------------------------------------
   punDatagram->auc8[0] = astCarData[eCarID].ucCurrentLanding;
   punDatagram->auc8[1] = astCarData[eCarID].ucDestinationLanding;
   punDatagram->auc8[2] = astCarData[eCarID].ucReachableLanding;
   punDatagram->auc8[3] = astCarData[eCarID].eDoor_F;
   punDatagram->auc8[4] = astCarData[eCarID].eDoor_R;
   punDatagram->auc8[5] = astCarData[eCarID].cMotionDir;
   punDatagram->auc8[6] = astCarData[eCarID].enPriority;
   punDatagram->auc8[7] = ( astCarData[eCarID].bFastGroupResend )
                        | ( astCarData[eCarID].bIdleDirection << 1 )
                        | ( astCarData[eCarID].bInSlowdown << 2 )
                        | ( astCarData[eCarID].bSuppressReopen << 3 );
 -----------------------------------------------------------------------------*/
void UnloadDatagram_CarStatus( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].ucCurrentLanding = punDatagram->auc8[0];
   astCarData[eCarID].ucDestinationLanding = punDatagram->auc8[1];
   astCarData[eCarID].ucReachableLanding = punDatagram->auc8[2];
   astCarData[eCarID].eDoor_F = ( punDatagram->auc8[3] & 0x0F );
   astCarData[eCarID].eDoor_R = ( punDatagram->auc8[4] & 0x0F );
   astCarData[eCarID].cMotionDir = punDatagram->auc8[5];
   astCarData[eCarID].enPriority = ( punDatagram->auc8[6] & 0x01 );
   astCarData[eCarID].bFastGroupResend = punDatagram->auc8[7] & 1;
   astCarData[eCarID].bIdleDirection = ( punDatagram->auc8[7] >> 1 ) & 1;
   astCarData[eCarID].bInSlowdown = ( punDatagram->auc8[7] >> 2 ) & 1;
   astCarData[eCarID].bSuppressReopen = ( punDatagram->auc8[7] >> 3 ) & 1;

   astCarData[eCarID].bDirty = 1;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UnloadDatagram_CarData( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   en_car_data_packets ePacket = punDatagram->auc8[0];
   astCarData[eCarID].ucFirstLanding = punDatagram->auc8[1];
   astCarData[eCarID].ucLastLanding = punDatagram->auc8[2];
   astCarData[eCarID].bMasterDispatcher = punDatagram->auc8[3];
   switch(ePacket)
   {
      case CAR_DATA_PACKET__OPENING_F_1:
         astCarData[eCarID].auiBF_OpeningMap_F[0] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__OPENING_F_2:
         astCarData[eCarID].auiBF_OpeningMap_F[1] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__OPENING_F_3:
         astCarData[eCarID].auiBF_OpeningMap_F[2] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__OPENING_R_1:
         astCarData[eCarID].auiBF_OpeningMap_R[0] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__OPENING_R_2:
         astCarData[eCarID].auiBF_OpeningMap_R[1] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__OPENING_R_3:
         astCarData[eCarID].auiBF_OpeningMap_R[2] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__HALLMASK_F:
         astCarData[eCarID].uiHallMask_F = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__HALLMASK_R:
         astCarData[eCarID].uiHallMask_R = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__LATCHABLE_HALLMASK_F:
         astCarData[eCarID].uiLatchableHallMask_F = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__PAIRED_HB_MASK:
         astCarData[eCarID].aucPairedHBMask[0] = ( punDatagram->aui32[1] ) & 0xFF;
         astCarData[eCarID].aucPairedHBMask[1] = ( punDatagram->aui32[1] >> 8 ) & 0xFF;
         astCarData[eCarID].aucPairedHBMask[2] = ( punDatagram->aui32[1] >> 16 ) & 0xFF;
         astCarData[eCarID].aucPairedHBMask[3] = ( punDatagram->aui32[1] >> 24 ) & 0xFF;
         break;
      case CAR_DATA_PACKET__SECURITY_F_1:
         astCarData[eCarID].auiBF_SecurityMap_F[0] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__SECURITY_F_2:
         astCarData[eCarID].auiBF_SecurityMap_F[1] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__SECURITY_F_3:
         astCarData[eCarID].auiBF_SecurityMap_F[2] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__SECURITY_R_1:
         astCarData[eCarID].auiBF_SecurityMap_R[0] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__SECURITY_R_2:
         astCarData[eCarID].auiBF_SecurityMap_R[1] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__SECURITY_R_3:
         astCarData[eCarID].auiBF_SecurityMap_R[2] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__TIME:
         astCarData[eCarID].ucMaxFloorToFloorTime_sec = ( punDatagram->aui32[1] >> 0 ) & 0xFF;
         astCarData[eCarID].ucDoorDwellTime_sec = ( punDatagram->aui32[1] >> 8 ) & 0xFF;
         astCarData[eCarID].ucDoorDwellHallTime_sec = ( punDatagram->aui32[1] >> 16 ) & 0xFF;
         break;
      case CAR_DATA_PACKET__HALL_SEC_1:
         astCarData[eCarID].auiBF_HallSecurityMap[0] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__HALL_SEC_2:
         astCarData[eCarID].auiBF_HallSecurityMap[1] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__HALL_SEC_3:
         astCarData[eCarID].auiBF_HallSecurityMap[2] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__REQUEST:
         astCarData[eCarID].eCarRequest = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__HALL_SEC_MASK:
         astCarData[eCarID].ucHallSecurityMask_F = ( punDatagram->aui32[1] ) & 0xFF;
         astCarData[eCarID].ucHallSecurityMask_R = ( punDatagram->aui32[1] >> 8 ) & 0xFF;
         astCarData[eCarID].bEnableHallCallSecurity = ( punDatagram->aui32[1] >> 31 ) & 1;
         break;
      case CAR_DATA_PACKET__LATCHABLE_HALLMASK_R:
         astCarData[eCarID].uiLatchableHallMask_R = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__HALL_MEDICAL:
         astCarData[eCarID].ucMedicalMask = ( punDatagram->aui32[1] ) & 0xFF;
         astCarData[eCarID].ucMedicalLanding_Plus1 = ( punDatagram->aui32[1] >> 8 ) & 0xFF;
         // 2 Byte unused
         break;
      case CAR_DATA_PACKET__FLAGS:
         astCarData[eCarID].bDynamicParking = GET_BITFLAG_U32( punDatagram->aui32[1], 0);
         astCarData[eCarID].bParking = GET_BITFLAG_U32( punDatagram->aui32[1], 1);
         astCarData[eCarID].bExtFloorLimit = GET_BITFLAG_U32( punDatagram->aui32[1], 2);
         astCarData[eCarID].bVIP = GET_BITFLAG_U32( punDatagram->aui32[1], 3);
         astCarData[eCarID].ucbClosestCar = GET_BITFLAG_U32( punDatagram->aui32[1], 4);
         astCarData[eCarID].bCarReady = GET_BITFLAG_U32( punDatagram->aui32[1], 5);
         astCarData[eCarID].bCarCapture = GET_BITFLAG_U32( punDatagram->aui32[1], 6);
         astCarData[eCarID].bFaulted = GET_BITFLAG_U32( punDatagram->aui32[1], 7);
         break;
      case CAR_DATA_PACKET__VIPMASK:
         astCarData[eCarID].uiVIPMask_F = punDatagram->aui32[1] & 0xFF;
         astCarData[eCarID].uiVIPMask_R = ( punDatagram->aui32[1] >> 8 ) & 0xFF;
         break;
      default:
         break;
   }
}
/*-----------------------------------------------------------------------------
   punDatagram->auc8[0] = astCarData[eCarID].eClassOp;
   switch( astCarData[eCarID].eClassOp )
   {
      case CLASSOP__AUTO:
         punDatagram->auc8[1] = astCarData[eCarID].eAutoMode;
         break;

      case CLASSOP__MANUAL:
         punDatagram->auc8[1] = astCarData[eCarID].eManualMode;
         break;

      case CLASSOP__SEMI_AUTO:
         punDatagram->auc8[1] = astCarData[eCarID].eLearnMode;
         break;

      default:
         break;
   }
   punDatagram->auc8[2] = astCarData[eCarID].eAutoState;
   punDatagram->auc8[3] = astCarData[eCarID].eRecallState;
   punDatagram->auc8[4] = astCarData[eCarID].eCaptureMode;
   punDatagram->auc8[5] = astCarData[eCarID].ucFireService;
   punDatagram->auc8[6] = astCarData[eCarID].eFire2State;
   punDatagram->auc8[7] = astCarData[eCarID].eEP_CommandFeedback & 0x07;
   punDatagram->auc8[7] |= (astCarData[eCarID].bEP_Disabled & 0x01) << 6;
   punDatagram->auc8[7] |= (astCarData[eCarID].bEP_CarRecalledFlag & 0x01) << 7;
 -----------------------------------------------------------------------------*/
void UnloadDatagram_CarOperation( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].eAutoMode = 0;
   astCarData[eCarID].eManualMode = 0;
   astCarData[eCarID].eLearnMode = 0;

   astCarData[eCarID].eClassOp = punDatagram->auc8[0] & 0x07;
   switch( astCarData[eCarID].eClassOp )
   {
      case CLASSOP__AUTO:
         astCarData[eCarID].eAutoMode = punDatagram->auc8[1];
         break;

      case CLASSOP__MANUAL:
         astCarData[eCarID].eManualMode = punDatagram->auc8[1];
         break;

      case CLASSOP__SEMI_AUTO:
         astCarData[eCarID].eLearnMode = punDatagram->auc8[1];
         break;
      default:
         break;
   }
   astCarData[eCarID].eAutoState = punDatagram->auc8[2];
   astCarData[eCarID].eRecallState = punDatagram->auc8[3] & 0x07;
   astCarData[eCarID].eCaptureMode = punDatagram->auc8[4] & 0x03;
   astCarData[eCarID].ucFireService = punDatagram->auc8[5];
   astCarData[eCarID].eFire2State = punDatagram->auc8[6];

   astCarData[eCarID].bEP_CarRecalledFlag = (punDatagram->auc8[7] >> 7) & 1;
   astCarData[eCarID].bEP_Disabled = (punDatagram->auc8[7] >> 6) & 1;
   astCarData[eCarID].eEP_CommandFeedback = punDatagram->auc8[7] & 0x07;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UnloadDatagram_CarCalls1( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].auiBF_CarCalls_F[0] = punDatagram->aui32[0];
   astCarData[eCarID].auiBF_CarCalls_R[0] = punDatagram->aui32[1];
}
void UnloadDatagram_CarCalls2( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].auiBF_CarCalls_F[1] = punDatagram->aui32[0];
   astCarData[eCarID].auiBF_CarCalls_R[1] = punDatagram->aui32[1];
}
void UnloadDatagram_CarCalls3( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].auiBF_CarCalls_F[2] = punDatagram->aui32[0];
   astCarData[eCarID].auiBF_CarCalls_R[2] = punDatagram->aui32[1];
}

/*-----------------------------------------------------------------------------
 I/O
 -----------------------------------------------------------------------------*/
void UnloadDatagram_InputMap1( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].auiBF_InputMap[0] = punDatagram->aui32[0];
   astCarData[eCarID].auiBF_InputMap[1] = punDatagram->aui32[1];
}
void UnloadDatagram_InputMap2( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].auiBF_InputMap[2] = punDatagram->aui32[0];
   astCarData[eCarID].auiBF_InputMap[3] = punDatagram->aui32[1];
}
void UnloadDatagram_InputMap3( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].auiBF_InputMap[4] = punDatagram->aui32[0];
   astCarData[eCarID].auiBF_InputMap[5] = punDatagram->aui32[1];
}

void UnloadDatagram_OutputMap1( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].auiBF_OutputMap[0] = punDatagram->aui32[0];
   astCarData[eCarID].auiBF_OutputMap[1] = punDatagram->aui32[1];
}
void UnloadDatagram_OutputMap2( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].auiBF_OutputMap[2] = punDatagram->aui32[0];
   astCarData[eCarID].auiBF_OutputMap[3] = punDatagram->aui32[1];
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t GetLatchedCarCall_Front_ByCar( uint8_t eCarID, uint8_t ucLanding )
{
   uint8_t bLatched = Sys_Bit_Get(&astCarData[eCarID].auiBF_CarCalls_F[0], ucLanding);
   return bLatched;
}
uint8_t GetLatchedCarCall_Rear_ByCar( uint8_t eCarID, uint8_t ucLanding )
{
   uint8_t bLatched = Sys_Bit_Get(&astCarData[eCarID].auiBF_CarCalls_R[0], ucLanding);
   return bLatched;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
en_dispatch_mode GetDispatchMode_ByCar(enum en_group_net_nodes eCarID)
{
   return astCarData[eCarID].enDispatchMode;
}
/*----------------------------------------------------------------------------
   Returns 1 if the current automatic mode of operation has higher priority than EMS1
 *----------------------------------------------------------------------------*/
static uint8_t CheckForHigherModePriority_EMS1(void)
{
   uint8_t bReturn = 0;
   enum en_mode_auto eAutoMode = GetOperation_AutoMode();
   if( ( eAutoMode == MODE_A__NONE ) // Not in auto operation
    || ( eAutoMode == MODE_A__CW_DRAIL )
    || ( eAutoMode == MODE_A__SEISMC )
    || ( eAutoMode == MODE_A__BATT_RESQ )
    || ( eAutoMode == MODE_A__FIRE2 )
    || ( eAutoMode == MODE_A__EMS2 )
    || ( eAutoMode == MODE_A__EPOWER )
    || ( eAutoMode == MODE_A__FIRE1 ) )
   {
      bReturn = 1;
   }
   return bReturn;
}
/*----------------------------------------------------------------------------
   Update structure for local car data
 *----------------------------------------------------------------------------*/
void UpdateLocalGroupCarData()
{
   enum en_group_net_nodes eCarID = GetFP_GroupCarIndex();
   ClrCarOfflineTimer_ByCar(eCarID);
   uint8_t ucLowestFloor = 0;
   if( ( GetOperation_AutoMode() == MODE_A__FLOOD_OP )
    && ( Param_ReadValue_1Bit(enPARAM1__FLOOD_OkayToRun) ) )
   {
      ucLowestFloor = Param_ReadValue_8Bit(enPARAM8__FLOOD_NumFloors_Plus1);
   }
   uint8_t ucFirstLanding = GetGroupMapping(ucLowestFloor);
   uint8_t ucLastLanding = ucFirstLanding+GetFP_NumFloors()-1;
   astCarData[eCarID].ucFirstLanding = ucFirstLanding;
   astCarData[eCarID].ucLastLanding = ucLastLanding;
   astCarData[eCarID].enDispatchMode = GetDispatchMode();

   /* Justify CCs and Openings in terms of landings instead of local floor index */
   if(ucFirstLanding < 32)
   {
      astCarData[eCarID].auiBF_CarCalls_F[0] = (GetCarCalls1_Front_MRA() << ucFirstLanding);
      astCarData[eCarID].auiBF_CarCalls_R[0] = (GetCarCalls1_Rear_MRA() << ucFirstLanding);

      astCarData[eCarID].auiBF_CarCalls_F[1] = (GetCarCalls2_Front_MRA() << ucFirstLanding)
                                             | (GetCarCalls1_Front_MRA() >> (NUM_BITS_PER_BITMAP32-ucFirstLanding));
      astCarData[eCarID].auiBF_CarCalls_R[1] = (GetCarCalls2_Rear_MRA() << ucFirstLanding)
                                             | (GetCarCalls1_Rear_MRA() >> (NUM_BITS_PER_BITMAP32-ucFirstLanding));
      astCarData[eCarID].auiBF_CarCalls_F[2] = (GetCarCalls3_Front_MRA() << ucFirstLanding)
                                             | (GetCarCalls2_Front_MRA() >> (NUM_BITS_PER_BITMAP32-ucFirstLanding));
      astCarData[eCarID].auiBF_CarCalls_R[2] = (GetCarCalls3_Rear_MRA() << ucFirstLanding)
                                             | (GetCarCalls2_Rear_MRA() >> (NUM_BITS_PER_BITMAP32-ucFirstLanding));


      astCarData[eCarID].auiBF_OpeningMap_F[0] = (Param_ReadValue_32Bit(enPARAM32__OpeningBitmapF_0) << ucFirstLanding);
      astCarData[eCarID].auiBF_OpeningMap_R[0] = (Param_ReadValue_32Bit(enPARAM32__OpeningBitmapR_0) << ucFirstLanding);

      astCarData[eCarID].auiBF_OpeningMap_F[1] = (Param_ReadValue_32Bit(enPARAM32__OpeningBitmapF_1) << ucFirstLanding)
                                               | (Param_ReadValue_32Bit(enPARAM32__OpeningBitmapF_0) >> (NUM_BITS_PER_BITMAP32-ucFirstLanding));
      astCarData[eCarID].auiBF_OpeningMap_R[1] = (Param_ReadValue_32Bit(enPARAM32__OpeningBitmapR_1) << ucFirstLanding)
                                               | (Param_ReadValue_32Bit(enPARAM32__OpeningBitmapR_0) >> (NUM_BITS_PER_BITMAP32-ucFirstLanding));
      astCarData[eCarID].auiBF_OpeningMap_F[2] = (Param_ReadValue_32Bit(enPARAM32__OpeningBitmapF_2) << ucFirstLanding)
                                               | (Param_ReadValue_32Bit(enPARAM32__OpeningBitmapF_1) >> (NUM_BITS_PER_BITMAP32-ucFirstLanding));
      astCarData[eCarID].auiBF_OpeningMap_R[2] = (Param_ReadValue_32Bit(enPARAM32__OpeningBitmapR_2) << ucFirstLanding)
                                               | (Param_ReadValue_32Bit(enPARAM32__OpeningBitmapR_1) >> (NUM_BITS_PER_BITMAP32-ucFirstLanding));

      astCarData[eCarID].auiBF_SecurityMap_F[0] = Param_ReadValue_32Bit(enPARAM32__SecureKeyedBitmapF_0) ^ GetVirtualCarCallSecurity(DOOR_FRONT, 0);
      astCarData[eCarID].auiBF_SecurityMap_R[0] = Param_ReadValue_32Bit(enPARAM32__SecureKeyedBitmapR_0) ^ GetVirtualCarCallSecurity(DOOR_REAR, 0);

      astCarData[eCarID].auiBF_SecurityMap_F[1] = Param_ReadValue_32Bit(enPARAM32__SecureKeyedBitmapF_1) ^ GetVirtualCarCallSecurity(DOOR_FRONT, 1);
      astCarData[eCarID].auiBF_SecurityMap_R[1] = Param_ReadValue_32Bit(enPARAM32__SecureKeyedBitmapR_1) ^ GetVirtualCarCallSecurity(DOOR_REAR, 1);

      astCarData[eCarID].auiBF_SecurityMap_F[2] = Param_ReadValue_32Bit(enPARAM32__SecureKeyedBitmapF_2) ^ GetVirtualCarCallSecurity(DOOR_FRONT, 2);
      astCarData[eCarID].auiBF_SecurityMap_R[2] = Param_ReadValue_32Bit(enPARAM32__SecureKeyedBitmapR_2) ^ GetVirtualCarCallSecurity(DOOR_REAR, 2);

      uint32_t uiHallSecureMap_0 = Param_ReadValue_16Bit(enPARAM16__HallSecureMap_0) | ( Param_ReadValue_16Bit(enPARAM16__HallSecureMap_1) << NUM_BITS_PER_BITMAP16 );
      uint32_t uiHallSecureMap_1 = Param_ReadValue_16Bit(enPARAM16__HallSecureMap_2) | ( Param_ReadValue_16Bit(enPARAM16__HallSecureMap_3) << NUM_BITS_PER_BITMAP16 );
      astCarData[eCarID].auiBF_HallSecurityMap[0] = (uiHallSecureMap_0 << ucFirstLanding);
      astCarData[eCarID].auiBF_HallSecurityMap[1] = (uiHallSecureMap_1 << ucFirstLanding)
                                                  | (uiHallSecureMap_0 >> (NUM_BITS_PER_BITMAP32-ucFirstLanding));
      uint32_t uiHallSecureMap_2 = Param_ReadValue_16Bit(enPARAM16__HallSecureMap_4) | ( Param_ReadValue_16Bit(enPARAM16__HallSecureMap_5) << NUM_BITS_PER_BITMAP16 );
      astCarData[eCarID].auiBF_HallSecurityMap[2] = (uiHallSecureMap_2 << ucFirstLanding)
                                                  | (uiHallSecureMap_1 >> (NUM_BITS_PER_BITMAP32-ucFirstLanding));
   }

   /* Hall Masks */
   if( ( !Param_ReadValue_1Bit(enPARAM1__EnableNeverDropHallCalls) ) /* Addresses complaint at 260 S Broad that the hall calls don't stay latched when all cars are taken out of group. */
    && ( GetFaultedCarRemovedFromGroupFlag() || HallCallsDisabled() || GetInputValue(enIN_DISABLE_ALL_HC) || ( GetOperation_AutoMode() == MODE_A__EMS1  ) ) )
   {
      astCarData[eCarID].uiLatchableHallMask_F = 0;
      astCarData[eCarID].uiLatchableHallMask_R = 0;
   }
   else
   {
      uint32_t uiLatchableHallMask = 0;
      if( ( GetOperation_AutoMode() != MODE_A__SWING ) || ( GetOperation_AutoMode() != MODE_A__VIP_MODE ) )      {
         uiLatchableHallMask |= Param_ReadValue_8Bit ( enPARAM8__HallCallMask);
         if( Param_ReadValue_1Bit(enPARAM1__Swing_CallsEnable) || Param_ReadValue_1Bit(enPARAM1__VIP_Priority_Dispatching))         {
            uiLatchableHallMask |= Param_ReadValue_8Bit ( enPARAM8__SwingCallMask );
         }
      }
      else
      {
         uiLatchableHallMask |= Param_ReadValue_8Bit ( enPARAM8__SwingCallMask );
         uiLatchableHallMask |= Param_ReadValue_8Bit ( enPARAM8__HallCallMask);
      }

      astCarData[eCarID].uiLatchableHallMask_F = uiLatchableHallMask & ~Param_ReadValue_8Bit ( enPARAM8__HallRearDoorMask);
      astCarData[eCarID].uiLatchableHallMask_R = uiLatchableHallMask & Param_ReadValue_8Bit ( enPARAM8__HallRearDoorMask);

      /* These modes support destination dispatching */
      if( ( GetOperation_AutoMode() == MODE_A__NORMAL )
       || ( GetOperation_AutoMode() == MODE_A__SWING )
       || ( GetOperation_AutoMode() != MODE_A__VIP_MODE )
       || ( GetOperation_AutoMode() == MODE_A__FLOOD_OP ) )
      {
         astCarData[eCarID].uiLatchableHallMask_F |= CarData_GetUniqueHallMask_Front(eCarID);
         astCarData[eCarID].uiLatchableHallMask_R |= CarData_GetUniqueHallMask_Rear(eCarID);
      }
   }

   /* Car has removed itself from destination assignments for a time after failing to take its assigned call */
   if( GetFaultedCarRemovedFromGroupFlag() || HallCallsDisabled() || GetInputValue(enIN_DISABLE_ALL_HC) || GetFullLoadFlag() )
   {
      astCarData[eCarID].uiHallMask_F = 0;
      astCarData[eCarID].uiHallMask_R = 0;
   }
   /* Allow continued DD panel assignments for the following conditions */
   else if( GetCarRemovedFromGroupFlag() || gstFault.bActiveFault )
   {
      astCarData[eCarID].uiHallMask_F = CarData_GetUniqueHallMask_Front(eCarID);
      astCarData[eCarID].uiHallMask_R = CarData_GetUniqueHallMask_Rear(eCarID);
   }
   else
   {
      //-------------------------------------------------------------
      uint32_t uiDestinationHallMask;
      if( GetOperation_AutoMode() == MODE_A__EMS1 )
      {
         /* Suppress the destination mask during EMS1 recall. Traditional dispatching will not be used. */
         uiDestinationHallMask = 0;
      }
      else if( ( GetOperation_AutoMode() == MODE_A__VIP_MODE ) && astCarData[eCarID].bCarReady && !astCarData[eCarID].bCarCapture )
      {
         uiDestinationHallMask = Param_ReadValue_8Bit ( enPARAM8__SwingCallMask);
      }
      else if ( ( GetOperation_AutoMode() == MODE_A__VIP_MODE ) && astCarData[eCarID].bCarCapture && !astCarData[eCarID].bCarReady)
      {
         uiDestinationHallMask = 0;
      }
      else if( ( GetOperation_AutoMode() == MODE_A__VIP_MODE ) && !astCarData[eCarID].bCarCapture && !astCarData[eCarID].bCarReady )
      {
         uiDestinationHallMask = 0;
      }
      else if( ( GetOperation_AutoMode() == MODE_A__SWING )
            && ( CheckIf_CarOutOfGroup() ) )
      {
         uiDestinationHallMask = 0;
      }
      else if( GetOperation_AutoMode() == MODE_A__SWING )
      {
         uiDestinationHallMask = Param_ReadValue_8Bit ( enPARAM8__SwingCallMask);
         if( Param_ReadValue_1Bit(enPARAM1__Swing_StayInGroup) )
         {
            uiDestinationHallMask |= Param_ReadValue_8Bit ( enPARAM8__HallCallMask);
         }
      }
      else
      {
         uiDestinationHallMask = Param_ReadValue_8Bit ( enPARAM8__HallCallMask);
      }
      // ks note: reassigned below
      uint32_t uiHallMask_F = uiDestinationHallMask
                           & ~Param_ReadValue_8Bit ( enPARAM8__HallRearDoorMask);
      /* Valid destination dispatching modes */
      if( ( GetOperation_AutoMode() == MODE_A__NORMAL )
       || ( GetOperation_AutoMode() == MODE_A__FLOOD_OP )
       || ( ( GetOperation_AutoMode() == MODE_A__SWING ) && ( Param_ReadValue_1Bit(enPARAM1__Swing_StayInGroup) ) ) )
      {
         uiHallMask_F |= CarData_GetUniqueHallMask_Front(eCarID);
      }

      uint32_t uiHallMask_R = 0;
      if( GetFP_RearDoors() )
      {
         uiHallMask_R = uiDestinationHallMask
                      & Param_ReadValue_8Bit ( enPARAM8__HallRearDoorMask);
         /* Valid destination dispatching modes */
         if( ( GetOperation_AutoMode() == MODE_A__NORMAL )
          || ( GetOperation_AutoMode() == MODE_A__FLOOD_OP )
          || ( ( GetOperation_AutoMode() == MODE_A__SWING ) && ( Param_ReadValue_1Bit(enPARAM1__Swing_StayInGroup) ) ) )
         {
            uiHallMask_R |= CarData_GetUniqueHallMask_Rear(eCarID);
         }
      }

      astCarData[eCarID].uiHallMask_F = uiHallMask_F;
      astCarData[eCarID].uiHallMask_R = uiHallMask_R;
   }

   uint8_t ucCurrentLanding = GetGroupMapping(GetOperation_CurrentFloor());
   uint8_t ucDestinationLanding = GetGroupMapping(GetOperation_DestinationFloor());
   uint8_t ucReachableLanding = GetGroupMapping(GetGroup_DispatchNextFloor());
   enum direction_enum cMotionDir = GetMotion_Direction();
   en_hc_dir enPriority = (GetCarPriority()) ? HC_DIR__UP:HC_DIR__DOWN;

   astCarData[eCarID].ucCurrentLanding = ucCurrentLanding;
   astCarData[eCarID].ucDestinationLanding = ucDestinationLanding;
   astCarData[eCarID].ucReachableLanding = ucReachableLanding;
   astCarData[eCarID].eDoor_F = GetDoorState_Front();
   astCarData[eCarID].eDoor_R = GetDoorState_Rear();
   astCarData[eCarID].cMotionDir = cMotionDir;
   astCarData[eCarID].enPriority = enPriority;

   // load operation data
   astCarData[eCarID].eClassOp = GetOperation_ClassOfOp();
   astCarData[eCarID].eAutoMode = GetOperation_AutoMode();
   astCarData[eCarID].eManualMode = GetOperation_ManualMode();
   astCarData[eCarID].eLearnMode = GetOperation_LearnMode();
   astCarData[eCarID].eRecallState = GetModState_Recall();
   astCarData[eCarID].eAutoState = GetModState_AutoState();
   astCarData[eCarID].ucFireService = GetModState_FireSrv();
   astCarData[eCarID].eFire2State = GetModState_FireSrv2();
   astCarData[eCarID].eCaptureMode = GetOperation3_CaptureMode_MR();

   astCarData[eCarID].aucPairedHBMask[0] = Param_ReadValue_8Bit(enPARAM8__LinkedHallMask_1);
   astCarData[eCarID].aucPairedHBMask[1] = Param_ReadValue_8Bit(enPARAM8__LinkedHallMask_2);
   astCarData[eCarID].aucPairedHBMask[2] = Param_ReadValue_8Bit(enPARAM8__LinkedHallMask_3);
   astCarData[eCarID].aucPairedHBMask[3] = Param_ReadValue_8Bit(enPARAM8__LinkedHallMask_4);

   astCarData[eCarID].bFastGroupResend = Param_ReadValue_1Bit(enPARAM1__Debug_FastGroupResend);

   astCarData[eCarID].auiBF_InputMap[0] = GetMappedInputs1_InputBitmap1_MRA();
   astCarData[eCarID].auiBF_InputMap[1] = GetMappedInputs1_InputBitmap2_MRA();
   astCarData[eCarID].auiBF_InputMap[2] = GetMappedInputs2_InputBitmap3_MRA();
   astCarData[eCarID].auiBF_InputMap[3] = GetMappedInputs2_InputBitmap4_MRA();
   astCarData[eCarID].auiBF_InputMap[4] = GetMappedInputs3_InputBitmap5_MRA();
   astCarData[eCarID].auiBF_InputMap[5] = GetMappedInputs3_InputBitmap6_MRA();

   astCarData[eCarID].auiBF_OutputMap[0] = GetMappedOutputs1_OutputBitmap1_MRA();
   astCarData[eCarID].auiBF_OutputMap[1] = GetMappedOutputs1_OutputBitmap2_MRA();
   astCarData[eCarID].auiBF_OutputMap[2] = GetMappedOutputs2_OutputBitmap3_MRA();
   astCarData[eCarID].auiBF_OutputMap[3] = GetMappedOutputs2_OutputBitmap4_MRA();

   astCarData[eCarID].bIdleDirection = CheckIf_IdleDirection();
   
   astCarData[eCarID].bEP_Disabled = Param_ReadValue_1Bit(enPARAM1__DisableEPower);
   astCarData[eCarID].bEP_CarRecalledFlag = GetGroup_EPower_CarRecalledFlag();
   astCarData[eCarID].eEP_CommandFeedback = GetGroup_EPower_CommandFeedback();

   astCarData[eCarID].ucDoorDwellTime_sec = Param_ReadValue_8Bit(enPARAM8__DoorDwellTime_1s);
   astCarData[eCarID].ucDoorDwellHallTime_sec = Param_ReadValue_8Bit(enPARAM8__DoorDwellHallTime_1s);
   astCarData[eCarID].ucMaxFloorToFloorTime_sec = GetDebug_GetMaxFloorToFloorTime_sec();

   astCarData[eCarID].bInSlowdown = CheckIf_InSlowdown();

   astCarData[eCarID].bSuppressReopen = CheckIf_SuppressReopen();

   astCarData[eCarID].eCarRequest = GetGroupCarRequest();

   astCarData[eCarID].ucHallSecurityMask_F = Param_ReadValue_8Bit(enPARAM8__HallSecurityMask) & ~Param_ReadValue_8Bit(enPARAM8__HallRearDoorMask);
   astCarData[eCarID].ucHallSecurityMask_R = Param_ReadValue_8Bit(enPARAM8__HallSecurityMask) & Param_ReadValue_8Bit(enPARAM8__HallRearDoorMask);
   astCarData[eCarID].bEnableHallCallSecurity = Param_ReadValue_1Bit(enPARAM1__EnableHallSecurity) && !GetInputValue(enIN_EnableAllHC);

   /* EMS emergency medical recall support */
   astCarData[eCarID].ucMedicalLanding_Plus1 = 0; // EMS mask applies to all landings
   if( GetCarRemovedFromGroupFlag() || CheckForHigherModePriority_EMS1() )
   {
      /*  If medical calls are disabled.*/
      astCarData[eCarID].ucMedicalMask = 0;
   }
   else if( ( GetOperation_AutoMode() == MODE_A__EMS1 ) && ( GetModState_Recall() != RECALL__UNKNOWN ) )
   {
      /*  If car is on EMS recall and locked into a floor, its EMS mask will only apply to its current recall landing */
      astCarData[eCarID].ucMedicalMask = Param_ReadValue_8Bit ( enPARAM8__HallMedicalMask );
      astCarData[eCarID].ucMedicalLanding_Plus1 = EMS_GetRecallLanding_Plus1(GetFP_GroupCarIndex());
   }
   else
   {
      astCarData[eCarID].ucMedicalMask = Param_ReadValue_8Bit ( enPARAM8__HallMedicalMask );
   }
   astCarData[eCarID].bDynamicParking = Param_ReadValue_1Bit(enPARAM1__Enable_Dynamic_Parking);
   astCarData[eCarID].bParking = GetGroup_ReadyToPark();

   astCarData[eCarID].bExtFloorLimit = Param_ReadValue_1Bit(enPARAM1__EnableExtFloorLimit);
   astCarData[eCarID].bVIP = Param_ReadValue_1Bit( enPARAM1__VIP_Priority_Dispatching );
   astCarData[eCarID].bCarReady = GetGroup_VIP_CarReady();
   astCarData[eCarID].bCarCapture = GetGroup_VIP_CarCapturing();

   /* Set VIP Mask */
   if( ( GetOperation_AutoMode() == MODE_A__VIP_MODE )
    && ( ( GetGroup_VIP_State() == VIP_STATE__ACCEPTING_CC ) || ( GetGroup_VIP_State() == VIP_STATE__WAIT_FOR_IDLE ) ) )
   {
      astCarData[eCarID].uiVIPMask_F = 0;
      astCarData[eCarID].uiVIPMask_R = 0;
   }
   else if( GetFaultedCarRemovedFromGroupFlag() || HallCallsDisabled() || GetInputValue(enIN_DISABLE_ALL_HC)
         || GetFullLoadFlag() || GetCarRemovedFromGroupFlag() || gstFault.bActiveFault || !astCarData[eCarID].bVIP )
   {
      astCarData[eCarID].uiVIPMask_F = 0;
      astCarData[eCarID].uiVIPMask_R = 0;
   }
   else
   {
      uint8_t ucVIPMask = Param_ReadValue_8Bit ( enPARAM8__SwingCallMask);
      astCarData[eCarID].uiVIPMask_F = ucVIPMask & ~Param_ReadValue_8Bit ( enPARAM8__HallRearDoorMask);
      astCarData[eCarID].uiVIPMask_R = ucVIPMask & Param_ReadValue_8Bit ( enPARAM8__HallRearDoorMask);
   }

   astCarData[eCarID].bFaulted = gstFault.bActiveFault;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
