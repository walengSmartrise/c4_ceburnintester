/***
 *       _____                          __         _
 *      / ___/ ____ ___   ____ _ _____ / /_ _____ (_)_____ ___
 *      \__ \ / __ `__ \ / __ `// ___// __// ___// // ___// _ \
 *     ___/ // / / / / // /_/ // /   / /_ / /   / /(__  )/  __/
 *    /____//_/ /_/ /_/ \__,_//_/    \__//_/   /_//____/ \___/
 *        ______               _                           _
 *       / ____/____   ____ _ (_)____   ___   ___   _____ (_)____   ____ _
 *      / __/  / __ \ / __ `// // __ \ / _ \ / _ \ / ___// // __ \ / __ `/
 *     / /___ / / / // /_/ // // / / //  __//  __// /   / // / / // /_/ /
 *    /_____//_/ /_/ \__, //_//_/ /_/ \___/ \___//_/   /_//_/ /_/ \__, /
 *                  /____/                                       /____/
 */
/******************************************************************************
 *
 * @file
 * @brief    Updates car destinations
 * @version
 * @date
 *
 * @note
 *
 ******************************************************************************/




/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sru_b.h"
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "shared_data.h"

#include "carData.h"
#include "carDestination.h"
#include "groupnet.h"
#include "groupnet_datagrams.h"
#include "group_net_data.h"
#include "XRegData.h"
#include "hallSecurity.h"
#include "masterCommand.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define REMOVE_FAULTED_CAR_FROM_GROUP_LIMIT_50MS          (600) //30s
#define CALL_SERVICED_TIMEOUT_50MS                        (40)
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Dispatching =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
/* Prevents car from servicing hall calls for user defined period after unsucessful call assignment. */
static uint8_t bRemoveCarFromGroupFlag;

static uint16_t uwRemoveFaultCarFromGroupCounter_50ms;

static uint8_t bEmergencyDispatch;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   If returns 1, hall calls are assumed lost and car needs to begin emergency dispatching.
   Car will proceed floor to floor opening its doors until it arrives at the top floor.
   Then it will return to the lowest floor. This behavior will repeat until hall calls are restored.
   Sabbath mode used to accomplish this behavior for now.

TODO review triggering case currently only triggers if hall network is lost.
will not flag on riser board loss or partial hall network loss
 -----------------------------------------------------------------------------*/
uint8_t Dispatch_GetEmergencyDispatchFlag(void)
{
   return bEmergencyDispatch;
}
static void Dispatch_UpdateEmergencyDispatchFlag(void)
{
   if( Param_ReadValue_1Bit(enPARAM1__EnableEmergencyDispatch) )
   {
      bEmergencyDispatch = 0;
      for(uint8_t i = 0; i < MAX_NUM_RISER_BOARDS; i++)
      {
         if( i != SECURITY_RISER_INDEX )
         {
            en_ris_exp_error eError = RiserData_GetError(i);
//            if( ( eError == ERROR_RIS_EXP__COM_HALL ) || ( eError == ERROR_RIS_EXP__HALL_BOARD_OFFLINE ) )// request to remove overly sensitive individual hall board condition
            if( eError == ERROR_RIS_EXP__COM_HALL )
            {
               bEmergencyDispatch = 1;
               break;
            }
         }
      }
   }
   else
   {
      bEmergencyDispatch = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t GetCarRemovedFromGroupFlag(void)
{
   return bRemoveCarFromGroupFlag; // Removed due to dispatching timeout
}
uint8_t GetFaultedCarRemovedFromGroupFlag(void)
{
   uint8_t bRemoved = ( uwRemoveFaultCarFromGroupCounter_50ms >= REMOVE_FAULTED_CAR_FROM_GROUP_LIMIT_50MS ); // Removed because faulted for more than 60s
   return bRemoved;
}
/*-----------------------------------------------------------------------------
   Clear any hall calls latched in inactive masks
 -----------------------------------------------------------------------------*/
static void ClearInvalidHallCalls(void)
{
   uint32_t uiXRegHallMask = XRegData_GetGroupHallCallMask();
   uint8_t ucHighestLanding = CarData_GetHighestActiveLanding();
   for (uint8_t i = 0; i <= ucHighestLanding; i++)
   {
      uint32_t uiMask = CarData_GetGroupHallCallMask(i, DOOR_ANY) | uiXRegHallMask | CarData_GetMedicalHallCallMask(i, DOOR_ANY);
      ClrRawLatchedHallCalls( i, HC_DIR__DOWN, ~uiMask );
      ClrRawLatchedHallCalls( i, HC_DIR__UP,   ~uiMask );
   }
}
/*-----------------------------------------------------------------------------
   Clears hall calls that have been cleared by a car
 -----------------------------------------------------------------------------*/
#define DEFAULT_CLEAR_TIME_50MS (100)
static void ClearServicedHallCalls(void)
{
   /* Delay hall call clearing to allow time for the call to reach the car,
    * otherwise arrival lantern assertion will be suppressed if doors are opened */
   static uint8_t aucCallServicedTimer_F_50ms[MAX_GROUP_CARS];
   static uint8_t aucCallServicedTimer_R_50ms[MAX_GROUP_CARS];
   static uint16_t uwCallTimeoutCounter_50ms;

   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      if(GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS)
      {
         st_cardata *pstCarData = GetCarDataStructure(i);
         uint8_t ucDestinationLanding = pstCarData->ucDestinationLanding;
         uint8_t ucCurrentLanding = pstCarData->ucCurrentLanding;
         en_hc_dir enPriority = pstCarData->enPriority;
         if( ( ucDestinationLanding < MAX_NUM_FLOORS )
          && ( ucDestinationLanding == ucCurrentLanding ) )
         {
            if( CarData_GetDoorOpen_Front(i) )
            {
               if( aucCallServicedTimer_F_50ms[i] >= CALL_SERVICED_TIMEOUT_50MS )
               {
                  aucCallServicedTimer_F_50ms[i] = 0;
                  /* For EMS1 allow calls to be cleared on the riser board. */
                  if( pstCarData->eAutoMode != MODE_A__EMS1 )
                  {
                     if( enPriority == HC_DIR__UP )
                     {
                        ClrRawLatchedHallCalls(ucDestinationLanding, HC_DIR__UP, pstCarData->uiHallMask_F);
                     }
                     else if( enPriority == HC_DIR__DOWN )
                     {
                        ClrRawLatchedHallCalls(ucDestinationLanding, HC_DIR__DOWN, pstCarData->uiHallMask_F);
                     }
                  }
               }
               else
               {
                  aucCallServicedTimer_F_50ms[i]++;
               }
            }
            else
            {
               aucCallServicedTimer_F_50ms[i] = 0;
            }

            if( CarData_GetDoorOpen_Rear(i) )
            {
               if( aucCallServicedTimer_R_50ms[i] >= CALL_SERVICED_TIMEOUT_50MS )
               {
                  aucCallServicedTimer_R_50ms[i] = 0;
                  /* For EMS1 allow calls to be cleared on the riser board. */
                  if( pstCarData->eAutoMode != MODE_A__EMS1 )
                  {
                     if( enPriority == HC_DIR__UP )
                     {
                        ClrRawLatchedHallCalls(ucDestinationLanding, HC_DIR__UP, pstCarData->uiHallMask_R);
                     }
                     else if( enPriority == HC_DIR__DOWN )
                     {
                        ClrRawLatchedHallCalls(ucDestinationLanding, HC_DIR__DOWN, pstCarData->uiHallMask_R);
                     }
                  }
               }
               else
               {
                  aucCallServicedTimer_R_50ms[i]++;
               }
            }
            else
            {
               aucCallServicedTimer_R_50ms[i] = 0;
            }
         }
         else
         {
            aucCallServicedTimer_F_50ms[i] = 0;
            aucCallServicedTimer_R_50ms[i] = 0;
         }
      }
      else
      {
         aucCallServicedTimer_F_50ms[i] = 0;
         aucCallServicedTimer_R_50ms[i] = 0;
      }
   }

   /* Special call timeout handling for manual doors.
    * Each car will independently send out clear command to other cars when timeout is detected. */
   if( ( GetFP_DoorType(DOOR_FRONT) == DOOR_TYPE__MANUAL )
    || ( GetFP_DoorType(DOOR_REAR) == DOOR_TYPE__MANUAL ) )
   {
      uint16_t uwCallTimeout_50ms = DEFAULT_CLEAR_TIME_50MS + (Param_ReadValue_8Bit( enPARAM8__DoorDwellHallTime_1s ) * 20);
      st_cardata *pstCarData = GetCarDataStructure(GetFP_GroupCarIndex());
      uint8_t ucDestinationLanding = pstCarData->ucDestinationLanding;
      uint8_t ucCurrentLanding = pstCarData->ucCurrentLanding;
      en_hc_dir enPriority = pstCarData->enPriority;
      if( ( pstCarData->uiHallMask_F || pstCarData->uiHallMask_R )
       && ( ucDestinationLanding < MAX_NUM_FLOORS )
       && ( ucDestinationLanding == ucCurrentLanding )
       && ( !GetMotion_RunFlag() )
       && ( pstCarData->eAutoMode != MODE_A__EMS1 ) )
      {
         if( ++uwCallTimeoutCounter_50ms >= uwCallTimeout_50ms )
         {
            uint32_t uiMask = 0;
            if( GetFP_DoorType(DOOR_FRONT) == DOOR_TYPE__MANUAL )
            {
               uiMask |= pstCarData->uiHallMask_F;
            }
            if( ( GetFP_RearDoors() )
             && ( GetFP_DoorType(DOOR_FRONT) == DOOR_TYPE__MANUAL ) )
            {
               uiMask |= pstCarData->uiHallMask_R;
            }

            if( enPriority == HC_DIR__UP )
            {
               ClrRawLatchedHallCalls(ucCurrentLanding, HC_DIR__UP, uiMask);
               MasterCommand_ClrHallCall(ucCurrentLanding, HC_DIR__UP, uiMask);
            }
            else if( enPriority == HC_DIR__DOWN )
            {
               ClrRawLatchedHallCalls(ucCurrentLanding, HC_DIR__DOWN, uiMask);
               MasterCommand_ClrHallCall(ucCurrentLanding, HC_DIR__DOWN, uiMask);
            }
            uwCallTimeoutCounter_50ms = 0;
         }
      }
      else
      {
         uwCallTimeoutCounter_50ms = 0;
      }
   }
}
/*-----------------------------------------------------------------------------
   Clear any hall calls latched for secured floors
 -----------------------------------------------------------------------------*/
static void ClearSecuredHallCalls(void)
{
   static uint8_t ucLanding;
   uint8_t ucHighestLanding = CarData_GetHighestActiveLanding();

   ClearSecuredHallCalls_ByLanding(ucLanding);

   ucLanding = (ucLanding+1) % (ucHighestLanding+1);
}
/*----------------------------------------------------------------------------
   Updates car data structures with received car calls
 *----------------------------------------------------------------------------*/
static void UpdateCarCalls()
{
   enum en_group_net_nodes eLocalCarID = GetFP_GroupCarIndex();
   un_sdata_datagram *punDatagram;
   GroupNet_DatagramID eGroupDatagramID;

   for( uint8_t i = 0; i < MAX_GROUP_CARS; i++ )
   {
      if( ( i != eLocalCarID )
       && ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS ) )
      {
         /* CC1 */
         eGroupDatagramID = GroupNet_LocalCarIDToGlobalID( i, DG_GroupNet_MRB__CarCalls1 );
         punDatagram = GetGroupDatagram(eGroupDatagramID);
         UnloadDatagram_CarCalls1(i, punDatagram);

         /* CC2 */
         eGroupDatagramID = GroupNet_LocalCarIDToGlobalID( i, DG_GroupNet_MRB__CarCalls2 );
         punDatagram = GetGroupDatagram(eGroupDatagramID);
         UnloadDatagram_CarCalls2(i, punDatagram);

         /* CC3 */
         eGroupDatagramID = GroupNet_LocalCarIDToGlobalID( i, DG_GroupNet_MRB__CarCalls3 );
         punDatagram = GetGroupDatagram(eGroupDatagramID);
         UnloadDatagram_CarCalls3(i, punDatagram);
      }
   }
}
/*-----------------------------------------------------------------------------
   Update car destinations
   Currently lowest index car has highest priority for taking a call
 -----------------------------------------------------------------------------*/
#define GROUP_DESTINATION_UPDATE_RATE_MS     (300)
static void UpdateGroupDestinations( struct st_module *pstThisModule )
{
   static uint16_t uwUpdateTimer_ms;
   if( uwUpdateTimer_ms < GROUP_DESTINATION_UPDATE_RATE_MS )
   {
      uwUpdateTimer_ms+=pstThisModule->uwRunPeriod_1ms;
   }
   else
   {
      UpdateGroupCarDestinations();

      Vip_Dispatching();

      uwUpdateTimer_ms = 0;
   }
}
/*-----------------------------------------------------------------------------
   This function detects when a car has been assigned but
   has not started moving for a user defined period. If this
   occurs the car will remove itself from the dispatching process for a user defined time.
 -----------------------------------------------------------------------------*/
#define MINIMUM_DISPATCH_TIMEOUT__MS         (10000)
static void CheckForDispatchingTimeout( struct st_module *pstThisModule )
{
   static uint32_t uiTimeoutCounter_ms;
   static uint32_t uiOfflineCountdown_ms;
   st_cardata *pstCarData = GetCarDataStructure(GetFP_GroupCarIndex());
   if( ( ( ( GetOperation_AutoMode() != MODE_A__ATTENDANT ) && Param_ReadValue_8Bit(enPARAM8__DispatchTimeout_1s) )
      || ( ( GetOperation_AutoMode() == MODE_A__ATTENDANT ) && Param_ReadValue_8Bit(enPARAM8__ATTD_DispatchTimeout_1s) ) )
    && ( Param_ReadValue_8Bit(enPARAM8__DispatchOffline_1s) )
    && ( pstCarData->uiHallMask_F || pstCarData->uiHallMask_R ) ) // Ignore check if not in group
   {
      uint32_t uiTimeoutLimit_ms = Param_ReadValue_8Bit(enPARAM8__DispatchTimeout_1s) * 1000; // Scale to ms
      if( GetOperation_AutoMode() == MODE_A__ATTENDANT )
      {
         uiTimeoutLimit_ms = Param_ReadValue_8Bit(enPARAM8__ATTD_DispatchTimeout_1s) * 1000; // Scale to ms
      }
      if( uiTimeoutLimit_ms < MINIMUM_DISPATCH_TIMEOUT__MS )
      {
         uiTimeoutLimit_ms = MINIMUM_DISPATCH_TIMEOUT__MS;
      }

      /* If a destination is assigned but car has not started moving for an extended period... */
      if( ( !GetMotion_RunFlag() )
       && ( CarDestination_GetLanding(GetFP_GroupCarIndex()) != INVALID_LANDING )
       && ( CarDestination_GetLanding(GetFP_GroupCarIndex()) != pstCarData->ucCurrentLanding )
       && ( CarDestination_GetDirection(GetFP_GroupCarIndex()) != DIR__NONE ) // Ignore CCs since they aren't blocking other cars
       )
      {
         if( uiTimeoutCounter_ms < uiTimeoutLimit_ms )
         {
            uiTimeoutCounter_ms += pstThisModule->uwRunPeriod_1ms;
         }
         else
         {
            uiTimeoutCounter_ms = 0;
            uiOfflineCountdown_ms = Param_ReadValue_8Bit(enPARAM8__DispatchOffline_1s) * 1000; // Scale to ms

            en_alarms eAlarm = ALM__DISPATCH_TIMEOUT_C1 + GetFP_GroupCarIndex();
            if( eAlarm > ALM__DISPATCH_TIMEOUT_C8 )
            {
               eAlarm = ALM__DISPATCH_TIMEOUT_C8;
            }
            SetAlarm( eAlarm );
         }
      }
      else
      {
         uiTimeoutCounter_ms = 0;
      }

      /* ...Then remove the car from the group for a user defined period. */
      if( uiOfflineCountdown_ms >= pstThisModule->uwRunPeriod_1ms )
      {
         uiOfflineCountdown_ms -= pstThisModule->uwRunPeriod_1ms;
         bRemoveCarFromGroupFlag = 1;
      }
      else
      {
         bRemoveCarFromGroupFlag = 0;
      }
   }
   else
   {
      bRemoveCarFromGroupFlag = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckForCarOffline( struct st_module *pstThisModule )
{
   static uint8_t abLastOnline[MAX_GROUP_CARS];
   IncrementAllCarOfflineTimers(pstThisModule);
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      uint8_t bOnline = 0;
      if( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
      {
         bOnline = 1;
      }
      else if( abLastOnline[i] )
      {
         SetAlarm(ALM__CAR_OFFLINE_1+i);
         Init_CarDataStructure_ByCar(i);
      }

      abLastOnline[i] = bOnline;
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   Init_CarDestinationStructure();
   Init_CarDataStructure();
   //------------------------------------------------
   pstThisModule->uwInitialDelay_1ms = 3000;
   pstThisModule->uwRunPeriod_1ms = 50;

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static uint8_t bLastMasterFlag;

   CheckForCarOffline(pstThisModule);

   ClearInvalidHallCalls();
   ClearServicedHallCalls();
   ClearSecuredHallCalls();
   UpdateCarCalls();

   if( GetMasterDispatcherFlag() )
   {
      UpdateGroupDestinations(pstThisModule);
   }
   else if(bLastMasterFlag)
   {
//      Init_CarDestinationStructure();
      //TODO set alarm
   }
   bLastMasterFlag = GetMasterDispatcherFlag();

   UpdateSwingCall();

   CheckForDispatchingTimeout(pstThisModule);

   /* Fully remove car from group (clear its latchable hall mask) if it has been faulted for an extended period. */
   if( gstFault.bActiveFault )
   {
      if( uwRemoveFaultCarFromGroupCounter_50ms < REMOVE_FAULTED_CAR_FROM_GROUP_LIMIT_50MS )
      {
         uwRemoveFaultCarFromGroupCounter_50ms++;
      }
   }
   else
   {
      uwRemoveFaultCarFromGroupCounter_50ms = 0;
   }

   Dispatch_UpdateEmergencyDispatchFlag();
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

