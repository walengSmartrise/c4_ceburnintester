/***
 *       _____                          __         _
 *      / ___/ ____ ___   ____ _ _____ / /_ _____ (_)_____ ___
 *      \__ \ / __ `__ \ / __ `// ___// __// ___// // ___// _ \
 *     ___/ // / / / / // /_/ // /   / /_ / /   / /(__  )/  __/
 *    /____//_/ /_/ /_/ \__,_//_/    \__//_/   /_//____/ \___/
 *        ______               _                           _
 *       / ____/____   ____ _ (_)____   ___   ___   _____ (_)____   ____ _
 *      / __/  / __ \ / __ `// // __ \ / _ \ / _ \ / ___// // __ \ / __ `/
 *     / /___ / / / // /_/ // // / / //  __//  __// /   / // / / // /_/ /
 *    /_____//_/ /_/ \__, //_//_/ /_/ \___/ \___//_/   /_//_/ /_/ \__, /
 *                  /____/                                       /____/
 */
/******************************************************************************
 *
 * @file
 * @brief    Logic for to handle VIP dispatching
 * @version
 * @date
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sru_b.h"
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "shared_data.h"

#include "carData.h"
#include "carDestination.h"
#include "groupnet.h"
#include "groupnet_datagrams.h"
#include "group_net_data.h"
#include "XRegData.h"
#include "hallSecurity.h"
#include "masterCommand.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static uint8_t VIP_GetLandingsAway( enum en_group_net_nodes eCarID, uint8_t ucDestLanding );
static uint8_t VIP_GetLatchedHallCallUp_Front_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding );
static uint8_t VIP_GetLatchedHallCallUp_Rear_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding );
static uint8_t VIP_GetLatchedHallCallDown_Front_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding );
static uint8_t VIP_GetLatchedHallCallDown_Rear_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding );
static void VIP_UpdateProposedDestination_ByCar( enum en_group_net_nodes eCarID );
static void VIP_ClrProposedDestination_ByCar(enum en_group_net_nodes eCarID);
static void VIP_ClrCarDestination_ByCar(enum en_group_net_nodes eCarID);
static void VIP_UpdateGroupCarDestinations();
static const st_carDestination stVIP_ClearedCarDestination =
{
  .bDirty = 0,
  .eCallDir = DIR__NONE,
  .eDoor = DOOR_FRONT,
  .ucLanding = INVALID_LANDING,
  .uiMask = 0,
};
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static st_carDestination aucVIPLandings_Plus1[MAX_GROUP_CARS];
static st_carDestination VIP_astProposedDestination[MAX_GROUP_CARS];
static uint8_t aucFaultDebounceCounter__300ms[MAX_GROUP_CARS];
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define FAULT_DEBOUNCE_COUNTER_LIMIT__300MS        (33) // ~10 seconds
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

void Vip_Dispatching()
{

   // ks note

   // 1 Send out dedicated swing call mask from each car.
   // 2 loop through each car and determing the best swing call for each (based on distance and served direction)
   // Look only at swing calls by modifying GetNextDestinationAbove with a new version that just checks the raw hall calls agains the swing mask

   // i.e. GetRawLatchedHallCalls( ucLanding, HC_DIR__DOWN ) & pstCarData->uiVIPMask_F. see GetLatchedHallCallDown_Rear_ByCar
   // modify GetNextDestinationAbove_ByCar to look at the modifid GetLatchedHallCallDown_Rear_ByCar functions
   // with modified GetNextDestinationAbove_ByCar functions, you can create a modified UpdateDestination_ByCar
   // with modified UpdateDestination_ByCar make modified UpdateGroupCarDestinations
   // the difference between the modified functions and the carDestination.c functions will be that they look only at swing calls and not standard hall calls and car calls.


   // 3 assignement timeouts if car fails to go on VIP mode after being assigned, or if the car fails to be captured.
   VIP_UpdateGroupCarDestinations();

   st_cardata *pstCarData = GetCarDataStructure( GetFP_GroupCarIndex() );
   pstCarData->ucVIPFloor_Plus1 = 0;
   if( aucVIPLandings_Plus1[GetFP_GroupCarIndex()].ucLanding != INVALID_LANDING )
   {
      pstCarData->ucVIPFloor_Plus1 = GetCarMapping(aucVIPLandings_Plus1[GetFP_GroupCarIndex()].ucLanding) + 1;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

void VIP_UnloadDatagram_MasterDispatcher( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )//Todo: Develop the logic for slave listening
{
   st_cardata *pstCarData = GetCarDataStructure( GetFP_GroupCarIndex() );
   if( !GetMasterDispatcherFlag() && GetMasterDispatcherFlag_ByCar(eCarID) )
   {
      if( pstCarData->bVIP
     && ( punDatagram->auc8[GetFP_GroupCarIndex()] != INVALID_LANDING ) )
      {
         pstCarData->ucVIPFloor_Plus1 = GetCarMapping(punDatagram->auc8[GetFP_GroupCarIndex()]) + 1;
      }
      else
      {
         pstCarData->ucVIPFloor_Plus1 = 0;
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define MASTER_DISPATCHER_MIN_RESEND_MS      (1000)
void VIP_LoadDatagram_MasterDispatcher( struct st_module *pstThisModule )
{
   un_sdata_datagram unDatagram;

   static uint16_t ucDelayCounter_5ms;
   if( GetMasterDispatcherFlag()  )
   {
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         unDatagram.auc8[i] = aucVIPLandings_Plus1[i].ucLanding;
      }

      SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__VIPMasterDispatcher, &unDatagram);
      if( !SDATA_DirtyBit_Get(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__VIPMasterDispatcher) )
      {
         if( ucDelayCounter_5ms < MASTER_DISPATCHER_MIN_RESEND_MS )
         {
            ucDelayCounter_5ms += pstThisModule->uwRunPeriod_1ms;
         }
         else
         {
            ucDelayCounter_5ms = 0;
            SDATA_DirtyBit_Set(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__VIPMasterDispatcher);
            InsertGroupNetDatagram_ByDatagramID(&unDatagram, GroupNet_LocalCarIDToGlobalID(GetFP_GroupCarIndex(), DG_GroupNet_MRB__VIPMasterDispatcher));
         }
      }
      else
      {
         ucDelayCounter_5ms = 0;
      }
   }
   else
   {
      SDATA_DirtyBit_Clr(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__VIPMasterDispatcher);
   }
}
/*-----------------------------------------------------------------------------
// ks note: should support multiple VIP calls.
 -----------------------------------------------------------------------------*/
static uint8_t VIP_GetLandingsAway( enum en_group_net_nodes eCarID, uint8_t ucDestLanding )
{
   uint8_t ucLandingsAway = INVALID_FLOOR;
   st_cardata *pstCarData = GetCarDataStructure(eCarID);
   uint8_t ucCurrentLanding = pstCarData->ucCurrentLanding;

   if(ucDestLanding > ucCurrentLanding)
   {
      ucLandingsAway = ucDestLanding - ucCurrentLanding;
   }
   else
   {
      ucLandingsAway = ucCurrentLanding - ucDestLanding;
   }
   return ucLandingsAway;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void VIP_SetGroupCarDestination( enum en_group_net_nodes eCarID, st_carDestination *pstDestination )
{
   /* Load new destination and mark for send if changed */
   if( ( aucVIPLandings_Plus1[eCarID].eCallDir != pstDestination->eCallDir )
    || ( aucVIPLandings_Plus1[eCarID].eDoor != pstDestination->eDoor )
    || ( aucVIPLandings_Plus1[eCarID].uiMask != pstDestination->uiMask )
    || ( aucVIPLandings_Plus1[eCarID].ucLanding != pstDestination->ucLanding ) )
   {
      aucVIPLandings_Plus1[eCarID].bDirty = 1;
      aucVIPLandings_Plus1[eCarID].eCallDir = pstDestination->eCallDir;
      aucVIPLandings_Plus1[eCarID].eDoor = pstDestination->eDoor;
      aucVIPLandings_Plus1[eCarID].uiMask = pstDestination->uiMask;
      aucVIPLandings_Plus1[eCarID].ucLanding = pstDestination->ucLanding;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void VIP_GetNextDestinationAbove_ByCar( enum en_group_net_nodes eCarID, uint8_t ucStartLanding, st_carDestination * stDestination)
{
    st_cardata *pstCarData = GetCarDataStructure( eCarID );
    uint8_t ucTopLanding = pstCarData->ucLastLanding;
    uint8_t ucBottomLanding = pstCarData->ucFirstLanding;

    //quick sanity check
    if ( (ucTopLanding < MAX_NUM_FLOORS)
      && (ucStartLanding <= ucTopLanding) )
    {
        /* Scan for Up Hall Calls or Car Calls on the way to to the top terminal */
        for (int8_t fl = ucStartLanding; fl <= ucTopLanding; fl++) {
            //if hall call,  if floor is set, set direction and exit, else set floor and direction
            uint8_t bLatchedCall_F = VIP_GetLatchedHallCallUp_Front_ByCar(eCarID, fl);
            uint8_t bLatchedCall_R = VIP_GetLatchedHallCallUp_Rear_ByCar(eCarID, fl);
            uint8_t bUpCallAllowed = (uint8_t) fl != ucTopLanding; // Don't check up call at top terminal
            bUpCallAllowed &= ( GetDispatchMode_ByCar(eCarID) != DISPATCH_MODE__DOWN) && ( GetDispatchMode_ByCar(eCarID) != DISPATCH_MODE__LOBBY);
            uint8_t bFloorLockout = pstCarData->bSuppressReopen && ( fl == pstCarData->ucCurrentLanding );
            if ( bUpCallAllowed && !bFloorLockout ) {
               if( bLatchedCall_F || bLatchedCall_R )
               {
                  if (stDestination->ucLanding == INVALID_LANDING)
                  {
                      stDestination->ucLanding = (uint8_t) fl;
                      stDestination->eCallDir = DIR__UP;
                      if( bLatchedCall_F )
                      {
                         stDestination->eDoor = ( ( stDestination->eDoor == DOOR_REAR ) || ( stDestination->eDoor == DOOR_ANY ) )
                                              ? DOOR_ANY : DOOR_FRONT;
                      }
                      if( bLatchedCall_R )
                      {
                         stDestination->eDoor = ( ( stDestination->eDoor == DOOR_FRONT ) || ( stDestination->eDoor == DOOR_ANY ) )
                                              ? DOOR_ANY : DOOR_REAR;
                      }
                  }
                  else if( ( stDestination->eCallDir == DIR__NONE ) && ( stDestination->ucLanding <= (uint8_t) fl ) )
                  {
                     stDestination->eCallDir = DIR__UP;
                  }
                  break;
               }
            }
        }

         //if only a car call is found in the direction, look for anything on the way back up to set direction.
         //if coming back up the floor is the same floor as the car call above, set direction to turn around.
         if ( ( stDestination->ucLanding == INVALID_LANDING ) || ( stDestination->eCallDir == DIR__NONE ) ) {
            for (int8_t fl = ucTopLanding; fl > ucStartLanding; fl--) {
               uint8_t bLatchedCall_F = VIP_GetLatchedHallCallDown_Front_ByCar(eCarID, fl);
               uint8_t bLatchedCall_R = VIP_GetLatchedHallCallDown_Rear_ByCar(eCarID, fl);
               uint8_t bDownCallAllowed = (uint8_t) fl != ucBottomLanding; // Don't check up call at bottom terminal
               bDownCallAllowed &= ( GetDispatchMode_ByCar(eCarID) != DISPATCH_MODE__UP) && ( GetDispatchMode_ByCar(eCarID) != DISPATCH_MODE__LOBBY);
               uint8_t bFloorLockout = pstCarData->bSuppressReopen && ( fl == pstCarData->ucCurrentLanding );
               if ( bDownCallAllowed && !bFloorLockout ) {
                    if( bLatchedCall_F || bLatchedCall_R )
                    {
                       /* No destination found in the priority direction, record the first down call encountered */
                       if (stDestination->ucLanding == INVALID_LANDING)
                       {
                           stDestination->ucLanding = (uint8_t) fl;
                           stDestination->eCallDir = DIR__DN;
                           if( bLatchedCall_F )
                           {
                              stDestination->eDoor = ( ( stDestination->eDoor == DOOR_REAR ) || ( stDestination->eDoor == DOOR_ANY ) )
                                                   ? DOOR_ANY : DOOR_FRONT;
                           }
                           if( bLatchedCall_R )
                           {
                              stDestination->eDoor = ( ( stDestination->eDoor == DOOR_FRONT ) || ( stDestination->eDoor == DOOR_ANY ) )
                                                   ? DOOR_ANY : DOOR_REAR;
                           }
                       }
                       /* First destination found is a CC, if next found is a HC above the CC, set the arrival direction */
                       else if (fl > stDestination->ucLanding)
                       {
                           stDestination->eCallDir = DIR__UP;
                       }
                       /* First destination found is a CC, if next found is a HC below or equal to the CC, set the arrival direction */
                       else if (fl < stDestination->ucLanding)
                       {
                           stDestination->eCallDir = DIR__DN;
                       }
                       else if (fl == stDestination->ucLanding)
                       {
                          stDestination->eCallDir = DIR__DN;
                          if( bLatchedCall_F )
                          {
                             stDestination->eDoor = ( ( stDestination->eDoor == DOOR_REAR ) || ( stDestination->eDoor == DOOR_ANY ) )
                                                  ? DOOR_ANY : DOOR_FRONT;
                          }
                          if( bLatchedCall_R )
                          {
                             stDestination->eDoor = ( ( stDestination->eDoor == DOOR_FRONT ) || ( stDestination->eDoor == DOOR_ANY ) )
                                                  ? DOOR_ANY : DOOR_REAR;
                          }
                       }
                       break;
                    }
                }
            }
        }
    }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void VIP_GetNextDestinationBelow_ByCar(  enum en_group_net_nodes eCarID, uint8_t ucStartLanding, st_carDestination * stDestination)
{
    st_cardata *pstCarData = GetCarDataStructure( eCarID );
    uint8_t ucTopLanding = pstCarData->ucLastLanding;
    uint8_t ucBottomLanding = pstCarData->ucFirstLanding;

    //sanity check
    if (ucStartLanding < MAX_NUM_FLOORS
     && ucStartLanding <= ucTopLanding ){
          /* Scan for Up Hall Calls or Car Calls on the way to to the top terminal */
          for (int8_t fl = ucStartLanding; fl >= ucBottomLanding; fl--)
          {
            //if hall call,  if floor is set, set direction and exit, else set floor and direction
            uint8_t bLatchedCall_F = VIP_GetLatchedHallCallDown_Front_ByCar(eCarID, fl);
            uint8_t bLatchedCall_R = VIP_GetLatchedHallCallDown_Rear_ByCar(eCarID, fl);
            uint8_t bDownCallAllowed = (uint8_t) fl != ucBottomLanding; // Don't check up call at bottom terminal
            bDownCallAllowed &= ( GetDispatchMode_ByCar(eCarID) != DISPATCH_MODE__UP) && ( GetDispatchMode_ByCar(eCarID) != DISPATCH_MODE__LOBBY);
            uint8_t bFloorLockout = pstCarData->bSuppressReopen && ( fl == pstCarData->ucCurrentLanding );
            if ( bDownCallAllowed && !bFloorLockout ) {
               if( bLatchedCall_F || bLatchedCall_R )
               {
                  if (stDestination->ucLanding == INVALID_LANDING)
                  {
                      stDestination->ucLanding = (uint8_t) fl;
                      stDestination->eCallDir = DIR__DN;
                      if( bLatchedCall_F )
                      {
                         stDestination->eDoor = ( ( stDestination->eDoor == DOOR_REAR ) || ( stDestination->eDoor == DOOR_ANY ) )
                                              ? DOOR_ANY : DOOR_FRONT;
                      }
                      if( bLatchedCall_R )
                      {
                         stDestination->eDoor = ( ( stDestination->eDoor == DOOR_FRONT ) || ( stDestination->eDoor == DOOR_ANY ) )
                                              ? DOOR_ANY : DOOR_REAR;
                      }
                  }
                  else if( ( stDestination->eCallDir == DIR__NONE ) && ( stDestination->ucLanding >= (uint8_t) fl ) )
                  {
                      stDestination->eCallDir = DIR__DN;
                  }
                  break;
               }
            }
        }

         //if only a car call is found in the direction, look for anything on the way back up to set direction.
         //if coming back up the floor is the same floor as the car call above, set direction to turn around.
         if ( ( stDestination->ucLanding == INVALID_LANDING ) || ( stDestination->eCallDir == DIR__NONE ) ) {
            for (int8_t fl = ucBottomLanding; fl < ucStartLanding; fl++) {
               uint8_t bLatchedCall_F = VIP_GetLatchedHallCallUp_Front_ByCar(eCarID, fl);
               uint8_t bLatchedCall_R = VIP_GetLatchedHallCallUp_Rear_ByCar(eCarID, fl);
               uint8_t bUpCallAllowed = (uint8_t) fl != ucTopLanding; // Don't check up call at top terminal
               bUpCallAllowed &= ( GetDispatchMode_ByCar(eCarID) != DISPATCH_MODE__DOWN) && ( GetDispatchMode_ByCar(eCarID) != DISPATCH_MODE__LOBBY);
               uint8_t bFloorLockout = pstCarData->bSuppressReopen && ( fl == pstCarData->ucCurrentLanding );
               if ( bUpCallAllowed && !bFloorLockout ) {
                    if( bLatchedCall_F || bLatchedCall_R )
                    {
                       /* No destination found in the priority direction, record the first up call encountered */
                       if (stDestination->ucLanding == INVALID_LANDING)
                       {
                           stDestination->ucLanding = (uint8_t) fl;
                           stDestination->eCallDir = DIR__UP;
                           if( bLatchedCall_F )
                           {
                              stDestination->eDoor = ( ( stDestination->eDoor == DOOR_REAR ) || ( stDestination->eDoor == DOOR_ANY ) )
                                                   ? DOOR_ANY : DOOR_FRONT;
                           }
                           if( bLatchedCall_R )
                           {
                              stDestination->eDoor = ( ( stDestination->eDoor == DOOR_FRONT ) || ( stDestination->eDoor == DOOR_ANY ) )
                                                   ? DOOR_ANY : DOOR_REAR;
                           }
                       }
                       /* First destination found is a CC, if next found is a HC below the CC, set the arrival direction */
                       else if (fl < stDestination->ucLanding)
                       {
                           stDestination->eCallDir = DIR__DN;
                       }
                       /* First destination found is a CC, if next found is a HC above or equal to the CC, set the arrival direction */
                       else if (fl > stDestination->ucLanding)
                       {
                           stDestination->eCallDir = DIR__UP;
                       }
                       else if (fl == stDestination->ucLanding)
                       {
                          stDestination->eCallDir = DIR__UP;
                          if( bLatchedCall_F )
                          {
                             stDestination->eDoor = ( ( stDestination->eDoor == DOOR_REAR ) || ( stDestination->eDoor == DOOR_ANY ) )
                                                  ? DOOR_ANY : DOOR_FRONT;
                          }
                          if( bLatchedCall_R )
                          {
                             stDestination->eDoor = ( ( stDestination->eDoor == DOOR_FRONT ) || ( stDestination->eDoor == DOOR_ANY ) )
                                                  ? DOOR_ANY : DOOR_REAR;
                          }
                       }
                       break;
                    }
                }
            }
        }
    }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t VIP_GetLatchedHallCallUp_Front_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding )
{
   uint8_t bLatched = 0;
   uint8_t bValidOpening = CarData_CheckOpening( eCarID, ucLanding, DOOR_FRONT );
   uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__UP );
   if( uiLatchedCalls && bValidOpening )
   {
      // mask out calls already assigned
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         if( ( i != eCarID )
          && ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( aucVIPLandings_Plus1[i].ucLanding == ucLanding )
          && ( aucVIPLandings_Plus1[i].eCallDir == DIR__UP )
          && ( aucVIPLandings_Plus1[i].eDoor != DOOR_REAR ) )
         {
            uiLatchedCalls &= ~(aucVIPLandings_Plus1[i].uiMask);
         }
      }
      // Look only at calls this car can take
      uiLatchedCalls &= CarData_VIP_GetHallMask(eCarID, DOOR_FRONT);
      if( uiLatchedCalls
       && ( ( GetHallCallSecurity(eCarID, ucLanding, DOOR_FRONT, uiLatchedCalls) ) ) )
      {
         bLatched = 1;
      }
   }
   return bLatched;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t VIP_GetLatchedHallCallUp_Rear_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding )
{
   uint8_t bLatched = 0;
   uint8_t bValidOpening = CarData_CheckOpening( eCarID, ucLanding, DOOR_REAR );
   uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__UP );
   if( uiLatchedCalls && bValidOpening )
   {
      // mask out calls already assigned
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         if( ( i != eCarID )
          && ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( aucVIPLandings_Plus1[i].ucLanding == ucLanding )
          && ( aucVIPLandings_Plus1[i].eCallDir == DIR__UP )
          && ( aucVIPLandings_Plus1[i].eDoor != DOOR_FRONT ) )
         {
            uiLatchedCalls &= ~(aucVIPLandings_Plus1[i].uiMask);
         }
      }

      // Look only at calls this car can take
      uiLatchedCalls &= CarData_VIP_GetHallMask(eCarID, DOOR_REAR);
      if( uiLatchedCalls
       && ( ( GetHallCallSecurity(eCarID, ucLanding, DOOR_REAR, uiLatchedCalls) ) ) )
      {
         bLatched = 1;
      }
   }
   return bLatched;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t VIP_GetLatchedHallCallDown_Front_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding )
{
   uint8_t bLatched = 0;
   uint8_t bValidOpening = CarData_CheckOpening( eCarID, ucLanding, DOOR_FRONT );
   uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__DOWN );
   if( uiLatchedCalls && bValidOpening )
   {
      // mask out calls already assigned
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         if( ( i != eCarID )
          && ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( aucVIPLandings_Plus1[i].ucLanding == ucLanding )
          && ( aucVIPLandings_Plus1[i].eCallDir == DIR__DN )
          && ( aucVIPLandings_Plus1[i].eDoor != DOOR_REAR ) )
         {
            uiLatchedCalls &= ~(aucVIPLandings_Plus1[i].uiMask);
         }
      }

      // Look only at calls this car can take
      uiLatchedCalls &= CarData_VIP_GetHallMask(eCarID, DOOR_FRONT);
      if( uiLatchedCalls
       && ( ( GetHallCallSecurity(eCarID, ucLanding, DOOR_FRONT, uiLatchedCalls) ) ) )
      {
         bLatched = 1;
      }
   }
   return bLatched;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t VIP_GetLatchedHallCallDown_Rear_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding )
{
   uint8_t bLatched = 0;
   uint8_t bValidOpening = CarData_CheckOpening( eCarID, ucLanding, DOOR_REAR );
   uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__DOWN );
   if( uiLatchedCalls && bValidOpening )
   {
      // mask out calls already assigned
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         if( ( i != eCarID )
          && ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( aucVIPLandings_Plus1[i].ucLanding == ucLanding )
          && ( aucVIPLandings_Plus1[i].eCallDir == DIR__DN )
          && ( aucVIPLandings_Plus1[i].eDoor != DOOR_FRONT ) )
         {
            uiLatchedCalls &= ~(aucVIPLandings_Plus1[i].uiMask);
         }
      }

      // Look only at calls this car can take
      uiLatchedCalls &= CarData_VIP_GetHallMask(eCarID, DOOR_REAR);
      if( uiLatchedCalls
       && ( ( GetHallCallSecurity(eCarID, ucLanding, DOOR_REAR, uiLatchedCalls) ) ) )
      {
         bLatched = 1;
      }
   }
   return bLatched;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void VIP_UpdateDestination_ByCar( enum en_group_net_nodes eCarID )
{
   // If car is online & in correct mode.
   st_carDestination stDestination;
   stDestination.eCallDir = DIR__NONE;
   stDestination.ucLanding = INVALID_LANDING;
   stDestination.eDoor = NUM_OF_DOORS;
   stDestination.uiMask = 0;

   st_cardata *pstCarData = GetCarDataStructure( eCarID );

   uint8_t bUpLamp = GetOutputValue_ByCar(eCarID, enOUT_ARV_UP_F) || GetOutputValue_ByCar(eCarID, enOUT_ARV_UP_R);
   uint8_t bDownLamp = GetOutputValue_ByCar(eCarID, enOUT_ARV_DN_F) || GetOutputValue_ByCar(eCarID, enOUT_ARV_DN_R);

   // If moving UP
   if( pstCarData->cMotionDir == DIR__UP )
   {
      //if destination floor is closer than the 'reachable' floor reassign reachable floor to destination
      uint8_t ucReachableLanding = pstCarData->ucReachableLanding;
      if( pstCarData->ucDestinationLanding < pstCarData->ucReachableLanding )
      {
         ucReachableLanding = pstCarData->ucDestinationLanding;
      }

      /* Don't allow checking of destinations above after already preparing
       * for a destination change. Otherwise car may try to move to a
       * destination opposite its indicated arrival direction */
      if( !( ( pstCarData->enPriority == HC_DIR__DOWN ) && ( pstCarData->ucCurrentLanding == pstCarData->ucDestinationLanding ) )
       && !bDownLamp )
      {
         VIP_GetNextDestinationAbove_ByCar( eCarID, ucReachableLanding, &stDestination);
      }

      if( !bUpLamp )
      {
         if ( ( stDestination.ucLanding == INVALID_LANDING ) || ( stDestination.eCallDir == DIR__NONE ) ) {
            VIP_GetNextDestinationBelow_ByCar( eCarID, ucReachableLanding, &stDestination);
         }
      }
   }
   // If moving DOWN
   else if( pstCarData->cMotionDir == DIR__DN )
   {
      //if destination floor is closer than the 'reachable' floor reassign reachable floor to destination
      uint8_t ucReachableLanding = pstCarData->ucReachableLanding;
      if( pstCarData->ucDestinationLanding > pstCarData->ucReachableLanding )
      {
         ucReachableLanding = pstCarData->ucDestinationLanding;
      }

      /* Don't allow checking of destinations below after already preparing
       * for a destination change. Otherwise car may try to move to a
       * destination opposite its indicated arrival direction */
      if( !( ( pstCarData->enPriority == HC_DIR__UP ) && ( pstCarData->ucCurrentLanding == pstCarData->ucDestinationLanding ) )
       && !bUpLamp )
      {
         VIP_GetNextDestinationBelow_ByCar( eCarID, ucReachableLanding, &stDestination);
      }

      if( !bDownLamp )
      {
         if ( ( stDestination.ucLanding == INVALID_LANDING ) || ( stDestination.eCallDir == DIR__NONE ) ) {
            VIP_GetNextDestinationAbove_ByCar( eCarID, ucReachableLanding, &stDestination);
         }
      }
   }
   // If stopped, and serving UP calls
   else if( pstCarData->enPriority == HC_DIR__UP )
   {
      /* Suppress above destination check if arrival down lamps already set. */
      if( !bDownLamp )
      {
         VIP_GetNextDestinationAbove_ByCar( eCarID, pstCarData->ucCurrentLanding, &stDestination);
      }

      if( !bUpLamp && pstCarData->bIdleDirection )
      {
         if ( ( stDestination.ucLanding == INVALID_LANDING ) || ( stDestination.eCallDir == DIR__NONE ) ) {
            VIP_GetNextDestinationBelow_ByCar( eCarID, pstCarData->ucCurrentLanding, &stDestination);
         }
      }
   }
   // If stopped, and serving DOWN calls
   else
   {
      /* Suppress below destination check if arrival up lamps already set. */
      if( !bUpLamp )
      {
         VIP_GetNextDestinationBelow_ByCar( eCarID, pstCarData->ucCurrentLanding, &stDestination);
      }

      if( !bDownLamp && pstCarData->bIdleDirection )
      {
         if ( ( stDestination.ucLanding == INVALID_LANDING ) || ( stDestination.eCallDir == DIR__NONE ) ) {
            VIP_GetNextDestinationAbove_ByCar( eCarID, pstCarData->ucCurrentLanding, &stDestination);
         }
      }
   }

   /* If a valid destination was found and a next destination direction was found,
    * set the destination mask to reserve hall calls in that destination. */
   if( stDestination.eCallDir != DIR__NONE )
   {
      /* Set mask based on destination door */
      if( stDestination.eDoor == DOOR_FRONT )
      {
         stDestination.uiMask = pstCarData->uiVIPMask_F;
      }
      else if( stDestination.eDoor == DOOR_REAR )
      {
         stDestination.uiMask = pstCarData->uiVIPMask_R;
      }
      /* Here DOOR_ANY indicates both doors will open */
      else if( stDestination.eDoor == DOOR_ANY )
      {
         stDestination.uiMask = pstCarData->uiVIPMask_F | pstCarData->uiVIPMask_R;
      }
   }

   VIP_SetGroupCarDestination(eCarID, &stDestination);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t VIP_GenerateAndValidateProposedDestinations()
{
   uint8_t bNoNewProposals = 1;
   /* Generate a proposed destination for cars with invalid destinations */
   for( uint8_t i = 0; i < MAX_GROUP_CARS; i++ )
   {
      if( ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
       && ( VIP_astProposedDestination[i].ucLanding >= MAX_NUM_FLOORS ) )
      {
         VIP_UpdateProposedDestination_ByCar(i);
         if( VIP_astProposedDestination[i].ucLanding < MAX_NUM_FLOORS )
         {
            bNoNewProposals = 0;
         }
      }
   }

   /* Asses proposed destinations, confirm unique destinations, determine closest of duplicates */
   for( uint8_t ucCar1 = 0; ucCar1 < MAX_GROUP_CARS; ucCar1++ )
   {
      uint8_t ucClosestCar_Plus1 = ucCar1+1;
      for( uint8_t ucCar2 = 0; ucCar2 < MAX_GROUP_CARS; ucCar2++ )
      {
         uint8_t ucDest1 = VIP_astProposedDestination[ucCar1].ucLanding;
         uint8_t ucDest2 = VIP_astProposedDestination[ucCar2].ucLanding;
         enum direction_enum eCallDir1 = VIP_astProposedDestination[ucCar1].eCallDir;
         enum direction_enum eCallDir2 = VIP_astProposedDestination[ucCar2].eCallDir;
         /* Check if destination is valid & two cars have same destination */
         if( ( ucCar1 != ucCar2 )
          && ( GetCarOfflineTimer_ByCar(ucCar1) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( GetCarOfflineTimer_ByCar(ucCar2) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( ucDest1 < MAX_NUM_FLOORS )
          && ( ucDest1 == ucDest2 )
          && ( eCallDir1 != DIR__NONE ) // Ignore CCs.
          && ( eCallDir1 == eCallDir2 ) )
         {
            uint8_t bClearCar1 = 0; // When set to 1, clears the proposed destination assigned to ucCar1 and moves on to the next car
            st_cardata *pstCarData1 = GetCarDataStructure(ucCar1);
            st_cardata *pstCarData2 = GetCarDataStructure(ucCar2);
            uint8_t ucDiff1 = VIP_GetLandingsAway(ucCar1, ucDest1);
            uint8_t ucDiff2 = VIP_GetLandingsAway(ucCar2, ucDest2);
            uint8_t bConflictingArrivalLamps1 =  ( ( eCallDir1 == DIR__UP ) && ( GetOutputValue_ByCar(ucCar1, enOUT_ARV_DN_F) || GetOutputValue_ByCar(ucCar1, enOUT_ARV_DN_R) ) )
                                              || ( ( eCallDir1 == DIR__DN ) && ( GetOutputValue_ByCar(ucCar1, enOUT_ARV_UP_F) || GetOutputValue_ByCar(ucCar1, enOUT_ARV_UP_R) ) );
//            uint8_t bConflictingArrivalLamps2 =  ( ( eCallDir2 == DIR__UP ) && ( GetOutputValue_ByCar(ucCar2, enOUT_ARV_DN_F) || GetOutputValue_ByCar(ucCar2, enOUT_ARV_DN_R) ) )
//                                              || ( ( eCallDir2 == DIR__DN ) && ( GetOutputValue_ByCar(ucCar2, enOUT_ARV_UP_F) || GetOutputValue_ByCar(ucCar2, enOUT_ARV_UP_R) ) );
            uint8_t bCorrectDirection1 = ( ( pstCarData1->enPriority == HC_DIR__UP )   && ( eCallDir1 == DIR__UP ) )
                                      || ( ( pstCarData1->enPriority == HC_DIR__DOWN ) && ( eCallDir1 == DIR__DN ) );
            uint8_t bCorrectDirection2 = ( ( pstCarData2->enPriority == HC_DIR__UP )   && ( eCallDir2 == DIR__UP ) )
                                      || ( ( pstCarData2->enPriority == HC_DIR__DOWN ) && ( eCallDir2 == DIR__DN ) );
//            enum en_doors eDoor1 = astProposedDestination[ucCar1].eDoor;
            enum en_doors eDoor2 = VIP_astProposedDestination[ucCar2].eDoor;
//            uint8_t bDoorAlreadyOpen1 = ( ( eDoor1 == DOOR_FRONT ) && CarData_GetDoorOpen_Front(ucCar1) )
//                                     || ( ( eDoor1 == DOOR_REAR ) && CarData_GetDoorOpen_Rear(ucCar1) )
//                                     || ( ( eDoor1 == DOOR_ANY ) && CarData_GetDoorOpen_Front(ucCar1) && CarData_GetDoorOpen_Rear(ucCar1) );
            uint8_t bDoorAlreadyOpen2 = ( ( eDoor2 == DOOR_FRONT ) && CarData_GetDoorOpen_Front(ucCar2) )
                                        || ( ( eDoor2 == DOOR_REAR ) && CarData_GetDoorOpen_Rear(ucCar2) )
                                        || ( ( eDoor2 == DOOR_ANY ) && CarData_GetDoorOpen_Front(ucCar2) && CarData_GetDoorOpen_Rear(ucCar2) );
            /* If car 1 has the incorrect direction
             * and either
             *    it is not able to currently switch directions,
             *    or it is signaling a conflicting arrival direction */

            /* If car 1 is at the destination floor
             * but can't take the call because it has already signaled a direction change,
             * or has incorrect direction and is not idle
             * then clear car 1. */
            if(0){}
//            if( ( !ucDiff1 )
//             && ( bConflictingArrivalLamps1
//             || ( !pstCarData1->cMotionDir && !bCorrectDirection1 && !pstCarData1->bIdleDirection ) ) )
//            {
//               bClearCar1 = 1;
//            }
            /* If both cars are the same distance... */
            else if( ucDiff1 == ucDiff2 )
            {
               /* If both cars are at the destination floor,
                * and car2 has the correct direction,
                * and car2 has the correct doors open
                * then clear car 1 */
               if( ( !ucDiff1 )
                && ( bCorrectDirection2 )
                && ( bDoorAlreadyOpen2 ) )
               {
                  bClearCar1 = 1;
               }
               /* If both cars are at the destination floor,
                * and car 2 has the correct direction, but
                * car 1 does not, clear car 1 */
               else if( ( !ucDiff1 )
                     && ( bCorrectDirection2 )
                     && ( !bCorrectDirection1 ) )
               {
                  bClearCar1 = 1;
               }
               /* If both cars are at the destination floor,
                * and both cars have incorrect direction,
                * and either
                *    car 2 is idle and car1 is not,
                *    or car 1 had opposing direction arrival lanterns
                * then clear car 1 */
               else if( ( !ucDiff1 )
                     && ( !bCorrectDirection1 && !bCorrectDirection2 )
                     && ( ( pstCarData2->bIdleDirection && !pstCarData1->bIdleDirection )
                       || ( bConflictingArrivalLamps1 ) ) )
               {
                  bClearCar1 = 1;
               }
               /*If Car 2 is in a dispatch mode and Car 1 is not, then clear Car 1*/
               else if( (pstCarData2->enDispatchMode != DISPATCH_MODE__NONE ) &&
                        (pstCarData1->enDispatchMode == DISPATCH_MODE__NONE) )
               {
                  bClearCar1 = 1;
               }
               /* Otherwise if car 1 is a higher index car, clear car 1. */
               else if( ucCar1 > ucCar2 )
               {
                  bClearCar1 = 1;
               }
            }
            /* If car 1 is further away than car 2,
             * clear car 1. */
            else if( ucDiff1 > ucDiff2 )
            {
               /* Unless car 2 is idle or heading in the correct direction
                * and car1 has an active conflicting direction */
//               if( ! ( ( !ucDiff2 )
//                    && ( bConflictingArrivalLamps2
//                    || ( !pstCarData2->cMotionDir && !bCorrectDirection2 && !pstCarData2->bIdleDirection ) ) ) )
//               {
                  bClearCar1 = 1;
//               }
            }

            if( bClearCar1 )
            {
               VIP_ClrProposedDestination_ByCar(ucCar1);
               ucClosestCar_Plus1 = 0;
               break;
            }
         }
      }
      if( ( ucClosestCar_Plus1 ) && ( VIP_astProposedDestination[ucClosestCar_Plus1-1].ucLanding < MAX_NUM_FLOORS ) )
      {
         VIP_SetGroupCarDestination(ucClosestCar_Plus1-1, &VIP_astProposedDestination[ucClosestCar_Plus1-1]);
      }
   }
   return bNoNewProposals;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void VIP_UpdateProposedDestination_ByCar( enum en_group_net_nodes eCarID )
{
   // If car is online & in correct mode.
   st_carDestination stDestination;
   stDestination.eCallDir = DIR__NONE;
   stDestination.ucLanding = INVALID_LANDING;
   stDestination.eDoor = NUM_OF_DOORS;
   stDestination.uiMask = 0;
   stDestination.bDirty = 0;

   st_cardata *pstCarData = GetCarDataStructure( eCarID );

   uint8_t bUpLamp = GetOutputValue_ByCar(eCarID, enOUT_ARV_UP_F) || GetOutputValue_ByCar(eCarID, enOUT_ARV_UP_R);
   uint8_t bDownLamp = GetOutputValue_ByCar(eCarID, enOUT_ARV_DN_F) || GetOutputValue_ByCar(eCarID, enOUT_ARV_DN_R);

   // If moving UP
   if( pstCarData->cMotionDir == DIR__UP )
   {
      //if destination floor is closer than the 'reachable' floor reassign reachable floor to destination
      uint8_t ucReachableLanding = pstCarData->ucReachableLanding;
      if( pstCarData->ucDestinationLanding < pstCarData->ucReachableLanding )
      {
         ucReachableLanding = pstCarData->ucDestinationLanding;
      }

      /* Don't allow checking of destinations above after already preparing
       * for a destination change. Otherwise car may try to move to a
       * destination opposite its indicated arrival direction */
      if( !( ( pstCarData->enPriority == HC_DIR__DOWN ) && ( pstCarData->ucCurrentLanding == pstCarData->ucDestinationLanding ) )
       && !bDownLamp )
      {
         VIP_GetNextDestinationAbove_ByCar( eCarID, ucReachableLanding, &stDestination);
      }

      if( !bUpLamp )
      {
         if ( ( stDestination.ucLanding == INVALID_LANDING ) || ( stDestination.eCallDir == DIR__NONE ) ) {
            VIP_GetNextDestinationBelow_ByCar( eCarID, ucReachableLanding, &stDestination);
         }
      }
   }
   // If moving DOWN
   else if( pstCarData->cMotionDir == DIR__DN )
   {
      //if destination floor is closer than the 'reachable' floor reassign reachable floor to destination
      uint8_t ucReachableLanding = pstCarData->ucReachableLanding;
      if( pstCarData->ucDestinationLanding > pstCarData->ucReachableLanding )
      {
         ucReachableLanding = pstCarData->ucDestinationLanding;
      }

      /* Don't allow checking of destinations below after already preparing
       * for a destination change. Otherwise car may try to move to a
       * destination opposite its indicated arrival direction */
      if( !( ( pstCarData->enPriority == HC_DIR__UP ) && ( pstCarData->ucCurrentLanding == pstCarData->ucDestinationLanding ) )
       && !bUpLamp )
      {
         VIP_GetNextDestinationBelow_ByCar( eCarID, ucReachableLanding, &stDestination);
      }

      if( !bDownLamp )
      {
         if ( ( stDestination.ucLanding == INVALID_LANDING ) || ( stDestination.eCallDir == DIR__NONE ) ) {
            VIP_GetNextDestinationAbove_ByCar( eCarID, ucReachableLanding, &stDestination);
         }
      }
   }
   // If stopped, and serving UP calls
   else if( pstCarData->enPriority == HC_DIR__UP )
   {
      /* Suppress above destination check if arrival down lamps already set. */
      if( !bDownLamp )
      {
         VIP_GetNextDestinationAbove_ByCar( eCarID, pstCarData->ucCurrentLanding, &stDestination);
      }

      if( !bUpLamp && pstCarData->bIdleDirection )
      {
         if ( ( stDestination.ucLanding == INVALID_LANDING ) || ( stDestination.eCallDir == DIR__NONE ) ) {
            VIP_GetNextDestinationBelow_ByCar( eCarID, pstCarData->ucCurrentLanding, &stDestination);
         }
      }
   }
   // If stopped, and serving DOWN calls
   else
   {
      /* Suppress below destination check if arrival up lamps already set. */
      if( !bUpLamp )
      {
         VIP_GetNextDestinationBelow_ByCar( eCarID, pstCarData->ucCurrentLanding, &stDestination);
      }

      if( !bDownLamp && pstCarData->bIdleDirection )
      {
         if ( ( stDestination.ucLanding == INVALID_LANDING ) || ( stDestination.eCallDir == DIR__NONE ) ) {
            VIP_GetNextDestinationAbove_ByCar( eCarID, pstCarData->ucCurrentLanding, &stDestination);
         }
      }
   }

   /* If a valid destination was found and a next destination direction was found,
    * set the destination mask to reserve hall calls in that destination. */
   if( stDestination.eCallDir != DIR__NONE )
   {
      /* Set mask based on destination door */
      if( stDestination.eDoor == DOOR_FRONT )
      {
         stDestination.uiMask = pstCarData->uiVIPMask_F;
      }
      else if( stDestination.eDoor == DOOR_REAR )
      {
         stDestination.uiMask = pstCarData->uiVIPMask_R;
      }
      /* Here DOOR_ANY indicates both doors will open */
      else if( stDestination.eDoor == DOOR_ANY )
      {
         stDestination.uiMask = pstCarData->uiVIPMask_F | pstCarData->uiVIPMask_R;
      }
   }

   /* Load proposed destination to data structure for comparison */
   memcpy(&VIP_astProposedDestination[eCarID], &stDestination, sizeof(st_carDestination));
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void VIP_ClrProposedDestination_ByCar(enum en_group_net_nodes eCarID)
{
   memcpy(&VIP_astProposedDestination[eCarID], &stVIP_ClearedCarDestination, sizeof(st_carDestination));
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void VIP_ClrCarDestination_ByCar(enum en_group_net_nodes eCarID)
{
      aucVIPLandings_Plus1[eCarID].eCallDir = stVIP_ClearedCarDestination.eCallDir;
      aucVIPLandings_Plus1[eCarID].eDoor = stVIP_ClearedCarDestination.eDoor;
      aucVIPLandings_Plus1[eCarID].ucLanding = stVIP_ClearedCarDestination.ucLanding;
      aucVIPLandings_Plus1[eCarID].uiMask = stVIP_ClearedCarDestination.uiMask;
      aucVIPLandings_Plus1[eCarID].bDirty = 1;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void VIP_UpdateGroupCarDestinations()
{
   /* Clear current destination assignments for each car */
   for( uint8_t i = 0; i < MAX_GROUP_CARS; i++ )
   {
      st_cardata *pstCarData = GetCarDataStructure( i );
      VIP_ClrProposedDestination_ByCar(i);

      /* Also update local debounced fault counters. if the car has been faulted for a minimum period, clear its VIP assignment. */
      if( pstCarData->bFaulted )
      {
         if( aucFaultDebounceCounter__300ms[i] < FAULT_DEBOUNCE_COUNTER_LIMIT__300MS )
         {
            aucFaultDebounceCounter__300ms[i]++;
         }
      }
      else
      {
         aucFaultDebounceCounter__300ms[i] = 0;
      }

      /* ks note: say in the last module cycle, you assigned the nearest car to go on VIP. It is capturing and is clearing out existing car calls. While this is happening the two potential VIP cars could move in a way that now causes the other car to be assessed as the car that should take that VIP call.
       * If the locked in car destination is cleared every cycle, this could cause the cars to bounce in and out of VIP before they've been fully captured. I'd recommend instead not clearing out locked destinations every cycle.
       * However, if you do not clear out the locked in destination every cycle, youll still want to do housekeeping every cycle.
       * For each locked in destination, i recommend checking that the car is still eligible to take the call. (is it still online, is its VIP mask still set, has it been X amount of time since the assignment where X is an adjustable timeout, that the hall call is still latched) */

      if( !GetCarOnlineFlag_ByCar(i) || !pstCarData->bVIP || ( aucVIPLandings_Plus1[i].ucLanding == INVALID_LANDING ) )
      {
         VIP_ClrCarDestination_ByCar(i);
      }
      // ks note: also do housekeeping to see if the destination assignments in aucVIPLandings_Plus1 are for a valid floor. Meaning for all the aucVIPLandings_Plus1 that are for a floor, check the status of the raw hall calls to determine if the call has already been cleared via VIP_GetLatchedHallCallDown_Front_ByCar.
      else if( !VIP_GetLatchedHallCallUp_Front_ByCar( i, aucVIPLandings_Plus1[i].ucLanding )
            && !VIP_GetLatchedHallCallUp_Rear_ByCar( i, aucVIPLandings_Plus1[i].ucLanding )
            && !VIP_GetLatchedHallCallDown_Front_ByCar( i, aucVIPLandings_Plus1[i].ucLanding )
            && !VIP_GetLatchedHallCallDown_Rear_ByCar( i, aucVIPLandings_Plus1[i].ucLanding ) )
      {
         VIP_ClrCarDestination_ByCar(i);
      }
      else if( aucFaultDebounceCounter__300ms[i] >= FAULT_DEBOUNCE_COUNTER_LIMIT__300MS )
      {
         VIP_ClrCarDestination_ByCar(i);
      }
   }
   /* Generate a proposed destinations for each car
    * and compare ones that conflict. The closer one is locked in and the other is cleared. */

   VIP_GenerateAndValidateProposedDestinations();

   for( uint8_t i = 0; i < MAX_GROUP_CARS; i++ )
   {
      if( ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
       && ( VIP_astProposedDestination[i].ucLanding >= MAX_NUM_FLOORS )
       && ( aucVIPLandings_Plus1[i].ucLanding == INVALID_LANDING ) )
      {
         VIP_UpdateDestination_ByCar(i);
      }
   }
}
