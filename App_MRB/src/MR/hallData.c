/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief
 * @version  V1.00
 * @date     27, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>

#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "carData.h"
#include "carDestination.h"
#include "group_net_data.h"
#include "hallSecurity.h"
#include "hallData.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
static st_hallcall_data stHallCallData;
/* The below is used instead of st_hallboard_status after unexplained variable clearing between unloads */
static en_hallboard_error eError;
static en_hb_com_state eCommState;
static uint8_t ucBF_RawIO; // see en_hb_io_shifts
static uint8_t ucSourceNode; // Source riser board index plus 1. For tracking duplicate HB DIPs from diff risers

static uint16_t uwLastUnloadedAddress_Plus1;
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t GetHallBoardStatus_IO(void)
{
   uint16_t uwRequest_Plus1 = GetUIRequest_HallStatusIndex_Plus1();
   if( uwRequest_Plus1 == uwLastUnloadedAddress_Plus1 )
   {
      return ucBF_RawIO;
   }
   else
   {
      return 0;
   }
}
en_hallboard_error GetHallBoardStatus_Error(void)
{
   uint16_t uwRequest_Plus1 = GetUIRequest_HallStatusIndex_Plus1();
   if( uwRequest_Plus1 == uwLastUnloadedAddress_Plus1 )
   {
      return eError;
   }
   else
   {
      return 0;
   }
}
en_hb_com_state GetHallBoardStatus_Comm(void)
{
   uint16_t uwRequest_Plus1 = GetUIRequest_HallStatusIndex_Plus1();
   if( uwRequest_Plus1 == uwLastUnloadedAddress_Plus1 )
   {
      return eCommState;
   }
   else
   {
      return 0;
   }
}
uint8_t GetHallBoardStatus_Source(void)
{
   uint16_t uwRequest_Plus1 = GetUIRequest_HallStatusIndex_Plus1();
   if( uwRequest_Plus1 == uwLastUnloadedAddress_Plus1 )
   {
      return ucSourceNode;
   }
   else
   {
      return 0;
   }
}

/*-----------------------------------------------------------------------------
   Returns 1 if a call is latched for this local car (For UI only. Does not mask out calls already assigned )
 -----------------------------------------------------------------------------*/
uint8_t GetLatchedHallCallUp_ByLanding( uint8_t ucLanding, enum en_doors eDoor )
{
   uint8_t bReturn = 0;
   uint32_t uiRawHallCalls = GetRawLatchedHallCalls(ucLanding, HC_DIR__UP);
   st_cardata * pstCarData = GetCarDataStructure(GetFP_GroupCarIndex());
   uint32_t uiMask = 0;
   if( eDoor == DOOR_FRONT )
   {
      uiMask = uiRawHallCalls & pstCarData->uiHallMask_F;
   }
   else//Rear
   {
      uiMask = uiRawHallCalls & pstCarData->uiHallMask_R;
   }

   if( uiMask )
   {
      bReturn = 1;
   }

   if(!GetHallCallSecurity(GetFP_GroupCarIndex(), ucLanding, eDoor, uiMask))
   {
      bReturn = 0;
   }
   else if( uiMask )
   {
      bReturn = 1;
   }
   return bReturn;
}
uint8_t GetLatchedHallCallDown_ByLanding( uint8_t ucLanding, enum en_doors eDoor )
{
   uint8_t bReturn = 0;
   uint32_t uiRawHallCalls = GetRawLatchedHallCalls(ucLanding, HC_DIR__DOWN);
   st_cardata * pstCarData = GetCarDataStructure(GetFP_GroupCarIndex());
   uint32_t uiMask = 0;
   if( eDoor == DOOR_FRONT )
   {
      uiMask = uiRawHallCalls & pstCarData->uiHallMask_F;
   }
   else//Rear
   {
      uiMask = uiRawHallCalls & pstCarData->uiHallMask_R;
   }

   if(!GetHallCallSecurity(GetFP_GroupCarIndex(), ucLanding, eDoor, uiMask))
   {
      bReturn = 0;
   }
   else if( uiMask )
   {
      bReturn = 1;
   }
   return bReturn;
}

/*-----------------------------------------------------------------------------
   Returns the raw latched hall calls reported by RIS boards
 -----------------------------------------------------------------------------*/
uint32_t GetRawLatchedHallCalls( uint8_t ucLanding, en_hc_dir eDir )
{
   uint32_t uiLatchedCalls = 0;
   if( eDir == HC_DIR__UP )
   {
      for(uint8_t i = 0; i < MAX_NUM_RISER_BOARDS; i++)
      {
         uiLatchedCalls |= stHallCallData.aauiRawLatched_Up[i][ucLanding];
      }
   }
   else
   {
      for(uint8_t i = 0; i < MAX_NUM_RISER_BOARDS; i++)
      {
         uiLatchedCalls |= stHallCallData.aauiRawLatched_Down[i][ucLanding];
      }
   }
   return uiLatchedCalls;
}
/*-----------------------------------------------------------------------------
   uiClrMask = Hall mask to be cleared
 -----------------------------------------------------------------------------*/
void ClrRawLatchedHallCalls( uint8_t ucLanding, en_hc_dir eDir, uint32_t uiClrMask )
{
   if( eDir == HC_DIR__UP )
   {
      for(uint8_t i = 0; i < MAX_NUM_RISER_BOARDS; i++)
      {
         stHallCallData.aauiRawLatched_Up[i][ucLanding] &= ~(uiClrMask);
      }
   }
   else
   {
      for(uint8_t i = 0; i < MAX_NUM_RISER_BOARDS; i++)
      {
         stHallCallData.aauiRawLatched_Down[i][ucLanding] &= ~(uiClrMask);
      }
   }
}
/*-----------------------------------------------------------------------------
   uiSetMask = Hall mask to be set
 -----------------------------------------------------------------------------*/
void SetRawLatchedHallCalls( uint8_t ucLanding, en_hc_dir eDir, uint32_t uiSetMask )
{
   if( eDir == HC_DIR__UP )
   {
      for(uint8_t i = 0; i < MAX_NUM_RISER_BOARDS; i++)
      {
         stHallCallData.aauiRawLatched_Up[i][ucLanding] |= uiSetMask;
      }
   }
   else
   {
      for(uint8_t i = 0; i < MAX_NUM_RISER_BOARDS; i++)
      {
         stHallCallData.aauiRawLatched_Down[i][ucLanding] |= uiSetMask;
      }
   }
}
/*-----------------------------------------------------------------------------
   Datagram functions
 -----------------------------------------------------------------------------*/
void UnloadDatagram_RiserCarRequest( uint8_t ucRiserIndex, un_sdata_datagram *punDatagram )
{
   en_car_request eRequest = GetCarRequest_ByCar(GetFP_GroupCarIndex());
   en_car_request eResponse = punDatagram->aui32[0];
   if( eRequest == eResponse )
   {
      uint32_t uiData = punDatagram->aui32[1];
      uint8_t ucRequestedRiserIndex = 0xFF;
      switch(eRequest)
      {
         case CAR_REQUEST__RIS1_CAN1 ... CAR_REQUEST__RIS4_CAN1:
            ucRequestedRiserIndex = eRequest - CAR_REQUEST__RIS1_CAN1;
            break;
         case CAR_REQUEST__RIS1_CAN2 ... CAR_REQUEST__RIS4_CAN2:
            ucRequestedRiserIndex = eRequest - CAR_REQUEST__RIS1_CAN2;
            break;
         default: break;

      }
      if( ucRequestedRiserIndex == ucRiserIndex )
      {
         SetResponseToGroupCarRequest(uiData);
      }
   }
}
void UnloadDatagram_RiserLatchedHallCalls( uint8_t ucRiserIndex, en_hc_dir eDir, un_sdata_datagram *punDatagram )
{
   uint8_t ucLanding = punDatagram->auc8[0];
   if( ucLanding < MAX_NUM_FLOORS )
   {
      if( eDir == HC_DIR__UP )
      {
         stHallCallData.aauiRawLatched_Up[ucRiserIndex][ucLanding] = punDatagram->aui32[1];
      }
      else
      {
         stHallCallData.aauiRawLatched_Down[ucRiserIndex][ucLanding] = punDatagram->aui32[1];
      }
   }
}

void UnloadDatagram_RiserHallboardStatus( uint8_t ucRiserIndex, un_sdata_datagram *punDatagram )
{
   uint16_t uwHallboardAddress = punDatagram->auw16[0];
   if( uwHallboardAddress < GetHallBoard_MaxNumberOfBoards() )
   {
      uint16_t uwAddress_Plus1 = uwHallboardAddress+1;
      uint16_t uwRequest_Plus1 = GetUIRequest_HallStatusIndex_Plus1();
      uint16_t uwSecurityRequest_Plus1 = GetUIRequest_HallStatusIndex_Plus1()-HALL_UI_REQ__HALL_SECURITY__START+1;
      if( ( ucRiserIndex != SECURITY_RISER_INDEX )
       && ( uwAddress_Plus1 == uwRequest_Plus1 ) )
      {
         /* Suppress hall boards inactive update to prevent overwriting
          * of valid HB status by riser boards where the hall board is inactive. */
         if( punDatagram->auc8[2] != HB_COM_STATE__INACTIVE )
         {
            uwLastUnloadedAddress_Plus1 = uwAddress_Plus1;
            eCommState = punDatagram->auc8[2];
            ucBF_RawIO = punDatagram->auc8[3];
            eError = punDatagram->auc8[4];
            ucSourceNode = ucRiserIndex+1;
         }
      }
      else if( ( ucRiserIndex == SECURITY_RISER_INDEX )
            && ( uwAddress_Plus1 == uwSecurityRequest_Plus1 ) )
      {
         uwLastUnloadedAddress_Plus1 = uwRequest_Plus1;
         eCommState = punDatagram->auc8[2];
         ucBF_RawIO = punDatagram->auc8[3];
         eError = punDatagram->auc8[4];
         ucSourceNode = ucRiserIndex+1;
      }
   }
}

static void GetNearestHallCalls( uint8_t* ucNearestHallCall_Plus1_Above, uint8_t* ucNearestHallCall_Plus1_Below )
{
   uint8_t ucCurrentLanding = GetGroupMapping(GetOperation_CurrentFloor());
   uint8_t ucTopLanding = GetGroupMapping(GetFP_NumFloors()-1);
   uint8_t ucBotLanding = GetGroupMapping(0);
   uint8_t ucHallCallLanding_UP = INVALID_LANDING;
   uint8_t ucHallCallLanding_DN = INVALID_LANDING;

   if( ( GetLatchedHallCallUp_ByLanding(ucCurrentLanding, DOOR_FRONT) ) || ( GetLatchedHallCallUp_ByLanding(ucCurrentLanding, DOOR_REAR) ) )
   {
      if( ucCurrentLanding != ucTopLanding )
      {
         ucHallCallLanding_UP = ucCurrentLanding;
      }
   }
   for( int8_t i = ucCurrentLanding+1; i <= ucTopLanding; i++ )
   {
      if( ucHallCallLanding_UP != INVALID_LANDING )
      {
         break;
      }
      else if( ( i != ucTopLanding ) && ( ( GetLatchedHallCallUp_ByLanding(i, DOOR_FRONT) ) || ( GetLatchedHallCallUp_ByLanding(i, DOOR_REAR) ) ) )
      {
         ucHallCallLanding_UP = i;
      }
      else if( ( i != ucBotLanding ) && ( ( GetLatchedHallCallDown_ByLanding(i, DOOR_FRONT) ) || ( GetLatchedHallCallDown_ByLanding(i, DOOR_REAR) ) ) )
      {
         ucHallCallLanding_UP = i;
      }
   }

   if( ( GetLatchedHallCallDown_ByLanding(ucCurrentLanding, DOOR_FRONT) ) || ( GetLatchedHallCallDown_ByLanding(ucCurrentLanding, DOOR_REAR) ) )
   {
      if( ucCurrentLanding != ucBotLanding )
      {
         ucHallCallLanding_DN = ucCurrentLanding;
      }
   }
   for( int8_t i = ucCurrentLanding-1; i >= 0; i-- )
   {
      if( ucHallCallLanding_DN != INVALID_LANDING )
      {
         break;
      }
      else if( ( i != ucTopLanding ) && ( ( GetLatchedHallCallUp_ByLanding(i, DOOR_FRONT) ) || ( GetLatchedHallCallUp_ByLanding(i, DOOR_REAR) ) ) )
      {
         ucHallCallLanding_DN = i;
      }
      else if( ( i != ucBotLanding ) && ( ( GetLatchedHallCallDown_ByLanding(i, DOOR_FRONT) ) || ( GetLatchedHallCallDown_ByLanding(i, DOOR_REAR) ) ) )
      {
         ucHallCallLanding_DN = i;
      }
   }
   uint8_t ucHallCallFloor_UP = GetCarMapping(ucHallCallLanding_UP);
   uint8_t ucHallCallFloor_DN = GetCarMapping(ucHallCallLanding_DN);
   *ucNearestHallCall_Plus1_Above = ( ucHallCallFloor_UP < GetFP_NumFloors() ) ? ucHallCallFloor_UP+1:0;
   *ucNearestHallCall_Plus1_Below = ( ucHallCallFloor_DN < GetFP_NumFloors() ) ? ucHallCallFloor_DN+1:0;
}
void LoadDatagram_HallCalls( un_sdata_datagram *punDatagram )
{
   st_carDestination *pstCarDestination = GetCarDestinationStructure(GetFP_GroupCarIndex());
   uint8_t ucDestinationLanding = pstCarDestination->ucLanding;
   uint8_t ucDestinationFloor = GetCarMapping(ucDestinationLanding);
   enum direction_enum eDir = pstCarDestination->eCallDir;

   memset(punDatagram, 0, sizeof(un_sdata_datagram));

   if ( ucDestinationFloor < GetFP_NumFloors() )
   {
      if( eDir == DIR__UP )
      {
         uint8_t ucFrontFloor_Plus1 = 0;
         uint8_t ucRearFloor_Plus1 = 0;
         if(GetLatchedHallCallUp_ByLanding( ucDestinationLanding, DOOR_FRONT ))
         {
            ucFrontFloor_Plus1 = ucDestinationFloor+1;
         }
         if(GetLatchedHallCallUp_ByLanding( ucDestinationLanding, DOOR_REAR ))
         {
            ucRearFloor_Plus1 = ucDestinationFloor+1;
         }

         punDatagram->auc8[0] = ucFrontFloor_Plus1;
         punDatagram->auc8[1] = ucRearFloor_Plus1;
      }
      else if( eDir == DIR__DN )
      {
         uint8_t ucFrontFloor_Plus1 = 0;
         uint8_t ucRearFloor_Plus1 = 0;
         if(GetLatchedHallCallDown_ByLanding( ucDestinationLanding, DOOR_FRONT ))
         {
            ucFrontFloor_Plus1 = ucDestinationFloor+1;
         }
         if(GetLatchedHallCallDown_ByLanding( ucDestinationLanding, DOOR_REAR ))
         {
            ucRearFloor_Plus1 = ucDestinationFloor+1;
         }

         punDatagram->auc8[2] = ucFrontFloor_Plus1;
         punDatagram->auc8[3] = ucRearFloor_Plus1;
      }
   }

   /* Add attendant HC indicators */
   if( GetOperation_AutoMode() == MODE_A__ATTENDANT )
   {
      GetNearestHallCalls(&punDatagram->auc8[4], &punDatagram->auc8[5]);
   }
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
