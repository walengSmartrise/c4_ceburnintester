/******************************************************************************
 *
 * @file     mod_param_mrb.c
 * @brief    
 * @version  V1.00
 * @date     13, April 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "sys.h"
#include "ui.h"
#include "motion.h"
#include "param.h"
#include "param_edit_protocol.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_ParamMRB =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

enum en_mod_param_states
{
   enPSTATE__AWAITING_VALID_RAM_COPY,
   enPSTATE__RUNNING,

   NUM_PSTATES
};

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
 Returns 1 if parameter updates are allowed
 -----------------------------------------------------------------------------*/
static uint8_t CheckIf_ValidParameterUpdateState()
{
   uint8_t bValid = 0;
//   if( !GetMotion_RunFlag()
//    && ( ( GetOperation_AutoMode() == MODE_A__TEST )
//      || ( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
//      || ( GetOperation_ClassOfOp() == CLASSOP__SEMI_AUTO )
//      || ( GetOperation3_CaptureMode_MR() == CAPTURE_IDLE ) ) )
   if( !GetMotion_RunFlag()
    && !SRU_Read_DIP_Switch( enSRU_DIP_A1 ) )
   {
      bValid = 1;
   }
   return bValid;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckForFault_ParamSync( struct st_param_control *pstParamControl )
{
   static uint32_t uiDebounceCounter;
   if( !GetMotion_RunFlag() )
   {
      if( pstParamControl->ucOutOfDateBlock_IndexPlus1 )
      {
         if(uiDebounceCounter >= PARAM_SYNC_FAULT_DEBOUNCE_LIMIT)
         {
            if( !GetOperation4_ParamSync() )
            {
               SetFault(FLT__PARAM_SYNC_MRB);             
            }

         }
         else
         {
            uiDebounceCounter++;
         }
      }
      else
      {
         uiDebounceCounter = 0;
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadDatagram_DG_AB_MRA__ParamMaster_BlockCRCs( struct st_param_control *pstParamControl )
{
   static uint8_t aucLastBlockIndex_Plus1[NUM_PARAM_BLOCKS];
   static uint32_t auiLastCRC[NUM_PARAM_BLOCKS];

   for( uint8_t i = 0; i < NUM_PARAM_BLOCKS; i++ )
   {
      Datagram_ID eID = GetNetworkDatagramID_MRA( DG_MRA__ParamMaster_BlockCRCs1 + i );
      if( GetDatagramDirtyBit( eID ) )
      {
         ClrDatagramDirtyBit( eID );
         uint8_t ucMessageType = GetParamMasterCRCs_MessageType_MRA( i );
         uint8_t ucBlockIndex_Plus1 = GetParamMasterCRCs_BlockIndex_MRA( i );
         uint32_t uiCRC = GetParamMasterCRCs_CRC_MRA( i );
         if( ( ucMessageType == enPARAM_DG__SENDING_BLOCK_CRC )  // sending a block CRC?
          && ( ucBlockIndex_Plus1 )// valid block?
          && ( aucLastBlockIndex_Plus1[ i ] == ucBlockIndex_Plus1 )
          && ( auiLastCRC[ i ] == uiCRC ))
         {
            Param_PutBlockCRC_Master( pstParamControl, ucBlockIndex_Plus1 - 1, uiCRC );
         }
         aucLastBlockIndex_Plus1[ i ] = ucBlockIndex_Plus1;
         auiLastCRC[ i ] = uiCRC;
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

static void UnloadDatagram_DG_AB_MRA__ParamMaster_ParamValues( struct st_param_control *pstParamControl )
{
   static uint8_t ucLastBlock_Plus1;
   static uint8_t ucLastChunk_Plus1;
   static uint32_t uiLastData;

   if( GetDatagramDirtyBit(DATAGRAM_ID_181) )
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_181);
      enum en_param_datagram_message_types enParamDatagramType = GetParamMasterValues2_MessageType_MRA();
      uint8_t ucBlock_Plus1 = GetParamMasterValues2_BlockIndex_MRA();
      uint8_t ucChunk_Plus1 = GetParamMasterValues2_ChunkIndex_MRA();
      uint32_t ulData = GetParamMasterValues2_DataChunk_MRA();
      if( ( enParamDatagramType == enPARAM_DG__SENDING_PARAM_CHUNK )
       && ( ucBlock_Plus1 )
       && ( ucChunk_Plus1 )
       && ( ucBlock_Plus1 == ucLastBlock_Plus1 )
       && ( ucChunk_Plus1 == ucLastChunk_Plus1 )
       && ( ulData == uiLastData ) )
      {
         pstParamControl->stSlaveReceivedChunk.ucBlock_Plus1 = ucBlock_Plus1;
         pstParamControl->stSlaveReceivedChunk.ucChunk_Plus1 = ucChunk_Plus1;
         Param_WriteChunk(ucBlock_Plus1 - 1, ucChunk_Plus1 - 1, ulData);
      }
      ucLastBlock_Plus1 = ucBlock_Plus1;
      ucLastChunk_Plus1 = ucChunk_Plus1;
      uiLastData = ulData;
   }
}
/*-----------------------------------------------------------------------------
   Checks for out of date block and confirms requsted chunk has been
   received before requesting the next
 -----------------------------------------------------------------------------*/
static uint8_t CheckIfParamChunkToRequest( struct st_param_control *pstParamControl )
{
   uint8_t bBlockOutOfDate = 0;
   uint8_t ucParamNodeID = gstParamControl.ucParamNodeType;
   if(pstParamControl->ucOutOfDateBlock_IndexPlus1)
   {
      bBlockOutOfDate = 1;
      struct st_param_chunk_req *pstRequestedChunk =
            &pstParamControl->astSlaveRequestedChunk[ucParamNodeID];
      uint8_t ucChunkIndex = 0;

      if( pstRequestedChunk->ucChunk_Plus1 )
      {
         ucChunkIndex = pstRequestedChunk->ucChunk_Plus1 - 1;
      }

      if( ( pstParamControl->stSlaveReceivedChunk.ucBlock_Plus1 == pstParamControl->ucOutOfDateBlock_IndexPlus1 )
       && ( pstParamControl->stSlaveReceivedChunk.ucChunk_Plus1 == pstRequestedChunk->ucChunk_Plus1 ) )
      {
         ucChunkIndex = (ucChunkIndex + 1) % NUM_OF_CHUNKS_PER_BLOCK;
      }
      else
      {
         //TODO add timeout
      }

      if( ucChunkIndex >= NUM_OF_CHUNKS_PER_BLOCK )
      {
         ucChunkIndex = NUM_OF_CHUNKS_PER_BLOCK - 1;
      }

      pstParamControl->ucRequestedChunk = ucChunkIndex;
      pstRequestedChunk->ucBlock_Plus1 = pstParamControl->ucOutOfDateBlock_IndexPlus1;
      pstRequestedChunk->ucChunk_Plus1 = ucChunkIndex + 1;
   }
   return bBlockOutOfDate;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadDatagram_DG_AB_MRB__ParamSlaveRequest( struct st_param_control *pstParamControl )
{
   struct st_sdata_control * pstSDataControl = &gstSData_LocalData;
   uint32_t uiDatagramID = DG_MRB__ParamSlaveRequest;

   uint8_t ucParamNodeID = gstParamControl.ucParamNodeType;
   if ( SDATA_DirtyBit_Get( pstSDataControl, uiDatagramID ) ==0)
   {
      un_sdata_datagram unDatagram;
      struct st_param_and_value *pstParameterToSave = &pstParamControl->astParmEditRequest[ucParamNodeID];

      if( CheckIfParamChunkToRequest(pstParamControl) )
      {
         unDatagram.auc8[0] = enPARAM_DG__REQ_FOR_PARAM_CHUNK;
         unDatagram.auc8[1] = pstParamControl->ucOutOfDateBlock_IndexPlus1;
         unDatagram.auc8[2] = pstParamControl->ucRequestedChunk + 1;
         unDatagram.auc8[3] = ucParamNodeID;
         unDatagram.aui32[1] = 0;
         SDATA_WriteDatagram( pstSDataControl,
                              uiDatagramID,
                              &unDatagram
                            );
         SDATA_DirtyBit_Set( pstSDataControl,
                             uiDatagramID );
      }
      else if ( pstParameterToSave->ucCountdown )
      {
         --pstParameterToSave->ucCountdown;

         unDatagram.auc8[ 0 ] = enPARAM_DG__REQ_TO_SET_A_PARAM;  // request to set a parameter value
         unDatagram.auc8[ 1 ] = pstParameterToSave->ucBlockIndex + 1;
         unDatagram.auw16[ 1 ] = pstParameterToSave->uwParamIndex + 1;
         unDatagram.aui32[ 1 ] = pstParameterToSave->uiValue;
         SDATA_WriteDatagram( pstSDataControl,
                              uiDatagramID,
                              &unDatagram
                            );
         SDATA_DirtyBit_Set( pstSDataControl,
                             uiDatagramID );
      }
      else
      {
         unDatagram.aui32[0] = 0;
         unDatagram.aui32[1] = 0;
         SDATA_WriteDatagram( pstSDataControl,
                              uiDatagramID,
                              &unDatagram
                            );
      }
   }
}
/*-----------------------------------------------------------------------------

   Clear send requests for parameter datagrams

 -----------------------------------------------------------------------------*/
static void ClearParamUpdateRequests()
{
   SDATA_DirtyBit_Clr( &gstSData_LocalData,
                        DG_MRB__ParamSlaveRequest);
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   gstParamControl.ucParamNodeType = enPARAM_NODE__MRB;
   gstParamControl.pstParamBlockInfo = &gstSys_ParamBlockInfo;

   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = 20;

   return 0;
}

/*-----------------------------------------------------------------------------

   The EEPROMI can take up to 3 ms to write a page so don't call Run() more
   often than that and never do more than one write operation per call.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static uint8_t ucState = enPSTATE__AWAITING_VALID_RAM_COPY;

   struct st_param_control *pstParamControl = &gstParamControl;

   switch( ucState )
   {
      case enPSTATE__RUNNING:
         if( !CheckIf_ValidParameterUpdateState() )
         {
            ClearParamUpdateRequests();
         }
         else
         {
            UnloadDatagram_DG_AB_MRA__ParamMaster_BlockCRCs( pstParamControl );
            UnloadDatagram_DG_AB_MRA__ParamMaster_ParamValues( pstParamControl );

            Param_LookForOutOfDateBlock( pstParamControl );

            LoadDatagram_DG_AB_MRB__ParamSlaveRequest( pstParamControl );
         }
         PEP_UpdateCRCDirtyBits(pstThisModule->uwRunPeriod_1ms);
         CheckForFault_ParamSync(pstParamControl);
         break;

      default:  // enPSTATE__AWAITING_VALID_RAM_COPY
         if ( Param_ParametersAreValid() )
         {
             pstParamControl->pauiMaster_CRCs = calloc( pstParamControl->pstParamBlockInfo->ucNumBlocks, sizeof( uint32_t ) );
             ucState = enPSTATE__RUNNING;
         }
         break;
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

