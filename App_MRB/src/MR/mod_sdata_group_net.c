/***
 *       _____                          __         _
 *      / ___/ ____ ___   ____ _ _____ / /_ _____ (_)_____ ___
 *      \__ \ / __ `__ \ / __ `// ___// __// ___// // ___// _ \
 *     ___/ // / / / / // /_/ // /   / /_ / /   / /(__  )/  __/
 *    /____//_/ /_/ /_/ \__,_//_/    \__//_/   /_//____/ \___/
 *        ______               _                           _
 *       / ____/____   ____ _ (_)____   ___   ___   _____ (_)____   ____ _
 *      / __/  / __ \ / __ `// // __ \ / _ \ / _ \ / ___// // __ \ / __ `/
 *     / /___ / / / // /_/ // // / / //  __//  __// /   / // / / // /_/ /
 *    /_____//_/ /_/ \__, //_//_/ /_/ \___/ \___//_/   /_//_/ /_/ \__, /
 *                  /____/                                       /____/
 */
/******************************************************************************
 *
 * @file
 * @brief    COntrols sending data for the group net
 * @version
 * @date
 *
 * @note
 *
 ******************************************************************************/




/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sru_b.h"
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "shared_data.h"
#include "halldata.h"

#include "group_net_data.h"
#include "mod_run_log.h"
#include "carData.h"
#include "carDestination.h"
#include "groupnet.h"
#include "groupnet_datagrams.h"
#include "emergency_power.h"
#include "masterCommand.h"
#include "param_edit_protocol.h"
#include "XRegData.h"
#include "XRegDestination.h"
#include "hallSecurity.h"
#include "remoteCommands.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define FAULT_ACTIVE_MIN_RESEND_RATE__MS        ( 1500 )
#define FAULT_INACTIVE_MIN_RESEND_RATE__MS      ( 15000 )
#define ALARM_ACTIVE_MIN_RESEND_RATE__MS        ( 1500 )
#define ALARM_INACTIVE_MIN_RESEND_RATE__MS      ( 15000 )

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_SData_GroupNet =
{
   .pfnInit = Init,
   .pfnRun = Run,
};


/* Below are all the nodes in Group Network */
struct st_sdata_control gstSData_GroupNet_MRB;

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_sdata_local gstSData_Config;

static CAN_MSG_T gstCAN_MSG_Tx;

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

void SharedData_GroupNet_Init()
{
   int iError;
   //--------------------------------------------------------
   //by default i set this id to be rejected as out of range so that i can't get garbage on start up.
   gstSData_Config.ucLocalNodeID = NUM_GROUP_NET_NODES;
   gstSData_Config.ucNetworkID   = SDATA_NET__GROUP;
   gstSData_Config.ucNumNodes    = NUM_GROUP_NET_NODES;
   gstSData_Config.ucDestNodeID  = 0x1F; /* All */

   iError = SDATA_CreateDatagrams( &gstSData_GroupNet_MRB, NUM_GroupNet_MRB_DATAGRAMS);
   if ( iError )
   {
      while ( 1 )
      {
          //SDA REVIEW: issue a fault or have some other error handling
         ;  // TODO: unable to allocate shared data
      }
   }
   gstSData_GroupNet_MRB.bDisableResend = 1;
   gstSData_GroupNet_MRB.bDisableHeartbeat = 1;
   gstCAN_MSG_Tx.DLC = 8;

   Initialize_RiserData();
}


/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/

/*
 * Riser board commands
 */
uint8_t RecieveRiserCommand( uint8_t ucRiser, uint8_t ucDatagramID, un_sdata_datagram *unDatagram )
{
   uint8_t bFailed = 1;
   if( ucRiser < MAX_NUM_RISER_BOARDS )
   {
      switch (ucDatagramID) {
         case DG_GroupNet_RIS__CAR_REQUEST:
            UnloadDatagram_RiserCarRequest( ucRiser, unDatagram );
            bFailed = 0;
            break;
         case  DG_GroupNet_RIS__LATCHED_HALL_CALLS_UP:
            if( ucRiser == SECURITY_RISER_INDEX )
            {
               UnloadDatagram_RiserSecurityContacts_F(unDatagram);
            }
            else
            {
               UnloadDatagram_RiserLatchedHallCalls( ucRiser, HC_DIR__UP, unDatagram );
            }
            bFailed = 0;
            break;
         case  DG_GroupNet_RIS__LATCHED_HALL_CALLS_DN:
            if( ucRiser == SECURITY_RISER_INDEX )
            {
               UnloadDatagram_RiserSecurityContacts_R(unDatagram);
            }
            else
            {
               UnloadDatagram_RiserLatchedHallCalls( ucRiser, HC_DIR__DOWN, unDatagram );
            }
            bFailed = 0;
            break;
         case  DG_GroupNet_RIS__INPUT_STATE:
            UnloadDatagram_RiserStatus( ucRiser, unDatagram );
            ClrRiserOfflineCounter(ucRiser);
            bFailed = 0;
            break;
         case  DG_GroupNet_RIS__HALLBOARD_STATUS:
            UnloadDatagram_RiserHallboardStatus( ucRiser, unDatagram );
            bFailed = 0;
            break;
         default:
            break;
      }
   }
   return bFailed;
}
static uint8_t LoadDatagram_Faults( un_sdata_datagram *punDatagram )
{
   static st_fault_data stData;
   static en_faults aeLastFault[NUM_FAULT_NODES];
   static enum en_fault_nodes eNode = NUM_FAULT_NODES;
   static uint8_t ucDirtyBits;
   static uint8_t ucState;  // Part of node data to load
   static uint16_t auwResendCounter_1ms[NUM_FAULT_NODES];
   uint8_t bNewFaultToSend = 0;

   /* Mark nodes to update */
   for( uint8_t i = 0; i < NUM_FAULT_NODES; i++ )
   {
      if(aeLastFault[i] != gstFault.aeActiveFaultNum[i])
      {
         aeLastFault[i] = gstFault.aeActiveFaultNum[i];
         auwResendCounter_1ms[i] = 0;
         Sys_Bit_Set(&ucDirtyBits, i, 1);
      }
      else if( Sys_Bit_Get(&ucDirtyBits, i) )
      {
         auwResendCounter_1ms[i] = 0;
      }
      else if( Param_ReadValue_1Bit(enPARAM1__ENA_DAD_FAULT_RESEND) )
      {
         auwResendCounter_1ms[i] += gstMod_SData_GroupNet.uwRunPeriod_1ms;
         if( gstFault.aeActiveFaultNum[i] )
         {
            if( auwResendCounter_1ms[i] >= FAULT_ACTIVE_MIN_RESEND_RATE__MS )
            {
               auwResendCounter_1ms[i] = 0;
               Sys_Bit_Set(&ucDirtyBits, i, 1);
            }
         }
         else
         {
            if( auwResendCounter_1ms[i] >= FAULT_INACTIVE_MIN_RESEND_RATE__MS )
            {
               auwResendCounter_1ms[i] = 0;
               Sys_Bit_Set(&ucDirtyBits, i, 1);
            }
         }
      }
   }

   /* If no valid node selected, check for a fault to update */
   if( eNode == NUM_FAULT_NODES )
   {
      for(uint8_t ucSlot = 0; ucSlot < NUM_FAULT_NODES; ucSlot++)
      {
         if ( Sys_Bit_Get(&ucDirtyBits, ucSlot) )
         {
            Sys_Bit_Set(&ucDirtyBits, ucSlot, 0);
            GetBoardActiveFault(&stData, ucSlot );
            eNode = ucSlot;
            ucState = 0;
            break;
         }
      }
   }
   /* Otherwise load data for that node */
   else
   {
      memset(punDatagram, 0, sizeof(un_sdata_datagram));
      if(ucState == 0)
      {
         punDatagram->auc8[0] = eNode | (ucState << 4);
         punDatagram->auc8[1] = stData.eFaultNum >> 8;
         punDatagram->auw16[1] = stData.wSpeed;
         punDatagram->aui32[1] = (stData.ulPosition - GetPositionReference() + 65535);
         ucState = 1;
      }
      else
      {
         punDatagram->auc8[0] = eNode | (ucState << 4);
         punDatagram->auc8[1] = stData.eFaultNum & 0xFF;
         punDatagram->auw16[1] = stData.wSpeed;
         punDatagram->aui32[1] = stData.ulTimestamp;
         eNode = NUM_FAULT_NODES;
         ucState = 0;
      }
      bNewFaultToSend = 1;
   }

   return bNewFaultToSend;
}

static uint8_t LoadDatagram_Alarms( un_sdata_datagram *punDatagram )
{
   static st_alarm_data stData;
   static en_alarms aeLastAlarm[NUM_ALARM_NODES];
   static en_alarm_nodes eNode = NUM_ALARM_NODES;
   static uint8_t ucDirtyBits;
   static uint8_t ucState;  // Part of node data to load
   static uint16_t auwResendCounter_1ms[NUM_ALARM_NODES];
   uint8_t bNewAlarmToSend = 0;

   /* Mark nodes to update */
   for( uint8_t i = 0; i < NUM_ALARM_NODES; i++ )
   {
      if(aeLastAlarm[i] != gstAlarm.aeActiveAlarmNum[i])
      {
         aeLastAlarm[i] = gstAlarm.aeActiveAlarmNum[i];
         auwResendCounter_1ms[i] = 0;
         Sys_Bit_Set(&ucDirtyBits, i, 1);
      }
      else if( Sys_Bit_Get(&ucDirtyBits, i) )
      {
         auwResendCounter_1ms[i] = 0;
      }
      else if( Param_ReadValue_1Bit(enPARAM1__ENA_DAD_FAULT_RESEND) )
      {
         if( gstAlarm.aeActiveAlarmNum[i] )
         {
            auwResendCounter_1ms[i] += gstMod_SData_GroupNet.uwRunPeriod_1ms;
            if( auwResendCounter_1ms[i] >= ALARM_ACTIVE_MIN_RESEND_RATE__MS )
            {
               auwResendCounter_1ms[i] = 0;
               Sys_Bit_Set(&ucDirtyBits, i, 1);
            }
         }
         else
         {
            if( auwResendCounter_1ms[i] >= ALARM_INACTIVE_MIN_RESEND_RATE__MS )
            {
               auwResendCounter_1ms[i] = 0;
               Sys_Bit_Set(&ucDirtyBits, i, 1);
            }
         }
      }
   }

   /* If no valid node selected, check for a fault to update */
   if( eNode == NUM_ALARM_NODES )
   {
      for(uint8_t ucSlot = 0; ucSlot < NUM_ALARM_NODES; ucSlot++)
      {
         if ( Sys_Bit_Get(&ucDirtyBits, ucSlot) )
         {
            Sys_Bit_Set(&ucDirtyBits, ucSlot, 0);
            GetActiveAlarmData(&stData, ucSlot );
            eNode = ucSlot;
            ucState = 0;
            break;
         }
      }
   }
   /* Otherwise load data for that node */
   else
   {
      memset(punDatagram, 0, sizeof(un_sdata_datagram));
      if(ucState == 0)
      {
         punDatagram->auc8[0] = eNode | (ucState << 4);
         punDatagram->auc8[1] = stData.eAlarmNum >> 8;
         punDatagram->auw16[1] = stData.wSpeed;
         punDatagram->aui32[1] = (stData.ulPosition - GetPositionReference() + 65535);
         ucState = 1;
      }
      else
      {
         punDatagram->auc8[0] = eNode | (ucState << 4);
         punDatagram->auc8[1] = stData.eAlarmNum & 0xFF;
         punDatagram->auw16[1] = stData.wSpeed;
         punDatagram->aui32[1] = stData.ulTimestamp;
         eNode = NUM_ALARM_NODES;
         ucState = 0;
      }
      bNewAlarmToSend = 1;
   }

   return bNewAlarmToSend;
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Transmit( void )
{
   un_sdata_datagram unDatagram;
   uint8_t iError = 1; // 1 = error, 0 = success.
   uint8_t ucDatagramID;
   for( uint8_t i = 0; i < CAN_BUFFER_LAST; i++ )
   {
      ucDatagramID = SDATA_GetNextDatagramToSendIndex_Plus1( &gstSData_GroupNet_MRB );
      if (ucDatagramID && (ucDatagramID <= NUM_GroupNet_MRB_DATAGRAMS) )
      {
         ucDatagramID--;
         SDATA_ReadDatagram( &gstSData_GroupNet_MRB,ucDatagramID,&unDatagram);

         memcpy( gstCAN_MSG_Tx.Data, unDatagram.auc8, sizeof( unDatagram.auc8 ) );

         enum en_sdata_networks eNet = SDATA_NET__GROUP;
         enum en_group_net_nodes eLocalNode = Param_ReadValue_8Bit(enPARAM8__GroupCarIndex);
         GroupNet_DatagramID eID = GroupNet_LocalCarIDToGlobalID(eLocalNode, ucDatagramID);
         uint16_t uwDest = GROUP_DEST__ALL;

         /* Todo rm after updating XREG which is using the wrong addressing*/
         if(ucDatagramID == DG_GroupNet_MRB__XRegCommand)
         {
            gstCAN_MSG_Tx.ID = GroupNet_EncodeCANMessageID(GROUP_NET_DATAGRAM_XREG_DATA0, eNet, eLocalNode, uwDest);
         }
         else
         {
            gstCAN_MSG_Tx.ID = GroupNet_EncodeCANMessageID(eID, eNet, eLocalNode, uwDest);
         }

         CAN_BUFFER_ID_T   TxBuf = Chip_CAN_GetFreeTxBuf(LPC_CAN2);
         // Sys_CAN_Send returns 0 for error and 1 for success. We need to invert this.
         iError = !Chip_CAN_Send(LPC_CAN2, TxBuf, &gstCAN_MSG_Tx );
         if ( iError ) // There is an error.
         {
            SDATA_DirtyBit_Set( &gstSData_GroupNet_MRB, ucDatagramID );

            if ( gstSData_GroupNet_MRB.uwLastSentIndex )
            {
               --gstSData_GroupNet_MRB.uwLastSentIndex;
            }
            else
            {
               gstSData_GroupNet_MRB.uwLastSentIndex = gstSData_GroupNet_MRB.uiNumDatagrams - 1;
            }
            break;
         }
      }
      else
      {
         break;
      }
   }
}




/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadData( void )
{
   // Unload functions moved to mod_can.c
}

void LoadDatagram_ParamSync_GUI(uint8_t* aucData)
{
   /* Ignore new message if attempt to transmit is already in progress */
   if( !SDATA_DirtyBit_Get(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__ParamEditResponse) )
   {
      un_sdata_datagram unDatagram;
      memcpy( unDatagram.auc8, aucData, sizeof( unDatagram.auc8 ) );
      SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__ParamEditResponse, &unDatagram);
   }
}

void LoadDatagram_SyncTime( uint32_t ucTime )
{
	un_sdata_datagram unDatagram;
	unDatagram.aui32[0] = ucTime;
	unDatagram.aui32[1] = 0; // Fixed uninitialized value
	SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__SyncClock, &unDatagram);
	SDATA_DirtyBit_Set(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__SyncClock);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define MAPPED_IO_DATAGRAM_UPDATE_RATE_MS          (1000)
#define MAPPED_IO_DATAGRAM_MINIMUM_RESEND_MS       (10000)
#define NUM_IO_DATAGRAMS                           (5)
static void Load_MappedIO()
{
   static uint16_t uwUpdateDelay_ms = MAPPED_IO_DATAGRAM_UPDATE_RATE_MS;
   un_sdata_datagram unDatagram;
   if( uwUpdateDelay_ms < MAPPED_IO_DATAGRAM_UPDATE_RATE_MS )
   {
      uwUpdateDelay_ms += gstMod_SData_GroupNet.uwRunPeriod_1ms;
   }
   else
   {
      uwUpdateDelay_ms = 0;
      /* Load all mapped inputs */
      unDatagram.aui32[0] = GetMappedInputs1_InputBitmap1_MRA();
      unDatagram.aui32[1] = GetMappedInputs1_InputBitmap2_MRA();
      SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__MappedInputs1, &unDatagram);

      unDatagram.aui32[0] = GetMappedInputs2_InputBitmap3_MRA();
      unDatagram.aui32[1] = GetMappedInputs2_InputBitmap4_MRA();
      SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__MappedInputs2, &unDatagram);

      unDatagram.aui32[0] = GetMappedInputs3_InputBitmap5_MRA();
      unDatagram.aui32[1] = GetMappedInputs3_InputBitmap6_MRA();
      SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__MappedInputs3, &unDatagram);

      /* Load all mapped outputs */
      unDatagram.aui32[0] = GetMappedOutputs1_OutputBitmap1_MRA();
      unDatagram.aui32[1] = GetMappedOutputs1_OutputBitmap2_MRA();
      SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__MappedOutputs1, &unDatagram);

      unDatagram.aui32[0] = GetMappedOutputs2_OutputBitmap3_MRA();
      unDatagram.aui32[1] = GetMappedOutputs2_OutputBitmap4_MRA();
      SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__MappedOutputs2, &unDatagram);
   }

   //---------------------------------------
   static const uint8_t aucDatagrams[NUM_IO_DATAGRAMS] =
   {
         DG_GroupNet_MRB__MappedInputs1,
         DG_GroupNet_MRB__MappedInputs2,
         DG_GroupNet_MRB__MappedInputs3,
         DG_GroupNet_MRB__MappedOutputs1,
         DG_GroupNet_MRB__MappedOutputs2,
   };
   static uint16_t auwMinResendDelay_ms[NUM_IO_DATAGRAMS];

   for(uint8_t i = 0; i < NUM_IO_DATAGRAMS; i++)
   {
      if( !SDATA_DirtyBit_Get(&gstSData_GroupNet_MRB, aucDatagrams[i]) )
      {
         if(auwMinResendDelay_ms[i] < MAPPED_IO_DATAGRAM_MINIMUM_RESEND_MS)
         {
            auwMinResendDelay_ms[i] += gstMod_SData_GroupNet.uwRunPeriod_1ms;
         }
         else
         {
            auwMinResendDelay_ms[i] = 0;
            SDATA_DirtyBit_Set(&gstSData_GroupNet_MRB, aucDatagrams[i]);
         }
      }
      else
      {
         auwMinResendDelay_ms[i] = 0;
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define CAR_STATUS_DATAGRAM_UPDATE_RATE_MS         (0)
#define CAR_STATUS_DATAGRAM_MINIMUM_RESEND_MS      (500)
static void Load_CarStatus()
{
   static uint16_t uwUpdateDelay_ms = CAR_STATUS_DATAGRAM_UPDATE_RATE_MS;
   un_sdata_datagram unDatagram;
   if( uwUpdateDelay_ms < CAR_STATUS_DATAGRAM_UPDATE_RATE_MS )
   {
      uwUpdateDelay_ms += gstMod_SData_GroupNet.uwRunPeriod_1ms;
   }
   else
   {
      uwUpdateDelay_ms = 0;
      LoadDatagram_CarStatus(&unDatagram);
      SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__CAR_STATUS, &unDatagram);
   }

   static uint16_t uwMinResendDelay_ms;
   if(!SDATA_DirtyBit_Get(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__CAR_STATUS))
   {
      uint32_t uiResendLimit = CAR_STATUS_DATAGRAM_MINIMUM_RESEND_MS;
      if(CarData_GetFastResend())
      {
         uiResendLimit /= 2;
      }

      if(uwMinResendDelay_ms < uiResendLimit)
      {
         uwMinResendDelay_ms += gstMod_SData_GroupNet.uwRunPeriod_1ms;
      }
      else
      {
         uwMinResendDelay_ms = 0;
         SDATA_DirtyBit_Set(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__CAR_STATUS);
      }
   }
   else
   {
      uwMinResendDelay_ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define CAR_POSITION_DATAGRAM_UPDATE_RATE_MS          (1000)
#define CAR_POSITION_DATAGRAM_MINIMUM_RESEND_MS       (10000)
static void Load_CarPosition()
{
   static uint16_t uwUpdateDelay_ms = CAR_POSITION_DATAGRAM_UPDATE_RATE_MS;
   un_sdata_datagram unDatagram;
   if( uwUpdateDelay_ms < CAR_POSITION_DATAGRAM_UPDATE_RATE_MS )
   {
      uwUpdateDelay_ms += gstMod_SData_GroupNet.uwRunPeriod_1ms;
   }
   else
   {
      uwUpdateDelay_ms = 0;
      unDatagram.auw16[0] = GetMotion_SpeedCommand();
      unDatagram.auw16[1] = GetPosition_Velocity();
      unDatagram.aui32[1] = (GetPosition_PositionCount() - GetPositionReference() + 65535);

      SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__CAR_POSITION, &unDatagram);
   }

   static uint16_t uwMinResendDelay_ms;
   if(!SDATA_DirtyBit_Get(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__CAR_POSITION))
   {
      if(uwMinResendDelay_ms < CAR_POSITION_DATAGRAM_MINIMUM_RESEND_MS)
      {
         uwMinResendDelay_ms += gstMod_SData_GroupNet.uwRunPeriod_1ms;
      }
      else
      {
         uwMinResendDelay_ms = 0;
         SDATA_DirtyBit_Set(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__CAR_POSITION);
      }
   }
   else
   {
      uwMinResendDelay_ms = 0;
   }
}
/*-----------------------------------------------------------------------------
 NOTE: Max starting landing of 32
 -----------------------------------------------------------------------------*/
#define CAR_CALLS_DATAGRAM_UPDATE_RATE_MS          (0)
#define CAR_CALLS_DATAGRAM_MINIMUM_RESEND_MS       (10000)
#define NUM_CC_DATAGRAMS                           (3)
static void Load_CarCalls()
{
   static uint16_t uwUpdateDelay_ms = CAR_CALLS_DATAGRAM_UPDATE_RATE_MS;
   un_sdata_datagram unDatagram;
   if( uwUpdateDelay_ms < CAR_CALLS_DATAGRAM_UPDATE_RATE_MS )
   {
      uwUpdateDelay_ms += gstMod_SData_GroupNet.uwRunPeriod_1ms;
   }
   else
   {
      uwUpdateDelay_ms = 0;
      st_cardata *pstCarData = GetCarDataStructure(GetFP_GroupCarIndex());

      unDatagram.aui32[0] = pstCarData->auiBF_CarCalls_F[0];
      unDatagram.aui32[1] = pstCarData->auiBF_CarCalls_R[0];
      SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__CarCalls1, &unDatagram);

      unDatagram.aui32[0] = pstCarData->auiBF_CarCalls_F[1];
      unDatagram.aui32[1] = pstCarData->auiBF_CarCalls_R[1];
      SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__CarCalls2, &unDatagram);

      unDatagram.aui32[0] = pstCarData->auiBF_CarCalls_F[2];
      unDatagram.aui32[1] = pstCarData->auiBF_CarCalls_R[2];
      SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__CarCalls3, &unDatagram);

   }

   //---------------------------------------
   static const uint8_t aucDatagrams[NUM_CC_DATAGRAMS] =
   {
         DG_GroupNet_MRB__CarCalls1,
         DG_GroupNet_MRB__CarCalls2,
         DG_GroupNet_MRB__CarCalls3,
   };
   static uint16_t auwMinResendDelay_ms[NUM_CC_DATAGRAMS];

   for(uint8_t i = 0; i < NUM_CC_DATAGRAMS; i++)
   {
      if( !SDATA_DirtyBit_Get(&gstSData_GroupNet_MRB, aucDatagrams[i]) )
      {
         if(auwMinResendDelay_ms[i] < CAR_CALLS_DATAGRAM_MINIMUM_RESEND_MS)
         {
            auwMinResendDelay_ms[i] += gstMod_SData_GroupNet.uwRunPeriod_1ms;
         }
         else
         {
            auwMinResendDelay_ms[i] = 0;
            SDATA_DirtyBit_Set(&gstSData_GroupNet_MRB, aucDatagrams[i]);
         }
      }
      else
      {
         auwMinResendDelay_ms[i] = 0;
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define RISER_OUTPUTS_DATAGRAM_UPDATE_RATE_MS         (0)
#define RISER_OUTPUTS_DATAGRAM_MINIMUM_RESEND_MS      (10000)
static void Load_RiserOutputs()
{
   static uint16_t uwUpdateDelay_ms = RISER_OUTPUTS_DATAGRAM_UPDATE_RATE_MS;
   un_sdata_datagram unDatagram;
   if( uwUpdateDelay_ms < RISER_OUTPUTS_DATAGRAM_UPDATE_RATE_MS )
   {
      uwUpdateDelay_ms += gstMod_SData_GroupNet.uwRunPeriod_1ms;
   }
   else
   {
      uwUpdateDelay_ms = 0;
      LoadDatagram_RiserOutputs(&unDatagram);
      SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__RiserOutputs, &unDatagram);
   }

   static uint16_t uwMinResendDelay_ms;
   if(!SDATA_DirtyBit_Get(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__RiserOutputs))
   {
      if(uwMinResendDelay_ms < RISER_OUTPUTS_DATAGRAM_MINIMUM_RESEND_MS)
      {
         uwMinResendDelay_ms += gstMod_SData_GroupNet.uwRunPeriod_1ms;
      }
      else
      {
         uwMinResendDelay_ms = 0;
         SDATA_DirtyBit_Set(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__RiserOutputs);
      }
   }
   else
   {
      uwMinResendDelay_ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Load_CarData()
{
   un_sdata_datagram unDatagram;
   if(!SDATA_DirtyBit_Get(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__CarData))
   {
      if(LoadDatagram_CarData(&unDatagram))
      {
         SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__CarData, &unDatagram);
         SDATA_DirtyBit_Set(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__CarData);
      }
   }
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Load_Faults(void)
{
   un_sdata_datagram unDatagram;
   if(!SDATA_DirtyBit_Get(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__Faults))
   {
      if(LoadDatagram_Faults(&unDatagram))
      {
         SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__Faults, &unDatagram);
         SDATA_DirtyBit_Set(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__Faults);
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Load_Alarms(void)
{
   un_sdata_datagram unDatagram;
   if(!SDATA_DirtyBit_Get(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__Alarms))
   {
      if(LoadDatagram_Alarms(&unDatagram))
      {
         SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__Alarms, &unDatagram);
         SDATA_DirtyBit_Set(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__Alarms);
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define RUN_LOG_DATAGRAM_UPDATE_RATE_MS      (200)
static void Load_RunLog()
{
   static uint16_t uwUpdateDelay_ms;
//   un_sdata_datagram unDatagram;
   if( uwUpdateDelay_ms < RUN_LOG_DATAGRAM_UPDATE_RATE_MS )
   {
      uwUpdateDelay_ms += gstMod_SData_GroupNet.uwRunPeriod_1ms;
   }
   else
   {
      uwUpdateDelay_ms = 0;
//      SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__RunLog, &unDatagram);
      LoadDatagram_RunLog_MRB();
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define MASTER_DISPATCHER_DATAGRAM_UPDATE_RATE_MS      (0)
static void Load_MasterDispatcher()
{
   static uint16_t uwUpdateDelay_ms;
   un_sdata_datagram unDatagram;
   if(!SDATA_DirtyBit_Get(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__MasterDispatcher))
   {
      if( uwUpdateDelay_ms < MASTER_DISPATCHER_DATAGRAM_UPDATE_RATE_MS )
      {
         uwUpdateDelay_ms += gstMod_SData_GroupNet.uwRunPeriod_1ms;
      }
      else
      {
         uwUpdateDelay_ms = 0;
         if(LoadDatagram_MasterDispatcher(&unDatagram, &gstMod_SData_GroupNet))
         {
            SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__MasterDispatcher, &unDatagram);
         }
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define VIP_MASTER_DISPATCHER_DATAGRAM_UPDATE_RATE_MS       (0)
static void Load_VIPMasterDispatcher()
{
   VIP_LoadDatagram_MasterDispatcher( &gstMod_SData_GroupNet );
}
/*-----------------------------------------------------------------------------
// moved to mod_dynam_parking.c
 -----------------------------------------------------------------------------*/
//static void Load_DynamicParking()

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define CAR_OPERATION_DATAGRAM_UPDATE_RATE_MS         (100)
#define CAR_OPERATION_DATAGRAM_MINIMUM_RESEND_MS      (10000)
static void Load_CarOperation()
{
   static uint16_t uwUpdateDelay_ms;
   un_sdata_datagram unDatagram;
   if( uwUpdateDelay_ms < CAR_OPERATION_DATAGRAM_UPDATE_RATE_MS )
   {
      uwUpdateDelay_ms += gstMod_SData_GroupNet.uwRunPeriod_1ms;
   }
   else
   {
      uwUpdateDelay_ms = 0;

      memset(&unDatagram, 0, sizeof(un_sdata_datagram));
      LoadDatagram_CarOperation( &unDatagram );
      SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__CarOperation, &unDatagram);
   }

   static uint16_t uwMinResendDelay_ms;
   if(!SDATA_DirtyBit_Get(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__CarOperation))
   {
      if(uwMinResendDelay_ms < CAR_OPERATION_DATAGRAM_MINIMUM_RESEND_MS)
      {
         uwMinResendDelay_ms += gstMod_SData_GroupNet.uwRunPeriod_1ms;
      }
      else
      {
         uwMinResendDelay_ms = 0;
         SDATA_DirtyBit_Set(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__CarOperation);
      }
   }
   else
   {
      uwMinResendDelay_ms = 0;
   }
}
/*-----------------------------------------------------------------------------
   - Hall call latch/delatch
 -----------------------------------------------------------------------------*/
#define MASTER_COMMAND_DATAGRAM_UPDATE_RATE_MS      (0)
static void Load_MasterCommand()
{
   static uint16_t uwUpdateDelay_ms;
   un_sdata_datagram unDatagram;
   if(!SDATA_DirtyBit_Get(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__MasterCommand))
   {
      if( uwUpdateDelay_ms < MASTER_COMMAND_DATAGRAM_UPDATE_RATE_MS )
      {
         uwUpdateDelay_ms += gstMod_SData_GroupNet.uwRunPeriod_1ms;
      }
      else
      {
         uwUpdateDelay_ms = 0;
         if(LoadDatagram_MasterCommand(&unDatagram))
         {
            SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__MasterCommand, &unDatagram);
            SDATA_DirtyBit_Set(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__MasterCommand);
         }
      }
   }
}
/*-----------------------------------------------------------------------------
   Emergency power command
 -----------------------------------------------------------------------------*/
#define EMERGENCY_POWER_DATAGRAM_UPDATE_RATE_MS      (10000)
static void Load_EmergencyPower()
{
   static uint16_t uwUpdateDelay_ms;
   un_sdata_datagram unDatagram;
   if( GetMasterDispatcherFlag() )
   {
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         unDatagram.auc8[i] = GetEPowerCommand(i);
      }
      SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__EmergencyPower, &unDatagram);

      /* Added a minimum update rate for unchanged packet */
      if(!SDATA_DirtyBit_Get(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__EmergencyPower))
      {
         if( uwUpdateDelay_ms < EMERGENCY_POWER_DATAGRAM_UPDATE_RATE_MS )
         {
            uwUpdateDelay_ms += gstMod_SData_GroupNet.uwRunPeriod_1ms;
         }
         else
         {
            uwUpdateDelay_ms = 0;
            SDATA_DirtyBit_Set(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__EmergencyPower);
         }
      }
      else
      {
         uwUpdateDelay_ms = 0;
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Load_ParamEditResponse()
{
   un_sdata_datagram unDatagram;
   if(!SDATA_DirtyBit_Get(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__ParamEditResponse))
   {
      if(LoadDatagram_ParamEditResponse(&unDatagram))
      {
         SDATA_WriteDatagram( &gstSData_GroupNet_MRB, DG_GroupNet_MRB__ParamEditResponse, &unDatagram );
         SDATA_DirtyBit_Set(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__ParamEditResponse);
         //TODO rm test
         InsertGroupNetDatagram_ByDatagramID(&unDatagram, GroupNet_LocalCarIDToGlobalID(GetFP_GroupCarIndex(), DG_GroupNet_MRB__ParamEditResponse));
      }
   }
}
/*-----------------------------------------------------------------------------
 Dispatching messages to the XREG dispatcher
 -----------------------------------------------------------------------------*/
static void LoadXRegCommand(void)
{
   un_sdata_datagram unDatagram;
   if(!SDATA_DirtyBit_Get(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__XRegCommand))
   {
      if(LoadDatagram_XRegDispatcher(&unDatagram, &gstMod_SData_GroupNet))
      {
         SDATA_WriteDatagram(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__XRegCommand, &unDatagram);
         SDATA_DirtyBit_Set(&gstSData_GroupNet_MRB, DG_GroupNet_MRB__XRegCommand);
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData( void )
{
   Load_MappedIO();
   Load_CarStatus();
   Load_CarPosition();
   Load_CarCalls();
   Load_RiserOutputs();
   Load_CarData();
   Load_VIPMasterDispatcher();
   Load_Faults();
   Load_Alarms();
   Load_RunLog();
   Load_MasterDispatcher();
//   Load_DynamicParking();// moved to mod_dynam_parking.c
   Load_CarOperation();
   Load_MasterCommand();
   Load_EmergencyPower();
   Load_ParamEditResponse();
   LoadXRegCommand();
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   //------------------------------------------------
   gstSData_Config.stCAN_Config.ucIndex = enCAN_2;

   gstSData_Config.stCAN_Config.ulBAUD = enCAN_BAUD_125k;

   gstSData_GroupNet_MRB.bDisableResend = 1;
   gstSData_GroupNet_MRB.bDisableHeartbeat = 1;
   //------------------------------------------------
   pstThisModule->uwInitialDelay_1ms = 5;
   pstThisModule->uwRunPeriod_1ms = 5;

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   gstSData_Config.ucLocalNodeID = Param_ReadValue_8Bit(enPARAM8__GroupCarIndex);
   SetFP_GroupCarIndex(Param_ReadValue_8Bit(enPARAM8__GroupCarIndex));

   UpdateMasterDispatcherFlag();

   UpdateLocalGroupCarData();

   UnloadData();

   LoadData();
   Transmit();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

