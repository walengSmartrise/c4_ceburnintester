
/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief
 * @version  V1.00
 * @date     27, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>

#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "masterCommand.h"
#include "hallData.h"
#include "carData.h"
#include "group_net_data.h"
#include "mod_ems.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
/* Buffer of last received command datagrams */
static un_sdata_datagram aunDatagramBuffer[NUM_MASTER_CMD];
static uint8_t aucDirtyBits[BITMAP8_SIZE(NUM_MASTER_CMD)]; /* Need to send bits */
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t MasterCommand_GetDirtyBit( en_master_cmd eCMD )
{
   uint8_t bDirty = Sys_Bit_Get(&aucDirtyBits[0], eCMD);
   return bDirty;
}
void MasterCommand_SetDirtyBit( en_master_cmd eCMD )
{
   Sys_Bit_Set(&aucDirtyBits[0], eCMD, 1);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void MasterCommand_ResetCommandBuffer( en_master_cmd eCMD )
{
   if( eCMD < NUM_MASTER_CMD )
   {
      Sys_Bit_Set(&aucDirtyBits[0], eCMD, 0);
      memset(&aunDatagramBuffer[eCMD], 0, sizeof(un_sdata_datagram));
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void MasterCommand_SetHallCall( uint8_t ucLanding, en_hc_dir eDir, uint32_t uiMask )
{
   un_sdata_datagram unDatagram;
   unDatagram.auc8[0] = MASTER_CMD__SetHallCall;
   unDatagram.auc8[1] = ucLanding;
   unDatagram.auc8[2] = eDir;
   unDatagram.auc8[3] = 0;
   unDatagram.aui32[1] = uiMask;
   if( ( unDatagram.aui32[0] != aunDatagramBuffer[MASTER_CMD__SetHallCall].aui32[0] )
    || ( unDatagram.aui32[1] != aunDatagramBuffer[MASTER_CMD__SetHallCall].aui32[1] ) )
   {
      Sys_Bit_Set(&aucDirtyBits[0], MASTER_CMD__SetHallCall, 1);
      aunDatagramBuffer[MASTER_CMD__SetHallCall].aui32[0] = unDatagram.aui32[0];
      aunDatagramBuffer[MASTER_CMD__SetHallCall].aui32[1] = unDatagram.aui32[1];

      /* Also latch locally */
      SetRawLatchedHallCalls(ucLanding, eDir, uiMask);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void MasterCommand_ClrHallCall( uint8_t ucLanding, en_hc_dir eDir, uint32_t uiMask )
{
   un_sdata_datagram unDatagram;
   unDatagram.auc8[0] = MASTER_CMD__ClrHallCall;
   unDatagram.auc8[1] = ucLanding;
   unDatagram.auc8[2] = eDir;
   unDatagram.auc8[3] = 0;
   unDatagram.aui32[1] = uiMask;
   if( ( unDatagram.aui32[0] != aunDatagramBuffer[MASTER_CMD__ClrHallCall].aui32[0] )
    || ( unDatagram.aui32[1] != aunDatagramBuffer[MASTER_CMD__ClrHallCall].aui32[1] ) )
   {
      Sys_Bit_Set(&aucDirtyBits[0], MASTER_CMD__ClrHallCall, 1);
      aunDatagramBuffer[MASTER_CMD__ClrHallCall].aui32[0] = unDatagram.aui32[0];
      aunDatagramBuffer[MASTER_CMD__ClrHallCall].aui32[1] = unDatagram.aui32[1];

      /* Also delatch locally */
      ClrRawLatchedHallCalls(ucLanding, eDir, uiMask);
   }
}
/*-----------------------------------------------------------------------------
   Validate car call landing prior to calling this function
 -----------------------------------------------------------------------------*/
void MasterCommand_SetCarCall( enum en_group_net_nodes eCarID, uint8_t ucLanding, enum en_doors eDoor )
{
   if(eCarID == GetFP_GroupCarIndex())
   {
      SetCarCall(GetCarMapping(ucLanding), eDoor);
   }
   else
   {
      un_sdata_datagram unDatagram;
      unDatagram.auc8[0] = MASTER_CMD__SetCarCall;
      unDatagram.auc8[1] = ucLanding;
      unDatagram.auc8[2] = eDoor;
      unDatagram.auc8[3] = eCarID;
      unDatagram.aui32[1] = 0;
      if( ( unDatagram.aui32[0] != aunDatagramBuffer[MASTER_CMD__SetCarCall].aui32[0] )
       || ( unDatagram.aui32[1] != aunDatagramBuffer[MASTER_CMD__SetCarCall].aui32[1] ) )
      {
         Sys_Bit_Set(&aucDirtyBits[0], MASTER_CMD__SetCarCall, 1);
         aunDatagramBuffer[MASTER_CMD__SetCarCall].aui32[0] = unDatagram.aui32[0];
         aunDatagramBuffer[MASTER_CMD__SetCarCall].aui32[1] = unDatagram.aui32[1];
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void MasterCommand_SetEMSAssignment( enum en_group_net_nodes eCarID, uint8_t ucLanding_Plus1 )
{
   un_sdata_datagram unDatagram;
   en_master_cmd eCommand = MASTER_CMD__EMS_Assignment_Car1 + eCarID;
   if( ( eCarID != GetFP_GroupCarIndex() )
    && ( eCommand >= MASTER_CMD__EMS_Assignment_Car1 )
    && ( eCommand <= MASTER_CMD__EMS_Assignment_Car8 ) )
   {
      unDatagram.auc8[0] = eCommand;
      unDatagram.auc8[1] = ucLanding_Plus1;
      unDatagram.auc8[2] = 0;
      unDatagram.auc8[3] = 0;
      unDatagram.aui32[1] = 0;
      if( ( unDatagram.aui32[0] != aunDatagramBuffer[eCommand].aui32[0] )
       || ( unDatagram.aui32[1] != aunDatagramBuffer[eCommand].aui32[1] ) )
      {
         Sys_Bit_Set(&aucDirtyBits[0], eCommand, 1);
         aunDatagramBuffer[eCommand].aui32[0] = unDatagram.aui32[0];
         aunDatagramBuffer[eCommand].aui32[1] = unDatagram.aui32[1];
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t LoadDatagram_MasterCommand( un_sdata_datagram *punDatagram )
{
   uint8_t bSend = 0;

   for(uint8_t i = MASTER_CMD__None+1; i < NUM_MASTER_CMD; i++)
   {
      if( Sys_Bit_Get(&aucDirtyBits[0], i) )
      {
         Sys_Bit_Set(&aucDirtyBits[0], i, 0);
         punDatagram->aui32[0] = aunDatagramBuffer[i].aui32[0];
         punDatagram->aui32[1] = aunDatagramBuffer[i].aui32[1];
         memset(&aunDatagramBuffer[i], 0, sizeof(un_sdata_datagram));
         bSend = 1;
         break;
      }
   }

   return bSend;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UnloadDatagram_MasterCommand( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   en_master_cmd eCMD = punDatagram->auc8[0];
   uint8_t ucLanding = punDatagram->auc8[1];
   en_hc_dir eDir = punDatagram->auc8[2];
   enum en_doors eDoor = punDatagram->auc8[2];
   enum en_group_net_nodes eCar = punDatagram->auc8[3];
   uint32_t uiMask = punDatagram->aui32[1];
   if( ucLanding < MAX_NUM_FLOORS )
   {
      if( eCMD == MASTER_CMD__SetHallCall )
      {
         if( eDir <= HC_DIR__UP )
         {
            SetRawLatchedHallCalls(ucLanding, eDir, uiMask);
         }
      }
      else if( eCMD == MASTER_CMD__ClrHallCall )
      {
         if( eDir <= HC_DIR__UP )
         {
            ClrRawLatchedHallCalls(ucLanding, eDir, uiMask);
         }
      }
      else if( eCMD == MASTER_CMD__SetCarCall )
      {
         if( ( eCar == GetFP_GroupCarIndex() )
          && ( eDoor < NUM_OF_DOORS ) )
         {
            uint8_t ucFloorIndex = GetCarMapping(ucLanding);
            SetCarCall(ucFloorIndex, eDoor);
         }
      }
   }

   if( ( CarData_GetMasterDispatcherFlag(eCarID) )
    && ( eCMD >= MASTER_CMD__EMS_Assignment_Car1 )
    && ( eCMD <= MASTER_CMD__EMS_Assignment_Car8 ) )
   {
      enum en_group_net_nodes eAssignedCar = eCMD - MASTER_CMD__EMS_Assignment_Car1;
      uint8_t ucLanding_Plus1 = punDatagram->auc8[1];
      EMS_SetRecallLanding_Plus1(eAssignedCar, ucLanding_Plus1);
   }
}


/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
