
#include "mod.h"
#include "board.h"
#include <stdio.h>
#include "sru.h"
#include "sru_b.h"
#include "sys.h"


static struct st_module * gastModules_MRB[] =
{
   &gstSys_Mod,  // must include the system module in addition to app modules
   &gstMod_Fault,
   &gstMod_FaultApp,
   &gstMod_Heartbeat,
   &gstMod_Watchdog,

   &gstMod_CAN,
   &gstMod_UART,
   &gstMod_ParamEEPROM,
   &gstMod_ParamApp,
   &gstMod_ParamMRB,

   &gstMod_SData,
   &gstMod_SData_GroupNet,
   &gstMod_SData_AuxNet,
   &gstMod_LCD,
   &gstMod_Buttons,

   &gstMod_UI,
   &gstMod_CEboards,
   &gstMod_RTC,
   &gstMod_Shield,
   &gstMod_AlarmApp,

   &gstMod_LocalOutputs,
   &gstMod_LoadWeigher,
   &gstMod_RunLog,
   &gstMod_Dispatching,
   &gstMod_EmergencyPowerMaster,

   &gstMod_LanternMonitor,
   &gstMod_XReg,
   &gstMod_EMS,
   &gstMod_DyPark,
};

static struct st_module_control gstModuleControl_MRB =
{
   .uiNumModules = sizeof(gastModules_MRB) / sizeof(gastModules_MRB[ 0 ]),

   .pastModules = &gastModules_MRB,

   .enMCU_ID = enMCUB_SRU_UI,

   .enDeployment = enSRU_DEPLOYMENT__MR,
};

static struct st_module * gastModules_MRB_Pre[] =
{
         &gstSys_Mod,  // must include the system module in addition to app modules

         &gstMod_ParamEEPROM,
         &gstMod_ParamApp,
         &gstMod_ParamMRB,
};

static struct st_module_control gstModuleControl_MRB_Pre =
{
   .uiNumModules = sizeof(gastModules_MRB_Pre) / sizeof(gastModules_MRB_Pre[ 0 ]),

   .pastModules = &gastModules_MRB_Pre,

   .enMCU_ID = enMCUB_SRU_UI,

   .enDeployment = enSRU_DEPLOYMENT__MR,
};
/**
 * @brief   main routine for systick example
 * @return  Function should not exit.
 */
int main_MR( void )
{
     __enable_irq();

     // These are the modules that need to be started before the main application.
     // Right now this is just parameters. This is because most modules require
     // parameters and the parameters are not valid till they are loaded into RAM
     Mod_InitAllModules( &gstModuleControl_MRB_Pre );
     while(!GetFP_FlashParamsReady())
     {
         Mod_RunOneModule( &gstModuleControl_MRB_Pre );
     }

     Mod_InitAllModules( &gstModuleControl_MRB );

    // This is really a while(1) loop. Sys_Shutdown() always returns 0. The
    // function is being provided in anticipation of a future time when this
    // application might be running on a PC simulator and we would need a
    // mechanism to terminate this application.
    while ( !Sys_Shutdown() )
    {
        Mod_RunOneModule( &gstModuleControl_MRB );

        uint8_t ucCurrModIndex = gstModuleControl_MRB.uiCurrModIndex;
        uint8_t ucNumOfMods = gstModuleControl_MRB.uiNumModules;

        struct st_module * (* pastModules)[] = gstModuleControl_MRB.pastModules;
        struct st_module * pstCurrentModule = (*pastModules)[ ucCurrModIndex ];

        if( ( pstCurrentModule->bTimeViolation )
         && ( ucCurrModIndex < ucNumOfMods )
         && ( ( Param_ReadValue_8Bit(enPARAM8__TimeViolationModule) == MRC__ALL )
           || ( ( Param_ReadValue_8Bit(enPARAM8__TimeViolationModule)-MRC__MRB_START ) == ucCurrModIndex ) )  )
        {
           SetAlarm( ALM__RUN_TIME_FAULT_MRB + ucCurrModIndex - 1);
        }
        Debug_CAN_MonitorUtilization();
    }

    return 0;
}


