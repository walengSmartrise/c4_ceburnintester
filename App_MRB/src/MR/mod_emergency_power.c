/******************************************************************************
 *
 * @file     mod_emergency_power.c
 * @brief    Manages group car response to EP inputs
 * @version  V1.00
 * @date     9, November 2017
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "emergency_power.h"
#include "carData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init(struct st_module * pstThisModule);
static uint32_t Run(struct st_module * pstThisModule);

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_EmergencyPowerMaster = {
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define INPUT_DEBOUNCE_LIMIT_50MS         (10)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static enum enEmergencyPower enEmergencyPowerMode = EP_OFF;
static enum enEmergencyPowerCommands aeEP_Commands[MAX_GROUP_CARS];

static uint8_t ucActiveCar;

static enum enEmergencyPower enLastEmergencyPowerMode;

/* Debounced EP inputs */
static uint8_t bPretransfer;
static uint8_t bOnEP;
static uint8_t bUpToSpeed;
static uint8_t bAutoSelect;
static uint8_t abCarSelect[MAX_GROUP_CARS];

/* Debounced all cars recalled flag */
static uint8_t bAllCarsRecalled;
/*---------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
enum enEmergencyPowerCommands GetEPowerCommand ( enum en_group_net_nodes eNode )
{
   if( eNode <= GROUP_NET__CAR_8 )
   {
      return aeEP_Commands[eNode];
   }
   else
   {
      return 0;
   }
}
void SetEPowerCommand(enum enEmergencyPowerCommands eCommand, enum en_group_net_nodes eNode )
{
   if( eNode <= GROUP_NET__CAR_8 )
   {
      aeEP_Commands[eNode] = eCommand;
   }
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void UnloadDatagram_EmergencyPower(enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram)
{
   if( CarData_GetMasterDispatcherFlag(eCarID) )
   {
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         aeEP_Commands[i] = punDatagram->auc8[i];
      }
   }
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static uint8_t CheckIf_CarInEmergencyPowerMode( enum en_group_net_nodes eCarID )
{
   uint8_t bValid = 0;
   st_cardata * pstCarData = GetCarDataStructure( eCarID );
   if( pstCarData->eAutoMode == MODE_A__EPOWER )
   {
      bValid = 1;
   }
   else if( pstCarData->eEP_CommandFeedback == EPC__RUN_AUTO )
   {
      bValid = 1;
   }

   return bValid;
}

static uint8_t CheckIf_CarRecallComplete( enum en_group_net_nodes eCarID )
{
   uint8_t bValid = 0;
   st_cardata * pstCarData = GetCarDataStructure( eCarID );
   /* Recall finished and not in motion */
   if( ( pstCarData->bEP_CarRecalledFlag )
    && ( pstCarData->cMotionDir == DIR__NONE )
    && ( ( pstCarData->eDoor_F == DOOR__OPEN ) || ( pstCarData->eDoor_R == DOOR__OPEN ) ) )
   {
      bValid = 1;
   }

   return bValid;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static uint8_t CheckIf_AllCarsRecalled(void)
{
   uint8_t bRecalled = ((enEmergencyPowerMode == EP_RECALL)
                     || (enEmergencyPowerMode == EP_RUN_CAR));
   for (uint8_t ucCarIndex = 0; ucCarIndex < MAX_GROUP_CARS; ucCarIndex++)
   {
      st_cardata *pstCarData = GetCarDataStructure(ucCarIndex);
      /* If car is active */
      if( ( GetCarOfflineTimer_ByCar(ucCarIndex) < GROUP_CAR_OFFLINE_TIMER_MS )
       && ( !pstCarData->bEP_Disabled ) )
      {
         /* If car is in EP mode, but recall not finished */
         if( ( CheckIf_CarInEmergencyPowerMode(ucCarIndex) )
          && ( !CheckIf_CarRecallComplete(ucCarIndex) )
          && ( GetEPowerCommand(ucCarIndex) != EPC__RUN_AUTO ) )
         {
            bRecalled = 0;
            break;
         }
      }
   }
   return bRecalled;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static uint8_t UpdateRunOnAutoCommand()
{
   uint8_t bValid = 0;
   uint8_t ucNumActiveCars = 0;
   uint8_t ucLimitActiveCars = ( Param_ReadValue_8Bit(enPARAM8__NumEPowerCars) )
                             ? ( Param_ReadValue_8Bit(enPARAM8__NumEPowerCars) )
                             : 1;
   /*  If Auto_Select is on, select first online car */
   if( bAutoSelect )
   {
      for (uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         uint8_t ucCarToCheck = ( Param_ReadValue_8Bit(enPARAM8__EPowerPriorityCar)+i ) % MAX_GROUP_CARS;
         st_cardata *pstCarData = GetCarDataStructure(ucCarToCheck);
         if( ( GetCarOfflineTimer_ByCar(ucCarToCheck) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( CheckIf_CarInEmergencyPowerMode(ucCarToCheck) )
          && ( !pstCarData->bEP_Disabled ) )
         {
            bValid = 1;
            if( ucNumActiveCars < ucLimitActiveCars )
            {
               ucNumActiveCars++;
               SetEPowerCommand(EPC__RUN_AUTO, ucCarToCheck);
            }
            else
            {
               SetEPowerCommand(EPC__RECALL, ucCarToCheck);
            }
         }
         else
         {
            SetEPowerCommand(EPC__RECALL, ucCarToCheck);
         }
      }
   }
   else
   {
      for (uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         uint8_t ucCarToCheck = ( Param_ReadValue_8Bit(enPARAM8__EPowerPriorityCar)+i ) % MAX_GROUP_CARS;
         st_cardata *pstCarData = GetCarDataStructure(ucCarToCheck);
         if( ( abCarSelect[ucCarToCheck] )
          && ( GetCarOfflineTimer_ByCar(ucCarToCheck) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( CheckIf_CarInEmergencyPowerMode(ucCarToCheck) )
          && ( ucNumActiveCars < ucLimitActiveCars )
          && ( !pstCarData->bEP_Disabled ) )
         {
            ucNumActiveCars++;
            SetEPowerCommand(EPC__RUN_AUTO, ucCarToCheck);
            ucActiveCar = ucCarToCheck;
            bValid = 1;
         }
         else
         {
            SetEPowerCommand(EPC__RECALL, ucCarToCheck);
         }
      }
   }
   return bValid;
}

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static uint8_t GetNextCarToRecall()
{
   uint8_t ucRecallCar = MAX_GROUP_CARS;
   for (uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      uint8_t ucCarToCheck = ( Param_ReadValue_8Bit(enPARAM8__EPowerPriorityCar)+i ) % MAX_GROUP_CARS;
      st_cardata *pstCarData = GetCarDataStructure(ucCarToCheck);
      if( ( GetCarOfflineTimer_ByCar(ucCarToCheck) < GROUP_CAR_OFFLINE_TIMER_MS )
       && ( CheckIf_CarInEmergencyPowerMode(ucCarToCheck) )
       && ( !CheckIf_CarRecallComplete(ucCarToCheck) )
       && ( !pstCarData->bEP_Disabled ) )
      {
         ucRecallCar = ucCarToCheck;
         break;
      }
   }
   return ucRecallCar;
}
/*-----------------------------------------------------------------------------
   Updates debounced inputs
-----------------------------------------------------------------------------*/
static void UpdateDebouncedInputs(void)
{
   static uint8_t ucPretransferDebounce_50ms;
   static uint8_t ucOnEPDebounce_50ms;
   static uint8_t ucUpToSpeedDebounce_50ms;
   static uint8_t ucAutoSelectDebounce_50ms;
   static uint8_t aucCarSelectDebounce_50ms[MAX_GROUP_CARS];

   uint8_t bNewPretransfer = GetInputValue( enIN_EP_PRETRANSFER );
   if( bPretransfer != bNewPretransfer )
   {
      if( ++ucPretransferDebounce_50ms >= INPUT_DEBOUNCE_LIMIT_50MS )
      {
         bPretransfer = bNewPretransfer;
         ucPretransferDebounce_50ms = 0;
      }
   }
   else
   {
      ucPretransferDebounce_50ms = 0;
   }

   uint8_t bNewOnEP = GetInputValue( enIN_EP_ON );
   if( bOnEP != bNewOnEP )
   {
      if( ++ucOnEPDebounce_50ms >= INPUT_DEBOUNCE_LIMIT_50MS )
      {
         bOnEP = bNewOnEP;
         ucOnEPDebounce_50ms = 0;
      }
   }
   else
   {
      ucOnEPDebounce_50ms = 0;
   }

   uint8_t bNewUpToSpeed = GetInputValue( enIN_EP_UP_TO_SPEED );
   if( bUpToSpeed != bNewUpToSpeed )
   {
      if( ++ucUpToSpeedDebounce_50ms >= INPUT_DEBOUNCE_LIMIT_50MS )
      {
         bUpToSpeed = bNewUpToSpeed;
         ucUpToSpeedDebounce_50ms = 0;
      }
   }
   else
   {
      ucUpToSpeedDebounce_50ms = 0;
   }

   uint8_t bNewAutoSelect = GetInputValue( enIN_EP_AUTO_SELECT );
   if( bAutoSelect != bNewAutoSelect )
   {
      if( ++ucAutoSelectDebounce_50ms >= INPUT_DEBOUNCE_LIMIT_50MS )
      {
         bAutoSelect = bNewAutoSelect;
         ucAutoSelectDebounce_50ms = 0;
      }
   }
   else
   {
      ucAutoSelectDebounce_50ms = 0;
   }

   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      uint8_t bNewCarSelect = GetInputValue( enIN_EP_SELECT_1+i );
      if( abCarSelect[i] != bNewCarSelect )
      {
         if( ++aucCarSelectDebounce_50ms[i] >= INPUT_DEBOUNCE_LIMIT_50MS )
         {
            abCarSelect[i] = bNewCarSelect;
            aucCarSelectDebounce_50ms[i] = 0;
         }
      }
      else
      {
         aucCarSelectDebounce_50ms[i] = 0;
      }
   }
}
/*-----------------------------------------------------------------------------
   State machine control
-----------------------------------------------------------------------------*/
static void UpdateEmergencyPowerMode()
{
   static uint8_t bStartup = 1;
   static uint16_t uwDebounceTimer_50ms;

   /* Delay transition to EP_RUN_CAR or EP_RECALL at
    * power on to allow car to transition to a valid
    * automatic operation. Otherwise car may be seen
    * as not able to recall and skipped. */
   if( bStartup )
   {
      bAllCarsRecalled = 0;
      if( ++uwDebounceTimer_50ms >= EMERGENCY_POWER_STARTUP_DELAY_50MS )
      {
         bStartup = 0;
         uwDebounceTimer_50ms = 0;
      }
   }
   else
   {
      bAllCarsRecalled = 0;
      if( CheckIf_AllCarsRecalled() )
      {
         if( uwDebounceTimer_50ms >= EMERGENCY_POWER_RECALLED_DELAY_50MS )
         {
            bAllCarsRecalled = 1;
         }
         else
         {
            uwDebounceTimer_50ms++;
         }
      }
      else
      {
         uwDebounceTimer_50ms = 0;
      }
   }

   if( bPretransfer )
   {
      enEmergencyPowerMode = EP_PRETRANSFER; // all cars stop at nearest landing, open doors and fault
   }
   else if( bOnEP )
   {
      if( bUpToSpeed && !bStartup ) // && for intergroup, all cars have been fully recalled on higher priority groups
      {
         if( bAllCarsRecalled ) // && for intergroup, there is still generator capacity for additional cars and all groups have fully recalled their cars.
         {
            enEmergencyPowerMode = EP_RUN_CAR; // Run X number of cars based on generator capacity and number of cars active in higher priority group
         }
         else
         {
            enEmergencyPowerMode = EP_RECALL; // Recall cars one at a time
         }
      }
      else
      {
         enEmergencyPowerMode = EP_ON; // all cars should emergency stop and fault at their current location
      }
   }
   else
   {
      enEmergencyPowerMode = EP_OFF; // emergency power is not active, all cars behave as they normally would
      ucActiveCar = 0;
   }
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void EmergencyPowerMaster(struct st_module * pstThisModule)
{
   static uint32_t uiTimeout_ms;
   static uint32_t uiDebounceState_ms;

   if( enEmergencyPowerMode != enLastEmergencyPowerMode )
   {
      uiTimeout_ms = 0;
      uiDebounceState_ms = 0;
      ucActiveCar = GetNextCarToRecall();
   }

   switch (enEmergencyPowerMode)
   {
      case EP_ON:
         /*
          * Tell cars to stop running immediately
          */
         for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
         {
            SetEPowerCommand(EPC__OOS, i);
         }
         break;

      case EP_RECALL:
         /* If EP_ON and EP_UP_TO_SPEED are tied, module may skip straight to EP_RECALL mode.
          * Command OOS for any cars not yet being recalled */
         for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
         {
            if( GetEPowerCommand(i) != EPC__RECALL )
            {
               SetEPowerCommand(EPC__OOS, i);
            }
         }
         /*
          * cycle through the cars telling them to recall,
          * flag as complete once recall is finished
          */
         st_cardata * pstCarData = GetCarDataStructure( ucActiveCar );
         if( ( ucActiveCar < MAX_GROUP_CARS )
          && ( CheckIf_CarInEmergencyPowerMode(ucActiveCar) )
          && ( CheckIf_CarRecallComplete(ucActiveCar) ) )
         {
            if(uiDebounceState_ms >= EMERGENCY_POWER_RECALL_DEBOUNCE_MS)
            {
               uiDebounceState_ms = 0;
               uiTimeout_ms = 0;
               ucActiveCar = GetNextCarToRecall();
            }
            else
            {
               uiDebounceState_ms += pstThisModule->uwRunPeriod_1ms;
            }
         }
         else if( uiTimeout_ms >= EMERGENCY_POWER_RECALL_TIMEOUT_MS )
         {
            uiDebounceState_ms = 0;
            uiTimeout_ms = 0;
            SetEPowerCommand(EPC__OOS, ucActiveCar);
            ucActiveCar = GetNextCarToRecall();
         }
         else if( ucActiveCar >= MAX_GROUP_CARS )
         {
            uiDebounceState_ms = 0;
            uiTimeout_ms = 0;
            ucActiveCar = GetNextCarToRecall();
         }
         else if( ( !pstCarData->cMotionDir ) )
         {
            uiDebounceState_ms = 0;
            uiTimeout_ms += pstThisModule->uwRunPeriod_1ms;
         }
         else
         {
            uiDebounceState_ms = 0;
            uiTimeout_ms = 0;
         }

         if( CheckIf_CarInEmergencyPowerMode(ucActiveCar) )
         {
            SetEPowerCommand(EPC__RECALL, ucActiveCar);
         }
         else
         {
            SetEPowerCommand(EPC__OOS, ucActiveCar);
         }
         break;

      case EP_RUN_CAR:
         /*
          * Select the car(s) to run on normal and command them to go
          * Maybe add in code here for stutter starting cars?
          */
         if( uiTimeout_ms >= EMERGENCY_POWER_CAR_UPDATE_TIMEOUT_MS )
         {
            if( UpdateRunOnAutoCommand() )
            {
               uiTimeout_ms = 0;
            }
         }
         else
         {
            uiTimeout_ms += pstThisModule->uwRunPeriod_1ms;
         }
         break;

      case EP_PRETRANSFER:
         /*
          * Tell cars to immediately ramp down to the nearest landing
          */
         for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
         {
            SetEPowerCommand(EPC__PRETRANSFER, i);
         }
         break;

      default:
         for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
         {
            SetEPowerCommand(EPC__OFF, i);
         }
         break;
   }
   enLastEmergencyPowerMode = enEmergencyPowerMode;
}
/*-----------------------------------------------------------------------------

Initialize anything required prior to running for the first time

-----------------------------------------------------------------------------*/
static uint32_t Init(struct st_module * pstThisModule)
{
   pstThisModule->uwInitialDelay_1ms = 500;
   pstThisModule->uwRunPeriod_1ms = 50;

   return 0;
}

/*-----------------------------------------------------------------------------
 *
-----------------------------------------------------------------------------*/
static uint32_t Run(struct st_module * pstThisModule)
{
   if( GetMasterDispatcherFlag() )
   {
      UpdateDebouncedInputs();
      UpdateEmergencyPowerMode();
      EmergencyPowerMaster(pstThisModule);
   }
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
*----------------------------------------------------------------------------*/
