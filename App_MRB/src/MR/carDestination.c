/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief
 * @version  V1.00
 * @date     27, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>

#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "carData.h"
#include "carDestination.h"
#include "XRegDestination.h"
#include "XRegData.h"
#include "hallSecurity.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void ClrProposedDestination_ByCar(enum en_group_net_nodes eCarID);
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static st_carDestination astProposedDestination[MAX_GROUP_CARS];

static st_carDestination astCarDestination[MAX_GROUP_CARS];

static const st_carDestination stClearedCarDestination =
{
  .bDirty = 0,
  .eCallDir = DIR__NONE,
  .eDoor = DOOR_FRONT,
  .ucLanding = INVALID_LANDING,
  .uiMask = 0,
};
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Initialize car data
 -----------------------------------------------------------------------------*/
void Init_CarDestinationStructure()
{
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      ClrProposedDestination_ByCar(i);
      ClrCarDestination_ByCar(i);
      astCarDestination[i].bDirty = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void ClrCarDestination_ByCar(enum en_group_net_nodes eCarID)
{
   if( (astCarDestination[eCarID].eCallDir != stClearedCarDestination.eCallDir)
    || (astCarDestination[eCarID].eDoor != stClearedCarDestination.eDoor)
    || (astCarDestination[eCarID].ucLanding != stClearedCarDestination.ucLanding)
    || (astCarDestination[eCarID].uiMask != stClearedCarDestination.uiMask))
   {
      astCarDestination[eCarID].eCallDir = stClearedCarDestination.eCallDir;
      astCarDestination[eCarID].eDoor = stClearedCarDestination.eDoor;
      astCarDestination[eCarID].ucLanding = stClearedCarDestination.ucLanding;
      astCarDestination[eCarID].uiMask = stClearedCarDestination.uiMask;
      astCarDestination[eCarID].bDirty = 1;
   }
}
static void ClrProposedDestination_ByCar(enum en_group_net_nodes eCarID)
{
   memcpy(&astProposedDestination[eCarID], &stClearedCarDestination, sizeof(st_carDestination));
}
/*-----------------------------------------------------------------------------
   Access functions
 -----------------------------------------------------------------------------*/
st_carDestination * GetCarDestinationStructure(enum en_group_net_nodes eCarID)
{
   return &astCarDestination[eCarID];
}
uint8_t CarDestination_GetLanding(enum en_group_net_nodes eCarID)
{
   return astCarDestination[eCarID].ucLanding;
}
uint32_t CarDestination_GetMask(enum en_group_net_nodes eCarID)
{
   return astCarDestination[eCarID].uiMask;
}
enum en_doors CarDestination_GetDoor(enum en_group_net_nodes eCarID)
{
   return astCarDestination[eCarID].eDoor;
}
enum direction_enum CarDestination_GetDirection(enum en_group_net_nodes eCarID)
{
   return astCarDestination[eCarID].eCallDir;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define MASTER_DISPATCHER_MIN_RESEND_MS      (500)
uint8_t LoadDatagram_MasterDispatcher( un_sdata_datagram *punDatagram, struct st_module *pstThisModule )
{
   static enum en_group_net_nodes eLastCarID;
   static uint16_t auwMinResendCounter_ms[MAX_GROUP_CARS];
   uint8_t bSend = 0;
   if(GetMasterDispatcherFlag())
   {
      /* Maintain a minimum resend rate for destination packets */
      for( uint8_t i = 0; i < MAX_GROUP_CARS; i++ )
      {
         if( astCarDestination[i].bDirty )
         {
            auwMinResendCounter_ms[i] = 0;
         }
         else
         {
            uint32_t uiResendLimit = MASTER_DISPATCHER_MIN_RESEND_MS;
            if(CarData_GetFastResend())
            {
               uiResendLimit /= 2;
            }

            if(auwMinResendCounter_ms[i] >= uiResendLimit)
            {
               astCarDestination[i].bDirty = 1;
            }
            else
            {
               auwMinResendCounter_ms[i] += pstThisModule->uwRunPeriod_1ms;
            }
         }
      }

      /* Round robin through cars to send destination commands */
      for( uint8_t i = 0; i < MAX_GROUP_CARS; i++ )
      {
         enum en_group_net_nodes eCarID = (eLastCarID+i+1) % MAX_GROUP_CARS;
//         if( ( eCarID != GetFP_GroupCarIndex() )
//          && ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
//          && ( astCarDestination[eCarID].bDirty ) )
         // For diagnostics, the master car will send out its own destination.
         if( ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( astCarDestination[eCarID].bDirty ) )
         {
            punDatagram->auc8[0] = eCarID;
            punDatagram->auc8[1] = astCarDestination[eCarID].ucLanding;
            punDatagram->auc8[2] = astCarDestination[eCarID].eDoor;
            punDatagram->auc8[3] = astCarDestination[eCarID].eCallDir;
            punDatagram->aui32[1] = astCarDestination[eCarID].uiMask;

            eLastCarID = eCarID;
            astCarDestination[eCarID].bDirty = 0;
            auwMinResendCounter_ms[eCarID] = 0;
            bSend = 1;
            break;
         }
      }
   }
   else
   {
      punDatagram->aui32[0] = 0;
      punDatagram->aui32[1] = 0;
      bSend = 1;
   }
   return bSend;

}
/*-----------------------------------------------------------------------------
   punDatagram->auc8[0] = eCarID;
   punDatagram->auc8[1] = astCarDestination[eCarID].ucLanding;
   punDatagram->auc8[2] = astCarDestination[eCarID].eDoor;
   punDatagram->auc8[3] = astCarDestination[eCarID].eCallDir;
   punDatagram->aui32[1] = astCarDestination[eCarID].uiMask;
 -----------------------------------------------------------------------------*/
void UnloadDatagram_MasterDispatcher( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   /* If the source of the destination assignment has a lower car ID, then listen to it */
   enum en_group_net_nodes eCommandedCarID = punDatagram->auc8[0];

   if( !GetMasterDispatcherFlag() )
   {
      astCarDestination[eCommandedCarID].ucLanding = punDatagram->auc8[1];
      astCarDestination[eCommandedCarID].eDoor = punDatagram->auc8[2];
      astCarDestination[eCommandedCarID].eCallDir = punDatagram->auc8[3];
      astCarDestination[eCommandedCarID].uiMask = punDatagram->aui32[1];
   }
}
/*-----------------------------------------------------------------------------
todo revisit cost of checks
 -----------------------------------------------------------------------------*/
uint8_t GetLatchedHallCallUp_Front_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding )
{
   uint8_t bLatched = 0;
   uint8_t bValidOpening = CarData_CheckOpening( eCarID, ucLanding, DOOR_FRONT );
   uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__UP );
   if( uiLatchedCalls && bValidOpening )
   {
      // mask out calls already assigned
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         st_carDestination *pstCarDestination = GetCarDestinationStructure( i );
         if( ( i != eCarID )
          && ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( pstCarDestination->ucLanding == ucLanding )
          && ( pstCarDestination->eCallDir == DIR__UP )
          && ( pstCarDestination->eDoor != DOOR_REAR ) )
         {
            uiLatchedCalls &= ~(pstCarDestination->uiMask);
         }
      }
      // Mask out XREG calls already assigned
      uint8_t ucNumXRegCars = Param_ReadValue_8Bit(enPARAM8__NumXRegCars);
      for(uint8_t i = 0; i < ucNumXRegCars; i++)
      {
         if( ( XRegData_GetCarActive(i) )
          && ( XRegDestination_GetLanding(i) == ucLanding )
          && ( XRegDestination_GetDirection(i) == DIR__UP )
          && ( XRegDestination_GetDoor(i) != DOOR_REAR ) )
         {
            uiLatchedCalls &= ~(XRegDestination_GetMask(i));
         }
      }

      // Look only at calls this car can take
      uiLatchedCalls &= CarData_GetHallMask(eCarID, DOOR_FRONT);
      if( uiLatchedCalls
       && ( ( GetHallCallSecurity(eCarID, ucLanding, DOOR_FRONT, uiLatchedCalls) )
         || ( uiLatchedCalls & CarData_GetUniqueHallMask_Front(eCarID) ) ) ) /* Uniquely assigned hall calls exempt from security check */
      {
         bLatched = 1;
      }
   }
   return bLatched;
}
uint8_t GetLatchedHallCallUp_Rear_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding )
{
   uint8_t bLatched = 0;
   uint8_t bValidOpening = CarData_CheckOpening( eCarID, ucLanding, DOOR_REAR );
   uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__UP );
   if( uiLatchedCalls && bValidOpening )
   {
      // mask out calls already assigned
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         st_carDestination *pstCarDestination = GetCarDestinationStructure( i );
         if( ( i != eCarID )
          && ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( pstCarDestination->ucLanding == ucLanding )
          && ( pstCarDestination->eCallDir == DIR__UP )
          && ( pstCarDestination->eDoor != DOOR_FRONT ) )
         {
            uiLatchedCalls &= ~(pstCarDestination->uiMask);
         }
      }
      // Mask out XREG calls already assigned
      uint8_t ucNumXRegCars = Param_ReadValue_8Bit(enPARAM8__NumXRegCars);
      for(uint8_t i = 0; i < ucNumXRegCars; i++)
      {
         if( ( XRegData_GetCarActive(i) )
          && ( XRegDestination_GetLanding(i) == ucLanding )
          && ( XRegDestination_GetDirection(i) == DIR__UP )
          && ( XRegDestination_GetDoor(i) != DOOR_FRONT ) )
         {
            uiLatchedCalls &= ~(XRegDestination_GetMask(i));
         }
      }

      // Look only at calls this car can take
      uiLatchedCalls &= CarData_GetHallMask(eCarID, DOOR_REAR);
      if( uiLatchedCalls
       && ( ( GetHallCallSecurity(eCarID, ucLanding, DOOR_REAR, uiLatchedCalls) )
         || ( uiLatchedCalls & CarData_GetUniqueHallMask_Rear(eCarID) ) ) ) /* Uniquely assigned hall calls exempt from security check */
      {
         bLatched = 1;
      }
   }
   return bLatched;
}
uint8_t GetLatchedHallCallDown_Front_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding )
{
   uint8_t bLatched = 0;
   uint8_t bValidOpening = CarData_CheckOpening( eCarID, ucLanding, DOOR_FRONT );
   uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__DOWN );
   if( uiLatchedCalls && bValidOpening )
   {
      // mask out calls already assigned
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         st_carDestination *pstCarDestination = GetCarDestinationStructure( i );
         if( ( i != eCarID )
          && ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( pstCarDestination->ucLanding == ucLanding )
          && ( pstCarDestination->eCallDir == DIR__DN )
          && ( pstCarDestination->eDoor != DOOR_REAR ) )
         {
            uiLatchedCalls &= ~(pstCarDestination->uiMask);
         }
      }
      // Mask out XREG calls already assigned
      uint8_t ucNumXRegCars = Param_ReadValue_8Bit(enPARAM8__NumXRegCars);
      for(uint8_t i = 0; i < ucNumXRegCars; i++)
      {
         if( ( XRegData_GetCarActive(i) )
          && ( XRegDestination_GetLanding(i) == ucLanding )
          && ( XRegDestination_GetDirection(i) == DIR__DN )
          && ( XRegDestination_GetDoor(i) != DOOR_REAR ) )
         {
            uiLatchedCalls &= ~(XRegDestination_GetMask(i));
         }
      }

      // Look only at calls this car can take
      uiLatchedCalls &= CarData_GetHallMask(eCarID, DOOR_FRONT);
      if( uiLatchedCalls
       && ( ( GetHallCallSecurity(eCarID, ucLanding, DOOR_FRONT, uiLatchedCalls) )
         || ( uiLatchedCalls & CarData_GetUniqueHallMask_Front(eCarID) ) ) ) /* Uniquely assigned hall calls exempt from security check */
      {
         bLatched = 1;
      }
   }
   return bLatched;
}
uint8_t GetLatchedHallCallDown_Rear_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding )
{
   uint8_t bLatched = 0;
   uint8_t bValidOpening = CarData_CheckOpening( eCarID, ucLanding, DOOR_REAR );
   uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__DOWN );
   if( uiLatchedCalls && bValidOpening )
   {
      // mask out calls already assigned
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         st_carDestination *pstCarDestination = GetCarDestinationStructure( i );
         if( ( i != eCarID )
          && ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( pstCarDestination->ucLanding == ucLanding )
          && ( pstCarDestination->eCallDir == DIR__DN )
          && ( pstCarDestination->eDoor != DOOR_FRONT ) )
         {
            uiLatchedCalls &= ~(pstCarDestination->uiMask);
         }
      }
      // Mask out XREG calls already assigned
      uint8_t ucNumXRegCars = Param_ReadValue_8Bit(enPARAM8__NumXRegCars);
      for(uint8_t i = 0; i < ucNumXRegCars; i++)
      {
         if( ( XRegData_GetCarActive(i) )
          && ( XRegDestination_GetLanding(i) == ucLanding )
          && ( XRegDestination_GetDirection(i) == DIR__DN )
          && ( XRegDestination_GetDoor(i) != DOOR_FRONT ) )
         {
            uiLatchedCalls &= ~(XRegDestination_GetMask(i));
         }
      }

      // Look only at calls this car can take
      uiLatchedCalls &= CarData_GetHallMask(eCarID, DOOR_REAR);
      if( uiLatchedCalls
       && ( ( GetHallCallSecurity(eCarID, ucLanding, DOOR_REAR, uiLatchedCalls) )
         || ( uiLatchedCalls & CarData_GetUniqueHallMask_Rear(eCarID) ) ) ) /* Uniquely assigned hall calls exempt from security check */
      {
         bLatched = 1;
      }
   }
   return bLatched;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void SetGroupCarDestination( enum en_group_net_nodes eCarID, st_carDestination *pstDestination )
{
   /* Load new destination and mark for send if changed */
   if( ( astCarDestination[eCarID].eCallDir != pstDestination->eCallDir )
    || ( astCarDestination[eCarID].eDoor != pstDestination->eDoor )
    || ( astCarDestination[eCarID].uiMask != pstDestination->uiMask )
    || ( astCarDestination[eCarID].ucLanding != pstDestination->ucLanding ) )
   {
      astCarDestination[eCarID].bDirty = 1;
      astCarDestination[eCarID].eCallDir = pstDestination->eCallDir;
      astCarDestination[eCarID].eDoor = pstDestination->eDoor;
      astCarDestination[eCarID].uiMask = pstDestination->uiMask;
      astCarDestination[eCarID].ucLanding = pstDestination->ucLanding;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void GetNextDestinationAbove_ByCar( enum en_group_net_nodes eCarID, uint8_t ucStartLanding, st_carDestination * stDestination)
{
    st_cardata *pstCarData = GetCarDataStructure( eCarID );
    uint8_t ucTopLanding = pstCarData->ucLastLanding;
    uint8_t ucBottomLanding = pstCarData->ucFirstLanding;

    //quick sanity check
    if ( (ucTopLanding < MAX_NUM_FLOORS)
      && (ucStartLanding <= ucTopLanding) )
    {
        /* Scan for Up Hall Calls or Car Calls on the way to to the top terminal */
        for (int8_t fl = ucStartLanding; fl <= ucTopLanding; fl++) {
            //if hall call,  if floor is set, set direction and exit, else set floor and direction
            uint8_t bLatchedCall_F = GetLatchedHallCallUp_Front_ByCar(eCarID, fl);
            uint8_t bLatchedCall_R = GetLatchedHallCallUp_Rear_ByCar(eCarID, fl);
            uint8_t bUpCallAllowed = (uint8_t) fl != ucTopLanding; // Don't check up call at top terminal
            bUpCallAllowed &= ( GetDispatchMode_ByCar(eCarID) != DISPATCH_MODE__DOWN) && ( GetDispatchMode_ByCar(eCarID) != DISPATCH_MODE__LOBBY);
            uint8_t bFloorLockout = pstCarData->bSuppressReopen && ( fl == pstCarData->ucCurrentLanding );
            if ( bUpCallAllowed && !bFloorLockout ) {
               if( bLatchedCall_F || bLatchedCall_R )
               {
                  if (stDestination->ucLanding == INVALID_LANDING)
                  {
                      stDestination->ucLanding = (uint8_t) fl;
                      stDestination->eCallDir = DIR__UP;
                      if( bLatchedCall_F )
                      {
                         stDestination->eDoor = ( ( stDestination->eDoor == DOOR_REAR ) || ( stDestination->eDoor == DOOR_ANY ) )
                                              ? DOOR_ANY : DOOR_FRONT;
                      }
                      if( bLatchedCall_R )
                      {
                         stDestination->eDoor = ( ( stDestination->eDoor == DOOR_FRONT ) || ( stDestination->eDoor == DOOR_ANY ) )
                                              ? DOOR_ANY : DOOR_REAR;
                      }
                  }
                  else if( ( stDestination->eCallDir == DIR__NONE ) && ( stDestination->ucLanding <= (uint8_t) fl ) )
                  {
                     stDestination->eCallDir = DIR__UP;
                  }
                  break;
               }
            }

            //if car call, and floor is invalid, set floor, if floor is valid, then set direction and exit
            bLatchedCall_F = GetLatchedCarCall_Front_ByCar(eCarID, fl);
            bLatchedCall_R = GetLatchedCarCall_Rear_ByCar(eCarID, fl);
            if ( bLatchedCall_F || bLatchedCall_R ) {
               if (stDestination->ucLanding == INVALID_LANDING)
               {
                   stDestination->ucLanding = (uint8_t) fl;
                   if( bLatchedCall_F )
                   {
                      stDestination->eDoor = ( ( stDestination->eDoor == DOOR_REAR ) || ( stDestination->eDoor == DOOR_ANY ) )
                                           ? DOOR_ANY : DOOR_FRONT;
                   }
                   if( bLatchedCall_R )
                   {
                      stDestination->eDoor = ( ( stDestination->eDoor == DOOR_FRONT ) || ( stDestination->eDoor == DOOR_ANY ) )
                                           ? DOOR_ANY : DOOR_REAR;
                   }
               }
               else if ( stDestination->eCallDir == DIR__NONE )
               {
                  if( stDestination->ucLanding < (uint8_t) fl )
                  {
                     stDestination->eCallDir = DIR__UP;
                     break;
                  }
                  else if( stDestination->ucLanding > (uint8_t) fl )
                  {
                     stDestination->eCallDir = DIR__DN;
                     break;
                  }
               }
            }
        }

         //if only a car call is found in the direction, look for anything on the way back up to set direction.
         //if coming back up the floor is the same floor as the car call above, set direction to turn around.
         if ( ( stDestination->ucLanding == INVALID_LANDING ) || ( stDestination->eCallDir == DIR__NONE ) ) {
            for (int8_t fl = ucTopLanding; fl > ucStartLanding; fl--) {
               uint8_t bLatchedCall_F = GetLatchedHallCallDown_Front_ByCar(eCarID, fl);
               uint8_t bLatchedCall_R = GetLatchedHallCallDown_Rear_ByCar(eCarID, fl);
               uint8_t bDownCallAllowed = (uint8_t) fl != ucBottomLanding; // Don't check up call at bottom terminal
               bDownCallAllowed &= ( GetDispatchMode_ByCar(eCarID) != DISPATCH_MODE__UP) && ( GetDispatchMode_ByCar(eCarID) != DISPATCH_MODE__LOBBY);
               uint8_t bFloorLockout = pstCarData->bSuppressReopen && ( fl == pstCarData->ucCurrentLanding );
               if ( bDownCallAllowed && !bFloorLockout ) {
                    if( bLatchedCall_F || bLatchedCall_R )
                    {
                       /* No destination found in the priority direction, record the first down call encountered */
                       if (stDestination->ucLanding == INVALID_LANDING)
                       {
                           stDestination->ucLanding = (uint8_t) fl;
                           stDestination->eCallDir = DIR__DN;
                           if( bLatchedCall_F )
                           {
                              stDestination->eDoor = ( ( stDestination->eDoor == DOOR_REAR ) || ( stDestination->eDoor == DOOR_ANY ) )
                                                   ? DOOR_ANY : DOOR_FRONT;
                           }
                           if( bLatchedCall_R )
                           {
                              stDestination->eDoor = ( ( stDestination->eDoor == DOOR_FRONT ) || ( stDestination->eDoor == DOOR_ANY ) )
                                                   ? DOOR_ANY : DOOR_REAR;
                           }
                       }
                       /* First destination found is a CC, if next found is a HC above the CC, set the arrival direction */
                       else if (fl > stDestination->ucLanding)
                       {
                           stDestination->eCallDir = DIR__UP;
                       }
                       /* First destination found is a CC, if next found is a HC below or equal to the CC, set the arrival direction */
                       else if (fl < stDestination->ucLanding)
                       {
                           stDestination->eCallDir = DIR__DN;
                       }
                       else if (fl == stDestination->ucLanding)
                       {
                          stDestination->eCallDir = DIR__DN;
                          if( bLatchedCall_F )
                          {
                             stDestination->eDoor = ( ( stDestination->eDoor == DOOR_REAR ) || ( stDestination->eDoor == DOOR_ANY ) )
                                                  ? DOOR_ANY : DOOR_FRONT;
                          }
                          if( bLatchedCall_R )
                          {
                             stDestination->eDoor = ( ( stDestination->eDoor == DOOR_FRONT ) || ( stDestination->eDoor == DOOR_ANY ) )
                                                  ? DOOR_ANY : DOOR_REAR;
                          }
                       }
                       break;
                    }
                }
            }
        }
    }
}
static void GetNextDestinationBelow_ByCar(  enum en_group_net_nodes eCarID, uint8_t ucStartLanding, st_carDestination * stDestination)
{
    st_cardata *pstCarData = GetCarDataStructure( eCarID );
    uint8_t ucTopLanding = pstCarData->ucLastLanding;
    uint8_t ucBottomLanding = pstCarData->ucFirstLanding;

    //sanity check
    if (ucStartLanding < MAX_NUM_FLOORS
     && ucStartLanding <= ucTopLanding ){
          /* Scan for Up Hall Calls or Car Calls on the way to to the top terminal */
          for (int8_t fl = ucStartLanding; fl >= ucBottomLanding; fl--)
          {
            //if hall call,  if floor is set, set direction and exit, else set floor and direction
            uint8_t bLatchedCall_F = GetLatchedHallCallDown_Front_ByCar(eCarID, fl);
            uint8_t bLatchedCall_R = GetLatchedHallCallDown_Rear_ByCar(eCarID, fl);
            uint8_t bDownCallAllowed = (uint8_t) fl != ucBottomLanding; // Don't check up call at bottom terminal
            bDownCallAllowed &= ( GetDispatchMode_ByCar(eCarID) != DISPATCH_MODE__UP) && ( GetDispatchMode_ByCar(eCarID) != DISPATCH_MODE__LOBBY);
            uint8_t bFloorLockout = pstCarData->bSuppressReopen && ( fl == pstCarData->ucCurrentLanding );
            if ( bDownCallAllowed && !bFloorLockout ) {
               if( bLatchedCall_F || bLatchedCall_R )
               {
                  if (stDestination->ucLanding == INVALID_LANDING)
                  {
                      stDestination->ucLanding = (uint8_t) fl;
                      stDestination->eCallDir = DIR__DN;
                      if( bLatchedCall_F )
                      {
                         stDestination->eDoor = ( ( stDestination->eDoor == DOOR_REAR ) || ( stDestination->eDoor == DOOR_ANY ) )
                                              ? DOOR_ANY : DOOR_FRONT;
                      }
                      if( bLatchedCall_R )
                      {
                         stDestination->eDoor = ( ( stDestination->eDoor == DOOR_FRONT ) || ( stDestination->eDoor == DOOR_ANY ) )
                                              ? DOOR_ANY : DOOR_REAR;
                      }
                  }
                  else if( ( stDestination->eCallDir == DIR__NONE ) && ( stDestination->ucLanding >= (uint8_t) fl ) )
                  {
                      stDestination->eCallDir = DIR__DN;
                  }
                  break;
               }
            }

            //if car call, and floor is invalid, set floor, if floor is valid, then set direction and exit
            bLatchedCall_F = GetLatchedCarCall_Front_ByCar(eCarID, fl);
            bLatchedCall_R = GetLatchedCarCall_Rear_ByCar(eCarID, fl);
            if ( bLatchedCall_F || bLatchedCall_R ) {
                if (stDestination->ucLanding == INVALID_LANDING)
                {
                    stDestination->ucLanding = (uint8_t) fl;
                    if( bLatchedCall_F )
                    {
                       stDestination->eDoor = ( ( stDestination->eDoor == DOOR_REAR ) || ( stDestination->eDoor == DOOR_ANY ) )
                                            ? DOOR_ANY : DOOR_FRONT;
                    }
                    if( bLatchedCall_R )
                    {
                       stDestination->eDoor = ( ( stDestination->eDoor == DOOR_FRONT ) || ( stDestination->eDoor == DOOR_ANY ) )
                                            ? DOOR_ANY : DOOR_REAR;
                    }
                }
                else if ( stDestination->eCallDir == DIR__NONE )
                {
                   if( stDestination->ucLanding < (uint8_t) fl )
                   {
                      stDestination->eCallDir = DIR__UP;
                      break;
                   }
                   else if( stDestination->ucLanding > (uint8_t) fl )
                   {
                      stDestination->eCallDir = DIR__DN;
                      break;
                   }
                }
            }
        }

         //if only a car call is found in the direction, look for anything on the way back up to set direction.
         //if coming back up the floor is the same floor as the car call above, set direction to turn around.
         if ( ( stDestination->ucLanding == INVALID_LANDING ) || ( stDestination->eCallDir == DIR__NONE ) ) {
            for (int8_t fl = ucBottomLanding; fl < ucStartLanding; fl++) {
               uint8_t bLatchedCall_F = GetLatchedHallCallUp_Front_ByCar(eCarID, fl);
               uint8_t bLatchedCall_R = GetLatchedHallCallUp_Rear_ByCar(eCarID, fl);
               uint8_t bUpCallAllowed = (uint8_t) fl != ucTopLanding; // Don't check up call at top terminal
               bUpCallAllowed &= ( GetDispatchMode_ByCar(eCarID) != DISPATCH_MODE__DOWN) && ( GetDispatchMode_ByCar(eCarID) != DISPATCH_MODE__LOBBY);
               uint8_t bFloorLockout = pstCarData->bSuppressReopen && ( fl == pstCarData->ucCurrentLanding );
               if ( bUpCallAllowed && !bFloorLockout ) {
                    if( bLatchedCall_F || bLatchedCall_R )
                    {
                       /* No destination found in the priority direction, record the first up call encountered */
                       if (stDestination->ucLanding == INVALID_LANDING)
                       {
                           stDestination->ucLanding = (uint8_t) fl;
                           stDestination->eCallDir = DIR__UP;
                           if( bLatchedCall_F )
                           {
                              stDestination->eDoor = ( ( stDestination->eDoor == DOOR_REAR ) || ( stDestination->eDoor == DOOR_ANY ) )
                                                   ? DOOR_ANY : DOOR_FRONT;
                           }
                           if( bLatchedCall_R )
                           {
                              stDestination->eDoor = ( ( stDestination->eDoor == DOOR_FRONT ) || ( stDestination->eDoor == DOOR_ANY ) )
                                                   ? DOOR_ANY : DOOR_REAR;
                           }
                       }
                       /* First destination found is a CC, if next found is a HC below the CC, set the arrival direction */
                       else if (fl < stDestination->ucLanding)
                       {
                           stDestination->eCallDir = DIR__DN;
                       }
                       /* First destination found is a CC, if next found is a HC above or equal to the CC, set the arrival direction */
                       else if (fl > stDestination->ucLanding)
                       {
                           stDestination->eCallDir = DIR__UP;
                       }
                       else if (fl == stDestination->ucLanding)
                       {
                          stDestination->eCallDir = DIR__UP;
                          if( bLatchedCall_F )
                          {
                             stDestination->eDoor = ( ( stDestination->eDoor == DOOR_REAR ) || ( stDestination->eDoor == DOOR_ANY ) )
                                                  ? DOOR_ANY : DOOR_FRONT;
                          }
                          if( bLatchedCall_R )
                          {
                             stDestination->eDoor = ( ( stDestination->eDoor == DOOR_FRONT ) || ( stDestination->eDoor == DOOR_ANY ) )
                                                  ? DOOR_ANY : DOOR_REAR;
                          }
                       }
                       break;
                    }
                }
            }
        }
    }
}

/*----------------------------------------------------------------------------
    Update per car destination on:
     - Hall call change
     - Car call change
     - Car destination change
 *----------------------------------------------------------------------------*/
void UpdateDestination_ByCar( enum en_group_net_nodes eCarID )
{
   // If car is online & in correct mode.
   st_carDestination stDestination;
   stDestination.eCallDir = DIR__NONE;
   stDestination.ucLanding = INVALID_LANDING;
   stDestination.eDoor = NUM_OF_DOORS;
   stDestination.uiMask = 0;

   st_cardata *pstCarData = GetCarDataStructure( eCarID );

   uint8_t bUpLamp = GetOutputValue_ByCar(eCarID, enOUT_ARV_UP_F) || GetOutputValue_ByCar(eCarID, enOUT_ARV_UP_R);
   uint8_t bDownLamp = GetOutputValue_ByCar(eCarID, enOUT_ARV_DN_F) || GetOutputValue_ByCar(eCarID, enOUT_ARV_DN_R);

   // If moving UP
   if( pstCarData->cMotionDir == DIR__UP )
   {
      //if destination floor is closer than the 'reachable' floor reassign reachable floor to destination
      uint8_t ucReachableLanding = pstCarData->ucReachableLanding;
      if( pstCarData->ucDestinationLanding < pstCarData->ucReachableLanding )
      {
         ucReachableLanding = pstCarData->ucDestinationLanding;
      }

      /* Don't allow checking of destinations above after already preparing
       * for a destination change. Otherwise car may try to move to a
       * destination opposite its indicated arrival direction */
      if( !( ( pstCarData->enPriority == HC_DIR__DOWN ) && ( pstCarData->ucCurrentLanding == pstCarData->ucDestinationLanding ) )
       && !bDownLamp )
      {
         GetNextDestinationAbove_ByCar( eCarID, ucReachableLanding, &stDestination);
      }

      if( !bUpLamp )
      {
         if ( ( stDestination.ucLanding == INVALID_LANDING ) || ( stDestination.eCallDir == DIR__NONE ) ) {
            GetNextDestinationBelow_ByCar( eCarID, ucReachableLanding, &stDestination);
         }
      }
   }
   // If moving DOWN
   else if( pstCarData->cMotionDir == DIR__DN )
   {
      //if destination floor is closer than the 'reachable' floor reassign reachable floor to destination
      uint8_t ucReachableLanding = pstCarData->ucReachableLanding;
      if( pstCarData->ucDestinationLanding > pstCarData->ucReachableLanding )
      {
         ucReachableLanding = pstCarData->ucDestinationLanding;
      }

      /* Don't allow checking of destinations below after already preparing
       * for a destination change. Otherwise car may try to move to a
       * destination opposite its indicated arrival direction */
      if( !( ( pstCarData->enPriority == HC_DIR__UP ) && ( pstCarData->ucCurrentLanding == pstCarData->ucDestinationLanding ) )
       && !bUpLamp )
      {
         GetNextDestinationBelow_ByCar( eCarID, ucReachableLanding, &stDestination);
      }

      if( !bDownLamp )
      {
         if ( ( stDestination.ucLanding == INVALID_LANDING ) || ( stDestination.eCallDir == DIR__NONE ) ) {
            GetNextDestinationAbove_ByCar( eCarID, ucReachableLanding, &stDestination);
         }
      }
   }
   // If stopped, and serving UP calls
   else if( pstCarData->enPriority == HC_DIR__UP )
   {
      /* Suppress above destination check if arrival down lamps already set. */
      if( !bDownLamp )
      {
         GetNextDestinationAbove_ByCar( eCarID, pstCarData->ucCurrentLanding, &stDestination);
      }

      if( !bUpLamp && pstCarData->bIdleDirection )
      {
         if ( ( stDestination.ucLanding == INVALID_LANDING ) || ( stDestination.eCallDir == DIR__NONE ) ) {
            GetNextDestinationBelow_ByCar( eCarID, pstCarData->ucCurrentLanding, &stDestination);
         }
      }
   }
   // If stopped, and serving DOWN calls
   else
   {
      /* Suppress below destination check if arrival up lamps already set. */
      if( !bUpLamp )
      {
         GetNextDestinationBelow_ByCar( eCarID, pstCarData->ucCurrentLanding, &stDestination);
      }

      if( !bDownLamp && pstCarData->bIdleDirection )
      {
         if ( ( stDestination.ucLanding == INVALID_LANDING ) || ( stDestination.eCallDir == DIR__NONE ) ) {
            GetNextDestinationAbove_ByCar( eCarID, pstCarData->ucCurrentLanding, &stDestination);
         }
      }
   }

   /* If a valid destination was found and a next destination direction was found,
    * set the destination mask to reserve hall calls in that destination. */
   if( stDestination.eCallDir != DIR__NONE )
   {
      /* Set mask based on destination door */
      if( stDestination.eDoor == DOOR_FRONT )
      {
         stDestination.uiMask = pstCarData->uiHallMask_F;
      }
      else if( stDestination.eDoor == DOOR_REAR )
      {
         stDestination.uiMask = pstCarData->uiHallMask_R;
      }
      /* Here DOOR_ANY indicates both doors will open */
      else if( stDestination.eDoor == DOOR_ANY )
      {
         stDestination.uiMask = pstCarData->uiHallMask_F | pstCarData->uiHallMask_R;
      }
   }

   SetGroupCarDestination(eCarID, &stDestination);
}

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UpdateProposedDestination_ByCar( enum en_group_net_nodes eCarID )
{
   // If car is online & in correct mode.
   st_carDestination stDestination;
   stDestination.eCallDir = DIR__NONE;
   stDestination.ucLanding = INVALID_LANDING;
   stDestination.eDoor = NUM_OF_DOORS;
   stDestination.uiMask = 0;
   stDestination.bDirty = 0;

   st_cardata *pstCarData = GetCarDataStructure( eCarID );

   uint8_t bUpLamp = GetOutputValue_ByCar(eCarID, enOUT_ARV_UP_F) || GetOutputValue_ByCar(eCarID, enOUT_ARV_UP_R);
   uint8_t bDownLamp = GetOutputValue_ByCar(eCarID, enOUT_ARV_DN_F) || GetOutputValue_ByCar(eCarID, enOUT_ARV_DN_R);

   // If moving UP
   if( pstCarData->cMotionDir == DIR__UP )
   {
      //if destination floor is closer than the 'reachable' floor reassign reachable floor to destination
      uint8_t ucReachableLanding = pstCarData->ucReachableLanding;
      if( pstCarData->ucDestinationLanding < pstCarData->ucReachableLanding )
      {
         ucReachableLanding = pstCarData->ucDestinationLanding;
      }

      /* Don't allow checking of destinations above after already preparing
       * for a destination change. Otherwise car may try to move to a
       * destination opposite its indicated arrival direction */
      if( !( ( pstCarData->enPriority == HC_DIR__DOWN ) && ( pstCarData->ucCurrentLanding == pstCarData->ucDestinationLanding ) )
       && !bDownLamp )
      {
         GetNextDestinationAbove_ByCar( eCarID, ucReachableLanding, &stDestination);
      }

      if( !bUpLamp )
      {
         if ( ( stDestination.ucLanding == INVALID_LANDING ) || ( stDestination.eCallDir == DIR__NONE ) ) {
            GetNextDestinationBelow_ByCar( eCarID, ucReachableLanding, &stDestination);
         }
      }
   }
   // If moving DOWN
   else if( pstCarData->cMotionDir == DIR__DN )
   {
      //if destination floor is closer than the 'reachable' floor reassign reachable floor to destination
      uint8_t ucReachableLanding = pstCarData->ucReachableLanding;
      if( pstCarData->ucDestinationLanding > pstCarData->ucReachableLanding )
      {
         ucReachableLanding = pstCarData->ucDestinationLanding;
      }

      /* Don't allow checking of destinations below after already preparing
       * for a destination change. Otherwise car may try to move to a
       * destination opposite its indicated arrival direction */
      if( !( ( pstCarData->enPriority == HC_DIR__UP ) && ( pstCarData->ucCurrentLanding == pstCarData->ucDestinationLanding ) )
       && !bUpLamp )
      {
         GetNextDestinationBelow_ByCar( eCarID, ucReachableLanding, &stDestination);
      }

      if( !bDownLamp )
      {
         if ( ( stDestination.ucLanding == INVALID_LANDING ) || ( stDestination.eCallDir == DIR__NONE ) ) {
            GetNextDestinationAbove_ByCar( eCarID, ucReachableLanding, &stDestination);
         }
      }
   }
   // If stopped, and serving UP calls
   else if( pstCarData->enPriority == HC_DIR__UP )
   {
      /* Suppress above destination check if arrival down lamps already set. */
      if( !bDownLamp )
      {
         GetNextDestinationAbove_ByCar( eCarID, pstCarData->ucCurrentLanding, &stDestination);
      }

      if( !bUpLamp && pstCarData->bIdleDirection )
      {
         if ( ( stDestination.ucLanding == INVALID_LANDING ) || ( stDestination.eCallDir == DIR__NONE ) ) {
            GetNextDestinationBelow_ByCar( eCarID, pstCarData->ucCurrentLanding, &stDestination);
         }
      }
   }
   // If stopped, and serving DOWN calls
   else
   {
      /* Suppress below destination check if arrival up lamps already set. */
      if( !bUpLamp )
      {
         GetNextDestinationBelow_ByCar( eCarID, pstCarData->ucCurrentLanding, &stDestination);
      }

      if( !bDownLamp && pstCarData->bIdleDirection )
      {
         if ( ( stDestination.ucLanding == INVALID_LANDING ) || ( stDestination.eCallDir == DIR__NONE ) ) {
            GetNextDestinationAbove_ByCar( eCarID, pstCarData->ucCurrentLanding, &stDestination);
         }
      }
   }

   /* If a valid destination was found and a next destination direction was found,
    * set the destination mask to reserve hall calls in that destination. */
   if( stDestination.eCallDir != DIR__NONE )
   {
      /* Set mask based on destination door */
      if( stDestination.eDoor == DOOR_FRONT )
      {
         stDestination.uiMask = pstCarData->uiHallMask_F;
      }
      else if( stDestination.eDoor == DOOR_REAR )
      {
         stDestination.uiMask = pstCarData->uiHallMask_R;
      }
      /* Here DOOR_ANY indicates both doors will open */
      else if( stDestination.eDoor == DOOR_ANY )
      {
         stDestination.uiMask = pstCarData->uiHallMask_F | pstCarData->uiHallMask_R;
      }
   }

   /* Load proposed destination to data structure for comparison */
   memcpy(&astProposedDestination[eCarID], &stDestination, sizeof(st_carDestination));
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static uint8_t GetLandingsAway( enum en_group_net_nodes eCarID, uint8_t ucDestLanding )
{
   uint8_t ucLandingsAway = INVALID_FLOOR;
   st_cardata *pstCarData = GetCarDataStructure(eCarID);
   uint8_t ucCurrentLanding = pstCarData->ucCurrentLanding;

   if(ucDestLanding > ucCurrentLanding)
   {
      ucLandingsAway = ucDestLanding - ucCurrentLanding;
   }
   else
   {
      ucLandingsAway = ucCurrentLanding - ucDestLanding;
   }
   return ucLandingsAway;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static uint8_t GenerateAndValidateProposedDestinations()
{
   uint8_t bNoNewProposals = 1;
   /* Generate a proposed destination for cars with invalid destinations */
   for( uint8_t i = 0; i < MAX_GROUP_CARS; i++ )
   {
      if( ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
       && ( astProposedDestination[i].ucLanding >= MAX_NUM_FLOORS ) )
      {
         UpdateProposedDestination_ByCar(i);
         if( astProposedDestination[i].ucLanding < MAX_NUM_FLOORS )
         {
            bNoNewProposals = 0;
         }
      }
   }

   /* Asses proposed destinations, confirm unique destinations, determine closest of duplicates */
   for( uint8_t ucCar1 = 0; ucCar1 < MAX_GROUP_CARS; ucCar1++ )
   {
      uint8_t ucClosestCar_Plus1 = ucCar1+1;
      for( uint8_t ucCar2 = 0; ucCar2 < MAX_GROUP_CARS; ucCar2++ )
      {
         uint8_t ucDest1 = astProposedDestination[ucCar1].ucLanding;
         uint8_t ucDest2 = astProposedDestination[ucCar2].ucLanding;
         enum direction_enum eCallDir1 = astProposedDestination[ucCar1].eCallDir;
         enum direction_enum eCallDir2 = astProposedDestination[ucCar2].eCallDir;
         /* Check if destination is valid & two cars have same destination */
         if( ( ucCar1 != ucCar2 )
          && ( GetCarOfflineTimer_ByCar(ucCar1) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( GetCarOfflineTimer_ByCar(ucCar2) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( ucDest1 < MAX_NUM_FLOORS )
          && ( ucDest1 == ucDest2 )
          && ( eCallDir1 != DIR__NONE ) // Ignore CCs.
          && ( eCallDir1 == eCallDir2 ) )
         {
            uint8_t bClearCar1 = 0; // When set to 1, clears the proposed destination assigned to ucCar1 and moves on to the next car
            st_cardata *pstCarData1 = GetCarDataStructure(ucCar1);
            st_cardata *pstCarData2 = GetCarDataStructure(ucCar2);
            uint8_t ucDiff1 = GetLandingsAway(ucCar1, ucDest1);
            uint8_t ucDiff2 = GetLandingsAway(ucCar2, ucDest2);
            uint8_t bConflictingArrivalLamps1 =  ( ( eCallDir1 == DIR__UP ) && ( GetOutputValue_ByCar(ucCar1, enOUT_ARV_DN_F) || GetOutputValue_ByCar(ucCar1, enOUT_ARV_DN_R) ) )
                                              || ( ( eCallDir1 == DIR__DN ) && ( GetOutputValue_ByCar(ucCar1, enOUT_ARV_UP_F) || GetOutputValue_ByCar(ucCar1, enOUT_ARV_UP_R) ) );
//            uint8_t bConflictingArrivalLamps2 =  ( ( eCallDir2 == DIR__UP ) && ( GetOutputValue_ByCar(ucCar2, enOUT_ARV_DN_F) || GetOutputValue_ByCar(ucCar2, enOUT_ARV_DN_R) ) )
//                                              || ( ( eCallDir2 == DIR__DN ) && ( GetOutputValue_ByCar(ucCar2, enOUT_ARV_UP_F) || GetOutputValue_ByCar(ucCar2, enOUT_ARV_UP_R) ) );
            uint8_t bCorrectDirection1 = ( ( pstCarData1->enPriority == HC_DIR__UP )   && ( eCallDir1 == DIR__UP ) )
                                      || ( ( pstCarData1->enPriority == HC_DIR__DOWN ) && ( eCallDir1 == DIR__DN ) );
            uint8_t bCorrectDirection2 = ( ( pstCarData2->enPriority == HC_DIR__UP )   && ( eCallDir2 == DIR__UP ) )
                                      || ( ( pstCarData2->enPriority == HC_DIR__DOWN ) && ( eCallDir2 == DIR__DN ) );
//            enum en_doors eDoor1 = astProposedDestination[ucCar1].eDoor;
            enum en_doors eDoor2 = astProposedDestination[ucCar2].eDoor;
//            uint8_t bDoorAlreadyOpen1 = ( ( eDoor1 == DOOR_FRONT ) && CarData_GetDoorOpen_Front(ucCar1) )
//                                     || ( ( eDoor1 == DOOR_REAR ) && CarData_GetDoorOpen_Rear(ucCar1) )
//                                     || ( ( eDoor1 == DOOR_ANY ) && CarData_GetDoorOpen_Front(ucCar1) && CarData_GetDoorOpen_Rear(ucCar1) );
            uint8_t bDoorAlreadyOpen2 = ( ( eDoor2 == DOOR_FRONT ) && CarData_GetDoorOpen_Front(ucCar2) )
                                        || ( ( eDoor2 == DOOR_REAR ) && CarData_GetDoorOpen_Rear(ucCar2) )
                                        || ( ( eDoor2 == DOOR_ANY ) && CarData_GetDoorOpen_Front(ucCar2) && CarData_GetDoorOpen_Rear(ucCar2) );
            /* If car 1 has the incorrect direction
             * and either
             *    it is not able to currently switch directions,
             *    or it is signaling a conflicting arrival direction */

            /* If car 1 is at the destination floor
             * but can't take the call because it has already signaled a direction change,
             * or has incorrect direction and is not idle
             * then clear car 1. */
            if(0){}
//            if( ( !ucDiff1 )
//             && ( bConflictingArrivalLamps1
//             || ( !pstCarData1->cMotionDir && !bCorrectDirection1 && !pstCarData1->bIdleDirection ) ) )
//            {
//               bClearCar1 = 1;
//            }
            /* If both cars are the same distance... */
            else if( ucDiff1 == ucDiff2 )
            {
               /* If both cars are at the destination floor,
                * and car2 has the correct direction,
                * and car2 has the correct doors open
                * then clear car 1 */
               if( ( !ucDiff1 )
                && ( bCorrectDirection2 )
                && ( bDoorAlreadyOpen2 ) )
               {
                  bClearCar1 = 1;
               }
               /* If both cars are at the destination floor,
                * and car 2 has the correct direction, but
                * car 1 does not, clear car 1 */
               else if( ( !ucDiff1 )
                     && ( bCorrectDirection2 )
                     && ( !bCorrectDirection1 ) )
               {
                  bClearCar1 = 1;
               }
               /* If both cars are at the destination floor,
                * and both cars have incorrect direction,
                * and either
                *    car 2 is idle and car1 is not,
                *    or car 1 had opposing direction arrival lanterns
                * then clear car 1 */
               else if( ( !ucDiff1 )
                     && ( !bCorrectDirection1 && !bCorrectDirection2 )
                     && ( ( pstCarData2->bIdleDirection && !pstCarData1->bIdleDirection )
                       || ( bConflictingArrivalLamps1 ) ) )
               {
                  bClearCar1 = 1;
               }
               /*If Car 2 is in a dispatch mode and Car 1 is not, then clear Car 1*/
               else if( (pstCarData2->enDispatchMode != DISPATCH_MODE__NONE ) &&
                        (pstCarData1->enDispatchMode == DISPATCH_MODE__NONE) )
               {
                  bClearCar1 = 1;
               }
               /* Otherwise if car 1 is a higher index car, clear car 1. */
               else if( ucCar1 > ucCar2 )
               {
                  bClearCar1 = 1;
               }
            }
            /* If car 1 is further away than car 2,
             * clear car 1. */
            else if( ucDiff1 > ucDiff2 )
            {
               /* Unless car 2 is idle or heading in the correct direction
                * and car1 has an active conflicting direction */
//               if( ! ( ( !ucDiff2 )
//                    && ( bConflictingArrivalLamps2
//                    || ( !pstCarData2->cMotionDir && !bCorrectDirection2 && !pstCarData2->bIdleDirection ) ) ) )
//               {
                  bClearCar1 = 1;
//               }
            }

            if( bClearCar1 )
            {
               ClrProposedDestination_ByCar(ucCar1);
               ucClosestCar_Plus1 = 0;
               break;
            }
         }
      }
      if( ( ucClosestCar_Plus1 ) && ( astProposedDestination[ucClosestCar_Plus1-1].ucLanding < MAX_NUM_FLOORS ) )
      {
         SetGroupCarDestination(ucClosestCar_Plus1-1, &astProposedDestination[ucClosestCar_Plus1-1]);
      }
   }
   return bNoNewProposals;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void UpdateGroupCarDestinations()
{
   /* Clear current destination assignments for each car */
   for( uint8_t i = 0; i < MAX_GROUP_CARS; i++ )
   {
      ClrProposedDestination_ByCar(i);
      ClrCarDestination_ByCar(i);
   }
   /* Generate a proposed destinations for each car
    * and compare ones that conflict. The closer one is locked in and the other is cleared. */
   for( uint8_t i = 0; i < MAX_GROUP_CARS; i++ )
   {
      if(GenerateAndValidateProposedDestinations())
      {
         break;
      }
   }
   for( uint8_t i = 0; i < MAX_GROUP_CARS; i++ )
   {
      if( ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
       && ( astProposedDestination[i].ucLanding >= MAX_NUM_FLOORS ) )
      {
         UpdateDestination_ByCar(i);
      }
   }
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
