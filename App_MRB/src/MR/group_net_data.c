/***
 *       _____                          __         _
 *      / ___/ ____ ___   ____ _ _____ / /_ _____ (_)_____ ___
 *      \__ \ / __ `__ \ / __ `// ___// __// ___// // ___// _ \
 *     ___/ // / / / / // /_/ // /   / /_ / /   / /(__  )/  __/
 *    /____//_/ /_/ /_/ \__,_//_/    \__//_/   /_//____/ \___/
 *        ______               _                           _
 *       / ____/____   ____ _ (_)____   ___   ___   _____ (_)____   ____ _
 *      / __/  / __ \ / __ `// // __ \ / _ \ / _ \ / ___// // __ \ / __ `/
 *     / /___ / / / // /_/ // // / / //  __//  __// /   / // / / // /_/ /
 *    /_____//_/ /_/ \__, //_//_/ /_/ \___/ \___//_/   /_//_/ /_/ \__, /
 *                  /____/                                       /____/
 */
/******************************************************************************
 *
 * @file
 * @brief   Functions for loading and unloading data on the groupnet
 * @version
 * @date
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "group_net_data.h"

#include <stdint.h>


// Used to import the parameter information
#include "sys.h"

#include "GlobalData.h"
#include "hallData.h"
#include "operation.h"
#include "motion.h"
#include "fault_app.h"
#include "carData.h"
#include "hallData.h"
#include "carDestination.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

// TODO: Look at the old group module and validate these timings. Scale them so the match the old values
// Also append the timing figure to the end of each of them
#define MIN_EMS_CALL_HOLD_500MS  (4)



/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

// Swing operation variable, 1 if a swing call is latched.
// Default is to assume no swing call is latched.
static uint8_t bCurrentSwingCall = 0;

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
   Swing Calls
 -----------------------------------------------------------------------------*/
void UpdateSwingCall()
{
   ClearSwingCall();
   st_cardata *pstCarData = GetCarDataStructure( GetFP_GroupCarIndex() );
   /* If swing calls should activate Auto_Swing, then flag swing calls to pass to MRA */
   if( pstCarData->bVIP && pstCarData->ucbClosestCar )
   {
      uint8_t ucFirstLanding = pstCarData->ucFirstLanding;
      uint8_t ucLastLanding = pstCarData->ucLastLanding;
      for( uint8_t ucLanding = ucFirstLanding; ucLanding <= ucLastLanding; ucLanding++ )
      {
         uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__UP )
                                 | GetRawLatchedHallCalls( ucLanding, HC_DIR__DOWN );
         uint8_t bValidCall = (uiLatchedCalls & Param_ReadValue_8Bit ( enPARAM8__SwingCallMask)) ? 1:0;
         if( bValidCall )
         {
            SetSwingCall();
            break;
         }
      }
   }
   else if( !pstCarData->bVIP && Param_ReadValue_1Bit( enPARAM1__Swing_CallsEnable ) )
   {
      uint8_t ucFirstLanding = pstCarData->ucFirstLanding;
      uint8_t ucLastLanding = pstCarData->ucLastLanding;
      for( uint8_t ucLanding = ucFirstLanding; ucLanding <= ucLastLanding; ucLanding++ )
      {
         uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__UP )
                                 | GetRawLatchedHallCalls( ucLanding, HC_DIR__DOWN );
         uint8_t bValidCall = (uiLatchedCalls & Param_ReadValue_8Bit ( enPARAM8__SwingCallMask)) ? 1:0;
         if( bValidCall )
         {
            SetSwingCall();
            break;
         }
      }
   }
}
uint8_t GetAnySwingCallActive( void )
{
    return bCurrentSwingCall;
}

void SetSwingCall( void )
{
    bCurrentSwingCall = 1;
}

void ClearSwingCall( void )
{
    bCurrentSwingCall = 0;
}

/*----------------------------------------------------------------------------
   Takes car's local floor index and converts it into the group-wide landing index
 *----------------------------------------------------------------------------*/
uint8_t GetGroupMapping ( uint8_t ucFloorIndex )
{
   uint8_t ucLandingIndex = ucFloorIndex + Param_ReadValue_8Bit(enPARAM8__GroupLandingOffset);
   if(ucLandingIndex >= MAX_NUM_FLOORS)
   {
      ucLandingIndex = INVALID_FLOOR;
   }

   return ucLandingIndex;
}
/*-----------------------------------------------------------------------------
   Takes group floor index and returns corresponding car floor index
 -----------------------------------------------------------------------------*/
uint8_t GetCarMapping ( uint8_t ucLandingIndex )
{
    uint8_t ucFloorIndex = INVALID_FLOOR;
    if(ucLandingIndex >= Param_ReadValue_8Bit(enPARAM8__GroupLandingOffset))
    {
       ucFloorIndex = ucLandingIndex - Param_ReadValue_8Bit(enPARAM8__GroupLandingOffset);
    }

    return ucFloorIndex;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
