/******************************************************************************
 *
 * @file     remoteCommands.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     13 August 2019
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include "mod.h"
#include "remoteCommands.h"
#include "carData.h"
#include "GlobalData.h"
#include "emergency_power.h"
#include "dynamic_parking.h"
#include <stdint.h>
#include <string.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   VIRTUAL_CARCALL_SECURITY,
   VIRTUAL_HALLCALL_SECURITY,
   VIRTUAL_SYSTEM_INPUTS,

   NUM_VIRTUAL_INPUTS_TYPES,
} en_virtual_input_types;

typedef enum
{
   REMOTE_DISPATCH_MODE,
   REMOTE_CARCALL_SECURITY_F0,
   REMOTE_CARCALL_SECURITY_F1,
   REMOTE_CARCALL_SECURITY_F2,

   REMOTE_CARCALL_SECURITY_R0,
   REMOTE_CARCALL_SECURITY_R1,
   REMOTE_CARCALL_SECURITY_R2,

   REMOTE_HALLCALL_SECURITY_F0,
   REMOTE_HALLCALL_SECURITY_F1,
   REMOTE_HALLCALL_SECURITY_F2,

   REMOTE_HALLCALL_SECURITY_R0,
   REMOTE_HALLCALL_SECURITY_R1,
   REMOTE_HALLCALL_SECURITY_R2,

   REMOTE_SYSTEM_INPUTS,

   NUM_REMOTE_DATA
} en_remote_data;
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static en_dispatch_mode enActiveDispatchCommand = DISPATCH_MODE__NONE;

static uint32_t auiBF_Virtual_CarCallSecurity[ NUM_OF_DOORS ][BITMAP32_SIZE(MAX_NUM_FLOORS)];
static uint32_t auiBF_Virtual_HallCallSecurity[ NUM_OF_DOORS ][BITMAP32_SIZE(MAX_NUM_FLOORS)];
static uint32_t uiBF_Virtual_SysInputs;
/*
    static uint32_t auiBF_Virtual_SysInputs[ BITMAP32_SIZE ( NUM_VIRTUAL_INPUTS ) ];
    Use if number of virtual system inputs becomes greater than 32
*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 * Sets virtual car call security enable bitmaps
 *----------------------------------------------------------------------------*/
void SetVirtualCarCallSecurity(enum en_doors enDoorIndex, uint8_t ucMapIndex, uint32_t uiValue)
{
   if((enDoorIndex < NUM_OF_DOORS) && (ucMapIndex < BITMAP32_SIZE(MAX_NUM_FLOORS)))
   {
      auiBF_Virtual_CarCallSecurity[enDoorIndex][ucMapIndex] = uiValue;
   }
}

uint32_t GetVirtualCarCallSecurity(enum en_doors enDoorIndex, uint8_t ucMapIndex)
{
   return auiBF_Virtual_CarCallSecurity[enDoorIndex][ucMapIndex];
}
/*----------------------------------------------------------------------------
 * Sets virtual hall call security enable bitmaps
 *----------------------------------------------------------------------------*/
void SetVirtualHallCallSecurity(enum en_doors enDoorIndex, uint8_t ucMapIndex, uint32_t uiValue)
{
   if((enDoorIndex < NUM_OF_DOORS) && (ucMapIndex < BITMAP32_SIZE(MAX_NUM_FLOORS)))
   {
      auiBF_Virtual_HallCallSecurity[enDoorIndex][ucMapIndex] = uiValue;
   }
}
/*----------------------------------------------------------------------------
 * Sets virtual system inputs bitmap. Modify if virtual sys inputs
 * grow past 32
 *----------------------------------------------------------------------------*/
void SetVirtualSystemInputs(uint32_t uiValue)
{
   uiBF_Virtual_SysInputs = uiValue;
}
/*----------------------------------------------------------------------------
 * Sets remote dispatch mode based on MRM command. Remote command will be OR
 * with the physical dispatch enables.
 *----------------------------------------------------------------------------*/
void RemoteCommand_SetDispatchMode(en_dispatch_mode enDispatchMode)
{
   if(enDispatchMode < NUM_DISPATCH_MODES)
   {
      enActiveDispatchCommand = enDispatchMode;
   }
}
/*----------------------------------------------------------------------------
 * Sets dynamic/predictive parking floor 
 *----------------------------------------------------------------------------*/
void RemoteCommand_SetParkingFloor(uint8_t ucFloor)
{

}
/*----------------------------------------------------------------------------
 * Updates parameter when a group parameter value changes on another car
 *----------------------------------------------------------------------------*/
void RemoteCommand_SetGroupParameter(enum en_param_block_types eParamType, uint16_t uwParamIndex, uint32_t uiParamValue)
{

}

/*----------------------------------------------------------------------------
 * Sets virtual input bitmap
 *----------------------------------------------------------------------------*/
void RemoteCommand_SetVirtualInputs( un_sdata_datagram *punDatagram)
{
   en_virtual_input_types enVirtualInputType = punDatagram->auc8[1];

   enum en_doors enDoorIndex = punDatagram->auc8[2];
   uint8_t ucMapIndex = punDatagram->auc8[3];
   uint32_t uiBF_Value = punDatagram->aui32[1];

   switch(enVirtualInputType)
   {
      case VIRTUAL_CARCALL_SECURITY:
         SetVirtualCarCallSecurity(enDoorIndex, ucMapIndex, uiBF_Value);
         break;
      case VIRTUAL_HALLCALL_SECURITY:
         SetVirtualHallCallSecurity(enDoorIndex, ucMapIndex, uiBF_Value);
         break;
      case VIRTUAL_SYSTEM_INPUTS:
         // Modify if virtual system inputs grow past 32
         SetVirtualSystemInputs(uiBF_Value);
         break;
      default:
         break;
   }
}
/*----------------------------------------------------------------------------
 * Returns current remote dispatch mode command
 *----------------------------------------------------------------------------*/
en_dispatch_mode RemoteCommand_GetDispatchMode(void)
{
   return enActiveDispatchCommand;
}

#define REMOTE_DATA_DEFAULT_RESEND_TIMER_5MS          (500)
#define REMOTE_DATA_INVALID_RESEND_TIMER_5MS          (0xFFFF)
/*----------------------------------------------------------------------------
 * Loads DG_MRB__RemoteCommands. Round robins through type of virtual inputs.
 *----------------------------------------------------------------------------*/
uint8_t LoadDatagram_RemoteCommands(un_sdata_datagram* punDatagram)
{
   uint8_t bSend = 0;

   static uint32_t auiLastData[NUM_REMOTE_DATA];
   static uint16_t auwResendTimer_5ms[NUM_REMOTE_DATA];
   static uint8_t aucDirtyBits[BITMAP8_SIZE(NUM_REMOTE_DATA)];
   static en_remote_data enLastPacket;

   /* Flag packets for resend if they have changed.
    * Update minimum resend timers. */
   uint32_t uiData;
   for( uint8_t i = 0; i < NUM_REMOTE_DATA; i++ )
   {
      uint16_t uwTimerLimit_5ms = REMOTE_DATA_INVALID_RESEND_TIMER_5MS;
      switch(i)
      {
         case REMOTE_DISPATCH_MODE:
            uiData = GetDispatchMode();
            uwTimerLimit_5ms = REMOTE_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case REMOTE_CARCALL_SECURITY_F0:
            uiData = auiBF_Virtual_CarCallSecurity[0][0];
            uwTimerLimit_5ms = REMOTE_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case REMOTE_CARCALL_SECURITY_F1:
            uiData = auiBF_Virtual_CarCallSecurity[0][1];
            uwTimerLimit_5ms = REMOTE_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case REMOTE_CARCALL_SECURITY_F2:
            uiData = auiBF_Virtual_CarCallSecurity[0][2];
            uwTimerLimit_5ms = REMOTE_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case REMOTE_CARCALL_SECURITY_R0:
            uiData = auiBF_Virtual_CarCallSecurity[1][0];
            uwTimerLimit_5ms = REMOTE_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case REMOTE_CARCALL_SECURITY_R1:
            uiData = auiBF_Virtual_CarCallSecurity[1][1];
            uwTimerLimit_5ms = REMOTE_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case REMOTE_CARCALL_SECURITY_R2:
            uiData = auiBF_Virtual_CarCallSecurity[1][2];
            uwTimerLimit_5ms = REMOTE_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case REMOTE_HALLCALL_SECURITY_F0:
            uiData = auiBF_Virtual_HallCallSecurity[0][0];
            uwTimerLimit_5ms = REMOTE_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case REMOTE_HALLCALL_SECURITY_F1:
            uiData = auiBF_Virtual_HallCallSecurity[0][1];
            uwTimerLimit_5ms = REMOTE_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case REMOTE_HALLCALL_SECURITY_F2:
            uiData = auiBF_Virtual_HallCallSecurity[0][2];
            uwTimerLimit_5ms = REMOTE_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case REMOTE_HALLCALL_SECURITY_R0:
            uiData = auiBF_Virtual_HallCallSecurity[1][0];
            uwTimerLimit_5ms = REMOTE_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case REMOTE_HALLCALL_SECURITY_R1:
            uiData = auiBF_Virtual_HallCallSecurity[1][1];
            uwTimerLimit_5ms = REMOTE_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         case REMOTE_HALLCALL_SECURITY_R2:
            uiData = auiBF_Virtual_HallCallSecurity[1][2];
            uwTimerLimit_5ms = REMOTE_DATA_DEFAULT_RESEND_TIMER_5MS;
            break;
         default:
            break;
      }

      if( uwTimerLimit_5ms == REMOTE_DATA_INVALID_RESEND_TIMER_5MS )
      {
         Sys_Bit_Set(&aucDirtyBits[0], i, 0);
      }
      else if( uiData != auiLastData[i] )
      {
         auiLastData[i] = uiData;
         Sys_Bit_Set(&aucDirtyBits[0], i, 1);
      }

      if( ( uwTimerLimit_5ms != REMOTE_DATA_INVALID_RESEND_TIMER_5MS )
       && ( auwResendTimer_5ms[i] > uwTimerLimit_5ms ) )
      {
         Sys_Bit_Set(&aucDirtyBits[0], i, 1);
      }
      else
      {
         auwResendTimer_5ms[i]++;
      }
   }

   /* Fetch next packet to send */
   en_remote_data enPacket = NUM_REMOTE_DATA;
   for(uint8_t i = 0; i < NUM_REMOTE_DATA; i++)
   {
      en_remote_data ePacketToCheck = (enLastPacket+i+1) % NUM_REMOTE_DATA;
      if(Sys_Bit_Get(&aucDirtyBits[0], ePacketToCheck))
      {
         enPacket = ePacketToCheck;
         break;
      }
   }

   if( enPacket < NUM_REMOTE_DATA )
   {
      bSend = 1;
      punDatagram->auc8[0] = ( Param_ReadValue_1Bit(enPARAM1__DisableEPower) ) ? EPC__OFF:GetEPowerCommand( GetFP_GroupCarIndex() );
      punDatagram->auc8[1] = DynamicParking_GetFloorAssignment();
      punDatagram->auc8[2] = GetMasterDispatcherFlag();
      punDatagram->auc8[3] = enPacket;
      switch(enPacket)
      {
         case REMOTE_DISPATCH_MODE:
            /*3 free bytes*/
            punDatagram->aui32[1] = GetDispatchMode();
            break;
         case REMOTE_CARCALL_SECURITY_F0:
            punDatagram->aui32[1] = auiBF_Virtual_CarCallSecurity[0][0];
            break;
         case REMOTE_CARCALL_SECURITY_F1:
            punDatagram->aui32[1] = auiBF_Virtual_CarCallSecurity[0][1];
            break;
         case REMOTE_CARCALL_SECURITY_F2:
            punDatagram->aui32[1] = auiBF_Virtual_CarCallSecurity[0][2];
            break;
         case REMOTE_CARCALL_SECURITY_R0:
            punDatagram->aui32[1] = auiBF_Virtual_CarCallSecurity[1][0];
            break;
         case REMOTE_CARCALL_SECURITY_R1:
            punDatagram->aui32[1] = auiBF_Virtual_CarCallSecurity[1][1];
            break;
         case REMOTE_CARCALL_SECURITY_R2:
            punDatagram->aui32[1] = auiBF_Virtual_CarCallSecurity[1][2];
            break;
         case REMOTE_HALLCALL_SECURITY_F0:
            punDatagram->aui32[1] = auiBF_Virtual_HallCallSecurity[0][0];
            break;
         case REMOTE_HALLCALL_SECURITY_F1:
            punDatagram->aui32[1] = auiBF_Virtual_HallCallSecurity[0][1];
            break;
         case REMOTE_HALLCALL_SECURITY_F2:
            punDatagram->aui32[1] = auiBF_Virtual_HallCallSecurity[0][2];
            break;
         case REMOTE_HALLCALL_SECURITY_R0:
            punDatagram->aui32[1] = auiBF_Virtual_HallCallSecurity[1][0];
            break;
         case REMOTE_HALLCALL_SECURITY_R1:
            punDatagram->aui32[1] = auiBF_Virtual_HallCallSecurity[1][1];
            break;
         case REMOTE_HALLCALL_SECURITY_R2:
            punDatagram->aui32[1] = auiBF_Virtual_HallCallSecurity[1][2];
            break;
         case REMOTE_SYSTEM_INPUTS:
            punDatagram->aui32[1] = uiBF_Virtual_SysInputs;
            break;
         default:
            bSend = 0;
            break;
      }
   }
   
   
   if(bSend)
   {
      enLastPacket = enPacket;
      Sys_Bit_Set(&aucDirtyBits[0], enPacket, 0);
      auwResendTimer_5ms[enPacket] = 0;
   }

   return bSend;
}
/*----------------------------------------------------------------------------
 * Reset all virtual inputs bitmaps and remote dispatch mode
 *----------------------------------------------------------------------------*/
void Reset_Virtual_Inputs(void)
{
   enActiveDispatchCommand = DISPATCH_MODE__NONE;

   memset(&auiBF_Virtual_CarCallSecurity[0], 0, sizeof(auiBF_Virtual_CarCallSecurity[0]));
   memset(&auiBF_Virtual_CarCallSecurity[1], 0, sizeof(auiBF_Virtual_CarCallSecurity[1]));

   memset(&auiBF_Virtual_HallCallSecurity[0], 0, sizeof(auiBF_Virtual_HallCallSecurity[0]));
   memset(&auiBF_Virtual_HallCallSecurity[1], 0, sizeof(auiBF_Virtual_HallCallSecurity[1]));

   memset(&uiBF_Virtual_SysInputs, 0, sizeof(uiBF_Virtual_SysInputs));

}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

