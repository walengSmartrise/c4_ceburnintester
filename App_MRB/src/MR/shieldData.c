
/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief
 * @version  V1.00
 * @date     27, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>

#include "sru_b.h"
#include <stdint.h>
//#include "SEGGER_RTT.h"
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "carData.h"
#include "carDestination.h"
#include "shieldData.h"
#include "hallData.h"
#include "hallboard.h"
#include "group_net_data.h"
#include "ui.h"
#include "shieldData.h"
#include "masterCommand.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint32_t uiTimeout_ms;
static uint16_t uwMessageCounter;
static en_shield_error eShieldError;
static uint8_t ucMajorVersion;// i.e. for version v1.2, Major version is 1, minor version is 2.
static uint8_t ucMinorVersion;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
// TODO Function called to reset shield data when communication is lost with the shield
void ShieldData_ResetData(void)
{
   uwMessageCounter = 0;
   eShieldError = 0;
   ucMajorVersion = 0;
   ucMinorVersion = 0;
}
/*-----------------------------------------------------------------------------
   Access functions
 -----------------------------------------------------------------------------*/
en_shield_error ShieldData_GetError(void) // TODO add bit option to enable shield error alarms on C4 system
{
   return eShieldError;
}
uint8_t ShieldData_GetVersion_Major(void)
{
   return ucMajorVersion;
}
uint8_t ShieldData_GetVersion_Minor(void)
{
   return ucMinorVersion;
}

/*-----------------------------------------------------------------------------
   Message counter functions
 -----------------------------------------------------------------------------*/
void ShieldData_IncMessageCounter( void )
{
   uwMessageCounter++;
}

uint16_t ShieldData_GetMessageCounter( void )
{
   return uwMessageCounter;
}
/*-----------------------------------------------------------------------------
   Timer functions
 -----------------------------------------------------------------------------*/
void ShieldData_IncTimeoutCounter( uint16_t uwInterval_ms )
{
   static uint8_t bLastOnlineFlag;
   uint8_t bOnlineFlag = 0;
   if( uiTimeout_ms < SHIELD_TIMEOUT_COUNT_MS )
   {
      uiTimeout_ms += uwInterval_ms;
      bOnlineFlag = 1;
   }
   else if( bLastOnlineFlag )
   {
      ShieldData_ResetData();
   }
   bLastOnlineFlag = bOnlineFlag;
}
void ShieldData_ClrTimeoutCounter()
{
   uiTimeout_ms = 0;
}
uint8_t ShieldData_CheckTimeoutCounter()
{
   uint8_t bTimeout = 0;
   if( uiTimeout_ms >= SHIELD_TIMEOUT_COUNT_MS )
   {
      bTimeout = 1;
   }
   return bTimeout;
}
uint16_t ShieldData_GetTimeoutCounter()
{
   return uiTimeout_ms;
}

/*-----------------------------------------------------------------------------
DG_GroupNet_SHIELD__SyncTime,
 -----------------------------------------------------------------------------*/
void UnloadDatagram_SyncTime( un_sdata_datagram *punDatagram )
{
   /* Unix time sent LSB to MSB */
   uint32_t uiUnixTime = ( punDatagram->auc8[7] << 0 )
                       | ( punDatagram->auc8[6] << 8 )
                       | ( punDatagram->auc8[5] << 16 )
                       | ( punDatagram->auc8[4] << 24 );

   if(Param_ReadValue_1Bit(enPARAM1__EnableBoardRTC))
   {
      RTC_WritePeripheralTime_Unix(uiUnixTime);
   }
   RTC_SetSyncTime(uiUnixTime);

   /* Unload fault and version data also encoded into this packet */
   en_shield_error eError = punDatagram->auc8[3];
   eShieldError = ( eError < NUM_SHIELD_ERRORS ) ? eError:SHIELD_ERROR__UNKNOWN;
   ucMajorVersion = punDatagram->auc8[1];
   uint8_t ucUpperNibble = ( punDatagram->auc8[2] >> 4 ) & 0xF;
   uint8_t ucLowerNibble = punDatagram->auc8[2] & 0xF;
   ucMinorVersion = ucLowerNibble + ( ucUpperNibble * 10 );
}
/*-----------------------------------------------------------------------------
DG_GroupNet_SHIELD__RemoteCommand,
 -----------------------------------------------------------------------------*/
void UnloadDatagram_RemoteCommand( un_sdata_datagram *punDatagram )
{
   en_remote_cmd enRemoteCommand = punDatagram->auc8[0];

   switch(enRemoteCommand)
   {
      case REMOTE_CMD__SET_DISPATCH:
         RemoteCommand_SetDispatchMode(punDatagram->auc8[1]);
         break;
      case REMOTE_CMD__SET_PARKING:
         RemoteCommand_SetParkingFloor(punDatagram->auc8[1]);
         break;
      case REMOTE_CMD__SET_GROUP_PARAMETER:
         RemoteCommand_SetGroupParameter(
            punDatagram->auc8[1],
            punDatagram->auw16[1],
            punDatagram->aui32[1]
         );
         break;
      case REMOTE_CMD__SET_VIRTUAL_INPUTS:
         RemoteCommand_SetVirtualInputs(punDatagram);
         break;
      default:
         break;
   }

}
/*-----------------------------------------------------------------------------
DG_GroupNet_SHIELD__LatchCarCall,
 -----------------------------------------------------------------------------*/
void UnloadDatagram_LatchCarCall( un_sdata_datagram *punDatagram )
{
   uint8_t ucLandingIndex = punDatagram->auc8[0];
   uint8_t bRearDoor = punDatagram->auc8[1];
   uint8_t ucFloorIndex = GetCarMapping(ucLandingIndex);
   SetCarCall(ucFloorIndex, bRearDoor);
}
/*-----------------------------------------------------------------------------
DG_GroupNet_SHIELD__LatchHallCall,
 -----------------------------------------------------------------------------*/
void UnloadDatagram_LatchHallCall( un_sdata_datagram *punDatagram )
{
   uint8_t ucLandingIndex = punDatagram->auc8[0];
   en_hc_dir eDir= (punDatagram->auc8[1]) ? HC_DIR__UP:HC_DIR__DOWN;
   enum en_doors enDoorIndex = punDatagram->auc8[2];
   if( ucLandingIndex < MAX_NUM_FLOORS )
   {
      MasterCommand_SetHallCall( ucLandingIndex, eDir, CarData_GetGroupHallCallMask(ucLandingIndex, enDoorIndex ) & 0x0000FFFF);
   }
}
/*-----------------------------------------------------------------------------
DG_GroupNet_SHIELD__ClearHallCall,
 -----------------------------------------------------------------------------*/
void UnloadDatagram_ClearHallCall( un_sdata_datagram *punDatagram )
{
   uint8_t ucLandingIndex = punDatagram->auc8[0];
   en_hc_dir eDir= (punDatagram->auc8[1]) ? HC_DIR__UP:HC_DIR__DOWN;
   uint32_t uiClrMask = ~punDatagram->aui32[1];
   if( ucLandingIndex < MAX_NUM_FLOORS )
   {
      ClrRawLatchedHallCalls( ucLandingIndex, eDir, uiClrMask );
   }
}
/*-----------------------------------------------------------------------------
DG_GroupNet_SHIELD__VirtualCCEnables,
 -----------------------------------------------------------------------------*/
void UnloadDatagram_VirtualCCEnables( un_sdata_datagram *punDatagram )
{
   enum en_doors enDoorIndex = punDatagram->auc8[0];
   uint8_t ucMapIndex = punDatagram->auc8[1];
   uint32_t uiSecurityBitmap = punDatagram->aui32[1];

   SetVirtualCarCallSecurity(enDoorIndex, ucMapIndex, uiSecurityBitmap);
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
