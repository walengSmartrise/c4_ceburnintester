/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     26, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sru_b.h"
#include "sys.h"
#include "group_net_data.h"
#include "datagrams.h"
#include "mod_lanternMonitor.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init(struct st_module *pstThisModule);
static uint32_t Run(struct st_module *pstThisModule);

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_SData_AuxNet =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_sdata_control gstSData_AuxNet_MRB;
static struct st_sdata_control gstSData_AuxNet_LW;
struct st_sdata_control * gpastSData_Nodes_AuxNet[ NUM_MR_AUX_NET_NODES ] =
{
   &gstSData_AuxNet_MRB,//MR_AUX_NET__MRB,
   &gstSData_AuxNet_LW,//MR_AUX_NET__LW,
};


uint8_t gaucPerNodeNumDatagrams_AuxNet[ NUM_MR_AUX_NET_NODES ] =
{
   NUM_AuxNet_MRB_DATAGRAMS,
   NUM_AuxNet_LW_DATAGRAMS,
};

static struct st_sdata_local gstSData_Config;

static CAN_MSG_T gstCAN_MSG_Tx;


/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SharedData_AuxNet_Init()
{
   int iError;
   int i;

   //--------------------------------------------------------
   gstSData_Config.ucLocalNodeID = MR_AUX_NET__MRB;

   gstSData_Config.ucNetworkID = SDATA_NET__AUX;

   gstSData_Config.ucNumNodes = NUM_MR_AUX_NET_NODES;

   gstSData_Config.ucDestNodeID = 0x1F;

   for ( i = 0; i < gstSData_Config.ucNumNodes; ++i )
   {
      iError = SDATA_CreateDatagrams( gpastSData_Nodes_AuxNet[ i ],
                                      gaucPerNodeNumDatagrams_AuxNet[ i ] );

      if ( iError )
      {
         while ( 1 )
         {
             //SDA REVIEW: issue a fault or some other kind of error recovery
            ;  // TODO: unable to allocate shared data
         }
      }
   }
   gstCAN_MSG_Tx.DLC = 8;
}
/*-----------------------------------------------------------------------------
Takes local datagram ID and assigns corresponding CAN
 network message ID to the outbound packet
 -----------------------------------------------------------------------------*/
static void SetDatagramMessageID(uint8_t ucLocalDatagramID)
{
   gstCAN_MSG_Tx.DLC = 8;
   if ( ucLocalDatagramID == DG_AuxNet_MRB__Message )
   {
      gstCAN_MSG_Tx.ID = 0x501;
      gstCAN_MSG_Tx.DLC = 4;
   }
   else if( ucLocalDatagramID == DG_AuxNet_MRB__Label )
   {
      gstCAN_MSG_Tx.ID = 0x502;
      gstCAN_MSG_Tx.DLC = 7;
   }
   else if( ucLocalDatagramID == DG_AuxNet_MRB__DiscretePI )
   {
      gstCAN_MSG_Tx.ID = 0x503;
   }
   else if( ucLocalDatagramID == DG_AuxNet_MRB__LoadWeigher )
   {
      gstCAN_MSG_Tx.ID = CAN_EXTEND_ID_USAGE;
      gstCAN_MSG_Tx.ID |= ( SDATA_NET__AUX & 0x0F ) << 25;
      gstCAN_MSG_Tx.ID |= ( MR_AUX_NET__MRB & 0x1F ) << 20;
      gstCAN_MSG_Tx.ID |= ( 0x1F & 0x1F ) << 15;
      gstCAN_MSG_Tx.ID |= ( DG_AuxNet_MRB__LoadWeigher & 0x1F ) << 10;
   }
   else if( ( ucLocalDatagramID >= DG_AuxNet_MRB__HallLamps_0 )
         && ( ucLocalDatagramID <= DG_AuxNet_MRB__HallLamps_15 ) )
   {
      /* To make hall boards function as hall lanterns, MRB will pretend to be RIS1 */
      uint8_t ucDatagramID = ucLocalDatagramID - DG_AuxNet_MRB__HallLamps_0;
      uint8_t ucLocalNodeID = HALL_NET__RIS;
      uint8_t ucNetworkID = SDATA_NET__HALL;
      uint8_t ucDestNodeID = 0x1F; // all nodes
      uint8_t ucDIPs = 0x80; // RIS1
      gstCAN_MSG_Tx.ID = CAN_EXTEND_ID_USAGE
                      | ( ucDIPs << HALLNET_CAN_ID_SHIFT__DIP )
                      | ( ucDatagramID << HALLNET_CAN_ID_SHIFT__DG )
                      | ( ucDestNodeID << HALLNET_CAN_ID_SHIFT__DEST )
                      | ( ( ucDestNodeID & 0xF ) << HALLNET_CAN_ID_SHIFT__DEST )
                      | ( ucLocalNodeID << HALLNET_CAN_ID_SHIFT__SRC )
                      | ( ucNetworkID << HALLNET_CAN_ID_SHIFT__NET );
   }
   else
   {
      gstCAN_MSG_Tx.ID = ( 1 << 30 ) | ( ucLocalDatagramID << 10 )
               | ( gstSData_Config.ucDestNodeID << 15 )
               | ( gstSData_Config.ucLocalNodeID << 20 )
               | ( gstSData_Config.ucNetworkID << 25 );

   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Transmit(void)
{
   un_sdata_datagram unDatagram;
   int iError = 1;
   uint8_t ucNodeID;
   uint8_t ucDatagramID;
   uint16_t uwDatagramID_Plus1 = 0;
   ucNodeID = gstSData_Config.ucLocalNodeID;

   uwDatagramID_Plus1 = SDATA_GetNextDatagramToSendIndex_Plus1(
            gpastSData_Nodes_AuxNet[ ucNodeID ] );
   if(uwDatagramID_Plus1)
   {
      ucDatagramID = uwDatagramID_Plus1 - 1;
      if ( ucDatagramID < gaucPerNodeNumDatagrams_AuxNet[ ucNodeID ] )
      {
         SDATA_ReadDatagram( gpastSData_Nodes_AuxNet[ ucNodeID ], ucDatagramID, &unDatagram );

         memcpy( gstCAN_MSG_Tx.Data, unDatagram.auc8,
                  sizeof( unDatagram.auc8 ) );

         SetDatagramMessageID(ucDatagramID);

         CAN_BUFFER_ID_T   TxBuf = Chip_CAN_GetFreeTxBuf(LPC_CAN1);
         iError =  !Sys_CAN_Send( LPC_CAN1, TxBuf, &gstCAN_MSG_Tx);
         if ( iError )
         {
            SDATA_DirtyBit_Set( gpastSData_Nodes_AuxNet[ ucNodeID ], ucDatagramID );

            if ( gpastSData_Nodes_AuxNet[ ucNodeID ]->uwLastSentIndex )
            {
               --gpastSData_Nodes_AuxNet[ ucNodeID ]->uwLastSentIndex;
            }
            else
            {
               gpastSData_Nodes_AuxNet[ ucNodeID ]->uwLastSentIndex =
                        gpastSData_Nodes_AuxNet[ ucNodeID ]->uiNumDatagrams - 1;
            }
         }
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData(void)
{
   LoadDatagram_HallLanterns(&gstSData_AuxNet_MRB);
}

/*-----------------------------------------------------------------------------

 Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init(struct st_module *pstThisModule)
{
   pstThisModule->uwInitialDelay_1ms = 5;
   pstThisModule->uwRunPeriod_1ms = 5;

   //------------------------------------------------

  // Set resend rates for each datagram in msec (Default 10 sec)
   *(gpastSData_Nodes_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_MRB__Message) = 250;
   *(gpastSData_Nodes_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_MRB__Label) = 250;
   *(gpastSData_Nodes_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_MRB__DiscretePI) = 250;
   *(gpastSData_Nodes_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_MRB__LoadWeigher) = DISABLED_DATAGRAM_RESEND_RATE_MS;

   if(Param_ReadValue_8Bit(enPARAM8__HallLanternMask))
   {
      /* Initialize send rates */
      for( uint8_t i = DG_AuxNet_MRB__HallLamps_0; i <= DG_AuxNet_MRB__HallLamps_15; i++ )
      {
         gstSData_AuxNet_MRB.paiResendRate_1ms[i] = 1000;
      }
   }
   else
   {
      /* Initialize send rates */
      for( uint8_t i = DG_AuxNet_MRB__HallLamps_0; i <= DG_AuxNet_MRB__HallLamps_15; i++ )
      {
         gstSData_AuxNet_MRB.paiResendRate_1ms[i] = DISABLED_DATAGRAM_RESEND_RATE_MS;
      }
   }
   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run(struct st_module *pstThisModule)
{
   //------------------------------------------------
   LoadData();
   Transmit();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
