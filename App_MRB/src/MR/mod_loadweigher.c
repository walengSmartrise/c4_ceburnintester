/******************************************************************************
 *
 * @file
 * @brief
 * @version  V1.00
 * @date     28, Nov 2018
 *
 * @note    For smartrise load weigher device
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "sru.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
#include "motion.h"
#include "mod_loadweigher.h"
#include "rtc.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_module gstMod_LoadWeigher =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define CALIBRATE_TRANSITION_DEBOUNCE_50MS    (200)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static int8_t cLastTorque;
static int8_t acTorqueSamples[NUM_LW_TORQUE_SAMPLES];
static uint8_t ucSampleIndex;
static uint16_t uwReceiveCounter;

static uint8_t aucSendCountdown[NUM_LW_REQ];

static uint16_t uwOfflineCounter_ms = LWD_OFFLINE_TIMER_UNK_MS;

static en_lwd_error eError;

/* In car weight */
static uint8_t ucInCarWeight_50lb;

/* Encoded load weigher flags in bitmap order: en_load_flag_bits */
static uint8_t ucLoadFlags;

/* Flag signaling if the LWD wifi should be on */
static uint8_t bWiFiFlag;

static float fAvgTorque;

static st_lwd_ui_request stUserRequest;

static float fUIRequest_ReadValue;

static en_lwd_calib_state eCalibrationState_Zero;
static en_lwd_calib_state eCalibrationState_FullLoad;

static uint8_t ucCurrentLearnFloor;
static uint8_t bLearnConfirmation;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void LoadWeigher_SetUIRequestReadData(float fValue)
{
   fUIRequest_ReadValue = fValue;
}
float LoadWeigher_GetUIRequestReadData(void)
{
   return fUIRequest_ReadValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void LoadWeigher_GetUIRequest(st_lwd_ui_request *pstUIRequest)
{
   memcpy(pstUIRequest, &stUserRequest, sizeof(st_lwd_ui_request));
}
void LoadWeigher_SetUIRequest(st_lwd_ui_request *pstUIRequest, uint8_t bRemote)
{
   /* Allow requests if the active command is to read, but write/generate requests must time out*/
   if( ( pstUIRequest->eRequest < NUM_LW_REQ )
    && ( pstUIRequest->eCommand < NUM_LW_CMD )
    && ( pstUIRequest->eRequest != LW_REQ__TORQUE )
    && ( stUserRequest.eCommand == LW_CMD__READ )  )
   {
      if( pstUIRequest->eCommand == LW_CMD__READ )
      {
         stUserRequest.eRequest = pstUIRequest->eRequest;
         stUserRequest.eCommand = pstUIRequest->eCommand;
         stUserRequest.ucSendCountdown = 1;
         stUserRequest.ucClearCountdown_50ms = ( bRemote )
                                             ? ( LW_UI_REQUEST_REMOTE_CLEAR_COUNTDOWN_50MS )
                                             : ( LW_UI_REQUEST_LOCAL_CLEAR_COUNTDOWN_50MS );
      }
      else
      {
         if( ( stUserRequest.eRequest != pstUIRequest->eRequest )
          || ( stUserRequest.eCommand != pstUIRequest->eCommand )
          || ( stUserRequest.fWriteData != pstUIRequest->fWriteData ) )
         {
            stUserRequest.eRequest = pstUIRequest->eRequest;
            stUserRequest.eCommand = pstUIRequest->eCommand;
            stUserRequest.fWriteData = pstUIRequest->fWriteData;
            stUserRequest.ucSendCountdown = 1;
            stUserRequest.ucClearCountdown_50ms = ( bRemote )
                                                ? ( LW_UI_REQUEST_REMOTE_CLEAR_COUNTDOWN_50MS )
                                                : ( LW_UI_REQUEST_LOCAL_CLEAR_COUNTDOWN_50MS );
         }
      }
   }
}
static void LoadWeigher_UpdateUIRequest(void)
{
   if( stUserRequest.ucClearCountdown_50ms )
   {
      stUserRequest.ucClearCountdown_50ms--;
   }
   else
   {
      stUserRequest.eRequest = LW_REQ__TORQUE;
      stUserRequest.eCommand = LW_CMD__READ;
      stUserRequest.fWriteData = 0;
      stUserRequest.ucSendCountdown = 0;
   }
}
static void LoadWeigher_ForceClearUIRequest(void)
{
   stUserRequest.eRequest = LW_REQ__TORQUE;
   stUserRequest.eCommand = LW_CMD__READ;
   stUserRequest.fWriteData = 0;
   stUserRequest.ucSendCountdown = 0;
}
/*-----------------------------------------------------------------------------
Flag signaling if LWD zeroing procedure should occur
 -----------------------------------------------------------------------------*/
en_lwd_calib_state LoadWeigher_GetCalibrationState(void)
{
   en_lwd_calib_state eState = ( eCalibrationState_Zero == LWD_CALIB_STATE__NONE )
                             ? ( eCalibrationState_FullLoad )
                             : ( eCalibrationState_Zero );
   return eState;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t LoadWeigher_GetLoadFlag(en_load_flag_bits eBit)
{
   uint8_t ucFlag = 0;
   if( eBit < NUM_LOAD_FLAG )
   {
      ucFlag = Sys_Bit_Get(&ucLoadFlags, eBit);
   }
   else if( eBit == NUM_LOAD_FLAG )
   {
      ucFlag = ucLoadFlags;
   }
   return ucFlag;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t LoadWeigher_GetInCarWeight_50lb(void)
{
   return ucInCarWeight_50lb;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
en_lwd_error LoadWeigher_GetError(void)
{
   return eError;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint16_t LoadWeigher_GetReceiveCounter(void)
{
   if(!Param_ReadValue_1Bit(enPARAM1__Debug_LWD))
   {
      return 0;
   }
   else
   {
      return uwReceiveCounter;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t LoadWeigher_GetOfflineStatus(void)
{
   return (uwOfflineCounter_ms >= CAN_TIMEOUT_COUNT_MS);
}
void LoadWeigher_UpdateOfflineCounter(uint16_t uwRunPeriod_ms)
{
   if( uwOfflineCounter_ms < CAN_TIMEOUT_COUNT_MS )
   {
      uwOfflineCounter_ms += uwRunPeriod_ms;
   }
   else if( uwOfflineCounter_ms != LWD_OFFLINE_TIMER_UNK_MS )
   {
      SetAlarm(ALM__LWD_OFFLINE);
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
int8_t LoadWeigher_GetRawTorquePercent(void)
{
   if(!Param_ReadValue_1Bit(enPARAM1__Debug_LWD))
   {
      return 0;
   }
   else
   {
      return cLastTorque;
   }
}
int8_t LoadWeigher_GetAverageTorquePercent(void)
{
   return (int8_t) fAvgTorque;
}

/*-----------------------------------------------------------------------------
At a regular interval, car should empty the car,
move to the bottom terminal floor, and zero out the load sensor
Currently calibration is triggered every sunday at midnight
 -----------------------------------------------------------------------------*/
static void CalibrateLWD(void)
{
   static uint8_t ucLastHourOfTheDay = 0xFF;
   static uint8_t ucDebounce_Zero_50ms;
   static uint8_t ucDebounce_FullLoad_50ms;
   // This flag is set to 1 when the car must run its monthly self calibraiton sequence
   static uint8_t bAutomaticRecalibrate;

   st_lwd_ui_request stUIRequest;
   uint8_t ucHourOfDay = RTC_GetTimeOfDay_Hour();
   if( GetOperation_AutoMode() == MODE_A__NORMAL )
   {
      /* Update auto recalibrate flag */
      if( Param_ReadValue_1Bit(enPARAM1__LWD_AutoRecalibrate) )
      {
         st_rtc_day_of_week stDayOfWeek = RTC_GetDayOfWeek();
         uint8_t ucDayOfMonth = RTC_GetDayOfTheMonth();
         uint8_t ucCalibrationHour = Param_ReadValue_8Bit(enPARAM8__LWD_MonthlyCalibrationHour);
         st_rtc_day_of_week stCalibrationDay = Param_ReadValue_8Bit(enPARAM8__LWD_MonthlyCalibrationDay);
         if( ( stDayOfWeek == stCalibrationDay )
          && ( ucDayOfMonth < 7 )
          && ( ucHourOfDay == ucCalibrationHour )
          && ( ucHourOfDay != ucLastHourOfTheDay )
          && ( ucLastHourOfTheDay != 0xFF ) ) // Startup indicator
         {
            bAutomaticRecalibrate = 1;
         }
      }
      else
      {
         bAutomaticRecalibrate = 0;
      }

      switch( eCalibrationState_Zero )
      {
         case LWD_CALIB_STATE__NONE:
            if( Param_ReadValue_1Bit(enPARAM1__LWD_TriggerRecalibrate) ) {
               if(++ucDebounce_Zero_50ms >= CALIBRATE_TRANSITION_DEBOUNCE_50MS) {
                  ucDebounce_Zero_50ms = 0;
                  eCalibrationState_Zero = LWD_CALIB_STATE__CAPTURE;
               }
            }
            else if( bAutomaticRecalibrate ) {
               ucDebounce_Zero_50ms = 0;
               eCalibrationState_Zero = LWD_CALIB_STATE__CAPTURE;
            }
            else {
               ucDebounce_Zero_50ms = 0;
            }
            break;
         case LWD_CALIB_STATE__CAPTURE:
            SetAlarm(ALM__LWD_RECALIBRATE);
            /* Wait for car to be emptied at the bottom terminal to start zeroing */
            if( ( GetOperation3_CaptureMode_MR() == CAPTURE_IDLE ) &&
                ( GetOperation4_FrontDoorState() == DOOR__CLOSED ) &&
                ( !GetFP_RearDoors() || ( GetOperation4_RearDoorState() == DOOR__CLOSED ) ) &&
                ( !GetMotion_RunFlag() ) ) {
               if(++ucDebounce_Zero_50ms >= CALIBRATE_TRANSITION_DEBOUNCE_50MS) {
                  ucDebounce_Zero_50ms = 0;
                  eCalibrationState_Zero = LWD_CALIB_STATE__MOVE_TO_BOTTOM;
               }
            }
            else {
               ucDebounce_Zero_50ms = 0;
            }
            break;
         case LWD_CALIB_STATE__MOVE_TO_BOTTOM:
            if( ( !GetMotion_RunFlag() ) && ( !GetOperation_CurrentFloor() ) && ( !gstFault.bActiveFault )
             && ( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R) || ( !GetFloorOpening_Front(GetOperation_CurrentFloor()) && !GetFloorOpening_Rear(GetOperation_CurrentFloor()) ) ) ) {
               if(++ucDebounce_Zero_50ms >= CALIBRATE_TRANSITION_DEBOUNCE_50MS) {
                  ucDebounce_Zero_50ms = 0;
                  ucCurrentLearnFloor = 0;
                  bLearnConfirmation = 0;
                  eCalibrationState_Zero = LWD_CALIB_STATE__LEARN_FLOOR;
               }
            }
            else {
               ucDebounce_Zero_50ms = 0;
            }
            break;
         case LWD_CALIB_STATE__LEARN_FLOOR:
            if( bLearnConfirmation ) {
               LoadWeigher_ForceClearUIRequest();
               if(++ucDebounce_Zero_50ms >= 60) {
                  ucDebounce_Zero_50ms = 0;
                  ucCurrentLearnFloor += 1;
                  if( ucCurrentLearnFloor >= GetFP_NumFloors() ) {
                     eCalibrationState_Zero = LWD_CALIB_STATE__COMPLETE;
                  }
                  else {
                     eCalibrationState_Zero = LWD_CALIB_STATE__MOVE_TO_NEXT;
                  }
               }
            }
            else {
               ucDebounce_Zero_50ms = 0;
               stUIRequest.eRequest = LW_REQ__ZERO;
               stUIRequest.eCommand = LW_CMD__GENERATE;
               stUIRequest.fWriteData = 0;
               LoadWeigher_SetUIRequest(&stUIRequest, 0);
            }
            break;
         case LWD_CALIB_STATE__MOVE_TO_NEXT:
            if( ( !GetMotion_RunFlag() ) && ( GetOperation_CurrentFloor() == ucCurrentLearnFloor ) && ( !gstFault.bActiveFault )
             && ( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R) || ( !GetFloorOpening_Front(GetOperation_CurrentFloor()) && !GetFloorOpening_Rear(GetOperation_CurrentFloor()) ) ) ) {
               if(++ucDebounce_Zero_50ms >= CALIBRATE_TRANSITION_DEBOUNCE_50MS) {
                  ucDebounce_Zero_50ms = 0;
                  bLearnConfirmation = 0;
                  eCalibrationState_Zero = LWD_CALIB_STATE__LEARN_FLOOR;
               }
            }
            else {
               ucDebounce_Zero_50ms = 0;
            }
            break;
         case LWD_CALIB_STATE__COMPLETE:
            ucDebounce_Zero_50ms = 0;
            ucCurrentLearnFloor = 0;
            bLearnConfirmation = 0;
            bAutomaticRecalibrate = 0;

            /* Modification from system where LWD requests top and bottom
             * position and the end of a learn, to one where the the C4
             * system proactively send it at the end of the learn */
            aucSendCountdown[LW_REQ__POS_BOT] = NUM_PACKET_RESEND;
            aucSendCountdown[LW_REQ__POS_TOP] = NUM_PACKET_RESEND;

            if( Param_ReadValue_1Bit(enPARAM1__LWD_TriggerRecalibrate) )
            {
               Param_WriteValue_1Bit(enPARAM1__LWD_TriggerRecalibrate, 0);
            }
            else
            {
               eCalibrationState_Zero = LWD_CALIB_STATE__NONE;
            }
            break;
         default:
            eCalibrationState_Zero = LWD_CALIB_STATE__NONE;
            break;
      }

      if( eCalibrationState_Zero == LWD_CALIB_STATE__NONE )
      {
         switch( eCalibrationState_FullLoad )
         {
            case LWD_CALIB_STATE__NONE:
               if( Param_ReadValue_1Bit(enPARAM1__LWD_TriggerLoadLearn) ) {
                  if(++ucDebounce_FullLoad_50ms >= CALIBRATE_TRANSITION_DEBOUNCE_50MS) {
                     ucDebounce_FullLoad_50ms = 0;
                     eCalibrationState_FullLoad = LWD_CALIB_STATE__CAPTURE;
                  }
               }
               else {
                  ucDebounce_FullLoad_50ms = 0;
               }
               break;
            case LWD_CALIB_STATE__CAPTURE:
               SetAlarm(ALM__LWD_LOAD_LEARN);
               /* Wait for car to be emptied at the bottom terminal to start zeroing */
               if( ( GetOperation3_CaptureMode_MR() == CAPTURE_IDLE ) &&
                   ( GetOperation4_FrontDoorState() == DOOR__CLOSED ) &&
                   ( !GetFP_RearDoors() || ( GetOperation4_RearDoorState() == DOOR__CLOSED ) ) &&
                   ( !GetMotion_RunFlag() ) ) {
                  if(++ucDebounce_FullLoad_50ms >= CALIBRATE_TRANSITION_DEBOUNCE_50MS) {
                     ucDebounce_FullLoad_50ms = 0;
                     eCalibrationState_FullLoad = LWD_CALIB_STATE__MOVE_TO_BOTTOM;
                  }
               }
               else {
                  ucDebounce_FullLoad_50ms = 0;
               }
               break;
            case LWD_CALIB_STATE__MOVE_TO_BOTTOM:
               if( ( !GetMotion_RunFlag() ) && ( !GetOperation_CurrentFloor() ) && ( !gstFault.bActiveFault )
                && ( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R) || ( !GetFloorOpening_Front(GetOperation_CurrentFloor()) && !GetFloorOpening_Rear(GetOperation_CurrentFloor()) ) ) ) {
                  if(++ucDebounce_FullLoad_50ms >= CALIBRATE_TRANSITION_DEBOUNCE_50MS) {
                     ucDebounce_FullLoad_50ms = 0;
                     ucCurrentLearnFloor = 0;
                     bLearnConfirmation = 0;
                     eCalibrationState_FullLoad = LWD_CALIB_STATE__LEARN_FLOOR;
                  }
               }
               else {
                  ucDebounce_FullLoad_50ms = 0;
               }
               break;
            case LWD_CALIB_STATE__LEARN_FLOOR:
               if( bLearnConfirmation ) {
                  LoadWeigher_ForceClearUIRequest();
                  if(++ucDebounce_FullLoad_50ms >= 60) {
                     ucDebounce_FullLoad_50ms = 0;
                     ucCurrentLearnFloor += 1;
                     if( ucCurrentLearnFloor >= GetFP_NumFloors() ) {
                        eCalibrationState_FullLoad = LWD_CALIB_STATE__COMPLETE;
                     }
                     else {
                        eCalibrationState_FullLoad = LWD_CALIB_STATE__MOVE_TO_NEXT;
                     }
                  }
               }
               else {
                  ucDebounce_FullLoad_50ms = 0;
                  stUIRequest.eRequest = LW_REQ__SPAN;
                  stUIRequest.eCommand = LW_CMD__GENERATE;
                  stUIRequest.fWriteData = 0;
                  LoadWeigher_SetUIRequest(&stUIRequest, 0);
               }
               break;
            case LWD_CALIB_STATE__MOVE_TO_NEXT:
               if( ( !GetMotion_RunFlag() ) && ( GetOperation_CurrentFloor() == ucCurrentLearnFloor ) && ( !gstFault.bActiveFault )
                && ( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R) || ( !GetFloorOpening_Front(GetOperation_CurrentFloor()) && !GetFloorOpening_Rear(GetOperation_CurrentFloor()) ) ) ) {
                  if(++ucDebounce_FullLoad_50ms >= CALIBRATE_TRANSITION_DEBOUNCE_50MS) {
                     ucDebounce_FullLoad_50ms = 0;
                     bLearnConfirmation = 0;
                     eCalibrationState_FullLoad = LWD_CALIB_STATE__LEARN_FLOOR;
                  }
               }
               else {
                  ucDebounce_FullLoad_50ms = 0;
               }
               break;
            case LWD_CALIB_STATE__COMPLETE:
               ucDebounce_FullLoad_50ms = 0;
               ucCurrentLearnFloor = 0;
               bLearnConfirmation = 0;

               /* Modification from system where LWD requests top and bottom
                * position and the end of a learn, to one where the the C4
                * system proactively send it at the end of the learn */
               aucSendCountdown[LW_REQ__POS_BOT] = NUM_PACKET_RESEND;
               aucSendCountdown[LW_REQ__POS_TOP] = NUM_PACKET_RESEND;

               if( Param_ReadValue_1Bit(enPARAM1__LWD_TriggerLoadLearn) )
               {
                  Param_WriteValue_1Bit(enPARAM1__LWD_TriggerLoadLearn, 0);
               }
               else
               {
                  eCalibrationState_FullLoad = LWD_CALIB_STATE__NONE;
               }
               break;
            default:
               eCalibrationState_FullLoad = LWD_CALIB_STATE__NONE;
               break;
         }
      }
      else
      {
         eCalibrationState_FullLoad = LWD_CALIB_STATE__NONE;
      }
   }
   else
   {
      eCalibrationState_Zero = LWD_CALIB_STATE__NONE;
      eCalibrationState_FullLoad = LWD_CALIB_STATE__NONE;
      ucCurrentLearnFloor = 0;
      bLearnConfirmation = 0;
      ucDebounce_Zero_50ms = 0;
      ucDebounce_FullLoad_50ms = 0;
      bAutomaticRecalibrate = 0;
   }

   /* Only update last captured time if a valid time has been received.
    * Otherwise recalibration can be triggered at startup. */
   if( RTC_GetLocalTime() )
   {
      ucLastHourOfTheDay = ucHourOfDay;
   }
}

/*-----------------------------------------------------------------------------
   Returns 1 if car state allows updating the car weight
 -----------------------------------------------------------------------------*/
static void UpdateAverageTorque(void)
{
   un_type_conv unConv_Offset;
   un_type_conv unConv_Scaling;
   float fTorque = 0;
   for(uint8_t i = 0; i < NUM_LW_TORQUE_SAMPLES; i++)
   {
      fTorque += acTorqueSamples[i];
   }
   fTorque /= NUM_LW_TORQUE_SAMPLES;

   unConv_Scaling.auc[0] = Param_ReadValue_8Bit(enPARAM8__LWD_TorqueScaling);
   fTorque *= unConv_Scaling.ac[0];
   fTorque /= 100;

   unConv_Offset.auc[0] = Param_ReadValue_8Bit(enPARAM8__LWD_TorqueOffset);
   fTorque += unConv_Offset.ac[0];

   if( fTorque > 100 )
   {
      fTorque = 100;
   }
   else if( fTorque < -100 )
   {
      fTorque = -100;
   }
   fAvgTorque = fTorque;
}

/*-----------------------------------------------------------------------------
   Returns 1 if car state allows updating the car weight
 -----------------------------------------------------------------------------*/
static uint8_t CheckIfValidLoadUpdateStage(void)
{
   uint8_t bReturn = 0;
   if( ( !GetMotion_RunFlag() )
    && ( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R) )
    && ( ( GetDoorState_Front() == DOOR__OPEN )
      || ( GetDoorState_Rear() == DOOR__OPEN )
      || ( GetDoorState_Front() == DOOR__CLOSING )
      || ( GetDoorState_Rear() == DOOR__CLOSING )
      || ( ( GetDoorState_Front() == DOOR__CLOSED ) && ( !GetFP_RearDoors() || ( GetDoorState_Rear() == DOOR__CLOSED ) ) ) ) )
   {
      bReturn = 1;
   }
   else if( Param_ReadValue_1Bit(enPARAM1__Debug_LWD) )
   {
      bReturn = 1;
   }
   return bReturn;
}
/*-----------------------------------------------------------------------------
  Unloads new load flags and updates the buffered load flag signals used in other modules

  LWD torque updates expected every 250ms
 -----------------------------------------------------------------------------*/
#define LWD_LOAD_FLAGS_DEBOUNCE_250MS    (6)
static void UpdateLoadFlags(uint8_t ucFlags)
{
   static uint8_t aucCounter_250ms[NUM_LOAD_FLAG];
   for(uint8_t i = 0; i < NUM_LOAD_FLAG; i++)
   {
      uint8_t bFlag = Sys_Bit_Get(&ucFlags, i);
      if( bFlag )
      {
         if(aucCounter_250ms[i] >= LWD_LOAD_FLAGS_DEBOUNCE_250MS)
         {
            Sys_Bit_Set(&ucLoadFlags, i, 1);
         }
         else
         {
            aucCounter_250ms[i]++;
         }
      }
      else
      {
         Sys_Bit_Set(&ucLoadFlags, i, 0);
         aucCounter_250ms[i] = 0;
      }
   }

}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void LoadWeigher_UnloadDatagram(un_sdata_datagram *punDatagram)
{
   uint8_t bValidPacket = 1;
   en_lw_resp eResponse = punDatagram->auc8[0];
   en_lw_cmd eCommand = punDatagram->auc8[1];
   uint8_t ucFloor = punDatagram->auc8[2];
   float fValue = punDatagram->af32[1];
   switch(eResponse)
   {
      case LW_RESP__TORQUE:
         cLastTorque = punDatagram->auc8[4];
         eError = punDatagram->auc8[7];
         if( CheckIfValidLoadUpdateStage() )
         {
            acTorqueSamples[ucSampleIndex] = punDatagram->auc8[4];
            ucSampleIndex = (ucSampleIndex+1) % NUM_LW_TORQUE_SAMPLES;
            ucInCarWeight_50lb = punDatagram->auc8[5];  /* In car weight in 50 lb counts */
            UpdateLoadFlags(punDatagram->auc8[6]);
            UpdateAverageTorque();
         }
         break;
      case LW_RESP__POS_BOT:
         if( stUserRequest.eRequest == (en_lw_req) eResponse )
         {
            fUIRequest_ReadValue = fValue;
         }
#if 0 // causes endless loop of position requests and responses
         else
         {
            aucSendCountdown[LW_REQ__POS_BOT] = NUM_PACKET_RESEND;
         }
#endif
         uwOfflineCounter_ms = 0;
         break;
      case LW_RESP__POS_TOP:
         if( stUserRequest.eRequest == (en_lw_req) eResponse )
         {
            fUIRequest_ReadValue = fValue;
         }
#if 0 // causes endless loop of position requests and responses
         else
         {
            aucSendCountdown[LW_REQ__POS_TOP] = NUM_PACKET_RESEND;
         }
#endif
         break;
      case LW_RESP__ZERO:
         if( stUserRequest.eRequest == (en_lw_req) eResponse )
         {
            fUIRequest_ReadValue = fValue;
            if( ( eCalibrationState_Zero == LWD_CALIB_STATE__LEARN_FLOOR )
             && ( ucCurrentLearnFloor == ucFloor )
             && ( eCommand == LW_CMD__GENERATE ) )
            {
               bLearnConfirmation = 1;
            }
         }
         break;
      case LW_RESP__SPAN:
         if( stUserRequest.eRequest == (en_lw_req) eResponse )
         {
            fUIRequest_ReadValue = fValue;
            if( ( eCalibrationState_FullLoad == LWD_CALIB_STATE__LEARN_FLOOR )
             && ( ucCurrentLearnFloor == ucFloor )
             && ( eCommand == LW_CMD__GENERATE ) )
            {
               bLearnConfirmation = 1;
            }
         }
         break;
      case LW_RESP__COMP_ROPE:
      case LW_RESP__MAX_CAP:
      case LW_RESP__CW_PERCENT:
      case LW_RESP__ROPE_LB_PER_FT:
      case LW_RESP__WIFI:
      case LW_RESP__LIGHT_LOAD:
      case LW_RESP__FULL_LOAD:
      case LW_RESP__OVER_LOAD:
      case LW_RESP__ROPE_DIAM:
      case LW_RESP__ROPE_COUNT:
      case LW_RESP__EMPTY_CAR_WEIGHT:
      case LW_RESP__WEIGHT_IN_CAR:
      case LW_RESP__READ_LOAD_CELL:
      case LW_RESP__CAR_SIDE_ROPE_WEIGHT:
      case LW_RESP__CAR_SIDE_COMP_ROPE_WEIGHT:
      case LW_RESP__CW_SIDE_ROPE_WEIGHT:
      case LW_RESP__CW_SIDE_COMP_ROPE_WEIGHT:
      case LW_RESP__TORQUE_OFFSET:
      case LW_RESP__TORQUE_SCALING:
         if( stUserRequest.eRequest == (en_lw_req) eResponse )
         {
            fUIRequest_ReadValue = fValue;
         }
         break;
      default:
         bValidPacket = 0;
         break;
   }

   if(bValidPacket)
   {
      uwOfflineCounter_ms = 0;
      uwReceiveCounter++;
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData( struct st_module *pstThisModule )
{
   static en_lw_req eLastRequest;
   static uint16_t uwResendCounter_Torque_ms;
   static uint16_t uwResendCounter_WiFi_ms;
   static uint16_t uwResendCounter_Request_ms;

   /* Check for UI Request */
   if( stUserRequest.eRequest != LW_REQ__TORQUE )
   {
      if(uwResendCounter_Request_ms < MAX_UI_REQ_RESEND_DELAY_MS)
      {
         uwResendCounter_Request_ms += pstThisModule->uwRunPeriod_1ms;
      }
      else
      {
#if 1
         aucSendCountdown[stUserRequest.eRequest] = 1;
         uwResendCounter_Request_ms = 0;
#else
         if( stUserRequest.ucSendCountdown )
         {
            stUserRequest.ucSendCountdown--;
            aucSendCountdown[stUserRequest.eRequest] = 1;
            uwResendCounter_Request_ms = 0;
         }
#endif
      }
   }
   else
   {
      /* Mark Torque packet for resend */
      if( !aucSendCountdown[LW_REQ__TORQUE] )
      {
         if(uwResendCounter_Torque_ms < MIN_TORQUE_RESEND_DELAY_MS)
         {
            uwResendCounter_Torque_ms += pstThisModule->uwRunPeriod_1ms;
         }
         else
         {
            aucSendCountdown[LW_REQ__TORQUE] = 1;
            uwResendCounter_Torque_ms = 0;
         }
      }
      else
      {
         uwResendCounter_Torque_ms = 0;
      }

      /* Mark WiFi message for transmit */
      uint8_t bWifi = ( Param_ReadValue_1Bit(enPARAM1__LWD_EnableWifi) )
                   && ( !eCalibrationState_Zero ) // Disable wifi during calibration
                   && ( !eCalibrationState_FullLoad );
      if(bWifi != bWiFiFlag)
      {
         aucSendCountdown[LW_REQ__WIFI] = 1;
      }
      bWiFiFlag = bWifi;

      if( !aucSendCountdown[LW_REQ__WIFI] )
      {
         if(uwResendCounter_WiFi_ms < MIN_WIFI_RESEND_DELAY_MS)
         {
            uwResendCounter_WiFi_ms += pstThisModule->uwRunPeriod_1ms;
         }
         else
         {
            aucSendCountdown[LW_REQ__WIFI] = 1;
            uwResendCounter_WiFi_ms = 0;
         }
      }
      else
      {
         uwResendCounter_WiFi_ms = 0;
      }
   }

   if( !SDATA_DirtyBit_Get(gpastSData_Nodes_AuxNet[MR_AUX_NET__MRB], DG_AuxNet_MRB__LoadWeigher) )
   {
      uint8_t bSuppressTorqueRequests = ( LoadWeigher_GetCalibrationState() != LWD_CALIB_STATE__NONE );
      /* Search for datagrams marked for transmit */
      en_lw_req eRequest = NUM_LW_REQ;
      for(uint8_t i = 0; i < NUM_LW_REQ; i++)
      {
         en_lw_req eCheck = (eLastRequest+i+1) % NUM_LW_REQ;
         if( ( ( eCheck != LW_REQ__TORQUE ) || !bSuppressTorqueRequests )
          && ( aucSendCountdown[eCheck] ) )
         {
            eRequest = eCheck;
            break;
         }
      }

      /* Load new packet to transmit if found */
      if(eRequest < NUM_LW_REQ)
      {
         un_sdata_datagram unDatagram;
         memset(&unDatagram, 0, sizeof(un_sdata_datagram));
         unDatagram.auc8[0] = eRequest;
         unDatagram.auc8[2] = GetOperation_CurrentFloor();
         switch(eRequest)
         {
            case LW_REQ__TORQUE:
               unDatagram.auc8[1] = LW_CMD__READ;
               unDatagram.auc8[3] = 1;//Param_ReadValue_1Bit(enPARAM1__LWD_TorqueV2); // Always use V2
               unDatagram.aui32[1] = Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0 + GetOperation_CurrentFloor());
               break;
            case LW_REQ__ZERO:
            case LW_REQ__SPAN:
            case LW_REQ__COMP_ROPE:
            case LW_REQ__MAX_CAP:
            case LW_REQ__CW_PERCENT:
            case LW_REQ__ROPE_LB_PER_FT:
            case LW_REQ__LIGHT_LOAD:
            case LW_REQ__FULL_LOAD:
            case LW_REQ__OVER_LOAD:
            case LW_REQ__ROPE_DIAM:
            case LW_REQ__ROPE_COUNT:
            case LW_REQ__EMPTY_CAR_WEIGHT:
            case LW_REQ__TORQUE_OFFSET:
            case LW_REQ__TORQUE_SCALING:
               unDatagram.auc8[1] = ( stUserRequest.eRequest == eRequest ) ? stUserRequest.eCommand:LW_CMD__READ;
               unDatagram.af32[1] = ( stUserRequest.eCommand != LW_CMD__READ ) ? stUserRequest.fWriteData:0;
               break;
            case LW_REQ__POS_BOT:
               unDatagram.auc8[1] = ( stUserRequest.eRequest == eRequest ) ? stUserRequest.eCommand:LW_CMD__WRITE;
               unDatagram.auc8[2] = 0; // Spoof current floor
               unDatagram.auc8[3] = 0; // Needs to be set for older software
               unDatagram.aui32[1] = Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0);
               break;
            case LW_REQ__POS_TOP:
               unDatagram.auc8[1] = ( stUserRequest.eRequest == eRequest ) ? stUserRequest.eCommand:LW_CMD__WRITE;
               unDatagram.auc8[2] = GetFP_NumFloors()-1; // Spoof current floor
               unDatagram.auc8[3] = GetFP_NumFloors()-1; // Needs to be set for older software
               unDatagram.aui32[1] = Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0 + GetFP_NumFloors()-1);
               break;
            case LW_REQ__WIFI:
               unDatagram.auc8[1] = ( stUserRequest.eRequest == eRequest ) ? LW_CMD__READ:LW_CMD__WRITE;
               unDatagram.aui32[1] = bWiFiFlag;
               break;
            case LW_REQ__WEIGHT_IN_CAR:
            case LW_REQ__READ_LOAD_CELL:
            case LW_REQ__CAR_SIDE_ROPE_WEIGHT:
            case LW_REQ__CAR_SIDE_COMP_ROPE_WEIGHT:
            case LW_REQ__CW_SIDE_ROPE_WEIGHT:
            case LW_REQ__CW_SIDE_COMP_ROPE_WEIGHT:
               unDatagram.auc8[1] = LW_CMD__READ;
               break;
            default: break;
         }

         SDATA_WriteDatagram( gpastSData_Nodes_AuxNet[MR_AUX_NET__MRB],
                              DG_AuxNet_MRB__LoadWeigher,
                              &unDatagram );
         SDATA_DirtyBit_Set( gpastSData_Nodes_AuxNet[MR_AUX_NET__MRB],
                             DG_AuxNet_MRB__LoadWeigher );
         aucSendCountdown[eRequest]--;
      }
   }
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 6000;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_LOAD_WEIGHER_1MS;

   aucSendCountdown[LW_REQ__WIFI] = NUM_PACKET_RESEND;
   aucSendCountdown[LW_REQ__POS_BOT] = NUM_PACKET_RESEND;
   aucSendCountdown[LW_REQ__POS_TOP] = NUM_PACKET_RESEND;

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   if( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__MR )
   {
      CalibrateLWD();

      LoadData(pstThisModule);

      LoadWeigher_UpdateOfflineCounter(pstThisModule->uwRunPeriod_1ms);

      if( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
       && ( GetOperation_AutoMode() != MODE_A__FIRE2 ) // A17 2.27.3.3.1 (l)
       && ( !GetMotion_RunFlag() )
       && ( LoadWeigher_GetLoadFlag(LOAD_FLAG__OVER_LOAD) )
       && ( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R) )
       && ( !eCalibrationState_Zero )
       && ( !eCalibrationState_FullLoad )
       && ( !Param_ReadValue_1Bit(enPARAM1__Debug_LWD) )
	   && ((GetOperation_AutoMode() != MODE_A__SABBATH) || !Param_ReadValue_1Bit(enPARAM1__SabbathDisableLWD)))
      {
         SetFault(FLT__OVERLOADED);
      }
   }

   LoadWeigher_UpdateUIRequest();

   if( ( GetSRU_Deployment() == enSRU_DEPLOYMENT__MR )
    && ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
    && ( GetOperation_AutoMode() != MODE_A__FIRE2 ) // A17 2.27.3.3.1 (l)
    && ( !GetMotion_RunFlag() )
    && ( GetInputValue(enIN_OVER_LOAD) )
    && ( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R) )
	&& ((GetOperation_AutoMode() != MODE_A__SABBATH) || !Param_ReadValue_1Bit(enPARAM1__SabbathDisableLWD)))
   {
      SetFault(FLT__OVERLOADED);
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
