/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief
 * @version  V1.00
 * @date     27, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>

#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "masterCommand.h"
#include "hallData.h"
#include "carData.h"
#include "carDestination.h"
#include "group_net_data.h"
#include "XRegData.h"
#include "XRegDestination.h"
#include "hallSecurity.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static st_xreg_car_data astXRegCarData[MAX_GROUP_CARS];
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Initialize XREGData
   ucCarIndex < 8
 -----------------------------------------------------------------------------*/
void XRegData_Init(uint8_t ucCarIndex)
{
   memset(&astXRegCarData[ucCarIndex], 0, sizeof(st_xreg_car_data));
   astXRegCarData[ucCarIndex].uwOfflineCounter_ms = XREG_CAR_OFFLINE_INACTIVE_MS;
}
/*-----------------------------------------------------------------------------
   Updates XREG car's bCarOnline flag.
   uiModRunPeriod_ms = run period of the module calling this function
   ucCarIndex < 8
 -----------------------------------------------------------------------------*/
void XRegData_UpdateCarOnlineFlag(uint8_t ucCarIndex, uint16_t uwModRunPeriod_ms)
{
   if(astXRegCarData[ucCarIndex].uwOfflineCounter_ms < XREG_CAR_OFFLINE_LIMIT_MS)
   {
      astXRegCarData[ucCarIndex].uwOfflineCounter_ms += uwModRunPeriod_ms;
      astXRegCarData[ucCarIndex].bCarOnline = 1;
   }
   else
   {
      astXRegCarData[ucCarIndex].bCarOnline = 0;
   }
}
/*-----------------------------------------------------------------------------
   Updates XREG car's first and last landing variables
 -----------------------------------------------------------------------------*/
void XRegData_UpdateFirstLastLanding()
{
   for(uint8_t ucCarIndex = 0; ucCarIndex < MAX_GROUP_CARS; ucCarIndex++)
   {
      uint8_t ucFirstLanding_Plus1 = 0;
      uint8_t ucLastLanding = 0;
      if( XRegData_GetCarActive( ucCarIndex ) )
      {
         for(uint8_t ucLanding = 0; ucLanding < MAX_NUM_FLOORS; ucLanding++)
         {
            if( Sys_Bit_Get(&astXRegCarData[ucCarIndex].aucBF_LandingMap[0], ucLanding) )
            {
               if( !ucFirstLanding_Plus1 )
               {
                  ucFirstLanding_Plus1 = ucLanding+1;
                  astXRegCarData[ucCarIndex].ucFirstLanding = ucLanding;
               }
               ucLastLanding = ucLanding;
            }
         }
         astXRegCarData[ucCarIndex].ucLastLanding = ucLastLanding;
      }
   }
}

/*-----------------------------------------------------------------------------
   Temporarily removed cars from group
   if destination not cleared by the adjustable timeout period
 -----------------------------------------------------------------------------*/
void XRegData_CheckForDispatchingTimeout(uint16_t uwRunPeriod_ms)
{
   static uint32_t auiOfflineCountdown_ms[MAX_GROUP_CARS];
   if( ( Param_ReadValue_8Bit(enPARAM8__XREG_DestinationTimeout_10sec) )
    && ( Param_ReadValue_8Bit(enPARAM8__XREG_DestinationOffline_10sec) ) )
   {
      for( uint8_t i = 0; i < MAX_GROUP_CARS; i++ )
      {
         uint32_t uiTimeoutLimit_ms = Param_ReadValue_8Bit(enPARAM8__XREG_DestinationTimeout_10sec) * 10000;
         if( XRegDestination_GetLanding(i) < MAX_NUM_FLOORS )
         {
            if( XRegDestination_GetTimeout(i) < uiTimeoutLimit_ms )
            {
               XRegDestination_IncrementTimeout(i, uwRunPeriod_ms);
            }
            else
            {
               // Take car out of group for the adjustable period, to allow the call to be reassigned.
               auiOfflineCountdown_ms[i] = Param_ReadValue_8Bit(enPARAM8__XREG_DestinationOffline_10sec) * 10000;
               XRegDestination_ClrTimeout(i);

               en_alarms eAlarm = ALM__DISPATCH_TIMEOUT_X1 + i;
               if( eAlarm > ALM__DISPATCH_TIMEOUT_X8 )
               {
                  eAlarm = ALM__DISPATCH_TIMEOUT_X8;
               }
               SetAlarm( eAlarm );
            }
         }

         if( auiOfflineCountdown_ms[i] >= uwRunPeriod_ms )
         {
            // Take car out of group until countdown has expired
            auiOfflineCountdown_ms[i] -= uwRunPeriod_ms;
            astXRegCarData[i].bRemovedFromGroup = 1;
         }
         else
         {
            astXRegCarData[i].bRemovedFromGroup = 0;
         }
      }
   }
   else
   {
      for( uint8_t i = 0; i < MAX_GROUP_CARS; i++ )
      {
         astXRegCarData[i].bRemovedFromGroup = 0;
         auiOfflineCountdown_ms[i] = 0;
      }
   }
}
/*-----------------------------------------------------------------------------
   Access variables
   ucCarIndex < 8
 -----------------------------------------------------------------------------*/
uint8_t XRegData_GetCarActive(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].bCarOnline
       && astXRegCarData[ucCarIndex].bInGroup
       && !astXRegCarData[ucCarIndex].bRemovedFromGroup;
}
enum en_classop XRegData_GetClassOp(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].eClassOp;
}
enum en_mode_auto XRegData_GetAutoMode(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].eAutoMode;
}
uint8_t XRegData_GetCurrentLanding(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].ucLandingIndex;
}
enum direction_enum XRegData_GetMotionDirection(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].cMotionDir;
}
enum direction_enum XRegData_GetArrivalDirection(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].cArrivalDir;
}
uint8_t XRegData_GetOpening_Front(uint8_t ucCarIndex, uint8_t ucLanding)
{
   uint8_t bActive = Sys_Bit_Get(&astXRegCarData[ucCarIndex].aucBF_FrontMap[0], ucLanding);
   return bActive;
}
uint8_t XRegData_GetOpening_Rear(uint8_t ucCarIndex, uint8_t ucLanding)
{
   uint8_t bActive = Sys_Bit_Get(&astXRegCarData[ucCarIndex].aucBF_RearMap[0], ucLanding);
   return bActive;
}
uint8_t XRegData_GetFirstLanding(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].ucFirstLanding;
}
uint8_t XRegData_GetLastLanding(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].ucLastLanding;
}
en_hc_dir XRegData_GetPriority(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].enPriority;
}
uint32_t XRegData_GetHallMask_F(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].uiHallMask_F;
}
uint32_t XRegData_GetHallMask_R(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].uiHallMask_R;
}
uint8_t XRegData_GetCarOnlineFlag(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].bCarOnline;
}
uint8_t XRegData_GetOpeningMap_F(uint8_t ucCarIndex, uint8_t ucIndex)
{
   return astXRegCarData[ucCarIndex].aucBF_FrontMap[ucIndex]
        & astXRegCarData[ucCarIndex].aucBF_LandingMap[ucIndex];
}
uint8_t XRegData_GetOpeningMap_R(uint8_t ucCarIndex, uint8_t ucIndex)
{
   return astXRegCarData[ucCarIndex].aucBF_RearMap[ucIndex]
        & astXRegCarData[ucCarIndex].aucBF_LandingMap[ucIndex];
}
uint8_t XRegData_GetGSW_F(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].bGSW_F;
}
uint8_t XRegData_GetGSW_R(uint8_t ucCarIndex)
{
   return astXRegCarData[ucCarIndex].bGSW_R;
}
/*-----------------------------------------------------------------------------
   Get latchable hall mask for XReg boards
 -----------------------------------------------------------------------------*/
uint16_t XRegData_GetGroupHallCallMask()
{
   uint16_t uwMask = 0;
   uint8_t ucNumXRegCars = Param_ReadValue_8Bit(enPARAM8__NumXRegCars);
   if( ucNumXRegCars && ( ucNumXRegCars <= MAX_GROUP_CARS ) )
   {
      for(uint8_t i = 0; i < ucNumXRegCars; i++)
      {
         if( XRegData_GetCarActive(i) )
         {
            uwMask |= XRegData_GetHallMask_F(i) | XRegData_GetHallMask_R(i);
         }
      }
   }
   return uwMask;
}
/*-----------------------------------------------------------------------------
   Set Variables
 -----------------------------------------------------------------------------*/
void XRegData_SetPriority(uint8_t ucCarIndex, en_hc_dir eDir)
{
   astXRegCarData[ucCarIndex].enPriority = eDir;
}
/*-----------------------------------------------------------------------------
todo revisit cost of checks
 -----------------------------------------------------------------------------*/
uint8_t GetLatchedHallCallUp_Front_ByXRegCar( uint8_t ucCarID, uint8_t ucLanding )
{
   uint8_t bLatched = 0;
   uint8_t bValidOpening = XRegData_GetOpening_Front(ucCarID, ucLanding);
   uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__UP );

   // mask out calls already assigned
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      st_carDestination *pstCarDestination = GetCarDestinationStructure( i );
      if( ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
       && ( pstCarDestination->ucLanding == ucLanding )
       && ( pstCarDestination->eCallDir == DIR__UP )
       && ( pstCarDestination->eDoor == DOOR_FRONT ) )
      {
         uiLatchedCalls &= ~(pstCarDestination->uiMask);
      }
   }
   // Mask out XREG calls already assigned
   uint8_t ucNumXRegCars = Param_ReadValue_8Bit(enPARAM8__NumXRegCars);
   for(uint8_t i = 0; i < ucNumXRegCars; i++)
   {
      if( ( i != ucCarID )
       && ( XRegData_GetCarActive(i) )
       && ( XRegDestination_GetLanding(i) == ucLanding )
       && ( XRegDestination_GetDirection(i) == DIR__UP )
       && ( XRegDestination_GetDoor(i) == DOOR_FRONT ) )
      {
         uiLatchedCalls &= ~(XRegDestination_GetMask(i));
      }
   }

   // Look only at calls this car can take
   uiLatchedCalls &= XRegData_GetHallMask_F(ucCarID);
   if( uiLatchedCalls
    && bValidOpening )
   {
      bLatched = 1;
   }
   return bLatched;
}
uint8_t GetLatchedHallCallUp_Rear_ByXRegCar( uint8_t ucCarID, uint8_t ucLanding )
{
   uint8_t bLatched = 0;
   uint8_t bValidOpening = XRegData_GetOpening_Rear(ucCarID, ucLanding);
   uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__UP );

   // mask out calls already assigned
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      st_carDestination *pstCarDestination = GetCarDestinationStructure( i );
      if( ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
       && ( pstCarDestination->ucLanding == ucLanding )
       && ( pstCarDestination->eCallDir == DIR__UP )
       && ( pstCarDestination->eDoor == DOOR_REAR ) )
      {
         uiLatchedCalls &= ~(pstCarDestination->uiMask);
      }
   }
   // Mask out XREG calls already assigned
   uint8_t ucNumXRegCars = Param_ReadValue_8Bit(enPARAM8__NumXRegCars);
   for(uint8_t i = 0; i < ucNumXRegCars; i++)
   {
      if( ( i != ucCarID )
       && ( XRegData_GetCarActive(i) )
       && ( XRegDestination_GetLanding(i) == ucLanding )
       && ( XRegDestination_GetDirection(i) == DIR__UP )
       && ( XRegDestination_GetDoor(i) == DOOR_REAR ) )
      {
         uiLatchedCalls &= ~(XRegDestination_GetMask(i));
      }
   }

   // Look only at calls this car can take
   uiLatchedCalls &= XRegData_GetHallMask_R(ucCarID);
   if( uiLatchedCalls
    && bValidOpening )
   {
      bLatched = 1;
   }
   return bLatched;
}
uint8_t GetLatchedHallCallDown_Front_ByXRegCar( uint8_t ucCarID, uint8_t ucLanding )
{
   uint8_t bLatched = 0;
   uint8_t bValidOpening = XRegData_GetOpening_Front(ucCarID, ucLanding);
   uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__DOWN );

   // mask out calls already assigned
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      st_carDestination *pstCarDestination = GetCarDestinationStructure( i );
      if( ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
       && ( pstCarDestination->ucLanding == ucLanding )
       && ( pstCarDestination->eCallDir == DIR__DN )
       && ( pstCarDestination->eDoor == DOOR_FRONT ) )
      {
         uiLatchedCalls &= ~(pstCarDestination->uiMask);
      }
   }
   // Mask out XREG calls already assigned
   uint8_t ucNumXRegCars = Param_ReadValue_8Bit(enPARAM8__NumXRegCars);
   for(uint8_t i = 0; i < ucNumXRegCars; i++)
   {
      if( ( i != ucCarID )
       && ( XRegData_GetCarActive(i) )
       && ( XRegDestination_GetLanding(i) == ucLanding )
       && ( XRegDestination_GetDirection(i) == DIR__DN )
       && ( XRegDestination_GetDoor(i) == DOOR_FRONT ) )
      {
         uiLatchedCalls &= ~(XRegDestination_GetMask(i));
      }
   }

   // Look only at calls this car can take
   uiLatchedCalls &= XRegData_GetHallMask_F(ucCarID);
   if( uiLatchedCalls
    && bValidOpening )
   {
      bLatched = 1;
   }
   return bLatched;
}
uint8_t GetLatchedHallCallDown_Rear_ByXRegCar( uint8_t ucCarID, uint8_t ucLanding )
{
   uint8_t bLatched = 0;
   uint8_t bValidOpening = XRegData_GetOpening_Rear(ucCarID, ucLanding);
   uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__DOWN );

   // mask out calls already assigned
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      st_carDestination *pstCarDestination = GetCarDestinationStructure( i );
      if( ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
       && ( pstCarDestination->ucLanding == ucLanding )
       && ( pstCarDestination->eCallDir == DIR__DN )
       && ( pstCarDestination->eDoor == DOOR_REAR ) )
      {
         uiLatchedCalls &= ~(pstCarDestination->uiMask);
      }
   }
   // Mask out XREG calls already assigned
   uint8_t ucNumXRegCars = Param_ReadValue_8Bit(enPARAM8__NumXRegCars);
   for(uint8_t i = 0; i < ucNumXRegCars; i++)
   {
      if( ( i != ucCarID )
       && ( XRegData_GetCarActive(i) )
       && ( XRegDestination_GetLanding(i) == ucLanding )
       && ( XRegDestination_GetDirection(i) == DIR__DN )
       && ( XRegDestination_GetDoor(i) == DOOR_REAR ) )
      {
         uiLatchedCalls &= ~(XRegDestination_GetMask(i));
      }
   }

   // Look only at calls this car can take
   uiLatchedCalls &= XRegData_GetHallMask_R(ucCarID);
   if( uiLatchedCalls
    && bValidOpening )
   {
      bLatched = 1;
   }
   return bLatched;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_CarStatus( un_sdata_datagram *punDatagram )
{
   uint8_t ucCarID = punDatagram->auc8[1];
   enum en_classop eClassOp = punDatagram->auc8[2];
   enum en_mode_auto eAutoMode = punDatagram->auc8[3];
   uint8_t bGSW_F = (punDatagram->auc8[5] & 0x01) ? 1:0;
   uint8_t bGSW_R = (punDatagram->auc8[5] & 0x02) ? 1:0;
   uint8_t bTrvUp = (punDatagram->auc8[5] & 0x04) ? 1:0;
   uint8_t bTrvDown = (punDatagram->auc8[5] & 0x08) ? 1:0;
   uint8_t bArvUp = (punDatagram->auc8[5] & 0x10) ? 1:0;
   uint8_t bArvDown = (punDatagram->auc8[5] & 0x20) ? 1:0;
   uint8_t bInGroup = (punDatagram->auc8[5] & 0x40) ? 1:0;
   uint8_t ucFloor_Plus1 = punDatagram->auc8[4];
   if( ( ucCarID < MAX_GROUP_CARS )
    && ( ucFloor_Plus1 )
    && ( ucFloor_Plus1 <= MAX_NUM_FLOORS ) )
   {
      astXRegCarData[ucCarID].eClassOp = eClassOp;
      astXRegCarData[ucCarID].eAutoMode = eAutoMode;
      astXRegCarData[ucCarID].ucLandingIndex = ucFloor_Plus1-1;

      if(bTrvUp)
      {
         astXRegCarData[ucCarID].cMotionDir = DIR__UP;
      }
      else if(bTrvDown)
      {
         astXRegCarData[ucCarID].cMotionDir = DIR__DN;
      }
      else
      {
         astXRegCarData[ucCarID].cMotionDir = DIR__NONE;
      }

      if(bArvUp)
      {
         astXRegCarData[ucCarID].cArrivalDir = DIR__UP;
         if( Param_ReadValue_1Bit(enPARAM1__XREG_PriorityFromArrivalDir) )
         {
            astXRegCarData[ucCarID].enPriority = HC_DIR__UP;
         }
      }
      else if(bArvDown)
      {
         astXRegCarData[ucCarID].cArrivalDir = DIR__DN;
         if( Param_ReadValue_1Bit(enPARAM1__XREG_PriorityFromArrivalDir) )
         {
            astXRegCarData[ucCarID].enPriority = HC_DIR__DOWN;
         }
      }
      else
      {
         astXRegCarData[ucCarID].cArrivalDir = DIR__NONE;
      }

      astXRegCarData[ucCarID].bInGroup = bInGroup;

      if( !Param_ReadValue_1Bit(enPARAM1__XREG_PriorityFromArrivalDir) )
      {
         astXRegCarData[ucCarID].enPriority = ucCarID % 2;
      }

      astXRegCarData[ucCarID].bGSW_F = bGSW_F;
      astXRegCarData[ucCarID].bGSW_R = bGSW_R;

      astXRegCarData[ucCarID].uwOfflineCounter_ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_LatchedHallCall( un_sdata_datagram *punDatagram )
{
   uint8_t ucCarID = punDatagram->auc8[1];
   enum en_doors eDoor = punDatagram->auc8[2];
   en_hc_dir eCallDir = punDatagram->auc8[3];
   uint8_t ucLanding = INVALID_LANDING;
   if( ( punDatagram->auc8[4] )// ucFloor_Plus1
    && ( ucCarID < MAX_GROUP_CARS ) )
   {
      ucLanding = punDatagram->auc8[4]-1;
      enum direction_enum eDir = XRegDestination_GetDirection(ucCarID);
      if( ( XRegDestination_GetLanding(ucCarID) == ucLanding )
       && ( XRegDestination_GetDoor(ucCarID) == eDoor ) )
      {
         if( ( eDir == DIR__UP )
          && ( eCallDir == HC_DIR__UP ) )
         {
            XRegDestination_ClrSendCountdown(ucCarID);
            XRegDestination_SetCallAccepted(ucCarID);
         }
         else if( ( eDir == DIR__DN )
               && ( eCallDir == HC_DIR__DOWN ) )
         {
            XRegDestination_ClrSendCountdown(ucCarID);
            XRegDestination_SetCallAccepted(ucCarID);
         }
      }

      astXRegCarData[ucCarID].uwOfflineCounter_ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_ClearHallCall( un_sdata_datagram *punDatagram )
{
   uint8_t ucCarID = punDatagram->auc8[1];
   enum en_doors eDoor = punDatagram->auc8[2];
   en_hc_dir eCallDir = punDatagram->auc8[3];
   uint8_t ucLanding = INVALID_LANDING;

   if( ( punDatagram->auc8[4] ) // ucFloor_Plus1
    && ( ucCarID < MAX_GROUP_CARS ) )
   {
      ucLanding = punDatagram->auc8[4]-1;
      if(eDoor == DOOR_FRONT)
      {
         ClrRawLatchedHallCalls( ucLanding, eCallDir, XRegData_GetHallMask_F(ucCarID) );
      }
      else
      {
         ClrRawLatchedHallCalls( ucLanding, eCallDir, XRegData_GetHallMask_R(ucCarID) );
      }

      astXRegCarData[ucCarID].uwOfflineCounter_ms = 0;
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_FloorDoorMap( un_sdata_datagram *punDatagram )
{
   uint8_t ucCarID = punDatagram->auc8[1];
   uint8_t ucFloor_Plus1 = punDatagram->auc8[2];
   uint8_t ucFloorMap = punDatagram->auc8[3];
   uint8_t ucFrontMap = punDatagram->auc8[4];
   uint8_t ucRearMap = punDatagram->auc8[5];
   uint8_t ucBitmapIndex = (ucFloor_Plus1-1) / 8;
   if( ( ucCarID < MAX_GROUP_CARS )
    && ( ucFloor_Plus1 )
    && ( ucBitmapIndex < BITMAP8_SIZE(MAX_NUM_FLOORS) ) )
   {
      uint8_t ucBitmapIndex = (ucFloor_Plus1-1) / 8;
      astXRegCarData[ucCarID].aucBF_LandingMap[ucBitmapIndex] = ucFloorMap;
      astXRegCarData[ucCarID].aucBF_FrontMap[ucBitmapIndex] = ucFrontMap;
      astXRegCarData[ucCarID].aucBF_RearMap[ucBitmapIndex] = ucRearMap;

      astXRegCarData[ucCarID].uwOfflineCounter_ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_HallCallMask( un_sdata_datagram *punDatagram )
{
   uint16_t uwHallMask_F = (punDatagram->auc8[1])
                         | (punDatagram->auc8[2] << 8);
   uint16_t uwHallMask_R = (punDatagram->auc8[3])
                         | (punDatagram->auc8[4] << 8);

   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      astXRegCarData[i].uiHallMask_F = uwHallMask_F;
      astXRegCarData[i].uiHallMask_R = uwHallMask_R;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UnloadDatagram_XRegStatus( un_sdata_datagram *punDatagram )
{
   en_xreg_stat eStatus = punDatagram->auc8[0];
   switch( eStatus )
   {
      case XREG_STAT__CAR_STATUS:
         Unload_CarStatus(punDatagram);
         break;
      case XREG_STAT__FIRE_EP:
         break;
      case XREG_STAT__LATCHED_HC:
         Unload_LatchedHallCall(punDatagram);
         break;
      case XREG_STAT__CLEAR_HC:
         Unload_ClearHallCall(punDatagram);
         break;
      case XREG_STAT__FLOOR_DOOR_MAP:
         Unload_FloorDoorMap(punDatagram);
         break;
      case XREG_STAT__HALL_CALL_MASK:
         Unload_HallCallMask(punDatagram);
         break;

      default:
         break;

   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
