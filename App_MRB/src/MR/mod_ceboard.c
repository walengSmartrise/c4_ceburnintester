/******************************************************************************
 *
 * @file
 * @brief
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_b.h"
#include "sru.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
#include "motion.h"
#include "group_net_data.h"
#include "contributors.h"
#include "carData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_CEboards =
{
   .pfnInit = Init,
   .pfnRun = Run,
};
extern struct st_sdata_control * gpastSData_Nodes_AuxNet[ NUM_MR_AUX_NET_NODES ];
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
struct st_ce_data_buffer
{
   uint8_t bArrivalF_DN;
   uint8_t bArrivalF_UP;

   uint8_t bArrivalR_DN;
   uint8_t bArrivalR_UP;

   uint8_t bTravel_UP;
   uint8_t bTravel_DN;

   uint8_t bStrobe;
   uint8_t bChime;
   uint8_t bFire;
   uint8_t ucMessageNum;
   uint8_t ucSlot;

   uint8_t ucCharL;
   uint8_t ucCharM;
   uint8_t ucCharR;
};

enum en_ce_messages
{
   CE_MSG__NONE,
   CE_MSG__FIRE_MAIN,//Enabled by DIP5, MF
   CE_MSG__FIRE_ALT,//Enabled by DIP5, AF
   CE_MSG__NUDGE,//Enabled by DIP5, DN
   CE_MSG__INDP_SRV,//Enabled by DIP5, NI
   CE_MSG__OVERLOAD,//Enabled by DIP5, LO
   CE_MSG__EPOWER,//Enabled by DIP5, PE
   CE_MSG__INSP,//Enabled by DIP5, SI
   CE_MSG__SEISMIC,//Enabled by DIP5, SS
   CE_MSG__OOS,//Enabled by DIP5, OS
   CE_MSG__FF2,//Enabled by DIP5, F2

   NUM_CE_MSG
};
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_ce_data_buffer stCE_DataBuffer;
/*Message levels From CE documentation*/
static uint8_t gaucMsgLevel[NUM_CE_MSG] =
{
      0,
      3,
      2,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
};

/* Message strings from CE documentation */
static const char * gpas_MsgStrings[NUM_CE_MSG] =
{
      "[[[",
      "FM",
      "FA",
      "ND",
      "IN",
      "OL",
      "EP",
      "IS",
      "SS",
      "OS",
      "F2",
};

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

   Egg: In car check for seeing if the elevator you're in is Smartrise C4.

   Press DOB & DCB together 3 times in succession, then hold both in for 3 seconds.
 -----------------------------------------------------------------------------*/
static uint8_t bSmartriseFlag;
static char aucSmartriseString[4];
static void UpdateSmartriseFlag(void)
{
   static char const * const pasStrings[ NUM_C4C ] =
   {
      C4C_TABLE(EXPAND_C4C_TABLE_AS_STRING_ARRAY)
   };
   static uint8_t bLastActive;
   static uint8_t ucToggleCounter;
   static uint32_t uiResetTimer_5ms;
   static uint16_t uwHoldTimer_5ms;
   static uint8_t ucState;
   if( GetOperation_AutoMode() == MODE_A__NORMAL )
   {
      if(bSmartriseFlag)
      {
         if(++uwHoldTimer_5ms >= FLAG_SEQUENCE_FLASH_RATE_5MS)
         {
            uwHoldTimer_5ms = 0;
            bLastActive ^= 1;
            ucToggleCounter = (bLastActive) ? ucToggleCounter+1:ucToggleCounter;
         }

         if(!ucState)
         {
            if(bLastActive)
            {
               aucSmartriseString[2] = 'R';
               aucSmartriseString[1] = 'S';
               aucSmartriseString[0] = ' ';
            }
            else
            {
               aucSmartriseString[2] = ' ';
               aucSmartriseString[1] = ' ';
               aucSmartriseString[0] = ' ';
            }

            if(ucToggleCounter >= FLAG_SEQUENCE_CYCLES_TO_FLASH)
            {
               ucToggleCounter = 0;
               ucState = 1;
               uiResetTimer_5ms = 0;
            }
         }
         else if(ucState < NUM_C4C+1)
         {
            if(bLastActive)
            {
               aucSmartriseString[2] = *(pasStrings[ucState-1]+2);
               aucSmartriseString[1] = *(pasStrings[ucState-1]+1);
               aucSmartriseString[0] = *(pasStrings[ucState-1]+0);
            }
            else
            {
               aucSmartriseString[2] = ' ';
               aucSmartriseString[1] = ' ';
               aucSmartriseString[0] = ' ';
            }

            if(ucToggleCounter >= FLAG_SEQUENCE_CYCLES_TO_FLASH/2)
            {
               ucToggleCounter = 0;
               ucState++;
            }
         }
         else
         {
            ucState = 0;
            bSmartriseFlag = 0;
         }
      }
      else
      {
         uint8_t bActive = GetInputValue(enIN_DOB_F) && GetInputValue(enIN_DCB_F);
         bActive |= GetInputValue(enIN_DOB_R) && GetInputValue(enIN_DCB_R);
         if( bActive )
         {
            uiResetTimer_5ms = 0;
            if(!bLastActive)
            {
               ucToggleCounter++;
            }
            else if( ucToggleCounter > FLAG_SEQUENCE_PRESS_LIMIT )
            {
               if( ++uwHoldTimer_5ms >= FLAG_SEQUENCE_HOLD_TIME_5MS )
               {
                  bSmartriseFlag = 1;
                  uiResetTimer_5ms = 0;
                  ucToggleCounter = 0;
                  uwHoldTimer_5ms = 0;
                  ucState = 0;
               }
            }
         }
         else
         {
            uwHoldTimer_5ms = 0;
         }
         bLastActive = bActive;

         if(++uiResetTimer_5ms >= FLAG_SEQUENCE_RESET_LIMIT_5MS)
         {
            uiResetTimer_5ms = 0;
            ucToggleCounter = 0;
            uwHoldTimer_5ms = 0;
         }
      }
   }
}
/*-----------------------------------------------------------------------------
(*ps) = PI Left Char
(*ps+1) = PI Right Char
 -----------------------------------------------------------------------------*/
char * Get_PI_Label( uint8_t ucFloor )
{
   static char aucChar[4];
   if(bSmartriseFlag)
   {
      return &aucSmartriseString[0];
   }
   else
   {
      uint32_t ulData = Param_ReadValue_24Bit(enPARAM24__PI_0 + ucFloor);
      aucChar[2] = ulData & 0xFF;
      aucChar[1] = ulData >> 8 & 0xFF;
      aucChar[0] = ulData >> 16 & 0xFF;
      aucChar[3] = 0;
      return &aucChar[0];
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define DOUBLE_CHIME_ARRIVAL_DOWN_ON_TIME_5MS         (100)
#define DOUBLE_CHIME_ARRIVAL_DOWN_OFF_TIME_5MS        (100)
static void Update_ArrivalArrows(void)
{
   static uint8_t aucDoubleChimeDownCounter_ON_5ms[NUM_OF_DOORS];
   static uint8_t aucDoubleChimeDownCounter_OFF_5ms[NUM_OF_DOORS];
   stCE_DataBuffer.bArrivalF_UP = GetOutputValue(enOUT_ARV_UP_F);
   stCE_DataBuffer.bArrivalR_UP = GetOutputValue(enOUT_ARV_UP_R);
   if( Param_ReadValue_1Bit(enPARAM1__DoubleChimeOnDown) )
   {
      uint8_t bArrivalF_DN = 0;
      uint8_t bArrivalR_DN = 0;
      if( GetOutputValue(enOUT_ARV_DN_F) )
      {
         if( aucDoubleChimeDownCounter_ON_5ms[DOOR_FRONT] >= DOUBLE_CHIME_ARRIVAL_DOWN_ON_TIME_5MS )
         {
            if( aucDoubleChimeDownCounter_OFF_5ms[DOOR_FRONT] >= DOUBLE_CHIME_ARRIVAL_DOWN_OFF_TIME_5MS )
            {
               bArrivalF_DN = 1;
            }
            else
            {
               aucDoubleChimeDownCounter_OFF_5ms[DOOR_FRONT]++;
            }
         }
         else
         {
            aucDoubleChimeDownCounter_ON_5ms[DOOR_FRONT]++;
            bArrivalF_DN = 1;
         }
      }
      else
      {
         aucDoubleChimeDownCounter_ON_5ms[DOOR_FRONT] = 0;
         aucDoubleChimeDownCounter_OFF_5ms[DOOR_FRONT] = 0;
      }

      if( GetOutputValue(enOUT_ARV_DN_R) )
      {
         if( aucDoubleChimeDownCounter_ON_5ms[DOOR_REAR] >= DOUBLE_CHIME_ARRIVAL_DOWN_ON_TIME_5MS )
         {
            if( aucDoubleChimeDownCounter_OFF_5ms[DOOR_REAR] >= DOUBLE_CHIME_ARRIVAL_DOWN_OFF_TIME_5MS )
            {
               bArrivalR_DN = 1;
            }
            else
            {
               aucDoubleChimeDownCounter_OFF_5ms[DOOR_REAR]++;
            }
         }
         else
         {
            aucDoubleChimeDownCounter_ON_5ms[DOOR_REAR]++;
            bArrivalR_DN = 1;
         }
      }
      else
      {
         aucDoubleChimeDownCounter_ON_5ms[DOOR_REAR] = 0;
         aucDoubleChimeDownCounter_OFF_5ms[DOOR_REAR] = 0;
      }
      stCE_DataBuffer.bArrivalF_DN = bArrivalF_DN;
      stCE_DataBuffer.bArrivalR_DN = bArrivalR_DN;
   }
   else
   {
      stCE_DataBuffer.bArrivalF_DN = GetOutputValue(enOUT_ARV_DN_F);
      stCE_DataBuffer.bArrivalR_DN = GetOutputValue(enOUT_ARV_DN_R);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_TravelArrows(void)
{
   if(GetMotion_Direction() > 0)
   {
     stCE_DataBuffer.bTravel_UP = 1;
     stCE_DataBuffer.bTravel_DN = 0;
   }
   else if(GetMotion_Direction() < 0)
   {
     stCE_DataBuffer.bTravel_UP = 0;
     stCE_DataBuffer.bTravel_DN = 1;
   }
   else if( !Param_ReadValue_1Bit(enPARAM1__DisableIdleTravelArrows) )
   {
      if( GetOutputValue(enOUT_ARV_UP_F) || GetOutputValue(enOUT_ARV_UP_R) )
      {
        stCE_DataBuffer.bTravel_UP = 1;
        stCE_DataBuffer.bTravel_DN = 0;
      }
      else if( GetOutputValue(enOUT_ARV_DN_F) || GetOutputValue(enOUT_ARV_DN_R) )
      {
        stCE_DataBuffer.bTravel_UP = 0;
        stCE_DataBuffer.bTravel_DN = 1;
      }
      else
      {
         stCE_DataBuffer.bTravel_UP = 0;
         stCE_DataBuffer.bTravel_DN = 0;
      }
   }
   else
   {
      stCE_DataBuffer.bTravel_UP = 0;
      stCE_DataBuffer.bTravel_DN = 0;
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_FireLamp(void)
{
   if(GetOperation_AutoMode() == MODE_A__FIRE1 || GetOperation_AutoMode() == MODE_A__FIRE2)
   {
      stCE_DataBuffer.bFire = 1;
   }
   else
   {
      stCE_DataBuffer.bFire = 0;
   }
}
/*-----------------------------------------------------------------------------
   For annunciator of floor levels
   Holds strobe high for 1 sec
 -----------------------------------------------------------------------------*/
static void Update_StrobeLamp(void)
{
#if 1 // Fix for annunciator inconsistency
   static uint16_t uwCountdown_5ms;
   static uint8_t bLastDoorOpen;
   uint8_t bDZ = GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R);
   uint8_t bDoorOpen = ( GetDoorState_Front() == DOOR__OPENING )
                    || ( GetDoorState_Front() == DOOR__OPEN )
                    || ( GetDoorState_Rear() == DOOR__OPENING )
                    || ( GetDoorState_Rear() == DOOR__OPEN );
   uint8_t bArrivalLantern = GetOutputValue(enOUT_ARV_UP_F)
                          || GetOutputValue(enOUT_ARV_DN_F)
                          || GetOutputValue(enOUT_ARV_UP_R)
                          || GetOutputValue(enOUT_ARV_DN_R);
   if( !Param_ReadValue_1Bit(enPARAM1__DisableCE_FloorPlus1) )
   {
      if( bArrivalLantern )
      {
         stCE_DataBuffer.ucSlot = GetOperation_DestinationFloor()+1;
      }
      else
      {
         stCE_DataBuffer.ucSlot = GetOperation_CurrentFloor()+1;
      }
   }
   else
   {
      if( bArrivalLantern )
      {
         stCE_DataBuffer.ucSlot = GetOperation_DestinationFloor();
      }
      else
      {
         stCE_DataBuffer.ucSlot = GetOperation_CurrentFloor();
      }
   }

   if( bDoorOpen
    && !bLastDoorOpen )
   {
      uwCountdown_5ms = 400;
   }

   if( bDZ
    && uwCountdown_5ms
    && GetOperation_CurrentFloor() == GetOperation_DestinationFloor()
    && bDoorOpen )
   {
      stCE_DataBuffer.bStrobe = 1;
   }
   else if( bDZ
         && bArrivalLantern
         && bDoorOpen
         && GetOperation_CurrentFloor() == GetOperation_DestinationFloor() )
   {
      stCE_DataBuffer.bStrobe = 1;
   }
   else
   {
      uwCountdown_5ms = 0;
      stCE_DataBuffer.bStrobe = 0;
   }

   if( uwCountdown_5ms )
   {
      uwCountdown_5ms--;
   }
   bLastDoorOpen = bDoorOpen;
#else
   static uint16_t uwCountdown_5ms;
   static uint8_t bLastDZ;
   uint8_t bDZ = GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R);
   uint8_t bDoorOpen = ( GetDoorState_Front() == DOOR__OPENING )
                    || ( GetDoorState_Front() == DOOR__OPEN )
                    || ( GetDoorState_Rear() == DOOR__OPENING )
                    || ( GetDoorState_Rear() == DOOR__OPEN );
   uint8_t bArrivalLantern = GetOutputValue(enOUT_ARV_UP_F)
                          || GetOutputValue(enOUT_ARV_DN_F)
                          || GetOutputValue(enOUT_ARV_UP_R)
                          || GetOutputValue(enOUT_ARV_DN_R);
   if( !Param_ReadValue_1Bit(enPARAM1__DisableCE_FloorPlus1) )
   {
      if( bArrivalLantern )
      {
         stCE_DataBuffer.ucSlot = GetOperation_DestinationFloor()+1;
      }
      else
      {
         stCE_DataBuffer.ucSlot = GetOperation_CurrentFloor()+1;
      }
   }
   else
   {
      if( bArrivalLantern )
      {
         stCE_DataBuffer.ucSlot = GetOperation_DestinationFloor();
      }
      else
      {
         stCE_DataBuffer.ucSlot = GetOperation_CurrentFloor();
      }
   }

   if( bDZ
    && !bLastDZ )
   {
      uwCountdown_5ms = 300;
   }

   if( bDZ
    && uwCountdown_5ms
    && GetOperation_CurrentFloor() == GetOperation_DestinationFloor()
    && bDoorOpen )
   {
      stCE_DataBuffer.bStrobe = 1;
   }
   else if( bArrivalLantern && bDoorOpen )
   {
      stCE_DataBuffer.bStrobe = 1;
   }
   else
   {
      uwCountdown_5ms = 0;
      stCE_DataBuffer.bStrobe = 0;
   }

   if( uwCountdown_5ms )
   {
      uwCountdown_5ms--;
   }
   bLastDZ = bDZ;
#endif
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_Message(void)
{
   if( ( Param_ReadValue_1Bit(enPARAM1__EnableCE_V2) )
    && ( GetOperation_AutoMode() == MODE_A__FIRE2 ) )
   {
      stCE_DataBuffer.ucMessageNum = CE_MSG__FF2;
   }
   else if( ( GetOperation_AutoMode() == MODE_A__FIRE1 )
         || ( GetOperation_AutoMode() == MODE_A__FIRE2 ) )
   {
      if( GetEmergencyBit( EmergencyBF_FirePhaseI_RecallToAltFloor ) )
      {
         stCE_DataBuffer.ucMessageNum = CE_MSG__FIRE_ALT;
      }
      else
      {
         stCE_DataBuffer.ucMessageNum = CE_MSG__FIRE_MAIN;
      }
   }
   else if( ( GetDoorState_Front() == DOOR__NUDGING )
         || ( GetDoorState_Rear() == DOOR__NUDGING ) )
   {
      stCE_DataBuffer.ucMessageNum = CE_MSG__NUDGE;
   }
   else if( GetOutputValue(enOUT_Overload) )
   {
      stCE_DataBuffer.ucMessageNum = CE_MSG__OVERLOAD;
   }
   else if(GetOperation_AutoMode() == MODE_A__INDP_SRV)
   {
      stCE_DataBuffer.ucMessageNum = CE_MSG__INDP_SRV;
   }
   else if( ( GetOperation_AutoMode() == MODE_A__EPOWER )
         && ( !CheckIf_EPowerCarInNormal() ) )
   {
      stCE_DataBuffer.ucMessageNum = CE_MSG__EPOWER;
   }
   else if(GetOperation_ManualMode() >= MODE_M__INSP_CT && GetOperation_ManualMode() < NUM_MODE_MANUAL)
   {
      stCE_DataBuffer.ucMessageNum = CE_MSG__INSP;
   }
   else if (GetOperation_AutoMode() == MODE_A__SEISMC)
   {
      stCE_DataBuffer.ucMessageNum = CE_MSG__SEISMIC;
   }
   else if( ( Param_ReadValue_1Bit(enPARAM1__EnableCE_V2) )
         && ( !Param_ReadValue_1Bit(enPARAM1__DisablePIOOS) )
         && ( ( GetOperation_AutoMode() == MODE_A__OOS ) ||
              ( ( GetOperation_AutoMode() == MODE_A__NORMAL ) && ( HallCallsDisabled() || ( GetOperation3_CaptureMode_MR() == CAPTURE_IDLE ) ) ) ) )
   {
      stCE_DataBuffer.ucMessageNum = CE_MSG__OOS;
   }
   else
   {
      stCE_DataBuffer.ucMessageNum = CE_MSG__NONE;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define OOS_STATUS_TOGGLE_RATE_5MS     (200)
static void Update_PI(void)
{
   /* Simulate a flashing OOS display if in OOS mode, if in normal and car is captured or faulted */
   static uint8_t bOOS;
   static uint8_t bCR;
   static uint8_t bVIP;
   static uint8_t ucToggleCounter_5ms;
   if( ( GetOperation_AutoMode() == MODE_A__OOS )
    || ( ( GetOperation_AutoMode() == MODE_A__NORMAL ) && ( !Param_ReadValue_1Bit(enPARAM1__DisablePIOOS) ) &&
         ( gstFault.bActiveFault || ( HallCallsDisabled() && ( GetOperation3_CaptureMode_MR() == CAPTURE_IDLE ) ) ) ) )
   {
      if(++ucToggleCounter_5ms >= OOS_STATUS_TOGGLE_RATE_5MS)
      {
         bOOS = !bOOS;
         ucToggleCounter_5ms = 0;
      }
   }
   else if(GetOperation_AutoMode() == MODE_A__ACTIVE_SHOOTER_MODE)
   {
      if(++ucToggleCounter_5ms >= OOS_STATUS_TOGGLE_RATE_5MS)
      {
         bCR = !bCR;
         ucToggleCounter_5ms = 0;
      }
   }
   else if( !GetGroup_VIP_CarCapturing() && ( GetOperation_AutoMode() == MODE_A__VIP_MODE ) )
   {
      if(++ucToggleCounter_5ms >= OOS_STATUS_TOGGLE_RATE_5MS)
      {
         bVIP = !bVIP;
         ucToggleCounter_5ms = 0;
      }
   }
   else
   {
      ucToggleCounter_5ms = 0;
      bOOS = 0;
      bCR = 0;
      bVIP = 0;
   }
   if(bOOS && !stCE_DataBuffer.ucMessageNum)
   {
      stCE_DataBuffer.ucCharL = 'O';
      stCE_DataBuffer.ucCharM = 'O';
      stCE_DataBuffer.ucCharR = 'S';
   }
   else if(bCR && !stCE_DataBuffer.ucMessageNum)
   {
      stCE_DataBuffer.ucCharL = ' ';
      stCE_DataBuffer.ucCharM = 'C';
      stCE_DataBuffer.ucCharR = 'R';
   }
   else if(bVIP && !stCE_DataBuffer.ucMessageNum)
   {
      stCE_DataBuffer.ucCharL = ' ';
      stCE_DataBuffer.ucCharM = 'V';
      stCE_DataBuffer.ucCharR = 'P';
   }
   else
   {
      char * ps = Get_PI_Label(GetOperation_CurrentFloor());
      stCE_DataBuffer.ucCharL = *(ps+0);
      stCE_DataBuffer.ucCharM = *(ps+1);
      stCE_DataBuffer.ucCharR = *(ps+2);

      if(stCE_DataBuffer.ucCharL == 0x20)
      {
         stCE_DataBuffer.ucCharL = 0x3B;
      }
      if(stCE_DataBuffer.ucCharM == 0x20)
      {
         stCE_DataBuffer.ucCharM = 0x3B;
      }
      if(stCE_DataBuffer.ucCharR == 0x20)
      {
         stCE_DataBuffer.ucCharR = 0x3B;
      }
   }
}

static void Update_Chime()
{
   stCE_DataBuffer.bChime = GetLocalChimeSignal();
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

static void Update_CEDisplayData( void )
{
   Update_TravelArrows();
   Update_ArrivalArrows();
   Update_FireLamp();
   Update_StrobeLamp();

   Update_PI();

   Update_Message();
   Update_Chime();
}
/*-----------------------------------------------------------------------------

   CAN ID: 0x501

   Byte 0: Bitmap
            Bit 7-3: Reserved
            Bit 2: Play strobe
            Bit 1: Arrival DN arrow
            Bit 0: Arrival UP arrow
   Byte 1: Scan slot (must match CAN ID 0x502 Byte 6 Field)
   Byte 2: Message number (0 if no message. 1-63 valid)
   Byte 3: Bitmap
            Bit 7: Rear Arrival DN arrow
            Bit 6: Rear Arrival UP arrow
            Bit 5: Mess level HIGH bit
            Bit 4: Mess level LOW bit
            Bit 3: Passing Chime
            Bit 2: Reserved
            Bit 1: Traveling DN arrow
            Bit 0: Traveling UP arrow
   Byte 4: Reserved
   Byte 5: Reserved
   Byte 6: Reserved
   Byte 7: Reserved

-----------------------------------------------------------------------------*/
static void LoadDatagram_CE_Message()
{
   un_sdata_datagram unDatagram;

   memset(&unDatagram, 0, sizeof(un_sdata_datagram));
   unDatagram.auc8[0] = stCE_DataBuffer.bArrivalF_UP
                     | (stCE_DataBuffer.bArrivalF_DN << 1)
                     | (stCE_DataBuffer.bStrobe << 2)
                     | (stCE_DataBuffer.bFire << 6)
                     ;
   unDatagram.auc8[1] = stCE_DataBuffer.ucSlot;
   unDatagram.auc8[2] = stCE_DataBuffer.ucMessageNum; //No message
   unDatagram.auc8[3] = stCE_DataBuffer.bTravel_UP
                     | (stCE_DataBuffer.bTravel_DN << 1)
                     | (stCE_DataBuffer.bChime << 3)
                     | (gaucMsgLevel[stCE_DataBuffer.ucMessageNum] << 4)
                     | (stCE_DataBuffer.bArrivalR_UP << 6)
                     | (stCE_DataBuffer.bArrivalR_DN << 7);

    SDATA_WriteDatagram( gpastSData_Nodes_AuxNet[MR_AUX_NET__MRB],
                         DG_AuxNet_MRB__Message,
                         &unDatagram
                         );
}
/*-----------------------------------------------------------------------------

   CAN ID: 0x502

   Byte 0: (N/A) ASCII Floor Char, msb
   Byte 1: ASCII Floor Char, mid
   Byte 2: ASCII Floor Char, lsb
   Byte 3: ASCII Message Char, msb
   Byte 4: ASCII Message Char, mid
   Byte 5: ASCII Message Char, lsb
   Byte 6: Lantern Position (match Scan Slot Byte 1 in ID 0x501 message)
   Byte 7: UNUSED

-----------------------------------------------------------------------------*/
static void LoadDatagram_CE_Label()
{
   un_sdata_datagram unDatagram;
   memset(&unDatagram, 0, sizeof(un_sdata_datagram));

   unDatagram.auc8[0] = stCE_DataBuffer.ucCharL;
   unDatagram.auc8[1] = stCE_DataBuffer.ucCharM;
   unDatagram.auc8[2] = stCE_DataBuffer.ucCharR;
   unDatagram.auc8[3] = *(gpas_MsgStrings[stCE_DataBuffer.ucMessageNum]+2);//No message
   unDatagram.auc8[4] = *(gpas_MsgStrings[stCE_DataBuffer.ucMessageNum]+0);//No message
   unDatagram.auc8[5] = *(gpas_MsgStrings[stCE_DataBuffer.ucMessageNum]+1);//No message
   unDatagram.auc8[6] = stCE_DataBuffer.ucSlot;
   unDatagram.auc8[7] = 0;
   SDATA_WriteDatagram( gpastSData_Nodes_AuxNet[MR_AUX_NET__MRB],
                        DG_AuxNet_MRB__Label,
                        &unDatagram
                        );
}
/*-----------------------------------------------------------------------------

   CAN ID: 0x503

   Byte 0:
   Byte 1:
   Byte 2:
   Byte 3: PI Indux Plus 1
   Byte 4: Msg Number
   Byte 5: TravelArrows
   Byte 6:
   Byte 7:

-----------------------------------------------------------------------------*/
static void LoadDatagram_DiscretePI()
{
   un_sdata_datagram unDatagram;
   memset(&unDatagram, 0, sizeof(un_sdata_datagram));

   unDatagram.auc8[3] = GetOperation_CurrentFloor()+1;
   unDatagram.auc8[4] = stCE_DataBuffer.ucMessageNum;
   unDatagram.auc8[5] = (stCE_DataBuffer.bTravel_DN << 1) | stCE_DataBuffer.bTravel_UP;
   unDatagram.auc8[6] = (stCE_DataBuffer.bArrivalF_DN << 1) | stCE_DataBuffer.bArrivalF_UP;
   unDatagram.auc8[7] = (stCE_DataBuffer.bArrivalR_DN << 1) | stCE_DataBuffer.bArrivalR_UP;
   SDATA_WriteDatagram( gpastSData_Nodes_AuxNet[MR_AUX_NET__MRB],
                        DG_AuxNet_MRB__DiscretePI,
                        &unDatagram
                        );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData( void )
{
   LoadDatagram_CE_Message();
   LoadDatagram_CE_Label();
   LoadDatagram_DiscretePI();
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Init_CEBoardData()
{
   stCE_DataBuffer.bArrivalF_DN = 0;
   stCE_DataBuffer.bArrivalF_UP = 0;
   stCE_DataBuffer.bArrivalR_DN = 0;
   stCE_DataBuffer.bArrivalR_UP = 0;
   stCE_DataBuffer.bChime = 0;
   stCE_DataBuffer.ucMessageNum = 0; // No message
   stCE_DataBuffer.bStrobe = 0;
   stCE_DataBuffer.bTravel_UP = 0;
   stCE_DataBuffer.bTravel_DN = 0;
   stCE_DataBuffer.ucSlot = 0;
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10000;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_CE_BOARD_1MS;
   Init_CEBoardData();
   return 0;
}

/*-----------------------------------------------------------------------------
   NOTES:
      Applies to chips labeled "P 445"
      Set DIP3 to enable CAN 125K Baudrate. Otherwise, CAN is at 100K.
      Set DIP5 to enable CE message display
      Runs on enCAN_1, labeled CAN3 on UI board.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   UpdateSmartriseFlag();
   UpdateLocalChimeSignal(pstThisModule->uwRunPeriod_1ms);
   Update_CEDisplayData();
   LoadData();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
