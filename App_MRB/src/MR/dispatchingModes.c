/******************************************************************************
 *
 * @file     dispatching.c
 * @brief    
 * @version  V1.00
 * @date     13 August 2019
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include "dispatching.h"
#include "remoteCommands.h"
#include "shieldData.h"
#include "GlobalData.h"
#include <stdint.h>


/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static en_dispatch_mode enCurrentDispatchMode = DISPATCH_MODE__NONE;

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Determines dispatching mode based on the physical inputs and remote command.
 *
 *----------------------------------------------------------------------------*/
void UpdateDispatchMode(void)
{
   en_dispatch_mode enDispatchMode = DISPATCH_MODE__NONE;

   if(GetInputValue(enIN_ENABLE_LOBBY_PEAK))
   {
      enDispatchMode = DISPATCH_MODE__LOBBY;
   }
   else if(GetInputValue(enIN_ENABLE_UP_PEAK))
   {
      enDispatchMode = DISPATCH_MODE__UP;
   }
   else if(GetInputValue(enIN_ENABLE_DOWN_PEAK))
   {
      enDispatchMode = DISPATCH_MODE__DOWN;
   }
   else if(!ShieldData_CheckTimeoutCounter())
   {
      enDispatchMode = RemoteCommand_GetDispatchMode();
   }
   
   enCurrentDispatchMode = enDispatchMode;
}

en_dispatch_mode GetDispatchMode(void)
{
   return enCurrentDispatchMode;
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

