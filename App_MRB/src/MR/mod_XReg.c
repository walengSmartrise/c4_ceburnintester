/***
 *       _____                          __         _
 *      / ___/ ____ ___   ____ _ _____ / /_ _____ (_)_____ ___
 *      \__ \ / __ `__ \ / __ `// ___// __// ___// // ___// _ \
 *     ___/ // / / / / // /_/ // /   / /_ / /   / /(__  )/  __/
 *    /____//_/ /_/ /_/ \__,_//_/    \__//_/   /_//____/ \___/
 *        ______               _                           _
 *       / ____/____   ____ _ (_)____   ___   ___   _____ (_)____   ____ _
 *      / __/  / __ \ / __ `// // __ \ / _ \ / _ \ / ___// // __ \ / __ `/
 *     / /___ / / / // /_/ // // / / //  __//  __// /   / // / / // /_/ /
 *    /_____//_/ /_/ \__, //_//_/ /_/ \___/ \___//_/   /_//_/ /_/ \__, /
 *                  /____/                                       /____/
 */
/******************************************************************************
 *
 * @file
 * @brief    Updates XReg car destinations
 * @version
 * @date
 *
 * @note
 *
 ******************************************************************************/




/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sru_b.h"
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "shared_data.h"

#include "carData.h"
#include "carDestination.h"
#include "groupnet.h"
#include "groupnet_datagrams.h"
#include "group_net_data.h"
#include "XRegData.h"
#include "XRegDestination.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_XReg =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Update car destinations
   Currently lowest index car has highest priority for taking a call
 -----------------------------------------------------------------------------*/
#define GROUP_DESTINATION_UPDATE_RATE_MS     (500)
static void UpdateGroupDestinations( struct st_module *pstThisModule )
{
   static uint16_t uwUpdateTimer_ms;
   if( uwUpdateTimer_ms < GROUP_DESTINATION_UPDATE_RATE_MS )
   {
      uwUpdateTimer_ms+=pstThisModule->uwRunPeriod_1ms;
   }
   else
   {
      XRegData_CheckForDispatchingTimeout(GROUP_DESTINATION_UPDATE_RATE_MS);
      UpdateXRegCarDestinations();
      uwUpdateTimer_ms = 0;
   }

}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      XRegData_Init(i);
   }

   //------------------------------------------------
   pstThisModule->uwInitialDelay_1ms = 10000;
   pstThisModule->uwRunPeriod_1ms = 50;

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static uint8_t bLastMasterFlag;
   uint8_t ucNumXRegCars = Param_ReadValue_8Bit(enPARAM8__NumXRegCars);
   if(ucNumXRegCars)
   {
      XRegData_UpdateFirstLastLanding();

      static uint8_t abLastXRegOnline[MAX_GROUP_CARS];
      for( uint8_t i = 0; i < ucNumXRegCars; i++ )
      {
         XRegData_UpdateCarOnlineFlag(i, pstThisModule->uwRunPeriod_1ms);

         if( abLastXRegOnline[i]
         && !XRegData_GetCarOnlineFlag(i) )
         {
            XRegData_Init(i);
            en_alarms eAlarm = ALM__XREG_OFFLINE_1 + i;
            if( eAlarm > ALM__XREG_OFFLINE_8 )
            {
               eAlarm = ALM__XREG_OFFLINE_8;
            }
            SetAlarm( eAlarm );
         }

         abLastXRegOnline[i] = XRegData_GetCarOnlineFlag(i);
      }

      if(GetMasterDispatcherFlag())
      {
         UpdateGroupDestinations(pstThisModule);
      }
      else if(bLastMasterFlag)
      {
         Init_XRegDestinationStructure();
      }
      bLastMasterFlag = GetMasterDispatcherFlag();
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

