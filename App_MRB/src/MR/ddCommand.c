/******************************************************************************
 *
 * @file     ddcommand.c
 * @brief
 * @version  V1.00
 * @date     13, June 2018
 * @author   Keith Soneda
 *
 * @note     Manages commands used for CC/HC assignments to cars.
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>

#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "ddCommand.h"
#include "hallData.h"
#include "GlobalData.h"
#include "group_net_data.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static en_ddm_alarms eAlarm;
static uint16_t uwOfflineTimer_ms = DDM_INACTIVE_TIMER__MS;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   TODO capture this data for display on GUI
 *----------------------------------------------------------------------------*/
en_ddm_alarms DDC_GetAlarm(void)
{
   return eAlarm;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint8_t DDC_GetOnlineFlag(void)
{
   return (uwOfflineTimer_ms < DDM_OFFLINE_TIMER__MS);
}
void DDC_ClrOfflineTimer(void)
{
   uwOfflineTimer_ms = 0;
}
void DDC_UpdateOfflineTimer( struct st_module *pstThisModule )
{
   if( uwOfflineTimer_ms < DDM_OFFLINE_TIMER__MS )
   {
      uwOfflineTimer_ms += pstThisModule->uwRunPeriod_1ms;
   }
   else if( uwOfflineTimer_ms != DDM_INACTIVE_TIMER__MS )
   {
      SetAlarm(ALM__DDM_OFFLINE);
   }
}

/*----------------------------------------------------------------------------
      punDatagram->auc8[0] = astHCRequest[ucIndex].ucCarID;
      punDatagram->auc8[1] = astHCRequest[ucIndex].ucLanding;
      punDatagram->auc8[2] = astHCRequest[ucIndex].eDir;
      punDatagram->auc8[3] = astHCRequest[ucIndex].bADA;
      if(astHCRequest[ucIndex].bRear)
      {
         punDatagram->aui32[1] = CarData_GetUniqueHallMask_Rear(astHCRequest[ucIndex].ucCarID);
      }
      else
      {
         punDatagram->aui32[1] = CarData_GetUniqueHallMask_Front(astHCRequest[ucIndex].ucCarID);
      }
 *----------------------------------------------------------------------------*/
void DDC_UnloadDatagram_LatchHC(un_sdata_datagram *punDatagram)
{
   uint8_t ucCarID = punDatagram->auc8[0];
   uint8_t ucLanding = punDatagram->auc8[1];
   en_hc_dir eDir = punDatagram->auc8[2];
   uint8_t bADA = punDatagram->auc8[3];
   uint32_t uiMask = punDatagram->aui32[1];
   if( ( ucCarID < MAX_GROUP_CARS )
    && ( ucLanding < MAX_NUM_FLOORS ) )
   {
      SetRawLatchedHallCalls(ucLanding, eDir, uiMask);

      if( ( ucCarID == GetFP_GroupCarIndex() )
       && ( bADA ) )
      {
         uint8_t ucFloor = GetCarMapping(ucLanding);
         en_ada_type eType = (eDir == HC_DIR__DOWN) ? ADA_TYPE__DN:ADA_TYPE__UP;
         SetADARequest( ucFloor, eType );
      }
   }
}
/*----------------------------------------------------------------------------
      punDatagram->auc8[0] = astCCRequest[ucIndex].ucCarID;
      punDatagram->auc8[1] = astCCRequest[ucIndex].ucLanding;
      punDatagram->auc8[2] = astCCRequest[ucIndex].bRear;
      punDatagram->auc8[3] = astCCRequest[ucIndex].bADA;
 *----------------------------------------------------------------------------*/
void DDC_UnloadDatagram_LatchCC(un_sdata_datagram *punDatagram)
{
   uint8_t ucCar = punDatagram->auc8[0];
   if( ucCar == GetFP_GroupCarIndex() )
   {
      uint8_t ucLanding = punDatagram->auc8[1];
      uint8_t bRear = punDatagram->auc8[2];
      uint8_t bADA = punDatagram->auc8[3];
      uint8_t ucFloor = GetCarMapping(ucLanding);

      if(ucFloor < MAX_NUM_FLOORS)
      {
         SetCarCall(ucFloor, bRear);

         if( bADA )
         {
            en_ada_type eType = ADA_TYPE__CC;
            SetADARequest( ucFloor, eType );
         }
      }
   }
}


/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
