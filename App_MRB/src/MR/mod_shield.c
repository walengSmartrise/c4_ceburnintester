/******************************************************************************
 *
 * @file
 * @brief
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note  Manages shield data (DAD unit) and timers
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "sru_b.h"
#include "sru.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
#include "motion.h"
#include "shieldData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_module gstMod_Shield =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckForShieldAlarm(void)
{
   if( Param_ReadValue_1Bit(enPARAM1__EnableShieldAlarms) )
   {
      if( !ShieldData_CheckTimeoutCounter() )
      {
         en_shield_error eError = ShieldData_GetError();
         switch(eError)
         {
            case SHIELD_ERROR__UNKNOWN:
               SetAlarm(ALM__SHIELD_UNKNOWN);
               break;
            case SHIELD_ERROR__POR_RESET:
               SetAlarm(ALM__SHIELD_POR_RESET);
               break;
            case SHIELD_ERROR__BOD_RESET:
               SetAlarm(ALM__SHIELD_BOD_RESET);
               break;
            case SHIELD_ERROR__WDT_RESET:
               SetAlarm(ALM__SHIELD_WDT_RESET);
               break;
            case SHIELD_ERROR__COM_GROUP:
               SetAlarm(ALM__SHIELD_COM_GROUP);
               break;
            case SHIELD_ERROR__COM_RPI:
               SetAlarm(ALM__SHIELD_COM_RPI);
               break;
            case SHIELD_ERROR__FAILED_RTC:
               SetAlarm(ALM__SHIELD_FAILED_RTC);
               break;
            case SHIELD_ERROR__UART_OVF_TX:
               SetAlarm(ALM__SHIELD_UART_OVF_TX);
               break;
            case SHIELD_ERROR__UART_OVF_RX:
               SetAlarm(ALM__SHIELD_UART_OVF_RX);
               break;
            case SHIELD_ERROR__CAN_OVF_TX:
               SetAlarm(ALM__SHIELD_CAN_OVF_TX);
               break;
            case SHIELD_ERROR__CAN_OVF_RX:
               SetAlarm(ALM__SHIELD_CAN_OVF_RX);
               break;
            case SHIELD_ERROR__CAN_BUS_RST:
               SetAlarm(ALM__SHIELD_CAN_BUS_RST);
               break;
            default: break;
         }
      }
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 1000;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_SHIELD_1MS;

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   ShieldData_IncTimeoutCounter(pstThisModule->uwRunPeriod_1ms);
   CheckForShieldAlarm();
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
