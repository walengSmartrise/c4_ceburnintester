#ifndef _ERRORS_H
#define _ERRORS_H

#include <stdint.h>

typedef enum
{
    noErr             = 0,
    SRecRangeError    = 1, //S-Record Out Of Range
    SRecOddError      = 2, //S-Record Size Must Be Even
    FlashProgramError = 3, //Flash Programming Error
    FlashEraseError   = 4, //Flash Erase Error
    BadHexData        = 5, //Bad Hex Data
    SRecTooLong       = 6, //S-Record Too Long
    CheckSumErr       = 7, //Checksum Error
    UnknownPartID     = 8 //Unknown Part ID
} Error;

extern int8_t * GetErrorString( uint8_t ErrorNumber );

#endif
