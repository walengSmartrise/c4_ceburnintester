#ifndef CONFIG_SMARTRISE_
#define CONFIG_SMARTRISE_


#define ID_MASK  (0xF00)
#define MR_A_ID  (0x100)
#define MR_B_ID  (0x200)
#define CT_A_ID  (0x300)
#define CT_B_ID  (0x400)
#define BBB_ID   (0x500)
#define COP_A_ID (0x600)
#define COP_B_ID (0x700)
#define DDM_A_ID (0x800)
#define DDM_B_ID (0x900)
#define RISER_ID (0x500)
#define SHIELD_ID (0xB00)

#define USER_FLASH_START 0x00010000
// Number of data byts in an Srecord.
// Should be <256 and a 256%X == 0
#define MaxSRecDataLen (128)

//LED (Name, Port, Pin, IOCON)
#define LED_IOCON (IOCON_FUNC0 | IOCON_MODE_PULLUP | IOCON_HYS_EN)

//(NAME, PORT, PIN, IO CONFIG)
#define LED_TABLE \
LED( Alarm,     3, 0, LED_IOCON) \
LED( Fault,     3, 1, LED_IOCON) \
LED( Heartbeat, 3, 2, LED_IOCON)

#define LED_TABLE2 \
LED( Alarm,     0, 24, LED_IOCON) \
LED( Fault,     0, 26, LED_IOCON) \
LED( Heartbeat, 0, 25, LED_IOCON)

#define LED_TABLE3 \
LED(FLT     , 3, 0 , IOCON_D_LED) \
LED(HB      , 3, 1 , IOCON_D_LED) \
LED(WIFI    , 3, 2 , IOCON_D_LED) \
LED(STATUS_R, 1, 0 , IOCON_D_LED) \
LED(STATUS_G, 1, 1 , IOCON_D_LED) \
LED(STATUS_B, 4, 31, IOCON_D_LED)

// UART(Port, Pin, IOCON)
#define TX_RX_UART1_IOCON (IOCON_FUNC1 | IOCON_HYS_EN)
#define TX_RX_UART3_IOCON (IOCON_FUNC2 | IOCON_HYS_EN)
#define RTS_IOCON   (IOCON_FUNC4 | IOCON_HYS_EN | IOCON_MODE_PULLUP)

#define UART_TABLE \
UART( 0, 15, TX_RX_UART1_IOCON) \
UART( 0, 16, TX_RX_UART1_IOCON) \
UART( 4, 28, TX_RX_UART3_IOCON) \
UART( 4, 29, TX_RX_UART3_IOCON)


#define MCUA_UART LPC_UART3
#define MCUA_UART_IRQ UART3_IRQn
#define MCUB_UART LPC_UART1
#define MCUB_UART_IRQ UART1_IRQn

#define DIP_IOCON_D (IOCON_FUNC0 | IOCON_HYS_EN)
#define DIP_IOCON_A (IOCON_FUNC0 | IOCON_HYS_EN | IOCON_FILT_DIS | IOCON_DIGMODE_EN)

#define DIP_TABLE \
DIP(A7,    0,  13, DIP_IOCON_A ) \
DIP(A7,    1,  15, DIP_IOCON_D ) \

#define TIMER_SELECTION     LPC_TIMER2
#define TIMER_IRQ_SELECTION TIMER2_IRQn
#define TIMER_HANDLER_NAME  TIMER2_IRQHandler

#define TICKS_PER_SECOND    ( 2 )
#define TIMOUT_VALUE_S      ( 5 )
#define TIMEOUT_VALUE_TICKS ( TIMOUT_VALUE_S * TICKS_PER_SECOND )
#define BOOT_LOADER_TIMEOUT_VALUE ( TIMEOUT_VALUE_TICKS )

#define CAN_IOCON_CAN1 (IOCON_FUNC1 | IOCON_HYS_EN | IOCON_MODE_PULLUP)
#define CAN_IOCON_CAN2 (IOCON_FUNC2 | IOCON_HYS_EN | IOCON_MODE_PULLUP)


#define CAN_TABLE \
CAN(CAN1_RD,0,0, CAN_IOCON_CAN1)\
CAN(CAN1_TD,0,1, CAN_IOCON_CAN1)\
CAN(CAN2_RD,0,4, CAN_IOCON_CAN2)\
CAN(CAN2_TD,0,5, CAN_IOCON_CAN2)



#endif // CONFIG_SMARTRISE_
