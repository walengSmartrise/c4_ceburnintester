#ifndef LED_SMARTRISE_
#define LED_SMARTRISE_

#include <stdint.h>
#include <stdbool.h>

#include "config.h"

// X macro the following
typedef enum
{
    #define LED(name, port, pin, iocon) name,
    LED_TABLE
    #undef LED

    Num_LEDS
}LED_Names;


/* Sets the state of a board LED to on or off */
void Board_LED_Set(LED_Names LEDNumber, bool On);

/* Toggles the current state of a board LED */
void Board_LED_Toggle(uint8_t LEDNumber);

void Board_LED_Init( void );

#endif // LED_SMARTRISE_
