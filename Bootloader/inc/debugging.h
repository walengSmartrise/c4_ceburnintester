#ifndef DEBUGGING_MESSAGES_
#define DEBUGGING_MESSAGES_

#include "SEGGER_RTT.h"


#ifdef DEBUG
    // https://gcc.gnu.org/onlinedocs/gcc/Variadic-Macros.html
    // Explains why to use ## __VA_ARGS__
    #define PRINT_ERROR(x, ...)   SEGGER_RTT_printf(0, RTT_CTRL_TEXT_BRIGHT_RED x RTT_CTRL_RESET   , ## __VA_ARGS__)
    #define PRINT_WARNING(x, ...) SEGGER_RTT_printf(0, RTT_CTRL_TEXT_BRIGHT_YELLOW x RTT_CTRL_RESET, ## __VA_ARGS__)
    #define PRINT_STATUS(x, ...)  SEGGER_RTT_printf(0, RTT_CTRL_TEXT_BRIGHT_GREEN x RTT_CTRL_RESET , ## __VA_ARGS__)
    #define PRINT_INFO(x, ...)    SEGGER_RTT_printf(0, RTT_CTRL_TEXT_BRIGHT_WHITE x RTT_CTRL_RESET , ## __VA_ARGS__)
#else
    #define PRINT_ERROR(x,...)
    #define PRINT_WARNING(x,...)
    #define PRINT_STATUS(x,...)
    #define PRINT_INFO(x,...)
#endif


#endif
