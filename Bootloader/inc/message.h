#ifndef MESSAGE_SMARTRISE_
#define MESSAGE_SMARTRISE_

extern uint8_t gbStartTimer;

typedef struct
{
        uint32_t id;
        uint8_t data[8];
}Packet;

void SetPacket(Packet* , const uint32_t  , const uint8_t const * );
void StorePacket(Packet* passed);
uint8_t GetNewMessage(Packet*);
uint8_t IsPacketID(Packet*, uint32_t);

void ForwardPacket(Packet*);
uint8_t Network_BootCodeReceived(Packet*);
uint8_t Network_StartCodeReceived(Packet* );
uint8_t Network_BootCodeAwk( Packet* packet );
uint8_t Network_StartUserAppReceived(Packet* packet);

void Network_SendBootCode( void);
void Network_SendStartUpCode( void );
void Network_StartBootloader( void );
void Network_StartUserApp( void );

void Network_UpdateMessages( void );
void Network_Init( void );
#endif
