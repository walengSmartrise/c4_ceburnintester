#ifndef SMARTRISE_DEPLOYMENT
#define SMARTRISE_DEPLOYMENT

#include <stdint.h>

typedef enum
{
    enSRU_DEPLOYMENT__UNKNOWN,
    enSRU_DEPLOYMENT__CT,
    enSRU_DEPLOYMENT__COP,
    enSRU_DEPLOYMENT__MR,
	enSRU_DEPLOYMENT__SHIELD,
	enSRU_DEPLOYMENT__DDM,
	enSRU_DEPLOYMENT__RISER
}en_sru_deployments;

typedef enum
{
    MCU_UNKNOWN,
    MCU_B,
    MCU_A

}en_deviceID;

en_deviceID GetMCU_ID( void );
void SetMCU_ID( void );
void SetDeployment( void );
en_sru_deployments GetDeployment( void );

uint8_t isMR_A ( void );
uint8_t isMR_B ( void );
uint8_t isCT_A ( void );
uint8_t isCT_B ( void );
uint8_t isCOP_A( void );
uint8_t isCOP_B( void );
uint8_t isDDM_A( void );
uint8_t isDDM_B( void );
uint8_t isRiser( void );
uint8_t isShield( void );

#endif
