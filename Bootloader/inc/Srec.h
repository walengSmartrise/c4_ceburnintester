/*******************************************************************************
* Freescale Semiconductor Inc.
* (c) Copyright 2004-2005 Freescale Semiconductor, Inc.
* ALL RIGHTS RESERVED.
********************************************************************************
Services performed by FREESCALE in this matter are performed AS IS and without 
any warranty. CUSTOMER retains the final decision relative to the total design 
and functionality of the end product. FREESCALE neither guarantees nor will be 
held liable by CUSTOMER for the success of this project.
FREESCALE DISCLAIMS ALL WARRANTIES, EXPRESSED, IMPLIED OR STATUTORY INCLUDING, 
BUT NOT LIMITED TO, IMPLIED WARRANTY OF MERCHANTABILITY OR FITNESS FOR 
A PARTICULAR PURPOSE ON ANY HARDWARE, SOFTWARE ORE ADVISE SUPPLIED TO THE PROJECT
BY FREESCALE, AND OR NAY PRODUCT RESULTING FROM FREESCALE SERVICES. IN NO EVENT
SHALL FREESCALE BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF 
THIS AGREEMENT.

CUSTOMER agrees to hold FREESCALE harmless against any and all claims demands or 
actions by anyone on account of any damage, or injury, whether commercial, 
contractual, or tortuous, rising directly or indirectly as a result of an advise
or assistance supplied CUSTOMER in connection with product, services or goods 
supplied under this Agreement.
********************************************************************************
* File      Srec.h
* Owner     b01802
* Version   1.0
* Date      Feb-24-2010
* Classification   General Business Information
* Brief     Srecord utilities
********************************************************************************
* Detailed Description:
********************************************************************************
Revision History:
Version  Date          Author    Description of Changes
1.0      Feb-24-2010   b01802    Initial version
*******************************************************************************/
#ifndef _UTILITIES_H /* Prevent duplicated includes */
#define _UTILITIES_H

#include <stdint.h>
#include "config.h"
#include "message.h"


#define START_APP_ADDR (0x10000)

#define BYTES_PER_LINE (128)

typedef enum
{
    SRecord_DataRecord  , // 1
    SRecord_EndRecord   , // 2
    SRecord_HeaderRecord  // 3
}SRecord_Type;

typedef enum
{
    SRecord_Error_None,
    SRecord_Error_InvalidMsg,
    SRecord_Error_InvalidRecordReceived,
    SRecord_Error_BadHexData,
    SRecord_Error_InvalidChecksum,

    SRecord_Complete

}SRecord_Status;

typedef struct
{
    uint8_t  RecType;
    uint8_t  NumBytes;
    uint32_t LoadAddr;
    uint8_t  Data[MaxSRecDataLen];
    uint8_t checksum;
} SRecDataRec;

extern uint32_t uiMemoryAddr;

SRecord_Status RcvSRecord(SRecDataRec *SRecData, uint8_t byteMSB, uint8_t byteLSB);
uint8_t ConvertPacketBufferToSRecord(SRecDataRec* result, Packet * packets);

#endif
/******************************************************************************/
