#ifndef DIP_SMARTRISE_
#define DIP_SMARTRISE_

#include <stdint.h>

#include "config.h"

// X macro the following
typedef enum
{
    DIP_0,
    DIP_1,
    DIP_2,
    DIP_3,
    DIP_4,
    DIP_5,
    DIP_6,
    DIP_7,

    Num_DipSwitch
}DipSwitch;




uint8_t GetDip( void );

#endif //DIP_SMARTRISE_
