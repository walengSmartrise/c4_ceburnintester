#ifndef CAN_SMARTRISE_
#define CAN_SMARTRISE_

#include "ring_buffer.h"
#include "message.h"
void Board_CAN_Init( void );
void Board_CAN_DeInit( void );
void UpdateMessages_MRB( void );
void ProcessCanMessage(RINGBUFF_T*);
void Board_CanSend(Packet*);
void Board_SendBootloaderStartup( Packet* );
void Board_SendMessageAwk( Packet* );
RINGBUFF_T* GetCanInstance(void);
#endif // CAN_SMARTRISE_
