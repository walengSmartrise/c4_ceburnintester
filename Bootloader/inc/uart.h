#ifndef UART_SMARTRISE_
#define UART_SMARTRISE_

#include "ring_buffer.h"

#include <stdint.h>
#include <stdbool.h>
#include "message.h"
void Board_UART_Init( void );
void Board_UART_DeInit( void );
/**
 * @brief   Sends a single character on the UART, required for printf redirection
 * @param   ch  : character to send
 * @return  None
 */
void Board_UARTPutChar(const uint8_t);

uint8_t ElementsInReceiveRB(void);
uint8_t Board_UARTGetChar( uint8_t* );
uint8_t Board_UARTGetStr( uint8_t* result, const int );
uint8_t UART_Finished( void);
void Board_UARTPutSTR(Packet*);

void Board_UART_Flush( void );


RINGBUFF_T* GetUartInstance(void);
void ProcessUartMessage(RINGBUFF_T*);
#endif // UART_SMARTRISE_
