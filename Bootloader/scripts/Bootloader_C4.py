#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import sys
import can
import time


can_message = {}


# 1 -> MR A
# 2 -> MR B
# 3 -> SRU A
# 4 -> SRU B
validFiles = {
    0x1: 0,
    0x2: 0,
    0x3: 0,
    0x4: 0,
    0x5: 0,
    0x6: 0,
    0x7: 0
}


mcuID = {
    'MRA'  : 0x100,
    'MRB'  : 0x200,
    'CTA'  : 0x300,
    'CTB'  : 0x400,
    'COPA' : 0x600,
    'COPB' : 0x700
}


def ProgressBar(progress):
    """Display or update a terminal progress bar.

    Accepts a float between 0 and 1. Any int will be converted to a float.
    A value under 0 represents a 'halt'.
    A value at 1 or bigger represents 100%
    Arguments:
        progress {[type]} -- [description]
    """
    barLength = 20  # Modify this to change the length of the progress bar
    status = ""
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = "Done...\r\n"
    block = int(round(barLength * progress))
    text = "\rPercent: [{0}] {1:.2f}% {2}".format("#" * block + "-" * (barLength - block), progress * 100, status)
    sys.stdout.write(text)
    sys.stdout.flush()


def ProgressData(overallPercent, board, boardPercent):
    """Display progress data on standard out.

    [description]

    Arguments:
        overallPercent {[type]} -- [description]
        board {[type]} -- [description]
        boardPercent {[type]} -- [description]
    """
    print(round(overallPercent, 3), ",", board, ",", round(boardPercent, 3))


def GenerateListOfData(passed):
    """Convert array of data to CAN message.

    [description]

    Arguments:
        passed {[type]} -- [description]
    """
    data = [passed[i:i + 2] for i in range(0, len(passed), 2)]

    if len(data) < 8:
        data.append('30')

    return [int(x, 16) for x in data]


def Update_MCU(mcu, bus, listOfLists, offSet, totalPackets, enableProgressBar):
    """Send & validate an MCU upgrade.

    [description]

    Arguments:
        mcu {[type]} -- [description]
        bus {[type]} -- [description]
        listOfLists {[type]} -- [description]
        offSet {[type]} -- [description]
        totalPackets {[type]} -- [description]
        enableProgressBar {[type]} -- [description]
    """
    counter = 0
    print('Updating ' + mcu)
    for line in listOfLists:
        sentOk = True
        while sentOk:
            for part in line:
                # Extract the ID
                id = int(part[0:3], 16)
                # Compare the extracted ID to the ID stored in
                # the database (id & 0x700) This simply extracts
                # the 3 most significant bits. The three ms bits
                # are the ones used for the mcu id
                if ((id & 0x700) == mcuID[mcu]):
                    while True:
                        try:
                            msg = can.Message(
                                arbitration_id=id,
                                # convert the data into an array of bytes
                                data=GenerateListOfData(part[4:]),
                                extended_id=False
                            )
                            bus.send(msg)
                            # This delay is not absolutly needed but it
                            # helps the system by not returning ASAP
                            # after executing the above command. This
                            # gives the hardware sometime to do its thing.
                            time.sleep(0.0005)
                        except can.CanError:
                            continue
                        else:
                            break

                    # Sent message. Need awk
                    # call(['cansend', 'can0',part],bufsize=-1)

                    # This is the end of line message
                    endOfLine = (format(mcuID[mcu], 'X') + "#454e444c4e303030")

                    # Check to see if an end of line message was sent.
                    # If so then wait for an AWK, NAK, or timeout.
                    # If a NAK or a timeout then resend the message
                    if(part == endOfLine) :
                        try:

                            # If no message is received in time (1 second)
                            # then resend the message
                            message = bus.recv(1)
                            recvCounter = 0
                            while (message.arbitration_id > 0x7FF) and (recvCounter < 100):
                                message = bus.recv(1)
                                recvCounter = recvCounter + 1

                            # Line not received correctly. resend the line
                            compare = bytearray([(id >> 8), 0x47, 0x4F, 0x4F, 0x44, 0x4C, 0x4E, 0x00])
                            if message.data != compare:
                               t = 2
                               # print(message)
                               # print("Error")
                            else :
                                # Line was received correctly. Send next line
                                sentOk = False
                        except:
                            print(mcu)
                            print("Timeout")
        counter += 1
        if enableProgressBar:
            ProgressBar((counter * 1.0) / len(listOfLists))
        else:
            # Default option is to always display the progress data
            ProgressData(((counter + offSet) * 1.0) / totalPackets, mcu, (counter * 1.0) / len(listOfLists))


def main():
    parser = argparse.ArgumentParser(description='Process CAN data from a serial device.')
    parser.add_argument('can_interface', type=str)
    parser.add_argument('sbf', type=str)
    parser.add_argument(
        '-B',
        '--Bar',
        action='store_true',
        help='Enable progress bar (Disables raw output)',
    )

    args = parser.parse_args()

    can_interface = args.can_interface
    sbf = args.sbf
    enableProgressBar = args.Bar
    # Open serial device with non-blocking read() (timeout=0)
    filter = [{"can_id": 0x7FF, "can_mask" : 0x7FF}, {"can_id": 0x500, "can_mask" : 0x7FF}]

    bus = can.interface.Bus(
        can_interface,
        bustype='socketcan_native',
        can_filters=filter
    )

    message = bus.recv()
    print(message.data)

    while message.data != bytearray([0xFE, 0xDC, 0xBA, 0x98, 0x76, 0x54, 0x32, 0x10]):
        message = bus.recv()

    try:
        msg = can.Message(
            arbitration_id=0x7ff,
            data=[0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xEB, 0xDA, 0xED],
            extended_id=False
        )
        bus.send(msg)
    except can.CanError:
        print('Error sending CAN frame')

    with open(sbf) as file:
        # Figure out how many packets need to be sent per device
        mr_a  = []
        mr_b  = []
        ct_a  = []
        ct_b  = []
        cop_a = []
        cop_b = []
        list  = []

        for line in file:
            rec = line.strip()
            if rec == "100#454e444c4e303030":
                list.append(rec)
                mr_a.append(list)
                list = []
            elif rec == "200#454e444c4e303030":
                list.append(rec)
                mr_b.append(list)
                list = []
            elif rec == "300#454e444c4e303030":
                list.append(rec)
                ct_a.append(list)
                list = []
            elif rec == "400#454e444c4e303030":
                list.append(rec)
                ct_b.append(list)
                list = []
            else:
                list.append(rec)
            id = int(line[0], 16)
            validFiles[id] += 1
        # Go through every element in the ct list of list
        # change the first 3 to 6 and 4 to 7
        list = []
        for i in ct_a:
            for j in i:
                rec = '6' + j[1:]
                list.append(rec)
                validFiles[0x6] += 1
            cop_a.append(list)
            list = []

        list = []
        for i in ct_b:
            for j in i:
                rec = '7' + j[1:]
                list.append(rec)
                validFiles[0x7] += 1
            cop_b.append(list)
            list = []

        print(validFiles)

    offSet = 0
    # Add the length of each list. This is the total number of lines that the system
    # will send for updating the MCU software
    totalPackets = len(mr_a) + len(mr_b) + len(ct_a) + len(ct_b) + len(cop_a) + len(cop_b)
    if validFiles[1] > 0:
        Update_MCU('MRA', bus, mr_a, offSet, totalPackets, enableProgressBar)
        offSet += len(mr_a)

    if validFiles[2] > 0:
        Update_MCU('MRB', bus, mr_b, offSet, totalPackets, enableProgressBar)
        offSet += len(mr_b)

    if validFiles[3] > 0:
        Update_MCU('CTA', bus, ct_a, offSet, totalPackets, enableProgressBar)
        offSet += len(ct_a)

    if validFiles[4] > 0:
        Update_MCU('CTB', bus, ct_b, offSet, totalPackets, enableProgressBar)
        offSet += len(ct_b)

    if validFiles[6] > 0:
        Update_MCU('COPA', bus, cop_a, offSet, totalPackets, enableProgressBar)
        offSet += len(cop_a)

    if validFiles[7] > 0:
        Update_MCU('COPB', bus, cop_b, offSet, totalPackets, enableProgressBar)
        offSet += len(cop_b)


if __name__ == "__main__":
    main()
