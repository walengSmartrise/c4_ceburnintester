#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys

# File name :
# 0010 -> MR A
# 0020 -> MR B
# 0030 -> CT A
# 0040 -> CT B
# 0060 -> COP A
# 0070 -> COP B
# 0080 -> DDM A
# 0090 -> DDM B
# 00A0 -> RIS
# 00B0 -> SHD

mcuID = {
    'MRA'   : '0010',
    'MRB'   : '0020',
    'CTA'  : '0030',
    'CTB'  : '0040',
    'COPA' : '0060',
    'COPB' : '0070',
    'DDMA' : '0080',
    'DDMB' : '0090',
    'RIS' : '00A0',
    'SHD' : '00B0',
}

# Pulls the version number from version.h file and appends it the the output file name
def UpdateOutputFileName(argv):
    fileName = "Smartrise"
    for i in argv[1:]:
        if i == "App_DDM_A.srec":
            fileName = "DDM"
            break
        elif i == "App_MCU_E.srec":
            fileName = "RIS"
            break
        elif i == "BBGW.srec":
            fileName = "SHIELD"
            break

    versionFile = open("version.h", "r")
    for i in versionFile:
        if 'Vers. ' in i:
            versionName = i.replace("#define SOFTWARE_RELEASE_VERSION (\"Vers. ", "")
            versionName = versionName.replace(".", "_")
            versionName = versionName.replace(" ", "")
            versionName = versionName.replace("\"", "")
            versionName = versionName.replace(")", "")
            versionName = versionName.replace("\n", "")
            fileName = fileName + "_" + versionName + ".sbf"
            print(fileName)
    return fileName

# Appends one of the provided srec files into a sbf file.
# A 3 byte address is also pre pended to each line. This is
# used to help route the bootloader over the network
# SBF = Smartrise Bootloader File
def AppendSingleFile(inFileName, mcu, outFileHandle):
    with open(inFileName, "r") as inFile:
        for i in inFile:
            if 'S3' in i:
                output = mcuID[mcu] + i
                outFileHandle.write(output)

def main(argv):
    with open(UpdateOutputFileName(argv), "w") as outFile:
        for i in argv[1:]:
            if i == "App_MRA.srec":
                AppendSingleFile(i, 'MRA' , outFile)
            elif i == "App_MRB.srec":
                AppendSingleFile(i, 'MRB' , outFile)
            elif i == "App_SRU_A.srec":
                AppendSingleFile(i, 'CTA' , outFile)
                AppendSingleFile(i, 'COPA' , outFile)
            elif i == "App_SRU_B.srec":
                AppendSingleFile(i, 'CTB' , outFile)
                AppendSingleFile(i, 'COPB' , outFile)
            elif i == "App_DDM_A.srec":
                AppendSingleFile(i, 'DDMA', outFile)
            elif i == "App_DDM_B.srec":
                AppendSingleFile(i, 'DDMB', outFile)
            elif i == "App_MCU_E.srec":
                AppendSingleFile(i, 'RIS', outFile)
            elif i == "BBGW.srec":
                AppendSingleFile(i, 'SHD', outFile)

if __name__ == "__main__":
    main(sys.argv)