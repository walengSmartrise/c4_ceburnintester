#include <stdint.h>

#include "dip.h"
#include "chip.h"
#include "deployment.h"
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t GetDip( void )
{
    uint8_t result = 0;
    if(isMR_A() || isCT_A() || isCOP_A() || isDDM_A())
    {
        result = !Chip_GPIO_GetPinState( LPC_GPIO, 1, 15  );
    }
    else if(isRiser())
    {
    	result = // !Chip_GPIO_GetPinState( LPC_GPIO, 0, 6  ) && // DIP 1
    			 // !Chip_GPIO_GetPinState( LPC_GPIO, 0, 9  ) && // DIP 4
				 !Chip_GPIO_GetPinState( LPC_GPIO, 2, 3  ) && //DIP 8
				// Chip_GPIO_GetPinState( LPC_GPIO, 0, 7  ) && //DIP 2
				// Chip_GPIO_GetPinState( LPC_GPIO, 0, 8  ) && // DIP3
				 !Chip_GPIO_GetPinState( LPC_GPIO, 2, 0  ) && //DIP5
				 !Chip_GPIO_GetPinState( LPC_GPIO, 2, 1  ) && //DIP6
				 !Chip_GPIO_GetPinState( LPC_GPIO, 2, 2  ) ; // DIP7
    }
    else // Must be a B processor
    {
        result  = !Chip_GPIO_GetPinState( LPC_GPIO, 0, 13  );
    }

    return result;
}
