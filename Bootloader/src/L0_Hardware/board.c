/*
 * @brief Embedded Artists LPC1788 and LPC4088 Development Kit board file
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2012
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#include "board_api.h"
#include "deployment.h"
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

// TODO: have the clk rate be a #def in the config file
const uint32_t OscRateIn = 12000000;

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/* Set up and initialize all required blocks and functions related to the
 board hardware */
void Board_Init( void )
{
    /* Initializes GPIO */

    /* Initialize LEDs */
    Board_LED_Init();

    /* Initialize UART */
    Board_UART_Init();

    // Initialize Timer1 */
    Board_TIMER_Init();

    /* Initializes CAN */
    Board_CAN_Init();

    // Enable all interrupts for bootloader
    Board_Enable_Interrupt();
}

void Board_Enable_Interrupt( void )
{
    // Enable the UART interrupt

    NVIC_SetPriority( UART1_IRQn, 1 );
    NVIC_EnableIRQ( UART1_IRQn );
    NVIC_SetPriority( UART3_IRQn, 1 );
    NVIC_EnableIRQ( UART3_IRQn );

    // Enable the Timer interrupt
    //NVIC_ClearPendingIRQ(TIMER_IRQ_SELECTION);
    NVIC_EnableIRQ( TIMER_IRQ_SELECTION );

    NVIC_EnableIRQ( CAN_IRQn );
    __enable_irq();
}



void System_Init (void)
{
    SetMCU_ID();
    SetDeployment();
}
