#include "timer.h"
#include "chip.h"
#include "config.h"
#include "led.h"

#define TICK_RATE_HZ1 (Chip_Clock_GetSystemClockRate() / (TICKS_PER_SECOND) )

void Board_TIMER_Init( void )
{
    Chip_TIMER_Init( TIMER_SELECTION );
    Chip_TIMER_Reset( TIMER_SELECTION );
    Chip_TIMER_MatchEnableInt( TIMER_SELECTION, 1 );
    Chip_TIMER_SetMatch( TIMER_SELECTION, 1, TICK_RATE_HZ1 );
    Chip_TIMER_ResetOnMatchEnable( TIMER_SELECTION, 1 );
    Chip_TIMER_Enable( TIMER_SELECTION );
}

void Board_TIMER_DeInit( void )
{
    Chip_TIMER_DeInit( TIMER_SELECTION );
    NVIC_DisableIRQ( TIMER_IRQ_SELECTION );
}

extern volatile uint32_t bootLoader_Timeout;
extern uint8_t inBootLoaderMode;
extern void ExecuteUserApllication( void );

void TIMER_HANDLER_NAME( void )
{
    static uint8_t pattern = 0;

    bootLoader_Timeout++;

    if ( Chip_TIMER_MatchPending( TIMER_SELECTION, 1 ) )
    {
        Chip_TIMER_ClearMatch( TIMER_SELECTION, 1 );
        // TODO: rewrite this so its not dependant on the hardware(eg 3 led what if i add or remove 1)
        // My idea is to use num_led and some counter to create the below pattern.
        if ( inBootLoaderMode )
        {
            switch ( pattern )
            {
                case 0:
                    Board_LED_Set( Fault, 1 );
                    Board_LED_Set( Alarm, 0 );
                    Board_LED_Set( Heartbeat, 0 );
                    pattern = 1;
                    break;
                case 1:
                    Board_LED_Set( Fault, 0 );
                    Board_LED_Set( Alarm, 1 );
                    Board_LED_Set( Heartbeat, 0 );
                    pattern = 2;
                    break;
                case 2:
                    Board_LED_Set( Fault, 0 );
                    Board_LED_Set( Alarm, 0 );
                    Board_LED_Set( Heartbeat, 1 );
                    pattern = 3;
                    break;
                case 3:
                    Board_LED_Set( Fault, 0 );
                    Board_LED_Set( Alarm, 1 );
                    Board_LED_Set( Heartbeat, 0 );
                    pattern = 0;
                    break;
            }
        }
        else
        {
            static uint8_t flag = 0;

            Board_LED_Toggle( Fault );
            Board_LED_Set( Alarm, 0 );
            Board_LED_Toggle( Heartbeat );

            flag = ~flag;
        }
    }


#if 0
    if ( bootLoader_Timeout > BOOT_LOADER_TIMEOUT_VALUE )
    {
        if ( !inBootLoaderMode )
        {
            switch ( GetMCU_ID() )
            {
                case MCU_A:
                    ExecuteUserApllication();
                    break;
                case MCU_B:

                    break;

                default:

                    break;
            }
        }
    }
#endif
}
