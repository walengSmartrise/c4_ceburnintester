#include "can.h"
#include "chip.h"
#include "config.h"
#include "deployment.h"
#include "message.h"
#include "uart.h"

#define CAN_RING_BUFFER_SIZE  (256)

RINGBUFF_T RxRingBuffer_CAN1;
RINGBUFF_T RxRingBuffer_CAN2;

RINGBUFF_T TxRingBuffer_CAN1;
RINGBUFF_T TxRingBuffer_CAN2;

static CAN_MSG_T RxBuffer_CAN1[CAN_RING_BUFFER_SIZE];
static CAN_MSG_T RxBuffer_CAN2[CAN_RING_BUFFER_SIZE];

static CAN_MSG_T TxBuffer_CAN1[CAN_RING_BUFFER_SIZE];
static CAN_MSG_T TxBuffer_CAN2[CAN_RING_BUFFER_SIZE];



void RingBufferInit_CAN( void )
{
    // Init the Rx Ring buffer.
    RingBuffer_Init( &RxRingBuffer_CAN1, RxBuffer_CAN1, sizeof ( RxBuffer_CAN1[0] ), CAN_RING_BUFFER_SIZE );
    RingBuffer_Init( &RxRingBuffer_CAN2, RxBuffer_CAN2, sizeof ( RxBuffer_CAN2[0] ), CAN_RING_BUFFER_SIZE );

    // Init the Tx Ring buffer.
    RingBuffer_Init( &TxRingBuffer_CAN1, TxBuffer_CAN1, sizeof ( TxBuffer_CAN1[0] ), CAN_RING_BUFFER_SIZE );
    RingBuffer_Init( &TxRingBuffer_CAN2, TxBuffer_CAN2, sizeof ( TxBuffer_CAN2[0] ), CAN_RING_BUFFER_SIZE );

}
void Board_CAN_Init( void )
{

    // SRU boards must have CAN 2 & 4 set to 250k so they can AWK the CEDES cammera
    if(GetDeployment( ) == enSRU_DEPLOYMENT__MR)
    {
        switch ( GetMCU_ID() )
        {
            case MCU_A:

                Chip_CAN_Init( LPC_CAN1, LPC_CANAF, LPC_CANAF_RAM );
                Chip_CAN_SetBitRate( LPC_CAN1, 125000 );
                Chip_CAN_EnableInt( LPC_CAN1, CAN_IER_BITMASK );
                Chip_CAN_SetAFMode( LPC_CANAF, CAN_AF_BYBASS_MODE );

                Chip_CAN_Init( LPC_CAN2, LPC_CANAF, LPC_CANAF_RAM );
                Chip_CAN_SetBitRate( LPC_CAN2, 125000 );
                Chip_CAN_EnableInt( LPC_CAN2, CAN_IER_BITMASK );
                Chip_CAN_SetAFMode( LPC_CANAF, CAN_AF_BYBASS_MODE );

                NVIC_EnableIRQ( CAN_IRQn );

                break;
            case MCU_B:

                Chip_CAN_Init( LPC_CAN1, LPC_CANAF, LPC_CANAF_RAM );
                Chip_CAN_SetBitRate( LPC_CAN1, 125000 );
                Chip_CAN_EnableInt( LPC_CAN1, CAN_IER_BITMASK );
                Chip_CAN_SetAFMode( LPC_CANAF, CAN_AF_NORMAL_MODE );

                // Set up the CAN acceptance filter, set up to accept any 11 bit ID and nothing else
                CAN_STD_ID_RANGE_ENTRY_T StdGrpEntryCan1 =
                {
                        // CAN INTERFACE, DISABLE, ID
                        {0, 0, 0x000},
                        {0, 0, 0x7FF}
                };
                Chip_CAN_InsertGroupSTDEntry(LPC_CANAF, LPC_CANAF_RAM, &StdGrpEntryCan1);

                Chip_CAN_Init( LPC_CAN2, LPC_CANAF, LPC_CANAF_RAM );
                Chip_CAN_SetBitRate( LPC_CAN2, 125000 );
                Chip_CAN_EnableInt( LPC_CAN2, CAN_IER_BITMASK );
                Chip_CAN_SetAFMode( LPC_CANAF, CAN_AF_NORMAL_MODE );
                CAN_STD_ID_RANGE_ENTRY_T StdGrpEntryCan2 =
                {
                        // CAN INTERFACE, DISABLE, ID
                        {1, 0, 0x000},
                        {1, 0, 0x7FF}
                };
                Chip_CAN_InsertGroupSTDEntry(LPC_CANAF, LPC_CANAF_RAM, &StdGrpEntryCan2);

                NVIC_EnableIRQ( CAN_IRQn );

                break;

            default:

                break;

        }
    }
    else if(isShield())
    {
        Chip_CAN_Init( LPC_CAN1, LPC_CANAF, LPC_CANAF_RAM );
        Chip_CAN_SetBitRate( LPC_CAN1, 125000 );
        Chip_CAN_EnableInt( LPC_CAN1, CAN_IER_BITMASK );
        Chip_CAN_SetAFMode( LPC_CANAF, CAN_AF_BYBASS_MODE );

        Chip_CAN_Init( LPC_CAN2, LPC_CANAF, LPC_CANAF_RAM );
        Chip_CAN_SetBitRate( LPC_CAN2, 125000 );
        Chip_CAN_EnableInt( LPC_CAN2, CAN_IER_BITMASK );
        Chip_CAN_SetAFMode( LPC_CANAF, CAN_AF_BYBASS_MODE );

        NVIC_EnableIRQ( CAN_IRQn );
    }
    // Anoy other board
    else
    {
        switch ( GetMCU_ID() )
        {
            case MCU_A:

                Chip_CAN_Init( LPC_CAN1, LPC_CANAF, LPC_CANAF_RAM );
                Chip_CAN_SetBitRate( LPC_CAN1, 125000 );
                Chip_CAN_EnableInt( LPC_CAN1, CAN_IER_BITMASK );
                Chip_CAN_SetAFMode( LPC_CANAF, CAN_AF_BYBASS_MODE );

                Chip_CAN_Init( LPC_CAN2, LPC_CANAF, LPC_CANAF_RAM );
                Chip_CAN_SetBitRate( LPC_CAN2, 250000 );
                Chip_CAN_EnableInt( LPC_CAN2, CAN_IER_BITMASK );
                Chip_CAN_SetAFMode( LPC_CANAF, CAN_AF_BYBASS_MODE );

                NVIC_EnableIRQ( CAN_IRQn );

                break;
            case MCU_B:

                Chip_CAN_Init( LPC_CAN1, LPC_CANAF, LPC_CANAF_RAM );
                Chip_CAN_SetBitRate( LPC_CAN1, 125000 );
                Chip_CAN_EnableInt( LPC_CAN1, CAN_IER_BITMASK );
                Chip_CAN_SetAFMode( LPC_CANAF, CAN_AF_BYBASS_MODE );

                Chip_CAN_Init( LPC_CAN2, LPC_CANAF, LPC_CANAF_RAM );
                Chip_CAN_SetBitRate( LPC_CAN2, 250000 );
                Chip_CAN_EnableInt( LPC_CAN2, CAN_IER_BITMASK );
                Chip_CAN_SetAFMode( LPC_CANAF, CAN_AF_BYBASS_MODE );

                NVIC_EnableIRQ( CAN_IRQn );

                break;

            default:

                break;

        }
    }

    RingBufferInit_CAN();
}

void Board_CanSend(Packet* packet)
{
    CAN_MSG_T temp;
    temp.ID  =  packet->id;
    temp.DLC = 8;
    temp.Type = 0;
    for(uint8_t i = 0; i<8; i++)
    {
        temp.Data[i] = packet->data[i];
    }

    if(isMR_A()||isRiser())
    {

        CAN_BUFFER_ID_T TxBuf_CarNet = Chip_CAN_GetFreeTxBuf( LPC_CAN1 );
        Chip_CAN_Send( LPC_CAN1, TxBuf_CarNet, &temp );
    }
    else if(isMR_B() || isShield())
    {
        CAN_BUFFER_ID_T TxBuf_CarNet = Chip_CAN_GetFreeTxBuf( LPC_CAN2 );
        Chip_CAN_Send( LPC_CAN2, TxBuf_CarNet, &temp );
    }
    else if(isCT_A() || isCOP_A() || isDDM_B())
    {
        CAN_BUFFER_ID_T TxBuf_CarNet = Chip_CAN_GetFreeTxBuf( LPC_CAN1 );
        Chip_CAN_Send( LPC_CAN1, TxBuf_CarNet, &temp );
    }
}


void Board_SendMessageAwk( Packet *packet)
{
    CAN_MSG_T temp;
    temp.ID   =  packet->id;
    temp.DLC  = 8;
    temp.Type = 0;

    for(uint8_t i = 0; i<8; i++)
    {
        temp.Data[i] = packet->data[i];
    }


    if(isMR_A())
    {
        Board_UARTPutSTR(packet);
    }
    else if(isMR_B() || isShield())
    {
        CAN_BUFFER_ID_T TxBuf_CarNet = Chip_CAN_GetFreeTxBuf( LPC_CAN2 );
        Chip_CAN_Send( LPC_CAN2, TxBuf_CarNet, &temp );
    }
    else if(isCT_A() || isCOP_A() || isDDM_B() || isRiser())
    {
        CAN_BUFFER_ID_T TxBuf_CarNet = Chip_CAN_GetFreeTxBuf( LPC_CAN1 );
        Chip_CAN_Send( LPC_CAN1, TxBuf_CarNet, &temp );
    }

}

void Board_SendBootloaderStartup( Packet* packet )
{
    CAN_MSG_T temp;
    temp.ID   =  packet->id;
    temp.DLC  = 8;
    temp.Type = 0;

    for(uint8_t i = 0; i<8; i++)
    {
        temp.Data[i] = packet->data[i];
    }


    if(isMR_B())
    {
        CAN_BUFFER_ID_T TxBuf_CarNet = Chip_CAN_GetFreeTxBuf( LPC_CAN2 );
        Chip_CAN_Send( LPC_CAN2, TxBuf_CarNet, &temp );
    }
    else if(isCT_A() || isCOP_A() || isDDM_A())
    {
        CAN_BUFFER_ID_T TxBuf_CarNet = Chip_CAN_GetFreeTxBuf( LPC_CAN1 );
        Chip_CAN_Send( LPC_CAN1, TxBuf_CarNet, &temp );
    }
    else if(isShield())
    {

    }

}

void Board_CAN_DeInit( void )
{

}

// MRA :
// MRB : Rx on CAN2 -> CAN3 on PCB
// SRUA:
// SRUB:

RINGBUFF_T* GetRxBuffer( void )
{
    RINGBUFF_T* result;

    if(GetMCU_ID() == MCU_A)
    {
        result = &RxRingBuffer_CAN1;
    }
    else
    {
        result = &RxRingBuffer_CAN2;
    }

    return result;
}

// Pop an element from the CAN ring buffer. If it relates to this node
// then set it as the recentRX message.
void ProcessCanMessage(RINGBUFF_T* rxBuffer)
{
    CAN_MSG_T tmp;
    Packet newPacket;
    // CAN contains error correcting within the protocol itself.
    // If there is a message there very small chance that there was an
    // error with the message
    if ( RingBuffer_Pop( rxBuffer, &tmp ) )
    {
        SetPacket(&newPacket,tmp.ID,tmp.Data);


        if(isMR_A())
        {
            // Forward to all nodes and store a copy locally
            if((newPacket.id == 0x7FF) || (newPacket.id == 0x284407e))
            {
                StorePacket(&newPacket);
                Board_CanSend(&newPacket);
            }
            else if((newPacket.id & ID_MASK) == MR_A_ID)
            {
                StorePacket(&newPacket);
            }
            // Pass Message Up
            else if((newPacket.id & ID_MASK) == BBB_ID)
            {
                Board_SendMessageAwk(&newPacket);
            }
            else
            {
                Board_CanSend(&newPacket);
                if((newPacket.id > 0x7FF))
                {
                    unsigned debug;
                    debug = 1;
                }
            }
        }
        else if(isMR_B())
        {
            // Forward to all nodes and store a copy locally
        	if((newPacket.id == 0x7FF) || (newPacket.id == 0x284407e))
            {
        		SEGGER_RTT_printf(0, "%x\n", newPacket.id);
                StorePacket(&newPacket);
                Board_UARTPutSTR(&newPacket);
            }
            // Store a coppy
            else if( (newPacket.id & ID_MASK) == MR_B_ID)
            {
                StorePacket(&newPacket);
            }
            // Pass Message Up
            else if((newPacket.id & ID_MASK) == BBB_ID)
            {
                Board_SendMessageAwk(&newPacket);
            }
            else
            {
                Board_UARTPutSTR(&newPacket);
            }
        }
        else if(isCT_A())
        {
            // Forward to all nodes and store a copy locally
        	if((newPacket.id == 0x7FF) || (newPacket.id == 0x284407e))
            {
                StorePacket(&newPacket);
                Board_UARTPutSTR(&newPacket);
            }
            else if((newPacket.id & ID_MASK) == CT_A_ID)
            {
                StorePacket(&newPacket);
            }
            // Pass Message Up
            else if((newPacket.id & ID_MASK) == BBB_ID)
            {
                if(newPacket.data[0] == CT_A_ID)
                {
                    Board_SendMessageAwk(&newPacket);
                }
            }
            else
            {
                Board_UARTPutSTR(&newPacket);
            }
        }
        else if(isCT_B())
        {
            // Forward to all nodes and store a copy locally
            if(newPacket.id & 0x7FF == 0x7FF)
            {
                //StorePacket(&newPacket);
            }
            // Store locally
            else if((newPacket.id & ID_MASK) == CT_B_ID)
            {

            }
            // Pass Message Up
            else if((newPacket.id & ID_MASK) == BBB_ID)
            {
                // Really this is not needed. SRUB only Tx/Rx on UART
                if(newPacket.data[0] == CT_B_ID)
                {
                    Board_SendMessageAwk(&newPacket);
                }
            }
            else
            {

            }
        }
        else if(isCOP_A())
        {
            // Forward to all nodes and store a copy locally
            if((newPacket.id == 0x7FF) || (newPacket.id == 0x284407e))
            {
                StorePacket(&newPacket);
                Board_UARTPutSTR(&newPacket);
            }
            else if((newPacket.id & ID_MASK) == COP_A_ID)
            {
                StorePacket(&newPacket);
            }
            // Pass Message Up
            else if((newPacket.id & ID_MASK) == BBB_ID)
            {
                if(newPacket.data[0] == COP_A_ID)
                {
                    Board_SendMessageAwk(&newPacket);
                }
            }
            else
            {
                Board_UARTPutSTR(&newPacket);
            }
        }
        else if(isCOP_B())
        {
            // Forward to all nodes and store a copy locally
            if(newPacket.id & 0x7FF)
            {
                //StorePacket(&newPacket);
            }
            // Store locally
            else if((newPacket.id & ID_MASK) == COP_B_ID)
            {
                //StorePacket(&newPacket);
            }
            // Pass Message Up
            else if((newPacket.id & ID_MASK) == BBB_ID)
            {
                // Really this is not needed. SRUB only Tx/Rx on UART
                if(newPacket.data[0] == COP_B_ID)
                {
                    Board_SendMessageAwk(&newPacket);
                }
            }
            else
            {

            }
        }
        else if(isDDM_A())
        {
            // Forward to all nodes and store a copy locally
        	SEGGER_RTT_printf(0,"%x\n",newPacket.id & 0xF00);
            if(newPacket.id == 0x7FF)
            {
                StorePacket(&newPacket);
//                Board_UARTPutSTR(&newPacket);
            }
            else if((newPacket.id & 0xF00) == DDM_A_ID)
            {
                StorePacket(&newPacket);
            }
            // Pass Message Up
            else if((newPacket.id & ID_MASK) == BBB_ID)
            {
                if(newPacket.data[0] == DDM_A_ID)
                {
                    Board_SendMessageAwk(&newPacket);
                }
            }
            else
            {

            	//Board_UARTPutSTR(&newPacket);
            }
        }
        else if(isDDM_B())
        {
            // Forward to all nodes and store a copy locally
            if(newPacket.id & 0x7FF == 0x7FF)
            {
                //StorePacket(&newPacket);
                StorePacket(&newPacket);
                Board_UARTPutSTR(&newPacket);
            }
            // Store locally
            else if((newPacket.id & 0xF00) == DDM_B_ID)
            {

            }
            // Pass Message Up
            else if((newPacket.id & ID_MASK) == BBB_ID)
            {
                // Really this is not needed. SRUB only Tx/Rx on UART
                if(newPacket.data[0] == DDM_B_ID)
                {
                    Board_SendMessageAwk(&newPacket);
                }
            }
            else
            {
            	Board_UARTPutSTR(&newPacket);
            }
        }
        else if(isRiser())
        {
        	if(newPacket.id == 0x7FF)
			{
				StorePacket(&newPacket);
			}
			else if((newPacket.id & 0xF00) == RISER_ID)
			{
				StorePacket(&newPacket);
			}
			// Pass Message Up
			else if((newPacket.id & ID_MASK) == BBB_ID)
			{
				if(newPacket.data[0] == RISER_ID)
				{
					Board_SendMessageAwk(&newPacket);
				}
			}
        }
        else if(isShield())
        {
        	if(newPacket.id == 0x7FF)
			{
				StorePacket(&newPacket);
			}
			else if((newPacket.id & 0xF00) == SHIELD_ID)
			{
				StorePacket(&newPacket);
			}
			// Pass Message Up
			else if((newPacket.id & ID_MASK) == BBB_ID)
			{
				if(newPacket.data[0] == SHIELD_ID)
				{
					Board_SendMessageAwk(&newPacket);
				}
			}
        }
    }
}


RINGBUFF_T* GetCanInstance(void)
{
    RINGBUFF_T* result = 0; // assume can periph
    if( isMR_B() || isShield() )
    {
        result = &RxRingBuffer_CAN2;
    }
    else if(isMR_A() || isRiser())
    {
        result = &RxRingBuffer_CAN1;
    }
    else if(isCT_A() || isCOP_A() || isDDM_A())
    {
        result = &RxRingBuffer_CAN1;
    }
    else if(isCT_B() || isCOP_B() || isDDM_B())
    {
        result = &RxRingBuffer_CAN2;
    }
    return result;
}


void CAN_IRQHandler( void )
{
    /* Clear CAN2 Interrupts by Reading ICR Register */
    uint32_t IntStatus_CAN1 = Chip_CAN_GetIntStatus( LPC_CAN1 );
    uint32_t IntStatus_CAN2 = Chip_CAN_GetIntStatus( LPC_CAN2 );

    if ( IntStatus_CAN1 & CAN_ICR_RI ) /* New Msg Received */
    {
        CAN_MSG_T RcvMsgBuf;
        Status result = Chip_CAN_Receive( LPC_CAN1, &RcvMsgBuf );

        if ( result == SUCCESS )
        {
            if(RcvMsgBuf.ID > 0x7FF)
            {
                   unsigned debug;
                   debug = 1;
            }

            // TODO: have a way to check the return value below
            if(!RingBuffer_Insert( &RxRingBuffer_CAN1, &RcvMsgBuf ))
            {
                // issue some kind of error?
            }
        }
        else
        {
            // issue some kind of error
        }
    }

    if ( IntStatus_CAN2 & CAN_ICR_RI ) /* New Msg Received */
    {
        CAN_MSG_T RcvMsgBuf;
        Status result = Chip_CAN_Receive( LPC_CAN2, &RcvMsgBuf );

        if ( result == SUCCESS )
        {
            // TODO: have a way to check the return value below
            if( !RingBuffer_Insert( &RxRingBuffer_CAN2, &RcvMsgBuf ))
            {
                // issue some kind of error
            }
        }
        else
        {
            // issue some kind of error
        }
    }
}
