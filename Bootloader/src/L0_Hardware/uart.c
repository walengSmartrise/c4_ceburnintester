#include "uart.h"
#include "chip.h"
#include "config.h"
#include "can.h"
#include "deployment.h"
#include "message.h"

static LPC_USART_T *uart;



/* Transmit and receive ring buffer sizes */
#define UART_RING_BUFFER_SIZE 128

#define STX (0x02)
#define ETX (0x03)
#define ESC (0x1B)


STATIC RINGBUFF_T RxRingBuffer_UART;
STATIC RINGBUFF_T TxRingBuffer_UART;

static uint8_t RxBuffer_UART[UART_RING_BUFFER_SIZE];
static uint8_t TxBuffer_UART[UART_RING_BUFFER_SIZE];

void RingBufferInit_UART( void )
{
    // Init the Rx Ring buffer.
    RingBuffer_Init( &RxRingBuffer_UART, RxBuffer_UART, sizeof ( RxBuffer_UART[0] ), UART_RING_BUFFER_SIZE );
    RingBuffer_Init( &TxRingBuffer_UART, TxBuffer_UART, sizeof ( RxBuffer_UART[0] ), UART_RING_BUFFER_SIZE );
}



// Pops data from the Rx ring buffer. If the data that is popped contains a new
// message then return true. If no new message then return false
uint8_t NewUartMessage( void )
{

    uint8_t data;
    while(RingBuffer_Pop(&RxRingBuffer_UART, &data))
    {

    }

}


/**
 * @brief   Prints a string to the UART
 * @param   str : Terminated string to output
 * @return  None
 */
void Board_UARTPutSTR(Packet* packet )
{
    // All packets are 12 bytes. 4 bytes for ID and 8 bytes of data.
    // Worst case is all bytes are a control word. In that case the buffer
    // to send would contain 26 bytes, 1 STX byte, 12 ESC byts,
    // 12 Data byte, and 1 ETX


    // STX = 0x02
    // ETX = 0x03
    // ESC = 0x1B

    uint8_t data[26] = {0};
    uint8_t index = 0;

    uint8_t id[4];

    // Break ID into bytes
    id[0]  = (packet -> id & 0xFF000000) >> 24;
    id[1]  = (packet -> id & 0x00FF0000) >> 16;
    id[2]  = (packet -> id & 0x0000FF00) >> 8;
    id[3]  = (packet -> id & 0x000000FF) >> 0;

    // Encode the byte stream with STX, ETX, and ESC characters
    // Packets contain always contain 12 bytes
    data[index++] = STX;
    // Transfer the ID to the buffer
    for(uint8_t i=0; i<4; i++)
    {
        if( (id[i] == 0x02) || (id[i] == 0x03) || (id[i] == 0x1B) )
        {
            data[index++] = ESC;

        }
        data[index++] = id[i];
    }
    // There are always 8 data bytes
    for(uint8_t i=0; i<8; i++)
    {
        if( (packet->data[i] == 0x02) || (packet->data[i] == 0x03) || (packet->data[i] == 0x1B) )
        {
            data[index++] = ESC;

        }
        data[index++] = packet->data[i];
    }
    data[index++] = ETX;


    // Place all 12 bytes int the uart transmit ring buffer
     int32_t sent = Chip_UART_SendRB(uart, &TxRingBuffer_UART, data, index);
    // Used to supress warnings for sent.
    // TODO: should have some kind of error checking that sned is equal to num bytes.
    // if they are not equal then an error occured.
    (void) sent;
}

/* Initialize UART pins */
void Board_UART_Init( void )
{
    // Initialize the pins
#define UART(port, pin, iocon) Chip_IOCON_PinMuxSet(LPC_IOCON, port, pin, iocon);
    UART_TABLE
#undef UART

    // Select which uart should be initialized.
    switch ( GetMCU_ID() )
    {
        case MCU_B:
            uart = MCUB_UART;
            break;
        case MCU_A:
            uart = MCUA_UART;
            break;
        default:
            //TODO: Issue some kind of error
            break;

    }

    /* Setup UART for 115.2K8N1 */
    Chip_UART_Init( uart );
    Chip_UART_SetBaud( uart, 115200 );
    Chip_UART_ConfigData( uart, ( UART_LCR_WLEN8 | UART_LCR_SBS_1BIT ) );
    Chip_UART_SetupFIFOS( uart, ( UART_FCR_FIFO_EN | UART_FCR_TRG_LEV3 ) );
    Chip_UART_TXEnable( uart );

    /* Before using the ring buffers, initialize them using the ring buffer init function */
    RingBufferInit_UART();

    /* Reset and enable FIFOs, FIFO trigger level 3 (14 chars) */
    Chip_UART_SetupFIFOS(uart,( UART_FCR_FIFO_EN | UART_FCR_RX_RS | UART_FCR_TX_RS | UART_FCR_TRG_LEV3 ) );

    /* Enable receive data and line status interrupt */
    Chip_UART_IntEnable(uart, (UART_IER_RBRINT | UART_IER_RLSINT));

    // Select which uart should be initilized.
    switch ( GetMCU_ID() )
    {
        case MCU_B:
            /* preemption = 1, sub-priority = 1 */
            NVIC_SetPriority(MCUB_UART_IRQ, 1);
            NVIC_EnableIRQ(MCUB_UART_IRQ);
            break;
        case MCU_A:
            /* preemption = 1, sub-priority = 1 */
            NVIC_SetPriority(MCUA_UART_IRQ, 1);
            NVIC_EnableIRQ(MCUA_UART_IRQ);
            break;
        default:

            break;

    }



}

//TODO: rename to GetUartRBInstance(void)
RINGBUFF_T* GetUartInstance(void)
{
    RINGBUFF_T* result = 0; // assume can periph
       if( isMR_A())
       {
           result = &RxRingBuffer_UART;
       }

       return result;
}

// Packet needs to be routed. If the ID matches this board then
// The packet shall be stored on on this board. If the packet has
// The global ID 7FF then it is stored and passed to all other
// boards on the network.
void ForwardMessage( Packet* newPacket )
{
    if(isMR_A())
    {
        // Forward to all nodes and store a copy locally
    	if((newPacket->id == 0x7FF) || (newPacket->id == 0x284407e))
        {
    		SEGGER_RTT_printf(0,"%x\n", newPacket->id);
            StorePacket(newPacket);
            Board_CanSend(newPacket);
        }
        else if((newPacket->id & ID_MASK) == MR_A_ID)
        {
            StorePacket(newPacket);
        }
        // Pass the message up
        else if((newPacket->id & ID_MASK) == BBB_ID)
        {
            Board_UARTPutSTR(newPacket);
        }
        else
        {
            Board_CanSend(newPacket);
        }
    }
    else if(isMR_B())
    {
        // Pass the message down and store a copy locally
    	if((newPacket->id == 0x7FF) || (newPacket->id == 0x284407e))
        {
            StorePacket(newPacket);
            Board_UARTPutSTR(newPacket);
        }
        // just copy locally
        else if((newPacket->id & ID_MASK) == MR_B_ID)
        {
            StorePacket(newPacket);
        }
        // Pass the message up
        else if((newPacket->id & ID_MASK) == BBB_ID)
        {
            Board_SendMessageAwk(newPacket);
        }
        else
        {
            Board_UARTPutSTR(newPacket);
        }
    }
    else if(isCT_A())
    {
        // Send up network to PC/BBB
    	if((newPacket->id == 0x7FF) || (newPacket->id == 0x4284407e))
        {
            StorePacket(newPacket);
            Board_UARTPutSTR(newPacket);
        }
        // Save copy locally
        else if((newPacket->id & ID_MASK) == CT_A_ID)
        {

                StorePacket(newPacket);

        }
        // Pass the message up
        else if((newPacket->id & ID_MASK) == BBB_ID)
        {
            if( (newPacket->data[0] == (CT_A_ID>>8)) || (newPacket->data[0] == (CT_B_ID>>8)))
            {
                Board_SendMessageAwk(newPacket);
            }
        }
        else
        {
            Board_UARTPutSTR(newPacket);
        }
    }
    else if(isCT_B())
    {
        // Forward to all nodes and store a copy locally
    	if((newPacket->id == 0x7FF) || (newPacket->id == 0x284407e))
        {
            StorePacket(newPacket);
        }
        else if((newPacket->id & ID_MASK) == CT_B_ID)
        {
            StorePacket(newPacket);
        }
        // Pass the message up
        else if((newPacket->id & ID_MASK) == BBB_ID)
        {
            if(newPacket->data[0] == CT_B_ID)
            {
                Board_SendMessageAwk(newPacket);
            }
        }
        else
        {

        }
    }
    else if(isCOP_A())
    {
        // Send up network to PC/BBB
    	if((newPacket->id == 0x7FF) || (newPacket->id == 0x284407e))
        {
            StorePacket(newPacket);
            Board_UARTPutSTR(newPacket);
        }
        // Save copy locally
        else if((newPacket->id & ID_MASK) == COP_A_ID)
        {
            StorePacket(newPacket);
        }
        // Pass the message up
        else if((newPacket->id & ID_MASK) == BBB_ID)
        {
            if( (newPacket->data[0] == (COP_A_ID>>8)) ||(newPacket->data[0] == (COP_B_ID>>8)) )
            {
                Board_SendMessageAwk(newPacket);
            }
        }
        else
        {
            Board_UARTPutSTR(newPacket);
        }
    }
    else if(isCOP_B())
    {
        // Forward to all nodes and store a copy locally
    	if((newPacket->id == 0x7FF) || (newPacket->id == 0x284407e))
        {
            StorePacket(newPacket);
        }
        else if((newPacket->id & ID_MASK) == COP_B_ID)
        {
            StorePacket(newPacket);
        }
        // Pass the message up
        else if((newPacket->id & ID_MASK) == BBB_ID)
        {
            if(newPacket->data[0] == COP_B_ID)
            {
                Board_SendMessageAwk(newPacket);
            }
        }
        else
        {

        }
    }
    else if(isDDM_A())
    {
        // Send up network to PC/BBB
        if(newPacket->id == 0x7FF)
        {
            StorePacket(newPacket);
            Board_UARTPutSTR(newPacket);
        }
        // Save copy locally
        else if((newPacket->id & 0xF00) == DDM_A_ID)
        {
            StorePacket(newPacket);
        }
        // Pass the message up
        else if((newPacket->id & ID_MASK) == BBB_ID)
        {
            if( (newPacket->data[0] == (DDM_A_ID>>8)) ||(newPacket->data[0] == (DDM_B_ID>>8)) )
            {
                Board_SendMessageAwk(newPacket);
            }
        }
        else
        {
            Board_UARTPutSTR(newPacket);
        }
    }
    else if(isDDM_B())
    {
        // Forward to all nodes and store a copy locally
        if(newPacket->id == 0x7FF)
        {
            StorePacket(newPacket);
        }
        else if((newPacket->id & 0xFF00) == DDM_B_ID)
        {
            StorePacket(newPacket);
        }
        // Pass the message up
        else if((newPacket->id & ID_MASK) == BBB_ID)
        {
            if(newPacket->data[0] == DDM_B_ID)
            {
                Board_SendMessageAwk(newPacket);
            }
        }
        else
        {

        }
    }
    else if(isRiser())
    {
    	if(newPacket->id == 0x7FF)
		{
			StorePacket(newPacket);
			Board_UARTPutSTR(newPacket);
		}
		// Save copy locally
		else if((newPacket->id & ID_MASK) == RISER_ID)
		{
			StorePacket(newPacket);
		}
		// Pass the message up
		else if((newPacket->id & ID_MASK) == BBB_ID)
		{
			if( (newPacket->data[0] == (RISER_ID>>8)) ||(newPacket->data[0] == (RISER_ID>>8)) )
			{
				Board_SendMessageAwk(newPacket);
			}
		}
    }
}

// Reads in uart characters that are stored in the ring buffer. It reads
// the characters in one at a time until a valid set of characters has been read in.
// A valid set contains a "Packet" that is enclosed in an ETX and STX
void ProcessUartMessage(RINGBUFF_T* rxBuffer)
{


    // Read bytes from teh uart ring buffer. Read in bytes until one of two things happen
    // finish reading a full packet or ring buffer becomes empty.
    // If the ring buffer is empty then then the data stored so far must be stored.

    // UART uses an STX, ETX, ESC protocol. This way the start and end of a sequence can
    // easliy be seen. If our data contains a byte that matches STX, ETX or ESC then that
    // byte must have an ESC char prepended. This lets the system know that the next character is data
    // not a control byte.

    // Data received from the ring buffer will be placed here
    static uint8_t data[26] = {0};
    static uint8_t lastCharWasESC = 0;
    // This is bounded from 0 to 25
    static uint8_t index = 0;
    uint8_t temp;

    Packet newPacket;
    // CAN contains error correcting within the protocol itself.
    // If there is a message there very small chance that there was an
    // error with the message

    while ( RingBuffer_Pop(&RxRingBuffer_UART , &temp ) )
    {

        // If last char was an ESC character then that means the current char
        // is not a command byte but a data byte.
        if(lastCharWasESC)
        {
            data[index++] = temp;
            lastCharWasESC = 0;
        }
        else if (index >= 26)
        {
            // Something bad happened. Reset the index counter
            index = 0;
        }
        else
        {
            // Start of text character. Reset the index and start
            // processing the new message
            if( (temp == STX))
            {
                index = 0;
            }
            // End of text character. Packet received. process the packet and break
            // out of this while loop
            else if(temp == ETX)
            {

                // Id is 4 bytes -> data index 0 to 3
                // Data is 8 bytes -> data index 4 to 7
                // The following function is splitting the array into these parts
                uint32_t id = 0;
                id = data[0]<<24;
                id |= data[1]<<16;
                id |= data[2]<<8;
                id |= data[3]<<0;


                SetPacket(&newPacket,id,&data[4]);

                ForwardMessage(&newPacket);

                break;
            }
            // ESC character was received so the next byte is not a
            // control byte but a data byte
            else if(temp == ESC)
            {
                lastCharWasESC = 1;
            }
            else
            {
                data[index++] = temp;
            }
        }



    }
}



//MCU B
void UART1_IRQHandler(void)
{
    /* Want to handle any errors? Do it here. */

    /* Use default ring buffer handler. Override this with your own
       code if you need more capability. */
    Chip_UART_IRQRBHandler(uart, &RxRingBuffer_UART, &TxRingBuffer_UART);
}

//MCU A
void UART3_IRQHandler(void)
{
    /* Want to handle any errors? Do it here. */

    /* Use default ring buffer handler. Override this with your own
       code if you need more capability. */
    Chip_UART_IRQRBHandler(uart, &RxRingBuffer_UART, &TxRingBuffer_UART);
}

void Board_UART_DeInit( void )
{
    Chip_UART_DeInit( uart );
}
