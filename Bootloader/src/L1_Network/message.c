#include <stdint.h>
#include "message.h"
#include "config.h"
#include "uart.h"
#include "can.h"
#include "ring_buffer.h"
#include "deployment.h"
#include "debugging.h"
#define PACKET_RING_BUFFER_SIZE  (32)

RINGBUFF_T RxRingBuffer_Packets;
static Packet RxBuffer_Packets[PACKET_RING_BUFFER_SIZE];

RINGBUFF_T TxRingBuffer_Packets;
static Packet TxBuffer_Packets[PACKET_RING_BUFFER_SIZE];


extern const Packet BootloaderCAN_BootladerStartup;
extern const Packet BootloaderCAN_BootCode;
extern const Packet BootloaderCAN_BootCodeAwk;
extern const Packet BootloaderCAN_ExecuteCode;
extern const Packet BootloaderCAN_StartCode;


void Network_Init( void )
{
    RingBuffer_Init( &RxRingBuffer_Packets, RxBuffer_Packets, sizeof ( RxBuffer_Packets[0] ), PACKET_RING_BUFFER_SIZE );
    RingBuffer_Init( &TxRingBuffer_Packets, TxBuffer_Packets, sizeof ( TxBuffer_Packets[0] ), PACKET_RING_BUFFER_SIZE );
}


// Insert the message into the input message ring buffer
void StorePacket(Packet* passed)
{
    // TODO: Only store the message if its within range
//    if(passed->id < 0x100)
//    {
//        PRINT_WARNING("Invalid Message\n");
//    }
    RingBuffer_Insert(&RxRingBuffer_Packets, passed);
}

// Sets bytes 0 - 3 of recent message
void SetRecentMessageID(Packet* packet, const uint32_t id)
{
    // Only lower 29 bits are supported
    packet->id = id & 0x1FFFFFFF;
}

void SetRecentMessageData( Packet* packet, const uint8_t const* data )
{
    packet->data[0] = data[0];
    packet->data[1] = data[1];
    packet->data[2] = data[2];
    packet->data[3] = data[3];
    packet->data[4] = data[4];
    packet->data[5] = data[5];
    packet->data[6] = data[6];
    packet->data[7] = data[7];
}

void SetPacket(Packet* packet, const uint32_t id, const uint8_t const * data)
{
    SetRecentMessageID(packet,id);
    SetRecentMessageData(packet,data);
}

uint8_t IsPacketID(Packet* packet, uint32_t id)
{
    return packet->id == id;
}

uint8_t GetNewMessage(Packet* packet)
{
    packet->id = 0;
    for(uint8_t i = 0; i<8; i++)
    {
        packet->data[i] = 0;
    }
    return RingBuffer_Pop(&RxRingBuffer_Packets, packet);
}

void SendExecuteCode_MRA(void)
{
    Board_CanSend( &BootloaderCAN_ExecuteCode);
}

void SendStartCode_MRA(void)
{
    Board_CanSend( &BootloaderCAN_StartCode);
}

void SendBootCode_MRB(void)
{
    Board_UARTPutSTR( &BootloaderCAN_BootCode);
}

void SendStartCode_MRB(void)
{
    Board_UARTPutSTR( &BootloaderCAN_StartCode);
}

void Network_SendBootCode( void)
{
    // Send to MRA from MRB
    if(isMR_B())
    {
        SendBootCode_MRB();
    }


}

#define NUM_HEADER_BYTES   ((uint32_t)6)
#define FLASH_BASE_ADDRESS ( ((uint32_t)0x9000) + NUM_HEADER_BYTES )

static uint8_t GetCarIndex()
{
   // Get the address offset : (32)    (24)      (16)       (8)     (car index parameter)
   uint32_t addressOffset = ((42*4) + (209*3) + (1160*2) + (121));//48
   // Add the base flash address
   addressOffset += FLASH_BASE_ADDRESS;
   uint8_t carIndex = *((uint8_t *)(addressOffset+0));
   SEGGER_RTT_printf(0, "groupIndex = %x\n\r",carIndex);
   return carIndex;
}


void Network_StartBootloader( void )
{
    Packet temp;

   if(isMR_A())
   {
       SendStartCode_MRA();
   }
   else if (isMR_B())
   {
      temp.id = (GetBoardID()<<8) & 0xF00   |
     		 	(GetCarIndex()<<4) & 0x0F0	|
				 0xF;
      for(uint8_t i = 0; i< 8; i++)
      {
          temp.data[i] = BootloaderCAN_StartCode.data[i];
      }

  	  Board_CanSend( &temp);
   }
   else if (isRiser())
   {

      temp.id = (GetBoardID()<<8) & 0xF00 |
     		 	(GetRiserID()<<4) & 0x0F0 |
			    0xF;
      for(uint8_t i = 0; i<8; i++)
      {
          temp.data[i] = BootloaderCAN_StartCode.data[i];
      }

  	  Board_CanSend( &temp);
   }
}

void Network_StartUserApp( void )
{
    // Send to MRA from MRB
    if(isMR_A())
    {
        SendExecuteCode_MRA();
    }
}

uint8_t MatchingStartCodeID(Packet* packet)
{
    uint8_t result = 0; // assuming false;

    if(packet->id == 0x284407e)
    {
        result = 1;
    }

    return result;
}

uint8_t MatchingStartCodeData(Packet* packet)
{
    uint8_t result = 1; // assuming false;


	if(packet->data[2] != 0x1)
	{
		result = 0;
	}

    return result;
}

uint8_t MatchingBootCodeID(Packet* packet)
{
    uint8_t result = 0; // assuming false;

    if(packet->id == BootloaderCAN_BootCode.id)
    {
        result = 1;
    }

    return result;
}

uint8_t MatchingBootCodeData(Packet* packet)
{
    uint8_t result = 1; // assuming false;

//    for(int8_t i = 0; i < 8; i++)
//    {
//        if(packet->data[i] != BootloaderCAN_BootCode.data[i])
//        {
//            result = 0;
//        }
//    }


    if(isDDM_A())
    {
    	if(((packet->data[5] >> 4) & 0xF) != GetBoardID() )
    	{
    		result = 0;
    	}
    }
    else if(isDDM_B())
    {
    	if((packet->data[5] & 0x0F) != GetBoardID())
    	{
    		result = 0;
    	}
    }
    else if(isRiser() || isShield())
    {
    	if(packet->data[5] != GetBoardID())
    	{
    		result = 0;
    	}
    }

    else if(packet->data[GetBoardID()] != GetBoardID())
    {
        result = 0;
    }
    return result;
}


uint8_t MatchingExecuteCodeID(Packet* packet)
{
    uint8_t result = 0; // assuming false;

    if(packet->id == BootloaderCAN_ExecuteCode.id)
    {
        result = 1;
    }

    return result;
}

uint8_t MatchingExecuteCodeData(Packet* packet)
{
    uint8_t result = 1; // assuming false;

    for(int8_t i = 0; i < 8; i++)
    {
        if(packet->data[i] != BootloaderCAN_ExecuteCode.data[i])
        {
            result = 0;
        }
    }
    return result;
}

uint8_t MatchingBootCodeAwkID(Packet* packet)
{
    uint8_t result = 0; // assuming false;

    if(packet->id == 1)
    {
        result = 1;
    }

    return result;
}

uint8_t MatchingBootCodeAwkData(Packet* packet)
{
    uint8_t result = 1; // assuming false;

    for(int8_t i = 0; i < 8; i++)
    {
        if(packet->data[i] != BootloaderCAN_BootCodeAwk.data[i])
        {
            result = 0;
        }
    }
    return result;
}

void SendBootCodeAwk( void)
{
    if(isMR_A())
    {
        Packet toSend = BootloaderCAN_BootCodeAwk;
        // a message id 2 means the packet is only for MRB.
        toSend.id = 1;
        // Send the awk to MRB.
        RingBuffer_Insert(&TxRingBuffer_Packets, &toSend);

    }
    else if(isMR_B())
    {
        // MRB does not need to awk that he received the bootcode
    }
}

uint8_t Network_BootCodeAwk( Packet* packet )
{
    uint8_t result = 0; // assume false

    if(MatchingBootCodeAwkID(packet) && MatchingBootCodeAwkData(packet))
    {
        result = 1;
    }

    return result;
}

uint8_t Network_BootCodeReceived(Packet* packet)
{
    uint8_t result = 0; // Assume false

    if(MatchingBootCodeID(packet) && MatchingBootCodeData(packet))
    {
        SendBootCodeAwk();
        result = 1;
    }

    return result;
}

uint8_t Network_StartUserAppReceived( Packet* packet)
{
    uint8_t result = 0; // Assume false

    if(MatchingExecuteCodeID(packet) && MatchingExecuteCodeData(packet))
    {
        result = 1;
    }

    return result;
}

uint8_t Network_StartCodeReceived(Packet* packet)
{
    uint8_t result = 0; // Assume false

    if(MatchingStartCodeID(packet) && MatchingStartCodeData(packet))
    {
        result = 1;
    }

    return result;
}

void Network_UpdateMessages( void )
{
    // Process all received messages. Place them in the Rx message buffer
    ProcessCanMessage(GetCanInstance());
    if(!isRiser())
    {
        ProcessUartMessage(GetUartInstance());
    }
}

// MRB sends a specific message over group net after startup to
// let the beagle bone know it could enter bootloader mode.
void Network_SendStartUpCode( void )
{
    Board_SendBootloaderStartup( &BootloaderCAN_BootladerStartup);
}
