/******************************************************************************
 *
 * @file     main.c
 * @brief    Program start point and main loop.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include <string.h>
#include "chip.h"
#include "board.h"
#include "board_api.h"
#include "SRec.h"
#include "Error.h"
#include "deployment.h"
#include "message.h"
#include "debugging.h"
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

#define PACKET_TIMEOUT (500000)

uint8_t gbStartTimer;

volatile uint32_t bootLoader_Timeout = 0;
uint8_t inBootLoaderMode = 0;
//0x000007FF is the CAN ID for config. It will be sent to everyone

Packet BootloaderCAN_BootladerStartup    =
{
        .id   = 0x000007FF,
        .data = {0xFE, 0xDC, 0xBA, 0x98, 0x76, 0x54, 0x32, 0x10}
};

Packet BootloaderCAN_BootCode    =
{
        .id   = 0x000007FF,
        .data = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xEB, 0xDA, 0xED}
};


Packet BootloaderCAN_StartCode    =
{
        .id   = 0x4284407e,
        .data = {0x00, 0x23, 0x1, 0x67, 0x89, 0xAB, 0xCD, 0xEF}
};
Packet BootloaderCAN_BootCodeAwk =
{
        .id   = 0x000007FF,
        .data = {0xDA, 0xED, 0xFE, 0xEB, 0xBE, 0xEF, 0xDE, 0xAD}
};

Packet BootloaderCAN_ExecuteCode =
{
        .id   = 0x000007FF,
        .data ={0xDE, 0xAF, 0xFE, 0xED, 0xDE, 0xEF, 0xFA, 0xED}
};

Packet BootloaderFlashReady_Ack =
{
		.id = 0x500,
		.data ={0x00, 0x46, 0x4c, 0x41, 0x53, 0x48, 0x4d, 0x45}
};



/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

#define RESET_VEC_SRC (0x10000)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint16_t uwFlashAddress;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

typedef struct
{
    uint8_t iapBuffer[256]; // Data that will be placed in flash
    uint16_t index; // Indexes the above array
    uint32_t address; // Starting address for this buffer
} FlashBuffer;

uint32_t userAddress = 0x00000000;


void ClearFlashBuffer( FlashBuffer* passed )
{
    for ( uint8_t i = 0; i < 255; i++ )
    {
        passed->iapBuffer[i] = 0xFF; // flash when reset is set to 0xff. This saves life of flash
        passed->index = 0;
        passed->address = 0;
    }
}

static Error EraseFlash( void )
{
    Error result = noErr; // Assume no errors
    PRINT_INFO("Erasing FLash\n");
    __disable_irq();
    uint8_t problem = Chip_IAP_PreSectorForReadWrite( 16, 29 );
    if ( problem == IAP_CMD_SUCCESS )
    {
        if ( Chip_IAP_EraseSector( 16, 29 ) != IAP_CMD_SUCCESS )
        {
            result = FlashEraseError;
        }
        else
        {
            result = noErr;
        }
    }
    else
    {
        result = FlashEraseError;
    }

    __enable_irq();
    return result;

}




typedef enum
{
    State_Reset,
    State_StartBootloader,
    State_BootLoaderRunning,
    State_BootLoaderEraseFlash,
    State_ProgramFlash,
    State_StartUserApplication,
} Bootloader_FSM;

// Check if the timer has expired.
uint8_t TimerExpiredBootloader( void )
{
    return ( bootLoader_Timeout >= BOOT_LOADER_TIMEOUT_VALUE ) ? 1 : 0;
}

uint8_t ValidUserApplication( void )
{
    return 1;
}

uint8_t FlashWrite( FlashBuffer iap)
{
    if(iap.address % 256)
    {
        PRINT_ERROR("Address not multiple of 256: %d\n",iap.address);
    }

    if ( ! ( ( iap.address >= 0x00010000UL ) && ( iap.address <= 0x0007FFFFUL ) ) )
    {
        __enable_irq();
        PRINT_ERROR("Address out of bounds\n");
        return ( SRecRangeError );
    }

    //address is OK, program the record to flash

	uint8_t k = Chip_IAP_PreSectorForReadWrite( 16, 29 );
	if ( k != IAP_CMD_SUCCESS )
	{
		__enable_irq();
		PRINT_ERROR("Not able to set R/W bit\n");
		return ( 0x10 | k );

	}


    /*  Copy data (already) located in RAM to flash */
    uint8_t result = Chip_IAP_CopyRamToFlash( iap.address, ( uint32_t * ) iap.iapBuffer, 256 );
    if ( result != IAP_CMD_SUCCESS )
    {
        __enable_irq();
        PRINT_ERROR("Not able to transfer to flash\n");
        return ( 0x20 | result );

    }

    /*  Verify the flash contents with the contents in RAM */
    result = Chip_IAP_Compare( iap.address, ( uint32_t ) &iap.iapBuffer, 256 );
    if ( result != IAP_CMD_SUCCESS )
    {
        __enable_irq();
        PRINT_ERROR("Buffer not stored in flash correctly\n");
        return ( 0x30 | result );

    }

    return 0;
}
uint8_t TransferToFlash( SRecDataRec* passedSRecord )
{
    // Double buffer the input. This way more data can be received
    // as one set is being programmed
    static FlashBuffer iap;
    PRINT_STATUS("Transfering to flash\n");
    __disable_irq();
    if ( iap.address == 0 )
    {
        iap.address = ( passedSRecord->LoadAddr );
    }

    PRINT_STATUS("Number Bytes: %d\n",passedSRecord->NumBytes);
    for ( uint32_t i = 0; i < ( passedSRecord->NumBytes ); i++ )
    {
        iap.iapBuffer[iap.index] = ( passedSRecord->Data[i] );
        iap.index = (iap.index >= 256) ? 256 : iap.index+1;
    }

    if ( iap.index == 256 )
    {
        //is the address within a physical flash?
    	uint8_t ucErrorCounter = 0;
    	uint8_t ucErrorCode = 0;
    	while( ucErrorCounter < 10 )
    	{
    		ucErrorCode = FlashWrite(iap);
    		if(ucErrorCode)
    		{
    			ucErrorCounter++;
    		}
    		else
    		{
    			break;
    		}
    	}

    	if(ucErrorCode)
    	{
    		EraseFlash();
    		ClearFlashBuffer(&iap);
    		return ucErrorCode;
    	}

        //TODO: What about the last few blocks? They wont be programmed right now
        ClearFlashBuffer( &iap );

        //feedback to serial port for each correctly programmed S-Record
    }
    else if(iap.index >= 256)
    {
        PRINT_ERROR("Error: iap buffer overflow\n");
        return 1;
    }
    __enable_irq();
    return noErr;

}



uint8_t ProcessSRecord( uint8_t byteMSB, uint8_t byteLSB )
{
    static SRecDataRec srecord = {0};
    // Double buffering this. That way we can read in a second
    // set of data as one is being programmed
    SRecord_Status srecordStatus = RcvSRecord( &srecord, byteMSB, byteLSB );

    if ( srecordStatus == SRecord_Complete )
    {
        // transfer is complete
        if ( srecord.RecType == 7 )
        {
            return 1;
        }
        else
        {
            uint8_t error = TransferToFlash( &srecord );
            if ( error != noErr )
            {
                uint8_t problem;
                problem = 0;
            }

            // Clear the srecord
            srecord.LoadAddr = 0;
            srecord.NumBytes = 0;
            srecord.RecType  = 0;
            for ( uint8_t i; i < 64; i++ )
            {
                srecord.Data[i] = 0;
            }


        }
    }
    else if ( srecordStatus != SRecord_Error_None )
    {
        uint8_t problem;
        problem = 1;
    }

    return 0;
}
uint8_t PacketToSrec(Packet* packet)
{
    for(uint8_t i = 0; i<8; i+=2)
    {
        // If proces srec returns a 1 that means the system has rexceived an s7
        // record soo it is done flashing itself.
        if(ProcessSRecord( packet->data[i], packet->data[i+1] ))
        {
            return 1;
        }
    }
    return 0;
}

void StartUserApplication( void )
{
    uint32_t startup;

    for(int i=0;i<500000;i++);

    __disable_irq();

    Board_UART_DeInit();
    Board_TIMER_DeInit();
    Board_CAN_DeInit();
    SCB->VTOR = ( USER_FLASH_START & 0x1FFFFF80 );
    //__set_MSP(USER_FLASH_START);
    startup = ( ( uint32_t* ) USER_FLASH_START )[1];
    ( ( void (*)( void ) ) startup )(); /* Jump to application startup code */
}


uint8_t Network_DoneSendingMessages()
{
    return 1;
}

uint8_t BootloaderFinished()
{
    return 1;
}

uint8_t BootloaderRunning( Packet* packet)
{
    return PacketToSrec(packet);
}

uint8_t GetRiserID()
{
		uint8_t AddressRiser;
		AddressRiser = !Chip_GPIO_GetPinState( LPC_GPIO, 0, 6  ) <<0 | // DIP 1
							!Chip_GPIO_GetPinState( LPC_GPIO, 0, 7  ) <<1 ; // DIP 2
		return AddressRiser;
}

uint8_t GetBoardID( void )
{
    uint8_t result;

    if(isMR_A())
    {
        result = 0x01;
    }
    else if (isMR_B())
    {
        result = 0x02;
    }
    else if(isCT_A())
    {
        result = 0x03;
    }
    else if(isCT_B())
    {
        result = 0x04;
    }
    else if(isCOP_A())
    {
        result = 0x06;
    }
    else if(isCOP_B())
    {
        result = 0x07;
    }
    else if(isDDM_A())
    {
    	result = 0x8;
    }
    else if(isDDM_B())
    {
    	result = 0x9;
    }
    else if(isRiser())
    {
    	result = 0x5;
    }
    else if(isShield())
    {
    	result = 0xB;
    }

    return result;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
int main( void )
{
    Packet currentMessage;
    Packet lastMessageSent;

    lastMessageSent.data[5] = (uiMemoryAddr >> 16) & 0xFF ;
    lastMessageSent.data[6] = (uiMemoryAddr >> 8) & 0xFF ;
    lastMessageSent.data[7] = (uiMemoryAddr & 0xFF) ;

    Bootloader_FSM bootloader_CurrentState = State_Reset;
    Bootloader_FSM bootloader_NextState    = State_Reset;

    SystemCoreClockUpdate();
    System_Init();
    Board_Init();
    Network_Init();

    //Network_SendStartUpCode();

    static uint8_t startLine[7] = "STARTLN";
    static uint8_t endLine[5]   = "ENDLN";

    static uint8_t counter = 0;
    uint32_t 	Seconds;
    static uint8_t recordCounter= 0;
    static uint8_t startCodeSent = 0;
    Packet packetBuffer[40] = {0};

    while ( 1 )
    {
        // If there is a new message then pass the message to the bootloader
        // state machine for processing. Otherwise do nothing.
        Network_UpdateMessages();


        switch ( bootloader_CurrentState )
        {
            case State_Reset:
                if(GetDip())
                {
                    bootloader_NextState = State_StartBootloader;
                }
                else if( (isMR_A() || isMR_B() || isRiser()) && !GetDip())
                {
                    Network_StartUserApp();
                    bootloader_NextState = State_StartUserApplication;
                }
                else if(GetNewMessage(&currentMessage))
                {
                   if(Network_StartUserAppReceived(&currentMessage))
                   {
                       bootloader_NextState = State_StartUserApplication;
                   }
                   else if(Network_StartCodeReceived(&currentMessage))
                   {
                       bootloader_NextState = State_StartBootloader;
                   }
                }
                else if(TimerExpiredBootloader() )
                {
                     bootloader_NextState = State_StartUserApplication;
                }
                break;
            case State_StartBootloader:
                // Boot cade was received. Then procede to start the bootloader.
                if(GetNewMessage(&currentMessage))
                {
                    if(Network_BootCodeReceived(&currentMessage))
                    {
                        inBootLoaderMode = 1;
                        bootloader_NextState = State_BootLoaderEraseFlash;
                    }
                }
                else
                {
               	 	if(isMR_A())
               	 	{
               	 		if(startCodeSent++ < 30)
               	 		  {
               	 		   	Network_StartBootloader();
              	 		     }
               	 	}
               	 	else if((bootLoader_Timeout - Seconds)> 1 )//MRB or riser
                		{
                			Network_StartBootloader();
                			Seconds = bootLoader_Timeout;
                		}
                }

                break;
            case State_BootLoaderEraseFlash:
                PRINT_INFO("Erasing Flash\n");

                EraseFlash();

                bootloader_NextState = State_ProgramFlash;
                break;
            case State_ProgramFlash:
                // Start processing received messages that should be placed into flash.
                if(GetNewMessage(&currentMessage))
                {
                	if( (currentMessage.id & 0xFFF) == ((GetBoardID() << 8) | 0xFF) ) //resend request
                	{
                		SEGGER_RTT_printf(0, "Resend RC\n");
                        if(isMR_B() || isCT_A() || isCOP_A() || isDDM_A() || isRiser() || isShield())
                        {
                            Board_SendMessageAwk( &lastMessageSent );
                        }
                        else // MRA or SRUB
                        {
                            Board_UARTPutSTR(&lastMessageSent);
                        }
                	}
                	else if((currentMessage.id &0xFFF) == ((GetBoardID() << 8) | 0xAA) )
                	{
                		BootloaderFlashReady_Ack.data[0] = GetBoardID();
                		if(isMR_B() || isCT_A() || isCOP_A() || isDDM_A() || isRiser() || isShield())
						{
							Board_SendMessageAwk( &BootloaderFlashReady_Ack );
						}
						else // MRA or SRUB
						{
							Board_UARTPutSTR(&BootloaderFlashReady_Ack);
						}

                	}
                	else if( (currentMessage.id & 0xFFF) == GetBoardID()<<8)
                    {
                        if(!memcmp(currentMessage.data,startLine,7))
                        {
                            counter = 0;
                        }
                        else if(!memcmp(currentMessage.data,endLine,5))
                        {
                            SRecDataRec record;
                            if(ConvertPacketBufferToSRecord(&record, packetBuffer))
                            {

                                int problem;
                                problem = 100;
                                PRINT_ERROR("Bad Srec Received\n");
                                Packet messageAwk;
                                messageAwk.id = 0x500;
                                messageAwk.data[0] = GetBoardID();
                                messageAwk.data[1] = 'B' ;
                                messageAwk.data[2] = 'A' ;
                                messageAwk.data[3] = 'D' ;
                                messageAwk.data[4] = 'L' ;
                                messageAwk.data[5] = (uiMemoryAddr >> 16) & 0xFF ;
                                messageAwk.data[6] = (uiMemoryAddr >> 8) & 0xFF ;
                                messageAwk.data[7] = (uiMemoryAddr & 0xFF);
                                if(isMR_B() || isCT_A() || isCOP_A() || isDDM_A() || isRiser() || isShield())
                                {
                                    Board_SendMessageAwk( &messageAwk );
                                }
                                else // MRA or SRUB
                                {
                                    Board_UARTPutSTR(&messageAwk);
                                }

                                memcpy(&lastMessageSent, &messageAwk, sizeof(Packet));
                            }
                            else
                            {
                                Packet messageAwk;
                                messageAwk.id = 0x500;

                                uint8_t error = TransferToFlash( &record );

                                if ( error != noErr )
                                {
                                    messageAwk.data[0] = GetBoardID();
                                    messageAwk.data[1] = 'E' ;
                                    messageAwk.data[2] = 'R' ;
                                    messageAwk.data[3] = 'R' ;
                                    messageAwk.data[4] = error ;
                                    messageAwk.data[5] = (uiMemoryAddr >> 16) & 0xFF ;
									messageAwk.data[6] = (uiMemoryAddr >> 8) & 0xFF ;
									messageAwk.data[7] = (uiMemoryAddr & 0xFF) ;
                                }
                                else
                                {
                                	uiMemoryAddr += BYTES_PER_LINE;
                                    messageAwk.data[0] = GetBoardID();
                                    messageAwk.data[1] = 'G' ;
                                    messageAwk.data[2] = 'O' ;
                                    messageAwk.data[3] = 'O' ;
                                    messageAwk.data[4] = 'D' ;
                                    messageAwk.data[5] = (uiMemoryAddr >> 16) & 0xFF ;
                                    messageAwk.data[6] = (uiMemoryAddr >> 8) & 0xFF ;
                                    messageAwk.data[7] = (uiMemoryAddr & 0xFF) ;
                                    recordCounter++;
                                }

                                if(isMR_B() || isCT_A() || isCOP_A() || isDDM_A() || isRiser() || isShield())
                                {
                                    Board_SendMessageAwk( &messageAwk );
                                }
                                else // MRA or SRUB
                                {
                                    Board_UARTPutSTR(&messageAwk);
                                }

                                memcpy(&lastMessageSent, &messageAwk, sizeof(Packet));

                            }
                        }
                    }
                    // Data message for this node
                    else if( currentMessage.id & (GetBoardID() | 1<<6 ))
                    {
                        if(counter <40 )
                        {
                            packetBuffer[counter] = currentMessage;
                            counter++;
                        }
                    }
                    else
                    {
                        int problem;
                        problem = 100;
                    }

                    //uint8_t tmp = BootloaderRunning(&currentMessage);
                    bootloader_NextState = State_ProgramFlash;
                    // Finished receiving update
                    if(Network_StartCodeReceived(&currentMessage) && isShield())
				   {
                    	StartUserApplication();
				   }
                }

                break;
            case State_StartUserApplication:
                // Make sure that any data in a Tx buffer is finished sending
                // before starting the user application
                if(Network_DoneSendingMessages())
                {
                    // Should never return from this function
                    StartUserApplication();
                }
                break;
            default:
                bootloader_NextState = State_Reset;
                break;
        }
        bootloader_CurrentState = bootloader_NextState;
    }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
