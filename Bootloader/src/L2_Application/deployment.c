#include "deployment.h"
#include "board_api.h"

static en_deviceID mcu_id = MCU_UNKNOWN;
static en_sru_deployments deployment = enSRU_DEPLOYMENT__UNKNOWN;

en_deviceID GetMCU_ID( void )
{
    return mcu_id;
}

void SetMCU_ID( void )
{
    // MCU selection. If 1 then MCU B, otherwise MCU A
    if(Chip_GPIO_GetPinState( LPC_GPIO, 0, 17 ))
    {
        mcu_id = MCU_B;
    }
    else
    {
        mcu_id = MCU_A;
    }
}

void SetDeployment( void )
{
    // MR or SRU bit. If true then deployment is an MR
    if(!Chip_GPIO_GetPinState( LPC_GPIO, 0, 10 ) && !Chip_GPIO_GetPinState( LPC_GPIO, 0, 11 ))
    {
        deployment = enSRU_DEPLOYMENT__MR;
    }
    else if((((Chip_GPIO_GetPinState( LPC_GPIO, 0, 10 ) << 3) |
    		(Chip_GPIO_GetPinState( LPC_GPIO, 0, 11 ) << 2) |
			(Chip_GPIO_GetPinState( LPC_GPIO, 0, 17 ) << 1) |
			(Chip_GPIO_GetPinState( LPC_GPIO, 0, 18 ))) == 0x07) &&
			(Chip_GPIO_GetPinState( LPC_GPIO, 2, 8)))
    {
    	deployment = enSRU_DEPLOYMENT__SHIELD;
    }
    // Deployment is an SR. Now determine if CT, COP, DDM.
    else if(!Chip_GPIO_GetPinState( LPC_GPIO, 0, 10 ) && Chip_GPIO_GetPinState( LPC_GPIO, 0, 11 ))
    {

        if(!Chip_GPIO_GetPinState( LPC_GPIO, 4, 11 ) && !Chip_GPIO_GetPinState( LPC_GPIO, 2, 8 ) &&
		   !Chip_GPIO_GetPinState( LPC_GPIO, 4, 10 ) && !Chip_GPIO_GetPinState( LPC_GPIO, 2, 7 ) &&
		   mcu_id == MCU_A)
        {
        	deployment = enSRU_DEPLOYMENT__DDM;
        }
        else if(!Chip_GPIO_GetPinState( LPC_GPIO, 1, 27 ) && !Chip_GPIO_GetPinState( LPC_GPIO, 1, 25 ) &&
        		!Chip_GPIO_GetPinState( LPC_GPIO, 1, 26 ) && !Chip_GPIO_GetPinState( LPC_GPIO, 4, 2 ) &&
        		mcu_id == MCU_B )
        {
        	deployment = enSRU_DEPLOYMENT__DDM;
        }
        // Checks CT/COP select switch. If 1 then CT, otherwise COP.`
        else if(Chip_GPIO_GetPinState( LPC_GPIO, 0, 18 ))
        {
        	deployment = enSRU_DEPLOYMENT__CT;
        }
        else
        {
            deployment = enSRU_DEPLOYMENT__COP;
        }
    }
    else
    {
    	deployment = enSRU_DEPLOYMENT__RISER;
    }
}

en_sru_deployments GetDeployment( void )
{
    return deployment;
}
uint8_t isMR_A( void )
{
    return ( (GetMCU_ID() == MCU_A) && (GetDeployment() == enSRU_DEPLOYMENT__MR) );
}
uint8_t isMR_B( void )
{
    return ( (GetMCU_ID() == MCU_B) && (GetDeployment() == enSRU_DEPLOYMENT__MR) );
}

uint8_t isCT_A( void )
{
    return  ((GetMCU_ID() == MCU_A) && (GetDeployment() == enSRU_DEPLOYMENT__CT) );
}
uint8_t isCT_B( void )
{
    return ( (GetMCU_ID() == MCU_B) && (GetDeployment() == enSRU_DEPLOYMENT__CT) );
}

uint8_t isCOP_A( void )
{
    return ( (GetMCU_ID() == MCU_A) && (GetDeployment() == enSRU_DEPLOYMENT__COP) );
}
uint8_t isCOP_B( void )
{
    return ( (GetMCU_ID() == MCU_B) && (GetDeployment() == enSRU_DEPLOYMENT__COP) );
}

uint8_t isDDM_A( void )
{
    return ( (GetMCU_ID() == MCU_A) && (GetDeployment() == enSRU_DEPLOYMENT__DDM) );
}
uint8_t isDDM_B( void )
{
    return ( (GetMCU_ID() == MCU_B) && (GetDeployment() == enSRU_DEPLOYMENT__DDM) );
}
uint8_t isRiser( void )
{
    return (GetDeployment() == enSRU_DEPLOYMENT__RISER);
}
uint8_t isShield( void )
{
	return (GetDeployment() == enSRU_DEPLOYMENT__SHIELD);
}
