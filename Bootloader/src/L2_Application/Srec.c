#include "Srec.h"
#include "board_api.h"
#include "config.h"

#include <ctype.h>

#include "debugging.h"

uint32_t uiMemoryAddr = START_APP_ADDR;

// This function converts the input to a hex number.
static uint8_t ToHexNibble( const uint8_t byte )
{
    uint8_t result = 0;

    // Checks if the ASCII value given is a hex character
    if ( !isxdigit( byte ) )
    {
        // Error
    }
    else
    {
        result = isdigit(byte) ? ( byte - '0' ) : ( byte - 'A' + 10 );
    }

    return result;
}

static uint8_t ToHexByte( uint8_t nibbleHi, uint8_t nibbleLow)
{
   return  (ToHexNibble( nibbleHi )<<4)  | (ToHexNibble( nibbleLow )<<0);
}


// Check to see if the packet contains the start of a new SRecord
// Start of a record is signified by an ASCII 'S'
// Input : Packet
// Output: 1: Packet is start of new SRecord
//         0: Packet is not start of new SRecord
//TODO Make the return be an enum either TRUE or FALES
uint8_t StartOfRecord( Packet* passed )
{
    uint8_t result = 0;

    // data bytes represent ASCII values
    if( passed->data[0] == 'S' )
    {
        result = 1;
    }

    return result;
}


// Input : Array of packets
// Output: 0  -> No problem encountered
//         !0 -> There was an issue creating the SRecord
// TODO: error output enum
uint8_t ConvertPacketBufferToSRecord(SRecDataRec* result, Packet * packets)
{
    uint8_t resultStatus = 0;

    // All S3 record are set up to have a max size of 270 bytes. First copy all of the raw data
    // from the packets into a buffer
    uint8_t buffer[270] = {0};

    // Bytes 0 and 1 of the first packet in the buffer if everything is working
    // should be SX where X is the record type.
    if(StartOfRecord(&packets[0]))
    {
        //Reconstruct the srecord form the individual bytes
        // This is really takling this 2D matrix that also has some extra data, and converts it
        // to a 1D matrix
        for(uint8_t i = 0; i < 17; i++)
        {
            for(uint8_t j = 0; j<8; j++)
            {
                buffer[i*8+j] = packets[i].data[j];
            }
        }

        // Now extract the data from the 1D buffer and build the correct SRecord.
        // Each value extracted from the packet will need to be converted from ASCII to hex.
        // Two ASCII values form 1 hex value

        uint32_t calculatedChecksum = 0;

        // Get the recod type
        result->RecType = ToHexByte('0',buffer[1]);

        // Get the number of bytes in the SRecord.
        uint8_t numberOfBytes = buffer[2];
        result->NumBytes = numberOfBytes;
        calculatedChecksum += result->NumBytes;

        // Extract the Address. For now all records are S3 records so extract 4 bytes

        uint8_t byte3 = buffer[3];
        uint8_t byte2 = buffer[4];
        uint8_t byte1 = buffer[5];
        uint8_t byte0 = buffer[6];

        uint32_t address = (byte3<<24) | (byte2<<16) | (byte1<<8) | (byte0<<0);
        result->LoadAddr = address;

        if(result->LoadAddr % 128)
		{
			PRINT_ERROR("Address not multiple of 256: %d\n",result->LoadAddr);
			resultStatus = 1;
		}

		if ( ! ( ( result->LoadAddr >= 0x00010000UL ) && ( result->LoadAddr <= 0x0007FFFFUL ) ) )
		{
			__enable_irq();
			PRINT_ERROR("Address out of bounds\n");
			resultStatus = 1;
		}

        if(result->LoadAddr != uiMemoryAddr)
		{
			PRINT_ERROR("Address not expected: %d\n",result->LoadAddr);
			resultStatus = 1;
		}

        calculatedChecksum += byte3;
        calculatedChecksum += byte2;
        calculatedChecksum += byte1;
        calculatedChecksum += byte0;

        // Transfer the data from the buffer to the srecord

        for(uint8_t i = 0; i < MaxSRecDataLen; i++)
        {
            result->Data[i] = buffer[7+i];
            calculatedChecksum += result->Data[i];
        }

        result->NumBytes = numberOfBytes-4-1;

        uint8_t checksum = buffer[135];

        calculatedChecksum = ~(calculatedChecksum) & 0xFF;
        if( !(calculatedChecksum ^ checksum))
        {
            result->checksum = checksum;
            // Give the number of data bytes. Subtract 4 bytes for the address
            // and the one byte for the checksum
            result->NumBytes = numberOfBytes-4-1;
        }
        else
        {
            PRINT_ERROR("ERROR: Buffer Srecord checksum mismatch\n");
            resultStatus = 2; // Buffer SRecord checksum does not match calculated checksum
        }
    }
    else
    {
        PRINT_ERROR("ERROR: Buffer does not contaon start of a record\n");
        resultStatus = 1; // Buffer does not contain the start of a SRecord
    }

    return resultStatus;

}

/******************************************************************************/
