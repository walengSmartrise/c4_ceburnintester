#include "led.h"
#include "chip.h"
#include "deployment.h"

#include <stdint.h>
#include <stdbool.h>


typedef struct{
    uint8_t ledPort;
    uint8_t ledPin;
} LED;


static const LED leds[] = {
    #define LED(name, port, pin, iocon) { port, pin},
    LED_TABLE
    #undef LED

};
static const LED leds2[] = {
    #define LED(name, port, pin, iocon) { port, pin},
    LED_TABLE2
    #undef LED2
};
static const LED leds3[] = {
    #define LED(name, port, pin, iocon) { port, pin},
    LED_TABLE3
    #undef LED2
};

/* Sets the state of a board LED to on or off */
void Board_LED_Set(LED_Names LEDNumber, bool On)
{
	if(GetDeployment() == enSRU_DEPLOYMENT__RISER)
	{
		if (LEDNumber < Num_LEDS) {
			Chip_GPIO_SetPinState(LPC_GPIO, leds2[LEDNumber].ledPort, leds2[LEDNumber].ledPin, !On);
		}
	}
	else if(GetDeployment() == enSRU_DEPLOYMENT__SHIELD)
	{
		for (uint8_t i = 0; i < 5; i++)
		{
			Chip_GPIO_SetPinState(LPC_GPIO, leds3[LEDNumber].ledPort, leds3[LEDNumber].ledPin, !On);
		}
	}
	else
	{
	    if (LEDNumber < Num_LEDS) {
	        /* Set state, low is on, high is off */
	        Chip_GPIO_SetPinState(LPC_GPIO, leds[LEDNumber].ledPort, leds[LEDNumber].ledPin, !On);
	    }
	}
}

/* Toggles the current state of a board LED */
void Board_LED_Toggle(uint8_t LEDNumber)
{
	if(GetDeployment() == enSRU_DEPLOYMENT__RISER)
	{
		if (LEDNumber < Num_LEDS) {
			Chip_GPIO_SetPinToggle(LPC_GPIO, leds2[LEDNumber].ledPort, leds2[LEDNumber].ledPin);
		}
	}
	else if(GetDeployment() == enSRU_DEPLOYMENT__SHIELD)
	{
		for (uint8_t i = 0; i < 5; i++)
		{
			Chip_GPIO_SetPinToggle(LPC_GPIO, leds3[LEDNumber].ledPort, leds3[LEDNumber].ledPin);
		}
	}
	else
	{
	    if (LEDNumber < Num_LEDS) {
	        Chip_GPIO_SetPinToggle(LPC_GPIO, leds[LEDNumber].ledPort, leds[LEDNumber].ledPin);
	    }
	}

}

void Board_LED_Init( void )
{
    /* Setup port direction and initial output state */
	if(GetDeployment() == enSRU_DEPLOYMENT__RISER)
	{
	    for (uint8_t i = 0; i < Num_LEDS; i++)
	    {
	        Chip_GPIO_SetPinDIROutput(LPC_GPIO, leds2[i].ledPort, leds2[i].ledPin);
	    }
	}
	else if(GetDeployment() == enSRU_DEPLOYMENT__SHIELD)
	{
		for (uint8_t i = 0; i < 5; i++)
		{
			Chip_GPIO_SetPinDIROutput(LPC_GPIO, leds3[i].ledPort, leds3[i].ledPin);
		}
	}
	else
	{
	    for (uint8_t i = 0; i < Num_LEDS; i++)
	    {
	        Chip_GPIO_SetPinDIROutput(LPC_GPIO, leds[i].ledPort, leds[i].ledPin);
	    }
	}

}
