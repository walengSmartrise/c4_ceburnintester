@echo off

@REM This file will update the system parameters located in Lib_System\inc\Parameters

@del defaults.txt /q
@del *.h          /q

@START Parameter_Generator.exe

@echo Generating Header Files

:waitforfiles
@if not exist param*bit.h (
	@timeout 1 > nul
	goto waitforfiles
)

@copy .\param1Bit.h  ..\..\Lib_System\inc\Parameters\
@copy .\param8Bit.h  ..\..\Lib_System\inc\Parameters\
@copy .\param16Bit.h ..\..\Lib_System\inc\Parameters\
@copy .\param24Bit.h ..\..\Lib_System\inc\Parameters\
@copy .\param32Bit.h ..\..\Lib_System\inc\Parameters\

@if exist c4param.log (
	@echo.
	@echo C4 Parameters Summary
	@type c4param.log
	@echo.
)

@pause