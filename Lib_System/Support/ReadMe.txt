To update system parameters, make changes to the SystemParameters.xlsx page, 
then run "Update_Parameters.bat" to automatically update the files
param1Bit.h, param8Bit.h, param16Bit.h, param24Bit.h and param32Bit.h 
used in the codebase.

To rebuild the executable

* Latest version built with Python 3.8.2

* Install all packages listed in requirements.txt
  pip install -r requirements.txt

* To build the executable

  pyinstaller --windowed --onefile --clean --name="Parameter_Generator" Parameter_Generator.py

* New executable at [./dist/] folder

