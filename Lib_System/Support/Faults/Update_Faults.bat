@echo off

@REM This file will update the system faults located in Lib_System\inc\

@del faults.h /q

@START C4_Fault_Generator.exe

@echo Generating Faults.h

:waitforfile
@if not exist faults.h (
	@timeout 1 > nul
	goto waitforfile
)

copy .\faults.h ..\..\..\Lib_System\inc\

@pause