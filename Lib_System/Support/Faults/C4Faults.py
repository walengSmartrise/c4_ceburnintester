
import enum

# C4Sheets is part of C4 Controller project, add path to PYTHONPATH
# Folder: <GIT>\<c4_system>\Lib_System\Support
# Current directory structure is just one level up
from C4Sheets import C4Sheets

# Enum definition to parse and access data in memory
class enumC4faultfld( enum.Enum ):
    number       = 0
    name         = enum.auto()
    oos          = enum.auto()
    construction = enum.auto()
    clear_type   = enum.auto()
    clear_ccs    = enum.auto()
    priority     = enum.auto()
    string       = enum.auto()
    definition   = enum.auto()
    solution     = enum.auto()
    total_enums  = enum.auto()           # this entry must be *last*

class C4Faults( C4Sheets ):

    # Constructor 
    def __init__(self): 
        C4Sheets.__init__( self )

    def f_loadSheet( self, p_list_all_sheets, p_workbook, p_sheetname, p_preload_columns = None ):

        # Translation table between 'Column Headers' and 'Enum(s)'
        t_InputSheetHeaders = { 
            str.lower( 'Number' )       : enumC4faultfld.number,
            str.lower( 'Tag' )          : enumC4faultfld.name,
            str.lower( 'OOS' )          : enumC4faultfld.oos,
            str.lower( 'Construction' ) : enumC4faultfld.construction,
            str.lower( 'Clear Type' )   : enumC4faultfld.clear_type,
            str.lower( 'Clear CCs' )    : enumC4faultfld.clear_ccs,
            str.lower( 'Priority' )     : enumC4faultfld.priority,
            str.lower( 'String' )       : enumC4faultfld.string,
            str.lower( 'Definition' )   : enumC4faultfld.definition,
            str.lower( 'Solution' )     : enumC4faultfld.solution,
        }

        b_SheetLoaded = super().f_loadSheet( p_list_all_sheets, p_workbook, 
                                             p_sheetname, t_InputSheetHeaders, p_preload_columns )

        return b_SheetLoaded

