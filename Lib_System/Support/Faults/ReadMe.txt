To update system faults, make changes to the SystemFaults.xlsx page, 
then run "Update_Faults.bat" to automatically update the faults.h used in the codebase.

To rebuild the executable

* Latest version built with Python 3.8.2

* Install all packages listed in requirements.txt
  pip install -r requirements.txt

* Update set_python_path.bat to the folder containing C4Param.py

* To build the executable

  pyinstaller --windowed --onefile --clean --name="C4_Fault_Generator" Fault_Generator.py

* New executable at [./dist/] folder

