
from xlrd import open_workbook
import logging
import time
import enum


# C4Sheets is part of C4 Controller project, add path to PYTHONPATH
# Folder: <GIT>\<c4_system>\Lib_System\Support
# Current directory structure is just one level up
from C4Faults import C4Faults, enumC4faultfld


# ---------------------------------------------------------------------------------------------------------

g_SheetCols2Load = [
    enumC4faultfld.name.value,
    enumC4faultfld.oos.value,
    enumC4faultfld.construction.value,
    enumC4faultfld.clear_type.value,
    enumC4faultfld.clear_ccs.value,
    enumC4faultfld.priority.value,
    enumC4faultfld.string.value,
]

# ---------------------------------------------------------------------------------------------------------
# Generate header file 

def f_Generate_Header_File( p_header_filename, p_sheet_colidx, p_rows ):

    idx_name     = enumC4faultfld.name.value
    idx_oos      = enumC4faultfld.oos.value
    idx_const    = enumC4faultfld.construction.value
    idx_clrtype  = enumC4faultfld.clear_type.value
    idx_clrccs   = enumC4faultfld.clear_ccs.value
    idx_priority = enumC4faultfld.priority.value
    idx_string   = enumC4faultfld.string.value

    t_enums_sorted = sorted( [ each_row[idx_name] for each_row in p_rows ], key=len, reverse=True) 
    t_len_longer_enum = 3 + len(t_enums_sorted[0])
    del t_enums_sorted

    t_strings_sorted = sorted( [ each_row[idx_string] for each_row in p_rows ], key=len, reverse=True) 
    t_len_longer_string = 3 + len(t_strings_sorted[0])
    del t_strings_sorted

    fh_faults_header  = open( p_header_filename, 'w' )

    # Header
    fh_faults_header.write('#ifndef FAULTS_H\n')
    fh_faults_header.write('#define FAULTS_H\n\n')

    fh_faults_header.write('/* NOTE: FILE UPDATED VIA LIB_SYSTEM - SUPPORT - FAULTS, \n'
                    ' * Edit SystemFaults.xlsx then save as tab delimited (SystemFaults.txt)  \n'
                    ' * Then run Update_Faults.bat to automatically update this file */\n\n')

    fh_faults_header.write('/* Includes */\n')
    fh_faults_header.write('#include \"sys.h\"\n\n')

    fh_faults_header.write('/* Defines */\n')
    fh_faults_header.write('#define MAX_LATCHED_FAULTS             ( 32 )\n')
    fh_faults_header.write('#define FAULT_LATCH_PERIOD_1MS         ( 3000U )\n')
    fh_faults_header.write('#define BOARD_RESET_FAULT_TIMER_MS     ( 10000 )\n')
    fh_faults_header.write('/* Update rate for fault packets when the processor is not faulted, and when it is faulted */\n')
    fh_faults_header.write('#define FAULT_RESEND_RATE_INACTIVE_1MS ( 2000 )\n')
    fh_faults_header.write('#define FAULT_RESEND_RATE_ACTIVE_1MS   ( 500 )\n\n')

    # Print Enum
    fh_faults_header.write( '// List of system faults\n' )
    fh_faults_header.write( 'typedef enum\n{\n' )
    for idx, xlsRow in enumerate( p_rows ):
        fh_faults_header.write( '   FLT__'
                                + str( xlsRow[idx_name] + ',' ).ljust( t_len_longer_enum )
                                + ' // FaultNum: ' + str(idx)
                                + '\n' )
    fh_faults_header.write( '\n   NUM_FAULTS' + ' // Total: ' + str(idx+1) )
    fh_faults_header.write( '\n} en_faults;\n\n' )

    # Latching Safety Faults
    fh_faults_header.write( '/* Fault that will be stored to FRAM */\n' )
    fh_faults_header.write( 'enum en_latching_safety_faults\n{\n' )
    fh_faults_header.write( '   LSF__GOV,\n' )
    fh_faults_header.write( '   LSF__EB1_DROPPED,\n' )
    fh_faults_header.write( '   LSF__UNINTENDED_MOVEMENT,\n' )
    fh_faults_header.write( '   LSF__TRACTION_LOSS,\n' )
    fh_faults_header.write( '   LSF__OVERSPEED,\n\n' )
    fh_faults_header.write( '   NUM_LATCHING_SAFETY_FAULTS /* Must be less than 32! */' )
    fh_faults_header.write( '};\n\n' )

    # Fault Nodes
    fh_faults_header.write( '// These are all the nodes that maintain a copy of the faults.\n' )
    fh_faults_header.write( 'enum en_fault_nodes\n{\n' )
    fh_faults_header.write( '   enFAULT_NODE__CPLD,\n' )
    fh_faults_header.write( '   enFAULT_NODE__MRA,\n' )
    fh_faults_header.write( '   enFAULT_NODE__MRB,\n' )
    fh_faults_header.write( '   enFAULT_NODE__CTA,\n' )
    fh_faults_header.write( '   enFAULT_NODE__CTB,\n' )
    fh_faults_header.write( '   enFAULT_NODE__COPA,\n' )
    fh_faults_header.write( '   enFAULT_NODE__COPB,\n\n' )
    fh_faults_header.write( '   NUM_FAULT_NODES,\n\n' )
    fh_faults_header.write( '   enFAULT_NODE__UNK,\n' )
    fh_faults_header.write( '};\n\n' )

    # Print Fault Entry Enums
    fh_faults_header.write( '/* FAULT_ENTRY(A, B, C, D, E, F, G) FORMAT:\n' )
    fh_faults_header.write( ' *    A = Unique Fault Number ID\n' )
    fh_faults_header.write( ' *    ------------------\n' )
    fh_faults_header.write( ' *    Fault Function Fields:\n' )
    fh_faults_header.write( ' *    B = en_faultentry_oos\n' )
    fh_faults_header.write( ' *    C = en_faultentry_con\n' )
    fh_faults_header.write( ' *    D = en_faultentry_cleartype\n' )
    fh_faults_header.write( ' *    E = en_faultentry_clearcalls\n' )
    fh_faults_header.write( ' *    F = en_faultentry_priority\n' )
    fh_faults_header.write( ' *    ------------------\n' )
    fh_faults_header.write( ' *    G = String\n' )
    fh_faults_header.write( ' * */\n\n' )

    fh_faults_header.write( '// OOS   If ON, the fault will contribute to the OOS limit.\n' )
    fh_faults_header.write( 'typedef enum\n{\n' )
    fh_faults_header.write( '   FT_OOS_OFF,\n' )
    fh_faults_header.write( '   FT_OOS__ON,\n' )
    fh_faults_header.write( '} en_faultentry_oos;\n' )

    fh_faults_header.write( '// Construction  If ON, the fault will be checked during construction operation.\n' )
    fh_faults_header.write( 'typedef enum\n{\n' )
    fh_faults_header.write( '   FT_CON_OFF,\n' )
    fh_faults_header.write( '   FT_CON__ON,\n' )
    fh_faults_header.write( '} en_faultentry_con;\n' )

    fh_faults_header.write( 'typedef enum\n{\n' )
    fh_faults_header.write( '   FT_CLR_NORM, // Self clears after 3 seconds,\n' )
    fh_faults_header.write( '   FT_CLR_LTCH, // Latches & persists after reset, (NOTE: see enum en_latching_safety_faults and GetLatchedFaultIndex_Plus1)\n' )
    fh_faults_header.write( '   FT_CLR_RSET, // Requires reset to clear\n' )
    fh_faults_header.write( '   FT_CLR_DIP1, // Requires DIP A1 and reset to clear,\n' )
    fh_faults_header.write( '} en_faultentry_cleartype;\n' )

    fh_faults_header.write( 'typedef enum\n{\n' )
    fh_faults_header.write( '   FT_CC_OFF,\n' )
    fh_faults_header.write( '   FT_CC__ON,\n' )
    fh_faults_header.write( '} en_faultentry_clearcalls;\n' )

    fh_faults_header.write( 'typedef enum\n{\n' )
    fh_faults_header.write( '   FT_PRI_000,// latching\n' )
    fh_faults_header.write( '   FT_PRI_001,//\n' )
    fh_faults_header.write( '   FT_PRI_002,// board hardware\n' )
    fh_faults_header.write( '   FT_PRI_003,// board config\n' )
    fh_faults_header.write( '   FT_PRI_004,// system hardware (const)\n' )
    fh_faults_header.write( '   FT_PRI_005,// system hardware (insp)\n' )
    fh_faults_header.write( '   FT_PRI_006,// Run (High)\n' )
    fh_faults_header.write( '   FT_PRI_007,// Run (Low)\n' )
    fh_faults_header.write( '   FT_PRI_008,\n' )
    fh_faults_header.write( '   FT_PRI_009,\n' )
    fh_faults_header.write( '   FT_PRI_010,\n' )
    fh_faults_header.write( '   FT_PRI_011,\n' )
    fh_faults_header.write( '   FT_PRI_012,\n' )
    fh_faults_header.write( '   FT_PRI_013,\n' )
    fh_faults_header.write( '   FT_PRI_014,\n' )
    fh_faults_header.write( '   FT_PRI_015,\n' )
    fh_faults_header.write( '   FT_PRI_016,\n' )
    fh_faults_header.write( '   FT_PRI_017,\n' )
    fh_faults_header.write( '   FT_PRI_018,\n' )
    fh_faults_header.write( '   FT_PRI_019,\n' )
    fh_faults_header.write( '   NUM_FT_PRI = 255\n' )
    fh_faults_header.write( '} en_faultentry_priority; /* Lowest value is highest priority */\n\n' )

    # Print Fault Table
    fh_faults_header.write( '/* Fault Table Definition */\n' )
    fh_faults_header.write( '#define FAULT_TABLE(FT_ENTRY) ' )
    for xlsRow in p_rows:
        fh_faults_header.write( ' \\\n   FT_ENTRY( ' )
        fh_faults_header.write( 'FLT__' + str( xlsRow[idx_name] + ',' ).ljust( t_len_longer_enum ) )
        fh_faults_header.write( 'FT_OOS__ON, ' if 'ON' == xlsRow[idx_oos].upper()    else 'FT_OOS_OFF, ' )
        fh_faults_header.write( 'FT_CON__ON, ' if 'ON' == xlsRow[idx_const].upper()  else 'FT_CON_OFF, ' )
        fh_faults_header.write( 'FT_CLR_' + xlsRow[idx_clrtype].upper() + ', ' )
        fh_faults_header.write( 'FT_CC__ON, '  if 'ON' == xlsRow[idx_clrccs].upper() else 'FT_CC_OFF, ' )
        if 255 == int(xlsRow[idx_priority]):
            fh_faults_header.write( 'NUM_FT_PRI, ' )
        else:
            fh_faults_header.write( 'FT_PRI_' + str(int(xlsRow[idx_priority])).rjust(3, '0') + ', ' )
        fh_faults_header.write( '\"' + str( xlsRow[idx_string] + '\"' ).ljust( t_len_longer_string ) + ' )' )
    fh_faults_header.write( '\n\n' )

    fh_faults_header.write( '/* Macros for accessing table elements */\n' )
    fh_faults_header.write( '#define EXPAND_FAULT_TABLE_AS_ENUMERATION(A, B, C, D, E, F, G) A, \n' )
    fh_faults_header.write( '#define EXPAND_FAULT_TABLE_AS_OOS_ARRAY(A, B, C, D, E, F, G) B, \n' )
    fh_faults_header.write( '#define EXPAND_FAULT_TABLE_AS_CONSTRUCTION_ARRAY(A, B, C, D, E, F, G) C, \n' )
    fh_faults_header.write( '#define EXPAND_FAULT_TABLE_AS_RESET_ARRAY(A, B, C, D, E, F, G) D, \n' )
    fh_faults_header.write( '#define EXPAND_FAULT_TABLE_AS_CC_ARRAY(A, B, C, D, E, F, G) E, \n' )
    fh_faults_header.write( '#define EXPAND_FAULT_TABLE_AS_PRIORITY_ARRAY(A, B, C, D, E, F, G) F, \n' )
    fh_faults_header.write( '#define EXPAND_FAULT_TABLE_AS_STRING_ARRAY(A, B, C, D, E, F, G) G, \n\n' )


    # End File
    fh_faults_header.write( '#endif' )

    logging.info( '  Total {} alarms'.format( len(p_rows) ))


# ---------------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------

g_timestamp_start = time.time()

logging.basicConfig( level=logging.INFO,
                     format='%(relativeCreated)6d:%(levelname)s:%(message)s' )

logging.info( 'Loading input files' )

# Open Input Files
g_workbook = open_workbook( 'SystemFaults.xlsx' )

objC4faults = C4Faults()

g_allSheets = list()
objC4faults.f_loadSheet( g_allSheets, g_workbook, 'SystemFaults', g_SheetCols2Load )

g_workbook.release_resources()
del g_workbook

#debugobj.f_debug_list( g_allSheets, p_depth = 1, p_keep_digging = 1 ) #, p_one_line_depth = 2 )


filename_faults_h = 'faults.h'

logging.info( 'Generating header files ' + filename_faults_h )

# Generate separate header files for each bitsize
for t_each_sheet in g_allSheets:
    f_Generate_Header_File( filename_faults_h, 
                            t_each_sheet[objC4faults.dictkey_colidx], 
                            t_each_sheet[objC4faults.dictkey_allrows] )

# Wrap it up, release and close everything
del g_allSheets

t_elapsed_time = int( time.time() - g_timestamp_start )
logging.info( 'C4 Faults generated' )
print( '\nC4 Faults generated. Total time [{:02d}:{:02d}:{:02d}]'.format( 
        t_elapsed_time // 3600, 
        ( t_elapsed_time % 3600 // 60 ),
        t_elapsed_time % 60 ) )
