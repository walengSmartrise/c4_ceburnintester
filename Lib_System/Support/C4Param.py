
import logging
import enum
from C4Sheets import C4Sheets


# Enum definition to parse and access data in memory
class enumC4paramfld( enum.Enum ):
    address     = 0                     # force first value to 0, enum.auto() will make it 1
    bit_index   = enum.auto()
    param_num   = enum.auto()
    sw_name     = enum.auto()
    string      = enum.auto()
    group       = enum.auto()
    min_value   = enum.auto()
    max_value   = enum.auto()
    def_value   = enum.auto()
    description = enum.auto()
    gui_page    = enum.auto()
    doc_topic   = enum.auto()
    tail_start  = enum.auto()           # Last  column from the spreadsheet
    bit_size    = tail_start            # First column calculated at run time
    byte_size   = enum.auto()
    total_enums = enum.auto()           # WARNING: This entry must be *last*

# Enum defintion to parse elements created by f_allSheets2ListOfValues
class enC4ListValues( enum.Enum ):
    param_addr    = 0                   # force first value to 0, enum.auto() will make it 1
    param_num     = enum.auto()
    byte_size     = enum.auto()
    param_value   = enum.auto()
    sw_name       = enum.auto()
    def_if_config = enum.auto()
    total_enums   = enum.auto()         # this entry must be *last*

# Based on C4Sheets, Definition of C4 Parameters
class C4Param( C4Sheets ):

    # Constructor 
    def __init__(self): 

        C4Sheets.__init__( self )
        # Predefine strings to access dictionary
        self.dictkey_bitsize = 'bitsize'


    def f_loadSheet( self, p_list_all_sheets, p_workbook, p_sheetname, 
                           p_bitsize, p_preload_columns = None ):

        # Translation table between 'Column Headers' and 'Enum(s)'
        t_InputSheetHeaders = { 
            str.lower( 'Default Address')   : enumC4paramfld.address,
            str.lower( 'Default Bit Index') : enumC4paramfld.bit_index,
            str.lower( 'Number')            : enumC4paramfld.param_num,
            str.lower( 'String' )           : enumC4paramfld.string,
            str.lower( 'Default Group' )    : enumC4paramfld.group,
            str.lower( 'Default Value' )    : enumC4paramfld.def_value,
            str.lower( 'Min Value' )        : enumC4paramfld.min_value,
            str.lower( 'Max Value' )        : enumC4paramfld.max_value,
            str.lower( 'Software Name' )    : enumC4paramfld.sw_name,
            str.lower( 'Description' )      : enumC4paramfld.description,
            str.lower( 'GUI Page' )         : enumC4paramfld.gui_page,
            str.lower( 'Document Topic' )   : enumC4paramfld.doc_topic,
        }

        # Warning: 1-bit parameters are stored in a whole byte
        bit_size  = int( p_bitsize ) 
        byte_size = int( ( bit_size if not 1 == bit_size else 8 ) / 8 )

        b_SheetLoaded = super().f_loadSheet( p_list_all_sheets, p_workbook, 
                                             p_sheetname, t_InputSheetHeaders, 
                                             p_preload_columns, p_tail_list = [bit_size, byte_size] )

        if b_SheetLoaded:
            p_list_all_sheets[-1][self.dictkey_bitsize] = str(p_bitsize)

        return b_SheetLoaded

# ---------------------------------------------------------------------------------------------------------
#  Create a master dictionary merging all parameters from all sheets
#  For each spreadsheet, identify the matching column based on the row headers
#  The resulting dictionary is controlled by the caller
#  One use case, use parameter_name as key, for each entry list min/max/default values

    def f_allSheets2dict( self, p_all_sheets, p_columns_to_load, p_key_enum, 
                          p_drop_unused = True ):

        ret_dict = dict()

        for next_sheet in p_all_sheets:

            t_sheet   = next_sheet[self.dictkey_sheethdlr]
            t_idx_map = next_sheet[self.dictkey_colidx]

            # Use idx_map instead of enum, b/c reading directly from spreadsheet
            idx_sw_name = t_idx_map[enumC4paramfld.sw_name.value]

            # Warning: 1-bit parameters are stored in a whole byte
            bit_size  = int( next_sheet[self.dictkey_bitsize] ) 
            byte_size = int( ( bit_size if not 1 == bit_size else 8 ) / 8 )
            t_tail_list = [ bit_size, byte_size ]

            for t_row_idx in range( 1, t_sheet.nrows ):     # skip first row/headers
                xlsRow = t_sheet.row_values( t_row_idx )

                if (p_drop_unused) and (str.lower(xlsRow[idx_sw_name]).startswith( 'unused_' )):
                    continue

                t_list_strings = [None] * enumC4paramfld.tail_start.value
                for t_colidx in p_columns_to_load:
                    if self.HDR_NOT_FOUND != t_idx_map[t_colidx]:
                        t_list_strings[t_colidx] = xlsRow[t_idx_map[t_colidx]]
                t_list_strings.extend( t_tail_list )

                ret_dict[xlsRow[t_idx_map[p_key_enum]]] = ( t_list_strings )

        return ret_dict

# ---------------------------------------------------------------------------------------------------------
#  Create a master list merging all parameters from all sheets
#  For each spreadsheet, identify the matching column based on the row headers
#  All rows from the spreadsheet must be preloaded when calling f_loadSheet
#  This function returns a list with the following fields
#      [ Param_Addr (int), Param_Number (int), Byte_Size (int), Default Value (binary) ]
#          Note: if the default reads 'config' the list will show zero
#  Additionally, caller may add extra elements to the list via p_tail_list
#  After merging all sheets, the master list is sorted 1st by Addr, 2nd by Number

    def f_allSheets2ListOfValues( self, p_master_list, p_byteorder, 
                                  #p_default_if_config = None,
                                  p_tail_list = None ):

        idx_number    = enumC4paramfld.param_num.value
        idx_addr      = enumC4paramfld.address.value
        idx_def_value = enumC4paramfld.def_value.value
        idx_min_value = enumC4paramfld.min_value.value
        idx_sw_name   = enumC4paramfld.sw_name.value

        ret_master_all_rows = list()

        for t_each_sheet in p_master_list:

            # Warning: 1-bit parameters are stored in a whole byte
            bit_size  = int( t_each_sheet[self.dictkey_bitsize] ) 
            byte_size = int( ( bit_size if not 1 == bit_size else 8 ) / 8 )

            for xlsRow in t_each_sheet[self.dictkey_allrows]:
                if 'config' == xlsRow[idx_def_value]:
                    t_value_if_config = int(max(0,xlsRow[idx_min_value])).to_bytes( byte_size, 
                                                                                    byteorder=p_byteorder )
                    t_param_value = None
                else:
                    t_value_if_config = t_param_value = int(xlsRow[idx_def_value]).to_bytes( byte_size, 
                                                                                             byteorder=p_byteorder )

                ret_master_all_rows.append(  [ 
                                               int(xlsRow[idx_addr]), 
                                               int(xlsRow[idx_number]), 
                                               byte_size, 
                                               t_param_value,
                                               xlsRow[idx_sw_name],
                                               t_value_if_config
                                             ] 
                                           + p_tail_list )

        ret_master_all_rows.sort( key = lambda x:(x[0], x[1] ) )

        return ret_master_all_rows
