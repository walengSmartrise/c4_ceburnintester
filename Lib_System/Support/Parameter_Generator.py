from xlrd import open_workbook
import logging
import time
import enum
import os
from C4Param import C4Param, enumC4paramfld


g_SheetCols2Load = [
    enumC4paramfld.sw_name.value,
    enumC4paramfld.address.value,
    enumC4paramfld.bit_index.value,
    enumC4paramfld.string.value,
    enumC4paramfld.group.value,
    enumC4paramfld.def_value.value,
    enumC4paramfld.param_num.value
]

gLogFile = 'c4param.log'

# ---------------------------------------------------------------------------------------------------------
#  

def f_Generate_DefaultsTxt( p_output_file, p_param_bitsize, p_sheet_colidx, p_rows ):

    idx_sw_name   = enumC4paramfld.sw_name.value
    idx_addr      = enumC4paramfld.address.value
    idx_bitidx    = enumC4paramfld.bit_index.value
    idx_def_value = enumC4paramfld.def_value.value

    logging.debug( 'GenerateDefault Sheet:{} Idx:[{},{},{}]'.format(
                  p_param_bitsize, idx_sw_name, idx_addr, idx_bitidx ))

    for xlsRow in p_rows:
        t_param_name      = 'CONFIG_' + str(xlsRow[idx_sw_name])
        t_param_adress    = str(int(xlsRow[idx_addr]))
        t_param_bitidx    = str(int(xlsRow[idx_bitidx]))
        t_param_def_value = str( 0 if 'config' == xlsRow[idx_def_value] else int(xlsRow[idx_def_value]) )
        p_output_file.write(  t_param_name.ljust(50)
                            + p_param_bitsize.ljust(8) 
                            + t_param_adress.ljust(8) 
                            + t_param_bitidx.ljust(3)
                            + t_param_def_value
                            + '\n' )

# ---------------------------------------------------------------------------------------------------------
# Generate header file for a particular spreadsheet

def f_Generate_Header_File( p_header_filename, p_param_bitsize, p_sheet_colidx, p_rows ):

    t_count_params_used = 0

    idx_sw_name = enumC4paramfld.sw_name.value
    idx_string  = enumC4paramfld.string.value
    idx_group   = enumC4paramfld.group.value
    idx_number  = enumC4paramfld.param_num.value

    logging.debug( 'F:[{}] Idx[{},{},{}]'.format(p_header_filename, idx_sw_name,idx_string,idx_group))

    t_names_sorted = sorted( [ each_row[idx_sw_name] for each_row in p_rows ], key=len, reverse=True) 
    t_len_longer_name = 3 + len(t_names_sorted[0])
    del t_names_sorted

    t_strings_sorted = sorted( [ each_row[idx_string] for each_row in p_rows ], key=len, reverse=True) 
    t_len_longer_string = 3 + len(t_strings_sorted[0])
    del t_strings_sorted

    v_ParamHeaderFile  = open( p_header_filename, 'w' )

    v_ParamHeaderFile.write( '#ifndef SMARTRISE_PARAMETERS_' + p_param_bitsize + 'BIT\n'   )
    v_ParamHeaderFile.write( '#define SMARTRISE_PARAMETERS_' + p_param_bitsize + 'BIT\n\n' )
    v_ParamHeaderFile.write( '/* NOTE: FILE UPDATED VIA LIB_SYSTEM - SUPPORT,\n'
                             ' * Edit SystemParameters.xlsx run Update_Parameters.bat\n'
                             ' * to automatically update this file. */\n\n' )
    v_ParamHeaderFile.write( 'enum en_' + p_param_bitsize + 'bit_params\n{\n' )
    #for i in range( len( p_rows ) ):
    for xlsRow in p_rows:
        v_ParamHeaderFile.write( '   enPARAM' + p_param_bitsize + '__' 
                                + str( xlsRow[idx_sw_name] + ',' ).ljust( t_len_longer_name )
                                + '// ' + p_param_bitsize.rjust(2,'0') + '-' + str(int(xlsRow[idx_number])).rjust( 4, '0' )
                                + '\n' )
    v_ParamHeaderFile.write( '\n   NUM_' + p_param_bitsize + 'BIT_PARAMS\n};\n\n' )

    # Print String Table
    v_ParamHeaderFile.write( '#define PARAM_STRINGS_' + p_param_bitsize + 'BIT(PARAM_STR_' + p_param_bitsize + 'BIT) ' )
    for xlsRow in p_rows:
        t_enum   = str(xlsRow[idx_sw_name])
        t_string = str(xlsRow[idx_string])
        v_ParamHeaderFile.write( ' \\\n' )
        v_ParamHeaderFile.write( '   PARAM_STR_' + p_param_bitsize + 'BIT( ' )
        v_ParamHeaderFile.write( str( t_enum + ',' ).ljust(t_len_longer_name))
        v_ParamHeaderFile.write( ' \"' )
        v_ParamHeaderFile.write( str( t_string + '\"').ljust(t_len_longer_string))
        v_ParamHeaderFile.write( ' )' )

        if not str.lower(xlsRow[idx_sw_name]).startswith( 'unused_' ):
            t_count_params_used += 1

    v_ParamHeaderFile.write( '\n\n' )

    # Print Default Entry Table
    v_ParamHeaderFile.write( '/* PARAM_DEFAULT_ENTRY_' + p_param_bitsize + 'BIT(A, B) FORMAT:\n' )
    v_ParamHeaderFile.write( ' *    A = Parameter Byte Map Index \n' )
    v_ParamHeaderFile.write( ' *    B = (GRP1) Default All 8-Bit Map\n' )
    v_ParamHeaderFile.write( ' *    C = (GRP2) Default Learned Floors 8-Bit Map\n' )
    v_ParamHeaderFile.write( ' *    D = (GRP3) Default S-Curve 8-Bit Map\n' )
    v_ParamHeaderFile.write( ' *    E = (GRP4) Default Run Timers 8-Bit Map\n' )
    v_ParamHeaderFile.write( ' *    F = (GRP5) Default (UNUSED) 8-Bit Map\n' )
    v_ParamHeaderFile.write( ' *    G = (GRP6) Default (UNUSED) 8-Bit Map\n' )
    v_ParamHeaderFile.write( ' *    H = (GRP7) Default (UNUSED) 8-Bit Map\n' )
    v_ParamHeaderFile.write( ' *    I = (GRP8) Default (UNUSED) 8-Bit Map\n' )
    v_ParamHeaderFile.write( ' * */\n' )
    v_ParamHeaderFile.write( '/* Parameter Default Table Definition */\n' )
    v_ParamHeaderFile.write( '#define PARAM_DEFAULT_TABLE_' + p_param_bitsize + 'BIT(PARAM_DEFAULT_ENTRY_' + p_param_bitsize + 'BIT) ' )

    MaxDefaultGroups = 8
    DefaultMaps = [0] * MaxDefaultGroups

    # Parse and convert each row in the spreadsheet
    t_nbr_rows = len( p_rows )
    for i, xlsRow in enumerate( p_rows ):
        t_groupnum = int(str(xlsRow[idx_group])[3]) - 1     # skip the first three letters 'GRP'
        t_byte_idx = int(int(i) / 8)
        t_bit_idx  = int(int(i) % 8)

        if t_groupnum < MaxDefaultGroups:
            DefaultMaps[t_groupnum] = DefaultMaps[t_groupnum] + ( 1 << t_bit_idx )
        
        # If end-of-byte, write to table and clear
        if ( ( t_bit_idx == 7 ) or ( i == (t_nbr_rows-1) ) ):
            v_ParamHeaderFile.write( ' \\\n' )
            v_ParamHeaderFile.write( '   PARAM_DEFAULT_ENTRY_' + p_param_bitsize + 'BIT( ' )
            v_ParamHeaderFile.write(        str(t_byte_idx).ljust(6) )
            for k in range(MaxDefaultGroups):
                v_ParamHeaderFile.write( ', ' + str(DefaultMaps[k]).ljust(4))
            v_ParamHeaderFile.write( ' )' )

            # reset all to zero (after writing to the file)
            for j in range(MaxDefaultGroups):
                DefaultMaps[j] = 0

    v_ParamHeaderFile.write( '\n\n' )

    v_ParamHeaderFile.write( '/* Macros for accessing table elements */\n' )
    v_ParamHeaderFile.write( '#define EXPAND_PARAM_STRING_TABLE_'  + p_param_bitsize + 'BIT_AS_ARRAY(A, B) B,\n\n' )
    v_ParamHeaderFile.write( '#define EXPAND_PARAM_DEFAULT_TABLE_' + p_param_bitsize + 'BIT_AS_DEFAULT_GRP_1(A, B, C, D, E, F, G, H, I) B,\n')
    v_ParamHeaderFile.write( '#define EXPAND_PARAM_DEFAULT_TABLE_' + p_param_bitsize + 'BIT_AS_DEFAULT_GRP_2(A, B, C, D, E, F, G, H, I) C,\n')
    v_ParamHeaderFile.write( '#define EXPAND_PARAM_DEFAULT_TABLE_' + p_param_bitsize + 'BIT_AS_DEFAULT_GRP_3(A, B, C, D, E, F, G, H, I) D,\n')
    v_ParamHeaderFile.write( '#define EXPAND_PARAM_DEFAULT_TABLE_' + p_param_bitsize + 'BIT_AS_DEFAULT_GRP_4(A, B, C, D, E, F, G, H, I) E,\n')
    v_ParamHeaderFile.write( '#define EXPAND_PARAM_DEFAULT_TABLE_' + p_param_bitsize + 'BIT_AS_DEFAULT_GRP_5(A, B, C, D, E, F, G, H, I) F,\n')
    v_ParamHeaderFile.write( '#define EXPAND_PARAM_DEFAULT_TABLE_' + p_param_bitsize + 'BIT_AS_DEFAULT_GRP_6(A, B, C, D, E, F, G, H, I) G,\n')
    v_ParamHeaderFile.write( '#define EXPAND_PARAM_DEFAULT_TABLE_' + p_param_bitsize + 'BIT_AS_DEFAULT_GRP_7(A, B, C, D, E, F, G, H, I) H,\n')
    v_ParamHeaderFile.write( '#define EXPAND_PARAM_DEFAULT_TABLE_' + p_param_bitsize + 'BIT_AS_DEFAULT_GRP_8(A, B, C, D, E, F, G, H, I) I,\n')

    v_ParamHeaderFile.write( '\n#endif' )
    v_ParamHeaderFile.close()

    #logging.info( '  Params {:02}-bit Total {: 4} of {:4}'.format( int(p_param_bitsize), t_count_params_used, t_nbr_rows ) )

    msg_summary = '  Params {:02}-bit Total {: 4} of {:4}'.format( int(p_param_bitsize), t_count_params_used, t_nbr_rows )
    logging.info( msg_summary )

    with open( gLogFile, 'a+' ) as fp:
        fp.write( msg_summary + '\n' )


# ---------------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------

g_timestamp_start = time.time()

logging.basicConfig( 
                     level  = logging.INFO,
                     format = '%(relativeCreated)6d:%(levelname)s:%(message)s',
                   )

logging.info( 'Loading input files' )

# If there is a previous log file, delete it
if os.path.exists( gLogFile ):
    os.remove( gLogFile )

# Open Input Files
g_workbook = open_workbook( 'SystemParameters.xlsx' )

obj_c4param = C4Param()

g_allSheets = list()
obj_c4param.f_loadSheet( g_allSheets, g_workbook, '1-bit',  1,  g_SheetCols2Load )
obj_c4param.f_loadSheet( g_allSheets, g_workbook, '8-bit',  8,  g_SheetCols2Load )
obj_c4param.f_loadSheet( g_allSheets, g_workbook, '16-bit', 16, g_SheetCols2Load )
obj_c4param.f_loadSheet( g_allSheets, g_workbook, '24-bit', 24, g_SheetCols2Load )
obj_c4param.f_loadSheet( g_allSheets, g_workbook, '32-bit', 32, g_SheetCols2Load )

g_workbook.release_resources()
del g_workbook

logging.info( 'Generating header (.h) files' )

g_output_defaults_txt = open( "defaults.txt", "w" )
# build in reverse order, same as previous version
for t_each_sheet in g_allSheets[::-1]:
    f_Generate_DefaultsTxt( g_output_defaults_txt, 
                            t_each_sheet[obj_c4param.dictkey_bitsize],
                            t_each_sheet[obj_c4param.dictkey_colidx],
                            t_each_sheet[obj_c4param.dictkey_allrows] )
g_output_defaults_txt.close()

# Generate separate header files for each bitsize
for t_each_sheet in g_allSheets:
    f_Generate_Header_File( 'param'+t_each_sheet[obj_c4param.dictkey_bitsize]+'Bit.h', 
                            t_each_sheet[obj_c4param.dictkey_bitsize],
                            t_each_sheet[obj_c4param.dictkey_colidx], 
                            t_each_sheet[obj_c4param.dictkey_allrows] )

# Wrap it up, release and close everything
del g_allSheets

t_elapsed_time = int( time.time() - g_timestamp_start )
logging.info( 'C4 Parameters generated' )
print( '\nC4 Parameters generated. Total time [{:02d}:{:02d}:{:02d}]'.format( 
        t_elapsed_time // 3600, 
        ( t_elapsed_time % 3600 // 60 ),
        t_elapsed_time % 60 ) )
