
import logging


class C4Sheets(object): 
       
    # Constructor 
    def __init__(self): 

        # Predefine strings to access dictionary
        self.dictkey_sheetname = 'name'
        self.dictkey_sheethdlr = 'sheet'
        self.dictkey_colidx    = 'colidx'        # only relevant if sheets are not pre-loaded
        self.dictkey_allrows   = 'allrows'

        self.HDR_NOT_FOUND = -1

    # ---------------------------------------------------------------------------------------------------------
    #  Lookup within workbook by sheet name
    #  identify the matching column based on the row headers
    #  optionally, based on p_preload_columns, load all sheet cells into a list of lists
    #                                          the order of the inner list will follow enumC4paramfld
    #                                          regardless of the order on the spreadsheet

    def f_loadSheet( self, p_list_all_sheets, p_workbook, p_sheetname, p_InputSheetHeaders, 
                     p_preload_columns = None, p_tail_list = [] ):

        b_SheetLoaded = False

        # Check if the sheetname matches before doing all the heavy lifting
        if str.lower( p_sheetname ) not in ( map( str.lower, p_workbook.sheet_names() ) ):
            print( '  ERROR:Sheet {} does not exist'.format( p_sheetname ) )
            return b_SheetLoaded

        # If p_preload_columns is received, make sure it is a list
        if p_preload_columns is not None:
            if type( p_preload_columns ) is not list:
                print( '  ERROR: calling Sheet:{} p_preload_columns is not a list'.format( p_sheetname ) )
                return b_SheetLoaded

        # Make sure p_InputSheetHeaders is a dictionary
        if type( p_InputSheetHeaders ) is not dict:
            print( '  ERROR: calling Sheet:{} p_InputSheetHeaders is not a dict'.format( p_sheetname ) )
            return b_SheetLoaded

        t_total_sheetheaders = len( p_InputSheetHeaders.keys() )

        # The previous comparison does not care about case-sensitive names
        # The main and only reason for this for-loop is to find the exact 
        # needed to pull the sheet by name
        for next_sheetname in ( p_workbook.sheet_names() ):
            if str.lower( p_sheetname ) == str.lower( next_sheetname ):

                t_sheet = p_workbook.sheet_by_name( next_sheetname )

                # Initialize an empty dictionary
                t_sheet_dict = dict()

                # Save details for the specific sheet
                t_sheet_dict[self.dictkey_sheetname] = next_sheetname
                t_sheet_dict[self.dictkey_sheethdlr] = t_sheet

                # Allocate an array to hold all index, default value is 'not found'
                t_idx_map = t_sheet_dict[self.dictkey_colidx] = [self.HDR_NOT_FOUND] * t_total_sheetheaders

                # Every sheet may have columns at different locations
                # This loop scans the first row and identifies the column
                # The following list is the decoder ring between enumC4paramfld and the actual sheet column
                # The decoder ring is not relevant when using p_preload_columns 
                for colidx, colname in enumerate( t_sheet.row_values(0) ):
                    logging.debug( ' Sheet:{} Idx:{:2} Name:{}'.format(p_sheetname,colidx,colname))
                    if str.lower(colname) in p_InputSheetHeaders.keys():
                        t_idx_map[p_InputSheetHeaders[str.lower(colname)].value] = colidx
                logging.debug( ' Sheet:{} List:[{}]'.format( p_sheetname, t_sheet_dict['colidx'] ) )

                t_idx_map.extend( range( t_total_sheetheaders, t_total_sheetheaders + len(p_tail_list) ) )

                if p_preload_columns is not None:
                    logging.debug( ' Sheet:{:2} loading {:4} rows'.format( p_sheetname, t_sheet.nrows-1 ) )

                    # Sheet rows are converted into list of lists
                    # For each row, create a list with the cells listed in p_preload_columns
                    # the order to store in the list will be based on enumC4paramfld

                    t_list_of_lists = t_sheet_dict[self.dictkey_allrows] = list()

                    for t_row_idx in range( 1, t_sheet.nrows ):     # skip first row/headers
                        xlsRow = t_sheet.row_values( t_row_idx )

                        t_list_strings = [None] * t_total_sheetheaders
                        for t_colidx in p_preload_columns:
                            if self.HDR_NOT_FOUND != t_idx_map[t_colidx]:
                                t_list_strings[t_colidx] = xlsRow[t_idx_map[t_colidx]]

                        t_list_of_lists.append( t_list_strings + p_tail_list )

                p_list_all_sheets.append( t_sheet_dict )
                b_SheetLoaded = abs

        return b_SheetLoaded

