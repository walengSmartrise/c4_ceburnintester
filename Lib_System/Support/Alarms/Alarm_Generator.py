
from xlrd import open_workbook
import logging
import time
import enum


# C4Sheets is part of C4 Controller project, add path to PYTHONPATH
# Folder: <GIT>\<c4_system>\Lib_System\Support
# Current directory structure is just one level up
from C4Alarms import C4Alarms, enumC4alarmfld


# ---------------------------------------------------------------------------------------------------------

g_SheetCols2Load = [
    enumC4alarmfld.tag.value,
    enumC4alarmfld.string.value,
]


# ---------------------------------------------------------------------------------------------------------
# Generate header file 

def f_Generate_Header_File( p_header_filename, p_sheet_colidx, p_rows ):

    idx_enum   = enumC4alarmfld.tag.value
    idx_string = enumC4alarmfld.string.value

    logging.debug( 'F:[{}] Idx[{},{}]'.format(p_header_filename, idx_enum, idx_string ))

    t_enums_sorted = sorted( [ each_row[idx_enum] for each_row in p_rows ], key=len, reverse=True) 
    t_len_longer_enum = 3 + len(t_enums_sorted[0])
    del t_enums_sorted

    t_strings_sorted = sorted( [ each_row[idx_string] for each_row in p_rows ], key=len, reverse=True) 
    t_len_longer_string = 3 + len(t_strings_sorted[0])
    del t_strings_sorted

    fh_alarms_header  = open( p_header_filename, 'w' )

    # Header
    fh_alarms_header.write( '#ifndef ALARMS_H\n'   )
    fh_alarms_header.write( '#define ALARMS_H\n\n' )

    fh_alarms_header.write( '/* NOTE: FILE UPDATED VIA LIB_SYSTEM - SUPPORT - ALARMS, \n'
                            ' * Edit SystemAlarms.xlsx then save as tab delimited (SystemAlarms.txt)  \n'
                            ' * Then run Update_Alarms.bat to automatically update this file */\n\n' )

    fh_alarms_header.write( '/* Includes */\n'
                            '#include \"sys.h\"\n\n' )

    fh_alarms_header.write( '/* Defines */\n'
                            '#define ALARM_LATCH_PERIOD_1MS               ( 3000U )\n'
                            '#define ALARM_RESEND_RATE_1MS                ( 1000U )\n\n' )

    # Print Enum
    fh_alarms_header.write( '// List of system alarms\n' )
    fh_alarms_header.write( 'typedef enum\n{\n' )

    for idx, xlsRow in enumerate( p_rows ):
        fh_alarms_header.write( '   ALM__'
                                + str( xlsRow[idx_enum] + ',' ).ljust( t_len_longer_enum )
                                + ' // AlarmNum: ' + str(idx)
                                + '\n' )

    fh_alarms_header.write( '\n   NUM_ALARMS' + ' // Total: ' + str(idx+1) )
    fh_alarms_header.write( '\n} en_alarms;\n\n'  )

    # Alarm Nodes
    fh_alarms_header.write( '// These are all the nodes that maintain a copy of the faults.\n' )
    fh_alarms_header.write( 'typedef enum\n'
                            '{\n'
                            '   enALARM_NODE__CPLD,\n'                
                            '   enALARM_NODE__MRA,\n'
                            '   enALARM_NODE__MRB,\n'
                            '   enALARM_NODE__CTA,\n'
                            '   enALARM_NODE__CTB,\n'
                            '   enALARM_NODE__COPA,\n'
                            '   enALARM_NODE__COPB,\n\n'
                            '   NUM_ALARM_NODES,\n'
                            '   enALARM_NODE__UNK,\n'
                            '} en_alarm_nodes;\n\n' )

    # Print Alarm Entry Enums
    fh_alarms_header.write( '/* ALARM_ENTRY(A, B) FORMAT:\n'
                            ' *    A = Unique Alarm Number ID\n'
                            ' *    B = Alarm String\n'
                            ' * */\n'
                            '/* Add Alarm HERE - LEAVE NO WHITESPACE BETWEEN LINES */\n\n' )

    # Print Alarm Table
    fh_alarms_header.write( '/* Alarm Table Definition */\n'  )
    fh_alarms_header.write( '#define ALARM_TABLE(ALM_ENTRY) ' )

    for xlsRow in p_rows:
        t_enum   = str(xlsRow[idx_enum])
        t_string = str(xlsRow[idx_string])
        fh_alarms_header.write( ' \\\n' )
        fh_alarms_header.write( '   ALM_ENTRY( ' )
        fh_alarms_header.write( 'ALM__' + str( t_enum + ',' ).ljust(t_len_longer_enum))
        fh_alarms_header.write( ' \"' )
        fh_alarms_header.write( 'A:' + str( t_string + '\"').ljust(t_len_longer_string))
        fh_alarms_header.write( ' )' )

    fh_alarms_header.write( '\n\n' )

    fh_alarms_header.write( '/* Macros for accessing table elements */\n'
                            '#define EXPAND_ALARM_TABLE_AS_ENUMERATION(A, B) A,\n'
                            '#define EXPAND_ALARM_TABLE_AS_STRING_ARRAY(A, B) B,\n\n' )

    # End File
    fh_alarms_header.write( '#endif' )

    logging.info( '  Total {} alarms'.format( len(p_rows) ))


# ---------------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------

g_timestamp_start = time.time()

logging.basicConfig( level=logging.INFO,
                     format='%(relativeCreated)6d:%(levelname)s:%(message)s' )

logging.info( 'Loading input files' )

# Open Input Files
g_workbook = open_workbook( 'SystemAlarms.xlsx' )

objC4alarms = C4Alarms()

g_allSheets = list()
objC4alarms.f_loadSheet( g_allSheets, g_workbook, 'SystemAlarms', g_SheetCols2Load )

g_workbook.release_resources()
del g_workbook

filename_alarms_h = 'alarms.h'

logging.info( 'Generating header files ' + filename_alarms_h )

# Generate separate header files for each bitsize
for t_each_sheet in g_allSheets:
    f_Generate_Header_File( filename_alarms_h, 
                            t_each_sheet[objC4alarms.dictkey_colidx], 
                            t_each_sheet[objC4alarms.dictkey_allrows] )

# Wrap it up, release and close everything
del g_allSheets

t_elapsed_time = int( time.time() - g_timestamp_start )
logging.info( 'C4 Alarms generated' )
print( '\nC4 Alarms generated. Total time [{:02d}:{:02d}:{:02d}]'.format( 
        t_elapsed_time // 3600, 
        ( t_elapsed_time % 3600 // 60 ),
        t_elapsed_time % 60 ) )
