@echo off

@REM This file will update the system alarms located in Lib_System\inc\

@del alarms.h /q

@START C4_Alarm_Generator.exe

@echo Generating Alarms.h

:waitforfile
@if not exist alarms.h (
	@timeout 1 > nul
	goto waitforfile
)

@copy .\alarms.h ..\..\..\Lib_System\inc\

@pause