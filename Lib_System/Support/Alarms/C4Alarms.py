
from xlrd import open_workbook
import enum


# C4Sheets is part of C4 Controller project, add path to PYTHONPATH
# Folder: <GIT>\<c4_system>\Lib_System\Support
# Current directory structure is just one level up
from C4Sheets import C4Sheets

# Enum definition to parse and access data in memory
class enumC4alarmfld( enum.Enum ):
    number      = 0
    tag         = enum.auto()
    string      = enum.auto()
    definition  = enum.auto()
    solution    = enum.auto()
    total_enums = enum.auto()           # WARNING: This entry must be *last*

class C4Alarms( C4Sheets ):

    # Constructor 
    def __init__(self): 
        C4Sheets.__init__( self )

    def f_loadSheet( self, p_list_all_sheets, p_workbook, p_sheetname, p_preload_columns = None ):

        # Translation table between 'Column Headers' and 'Enum(s)'
        t_InputSheetHeaders = { 
            str.lower( '#' )          : enumC4alarmfld.number,
            str.lower( 'Tag' )        : enumC4alarmfld.tag,
            str.lower( 'String' )     : enumC4alarmfld.string,
            str.lower( 'Definition' ) : enumC4alarmfld.definition,
            str.lower( 'Solution' )   : enumC4alarmfld.solution,
        }

        b_SheetLoaded = super().f_loadSheet( p_list_all_sheets, p_workbook, 
                                             p_sheetname, t_InputSheetHeaders, p_preload_columns )

        return b_SheetLoaded

