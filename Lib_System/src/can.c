#include <string.h>
#include "sys.h"
#include "position.h"
#include "chip.h"

RINGBUFF_T RxRingBuffer_CAN1;
RINGBUFF_T RxRingBuffer_CAN2;
RINGBUFF_T TxRingBuffer_CAN1;
RINGBUFF_T TxRingBuffer_CAN2;
RINGBUFF_T RxPositionRingBuffer;

RINGBUFF_T RxCedesRingBuffer_0x21;
RINGBUFF_T RxCedesRingBuffer_0x22;

RINGBUFF_T RxCedesRingBuffer_0x1A;
RINGBUFF_T RxCedesRingBuffer_0x1B;
RINGBUFF_T RxCedesRingBuffer_0x2A;
RINGBUFF_T RxCedesRingBuffer_0x2B;

/*----------------------------------------------------------------------------
Will select any free Tx Buffer.
Intended for highest priority data. (i.e. position data)
 *----------------------------------------------------------------------------*/
inline CAN_BUFFER_ID_T CAN_GetFreeTxBuf_Pr1(LPC_CAN_T *pCAN)
{
   CAN_BUFFER_ID_T TxBufID = 0;

   /* Select a free buffer */
   for (CAN_BUFFER_ID_T i = CAN_BUFFER_1; i < CAN_BUFFER_LAST; i++) {
      if (Chip_CAN_GetStatus(pCAN) & CAN_SR_TBS(i))
      {
         TxBufID = i;
         break;
      }
   }

   return TxBufID;
}
/*----------------------------------------------------------------------------
Will select free buffer from only Buffers 2/3, not 1.
Intended for lower priority data. (i.e. NON-position data)
 *----------------------------------------------------------------------------*/
inline CAN_BUFFER_ID_T CAN_GetFreeTxBuf_Pr2(LPC_CAN_T *pCAN)
{
   CAN_BUFFER_ID_T TxBufID = 0;

   /* Select a free buffer */
   for (CAN_BUFFER_ID_T i = CAN_BUFFER_2; i < CAN_BUFFER_LAST; i++) {
      if (Chip_CAN_GetStatus(pCAN) & CAN_SR_TBS(i))
      {
         TxBufID = i;
         break;
      }
   }
   return TxBufID;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UnloadPosition( CAN_MSG_T *RcvMsgBuf )
{

    switch ( RcvMsgBuf->ID )
    {
        case 0x1A:
            RingBuffer_Insert(&RxCedesRingBuffer_0x1A, RcvMsgBuf);
            break;
        case 0x1B:
            RingBuffer_Insert(&RxCedesRingBuffer_0x1B, RcvMsgBuf);
            break;
        /*0x21 and 0x22 are from old protocol */
        case 0x21:
            RingBuffer_Insert(&RxCedesRingBuffer_0x21, RcvMsgBuf);
            break;

        case 0x22:
            RingBuffer_Insert(&RxCedesRingBuffer_0x22, RcvMsgBuf);
            break;

        case 0x2A:
            RingBuffer_Insert(&RxCedesRingBuffer_0x2A, RcvMsgBuf);
            break;
        case 0x2B:
            RingBuffer_Insert(&RxCedesRingBuffer_0x2B, RcvMsgBuf);
            break;
        default:
           while ( 1 )
           {
               // There is an unknown message sent over CAN.
           }
           break;
    }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static inline uint8_t  CheckIfCEDES( CAN_MSG_T *RcvMsgBuf )
{
    // Must be an 11 bit ID and have ID of 0x21 or 0x22
    uint8_t bOldID = ( RcvMsgBuf->ID == 0x21 || RcvMsgBuf->ID == 0x22 );
    uint8_t bNewID = ( (RcvMsgBuf->ID == 0x1A) || (RcvMsgBuf->ID == 0x1B) || (RcvMsgBuf->ID == 0x2A) || (RcvMsgBuf->ID == 0x2B ) );

    return (!( RcvMsgBuf->ID & CAN_RFS_FF ) && ( bNewID || bOldID));
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static inline uint8_t CheckIf_ForwardedPosition( CAN_MSG_T *RcvMsgBuf )
{
   return (RcvMsgBuf->ID == POS_FWD_MSG_ID);
}

/*-----------------------------------------------------------------------------
   Adapted from LPCOpen can_17xx_40xx.c Chip_CAN_Send
   Added error checking to the original function
   Returns 1 if successful
   Returns 0 if invalid TxBufID or attempting to load to nonempty buffer
 -----------------------------------------------------------------------------*/
/* Set Tx Frame Information */
STATIC INLINE void setSendFrameInfo(LPC_CAN_T *pCAN, CAN_BUFFER_ID_T TxBufID,
                           LPC_CAN_TX_T *pTxFrame)
{
   pCAN->TX[TxBufID] = *pTxFrame;
}
Status Sys_CAN_Send(LPC_CAN_T *pCAN, CAN_BUFFER_ID_T TxBufID, CAN_MSG_T *pMsg)
{
   Status eStatus = ERROR;
   uint8_t i = 0;
   LPC_CAN_TX_T TxFrame;
   if( TxBufID <= CAN_BUFFER_3 )
   {
      /* Write Frame Information */
      TxFrame.TFI = 0;
      if (pMsg->Type & CAN_REMOTE_MSG) {
         TxFrame.TFI |= CAN_TFI_RTR;
      }
      else {
         TxFrame.TFI |= CAN_TFI_DLC(pMsg->DLC);
         for (i = 0; i < (CAN_MSG_MAX_DATA_LEN + 3) / 4; i++) {
            TxFrame.TD[i] =
               pMsg->Data[4 *
                        i] |
               (pMsg->Data[4 * i +
                        1] << 8) | (pMsg->Data[4 * i + 2] << 16) | (pMsg->Data[4 * i + 3] << 24);
         }
      }

      if (pMsg->ID & CAN_EXTEND_ID_USAGE) {
         TxFrame.TFI |= CAN_TFI_FF;
         TxFrame.TID = CAN_TID_ID29(pMsg->ID);
      }
      else {
         TxFrame.TID = CAN_TID_ID11(pMsg->ID);
      }
      if(Chip_CAN_GetStatus(pCAN) & CAN_SR_TBS(TxBufID))
      {
         /* Set message information */
         setSendFrameInfo(pCAN, TxBufID, &TxFrame);

         /* Select buffer and Write Transmission Request */
         if (Chip_CAN_GetMode(pCAN) == CAN_SELFTEST_MODE) {
            Chip_CAN_SetCmd(pCAN, CAN_CMR_STB(TxBufID) | CAN_CMR_SRR);
         }
         else {
            Chip_CAN_SetCmd(pCAN, CAN_CMR_STB(TxBufID) | CAN_CMR_TR);
         }
         eStatus = SUCCESS;
      }
   }

   return eStatus;
}
/*-----------------------------------------------------------------------------

   **************** Interrupt Service Routine -- CAN ****************

   This function is in the MCU's interrupt vector table and is called
   asynchronously whenever an interrupt is pending on the peripheral device.

 -----------------------------------------------------------------------------*/
void CAN_IRQHandler( void )
{
   CAN_MSG_T RcvMsgBuf;

   /* Clear CAN2 Interrupts by Reading ICR Register */
   uint32_t IntStatus_CAN1 = Chip_CAN_GetIntStatus(LPC_CAN1);
   uint32_t IntStatus_CAN2 = Chip_CAN_GetIntStatus(LPC_CAN2);

   if ( IntStatus_CAN1 & CAN_ICR_RI ) /* New Msg Received */
   {
       if(Chip_CAN_Receive(LPC_CAN1, &RcvMsgBuf))
       {
          if(CheckIf_ForwardedPosition(&RcvMsgBuf))
          {
             RingBuffer_Insert(&RxPositionRingBuffer, &RcvMsgBuf);
          }
          else
          {
             RingBuffer_Insert(&RxRingBuffer_CAN1, &RcvMsgBuf);
          }
       }
   }

   if ( IntStatus_CAN2 & CAN_ICR_RI ) /* New Msg Received */
   {
      if(Chip_CAN_Receive(LPC_CAN2, &RcvMsgBuf))
      {
         /* If this is a channel 2 packet, Forward it Immediately */
         if( CheckIfCEDES( &RcvMsgBuf ) )
         {
            UnloadPosition( &RcvMsgBuf ); // CTA recieved from CEDES
         }
         else if(CheckIf_ForwardedPosition(&RcvMsgBuf))
         {
            RingBuffer_Insert(&RxPositionRingBuffer, &RcvMsgBuf);// CTA to MRA
         }
         else
         {
            RingBuffer_Insert(&RxRingBuffer_CAN2, &RcvMsgBuf);
         }
      }
   }
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
