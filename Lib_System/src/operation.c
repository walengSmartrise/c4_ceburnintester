#include "operation.h"

static struct st_operation gstOperation;

inline enum en_classop GetOperation_ClassOfOp( void )
{
    return gstOperation.eClassOfOperation;
}
inline enum en_mocmd GetOperation_MotionCmd( void )
{
    return gstOperation.eCommand;
}
inline enum en_mode_learn GetOperation_LearnMode( void )
{
    return gstOperation.eLearnMode;
}
inline enum en_mode_auto GetOperation_AutoMode( void )
{
    return gstOperation.eAutoMode;
}
inline enum en_mode_manual GetOperation_ManualMode( void )
{
    return gstOperation.eManualMode;
}
inline uint32_t GetOperation_RequestedDestination( void )
{
    return gstOperation.ulRequestedDest & MAX_24BIT_VALUE;
}
inline uint32_t GetOperation_PositionLimit_UP( void )
{
    return gstOperation.ulPosLimit_UP & MAX_24BIT_VALUE;
}
inline uint32_t GetOperation_PositionLimit_DN( void )
{
    return gstOperation.ulPosLimit_DN & MAX_24BIT_VALUE;
}

inline uint16_t GetOperation_SpeedLimit( void )
{
    return gstOperation.uwSpeedLimit;
}
inline uint8_t GetOperation_CurrentFloor( void )
{
    return gstOperation.ucCurrentFloor;
}
inline uint8_t GetOperation_DestinationFloor( void )
{
    return gstOperation.ucDestinationFloor;
}
inline uint8_t GetOperation_BypassTermLimits( void )
{
    return gstOperation.bBypTermLimits;
}


void SetOperation_ClassOfOp( enum en_classop passed )
{
    gstOperation.eClassOfOperation = passed;
}
void SetOperation_MotionCmd( enum en_mocmd passed )
{
    gstOperation.eCommand = passed;
}
void SetOperation_LearnMode( enum en_mode_learn passed )
{
    gstOperation.eLearnMode = passed;
}
void SetOperation_AutoMode( enum en_mode_auto passed )
{
    gstOperation.eAutoMode = passed;
}
void SetOperation_ManualMode( enum en_mode_manual passed )
{
    gstOperation.eManualMode = passed;
}
void SetOperation_RequestedDestination( uint32_t passed )
{
    gstOperation.ulRequestedDest = passed;
}
void SetOperation_PositionLimit_UP( uint32_t passed )
{
    gstOperation.ulPosLimit_UP = passed;
}
void SetOperation_PositionLimit_DN( uint32_t passed )
{
    gstOperation.ulPosLimit_DN = passed;
}
void SetOperation_SpeedLimit( uint16_t passed )
{
    gstOperation.uwSpeedLimit = passed;
}
void SetOperation_CurrentFloor( uint8_t passed )
{
    gstOperation.ucCurrentFloor = passed;
}
void SetOperation_DestinationFloor( uint8_t passed )
{
    gstOperation.ucDestinationFloor = passed;
}
void SetOperation_BypassTermLimits( uint8_t passed )
{
    gstOperation.bBypTermLimits = passed;
}

