#include "position.h"

static struct st_position gstPosition;

inline void SetPosition_PositionCount( uint32_t uiPositionCount )
{
   gstPosition.ulPosCount = uiPositionCount & MAX_24BIT_VALUE;
}
inline void SetPosition_Velocity( int16_t wVelocity )
{
   gstPosition.wVelocity = wVelocity;
}

inline uint32_t GetPosition_PositionCount()
{
   return gstPosition.ulPosCount;
}
inline int16_t GetPosition_Velocity()
{
   return gstPosition.wVelocity;
}
