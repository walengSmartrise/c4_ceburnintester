#include <stdint.h>
#include "DefaultAll.h"
#include "config_System.h"
#include "param.h"

#define NUM_HEADER_BYTES   ((uint32_t)6)
#define FLASH_BASE_ADDRESS ( ((uint32_t)0x9000) + NUM_HEADER_BYTES )

static uint32_t defaultAllParameterIndex = 0;
/* Bitmaps for which default group each parameter belongs to */
static uint8_t aucDefaultBitmaps_1Bit[DEFAULT_CMD__GRP8-DEFAULT_CMD__ALL+1][BITMAP8_SIZE(NUM_1BIT_PARAMS)] =
{
   { PARAM_DEFAULT_TABLE_1BIT(EXPAND_PARAM_DEFAULT_TABLE_1BIT_AS_DEFAULT_GRP_1) },
   { PARAM_DEFAULT_TABLE_1BIT(EXPAND_PARAM_DEFAULT_TABLE_1BIT_AS_DEFAULT_GRP_2) },
   { PARAM_DEFAULT_TABLE_1BIT(EXPAND_PARAM_DEFAULT_TABLE_1BIT_AS_DEFAULT_GRP_3) },
   { PARAM_DEFAULT_TABLE_1BIT(EXPAND_PARAM_DEFAULT_TABLE_1BIT_AS_DEFAULT_GRP_4) },
   { PARAM_DEFAULT_TABLE_1BIT(EXPAND_PARAM_DEFAULT_TABLE_1BIT_AS_DEFAULT_GRP_5) },
   { PARAM_DEFAULT_TABLE_1BIT(EXPAND_PARAM_DEFAULT_TABLE_1BIT_AS_DEFAULT_GRP_6) },
   { PARAM_DEFAULT_TABLE_1BIT(EXPAND_PARAM_DEFAULT_TABLE_1BIT_AS_DEFAULT_GRP_7) },
   { PARAM_DEFAULT_TABLE_1BIT(EXPAND_PARAM_DEFAULT_TABLE_1BIT_AS_DEFAULT_GRP_8) },
};
static uint8_t aucDefaultBitmaps_8Bit[DEFAULT_CMD__GRP8-DEFAULT_CMD__ALL+1][BITMAP8_SIZE(NUM_8BIT_PARAMS)] =
{
   { PARAM_DEFAULT_TABLE_8BIT(EXPAND_PARAM_DEFAULT_TABLE_8BIT_AS_DEFAULT_GRP_1) },
   { PARAM_DEFAULT_TABLE_8BIT(EXPAND_PARAM_DEFAULT_TABLE_8BIT_AS_DEFAULT_GRP_2) },
   { PARAM_DEFAULT_TABLE_8BIT(EXPAND_PARAM_DEFAULT_TABLE_8BIT_AS_DEFAULT_GRP_3) },
   { PARAM_DEFAULT_TABLE_8BIT(EXPAND_PARAM_DEFAULT_TABLE_8BIT_AS_DEFAULT_GRP_4) },
   { PARAM_DEFAULT_TABLE_8BIT(EXPAND_PARAM_DEFAULT_TABLE_8BIT_AS_DEFAULT_GRP_5) },
   { PARAM_DEFAULT_TABLE_8BIT(EXPAND_PARAM_DEFAULT_TABLE_8BIT_AS_DEFAULT_GRP_6) },
   { PARAM_DEFAULT_TABLE_8BIT(EXPAND_PARAM_DEFAULT_TABLE_8BIT_AS_DEFAULT_GRP_7) },
   { PARAM_DEFAULT_TABLE_8BIT(EXPAND_PARAM_DEFAULT_TABLE_8BIT_AS_DEFAULT_GRP_8) },
};
static uint8_t aucDefaultBitmaps_16Bit[DEFAULT_CMD__GRP8-DEFAULT_CMD__ALL+1][BITMAP8_SIZE(NUM_16BIT_PARAMS)] =
{
   { PARAM_DEFAULT_TABLE_16BIT(EXPAND_PARAM_DEFAULT_TABLE_16BIT_AS_DEFAULT_GRP_1) },
   { PARAM_DEFAULT_TABLE_16BIT(EXPAND_PARAM_DEFAULT_TABLE_16BIT_AS_DEFAULT_GRP_2) },
   { PARAM_DEFAULT_TABLE_16BIT(EXPAND_PARAM_DEFAULT_TABLE_16BIT_AS_DEFAULT_GRP_3) },
   { PARAM_DEFAULT_TABLE_16BIT(EXPAND_PARAM_DEFAULT_TABLE_16BIT_AS_DEFAULT_GRP_4) },
   { PARAM_DEFAULT_TABLE_16BIT(EXPAND_PARAM_DEFAULT_TABLE_16BIT_AS_DEFAULT_GRP_5) },
   { PARAM_DEFAULT_TABLE_16BIT(EXPAND_PARAM_DEFAULT_TABLE_16BIT_AS_DEFAULT_GRP_6) },
   { PARAM_DEFAULT_TABLE_16BIT(EXPAND_PARAM_DEFAULT_TABLE_16BIT_AS_DEFAULT_GRP_7) },
   { PARAM_DEFAULT_TABLE_16BIT(EXPAND_PARAM_DEFAULT_TABLE_16BIT_AS_DEFAULT_GRP_8) },
};
static uint8_t aucDefaultBitmaps_24Bit[DEFAULT_CMD__GRP8-DEFAULT_CMD__ALL+1][BITMAP8_SIZE(NUM_24BIT_PARAMS)] =
{
   { PARAM_DEFAULT_TABLE_24BIT(EXPAND_PARAM_DEFAULT_TABLE_24BIT_AS_DEFAULT_GRP_1) },
   { PARAM_DEFAULT_TABLE_24BIT(EXPAND_PARAM_DEFAULT_TABLE_24BIT_AS_DEFAULT_GRP_2) },
   { PARAM_DEFAULT_TABLE_24BIT(EXPAND_PARAM_DEFAULT_TABLE_24BIT_AS_DEFAULT_GRP_3) },
   { PARAM_DEFAULT_TABLE_24BIT(EXPAND_PARAM_DEFAULT_TABLE_24BIT_AS_DEFAULT_GRP_4) },
   { PARAM_DEFAULT_TABLE_24BIT(EXPAND_PARAM_DEFAULT_TABLE_24BIT_AS_DEFAULT_GRP_5) },
   { PARAM_DEFAULT_TABLE_24BIT(EXPAND_PARAM_DEFAULT_TABLE_24BIT_AS_DEFAULT_GRP_6) },
   { PARAM_DEFAULT_TABLE_24BIT(EXPAND_PARAM_DEFAULT_TABLE_24BIT_AS_DEFAULT_GRP_7) },
   { PARAM_DEFAULT_TABLE_24BIT(EXPAND_PARAM_DEFAULT_TABLE_24BIT_AS_DEFAULT_GRP_8) },
};
static uint8_t aucDefaultBitmaps_32Bit[DEFAULT_CMD__GRP8-DEFAULT_CMD__ALL+1][BITMAP8_SIZE(NUM_32BIT_PARAMS)] =
{
   { PARAM_DEFAULT_TABLE_32BIT(EXPAND_PARAM_DEFAULT_TABLE_32BIT_AS_DEFAULT_GRP_1) },
   { PARAM_DEFAULT_TABLE_32BIT(EXPAND_PARAM_DEFAULT_TABLE_32BIT_AS_DEFAULT_GRP_2) },
   { PARAM_DEFAULT_TABLE_32BIT(EXPAND_PARAM_DEFAULT_TABLE_32BIT_AS_DEFAULT_GRP_3) },
   { PARAM_DEFAULT_TABLE_32BIT(EXPAND_PARAM_DEFAULT_TABLE_32BIT_AS_DEFAULT_GRP_4) },
   { PARAM_DEFAULT_TABLE_32BIT(EXPAND_PARAM_DEFAULT_TABLE_32BIT_AS_DEFAULT_GRP_5) },
   { PARAM_DEFAULT_TABLE_32BIT(EXPAND_PARAM_DEFAULT_TABLE_32BIT_AS_DEFAULT_GRP_6) },
   { PARAM_DEFAULT_TABLE_32BIT(EXPAND_PARAM_DEFAULT_TABLE_32BIT_AS_DEFAULT_GRP_7) },
   { PARAM_DEFAULT_TABLE_32BIT(EXPAND_PARAM_DEFAULT_TABLE_32BIT_AS_DEFAULT_GRP_8) },
};

uint32_t GetDefaultAllParameterIndex( void )
{
    return defaultAllParameterIndex;
}

void IncDefaultAllParameterIndex( void )
{
    defaultAllParameterIndex++;
}

void ResetdefaultAllParameterIndex( void )
{
    defaultAllParameterIndex = 0;
}

void SetDefaultAllParameterIndex( uint32_t passed )
{
    defaultAllParameterIndex = passed;
}

static uint8_t Get1BitParameterDefaultFromFlash(const enum en_1bit_params parameter)
{
    // Get the address offset
    uint32_t addressOffset = ( (NUM_32BIT_PARAMS*4) + (NUM_24BIT_PARAMS*3) + (NUM_16BIT_PARAMS*2) + (NUM_8BIT_PARAMS*1) + (parameter*1));
    // Add the base flash address
    addressOffset += FLASH_BASE_ADDRESS;

    uint8_t byte0 = *((uint8_t *)(addressOffset+0));
    return byte0;
}

static uint8_t Get8BitParameterDefaultFromFlash(const enum en_8bit_params parameter)
{
    // Get the address offset
    uint32_t addressOffset = ((NUM_32BIT_PARAMS*4) + (NUM_24BIT_PARAMS*3) + (NUM_16BIT_PARAMS*2) + (parameter*1));
    // Add the base flash address
    addressOffset += FLASH_BASE_ADDRESS;

    uint8_t byte0 = *((uint8_t *)(addressOffset+0));
    return byte0;
}

static uint16_t Get16BitParameterDefaultFromFlash(const enum en_16bit_params parameter)
{
    // Get the address offset
    uint32_t addressOffset = ((NUM_32BIT_PARAMS*4) + (NUM_24BIT_PARAMS*3) + (parameter*2));
    // Add the base flash address
    addressOffset += FLASH_BASE_ADDRESS;

    uint8_t byte0 = *((uint8_t *)(addressOffset+0));
    uint8_t byte1 = *((uint8_t *)(addressOffset+1));
    uint32_t result = (byte1<<8) | (byte0<<0);

    return result;
}

static uint32_t Get24BitParameterDefaultFromFlash(const enum en_24bit_params parameter)
{
    // Get the address offset
    uint32_t addressOffset = ((NUM_32BIT_PARAMS*4) + (parameter*3));
    // Add the base flash address
    addressOffset += FLASH_BASE_ADDRESS;

    uint8_t byte0 = *((uint8_t *)(addressOffset+0));
    uint8_t byte1 = *((uint8_t *)(addressOffset+1));
    uint8_t byte2 = *((uint8_t *)(addressOffset+2));
    uint32_t result = (byte2<<16) | (byte1<<8) | (byte0<<0);

    return result;
}

static uint32_t Get32BitParameterDefaultFromFlash(const enum en_32bit_params parameter)
{
    // Get the address offset
    uint32_t addressOffset = (parameter*4);
    // Add the base flash address
    addressOffset += FLASH_BASE_ADDRESS;

    uint8_t byte0 = *((uint8_t *)(addressOffset+0));
    uint8_t byte1 = *((uint8_t *)(addressOffset+1));
    uint8_t byte2 = *((uint8_t *)(addressOffset+2));
    uint8_t byte3 = *((uint8_t *)(addressOffset+3));
    uint32_t result = (byte3<<24) | (byte2<<16) | (byte1<<8) | (byte0<<0);

    return result;
}

uint8_t DefaultAll_1Bit( enum_default_cmd eCommand )
{
    uint8_t currentParameterTransferedToEEPROM = 0;
    uint8_t allParameteresTransferedToEEPROM   = 0;
    static enum en_1bit_params parameter_1Bit = 0;

    if( parameter_1Bit <  NUM_1BIT_PARAMS )
    {
        if( ( eCommand == DEFAULT_CMD__FACTORY )
         || ( Sys_Bit_Get( &aucDefaultBitmaps_1Bit[eCommand-DEFAULT_CMD__ALL][0], parameter_1Bit ) ) )
        {
           currentParameterTransferedToEEPROM = Param_WriteValue_1Bit(parameter_1Bit, Get1BitParameterDefaultFromFlash(parameter_1Bit));
           if(currentParameterTransferedToEEPROM)
           {
              parameter_1Bit++;
              IncDefaultAllParameterIndex();
           }
        }
        else
        {
           parameter_1Bit++;
           IncDefaultAllParameterIndex();
        }
    }
    else if(parameter_1Bit >= NUM_1BIT_PARAMS )
    {
       parameter_1Bit = 0;
       ResetdefaultAllParameterIndex();
       allParameteresTransferedToEEPROM = 1;
    }

    return allParameteresTransferedToEEPROM;
}

uint8_t DefaultAll_8Bit( enum_default_cmd eCommand )
{
    uint8_t currentParameterTransferedToEEPROM = 0;
    uint8_t allParameteresTransferedToEEPROM   = 0;
    static enum en_8bit_params parameter_8Bit = 0;

    if( parameter_8Bit <  NUM_8BIT_PARAMS )
    {
       if( ( eCommand == DEFAULT_CMD__FACTORY )
        || ( Sys_Bit_Get( &aucDefaultBitmaps_8Bit[eCommand-DEFAULT_CMD__ALL][0], parameter_8Bit ) ) )
       {
          currentParameterTransferedToEEPROM = Param_WriteValue_8Bit(parameter_8Bit, Get8BitParameterDefaultFromFlash(parameter_8Bit));
          if(currentParameterTransferedToEEPROM)
          {
             parameter_8Bit++;
             IncDefaultAllParameterIndex();
          }
       }
       else
       {
          parameter_8Bit++;
          IncDefaultAllParameterIndex();
       }
    }
    else if(parameter_8Bit >= NUM_8BIT_PARAMS )
    {
       parameter_8Bit = 0;
       ResetdefaultAllParameterIndex();
       allParameteresTransferedToEEPROM = 1;
    }

    return allParameteresTransferedToEEPROM;
}

uint8_t DefaultAll_16Bit( enum_default_cmd eCommand )
{
    uint8_t currentParameterTransferedToEEPROM = 0;
    uint8_t allParameteresTransferedToEEPROM   = 0;
    static enum en_16bit_params parameter_16Bit = 0;

    if( parameter_16Bit <  NUM_16BIT_PARAMS )
    {
       if( ( eCommand == DEFAULT_CMD__FACTORY )
        || ( Sys_Bit_Get( &aucDefaultBitmaps_16Bit[eCommand-DEFAULT_CMD__ALL][0], parameter_16Bit ) ) )
       {
          currentParameterTransferedToEEPROM = Param_WriteValue_16Bit(parameter_16Bit, Get16BitParameterDefaultFromFlash(parameter_16Bit));
          if(currentParameterTransferedToEEPROM)
          {
             parameter_16Bit++;
             IncDefaultAllParameterIndex();
          }
       }
       else
       {
          parameter_16Bit++;
          IncDefaultAllParameterIndex();
       }
    }
    else if(parameter_16Bit >= NUM_16BIT_PARAMS )
    {
       parameter_16Bit = 0;
       ResetdefaultAllParameterIndex();
       allParameteresTransferedToEEPROM = 1;
    }

    return allParameteresTransferedToEEPROM;
}

uint8_t DefaultAll_24Bit( enum_default_cmd eCommand )
{
    uint8_t currentParameterTransferedToEEPROM = 0;
    uint8_t allParameteresTransferedToEEPROM   = 0;
    static enum en_24bit_params parameter_24Bit = 0;

    if( parameter_24Bit <  NUM_24BIT_PARAMS )
    {
       if( ( eCommand == DEFAULT_CMD__FACTORY )
        || ( Sys_Bit_Get( &aucDefaultBitmaps_24Bit[eCommand-DEFAULT_CMD__ALL][0], parameter_24Bit ) ) )
       {
          currentParameterTransferedToEEPROM = Param_WriteValue_24Bit(parameter_24Bit, Get24BitParameterDefaultFromFlash(parameter_24Bit));
          if(currentParameterTransferedToEEPROM)
          {
             parameter_24Bit++;
             IncDefaultAllParameterIndex();
          }
       }
       else
       {
          parameter_24Bit++;
          IncDefaultAllParameterIndex();
       }
    }
    else if(parameter_24Bit >= NUM_24BIT_PARAMS )
    {
       parameter_24Bit = 0;
       ResetdefaultAllParameterIndex();
       allParameteresTransferedToEEPROM = 1;
    }

    return allParameteresTransferedToEEPROM;
}

uint8_t DefaultAll_32Bit( enum_default_cmd eCommand )
{
    uint8_t currentParameterTransferedToEEPROM = 0;
    uint8_t allParameteresTransferedToEEPROM   = 0;
    static enum en_32bit_params parameter_32Bit = 0;

    if( parameter_32Bit <  NUM_32BIT_PARAMS )
    {
       if( ( eCommand == DEFAULT_CMD__FACTORY )
        || ( Sys_Bit_Get( &aucDefaultBitmaps_32Bit[eCommand-DEFAULT_CMD__ALL][0], parameter_32Bit ) ) )
       {
          currentParameterTransferedToEEPROM = Param_WriteValue_32Bit(parameter_32Bit, Get32BitParameterDefaultFromFlash(parameter_32Bit));
          if(currentParameterTransferedToEEPROM)
          {
             parameter_32Bit++;
             IncDefaultAllParameterIndex();
          }
       }
       else
       {
          parameter_32Bit++;
          IncDefaultAllParameterIndex();
       }
    }
    else if(parameter_32Bit >= NUM_32BIT_PARAMS )
    {
       parameter_32Bit = 0;
       ResetdefaultAllParameterIndex();
       allParameteresTransferedToEEPROM = 1;
    }

    return allParameteresTransferedToEEPROM;
}


