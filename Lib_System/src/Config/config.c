#ifndef PARAMETERS_SMARTRISE
#define PARAMETERS_SMARTRISE

#include <stdint.h>
#include "DefaultAll.h"
#include "param.h"
#include "config.h"
#include "config_System.h"


static DefaultAll_FSM defaultAllState = DEFAULT_ALL_IDLE;
DefaultAll_FSM GetDefaultAllState(void)
{
    return defaultAllState;
}

void SetDefaultAllState(DefaultAll_FSM newState)
{
    defaultAllState = newState;
}



uint8_t DefaultAll( uint8_t *pucState, uint32_t *puiIndex, enum_default_cmd eCommand )
{
    uint8_t finished = 0;
    static uint8_t state = 0;

    switch (state)
    {
        case 0:
            SetDefaultAllState(DEFAULT_ALL_ONE_BIT);
            if(DefaultAll_1Bit(eCommand))
            {
                state++;
            }
            break;
        case 1:
            SetDefaultAllState(DEFAULT_ALL_EIGHT_BIT);
            if(DefaultAll_8Bit(eCommand))
            {
                state++;
            }
            break;
        case 2:
            SetDefaultAllState(DEFAULT_ALL_SIXTEEN_BIT);
            if(DefaultAll_16Bit(eCommand))
            {
                state++;
            }
            break;
        case 3:
            SetDefaultAllState(DEFAULT_ALL_TWENTYFOUR_BIT);
            if(DefaultAll_24Bit(eCommand))
            {
                state++;
            }
            break;
        case 4:
            SetDefaultAllState(DEFAULT_ALL_THIRTYTWO_BIT);
            if(DefaultAll_32Bit(eCommand))
            {
                state = 0;
                finished = 1;
                SetDefaultAllState(DEFAULT_ALL_IDLE);
            }
            break;
    }

    *pucState = state;
    return finished;
}


#endif // PARAMETERS_SMARTRISE
