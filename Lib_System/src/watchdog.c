/*
 * LPC_WWDT->c
 *
 *  Created on: May 21, 2016
 *      Author: sean
 */


/*
 * This implements the watchdog interface. This code is used across all
 * boards to implement there own watchdog module.
 */

#include "sys.h"

#include <stdint.h>
#include "watchdog.h"

#define WATCHDOG_TIMEOUT_SEC            (1.0f)//(0.3f)
#define WATCHDOG_OSCILLATOR_FREQ        (500000)// From UM10562 3.8.4
#define WATCHDOG_TIMEOUT_COUNT          (WATCHDOG_OSCILLATOR_FREQ * WATCHDOG_TIMEOUT_SEC)

void watchdog_init()
{
   // WDT should timeout every 200ms => 200ms * WDOSC_FREQ = 100000
   LPC_WWDT->TC = (uint32_t) WATCHDOG_TIMEOUT_COUNT;

   //    Bit 0: enable
   //    Bit 1: reset when watch dog times out
   LPC_WWDT->MOD  = 0x03;

   LPC_WWDT->FEED = 0xAA;
   LPC_WWDT->FEED = 0x55;
}

void watchdog_feed()
{
   LPC_WWDT->FEED = 0xAA;
   LPC_WWDT->FEED = 0x55;
}
