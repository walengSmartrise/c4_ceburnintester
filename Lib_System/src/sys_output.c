#include "sys.h"

static const uint8_t aucFunctionsPerOutputGroup[ NUM_OUTPUT_GROUPS ] =
{
      1,//OUTPUT_GROUP__NONE,
      NUM_COG_AUTO_OPER,//OUTPUT_GROUP__AUTO_OPER,
      NUM_COG_DOORS,//OUTPUT_GROUP__DOORS_F,
      NUM_COG_DOORS,//OUTPUT_GROUP__DOORS_R,
      NUM_COG_FIRE_EQ,//OUTPUT_GROUP__FIRE_EQ,
      NUM_COG_INSP,//OUTPUT_GROUP__INSP,
      NUM_COG_CTRL,//OUTPUT_GROUP__CTRL,
      NUM_COG_SAFETY,//OUTPUT_GROUP__SAFETY,
      MAX_NUM_FLOORS,//OUTPUT_GROUP__CCL_F,
      MAX_NUM_FLOORS,//OUTPUT_GROUP__CCL_R,
      NUM_COG_EPWR,//OUTPUT_GROUP__EPWR,
};
static const char * const pasStrings_COG[ NUM_OUTPUT_GROUPS ] =
{
      "<unused>",//OUTPUT_GROUP__NONE,
      "Auto Operation",//OUTPUT_GROUP__AUTO_OPER,
      "Front Doors",//OUTPUT_GROUP__DOORS_F,
      "Rear Doors",//OUTPUT_GROUP__DOORS_R,
      "Fire EQ",//OUTPUT_GROUP__FIRE_EQ,
      "Inspection",//OUTPUT_GROUP__INSP,
      "Controller",//OUTPUT_GROUP__CTRL,
      "Safety",//OUTPUT_GROUP__SAFETY,
      "CCL Front",//OUTPUT_GROUP__CCL_F,
      "CCL Rear",//OUTPUT_GROUP__CCL_R,
      "E-Power",//OUTPUT_GROUP__EPWR,
};
//-----------------------------------------------
static const enum en_output_functions aeCOG_Mapping_AutoOper[NUM_COG_AUTO_OPER] =
{
      enOUT_TRV_UP,//COG_AUTO_OPER__TRV_UP,
      enOUT_TRV_DN,//COG_AUTO_OPER__TRV_DN,
      UNUSED_OUTPUT,//COG_AUTO_OPER__UNUSED2,
      enOUT_LMP_Parking,//COG_AUTO_OPER__LMP_PARKING,
      enOUT_BUZZER,//COG_AUTO_OPER__BUZZER,
      enOUT_CHIME,//COG_AUTO_OPER__CHIME,
      enOUT_CAR_TO_LOBBY,//COG_AUTO_OPER__CAR_TO_LOBBY,
      enOUT_IN_SERVICE,//COG_AUTO_OPER__IN_SERV,
      enOUT_Overload,//COG_AUTO_OPER__OVERLOAD,
      enOUT_LMP_EMS,//COG_AUTO_OPER__LMP_EMS,
      enOUT_LMP_ATTD_ABOVE,//COG_AUTO_OPER__LMP_ATTD_ABOVE,
      enOUT_LMP_ATTD_BELOW,//COG_AUTO_OPER__LMP_ATTD_BELOW,
      enOUT_LMP_INDP_SRV,//COG_AUTO_OPER__LMP_INDP_SRV
      enOUT_IN_USE,//COG_AUTO_OPER__IN_USE,
      enOUT_ACCELERATING,//COG_AUTO_OPER__ACCELERATING,
      enOUT_DECELERATING,//COG_AUTO_OPER__DECELERATING,
      enOUT_LMP_SABBATH, //COG_AUTO_OPER__LMP_SABBATH,
      enOUT_At_Recall,//COG_AUTO_OPER__AT_RECALL,
      enOUT_ARV_UP_1,//COG_AUTO_OPER__ARV_UP_1,
      enOUT_ARV_DN_1,//COG_AUTO_OPER__ARV_DN_1,
      enOUT_ARV_UP_2,//COG_AUTO_OPER__ARV_UP_2,
      enOUT_ARV_DN_2,//COG_AUTO_OPER__ARV_DN_2,
      enOUT_ARV_UP_3,//COG_AUTO_OPER__ARV_UP_3,
      enOUT_ARV_DN_3,//COG_AUTO_OPER__ARV_DN_3,
      enOUT_ARV_UP_4,//COG_AUTO_OPER__ARV_UP_4,
      enOUT_ARV_DN_4,//COG_AUTO_OPER__ARV_DN_4,
      enOUT_ARV_UP_5,//COG_AUTO_OPER__ARV_UP_5,
      enOUT_ARV_DN_5,//COG_AUTO_OPER__ARV_DN_5,
      enOUT_DISTRESS_LAMP, //COG_AUTO_OPER__DISTRESS_LAMP,
      enOUT_DISTRESS_BUZZER, //COG_AUTO_OPER__DISTRESS_BUZZER,
      enOUT_CC_ACKNOWLEDGE, //COG_AUTO_OPER__CC_ACKNOWLEDGE,
};
static const char * const pasOutputStrings_AutoOper[ NUM_COG_AUTO_OPER ] =
{
      "Travel Up",//COG_AUTO_OPER__TRV_UP,
      "Travel Dn",//COG_AUTO_OPER__TRV_DN,
      "UNUSED",//COG_AUTO_OPER__UNUSED2,
      "Lamp Parking",//COG_AUTO_OPER__LMP_PARKING,
      "Buzzer",//COG_AUTO_OPER__BUZZER,
      "Chime",//COG_AUTO_OPER__CHIME,
      "Car To Lobby",//COG_AUTO_OPER__CAR_TO_LOBBY,
      "In Service",//COG_AUTO_OPER__IN_SERV,
      "Overloaded",//COG_AUTO_OPER__OVERLOAD,
      "Lamp EMS",//COG_AUTO_OPER__LMP_EMS,
      "Lamp Attd Above",//COG_AUTO_OPER__LMP_ATTD_ABOVE,
      "Lamp Attd Below",//COG_AUTO_OPER__LMP_ATTD_BELOW,
      "Lamp Indp Srv",//COG_AUTO_OPER__LMP_INDP_SRV
      "In Use",//COG_AUTO_OPER__IN_USE,
      "Accelerating",//COG_AUTO_OPER__ACCELERATING,
      "Decelerating",//COG_AUTO_OPER__DECELERATING,
      "Lamp Sabbath",//COG_AUTO_OPER__LMP_SABBATH
      "Lamp At Recall",//COG_AUTO_OPER__AT_RECALL,
      "Arrival UP 1",//COG_AUTO_OPER__ARV_UP_1,
      "Arrival DN 1",//COG_AUTO_OPER__ARV_DN_1,
      "Arrival UP 2",//COG_AUTO_OPER__ARV_UP_2,
      "Arrival DN 2",//COG_AUTO_OPER__ARV_DN_2,
      "Arrival UP 3",//COG_AUTO_OPER__ARV_UP_3,
      "Arrival DN 3",//COG_AUTO_OPER__ARV_DN_3,
      "Arrival UP 4",//COG_AUTO_OPER__ARV_UP_4,
      "Arrival DN 4",//COG_AUTO_OPER__ARV_DN_4,
      "Arrival UP 5",//COG_AUTO_OPER__ARV_UP_5,
      "Arrival DN 5",//COG_AUTO_OPER__ARV_DN_5,
      "Distress Lamp", //COG_AUTO_OPER__DISTRESS_LAMP,
      "Distress Buzzer", //COG_AUTO_OPER__DISTRESS_BUZZER,
      "CC Acknowledge", //COG_AUTO_OPER__CC_ACKNOWLEDGE,
};
//-----------------------------------------------
static const enum en_output_functions aeCOG_Mapping_DoorFront[NUM_COG_DOORS] =
{
      enOUT_DO_F,//COG_DOORS__DO,
      enOUT_DC_F,//COG_DOORS__DC,
      enOUT_NDG_F,//COG_DOORS__NDG,
      enOUT_GATE_RELEASE_F,//COG_DOORS__GATE_RELEASE,
      enOUT_DCP_F,//COG_DOORS__DCP,
      enOUT_CAM_F,//COG_DOORS__CAM
      enOUT_DOL_F,//COG_DOORS__DOL
      enOUT_DOOR_RESTRICTOR_F,//COG_DOORS__RESTRICTOR
      enOUT_ARV_UP_F,//COG_DOORS__ARV_UP,
      enOUT_ARV_DN_F,//COG_DOORS__ARV_DN,
      enOUT_DoorHold_F,//COG_DOORS__HOLD,

      enOUT_FreightDoor_Buzzer_F, //COG_DOORS__BUZZER
      enOUT_FreightDoor_DCM_F, //COG_DOORS__DCM
      enOUT_FreightDoor_Test_F, //COG_DOORS__TEST

};

//-----------------------------------------------
static const uint16_t aeCOG_Mapping_DoorRear[NUM_COG_DOORS] =
{
      enOUT_DO_R,//COG_DOORS__DO,
      enOUT_DC_R,//COG_DOORS__DC,
      enOUT_NDG_R,//COG_DOORS__NDG,
      enOUT_GATE_RELEASE_R,//COG_DOORS__GATE_RELEASE,
      enOUT_DCP_R,//COG_DOORS__DCP,
      enOUT_CAM_R,//COG_DOORS__CAM
      enOUT_DOL_R,//COG_DOORS__DOL
      enOUT_DOOR_RESTRICTOR_R,//COG_DOORS__RESTRICTOR
      enOUT_ARV_UP_R,//COG_DOORS__ARV_UP,
      enOUT_ARV_DN_R,//COG_DOORS__ARV_DN,
      enOUT_DoorHold_R,//COG_DOORS__HOLD,

      enOUT_FreightDoor_Buzzer_R, //COG_DOORS__BUZZER
      enOUT_FreightDoor_DCM_R, //COG_DOORS__DCM
      enOUT_FreightDoor_Test_R, //COG_DOORS__TEST

};
static const char * const pasOutputStrings_Doors[ NUM_COG_DOORS ] =
{
      "DO",//COG_DOORS__DO,
      "DC",//COG_DOORS__DC,
      "NDG",//COG_DOORS__NDG,
      "Gate Release",//COG_DOORS__GATE_RELEASE,
      "DCP",//COG_DOORS__DCP,
      "CAM",//COG_DOORS__CAM
      "DOL",//COG_DOORS__DOL
      "Restrictor",//COG_DOORS__RESTRICTOR
      "Arrival Up",//COG_DOORS__ARV_UP,
      "Arrival Down",//COG_DOORS__ARV_DN,
      "Hold Lamp",//COG_DOORS__HOLD,
      "Warning Buzzer", //COG_DOORS__BUZZER
      "DCM", //COG_DOORS__DCM
      "Test", //COG_DOORS__TEST

};
//-----------------------------------------------
static const enum en_output_functions aeCOG_Mapping_FireEQ[NUM_COG_FIRE_EQ] =
{
      enOUT_LMP_FIRE,//COG_FIRE_EQ__LMP_FIRE,
      enOUT_LMP_EQ,//COG_FIRE_EQ__LMP_EQ,
      enOUT_FIRE_LOBBY_LAMP,//COG_FIRE_EQ__LMP_FIRE_LOBBY,
      enOUT_FIRE_I_ACTIVE,//COG_FIRE_EQ__FIRE_I_ACTIVE,
      enOUT_FIRE_SHUNT,//COG_FIRE_EQ__FIRE_SHUNT,
      enOUT_FIRE_II_ACTIVE, //COG_FIRE_EQ__FIRE_II_ACTIVE,
      enOUT_FIRE_II_HOLD, //COG_FIRE_EQ__FIRE_II_HOLD,
      enOUT_EQ_SLOW_LAMP, //COG_EQ_LAMP,
      enOUT_LMP_SEISMIC_STATUS, //COG_LMP_SEISMIC_STATUS
};
static const char * const pasOutputStrings_FireEQ[ NUM_COG_FIRE_EQ ] =
{
      "Lamp Fire",//COG_FIRE_EQ__LMP_FIRE,
      "Lamp EQ",//COG_FIRE_EQ__LMP_EQ,
      "Lamp Fire Lobby",//COG_FIRE_EQ__LMP_FIRE_LOBBY,
      "Fire I Active",//COG_FIRE_EQ__FIRE_I_ACTIVE,
      "Fire Shunt",//COG_FIRE_EQ__FIRE_SHUNT,
      "Fire II Active", //COG_FIRE_EQ__FIRE_II_ACTIVE
      "Fire II Hold", //COG_FIRE_EQ__FIRE_II_HOLD
      "EQ Slow Lamp", //COG_EQ_LAMP
      "Lamp Seismic Status", //COG_LMP_SEISMIC_STATUS
};
//-----------------------------------------------
static const enum en_output_functions aeCOG_Mapping_Insp[NUM_COG_INSP] =
{
      enOUT_LMP_INSP,//COG_INSP__LMP_INSP,
};
static const char * const pasOutputStrings_Insp[ NUM_COG_INSP ] =
{
      "Lamp Insp",//COG_INSP__LMP_INSP,
};
//-----------------------------------------------
static const enum en_output_functions aeCOG_Mapping_Ctrl[NUM_COG_CTRL] =
{
      enOUT_LIGHTFAN,//COG_CTRL__LIGHT_FAN,
      enOUT_REGEN_ENABLE,//COG_CTRL__REGEN_EN,
      enOUT_REGEN_RESET,//COG_CTRL__REGEN_RST,
      enOUT_DRV_HW_ENABLE,//COG_CTRL__DRV_HW_ENABLE,
      enOUT_RecTrvDir,//COG_CTRL__REC_TRV_ENABLE,
      enOUT_BatteryPwr,//COG_CTRL__BATTERY_PWR,
      enOUT_SAFETY_RESCUE,//COG_CTRL__SAFETY_RESCUE,
      enOUT_AUTO_RESCUE,//COG_CTRL__AUTO_RESCUE,
      enOUT_MR_FAN,//COG_CTRL__MR_FAN,
      enOUT_START_MOTOR,//COG_CTRL__START_MOTOR,
      enOUT_VALVE_INSP,//COG_CTRL__VALVE_INSP,
      enOUT_VALVE_HIGH_UP,//COG_CTRL__VALVE_HIGH_UP,
      enOUT_VALVE_HIGH_DOWN,//COG_CTRL__VALVE_HIGH_DOWN,
      enOUT_VALVE_MED_UP,//COG_CTRL__VALVE_MID_UP,
      enOUT_VALVE_MED_DOWN,//COG_CTRL__VALVE_MID_DOWN,
      enOUT_VALVE_LOW_UP,//COG_CTRL__VALVE_LOW_UP,
      enOUT_VALVE_LOW_DOWN,//COG_CTRL__VALVE_LOW_DOWN,
      enOUT_VALVE_LEVEL_UP,//COG_CTRL__VALVE_LEVEL_UP,
      enOUT_VALVE_LEVEL_DOWN,//COG_CTRL__VALVE_LEVEL_DOWN,
      enOUT_BRAKE1_PICK,//COG_CTRL__BRAKE1_PICK,
      enOUT_BRAKE2_PICK,//COG_CTRL__BRAKE2_PICK,
};
static const char * const pasOutputStrings_Ctrl[ NUM_COG_CTRL ] =
{
      "Light Fan",//COG_CTRL__LIGHT_FAN,
      "Regen Enable",//COG_CTRL__REGEN_EN,
      "Regen Reset",//COG_CTRL__REGEN_RST,
      "Drive HW Enable",//COG_CTRL__DRV_HW_ENABLE,
      "Rec Trv Enable",//COG_CTRL__REC_TRV_ENABLE,
      "Battery Pwr",//COG_CTRL__BATTERY_PWR,
      "Safety Rescue",//COG_CTRL__SAFETY_RESCUE,
      "Auto Rescue",//COG_CTRL__AUTO_RESCUE,
      "MR Fan",//COG_CTRL__MR_FAN,
      "SM",//COG_CTRL__START_MOTOR,
      "Valve Insp",//COG_CTRL__VALVE_INSP,
      "Valve High Up",//COG_CTRL__VALVE_HIGH_UP,
      "Valve High Down",//COG_CTRL__VALVE_HIGH_DOWN,
      "Valve Mid Up",//COG_CTRL__VALVE_MID_UP,
      "Valve Mid Down",//COG_CTRL__VALVE_MID_DOWN,
      "Valve Low Up",//COG_CTRL__VALVE_LOW_UP,
      "Valve Low Down",//COG_CTRL__VALVE_LOW_DOWN,
      "Valve Level Up",//COG_CTRL__VALVE_LEVEL_UP,
      "Valve Level Down",//COG_CTRL__VALVE_LEVEL_DOWN,
      "Brake1 Pick",//COG_CTRL__BRAKE1_PICK,
      "Brake2 Pick",//COG_CTRL__BRAKE2_PICK,
};

//-----------------------------------------------
static const enum en_output_functions aeCOG_Mapping_Safety[NUM_COG_SAFETY] =
{
      enOUT_LMP_FLOOD,//COG_SAFETY__LMP_FLOOD,
      enOUT_PhoneFailLamp,//COG_SAFETY__PHONE_FAIL_LAMP,
      enOUT_PhoneFailBuzzer,//COG_SAFETY__PHONE_FAIL_BUZZER,
};
static const char * const pasOutputStrings_Safety[ NUM_COG_SAFETY ] =
{
      "Lamp Flood",//COG_SAFETY__LMP_FLOOD,
      "Phone Fail Lamp",//COG_SAFETY__PHONE_FAIL_LAMP,
      "Phone Fail Buzzer",//COG_SAFETY__PHONE_FAIL_BUZZER,
};
//-----------------------------------------------
static const enum en_output_functions aeCOG_Mapping_EPwr[ NUM_COG_EPWR ] =
{
      enOUT_LMP_EPWR,//COG_EPWR__LMP_EPWR,
      enOUT_LMP_Select1,//COG_EPWR__LMP_SELECT1,
      enOUT_LMP_Select2,//COG_EPWR__LMP_SELECT2,
      enOUT_LMP_Select3,//COG_EPWR__LMP_SELECT3,
      enOUT_LMP_Select4,//COG_EPWR__LMP_SELECT4,
      enOUT_LMP_Select5,//COG_EPWR__LMP_SELECT5,
      enOUT_LMP_Select6,//COG_EPWR__LMP_SELECT6,
      enOUT_LMP_Select7,//COG_EPWR__LMP_SELECT7,
      enOUT_LMP_Select8,//COG_EPWR__LMP_SELECT8,
};
static const char * const pasOutputStrings_EPwr[ NUM_COG_EPWR ] =
{
      "Lamp On EP",//COG_EPWR__LMP_EPWR,
      "Select 1",//COG_EPWR__LMP_SELECT1,
      "Select 2",//COG_EPWR__LMP_SELECT2,
      "Select 3",//COG_EPWR__LMP_SELECT3,
      "Select 4",//COG_EPWR__LMP_SELECT4,
      "Select 5",//COG_EPWR__LMP_SELECT5,
      "Select 6",//COG_EPWR__LMP_SELECT6,
      "Select 7",//COG_EPWR__LMP_SELECT7,
      "Select 8",//COG_EPWR__LMP_SELECT8,
};
/*----------------------------------------------------------------------------
   Returns index into system input bitmap, from an input in a specified input group
 *----------------------------------------------------------------------------*/
enum en_output_functions SysIO_GetOutputFunctionFromCOG( enum_output_groups eCOG, uint8_t ucIndex )
{
   enum en_output_functions eOutput = UNUSED_OUTPUT;
   switch( eCOG )
   {
      case OUTPUT_GROUP__AUTO_OPER:
         if(ucIndex < NUM_COG_AUTO_OPER)
         {
            eOutput = aeCOG_Mapping_AutoOper[ucIndex];
         }
         break;
      case OUTPUT_GROUP__DOORS_F:
         if(ucIndex < NUM_COG_DOORS)
         {
            eOutput = aeCOG_Mapping_DoorFront[ucIndex];
         }
         break;
      case OUTPUT_GROUP__DOORS_R:
         if(ucIndex < NUM_COG_DOORS)
         {
            eOutput = aeCOG_Mapping_DoorRear[ucIndex];
         }
         break;
      case OUTPUT_GROUP__FIRE_EQ:
         if(ucIndex < NUM_COG_FIRE_EQ)
         {
            eOutput = aeCOG_Mapping_FireEQ[ucIndex];
         }
         break;
      case OUTPUT_GROUP__INSP:
         if(ucIndex < NUM_COG_INSP)
         {
            eOutput = aeCOG_Mapping_Insp[ucIndex];
         }
         break;
      case OUTPUT_GROUP__CTRL:
         if(ucIndex < NUM_COG_CTRL)
         {
            eOutput = aeCOG_Mapping_Ctrl[ucIndex];
         }
         break;
      case OUTPUT_GROUP__SAFETY:
         if(ucIndex < NUM_COG_SAFETY)
         {
            eOutput = aeCOG_Mapping_Safety[ucIndex];
         }
         break;
      case OUTPUT_GROUP__EPWR:
         if(ucIndex < NUM_COG_EPWR)
         {
            eOutput = aeCOG_Mapping_EPwr[ucIndex];
         }
         break;
      default:
         break;
   }
   return eOutput;
}
/*----------------------------------------------------------------------------
  Returns functions per input group
 *----------------------------------------------------------------------------*/
uint8_t SysIO_GetSizeOfCOG( enum_output_groups eCOG )
{
   uint8_t ucSize = ( eCOG < NUM_OUTPUT_GROUPS ) ? aucFunctionsPerOutputGroup[eCOG]:1;
   return ucSize;
}

/*----------------------------------------------------------------------------
  Get string for COG type
 *----------------------------------------------------------------------------*/
const char * SysIO_GetOutputGroupString( enum_output_groups eCOG )
{
   return pasStrings_COG[eCOG];
}
/*----------------------------------------------------------------------------
  Get string for input for a specific COG
 *----------------------------------------------------------------------------*/
const char * SysIO_GetOutputGroupString_ByFunctionIndex( enum_output_groups eCOG, uint8_t ucIndex )
{
   if( ucIndex >= SysIO_GetSizeOfCOG( eCOG ) )
   {
      return pasStrings_COG[OUTPUT_GROUP__NONE];
   }
   switch( eCOG )
   {
      case OUTPUT_GROUP__AUTO_OPER:
         return pasOutputStrings_AutoOper[ucIndex];
         break;
      case OUTPUT_GROUP__DOORS_F:
      case OUTPUT_GROUP__DOORS_R:
         return pasOutputStrings_Doors[ucIndex];
         break;
      case OUTPUT_GROUP__FIRE_EQ:
         return pasOutputStrings_FireEQ[ucIndex];
         break;
      case OUTPUT_GROUP__INSP:
         return pasOutputStrings_Insp[ucIndex];
         break;
      case OUTPUT_GROUP__CTRL:
         return pasOutputStrings_Ctrl[ucIndex];
         break;
      case OUTPUT_GROUP__SAFETY:
         return pasOutputStrings_Safety[ucIndex];
         break;
      case OUTPUT_GROUP__EPWR:
         return pasOutputStrings_EPwr[ucIndex];
         break;
      default:
         return pasStrings_COG[OUTPUT_GROUP__NONE];
         break;
   }
}
