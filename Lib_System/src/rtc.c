/******************************************************************************
 *
 * @file     rtc.c
 * @brief
 * @version  V1.00
 * @date     23, March 2016
 *
 * @note     These functions support setting and accessing time from
 *           the MR SRU RTC peripheral. This peripheral is available
 *           on MR SRUs version SR-3032m4 and on.
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "sys.h"
#include <time.h>

#define MCUB_RTC (LPC_RTC)

#define SECONDS_IN_MINUTE (60)
#define SECONDS_IN_HOUR (3600)
#define SECONDS_IN_DAY (86400)
#define SECONDS_IN_YEAR (3155760)
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   @fn void RTC_InitPeripheral( void )
   @return none
   @brief Initializes the chip's RTC peripheral
 *----------------------------------------------------------------------------*/
void RTC_InitPeripheral( void )
{
   Chip_RTC_Init(MCUB_RTC);
   Chip_RTC_Enable(MCUB_RTC, ENABLE);
}
/*----------------------------------------------------------------------------
   @fn static void RTC_SetFullTime(RTC_TIME_T* pstFullTime)
   @return none
   @brief Writes the time to the chip's RTC peripheral with a time structure as the argument
 *----------------------------------------------------------------------------*/
static void RTC_SetFullTime(RTC_TIME_T* pstFullTime)
{
   Chip_RTC_SetFullTime(MCUB_RTC, pstFullTime);
}
/*----------------------------------------------------------------------------
   @fn static void RTC_GetFullTime(RTC_TIME_T* pstFullTime)
   @return Pointer to structure RTC_TIME_T that contains the current time
   @brief Reads the current time from the chip's RTC peripheral
 *----------------------------------------------------------------------------*/
static void RTC_GetFullTime(RTC_TIME_T* pstFullTime)
{
   Chip_RTC_GetFullTime(MCUB_RTC, pstFullTime);
}
/*----------------------------------------------------------------------------
   @fn void RTC_WritePeripheralTime_Unix( uint32_t ulUnixTime )
   @return none
   @brief Writes the time to the chip's RTC peripheral with a unix time argument
 *----------------------------------------------------------------------------*/
void RTC_WritePeripheralTime_Unix( uint32_t ulUnixTime )
{
   RTC_TIME_T stFullTime;
   time_t stRawTime = ulUnixTime;
   struct tm * stWriteGmTime;
   stWriteGmTime = localtime( &stRawTime );

   stFullTime.time[RTC_TIMETYPE_YEAR] = stWriteGmTime->tm_year + 1900;
   stFullTime.time[RTC_TIMETYPE_DAYOFYEAR] = stWriteGmTime->tm_yday;
   stFullTime.time[RTC_TIMETYPE_MONTH] = stWriteGmTime->tm_mon + 1;
   stFullTime.time[RTC_TIMETYPE_DAYOFMONTH] = stWriteGmTime->tm_mday ;
   stFullTime.time[RTC_TIMETYPE_DAYOFWEEK] = stWriteGmTime->tm_wday ;
   stFullTime.time[RTC_TIMETYPE_HOUR] = stWriteGmTime->tm_hour ;
   stFullTime.time[RTC_TIMETYPE_MINUTE] = stWriteGmTime->tm_min ;
   stFullTime.time[RTC_TIMETYPE_SECOND] = stWriteGmTime->tm_sec ;

   RTC_SetFullTime(&stFullTime);
}
/*----------------------------------------------------------------------------
   @fn uint32_t RTC_ReadPeripheralTime_Unix(void)
   @return Unix time currently stored on chip's RTC peripheral
   @brief Reads the time on the chip's RTC peripheral
 *----------------------------------------------------------------------------*/
uint32_t RTC_ReadPeripheralTime_Unix(void)
{
   uint32_t uiUnixTime;
   RTC_TIME_T stPeripheralTimeStruct;
   struct tm stLibraryTimeStruct;
   RTC_GetFullTime(&stPeripheralTimeStruct);
   // Library uses years since 1900
//   uint32_t uiYear = ( stPeripheralTimeStruct.time[RTC_TIMETYPE_YEAR] > 1900 )
//                   ? ( stPeripheralTimeStruct.time[RTC_TIMETYPE_YEAR] - 1900 )
//                   : ( 0 );
//   stLibraryTimeStruct.tm_year = uiYear;
   stLibraryTimeStruct.tm_isdst = -1;
   stLibraryTimeStruct.tm_year = stPeripheralTimeStruct.time[RTC_TIMETYPE_YEAR] - 1900;
   stLibraryTimeStruct.tm_yday = stPeripheralTimeStruct.time[RTC_TIMETYPE_DAYOFYEAR];
   stLibraryTimeStruct.tm_wday = stPeripheralTimeStruct.time[RTC_TIMETYPE_DAYOFWEEK];
   stLibraryTimeStruct.tm_mon = stPeripheralTimeStruct.time[RTC_TIMETYPE_MONTH] - 1;
   stLibraryTimeStruct.tm_mday = stPeripheralTimeStruct.time[RTC_TIMETYPE_DAYOFMONTH];
   stLibraryTimeStruct.tm_hour = stPeripheralTimeStruct.time[RTC_TIMETYPE_HOUR];
   stLibraryTimeStruct.tm_min = stPeripheralTimeStruct.time[RTC_TIMETYPE_MINUTE];
   stLibraryTimeStruct.tm_sec = stPeripheralTimeStruct.time[RTC_TIMETYPE_SECOND];
   uiUnixTime = mktime(&stLibraryTimeStruct);
   return uiUnixTime;
}
uint32_t RTC_GetFullTime_Unix( void )
{
   uint32_t ulUnixTime;
   RTC_TIME_T pstFullTime;
   RTC_GetFullTime(&pstFullTime);

   ulUnixTime = pstFullTime.time[RTC_TIMETYPE_SECOND];
   ulUnixTime += (pstFullTime.time[RTC_TIMETYPE_MINUTE] * SECONDS_IN_MINUTE);
   ulUnixTime += (pstFullTime.time[RTC_TIMETYPE_HOUR] * SECONDS_IN_HOUR);
   ulUnixTime += (pstFullTime.time[RTC_TIMETYPE_DAYOFYEAR] * SECONDS_IN_DAY);
   ulUnixTime += ((pstFullTime.time[RTC_TIMETYPE_YEAR] - 1970) * SECONDS_IN_YEAR);

   return ulUnixTime;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
