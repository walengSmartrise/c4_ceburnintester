/******************************************************************************
 *
 * @file     shared_data.c
 * @brief    Bit manipulation functions
 * @version  V1.00
 * @date     26, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "sys.h"

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
uint8_t SDATA_CompareDatagrams( un_sdata_datagram *punDG1,
                                un_sdata_datagram *punDG2 )
{
   uint8_t bDiff = 0;
   for(uint8_t i = 0; i < 2; i++)
   {
      bDiff |= punDG1->aui32[i] != punDG2->aui32[i];
   }
   return bDiff;
}
/*-----------------------------------------------------------------------------
   Set or clear the dirty bit
 -----------------------------------------------------------------------------*/
static void SDATA_DirtyBit_Write( struct st_sdata_control *pstSData_Control,
                                  uint32_t uiDatagramIndex,
                                  uint8_t bSet
                                )
{
   if ( pstSData_Control )
   {
      if ( uiDatagramIndex < pstSData_Control->uiNumDatagrams )
      {
         if ( pstSData_Control->paucDatagramDirtyBits )
         {
            Sys_Bit_Set( pstSData_Control->paucDatagramDirtyBits,
                         uiDatagramIndex,
                         bSet
                       );
         }
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
int SDATA_DirtyBit_Get( struct st_sdata_control *pstSData_Control,
                        uint32_t uiDatagramIndex
                      )
{

	int iDirtyBit = 0;

   if ( pstSData_Control )
   {
      if ( uiDatagramIndex < pstSData_Control->uiNumDatagrams )
      {
         if ( pstSData_Control->paucDatagramDirtyBits )
         {
            iDirtyBit = Sys_Bit_Get( pstSData_Control->paucDatagramDirtyBits,
                                     uiDatagramIndex
                                   );
         }
      }
   }

   return iDirtyBit;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SDATA_DirtyBit_Set( struct st_sdata_control *pstSData_Control,
                         uint32_t uiDatagramIndex
                      )
{
   SDATA_DirtyBit_Write( pstSData_Control, uiDatagramIndex, 1 );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SDATA_DirtyBit_Clr( struct st_sdata_control *pstSData_Control,
                         uint32_t uiDatagramIndex
                      )
{
   SDATA_DirtyBit_Write( pstSData_Control, uiDatagramIndex, 0 );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
int SDATA_ReadDatagram( struct st_sdata_control *pstSData_Control,
                        uint32_t uiDatagramIndex,
                        un_sdata_datagram *punDatagram
                      )
{
   int iReturnValue = 1; // assume error

   if ( pstSData_Control )
   {
      if ( uiDatagramIndex < pstSData_Control->uiNumDatagrams )
      {
         if ( pstSData_Control->paunDatagrams )
         {
            memcpy( punDatagram,
                    pstSData_Control->paunDatagrams + uiDatagramIndex,
                    sizeof(un_sdata_datagram)
                  );

            iReturnValue = 0;
         }
      }
   }

   return iReturnValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
int SDATA_WriteDatagram( struct st_sdata_control *pstSData_Control,
                         uint32_t uiDatagramIndex,
                         un_sdata_datagram *punDatagram
                       )
{
   int iReturnValue = 1; // assume error

   if ( pstSData_Control )
   {
      if ( uiDatagramIndex < pstSData_Control->uiNumDatagrams )
      {
         if ( pstSData_Control->paunDatagrams )
         {
            //---------------------------------------
            // Compare the data being written with the data currently in the
            // datagram and only update the datagram if the new data is
            // different.
            int bDiff = (int) SDATA_CompareDatagrams(punDatagram,pstSData_Control->paunDatagrams + uiDatagramIndex);
            if ( bDiff )
            {
               //---------------------------------------
               // New data is different so update the datagram and set the dirty
               // bit.
               memcpy( pstSData_Control->paunDatagrams + uiDatagramIndex,punDatagram,sizeof(un_sdata_datagram)
                     );
               SDATA_DirtyBit_Set( pstSData_Control, uiDatagramIndex );
            }

            iReturnValue = 0;
         }
      }
   }

   return iReturnValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
int SDATA_CreateDatagrams( struct st_sdata_control *pstSData_Control,
                           uint32_t uiNumDatagrams
                         )
{
   int iReturnValue = 1; // assume error

   if ( pstSData_Control )
   {
      //---------------------------------------
      // Allocate and initialize to zero the datagrams.
      pstSData_Control->paunDatagrams = calloc( uiNumDatagrams,
                                                   sizeof(un_sdata_datagram)
                                                 );

      if ( pstSData_Control->paunDatagrams )
      {
         //---------------------------------------
         // Allocate and initialize to zero the dirty bits.
         int iNumDirtyBitBytes = BITMAP8_SIZE(uiNumDatagrams);

         pstSData_Control->paucDatagramDirtyBits = calloc( iNumDirtyBitBytes,
                                                              sizeof( uint8_t )
                                                            );
         //-----------------------------------------
         pstSData_Control->paiLastSendCount_1ms = calloc( uiNumDatagrams, sizeof( int32_t ) );
         pstSData_Control->paiResendRate_1ms = calloc( uiNumDatagrams, sizeof( int32_t ) );
         for(uint8_t i = 0; i < uiNumDatagrams; i++)
         {
            int32_t *piResendRate_1ms = pstSData_Control->paiResendRate_1ms + i;
            *piResendRate_1ms = DEFAULT_DATAGRAM_RESEND_RATE_1MS;
         }
         pstSData_Control->bDisableResend = 0; // set to 0 by default
         //-----------------------------------------
         pstSData_Control->uwHeartbeatInterval_1ms = 65535;
         pstSData_Control->bDisableHeartbeat = 0;

         pstSData_Control->iLastCountHB_1ms = 0;
         //-----------------------------------------
         if ( pstSData_Control->paucDatagramDirtyBits )
         {
            pstSData_Control->uiNumDatagrams = uiNumDatagrams;

            iReturnValue = 0;
         }
         else
         {
            // This should never happen.
            free( pstSData_Control->paunDatagrams );
         }
      }
   }

   return iReturnValue;
}
/*-----------------------------------------------------------------------------
   Returns index of next valid datagram to send
 -----------------------------------------------------------------------------*/
static uint16_t SDATA_GetNextHeartbeatDatagram( struct st_sdata_control *pstSData_Control )
{
   uint16_t uwDatagramIndex = (pstSData_Control->uwLastSentIndex + 1)
                             % pstSData_Control->uiNumDatagrams;
   for ( uint16_t i = 0; i < pstSData_Control->uiNumDatagrams; ++i )
   {
      uint16_t uwDatagramToCheck_Index = (pstSData_Control->uwLastSentIndex + i + 1)
                                       % pstSData_Control->uiNumDatagrams;
      int32_t *piResendRate = pstSData_Control->paiResendRate_1ms + uwDatagramToCheck_Index;
      /* Select the first datagram without a disabled status */
      if( *piResendRate != DISABLED_DATAGRAM_RESEND_RATE_MS )
      {
         uwDatagramIndex = uwDatagramToCheck_Index;
         break;
      }
   }

   return uwDatagramIndex;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint16_t SDATA_GetNextDatagramToSendIndex_Plus1( struct st_sdata_control *pstSData_Control )
{
   //------------------------------------------------
   // When considering which datagram to send next, always start with the one
   // right after the one that was previously sent. (NOTE: It gets incremented
   // below.)
   uint_fast16_t uwDatagramToCheck_Index = 0;
   uint_fast16_t uwDatagramToSend_Plus1 = 0;
   uint16_t uwLastSendIndex = pstSData_Control->uwLastSentIndex;
   //------------------------------------------------
   // By looping through all the datagrams, we'll either find one with it's
   // dirty bit set or we'll encounter the next round robin datagram and send
   // it.
   for ( uint16_t i = 0; i < pstSData_Control->uiNumDatagrams; ++i )
   {
      //------------------------------------------------
      // Increment to the next datagram, wrapping if necessary.
      uwDatagramToCheck_Index = (uwLastSendIndex + i + 1)
            % pstSData_Control->uiNumDatagrams;


      //------------------------------------------------
      //Check for resend timeout
      int32_t iLastSendCount = pstSData_Control->paiLastSendCount_1ms[ uwDatagramToCheck_Index ];
      int32_t iCountDiff = -1 * Sys_TickCount_Compare(
            iLastSendCount,
            enTickCount_1ms );
      int32_t *piResendRate = pstSData_Control->paiResendRate_1ms + uwDatagramToCheck_Index;
      uint8_t bDisableResend = *piResendRate == DISABLED_DATAGRAM_RESEND_RATE_MS;
      //------------------------------------------------
      int bDirtyBit = SDATA_DirtyBit_Get( pstSData_Control, uwDatagramToCheck_Index );

      if ( bDirtyBit != 0 )
      {
         uwDatagramToSend_Plus1 = uwDatagramToCheck_Index + 1;
         break;
      }
      else if( ( !pstSData_Control->bDisableResend ) // For all datagram
            && ( !bDisableResend ) // For individual datagram
            && ( iCountDiff >= *piResendRate ) )
      {
         uwDatagramToSend_Plus1 = uwDatagramToCheck_Index + 1;
         break;
      }
   }

   //-----------------------------------------------------------------
   // If theres no DG to send, check for HB
   if(!pstSData_Control->bDisableHeartbeat
   && !uwDatagramToSend_Plus1)
   {
      int32_t iCountSinceLastSend_1ms = Sys_TickCount_Compare(pstSData_Control->iLastCountHB_1ms,
                                                              enTickCount_1ms);

      iCountSinceLastSend_1ms = (iCountSinceLastSend_1ms > 0)
                                ? 0 : (-1*iCountSinceLastSend_1ms);

      uint8_t bNeedToSend = iCountSinceLastSend_1ms >= ( (int32_t) pstSData_Control->uwHeartbeatInterval_1ms );

      if( bNeedToSend )
      {
         uint16_t uwDatagramToSend = SDATA_GetNextHeartbeatDatagram(pstSData_Control);
         uwDatagramToSend_Plus1 = uwDatagramToSend + 1;
      }
   }
   //-----------------------------------------------------------------
   // Update last send index, count, and HB variables.
   if( uwDatagramToSend_Plus1 )
   {
      uint16_t uwIndex = uwDatagramToSend_Plus1 - 1;
      pstSData_Control->uwLastSentIndex = uwIndex;
      pstSData_Control->paiLastSendCount_1ms[ uwIndex ] =
                                          Sys_GetTickCount(enTickCount_1ms);
      pstSData_Control->iLastCountHB_1ms = Sys_GetTickCount(enTickCount_1ms);
      SDATA_DirtyBit_Clr( pstSData_Control, uwIndex );
   }
   return uwDatagramToSend_Plus1;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
