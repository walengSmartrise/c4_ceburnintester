/******************************************************************************
 *
 * @file     main.c
 * @brief    Program start point and main loop.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "datagrams.h"
#include "shared_data.h"
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define        CAN_MONITOR_WINDOW_SIZE       (1000000U)
#define        CALL_RATE_WINDOW_SIZE  (20)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint32_t uiNoTXCount_C1;
static uint32_t uiNoTXCount_C2;

static uint32_t ulCycleCount;
static uint32_t ulSendCount_C1;//buffer filled
static uint32_t ulSendCount_C2;
static float fSendRatio_C1;
static float fSendRatio_C2;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
   Place in an area of code and debug at "ucIndex = 0;"
   to see the rate the code is being revisited.
 -----------------------------------------------------------------------------*/
void Debug_CallRate()
{
   static uint8_t bFirst = 1;
   static uint32_t uiLastTime_1us;
   static uint32_t uiNewTime_1us;
   static uint32_t uiDiffLog[CALL_RATE_WINDOW_SIZE];
   static uint16_t ucIndex;
   static float fMaxTime_us;
   static float fMinTime_us;
   static float fMeanTime_us;
   if(bFirst)
   {
      bFirst = 0;
      uiLastTime_1us = LPC_TIMER1->TC;
   }
   else
   {
      uint32_t uiDiffTime_1us;
      uiNewTime_1us = LPC_TIMER1->TC;
      if(uiNewTime_1us < uiLastTime_1us)
      {
         uiDiffTime_1us = 0xFFFFFFFF - uiLastTime_1us;
         uiDiffTime_1us += uiNewTime_1us;
      }
      else
      {
         uiDiffTime_1us = uiNewTime_1us - uiLastTime_1us;
      }
      uiDiffLog[ucIndex] = uiDiffTime_1us;

      uiLastTime_1us = uiNewTime_1us;
      if(++ucIndex >= CALL_RATE_WINDOW_SIZE)
      {
         fMaxTime_us = 0;
         fMinTime_us = 0;
         for( ucIndex = 0; ucIndex < CALL_RATE_WINDOW_SIZE; ucIndex++)
         {
            float fDiff = (float)uiDiffLog[ucIndex];
            if(fDiff < 100000)
            {
               fMeanTime_us+=fDiff;
            }
            else
            {
               fMeanTime_us += uiDiffLog[ucIndex+1];
            }

            if(fDiff > fMaxTime_us)
            {
               fMaxTime_us = fDiff;
            }
            if(fDiff < fMinTime_us)
            {
               fMinTime_us = fDiff;
            }
         }
         fMeanTime_us/=CALL_RATE_WINDOW_SIZE;
         ucIndex = 0;
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void DebugPackets(uint16_t uwDatagramToSend_Plus1, uint8_t ucNet)
{
   static uint32_t ulDebugPacketTransmitCount[4][255];
   ulDebugPacketTransmitCount[ucNet][uwDatagramToSend_Plus1]++;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void Debug_ViewPackets(CAN_MSG_T *pstMsg)
{
   static uint32_t ulPacketCounter[255];
   uint8_t ucDatagramID = ExtractFromCAN_DatagramID(pstMsg->ID);

   if(ucDatagramID == DATAGRAM_ID_2)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_3)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_4)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_5)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_6)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_7)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_8)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_9)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_10)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_11)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_12)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_13)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_14)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_15)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_16)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_17)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_18)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_19)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_20)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_21)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_22)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_23)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_24)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_25)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_26)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_27)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_28)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_29)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_30)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_31)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_32)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_33)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_34)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_35)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_36)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_37)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_38)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_39)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_40)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_41)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_42)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_43)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_44)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_45)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_46)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_47)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_48)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_49)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_50)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_51)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_52)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_53)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_54)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_55)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_56)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_57)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_58)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_59)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_60)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_61)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_62)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_63)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_64)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_65)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_66)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_67)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_68)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_69)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_70)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_71)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_72)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_73)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_74)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_75)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_76)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_77)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_78)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_79)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_80)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_81)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_82)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_83)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_84)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_85)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_86)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_87)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_88)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_89)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_90)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_91)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_92)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_93)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_94)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_95)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_96)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_97)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_98)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_99)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_100)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_101)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_102)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_103)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_104)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_105)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_106)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_107)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_108)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_109)
   {
      ulPacketCounter[ucDatagramID]++;
   }


   //110
   else if(ucDatagramID == DATAGRAM_ID_100)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_101)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_102)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_103)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_104)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_105)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_106)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_107)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_108)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_109)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_110)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_111)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_112)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_113)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_114)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_115)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_116)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_117)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_118)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_119)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_120)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_121)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_122)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_123)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_124)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_125)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_126)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_127)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_128)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_129)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_130)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_131)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_132)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_133)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_134)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_135)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_136)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_137)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_138)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_139)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_140)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_141)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_142)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_143)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_144)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_145)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_146)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_147)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_148)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_149)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_150)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_151)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_152)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_153)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_154)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_155)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_156)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_157)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_158)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_159)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_160)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_161)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_162)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_163)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_164)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_165)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_166)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_167)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_168)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_169)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_170)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_171)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_172)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_173)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_174)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_175)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_176)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_177)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_178)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_179)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_180)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_181)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_182)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_183)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_184)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_185)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_186)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_187)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_188)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_189)
   {
      ulPacketCounter[ucDatagramID]++;
   }

   else if(ucDatagramID == DATAGRAM_ID_190)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_191)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_192)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_193)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_194)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_195)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_196)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_197)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_198)
   {
      ulPacketCounter[ucDatagramID]++;
   }
   else if(ucDatagramID == DATAGRAM_ID_199)
   {
      ulPacketCounter[ucDatagramID]++;
   }

}
/*----------------------------------------------------------------------------

Returns percent value

 *----------------------------------------------------------------------------*/
uint8_t Debug_CAN_GetUtilization(uint8_t ucNet)
{
   if(!ucNet)
   {
      return (uint8_t) fSendRatio_C1;
   }
   else
   {
      return (uint8_t) fSendRatio_C2;
   }
}
/*----------------------------------------------------------------------------
Polls the Transmitting and Recieving CAN status registers
Percentage of time that CAN controller is sending/receiving is taken for a
This gives a rough estimate of bus activity.


To be placed in main.c in main loop.
Debug_CAN_GetUtilization
Note:
   CAN_MONITOR_WINDOW_SIZE:
      Lower to get more regular, but less accurate updates
      Increase to get less regular but more accurate updates

 *----------------------------------------------------------------------------*/
void Debug_CAN_MonitorUtilization(void)
{
#if 1
   if(ulCycleCount >= CAN_MONITOR_WINDOW_SIZE)
   {
      fSendRatio_C1 = 0;
      fSendRatio_C2 = 0;
      if(uiNoTXCount_C1 < CAN_MONITOR_WINDOW_SIZE/2)
      {
         fSendRatio_C1 = (((float)ulSendCount_C1)/((float)ulCycleCount))*100.0f;
      }
      if(uiNoTXCount_C2 < CAN_MONITOR_WINDOW_SIZE/2)
      {
         fSendRatio_C2 = (((float)ulSendCount_C2)/((float)ulCycleCount))*100.0f;
      }
      ulCycleCount = 0;
      ulSendCount_C1 = 0;
      ulSendCount_C2 = 0;
      uiNoTXCount_C1 = 0;
      uiNoTXCount_C2 = 0;
   }
   else
   {
      ulCycleCount++;
   }
   uint32_t uiStatus1 = Chip_CAN_GetGlobalStatus( LPC_CAN1 );
   /* If transmit or receive is in progress */
   uint8_t bBusBusy1 = !!( uiStatus1 & (CAN_GSR_RS | CAN_GSR_TS) );
   if(bBusBusy1)
   {
      ulSendCount_C1++;
   }

   uint32_t uiStatus2 = Chip_CAN_GetGlobalStatus( LPC_CAN2 );
   uint8_t bBusBusy2 = !!( uiStatus2 & (CAN_GSR_RS | CAN_GSR_TS) );
   if(bBusBusy2)
   {
      ulSendCount_C2++;
   }

#if 1 //After can bus reset, utilization does not reset
   /* Check for no complete transmissions */
   uiNoTXCount_C1++;
   if( CAN_SR_TCS( Chip_CAN_GetStatus(LPC_CAN1) ) )
   {
      uiNoTXCount_C1 = 0;
   }
   uiNoTXCount_C2++;
   if( CAN_SR_TCS( Chip_CAN_GetStatus(LPC_CAN2) ) )
   {
      uiNoTXCount_C2 = 0;
   }
#else
   /* Check for no complete transmissions */
   uiNoTXCount_C1++;
   if( uiStatus1 & CAN_GSR_TCS )
   {
      uiNoTXCount_C1 = 0;
   }
   uiNoTXCount_C2++;
   if( uiStatus2 & CAN_GSR_TCS )
   {
      uiNoTXCount_C2 = 0;
   }
#endif

#else
   if(ulCycleCount >= CAN_MONITOR_WINDOW_SIZE)
   {
      fSendRatio_C1 = (((float)ulSendCount_C1)/((float)ulCycleCount))*100.0f;
      fSendRatio_C2 = (((float)ulSendCount_C2)/((float)ulCycleCount))*100.0f;
      ulCycleCount = 0;
      ulSendCount_C1 = 0;
      ulSendCount_C2 = 0;
   }
   else
   {
      ulCycleCount++;
   }
   uint8_t bBufferStatus1 = !(LPC_CAN1->GSR >> 3 & 0x01) || (LPC_CAN1->GSR >> 4 & 0x01);//!(( LPC_CANCR->TxSR >> 16 ) & 1 )||( LPC_CANCR->RxSR & 1 );//tcs

   if(bBufferStatus1)
   {
      ulSendCount_C1++;
   }

   uint8_t bBufferStatus2 = !(LPC_CAN2->GSR >> 3 & 0x01) || (LPC_CAN2->GSR >> 4 & 0x01);//!(( LPC_CANCR->TxSR >> 17 ) & 1 )||( LPC_CANCR->RxSR & 2 );//tcs
   if(bBufferStatus2)
   {
      ulSendCount_C2++;
   }
#endif
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
