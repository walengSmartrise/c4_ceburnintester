/******************************************************************************
 *
 * @file     params.c
 * @brief    Parameters
 * @version  V1.00
 * @date     28, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "sys.h"
#include "param.h"
#include <stdint.h>
#include <string.h>

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_param_control gstParamControl;

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

#define RING_BUFFER_SIZE      (64)

#define EN_REQ_PARAM_BY_INDEX (0)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
// Ring buffer of can messages
// This is accessed outside of this module.
RINGBUFF_T RingBuffer_Param;

static struct st_param_and_value Buffer_Param[RING_BUFFER_SIZE];

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
enum en_buffer_states
{
   WAITING,
   FETCHING,
   SAVING,
   FIRSTRUN,
};
static uint8_t ucState = FIRSTRUN;

/*--------------------GENERATED TOP------------------------------------------*/
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_00 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT32
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_01 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT32
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_02 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT32
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_03 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT24
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_04 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT24
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_05 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT24
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_06 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT24
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_07 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT24
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_08 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT24
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_09 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT24
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_10 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT24
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_11 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT24
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_12 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT24
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_13 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT24
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_14 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_15 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_16 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_17 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_18 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_19 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_20 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_21 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_22 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_23 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_24 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_25 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_26 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_27 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_28 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_29 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_30 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_31 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_32 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_33 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_34 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_35 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_36 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_37 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_38 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_39 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_40 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_41 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_42 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_43 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_44 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_45 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_46 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_47 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_48 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_49 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_50 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_51 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_52 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_53 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT16
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_54 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT8
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_55 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT8
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_56 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT8
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_57 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT8
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_58 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__UINT8
};
//-----------------------------------------------------------------------------
static const struct st_parameter_block gstParamBlock_59 =
{
   .ueBlockType = enPARAM_BLOCK_TYPE__BIT
};
/*--------------------GENERATED BOT------------------------------------------*/
//-----------------------------------------------------------------------------
static const struct st_parameter_block * gastParameterBlocks[ NUM_PARAM_BLOCKS ] =
{
   &gstParamBlock_00,
   &gstParamBlock_01,
   &gstParamBlock_02,
   &gstParamBlock_03,
   &gstParamBlock_04,
   &gstParamBlock_05,
   &gstParamBlock_06,
   &gstParamBlock_07,
   &gstParamBlock_08,
   &gstParamBlock_09,
   &gstParamBlock_10,
   &gstParamBlock_11,
   &gstParamBlock_12,
   &gstParamBlock_13,
   &gstParamBlock_14,
   &gstParamBlock_15,
   &gstParamBlock_16,
   &gstParamBlock_17,
   &gstParamBlock_18,
   &gstParamBlock_19,
   &gstParamBlock_20,
   &gstParamBlock_21,
   &gstParamBlock_22,
   &gstParamBlock_23,
   &gstParamBlock_24,
   &gstParamBlock_25,
   &gstParamBlock_26,
   &gstParamBlock_27,
   &gstParamBlock_28,
   &gstParamBlock_29,
   &gstParamBlock_30,
   &gstParamBlock_31,
   &gstParamBlock_32,
   &gstParamBlock_33,
   &gstParamBlock_34,
   &gstParamBlock_35,
   &gstParamBlock_36,
   &gstParamBlock_37,
   &gstParamBlock_38,
   &gstParamBlock_39,
   &gstParamBlock_40,
   &gstParamBlock_41,
   &gstParamBlock_42,
   &gstParamBlock_43,
   &gstParamBlock_44,
   &gstParamBlock_45,
   &gstParamBlock_46,
   &gstParamBlock_47,
   &gstParamBlock_48,
   &gstParamBlock_49,
   &gstParamBlock_50,
   &gstParamBlock_51,
   &gstParamBlock_52,

   &gstParamBlock_53,
   &gstParamBlock_54,
   &gstParamBlock_55,
   &gstParamBlock_56,
   &gstParamBlock_57,
   &gstParamBlock_58,
   &gstParamBlock_59,
};
/*--------------------GENERATED BOT------------------------------------------*/
//-----------------------------------------------------------------------------
struct st_param_block_info gstSys_ParamBlockInfo =
{
   .ucNumBlocks = NUM_PARAM_BLOCKS,

   .ucEEPROMI_Page__Block00 = enEEPROMI_PAGE__BLOCK_00,
   .ucEEPROMI_Page__Temp = enEEPROMI_PAGE__TEMP,

   .pastParamBlocks = &gastParameterBlocks,
};

struct st_param_buffer
{
      struct st_param_and_value gastParamBuffer[SIZE__PARAM_SAVE_QUEUE];
      uint8_t ucHead;
      uint8_t ucTail;
      uint8_t ucMaxSize;
      uint8_t ucElementSize;
};

static struct st_param_buffer gstParamBuffer;
static enum en_param_faults genParamFault = PARAM_FAULT__NONE;
static uint8_t gbMaster = 0;
static uint8_t gbReadyToSave = 1;//param master only
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint8_t Param_EmptySlotsInBuffer( void )
{
   uint8_t bReturn = (RingBuffer_GetFree(&RingBuffer_Param) + 1);
   return bReturn;
}
/*----------------------------------------------------------------------------
   Returns 1 both struct st_param_and_value are different
 *----------------------------------------------------------------------------*/
static uint8_t ParamBuff_ComparePVElements( struct st_param_and_value *pstBufferElement, struct st_param_and_value *pstParam )
{
   uint8_t bDiff = pstBufferElement->ucBlockIndex != pstParam->ucBlockIndex;
   bDiff |= pstBufferElement->uwParamIndex != pstParam->uwParamIndex;
   bDiff |= pstBufferElement->uiValue != pstParam->uiValue;
   return bDiff;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static uint8_t ParamBuff_IsInBuffer( struct st_param_and_value *pstParam )
{
   uint8_t bReturn = 0;
   for( uint8_t i = 0; i < RingBuffer_GetCount(&RingBuffer_Param); i++ )
   {
      uint8_t *ptr = RingBuffer_Param.data;
      uint32_t uiIndent = (RingBuffer_Param.head + i) & (RingBuffer_Param.count - 1);
      uiIndent *= RingBuffer_Param.itemSz;

      ptr += uiIndent;

      if( !ParamBuff_ComparePVElements( pstParam, (struct st_param_and_value * ) ptr ) )
      {
         bReturn = 1;
         break;
      }
   }
   return bReturn;
}
static uint8_t ParamBuff_IsEmpty( void )
{
   uint8_t bReturn = (RingBuffer_IsEmpty( &RingBuffer_Param )) ? 1:0;
   return bReturn;
}
/*----------------------------------------------------------------------------
   Load next element to the write parameters buffer
 *----------------------------------------------------------------------------*/
static uint8_t ParamBuff_LoadData( struct st_param_and_value *pstParam )
{
   uint8_t bReturn = 0;
   if(gbMaster)
   {
      Param_WriteValue(pstParam);
      bReturn = 1;
   }
   else
   {
      if( ParamBuff_IsInBuffer( pstParam ) )
      {
         bReturn = 1;
      }
      else if( !RingBuffer_IsFull(&RingBuffer_Param) )
      {
         RingBuffer_Insert(&RingBuffer_Param, pstParam);
         bReturn = 1;
      }
      else
      {
         if(ucState != FIRSTRUN)
         {
            genParamFault = PARAM_FAULT__OVERFLOW;
         }
      }
   }
   return bReturn;
}

static void ParamMaster_ServiceBuffer(void)
{
   gbReadyToSave = 1;
}
static void ParamSlave_ServiceBuffer(void)
{
   static struct st_param_and_value stParamToSave;
   static struct st_param_and_value stParamRead;
   static uint32_t ulTimeoutCounter_10ms;
   static uint8_t bSaving = 0;
   static uint8_t bFirst = 1;
   if(bFirst)
   {
      if(gstParamControl.bRAM_CopyValid)
      {
         bFirst = 0;
         gstParamBuffer.ucElementSize = sizeof(stParamToSave);
         gstParamBuffer.ucMaxSize = SIZE__PARAM_SAVE_QUEUE;
      }
   }
   else
   {
      if(!bSaving)
      {
         if(!ParamBuff_IsEmpty())
         {
            if(RingBuffer_Pop(&RingBuffer_Param, &stParamToSave))
            {
               bSaving = 1;
               stParamRead.ucBlockIndex = stParamToSave.ucBlockIndex;
               stParamRead.uwParamIndex = stParamToSave.uwParamIndex;
            }
         }
         ulTimeoutCounter_10ms = 0;
      }
      //--------------------------------------------
      if(bSaving)
      {
         if( Param_ReadValue( &stParamRead ) == stParamToSave.uiValue )
         {
            bSaving = 0;
         }
         else
         {
            if( gstParamControl.bMaster )
            {
               Param_WriteValue( &stParamToSave );
            }
            else
            {
               Param_SaveParameter( &stParamToSave );
            }
         }
         //--------------------------------------------
         if( ulTimeoutCounter_10ms >= TIMEOUT_PARAM_SAVE_10MS )
         {
//            genParamFault = PARAM_FAULT__TIMEOUT;
         }
         else if(genParamFault && ParamBuff_IsEmpty())
         {
            ParamBuff_ClrFault();
         }
         else
         {
            ulTimeoutCounter_10ms++;
         }
      }
   }
}
/*----------------------------------------------------------------------------
   Initialize ring buffers
 *----------------------------------------------------------------------------*/
void Param_InitRingBuffers( void )
{
    RingBuffer_Init(&RingBuffer_Param, Buffer_Param, sizeof(Buffer_Param[0]), RING_BUFFER_SIZE);
}
/*----------------------------------------------------------------------------
   Processes next element in the param save buffer
 *----------------------------------------------------------------------------*/
void Param_ServiceBuffer( void )
{
   if(gbMaster)
   {
      ParamMaster_ServiceBuffer();
   }
   else
   {
      ParamSlave_ServiceBuffer();
   }
}
/*----------------------------------------------------------------------------
   Gets fault state of param module
 *----------------------------------------------------------------------------*/
uint8_t ParamBuff_GetFault( void )
{
   return genParamFault;
}
/*----------------------------------------------------------------------------
   Clears fault state of param buffer
 *----------------------------------------------------------------------------*/
void ParamBuff_ClrFault( void )
{
   genParamFault = PARAM_FAULT__NONE;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint8_t Param_ReadValue_1Bit( enum en_1bit_params enParamNum )
{
   if( enParamNum < NUM_1BIT_PARAMS )
   {
      struct st_param_and_value stParam;
      uint8_t ucBlockOffset = enParamNum / enPARAM_NUM_PER_TYPE__BIT;
      uint16_t uwParamIndex = enParamNum % enPARAM_NUM_PER_TYPE__BIT;
      stParam.ucBlockIndex = STARTING_BLOCK_BIT + ucBlockOffset;
      stParam.uwParamIndex = uwParamIndex;
      return Param_ReadValue(&stParam);
   }
   return 0;
}
uint8_t Param_ReadValue_8Bit( enum en_8bit_params enParamNum )
{
   if( enParamNum < NUM_8BIT_PARAMS )
   {
      struct st_param_and_value stParam;
      uint8_t ucBlockOffset = enParamNum / enPARAM_NUM_PER_TYPE__UINT8;
      uint16_t uwParamIndex = enParamNum % enPARAM_NUM_PER_TYPE__UINT8;
      stParam.ucBlockIndex = STARTING_BLOCK_U8 + ucBlockOffset;
      stParam.uwParamIndex = uwParamIndex;
      return Param_ReadValue(&stParam);
   }
   return 0;
}
uint16_t Param_ReadValue_16Bit( enum en_16bit_params enParamNum )
{
   if( enParamNum < NUM_16BIT_PARAMS )
   {
      struct st_param_and_value stParam;
      uint8_t ucBlockOffset = enParamNum / enPARAM_NUM_PER_TYPE__UINT16;
      uint16_t uwParamIndex = enParamNum % enPARAM_NUM_PER_TYPE__UINT16;
      stParam.ucBlockIndex = STARTING_BLOCK_U16 + ucBlockOffset;
      stParam.uwParamIndex = uwParamIndex;
      return Param_ReadValue(&stParam);
   }
   return 0;
}
uint32_t Param_ReadValue_24Bit( enum en_24bit_params enParamNum )
{
   if( enParamNum < NUM_24BIT_PARAMS )
   {
      struct st_param_and_value stParam;
      uint8_t ucBlockOffset = enParamNum / enPARAM_NUM_PER_TYPE__UINT24;
      uint16_t uwParamIndex = enParamNum % enPARAM_NUM_PER_TYPE__UINT24;
      stParam.ucBlockIndex = STARTING_BLOCK_U24 + ucBlockOffset;
      stParam.uwParamIndex = uwParamIndex;
      return Param_ReadValue(&stParam);
   }
   return 0;
}
uint32_t Param_ReadValue_32Bit( enum en_32bit_params enParamNum )
{
   if( enParamNum < NUM_32BIT_PARAMS )
   {
      struct st_param_and_value stParam;
      uint8_t ucBlockOffset = enParamNum / enPARAM_NUM_PER_TYPE__UINT32;
      uint16_t uwParamIndex = enParamNum % enPARAM_NUM_PER_TYPE__UINT32;
      stParam.ucBlockIndex = STARTING_BLOCK_U32 + ucBlockOffset;
      stParam.uwParamIndex = uwParamIndex;
      return Param_ReadValue(&stParam);
   }
   return 0;
}

uint32_t Param_ReadValue_ByType( enum en_param_block_types eParamType, uint16_t uwParameterIndex )
{
   uint32_t uiReturn = 0;
   switch( eParamType )
   {
      case enPARAM_BLOCK_TYPE__BIT:
         uiReturn = Param_ReadValue_1Bit( uwParameterIndex );
         break;
      case enPARAM_BLOCK_TYPE__UINT8:
         uiReturn = Param_ReadValue_8Bit( uwParameterIndex );
         break;
      case enPARAM_BLOCK_TYPE__UINT16:
         uiReturn = Param_ReadValue_16Bit( uwParameterIndex );
         break;
      case enPARAM_BLOCK_TYPE__UINT24:
         uiReturn = Param_ReadValue_24Bit( uwParameterIndex );
         break;
      case enPARAM_BLOCK_TYPE__UINT32:
         uiReturn = Param_ReadValue_32Bit( uwParameterIndex );
         break;
      default:
         break;
   }
   return uiReturn;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/

uint8_t Param_WriteValue_1Bit( enum en_1bit_params enParamNum, uint8_t bValue )
{
   if( enParamNum < NUM_1BIT_PARAMS )
   {
      struct st_param_and_value stParam;
      uint8_t ucBlockOffset = enParamNum / enPARAM_NUM_PER_TYPE__BIT;
      uint16_t uwParamIndex = enParamNum % enPARAM_NUM_PER_TYPE__BIT;
      stParam.ucBlockIndex = STARTING_BLOCK_BIT + ucBlockOffset;
      stParam.uwParamIndex = uwParamIndex;
      stParam.uiValue = bValue;
      return ParamBuff_LoadData(&stParam);
   }
   return 0;
}
uint8_t Param_WriteValue_8Bit( enum en_8bit_params enParamNum, uint8_t ucValue )
{
   if( enParamNum < NUM_8BIT_PARAMS )
   {
      struct st_param_and_value stParam;
      uint8_t ucBlockOffset = enParamNum / enPARAM_NUM_PER_TYPE__UINT8;
      uint16_t uwParamIndex = enParamNum % enPARAM_NUM_PER_TYPE__UINT8;
      stParam.ucBlockIndex = STARTING_BLOCK_U8 + ucBlockOffset;
      stParam.uwParamIndex = uwParamIndex;
      stParam.uiValue = ucValue;
      return ParamBuff_LoadData(&stParam);
   }
   return 0;
}
uint8_t Param_WriteValue_16Bit( enum en_16bit_params enParamNum, uint16_t uwValue )
{
   if( enParamNum < NUM_16BIT_PARAMS )
   {
      struct st_param_and_value stParam;
      uint8_t ucBlockOffset = enParamNum / enPARAM_NUM_PER_TYPE__UINT16;
      uint16_t uwParamIndex = enParamNum % enPARAM_NUM_PER_TYPE__UINT16;
      stParam.ucBlockIndex = STARTING_BLOCK_U16 + ucBlockOffset;
      stParam.uwParamIndex = uwParamIndex;
      stParam.uiValue = uwValue;
      return ParamBuff_LoadData(&stParam);
   }
   return 0;
}
uint8_t Param_WriteValue_24Bit( enum en_24bit_params enParamNum, uint32_t ulValue )
{
   if( enParamNum < NUM_24BIT_PARAMS )
   {
      struct st_param_and_value stParam;
      uint8_t ucBlockOffset = enParamNum / enPARAM_NUM_PER_TYPE__UINT24;
      uint16_t uwParamIndex = enParamNum % enPARAM_NUM_PER_TYPE__UINT24;
      stParam.ucBlockIndex = STARTING_BLOCK_U24 + ucBlockOffset;
      stParam.uwParamIndex = uwParamIndex;
      stParam.uiValue = ulValue;
      return ParamBuff_LoadData(&stParam);
   }
   return 0;
}
uint8_t Param_WriteValue_32Bit( enum en_32bit_params enParamNum, uint32_t ulValue )
{
   if( enParamNum < NUM_32BIT_PARAMS )
   {
      struct st_param_and_value stParam;
      uint8_t ucBlockOffset = enParamNum / enPARAM_NUM_PER_TYPE__UINT32;
      uint16_t uwParamIndex = enParamNum % enPARAM_NUM_PER_TYPE__UINT32;
      stParam.ucBlockIndex = STARTING_BLOCK_U32 + ucBlockOffset;
      stParam.uwParamIndex = uwParamIndex;
      stParam.uiValue = ulValue;
      return ParamBuff_LoadData(&stParam);
   }
   return 0;
}

uint8_t Param_WriteValue_ByType( enum en_param_block_types eParamType, uint16_t uwParamIndex, uint32_t uiParamValue )
{
   uint8_t bSuccess = 0;

   switch( eParamType )
   {
      case enPARAM_BLOCK_TYPE__BIT:
         bSuccess = Param_WriteValue_1Bit( uwParamIndex, uiParamValue );
         break;
      case enPARAM_BLOCK_TYPE__UINT8:
         bSuccess = Param_WriteValue_8Bit( uwParamIndex, uiParamValue );
         break;
      case enPARAM_BLOCK_TYPE__UINT16:
         bSuccess = Param_WriteValue_16Bit( uwParamIndex, uiParamValue );
         break;
      case enPARAM_BLOCK_TYPE__UINT24:
         bSuccess = Param_WriteValue_24Bit( uwParamIndex, uiParamValue );
         break;
      case enPARAM_BLOCK_TYPE__UINT32:
         bSuccess = Param_WriteValue_32Bit( uwParamIndex, uiParamValue );
         break;
      default:
         break;
   }

   return bSuccess;
}

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint8_t Param_GetBlockIndex_1Bit( enum en_1bit_params enParamNum )
{
   uint8_t ucBlockOffset = enParamNum / enPARAM_NUM_PER_TYPE__BIT;
   return STARTING_BLOCK_BIT + ucBlockOffset;
}
uint8_t Param_GetBlockIndex_8Bit( enum en_8bit_params enParamNum )
{
   uint8_t ucBlockOffset = enParamNum / enPARAM_NUM_PER_TYPE__UINT8;
   return STARTING_BLOCK_U8 + ucBlockOffset;
}
uint8_t Param_GetBlockIndex_16Bit( enum en_16bit_params enParamNum )
{
   uint8_t ucBlockOffset = enParamNum / enPARAM_NUM_PER_TYPE__UINT16;
   return STARTING_BLOCK_U16 + ucBlockOffset;
}
uint8_t Param_GetBlockIndex_24Bit( enum en_24bit_params enParamNum )
{
   uint8_t ucBlockOffset = enParamNum / enPARAM_NUM_PER_TYPE__UINT24;
   return STARTING_BLOCK_U24 + ucBlockOffset;
}
uint8_t Param_GetBlockIndex_32Bit( enum en_32bit_params enParamNum )
{
   uint8_t ucBlockOffset = enParamNum / enPARAM_NUM_PER_TYPE__UINT32;
   return STARTING_BLOCK_U32 + ucBlockOffset;
}
/*----------------------------------------------------------------------------
Get the parameter index within a block
 *----------------------------------------------------------------------------*/
uint16_t Param_GetParamIndex_1Bit( enum en_1bit_params enParamNum )
{
   return enParamNum % enPARAM_NUM_PER_TYPE__BIT;
}
uint16_t Param_GetParamIndex_8Bit( enum en_8bit_params enParamNum )
{
   return enParamNum % enPARAM_NUM_PER_TYPE__UINT8;
}
uint16_t Param_GetParamIndex_16Bit( enum en_16bit_params enParamNum )
{
   return enParamNum % enPARAM_NUM_PER_TYPE__UINT16;
}
uint16_t Param_GetParamIndex_24Bit( enum en_24bit_params enParamNum )
{
   return enParamNum % enPARAM_NUM_PER_TYPE__UINT24;
}
uint16_t Param_GetParamIndex_32Bit( enum en_32bit_params enParamNum )
{
   return enParamNum % enPARAM_NUM_PER_TYPE__UINT32;
}

/*----------------------------------------------------------------------------
 Set Param Master. Called only on MRA
 *----------------------------------------------------------------------------*/
void Param_SetMaster(void)
{
   gbMaster = 1;
}

/*-----------------------------------------------------------------------------

   Converts a block index value to a EEPROM paage value.

   Parameter blocks are sized to fit one block per 64-byte EEPROM page.
   Because the first few pages of the EEPROM are reserved for special use,
   parameter block 00 is not stored on EEPROM page 00 so this function is
   need for translation.

 -----------------------------------------------------------------------------*/
int Param_BlockToEEPROM_Page( struct st_param_block_info *pstParamBlockInfo, uint8_t ucBlockIndex )
{
   return pstParamBlockInfo->ucEEPROMI_Page__Block00 + ucBlockIndex;
}

/*-----------------------------------------------------------------------------
   Each EEPROM page is 64 bytes in size. I'm giving each block an entire page.
   The first 4 bytes of the page hold the CRC.
   The next 2 bytes hold the page ID and its 1's compliment.

   That leaves 58 bytes for the actual block data. Therefore a block of 8-bit
   parameters can store 58 parameters per block. If parameters are 16-bits in
   size the number is halved to 29.
 -----------------------------------------------------------------------------*/
int Param_NumParams( int iBlockIndex )
{
   struct st_param_block_info * pstParamBlockInfo;

   pstParamBlockInfo = &gstSys_ParamBlockInfo;

   int iNumParams = 0;

   if ( iBlockIndex < pstParamBlockInfo->ucNumBlocks )
   {
      const struct st_parameter_block *pstParamBlock;

      pstParamBlock = (*pstParamBlockInfo->pastParamBlocks)[ iBlockIndex ];

      switch( pstParamBlock->ueBlockType )
      {
         case enPARAM_BLOCK_TYPE__UINT32: iNumParams = 14; break;

         case enPARAM_BLOCK_TYPE__BIT: iNumParams = 58 * 8; break;
         case enPARAM_BLOCK_TYPE__UINT8: iNumParams = 58; break;
         case enPARAM_BLOCK_TYPE__UINT16: iNumParams = 29; break;
         case enPARAM_BLOCK_TYPE__UINT24: iNumParams = 19; break;
      }
   }

   return iNumParams;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
int Param_NumBlocks( void )
{
   return gstSys_ParamBlockInfo.ucNumBlocks;
}

/*-----------------------------------------------------------------------------

   Returns non-zero if the iBlockIndex, iParamIndex pair refer to an actual
   parameter. Otherwise, returns zero.

 -----------------------------------------------------------------------------*/
int Sys_Param_IsValid( int iBlockIndex, int iParamIndex )
{
   struct st_param_block_info * pstParamBlockInfo;

   pstParamBlockInfo = &gstSys_ParamBlockInfo;

   int bValid = 0;

   if ( iBlockIndex < pstParamBlockInfo->ucNumBlocks )
   {
      if ( iParamIndex < Param_NumParams( iBlockIndex ) )
      {
         bValid = 1;
      }
   }

   return bValid;
}

/*-----------------------------------------------------------------------------

   Returns non-zero if the ucBlockIndex, ucChunk pair refer to an actual
   parameter. Otherwise, returns zero.

 -----------------------------------------------------------------------------*/
uint8_t Sys_ParamChunk_IsValid( uint8_t ucBlockIndex, uint8_t ucChunk )
{
   struct st_param_block_info * pstParamBlockInfo;

   pstParamBlockInfo = &gstSys_ParamBlockInfo;

   int bValid = 0;

   if ( ucBlockIndex < pstParamBlockInfo->ucNumBlocks )
   {
      if ( ucChunk < NUM_OF_CHUNKS_PER_BLOCK )
      {
         bValid = 1;
      }
   }

   return bValid;
}

/*-----------------------------------------------------------------------------

   Takes a block index as an argument and returns the type of data stored in
   that block.

   Return types include:
      enPARAM_BLOCK_TYPE__BIT,
      enPARAM_BLOCK_TYPE__UINT8,
      enPARAM_BLOCK_TYPE__UINT16,
      enPARAM_BLOCK_TYPE__UINT24,
      enPARAM_BLOCK_TYPE__UINT32,

   Returns enPARAM_BLOCK_TYPE__UNKNOWN on error.

 -----------------------------------------------------------------------------*/
enum en_param_block_types Sys_Param_BlockType( int iBlockIndex )
{
   struct st_param_block_info * pstParamBlockInfo;

   pstParamBlockInfo = &gstSys_ParamBlockInfo;

   enum en_param_block_types enBlockType = enPARAM_BLOCK_TYPE__UNKNOWN;

   if ( iBlockIndex < pstParamBlockInfo->ucNumBlocks )
   {
      const struct st_parameter_block *pstParamBlock;

      pstParamBlock = (*pstParamBlockInfo->pastParamBlocks)[ iBlockIndex ];

      enBlockType = pstParamBlock->ueBlockType;
   }

   return enBlockType;
}


/*-----------------------------------------------------------------------------

   Returns the starting address of the specified parameter block in RAM.
   Address is returned as a uint32_t, not a pointer.

   Caller specifies parameter block in:
      pstParamControl->stParamAndValue_Arg.ucBlockIndex
 -----------------------------------------------------------------------------*/
static uint32_t GetBlockAddress( struct st_param_control *pstParamControl )
{
   uint32_t uiAddressOfBlock = 0;

   struct st_param_and_value *pstParamAndValue = &(pstParamControl->stParamAndValue_Arg);

   if ( pstParamAndValue->ucBlockIndex < Param_NumBlocks() )
   {
      uiAddressOfBlock = (uint32_t) pstParamControl->paucParamBlocksInRAM;

      uint32_t uiOffsetOfBlockStartInRAM = EEPROM_INT__BYTES_PER_PAGE;
      uiOffsetOfBlockStartInRAM *= pstParamAndValue->ucBlockIndex;

      uiAddressOfBlock += uiOffsetOfBlockStartInRAM;
   }

   return uiAddressOfBlock;
}

/*-----------------------------------------------------------------------------

   Returns the address of the specified parameter in RAM.
   Address is returned as a uint32_t, not a pointer.

   Caller specifies parameter in:
      pstParamControl->stParamAndValue_Arg.ucBlockIndex
      pstParamControl->stParamAndValue_Arg.uwParamIndex

 -----------------------------------------------------------------------------*/
static uint32_t GetParamAddress( struct st_param_control *pstParamControl )
{
   uint32_t uiAddressOfParam = 0;

   uint32_t uiAddressOfBlock = GetBlockAddress( pstParamControl );

   struct st_param_and_value *pstParamAndValue = &(pstParamControl->stParamAndValue_Arg);

   int enBlockType = Sys_Param_BlockType( pstParamAndValue->ucBlockIndex );

   int bValidParam = Sys_Param_IsValid( pstParamAndValue->ucBlockIndex, pstParamAndValue->uwParamIndex );

   if ( uiAddressOfBlock
     && bValidParam )
   {
      uint32_t uiOffsetOfParamFromBlockStart = pstParamAndValue->uwParamIndex;

      if ( enBlockType == enPARAM_BLOCK_TYPE__BIT )
      {
         uiOffsetOfParamFromBlockStart = pstParamAndValue->uwParamIndex >> 3;  // divide by 8
      }
      else
      {
         uiOffsetOfParamFromBlockStart = pstParamAndValue->uwParamIndex;

         switch ( enBlockType )
         {
            case enPARAM_BLOCK_TYPE__UINT32:
               uiOffsetOfParamFromBlockStart *= 4;
               break;

            case enPARAM_BLOCK_TYPE__UINT16:
               uiOffsetOfParamFromBlockStart *= 2;
               break;

            case enPARAM_BLOCK_TYPE__UINT24:
               uiOffsetOfParamFromBlockStart *= 3;
               break;

            case enPARAM_BLOCK_TYPE__UINT8:
            default:
               uiOffsetOfParamFromBlockStart *= 1;
               break;
         }
      }
      uiOffsetOfParamFromBlockStart += 6;  // first 6 bytes of every block are ID and CRC

      uiAddressOfParam = uiAddressOfBlock + uiOffsetOfParamFromBlockStart;
   }

   return uiAddressOfParam;
}

/*-----------------------------------------------------------------------------

   Recalculates the CRC for the block in RAM and stores it in the RAM block.

   Caller specifies parameter block in:
      pstParamControl->stParamAndValue_Arg.ucBlockIndex

 -----------------------------------------------------------------------------*/
void UpdateBlockCRC( struct st_param_control *pstParamControl )
{
   uint32_t uiAddressOfBlock = GetBlockAddress( pstParamControl );

   if ( uiAddressOfBlock )
   {
      uint32_t uiCRC = Chip_CRC_CRC32( (uint32_t *) (uiAddressOfBlock + 4),
                                       NUM_OF_CHUNKS_PER_BLOCK);
      * (uint32_t *) uiAddressOfBlock = uiCRC;
   }
}

/*-----------------------------------------------------------------------------

   Updates the value of a single parameter in RAM.
   Updates the CRC of the RAM block containing the parameter.

   Caller specifies parameter and value in:
      pstParamControl->stParamAndValue_Arg.ucBlockIndex
      pstParamControl->stParamAndValue_Arg.uwParamIndex
      pstParamControl->stParamAndValue_Arg.uiValue

 -----------------------------------------------------------------------------*/
static void WriteParamToRAM( struct st_param_control *pstParamControl )
{
   uint32_t uiAddressOfParam = GetParamAddress( pstParamControl );

   struct st_param_and_value *pstParamAndValue = &(pstParamControl->stParamAndValue_Arg);

   if ( uiAddressOfParam )
   {

      int enBlockType = Sys_Param_BlockType( pstParamAndValue->ucBlockIndex );

      if ( enBlockType == enPARAM_BLOCK_TYPE__BIT )
      {
         // uiAddressOfParam tells us which byte holds the parameter but not
         // which bit on the byte.

         uint8_t ucBitmask = 1 << (pstParamAndValue->uwParamIndex & 0x07);  // % 8
         uint8_t *pucByte = (uint8_t *) uiAddressOfParam;

         if ( pstParamAndValue->uiValue )
         {
            *pucByte |= ucBitmask;
         }
         else
         {
            *pucByte &= ~ucBitmask;
         }
      }
      else
      {
         switch ( enBlockType )
         {
            case enPARAM_BLOCK_TYPE__UINT32:
               * (uint32_t *) uiAddressOfParam = (uint32_t) pstParamAndValue->uiValue;
               break;

            case enPARAM_BLOCK_TYPE__UINT16:
               * (uint16_t *) uiAddressOfParam = (uint16_t) pstParamAndValue->uiValue;
               break;

            case enPARAM_BLOCK_TYPE__UINT24:
               * (uint8_t *) uiAddressOfParam = (uint8_t) pstParamAndValue->uiValue;
               * (uint8_t *) (uiAddressOfParam + 1) = (uint8_t) (pstParamAndValue->uiValue >> 8);
               * (uint8_t *) (uiAddressOfParam + 2) = (uint8_t) (pstParamAndValue->uiValue >> 16);
               break;

            case enPARAM_BLOCK_TYPE__UINT8:
               * (uint8_t *) uiAddressOfParam = (uint8_t) pstParamAndValue->uiValue;
               break;
            default:
               break;
         }
      }

      UpdateBlockCRC( pstParamControl );
   }
}

/*-----------------------------------------------------------------------------

   Returns the CRC of the specified parameter block in RAM.
   Note that the CRC is not recalculated.

 -----------------------------------------------------------------------------*/
uint32_t Param_GetBlockCRC_RAM( uint8_t ucBlockIndex )
{
   struct st_param_control *pstParamControl = &gstParamControl;

   gstParamControl.stParamAndValue_Arg.ucBlockIndex = ucBlockIndex;

   uint32_t uiCRC = 0;

   uint32_t uiAddressOfBlock = GetBlockAddress( pstParamControl );

   if ( uiAddressOfBlock )
   {
      uiCRC = * (uint32_t *) uiAddressOfBlock;
   }

   return uiCRC;
}

/*-----------------------------------------------------------------------------

   Returns the CRC of the specified parameter block in EEPROM. By comparing the
   CRC of the RAM and EEPROM copies, you can detect when the RAM copy has been
   modified and updates need to be written to the EEPROM.

   Note that the EEPROM is not actually read when this function is called. A
   copy of the EEPROM block CRCs is maintained in RAM and updated whenever a
   write to the EEPROM occurs.

   Caller specifies parameter block in:
      pstParamControl->stParamAndValue_Arg.ucBlockIndex

 -----------------------------------------------------------------------------*/
uint32_t Param_GetBlockCRC_EEPROM( struct st_param_control *pstParamControl )
{
   uint32_t uiCRC = 0;

   if ( pstParamControl->pauiEEPROM_CRCs )
   {
      uint8_t ucBlockIndex = pstParamControl->stParamAndValue_Arg.ucBlockIndex;
      if( ucBlockIndex < Param_NumBlocks() )
      {
         uiCRC = *(pstParamControl->pauiEEPROM_CRCs + ucBlockIndex);
      }
   }

   return uiCRC;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint32_t Param_GetBlockCRC_Master( uint8_t ucBlockIndex )
{
   struct st_param_control *pstParamControl = &gstParamControl;

   uint32_t uiCRC = 0;

   if ( pstParamControl->pauiMaster_CRCs
   && ( ucBlockIndex < Param_NumBlocks() ) )
   {
      uiCRC = *(pstParamControl->pauiMaster_CRCs + ucBlockIndex);
   }

   return uiCRC;
}

/*-----------------------------------------------------------------------------

   Updates the EEPROM CRC copy stored in RAM.

   Note that the EEPROM itself is not updated. This function just updates a RAM
   copy of the CRC values stored in the EEPROM. RAM copies are maintained due to
   the relatively long access times required to read and write the EEPROM.

   The CRC on the EEPROM itself is updated when the block data is written to the
   device.

   This function should be called after calling EEPROMI__Write().

   Caller specifies block index and CRC in:
      ucBlockIndex
      uiCRC

 -----------------------------------------------------------------------------*/
uint32_t Param_PutBlockCRC_EEPROM( struct st_param_control *pstParamControl,
                                   uint8_t ucBlockIndex,
                                   uint32_t uiCRC
                                 )
{
   if ( pstParamControl->pauiEEPROM_CRCs
   && ( ucBlockIndex < Param_NumBlocks() ) )
   {
      *(pstParamControl->pauiEEPROM_CRCs + ucBlockIndex) = uiCRC;
   }

   return uiCRC;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint32_t Param_PutBlockCRC_Master( struct st_param_control *pstParamControl,
                                   uint8_t ucBlockIndex,
                                   uint32_t uiCRC
                                 )
{
   if ( pstParamControl->pauiMaster_CRCs
   && ( ucBlockIndex < Param_NumBlocks() ) )
   {
      *(pstParamControl->pauiMaster_CRCs + ucBlockIndex) = uiCRC;
   }

   return uiCRC;
}

/*-----------------------------------------------------------------------------

   Returns the value of the specified parameter.

   Value returned is from the copy of the parameters stored in RAM.

   Caller specifies parameter in:
      pstParamAndValue->ucBlockIndex
      pstParamAndValue->uwParamIndex

 -----------------------------------------------------------------------------*/
uint32_t Param_ReadValue( struct st_param_and_value *pstParamAndValue )
{
   uint32_t uiParamValue = 0;

   struct st_param_control *pstParamControl = &gstParamControl;

   memcpy( &pstParamControl->stParamAndValue_Arg,
           pstParamAndValue,
           sizeof( pstParamControl->stParamAndValue_Arg )
         );

   uint32_t uiAddressOfParam = GetParamAddress( pstParamControl );

   if ( uiAddressOfParam )
   {
      int enBlockType = Sys_Param_BlockType( pstParamAndValue->ucBlockIndex );

      if ( enBlockType == enPARAM_BLOCK_TYPE__BIT )
      {
         // uiAddressOfParam tells us which byte holds the parameter but not
         // which bit on the byte.

         uint8_t ucBitmask = 1 << (pstParamAndValue->uwParamIndex & 0x07);  // % 8
         uint8_t *pucByte = (uint8_t *) uiAddressOfParam;

         if ( *pucByte & ucBitmask )
         {
            uiParamValue = 1;
         }
         else
         {
            uiParamValue = 0;
         }
      }
      else
      {
         switch ( enBlockType )
         {
            case enPARAM_BLOCK_TYPE__UINT32:
               uiParamValue = * (uint32_t *) uiAddressOfParam;
               break;

            case enPARAM_BLOCK_TYPE__UINT16:
               uiParamValue = * (uint16_t *) uiAddressOfParam;
               break;

            case enPARAM_BLOCK_TYPE__UINT24:
               uiParamValue = * (uint8_t *) uiAddressOfParam;
               uiParamValue |= (* (uint8_t *) (uiAddressOfParam + 1)) << 8;
               uiParamValue |= (* (uint8_t *) (uiAddressOfParam + 2)) << 16;
               break;

            default:
               uiParamValue = * (uint8_t *) uiAddressOfParam;
               break;
         }
      }
   }

   pstParamAndValue->uiValue = uiParamValue;

   return uiParamValue;
}

/*-----------------------------------------------------------------------------

   Updates the value of a single parameter in RAM.
   Updates the CRC of the RAM block containing the parameter.

   Note that this function reads the current value first and only writes the new
   value if it is different. This saves time since the block CRC only gets
   recalculated if the data changed.

   Note that the values in EEPROM are not updated.

   Caller specifies parameter and value in:
      pstParamControl->stParamAndValue_Arg.ucBlockIndex
      pstParamControl->stParamAndValue_Arg.uwParamIndex
      pstParamControl->stParamAndValue_Arg.uiValue

 -----------------------------------------------------------------------------*/
static void Param_WriteValue2( struct st_param_control *pstParamControl )
{
   uint32_t uiValue_New = pstParamControl->stParamAndValue_Arg.uiValue;

   struct st_param_and_value stCurrent;

   stCurrent.ucBlockIndex = pstParamControl->stParamAndValue_Arg.ucBlockIndex;
   stCurrent.uwParamIndex = pstParamControl->stParamAndValue_Arg.uwParamIndex;

   Param_ReadValue( &stCurrent );

   if ( uiValue_New != stCurrent.uiValue )
   {
      pstParamControl->stParamAndValue_Arg.uiValue = uiValue_New;

      WriteParamToRAM( pstParamControl );

      memcpy( &pstParamControl->stRecentlyUpdatedParam,
              &pstParamControl->stParamAndValue_Arg,
              sizeof( pstParamControl->stRecentlyUpdatedParam )
            );

      // The countdown value specifies how many times the param master will give
      // this parameter priority when transmitting shared data to the slaves.
      pstParamControl->stRecentlyUpdatedParam.ucCountdown = 20;
   }
}

/*-----------------------------------------------------------------------------

   This function will cause the specified parameter to be updated in RAM on the
   local node. On nodes other than MRA, only the parameter modules should call
   this function. If another module called this function, it would only update
   the parameter on the node on which the caller is running. This would result
   in unsynchronized parameter data across the system.

   Other modules may call this function if they are running on the MRA node.
   Updates to the RAM copy of parameters on MRA will cause the master parameter
   modules (which runs only on MRA) to broadcast the changes system wide.

   Updates to the RAM copy of parameters will automatically be written to the
   EEPROM by the gstMod_ParamEEPROM module which runs in the background on all
   nodes.

 -----------------------------------------------------------------------------*/
void Param_WriteValue( struct st_param_and_value *pstParamAndValue )
{
   struct st_param_control *pstParamControl = &gstParamControl;
   int bValidParam = Sys_Param_IsValid( pstParamAndValue->ucBlockIndex, pstParamAndValue->uwParamIndex );
   if( bValidParam )
   {
      memcpy( &pstParamControl->stParamAndValue_Arg,
              pstParamAndValue,
              sizeof( struct st_param_and_value )
            );

      Param_WriteValue2( pstParamControl );
   }
}

/*-----------------------------------------------------------------------------

   Copies one block of parameters from RAM to EEPROM. Updates the RAM copy of
   the EEPROM CRCs.

   Note that the call to EEPROMI__Write() initiates a wrtie operation on the
   EEPROM device that can take up to 3 ms to complete. Although the call to
   EEPROMI__Write() returns immediately, it must not be called more often than
   every 3 ms to ensure no loss of data. Therefore, this function must not be
   called more often than every 3 ms.

   Caller specifies parameter block in:
      pstParamControl->stParamAndValue_Arg.ucBlockIndex

 -----------------------------------------------------------------------------*/
void Param_CopyBlock_RAM_to_EEPROM( struct st_param_control *pstParamControl )
{
   uint32_t uiAddressOfBlock = GetBlockAddress( pstParamControl );

   if ( uiAddressOfBlock )
   {
      uint8_t ucBlockIndex = pstParamControl->stParamAndValue_Arg.ucBlockIndex;

      uint8_t aucBuffer[ EEPROM_INT__BYTES_PER_PAGE ];

      memcpy( &(aucBuffer[ 0 ]),
              (uint32_t *) uiAddressOfBlock,
              EEPROM_INT__BYTES_PER_PAGE
            );

      uint_fast8_t ucPage = Param_BlockToEEPROM_Page( pstParamControl->pstParamBlockInfo,
                                                      ucBlockIndex
                                                    );

      EEPROMI__Write( ucPage, &(aucBuffer[ 0 ]) );

      uint32_t uiCRC = * (uint32_t *) &(aucBuffer[ 0 ]);

      Param_PutBlockCRC_EEPROM( pstParamControl, ucBlockIndex, uiCRC );
   }
}

/*-----------------------------------------------------------------------------

   Returns non-zero if the parameters in RAM are valid and ready for use.
   Every module in the system that uses parameters should poll this function
   at boot-up and not begin using parameters until the RAM copy has become
   valid.

 -----------------------------------------------------------------------------*/
int Param_ParametersAreValid( void )
{
   return gstParamControl.bRAM_CopyValid;
}

/*-----------------------------------------------------------------------------

   Increment the param index to reference the next parameter. If the end of the
   block is encountered, the param index is reset to 0 and the block index is
   incremented to the next block unless the bStayInBlock flag is non-zero.

   If the block index is incremented past the last block then it too is reset
   to zero.

 -----------------------------------------------------------------------------*/
void Param_NextParam( struct st_param_and_value *pstParam, uint_fast8_t bStayInBlock )
{
   if ( ++pstParam->uwParamIndex >= Param_NumParams( pstParam->ucBlockIndex ) )
   {
      pstParam->uwParamIndex = 0;

      if ( !bStayInBlock )
      {
         if ( ++pstParam->ucBlockIndex >= Param_NumBlocks() )
         {
            pstParam->ucBlockIndex = 0;
         }
      }
   }
}
/*-----------------------------------------------------------------------------
   Compares CRC with master.
   If different, sends request for that block to master
 -----------------------------------------------------------------------------*/
void Param_LookForOutOfDateBlock( struct st_param_control *pstParamControl )
{
   uint8_t ucBlockIndex;
   uint8_t ucOutOfDateBlock_IndexPlus1 = 0;
   uint8_t ucNumBlocks = pstParamControl->pstParamBlockInfo->ucNumBlocks;
   for ( ucBlockIndex = 0; ucBlockIndex < ucNumBlocks; ++ucBlockIndex )
   {
      pstParamControl->stParamAndValue_Arg.ucBlockIndex = ucBlockIndex;
      UpdateBlockCRC(pstParamControl);

      uint32_t uiCRC_Local = Param_GetBlockCRC_RAM( ucBlockIndex );
      uint32_t uiCRC_Master = Param_GetBlockCRC_Master( ucBlockIndex );

      if ( uiCRC_Local != uiCRC_Master )
      {
         ucOutOfDateBlock_IndexPlus1 = ucBlockIndex + 1;
         break;
      }
   }
   pstParamControl->ucOutOfDateBlock_IndexPlus1 = ucOutOfDateBlock_IndexPlus1;
}

/*-----------------------------------------------------------------------------
   Writes a parameter save request to the astParmEditRequest[] field of the
   specified node.
   
   If another request was already pending, it will be overwritten.

   This function can by processors that forward save requests. For example,
   the CTA node receives save requests from CTB which it must forwarded to MRA.
   CTA would call this function passing the save request from CTB and setting
   ucParamNodeType_SavingNode = enPARAM_NODE__CTB.
 -----------------------------------------------------------------------------*/
void Param_SaveParameterFromNode( struct st_param_and_value *pstParamAndValue,
                                  uint_fast8_t ucParamNodeType_SavingNode
                                )
{
   if ( ucParamNodeType_SavingNode < NUM_PARAM_NODES )
   {
      pstParamAndValue->ucCountdown = NUM_TIMES_TO_RESEND_PARAM_SAVE_REQ;  // specifies how many times to transmit the request

      memcpy( &(gstParamControl.astParmEditRequest[ ucParamNodeType_SavingNode ]),
              pstParamAndValue,
              sizeof( struct st_param_and_value )
            );

   }
}

/*-----------------------------------------------------------------------------
   Writes a parameter save request to the astParmEditRequest[] field of this
   node.

   UI screens which edit parameters can call this function to save them.
   
   If another request was already pending, it will be overwritten.
 -----------------------------------------------------------------------------*/
void Param_SaveParameter( struct st_param_and_value *pstParamAndValue )
{
   struct st_param_control *pstParamControl = &gstParamControl;

   uint_fast8_t ucParamNodeType = pstParamControl->ucParamNodeType;

   if ( ucParamNodeType < NUM_PARAM_NODES )
   {
      Param_SaveParameterFromNode( pstParamAndValue, ucParamNodeType );
   }
}

/*-----------------------------------------------------------------------------
   USE ONLY IN mod_param_app and mod_param_master files
 -----------------------------------------------------------------------------*/
uint32_t Param_ReadChunk( uint8_t ucBlockIndex, uint8_t ucChunkIndex )
{
   struct st_param_control *pstParamControl = &gstParamControl;

   pstParamControl->stParamAndValue_Arg.ucBlockIndex = ucBlockIndex;

   uint32_t uiAddressOfBlock = GetBlockAddress( pstParamControl );

   uint32_t uiAddressOfChunk = 0;
   if ( uiAddressOfBlock )
   {
      uint32_t uiOffsetOfChunkFromBlockStart = ucChunkIndex;
      uiOffsetOfChunkFromBlockStart = ucChunkIndex;
      uiOffsetOfChunkFromBlockStart *= BYTES_PER_PARAM_CHUNK; //Bytes per chunk
      uiOffsetOfChunkFromBlockStart += START_OF_CHUNK_BYTES_IN_BLOCK;  // first 6 bytes of every block are ID and CRC

      uiAddressOfChunk = uiAddressOfBlock + uiOffsetOfChunkFromBlockStart;
      if(uiAddressOfChunk)
      {
         uint32_t ulData = * (uint32_t *) uiAddressOfChunk;
         return ulData;
      }
   }
   return 0;
}
void Param_WriteChunk( uint8_t ucBlockIndex, uint8_t ucChunkIndex, uint32_t ulData )
{
   uint8_t ucMaxBlocks = Param_NumBlocks();
   if(ucChunkIndex < NUM_OF_CHUNKS_PER_BLOCK && ucBlockIndex < ucMaxBlocks)
   {
      struct st_param_control *pstParamControl = &gstParamControl;

      pstParamControl->stParamAndValue_Arg.ucBlockIndex = ucBlockIndex;

      uint32_t uiAddressOfBlock = GetBlockAddress( pstParamControl );

      if ( uiAddressOfBlock )
      {
         uint32_t uiAddressOfChunk;
         uint32_t uiOffsetOfChunkFromBlockStart;
         uiOffsetOfChunkFromBlockStart = ucChunkIndex;
         uiOffsetOfChunkFromBlockStart *= BYTES_PER_PARAM_CHUNK; //Bytes per chunk
         uiOffsetOfChunkFromBlockStart += START_OF_CHUNK_BYTES_IN_BLOCK;  // first 4 bytes of every block are ID and CRC

         uiAddressOfChunk = uiAddressOfBlock + uiOffsetOfChunkFromBlockStart;

         * (uint32_t *) uiAddressOfChunk = ulData;

         UpdateBlockCRC( pstParamControl );
      }
   }
}
/*-----------------------------------------------------------------------------
   Reads from the system's 64 byte parameter 'blocks' in 4 byte 'chunks'.
   Differs from Param_ReadChunk() in that it can access the local CRC as well.
 -----------------------------------------------------------------------------*/
uint32_t Param_ReadChunk_PEP( uint8_t ucBlockIndex, uint8_t ucChunkIndex )
{
   struct st_param_control *pstParamControl = &gstParamControl;

   pstParamControl->stParamAndValue_Arg.ucBlockIndex = ucBlockIndex;

   uint32_t uiAddressOfBlock = GetBlockAddress( pstParamControl );

   uint32_t uiAddressOfChunk = 0;
   if ( uiAddressOfBlock )
   {
      uint32_t uiOffsetOfChunkFromBlockStart = ucChunkIndex;
      uiOffsetOfChunkFromBlockStart = ucChunkIndex;
      uiOffsetOfChunkFromBlockStart *= BYTES_PER_PARAM_CHUNK; //Bytes per chunk

      uiAddressOfChunk = uiAddressOfBlock + uiOffsetOfChunkFromBlockStart;
      if(uiAddressOfChunk)
      {
         uint32_t ulData = * (uint32_t *) uiAddressOfChunk;
         return ulData;
      }
   }
   return 0;
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
