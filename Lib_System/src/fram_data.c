/*
 * fram_data.c
 *
 *  Created on: Dec 5, 2016
 *      Author: SeanA
 */
#include "fram_data.h"

_Static_assert ( (Num_EmergenyBF <= 16), "To many items in emergency bit map");
