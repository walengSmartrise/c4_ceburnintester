/******************************************************************************
 *
 * @file     bit.c
 * @brief    Bit manipulation functions
 * @version  V1.00
 * @date     21, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "sys.h"

#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void Sys_Bit_Set( void *pvBlock, uint32_t uiBitIndex, uint8_t bSet )
{
   uint32_t uiByteOffset = uiBitIndex >> 3;  // divide by 8
   uint32_t uiBitOffset = uiBitIndex & 0x07;  // mod 8
   uint8_t ucMask = 1 << uiBitOffset;

   uint8_t *pucByteToModify = pvBlock + uiByteOffset;

   uint8_t ucByteValue = *pucByteToModify;

   if ( bSet )
   {
      ucByteValue |= ucMask;
   }
   else  // clear
   {
      ucByteValue &= ~ucMask;
   }

   *pucByteToModify = ucByteValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t Sys_Bit_Get( void * const pvBlock, uint32_t uiBitIndex )
{
   uint32_t uiByteOffset = uiBitIndex >> 3;  // divide by 8
   uint32_t uiBitOffset = uiBitIndex & 0x07;  // mod 8

   uint8_t *pucByteToRead = pvBlock + uiByteOffset;

   uint8_t ucByteValue = *pucByteToRead;

   return (ucByteValue >> uiBitOffset) & 1;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void Sys_Bit_Set_Array32( uint32_t * aulArray, uint8_t ucBitToSet, uint8_t ucArraySize, uint8_t bValue )
{
   uint_fast8_t ucIndex = ucBitToSet / 32;
   uint_fast8_t ucBit  = ucBitToSet % 32;

   if ( bValue )
   {
      aulArray[ ucIndex ] |= ( 1 << ucBit );
   }
   else
   {
      aulArray[ ucIndex ] &= ~( 1 << ucBit );
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t Sys_Bit_Get_Array32( uint32_t * aulArray, uint8_t ucBitToGet, uint8_t ucArraySize )
{
   uint_fast8_t ucIndex = ucBitToGet / 32;
   uint_fast8_t ucBit  = ucBitToGet % 32;

   return ( ( aulArray[ ucIndex ] >> ucBit ) & 1 );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
inline void Sys_Byte_Set_Array32( uint32_t * aulArray, uint8_t ucByteToSet, uint8_t ucArraySize, uint8_t ucValue )
{
   uint_fast8_t ucIndex = ucByteToSet / 4;
   uint_fast8_t ucByte  = ucByteToSet % 4;
   if( ucIndex < ucArraySize )
   {
      aulArray[ ucIndex ] &= ~( 0xFF << ( ucByte*8 ) );
      aulArray[ ucIndex ] |= ( ucValue << ( ucByte*8 ) );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
inline uint8_t Sys_Byte_Get_Array32( uint32_t * aulArray, uint8_t ucByteToSet, uint8_t ucArraySize )
{
   uint_fast8_t ucIndex = ucByteToSet / 4;
   uint_fast8_t ucByte  = ucByteToSet % 4;
   uint8_t ucValue = (ucIndex < ucArraySize) ? ( ( aulArray[ ucIndex ] >> ( ucByte*8 ) ) & 0xFF ):0;
   return ucValue;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
