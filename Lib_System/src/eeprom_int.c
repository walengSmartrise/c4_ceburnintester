/******************************************************************************
 *
 * @file     eeprom_int.c
 * @brief    Support for internal 4KB EEPROM of LPC4078 MCU.
 * @version  V1.00
 * @date     10, April 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "sys.h"

#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

// See NXP user manual.
#define EEPROM_BASE        (0x00200000UL)

//-------------------------------------------
// Assumes a 120 MHz system clock (cclk).
// See NXP user manual.
#define WAIT_STATE__PHASE1 4
#define WAIT_STATE__PHASE2 6
#define WAIT_STATE__PHASE3 2

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

struct stEEPROM_ControlRegisters
{
   //------------------------------
   // 0x000
         uint8_t  RESERVED1[ 0x80 ]; //Reserved

   //------------------------------
   // EEPROM Registers
   // 0x080
   __IO  uint32_t  CMD;

   // 0x084
   __IO  uint32_t  ADDR;

   // 0x088
   __O   uint32_t  WDATA;

   // 0x08C
   __I   uint32_t  RDATA;

   // 0x090
   __IO  uint32_t  WSTATE;

   // 0x094
   __IO  uint32_t  CLKDIV;

   // 0x098
   __IO  uint32_t  PWRDWN;

   // 0x09C
         uint8_t  RESERVED2[ 0xF3C ]; //Reserved

   //------------------------------
   // EEPROM Interrupt Registers
   // 0xFD8
   __O   uint32_t  INTENCLR;

   // 0xFDC
   __O   uint32_t  INTENSET;

   // 0xFE0
   __I   uint32_t  INTSTAT;

   // 0xFE4
   __I   uint32_t  INTEN;

   // 0xFE8
   __O   uint32_t  INTSTATCLR;

   // 0xFEC
   __O   uint32_t  INTSTATSET;
};

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

   Assumes a 120 MHz system clock (cclk).

 -----------------------------------------------------------------------------*/
void EEPROMI__Init( void )
{
   struct stEEPROM_ControlRegisters *pstControlRegs;

   pstControlRegs = (struct stEEPROM_ControlRegisters *) EEPROM_BASE;

   // Set the Wait State Register.
   pstControlRegs->WSTATE =   WAIT_STATE__PHASE3
                           | (WAIT_STATE__PHASE2 << 8)
                           | (WAIT_STATE__PHASE1 << 16);

   // cclk/375kHz - 1 = 120MHz/375kHz - 1 = 319.
   // See NXP user manual.
   pstControlRegs->CLKDIV = 319;

   // Set the END_OF_PROG1 flag on INTSTAT to indicate the EEPROM is not in the
   // middle of a program cycle. This flag will be our method of checking if
   // the device is busy (0) or ready to accept a new command (1).
   pstControlRegs->INTSTATSET = 0x10000000;

   // Enable Timer0 interrupts on the  Nested Vectored Interrupt Controller.
//   NVIC_EnableIRQ( EEPROM_IRQn ); 
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
int EEPROMI__Read( uint_fast8_t ucPage, void * pvPageData )
{
   int iBusy = 1;  // assume the EEPROM is busy

   struct stEEPROM_ControlRegisters *pstControlRegs;

   pstControlRegs = (struct stEEPROM_ControlRegisters *) EEPROM_BASE;

   // We will use the END_OF_PROG1 flag as our indication that the EEPROM is
   // not busy.
   if ( pstControlRegs->INTSTAT & 0x10000000 )
   {
      iBusy = 0;  // since the END_OF_PROG1 flag is set, the device is not busy

      uint32_t *pauiPageData = (uint32_t *) pvPageData;

      // Set ADDR register to start of EEPROM page. Each read from RDATA will
      // automatically increment the ADDR register by the size of the data read.
      pstControlRegs->ADDR = ucPage * EEPROM_INT__BYTES_PER_PAGE;

      //------------------------------
      // Since the EEPROM page size is a multiple of 32-bits and since this
      // function writes the entire page at once, we will use 32-bit writes to
      // improve speed.
      for ( int i = 0; i < EEPROM_INT__UINT32s_PER_PAGE; ++i )
      {
         pstControlRegs->INTSTATCLR = 0x4000000;  // clear END_OF_RDWR flag on INTSTAT

         pstControlRegs->CMD = 2;  // 32-bit read

         // The NXP user manual recommends against using interrupts for reading
         // and writing the EEPROM since those operations complete very quickly.
         // My own testing shows that the following while loop executes just one
         // time for each 32-bit read command.
         while ( !(pstControlRegs->INTSTAT & 0x4000000) )  // wait for END_OF_RDWR flag
         {
            ;
         }

         *pauiPageData++ = pstControlRegs->RDATA;
      }
   }

   return iBusy;
}

/*-----------------------------------------------------------------------------

   Writes 64-bytes of data pointed to by pvPageData to EEPROM page ucPage.

   ucPage refers to EEPROM page 0 through (EEPROM_INT__NUM_PAGES - 1).

   pvPageData must point to a buffer of at least 64 bytes.

   Return value:
      0 = write command was successfully initiated
      1 = device is busy and unable to execute the write command

 -----------------------------------------------------------------------------*/
int EEPROMI__Write( uint_fast8_t ucPage, void * pvPageData )
{
   int iBusy = 1;  // assume the EEPROM is busy

   struct stEEPROM_ControlRegisters *pstControlRegs;

   pstControlRegs = (struct stEEPROM_ControlRegisters *) EEPROM_BASE;

   // We will use the END_OF_PROG1 flag as our indication that the EEPROM is
   // not busy.
   if ( pstControlRegs->INTSTAT & 0x10000000 )
   {
      iBusy = 0;  // since the END_OF_PROG1 flag is set, the device is not busy

      uint32_t *pauiPageData = (uint32_t *) pvPageData;

      // Set ADDR register to start of EEPROM page. Each write to WDATA will
      // automatically increment the ADDR register by the size of the data
      // written.
      uint32_t uiPageBaseAddress = ucPage * EEPROM_INT__BYTES_PER_PAGE;
      pstControlRegs->ADDR = uiPageBaseAddress;

      //------------------------------
      // Since the EEPROM page size is a multiple of 32-bits and since this
      // function writes the entire page at once, we will use 32-bit writes to
      // improve speed.
      pstControlRegs->CMD = 5;  // 32-bit write

      for ( int i = 0; i < EEPROM_INT__UINT32s_PER_PAGE; ++i )
      {
         pstControlRegs->INTSTATCLR = 0x4000000;  // clear END_OF_RDWR flag on INTSTAT

         pstControlRegs->WDATA = *pauiPageData++;

         // The NXP user manual recommends against using interrupts for reading
         // and writing the EEPROM since those operations complete very quickly.
         // My own testing shows that the following while loop executes just one
         // time for each write to WDATA register.
         while ( !(pstControlRegs->INTSTAT & 0x4000000) )  // wait for END_OF_RDWR flag
         {
            ;
         }
      }

      //------------------------------
      // The above loop just wrote the data to a temporary holding buffer on the
      // EEPROM. The "erase/program" command actually updates the EEPROM's non-
      // volitile memory. This command can take up to 3 ms to execute during
      // which time the device will not accept commands.
      pstControlRegs->ADDR = uiPageBaseAddress;

      pstControlRegs->INTSTATCLR = 0x10000000;  // clear END_OF_PROG1 flag on INTSTAT

      pstControlRegs->CMD = 6;  // erase/program
   }

   return iBusy;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
