/******************************************************************************
 *
 * @file     mod.c
 * @brief    Systen File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "sys.h"
#include "sys_private.h"
#include "param.h"

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstSys_Mod =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 1;
   pstThisModule->uwRunPeriod_1ms = 50;

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   Sys_UpdateTimers();

   return 0;
}

/*-----------------------------------------------------------------------------

   Call the Init function of all modules in pstModuleControl.

 -----------------------------------------------------------------------------*/
void Mod_InitAllModules( struct st_module_control * pstModuleControl )
{
   struct st_module * (* pastModules)[] = pstModuleControl->pastModules;

   for ( int i = 0; i < pstModuleControl->uiNumModules; ++i )
   {
      struct st_module * pstCurrentModule = (*pastModules)[ i ];

      pstCurrentModule->pfnInit( pstCurrentModule );

      pstCurrentModule->iTimeOfNextRun_1ms = pstCurrentModule->uwInitialDelay_1ms;

      pstCurrentModule->iLongestRun_1us = 0;

      pstCurrentModule->bTimeViolation = 0;
   }

   pstModuleControl->uiCurrModIndex = 0;
}

/*-----------------------------------------------------------------------------

   Call the Run function of one module in pstModuleControl. Modules are called
   sequentially and pstModuleControl keeps track of which module ran last time.

 -----------------------------------------------------------------------------*/
void Mod_RunOneModule( struct st_module_control * pstModuleControl )
{
   static int32_t iTimestamp_SuperLoopCycleBegin_1us;
   static uint8_t bFirstCycle = 1;

   uint32_t uiCurrModIndex = pstModuleControl->uiCurrModIndex;

   uint16_t uwTimeViolation_1ms = Param_ReadValue_16Bit( enPARAM16__Time_Violation_1ms );

   float flTimeViolationRate = 1.0 + ( (float) Param_ReadValue_8Bit( enPARAM8__Time_Violation_Rate) / 100 );

   if ( uiCurrModIndex >= pstModuleControl->uiNumModules )
   {
      uiCurrModIndex = 0;

      if ( bFirstCycle )
      {
         bFirstCycle = 0;
      }
      else
      {
         int32_t iElapsedTime_1us = Sys_TickCount_Compare( iTimestamp_SuperLoopCycleBegin_1us, enTickCount_1us );
         iElapsedTime_1us *= -1;

         if ( iElapsedTime_1us > pstModuleControl->iLongestSuperLoopCycleTime_1us )
         {
            pstModuleControl->iLongestSuperLoopCycleTime_1us = iElapsedTime_1us;
         }
      }

      iTimestamp_SuperLoopCycleBegin_1us = Sys_GetTickCount( enTickCount_1us );
   }

   struct st_module * (* pastModules)[] = pstModuleControl->pastModules;

   struct st_module * pstCurrentModule = (*pastModules)[ uiCurrModIndex ];

   // Check the system timer to see if it's time to run the module code.
   int32_t iTimeTilNextRun_1ms = Sys_TickCount_Compare( pstCurrentModule->iTimeOfNextRun_1ms, enTickCount_1ms );

   //------------------------------------------------------------
   // Is it time to run the module code?
   if ( iTimeTilNextRun_1ms <= 0 )
   {
      int32_t iTimestamp_ModuleRunBegin_1us = Sys_GetTickCount( enTickCount_1us );

      pstCurrentModule->pfnRun( pstCurrentModule );

      int32_t iElapsedTime_1us = Sys_TickCount_Compare( iTimestamp_ModuleRunBegin_1us, enTickCount_1us );
      iElapsedTime_1us *= -1;
      if(iElapsedTime_1us > 0)
      {
         pstCurrentModule->fWindowTotal += iElapsedTime_1us;
         if(++pstCurrentModule->ucAverageWindow >= 200)
         {
            pstCurrentModule->fAverageRun_1us =
                  pstCurrentModule->fWindowTotal / pstCurrentModule->ucAverageWindow;
            pstCurrentModule->fWindowTotal = 0;
            pstCurrentModule->ucAverageWindow = 0;
         }
      }
      //------------------------------------------------------------
      if ( iElapsedTime_1us > pstCurrentModule->iLongestRun_1us )
      {
         pstCurrentModule->iLongestRun_1us = iElapsedTime_1us;

         if ( iElapsedTime_1us > pstModuleControl->iModuleWithLongestRunTime_1us )
         {
            pstModuleControl->iModuleWithLongestRunTime_1us = iElapsedTime_1us;
            pstModuleControl->uiModuleWithLongestRunTime_ModIndex = uiCurrModIndex;
         }
      }

      if( ((iElapsedTime_1us / 1000) > uwTimeViolation_1ms) &&
    		uwTimeViolation_1ms)
      {
         pstCurrentModule->bTimeViolation = 1;
      }
      else if( flTimeViolationRate > 1.0 )
      {
         float flPercentageOfRunPeriod = (float) (iElapsedTime_1us / 1000) / (float) pstCurrentModule->uwRunPeriod_1ms;

         if( flPercentageOfRunPeriod > flTimeViolationRate )
         {
            pstCurrentModule->bTimeViolation = 1;
         }
         else
         {
            pstCurrentModule->bTimeViolation = 0;
         }
      }
      else
      {
         pstCurrentModule->bTimeViolation = 0;
      }

      //------------------------------------------------------------
      // Update iTimeTilNextRun_1ms to schedule module's next run.
         // Add the module's uwRunPeriod_1ms to the time it was scheduled to
         // run. This means that if the module gets delayed by a slow
         // super-loop, it will attempt to catch up.
         pstCurrentModule->iTimeOfNextRun_1ms += pstCurrentModule->uwRunPeriod_1ms;

   }

   ++uiCurrModIndex;

   pstModuleControl->uiCurrModIndex = uiCurrModIndex;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
