/******************************************************************************
 *
 * @file     mod_param_eeprom.c
 * @brief    
 * @version  V1.00
 * @date     25, April 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

//#include "mod.h"
#include "sys.h"

#include <stdlib.h>
#include <string.h>

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_ParamEEPROM =
{
   .pfnInit = Init,
   .pfnRun = Run,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

enum en_param_eeprom_states
{
   enPESTATE__CHECK_TEMP_BLOCK,
   enPESTATE__TEMP_PAGE_RESTORE_IN_PROGRESS,
   enPESTATE__INITIAL_VALIDATION,
   enPESTATE__CREATE_RAM_COPY,
   enPESTATE__RUNNING,
};

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
//TODO rm test
//static uint16_t auiTestEEPROM[NUM_PARAM_BLOCKS];
//static uint16_t auiTestRAM[NUM_PARAM_BLOCKS];
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static int ValidateEEPROMI_Page( uint8_t ucEEPROMI_Page, uint32_t *puiCRC )
{
   int iError = 1;

   if ( ucEEPROMI_Page < EEPROM_INT__NUM_PAGES )
   {
      uint8_t aucBuffer[ EEPROM_INT__BYTES_PER_PAGE ];

      EEPROMI__Read( ucEEPROMI_Page, &aucBuffer );

      uint8_t uc1 = aucBuffer[ 4 ];
      uint8_t uc2 = ~aucBuffer[ 5 ];

      if ( uc1 == uc2 )
      {
         iError = 3;

         *puiCRC = Chip_CRC_CRC32( (uint32_t *) &(aucBuffer[ 4 ]),
                                   NUM_OF_CHUNKS_PER_BLOCK);

         if ( *puiCRC == * ((uint32_t *) (&aucBuffer[ 0 ])) )
         {
            iError = 0;
         }
      }
   }

   return iError;
}

/*-----------------------------------------------------------------------------

   Checks if the Temporary Block Page in EEPROMI holds a valid parameter block
   and if so, the block is copied to it's permanent EEPROMI page.

   Returns 0 if a valid block was found in the Temporary Page and is now being
   written to its permanent page.

 -----------------------------------------------------------------------------*/
static int RestoreFromTempBlock( struct st_param_block_info *pstParamBlockInfo )
{
   int iError = 1;

   uint32_t uiCRC;

   if ( ValidateEEPROMI_Page( pstParamBlockInfo->ucEEPROMI_Page__Temp, &uiCRC ) == 0 )
   {
      uint8_t aucBuffer[ EEPROM_INT__BYTES_PER_PAGE ];

      EEPROMI__Read( pstParamBlockInfo->ucEEPROMI_Page__Temp, &aucBuffer );

      uint8_t ucBlockIndex = aucBuffer[ 4 ];

      if ( ucBlockIndex < pstParamBlockInfo->ucNumBlocks )
      {
         uint8_t ucEEPROMI_Page__Dest = Param_BlockToEEPROM_Page( pstParamBlockInfo, ucBlockIndex );

         iError = EEPROMI__Write( ucEEPROMI_Page__Dest, &aucBuffer );
      }
   }

   return iError;
}

/*-----------------------------------------------------------------------------

   Clear the Temporary Block Page to all zeros.

 -----------------------------------------------------------------------------*/
static int ClearTempBlock( struct st_param_block_info *pstParamBlockInfo )
{
   uint8_t aucBuffer[ EEPROM_INT__BYTES_PER_PAGE ];

   memset( &aucBuffer, 0, sizeof( aucBuffer ) );

   return EEPROMI__Write( pstParamBlockInfo->ucEEPROMI_Page__Temp, &aucBuffer );
}

/*-----------------------------------------------------------------------------

   Returns 0 if the page in EEPROMI holding this parameter block is valid.

 -----------------------------------------------------------------------------*/
static int ValidateParamBlock( struct st_param_control *pstParamControl,
                               uint8_t ucBlockIndex
                             )
{
   int iError = 1;

   struct st_param_block_info *pstParamBlockInfo = pstParamControl->pstParamBlockInfo;

   if ( ucBlockIndex < pstParamBlockInfo->ucNumBlocks )
   {
      iError = 2;

      uint_fast8_t ucEEPROMI_Page = Param_BlockToEEPROM_Page( pstParamBlockInfo, ucBlockIndex );

      uint32_t uiCRC;

      if ( ValidateEEPROMI_Page( ucEEPROMI_Page, &uiCRC ) == 0 )
      {
         Param_PutBlockCRC_EEPROM( pstParamControl, ucBlockIndex, uiCRC );

         iError = 0;
      }
   }

   return iError;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static int DefaultTheBlock( struct st_param_control *pstParamControl,
                            uint8_t ucBlockIndex
                          )
{
   int iError = 1;

   struct st_param_block_info *pstParamBlockInfo = pstParamControl->pstParamBlockInfo;

   if ( ucBlockIndex < pstParamBlockInfo->ucNumBlocks )
   {
      iError = 2;

      uint_fast8_t ucEEPROMI_Page = Param_BlockToEEPROM_Page( pstParamBlockInfo, ucBlockIndex );

      uint8_t aucBuffer[ EEPROM_INT__BYTES_PER_PAGE ];

      memset( &aucBuffer, 0, sizeof( aucBuffer ) );

      aucBuffer[ 4 ] = ucBlockIndex;
      aucBuffer[ 5 ] = ~ucBlockIndex;

      uint32_t uiCRC = Chip_CRC_CRC32( (uint32_t *) &(aucBuffer[ 4 ]),
                                       NUM_OF_CHUNKS_PER_BLOCK);

      * ((uint32_t *) (&aucBuffer[ 0 ])) = uiCRC;

      EEPROMI__Write( ucEEPROMI_Page, &aucBuffer );

      Param_PutBlockCRC_EEPROM( pstParamControl, ucBlockIndex, uiCRC );
   }

   return iError;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static int CreateRAM_Copy( struct st_param_control *pstParamControl )
{
   struct st_param_block_info *pstParamBlockInfo = pstParamControl->pstParamBlockInfo;

   pstParamControl->paucParamBlocksInRAM = malloc( pstParamBlockInfo->ucNumBlocks * EEPROM_INT__BYTES_PER_PAGE );

   uint8_t *paucParamBlocksInRAM = pstParamControl->paucParamBlocksInRAM;

   for ( uint_fast8_t ucBlockIndex = 0; ucBlockIndex < pstParamBlockInfo->ucNumBlocks; ++ucBlockIndex )
   {
      uint_fast8_t ucEEPROMI_Page = Param_BlockToEEPROM_Page( pstParamBlockInfo, ucBlockIndex );

      EEPROMI__Read( ucEEPROMI_Page, paucParamBlocksInRAM );

      paucParamBlocksInRAM += EEPROM_INT__BYTES_PER_PAGE;

      /* If parameter block is 32 bit, clear out the unused last 2 bytes of page to make CRC check easier. */
      if( Sys_Param_BlockType( ucBlockIndex ) == enPARAM_BLOCK_TYPE__UINT32 )
      {
         * (uint8_t *) (paucParamBlocksInRAM-1) = 0;
         * (uint8_t *) (paucParamBlocksInRAM-2) = 0;
         pstParamControl->stParamAndValue_Arg.ucBlockIndex = ucBlockIndex;
         UpdateBlockCRC( pstParamControl );
      }
      /* If parameter block is 24 bit, clear out the unused last 1 bytes of page to make CRC check easier. */
      else if( Sys_Param_BlockType( ucBlockIndex ) == enPARAM_BLOCK_TYPE__UINT24 )
      {
         * (uint8_t *) (paucParamBlocksInRAM-1) = 0;
         pstParamControl->stParamAndValue_Arg.ucBlockIndex = ucBlockIndex;
         UpdateBlockCRC( pstParamControl );
      }
   }

   return 0;
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = 50;

   return 0;
}

/*-----------------------------------------------------------------------------

   The EEPROMI can take up to 3 ms to write a page so don't call Run() more
   often than that and never do more than one write operation per call.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   int iError = 0;
   uint8_t ucBlockIndex;
   static uint8_t ucState = enPESTATE__CHECK_TEMP_BLOCK;

   struct st_param_control *pstParamControl = &gstParamControl;
   struct st_param_block_info *pstParamBlockInfo = pstParamControl->pstParamBlockInfo;

   //-----------------------------------------------------------
   switch( ucState )
   {
      //-----------------------------------------------------------
      // This first state checks to see if there is a valid parameter block in
      // the EEPROM's temporary page. Its presence would indicate that a write
      // to the EEPROM was in progress when the MCU was reset/powered off.   
      case enPESTATE__CHECK_TEMP_BLOCK:
         pstParamControl->pauiEEPROM_CRCs = calloc( NUM_PARAM_BLOCKS, sizeof( uint32_t ) );

         // Was a save in progress when CPU reset or powered off?
         if ( RestoreFromTempBlock( pstParamBlockInfo ) == 0 )
         {
            ucState = enPESTATE__TEMP_PAGE_RESTORE_IN_PROGRESS;
         }
         else
         {
            ucState = enPESTATE__INITIAL_VALIDATION;
         }
         break;

      //-----------------------------------------------------------
      // This state is entered if a valid temporary page was found by the
      // initial state enPESTATE__CHECK_TEMP_BLOCK. That initial state would
      // have recovered the temporary data and written it to its permanent
      // page but we still need to delete the temporary page's copy.
      case enPESTATE__TEMP_PAGE_RESTORE_IN_PROGRESS:
         ClearTempBlock( pstParamBlockInfo );

         ucState = enPESTATE__INITIAL_VALIDATION;
         break;


      //-----------------------------------------------------------
      // This state checks each of the parameter pages stored in the EEPROM and
      // verifies that they are valid by checking their CRCs. Any pages with
      // invalid data are defaulted.
      case enPESTATE__INITIAL_VALIDATION:
         for ( ucBlockIndex = 0; ucBlockIndex < NUM_PARAM_BLOCKS; ++ucBlockIndex )
         {
            iError = ValidateParamBlock( pstParamControl, ucBlockIndex );

            if ( iError )
            {
               DefaultTheBlock( pstParamControl, ucBlockIndex );
               break;
            }
         }

         if ( !iError )
         {
            ucState = enPESTATE__CREATE_RAM_COPY;
         }
         break;

      //-----------------------------------------------------------
      // At this point, the parameter blocks stored in the EEPROM all have valid
      // data. We will now copy them to RAM. RAM copies are necessary due to the
      // slowness of the EEPROM device and the page oriented method if accessing
      // its data. Note that we set the flag pstParamControl->bRAM_CopyValid
      // to indicate the RAM copy of the parameters is now valid and ready for
      // use.
      case enPESTATE__CREATE_RAM_COPY:
         CreateRAM_Copy( pstParamControl );

         pstParamControl->bRAM_CopyValid = 1;

         ucState = enPESTATE__RUNNING;
         break;

      //-----------------------------------------------------------
      // Once boot-up is complete, this module will remain in the state
      // enPESTATE__RUNNING. This state simply watches for changes to the RAM
      // copy of the parameters and when detected, updates the EEPROM with the
      // changes. This allows the application code to deal only with the RAM
      // copy of the parameters while this module keeps the EEPROM up to date
      // in the background.
      case enPESTATE__RUNNING:
         for ( ucBlockIndex = 0; ucBlockIndex < NUM_PARAM_BLOCKS; ++ucBlockIndex )
         {
            //TODO rm test
//            auiTestRAM[ucBlockIndex] = (Param_ReadChunk(ucBlockIndex, 14) >> 16) & 0xFFFF;
            pstParamControl->stParamAndValue_Arg.ucBlockIndex = ucBlockIndex;

            uint32_t uiCRC_RAM = Param_GetBlockCRC_RAM( ucBlockIndex );
            uint32_t uiCRC_EEPROM = Param_GetBlockCRC_EEPROM( pstParamControl );
            if ( uiCRC_RAM != uiCRC_EEPROM )
            {
               // After calling Param_CopyBlock_RAM_to_EEPROM() the EEPROM
               // device will be busy for up to 3 ms so break out of the loop
               // and wait until the next time this module runs.
               Param_CopyBlock_RAM_to_EEPROM( pstParamControl );
               break;
            }
         }

         break;

      //-----------------------------------------------------------
      default:  // should never get here
         ucState = enPESTATE__CHECK_TEMP_BLOCK;
         break;
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
