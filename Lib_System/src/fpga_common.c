/******************************************************************************
 *
 * @file     fpga_common.c
 * @brief    FPGA communication initialization and data access
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "fpga_api.h"
#include "ring_buffer.h"
#include <string.h>
#include "sys.h"
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define GPIO_INTERRUPT_CS        (6)   /* GPIO pin number mapped to interrupt */
#define GPIO_INTERRUPT_CLK       (7)
#define GPIO_INTERRUPT_MOSI      (9)
#define GPIO_INTERRUPT_MISO      (8)
#define GPIO_INTERRUPT_PORT      GPIOINT_PORT0    /* GPIO port number mapped to interrupt */
#define GPIO_IRQ_HANDLER         GPIO_IRQHandler/* GPIO interrupt IRQ function name */
#define GPIO_INTERRUPT_NVIC_NAME GPIO_IRQn   /* GPIO interrupt NVIC interrupt name */

#define RING_BUFFER_SIZE  (12)

#define ExtractByte3(unsignedInt) (( (0xFF000000 & unsignedInt) >> 24))
#define ExtractByte2(unsignedInt) (( (0x00FF0000 & unsignedInt) >> 16))
#define ExtractByte1(unsignedInt) (( (0x0000FF00 & unsignedInt) >> 8 ))
#define ExtractByte0(unsignedInt) (( (0x000000FF & unsignedInt) >> 0 ))

/* CPLD V3 definitons */
#define CPLD_V3_SPI_REGISTER              (LPC_SSP1)
#define CPLD_V3_SPI_IRQ                   (SSP1_IRQn)
#define CPLD_V3_SPI_CLOCK_CTRL            (SYSCTL_CLOCK_SSP1)
#define CPLD_V3_SPI_BAUD_RATE             (2400)
#define CPLD_V3_SPI_UNLOAD_BUFFER_SIZE    (24)


/* CPLD packet encodings */
#define CPLD_RAW_PACKET_START_BYTE            (0xFF) // STX
#define CPLD_RAW_PACKET_END_BYTE              (0x80) // ETX
#define CPLD_RAW_PACKET_SIZE_IN_BYTES         (16)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint32_t fpgaRawBitstream[4];

static RINGBUFF_T RxRingBuffer_FPGA;
static FPGA_Packet RxBuffer[RING_BUFFER_SIZE];

static FPGA_Packet astFPGA_Packets[NUM_FPGA_LOC];

static uint8_t bBufferOverflow;

/* CPLD V3 unload variables */
static uint8_t aucCPLD_V3_UnloadBuffer[CPLD_V3_SPI_UNLOAD_BUFFER_SIZE];
static uint8_t ucCPLD_V3_BufferIndex;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   Buffer overflow flag access functions
 *----------------------------------------------------------------------------*/
uint8_t FPGA_GetBufferOverflowFlag(void)
{
   return bBufferOverflow;
}
void FPGA_ClrBufferOverflowFlag(void)
{
   bBufferOverflow = 0;
}
/*----------------------------------------------------------------------------
   Access functions for the most recently unloaded packets.
 *----------------------------------------------------------------------------*/
void FPGA_SetRecentPacket( FPGA_Packet *pstPacket, en_fpga_locations eLoc )
{
   memcpy(&astFPGA_Packets[eLoc], pstPacket, sizeof(FPGA_Packet));
}
void FPGA_GetRecentPacket( FPGA_Packet *pstPacket, en_fpga_locations eLoc )
{
   memcpy(pstPacket, &astFPGA_Packets[eLoc], sizeof(FPGA_Packet));
}
/* Unload compressed FPGA packet data sent to the UI for display. */
void FPGA_UnloadUIData( uint32_t uiData, uint8_t ucIndex, en_fpga_locations eLoc )
{
   en_fpga_bytes eByteIndex_Start = ucIndex*NUM_BYTES_PER_BITMAP32;
   for(uint8_t i = 0; i < NUM_BYTES_PER_BITMAP32; i++)
   {
      uint8_t ucData = GET_BYTE_U32(uiData, i);
      uint8_t ucByteIndex = eByteIndex_Start+i;
      if(ucByteIndex < NUM_FPGA_BYTES)
      {
         astFPGA_Packets[eLoc].aucData[eByteIndex_Start+i] = ucData;
      }
   }
}
uint8_t FPGA_GetFault( en_fpga_locations eLoc )
{
   uint8_t ucFault;
   if( Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V2) || Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) )
   {
      ucFault = astFPGA_Packets[eLoc].aucData[FPGA_BYTE__DATA10];
   }
   else
   {
      ucFault = astFPGA_Packets[eLoc].aucData[FPGA_BYTE__DATA11];
   }

   return ucFault;
}
PreflightStatus FPGA_GetPreflightStatus( en_fpga_locations eLoc )
{
   PreflightStatus eStatus;
   if( Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V2) || Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) )
   {
      eStatus = ( astFPGA_Packets[eLoc].aucData[FPGA_BYTE__DATA8] ) & 0x03;
   }
   else
   {
      eStatus = ( astFPGA_Packets[eLoc].aucData[FPGA_BYTE__DATA12] ) & 0x03;
   }

   return eStatus;
}
PreflightMCU_Command FPGA_GetPreflightCommand( en_fpga_locations eLoc )
{
   PreflightMCU_Command eCommand;
   if( Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V2) || Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) )
   {
      eCommand = ( astFPGA_Packets[eLoc].aucData[FPGA_BYTE__DATA7] >> 3 ) & 0x07;
   }
   else
   {
      if( eLoc == FPGA_LOC__MRA )
      {
         eCommand = ( astFPGA_Packets[eLoc].aucData[FPGA_BYTE__DATA5] >> 1 ) & 0x07;
      }
      else
      {
         eCommand = 0;
      }
   }
   return eCommand;
}
PreflightErrors FPGA_GetPreflightErrors( en_fpga_locations eLoc )
{
   PreflightErrors eError;
   if( Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V2) || Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) )
   {
      eError = ( astFPGA_Packets[eLoc].aucData[FPGA_BYTE__DATA8] >> 2 ) & 0x0F;
   }
   else
   {
      eError = PREFLIGHT_ERROR__NONE;
   }

   return eError;
}
/* Bits 0 to 3 are the version number.
 * Bits 4 to 5 are the board number. (1 = MR, 2 = CT, 3 = COP) */
uint8_t FPGA_GetVersion( en_fpga_locations eLoc )
{
   uint8_t ucVersion;
   if( Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V2) || Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) )
   {
      ucVersion = astFPGA_Packets[eLoc].aucData[FPGA_BYTE__DATA11];
   }
   else
   {
      ucVersion = 0;
   }
   return ucVersion;
}
uint8_t FPGA_GetDataBit( en_fpga_locations eLoc, uint8_t ucBit )
{
   uint8_t ucByteIndex = ucBit / FPGA_DATA_BITS_PER_BYTE;
   uint8_t ucBitIndex = ucBit % FPGA_DATA_BITS_PER_BYTE;
   uint8_t bActive = ( astFPGA_Packets[eLoc].aucData[ucByteIndex] >> ucBitIndex ) & 1;
   return bActive;
}
uint8_t FPGA_GetDataRange( en_fpga_locations eLoc, uint8_t ucBit_Start, uint8_t ucBit_End )
{
   uint8_t ucData = 0;
   for(uint8_t ucBit = ucBit_Start; ucBit <= ucBit_End; ucBit++)
   {
      uint8_t ucShift = ucBit - ucBit_Start;
      uint8_t ucByteIndex = ucBit / FPGA_DATA_BITS_PER_BYTE;
      uint8_t ucBitIndex = ucBit % FPGA_DATA_BITS_PER_BYTE;
      uint8_t bDataBit = ( astFPGA_Packets[eLoc].aucData[ucByteIndex] >> ucBitIndex ) & 1;
      ucData |= bDataBit << ucShift;
   }
   return ucData;
}
/*----------------------------------------------------------------------------
   Inserts a newly recieved bit into the raw bitstream buffer
 *----------------------------------------------------------------------------*/
static void InsertBit(uint32_t* msgBuffer, uint8_t bit)
{
    // Insert the bit in into the LSB position
    msgBuffer[3] = (msgBuffer[3]<<1) | (0x00000001 & (msgBuffer[2]>>31));
    msgBuffer[2] = (msgBuffer[2]<<1) | (0x00000001 & (msgBuffer[1]>>31));
    msgBuffer[1] = (msgBuffer[1]<<1) | (0x00000001 & (msgBuffer[0]>>31));
    msgBuffer[0] = (msgBuffer[0]<<1) | (0x01 & bit);
}

/*----------------------------------------------------------------------------
   Checks the raw bit stream buffer for a complete and valid packet.
 *----------------------------------------------------------------------------*/
uint8_t ValidFPGAMessage( uint32_t* msgBuffer)
{
    uint8_t byte15 = ExtractByte3(msgBuffer[3]);
    uint8_t byte14 = ExtractByte2(msgBuffer[3]);
    uint8_t byte13 = ExtractByte1(msgBuffer[3]);
    uint8_t byte12 = ExtractByte0(msgBuffer[3]);

    uint8_t byte11 = ExtractByte3(msgBuffer[2]);
    uint8_t byte10 = ExtractByte2(msgBuffer[2]);
    uint8_t byte9  = ExtractByte1(msgBuffer[2]);
    uint8_t byte8  = ExtractByte0(msgBuffer[2]);

    uint8_t byte7  = ExtractByte3(msgBuffer[1]);
    uint8_t byte6  = ExtractByte2(msgBuffer[1]);
    uint8_t byte5  = ExtractByte1(msgBuffer[1]);
    uint8_t byte4  = ExtractByte0(msgBuffer[1]);

    uint8_t byte3  = ExtractByte3(msgBuffer[0]);
    uint8_t byte2  = ExtractByte2(msgBuffer[0]);
    uint8_t byte1  = ExtractByte1(msgBuffer[0]);
    uint8_t byte0  = ExtractByte0(msgBuffer[0]);


   // Start and stop characters are 0x11111111 and 0x10000000
   uint8_t validStartAndStop = ((byte0 == 0x80 ) && (byte15 == 0xFF));

   // xor all of the data bytes. this xor the bcc byte should be 0.
   // If not zero then problem in transmission.
   // Only xor bytes 1 to 14. Bytes 0 and 15 are the ETX and STX characters.
   uint8_t validBCC = !(
                           byte1
                         ^ byte2
                         ^ byte3
                         ^ byte4
                         ^ byte5
                         ^ byte6
                         ^ byte7
                         ^ byte8
                         ^ byte9
                         ^ byte10
                         ^ byte11
                         ^ byte12
                         ^ byte13
                         ^ byte14
                       );

   return (validStartAndStop && validBCC);

}

/*----------------------------------------------------------------------------
   Initialize FPGA communication and communication buffer
 *----------------------------------------------------------------------------*/
static void RingBufferInit_FPGA( void )
{
    // Init the Rx Ring buffer.
    RingBuffer_Init( &RxRingBuffer_FPGA, RxBuffer, sizeof ( RxBuffer[0] ), RING_BUFFER_SIZE );
}

/* Run when Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) is OFF */
void FPGA_Init_V1_V2( void )
{
    // Set up the ring buffers
    RingBufferInit_FPGA();

    /* Configure GPIO interrupt pin as input */
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, GPIO_INTERRUPT_PORT, GPIO_INTERRUPT_CS);
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, GPIO_INTERRUPT_PORT, GPIO_INTERRUPT_CLK);
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, GPIO_INTERRUPT_PORT, GPIO_INTERRUPT_MOSI);
    Chip_GPIO_SetPinDIROutput(LPC_GPIO, GPIO_INTERRUPT_PORT, GPIO_INTERRUPT_MISO);
    /* Configure the GPIO interrupt */
    //Chip_GPIOINT_SetIntFalling(LPC_GPIOINT, GPIO_INTERRUPT_PORT, 1 << GPIO_INTERRUPT_CS );
    Chip_GPIOINT_SetIntRising (LPC_GPIOINT, GPIO_INTERRUPT_PORT, 1 << GPIO_INTERRUPT_CLK);

    /* Enable interrupt in the NVIC */
    NVIC_ClearPendingIRQ(GPIO_INTERRUPT_NVIC_NAME);
#if INCREASED_PRIORITY_FOR_BITBANGED_CPLD_SPI
    NVIC_SetPriority(GPIO_INTERRUPT_NVIC_NAME, 0);
#endif
    NVIC_EnableIRQ(GPIO_INTERRUPT_NVIC_NAME);
}

/* Run when Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) is ON */
void FPGA_Init_V3( void )
{
   /* Initialize ring buffer */
   RingBufferInit_FPGA();

   /* Initialize SPI pins */
   Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 9, IOCON_FUNC2 | IOCON_DIGMODE_EN); // SSP1_MOSI
   Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 8, IOCON_FUNC2 | IOCON_DIGMODE_EN); // SSP1_MISO
   Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 7, IOCON_FUNC2 | IOCON_DIGMODE_EN); // SSP1_SCK
   Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 6, IOCON_FUNC2); // SSP1_SSEL

   /* Initialize SPI peripheral */
   Chip_Clock_EnablePeriphClock(CPLD_V3_SPI_CLOCK_CTRL);
   Chip_SSP_Set_Mode(CPLD_V3_SPI_REGISTER, SSP_MODE_SLAVE);
   Chip_SSP_SetFormat(CPLD_V3_SPI_REGISTER, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_CPHA1_CPOL1);
   Chip_SSP_SetBitRate(CPLD_V3_SPI_REGISTER, CPLD_V3_SPI_BAUD_RATE);
   Chip_SSP_DisableLoopBack(CPLD_V3_SPI_REGISTER);

   /* Slave Output Disable. Prevents the SSP controller from driving the transmit data line MISO */
   CPLD_V3_SPI_REGISTER->CR1 |= SSP_CR1_SO_DISABLE;

   Chip_SSP_Enable(CPLD_V3_SPI_REGISTER);
   CPLD_V3_SPI_REGISTER->IMSC = SSP_RXIM; /* Enable just the RX interrupt */
   Chip_SSP_Int_FlushData(CPLD_V3_SPI_REGISTER);

   /* Initialize system interrupt */
   NVIC_ClearPendingIRQ(CPLD_V3_SPI_IRQ);
   NVIC_EnableIRQ(CPLD_V3_SPI_IRQ);
}
/*----------------------------------------------------------------------------
   SPI Interrupt Function when Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) is ON
 *----------------------------------------------------------------------------*/
void SSP1_IRQHandler( void )
{
   for(uint8_t i = 0; i < CPLD_V3_SPI_UNLOAD_BUFFER_SIZE; i++)
   {
      if( Chip_SSP_GetStatus(CPLD_V3_SPI_REGISTER, SSP_STAT_RNE) == SET )
      {
         uint16_t uwRawData = Chip_SSP_ReceiveFrame(CPLD_V3_SPI_REGISTER);
         uint8_t ucData = uwRawData & 0xFF;
         aucCPLD_V3_UnloadBuffer[ucCPLD_V3_BufferIndex] = ucData;

         if( ucData == CPLD_RAW_PACKET_END_BYTE )
         {
            uint8_t ucIndex_Start = ( ucCPLD_V3_BufferIndex >= ( CPLD_RAW_PACKET_SIZE_IN_BYTES-1 ) )
                                  ? ( ucCPLD_V3_BufferIndex - ( CPLD_RAW_PACKET_SIZE_IN_BYTES-1 ) )
                                  : ( CPLD_V3_SPI_UNLOAD_BUFFER_SIZE - ( CPLD_RAW_PACKET_SIZE_IN_BYTES - ucCPLD_V3_BufferIndex - 1 ) );

            uint8_t ucValue_Start = aucCPLD_V3_UnloadBuffer[ucIndex_Start];
            if( ucValue_Start == CPLD_RAW_PACKET_START_BYTE )
            {
               FPGA_Packet stPacket;
               stPacket.aucData[FPGA_BYTE__DATA0] = aucCPLD_V3_UnloadBuffer[(ucIndex_Start+1)%CPLD_V3_SPI_UNLOAD_BUFFER_SIZE];
               stPacket.aucData[FPGA_BYTE__DATA1] = aucCPLD_V3_UnloadBuffer[(ucIndex_Start+2)%CPLD_V3_SPI_UNLOAD_BUFFER_SIZE];
               stPacket.aucData[FPGA_BYTE__DATA2] = aucCPLD_V3_UnloadBuffer[(ucIndex_Start+3)%CPLD_V3_SPI_UNLOAD_BUFFER_SIZE];
               stPacket.aucData[FPGA_BYTE__DATA3] = aucCPLD_V3_UnloadBuffer[(ucIndex_Start+4)%CPLD_V3_SPI_UNLOAD_BUFFER_SIZE];
               stPacket.aucData[FPGA_BYTE__DATA4] = aucCPLD_V3_UnloadBuffer[(ucIndex_Start+5)%CPLD_V3_SPI_UNLOAD_BUFFER_SIZE];
               stPacket.aucData[FPGA_BYTE__DATA5] = aucCPLD_V3_UnloadBuffer[(ucIndex_Start+6)%CPLD_V3_SPI_UNLOAD_BUFFER_SIZE];
               stPacket.aucData[FPGA_BYTE__DATA6] = aucCPLD_V3_UnloadBuffer[(ucIndex_Start+7)%CPLD_V3_SPI_UNLOAD_BUFFER_SIZE];
               stPacket.aucData[FPGA_BYTE__DATA7] = aucCPLD_V3_UnloadBuffer[(ucIndex_Start+8)%CPLD_V3_SPI_UNLOAD_BUFFER_SIZE];
               stPacket.aucData[FPGA_BYTE__DATA8] = aucCPLD_V3_UnloadBuffer[(ucIndex_Start+9)%CPLD_V3_SPI_UNLOAD_BUFFER_SIZE];
               stPacket.aucData[FPGA_BYTE__DATA9] = aucCPLD_V3_UnloadBuffer[(ucIndex_Start+10)%CPLD_V3_SPI_UNLOAD_BUFFER_SIZE];
               stPacket.aucData[FPGA_BYTE__DATA10] = aucCPLD_V3_UnloadBuffer[(ucIndex_Start+11)%CPLD_V3_SPI_UNLOAD_BUFFER_SIZE];
               stPacket.aucData[FPGA_BYTE__DATA11] = aucCPLD_V3_UnloadBuffer[(ucIndex_Start+12)%CPLD_V3_SPI_UNLOAD_BUFFER_SIZE];
               stPacket.aucData[FPGA_BYTE__DATA12] = aucCPLD_V3_UnloadBuffer[(ucIndex_Start+13)%CPLD_V3_SPI_UNLOAD_BUFFER_SIZE];

               /* BCC calculated by XOR of byte index 1 to byte index 13 should equal the one recieved from the CPLD in byte index 14 */
               uint8_t ucValue_BCC = aucCPLD_V3_UnloadBuffer[(ucIndex_Start+14)%CPLD_V3_SPI_UNLOAD_BUFFER_SIZE];

               for( uint8_t ucByteIndex = FPGA_BYTE__DATA0; ucByteIndex <= FPGA_BYTE__DATA12; ucByteIndex++ )
               {
                  ucValue_BCC ^= stPacket.aucData[ucByteIndex];
               }
               if( !ucValue_BCC )
               {
                  if(!RingBuffer_Insert( &RxRingBuffer_FPGA, &stPacket ))
                  {
                     bBufferOverflow = 1;
                  }
               }
            }
         }

         ucCPLD_V3_BufferIndex = ( ucCPLD_V3_BufferIndex+1 ) % CPLD_V3_SPI_UNLOAD_BUFFER_SIZE;
      }
      else
      {
         break;
      }
   }

   Chip_SSP_ClearIntPending(LPC_SSP1, SSP_INT_CLEAR_BITMASK);
   NVIC_ClearPendingIRQ(CPLD_V3_SPI_IRQ);
}
/*----------------------------------------------------------------------------
   Bit banged SPI communcation with the FPGA
   // Note: currently no serial communication from the MCU to the FPGA
 *----------------------------------------------------------------------------*/
void GPIO_IRQHandler( void )
{
    // Get the cause of the interrupt and also clear the gpio interrupt
    uint8_t ubInterrupt_clock      = Chip_GPIO_GetPinState(LPC_GPIO,GPIO_INTERRUPT_PORT,GPIO_INTERRUPT_CLK);
    uint8_t ubInterrupt_chipSelect = Chip_GPIO_GetPinState(LPC_GPIO,GPIO_INTERRUPT_PORT,GPIO_INTERRUPT_CS);
    Chip_GPIOINT_ClearIntStatus(LPC_GPIOINT, GPIO_INTERRUPT_PORT, 1 << GPIO_INTERRUPT_CS);
    Chip_GPIOINT_ClearIntStatus(LPC_GPIOINT, GPIO_INTERRUPT_PORT, 1 << GPIO_INTERRUPT_CLK);

    // If the "SCK" is on a rising edge then start transferring the data to the raw FPGA variable
    // If the "SCK" is on a rising edge then start transferring the data to the raw FPGA variable
    if( ubInterrupt_clock && !ubInterrupt_chipSelect)
    {
        // Read the the data from the "MOSI" pin
        uint8_t ubBitFromFPGA = Chip_GPIO_GetPinState(LPC_GPIO,GPIO_INTERRUPT_PORT,GPIO_INTERRUPT_MOSI);

        // Data from FPGA is sent MSB first. So it needs to be placed into the array MSB first
        // or in the new bit in the lsb position. Move the MSB to the next int in the array.
        InsertBit(fpgaRawBitstream,ubBitFromFPGA );

        // Check the raw bit stream for a full and valid packet. Unload to a ring buffer if found.
        if( ValidFPGAMessage(fpgaRawBitstream) )
        {
            FPGA_Packet RcvMsg;
            RcvMsg.aucData[FPGA_BYTE__DATA0]  = ExtractByte2(fpgaRawBitstream[3]);  // data 0
            RcvMsg.aucData[FPGA_BYTE__DATA1]  = ExtractByte1(fpgaRawBitstream[3]);  // data 1
            RcvMsg.aucData[FPGA_BYTE__DATA2]  = ExtractByte0(fpgaRawBitstream[3]);  // data 2
            RcvMsg.aucData[FPGA_BYTE__DATA3]  = ExtractByte3(fpgaRawBitstream[2]);  // data 3
            RcvMsg.aucData[FPGA_BYTE__DATA4]  = ExtractByte2(fpgaRawBitstream[2]);  // data 4
            RcvMsg.aucData[FPGA_BYTE__DATA5]  = ExtractByte1(fpgaRawBitstream[2]);  // data 5
            RcvMsg.aucData[FPGA_BYTE__DATA6]  = ExtractByte0(fpgaRawBitstream[2]);  // data 6
            RcvMsg.aucData[FPGA_BYTE__DATA7]  = ExtractByte3(fpgaRawBitstream[1]);  // data 7
            RcvMsg.aucData[FPGA_BYTE__DATA8]  = ExtractByte2(fpgaRawBitstream[1]);  // data 8
            RcvMsg.aucData[FPGA_BYTE__DATA9]  = ExtractByte1(fpgaRawBitstream[1]);  // data 9
            RcvMsg.aucData[FPGA_BYTE__DATA10] = ExtractByte0(fpgaRawBitstream[1]);  // data 10
            RcvMsg.aucData[FPGA_BYTE__DATA11] = ExtractByte3(fpgaRawBitstream[0]);  // system status
            RcvMsg.aucData[FPGA_BYTE__DATA12] = ExtractByte2(fpgaRawBitstream[0]);  // system status extra byte

            if(!RingBuffer_Insert( &RxRingBuffer_FPGA, &RcvMsg ))
            {
               bBufferOverflow = 1;
            }
        }
    }
}

/*----------------------------------------------------------------------------
   Checks if a full FPGA packet has been recieved. If so, unloads
   it from the ring buffer and passes it to a global access buffer
 *---------------------------------------------------------------------------*/
uint8_t FPGA_UnloadData( en_fpga_locations eLoc )
{
    uint8_t bPacketRecieved = 0;
    FPGA_Packet stPacket;
    /* Take the most recent element of the ring buffer */
    for(uint8_t i = 0; i < RING_BUFFER_SIZE; i++)
    {
       if( RingBuffer_Pop( &RxRingBuffer_FPGA, &stPacket ) )
       {
          bPacketRecieved = 1;
       }
       else
       {
          break;
       }
    }

    /* Each data byte only uses the middle 6 bits.
     * drop the MS and LS bits, append each byte together,
     *  and load it to the global buffer. */
    if( bPacketRecieved )
    {
       for(uint8_t i = 0; i < NUM_FPGA_BYTES; i++)
       {
          uint8_t ucData = FPGA_BYTE_EXTRACT_DATA( stPacket.aucData[i] );
          stPacket.aucData[i] = ucData;
       }
       FPGA_SetRecentPacket(&stPacket, eLoc);
    }

    return bPacketRecieved;
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
