#include "sys.h"
#include "sru.h"

//-----------------------------------------------
static const uint8_t aucFunctionsPerInputGroup[ NUM_INPUT_GROUPS ] =
{
      1,//INPUT_GROUP__NONE,
      NUM_CIG_AUTO_OPER,//INPUT_GROUP__AUTO_OP,
      NUM_CIG_DOORS,//INPUT_GROUP__DOORS_F,
      NUM_CIG_DOORS,//INPUT_GROUP__DOORS_R,
      NUM_CIG_FIRE_EQ,//INPUT_GROUP__FIRE,
      NUM_CIG_INSP,//INPUT_GROUP__INSP,
      NUM_CIG_CTRL,//INPUT_GROUP__CTRL,
      NUM_CIG_SAFETY,//INPUT_GROUP__SAFETY,
      MAX_NUM_FLOORS,//INPUT_GROUP__CCB_F,
      MAX_NUM_FLOORS,//INPUT_GROUP__CC_ENABLE_F,
      MAX_NUM_FLOORS,//INPUT_GROUP__CCB_R,
      MAX_NUM_FLOORS,//INPUT_GROUP__CC_ENABLE_R,
      NUM_CIG_EPWR,//INPUT_GROUP__EPWR,
};
static const char * const pasStrings_CIG[ NUM_INPUT_GROUPS ] =
{
      "<unused>",
      "Auto Operation",
      "Doors (F)",
      "Doors (R)",
      "Fire EQ",
      "Inspect/Access",
      "Controller",
      "Safety",
      "CC Button (F)",
      "CC Enable (F)",
      "CC Button (R)",
      "CC Enable (R)",
      "E-Power",
};
//-----------------------------------------------
static const enum en_input_functions aeCIG_Mapping_AutoOper[NUM_CIG_AUTO_OPER] =
{
      ///* Fixed Inputs */
      ///* Unfixed Inputs */
      enIN_INDP,//CIG_AUTO_OPER__INDP,
      enIN_CAR_TO_LOBBY,//CIG_AUTO_OPER__CAR_TO_LOBBY,
      enIN_LIGHT_LOAD,//CIG_AUTO_OPER__LIGHT_LOAD,
      enIN_SABBATH,//CIG_AUTO_OPER__SABBATH,
      enIN_SECURITY_ENABLE_ALL_CAR_CALLS,//CIG_AUTO_OPER__ENABLE_ALL_CC,
      enIN_SWING_ENABLE_KEY,//CIG_AUTO_OPER__SWING,
      enIN_CUSTOM,//CIG_AUTO_OPER__CUSTOM,
      enIN_EMS2_ON,//CIG_AUTO_OPER__EMS2_ON,
      enIN_PARKING_OFF,//CIG_AUTO_OPER__PARKING_OFF,
      enIN_ATTD_ON,//CIG_AUTO_OPER__ATTD_ON,
      enIN_ATTD_UP,//CIG_AUTO_OPER__ATTD_UP,
      enIN_ATTD_DN,//CIG_AUTO_OPER__ATTD_DN,
      enIN_ATTD_BYP,//CIG_AUTO_OPER__ATTD_BYP,
      enIN_WANDER_GUARD,//CIG_AUTO_OPER__WANDER_GUARD,
      enIN_EnableAllHC,//CIG_AUTO_OPER__ENABLE_ALL_HC,
      enIN_PASSING_CHIME_DISABLE,//CIG_AUTO_OPER__PASSING_CHIME_DISABLE,
      enIN_DISABLE_ALL_HC,//CIG_AUTO_OPER__DISABLE_ALL_HC,
      enIN_ENABLE_LOBBY_PEAK, //CIG_AUTO_OPER__ENABLE_LOBBY_PEAK
      enIN_ENABLE_UP_PEAK, //CIG_AUTO_OPER__ENABLE_UP_PEAK
      enIN_ENABLE_DOWN_PEAK, //CIG_AUTO_OPER__ENABLE_DOWN_PEAK
      enIN_MARSHAL_MODE, //CIG_AUTO_OPER__MARSHAL_MODE
      enIN_ACTIVE_SHOOTER_MODE, // CIG_AUTO_OPER__ACTIVE_SHOOTER_MODE
      enIN_DISTRESS_BUTTON, //CIG_AUTO_OPER__DISTRESS_BUTTON,
      enIN_DISTRESS_ACKNOWLEDGE, //CIG_AUTO_OPER__DISTRESS_ACKNOWLEDGE,
      enIN_ENABLE_ALL_CC_FRONT, //CIG_AUTO_OPER__ALL_CC_FRONT,
      enIN_ENABLE_ALL_CC_REAR,// CIG_AUTO_OPER__ALL_CC_FRONT,
};

static const uint16_t auwFixedTerminal_Plus1_Auto[ NUM_CIG_AUTO_OPER ] =
{
      ///* Fixed Inputs */
      ///* Unfixed Inputs */
      0,//CIG_AUTO_OPER__INDP,
      0,//CIG_AUTO_OPER__CAR_TO_LOBBY,
      0,//CIG_AUTO_OPER__LIGHT_LOAD,
      0,//CIG_AUTO_OPER__SABBATH,
      0,//CIG_AUTO_OPER__ENABLE_ALL_CC,
      0,//CIG_AUTO_OPER__SWING,
      0,//CIG_AUTO_OPER__CUSTOM,
      0,//CIG_AUTO_OPER__EMS2_ON,
      0,//CIG_AUTO_OPER__PARKING_OFF,
      0,//CIG_AUTO_OPER__ATTD_ON,
      0,//CIG_AUTO_OPER__ATTD_UP,
      0,//CIG_AUTO_OPER__ATTD_DN,
      0,//CIG_AUTO_OPER__ATTD_BYP,
      0,//CIG_AUTO_OPER__WANDER_GUARD,
      0,//CIG_AUTO_OPER__ENABLE_ALL_HC,
      0,//CIG_AUTO_OPER__PASSING_CHIME_DISABLE,
      0,//CIG_AUTO_OPER__DISABLE_ALL_HC,
      0, //CIG_AUTO_OPER__ENABLE_LOBBY_PEAK
      0, //CIG_AUTO_OPER__ENABLE_UP_PEAK
      0, //CIG_AUTO_OPER__ENABLE_DOWN_PEAK
      0, //CIG_AUTO_OPER__MARSHAL_MODE
      0, //CIG_AUTO_OPER__ACTIVE_SHOOTER_MODE
      0, //CIG_AUTO_OPER__DISTRESS_BUTTON,
      0, //CIG_AUTO_OPER__DISTRESS_ACKNOWLEDGE,
      0, //CIG_AUTO_OPER__ENABLE_ALL_CC_FRONT,
      0, //CIG_AUTO_OPER__ENABLE_ALL_CC_REAR,
};
static const char * const pasInputStrings_AutoOper[ NUM_CIG_AUTO_OPER ] =
{
      ///* Fixed Inputs */
      ///* Unfixed Inputs */
      "Indep Srv",//CIG_AUTO_OPER__INDP,
      "Car To Lobby",//CIG_AUTO_OPER__CAR_TO_LOBBY,
      "Light Load",//CIG_AUTO_OPER__LIGHT_LOAD,
      "Sabbath",//CIG_AUTO_OPER__SABBATH,
      "Enable All CC",//CIG_AUTO_OPER__ENABLE_ALL_CC,
      "Enable Swing",//CIG_AUTO_OPER__SWING,
      "Custom Oper",//CIG_AUTO_OPER__CUSTOM,
      "EMS2 On",//CIG_AUTO_OPER__EMS2_ON,
      "Parking Off",//CIG_AUTO_OPER__PARKING_OFF,
      "Attd On",//CIG_AUTO_OPER__ATTD_ON,
      "Attd Up",//CIG_AUTO_OPER__ATTD_UP,
      "Attd Down",//CIG_AUTO_OPER__ATTD_DN,
      "Attd Byp",//CIG_AUTO_OPER__ATTD_BYP,
      "Wander Guard",//CIG_AUTO_OPER__WANDER_GUARD,
      "Enable All HC",//CIG_AUTO_OPER__ENABLE_ALL_HC,
      "DISA Pass Chime",//CIG_AUTO_OPER__PASSING_CHIME_DISABLE,
      "DISA All HC",//CIG_AUTO_OPER__DISABLE_ALL_HC,
      "ENA Lobby Peak", //CIG_AUTO_OPER__ENABLE_LOBBY_PEAK
      "ENA Up Peak", //CIG_AUTO_OPER__ENABLE_UP_PEAK
      "ENA Down Peak", //CIG_AUTO_OPER__ENABLE_DOWN_PEAK
      "Marshal Mode", //CIG_AUTO_OPER__MARSHAL_MODE
      "Active Shooter Mode", // CIG_AUTO_OPER__ACTIVE_SHOOTER_MODE
      "Distress BTN", //CIG_AUTO_OPER__DISTRESS_BUTTON,
      "Distress ACK", //CIG_AUTO_OPER__DISTRESS_ACKNOWLEDGE,
      "Enable All CC Front", //CIG_AUTO_OPER__ENABLE_ALL_CC_FRONT,
      "Enable All CC Rear", //CIG_AUTO_OPER__ENABLE_ALL_CC REAR,
};
//-----------------------------------------------
static const enum en_input_functions aeCIG_Mapping_DoorFront[NUM_CIG_DOORS] =
{
      ///* Fixed Inputs */
      enIN_GSWF,//CIG_DOORS__GSW,
      enIN_DZ_F,//CIG_DOORS__DZ,
      ///* Unfixed Inputs */
      enIN_DPM_F,//CIG_DOORS__DPM,
      enIN_DCL_F,//CIG_DOORS__DCL,
      enIN_DOL_F,//CIG_DOORS__DOL,
      enIN_PHE_F,//CIG_DOORS__PHE,
      enIN_DCB_F,//CIG_DOORS__DCB,
      enIN_DOB_F,//CIG_DOORS__DOB,
      enIN_DoorHold_F,//CIG_DOORS__HOLD,
      enIN_SafetyEdge_F,//CIG_DOORS__SE
      enIN_PHE_F2,//CIG_DOORS__PHE_ALT
      enIN_TCL_F,//CIG_DOORS__TCL,
      enIN_MCL_F,//CIG_DOORS__MCL,
      enIN_BCL_F,//CIG_DOORS__BCL,
};
static const uint16_t auwFixedTerminal_Plus1_DoorFront[ NUM_CIG_DOORS ] =
{
      ///* Fixed Inputs */
      TERMINAL_INDEX_START__CTA + 1,//CIG_DOORS__GSW,
      TERMINAL_INDEX_START__CTA + 2 + 1,//CIG_DOORS__DZ,
      ///* Unfixed Inputs */
      0,//CIG_DOORS__DPM,
      0,//CIG_DOORS__DCL,
      0,//CIG_DOORS__DOL,
      0,//CIG_DOORS__PHE,
      0,//CIG_DOORS__DCB,
      0,//CIG_DOORS__DOB,
      0,//CIG_DOORS__HOLD,
      0,//CIG_DOORS__PHE_ALT,
      0,//CIG_DOORS__TCL,
      0,//CIG_DOORS__MCL,
      0,//CIG_DOORS__BCL,
};
//-----------------------------------------------
static const uint16_t aeCIG_Mapping_DoorRear[NUM_CIG_DOORS] =
{
      ///* Fixed Inputs */
      enIN_GSWR,//CIG_DOORS__GSW,
      enIN_DZ_R,//CIG_DOORS__DZ,
      ///* Unfixed Inputs */
      enIN_DPM_R,//CIG_DOORS__DPM,
      enIN_DCL_R,//CIG_DOORS__DCL,
      enIN_DOL_R,//CIG_DOORS__DOL,
      enIN_PHE_R,//CIG_DOORS__PHE,
      enIN_DCB_R,//CIG_DOORS__DCB,
      enIN_DOB_R,//CIG_DOORS__DOB,
      enIN_DoorHold_R,//CIG_DOORS__HOLD,
      enIN_SafetyEdge_R,//CIG_DOORS__SE
      enIN_PHE_R2,//CIG_DOORS__PHE_ALT
      enIN_TCL_R,//CIG_DOORS__TCL,
      enIN_MCL_R,//CIG_DOORS__MCL,
      enIN_BCL_R,//CIG_DOORS__BCL,
};
static const uint16_t auwFixedTerminal_Plus1_DoorRear[ NUM_CIG_DOORS ] =
{
      ///* Fixed Inputs */
      TERMINAL_INDEX_START__CTA + 1 + 1,//CIG_DOORS__GSW,
      TERMINAL_INDEX_START__CTA + 3 + 1,//CIG_DOORS__DZ,
      ///* Unfixed Inputs */
      0,//CIG_DOORS__DPM,
      0,//CIG_DOORS__DCL,
      0,//CIG_DOORS__DOL,
      0,//CIG_DOORS__PHE,
      0,//CIG_DOORS__DCB,
      0,//CIG_DOORS__DOB,
      0,//CIG_DOORS__HOLD,
      0,//CIG_DOORS__PHE_ALT,
      0,//CIG_DOORS__TCL,
      0,//CIG_DOORS__MCL,
      0,//CIG_DOORS__BCL,
};
static const char * const pasInputStrings_Doors[ NUM_CIG_DOORS ] =
{
      ///* Fixed Inputs */
      "GSW",//CIG_DOORS__GSW,
      "DZ",//CIG_DOORS__DZ,
      ///* Unfixed Inputs */
      "DPM",//CIG_DOORS__DPM,
      "DCL",//CIG_DOORS__DCL,
      "DOL",//CIG_DOORS__DOL,
      "PHE",//CIG_DOORS__PHE,
      "DCB",//CIG_DOORS__DCB,
      "DOB",//CIG_DOORS__DOB,
      "HOLD",//CIG_DOORS__HOLD,
      "Safety Edge",//CIG_DOORS__SE
      "PHE ALT",//CIG_DOORS__PHE_ALT
      "TCL",//CIG_DOORS__TCL,
      "MCL",//CIG_DOORS__MCL,
      "BCL",//CIG_DOORS__BCL,
};
//-----------------------------------------------
static const enum en_input_functions aeCIG_Mapping_FireEQ[NUM_CIG_FIRE_EQ] =
{
      ///* Fixed Inputs */
      ///* Unfixed Inputs */
      enIN_SMOKE_HA,//CIG_FIRE_EQ__SMOKE_HA,
      enIN_SMOKE_MR,//CIG_FIRE_EQ__SMOKE_MR,
      enIN_SMOKE_MAIN,//CIG_FIRE_EQ__SMOKE_MAIN,
      enIN_SMOKE_ALT,//CIG_FIRE_EQ__SMOKE_ALT,
      enIN_SEISMIC,//CIG_FIRE_EQ__SEISMIC,
      enIN_REMOTE_FIRE_KEY_SWITCH,//CIG_FIRE_EQ__FIRE_REMOTE_KEY_SW,
      enIN_FIRE_RECALL_OFF,//CIG_FIRE_EQ__FIRE_RECALL_OFF,
      enIN_FIRE_RECALL_RESET,//CIG_FIRE_EQ__FIRE_RECALL_RESET,
      enIN_FIRE_II_ON,//CIG_FIRE_EQ__FIRE_II_ON,
      enIN_FIRE_II_OFF,//CIG_FIRE_EQ__FIRE_II_OFF,
      enIN_FIRE_II_HOLD,//CIG_FIRE_EQ__FIRE_II_HOLD,
      enIN_FIRE_II_CNCL,//CIG_FIRE_EQ__FIRE_II_CNCL,
      enIN_SMOKE_PIT,//CIG_FIRE_EQ__SMOKE_PIT,
      enIN_SMOKE_MR_2,//CIG_FIRE_EQ__SMOKE_MR_2,
      enIN_SMOKE_HA_2,//CIG_FIRE_EQ__SMOKE_HA_2,
      enIN_EQ_HOISTWAY_SCAN, //CIG_FIRE_EQ__EQ_HOISTWAY_SCAN
};
static const uint16_t auwFixedTerminal_Plus1_Fire[ NUM_CIG_FIRE_EQ ] =
{
      ///* Fixed Inputs */
      ///* Unfixed Inputs */
      0,//CIG_FIRE_EQ__SMOKE_HA,
      0,//CIG_FIRE_EQ__SMOKE_MR,
      0,//CIG_FIRE_EQ__SMOKE_MAIN,
      0,//CIG_FIRE_EQ__SMOKE_ALT,
      0,//CIG_FIRE_EQ__SEISMIC,
      0,//CIG_FIRE_EQ__FIRE_REMOTE_KEY_SW,
      0,//CIG_FIRE_EQ__FIRE_RECALL_OFF,
      0,//CIG_FIRE_EQ__FIRE_RECALL_RESET,
      0,//CIG_FIRE_EQ__FIRE_II_ON,
      0,//CIG_FIRE_EQ__FIRE_II_OFF,
      0,//CIG_FIRE_EQ__FIRE_II_HOLD,
      0,//CIG_FIRE_EQ__FIRE_II_CNCL,
      0,//CIG_FIRE_EQ__SMOKE_PIT,
      0,//CIG_FIRE_EQ__SMOKE_MR_2,
      0,//CIG_FIRE_EQ__SMOKE_HA_2,
      0,//CIG_FIRE_EQ__EQ_HOISTWAY_SCAN,
};
static const char * const pasInputStrings_Fire[ NUM_CIG_FIRE_EQ ] =
{
   ///* Fixed Inputs */
   ///* Unfixed Inputs */
   "Smoke HA",//CIG_FIRE_EQ__SMOKE_HA,
   "Smoke MR",//CIG_FIRE_EQ__SMOKE_MR,
   "Smoke Main",//CIG_FIRE_EQ__SMOKE_MAIN,
   "Smoke Alt",//CIG_FIRE_EQ__SMOKE_ALT,
   "Seismic",//CIG_FIRE_EQ__SEISMIC,
   "Remote Fire Key",//CIG_FIRE_EQ__FIRE_REMOTE_KEY_SW,
   "Fire Recall Off",//CIG_FIRE_EQ__FIRE_RECALL_OFF,
   "Fire Recall Rst",//CIG_FIRE_EQ__FIRE_RECALL_RESET,
   "Fire2 On",//CIG_FIRE_EQ__FIRE_II_ON,
   "Fire2 Off",//CIG_FIRE_EQ__FIRE_II_OFF,
   "Fire2 Hold",//CIG_FIRE_EQ__FIRE_II_HOLD,
   "Fire2 Cncl",//CIG_FIRE_EQ__FIRE_II_CNCL,
   "Smoke Pit",//CIG_FIRE_EQ__SMOKE_PIT,
   "Smoke MR 2",//CIG_FIRE_EQ__SMOKE_MR_2,
   "Smoke HA 2",//CIG_FIRE_EQ__SMOKE_HA_2,
   "EQ Hoistway Scan", //CIG_FIRE_EQ__EQ_HOISTWAY_SCAN
};
//-----------------------------------------------
static const enum en_input_functions aeCIG_Mapping_Insp[NUM_CIG_INSP] =
{
   ///* Fixed Inputs */
   enIN_IPTR,//CIG_INSP__IP_TR,
   enIN_ILTR,//CIG_INSP__IL_TR,
   enIN_UNUSED,//CIG_INSP__UNUSED2,
   enIN_UNUSED,//CIG_INSP__UNUSED3,
   enIN_CTUP,//CIG_INSP__CT_UP,
   enIN_CTDN,//CIG_INSP__CT_DN,
   enIN_ICUP,//CIG_INSP__IC_UP,
   enIN_ICDN,//CIG_INSP__IC_DN,
   enIN_CTEN,// CIG_INSP__CT_EN
   ///* Unfixed Inputs */
   enIN_IPUP,//CIG_INSP__IP_UP,
   enIN_IPDN,//CIG_INSP__IP_DN,
   enIN_ILUP,//CIG_INSP__IL_UP,
   enIN_ILDN,//CIG_INSP__IL_DN,
};
static const uint16_t auwFixedTerminal_Plus1_Insp[ NUM_CIG_INSP ] =
{
   ///* Dedicated Inputs */
   ///* Fixed Inputs */
   (TERMINAL_INDEX_START__MRA + 1) | MASK_INVERT_FIXED_TERMINAL_PLUS1,//CIG_INSP__IP_TR,
   (TERMINAL_INDEX_START__MRA + 1 + 1) | MASK_INVERT_FIXED_TERMINAL_PLUS1,//CIG_INSP__IL_TR,
   0,//CIG_INSP__UNUSED2,
   0,//CIG_INSP__UNUSED3,
   TERMINAL_INDEX_START__CTA + 4 + 1,//CIG_INSP__CT_UP,
   TERMINAL_INDEX_START__CTA + 5 + 1,//CIG_INSP__CT_DN,
   TERMINAL_INDEX_START__COP + 1,//CIG_INSP__IC_UP,
   TERMINAL_INDEX_START__COP + 1 + 1,//CIG_INSP__IC_DN,
   TERMINAL_INDEX_START__CTA + 6 + 1,// CIG_INSP__CT_EN
   ///* Unfixed Inputs */
   0,//CIG_INSP__IP_UP,
   0,//CIG_INSP__IP_DN,
   0,//CIG_INSP__IL_UP,
   0,//CIG_INSP__IL_DN,
};
static const char * const pasInputStrings_Insp[ NUM_CIG_INSP ] =
{
   ///* Dedicated Inputs */
   ///* Fixed Inputs */
   "IP TR",//CIG_INSP__IP_TR,
   "IL TR",//CIG_INSP__IL_TR,
   "Unused2",//CIG_INSP__UNUSED2,
   "Unused3",//CIG_INSP__UNUSED3,
   "CT Up",//CIG_INSP__CT_UP,
   "CT Down",//CIG_INSP__CT_DN,
   "IC Up",//CIG_INSP__IC_UP,
   "IC Down",//CIG_INSP__IC_DN,
   "CT Enable", //CIG_INSP__CT_EN
   /* Unfixed Inputs */
   "IP Up",//CIG_INSP__IP_UP,
   "IP Down",//CIG_INSP__IP_DN,
   "IL Up",//CIG_INSP__IL_UP,
   "IL Down",//CIG_INSP__IL_DN,
};
//-----------------------------------------------
static const enum en_input_functions aeCIG_Mapping_Ctrl[NUM_CIG_CTRL] =
{
   ///* Fixed Inputs */
   enIN_UNUSED,//CIG_CTRL__UNUSED0,
   ///* Unfixed Inputs */
   enIN_FAN_AND_LIGHT,//CIG_CTRL__FAN_AND_LIGHT,
   enIN_REGEN_FLT,//CIG_CTRL__REGEN_FLT,
   enIN_OOS,//CIG_CTRL__OOS,
   enIN_BATTERY_POWER,//CIG_CTRL__BATTERY_POWER,
   enIN_AUTO_RESCUE,//CIG_CTRL__AUTO_RESCUE,
   enIN_RecTrvOn,//CIG_CTRL__REC_TRV_ON,
   enIN_RecTrvDir,//CIG_CTRL__REC_TRV_DIR,
   enIN_MANUAL_PICK,//CIG_CTRL__MANUAL_PICK,
   enIN_BATTERY_FLT,//CIG_CTRL__BATTERY_FAULT
   enIN_FAULT,//CIG_CTRL__FAULT
   enIN_DSD_RunEngaged,//CIG_CTRL__DSD_RUN_ENGAGED
   enIN_UNUSED,//CIG_CTRL__VALVE_FLT
   enIN_BRAKE1_BPS,//CIG_CTRL__BRAKE1_BPS,
   enIN_BRAKE2_BPS,//CIG_CTRL__BRAKE2_BPS,
};
static const uint16_t auwFixedTerminal_Plus1_Ctrl[ NUM_CIG_CTRL ] =
{
   ///* Fixed Inputs */
   0,//CIG_CTRL__UNUSED0,
   ///* Unfixed Inputs */
   0,//CIG_CTRL__FAN_AND_LIGHT,
   0,//CIG_CTRL__REGEN_FLT,
   0,//CIG_CTRL__OOS,
   0,//CIG_CTRL__BATTERY_POWER,
   0,//CIG_CTRL__AUTO_RESCUE,
   0,//CIG_CTRL__REC_TRV_ON,
   0,//CIG_CTRL__REC_TRV_DIR,
   0,//CIG_CTRL__MANUAL_PICK,
   0,//CIG_CTRL__BATTERY_FAULT,
   0,//CIG_CTRL__FAULT
   0,//CIG_CTRL__DSD_RUN_ENGAGED
   0,//CIG_CTRL__VALVE_FLT
   0,//CIG_CTRL__BRAKE1_BPS,
   0,//CIG_CTRL__BRAKE2_BPS,
};
static const char * const pasInputStrings_Ctrl[ NUM_CIG_CTRL ] =
{
   ///* Fixed Inputs */
   "UNUSED0",//CIG_CTRL__UNUSED0,
   ///* Unfixed Inputs */
   "Fan And Light",//CIG_CTRL__FAN_AND_LIGHT,
   "Regen Flt",//CIG_CTRL__REGEN_FLT,
   "OOS",//CIG_CTRL__OOS,
   "Battery Power",//CIG_CTRL__BATTERY_POWER,
   "Auto Rescue",//CIG_CTRL__AUTO_RESCUE,
   "Rec Trv On",//CIG_CTRL__REC_TRV_ON,
   "Rec Trv Dir",//CIG_CTRL__REC_TRV_DIR,
   "Manual Pick",//CIG_CTRL__MANUAL_PICK,
   "Battery Fault",//CIG_CTRL__BATTERTY_FAULT,
   "Fault",//CIG_CTRL__FAULT
   "DSD Run Engaged",//CIG_CTRL__DSD_RUN_ENGAGED
   "N/A",//CIG_CTRL__VALVE_FLT
   "Brake1 BPS",//CIG_CTRL__BRAKE1_BPS,
   "Brake2 BPS",//CIG_CTRL__BRAKE2_BPS,
};
//-----------------------------------------------
static const enum en_input_functions aeCIG_Mapping_Safety[NUM_CIG_SAFETY] =
{
   ///* Fixed Inputs */
   ///* Unfixed Inputs */
   enIN_OVER_LOAD,//CIG_SAFETY__OVER_LOAD,
   enIN_FULL_LOAD,//CIG_SAFETY__FULL_LOAD,
   enIN_FLOOD,//CIG_SAFETY__FLOOD,
   enIN_PhoneFailActive,//CIG_SAFETY__PHONE_FAIL_ACTIVE,
   enIN_PhoneFailReset,//CIG_SAFETY__PHONE_FAIL_RESET,
   enIN_TLOSS_RESET,//CIG_SAFETY__TLOSS_RESET,
   enIN_MOTOR_OVERHEAT,//CIG_SAFETY__MOTOR_OVERHEAT, //(Hydro Only) N/C input signals motor overheat.
   enIN_LOW_OIL,//CIG_SAFETY__LOW_OIL, //(Hydro Only) N/C input signals low oil.
   enIN_LOW_PRESSURE,//CIG_SAFETY__LOW_PRESSURE, //(Hydro Only) N/C input signals low pressure.
   enIN_VISCOSITY,//CIG_SAFETY__VISCOSITY, //(Hydro Only) N/C input signals cold oil.
};
static const uint16_t auwFixedTerminal_Plus1_Safety[ NUM_CIG_SAFETY ] =
{
   ///* Fixed Inputs */
   ///* Unfixed Inputs */
   0,//CIG_SAFETY__OVER_LOAD,
   0,//CIG_SAFETY__FULL_LOAD,
   0,//CIG_SAFETY__FLOOD,
   0,//CIG_SAFETY__PHONE_FAIL_ACTIVE,
   0,//CIG_SAFETY__PHONE_FAIL_RESET,
   0,//CIG_SAFETY__TLOSS_RESET,
   0,//CIG_SAFETY__MOTOR_OVERHEAT, //(Hydro Only) N/C input signals motor overheat.
   0,//CIG_SAFETY__LOW_OIL, //(Hydro Only) N/C input signals low oil.
   0,//CIG_SAFETY__LOW_PRESSURE, //(Hydro Only) N/C input signals low pressure.
   0,//CIG_SAFETY__VISCOSITY, //(Hydro Only) N/C input signals cold oil.
};
static const char * const pasInputStrings_Safety[ NUM_CIG_SAFETY ] =
{
   ///* Fixed Inputs */
   ///* Unfixed Inputs */
   "Over Load",//CIG_SAFETY__OVER_LOAD,
   "Full Load",//CIG_SAFETY__FULL_LOAD,
   "Flood",//CIG_SAFETY__FLOOD,
   "Phone Failure",//CIG_SAFETY__PHONE_FAIL_ACTIVE,
   "Phone Reset",//CIG_SAFETY__PHONE_FAIL_RESET,
   "TLoss Reset",//CIG_SAFETY__TLOSS_RESET,
   "Motor OVHT",//CIG_SAFETY__MOTOR_OVERHEAT, //(Hydro Only) N/C input signals motor overheat.
   "Low Oil",//CIG_SAFETY__LOW_OIL, //(Hydro Only) N/C input signals low oil.
   "Low PRESS",//CIG_SAFETY__LOW_PRESSURE, //(Hydro Only) N/C input signals low pressure.
   "Viscosity",//CIG_SAFETY__VISCOSITY, //(Hydro Only) N/C input signals cold oil.
};

//-----------------------------------------------
static const enum en_input_functions aeCIG_Mapping_EPWR[NUM_CIG_EPWR] =
{
   /* Fixed Inputs */
   /* Unfixed Inputs */
   enIN_EP_ON,//CIG_EPWR__ON,
   enIN_EP_UP_TO_SPEED,//CIG_EPWR__UP_TO_SPEED,
   enIN_EP_AUTO_SELECT,//CIG_EPWR__AUTO_SELECT,
   enIN_EP_SELECT_1,//CIG_EPWR__SELECT_1,
   enIN_EP_SELECT_2,//CIG_EPWR__SELECT_2,
   enIN_EP_SELECT_3,//CIG_EPWR__SELECT_3,
   enIN_EP_SELECT_4,//CIG_EPWR__SELECT_4,
   enIN_EP_SELECT_5,//CIG_EPWR__SELECT_5,
   enIN_EP_SELECT_6,//CIG_EPWR__SELECT_6,
   enIN_EP_SELECT_7,//CIG_EPWR__SELECT_7,
   enIN_EP_SELECT_8,//CIG_EPWR__SELECT_8,
   enIN_EP_PRETRANSFER,//CIG_EPWR__PRETRANSFER,
};
static const uint16_t auwFixedTerminal_Plus1_EPWR[ NUM_CIG_EPWR ] =
{
   /* Fixed Inputs */
   /* Unfixed Inputs */
   0,//CIG_EPWR__ON,
   0,//CIG_EPWR__UP_TO_SPEED,
   0,//CIG_EPWR__AUTO_SELECT,
   0,//CIG_EPWR__SELECT_1,
   0,//CIG_EPWR__SELECT_2,
   0,//CIG_EPWR__SELECT_3,
   0,//CIG_EPWR__SELECT_4,
   0,//CIG_EPWR__SELECT_5,
   0,//CIG_EPWR__SELECT_6,
   0,//CIG_EPWR__SELECT_7,
   0,//CIG_EPWR__SELECT_8,
   0,//CIG_EPWR__PRETRANSFER,
};
static const char * const pasInputStrings_EPWR[ NUM_CIG_EPWR ] =
{
   /* Fixed Inputs */
   /* Unfixed Inputs */
   "EP On",//CIG_EPWR__ON,
   "UpToSpeed",//CIG_EPWR__UP_TO_SPEED,
   "AutoSelect",//CIG_EPWR__AUTO_SELECT,
   "Select1",//CIG_EPWR__SELECT_1,
   "Select2",//CIG_EPWR__SELECT_2,
   "Select3",//CIG_EPWR__SELECT_3,
   "Select4",//CIG_EPWR__SELECT_4,
   "Select5",//CIG_EPWR__SELECT_5,
   "Select6",//CIG_EPWR__SELECT_6,
   "Select7",//CIG_EPWR__SELECT_7,
   "Select8",//CIG_EPWR__SELECT_8,
   "Pretransfer",//CIG_EPWR__PRETRANSFER,
};
/*----------------------------------------------------------------------------
   Returns index into system input bitmap, from an input in a specified input group
 *----------------------------------------------------------------------------*/
enum en_input_functions SysIO_GetInputFunctionFromCIG( enum_input_groups eCIG, uint8_t ucIndex )
{
   enum en_input_functions eInput = enIN_UNUSED;
   switch( eCIG )
   {
      case INPUT_GROUP__AUTO_OP:
         if(ucIndex < NUM_CIG_AUTO_OPER)
         {
            eInput = aeCIG_Mapping_AutoOper[ucIndex];
         }
         break;
      case INPUT_GROUP__DOORS_F:
         if(ucIndex < NUM_CIG_DOORS)
         {
            eInput = aeCIG_Mapping_DoorFront[ucIndex];
         }
         break;
      case INPUT_GROUP__DOORS_R:
         if(ucIndex < NUM_CIG_DOORS)
         {
            eInput = aeCIG_Mapping_DoorRear[ucIndex];
         }
         break;
      case INPUT_GROUP__FIRE:
         if(ucIndex < NUM_CIG_FIRE_EQ)
         {
            eInput = aeCIG_Mapping_FireEQ[ucIndex];
         }
         break;
      case INPUT_GROUP__INSP:
         if(ucIndex < NUM_CIG_INSP)
         {
            eInput = aeCIG_Mapping_Insp[ucIndex];
         }
         break;
      case INPUT_GROUP__CTRL:
         if(ucIndex < NUM_CIG_CTRL)
         {
            eInput = aeCIG_Mapping_Ctrl[ucIndex];
         }
         break;
      case INPUT_GROUP__SAFETY:
         if(ucIndex < NUM_CIG_SAFETY)
         {
            eInput = aeCIG_Mapping_Safety[ucIndex];
         }
         break;
      case INPUT_GROUP__EPWR:
         if(ucIndex < NUM_CIG_EPWR)
         {
            eInput = aeCIG_Mapping_EPWR[ucIndex];
         }
         break;
      default:
         break;
   }
   return eInput;
}
/*----------------------------------------------------------------------------
  Returns functions per input group
 *----------------------------------------------------------------------------*/
uint8_t SysIO_GetSizeOfCIG( enum_input_groups eCIG )
{
   uint8_t ucSize = ( eCIG < NUM_INPUT_GROUPS ) ? aucFunctionsPerInputGroup[eCIG]:1;
   return ucSize;
}
/*----------------------------------------------------------------------------
   Returns 1 if a CIG input is fixed
 *----------------------------------------------------------------------------*/
inline uint16_t SysIO_GetFixedTerminalFromCIG_Plus1( enum_input_groups eCIG, uint8_t ucIndex )
{
   uint16_t uwTerminal_Plus1 = 0;
   switch( eCIG )
   {
      case INPUT_GROUP__AUTO_OP:
         if(ucIndex < NUM_CIG_AUTO_OPER)
         {
            uwTerminal_Plus1 = auwFixedTerminal_Plus1_Auto[ucIndex];
         }
         break;
      case INPUT_GROUP__DOORS_F:
         if(ucIndex < NUM_CIG_DOORS)
         {
            uwTerminal_Plus1 = auwFixedTerminal_Plus1_DoorFront[ucIndex];
         }
         break;
      case INPUT_GROUP__DOORS_R:
         if(ucIndex < NUM_CIG_DOORS)
         {
            uwTerminal_Plus1 = auwFixedTerminal_Plus1_DoorRear[ucIndex];
         }
         break;
      case INPUT_GROUP__FIRE:
         if(ucIndex < NUM_CIG_FIRE_EQ)
         {
            uwTerminal_Plus1 = auwFixedTerminal_Plus1_Fire[ucIndex];
         }
         break;
      case INPUT_GROUP__INSP:
         if(ucIndex < NUM_CIG_INSP)
         {
            uwTerminal_Plus1 = auwFixedTerminal_Plus1_Insp[ucIndex];
         }
         break;
      case INPUT_GROUP__CTRL:
         if(ucIndex < NUM_CIG_CTRL)
         {
            uwTerminal_Plus1 = auwFixedTerminal_Plus1_Ctrl[ucIndex];
         }
         break;
      case INPUT_GROUP__SAFETY:
         if(ucIndex < NUM_CIG_SAFETY)
         {
            uwTerminal_Plus1 = auwFixedTerminal_Plus1_Safety[ucIndex];
         }
         break;
      case INPUT_GROUP__EPWR:
         if(ucIndex < NUM_CIG_EPWR)
         {
            uwTerminal_Plus1 = auwFixedTerminal_Plus1_EPWR[ucIndex];
         }
         break;
      default:
         break;
   }
   return uwTerminal_Plus1;
}
/*----------------------------------------------------------------------------
  Get string for CIG type
 *----------------------------------------------------------------------------*/
const char * SysIO_GetInputGroupString( enum_input_groups eCIG )
{
   return pasStrings_CIG[eCIG];
}
/*----------------------------------------------------------------------------
  Get string for input for a specific CIG
 *----------------------------------------------------------------------------*/
const char * SysIO_GetInputGroupString_ByFunctionIndex( enum_input_groups eCIG, uint8_t ucIndex )
{
   if( ucIndex >= SysIO_GetSizeOfCIG( eCIG ) )
   {
      return pasStrings_CIG[INPUT_GROUP__NONE];
   }
   switch( eCIG )
   {
      case INPUT_GROUP__AUTO_OP:
         return pasInputStrings_AutoOper[ucIndex];
         break;
      case INPUT_GROUP__DOORS_F:
      case INPUT_GROUP__DOORS_R:
         return pasInputStrings_Doors[ucIndex];
         break;
      case INPUT_GROUP__FIRE:
         return pasInputStrings_Fire[ucIndex];
         break;
      case INPUT_GROUP__INSP:
         return pasInputStrings_Insp[ucIndex];
         break;
      case INPUT_GROUP__CTRL:
         return pasInputStrings_Ctrl[ucIndex];
         break;
      case INPUT_GROUP__SAFETY:
         return pasInputStrings_Safety[ucIndex];
         break;
      case INPUT_GROUP__EPWR:
         return pasInputStrings_EPWR[ucIndex];
         break;
      default:
         return pasStrings_CIG[INPUT_GROUP__NONE];
         break;
   }
}
