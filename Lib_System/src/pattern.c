/******************************************************************************
 *
 * @file     pattern.c
 * @brief    Pattern functions
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note     Functions for pattern generation
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "sys.h"
#include "pattern.h"
#include "motion.h"
#include "position.h"
#include "operation.h"
#define ARM_MATH
#ifdef ARM_MATH
#include "arm_math.h"
#else
#include <math.h>
#endif
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
static Curve_Parameters stCurveParameters[NUM_MOTION_PROFILES];
static Motion_Control stMotionCtrl;

Curve_Parameters * gpstCurveParameters = stCurveParameters;
Motion_Control * gpstMotionCtrl = &stMotionCtrl;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define SQRT_12            ( 3.4641f )    /* square root of 12 */
#define CBRT_18            ( 2.6207f )    /* cubic root of 18 */
#define ONE_THIRD          ( 0.6667f )    /* 1/3 in float */
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------
   Check if motion parameters have changed when car is stopped
   Returns 1 if changed
--------------------------------------------------------------------------*/
uint8_t Pattern_UpdateParameters( Motion_Profile eProfile )
{
   uint8_t bReturn = 0;
   static uint16_t uwLastContractSpeed[NUM_MOTION_PROFILES];
   static uint16_t uwLastLevelingSpeed[NUM_MOTION_PROFILES];
   static uint8_t ucLastAccel[NUM_MOTION_PROFILES];
   static uint8_t ucLastDecel[NUM_MOTION_PROFILES];
   static uint8_t ucLastJerkInAccel[NUM_MOTION_PROFILES];
   static uint8_t ucLastJerkOutAccel[NUM_MOTION_PROFILES];
   static uint8_t ucLastJerkInDecel[NUM_MOTION_PROFILES];
   static uint8_t ucLastJerkOutDecel[NUM_MOTION_PROFILES];
   static uint8_t ucLastMotionResolution[NUM_MOTION_PROFILES];
   static uint8_t ucLastLevelingDist[NUM_MOTION_PROFILES];
   static uint8_t ucLastOffsetFromNTS[NUM_MOTION_PROFILES];

   static const enum en_8bit_params aeParamIndex_Accel[NUM_MOTION_PROFILES] =
   {
      enPARAM8__P1_Accel_x10,
      enPARAM8__P2_Accel_x10,
      enPARAM8__P3_Accel_x10,
      enPARAM8__P4_Accel_x10,
   };
   static const enum en_8bit_params aeParamIndex_JerkInAccel[NUM_MOTION_PROFILES] =
   {
      enPARAM8__P1_JerkInAccel_x10,
      enPARAM8__P2_JerkInAccel_x10,
      enPARAM8__P3_JerkInAccel_x10,
      enPARAM8__P4_JerkInAccel_x10,
   };
   static const enum en_8bit_params aeParamIndex_JerkOutAccel[NUM_MOTION_PROFILES] =
   {
      enPARAM8__P1_JerkOutAccel_x10,
      enPARAM8__P2_JerkOutAccel_x10,
      enPARAM8__P3_JerkOutAccel_x10,
      enPARAM8__P4_JerkOutAccel_x10,
   };
   static const enum en_8bit_params aeParamIndex_Decel[NUM_MOTION_PROFILES] =
   {
      enPARAM8__P1_Decel_x10,
      enPARAM8__P2_Decel_x10,
      enPARAM8__P3_Decel_x10,
      enPARAM8__P4_Decel_x10,
   };
   static const enum en_8bit_params aeParamIndex_JerkInDecel[NUM_MOTION_PROFILES] =
   {
      enPARAM8__P1_JerkOutDecel_x10,
      enPARAM8__P2_JerkOutDecel_x10,
      enPARAM8__P3_JerkOutDecel_x10,
      enPARAM8__P4_JerkOutDecel_x10,
   };
   static const enum en_8bit_params aeParamIndex_JerkOutDecel[NUM_MOTION_PROFILES] =
   {
      enPARAM8__P1_JerkInDecel_x10,
      enPARAM8__P2_JerkInDecel_x10,
      enPARAM8__P3_JerkInDecel_x10,
      enPARAM8__P4_JerkInDecel_x10,
   };
   static const enum en_8bit_params aeParamIndex_LevelingDistance[NUM_MOTION_PROFILES] =
   {
      enPARAM8__P1_LevelingDistance_5mm,
      enPARAM8__P2_LevelingDistance_5mm,
      enPARAM8__P3_LevelingDistance_5mm,
      enPARAM8__P4_LevelingDistance_5mm,
   };

   //----------------------------------------
   if( ( stMotionCtrl.eMotionState == MOTION__STOPPED )
    && ( eProfile < NUM_MOTION_PROFILES ) )
   {
      uint16_t uwContractSpeed = Param_ReadValue_16Bit(enPARAM16__ContractSpeed);

      if( eProfile == MOTION_PROFILE__3 )
      {
         uwContractSpeed = Param_ReadValue_16Bit(enPARAM16__EPowerSpeed_fpm);
      }

      if(uwContractSpeed > MAX_CAR_SPEED)
      {
         uwContractSpeed = MAX_CAR_SPEED;
      }
      else if(uwContractSpeed < MIN_CONTRACT_SPEED)
      {
         uwContractSpeed = MIN_CONTRACT_SPEED;
      }
      if(uwLastContractSpeed[eProfile] != uwContractSpeed)
      {
         uwLastContractSpeed[eProfile] = uwContractSpeed;
         bReturn = 1;
         if( eProfile != MOTION_PROFILE__3 )
         {
            stMotionCtrl.flContractSpeed_FPS = uwContractSpeed;
            stMotionCtrl.flContractSpeed_FPS /= 60.0f;
            stMotionCtrl.uwContractSpeed_FPM = uwContractSpeed;
         }
      }
      //----------------------------------------
      uint16_t uwLevelingSpeed = Param_ReadValue_16Bit(enPARAM16__LevelingSpeed);
      if(!Param_ReadValue_8Bit( aeParamIndex_LevelingDistance[eProfile] ) )
      {
         uwLevelingSpeed = 0;
      }
      if(uwLastLevelingSpeed[eProfile] != uwLevelingSpeed)
      {
         uwLastLevelingSpeed[eProfile] = uwLevelingSpeed;
         bReturn = 1;
         stCurveParameters[eProfile].uwLevelingSpeed_FPM = uwLevelingSpeed;
      }
      //----------------------------------------
      uint8_t ucAccel = Param_ReadValue_8Bit(aeParamIndex_Accel[eProfile]);
      if( ucAccel > MAX_ACCEL_X10 )
      {
         ucAccel = MAX_ACCEL_X10;
      }
      else if( ucAccel < MIN_ACCEL_X10 )
      {
         ucAccel = MIN_ACCEL_X10;
      }
      if(ucLastAccel[eProfile] != ucAccel)
      {
         ucLastAccel[eProfile] = ucAccel;
         bReturn = 1;
         stCurveParameters[eProfile].fAccel = ucAccel;
         stCurveParameters[eProfile].fAccel /= 10.0f;
      }
      //----------------------------------------
      uint8_t ucDecel = Param_ReadValue_8Bit(aeParamIndex_Decel[eProfile]);
      if( ucDecel > MAX_ACCEL_X10 )
      {
         ucDecel = MAX_ACCEL_X10;
      }
      else if( ucDecel < MIN_ACCEL_X10 )
      {
         ucDecel = MIN_ACCEL_X10;
      }
      if(ucLastDecel[eProfile] != ucDecel)
      {
         ucLastDecel[eProfile] = ucDecel;
         bReturn = 1;
         stCurveParameters[eProfile].fDecel = ucDecel;
         stCurveParameters[eProfile].fDecel /= 10.0f;
      }
      //----------------------------------------
      uint8_t ucJerkInAccel = Param_ReadValue_8Bit(aeParamIndex_JerkInAccel[eProfile]);
      if( ucJerkInAccel > MAX_JERK_X10 )
      {
         ucJerkInAccel = MAX_JERK_X10;
      }
      else if( ucJerkInAccel < MIN_JERK_X10 )
      {
         ucJerkInAccel = MIN_JERK_X10;
      }
      if(ucLastJerkInAccel[eProfile] != ucJerkInAccel)
      {
         ucLastJerkInAccel[eProfile] = ucJerkInAccel;
         bReturn = 1;
         stCurveParameters[eProfile].fJerkInAccel = ucJerkInAccel;
         stCurveParameters[eProfile].fJerkInAccel /= 10.0f;
      }
      //----------------------------------------
      uint8_t ucJerkOutAccel = Param_ReadValue_8Bit(aeParamIndex_JerkOutAccel[eProfile]);
      if( ucJerkOutAccel > MAX_JERK_X10 )
      {
         ucJerkOutAccel = MAX_JERK_X10;
      }
      else if( ucJerkOutAccel < MIN_JERK_X10 )
      {
         ucJerkOutAccel = MIN_JERK_X10;
      }
      if(ucLastJerkOutAccel[eProfile] != ucJerkOutAccel)
      {
         ucLastJerkOutAccel[eProfile] = ucJerkOutAccel;
         bReturn = 1;
         stCurveParameters[eProfile].fJerkOutAccel = ucJerkOutAccel;
         stCurveParameters[eProfile].fJerkOutAccel /= 10.0f;
      }
      //----------------------------------------
      uint8_t ucJerkInDecel = Param_ReadValue_8Bit(aeParamIndex_JerkInDecel[eProfile]);
      if( ucJerkInDecel > MAX_JERK_X10 )
      {
         ucJerkInDecel = MAX_JERK_X10;
      }
      else if( ucJerkInDecel < MIN_JERK_X10 )
      {
         ucJerkInDecel = MIN_JERK_X10;
      }
      if(ucLastJerkInDecel[eProfile] != ucJerkInDecel)
      {
         ucLastJerkInDecel[eProfile] = ucJerkInDecel;
         bReturn = 1;
         stCurveParameters[eProfile].fJerkInDecel = ucJerkInDecel;
         stCurveParameters[eProfile].fJerkInDecel /= 10.0f;
      }
      //----------------------------------------
      uint8_t ucJerkOutDecel = Param_ReadValue_8Bit(aeParamIndex_JerkOutDecel[eProfile]);
      if( ucJerkOutDecel > MAX_JERK_X10 )
      {
         ucJerkOutDecel = MAX_JERK_X10;
      }
      else if( ucJerkOutDecel < MIN_JERK_X10 )
      {
         ucJerkOutDecel = MIN_JERK_X10;
      }
      if(ucLastJerkOutDecel[eProfile] != ucJerkOutDecel)
      {
         ucLastJerkOutDecel[eProfile] = ucJerkOutDecel;
         bReturn = 1;
         stCurveParameters[eProfile].fJerkOutDecel = ucJerkOutDecel;
         stCurveParameters[eProfile].fJerkOutDecel /= 10.0f;
      }
      //----------------------------------------
      uint8_t ucMotionResolution = Param_ReadValue_8Bit(enPARAM8__MotionRes_1ms);
      if(ucMotionResolution < MIN_PATTERN_RESOLUTION__MS)
      {
         ucMotionResolution = MIN_PATTERN_RESOLUTION__MS;
      }
      else if(ucMotionResolution > MAX_PATTERN_RESOLUTION__MS)
      {
         ucMotionResolution = MAX_PATTERN_RESOLUTION__MS;
      }
      if(ucLastMotionResolution[eProfile] != ucMotionResolution)
      {
         ucLastMotionResolution[eProfile] = ucMotionResolution;
         bReturn = 1;
         stMotionCtrl.flPatternTimeRes_sec = ucMotionResolution;
         stMotionCtrl.flPatternTimeRes_sec /= 1000.0f;
      }
      //----------------------------------------
      uint8_t ucLevelingDist = Param_ReadValue_8Bit( aeParamIndex_LevelingDistance[eProfile] );
      if( ucLevelingDist > MAX_LEVELING_DISTANCE_5MM )
      {
         ucLevelingDist = MAX_LEVELING_DISTANCE_5MM;
      }
      if(ucLastLevelingDist[eProfile] != ucLevelingDist)
      {
         ucLastLevelingDist[eProfile] = ucLevelingDist;
         bReturn = 1;
         stCurveParameters[eProfile].uwLevelingDistance = ucLevelingDist * 10.0f;
      }
      //----------------------------------------
      uint8_t ucOffsetFromNTS = Param_ReadValue_8Bit( enPARAM8__ETS_OffsetFromNTS_5mm );
      if( ucLastOffsetFromNTS[eProfile] != ucOffsetFromNTS )
      {
         ucLastOffsetFromNTS[eProfile] = ucOffsetFromNTS;
         bReturn = 1;
      }
      //----------------------------------------
      uint16_t uwMinAccelSpeed = Param_ReadValue_16Bit(enPARAM16__MinAccelSpeed);
      /* Battery rescue will have a fixed minimum accel speed for assessing drive's chosen direction */
      if( GetOperation_AutoMode() == MODE_A__BATT_RESQ )
      {
         uwMinAccelSpeed = BATTERY_RESCUE_MIN_ACCEL_SPEED;
      }
      if(uwMinAccelSpeed > MAX_MINIMUM_ACCEL_SPEED)
      {
         uwMinAccelSpeed = MAX_MINIMUM_ACCEL_SPEED;
      }
      else if(uwMinAccelSpeed < MIN_MINIMUM_ACCEL_SPEED)
      {
         uwMinAccelSpeed = MIN_MINIMUM_ACCEL_SPEED;
      }
      stMotionCtrl.uwMinAccelSpeed_FPM = uwMinAccelSpeed;

      uint16_t ucMinRelevelSpeed = Param_ReadValue_8Bit(enPARAM8__MinRelevelSpeed);
      if(ucMinRelevelSpeed > MAX_MINIMUM_ACCEL_SPEED)
      {
         ucMinRelevelSpeed = MAX_MINIMUM_ACCEL_SPEED;
      }
      else if(ucMinRelevelSpeed < MIN_MINIMUM_ACCEL_SPEED)
      {
         ucMinRelevelSpeed = MIN_MINIMUM_ACCEL_SPEED;
      }
      stMotionCtrl.ucMinRelevelSpeed_FPM = ucMinRelevelSpeed;
   }
   return bReturn;
}
/*--------------------------------------------------------------------------
   Recalculate curve limits based on new parameters
--------------------------------------------------------------------------*/
void Pattern_UpdateCurveLimits( Motion_Profile eProfile )
{
   float flTemp = 0;
   gpstCurveParameters = stCurveParameters;
   gpstMotionCtrl = &stMotionCtrl;
   stCurveParameters[ eProfile ].fShortPatternMinAccelSpeed_FPS = stCurveParameters[ eProfile ].fAccel*stCurveParameters[ eProfile ].fAccel*(1.0f/stCurveParameters[ eProfile ].fJerkInAccel + 1.0f/stCurveParameters[ eProfile ].fJerkOutAccel)/2.0f;

   float fContractSpd_fps = stMotionCtrl.flContractSpeed_FPS;
   if( eProfile == MOTION_PROFILE__3 )
   {
      fContractSpd_fps = Param_ReadValue_16Bit(enPARAM16__EPowerSpeed_fpm)/60.0f;
   }
   //----------------------------------------
   /* ACCEL */
   if ( stCurveParameters[ eProfile ].fShortPatternMinAccelSpeed_FPS >= fContractSpd_fps )
   {
#ifdef ARM_MATH
      arm_sqrt_f32( 2.0f*(stCurveParameters[ eProfile ].fJerkInAccel + stCurveParameters[ eProfile ].fJerkOutAccel)*fContractSpd_fps/(stCurveParameters[ eProfile ].fJerkInAccel*stCurveParameters[ eProfile ].fJerkOutAccel), &flTemp );
#else
      flTemp = sqrtf( 2.0f*(stCurveParameters[ eProfile ].fJerkInAccel + stCurveParameters[ eProfile ].fJerkOutAccel)*fContractSpd_fps/(stCurveParameters[ eProfile ].fJerkInAccel*stCurveParameters[ eProfile ].fJerkOutAccel) );
#endif

      stCurveParameters[ eProfile ].uiMaxAccelTime = (uint32_t)( flTemp/stMotionCtrl.flPatternTimeRes_sec + 0.5f );

      stCurveParameters[ eProfile ].fFullPatternMinAccelDistance_FT = 0xFFFFFFFF; // All Patterns will be Very Short
      stCurveParameters[ eProfile ].fShortPatternMinAccelDistance_FT = 0xFFFFFFFF; // All Patterns will be Very Short
      stCurveParameters[ eProfile ].uiShortPatternMinAccelDistance = 0xFFFFFFFF; // All Patterns will be Very Short
   }
   else
   {
      flTemp = (fContractSpd_fps/stCurveParameters[ eProfile ].fAccel + stCurveParameters[ eProfile ].fAccel*(1.0f/stCurveParameters[ eProfile ].fJerkInAccel + 1.0f/stCurveParameters[ eProfile ].fJerkOutAccel)/2.0f);
      stCurveParameters[ eProfile ].uiMaxAccelTime = (uint32_t)( flTemp/stMotionCtrl.flPatternTimeRes_sec + 0.5f );

      float flTempA1 = powf(stCurveParameters[ eProfile ].fAccel,4.0f)*(powf(stCurveParameters[ eProfile ].fJerkOutAccel,2.0f) - powf(stCurveParameters[ eProfile ].fJerkInAccel,2.0f));
      float flTempA2 = 12.0f*powf(stCurveParameters[ eProfile ].fAccel,2.0f)*powf(stCurveParameters[ eProfile ].fJerkInAccel,2.0f)*stCurveParameters[ eProfile ].fJerkOutAccel;
      float flTempA3 = 12.0f*powf(stCurveParameters[ eProfile ].fJerkOutAccel,2.0f)*powf(stCurveParameters[ eProfile ].fJerkInAccel,2.0f);
      float flTempA4 = 24.0f*stCurveParameters[ eProfile ].fAccel*powf(stCurveParameters[ eProfile ].fJerkOutAccel,2.0f)*powf(stCurveParameters[ eProfile ].fJerkInAccel,2.0f);

      stCurveParameters[ eProfile ].fFullPatternMinAccelDistance_FT = (flTempA1 + flTempA2*fContractSpd_fps + flTempA3*powf(fContractSpd_fps,2))/flTempA4;
      stCurveParameters[ eProfile ].fShortPatternMinAccelDistance_FT = (flTempA1 + flTempA2*stCurveParameters[ eProfile ].fShortPatternMinAccelSpeed_FPS + flTempA3*powf(stCurveParameters[ eProfile ].fShortPatternMinAccelSpeed_FPS,2))/flTempA4;
      stCurveParameters[ eProfile ].uiShortPatternMinAccelDistance = stCurveParameters[ eProfile ].fShortPatternMinAccelDistance_FT * HALF_MM_PER_FT + 0.5f;
   }

   //----------------------------------------
   /* DECEL */
   flTemp = 0;
   stCurveParameters[ eProfile ].fShortPatternMinDecelSpeed_FPS = stCurveParameters[ eProfile ].fDecel*stCurveParameters[ eProfile ].fDecel*(1.0f/stCurveParameters[ eProfile ].fJerkInDecel + 1.0f/stCurveParameters[ eProfile ].fJerkOutDecel)/2.0f;

   if ( stCurveParameters[ eProfile ].fShortPatternMinDecelSpeed_FPS >= fContractSpd_fps )
   {
#ifdef ARM_MATH
      arm_sqrt_f32( 2.0f*(stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel)*fContractSpd_fps/(stCurveParameters[ eProfile ].fJerkInDecel*stCurveParameters[ eProfile ].fJerkOutDecel), &flTemp );
#else
      flTemp = sqrtf( 2.0f*(stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel)*fContractSpd_fps/(stCurveParameters[ eProfile ].fJerkInDecel*stCurveParameters[ eProfile ].fJerkOutDecel) );
#endif

      stCurveParameters[ eProfile ].uiMaxDecelTime = (uint32_t)( flTemp/stMotionCtrl.flPatternTimeRes_sec + 0.5f );

      stCurveParameters[ eProfile ].fFullPatternMinDecelDistance_FT = 0xFFFFFFFF; // All Patterns will be Very Short
      stCurveParameters[ eProfile ].fShortPatternMinDecelDistance_FT = 0xFFFFFFFF; // All Patterns will be Very Short
      stCurveParameters[ eProfile ].uiShortPatternMinDecelDistance = 0xFFFFFFFF; // All Patterns will be Very Short
   }
   else
   {
      flTemp = (fContractSpd_fps/stCurveParameters[ eProfile ].fDecel + stCurveParameters[ eProfile ].fDecel*(1.0f/stCurveParameters[ eProfile ].fJerkInDecel + 1.0f/stCurveParameters[ eProfile ].fJerkOutDecel)/2.0f);
      stCurveParameters[ eProfile ].uiMaxDecelTime = (uint32_t)( flTemp/stMotionCtrl.flPatternTimeRes_sec + 0.5f );

      float flTempD1 = powf(stCurveParameters[ eProfile ].fDecel,4.0f)*(powf(stCurveParameters[ eProfile ].fJerkOutDecel,2.0f) - powf(stCurveParameters[ eProfile ].fJerkInDecel,2.0f));
      float flTempD2 = 12.0f*powf(stCurveParameters[ eProfile ].fDecel,2.0f)*powf(stCurveParameters[ eProfile ].fJerkInDecel,2.0f)*stCurveParameters[ eProfile ].fJerkOutDecel;
      float flTempD3 = 12.0f*powf(stCurveParameters[ eProfile ].fJerkOutDecel,2.0f)*powf(stCurveParameters[ eProfile ].fJerkInDecel,2.0f);
      float flTempD4 = 24.0f*stCurveParameters[ eProfile ].fDecel*powf(stCurveParameters[ eProfile ].fJerkOutDecel,2.0f)*powf(stCurveParameters[ eProfile ].fJerkInDecel,2.0f);

      stCurveParameters[ eProfile ].fFullPatternMinDecelDistance_FT = (flTempD1 + flTempD2*fContractSpd_fps + flTempD3*powf(fContractSpd_fps,2))/flTempD4;
      stCurveParameters[ eProfile ].fShortPatternMinDecelDistance_FT = (flTempD1 + flTempD2*stCurveParameters[ eProfile ].fShortPatternMinDecelSpeed_FPS + flTempD3*powf(stCurveParameters[ eProfile ].fShortPatternMinDecelSpeed_FPS,2))/flTempD4;
      stCurveParameters[ eProfile ].uiShortPatternMinDecelDistance = stCurveParameters[ eProfile ].fShortPatternMinDecelDistance_FT * HALF_MM_PER_FT + 0.5f;
   }
}
/*--------------------------------------------------------------------------
   This function calculates the maximum speed the car will be able to achieve
   given a certain travel distance.

   uiDistance: in position counts
   flDistance: in feet

   TODO: test with uiRampUpDistance = greater of the two min distances. and reduce from there
--------------------------------------------------------------------------*/
float Pattern_GetMaxAccelSpeed_FPS( uint32_t uiDistance )
{
   float flDistance = ((float)uiDistance) / ( HALF_MM_PER_FT );
   float flRampUpDistance = flDistance/2.0f;
   float flSpeed, flTemp;
   Motion_Profile eProfile = GetMotion_Profile();
   float fFullPatternMinDistance = stCurveParameters[ eProfile ].fFullPatternMinAccelDistance_FT
                                 + stCurveParameters[ eProfile ].fFullPatternMinDecelDistance_FT;

   float fShortPatternMinDistance = stCurveParameters[ eProfile ].fShortPatternMinAccelDistance_FT
                                  + stCurveParameters[ eProfile ].fShortPatternMinDecelDistance_FT;
   if ( flDistance > fFullPatternMinDistance )
   {
      flSpeed = stMotionCtrl.flContractSpeed_FPS;
   }
   else if ( flDistance > fShortPatternMinDistance )
   {
      float flConst_A = 24.0f*stCurveParameters[ eProfile ].fAccel*powf(stCurveParameters[ eProfile ].fJerkInAccel, 2.0f)*powf(stCurveParameters[ eProfile ].fJerkOutAccel, 2.0f);
      float flConst_B = 4.0f*powf(stCurveParameters[ eProfile ].fAccel, 4.0f)*(4.0f*powf(stCurveParameters[ eProfile ].fJerkInAccel, 2.0f) - powf(stCurveParameters[ eProfile ].fJerkOutAccel, 2.0f));
      float flConst_C = SQRT_12*stCurveParameters[ eProfile ].fJerkInAccel*stCurveParameters[ eProfile ].fJerkOutAccel;
      float flConst_D = powf(stCurveParameters[ eProfile ].fAccel, 2.0f)/(2*stCurveParameters[ eProfile ].fJerkOutAccel);

#ifdef ARM_MATH
      arm_sqrt_f32( flConst_A*flRampUpDistance + flConst_B, &flTemp );
#else
      flTemp = sqrtf( flConst_A*flRampUpDistance + flConst_B );
#endif

      flSpeed = flTemp/flConst_C - flConst_D;
   }
   else
   {
      float flConst_1 = CBRT_18*(stCurveParameters[ eProfile ].fJerkInAccel + stCurveParameters[ eProfile ].fJerkOutAccel);
      float flConst_2 = powf(stCurveParameters[ eProfile ].fJerkInAccel, 2.0f) * powf(stCurveParameters[ eProfile ].fJerkOutAccel, 2.0f);
      float flConst_3 = 4.0f*powf(stCurveParameters[ eProfile ].fJerkInAccel, 2.0f) + 6.0f*stCurveParameters[ eProfile ].fJerkInAccel*stCurveParameters[ eProfile ].fJerkOutAccel + 2.0f*powf(stCurveParameters[ eProfile ].fJerkOutAccel, 2.0f);

      flSpeed = flConst_1 * powf(flConst_2*flRampUpDistance/flConst_3, ONE_THIRD) / (stCurveParameters[ eProfile ].fJerkInAccel * stCurveParameters[ eProfile ].fJerkOutAccel);
   }

   /* Scale the max speed until the run distance exceeds the ramp up + slowdown distance */
   uint16_t uwReducedSpeed_fpm;
   uint8_t bSpeedFound = 0;

   uint8_t ucCycleLimit = 10;
   float fCycleOffset = 10.0f; // % speed difference between max speed checks
   /* Improve max run speeds by checking the possible speeds in smaller steps */
   if( Param_ReadValue_1Bit(enPARAM1__ImproveMaxSpeed) )
   {
      ucCycleLimit = 20;
      fCycleOffset = 5.0f;
   }

   /* Iterate through possible max speeds and check if they're possible for the given run distance */
   for(uint8_t i = 0; i < ucCycleLimit; i++)
   {
      float fReducedSpeed_fpm = ((flSpeed * 60.0f) * ((100.0f - ((float)i * fCycleOffset)) / 100.0f));
      uwReducedSpeed_fpm = (uint16_t) fReducedSpeed_fpm + 0.5f;
      uint32_t uiRampUp = Pattern_CalculateRampUpDistance( uwReducedSpeed_fpm );
      uint32_t uiSlowdown = Pattern_CalculateSlowdownDistance( uwReducedSpeed_fpm );
      uint32_t uiEstimatedDistance = ( uiRampUp + uiSlowdown ) * 1.05f;
      if( Param_ReadValue_1Bit(enPARAM1__ReducedMaxSpeed) )
      {
         uiEstimatedDistance = ( uiRampUp + uiSlowdown ) * 1.15f;
      }
      if( uiDistance >= uiEstimatedDistance )
      {
         bSpeedFound = 1;
         break;
      }
   }

   if(!bSpeedFound)
   {
      uwReducedSpeed_fpm = stMotionCtrl.ucMinRelevelSpeed_FPM;
   }

   flSpeed = uwReducedSpeed_fpm/60.0f;
   if( flSpeed > stMotionCtrl.flContractSpeed_FPS )
   {
      flSpeed = stMotionCtrl.flContractSpeed_FPS;
   }
   return flSpeed;
}
/*--------------------------------------------------------------------------

   This function calculates the maximum speed the car will be able to achieve
   given a certain travel distance.

   uiDistance: in position counts
   flDistance: in feet

--------------------------------------------------------------------------*/
float Pattern_GetMaxAddedAccelSpeed_FPS( uint32_t uiDistance )
{
   float flSpeed = stMotionCtrl.flContractSpeed_FPS;
   uint16_t uwStartingSpeed = (stMotionCtrl.stAccelVars.fEndSpeed_JerkOutAccel * 60.0f) + 0.5f;
   /* Scale the max speed until the run distance exceeds the ramp up + slowdown distance */
   uint16_t uwReducedSpeed_fpm;
   uint8_t bSpeedFound = 0;

   uint8_t ucCycleLimit = 10;
   float fCycleOffset = 10.0f; // % speed difference between max speed checks
   /* Improve max run speeds by checking the possible speeds in smaller steps */
   if( Param_ReadValue_1Bit(enPARAM1__ImproveMaxSpeed) )
   {
      ucCycleLimit = 20;
      fCycleOffset = 5.0f;
   }

   /* Iterate through possible max speeds and check if they're possible for the given run distance */
   for(uint8_t i = 0; i < ucCycleLimit; i++)
   {
      float fReducedSpeed_fpm = ((flSpeed * 60.0f) * ((100.0f - ((float)i * fCycleOffset)) / 100.0f));
      uwReducedSpeed_fpm = (uint16_t) fReducedSpeed_fpm + 0.5f;
      uint32_t uiRampUp = Pattern_CalculateAddedRampUpDistance( uwStartingSpeed, uwReducedSpeed_fpm );
      uint32_t uiSlowdown = Pattern_CalculateSlowdownDistance( uwReducedSpeed_fpm );
      uint32_t uiEstimatedDistance = ( uiRampUp + uiSlowdown ) * 1.05f;
      if( Param_ReadValue_1Bit(enPARAM1__ReducedMaxSpeed) )
      {
         uiEstimatedDistance = ( uiRampUp + uiSlowdown ) * 1.15f;
      }
      if( uiDistance >= uiEstimatedDistance )
      {
         bSpeedFound = 1;
         break;
      }
   }

   if(!bSpeedFound)
   {
      uwReducedSpeed_fpm = stMotionCtrl.ucMinRelevelSpeed_FPM;
   }

   flSpeed = uwReducedSpeed_fpm/60.0f;
   if( flSpeed > stMotionCtrl.flContractSpeed_FPS )
   {
      flSpeed = stMotionCtrl.flContractSpeed_FPS;
   }
   return flSpeed;
}
/*--------------------------------------------------------------------------

   Get ramp up distance distance. slimmed down from Pattern_GenerateAccelRun

--------------------------------------------------------------------------*/
uint32_t Pattern_CalculateRampUpDistance( uint16_t uwSpeed )
{
   float flPosition;
   float flTa, flTb, flTc, flVa, flVb, flVc, flXa, flXb, flAa;
   uint32_t uiReturn = 0;
   float fSpeed = uwSpeed/60.0f;
   Motion_Profile eProfile = GetMotion_Profile();
   if ( fSpeed >= stCurveParameters[ eProfile ].fShortPatternMinAccelSpeed_FPS )
   {
      flTa = stCurveParameters[ eProfile ].fAccel/stCurveParameters[ eProfile ].fJerkInAccel;
      flTb = fSpeed/stCurveParameters[ eProfile ].fAccel
           + stCurveParameters[ eProfile ].fAccel*(1.0f/stCurveParameters[ eProfile ].fJerkInAccel - 1.0f/stCurveParameters[ eProfile ].fJerkOutAccel)/2.0f;
      flTc = fSpeed/stCurveParameters[ eProfile ].fAccel
           + stCurveParameters[ eProfile ].fAccel*(1.0f/stCurveParameters[ eProfile ].fJerkInAccel + 1.0f/stCurveParameters[ eProfile ].fJerkOutAccel)/2.0f;

      flVa = stCurveParameters[ eProfile ].fJerkInAccel * powf(flTa, 2.0f)/2.0f;

      flVb = stCurveParameters[ eProfile ].fAccel * ( flTb - flTa )
           + flVa;
      flVc = fSpeed;

      flXa = stCurveParameters[ eProfile ].fJerkInAccel * powf(flTa, 3.0f)/6.0f;
      flXb = stCurveParameters[ eProfile ].fAccel * powf( flTb - flTa, 2.0f)/2.0f
           + flVa * ( flTb - flTa )
           + flXa;

      flAa = 0;

      flPosition = stCurveParameters[ eProfile ].fAccel * powf(flTc - flTb, 2.0f)/2.0f
                 - stCurveParameters[ eProfile ].fJerkOutAccel * powf(flTc - flTb, 3.0f)/6.0f
                 + flVb *(flTc - flTb)
                 + flXb;

      uiReturn = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
   }
   else /* Use very short pattern */
   {
      flTc = fSpeed;
      flVc = fSpeed;

#ifdef ARM_MATH
      arm_sqrt_f32( 2.0f*(stCurveParameters[ eProfile ].fJerkInAccel + stCurveParameters[ eProfile ].fJerkOutAccel)*flVc/(stCurveParameters[ eProfile ].fJerkInAccel*stCurveParameters[ eProfile ].fJerkOutAccel), &flTc );
#else
      flTc = sqrtf( 2.0f*(stCurveParameters[ eProfile ].fJerkInAccel + stCurveParameters[ eProfile ].fJerkOutAccel)*flVc/(stCurveParameters[ eProfile ].fJerkInAccel*stCurveParameters[ eProfile ].fJerkOutAccel) );
#endif

      flTa = flTc*stCurveParameters[ eProfile ].fJerkOutAccel/(stCurveParameters[ eProfile ].fJerkInAccel + stCurveParameters[ eProfile ].fJerkOutAccel);
      flVa = stCurveParameters[ eProfile ].fJerkInAccel * powf( flTa, 2.0f )/2.0f;
      flXa = stCurveParameters[ eProfile ].fJerkInAccel * powf( flTa, 3.0f )/6.0f;

      flAa = stCurveParameters[ eProfile ].fJerkInAccel * flTa;

      flPosition = flAa * powf(flTc - flTa, 2.0f)/2.0f
                 - stCurveParameters[ eProfile ].fJerkOutAccel * powf(flTc - flTa, 3.0f)/6.0f
                 + flVa*( flTc - flTa )
                 + flXa;

      uiReturn = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
   }

   return uiReturn;
}

/*--------------------------------------------------------------------------

   Get slowdown distance. slimmed down from Pattern_GenerateDecelRun
--------------------------------------------------------------------------*/
uint32_t Pattern_CalculateSlowdownDistance( uint16_t uwSpeed )
{
   float flPosition;
   float flTa, flTb, flTc, flVi, flVa, flVb, flXa, flXb, flAa;
   uint32_t uiReturn = 0;
   float fSpeed = uwSpeed/60.0f;
   float fLevelingSpeed = 0;
   Motion_Profile eProfile = GetMotion_Profile();
   if(stCurveParameters[ eProfile ].uwLevelingDistance)
   {
      fLevelingSpeed = stCurveParameters[ eProfile ].uwLevelingSpeed_FPM / 60.0f;
   }
   float fSpeedChange = fSpeed - fLevelingSpeed;
   if ( fSpeedChange >= stCurveParameters[ eProfile ].fShortPatternMinDecelSpeed_FPS )
   {
      flTa = stCurveParameters[ eProfile ].fDecel/stCurveParameters[ eProfile ].fJerkInDecel;
      flTb = fSpeed/stCurveParameters[ eProfile ].fDecel
           + stCurveParameters[ eProfile ].fDecel*(1.0f/stCurveParameters[ eProfile ].fJerkInDecel - 1.0f/stCurveParameters[ eProfile ].fJerkOutDecel)/2.0f;
      flTc = fSpeed/stCurveParameters[ eProfile ].fDecel
           + stCurveParameters[ eProfile ].fDecel*(1.0f/stCurveParameters[ eProfile ].fJerkInDecel + 1.0f/stCurveParameters[ eProfile ].fJerkOutDecel)/2.0f;

      flVi = fLevelingSpeed;
      flVa = stCurveParameters[ eProfile ].fJerkInDecel * powf(flTa, 2.0f)/2.0f
           + flVi;

      flVb = stCurveParameters[ eProfile ].fDecel*(flTb - flTa)
           + flVa;

      flXa = flVi * flTa + stCurveParameters[ eProfile ].fJerkInDecel * powf(flTa, 3.0f)/6.0f;
      flXb = stCurveParameters[ eProfile ].fDecel * powf((flTb - flTa), 2.0f)/2.0f
           + flVa * (flTb - flTa)
           + flXa;

      flAa = 0;

      flPosition = stCurveParameters[ eProfile ].fDecel * powf(( flTc - flTb ), 2.0f)/2.0f
                 - stCurveParameters[ eProfile ].fJerkOutDecel * powf(( flTc - flTb ), 3.0f)/6.0f
                 + flVb * ( flTc - flTb )
                 + flXb;
      uiReturn = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
   }
   else /* Use very short pattern */
   {
      flTc = fSpeed;
#ifdef ARM_MATH
      arm_sqrt_f32( 2.0f*(stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel)*fSpeedChange/(stCurveParameters[ eProfile ].fJerkInDecel*stCurveParameters[ eProfile ].fJerkOutDecel), &flTc );
#else
      flTc = sqrtf( 2.0f*(stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel)*flVc/(stCurveParameters[ eProfile ].fJerkInDecel*stCurveParameters[ eProfile ].fJerkOutDecel) );
#endif

      flTa = flTc*stCurveParameters[ eProfile ].fJerkOutDecel/(stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel);

      flAa = flTa*stCurveParameters[ eProfile ].fJerkInDecel;
      flVi = fLevelingSpeed;
      flVa = flVi + flTa*flAa/2.0f;

      flXa = flVi*flTa + stCurveParameters[ eProfile ].fJerkInDecel * powf(flTa, 3.0f)/6.0f;

      flPosition = flAa * powf(( flTc - flTa ), 2.0f)/2.0f
                 - stCurveParameters[ eProfile ].fJerkOutDecel * powf(( flTc - flTa ), 3.0f)/6.0f
                 + flVa * ( flTc - flTa )
                 + flXa;

      uiReturn = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
   }
   /* Add leveling distance */
   uiReturn += stCurveParameters[ eProfile ].uwLevelingDistance;
   return uiReturn;
}

/*--------------------------------------------------------------------------

   Get ramp up distance for added accel. slimmed down from Pattern_GenerateAddedAccelRun

--------------------------------------------------------------------------*/
uint32_t Pattern_CalculateAddedRampUpDistance( uint16_t uwStartSpeed_FPM, uint16_t uwEndSpeed_FPM )
{
   float flPosition;
   float flTa, flTb, flTc, flVi, flVa, flVb, flXa, flXb, flAa;
   uint32_t uiReturn = 0;
   Motion_Profile eProfile = GetMotion_Profile();
   float fSpeedChange = ( uwEndSpeed_FPM - uwStartSpeed_FPM )/60.0f;

   if ( fSpeedChange >= stCurveParameters[ eProfile ].fShortPatternMinAccelSpeed_FPS )
   {
      flTa = stCurveParameters[ eProfile ].fAccel/stCurveParameters[ eProfile ].fJerkInAccel;
      flTb = fSpeedChange/stCurveParameters[ eProfile ].fAccel
           + stCurveParameters[ eProfile ].fAccel*(1.0f/stCurveParameters[ eProfile ].fJerkInAccel - 1.0f/stCurveParameters[ eProfile ].fJerkOutAccel)/2.0f;
      flTc = fSpeedChange/stCurveParameters[ eProfile ].fAccel
           + stCurveParameters[ eProfile ].fAccel*(1.0f/stCurveParameters[ eProfile ].fJerkInAccel + 1.0f/stCurveParameters[ eProfile ].fJerkOutAccel)/2.0f;

      flVi = uwStartSpeed_FPM/60.0f;
      flVa = stCurveParameters[ eProfile ].fJerkInAccel * powf(flTa, 2.0f) / 2.0f
           + flVi;

      flVb = stCurveParameters[ eProfile ].fAccel * (flTb - flTa)
           + flVa;

      flXa = stCurveParameters[ eProfile ].fJerkInAccel * powf(flTa, 3.0f)/6.0f
           + flVi * flTa;

      flXb = stCurveParameters[ eProfile ].fAccel * powf(flTb - flTa, 2.0f)/2.0f
           + flVa*(flTb - flTa)
           + flXa;

      flAa = 0;

      flPosition = stCurveParameters[ eProfile ].fAccel * powf(flTc - flTb, 2.0f)/2.0f
                 - stCurveParameters[ eProfile ].fJerkOutAccel * powf(flTc - flTb, 3.0f)/6.0f
                 + flVb *(flTc - flTb)
                 + flXb;

      uiReturn = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
   }
   else /* Use very short pattern */
   {
      float flTc = fSpeedChange;
#ifdef ARM_MATH
      arm_sqrt_f32( 2*(stCurveParameters[ eProfile ].fJerkInAccel + stCurveParameters[ eProfile ].fJerkOutAccel)*fSpeedChange
                    / (stCurveParameters[ eProfile ].fJerkInAccel * stCurveParameters[ eProfile ].fJerkOutAccel), &flTc );
#else
      flTc = sqrtf( 2*(stCurveParameters[ eProfile ].fJerkInAccel + stCurveParameters[ eProfile ].fJerkOutAccel)*fSpeedChange
                    / (stCurveParameters[ eProfile ].fJerkInAccel * stCurveParameters[ eProfile ].fJerkOutAccel) );
#endif
      flTa = flTc * stCurveParameters[ eProfile ].fJerkOutAccel
          / (stCurveParameters[ eProfile ].fJerkInAccel + stCurveParameters[ eProfile ].fJerkOutAccel);
      flVi = uwStartSpeed_FPM/60.0f;
      flVa = stCurveParameters[ eProfile ].fJerkInAccel * powf( flTa, 2.0f )/2.0f
           + flVi;

      flXa = stCurveParameters[ eProfile ].fJerkInAccel * powf( flTa, 3.0f )/6.0f
           + flVi * flTa;

      flAa = flTa*stCurveParameters[ eProfile ].fJerkInAccel;

      flPosition = flAa * powf(flTc - flTa, 2.0f)/2.0f
                 - stCurveParameters[ eProfile ].fJerkOutAccel * powf(flTc - flTa, 3.0f)/6.0f
                 + flVa*( flTc - flTa )
                 + flXa;

      uiReturn = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
   }

   return uiReturn;
}
/*--------------------------------------------------------------------------

   Get ramp down distance for reduced speed run. slimmed down from Pattern_GenerateReducedSpdRun
   Requires uwStartSpeed_FPM > uwEndSpeed_FPM
--------------------------------------------------------------------------*/
uint32_t Pattern_CalculateReducedSpdDistance( uint16_t uwStartSpeed_FPM, uint16_t uwEndSpeed_FPM )
{
   float flPosition;
   float flTa, flTb, flTc, flVi, flVa, flVb, flXa, flXb, flAa;
   uint32_t uiReturn = 0;
   Motion_Profile eProfile = GetMotion_Profile();
   float fSpeedChange = ( uwStartSpeed_FPM - uwEndSpeed_FPM )/60.0f;

   if ( fSpeedChange >= stCurveParameters[ eProfile ].fShortPatternMinDecelSpeed_FPS )
   {
      flTa = stCurveParameters[ eProfile ].fDecel/stCurveParameters[ eProfile ].fJerkInDecel;
      flTb = fSpeedChange/stCurveParameters[ eProfile ].fDecel
           + stCurveParameters[ eProfile ].fDecel*(1.0f/stCurveParameters[ eProfile ].fJerkInDecel - 1.0f/stCurveParameters[ eProfile ].fJerkOutDecel)/2.0f;
      flTc = fSpeedChange/stCurveParameters[ eProfile ].fDecel
           + stCurveParameters[ eProfile ].fDecel*(1.0f/stCurveParameters[ eProfile ].fJerkInDecel + 1.0f/stCurveParameters[ eProfile ].fJerkOutDecel)/2.0f;

      flVi = uwEndSpeed_FPM/60.0f;
      flVa = stCurveParameters[ eProfile ].fJerkInDecel * powf(flTa, 2.0f) / 2.0f
           + flVi;

      flVb = stCurveParameters[ eProfile ].fDecel * (flTb - flTa)
           + flVa;

      flXa = stCurveParameters[ eProfile ].fJerkInDecel * powf(flTa, 3.0f)/6.0f
           + flVi * flTa;

      flXb = stCurveParameters[ eProfile ].fDecel * powf(flTb - flTa, 2.0f)/2.0f
           + flVa*(flTb - flTa)
           + flXa;

      flAa = 0;

      flPosition = stCurveParameters[ eProfile ].fDecel * powf(flTc - flTb, 2.0f)/2.0f
                 - stCurveParameters[ eProfile ].fJerkOutDecel * powf(flTc - flTb, 3.0f)/6.0f
                 + flVb *(flTc - flTb)
                 + flXb;

      uiReturn = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
   }
   else /* Use very short pattern */
   {
      flTc = fSpeedChange;
#ifdef ARM_MATH
      arm_sqrt_f32( 2*(stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel)*fSpeedChange
                    / (stCurveParameters[ eProfile ].fJerkInDecel * stCurveParameters[ eProfile ].fJerkOutDecel), &flTc );
#else
      flTc = sqrtf( 2*(stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel)*fSpeedChange
                    / (stCurveParameters[ eProfile ].fJerkInDecel * stCurveParameters[ eProfile ].fJerkOutDecel) );
#endif
      flTa = flTc * stCurveParameters[ eProfile ].fJerkOutDecel
           / (stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel);
      flVi = uwEndSpeed_FPM/60.0f;
      flVa = stCurveParameters[ eProfile ].fJerkInDecel * powf( flTa, 2.0f )/2.0f
           + flVi;

      flXa = stCurveParameters[ eProfile ].fJerkInDecel * powf( flTa, 3.0f )/6.0f
           + flVi * flTa;

      flAa = flTa*stCurveParameters[ eProfile ].fJerkInDecel;

      flPosition = flAa * powf(flTc - flTa, 2.0f)/2.0f
                 - stCurveParameters[ eProfile ].fJerkOutDecel * powf(flTc - flTa, 3.0f)/6.0f
                 + flVa*( flTc - flTa )
                 + flXa;

      uiReturn = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
   }

   return uiReturn;
}
/*--------------------------------------------------------------------------

   This function updates gauwPattern_Speed[time] and gauiPattern_Position[time]
   with new values based on the desired speed to be achieved.
   One of three different pattern profiles is used depending on the input speed

--------------------------------------------------------------------------*/
void Pattern_GenerateAccelRun( float flMaxSpeed_FPS )
{
   float flPosition, flTime_sec;
   float flTa, flTb, flTc, flVa, flVb, flVc, flXa, flXb, flAa;
   uint32_t uiTime_10ms;
   Motion_Profile eProfile = GetMotion_Profile();
   if ( flMaxSpeed_FPS >= stCurveParameters[ eProfile ].fShortPatternMinAccelSpeed_FPS )
   {
      flTa = stCurveParameters[ eProfile ].fAccel/stCurveParameters[ eProfile ].fJerkInAccel;
      flTb = flMaxSpeed_FPS/stCurveParameters[ eProfile ].fAccel
           + stCurveParameters[ eProfile ].fAccel*(1.0f/stCurveParameters[ eProfile ].fJerkInAccel - 1.0f/stCurveParameters[ eProfile ].fJerkOutAccel)/2.0f;
      flTc = flMaxSpeed_FPS/stCurveParameters[ eProfile ].fAccel
           + stCurveParameters[ eProfile ].fAccel*(1.0f/stCurveParameters[ eProfile ].fJerkInAccel + 1.0f/stCurveParameters[ eProfile ].fJerkOutAccel)/2.0f;


      flVa = stCurveParameters[ eProfile ].fJerkInAccel * powf( flTa, 2.0f ) / 2.0f;
      flVb = stCurveParameters[ eProfile ].fAccel * ( flTb - flTa )
           + flVa;
      flVc = flMaxSpeed_FPS;


      flXa = stCurveParameters[ eProfile ].fJerkInAccel * powf( flTa, 3.0f )/6.0f;
      flXb = stCurveParameters[ eProfile ].fAccel * powf( flTb - flTa, 2.0f )/2.0f
           + flVa * ( flTb - flTa )
           + flXa;

      flAa = 0;

      stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel = ( uint32_t )( flTa/stMotionCtrl.flPatternTimeRes_sec + 0.5f );
      stMotionCtrl.stRunParameters.uiEndTime_ConstantAccel = ( uint32_t )( flTb/stMotionCtrl.flPatternTimeRes_sec  + 0.5f );
      stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel = ( uint32_t )( flTc/stMotionCtrl.flPatternTimeRes_sec + 0.5f );

      /* Bound pattern table */
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_ConstantAccel > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_ConstantAccel = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel = MAX_PATTERN_SIZE_BYTES;
      }
      stMotionCtrl.stRunParameters.uiPatternAccelTime = stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel;

      for( uiTime_10ms = 0; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fJerkInAccel*powf(flTime_sec, 3.0f)/6.0f;

         stMotionCtrl.auiAccelPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      for( uiTime_10ms = stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_ConstantAccel; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fAccel*powf(flTime_sec - flTa, 2.0f)/2.0f
                    + flVa * (flTime_sec - flTa)
                    + flXa;

         stMotionCtrl.auiAccelPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      for( uiTime_10ms = stMotionCtrl.stRunParameters.uiEndTime_ConstantAccel; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fAccel * powf( flTime_sec - flTb, 2.0f ) / 2.0f
                    - stCurveParameters[ eProfile ].fJerkOutAccel * powf( flTime_sec - flTb, 3.0f )/6.0f
                    + flVb * ( flTime_sec - flTb )
                    + flXb;

         stMotionCtrl.auiAccelPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      if ( flMaxSpeed_FPS >= stMotionCtrl.flContractSpeed_FPS )
      {
         stMotionCtrl.stAccelVars.ePatternProfile = PATTERN__FULL;
      }
      else
      {
         stMotionCtrl.stAccelVars.ePatternProfile = PATTERN__SHORT;
      }

      stMotionCtrl.stAccelVars.fEndTime_JerkInAccel = flTa;
      stMotionCtrl.stAccelVars.fEndTime_ConstantAccel = flTb;
      stMotionCtrl.stAccelVars.fEndTime_JerkOutAccel = flTc;
      stMotionCtrl.stAccelVars.fEndSpeed_JerkInAccel = flVa;
      stMotionCtrl.stAccelVars.fEndSpeed_ConstantAccel = flVb;
      stMotionCtrl.stAccelVars.fEndSpeed_JerkOutAccel = flVc;
   }
   else /* Use very short pattern */
   {

      flVc = flMaxSpeed_FPS;
      flTc = flMaxSpeed_FPS;

#ifdef ARM_MATH
      arm_sqrt_f32( 2.0f*(stCurveParameters[ eProfile ].fJerkInAccel + stCurveParameters[ eProfile ].fJerkOutAccel)*flVc/(stCurveParameters[ eProfile ].fJerkInAccel*stCurveParameters[ eProfile ].fJerkOutAccel), &flTc );
#else
      flTc = sqrtf( 2.0f*(stCurveParameters[ eProfile ].fJerkInAccel + stCurveParameters[ eProfile ].fJerkOutAccel)*flVc/(stCurveParameters[ eProfile ].fJerkInAccel*stCurveParameters[ eProfile ].fJerkOutAccel) );
#endif

      flTa = flTc * stCurveParameters[ eProfile ].fJerkOutAccel
           / (stCurveParameters[ eProfile ].fJerkInAccel + stCurveParameters[ eProfile ].fJerkOutAccel);

      flVa = stCurveParameters[ eProfile ].fJerkInAccel * powf( flTa, 2.0f )/2.0f;

      flXa = stCurveParameters[ eProfile ].fJerkInAccel * powf( flTa, 3.0f )/6.0f;

      flAa = flTa*stCurveParameters[ eProfile ].fJerkInAccel;

      stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel = ( uint32_t )( flTa/stMotionCtrl.flPatternTimeRes_sec + 0.5f );
      stMotionCtrl.stRunParameters.uiEndTime_ConstantAccel = stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel;
      stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel = ( uint32_t )( flTc/stMotionCtrl.flPatternTimeRes_sec + 0.5f );

      /* Bound pattern table */
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_ConstantAccel > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_ConstantAccel = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel = MAX_PATTERN_SIZE_BYTES;
      }
      stMotionCtrl.stRunParameters.uiPatternAccelTime = stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel;

      for( uiTime_10ms = 0; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fJerkInAccel*powf(flTime_sec, 3.0f)/6.0f;

         stMotionCtrl.auiAccelPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      for( uiTime_10ms = stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = flAa * powf( flTime_sec - flTa, 2.0f )/2.0f
                    - stCurveParameters[ eProfile ].fJerkOutAccel * powf(( flTime_sec - flTa ), 3.0f)/6.0f
                    + flVa * ( flTime_sec - flTa )
                    + flXa;

         stMotionCtrl.auiAccelPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      stMotionCtrl.stAccelVars.ePatternProfile = PATTERN__VERY_SHORT;

      stMotionCtrl.stAccelVars.fEndTime_JerkInAccel = flTa;
      stMotionCtrl.stAccelVars.fEndTime_JerkOutAccel = flTc;
      stMotionCtrl.stAccelVars.fEndSpeed_JerkInAccel = flVa;
      stMotionCtrl.stAccelVars.fEndSpeed_ConstantAccel = flVa;
      stMotionCtrl.stAccelVars.fEndSpeed_JerkOutAccel = flVc;
      stMotionCtrl.stAccelVars.fMaxAccel = flAa;
   }
   stMotionCtrl.uwMaxSpeedOfCurrentRun_FPM = ( uint16_t )( 60.0f*flMaxSpeed_FPS  + 0.5f );
   stMotionCtrl.stRunParameters.uiRampUpDistance = stMotionCtrl.auiAccelPattern_Position[ stMotionCtrl.stRunParameters.uiPatternAccelTime - 1 ];
}
/*--------------------------------------------------------------------------

   This function updates gauwPattern_Speed[time] and gauiPattern_Position[time]
   with new values based on the desired speed to be achieved.
   One of three different pattern profiles is used depending on the input speed

--------------------------------------------------------------------------*/
void Pattern_GenerateDecelRun( float flMaxSpeed_FPS )
{
   float flPosition, flTime_sec;
   float flTa, flTb, flTc, flVi, flVa, flVb, flVc, flXa, flXb, flAa;
   uint32_t uiTime_10ms;
   float fLevelingSpeed = 0;
   Motion_Profile eProfile = GetMotion_Profile();
   if(stCurveParameters[ eProfile ].uwLevelingDistance)
   {
      fLevelingSpeed = stCurveParameters[ eProfile ].uwLevelingSpeed_FPM / 60.0f;
   }
   float fSpeedChange = flMaxSpeed_FPS - fLevelingSpeed;
   if ( fSpeedChange >= stCurveParameters[ eProfile ].fShortPatternMinDecelSpeed_FPS )
   {
      flTa = stCurveParameters[ eProfile ].fDecel/stCurveParameters[ eProfile ].fJerkInDecel;
      flTb = fSpeedChange/stCurveParameters[ eProfile ].fDecel
           + stCurveParameters[ eProfile ].fDecel*(1.0f/stCurveParameters[ eProfile ].fJerkInDecel - 1.0f/stCurveParameters[ eProfile ].fJerkOutDecel)/2.0f;
      flTc = fSpeedChange/stCurveParameters[ eProfile ].fDecel
           + stCurveParameters[ eProfile ].fDecel*(1.0f/stCurveParameters[ eProfile ].fJerkInDecel + 1.0f/stCurveParameters[ eProfile ].fJerkOutDecel)/2.0f;

      flVi = fLevelingSpeed;
      flVa = stCurveParameters[ eProfile ].fJerkInDecel * powf(flTa, 2.0f)/2.0f
           + flVi;
      flVb = stCurveParameters[ eProfile ].fDecel*(flTb - flTa)
           + flVa;
      flVc = fSpeedChange
           + flVi;

      flXa = stCurveParameters[ eProfile ].fJerkInDecel * powf(flTa, 3.0f)/6.0f
           + flVi * flTa;
      flXb = stCurveParameters[ eProfile ].fDecel * powf( flTb - flTa, 2.0f )/2.0f
           + flVa * (flTb - flTa)
           + flXa;

      flAa = 0;

      stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel = ( uint32_t )( flTa/stMotionCtrl.flPatternTimeRes_sec + 0.5f );
      stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel = ( uint32_t )( flTb/stMotionCtrl.flPatternTimeRes_sec  + 0.5f );
      stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel = ( uint32_t )( flTc/stMotionCtrl.flPatternTimeRes_sec + 0.5f );

      /* Bound pattern table */
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel = MAX_PATTERN_SIZE_BYTES;
      }
      stMotionCtrl.stRunParameters.uiPatternDecelTime = stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel;

      for( uiTime_10ms = 0; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fJerkInDecel*powf(flTime_sec, 3.0f)/6.0f
                    + flVi * flTime_sec;

         stMotionCtrl.auiDecelPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      for( uiTime_10ms = stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fDecel*powf((flTime_sec - flTa), 2.0f)/2.0f
                    + flVa*(flTime_sec - flTa)
                    + flXa;

         stMotionCtrl.auiDecelPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      for( uiTime_10ms = stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fDecel * powf( flTime_sec - flTb, 2.0f )/2.0f
                    - stCurveParameters[ eProfile ].fJerkOutDecel * powf( flTime_sec - flTb, 3.0f)/6.0f
                    + flVb * ( flTime_sec - flTb )
                    + flXb;

         stMotionCtrl.auiDecelPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      if ( flMaxSpeed_FPS >= stMotionCtrl.flContractSpeed_FPS )
      {
         stMotionCtrl.stDecelVars.ePatternProfile = PATTERN__FULL;
      }
      else
      {
         stMotionCtrl.stDecelVars.ePatternProfile = PATTERN__SHORT;
      }

      stMotionCtrl.stDecelVars.fEndTime_JerkInDecel = flTa;
      stMotionCtrl.stDecelVars.fEndTime_ConstantDecel = flTb;
      stMotionCtrl.stDecelVars.fEndTime_JerkOutDecel = flTc;
      stMotionCtrl.stDecelVars.fStartSpeed_JerkInDecel = flVi;
      stMotionCtrl.stDecelVars.fEndSpeed_JerkInDecel = flVa;
      stMotionCtrl.stDecelVars.fEndSpeed_ConstantDecel = flVb;
      stMotionCtrl.stDecelVars.fEndSpeed_JerkOutDecel = flVc;

      stMotionCtrl.stDecelVars.fMaxDecel = stCurveParameters[ eProfile ].fDecel;
   }
   else /* Use very short pattern */
   {
      flVc = flMaxSpeed_FPS;
      flTc = flMaxSpeed_FPS;
#ifdef ARM_MATH
      arm_sqrt_f32( 2.0f*(stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel)*fSpeedChange/(stCurveParameters[ eProfile ].fJerkInDecel*stCurveParameters[ eProfile ].fJerkOutDecel), &flTc );
#else
      flTc = sqrtf( 2.0f*(stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel)*flVc/(stCurveParameters[ eProfile ].fJerkInDecel*stCurveParameters[ eProfile ].fJerkOutDecel) );
#endif

      flTa = flTc*stCurveParameters[ eProfile ].fJerkOutDecel
           / (stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel);

      flAa = flTa*stCurveParameters[ eProfile ].fJerkInDecel;
      flVi = fLevelingSpeed;
      flVa = stCurveParameters[ eProfile ].fJerkInDecel * powf(flTa, 2.0f)/2.0f
           + flVi;

      flXa = flVi*flTa + stCurveParameters[ eProfile ].fJerkInDecel * powf(flTa, 3.0f)/6.0f;

      stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel = ( uint32_t )( flTa/stMotionCtrl.flPatternTimeRes_sec + 0.5f );
      stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel = stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel;
      stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel = ( uint32_t )( flTc/stMotionCtrl.flPatternTimeRes_sec + 0.5f );

      /* Bound pattern table */
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel = MAX_PATTERN_SIZE_BYTES;
      }
      stMotionCtrl.stRunParameters.uiPatternDecelTime = stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel;

      for( uiTime_10ms = 0; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fJerkInDecel*powf(flTime_sec, 3.0f)/6.0f
                    + flVi*flTime_sec;

         stMotionCtrl.auiDecelPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      for( uiTime_10ms = stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = flAa * powf( flTime_sec - flTa, 2.0f )/2.0f
                    - stCurveParameters[ eProfile ].fJerkOutDecel * powf( flTime_sec - flTa, 3.0f)/6.0f
                    + flVa * ( flTime_sec - flTa )
                    + flXa;

         stMotionCtrl.auiDecelPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }
      stMotionCtrl.stDecelVars.fEndTime_JerkInDecel = flTa;
      stMotionCtrl.stDecelVars.fEndTime_JerkOutDecel = flTc;

      stMotionCtrl.stDecelVars.fStartSpeed_JerkInDecel = flVi;
      stMotionCtrl.stDecelVars.fEndSpeed_JerkInDecel = flVa;
      stMotionCtrl.stDecelVars.fEndSpeed_ConstantDecel = flVa;
      stMotionCtrl.stDecelVars.fEndSpeed_JerkOutDecel = flVc;

      stMotionCtrl.stDecelVars.fMaxDecel = flAa;

      stMotionCtrl.stDecelVars.ePatternProfile = PATTERN__VERY_SHORT;
   }

   stMotionCtrl.stRunParameters.uiSlowdownDistance = stMotionCtrl.auiDecelPattern_Position[ stMotionCtrl.stRunParameters.uiPatternDecelTime - 1 ]
                                                   + stCurveParameters[ eProfile ].uwLevelingDistance;
}

/*--------------------------------------------------------------------------

   This is called from DestinationChangeDuringAccel() when
   a request to extend the travel distance is received while
   the car is accelerating to a non-contract speed. The car will
   then finish the current pattern and then add another pattern
   from the current max speed to the new max speed.

--------------------------------------------------------------------------*/
void Pattern_GenerateAddedAccelRun( float flMaxSpeed_FPS )
{
   float flPosition, flTime_sec;
   float flTa, flTb, flTc, flVi, flVa, flVb, flVc, flXa, flXb, flAa;
   uint32_t uiTime_10ms;

   flVi = stMotionCtrl.stAccelVars.fEndSpeed_JerkOutAccel;
   float fSpeedChange = flMaxSpeed_FPS - flVi;
   Motion_Profile eProfile = GetMotion_Profile();
   if ( fSpeedChange >= stCurveParameters[ eProfile ].fShortPatternMinAccelSpeed_FPS )
   {
      flTa = stCurveParameters[ eProfile ].fAccel/stCurveParameters[ eProfile ].fJerkInAccel;
      flTb = fSpeedChange/stCurveParameters[ eProfile ].fAccel
           + stCurveParameters[ eProfile ].fAccel*(1.0f/stCurveParameters[ eProfile ].fJerkInAccel - 1.0f/stCurveParameters[ eProfile ].fJerkOutAccel)/2.0f;
      flTc = fSpeedChange/stCurveParameters[ eProfile ].fAccel
           + stCurveParameters[ eProfile ].fAccel*(1.0f/stCurveParameters[ eProfile ].fJerkInAccel + 1.0f/stCurveParameters[ eProfile ].fJerkOutAccel)/2.0f;

      flVa = stCurveParameters[ eProfile ].fJerkInAccel * powf(flTa, 2.0f) / 2.0f
           + flVi;
      flVb = stCurveParameters[ eProfile ].fAccel * (flTb - flTa)
           + flVa;
      flVc = flMaxSpeed_FPS;

      flXa = stCurveParameters[ eProfile ].fJerkInAccel * powf(flTa, 3.0f)/6.0f
           + flVi * flTa;

      flXb = stCurveParameters[ eProfile ].fAccel * powf(flTb - flTa, 2.0f)/2.0f
           + flVa*(flTb - flTa)
           + flXa;

      flAa = 0;

      stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel2 = ( uint32_t )( flTa/stMotionCtrl.flPatternTimeRes_sec + 0.5f );
      stMotionCtrl.stRunParameters.uiEndTime_ConstantAccel2 = ( uint32_t )( flTb/stMotionCtrl.flPatternTimeRes_sec  + 0.5f );
      stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel2 = ( uint32_t )( flTc/stMotionCtrl.flPatternTimeRes_sec + 0.5f );

      /* Bound pattern table */
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel2 > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel2 = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_ConstantAccel2 > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_ConstantAccel2 = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel2 > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel2 = MAX_PATTERN_SIZE_BYTES;
      }
      stMotionCtrl.stRunParameters.uiAddedAccelTime = stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel2;

      for( uiTime_10ms = 0; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel2; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fJerkInAccel*powf(flTime_sec, 3.0f)/6.0f
                    + flVi * flTime_sec;

         stMotionCtrl.auiAddedPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      for( uiTime_10ms = stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel2; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_ConstantAccel2; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fAccel*powf( flTime_sec - flTa, 2.0f )/2.0f
                    + flVa * ( flTime_sec - flTa )
                    + flXa;

         stMotionCtrl.auiAddedPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      for( uiTime_10ms = stMotionCtrl.stRunParameters.uiEndTime_ConstantAccel2; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel2; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fAccel * powf( flTime_sec - flTb, 2.0f )/2.0f
                    - stCurveParameters[ eProfile ].fJerkOutAccel * powf( flTime_sec - flTb, 3.0f )/6.0f
                    + flVb*(flTime_sec - flTb)
                    + flXb;

         stMotionCtrl.auiAddedPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      stMotionCtrl.stAddedAccelVars.fEndTime_JerkInAccel = flTa;
      stMotionCtrl.stAddedAccelVars.fEndTime_ConstantAccel = flTb;
      stMotionCtrl.stAddedAccelVars.fEndTime_JerkOutAccel = flTc;
      stMotionCtrl.stAddedAccelVars.fEndSpeed_JerkInAccel = flVa;
      stMotionCtrl.stAddedAccelVars.fEndSpeed_JerkOutAccel = flVc;

      stMotionCtrl.stAddedAccelVars.fMaxAccel = stCurveParameters[ eProfile ].fAccel;

      if ( fSpeedChange >= stMotionCtrl.flContractSpeed_FPS )
      {
         stMotionCtrl.stAddedAccelVars.ePatternProfile = PATTERN__FULL;
      }
      else
      {
         stMotionCtrl.stAddedAccelVars.ePatternProfile = PATTERN__SHORT;
      }
   }
   else /* Use very short pattern */
   {
      flTc = flMaxSpeed_FPS;
#ifdef ARM_MATH
      arm_sqrt_f32( 2*(stCurveParameters[ eProfile ].fJerkInAccel + stCurveParameters[ eProfile ].fJerkOutAccel)*fSpeedChange
                    / (stCurveParameters[ eProfile ].fJerkInAccel * stCurveParameters[ eProfile ].fJerkOutAccel), &flTc );
#else
      flTc = sqrtf( 2*(stCurveParameters[ eProfile ].fJerkInAccel + stCurveParameters[ eProfile ].fJerkOutAccel)*fSpeedChange
                    / (stCurveParameters[ eProfile ].fJerkInAccel * stCurveParameters[ eProfile ].fJerkOutAccel) );
#endif
      flTa = flTc * stCurveParameters[ eProfile ].fJerkOutAccel
           / (stCurveParameters[ eProfile ].fJerkInAccel + stCurveParameters[ eProfile ].fJerkOutAccel);

      flVa = stCurveParameters[ eProfile ].fJerkInAccel * powf( flTa, 2.0f )/2.0f
           + flVi;
      flVc = fSpeedChange
           + flVi;

      flXa = stCurveParameters[ eProfile ].fJerkInAccel * powf( flTa, 3.0f )/6.0f
           + flVi * flTa;

      flAa = flTa*stCurveParameters[ eProfile ].fJerkInAccel;

      stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel2 = ( uint32_t )( flTa/stMotionCtrl.flPatternTimeRes_sec + 0.5f );
      stMotionCtrl.stRunParameters.uiEndTime_ConstantAccel2 = stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel2;
      stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel2 = ( uint32_t )( flTc/stMotionCtrl.flPatternTimeRes_sec + 0.5f );

      /* Bound pattern table */
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel2 > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel2 = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_ConstantAccel2 > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_ConstantAccel2 = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel2 > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel2 = MAX_PATTERN_SIZE_BYTES;
      }
      stMotionCtrl.stRunParameters.uiAddedAccelTime = stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel2;

      for( uiTime_10ms = 0; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel2; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fJerkInAccel*powf(flTime_sec, 3.0f)/6.0f
                    + flVi * flTime_sec;

         stMotionCtrl.auiAddedPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      for( uiTime_10ms = stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel2; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel2; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = flAa * powf( flTime_sec - flTa, 2.0f )/2.0f
                    - stCurveParameters[ eProfile ].fJerkOutAccel * powf(flTime_sec - flTa, 3.0f)/6.0f
                    + flVa*(flTime_sec - flTa)
                    + flXa;

         stMotionCtrl.auiAddedPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }
      stMotionCtrl.stAddedAccelVars.fEndTime_JerkInAccel = flTa;
      stMotionCtrl.stAddedAccelVars.fEndTime_JerkOutAccel = flTc;
      stMotionCtrl.stAddedAccelVars.fEndSpeed_JerkInAccel = flVa;
      stMotionCtrl.stAddedAccelVars.fEndSpeed_JerkOutAccel = flVc;
      stMotionCtrl.stAddedAccelVars.fMaxAccel = flAa;

      stMotionCtrl.stAddedAccelVars.ePatternProfile = PATTERN__VERY_SHORT;
   }
   stMotionCtrl.uwMaxSpeedOfCurrentRun_FPM = ( uint16_t )( 60.0f*flMaxSpeed_FPS  + 0.5f );
}
/*--------------------------------------------------------------------------

   Called from SpeedLimitChangeDuringAccel()/SpeedLimitChangeDuringCruising()

   Require flStartSpeed_FPS > flEndSpeed_FPS
--------------------------------------------------------------------------*/
void Pattern_GenerateReducedSpdRun( float flStartSpeed_FPS, float flEndSpeed_FPS)
{
   float flPosition, flTime_sec;
   float flTa, flTb, flTc, flVi, flVa, flVb, flVc, flXa, flXb, flAa;
   uint32_t uiTime_10ms;
   Motion_Profile eProfile = GetMotion_Profile();
   float fSpeedChange = flStartSpeed_FPS - flEndSpeed_FPS;
   if ( fSpeedChange >= stCurveParameters[ eProfile ].fShortPatternMinDecelSpeed_FPS )
   {
      flTa = stCurveParameters[ eProfile ].fDecel/stCurveParameters[ eProfile ].fJerkInDecel;
      flTb = fSpeedChange/stCurveParameters[ eProfile ].fDecel
           + stCurveParameters[ eProfile ].fDecel*(1.0f/stCurveParameters[ eProfile ].fJerkInDecel - 1.0f/stCurveParameters[ eProfile ].fJerkOutDecel)/2.0f;
      flTc = fSpeedChange/stCurveParameters[ eProfile ].fDecel
           + stCurveParameters[ eProfile ].fDecel*(1.0f/stCurveParameters[ eProfile ].fJerkInDecel + 1.0f/stCurveParameters[ eProfile ].fJerkOutDecel)/2.0f;

      flVi = flEndSpeed_FPS;
      flVa = stCurveParameters[ eProfile ].fJerkInDecel * powf(flTa, 2.0f)/2.0f
           + flVi;
      flVb = stCurveParameters[ eProfile ].fDecel*(flTb - flTa)
           + flVa;
      flVc = fSpeedChange
           + flVi;

      flXa = stCurveParameters[ eProfile ].fJerkInDecel * powf(flTa, 3.0f)/6.0f
           + flVi * flTa;
      flXb = stCurveParameters[ eProfile ].fDecel * powf( flTb - flTa, 2.0f )/2.0f
           + flVa * (flTb - flTa)
           + flXa;

      flAa = 0;

      stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel2 = ( uint32_t )( flTa/stMotionCtrl.flPatternTimeRes_sec + 0.5f );
      stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel2 = ( uint32_t )( flTb/stMotionCtrl.flPatternTimeRes_sec  + 0.5f );
      stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel2 = ( uint32_t )( flTc/stMotionCtrl.flPatternTimeRes_sec + 0.5f );

      /* Bound pattern table */
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel2 > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel2 = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel2 > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel2 = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel2 > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel2 = MAX_PATTERN_SIZE_BYTES;
      }
      stMotionCtrl.stRunParameters.uiRSLTime = stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel2;

      for( uiTime_10ms = 0; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel2; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fJerkInDecel*powf(flTime_sec, 3.0f)/6.0f
                    + flVi * flTime_sec;

         stMotionCtrl.auiAddedPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      for( uiTime_10ms = stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel2; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel2; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fDecel*powf((flTime_sec - flTa), 2.0f)/2.0f
                    + flVa*(flTime_sec - flTa)
                    + flXa;

         stMotionCtrl.auiAddedPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      for( uiTime_10ms = stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel2; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel2; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fDecel * powf( flTime_sec - flTb, 2.0f )/2.0f
                    - stCurveParameters[ eProfile ].fJerkOutDecel * powf( flTime_sec - flTb, 3.0f)/6.0f
                    + flVb * ( flTime_sec - flTb )
                    + flXb;

         stMotionCtrl.auiAddedPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      stMotionCtrl.stRSLDecelVars.ePatternProfile = PATTERN__SHORT;

      stMotionCtrl.stRSLDecelVars.fEndTime_JerkInDecel = flTa;
      stMotionCtrl.stRSLDecelVars.fEndTime_ConstantDecel = flTb;
      stMotionCtrl.stRSLDecelVars.fEndTime_JerkOutDecel = flTc;
      stMotionCtrl.stRSLDecelVars.fStartSpeed_JerkInDecel = flVi;
      stMotionCtrl.stRSLDecelVars.fEndSpeed_JerkInDecel = flVa;
      stMotionCtrl.stRSLDecelVars.fEndSpeed_ConstantDecel = flVb;
      stMotionCtrl.stRSLDecelVars.fEndSpeed_JerkOutDecel = flVc;

      stMotionCtrl.stRSLDecelVars.fMaxDecel = stCurveParameters[ eProfile ].fDecel;
   }
   else /* Use very short pattern */
   {
      flVc = flStartSpeed_FPS;
      flTc = flStartSpeed_FPS;
#ifdef ARM_MATH
      arm_sqrt_f32( 2.0f*(stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel)*fSpeedChange/(stCurveParameters[ eProfile ].fJerkInDecel*stCurveParameters[ eProfile ].fJerkOutDecel), &flTc );
#else
      flTc = sqrtf( 2.0f*(stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel)*flVc/(stCurveParameters[ eProfile ].fJerkInDecel*stCurveParameters[ eProfile ].fJerkOutDecel) );
#endif

      flTa = flTc*stCurveParameters[ eProfile ].fJerkOutDecel
           / (stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel);

      flAa = flTa*stCurveParameters[ eProfile ].fJerkInDecel;
      flVi = flEndSpeed_FPS;
      flVa = stCurveParameters[ eProfile ].fJerkInDecel * powf(flTa, 2.0f)/2.0f
           + flVi;

      flXa = flVi*flTa
           + stCurveParameters[ eProfile ].fJerkInDecel * powf(flTa, 3.0f)/6.0f;

      stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel2 = ( uint32_t )( flTa/stMotionCtrl.flPatternTimeRes_sec + 0.5f );
      stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel2 = stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel2;
      stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel2 = ( uint32_t )( flTc/stMotionCtrl.flPatternTimeRes_sec + 0.5f );

      /* Bound pattern table */
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel2 > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel2 = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel2 > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel2 = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel2 > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel2 = MAX_PATTERN_SIZE_BYTES;
      }
      stMotionCtrl.stRunParameters.uiRSLTime = stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel2;

      for( uiTime_10ms = 0; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel2; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fJerkInDecel*powf(flTime_sec, 3.0f)/6.0f
                    + flVi*flTime_sec;

         stMotionCtrl.auiAddedPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      for( uiTime_10ms = stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel2; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel2; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = flAa * powf( flTime_sec - flTa, 2.0f )/2.0f
                    - stCurveParameters[ eProfile ].fJerkOutDecel * powf( flTime_sec - flTa, 3.0f)/6.0f
                    + flVa * ( flTime_sec - flTa )
                    + flXa;

         stMotionCtrl.auiAddedPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }
      stMotionCtrl.stRSLDecelVars.fEndTime_JerkInDecel = flTa;
      stMotionCtrl.stRSLDecelVars.fEndTime_JerkOutDecel = flTc;

      stMotionCtrl.stRSLDecelVars.fStartSpeed_JerkInDecel = flVi;
      stMotionCtrl.stRSLDecelVars.fEndSpeed_JerkInDecel = flVa;
      stMotionCtrl.stRSLDecelVars.fEndSpeed_ConstantDecel = flVa;
      stMotionCtrl.stRSLDecelVars.fEndSpeed_JerkOutDecel = flVc;

      stMotionCtrl.stRSLDecelVars.fMaxDecel = flAa;

      stMotionCtrl.stRSLDecelVars.ePatternProfile = PATTERN__VERY_SHORT;
   }

   stMotionCtrl.stRunParameters.uiRSLDecelDistance = stMotionCtrl.auiAddedPattern_Position[ stMotionCtrl.stRunParameters.uiRSLTime - 1 ];
}

/*--------------------------------------------------------------------------
   Takes time index into the position array,
   and returns the speed to command
--------------------------------------------------------------------------*/
inline uint16_t Pattern_GetAccelCommandSpeed(uint32_t uiTimeIndex)
{
   uint16_t uwCommandSpeed = 0;
   float flSpeed;
   float flTime_sec = uiTimeIndex * stMotionCtrl.flPatternTimeRes_sec;
   stMotionCtrl.ucSubAlarm_Plus1 = 0;
   if( stMotionCtrl.stAccelVars.ePatternProfile != PATTERN__VERY_SHORT )
   {
      if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel )
      {
         flSpeed = stCurveParameters[ GetMotion_Profile() ].fJerkInAccel * powf(flTime_sec, 2.0f)/2.0f;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_ConstantAccel )
      {
         flSpeed = stCurveParameters[ GetMotion_Profile() ].fAccel*(flTime_sec - stMotionCtrl.stAccelVars.fEndTime_JerkInAccel)
                 + stMotionCtrl.stAccelVars.fEndSpeed_JerkInAccel;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel )
      {
         flSpeed = -stCurveParameters[ GetMotion_Profile() ].fJerkOutAccel*powf(stMotionCtrl.stAccelVars.fEndTime_JerkOutAccel - flTime_sec, 2.0f)/2.0f
                 + stMotionCtrl.stAccelVars.fEndSpeed_JerkOutAccel;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else
      {
         stMotionCtrl.ucSubAlarm_Plus1 = SUBALARM_SYS_DEBUG__ACCEL_CMD_T+1;
      }
   }
   else
   {
      if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel )
      {
         flSpeed = stCurveParameters[ GetMotion_Profile() ].fJerkInAccel*powf(flTime_sec, 2.0f)/2.0f;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel )
      {
         flSpeed = -stCurveParameters[ GetMotion_Profile() ].fJerkOutAccel*powf(stMotionCtrl.stAccelVars.fEndTime_JerkInAccel - flTime_sec, 2.0f)/2.0f
                 - stMotionCtrl.stAccelVars.fMaxAccel*(stMotionCtrl.stAccelVars.fEndTime_JerkInAccel - flTime_sec)
                 + stMotionCtrl.stAccelVars.fEndSpeed_JerkInAccel;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else
      {
         stMotionCtrl.ucSubAlarm_Plus1 = SUBALARM_SYS_DEBUG__ACCEL_CMD_T+1;
      }
   }

   /* Bound speed */
   if( uwCommandSpeed > stMotionCtrl.uwContractSpeed_FPM )
   {
      uwCommandSpeed = stMotionCtrl.uwContractSpeed_FPM;
      stMotionCtrl.ucSubAlarm_Plus1 = SUBALARM_SYS_DEBUG__ACCEL_CMD_V+1;
   }
   else if( ( stMotionCtrl.bReleveling )
         && ( uwCommandSpeed < stMotionCtrl.ucMinRelevelSpeed_FPM ) )
   {
      uwCommandSpeed = stMotionCtrl.ucMinRelevelSpeed_FPM;
   }
   else if( uwCommandSpeed < stMotionCtrl.uwMinAccelSpeed_FPM )
   {
      uwCommandSpeed = stMotionCtrl.uwMinAccelSpeed_FPM;
   }

   return uwCommandSpeed;
}

inline uint16_t Pattern_GetDecelCommandSpeed(uint32_t uiTimeIndex)
{
   uint16_t uwCommandSpeed = 0;
   float flSpeed;
   float flTime_sec = uiTimeIndex * stMotionCtrl.flPatternTimeRes_sec;
   stMotionCtrl.ucSubAlarm_Plus1 = 0;
   if( stMotionCtrl.stDecelVars.ePatternProfile != PATTERN__VERY_SHORT )
   {
      if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel )
      {
         flSpeed = stMotionCtrl.stDecelVars.fStartSpeed_JerkInDecel + stCurveParameters[ GetMotion_Profile() ].fJerkInDecel*powf(flTime_sec, 2.0f)/2.0f;

         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel )
      {
         flSpeed = stMotionCtrl.stDecelVars.fEndSpeed_JerkInDecel
                 + stCurveParameters[ GetMotion_Profile() ].fDecel*(flTime_sec - stMotionCtrl.stDecelVars.fEndTime_JerkInDecel);
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel )
      {
         flSpeed = stMotionCtrl.stDecelVars.fEndSpeed_JerkOutDecel - stCurveParameters[ GetMotion_Profile() ].fJerkOutDecel*powf(stMotionCtrl.stDecelVars.fEndTime_JerkOutDecel - flTime_sec, 2.0f)/2.0f;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else
      {
         stMotionCtrl.ucSubAlarm_Plus1 = SUBALARM_SYS_DEBUG__DECEL_CMD_T+1;
      }
   }
   else
   {
      if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel )
      {
         flSpeed = stMotionCtrl.stDecelVars.fStartSpeed_JerkInDecel
                 + stCurveParameters[ GetMotion_Profile() ].fJerkInDecel*powf(flTime_sec, 2.0f)/2.0f;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel )
      {
//         flSpeed = stMotionCtrl.stDecelVars.fEndSpeed_JerkOutDecel
//                 - stCurveParameters[ GetMotion_Profile() ].fJerkOutDecel*powf(stMotionCtrl.stDecelVars.fEndTime_JerkOutDecel - flTime_sec, 2.0f)/2.0f;
         flSpeed = -stCurveParameters[ GetMotion_Profile() ].fJerkOutDecel*powf(stMotionCtrl.stDecelVars.fEndTime_JerkInDecel - flTime_sec, 2.0f)/2.0f
                   - stMotionCtrl.stDecelVars.fMaxDecel*(stMotionCtrl.stDecelVars.fEndTime_JerkInDecel - flTime_sec)
                   + stMotionCtrl.stDecelVars.fEndSpeed_JerkInDecel;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else
      {
         stMotionCtrl.ucSubAlarm_Plus1 = SUBALARM_SYS_DEBUG__DECEL_CMD_T+1;
      }
   }

   /* Bound speed */
   if( uwCommandSpeed > stMotionCtrl.uwContractSpeed_FPM )
   {
      uwCommandSpeed = stMotionCtrl.uwContractSpeed_FPM;
      stMotionCtrl.ucSubAlarm_Plus1 = SUBALARM_SYS_DEBUG__DECEL_CMD_V+1;
   }
   else if(!uwCommandSpeed)
   {
      uwCommandSpeed = 1;
   }

   return uwCommandSpeed;
}

inline uint16_t Pattern_GetAddedAccelCommandSpeed(uint32_t uiTimeIndex)
{
   uint16_t uwCommandSpeed = 0;
   float flSpeed;
   float flTime_sec = uiTimeIndex * stMotionCtrl.flPatternTimeRes_sec;
   float flVi = stMotionCtrl.stAccelVars.fEndSpeed_JerkOutAccel;
   stMotionCtrl.ucSubAlarm_Plus1 = 0;
   if( stMotionCtrl.stAddedAccelVars.ePatternProfile != PATTERN__VERY_SHORT )
   {
      if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel2 )
      {
         flSpeed = stCurveParameters[ GetMotion_Profile() ].fJerkInAccel * powf(flTime_sec, 2.0f)/2.0f
                 + flVi;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_ConstantAccel2 )
      {
         flSpeed = stCurveParameters[ GetMotion_Profile() ].fAccel*(flTime_sec - stMotionCtrl.stAddedAccelVars.fEndTime_JerkInAccel)
                 + stMotionCtrl.stAddedAccelVars.fEndSpeed_JerkInAccel;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel2 )
      {
         flSpeed = -stCurveParameters[ GetMotion_Profile() ].fJerkOutAccel*powf(stMotionCtrl.stAddedAccelVars.fEndTime_JerkOutAccel - flTime_sec, 2.0f)/2.0f
                 + stMotionCtrl.stAddedAccelVars.fEndSpeed_JerkOutAccel;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else
      {
         stMotionCtrl.ucSubAlarm_Plus1 = SUBALARM_SYS_DEBUG__A_ACCEL_CMD_T+1;
      }
   }
   else
   {
      if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkInAccel2 )
      {
         flSpeed = stCurveParameters[ GetMotion_Profile() ].fJerkInAccel*powf(flTime_sec, 2.0f)/2.0f
                 + flVi;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkOutAccel2 )
      {
//         flSpeed = -stCurveParameters[ GetMotion_Profile() ].fJerkOutAccel*powf(stMotionCtrl.stAddedAccelVars.fEndTime_JerkOutAccel - flTime_sec, 2.0f)/2.0f
//                 + stMotionCtrl.stAddedAccelVars.fEndSpeed_JerkOutAccel;
         flSpeed = -stCurveParameters[ GetMotion_Profile() ].fJerkOutAccel*powf(stMotionCtrl.stAddedAccelVars.fEndTime_JerkInAccel - flTime_sec, 2.0f)/2.0f
                 - stMotionCtrl.stAddedAccelVars.fMaxAccel*(stMotionCtrl.stAddedAccelVars.fEndTime_JerkInAccel - flTime_sec)
                 + stMotionCtrl.stAddedAccelVars.fEndSpeed_JerkInAccel;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else
      {
         stMotionCtrl.ucSubAlarm_Plus1 = SUBALARM_SYS_DEBUG__A_ACCEL_CMD_T+1;
      }
   }

   /* Bound speed */
   if( uwCommandSpeed > stMotionCtrl.uwContractSpeed_FPM )
   {
      uwCommandSpeed = stMotionCtrl.uwContractSpeed_FPM;
      stMotionCtrl.ucSubAlarm_Plus1 = SUBALARM_SYS_DEBUG__A_ACCEL_CMD_V+1;
   }

   return uwCommandSpeed;
}

inline uint16_t Pattern_GetReducedSpdCommandSpeed(uint32_t uiTimeIndex)
{
   uint16_t uwCommandSpeed = 0;
   float flSpeed;
   float flTime_sec = uiTimeIndex * stMotionCtrl.flPatternTimeRes_sec;
   stMotionCtrl.ucSubAlarm_Plus1 = 0;
   if( stMotionCtrl.stRSLDecelVars.ePatternProfile != PATTERN__VERY_SHORT )
   {
      if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel2 )
      {
         flSpeed = stMotionCtrl.stRSLDecelVars.fStartSpeed_JerkInDecel + stCurveParameters[ GetMotion_Profile() ].fJerkInDecel*powf(flTime_sec, 2.0f)/2.0f;

         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel2 )
      {
         flSpeed = stMotionCtrl.stRSLDecelVars.fEndSpeed_JerkInDecel
                 + stCurveParameters[ GetMotion_Profile() ].fDecel*(flTime_sec - stMotionCtrl.stRSLDecelVars.fEndTime_JerkInDecel);
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel2 )
      {
         flSpeed = stMotionCtrl.stRSLDecelVars.fEndSpeed_JerkOutDecel - stCurveParameters[ GetMotion_Profile() ].fJerkOutDecel*powf(stMotionCtrl.stRSLDecelVars.fEndTime_JerkOutDecel - flTime_sec, 2.0f)/2.0f;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else
      {
         stMotionCtrl.ucSubAlarm_Plus1 = SUBALARM_SYS_DEBUG__RSL_DECEL_CMD_T+1;
      }
   }
   else
   {
      if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel2 )
      {
         flSpeed = stMotionCtrl.stRSLDecelVars.fStartSpeed_JerkInDecel
                 + stCurveParameters[ GetMotion_Profile() ].fJerkInDecel*powf(flTime_sec, 2.0f)/2.0f;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel2 )
      {
//         flSpeed = stMotionCtrl.stRSLDecelVars.fEndSpeed_JerkOutDecel
//                 - stCurveParameters[ GetMotion_Profile() ].fJerkOutDecel*powf(stMotionCtrl.stRSLDecelVars.fEndTime_JerkOutDecel - flTime_sec, 2.0f)/2.0f;
         flSpeed = -stCurveParameters[ GetMotion_Profile() ].fJerkOutDecel*powf(stMotionCtrl.stRSLDecelVars.fEndTime_JerkInDecel - flTime_sec, 2.0f)/2.0f
                   - stMotionCtrl.stRSLDecelVars.fMaxDecel*(stMotionCtrl.stRSLDecelVars.fEndTime_JerkInDecel - flTime_sec)
                   + stMotionCtrl.stRSLDecelVars.fEndSpeed_JerkInDecel;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else
      {
         stMotionCtrl.ucSubAlarm_Plus1 = SUBALARM_SYS_DEBUG__RSL_DECEL_CMD_T+1;
      }
   }

   /* Bound speed */
   if( uwCommandSpeed > stMotionCtrl.uwContractSpeed_FPM )
   {
      uwCommandSpeed = stMotionCtrl.uwContractSpeed_FPM;
      stMotionCtrl.ucSubAlarm_Plus1 = SUBALARM_SYS_DEBUG__RSL_DECEL_CMD_V+1;
   }
   else if(!uwCommandSpeed)
   {
      uwCommandSpeed = 1;
   }

   return uwCommandSpeed;
}
/*--------------------------------------------------------------------------
   Takes current time index into run's pattern array
   and returns the position stored in the lookup table at that time index
--------------------------------------------------------------------------*/
inline uint32_t Pattern_GetAccelPosition(uint32_t uiTimeIndex)
{
   return stMotionCtrl.auiAccelPattern_Position[uiTimeIndex];
}
inline uint32_t Pattern_GetDecelPosition(uint32_t uiTimeIndex)
{
   return stMotionCtrl.auiDecelPattern_Position[uiTimeIndex];
}
inline uint32_t Pattern_GetAddedAccelPosition(uint32_t uiTimeIndex)
{
   return stMotionCtrl.auiAddedPattern_Position[uiTimeIndex];
}
inline uint32_t Pattern_GetReducedSpdPosition(uint32_t uiTimeIndex)
{
   return stMotionCtrl.auiAddedPattern_Position[uiTimeIndex];
}
/*----------------------------------------------------------------------------
   Returns in half mm counts, the minimum distance to perform
   a short run pattern with current motion profile
 *----------------------------------------------------------------------------*/
uint32_t Pattern_GetMinShortRunDistance_Counts()
{
   Motion_Profile eProfile = GetMotion_Profile();
   uint32_t uiDistance = stCurveParameters[ eProfile ].uiShortPatternMinAccelDistance
                       + stCurveParameters[ eProfile ].uiShortPatternMinDecelDistance
                       + stCurveParameters[ eProfile ].uwLevelingDistance;
   return uiDistance * 1.05;
}
/*----------------------------------------------------------------------------
   Run in progress estimation functions
 *----------------------------------------------------------------------------*/
uint32_t Pattern_GetRemainingRunTime()
{
   //todo
   return 0;
}
/*----------------------------------------------------------------------------
   TODO move
   Returns next reachable floor
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   Returns 1 if s curve is invalid
 *----------------------------------------------------------------------------*/
uint8_t Pattern_CheckForInvalidSCurve(Motion_Profile eProfile)
{
   uint8_t bInvalid  = (stCurveParameters[eProfile].uiMaxAccelTime >= MAX_PATTERN_SIZE_BYTES) ? 1:0;
   uint8_t bInvalid2 = (stCurveParameters[eProfile].uiMaxDecelTime >= MAX_PATTERN_SIZE_BYTES) ? 1:0;
   return bInvalid | bInvalid2;
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
