/******************************************************************************
 * @file     mod_rtc.c
 * @author   Keith Soneda
 * @version  V1.00
 * @date     1, August 2016
 *
 * @note   Manages time between RTC sync time updates.
 *          Time - RTC in Unix format (seconds since Jan 01 1970. (UTC))
 *          SystemTime = time maintained locally, w.r.t. SyncTime.
 *          SyncTime = time received from internet connected source.
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru.h"
#include <stdint.h>
#include "sys.h"
#include <time.h>
#include "datagrams.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_module gstMod_RTC =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define RTC_MOD_RUN_PERIOD_1MS        (1000U)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint32_t ulReceivedSyncTime;// Latest sync time packet received
static uint32_t ulSyncTime;// loaded sync time
static uint32_t ulSysTime;
static int32_t iLocalCount_1s;
static uint32_t ulDifferenceCount_1s;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Unix time 1s res
 -----------------------------------------------------------------------------*/
void RTC_SetSyncTime( uint32_t ulSyncTime_1s )
{
   ulReceivedSyncTime = ulSyncTime_1s;
}
/*-----------------------------------------------------------------------------
Unix time 1s res
 -----------------------------------------------------------------------------*/
uint32_t RTC_GetSyncTime()
{
   return ulReceivedSyncTime;
}
/*-----------------------------------------------------------------------------
Unix time 1s res
 -----------------------------------------------------------------------------*/
uint32_t RTC_GetLocalTime()
{
   return ulSysTime;
}
/*-----------------------------------------------------------------------------
Returns time of day in seconds
-----------------------------------------------------------------------------*/
uint32_t RTC_GetTimeOfDay_TotalSeconds(void)
{
   uint32_t uiSeconds = RTC_GetLocalTime() % 86400;
   return uiSeconds;
}

/* seconds after the minute, 0 to 59 */
uint8_t RTC_GetTimeOfDay_Sec(void)
{
   time_t uiUnixTime = RTC_GetLocalTime();
   struct tm *pstTime = gmtime(&uiUnixTime);
   return pstTime->tm_sec;
}
/* minutes after the hour, 0 to 59 */
uint8_t RTC_GetTimeOfDay_Min(void)
{
   time_t uiUnixTime = RTC_GetLocalTime();
   struct tm *pstTime = gmtime(&uiUnixTime);
   return pstTime->tm_min;
}
/* hours since midnight, 0 to 23 */
uint8_t RTC_GetTimeOfDay_Hour(void)
{
   time_t uiUnixTime = RTC_GetLocalTime();
   struct tm *pstTime = gmtime(&uiUnixTime);
   return pstTime->tm_hour;
}
/* days since Sunday, 0 to 6 */
st_rtc_day_of_week RTC_GetDayOfWeek(void)
{
   time_t uiUnixTime = RTC_GetLocalTime();
   struct tm *pstTime = gmtime(&uiUnixTime);
   return pstTime->tm_wday;
}
/* months since January, 0 to 11 */
uint8_t RTC_GetMonthOfTheYear(void)
{
   time_t uiUnixTime = RTC_GetLocalTime();
   struct tm *pstTime = gmtime(&uiUnixTime);
   return pstTime->tm_mon;
}
/* day of the month, 1 to 31 */
uint8_t RTC_GetDayOfTheMonth(void)
{
   time_t uiUnixTime = RTC_GetLocalTime();
   struct tm *pstTime = gmtime(&uiUnixTime);
   return pstTime->tm_mday;
}
/* year */
uint16_t RTC_GetYear(void)
{
   time_t uiUnixTime = RTC_GetLocalTime();
   struct tm *pstTime = gmtime(&uiUnixTime);
   return pstTime->tm_year+1900;
}
/*-----------------------------------------------------------------------------
   If new sync time received, update & reset local count.
 -----------------------------------------------------------------------------*/
static void RTC_UpdateSyncTime()
{
   if( ulSyncTime != ulReceivedSyncTime )
   {
      ulSyncTime = ulReceivedSyncTime;
      iLocalCount_1s = Sys_GetTickCount(enTickCount_1s);
   }
}

/*-----------------------------------------------------------------------------
Maintain time locally w.r.t. SyncTime packet.

 -----------------------------------------------------------------------------*/
static void RTC_UpdateLocalTime( void )
{
   /* If this is a MRB processor with an on board RTC, suppress the local time tracking */
   if( ( Param_ReadValue_1Bit(enPARAM1__EnableBoardRTC) )
    && ( GetSystemNodeID() == SYS_NODE__MRB ) )
   {
      ulSysTime = ulSyncTime;
      ulDifferenceCount_1s = 0;
   }
   else
   {
      int32_t lCountDifference = Sys_TickCount_Compare(iLocalCount_1s, enTickCount_1s);
      if(lCountDifference < 0)
      {
         ulDifferenceCount_1s = (uint32_t)(-1*lCountDifference);
      }
      else
      {
         ulDifferenceCount_1s = 0;
      }
   }
   ulSysTime = ulSyncTime + ulDifferenceCount_1s;
}


/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 500;
   pstThisModule->uwRunPeriod_1ms = RTC_MOD_RUN_PERIOD_1MS;

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   /* If this is a MRB processor with an on board RTC, update the local time via the RTC */
   if( ( Param_ReadValue_1Bit(enPARAM1__EnableBoardRTC) )
    && ( GetSystemNodeID() == SYS_NODE__MRB ) )
   {
      uint32_t uiReadTime_sec = RTC_ReadPeripheralTime_Unix();
      RTC_SetSyncTime(uiReadTime_sec);
   }

   RTC_UpdateSyncTime();
   RTC_UpdateLocalTime();

   return 0;
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
