/******************************************************************************
 *
 * @file     lifo.h
 * @brief    LIFO log header file
 * @version  V1.00
 * @date     12/19/16
 * @author   Keith Soneda
 *
 * @note    Adapted from lpc open ringbuffer.c
 *          Data stored newest (head) to oldest (tail)
 *          Elements not removed on retrieval
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "lifo.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define RB_INDH(rb)                ((rb)->head & ((rb)->count - 1))
#define RB_INDT(rb)                ((rb)->tail & ((rb)->count - 1))
#define RB_INDI(rb, i)                (((rb)->head + i ) & ((rb)->count - 1))//index wrt head
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
   Initialize the LIFO
 -----------------------------------------------------------------------------*/
uint8_t LIFO_Init(RINGBUFF_T *LIFO, void *buffer, int itemSize, int count)
{
   LIFO->data = buffer;
   LIFO->count = count;
   LIFO->itemSz = itemSize;
   LIFO->head = LIFO->tail = 0;

   return 1;
}
/*-----------------------------------------------------------------------------
   Read element in LIFO,the
 -----------------------------------------------------------------------------*/
uint8_t LIFO_GetElement(RINGBUFF_T *LIFO, void *data, uint8_t ucIndex )
{
   uint8_t bReturn = 0;

   if( ucIndex < RingBuffer_GetCount(LIFO) )
   {
      uint8_t *ptr = LIFO->data;
      ptr += RB_INDI(LIFO, ucIndex) * LIFO->itemSz;
      memcpy(data, ptr, LIFO->itemSz);
      bReturn = 1;
   }

   return bReturn;
}

/*-----------------------------------------------------------------------------
Insert a single item into Ring Buffer. If full, remove the oldest element
 -----------------------------------------------------------------------------*/
uint8_t LIFO_Insert(RINGBUFF_T *LIFO, const void *data)
{
   uint8_t *ptr = LIFO->data;

   /* If the LIFO is full, remove the oldest element to make room */
   if (RingBuffer_IsFull(LIFO))
   {
      LIFO->tail++;
   }

   ptr += RB_INDH(LIFO) * LIFO->itemSz;
   memcpy(ptr, data, LIFO->itemSz);
   LIFO->head++;

   return 1;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
