/******************************************************************************
 *
 * @file     sys.c
 * @brief    Systen File
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "sys.h"

#include "timer_17xx_40xx.h"
#include <stdint.h>

// TODO: find a better place for this
const uint32_t OscRateIn = 12000000;

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

struct sys_data
{
   int32_t giSys_RunningTickCount[ NUM_SYS_TICK_COUNTERS ];

   int32_t giSys_UpTime_1s;
};

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct sys_data gstSysData;

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
int32_t Sys_GetTickCount( enum enum_sys_tick_count_resolutions enResolution )
{
   int32_t iTickCount = 0;

   if ( enResolution < NUM_SYS_TICK_COUNTERS )
   {
      // To minimize CPU use, the 1 us timer is read from hardware as needed.
      LPC_TIMER_T * pTMR = LPC_TIMER1;
      gstSysData.giSys_RunningTickCount[ enTickCount_1us ] = pTMR->TC;

      iTickCount = gstSysData.giSys_RunningTickCount[ enResolution ];
   }

   return iTickCount;
}

/*-----------------------------------------------------------------------------

   Compares the passed tick count argument iArg_TickCount_1ms with the current
   running 1ms system tick count. Returns a signed value indicating if
   iArg_TickCount occurs before or after the current tick count.

   Return value:
      A positive return value indicates the number of counts in the future
      that iArg_TickCount will match the system tick count.

      A negative return value indicates the number of counts in the past
      that iArg_TickCount matched the system tick count.

      A return value of zero means that iArg_TickCount is equal to the
      current system tick count.

 -----------------------------------------------------------------------------*/
int32_t Sys_TickCount_Compare( int32_t iArg_TickCount,
                               enum enum_sys_tick_count_resolutions enResolution
                             )
{
   int32_t iRelativeTime = 0;

   if ( enResolution < NUM_SYS_TICK_COUNTERS )
   {
      iRelativeTime = iArg_TickCount - Sys_GetTickCount( enResolution );
   }

   return iRelativeTime;
}

/*-----------------------------------------------------------------------------

   This function is called from the main loop to update system timers except
   for the 1ms running counter which is updated by the timer ISR and the 1 us
   running counter which is incremented by the Timer1 hardware.

   This function should be called at least every 50 ms to provide decent updates
   of the 100 ms tick count.

 -----------------------------------------------------------------------------*/
void Sys_UpdateTimers( void )
{
   int32_t iRelativeTimeOfLastUpdate_1ms;
   static int32_t iTickCount_100ms;
   static int32_t iTickCount_500ms;
   static int32_t iTimeOfNextUpdate_1ms = 100;  // first update 100ms after start-up

   iRelativeTimeOfLastUpdate_1ms = Sys_TickCount_Compare( iTimeOfNextUpdate_1ms, enTickCount_1ms );

   // Is it time to update?
   if ( iRelativeTimeOfLastUpdate_1ms <= 0 )
   {
      // Adjust iTimeOfNextUpdate_1ms 100 ms into the future.
      iTimeOfNextUpdate_1ms += 100;

      ++gstSysData.giSys_RunningTickCount[ enTickCount_100ms ];

      ++iTickCount_100ms;

      if ( iTickCount_100ms >= 5 )
      {
         iTickCount_100ms = 0;

         ++gstSysData.giSys_RunningTickCount[ enTickCount_500ms ];

         ++iTickCount_500ms;

         if ( iTickCount_500ms >= 2 )
         {
            iTickCount_500ms = 0;

            ++gstSysData.giSys_RunningTickCount[ enTickCount_1s ];
         }
      }
   }
}

/*-----------------------------------------------------------------------------

   Set hardware Timer0 to interrupt the system every 1 millisecond.

 -----------------------------------------------------------------------------*/
static void Init_Timer0( void )
{
   LPC_TIMER_T * pTMR = LPC_TIMER0;

   // The Prescale Counter (PC) count to 60 in 1 us. It then increments the
   // Timer Counter (TC). The TC will reach 1000 in 1 ms.
   pTMR->PR = 60;

   // Set Match Register 0 to wait for the TC to reach 1000 then interrupt
   // the system. Also reset TC back to 0 when this happens so it can
   // continue counting.
   pTMR->MR[ 0 ] = 1000;
   pTMR->MCR = 0x03;  // when TC matches MR[0] reset(1) and interrupt(0)

   // Enable the timer to start counting.
   pTMR->TCR = 0x01;

   // Enable Timer0 interrupts on the  Nested Vectored Interrupt Controller.
   NVIC_EnableIRQ( TIMER0_IRQn ); 
}

/*-----------------------------------------------------------------------------

   Set hardware Timer1 as a free running, non-interrupting  1 us timer.

 -----------------------------------------------------------------------------*/
static void Init_Timer1( void )
{
   LPC_TIMER_T * pTMR = LPC_TIMER1;

   // The Prescale Counter (PC) count to 60 in 1 us. It then increments the
   // Timer Counter (TC). The TC will reach 1000 in 1 ms.
   pTMR->PR = 60;

   // Enable the timer to start counting.
   pTMR->TCR = 0x01;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t Sys_MCU_Pin_In( uint32_t uiPort, uint32_t uiPin )
{
   uint_fast8_t bValue = 0;

   // MCU has 6 ports (P0..P5) with 32 pins each.
   if ( (uiPort < 6) && (uiPin < 32) )
   {
      LPC_GPIO_T *ptLPC_GPIO;

      switch ( uiPort )
      {
         case 0: ptLPC_GPIO = LPC_GPIO; break;
         case 1: ptLPC_GPIO = LPC_GPIO1; break;
         case 2: ptLPC_GPIO = LPC_GPIO2; break;
         case 3: ptLPC_GPIO = LPC_GPIO3; break;
         case 4: ptLPC_GPIO = LPC_GPIO4; break;
         case 5: ptLPC_GPIO = LPC_GPIO5; break;
         default: ptLPC_GPIO = 0;
         break;
      }

      //------------------------------------------------
      if ( ptLPC_GPIO )
      {
         uint32_t uiMask = 1 << uiPin;

         if ( ptLPC_GPIO->PIN & uiMask )
         {
            bValue = 1;
         }
      }
   }

   return bValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void Sys_MCU_Pin_Out( uint32_t uiPort, uint32_t uiPin, uint32_t bValue )
{
   //------------------------------------------------
   // MCU has 6 ports (P0..P5) with 32 pins each.
   if ( (uiPort < 6) && (uiPin < 32) )
   {
      //------------------------------------------------
      LPC_GPIO_T *ptLPC_GPIO;

      switch ( uiPort )
      {
         case 0: ptLPC_GPIO = LPC_GPIO; break;
         case 1: ptLPC_GPIO = LPC_GPIO1; break;
         case 2: ptLPC_GPIO = LPC_GPIO2; break;
         case 3: ptLPC_GPIO = LPC_GPIO3; break;
         case 4: ptLPC_GPIO = LPC_GPIO4; break;
         case 5: ptLPC_GPIO = LPC_GPIO5; break;
         default: ptLPC_GPIO = 0;
         break;
      }

      //------------------------------------------------
      if ( ptLPC_GPIO )
      {
         uint32_t uiMask = 1 << uiPin;

         if ( bValue )
         {
            ptLPC_GPIO->SET = uiMask;
         }
         else
         {
            ptLPC_GPIO->CLR = uiMask;
         }
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void Sys_MCU_Pin_SetDir( uint32_t uiPort, uint32_t uiPin )
{
   //------------------------------------------------
   // MCU has 6 ports (P0..P5) with 32 pins each.
   if ( (uiPort < 6) && (uiPin < 32) )
   {
      //------------------------------------------------
      LPC_GPIO_T *ptLPC_GPIO;

      switch ( uiPort )
      {
         case 0: ptLPC_GPIO = LPC_GPIO; break;
         case 1: ptLPC_GPIO = LPC_GPIO1; break;
         case 2: ptLPC_GPIO = LPC_GPIO2; break;
         case 3: ptLPC_GPIO = LPC_GPIO3; break;
         case 4: ptLPC_GPIO = LPC_GPIO4; break;
         case 5: ptLPC_GPIO = LPC_GPIO5; break;
         default: ptLPC_GPIO = 0;
         break;
      }

      //------------------------------------------------
      if ( ptLPC_GPIO )
      {
         uint32_t uiMask = 1 << uiPin;

         ptLPC_GPIO->DIR |= uiMask;
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint32_t Sys_MCU_Port_In( uint32_t uiPort )
{
   uint32_t uiValue = 0;

   //------------------------------------------------
   // MCU has 6 ports (P0..P5).
   if ( uiPort < 6 )
   {
      //------------------------------------------------
      LPC_GPIO_T *ptLPC_GPIO;

      switch ( uiPort )
      {
         case 0: ptLPC_GPIO = LPC_GPIO; break;
         case 1: ptLPC_GPIO = LPC_GPIO1; break;
         case 2: ptLPC_GPIO = LPC_GPIO2; break;
         case 3: ptLPC_GPIO = LPC_GPIO3; break;
         case 4: ptLPC_GPIO = LPC_GPIO4; break;
         case 5: ptLPC_GPIO = LPC_GPIO5; break;
         default: ptLPC_GPIO = 0;
         break;
      }

      //------------------------------------------------
      if ( ptLPC_GPIO )
      {
         uiValue = ptLPC_GPIO->PIN;
      }
   }

   return uiValue;
}

/*-----------------------------------------------------------------------------

   Bits set to 1 in uiWriteEnableMask will be overwritten by uiValue.
   Bits set to 0 will not be written.

   ***NOTE: This is functionally opposite of the hardware MASKn registers.

 -----------------------------------------------------------------------------*/
void Sys_MCU_Port_Out( uint32_t uiPort, uint32_t uiWriteEnableMask, uint32_t uiValue )
{
   //------------------------------------------------
   // MCU has 6 ports (P0..P5).
   if ( uiPort < 6 )
   {
      //------------------------------------------------
      LPC_GPIO_T *ptLPC_GPIO;

      switch ( uiPort )
      {
         case 0: ptLPC_GPIO = LPC_GPIO; break;
         case 1: ptLPC_GPIO = LPC_GPIO1; break;
         case 2: ptLPC_GPIO = LPC_GPIO2; break;
         case 3: ptLPC_GPIO = LPC_GPIO3; break;
         case 4: ptLPC_GPIO = LPC_GPIO4; break;
         case 5: ptLPC_GPIO = LPC_GPIO5; break;
         default: ptLPC_GPIO = 0;
         break;
      }

      //------------------------------------------------
      if ( ptLPC_GPIO )
      {
         // The MCU hardware MASKn registers use set bits to prevent writing to
         // the corresponding pin. I find this more prone to errors than the
         // opposite of specifying which pins to want to write to.
         uint32_t uiWriteProtectMask = ~uiWriteEnableMask;

         // Disabling interrupts is just a precaution in case any ISR needs to
         // write port pins in the future.
         __disable_irq();

         ptLPC_GPIO->MASK = uiWriteProtectMask;

         ptLPC_GPIO->PIN = uiValue;

         ptLPC_GPIO->MASK = 0;  // enable writes to all port pins

         __enable_irq();
      }
   }
}

/*-----------------------------------------------------------------------------
   Returns the ID of the MCU. This value is based on the four MCU pins which
   uniquely identify what type of board this processor is installed in.

   Possible return values are:
      enMCU_ID__Unknown
      enMCU_ID__A_SRU_Base
      enMCU_ID__B_SRU_UI
      enMCU_ID__C_Hydro
      enMCU_ID__D_Traction

 -----------------------------------------------------------------------------*/
enum_sys_mcu_ids Sys_GetMCU_ID( void )
{
   uint_fast8_t ucID;
// TODO: This is hard coded. must change this.
   ucID = Sys_MCU_Pin_In( 0, 10 ) ? 1 : 0;

   if ( Sys_MCU_Pin_In( 0, 11 ) )
   {
      ucID |= 0x02;
   }

   if ( Sys_MCU_Pin_In( 0, 17 ) )
   {
      ucID |= 0x04;
   }

   if ( Sys_MCU_Pin_In( 0, 18 ) )
   {
      ucID |= 0x08;
   }

   enum_sys_mcu_ids enMCU_ID;

   switch ( ucID )
   {
      case 1: enMCU_ID = enMCUA_SRU_Base; break;
      case 2: enMCU_ID = enMCUB_SRU_UI; break;
      case 3: enMCU_ID = enMCUC_Hydro; break;
      case 4: enMCU_ID = enMCUD_Traction; break;
      default: enMCU_ID = enMCU_Unknown;
      break;
   }

   return enMCU_ID;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void Sys_Init( void )
{
   // Enable power to the UARTS, CAN controller, Timers, GPIO, and other
   // peripherals that we will be using.
   LPC_SYSCON->PCONP = 0x0200e29e;

   // Initialize hardware Timer0 to interrupt the system every 1.00 ms.
   Init_Timer0();

   // Initialize hardware Timer1 as a free running, non-interrupting  1 us
   // timer. It can be polled to as needed to get high percision timestamps.
   Init_Timer1();

   // Initialize the internal 4032 byte EEPROM.
   EEPROMI__Init();
}

/*-----------------------------------------------------------------------------
   This function is being provided for simulation support. It will return
   non-zero to tell the application to terminate. This will never happen on
   a real controller of course, since the main loop runs forever. That is why
   this function simply returns zero. If in the future, we develop a PC based
   simulator, the Lib_System could be replaced with a simulation library and
   it might be useful to exit the main loop to simulate a power loss.
 -----------------------------------------------------------------------------*/
int Sys_Shutdown( void )
{
   return 0;
}

/*-----------------------------------------------------------------------------

   **************** Interrupt Service Routine -- Timer0 ****************

   This function is in the MCU's interrupt vector table and is called
   asynchronously whenever an interrupt is pending on the peripheral device.

 -----------------------------------------------------------------------------*/
__attribute__ ((section(".after_vectors")))
void TIMER0_IRQHandler( void )
{
   LPC_TIMER_T * pTMR = LPC_TIMER0;

   // Writing a 1 to bit 0 clears the MR0INT flag
   pTMR->IR |= 0x01;

   // Increment the 1 ms system tick counter.
   ++gstSysData.giSys_RunningTickCount[ enTickCount_1ms ];

}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
