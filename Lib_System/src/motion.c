#include "motion.h"

static struct st_motion gstMotion;

inline void SetMotion_RunFlag( uint8_t bRunFlag )
{
   gstMotion.bRunFlag = (bRunFlag) ? 1:0;
}
inline void SetMotion_Direction( enum direction_enum eDirection )
{
   gstMotion.cDirection = eDirection;
}
inline void SetMotion_SpeedCommand( int16_t wSpeed_FPM )
{
   gstMotion.wSpeedCMD = wSpeed_FPM;
}
inline void SetMotion_Destination( uint32_t ulDestination )
{
   gstMotion.ulActualDest = ulDestination & MAX_24BIT_VALUE;
}
inline void SetMotion_Profile( Motion_Profile eProfile )
{
   gstMotion.eProfile = eProfile;
}
//----------------------------------------------
inline uint8_t GetMotion_RunFlag()
{
   return gstMotion.bRunFlag;
}
inline enum direction_enum GetMotion_Direction()
{
   return gstMotion.cDirection;
}
inline int16_t GetMotion_SpeedCommand()
{
   return gstMotion.wSpeedCMD;
}
inline uint32_t GetMotion_Destination()
{
   return gstMotion.ulActualDest;
}
inline Motion_Profile GetMotion_Profile()
{
   return gstMotion.eProfile;
}
