/******************************************************************************
 * @file     param_edit_protocol.c
 * @author   Keith Soneda
 * @version  V1.00
 * @date     1, August 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <string.h>
#include "sru.h"
#include "sys.h"
#include "sys_drive.h"
#include "param_edit_protocol.h"
#include "shared_data.h"
#include "motion.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define DRIVE_WRITE_COUNTDOWN__MS      (300)
#define CRC_MIN_UPDATE_RATE__MS        (60000)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static st_drive_edit_cmd stDriveEditCmd;
//------------------------------------------
static un_sdata_datagram unResponseDatagram;
static uint8_t bDirtyBit; // need to send response

static en_param_edit_protocol eLastReadProtocol; // For quick ID of read requests that take time to answer
static uint16_t uwLastReadIndex;
static uint8_t ucLastBlockIndex;// For PEP__Backup only
static uint8_t ucLastChunkIndex;// For PEP__Backup only

static uint8_t ucLastCRCIndex; // Block index of the last CRC update sent on the group network
static uint32_t auiLastCRCs[NUM_PARAM_BLOCKS];
static uint8_t aucCRC_DirtyBits[BITMAP8_SIZE(NUM_PARAM_BLOCKS)];
static uint16_t auwCRC_ResendCounter_ms[NUM_PARAM_BLOCKS];

static uint8_t bWriteResponse;  // When set to 1, the controller is responding to a write request
static uint32_t uiLastWriteValue; // Contains the value of the last write request recieved from the DAD unit, used to form the response packet
#if ENABLE_PEP_TEST   //todo RM TEST
static uint8_t aucParameterBytes[64];//todo rm test
#endif
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Dirty bit handling for triggering CRC updates sent to the DAD unit
 -----------------------------------------------------------------------------*/
void PEP_SetCRCDirtyBit( uint8_t ucBlockIndex )
{
   Sys_Bit_Set(&aucCRC_DirtyBits[0], ucBlockIndex, 1);
}
void PEP_ClrCRCDirtyBit( uint8_t ucBlockIndex )
{
   Sys_Bit_Set(&aucCRC_DirtyBits[0], ucBlockIndex, 0);
}
uint8_t PEP_GetCRCDirtyBit( uint8_t ucBlockIndex )
{
   return Sys_Bit_Get(&aucCRC_DirtyBits[0], ucBlockIndex);
}
void PEP_ClearCRCResendCounter( uint8_t ucBlockIndex )
{
   auwCRC_ResendCounter_ms[ucBlockIndex] = 0;
}
void PEP_UpdateCRCDirtyBits( uint16_t uwRunPeriod_ms )
{
   for(uint8_t i = 0; i < NUM_PARAM_BLOCKS; i++)
   {
      uint32_t uiNewCRC = Param_GetBlockCRC_RAM(i);
      if( uiNewCRC != auiLastCRCs[i] )
      {
         auiLastCRCs[i] = uiNewCRC;
         PEP_SetCRCDirtyBit(i);
      }
      else if( auwCRC_ResendCounter_ms[i] >= CRC_MIN_UPDATE_RATE__MS )
      {
         PEP_SetCRCDirtyBit(i);
      }
      else
      {
         auwCRC_ResendCounter_ms[i] += uwRunPeriod_ms;
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void GetEditDriveParameterCommand( st_drive_edit_cmd * pstDriveEditCmd )
{
   memcpy(pstDriveEditCmd, &stDriveEditCmd, sizeof(st_drive_edit_cmd));
}

/*-----------------------------------------------------------------------------
// TODO check if parameter is in range
 -----------------------------------------------------------------------------*/
uint8_t SetEditDriveParameterCommand( st_drive_edit_cmd * pstDriveEditCmd, uint8_t bSuppressCounterRefresh )
{
   uint8_t bSuccess = 0;
   if( pstDriveEditCmd->eCommand < NUM_DRIVE_CMD )
   {
      stDriveEditCmd.eCommand = pstDriveEditCmd->eCommand;
      stDriveEditCmd.uwID = pstDriveEditCmd->uwID;
      stDriveEditCmd.iValue = pstDriveEditCmd->iValue;
      if(!bSuppressCounterRefresh)
      {
         stDriveEditCmd.uwCountdown_ms = DRIVE_WRITE_COUNTDOWN__MS;
      }

      bSuccess = 1;
   }
   return bSuccess;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UpdateEditDriveParameterRequest( struct st_module *pstThisModule )
{
   if( stDriveEditCmd.uwCountdown_ms >= pstThisModule->uwRunPeriod_1ms )
   {
      stDriveEditCmd.uwCountdown_ms -= pstThisModule->uwRunPeriod_1ms;
   }
   else
   {
      stDriveEditCmd.uwCountdown_ms = 0;
      stDriveEditCmd.eCommand = DRIVE_CMD__NONE;
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void LoadDatagram_ParamEditRequest( un_sdata_datagram *punDatagram, st_param_edit_req *pstParamEditReq )
{
   punDatagram->aui32[0] = 0;

   punDatagram->auc8[0] |= (pstParamEditReq->bWriteBit & 1) << 7;
   punDatagram->auc8[0] |= (pstParamEditReq->eProtocol & 0x0F) << 3;

   if(pstParamEditReq->eProtocol == PEP__Backup)
   {
      punDatagram->auc8[0] |= (pstParamEditReq->ucBlockIndex & 0x70) >> 4;
      punDatagram->auc8[1] |= (pstParamEditReq->ucBlockIndex & 0x0F) << 4;
      punDatagram->auc8[1] |= (pstParamEditReq->ucChunkIndex & 0x0F);
   }
   else
   {
      punDatagram->auc8[0] |= (pstParamEditReq->uwParamIndex >> 8) & 0x07;
      punDatagram->auc8[1] |= (pstParamEditReq->uwParamIndex & 0xFF);
   }

   punDatagram->aui32[1] = pstParamEditReq->uiData;
}

/*-----------------------------------------------------------------------------
Returns nonzero value for a successful write. en_param_edit_protocol+1
 -----------------------------------------------------------------------------*/
uint8_t UnloadDatagram_ParamEditRequest( un_sdata_datagram *punDatagram, uint8_t bEnableWrite )
{
   uint8_t ucSuccessfulWrite = 0;
   uint8_t bWriteBit = (punDatagram->auc8[0] >> 7) & 1;
   en_param_edit_protocol eProtocol = (punDatagram->auc8[0] >> 3) & 0x0F;
   uint8_t ucBlock = (punDatagram->auc8[1] & 0xF0) >> 4; // For PEP__Backup only
   ucBlock |= (punDatagram->auc8[0] & 0x07 ) << 4;
   uint8_t ucChunk = (punDatagram->auc8[1] & 0x0F); // For PEP__Backup only
   uint16_t uwParamIndex = ( punDatagram->auc8[0] & 0x07 ) << 8;
   uwParamIndex |= punDatagram->auc8[1];
   uint32_t uiData = ( punDatagram->auc8[7] << 0 )  |
                     ( punDatagram->auc8[6] << 8 )  |
                     ( punDatagram->auc8[5] << 16 ) |
                     ( punDatagram->auc8[4] << 24 );
   uint8_t bValid = 0; // When 1, send an immediate read response
   if(bWriteBit)
   {
      if( bEnableWrite )
      {
         switch(eProtocol)
         {
            case PEP__1Bit:
               uiLastWriteValue = uiData & 0x1;
               ucSuccessfulWrite = Param_WriteValue_1Bit(uwParamIndex, uiLastWriteValue);
               bWriteResponse = ucSuccessfulWrite;
               break;
            case PEP__8Bit:
               uiLastWriteValue = uiData & 0xFF;
               ucSuccessfulWrite = Param_WriteValue_8Bit(uwParamIndex, uiLastWriteValue);
               bWriteResponse = ucSuccessfulWrite;
               break;
            case PEP__16Bit:
               uiLastWriteValue = uiData & 0xFFFF;
               ucSuccessfulWrite = Param_WriteValue_16Bit(uwParamIndex, uiLastWriteValue);
               bWriteResponse = ucSuccessfulWrite;
               break;
            case PEP__24Bit:
               uiLastWriteValue = uiData & 0xFFFFFF;
               ucSuccessfulWrite = Param_WriteValue_24Bit(uwParamIndex, uiLastWriteValue);
               bWriteResponse = ucSuccessfulWrite;
               break;
            case PEP__32Bit:
               uiLastWriteValue = uiData;
               ucSuccessfulWrite = Param_WriteValue_32Bit(uwParamIndex, uiLastWriteValue);
               bWriteResponse = ucSuccessfulWrite;
               break;
            case PEP__Magnetek:
               if( Param_ReadValue_8Bit(enPARAM8__DriveSelect) == enDriveType__HPV )
               {
                  st_drive_edit_cmd eEditCmd;
                  eEditCmd.eCommand = DRIVE_CMD__WRITE;
                  eEditCmd.uwID = uwParamIndex;
                  eEditCmd.iValue = uiData;
                  ucSuccessfulWrite = SetEditDriveParameterCommand(&eEditCmd, 0);
               }
               break;
            case PEP__KEB:
               if( Param_ReadValue_8Bit(enPARAM8__DriveSelect) == enDriveType__KEB )
               {
                  st_drive_edit_cmd eEditCmd;
                  eEditCmd.eCommand = DRIVE_CMD__WRITE;
                  eEditCmd.uwID = uwParamIndex;
                  eEditCmd.iValue = uiData;
                  ucSuccessfulWrite = SetEditDriveParameterCommand(&eEditCmd, 0);
               }
               break;
            case PEP__Backup:
               if( ( ucBlock < NUM_PARAM_BLOCKS )
                && ( ucChunk < PEP_BACKUP_CHUNKS_PER_BLOCK ) )
               {
                  //TODO add option to restore car parameters in bulk
               }
               break;
            default: break;
         }
         if(ucSuccessfulWrite)
         {
            ucSuccessfulWrite += eProtocol;
         }
      }
   }
   else
   {
      bWriteResponse = 0;
      uiLastWriteValue = 0;
      unResponseDatagram.auc8[0] = punDatagram->auc8[0];
      unResponseDatagram.auc8[1] = punDatagram->auc8[1];
      unResponseDatagram.auc8[2] = 0;
      unResponseDatagram.auc8[3] = 0;
      unResponseDatagram.aui32[1] = 0;
      switch(eProtocol)
      {
         case PEP__1Bit:
         case PEP__8Bit:
         case PEP__16Bit:
         case PEP__24Bit:
         case PEP__32Bit:
            bValid = 1;
            break;
         case PEP__Magnetek:
            if( Param_ReadValue_8Bit(enPARAM8__DriveSelect) == enDriveType__HPV )
            {
               st_drive_edit_cmd eEditCmd;
               eEditCmd.eCommand = DRIVE_CMD__READ;
               eEditCmd.uwID = uwParamIndex;
               eEditCmd.iValue = 0;
               bValid = SetEditDriveParameterCommand(&eEditCmd, 0);
            }
            break;
         case PEP__KEB:
            if( Param_ReadValue_8Bit(enPARAM8__DriveSelect) == enDriveType__KEB )
            {
               st_drive_edit_cmd eEditCmd;
               eEditCmd.eCommand = DRIVE_CMD__READ;
               eEditCmd.uwID = uwParamIndex;
               eEditCmd.iValue = 0;
               bValid = SetEditDriveParameterCommand(&eEditCmd, 0);
            }
            break;
         case PEP__Backup:
            if( ( ucBlock < NUM_PARAM_BLOCKS )
             && ( ucChunk < PEP_BACKUP_CHUNKS_PER_BLOCK ) )
            {
               bValid = 1;
#if ENABLE_PEP_TEST   //todo RM TEST
               uint8_t ucOffset = ucChunk * 6;
               memcpy(&aucParameterBytes[ucOffset], &punDatagram->auw16[1], sizeof(punDatagram->auw16[1]));
               memcpy(&aucParameterBytes[ucOffset+2], &punDatagram->aui32[1], sizeof(punDatagram->aui32[1]));
               for(uint8_t i = 0; i < enPARAM_NUM_PER_TYPE__UINT32; i++)
               {
                  uint8_t a =0;
                  uint8_t *ptr = &aucParameterBytes[0] + 4*i;
                  ptr+= 2;
                  uint32_t uiData = * (uint32_t *)ptr;
                  a++;
               }
#endif
            }
            break;
         default: break;
      }
   }

   if( bValid || bWriteResponse )
   {
      bDirtyBit = 1;
      eLastReadProtocol = eProtocol;
      uwLastReadIndex = uwParamIndex;
      ucLastBlockIndex = ucBlock;
      ucLastChunkIndex = ucChunk;
   }

   return ucSuccessfulWrite;
}

/*-----------------------------------------------------------------------------
Returns 1 if drive read request has is complete
 -----------------------------------------------------------------------------*/
static uint8_t UnloadDriveReadFeedback()
{
   uint8_t bComplete = 0;
   if( ( GetDriveData_Command() == DRIVE_CMD__READ )
    && ( GetDriveData_ParameterID() == uwLastReadIndex ) )
   {
      int32_t iSavedValue = GetDriveData_ParameterValue();
      /* EXCEPTION FOR MAG DRIVES. RETURNED VALUES ARE 24BIT, BUT REPRESENT SIGNED INT16_T VALUES.
       * SIGN EXTENSION REQUIRED */
      if(( Param_ReadValue_8Bit(enPARAM8__DriveSelect) == enDriveType__HPV )
      && ( iSavedValue & 0x00008000 )
     && (( iSavedValue & 0xFFFF0000 ) == 0 ))
      {
         iSavedValue |= 0xFFFF0000;
      }

      unResponseDatagram.aui32[1] = iSavedValue;
      bComplete = 1;
   }
   return bComplete;
}

/*-----------------------------------------------------------------------------
Returns 1 if there is packet to send
 -----------------------------------------------------------------------------*/
uint8_t LoadDatagram_ParamEditResponse( un_sdata_datagram *punDatagram )
{
   uint8_t bPacketToSend = 0;
   if(bDirtyBit)
   {
      unResponseDatagram.auc8[0] = ( eLastReadProtocol & 0x0F ) << 3;
      if( eLastReadProtocol == PEP__Backup )
      {
         unResponseDatagram.auc8[0] |= ( ucLastBlockIndex >> 4 ) & 0x07;
         unResponseDatagram.auc8[1]  = ( ucLastBlockIndex & 0x0F ) << 4;
         unResponseDatagram.auc8[1] |= ucLastChunkIndex & 0x0F;
      }
      else
      {
         unResponseDatagram.auc8[0] |= ( uwLastReadIndex >> 8 ) & 0x07;
         unResponseDatagram.auc8[1] = uwLastReadIndex & 0xFF;
      }

      switch(eLastReadProtocol)
      {
         case PEP__1Bit:
            unResponseDatagram.aui32[1] = ( bWriteResponse ) ? ( uiLastWriteValue ):( Param_ReadValue_1Bit(uwLastReadIndex) );
            bPacketToSend = 1;
            break;
         case PEP__8Bit:
            unResponseDatagram.aui32[1] = ( bWriteResponse ) ? ( uiLastWriteValue ):( Param_ReadValue_8Bit(uwLastReadIndex) );
            bPacketToSend = 1;
            break;
         case PEP__16Bit:
            unResponseDatagram.aui32[1] = ( bWriteResponse ) ? ( uiLastWriteValue ):( Param_ReadValue_16Bit(uwLastReadIndex) );
            bPacketToSend = 1;
            break;
         case PEP__24Bit:
            unResponseDatagram.aui32[1] = ( bWriteResponse ) ? ( uiLastWriteValue ):( Param_ReadValue_24Bit(uwLastReadIndex) );
            bPacketToSend = 1;
            break;
         case PEP__32Bit:
            unResponseDatagram.aui32[1] = ( bWriteResponse ) ? ( uiLastWriteValue ):( Param_ReadValue_32Bit(uwLastReadIndex) );
            bPacketToSend = 1;
            break;
         case PEP__Magnetek:
            if( Param_ReadValue_8Bit(enPARAM8__DriveSelect) == enDriveType__HPV )
            {
               bPacketToSend = UnloadDriveReadFeedback();
            }
            break;
         case PEP__KEB:
            if( Param_ReadValue_8Bit(enPARAM8__DriveSelect) == enDriveType__KEB )
            {
               bPacketToSend = UnloadDriveReadFeedback();
            }
            break;
         case PEP__Backup:
            if( ( ucLastBlockIndex < NUM_PARAM_BLOCKS )
             && ( ucLastChunkIndex < PEP_BACKUP_CHUNKS_PER_BLOCK ) )
            {
               switch(ucLastChunkIndex)
               {
                  case 0:
                     unResponseDatagram.auw16[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 0) & 0x0000FFFF );
                     unResponseDatagram.aui32[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 0) & 0xFFFF0000 ) >> 16;
                     unResponseDatagram.aui32[1] |= ( Param_ReadChunk_PEP(ucLastBlockIndex, 1) & 0x0000FFFF ) << 16;
                     break;
                  case 1:
                     unResponseDatagram.auw16[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 1) & 0xFFFF0000 ) >> 16;
                     unResponseDatagram.aui32[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 2) );
                     break;
                  case 2:
                     unResponseDatagram.auw16[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 3) & 0x0000FFFF );
                     unResponseDatagram.aui32[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 3) & 0xFFFF0000 ) >> 16;
                     unResponseDatagram.aui32[1] |= ( Param_ReadChunk_PEP(ucLastBlockIndex, 4) & 0x0000FFFF ) << 16;
                     break;
                  case 3:
                     unResponseDatagram.auw16[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 4) & 0xFFFF0000 ) >> 16;
                     unResponseDatagram.aui32[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 5) );
                     break;
                  case 4:
                     unResponseDatagram.auw16[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 6) & 0x0000FFFF );
                     unResponseDatagram.aui32[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 6) & 0xFFFF0000 ) >> 16;
                     unResponseDatagram.aui32[1] |= ( Param_ReadChunk_PEP(ucLastBlockIndex, 7) & 0x0000FFFF ) << 16;
                     break;
                  case 5:
                     unResponseDatagram.auw16[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 7) & 0xFFFF0000 ) >> 16;
                     unResponseDatagram.aui32[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 8) );
                     break;
                  case 6:
                     unResponseDatagram.auw16[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 9) & 0x0000FFFF );
                     unResponseDatagram.aui32[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 9) & 0xFFFF0000 ) >> 16;
                     unResponseDatagram.aui32[1] |= ( Param_ReadChunk_PEP(ucLastBlockIndex, 10) & 0x0000FFFF ) << 16;
                     break;
                  case 7:
                     unResponseDatagram.auw16[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 10) & 0xFFFF0000 ) >> 16;
                     unResponseDatagram.aui32[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 11) );
                     break;
                  case 8:
                     unResponseDatagram.auw16[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 12) & 0x0000FFFF );
                     unResponseDatagram.aui32[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 12) & 0xFFFF0000 ) >> 16;
                     unResponseDatagram.aui32[1] |= ( Param_ReadChunk_PEP(ucLastBlockIndex, 13) & 0x0000FFFF ) << 16;
                     break;
                  case 9:
                     unResponseDatagram.auw16[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 13) & 0xFFFF0000 ) >> 16;
                     unResponseDatagram.aui32[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 14) );
                     break;
                  case 10:
                     unResponseDatagram.auw16[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 15) & 0x0000FFFF );
                     unResponseDatagram.aui32[1] =  ( Param_ReadChunk_PEP(ucLastBlockIndex, 15) & 0xFFFF0000 ) >> 16;
                     break;
                  default: break;
               }
               bPacketToSend = 1;
            }
            break;
         default: break;
      }
   }
   else /* Send local parameter CRCs on change or every 10 seconds */
   {
      for(uint8_t i = 0; i < NUM_PARAM_BLOCKS; i++)
      {
         uint8_t ucBlockToCheck = ( ucLastCRCIndex+i+1 ) % NUM_PARAM_BLOCKS;
         if( PEP_GetCRCDirtyBit(ucBlockToCheck) )
         {
            uint8_t ucChunkIndex = 0; // CRC is always contained within chunk index 0
            PEP_ClrCRCDirtyBit(ucBlockToCheck);
            PEP_ClearCRCResendCounter(ucBlockToCheck);
            eLastReadProtocol = PEP__Backup;
            unResponseDatagram.auc8[0] = ( eLastReadProtocol & 0x0F ) << 3;
            unResponseDatagram.auc8[0] |= ( ucBlockToCheck >> 4 ) & 0x07;
            unResponseDatagram.auc8[1]  = ( ucBlockToCheck & 0x0F ) << 4;
            unResponseDatagram.auc8[1] |= ucChunkIndex & 0x0F;
            unResponseDatagram.auw16[1] =  ( Param_ReadChunk_PEP(ucBlockToCheck, 0) & 0x0000FFFF );
            unResponseDatagram.aui32[1] =  ( Param_ReadChunk_PEP(ucBlockToCheck, 0) & 0xFFFF0000 ) >> 16;
            unResponseDatagram.aui32[1] |= ( Param_ReadChunk_PEP(ucBlockToCheck, 1) & 0x0000FFFF ) << 16;
            ucLastCRCIndex = ucBlockToCheck;
            bPacketToSend = 1;
            break;
         }
      }
   }

   /* Clear need to send slot */
   if(bPacketToSend)
   {
      memcpy(punDatagram, &unResponseDatagram, sizeof(un_sdata_datagram));
      eLastReadProtocol = NUM_PEP;
      uwLastReadIndex = 0;
      ucLastBlockIndex = 0;
      ucLastChunkIndex = 0;
      bWriteResponse = 0;
      uiLastWriteValue = 0;
      bDirtyBit = 0;
   }

   return bPacketToSend;
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
