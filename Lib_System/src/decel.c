/******************************************************************************
 *
 * @file     decel.c
 * @brief    Decel curve functions
 * @version  V1.00
 * @date     11, July 2018
 *
 * @note     This is pattern.c with a reduced memory requirement for
 *           use in ETSL/NTS point generation.
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "decel.h"
#include "sys.h"
#include "motion.h"
#include "position.h"
#define ARM_MATH
#ifdef ARM_MATH
#include "arm_math.h"
#else
#include <math.h>
#endif
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
static Decel_Curve_Parameters stCurveParameters[NUM_MOTION_PROFILES];
static Decel_Motion_Control stMotionCtrl;

Decel_Curve_Parameters * gpstDecelCurveParameters = stCurveParameters;
Decel_Motion_Control * gpstDecelMotionCtrl = &stMotionCtrl;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define SQRT_12            ( 3.4641f )    /* square root of 12 */
#define CBRT_18            ( 2.6207f )    /* cubic root of 18 */
#define ONE_THIRD          ( 0.6667f )    /* 1/3 in float */
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------
   Check if motion parameters have changed when car is stopped
   Returns 1 if changed
--------------------------------------------------------------------------*/
uint8_t Decel_UpdateParameters( Motion_Profile eProfile )
{
   uint8_t bReturn = 0;
   static uint16_t uwLastContractSpeed[NUM_MOTION_PROFILES];
   static uint16_t uwLastLevelingSpeed[NUM_MOTION_PROFILES];
   static uint8_t ucLastDecel[NUM_MOTION_PROFILES];
   static uint8_t ucLastJerkInDecel[NUM_MOTION_PROFILES];
   static uint8_t ucLastJerkOutDecel[NUM_MOTION_PROFILES];
   static uint8_t ucLastMotionResolution[NUM_MOTION_PROFILES];
   static uint8_t ucLastLevelingDist[NUM_MOTION_PROFILES];
   static uint8_t ucLastOffsetFromNTS[NUM_MOTION_PROFILES];

   static const enum en_8bit_params aeParamIndex_Decel[NUM_MOTION_PROFILES] =
   {
      enPARAM8__P1_Decel_x10,
      enPARAM8__P2_Decel_x10,
      enPARAM8__P3_Decel_x10,
      enPARAM8__P4_Decel_x10,
   };
   static const enum en_8bit_params aeParamIndex_JerkInDecel[NUM_MOTION_PROFILES] =
   {
      enPARAM8__P1_JerkOutDecel_x10,
      enPARAM8__P2_JerkOutDecel_x10,
      enPARAM8__P3_JerkOutDecel_x10,
      enPARAM8__P4_JerkOutDecel_x10,
   };
   static const enum en_8bit_params aeParamIndex_JerkOutDecel[NUM_MOTION_PROFILES] =
   {
      enPARAM8__P1_JerkInDecel_x10,
      enPARAM8__P2_JerkInDecel_x10,
      enPARAM8__P3_JerkInDecel_x10,
      enPARAM8__P4_JerkInDecel_x10,
   };
   static const enum en_8bit_params aeParamIndex_LevelingDistance[NUM_MOTION_PROFILES] =
   {
      enPARAM8__P1_LevelingDistance_5mm,
      enPARAM8__P2_LevelingDistance_5mm,
      enPARAM8__P3_LevelingDistance_5mm,
      enPARAM8__P4_LevelingDistance_5mm,
   };

   //----------------------------------------
   if( eProfile < NUM_MOTION_PROFILES )
   {
      uint16_t uwContractSpeed = Param_ReadValue_16Bit(enPARAM16__ContractSpeed);

      if( eProfile == MOTION_PROFILE__3 )
      {
         uwContractSpeed = Param_ReadValue_16Bit(enPARAM16__EPowerSpeed_fpm);
      }

      if(uwContractSpeed > MAX_CAR_SPEED)
      {
         uwContractSpeed = MAX_CAR_SPEED;
      }
      else if(uwContractSpeed < MIN_CONTRACT_SPEED)
      {
         uwContractSpeed = MIN_CONTRACT_SPEED;
      }
      if(uwLastContractSpeed[eProfile] != uwContractSpeed)
      {
         uwLastContractSpeed[eProfile] = uwContractSpeed;
         bReturn = 1;
         if( eProfile != MOTION_PROFILE__3 )
         {
            stMotionCtrl.flContractSpeed_FPS = uwContractSpeed;
            stMotionCtrl.flContractSpeed_FPS /= 60.0f;
            stMotionCtrl.uwContractSpeed_FPM = uwContractSpeed;
         }
      }
      //----------------------------------------
      uint16_t uwLevelingSpeed = Param_ReadValue_16Bit(enPARAM16__LevelingSpeed);
      if(!Param_ReadValue_8Bit( aeParamIndex_LevelingDistance[eProfile] ) )
      {
         uwLevelingSpeed = 0;
      }
      if(uwLastLevelingSpeed[eProfile] != uwLevelingSpeed)
      {
         uwLastLevelingSpeed[eProfile] = uwLevelingSpeed;
         bReturn = 1;
         stCurveParameters[eProfile].uwLevelingSpeed_FPM = uwLevelingSpeed;
      }
      //----------------------------------------
      uint8_t ucDecel = Param_ReadValue_8Bit(aeParamIndex_Decel[eProfile]);
      if( ucDecel > MAX_ACCEL_X10 )
      {
         ucDecel = MAX_ACCEL_X10;
      }
      else if( ucDecel < MIN_ACCEL_X10 )
      {
         ucDecel = MIN_ACCEL_X10;
      }
      if(ucLastDecel[eProfile] != ucDecel)
      {
         ucLastDecel[eProfile] = ucDecel;
         bReturn = 1;
         stCurveParameters[eProfile].fDecel = ucDecel;
         stCurveParameters[eProfile].fDecel /= 10.0f;
      }

      //----------------------------------------
      uint8_t ucJerkInDecel = Param_ReadValue_8Bit(aeParamIndex_JerkInDecel[eProfile]);
      if( ucJerkInDecel > MAX_JERK_X10 )
      {
         ucJerkInDecel = MAX_JERK_X10;
      }
      else if( ucJerkInDecel < MIN_JERK_X10 )
      {
         ucJerkInDecel = MIN_JERK_X10;
      }
      if(ucLastJerkInDecel[eProfile] != ucJerkInDecel)
      {
         ucLastJerkInDecel[eProfile] = ucJerkInDecel;
         bReturn = 1;
         stCurveParameters[eProfile].fJerkInDecel = ucJerkInDecel;
         stCurveParameters[eProfile].fJerkInDecel /= 10.0f;
      }
      //----------------------------------------
      uint8_t ucJerkOutDecel = Param_ReadValue_8Bit(aeParamIndex_JerkOutDecel[eProfile]);
      if( ucJerkOutDecel > MAX_JERK_X10 )
      {
         ucJerkOutDecel = MAX_JERK_X10;
      }
      else if( ucJerkOutDecel < MIN_JERK_X10 )
      {
         ucJerkOutDecel = MIN_JERK_X10;
      }
      if(ucLastJerkOutDecel[eProfile] != ucJerkOutDecel)
      {
         ucLastJerkOutDecel[eProfile] = ucJerkOutDecel;
         bReturn = 1;
         stCurveParameters[eProfile].fJerkOutDecel = ucJerkOutDecel;
         stCurveParameters[eProfile].fJerkOutDecel /= 10.0f;
      }
      //----------------------------------------
      uint8_t ucMotionResolution = Param_ReadValue_8Bit(enPARAM8__MotionRes_1ms);
      if(ucMotionResolution < MIN_PATTERN_RESOLUTION__MS)
      {
         ucMotionResolution = MIN_PATTERN_RESOLUTION__MS;
      }
      else if(ucMotionResolution > MAX_PATTERN_RESOLUTION__MS)
      {
         ucMotionResolution = MAX_PATTERN_RESOLUTION__MS;
      }
      if(ucLastMotionResolution[eProfile] != ucMotionResolution)
      {
         ucLastMotionResolution[eProfile] = ucMotionResolution;
         bReturn = 1;
         stMotionCtrl.flPatternTimeRes_sec = ucMotionResolution;
         stMotionCtrl.flPatternTimeRes_sec /= 1000.0f;
      }
      //----------------------------------------
      uint8_t ucLevelingDist = Param_ReadValue_8Bit( aeParamIndex_LevelingDistance[eProfile] );
      if( ucLevelingDist > MAX_LEVELING_DISTANCE_5MM )
      {
         ucLevelingDist = MAX_LEVELING_DISTANCE_5MM;
      }
      if(ucLastLevelingDist[eProfile] != ucLevelingDist)
      {
         ucLastLevelingDist[eProfile] = ucLevelingDist;
         bReturn = 1;
         stCurveParameters[eProfile].uwLevelingDistance = ucLevelingDist * 10.0f;
      }
      //----------------------------------------
      uint8_t ucOffsetFromNTS = Param_ReadValue_8Bit( enPARAM8__ETS_OffsetFromNTS_5mm );
      if( ucLastOffsetFromNTS[eProfile] != ucOffsetFromNTS )
      {
         ucLastOffsetFromNTS[eProfile] = ucOffsetFromNTS;
         bReturn = 1;
      }
   }
   return bReturn;
}
/*--------------------------------------------------------------------------
   Recalculate curve limits based on new parameters
--------------------------------------------------------------------------*/
void Decel_UpdateCurveLimits( Motion_Profile eProfile )
{
   float flTemp = 0;
   gpstDecelCurveParameters = stCurveParameters;
   gpstDecelMotionCtrl = &stMotionCtrl;

   //----------------------------------------
   /* DECEL */
   flTemp = 0;
   stCurveParameters[ eProfile ].fShortPatternMinDecelSpeed_FPS = stCurveParameters[ eProfile ].fDecel*stCurveParameters[ eProfile ].fDecel*(1.0f/stCurveParameters[ eProfile ].fJerkInDecel + 1.0f/stCurveParameters[ eProfile ].fJerkOutDecel)/2.0f;

   float fContractSpd_fps = stMotionCtrl.flContractSpeed_FPS;
   if( eProfile == MOTION_PROFILE__3 )
   {
      fContractSpd_fps = Param_ReadValue_16Bit(enPARAM16__EPowerSpeed_fpm)/60.0f;
   }

   if ( stCurveParameters[ eProfile ].fShortPatternMinDecelSpeed_FPS >= fContractSpd_fps )
   {
#ifdef ARM_MATH
      arm_sqrt_f32( 2.0f*(stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel)*fContractSpd_fps/(stCurveParameters[ eProfile ].fJerkInDecel*stCurveParameters[ eProfile ].fJerkOutDecel), &flTemp );
#else
      flTemp = sqrtf( 2.0f*(stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel)*fContractSpd_fps/(stCurveParameters[ eProfile ].fJerkInDecel*stCurveParameters[ eProfile ].fJerkOutDecel) );
#endif

      stCurveParameters[ eProfile ].uiMaxDecelTime = (uint32_t)( flTemp/stMotionCtrl.flPatternTimeRes_sec + 0.5f );

      stCurveParameters[ eProfile ].fFullPatternMinDecelDistance_FT = 0xFFFFFFFF; // All Patterns will be Very Short
      stCurveParameters[ eProfile ].fShortPatternMinDecelDistance_FT = 0xFFFFFFFF; // All Patterns will be Very Short
      stCurveParameters[ eProfile ].uiShortPatternMinDecelDistance = 0xFFFFFFFF; // All Patterns will be Very Short
   }
   else
   {
      flTemp = (fContractSpd_fps/stCurveParameters[ eProfile ].fDecel + stCurveParameters[ eProfile ].fDecel*(1.0f/stCurveParameters[ eProfile ].fJerkInDecel + 1.0f/stCurveParameters[ eProfile ].fJerkOutDecel)/2.0f);
      stCurveParameters[ eProfile ].uiMaxDecelTime = (uint32_t)( flTemp/stMotionCtrl.flPatternTimeRes_sec + 0.5f );

      float flTempD1 = powf(stCurveParameters[ eProfile ].fDecel,4.0f)*(powf(stCurveParameters[ eProfile ].fJerkOutDecel,2.0f) - powf(stCurveParameters[ eProfile ].fJerkInDecel,2.0f));
      float flTempD2 = 12.0f*powf(stCurveParameters[ eProfile ].fDecel,2.0f)*powf(stCurveParameters[ eProfile ].fJerkInDecel,2.0f)*stCurveParameters[ eProfile ].fJerkOutDecel;
      float flTempD3 = 12.0f*powf(stCurveParameters[ eProfile ].fJerkOutDecel,2.0f)*powf(stCurveParameters[ eProfile ].fJerkInDecel,2.0f);
      float flTempD4 = 24.0f*stCurveParameters[ eProfile ].fDecel*powf(stCurveParameters[ eProfile ].fJerkOutDecel,2.0f)*powf(stCurveParameters[ eProfile ].fJerkInDecel,2.0f);

      stCurveParameters[ eProfile ].fFullPatternMinDecelDistance_FT = (flTempD1 + flTempD2*fContractSpd_fps + flTempD3*powf(fContractSpd_fps,2))/flTempD4;
      stCurveParameters[ eProfile ].fShortPatternMinDecelDistance_FT = (flTempD1 + flTempD2*stCurveParameters[ eProfile ].fShortPatternMinDecelSpeed_FPS + flTempD3*powf(stCurveParameters[ eProfile ].fShortPatternMinDecelSpeed_FPS,2))/flTempD4;
      stCurveParameters[ eProfile ].uiShortPatternMinDecelDistance = stCurveParameters[ eProfile ].fShortPatternMinDecelDistance_FT * HALF_MM_PER_FT + 0.5f;
   }
}

/*--------------------------------------------------------------------------

   This function updates gauwPattern_Speed[time] and gauiPattern_Position[time]
   with new values based on the desired speed to be achieved.
   One of three different pattern profiles is used depending on the input speed

--------------------------------------------------------------------------*/
void Decel_GenerateDecelRun( float flMaxSpeed_FPS )
{
   float flPosition, flTime_sec;
   uint32_t uiTime_10ms;
   float fLevelingSpeed = 0;
   Motion_Profile eProfile = GetMotion_Profile();
   if(stCurveParameters[ eProfile ].uwLevelingDistance)
   {
      fLevelingSpeed = stCurveParameters[ eProfile ].uwLevelingSpeed_FPM / 60.0f;
   }
   float fSpeedChange = flMaxSpeed_FPS - fLevelingSpeed;
   if ( fSpeedChange >= stCurveParameters[ eProfile ].fShortPatternMinDecelSpeed_FPS )
   {
      float flTa = stCurveParameters[ eProfile ].fDecel/stCurveParameters[ eProfile ].fJerkInDecel;
      float flTb = fSpeedChange/stCurveParameters[ eProfile ].fDecel
                 + stCurveParameters[ eProfile ].fDecel*(1.0f/stCurveParameters[ eProfile ].fJerkInDecel - 1.0f/stCurveParameters[ eProfile ].fJerkOutDecel)/2.0f;
      float flTc = fSpeedChange/stCurveParameters[ eProfile ].fDecel
                 + stCurveParameters[ eProfile ].fDecel*(1.0f/stCurveParameters[ eProfile ].fJerkInDecel + 1.0f/stCurveParameters[ eProfile ].fJerkOutDecel)/2.0f;

      float flVi = fLevelingSpeed;
      float flVa = stCurveParameters[ eProfile ].fJerkInDecel * powf(flTa, 2.0f)/2.0f
                 + flVi;
      float flVb = stCurveParameters[ eProfile ].fDecel*(flTb - flTa)
                 + flVa;
      float flVc = fSpeedChange
                 + flVi;

      float flXa = stCurveParameters[ eProfile ].fJerkInDecel * powf(flTa, 3.0f)/6.0f
                 + flVi * flTa;
      float flXb = stCurveParameters[ eProfile ].fDecel * powf( flTb - flTa, 2.0f )/2.0f
                 + flVa * (flTb - flTa)
                 + flXa;

      stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel = ( uint32_t )( flTa/stMotionCtrl.flPatternTimeRes_sec + 0.5f );
      stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel = ( uint32_t )( flTb/stMotionCtrl.flPatternTimeRes_sec  + 0.5f );
      stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel = ( uint32_t )( flTc/stMotionCtrl.flPatternTimeRes_sec + 0.5f );

      /* Bound pattern table */
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel = MAX_PATTERN_SIZE_BYTES;
      }
      stMotionCtrl.stRunParameters.uiPatternDecelTime = stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel;

      for( uiTime_10ms = 0; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fJerkInDecel*powf(flTime_sec, 3.0f)/6.0f
                    + flVi * flTime_sec;

         stMotionCtrl.auiDecelPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      for( uiTime_10ms = stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fDecel*powf((flTime_sec - flTa), 2.0f)/2.0f
                    + flVa*(flTime_sec - flTa)
                    + flXa;

         stMotionCtrl.auiDecelPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      for( uiTime_10ms = stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fDecel * powf( flTime_sec - flTb, 2.0f )/2.0f
                    - stCurveParameters[ eProfile ].fJerkOutDecel * powf( flTime_sec - flTb, 3.0f)/6.0f
                    + flVb * ( flTime_sec - flTb )
                    + flXb;

         stMotionCtrl.auiDecelPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      if ( flMaxSpeed_FPS >= stMotionCtrl.flContractSpeed_FPS )
      {
         stMotionCtrl.stDecelVars.ePatternProfile = PATTERN__FULL;
      }
      else
      {
         stMotionCtrl.stDecelVars.ePatternProfile = PATTERN__SHORT;
      }

      stMotionCtrl.stDecelVars.fEndTime_JerkInDecel = flTa;
      stMotionCtrl.stDecelVars.fEndTime_ConstantDecel = flTb;
      stMotionCtrl.stDecelVars.fEndTime_JerkOutDecel = flTc;
      stMotionCtrl.stDecelVars.fStartSpeed_JerkInDecel = flVi;
      stMotionCtrl.stDecelVars.fEndSpeed_JerkInDecel = flVa;
      stMotionCtrl.stDecelVars.fEndSpeed_ConstantDecel = flVb;
      stMotionCtrl.stDecelVars.fEndSpeed_JerkOutDecel = flVc;

      stMotionCtrl.stDecelVars.fMaxDecel = stCurveParameters[ eProfile ].fDecel;

      stMotionCtrl.stRunParameters.uiSlowdownDistance = stMotionCtrl.auiDecelPattern_Position[ stMotionCtrl.stRunParameters.uiPatternDecelTime - 1 ]
                                                      + stCurveParameters[ eProfile ].uwLevelingDistance;
   }
   else /* Use very short pattern */
   {
      float flVc = flMaxSpeed_FPS;
      float flTc = flMaxSpeed_FPS;
#ifdef ARM_MATH
      arm_sqrt_f32( 2.0f*(stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel)*fSpeedChange/(stCurveParameters[ eProfile ].fJerkInDecel*stCurveParameters[ eProfile ].fJerkOutDecel), &flTc );
#else
      flTc = sqrtf( 2.0f*(stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel)*flVc/(stCurveParameters[ eProfile ].fJerkInDecel*stCurveParameters[ eProfile ].fJerkOutDecel) );
#endif

      float flTa = flTc*stCurveParameters[ eProfile ].fJerkOutDecel
                 / (stCurveParameters[ eProfile ].fJerkInDecel + stCurveParameters[ eProfile ].fJerkOutDecel);

      float flAa = flTa*stCurveParameters[ eProfile ].fJerkInDecel;
      float flVi = fLevelingSpeed;
      float flVa = stCurveParameters[ eProfile ].fJerkInDecel * powf(flTa, 2.0f)/2.0f
                 + flVi;

      float flXa = flVi*flTa + stCurveParameters[ eProfile ].fJerkInDecel * powf(flTa, 3.0f)/6.0f;

      stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel = ( uint32_t )( flTa/stMotionCtrl.flPatternTimeRes_sec + 0.5f );
      stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel = stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel;
      stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel = ( uint32_t )( flTc/stMotionCtrl.flPatternTimeRes_sec + 0.5f );

      /* Bound pattern table */
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel = MAX_PATTERN_SIZE_BYTES;
      }
      if( stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel > MAX_PATTERN_SIZE_BYTES )
      {
         stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel = MAX_PATTERN_SIZE_BYTES;
      }
      stMotionCtrl.stRunParameters.uiPatternDecelTime = stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel;

      for( uiTime_10ms = 0; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = stCurveParameters[ eProfile ].fJerkInDecel*powf(flTime_sec, 3.0f)/6.0f
                    + flVi*flTime_sec;

         stMotionCtrl.auiDecelPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }

      for( uiTime_10ms = stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel; uiTime_10ms < stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel; uiTime_10ms++ )
      {
         flTime_sec = uiTime_10ms * stMotionCtrl.flPatternTimeRes_sec;

         flPosition = flAa * powf( flTime_sec - flTa, 2.0f )/2.0f
                    - stCurveParameters[ eProfile ].fJerkOutDecel * powf( flTime_sec - flTa, 3.0f)/6.0f
                    + flVa * ( flTime_sec - flTa )
                    + flXa;

         stMotionCtrl.auiDecelPattern_Position[ uiTime_10ms ] = ( uint32_t )( HALF_MM_PER_FT*flPosition + 0.5f );
      }
      stMotionCtrl.stDecelVars.fEndTime_JerkInDecel = flTa;
      stMotionCtrl.stDecelVars.fEndTime_JerkOutDecel = flTc;

      stMotionCtrl.stDecelVars.fStartSpeed_JerkInDecel = flVi;
      stMotionCtrl.stDecelVars.fEndSpeed_JerkInDecel = flVa;
      stMotionCtrl.stDecelVars.fEndSpeed_ConstantDecel = flVa;
      stMotionCtrl.stDecelVars.fEndSpeed_JerkOutDecel = flVc;

      stMotionCtrl.stDecelVars.fMaxDecel = flAa;

      stMotionCtrl.stDecelVars.ePatternProfile = PATTERN__VERY_SHORT;
      stMotionCtrl.stRunParameters.uiSlowdownDistance = stMotionCtrl.auiDecelPattern_Position[ stMotionCtrl.stRunParameters.uiPatternDecelTime - 1 ]
                                                      + stCurveParameters[ eProfile ].uwLevelingDistance;
   }
}
/*--------------------------------------------------------------------------

--------------------------------------------------------------------------*/
inline uint16_t Decel_GetDecelCommandSpeed(uint32_t uiTimeIndex)
{
   uint16_t uwCommandSpeed = 0;
   float flSpeed;
   float flTime_sec = uiTimeIndex * stMotionCtrl.flPatternTimeRes_sec;

   if( stMotionCtrl.stDecelVars.ePatternProfile != PATTERN__VERY_SHORT )
   {
      if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel )
      {
         flSpeed = stMotionCtrl.stDecelVars.fStartSpeed_JerkInDecel + stCurveParameters[ GetMotion_Profile() ].fJerkInDecel*powf(flTime_sec, 2.0f)/2.0f;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_ConstantDecel )
      {
         flSpeed = stMotionCtrl.stDecelVars.fEndSpeed_JerkInDecel
                 + stCurveParameters[ GetMotion_Profile() ].fDecel*(flTime_sec - stMotionCtrl.stDecelVars.fEndTime_JerkInDecel);
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel )
      {
         flSpeed = stMotionCtrl.stDecelVars.fEndSpeed_JerkOutDecel - stCurveParameters[ GetMotion_Profile() ].fJerkOutDecel*powf(stMotionCtrl.stDecelVars.fEndTime_JerkOutDecel - flTime_sec, 2.0f)/2.0f;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
   }
   else
   {
      if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkInDecel )
      {
         flSpeed = stMotionCtrl.stDecelVars.fStartSpeed_JerkInDecel
                 + stCurveParameters[ GetMotion_Profile() ].fJerkInDecel*powf(flTime_sec, 2.0f)/2.0f;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
      else if( uiTimeIndex < stMotionCtrl.stRunParameters.uiEndTime_JerkOutDecel )
      {
         flSpeed = -stCurveParameters[ GetMotion_Profile() ].fJerkOutDecel*powf(stMotionCtrl.stDecelVars.fEndTime_JerkInDecel - flTime_sec, 2.0f)/2.0f
                   - stMotionCtrl.stDecelVars.fMaxDecel*(stMotionCtrl.stDecelVars.fEndTime_JerkInDecel - flTime_sec)
                   + stMotionCtrl.stDecelVars.fEndSpeed_JerkInDecel;
         uwCommandSpeed = ( uint16_t )( 60.0f*flSpeed  + 0.5f );
      }
   }

   /* Bound speed */
   if( uwCommandSpeed > stMotionCtrl.uwContractSpeed_FPM )
   {
      uwCommandSpeed = stMotionCtrl.uwContractSpeed_FPM;
   }
   else if(!uwCommandSpeed)
   {
      uwCommandSpeed = 1;
   }

   return uwCommandSpeed;
}

/*--------------------------------------------------------------------------
   Takes current time index into run's pattern array
   and returns the position stored in the lookup table at that time index
--------------------------------------------------------------------------*/
inline uint32_t Decel_GetDecelPosition(uint32_t uiTimeIndex)
{
   return stMotionCtrl.auiDecelPattern_Position[uiTimeIndex];
}
/*----------------------------------------------------------------------------
   Returns 1 if s curve is invalid
 *----------------------------------------------------------------------------*/
uint8_t Decel_CheckForInvalidSCurve(Motion_Profile eProfile)
{
   uint8_t bInvalid  = (stCurveParameters[eProfile].uiMaxDecelTime >= MAX_PATTERN_SIZE_BYTES) ? 1:0;
   return bInvalid;
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
