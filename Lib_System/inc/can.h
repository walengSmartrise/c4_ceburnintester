/*
 * can.h
 *
 *  Created on: Sep 12, 2016
 *      Author: Keith
 */

#ifndef CAN_H_
#define CAN_H_

#include "sys.h"


CAN_BUFFER_ID_T CAN_GetFreeTxBuf_Pr1(LPC_CAN_T *pCAN);
CAN_BUFFER_ID_T CAN_GetFreeTxBuf_Pr2(LPC_CAN_T *pCAN);
Status Sys_CAN_Send(LPC_CAN_T *pCAN, CAN_BUFFER_ID_T TxBufID, CAN_MSG_T *pMsg);

#endif /* CAN_H_ */
