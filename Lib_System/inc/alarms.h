#ifndef ALARMS_H
#define ALARMS_H

/* NOTE: FILE UPDATED VIA LIB_SYSTEM - SUPPORT - ALARMS, 
 * Edit SystemAlarms.xlsx then save as tab delimited (SystemAlarms.txt)  
 * Then run Update_Alarms.bat to automatically update this file */

/* Includes */
#include "sys.h"

/* Defines */
#define ALARM_LATCH_PERIOD_1MS               ( 3000U )
#define ALARM_RESEND_RATE_1MS                ( 1000U )

// List of system alarms
typedef enum
{
   ALM__NONE,                         // AlarmNum: 0
   ALM__NTS_UP_P1_1,                  // AlarmNum: 1
   ALM__NTS_UP_P1_2,                  // AlarmNum: 2
   ALM__NTS_UP_P1_3,                  // AlarmNum: 3
   ALM__NTS_UP_P1_4,                  // AlarmNum: 4
   ALM__NTS_UP_P1_5,                  // AlarmNum: 5
   ALM__NTS_UP_P1_6,                  // AlarmNum: 6
   ALM__NTS_UP_P1_7,                  // AlarmNum: 7
   ALM__NTS_UP_P1_8,                  // AlarmNum: 8
   ALM__NTS_UP_P2_1,                  // AlarmNum: 9
   ALM__NTS_UP_P2_2,                  // AlarmNum: 10
   ALM__NTS_UP_P2_3,                  // AlarmNum: 11
   ALM__NTS_UP_P2_4,                  // AlarmNum: 12
   ALM__NTS_UP_P2_5,                  // AlarmNum: 13
   ALM__NTS_UP_P2_6,                  // AlarmNum: 14
   ALM__NTS_UP_P2_7,                  // AlarmNum: 15
   ALM__NTS_UP_P2_8,                  // AlarmNum: 16
   ALM__NTS_UP_P3_1,                  // AlarmNum: 17
   ALM__NTS_UP_P3_2,                  // AlarmNum: 18
   ALM__NTS_UP_P3_3,                  // AlarmNum: 19
   ALM__NTS_UP_P3_4,                  // AlarmNum: 20
   ALM__NTS_UP_P3_5,                  // AlarmNum: 21
   ALM__NTS_UP_P3_6,                  // AlarmNum: 22
   ALM__NTS_UP_P3_7,                  // AlarmNum: 23
   ALM__NTS_UP_P3_8,                  // AlarmNum: 24
   ALM__NTS_UP_P4_1,                  // AlarmNum: 25
   ALM__NTS_UP_P4_2,                  // AlarmNum: 26
   ALM__NTS_UP_P4_3,                  // AlarmNum: 27
   ALM__NTS_UP_P4_4,                  // AlarmNum: 28
   ALM__NTS_UP_P4_5,                  // AlarmNum: 29
   ALM__NTS_UP_P4_6,                  // AlarmNum: 30
   ALM__NTS_UP_P4_7,                  // AlarmNum: 31
   ALM__NTS_UP_P4_8,                  // AlarmNum: 32
   ALM__NTS_DN_P1_1,                  // AlarmNum: 33
   ALM__NTS_DN_P1_2,                  // AlarmNum: 34
   ALM__NTS_DN_P1_3,                  // AlarmNum: 35
   ALM__NTS_DN_P1_4,                  // AlarmNum: 36
   ALM__NTS_DN_P1_5,                  // AlarmNum: 37
   ALM__NTS_DN_P1_6,                  // AlarmNum: 38
   ALM__NTS_DN_P1_7,                  // AlarmNum: 39
   ALM__NTS_DN_P1_8,                  // AlarmNum: 40
   ALM__NTS_DN_P2_1,                  // AlarmNum: 41
   ALM__NTS_DN_P2_2,                  // AlarmNum: 42
   ALM__NTS_DN_P2_3,                  // AlarmNum: 43
   ALM__NTS_DN_P2_4,                  // AlarmNum: 44
   ALM__NTS_DN_P2_5,                  // AlarmNum: 45
   ALM__NTS_DN_P2_6,                  // AlarmNum: 46
   ALM__NTS_DN_P2_7,                  // AlarmNum: 47
   ALM__NTS_DN_P2_8,                  // AlarmNum: 48
   ALM__NTS_DN_P3_1,                  // AlarmNum: 49
   ALM__NTS_DN_P3_2,                  // AlarmNum: 50
   ALM__NTS_DN_P3_3,                  // AlarmNum: 51
   ALM__NTS_DN_P3_4,                  // AlarmNum: 52
   ALM__NTS_DN_P3_5,                  // AlarmNum: 53
   ALM__NTS_DN_P3_6,                  // AlarmNum: 54
   ALM__NTS_DN_P3_7,                  // AlarmNum: 55
   ALM__NTS_DN_P3_8,                  // AlarmNum: 56
   ALM__NTS_DN_P4_1,                  // AlarmNum: 57
   ALM__NTS_DN_P4_2,                  // AlarmNum: 58
   ALM__NTS_DN_P4_3,                  // AlarmNum: 59
   ALM__NTS_DN_P4_4,                  // AlarmNum: 60
   ALM__NTS_DN_P4_5,                  // AlarmNum: 61
   ALM__NTS_DN_P4_6,                  // AlarmNum: 62
   ALM__NTS_DN_P4_7,                  // AlarmNum: 63
   ALM__NTS_DN_P4_8,                  // AlarmNum: 64
   ALM__NTS_INVALID_P1,               // AlarmNum: 65
   ALM__NTS_INVALID_P2,               // AlarmNum: 66
   ALM__NTS_INVALID_P3,               // AlarmNum: 67
   ALM__NTS_INVALID_P4,               // AlarmNum: 68
   ALM__ESTOP_CLASS_OP,               // AlarmNum: 69
   ALM__ESTOP_STOP_TIMEOUT,           // AlarmNum: 70
   ALM__ESTOP_MOVE_TIMEOUT,           // AlarmNum: 71
   ALM__ESTOP_INVALID_INSP,           // AlarmNum: 72
   ALM__ESTOP_RECALL_DEST,            // AlarmNum: 73
   ALM__STOP_AT_NEXT_FLOOR,           // AlarmNum: 74
   ALM__ESTOP_EQ,                     // AlarmNum: 75
   ALM__ESTOP_FLOOD,                  // AlarmNum: 76
   ALM__STOP_NO_DZ,                   // AlarmNum: 77
   ALM__RELEVELING,                   // AlarmNum: 78
   ALM__DEFAULT_PARAM_1BIT,           // AlarmNum: 79
   ALM__DEFAULT_PARAM_8BIT,           // AlarmNum: 80
   ALM__DEFAULT_PARAM_16BIT,          // AlarmNum: 81
   ALM__DEFAULT_PARAM_24BIT,          // AlarmNum: 82
   ALM__DEFAULT_PARAM_32BIT,          // AlarmNum: 83
   ALM__RECALL_INVALID_DOOR,          // AlarmNum: 84
   ALM__RECALL_INVALID_FLOOR,         // AlarmNum: 85
   ALM__RECALL_INVALID_OPENING,       // AlarmNum: 86
   ALM__WDT_DISABLED_MRA,             // AlarmNum: 87
   ALM__WDT_DISABLED_MRB,             // AlarmNum: 88
   ALM__WDT_DISABLED_CTA,             // AlarmNum: 89
   ALM__WDT_DISABLED_CTB,             // AlarmNum: 90
   ALM__WDT_DISABLED_COPA,            // AlarmNum: 91
   ALM__WDT_DISABLED_COPB,            // AlarmNum: 92
   ALM__MR_CAN_RESET_1,               // AlarmNum: 93
   ALM__MR_CAN_RESET_2,               // AlarmNum: 94
   ALM__MR_CAN_RESET_3,               // AlarmNum: 95
   ALM__MR_CAN_RESET_4,               // AlarmNum: 96
   ALM__CT_CAN_RESET_1,               // AlarmNum: 97
   ALM__CT_CAN_RESET_2,               // AlarmNum: 98
   ALM__CT_CAN_RESET_3,               // AlarmNum: 99
   ALM__CT_CAN_RESET_4,               // AlarmNum: 100
   ALM__COP_CAN_RESET_1,              // AlarmNum: 101
   ALM__COP_CAN_RESET_2,              // AlarmNum: 102
   ALM__COP_CAN_RESET_3,              // AlarmNum: 103
   ALM__COP_CAN_RESET_4,              // AlarmNum: 104
   ALM__DRIVE_RESET,                  // AlarmNum: 105
   ALM__DRIVE_RESET_LIMIT,            // AlarmNum: 106
   ALM__FULLY_LOADED,                 // AlarmNum: 107
   ALM__REMOTE_PU_1BIT,               // AlarmNum: 108
   ALM__REMOTE_PU_8BIT,               // AlarmNum: 109
   ALM__REMOTE_PU_16BIT,              // AlarmNum: 110
   ALM__REMOTE_PU_24BIT,              // AlarmNum: 111
   ALM__REMOTE_PU_32BIT,              // AlarmNum: 112
   ALM__REMOTE_PU_MAG,                // AlarmNum: 113
   ALM__REMOTE_PU_KEB,                // AlarmNum: 114
   ALM__INVALID_MAN_RUN_DOOR,         // AlarmNum: 115
   ALM__INVALID_MAN_RUN_LOCK,         // AlarmNum: 116
   ALM__INVALID_MAN_RUN_ARM,          // AlarmNum: 117
   ALM__INVALID_MAN_RUN_DCBF,         // AlarmNum: 118
   ALM__INVALID_MAN_RUN_DCBR,         // AlarmNum: 119
   ALM__INVALID_MAN_RUN_DOBF,         // AlarmNum: 120
   ALM__INVALID_MAN_RUN_DOBR,         // AlarmNum: 121
   ALM__INVALID_MAN_RUN_HA,           // AlarmNum: 122
   ALM__INVALID_MAN_RUN_CT_EN,        // AlarmNum: 123
   ALM__IDLE_DIRECTION_TIMEOUT,       // AlarmNum: 124
   ALM__CPLD_OFFLINE_MR,              // AlarmNum: 125
   ALM__CPLD_OFFLINE_CT,              // AlarmNum: 126
   ALM__CPLD_OFFLINE_COP,             // AlarmNum: 127
   ALM__NO_DEST_STOP,                 // AlarmNum: 128
   ALM__FLOOD_SWITCH,                 // AlarmNum: 129
   ALM__REMOTE_PU_BACKUP,             // AlarmNum: 130
   ALM__UNUSED131,                    // AlarmNum: 131
   ALM__UNUSED132,                    // AlarmNum: 132
   ALM__UNUSED133,                    // AlarmNum: 133
   ALM__UNUSED134,                    // AlarmNum: 134
   ALM__UNUSED135,                    // AlarmNum: 135
   ALM__UNUSED136,                    // AlarmNum: 136
   ALM__UNUSED137,                    // AlarmNum: 137
   ALM__UNUSED138,                    // AlarmNum: 138
   ALM__UNUSED139,                    // AlarmNum: 139
   ALM__UNUSED140,                    // AlarmNum: 140
   ALM__UNUSED141,                    // AlarmNum: 141
   ALM__UNUSED142,                    // AlarmNum: 142
   ALM__UNUSED143,                    // AlarmNum: 143
   ALM__LWD_LOAD_LEARN,               // AlarmNum: 144
   ALM__LWD_RECALIBRATE,              // AlarmNum: 145
   ALM__MODE_CHANGE,                  // AlarmNum: 146
   ALM__RIS1_OFFLINE,                 // AlarmNum: 147
   ALM__RIS1_UNK,                     // AlarmNum: 148
   ALM__RIS1_POR_RST,                 // AlarmNum: 149
   ALM__RIS1_WDT_RST,                 // AlarmNum: 150
   ALM__RIS1_BOD_RST,                 // AlarmNum: 151
   ALM__RIS1_GRP_COM,                 // AlarmNum: 152
   ALM__RIS1_HALL_COM,                // AlarmNum: 153
   ALM__RIS1_CAR_COM,                 // AlarmNum: 154
   ALM__RIS1_MST_COM,                 // AlarmNum: 155
   ALM__RIS1_SLV_COM,                 // AlarmNum: 156
   ALM__RIS1_ADDRESS,                 // AlarmNum: 157
   ALM__RIS1_BUS_RST_1,               // AlarmNum: 158
   ALM__RIS1_BUS_RST_2,               // AlarmNum: 159
   ALM__RIS1_BUS_INV_MSG_1,           // AlarmNum: 160
   ALM__RIS1_BUS_INV_MSG_2,           // AlarmNum: 161
   ALM__RIS2_OFFLINE,                 // AlarmNum: 162
   ALM__RIS2_UNK,                     // AlarmNum: 163
   ALM__RIS2_POR_RST,                 // AlarmNum: 164
   ALM__RIS2_WDT_RST,                 // AlarmNum: 165
   ALM__RIS2_BOD_RST,                 // AlarmNum: 166
   ALM__RIS2_GRP_COM,                 // AlarmNum: 167
   ALM__RIS2_HALL_COM,                // AlarmNum: 168
   ALM__RIS2_CAR_COM,                 // AlarmNum: 169
   ALM__RIS2_MST_COM,                 // AlarmNum: 170
   ALM__RIS2_SLV_COM,                 // AlarmNum: 171
   ALM__RIS2_ADDRESS,                 // AlarmNum: 172
   ALM__RIS2_BUS_RST_1,               // AlarmNum: 173
   ALM__RIS2_BUS_RST_2,               // AlarmNum: 174
   ALM__RIS2_BUS_INV_MSG_1,           // AlarmNum: 175
   ALM__RIS2_BUS_INV_MSG_2,           // AlarmNum: 176
   ALM__RIS3_OFFLINE,                 // AlarmNum: 177
   ALM__RIS3_UNK,                     // AlarmNum: 178
   ALM__RIS3_POR_RST,                 // AlarmNum: 179
   ALM__RIS3_WDT_RST,                 // AlarmNum: 180
   ALM__RIS3_BOD_RST,                 // AlarmNum: 181
   ALM__RIS3_GRP_COM,                 // AlarmNum: 182
   ALM__RIS3_HALL_COM,                // AlarmNum: 183
   ALM__RIS3_CAR_COM,                 // AlarmNum: 184
   ALM__RIS3_MST_COM,                 // AlarmNum: 185
   ALM__RIS3_SLV_COM,                 // AlarmNum: 186
   ALM__RIS3_ADDRESS,                 // AlarmNum: 187
   ALM__RIS3_BUS_RST_1,               // AlarmNum: 188
   ALM__RIS3_BUS_RST_2,               // AlarmNum: 189
   ALM__RIS3_BUS_INV_MSG_1,           // AlarmNum: 190
   ALM__RIS3_BUS_INV_MSG_2,           // AlarmNum: 191
   ALM__RIS4_OFFLINE,                 // AlarmNum: 192
   ALM__RIS4_UNK,                     // AlarmNum: 193
   ALM__RIS4_POR_RST,                 // AlarmNum: 194
   ALM__RIS4_WDT_RST,                 // AlarmNum: 195
   ALM__RIS4_BOD_RST,                 // AlarmNum: 196
   ALM__RIS4_GRP_COM,                 // AlarmNum: 197
   ALM__RIS4_HALL_COM,                // AlarmNum: 198
   ALM__RIS4_CAR_COM,                 // AlarmNum: 199
   ALM__RIS4_MST_COM,                 // AlarmNum: 200
   ALM__RIS4_SLV_COM,                 // AlarmNum: 201
   ALM__RIS4_ADDRESS,                 // AlarmNum: 202
   ALM__RIS4_BUS_RST_1,               // AlarmNum: 203
   ALM__RIS4_BUS_RST_2,               // AlarmNum: 204
   ALM__RIS4_BUS_INV_MSG_1,           // AlarmNum: 205
   ALM__RIS4_BUS_INV_MSG_2,           // AlarmNum: 206
   ALM__DISPATCH_TIMEOUT_C1,          // AlarmNum: 207
   ALM__DISPATCH_TIMEOUT_C2,          // AlarmNum: 208
   ALM__DISPATCH_TIMEOUT_C3,          // AlarmNum: 209
   ALM__DISPATCH_TIMEOUT_C4,          // AlarmNum: 210
   ALM__DISPATCH_TIMEOUT_C5,          // AlarmNum: 211
   ALM__DISPATCH_TIMEOUT_C6,          // AlarmNum: 212
   ALM__DISPATCH_TIMEOUT_C7,          // AlarmNum: 213
   ALM__DISPATCH_TIMEOUT_C8,          // AlarmNum: 214
   ALM__DISPATCH_TIMEOUT_X1,          // AlarmNum: 215
   ALM__DISPATCH_TIMEOUT_X2,          // AlarmNum: 216
   ALM__DISPATCH_TIMEOUT_X3,          // AlarmNum: 217
   ALM__DISPATCH_TIMEOUT_X4,          // AlarmNum: 218
   ALM__DISPATCH_TIMEOUT_X5,          // AlarmNum: 219
   ALM__DISPATCH_TIMEOUT_X6,          // AlarmNum: 220
   ALM__DISPATCH_TIMEOUT_X7,          // AlarmNum: 221
   ALM__DISPATCH_TIMEOUT_X8,          // AlarmNum: 222
   ALM__XREG_OFFLINE_1,               // AlarmNum: 223
   ALM__XREG_OFFLINE_2,               // AlarmNum: 224
   ALM__XREG_OFFLINE_3,               // AlarmNum: 225
   ALM__XREG_OFFLINE_4,               // AlarmNum: 226
   ALM__XREG_OFFLINE_5,               // AlarmNum: 227
   ALM__XREG_OFFLINE_6,               // AlarmNum: 228
   ALM__XREG_OFFLINE_7,               // AlarmNum: 229
   ALM__XREG_OFFLINE_8,               // AlarmNum: 230
   ALM__UNUSED_231,                   // AlarmNum: 231
   ALM__RUN_TIME_FAULT,               // AlarmNum: 232
   ALM__RUN_TIME_FAULTAPP,            // AlarmNum: 233
   ALM__RUN_TIME_HEARTBEAT,           // AlarmNum: 234
   ALM__RUN_TIME_WATCHDOG,            // AlarmNum: 235
   ALM__RUN_TIME_INPUTS,              // AlarmNum: 236
   ALM__RUN_TIME_CAN,                 // AlarmNum: 237
   ALM__RUN_TIME_UART,                // AlarmNum: 238
   ALM__RUN_TIME_SDATA,               // AlarmNum: 239
   ALM__RUN_TIME_PARAM_EEPROM,        // AlarmNum: 240
   ALM__RUN_TIME_PARAM_APP,           // AlarmNum: 241
   ALM__RUN_TIME_PARAM_MASTER,        // AlarmNum: 242
   ALM__RUN_TIME_PARAM_SLAVE,         // AlarmNum: 243
   ALM__RUN_TIME_MODE_OPER,           // AlarmNum: 244
   ALM__RUN_TIME_OPER_AUTO,           // AlarmNum: 245
   ALM__RUN_TIME_OPER_MANUAL,         // AlarmNum: 246
   ALM__RUN_TIME_OPER_LEARN,          // AlarmNum: 247
   ALM__RUN_TIME_FIRE,                // AlarmNum: 248
   ALM__RUN_TIME_FLOOD,               // AlarmNum: 249
   ALM__RUN_TIME_SABBATH,             // AlarmNum: 250
   ALM__RUN_TIME_CARCALLS,            // AlarmNum: 251
   ALM__RUN_TIME_DISPATCH,            // AlarmNum: 252
   ALM__RUN_TIME_EARTHQUAKE,          // AlarmNum: 253
   ALM__RUN_TIME_CAPTURE,             // AlarmNum: 254
   ALM__RUN_TIME_CARTOLOBBY,          // AlarmNum: 255
   ALM__RUN_TIME_MAXRUNTIME,          // AlarmNum: 256
   ALM__RUN_TIME_RELEVELING,          // AlarmNum: 257
   ALM__RUN_TIME_DOORS,               // AlarmNum: 258
   ALM__RUN_TIME_INPUTMAPPING,        // AlarmNum: 259
   ALM__RUN_TIME_OUTPUTMAPPING,       // AlarmNum: 260
   ALM__RUN_TIME_RTC,                 // AlarmNum: 261
   ALM__RUN_TIME_ALARMAPP,            // AlarmNum: 262
   ALM__RUN_TIME_TEMPTEST,            // AlarmNum: 263
   ALM__RUN_TIME_POS,                 // AlarmNum: 264
   ALM__RUN_TIME_MOTION,              // AlarmNum: 265
   ALM__RUN_TIME_BRAKE,               // AlarmNum: 266
   ALM__RUN_TIME_DRIVE,               // AlarmNum: 267
   ALM__RUN_TIME_SAFETY,              // AlarmNum: 268
   ALM__RUN_TIME_FRAM,                // AlarmNum: 269
   ALM__RUN_TIME_RUNTIMER,            // AlarmNum: 270
   ALM__RUN_TIME_SDATA_CTRL,          // AlarmNum: 271
   ALM__RUN_TIME_FPGA,                // AlarmNum: 272
   ALM__RUN_TIME_ACCEPTANCE,          // AlarmNum: 273
   ALM__RUN_TIME_RUNLOG,              // AlarmNum: 274
   ALM__RUN_TIME_ETS,                 // AlarmNum: 275
   ALM__RUN_TIME_CARCALLSECURE,       // AlarmNum: 276
   ALM__RUN_TIME_EMS,                 // AlarmNum: 277
   ALM__RUN_TIME_RESCUE,              // AlarmNum: 278
   ALM__MRA_MOD_45,                   // AlarmNum: 279
   ALM__MRA_MOD_46,                   // AlarmNum: 280
   ALM__MRA_MOD_47,                   // AlarmNum: 281
   ALM__MRA_MOD_48,                   // AlarmNum: 282
   ALM__MRA_MOD_49,                   // AlarmNum: 283
   ALM__MRA_MOD_50,                   // AlarmNum: 284
   ALM__MRA_MOD_51,                   // AlarmNum: 285
   ALM__MRA_MOD_52,                   // AlarmNum: 286
   ALM__MRA_MOD_53,                   // AlarmNum: 287
   ALM__MRA_MOD_54,                   // AlarmNum: 288
   ALM__MRA_MOD_55,                   // AlarmNum: 289
   ALM__MRA_MOD_56,                   // AlarmNum: 290
   ALM__MRA_MOD_57,                   // AlarmNum: 291
   ALM__MRA_MOD_58,                   // AlarmNum: 292
   ALM__MRA_MOD_59,                   // AlarmNum: 293
   ALM__MRA_MOD_60,                   // AlarmNum: 294
   ALM__MRA_MOD_61,                   // AlarmNum: 295
   ALM__MRA_MOD_62,                   // AlarmNum: 296
   ALM__MRA_MOD_63,                   // AlarmNum: 297
   ALM__MRA_MOD_64,                   // AlarmNum: 298
   ALM__RUN_TIME_FAULT_MRB,           // AlarmNum: 299
   ALM__RUN_TIME_FAULTAPP_MRB,        // AlarmNum: 300
   ALM__RUN_TIME_HEARTBEAT_MRB,       // AlarmNum: 301
   ALM__RUN_TIME_WATCHDOG_MRB,        // AlarmNum: 302
   ALM__RUN_TIME_CAN_MRB,             // AlarmNum: 303
   ALM__RUN_TIME_UART_MRB,            // AlarmNum: 304
   ALM__RUN_TIME_PARAM_EEPROM_MRB,    // AlarmNum: 305
   ALM__RUN_TIME_PARMAPP_MRB,         // AlarmNum: 306
   ALM__RUN_TIME_PARAM_MRB,           // AlarmNum: 307
   ALM__RUN_TIME_SDATA_MRB,           // AlarmNum: 308
   ALM__RUN_TIME_GROUPNET,            // AlarmNum: 309
   ALM__RUN_TIME_AUXNET_MRB,          // AlarmNum: 310
   ALM__RUN_TIME_LCD,                 // AlarmNum: 311
   ALM__RUN_TIME_BUTTONS,             // AlarmNum: 312
   ALM__RUN_TIME_UI,                  // AlarmNum: 313
   ALM__RUN_TIME_CE,                  // AlarmNum: 314
   ALM__RUN_TIME_RTC_MRB,             // AlarmNum: 315
   ALM__RUN_TIME_ALARMAPP_MRB,        // AlarmNum: 316
   ALM__RUN_TIME_OUTPUTS_MRB,         // AlarmNum: 317
   ALM__RUN_TIME_LW,                  // AlarmNum: 318
   ALM__RUN_TIME_RUNLOG_MRB,          // AlarmNum: 319
   ALM__RUN_TIME_DISPATCHING,         // AlarmNum: 320
   ALM__RUN_TIME_EPOWER_MASTER,       // AlarmNum: 321
   ALM__RUN_TIME_LANTERNMONITOR,      // AlarmNum: 322
   ALM__RUN_TIME_XREG,                // AlarmNum: 323
   ALM__MRB_MOD_26,                   // AlarmNum: 324
   ALM__MRB_MOD_27,                   // AlarmNum: 325
   ALM__MRB_MOD_28,                   // AlarmNum: 326
   ALM__MRB_MOD_29,                   // AlarmNum: 327
   ALM__MRB_MOD_30,                   // AlarmNum: 328
   ALM__MRB_MOD_31,                   // AlarmNum: 329
   ALM__MRB_MOD_32,                   // AlarmNum: 330
   ALM__MRB_MOD_33,                   // AlarmNum: 331
   ALM__MRB_MOD_34,                   // AlarmNum: 332
   ALM__MRB_MOD_35,                   // AlarmNum: 333
   ALM__MRB_MOD_36,                   // AlarmNum: 334
   ALM__MRB_MOD_37,                   // AlarmNum: 335
   ALM__MRB_MOD_38,                   // AlarmNum: 336
   ALM__MRB_MOD_39,                   // AlarmNum: 337
   ALM__MRB_MOD_40,                   // AlarmNum: 338
   ALM__MRB_MOD_41,                   // AlarmNum: 339
   ALM__MRB_MOD_42,                   // AlarmNum: 340
   ALM__MRB_MOD_43,                   // AlarmNum: 341
   ALM__MRB_MOD_44,                   // AlarmNum: 342
   ALM__MRB_MOD_45,                   // AlarmNum: 343
   ALM__MRB_MOD_46,                   // AlarmNum: 344
   ALM__MRB_MOD_47,                   // AlarmNum: 345
   ALM__MRB_MOD_48,                   // AlarmNum: 346
   ALM__MRB_MOD_49,                   // AlarmNum: 347
   ALM__MRB_MOD_50,                   // AlarmNum: 348
   ALM__MRB_MOD_51,                   // AlarmNum: 349
   ALM__MRB_MOD_52,                   // AlarmNum: 350
   ALM__MRB_MOD_53,                   // AlarmNum: 351
   ALM__MRB_MOD_54,                   // AlarmNum: 352
   ALM__MRB_MOD_55,                   // AlarmNum: 353
   ALM__MRB_MOD_56,                   // AlarmNum: 354
   ALM__MRB_MOD_57,                   // AlarmNum: 355
   ALM__MRB_MOD_58,                   // AlarmNum: 356
   ALM__MRB_MOD_59,                   // AlarmNum: 357
   ALM__MRB_MOD_60,                   // AlarmNum: 358
   ALM__MRB_MOD_61,                   // AlarmNum: 359
   ALM__MRB_MOD_62,                   // AlarmNum: 360
   ALM__MRB_MOD_63,                   // AlarmNum: 361
   ALM__MRB_MOD_64,                   // AlarmNum: 362
   ALM__RUN_TIME_FAULT_CTA,           // AlarmNum: 363
   ALM__RUN_TIME_FAULTAPP_CTA,        // AlarmNum: 364
   ALM__RUN_TIME_HEARTBEAT_CTA,       // AlarmNum: 365
   ALM__RUN_TIME_WD_CTA,              // AlarmNum: 366
   ALM__RUN_TIME_INPUTS_CTA,          // AlarmNum: 367
   ALM__RUN_TIME_CAN_CTA,             // AlarmNum: 368
   ALM__RUN_TIME_UART_CTA,            // AlarmNum: 369
   ALM__RUN_TIME_SDATA_CTA,           // AlarmNum: 370
   ALM__RUN_TIME_PARAMEEPROM_CTA,     // AlarmNum: 371
   ALM__RUN_TIME_PARAMAPP_CTA,        // AlarmNum: 372
   ALM__RUN_TIME_PARAM_CTA,           // AlarmNum: 373
   ALM__RUN_TIME_PARAM_MASTER_CTA,    // AlarmNum: 374
   ALM__RUN_TIME_FPGA_CTA,            // AlarmNum: 375
   ALM__RUN_TIME_RTC_CTA,             // AlarmNum: 376
   ALM__RUN_TIME_ALARM_APP_CTA,       // AlarmNum: 377
   ALM__RUN_TIME_POS_CTA,             // AlarmNum: 378
   ALM__RUN_TIME_SAFETY_CTA,          // AlarmNum: 379
   ALM__CTA_MOD_17,                   // AlarmNum: 380
   ALM__CTA_MOD_18,                   // AlarmNum: 381
   ALM__CTA_MOD_19,                   // AlarmNum: 382
   ALM__CTA_MOD_20,                   // AlarmNum: 383
   ALM__CTA_MOD_21,                   // AlarmNum: 384
   ALM__CTA_MOD_22,                   // AlarmNum: 385
   ALM__CTA_MOD_23,                   // AlarmNum: 386
   ALM__CTA_MOD_24,                   // AlarmNum: 387
   ALM__CTA_MOD_25,                   // AlarmNum: 388
   ALM__CTA_MOD_26,                   // AlarmNum: 389
   ALM__CTA_MOD_27,                   // AlarmNum: 390
   ALM__CTA_MOD_28,                   // AlarmNum: 391
   ALM__CTA_MOD_29,                   // AlarmNum: 392
   ALM__CTA_MOD_30,                   // AlarmNum: 393
   ALM__CTA_MOD_31,                   // AlarmNum: 394
   ALM__CTA_MOD_32,                   // AlarmNum: 395
   ALM__CTA_MOD_33,                   // AlarmNum: 396
   ALM__CTA_MOD_34,                   // AlarmNum: 397
   ALM__CTA_MOD_35,                   // AlarmNum: 398
   ALM__CTA_MOD_36,                   // AlarmNum: 399
   ALM__CTA_MOD_37,                   // AlarmNum: 400
   ALM__CTA_MOD_38,                   // AlarmNum: 401
   ALM__CTA_MOD_39,                   // AlarmNum: 402
   ALM__CTA_MOD_40,                   // AlarmNum: 403
   ALM__CTA_MOD_41,                   // AlarmNum: 404
   ALM__CTA_MOD_42,                   // AlarmNum: 405
   ALM__CTA_MOD_43,                   // AlarmNum: 406
   ALM__CTA_MOD_44,                   // AlarmNum: 407
   ALM__CTA_MOD_45,                   // AlarmNum: 408
   ALM__CTA_MOD_46,                   // AlarmNum: 409
   ALM__CTA_MOD_47,                   // AlarmNum: 410
   ALM__CTA_MOD_48,                   // AlarmNum: 411
   ALM__CTA_MOD_49,                   // AlarmNum: 412
   ALM__CTA_MOD_50,                   // AlarmNum: 413
   ALM__CTA_MOD_51,                   // AlarmNum: 414
   ALM__CTA_MOD_52,                   // AlarmNum: 415
   ALM__CTA_MOD_53,                   // AlarmNum: 416
   ALM__CTA_MOD_54,                   // AlarmNum: 417
   ALM__CTA_MOD_55,                   // AlarmNum: 418
   ALM__CTA_MOD_56,                   // AlarmNum: 419
   ALM__CTA_MOD_57,                   // AlarmNum: 420
   ALM__CTA_MOD_58,                   // AlarmNum: 421
   ALM__CTA_MOD_59,                   // AlarmNum: 422
   ALM__CTA_MOD_60,                   // AlarmNum: 423
   ALM__CTA_MOD_61,                   // AlarmNum: 424
   ALM__CTA_MOD_62,                   // AlarmNum: 425
   ALM__CTA_MOD_63,                   // AlarmNum: 426
   ALM__CTA_MOD_64,                   // AlarmNum: 427
   ALM__RUN_TIME_FAULT_CTB,           // AlarmNum: 428
   ALM__RUN_TIME_FAULTAPP_CTB,        // AlarmNum: 429
   ALM__RUN_TIME_HEARTBEAT_CTB,       // AlarmNum: 430
   ALM__RUN_TIME_WD_CTB,              // AlarmNum: 431
   ALM__RUN_TIME_CAN_CTB,             // AlarmNum: 432
   ALM__RUN_TIME_PARAMEEPROM_CTB,     // AlarmNum: 433
   ALM__RUN_TIME_PARAMAPP_CTB,        // AlarmNum: 434
   ALM__RUN_TIME_PARAMCTB,            // AlarmNum: 435
   ALM__RUN_TIME_UART_CTB,            // AlarmNum: 436
   ALM__RUN_TIME_SDATA_CTB,           // AlarmNum: 437
   ALM__RUN_TIME_POS_CTB,             // AlarmNum: 438
   ALM__RUN_TIME_LCD_CTB,             // AlarmNum: 439
   ALM__RUN_TIME_BUTTONS_CTB,         // AlarmNum: 440
   ALM__RUN_TIME_UI_CTB,              // AlarmNum: 441
   ALM__RUN_TIME_NTS,                 // AlarmNum: 442
   ALM__RUN_TIME_RTC_CTB,             // AlarmNum: 443
   ALM__RUN_TIME_ALARM_CTB,           // AlarmNum: 444
   ALM__RUN_TIME_OUTPUTS_CTB,         // AlarmNum: 445
   ALM__RUN_TIME_AUXNET_CTB,          // AlarmNum: 446
   ALM__RUN_TIME_LW_CTB,              // AlarmNum: 447
   ALM__RUN_TIME_CE_CTB,              // AlarmNum: 448
   ALM__CTB_MOD_22,                   // AlarmNum: 449
   ALM__CTB_MOD_23,                   // AlarmNum: 450
   ALM__CTB_MOD_24,                   // AlarmNum: 451
   ALM__CTB_MOD_25,                   // AlarmNum: 452
   ALM__CTB_MOD_26,                   // AlarmNum: 453
   ALM__CTB_MOD_27,                   // AlarmNum: 454
   ALM__CTB_MOD_28,                   // AlarmNum: 455
   ALM__CTB_MOD_29,                   // AlarmNum: 456
   ALM__CTB_MOD_30,                   // AlarmNum: 457
   ALM__CTB_MOD_31,                   // AlarmNum: 458
   ALM__CTB_MOD_32,                   // AlarmNum: 459
   ALM__CTB_MOD_33,                   // AlarmNum: 460
   ALM__CTB_MOD_34,                   // AlarmNum: 461
   ALM__CTB_MOD_35,                   // AlarmNum: 462
   ALM__CTB_MOD_36,                   // AlarmNum: 463
   ALM__CTB_MOD_37,                   // AlarmNum: 464
   ALM__CTB_MOD_38,                   // AlarmNum: 465
   ALM__CTB_MOD_39,                   // AlarmNum: 466
   ALM__CTB_MOD_40,                   // AlarmNum: 467
   ALM__CTB_MOD_41,                   // AlarmNum: 468
   ALM__CTB_MOD_42,                   // AlarmNum: 469
   ALM__CTB_MOD_43,                   // AlarmNum: 470
   ALM__CTB_MOD_44,                   // AlarmNum: 471
   ALM__CTB_MOD_45,                   // AlarmNum: 472
   ALM__CTB_MOD_46,                   // AlarmNum: 473
   ALM__CTB_MOD_47,                   // AlarmNum: 474
   ALM__CTB_MOD_48,                   // AlarmNum: 475
   ALM__CTB_MOD_49,                   // AlarmNum: 476
   ALM__CTB_MOD_50,                   // AlarmNum: 477
   ALM__CTB_MOD_51,                   // AlarmNum: 478
   ALM__CTB_MOD_52,                   // AlarmNum: 479
   ALM__CTB_MOD_53,                   // AlarmNum: 480
   ALM__CTB_MOD_54,                   // AlarmNum: 481
   ALM__CTB_MOD_55,                   // AlarmNum: 482
   ALM__CTB_MOD_56,                   // AlarmNum: 483
   ALM__CTB_MOD_57,                   // AlarmNum: 484
   ALM__CTB_MOD_58,                   // AlarmNum: 485
   ALM__CTB_MOD_59,                   // AlarmNum: 486
   ALM__CTB_MOD_60,                   // AlarmNum: 487
   ALM__CTB_MOD_61,                   // AlarmNum: 488
   ALM__CTB_MOD_62,                   // AlarmNum: 489
   ALM__CTB_MOD_63,                   // AlarmNum: 490
   ALM__CTB_MOD_64,                   // AlarmNum: 491
   ALM__RUN_TIME_FAULT_COPA,          // AlarmNum: 492
   ALM__RUN_TIME_FAULTAPP_COPA,       // AlarmNum: 493
   ALM__RUN_TIME_HEARTBEAT_COPA,      // AlarmNum: 494
   ALM__RUN_TIME_WD_COPA,             // AlarmNum: 495
   ALM__RUN_TIME_INPUTS_COPA,         // AlarmNum: 496
   ALM__RUN_TIME_CAN_COPA,            // AlarmNum: 497
   ALM__RUN_TIME_UART_COPA,           // AlarmNum: 498
   ALM__RUN_TIME_SDATA_COPA,          // AlarmNum: 499
   ALM__RUN_TIME_PARAMEEPORM_COPA,    // AlarmNum: 500
   ALM__RUN_TIME_PARAMAPP_COPA,       // AlarmNum: 501
   ALM__RUN_TIME_ALARM_COPA,          // AlarmNum: 502
   ALM__RUN_TIME_RTC_COPA,            // AlarmNum: 503
   ALM__RUN_TIME_FPGA_COPA,           // AlarmNum: 504
   ALM__RUN_TIME_SAFETY_COPA,         // AlarmNum: 505
   ALM__RUN_TIME_POS_COPA,            // AlarmNum: 506
   ALM__COPA_MOD_16,                  // AlarmNum: 507
   ALM__COPA_MOD_17,                  // AlarmNum: 508
   ALM__COPA_MOD_18,                  // AlarmNum: 509
   ALM__COPA_MOD_19,                  // AlarmNum: 510
   ALM__COPA_MOD_20,                  // AlarmNum: 511
   ALM__COPA_MOD_21,                  // AlarmNum: 512
   ALM__COPA_MOD_22,                  // AlarmNum: 513
   ALM__COPA_MOD_23,                  // AlarmNum: 514
   ALM__COPA_MOD_24,                  // AlarmNum: 515
   ALM__COPA_MOD_25,                  // AlarmNum: 516
   ALM__COPA_MOD_26,                  // AlarmNum: 517
   ALM__COPA_MOD_27,                  // AlarmNum: 518
   ALM__COPA_MOD_28,                  // AlarmNum: 519
   ALM__COPA_MOD_29,                  // AlarmNum: 520
   ALM__COPA_MOD_30,                  // AlarmNum: 521
   ALM__COPA_MOD_31,                  // AlarmNum: 522
   ALM__COPA_MOD_32,                  // AlarmNum: 523
   ALM__COPA_MOD_33,                  // AlarmNum: 524
   ALM__COPA_MOD_34,                  // AlarmNum: 525
   ALM__COPA_MOD_35,                  // AlarmNum: 526
   ALM__COPA_MOD_36,                  // AlarmNum: 527
   ALM__COPA_MOD_37,                  // AlarmNum: 528
   ALM__COPA_MOD_38,                  // AlarmNum: 529
   ALM__COPA_MOD_39,                  // AlarmNum: 530
   ALM__COPA_MOD_40,                  // AlarmNum: 531
   ALM__COPA_MOD_41,                  // AlarmNum: 532
   ALM__COPA_MOD_42,                  // AlarmNum: 533
   ALM__COPA_MOD_43,                  // AlarmNum: 534
   ALM__COPA_MOD_44,                  // AlarmNum: 535
   ALM__COPA_MOD_45,                  // AlarmNum: 536
   ALM__COPA_MOD_46,                  // AlarmNum: 537
   ALM__COPA_MOD_47,                  // AlarmNum: 538
   ALM__COPA_MOD_48,                  // AlarmNum: 539
   ALM__COPA_MOD_49,                  // AlarmNum: 540
   ALM__COPA_MOD_50,                  // AlarmNum: 541
   ALM__COPA_MOD_51,                  // AlarmNum: 542
   ALM__COPA_MOD_52,                  // AlarmNum: 543
   ALM__COPA_MOD_53,                  // AlarmNum: 544
   ALM__COPA_MOD_54,                  // AlarmNum: 545
   ALM__COPA_MOD_55,                  // AlarmNum: 546
   ALM__COPA_MOD_56,                  // AlarmNum: 547
   ALM__COPA_MOD_57,                  // AlarmNum: 548
   ALM__COPA_MOD_58,                  // AlarmNum: 549
   ALM__COPA_MOD_59,                  // AlarmNum: 550
   ALM__COPA_MOD_60,                  // AlarmNum: 551
   ALM__COPA_MOD_61,                  // AlarmNum: 552
   ALM__COPA_MOD_62,                  // AlarmNum: 553
   ALM__COPA_MOD_63,                  // AlarmNum: 554
   ALM__COPA_MOD_64,                  // AlarmNum: 555
   ALM__RUN_TIME_FAULT_COPB,          // AlarmNum: 556
   ALM__RUN_TIME_FAULTAPP_COPB,       // AlarmNum: 557
   ALM__RUN_TIME_HEARTBEAT_COPB,      // AlarmNum: 558
   ALM__RUN_TIME_WD_COPB,             // AlarmNum: 559
   ALM__RUN_TIME_CAN_COPB,            // AlarmNum: 560
   ALM__RUN_TIME_PARAMEEPROM_COPB,    // AlarmNum: 561
   ALM__RUN_TIME_PARAMAPP_COPB,       // AlarmNum: 562
   ALM__RUN_TIME_PARAMC_COPB,         // AlarmNum: 563
   ALM__RUN_TIME_UART_COPB,           // AlarmNum: 564
   ALM__RUN_TIME_SDATA_COPB,          // AlarmNum: 565
   ALM__RUN_TIME_LCD_COPB,            // AlarmNum: 566
   ALM__RUN_TIME_BUTTONS_COPB,        // AlarmNum: 567
   ALM__RUN_TIME_UI_COPB,             // AlarmNum: 568
   ALM__RUN_TIME_RTC_COPB,            // AlarmNum: 569
   ALM__RUIN_TIME_ALARM_COPB,         // AlarmNum: 570
   ALM__RUN_TIME_OUTPUTS_COPB,        // AlarmNum: 571
   ALM__UNUSED572,                    // AlarmNum: 572
   ALM__RUN_TIME_AUXNET_COPB,         // AlarmNum: 573
   ALM__RUN_TIME_CE_COPB,             // AlarmNum: 574
   ALM__COPB_MOD_20,                  // AlarmNum: 575
   ALM__COPB_MOD_21,                  // AlarmNum: 576
   ALM__COPB_MOD_22,                  // AlarmNum: 577
   ALM__COPB_MOD_23,                  // AlarmNum: 578
   ALM__COPB_MOD_24,                  // AlarmNum: 579
   ALM__COPB_MOD_25,                  // AlarmNum: 580
   ALM__COPB_MOD_26,                  // AlarmNum: 581
   ALM__COPB_MOD_27,                  // AlarmNum: 582
   ALM__COPB_MOD_28,                  // AlarmNum: 583
   ALM__COPB_MOD_29,                  // AlarmNum: 584
   ALM__COPB_MOD_30,                  // AlarmNum: 585
   ALM__COPB_MOD_31,                  // AlarmNum: 586
   ALM__COPB_MOD_32,                  // AlarmNum: 587
   ALM__COPB_MOD_33,                  // AlarmNum: 588
   ALM__COPB_MOD_34,                  // AlarmNum: 589
   ALM__COPB_MOD_35,                  // AlarmNum: 590
   ALM__COPB_MOD_36,                  // AlarmNum: 591
   ALM__COPB_MOD_37,                  // AlarmNum: 592
   ALM__COPB_MOD_38,                  // AlarmNum: 593
   ALM__COPB_MOD_39,                  // AlarmNum: 594
   ALM__COPB_MOD_40,                  // AlarmNum: 595
   ALM__COPB_MOD_41,                  // AlarmNum: 596
   ALM__COPB_MOD_42,                  // AlarmNum: 597
   ALM__COPB_MOD_43,                  // AlarmNum: 598
   ALM__COPB_MOD_44,                  // AlarmNum: 599
   ALM__COPB_MOD_45,                  // AlarmNum: 600
   ALM__COPB_MOD_46,                  // AlarmNum: 601
   ALM__COPB_MOD_47,                  // AlarmNum: 602
   ALM__COPB_MOD_48,                  // AlarmNum: 603
   ALM__COPB_MOD_49,                  // AlarmNum: 604
   ALM__COPB_MOD_50,                  // AlarmNum: 605
   ALM__COPB_MOD_51,                  // AlarmNum: 606
   ALM__COPB_MOD_52,                  // AlarmNum: 607
   ALM__COPB_MOD_53,                  // AlarmNum: 608
   ALM__COPB_MOD_54,                  // AlarmNum: 609
   ALM__COPB_MOD_55,                  // AlarmNum: 610
   ALM__COPB_MOD_56,                  // AlarmNum: 611
   ALM__COPB_MOD_57,                  // AlarmNum: 612
   ALM__COPB_MOD_58,                  // AlarmNum: 613
   ALM__COPB_MOD_59,                  // AlarmNum: 614
   ALM__COPB_MOD_60,                  // AlarmNum: 615
   ALM__COPB_MOD_61,                  // AlarmNum: 616
   ALM__COPB_MOD_62,                  // AlarmNum: 617
   ALM__COPB_MOD_63,                  // AlarmNum: 618
   ALM__COPB_MOD_64,                  // AlarmNum: 619
   ALM__CAR_OFFLINE_1,                // AlarmNum: 620
   ALM__CAR_OFFLINE_2,                // AlarmNum: 621
   ALM__CAR_OFFLINE_3,                // AlarmNum: 622
   ALM__CAR_OFFLINE_4,                // AlarmNum: 623
   ALM__CAR_OFFLINE_5,                // AlarmNum: 624
   ALM__CAR_OFFLINE_6,                // AlarmNum: 625
   ALM__CAR_OFFLINE_7,                // AlarmNum: 626
   ALM__CAR_OFFLINE_8,                // AlarmNum: 627
   ALM__DDM_OFFLINE,                  // AlarmNum: 628
   ALM__DOOR_OPEN_TEST,               // AlarmNum: 629
   ALM__FRAM_REDUNDANCY,              // AlarmNum: 630
   ALM__DO_DURING_RUN,                // AlarmNum: 631
   ALM__IN_DEST_DZ_DURING_RUN,        // AlarmNum: 632
   ALM__DUPLICATE_MR_501,             // AlarmNum: 633
   ALM__DUPLICATE_MR_502,             // AlarmNum: 634
   ALM__DUPLICATE_MR_503,             // AlarmNum: 635
   ALM__DUPLICATE_MR_504,             // AlarmNum: 636
   ALM__DUPLICATE_MR_505,             // AlarmNum: 637
   ALM__DUPLICATE_MR_506,             // AlarmNum: 638
   ALM__DUPLICATE_MR_507,             // AlarmNum: 639
   ALM__DUPLICATE_MR_508,             // AlarmNum: 640
   ALM__DUPLICATE_CT_501,             // AlarmNum: 641
   ALM__DUPLICATE_CT_502,             // AlarmNum: 642
   ALM__DUPLICATE_CT_503,             // AlarmNum: 643
   ALM__DUPLICATE_CT_504,             // AlarmNum: 644
   ALM__DUPLICATE_CT_505,             // AlarmNum: 645
   ALM__DUPLICATE_CT_506,             // AlarmNum: 646
   ALM__DUPLICATE_CT_507,             // AlarmNum: 647
   ALM__DUPLICATE_CT_508,             // AlarmNum: 648
   ALM__DUPLICATE_CT_509,             // AlarmNum: 649
   ALM__DUPLICATE_CT_510,             // AlarmNum: 650
   ALM__DUPLICATE_CT_511,             // AlarmNum: 651
   ALM__DUPLICATE_CT_512,             // AlarmNum: 652
   ALM__DUPLICATE_CT_513,             // AlarmNum: 653
   ALM__DUPLICATE_CT_514,             // AlarmNum: 654
   ALM__DUPLICATE_CT_515,             // AlarmNum: 655
   ALM__DUPLICATE_CT_516,             // AlarmNum: 656
   ALM__DUPLICATE_COP_501,            // AlarmNum: 657
   ALM__DUPLICATE_COP_502,            // AlarmNum: 658
   ALM__DUPLICATE_COP_503,            // AlarmNum: 659
   ALM__DUPLICATE_COP_504,            // AlarmNum: 660
   ALM__DUPLICATE_COP_505,            // AlarmNum: 661
   ALM__DUPLICATE_COP_506,            // AlarmNum: 662
   ALM__DUPLICATE_COP_507,            // AlarmNum: 663
   ALM__DUPLICATE_COP_508,            // AlarmNum: 664
   ALM__DUPLICATE_COP_509,            // AlarmNum: 665
   ALM__DUPLICATE_COP_510,            // AlarmNum: 666
   ALM__DUPLICATE_COP_511,            // AlarmNum: 667
   ALM__DUPLICATE_COP_512,            // AlarmNum: 668
   ALM__DUPLICATE_COP_513,            // AlarmNum: 669
   ALM__DUPLICATE_COP_514,            // AlarmNum: 670
   ALM__DUPLICATE_COP_515,            // AlarmNum: 671
   ALM__DUPLICATE_COP_516,            // AlarmNum: 672
   ALM__DUPLICATE_RIS1_501,           // AlarmNum: 673
   ALM__DUPLICATE_RIS1_502,           // AlarmNum: 674
   ALM__DUPLICATE_RIS1_503,           // AlarmNum: 675
   ALM__DUPLICATE_RIS1_504,           // AlarmNum: 676
   ALM__DUPLICATE_RIS1_505,           // AlarmNum: 677
   ALM__DUPLICATE_RIS1_506,           // AlarmNum: 678
   ALM__DUPLICATE_RIS1_507,           // AlarmNum: 679
   ALM__DUPLICATE_RIS1_508,           // AlarmNum: 680
   ALM__DUPLICATE_RIS2_501,           // AlarmNum: 681
   ALM__DUPLICATE_RIS2_502,           // AlarmNum: 682
   ALM__DUPLICATE_RIS2_503,           // AlarmNum: 683
   ALM__DUPLICATE_RIS2_504,           // AlarmNum: 684
   ALM__DUPLICATE_RIS2_505,           // AlarmNum: 685
   ALM__DUPLICATE_RIS2_506,           // AlarmNum: 686
   ALM__DUPLICATE_RIS2_507,           // AlarmNum: 687
   ALM__DUPLICATE_RIS2_508,           // AlarmNum: 688
   ALM__DUPLICATE_RIS3_501,           // AlarmNum: 689
   ALM__DUPLICATE_RIS3_502,           // AlarmNum: 690
   ALM__DUPLICATE_RIS3_503,           // AlarmNum: 691
   ALM__DUPLICATE_RIS3_504,           // AlarmNum: 692
   ALM__DUPLICATE_RIS3_505,           // AlarmNum: 693
   ALM__DUPLICATE_RIS3_506,           // AlarmNum: 694
   ALM__DUPLICATE_RIS3_507,           // AlarmNum: 695
   ALM__DUPLICATE_RIS3_508,           // AlarmNum: 696
   ALM__DUPLICATE_RIS4_501,           // AlarmNum: 697
   ALM__DUPLICATE_RIS4_502,           // AlarmNum: 698
   ALM__DUPLICATE_RIS4_503,           // AlarmNum: 699
   ALM__DUPLICATE_RIS4_504,           // AlarmNum: 700
   ALM__DUPLICATE_RIS4_505,           // AlarmNum: 701
   ALM__DUPLICATE_RIS4_506,           // AlarmNum: 702
   ALM__DUPLICATE_RIS4_507,           // AlarmNum: 703
   ALM__DUPLICATE_RIS4_508,           // AlarmNum: 704
   ALM__DUPLICATE_EXP1_501,           // AlarmNum: 705
   ALM__DUPLICATE_EXP1_502,           // AlarmNum: 706
   ALM__DUPLICATE_EXP1_503,           // AlarmNum: 707
   ALM__DUPLICATE_EXP1_504,           // AlarmNum: 708
   ALM__DUPLICATE_EXP1_505,           // AlarmNum: 709
   ALM__DUPLICATE_EXP1_506,           // AlarmNum: 710
   ALM__DUPLICATE_EXP1_507,           // AlarmNum: 711
   ALM__DUPLICATE_EXP1_508,           // AlarmNum: 712
   ALM__DUPLICATE_EXP2_501,           // AlarmNum: 713
   ALM__DUPLICATE_EXP2_502,           // AlarmNum: 714
   ALM__DUPLICATE_EXP2_503,           // AlarmNum: 715
   ALM__DUPLICATE_EXP2_504,           // AlarmNum: 716
   ALM__DUPLICATE_EXP2_505,           // AlarmNum: 717
   ALM__DUPLICATE_EXP2_506,           // AlarmNum: 718
   ALM__DUPLICATE_EXP2_507,           // AlarmNum: 719
   ALM__DUPLICATE_EXP2_508,           // AlarmNum: 720
   ALM__DUPLICATE_EXP3_501,           // AlarmNum: 721
   ALM__DUPLICATE_EXP3_502,           // AlarmNum: 722
   ALM__DUPLICATE_EXP3_503,           // AlarmNum: 723
   ALM__DUPLICATE_EXP3_504,           // AlarmNum: 724
   ALM__DUPLICATE_EXP3_505,           // AlarmNum: 725
   ALM__DUPLICATE_EXP3_506,           // AlarmNum: 726
   ALM__DUPLICATE_EXP3_507,           // AlarmNum: 727
   ALM__DUPLICATE_EXP3_508,           // AlarmNum: 728
   ALM__DUPLICATE_EXP4_501,           // AlarmNum: 729
   ALM__DUPLICATE_EXP4_502,           // AlarmNum: 730
   ALM__DUPLICATE_EXP4_503,           // AlarmNum: 731
   ALM__DUPLICATE_EXP4_504,           // AlarmNum: 732
   ALM__DUPLICATE_EXP4_505,           // AlarmNum: 733
   ALM__DUPLICATE_EXP4_506,           // AlarmNum: 734
   ALM__DUPLICATE_EXP4_507,           // AlarmNum: 735
   ALM__DUPLICATE_EXP4_508,           // AlarmNum: 736
   ALM__DUPLICATE_EXP5_501,           // AlarmNum: 737
   ALM__DUPLICATE_EXP5_502,           // AlarmNum: 738
   ALM__DUPLICATE_EXP5_503,           // AlarmNum: 739
   ALM__DUPLICATE_EXP5_504,           // AlarmNum: 740
   ALM__DUPLICATE_EXP5_505,           // AlarmNum: 741
   ALM__DUPLICATE_EXP5_506,           // AlarmNum: 742
   ALM__DUPLICATE_EXP5_507,           // AlarmNum: 743
   ALM__DUPLICATE_EXP5_508,           // AlarmNum: 744
   ALM__DUPLICATE_EXP6_501,           // AlarmNum: 745
   ALM__DUPLICATE_EXP6_502,           // AlarmNum: 746
   ALM__DUPLICATE_EXP6_503,           // AlarmNum: 747
   ALM__DUPLICATE_EXP6_504,           // AlarmNum: 748
   ALM__DUPLICATE_EXP6_505,           // AlarmNum: 749
   ALM__DUPLICATE_EXP6_506,           // AlarmNum: 750
   ALM__DUPLICATE_EXP6_507,           // AlarmNum: 751
   ALM__DUPLICATE_EXP6_508,           // AlarmNum: 752
   ALM__DUPLICATE_EXP7_501,           // AlarmNum: 753
   ALM__DUPLICATE_EXP7_502,           // AlarmNum: 754
   ALM__DUPLICATE_EXP7_503,           // AlarmNum: 755
   ALM__DUPLICATE_EXP7_504,           // AlarmNum: 756
   ALM__DUPLICATE_EXP7_505,           // AlarmNum: 757
   ALM__DUPLICATE_EXP7_506,           // AlarmNum: 758
   ALM__DUPLICATE_EXP7_507,           // AlarmNum: 759
   ALM__DUPLICATE_EXP7_508,           // AlarmNum: 760
   ALM__DUPLICATE_EXP8_501,           // AlarmNum: 761
   ALM__DUPLICATE_EXP8_502,           // AlarmNum: 762
   ALM__DUPLICATE_EXP8_503,           // AlarmNum: 763
   ALM__DUPLICATE_EXP8_504,           // AlarmNum: 764
   ALM__DUPLICATE_EXP8_505,           // AlarmNum: 765
   ALM__DUPLICATE_EXP8_506,           // AlarmNum: 766
   ALM__DUPLICATE_EXP8_507,           // AlarmNum: 767
   ALM__DUPLICATE_EXP8_508,           // AlarmNum: 768
   ALM__DUPLICATE_EXP9_501,           // AlarmNum: 769
   ALM__DUPLICATE_EXP9_502,           // AlarmNum: 770
   ALM__DUPLICATE_EXP9_503,           // AlarmNum: 771
   ALM__DUPLICATE_EXP9_504,           // AlarmNum: 772
   ALM__DUPLICATE_EXP9_505,           // AlarmNum: 773
   ALM__DUPLICATE_EXP9_506,           // AlarmNum: 774
   ALM__DUPLICATE_EXP9_507,           // AlarmNum: 775
   ALM__DUPLICATE_EXP9_508,           // AlarmNum: 776
   ALM__DUPLICATE_EXP10_501,          // AlarmNum: 777
   ALM__DUPLICATE_EXP10_502,          // AlarmNum: 778
   ALM__DUPLICATE_EXP10_503,          // AlarmNum: 779
   ALM__DUPLICATE_EXP10_504,          // AlarmNum: 780
   ALM__DUPLICATE_EXP10_505,          // AlarmNum: 781
   ALM__DUPLICATE_EXP10_506,          // AlarmNum: 782
   ALM__DUPLICATE_EXP10_507,          // AlarmNum: 783
   ALM__DUPLICATE_EXP10_508,          // AlarmNum: 784
   ALM__DUPLICATE_EXP11_501,          // AlarmNum: 785
   ALM__DUPLICATE_EXP11_502,          // AlarmNum: 786
   ALM__DUPLICATE_EXP11_503,          // AlarmNum: 787
   ALM__DUPLICATE_EXP11_504,          // AlarmNum: 788
   ALM__DUPLICATE_EXP11_505,          // AlarmNum: 789
   ALM__DUPLICATE_EXP11_506,          // AlarmNum: 790
   ALM__DUPLICATE_EXP11_507,          // AlarmNum: 791
   ALM__DUPLICATE_EXP11_508,          // AlarmNum: 792
   ALM__DUPLICATE_EXP12_501,          // AlarmNum: 793
   ALM__DUPLICATE_EXP12_502,          // AlarmNum: 794
   ALM__DUPLICATE_EXP12_503,          // AlarmNum: 795
   ALM__DUPLICATE_EXP12_504,          // AlarmNum: 796
   ALM__DUPLICATE_EXP12_505,          // AlarmNum: 797
   ALM__DUPLICATE_EXP12_506,          // AlarmNum: 798
   ALM__DUPLICATE_EXP12_507,          // AlarmNum: 799
   ALM__DUPLICATE_EXP12_508,          // AlarmNum: 800
   ALM__DUPLICATE_EXP13_501,          // AlarmNum: 801
   ALM__DUPLICATE_EXP13_502,          // AlarmNum: 802
   ALM__DUPLICATE_EXP13_503,          // AlarmNum: 803
   ALM__DUPLICATE_EXP13_504,          // AlarmNum: 804
   ALM__DUPLICATE_EXP13_505,          // AlarmNum: 805
   ALM__DUPLICATE_EXP13_506,          // AlarmNum: 806
   ALM__DUPLICATE_EXP13_507,          // AlarmNum: 807
   ALM__DUPLICATE_EXP13_508,          // AlarmNum: 808
   ALM__DUPLICATE_EXP14_501,          // AlarmNum: 809
   ALM__DUPLICATE_EXP14_502,          // AlarmNum: 810
   ALM__DUPLICATE_EXP14_503,          // AlarmNum: 811
   ALM__DUPLICATE_EXP14_504,          // AlarmNum: 812
   ALM__DUPLICATE_EXP14_505,          // AlarmNum: 813
   ALM__DUPLICATE_EXP14_506,          // AlarmNum: 814
   ALM__DUPLICATE_EXP14_507,          // AlarmNum: 815
   ALM__DUPLICATE_EXP14_508,          // AlarmNum: 816
   ALM__DUPLICATE_EXP15_501,          // AlarmNum: 817
   ALM__DUPLICATE_EXP15_502,          // AlarmNum: 818
   ALM__DUPLICATE_EXP15_503,          // AlarmNum: 819
   ALM__DUPLICATE_EXP15_504,          // AlarmNum: 820
   ALM__DUPLICATE_EXP15_505,          // AlarmNum: 821
   ALM__DUPLICATE_EXP15_506,          // AlarmNum: 822
   ALM__DUPLICATE_EXP15_507,          // AlarmNum: 823
   ALM__DUPLICATE_EXP15_508,          // AlarmNum: 824
   ALM__DUPLICATE_EXP16_501,          // AlarmNum: 825
   ALM__DUPLICATE_EXP16_502,          // AlarmNum: 826
   ALM__DUPLICATE_EXP16_503,          // AlarmNum: 827
   ALM__DUPLICATE_EXP16_504,          // AlarmNum: 828
   ALM__DUPLICATE_EXP16_505,          // AlarmNum: 829
   ALM__DUPLICATE_EXP16_506,          // AlarmNum: 830
   ALM__DUPLICATE_EXP16_507,          // AlarmNum: 831
   ALM__DUPLICATE_EXP16_508,          // AlarmNum: 832
   ALM__DUPLICATE_EXP17_501,          // AlarmNum: 833
   ALM__DUPLICATE_EXP17_502,          // AlarmNum: 834
   ALM__DUPLICATE_EXP17_503,          // AlarmNum: 835
   ALM__DUPLICATE_EXP17_504,          // AlarmNum: 836
   ALM__DUPLICATE_EXP17_505,          // AlarmNum: 837
   ALM__DUPLICATE_EXP17_506,          // AlarmNum: 838
   ALM__DUPLICATE_EXP17_507,          // AlarmNum: 839
   ALM__DUPLICATE_EXP17_508,          // AlarmNum: 840
   ALM__DUPLICATE_EXP18_501,          // AlarmNum: 841
   ALM__DUPLICATE_EXP18_502,          // AlarmNum: 842
   ALM__DUPLICATE_EXP18_503,          // AlarmNum: 843
   ALM__DUPLICATE_EXP18_504,          // AlarmNum: 844
   ALM__DUPLICATE_EXP18_505,          // AlarmNum: 845
   ALM__DUPLICATE_EXP18_506,          // AlarmNum: 846
   ALM__DUPLICATE_EXP18_507,          // AlarmNum: 847
   ALM__DUPLICATE_EXP18_508,          // AlarmNum: 848
   ALM__DUPLICATE_EXP19_501,          // AlarmNum: 849
   ALM__DUPLICATE_EXP19_502,          // AlarmNum: 850
   ALM__DUPLICATE_EXP19_503,          // AlarmNum: 851
   ALM__DUPLICATE_EXP19_504,          // AlarmNum: 852
   ALM__DUPLICATE_EXP19_505,          // AlarmNum: 853
   ALM__DUPLICATE_EXP19_506,          // AlarmNum: 854
   ALM__DUPLICATE_EXP19_507,          // AlarmNum: 855
   ALM__DUPLICATE_EXP19_508,          // AlarmNum: 856
   ALM__DUPLICATE_EXP20_501,          // AlarmNum: 857
   ALM__DUPLICATE_EXP20_502,          // AlarmNum: 858
   ALM__DUPLICATE_EXP20_503,          // AlarmNum: 859
   ALM__DUPLICATE_EXP20_504,          // AlarmNum: 860
   ALM__DUPLICATE_EXP20_505,          // AlarmNum: 861
   ALM__DUPLICATE_EXP20_506,          // AlarmNum: 862
   ALM__DUPLICATE_EXP20_507,          // AlarmNum: 863
   ALM__DUPLICATE_EXP20_508,          // AlarmNum: 864
   ALM__DUPLICATE_EXP21_501,          // AlarmNum: 865
   ALM__DUPLICATE_EXP21_502,          // AlarmNum: 866
   ALM__DUPLICATE_EXP21_503,          // AlarmNum: 867
   ALM__DUPLICATE_EXP21_504,          // AlarmNum: 868
   ALM__DUPLICATE_EXP21_505,          // AlarmNum: 869
   ALM__DUPLICATE_EXP21_506,          // AlarmNum: 870
   ALM__DUPLICATE_EXP21_507,          // AlarmNum: 871
   ALM__DUPLICATE_EXP21_508,          // AlarmNum: 872
   ALM__DUPLICATE_EXP22_501,          // AlarmNum: 873
   ALM__DUPLICATE_EXP22_502,          // AlarmNum: 874
   ALM__DUPLICATE_EXP22_503,          // AlarmNum: 875
   ALM__DUPLICATE_EXP22_504,          // AlarmNum: 876
   ALM__DUPLICATE_EXP22_505,          // AlarmNum: 877
   ALM__DUPLICATE_EXP22_506,          // AlarmNum: 878
   ALM__DUPLICATE_EXP22_507,          // AlarmNum: 879
   ALM__DUPLICATE_EXP22_508,          // AlarmNum: 880
   ALM__DUPLICATE_EXP23_501,          // AlarmNum: 881
   ALM__DUPLICATE_EXP23_502,          // AlarmNum: 882
   ALM__DUPLICATE_EXP23_503,          // AlarmNum: 883
   ALM__DUPLICATE_EXP23_504,          // AlarmNum: 884
   ALM__DUPLICATE_EXP23_505,          // AlarmNum: 885
   ALM__DUPLICATE_EXP23_506,          // AlarmNum: 886
   ALM__DUPLICATE_EXP23_507,          // AlarmNum: 887
   ALM__DUPLICATE_EXP23_508,          // AlarmNum: 888
   ALM__DUPLICATE_EXP24_501,          // AlarmNum: 889
   ALM__DUPLICATE_EXP24_502,          // AlarmNum: 890
   ALM__DUPLICATE_EXP24_503,          // AlarmNum: 891
   ALM__DUPLICATE_EXP24_504,          // AlarmNum: 892
   ALM__DUPLICATE_EXP24_505,          // AlarmNum: 893
   ALM__DUPLICATE_EXP24_506,          // AlarmNum: 894
   ALM__DUPLICATE_EXP24_507,          // AlarmNum: 895
   ALM__DUPLICATE_EXP24_508,          // AlarmNum: 896
   ALM__DUPLICATE_EXP25_501,          // AlarmNum: 897
   ALM__DUPLICATE_EXP25_502,          // AlarmNum: 898
   ALM__DUPLICATE_EXP25_503,          // AlarmNum: 899
   ALM__DUPLICATE_EXP25_504,          // AlarmNum: 900
   ALM__DUPLICATE_EXP25_505,          // AlarmNum: 901
   ALM__DUPLICATE_EXP25_506,          // AlarmNum: 902
   ALM__DUPLICATE_EXP25_507,          // AlarmNum: 903
   ALM__DUPLICATE_EXP25_508,          // AlarmNum: 904
   ALM__DUPLICATE_EXP26_501,          // AlarmNum: 905
   ALM__DUPLICATE_EXP26_502,          // AlarmNum: 906
   ALM__DUPLICATE_EXP26_503,          // AlarmNum: 907
   ALM__DUPLICATE_EXP26_504,          // AlarmNum: 908
   ALM__DUPLICATE_EXP26_505,          // AlarmNum: 909
   ALM__DUPLICATE_EXP26_506,          // AlarmNum: 910
   ALM__DUPLICATE_EXP26_507,          // AlarmNum: 911
   ALM__DUPLICATE_EXP26_508,          // AlarmNum: 912
   ALM__DUPLICATE_EXP27_501,          // AlarmNum: 913
   ALM__DUPLICATE_EXP27_502,          // AlarmNum: 914
   ALM__DUPLICATE_EXP27_503,          // AlarmNum: 915
   ALM__DUPLICATE_EXP27_504,          // AlarmNum: 916
   ALM__DUPLICATE_EXP27_505,          // AlarmNum: 917
   ALM__DUPLICATE_EXP27_506,          // AlarmNum: 918
   ALM__DUPLICATE_EXP27_507,          // AlarmNum: 919
   ALM__DUPLICATE_EXP27_508,          // AlarmNum: 920
   ALM__DUPLICATE_EXP28_501,          // AlarmNum: 921
   ALM__DUPLICATE_EXP28_502,          // AlarmNum: 922
   ALM__DUPLICATE_EXP28_503,          // AlarmNum: 923
   ALM__DUPLICATE_EXP28_504,          // AlarmNum: 924
   ALM__DUPLICATE_EXP28_505,          // AlarmNum: 925
   ALM__DUPLICATE_EXP28_506,          // AlarmNum: 926
   ALM__DUPLICATE_EXP28_507,          // AlarmNum: 927
   ALM__DUPLICATE_EXP28_508,          // AlarmNum: 928
   ALM__DUPLICATE_EXP29_501,          // AlarmNum: 929
   ALM__DUPLICATE_EXP29_502,          // AlarmNum: 930
   ALM__DUPLICATE_EXP29_503,          // AlarmNum: 931
   ALM__DUPLICATE_EXP29_504,          // AlarmNum: 932
   ALM__DUPLICATE_EXP29_505,          // AlarmNum: 933
   ALM__DUPLICATE_EXP29_506,          // AlarmNum: 934
   ALM__DUPLICATE_EXP29_507,          // AlarmNum: 935
   ALM__DUPLICATE_EXP29_508,          // AlarmNum: 936
   ALM__DUPLICATE_EXP30_501,          // AlarmNum: 937
   ALM__DUPLICATE_EXP30_502,          // AlarmNum: 938
   ALM__DUPLICATE_EXP30_503,          // AlarmNum: 939
   ALM__DUPLICATE_EXP30_504,          // AlarmNum: 940
   ALM__DUPLICATE_EXP30_505,          // AlarmNum: 941
   ALM__DUPLICATE_EXP30_506,          // AlarmNum: 942
   ALM__DUPLICATE_EXP30_507,          // AlarmNum: 943
   ALM__DUPLICATE_EXP30_508,          // AlarmNum: 944
   ALM__DUPLICATE_EXP31_501,          // AlarmNum: 945
   ALM__DUPLICATE_EXP31_502,          // AlarmNum: 946
   ALM__DUPLICATE_EXP31_503,          // AlarmNum: 947
   ALM__DUPLICATE_EXP31_504,          // AlarmNum: 948
   ALM__DUPLICATE_EXP31_505,          // AlarmNum: 949
   ALM__DUPLICATE_EXP31_506,          // AlarmNum: 950
   ALM__DUPLICATE_EXP31_507,          // AlarmNum: 951
   ALM__DUPLICATE_EXP31_508,          // AlarmNum: 952
   ALM__DUPLICATE_EXP32_501,          // AlarmNum: 953
   ALM__DUPLICATE_EXP32_502,          // AlarmNum: 954
   ALM__DUPLICATE_EXP32_503,          // AlarmNum: 955
   ALM__DUPLICATE_EXP32_504,          // AlarmNum: 956
   ALM__DUPLICATE_EXP32_505,          // AlarmNum: 957
   ALM__DUPLICATE_EXP32_506,          // AlarmNum: 958
   ALM__DUPLICATE_EXP32_507,          // AlarmNum: 959
   ALM__DUPLICATE_EXP32_508,          // AlarmNum: 960
   ALM__DUPLICATE_EXP33_501,          // AlarmNum: 961
   ALM__DUPLICATE_EXP33_502,          // AlarmNum: 962
   ALM__DUPLICATE_EXP33_503,          // AlarmNum: 963
   ALM__DUPLICATE_EXP33_504,          // AlarmNum: 964
   ALM__DUPLICATE_EXP33_505,          // AlarmNum: 965
   ALM__DUPLICATE_EXP33_506,          // AlarmNum: 966
   ALM__DUPLICATE_EXP33_507,          // AlarmNum: 967
   ALM__DUPLICATE_EXP33_508,          // AlarmNum: 968
   ALM__DUPLICATE_EXP34_501,          // AlarmNum: 969
   ALM__DUPLICATE_EXP34_502,          // AlarmNum: 970
   ALM__DUPLICATE_EXP34_503,          // AlarmNum: 971
   ALM__DUPLICATE_EXP34_504,          // AlarmNum: 972
   ALM__DUPLICATE_EXP34_505,          // AlarmNum: 973
   ALM__DUPLICATE_EXP34_506,          // AlarmNum: 974
   ALM__DUPLICATE_EXP34_507,          // AlarmNum: 975
   ALM__DUPLICATE_EXP34_508,          // AlarmNum: 976
   ALM__DUPLICATE_EXP35_501,          // AlarmNum: 977
   ALM__DUPLICATE_EXP35_502,          // AlarmNum: 978
   ALM__DUPLICATE_EXP35_503,          // AlarmNum: 979
   ALM__DUPLICATE_EXP35_504,          // AlarmNum: 980
   ALM__DUPLICATE_EXP35_505,          // AlarmNum: 981
   ALM__DUPLICATE_EXP35_506,          // AlarmNum: 982
   ALM__DUPLICATE_EXP35_507,          // AlarmNum: 983
   ALM__DUPLICATE_EXP35_508,          // AlarmNum: 984
   ALM__DUPLICATE_EXP36_501,          // AlarmNum: 985
   ALM__DUPLICATE_EXP36_502,          // AlarmNum: 986
   ALM__DUPLICATE_EXP36_503,          // AlarmNum: 987
   ALM__DUPLICATE_EXP36_504,          // AlarmNum: 988
   ALM__DUPLICATE_EXP36_505,          // AlarmNum: 989
   ALM__DUPLICATE_EXP36_506,          // AlarmNum: 990
   ALM__DUPLICATE_EXP36_507,          // AlarmNum: 991
   ALM__DUPLICATE_EXP36_508,          // AlarmNum: 992
   ALM__DUPLICATE_EXP37_501,          // AlarmNum: 993
   ALM__DUPLICATE_EXP37_502,          // AlarmNum: 994
   ALM__DUPLICATE_EXP37_503,          // AlarmNum: 995
   ALM__DUPLICATE_EXP37_504,          // AlarmNum: 996
   ALM__DUPLICATE_EXP37_505,          // AlarmNum: 997
   ALM__DUPLICATE_EXP37_506,          // AlarmNum: 998
   ALM__DUPLICATE_EXP37_507,          // AlarmNum: 999
   ALM__DUPLICATE_EXP37_508,          // AlarmNum: 1000
   ALM__DUPLICATE_EXP38_501,          // AlarmNum: 1001
   ALM__DUPLICATE_EXP38_502,          // AlarmNum: 1002
   ALM__DUPLICATE_EXP38_503,          // AlarmNum: 1003
   ALM__DUPLICATE_EXP38_504,          // AlarmNum: 1004
   ALM__DUPLICATE_EXP38_505,          // AlarmNum: 1005
   ALM__DUPLICATE_EXP38_506,          // AlarmNum: 1006
   ALM__DUPLICATE_EXP38_507,          // AlarmNum: 1007
   ALM__DUPLICATE_EXP38_508,          // AlarmNum: 1008
   ALM__DUPLICATE_EXP39_501,          // AlarmNum: 1009
   ALM__DUPLICATE_EXP39_502,          // AlarmNum: 1010
   ALM__DUPLICATE_EXP39_503,          // AlarmNum: 1011
   ALM__DUPLICATE_EXP39_504,          // AlarmNum: 1012
   ALM__DUPLICATE_EXP39_505,          // AlarmNum: 1013
   ALM__DUPLICATE_EXP39_506,          // AlarmNum: 1014
   ALM__DUPLICATE_EXP39_507,          // AlarmNum: 1015
   ALM__DUPLICATE_EXP39_508,          // AlarmNum: 1016
   ALM__DUPLICATE_EXP40_501,          // AlarmNum: 1017
   ALM__DUPLICATE_EXP40_502,          // AlarmNum: 1018
   ALM__DUPLICATE_EXP40_503,          // AlarmNum: 1019
   ALM__DUPLICATE_EXP40_504,          // AlarmNum: 1020
   ALM__DUPLICATE_EXP40_505,          // AlarmNum: 1021
   ALM__DUPLICATE_EXP40_506,          // AlarmNum: 1022
   ALM__DUPLICATE_EXP40_507,          // AlarmNum: 1023
   ALM__DUPLICATE_EXP40_508,          // AlarmNum: 1024
   ALM__DUPLICATE_MR_601,             // AlarmNum: 1025
   ALM__DUPLICATE_MR_602,             // AlarmNum: 1026
   ALM__DUPLICATE_MR_603,             // AlarmNum: 1027
   ALM__DUPLICATE_MR_604,             // AlarmNum: 1028
   ALM__DUPLICATE_MR_605,             // AlarmNum: 1029
   ALM__DUPLICATE_MR_606,             // AlarmNum: 1030
   ALM__DUPLICATE_MR_607,             // AlarmNum: 1031
   ALM__DUPLICATE_MR_608,             // AlarmNum: 1032
   ALM__DUPLICATE_CT_601,             // AlarmNum: 1033
   ALM__DUPLICATE_CT_602,             // AlarmNum: 1034
   ALM__DUPLICATE_CT_603,             // AlarmNum: 1035
   ALM__DUPLICATE_CT_604,             // AlarmNum: 1036
   ALM__DUPLICATE_CT_605,             // AlarmNum: 1037
   ALM__DUPLICATE_CT_606,             // AlarmNum: 1038
   ALM__DUPLICATE_CT_607,             // AlarmNum: 1039
   ALM__DUPLICATE_CT_608,             // AlarmNum: 1040
   ALM__DUPLICATE_CT_609,             // AlarmNum: 1041
   ALM__DUPLICATE_CT_610,             // AlarmNum: 1042
   ALM__DUPLICATE_CT_611,             // AlarmNum: 1043
   ALM__DUPLICATE_CT_612,             // AlarmNum: 1044
   ALM__DUPLICATE_CT_613,             // AlarmNum: 1045
   ALM__DUPLICATE_CT_614,             // AlarmNum: 1046
   ALM__DUPLICATE_CT_615,             // AlarmNum: 1047
   ALM__DUPLICATE_CT_616,             // AlarmNum: 1048
   ALM__DUPLICATE_COP_601,            // AlarmNum: 1049
   ALM__DUPLICATE_COP_602,            // AlarmNum: 1050
   ALM__DUPLICATE_COP_603,            // AlarmNum: 1051
   ALM__DUPLICATE_COP_604,            // AlarmNum: 1052
   ALM__DUPLICATE_COP_605,            // AlarmNum: 1053
   ALM__DUPLICATE_COP_606,            // AlarmNum: 1054
   ALM__DUPLICATE_COP_607,            // AlarmNum: 1055
   ALM__DUPLICATE_COP_608,            // AlarmNum: 1056
   ALM__DUPLICATE_COP_609,            // AlarmNum: 1057
   ALM__DUPLICATE_COP_610,            // AlarmNum: 1058
   ALM__DUPLICATE_COP_611,            // AlarmNum: 1059
   ALM__DUPLICATE_COP_612,            // AlarmNum: 1060
   ALM__DUPLICATE_COP_613,            // AlarmNum: 1061
   ALM__DUPLICATE_COP_614,            // AlarmNum: 1062
   ALM__DUPLICATE_COP_615,            // AlarmNum: 1063
   ALM__DUPLICATE_COP_616,            // AlarmNum: 1064
   ALM__DUPLICATE_RIS1_601,           // AlarmNum: 1065
   ALM__DUPLICATE_RIS1_602,           // AlarmNum: 1066
   ALM__DUPLICATE_RIS1_603,           // AlarmNum: 1067
   ALM__DUPLICATE_RIS1_604,           // AlarmNum: 1068
   ALM__DUPLICATE_RIS1_605,           // AlarmNum: 1069
   ALM__DUPLICATE_RIS1_606,           // AlarmNum: 1070
   ALM__DUPLICATE_RIS1_607,           // AlarmNum: 1071
   ALM__DUPLICATE_RIS1_608,           // AlarmNum: 1072
   ALM__DUPLICATE_RIS2_601,           // AlarmNum: 1073
   ALM__DUPLICATE_RIS2_602,           // AlarmNum: 1074
   ALM__DUPLICATE_RIS2_603,           // AlarmNum: 1075
   ALM__DUPLICATE_RIS2_604,           // AlarmNum: 1076
   ALM__DUPLICATE_RIS2_605,           // AlarmNum: 1077
   ALM__DUPLICATE_RIS2_606,           // AlarmNum: 1078
   ALM__DUPLICATE_RIS2_607,           // AlarmNum: 1079
   ALM__DUPLICATE_RIS2_608,           // AlarmNum: 1080
   ALM__DUPLICATE_RIS3_601,           // AlarmNum: 1081
   ALM__DUPLICATE_RIS3_602,           // AlarmNum: 1082
   ALM__DUPLICATE_RIS3_603,           // AlarmNum: 1083
   ALM__DUPLICATE_RIS3_604,           // AlarmNum: 1084
   ALM__DUPLICATE_RIS3_605,           // AlarmNum: 1085
   ALM__DUPLICATE_RIS3_606,           // AlarmNum: 1086
   ALM__DUPLICATE_RIS3_607,           // AlarmNum: 1087
   ALM__DUPLICATE_RIS3_608,           // AlarmNum: 1088
   ALM__DUPLICATE_RIS4_601,           // AlarmNum: 1089
   ALM__DUPLICATE_RIS4_602,           // AlarmNum: 1090
   ALM__DUPLICATE_RIS4_603,           // AlarmNum: 1091
   ALM__DUPLICATE_RIS4_604,           // AlarmNum: 1092
   ALM__DUPLICATE_RIS4_605,           // AlarmNum: 1093
   ALM__DUPLICATE_RIS4_606,           // AlarmNum: 1094
   ALM__DUPLICATE_RIS4_607,           // AlarmNum: 1095
   ALM__DUPLICATE_RIS4_608,           // AlarmNum: 1096
   ALM__DUPLICATE_EXP1_601,           // AlarmNum: 1097
   ALM__DUPLICATE_EXP1_602,           // AlarmNum: 1098
   ALM__DUPLICATE_EXP1_603,           // AlarmNum: 1099
   ALM__DUPLICATE_EXP1_604,           // AlarmNum: 1100
   ALM__DUPLICATE_EXP1_605,           // AlarmNum: 1101
   ALM__DUPLICATE_EXP1_606,           // AlarmNum: 1102
   ALM__DUPLICATE_EXP1_607,           // AlarmNum: 1103
   ALM__DUPLICATE_EXP1_608,           // AlarmNum: 1104
   ALM__DUPLICATE_EXP2_601,           // AlarmNum: 1105
   ALM__DUPLICATE_EXP2_602,           // AlarmNum: 1106
   ALM__DUPLICATE_EXP2_603,           // AlarmNum: 1107
   ALM__DUPLICATE_EXP2_604,           // AlarmNum: 1108
   ALM__DUPLICATE_EXP2_605,           // AlarmNum: 1109
   ALM__DUPLICATE_EXP2_606,           // AlarmNum: 1110
   ALM__DUPLICATE_EXP2_607,           // AlarmNum: 1111
   ALM__DUPLICATE_EXP2_608,           // AlarmNum: 1112
   ALM__DUPLICATE_EXP3_601,           // AlarmNum: 1113
   ALM__DUPLICATE_EXP3_602,           // AlarmNum: 1114
   ALM__DUPLICATE_EXP3_603,           // AlarmNum: 1115
   ALM__DUPLICATE_EXP3_604,           // AlarmNum: 1116
   ALM__DUPLICATE_EXP3_605,           // AlarmNum: 1117
   ALM__DUPLICATE_EXP3_606,           // AlarmNum: 1118
   ALM__DUPLICATE_EXP3_607,           // AlarmNum: 1119
   ALM__DUPLICATE_EXP3_608,           // AlarmNum: 1120
   ALM__DUPLICATE_EXP4_601,           // AlarmNum: 1121
   ALM__DUPLICATE_EXP4_602,           // AlarmNum: 1122
   ALM__DUPLICATE_EXP4_603,           // AlarmNum: 1123
   ALM__DUPLICATE_EXP4_604,           // AlarmNum: 1124
   ALM__DUPLICATE_EXP4_605,           // AlarmNum: 1125
   ALM__DUPLICATE_EXP4_606,           // AlarmNum: 1126
   ALM__DUPLICATE_EXP4_607,           // AlarmNum: 1127
   ALM__DUPLICATE_EXP4_608,           // AlarmNum: 1128
   ALM__DUPLICATE_EXP5_601,           // AlarmNum: 1129
   ALM__DUPLICATE_EXP5_602,           // AlarmNum: 1130
   ALM__DUPLICATE_EXP5_603,           // AlarmNum: 1131
   ALM__DUPLICATE_EXP5_604,           // AlarmNum: 1132
   ALM__DUPLICATE_EXP5_605,           // AlarmNum: 1133
   ALM__DUPLICATE_EXP5_606,           // AlarmNum: 1134
   ALM__DUPLICATE_EXP5_607,           // AlarmNum: 1135
   ALM__DUPLICATE_EXP5_608,           // AlarmNum: 1136
   ALM__DUPLICATE_EXP6_601,           // AlarmNum: 1137
   ALM__DUPLICATE_EXP6_602,           // AlarmNum: 1138
   ALM__DUPLICATE_EXP6_603,           // AlarmNum: 1139
   ALM__DUPLICATE_EXP6_604,           // AlarmNum: 1140
   ALM__DUPLICATE_EXP6_605,           // AlarmNum: 1141
   ALM__DUPLICATE_EXP6_606,           // AlarmNum: 1142
   ALM__DUPLICATE_EXP6_607,           // AlarmNum: 1143
   ALM__DUPLICATE_EXP6_608,           // AlarmNum: 1144
   ALM__DUPLICATE_EXP7_601,           // AlarmNum: 1145
   ALM__DUPLICATE_EXP7_602,           // AlarmNum: 1146
   ALM__DUPLICATE_EXP7_603,           // AlarmNum: 1147
   ALM__DUPLICATE_EXP7_604,           // AlarmNum: 1148
   ALM__DUPLICATE_EXP7_605,           // AlarmNum: 1149
   ALM__DUPLICATE_EXP7_606,           // AlarmNum: 1150
   ALM__DUPLICATE_EXP7_607,           // AlarmNum: 1151
   ALM__DUPLICATE_EXP7_608,           // AlarmNum: 1152
   ALM__DUPLICATE_EXP8_601,           // AlarmNum: 1153
   ALM__DUPLICATE_EXP8_602,           // AlarmNum: 1154
   ALM__DUPLICATE_EXP8_603,           // AlarmNum: 1155
   ALM__DUPLICATE_EXP8_604,           // AlarmNum: 1156
   ALM__DUPLICATE_EXP8_605,           // AlarmNum: 1157
   ALM__DUPLICATE_EXP8_606,           // AlarmNum: 1158
   ALM__DUPLICATE_EXP8_607,           // AlarmNum: 1159
   ALM__DUPLICATE_EXP8_608,           // AlarmNum: 1160
   ALM__DUPLICATE_EXP9_601,           // AlarmNum: 1161
   ALM__DUPLICATE_EXP9_602,           // AlarmNum: 1162
   ALM__DUPLICATE_EXP9_603,           // AlarmNum: 1163
   ALM__DUPLICATE_EXP9_604,           // AlarmNum: 1164
   ALM__DUPLICATE_EXP9_605,           // AlarmNum: 1165
   ALM__DUPLICATE_EXP9_606,           // AlarmNum: 1166
   ALM__DUPLICATE_EXP9_607,           // AlarmNum: 1167
   ALM__DUPLICATE_EXP9_608,           // AlarmNum: 1168
   ALM__DUPLICATE_EXP10_601,          // AlarmNum: 1169
   ALM__DUPLICATE_EXP10_602,          // AlarmNum: 1170
   ALM__DUPLICATE_EXP10_603,          // AlarmNum: 1171
   ALM__DUPLICATE_EXP10_604,          // AlarmNum: 1172
   ALM__DUPLICATE_EXP10_605,          // AlarmNum: 1173
   ALM__DUPLICATE_EXP10_606,          // AlarmNum: 1174
   ALM__DUPLICATE_EXP10_607,          // AlarmNum: 1175
   ALM__DUPLICATE_EXP10_608,          // AlarmNum: 1176
   ALM__DUPLICATE_EXP11_601,          // AlarmNum: 1177
   ALM__DUPLICATE_EXP11_602,          // AlarmNum: 1178
   ALM__DUPLICATE_EXP11_603,          // AlarmNum: 1179
   ALM__DUPLICATE_EXP11_604,          // AlarmNum: 1180
   ALM__DUPLICATE_EXP11_605,          // AlarmNum: 1181
   ALM__DUPLICATE_EXP11_606,          // AlarmNum: 1182
   ALM__DUPLICATE_EXP11_607,          // AlarmNum: 1183
   ALM__DUPLICATE_EXP11_608,          // AlarmNum: 1184
   ALM__DUPLICATE_EXP12_601,          // AlarmNum: 1185
   ALM__DUPLICATE_EXP12_602,          // AlarmNum: 1186
   ALM__DUPLICATE_EXP12_603,          // AlarmNum: 1187
   ALM__DUPLICATE_EXP12_604,          // AlarmNum: 1188
   ALM__DUPLICATE_EXP12_605,          // AlarmNum: 1189
   ALM__DUPLICATE_EXP12_606,          // AlarmNum: 1190
   ALM__DUPLICATE_EXP12_607,          // AlarmNum: 1191
   ALM__DUPLICATE_EXP12_608,          // AlarmNum: 1192
   ALM__DUPLICATE_EXP13_601,          // AlarmNum: 1193
   ALM__DUPLICATE_EXP13_602,          // AlarmNum: 1194
   ALM__DUPLICATE_EXP13_603,          // AlarmNum: 1195
   ALM__DUPLICATE_EXP13_604,          // AlarmNum: 1196
   ALM__DUPLICATE_EXP13_605,          // AlarmNum: 1197
   ALM__DUPLICATE_EXP13_606,          // AlarmNum: 1198
   ALM__DUPLICATE_EXP13_607,          // AlarmNum: 1199
   ALM__DUPLICATE_EXP13_608,          // AlarmNum: 1200
   ALM__DUPLICATE_EXP14_601,          // AlarmNum: 1201
   ALM__DUPLICATE_EXP14_602,          // AlarmNum: 1202
   ALM__DUPLICATE_EXP14_603,          // AlarmNum: 1203
   ALM__DUPLICATE_EXP14_604,          // AlarmNum: 1204
   ALM__DUPLICATE_EXP14_605,          // AlarmNum: 1205
   ALM__DUPLICATE_EXP14_606,          // AlarmNum: 1206
   ALM__DUPLICATE_EXP14_607,          // AlarmNum: 1207
   ALM__DUPLICATE_EXP14_608,          // AlarmNum: 1208
   ALM__DUPLICATE_EXP15_601,          // AlarmNum: 1209
   ALM__DUPLICATE_EXP15_602,          // AlarmNum: 1210
   ALM__DUPLICATE_EXP15_603,          // AlarmNum: 1211
   ALM__DUPLICATE_EXP15_604,          // AlarmNum: 1212
   ALM__DUPLICATE_EXP15_605,          // AlarmNum: 1213
   ALM__DUPLICATE_EXP15_606,          // AlarmNum: 1214
   ALM__DUPLICATE_EXP15_607,          // AlarmNum: 1215
   ALM__DUPLICATE_EXP15_608,          // AlarmNum: 1216
   ALM__DUPLICATE_EXP16_601,          // AlarmNum: 1217
   ALM__DUPLICATE_EXP16_602,          // AlarmNum: 1218
   ALM__DUPLICATE_EXP16_603,          // AlarmNum: 1219
   ALM__DUPLICATE_EXP16_604,          // AlarmNum: 1220
   ALM__DUPLICATE_EXP16_605,          // AlarmNum: 1221
   ALM__DUPLICATE_EXP16_606,          // AlarmNum: 1222
   ALM__DUPLICATE_EXP16_607,          // AlarmNum: 1223
   ALM__DUPLICATE_EXP16_608,          // AlarmNum: 1224
   ALM__DUPLICATE_EXP17_601,          // AlarmNum: 1225
   ALM__DUPLICATE_EXP17_602,          // AlarmNum: 1226
   ALM__DUPLICATE_EXP17_603,          // AlarmNum: 1227
   ALM__DUPLICATE_EXP17_604,          // AlarmNum: 1228
   ALM__DUPLICATE_EXP17_605,          // AlarmNum: 1229
   ALM__DUPLICATE_EXP17_606,          // AlarmNum: 1230
   ALM__DUPLICATE_EXP17_607,          // AlarmNum: 1231
   ALM__DUPLICATE_EXP17_608,          // AlarmNum: 1232
   ALM__DUPLICATE_EXP18_601,          // AlarmNum: 1233
   ALM__DUPLICATE_EXP18_602,          // AlarmNum: 1234
   ALM__DUPLICATE_EXP18_603,          // AlarmNum: 1235
   ALM__DUPLICATE_EXP18_604,          // AlarmNum: 1236
   ALM__DUPLICATE_EXP18_605,          // AlarmNum: 1237
   ALM__DUPLICATE_EXP18_606,          // AlarmNum: 1238
   ALM__DUPLICATE_EXP18_607,          // AlarmNum: 1239
   ALM__DUPLICATE_EXP18_608,          // AlarmNum: 1240
   ALM__DUPLICATE_EXP19_601,          // AlarmNum: 1241
   ALM__DUPLICATE_EXP19_602,          // AlarmNum: 1242
   ALM__DUPLICATE_EXP19_603,          // AlarmNum: 1243
   ALM__DUPLICATE_EXP19_604,          // AlarmNum: 1244
   ALM__DUPLICATE_EXP19_605,          // AlarmNum: 1245
   ALM__DUPLICATE_EXP19_606,          // AlarmNum: 1246
   ALM__DUPLICATE_EXP19_607,          // AlarmNum: 1247
   ALM__DUPLICATE_EXP19_608,          // AlarmNum: 1248
   ALM__DUPLICATE_EXP20_601,          // AlarmNum: 1249
   ALM__DUPLICATE_EXP20_602,          // AlarmNum: 1250
   ALM__DUPLICATE_EXP20_603,          // AlarmNum: 1251
   ALM__DUPLICATE_EXP20_604,          // AlarmNum: 1252
   ALM__DUPLICATE_EXP20_605,          // AlarmNum: 1253
   ALM__DUPLICATE_EXP20_606,          // AlarmNum: 1254
   ALM__DUPLICATE_EXP20_607,          // AlarmNum: 1255
   ALM__DUPLICATE_EXP20_608,          // AlarmNum: 1256
   ALM__DUPLICATE_EXP21_601,          // AlarmNum: 1257
   ALM__DUPLICATE_EXP21_602,          // AlarmNum: 1258
   ALM__DUPLICATE_EXP21_603,          // AlarmNum: 1259
   ALM__DUPLICATE_EXP21_604,          // AlarmNum: 1260
   ALM__DUPLICATE_EXP21_605,          // AlarmNum: 1261
   ALM__DUPLICATE_EXP21_606,          // AlarmNum: 1262
   ALM__DUPLICATE_EXP21_607,          // AlarmNum: 1263
   ALM__DUPLICATE_EXP21_608,          // AlarmNum: 1264
   ALM__DUPLICATE_EXP22_601,          // AlarmNum: 1265
   ALM__DUPLICATE_EXP22_602,          // AlarmNum: 1266
   ALM__DUPLICATE_EXP22_603,          // AlarmNum: 1267
   ALM__DUPLICATE_EXP22_604,          // AlarmNum: 1268
   ALM__DUPLICATE_EXP22_605,          // AlarmNum: 1269
   ALM__DUPLICATE_EXP22_606,          // AlarmNum: 1270
   ALM__DUPLICATE_EXP22_607,          // AlarmNum: 1271
   ALM__DUPLICATE_EXP22_608,          // AlarmNum: 1272
   ALM__DUPLICATE_EXP23_601,          // AlarmNum: 1273
   ALM__DUPLICATE_EXP23_602,          // AlarmNum: 1274
   ALM__DUPLICATE_EXP23_603,          // AlarmNum: 1275
   ALM__DUPLICATE_EXP23_604,          // AlarmNum: 1276
   ALM__DUPLICATE_EXP23_605,          // AlarmNum: 1277
   ALM__DUPLICATE_EXP23_606,          // AlarmNum: 1278
   ALM__DUPLICATE_EXP23_607,          // AlarmNum: 1279
   ALM__DUPLICATE_EXP23_608,          // AlarmNum: 1280
   ALM__DUPLICATE_EXP24_601,          // AlarmNum: 1281
   ALM__DUPLICATE_EXP24_602,          // AlarmNum: 1282
   ALM__DUPLICATE_EXP24_603,          // AlarmNum: 1283
   ALM__DUPLICATE_EXP24_604,          // AlarmNum: 1284
   ALM__DUPLICATE_EXP24_605,          // AlarmNum: 1285
   ALM__DUPLICATE_EXP24_606,          // AlarmNum: 1286
   ALM__DUPLICATE_EXP24_607,          // AlarmNum: 1287
   ALM__DUPLICATE_EXP24_608,          // AlarmNum: 1288
   ALM__DUPLICATE_EXP25_601,          // AlarmNum: 1289
   ALM__DUPLICATE_EXP25_602,          // AlarmNum: 1290
   ALM__DUPLICATE_EXP25_603,          // AlarmNum: 1291
   ALM__DUPLICATE_EXP25_604,          // AlarmNum: 1292
   ALM__DUPLICATE_EXP25_605,          // AlarmNum: 1293
   ALM__DUPLICATE_EXP25_606,          // AlarmNum: 1294
   ALM__DUPLICATE_EXP25_607,          // AlarmNum: 1295
   ALM__DUPLICATE_EXP25_608,          // AlarmNum: 1296
   ALM__DUPLICATE_EXP26_601,          // AlarmNum: 1297
   ALM__DUPLICATE_EXP26_602,          // AlarmNum: 1298
   ALM__DUPLICATE_EXP26_603,          // AlarmNum: 1299
   ALM__DUPLICATE_EXP26_604,          // AlarmNum: 1300
   ALM__DUPLICATE_EXP26_605,          // AlarmNum: 1301
   ALM__DUPLICATE_EXP26_606,          // AlarmNum: 1302
   ALM__DUPLICATE_EXP26_607,          // AlarmNum: 1303
   ALM__DUPLICATE_EXP26_608,          // AlarmNum: 1304
   ALM__DUPLICATE_EXP27_601,          // AlarmNum: 1305
   ALM__DUPLICATE_EXP27_602,          // AlarmNum: 1306
   ALM__DUPLICATE_EXP27_603,          // AlarmNum: 1307
   ALM__DUPLICATE_EXP27_604,          // AlarmNum: 1308
   ALM__DUPLICATE_EXP27_605,          // AlarmNum: 1309
   ALM__DUPLICATE_EXP27_606,          // AlarmNum: 1310
   ALM__DUPLICATE_EXP27_607,          // AlarmNum: 1311
   ALM__DUPLICATE_EXP27_608,          // AlarmNum: 1312
   ALM__DUPLICATE_EXP28_601,          // AlarmNum: 1313
   ALM__DUPLICATE_EXP28_602,          // AlarmNum: 1314
   ALM__DUPLICATE_EXP28_603,          // AlarmNum: 1315
   ALM__DUPLICATE_EXP28_604,          // AlarmNum: 1316
   ALM__DUPLICATE_EXP28_605,          // AlarmNum: 1317
   ALM__DUPLICATE_EXP28_606,          // AlarmNum: 1318
   ALM__DUPLICATE_EXP28_607,          // AlarmNum: 1319
   ALM__DUPLICATE_EXP28_608,          // AlarmNum: 1320
   ALM__DUPLICATE_EXP29_601,          // AlarmNum: 1321
   ALM__DUPLICATE_EXP29_602,          // AlarmNum: 1322
   ALM__DUPLICATE_EXP29_603,          // AlarmNum: 1323
   ALM__DUPLICATE_EXP29_604,          // AlarmNum: 1324
   ALM__DUPLICATE_EXP29_605,          // AlarmNum: 1325
   ALM__DUPLICATE_EXP29_606,          // AlarmNum: 1326
   ALM__DUPLICATE_EXP29_607,          // AlarmNum: 1327
   ALM__DUPLICATE_EXP29_608,          // AlarmNum: 1328
   ALM__DUPLICATE_EXP30_601,          // AlarmNum: 1329
   ALM__DUPLICATE_EXP30_602,          // AlarmNum: 1330
   ALM__DUPLICATE_EXP30_603,          // AlarmNum: 1331
   ALM__DUPLICATE_EXP30_604,          // AlarmNum: 1332
   ALM__DUPLICATE_EXP30_605,          // AlarmNum: 1333
   ALM__DUPLICATE_EXP30_606,          // AlarmNum: 1334
   ALM__DUPLICATE_EXP30_607,          // AlarmNum: 1335
   ALM__DUPLICATE_EXP30_608,          // AlarmNum: 1336
   ALM__DUPLICATE_EXP31_601,          // AlarmNum: 1337
   ALM__DUPLICATE_EXP31_602,          // AlarmNum: 1338
   ALM__DUPLICATE_EXP31_603,          // AlarmNum: 1339
   ALM__DUPLICATE_EXP31_604,          // AlarmNum: 1340
   ALM__DUPLICATE_EXP31_605,          // AlarmNum: 1341
   ALM__DUPLICATE_EXP31_606,          // AlarmNum: 1342
   ALM__DUPLICATE_EXP31_607,          // AlarmNum: 1343
   ALM__DUPLICATE_EXP31_608,          // AlarmNum: 1344
   ALM__DUPLICATE_EXP32_601,          // AlarmNum: 1345
   ALM__DUPLICATE_EXP32_602,          // AlarmNum: 1346
   ALM__DUPLICATE_EXP32_603,          // AlarmNum: 1347
   ALM__DUPLICATE_EXP32_604,          // AlarmNum: 1348
   ALM__DUPLICATE_EXP32_605,          // AlarmNum: 1349
   ALM__DUPLICATE_EXP32_606,          // AlarmNum: 1350
   ALM__DUPLICATE_EXP32_607,          // AlarmNum: 1351
   ALM__DUPLICATE_EXP32_608,          // AlarmNum: 1352
   ALM__DUPLICATE_EXP33_601,          // AlarmNum: 1353
   ALM__DUPLICATE_EXP33_602,          // AlarmNum: 1354
   ALM__DUPLICATE_EXP33_603,          // AlarmNum: 1355
   ALM__DUPLICATE_EXP33_604,          // AlarmNum: 1356
   ALM__DUPLICATE_EXP33_605,          // AlarmNum: 1357
   ALM__DUPLICATE_EXP33_606,          // AlarmNum: 1358
   ALM__DUPLICATE_EXP33_607,          // AlarmNum: 1359
   ALM__DUPLICATE_EXP33_608,          // AlarmNum: 1360
   ALM__DUPLICATE_EXP34_601,          // AlarmNum: 1361
   ALM__DUPLICATE_EXP34_602,          // AlarmNum: 1362
   ALM__DUPLICATE_EXP34_603,          // AlarmNum: 1363
   ALM__DUPLICATE_EXP34_604,          // AlarmNum: 1364
   ALM__DUPLICATE_EXP34_605,          // AlarmNum: 1365
   ALM__DUPLICATE_EXP34_606,          // AlarmNum: 1366
   ALM__DUPLICATE_EXP34_607,          // AlarmNum: 1367
   ALM__DUPLICATE_EXP34_608,          // AlarmNum: 1368
   ALM__DUPLICATE_EXP35_601,          // AlarmNum: 1369
   ALM__DUPLICATE_EXP35_602,          // AlarmNum: 1370
   ALM__DUPLICATE_EXP35_603,          // AlarmNum: 1371
   ALM__DUPLICATE_EXP35_604,          // AlarmNum: 1372
   ALM__DUPLICATE_EXP35_605,          // AlarmNum: 1373
   ALM__DUPLICATE_EXP35_606,          // AlarmNum: 1374
   ALM__DUPLICATE_EXP35_607,          // AlarmNum: 1375
   ALM__DUPLICATE_EXP35_608,          // AlarmNum: 1376
   ALM__DUPLICATE_EXP36_601,          // AlarmNum: 1377
   ALM__DUPLICATE_EXP36_602,          // AlarmNum: 1378
   ALM__DUPLICATE_EXP36_603,          // AlarmNum: 1379
   ALM__DUPLICATE_EXP36_604,          // AlarmNum: 1380
   ALM__DUPLICATE_EXP36_605,          // AlarmNum: 1381
   ALM__DUPLICATE_EXP36_606,          // AlarmNum: 1382
   ALM__DUPLICATE_EXP36_607,          // AlarmNum: 1383
   ALM__DUPLICATE_EXP36_608,          // AlarmNum: 1384
   ALM__DUPLICATE_EXP37_601,          // AlarmNum: 1385
   ALM__DUPLICATE_EXP37_602,          // AlarmNum: 1386
   ALM__DUPLICATE_EXP37_603,          // AlarmNum: 1387
   ALM__DUPLICATE_EXP37_604,          // AlarmNum: 1388
   ALM__DUPLICATE_EXP37_605,          // AlarmNum: 1389
   ALM__DUPLICATE_EXP37_606,          // AlarmNum: 1390
   ALM__DUPLICATE_EXP37_607,          // AlarmNum: 1391
   ALM__DUPLICATE_EXP37_608,          // AlarmNum: 1392
   ALM__DUPLICATE_EXP38_601,          // AlarmNum: 1393
   ALM__DUPLICATE_EXP38_602,          // AlarmNum: 1394
   ALM__DUPLICATE_EXP38_603,          // AlarmNum: 1395
   ALM__DUPLICATE_EXP38_604,          // AlarmNum: 1396
   ALM__DUPLICATE_EXP38_605,          // AlarmNum: 1397
   ALM__DUPLICATE_EXP38_606,          // AlarmNum: 1398
   ALM__DUPLICATE_EXP38_607,          // AlarmNum: 1399
   ALM__DUPLICATE_EXP38_608,          // AlarmNum: 1400
   ALM__DUPLICATE_EXP39_601,          // AlarmNum: 1401
   ALM__DUPLICATE_EXP39_602,          // AlarmNum: 1402
   ALM__DUPLICATE_EXP39_603,          // AlarmNum: 1403
   ALM__DUPLICATE_EXP39_604,          // AlarmNum: 1404
   ALM__DUPLICATE_EXP39_605,          // AlarmNum: 1405
   ALM__DUPLICATE_EXP39_606,          // AlarmNum: 1406
   ALM__DUPLICATE_EXP39_607,          // AlarmNum: 1407
   ALM__DUPLICATE_EXP39_608,          // AlarmNum: 1408
   ALM__DUPLICATE_EXP40_601,          // AlarmNum: 1409
   ALM__DUPLICATE_EXP40_602,          // AlarmNum: 1410
   ALM__DUPLICATE_EXP40_603,          // AlarmNum: 1411
   ALM__DUPLICATE_EXP40_604,          // AlarmNum: 1412
   ALM__DUPLICATE_EXP40_605,          // AlarmNum: 1413
   ALM__DUPLICATE_EXP40_606,          // AlarmNum: 1414
   ALM__DUPLICATE_EXP40_607,          // AlarmNum: 1415
   ALM__DUPLICATE_EXP40_608,          // AlarmNum: 1416
   ALM__LWD_OFFLINE,                  // AlarmNum: 1417
   ALM__DL20_OFFLINE_CT,              // AlarmNum: 1418
   ALM__DL20_OFFLINE_COP,             // AlarmNum: 1419
   ALM__CPLD_OVF_MR,                  // AlarmNum: 1420
   ALM__CPLD_OVF_CT,                  // AlarmNum: 1421
   ALM__CPLD_OVF_COP,                 // AlarmNum: 1422
   ALM__FIRE_KEY_MAIN,                // AlarmNum: 1423
   ALM__FIRE_KEY_REMOTE,              // AlarmNum: 1424
   ALM__FIRE_SMOKE_MAIN,              // AlarmNum: 1425
   ALM__FIRE_SMOKE_ALT,               // AlarmNum: 1426
   ALM__FIRE_SMOKE_MR,                // AlarmNum: 1427
   ALM__FIRE_SMOKE_HA,                // AlarmNum: 1428
   ALM__FIRE_SMOKE_SAVED,             // AlarmNum: 1429
   ALM__FIRE_SMOKE_PIT,               // AlarmNum: 1430
   ALM__FIRE_SMOKE_MR_2,              // AlarmNum: 1431
   ALM__FIRE_SMOKE_HA_2,              // AlarmNum: 1432
   ALM__NEED_TO_RESET_MR,             // AlarmNum: 1433
   ALM__NEED_TO_RESET_CT,             // AlarmNum: 1434
   ALM__NEED_TO_RESET_COP,            // AlarmNum: 1435
   ALM__UNINTENDED_MOV_TEST_ACTIVE,   // AlarmNum: 1436
   ALM__DUPAR_COP_OFFLINE,            // AlarmNum: 1437
   ALM__RIS1_HB_OFFLINE,              // AlarmNum: 1438
   ALM__RIS2_HB_OFFLINE,              // AlarmNum: 1439
   ALM__RIS3_HB_OFFLINE,              // AlarmNum: 1440
   ALM__RIS4_HB_OFFLINE,              // AlarmNum: 1441
   ALM__SHIELD_UNKNOWN,               // AlarmNum: 1442
   ALM__SHIELD_POR_RESET,             // AlarmNum: 1443
   ALM__SHIELD_BOD_RESET,             // AlarmNum: 1444
   ALM__SHIELD_WDT_RESET,             // AlarmNum: 1445
   ALM__SHIELD_COM_GROUP,             // AlarmNum: 1446
   ALM__SHIELD_COM_RPI,               // AlarmNum: 1447
   ALM__SHIELD_FAILED_RTC,            // AlarmNum: 1448
   ALM__SHIELD_UART_OVF_TX,           // AlarmNum: 1449
   ALM__SHIELD_UART_OVF_RX,           // AlarmNum: 1450
   ALM__SHIELD_CAN_OVF_TX,            // AlarmNum: 1451
   ALM__SHIELD_CAN_OVF_RX,            // AlarmNum: 1452
   ALM__SHIELD_CAN_BUS_RST,           // AlarmNum: 1453
   ALM__VIP_TIMEOUT,                  // AlarmNum: 1454
   ALM__FIRE_VIRTUAL_REMOTE_RECALL,   // AlarmNum: 1455
   ALM__EMS2_NOT_AT_RECALL,           // AlarmNum: 1456

   NUM_ALARMS // Total: 1457
} en_alarms;

// These are all the nodes that maintain a copy of the faults.
typedef enum
{
   enALARM_NODE__CPLD,
   enALARM_NODE__MRA,
   enALARM_NODE__MRB,
   enALARM_NODE__CTA,
   enALARM_NODE__CTB,
   enALARM_NODE__COPA,
   enALARM_NODE__COPB,

   NUM_ALARM_NODES,
   enALARM_NODE__UNK,
} en_alarm_nodes;

/* ALARM_ENTRY(A, B) FORMAT:
 *    A = Unique Alarm Number ID
 *    B = Alarm String
 * */
/* Add Alarm HERE - LEAVE NO WHITESPACE BETWEEN LINES */

/* Alarm Table Definition */
#define ALARM_TABLE(ALM_ENTRY)  \
   ALM_ENTRY( ALM__NONE,                         "A:No Alarm"                     ) \
   ALM_ENTRY( ALM__NTS_UP_P1_1,                  "A:NTS Up P1-1"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P1_2,                  "A:NTS Up P1-2"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P1_3,                  "A:NTS Up P1-3"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P1_4,                  "A:NTS Up P1-4"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P1_5,                  "A:NTS Up P1-5"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P1_6,                  "A:NTS Up P1-6"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P1_7,                  "A:NTS Up P1-7"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P1_8,                  "A:NTS Up P1-8"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P2_1,                  "A:NTS Up P2-1"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P2_2,                  "A:NTS Up P2-2"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P2_3,                  "A:NTS Up P2-3"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P2_4,                  "A:NTS Up P2-4"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P2_5,                  "A:NTS Up P2-5"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P2_6,                  "A:NTS Up P2-6"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P2_7,                  "A:NTS Up P2-7"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P2_8,                  "A:NTS Up P2-8"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P3_1,                  "A:NTS Up P3-1"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P3_2,                  "A:NTS Up P3-2"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P3_3,                  "A:NTS Up P3-3"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P3_4,                  "A:NTS Up P3-4"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P3_5,                  "A:NTS Up P3-5"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P3_6,                  "A:NTS Up P3-6"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P3_7,                  "A:NTS Up P3-7"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P3_8,                  "A:NTS Up P3-8"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P4_1,                  "A:NTS Up P4-1"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P4_2,                  "A:NTS Up P4-2"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P4_3,                  "A:NTS Up P4-3"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P4_4,                  "A:NTS Up P4-4"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P4_5,                  "A:NTS Up P4-5"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P4_6,                  "A:NTS Up P4-6"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P4_7,                  "A:NTS Up P4-7"                  ) \
   ALM_ENTRY( ALM__NTS_UP_P4_8,                  "A:NTS Up P4-8"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P1_1,                  "A:NTS Dn P1-1"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P1_2,                  "A:NTS Dn P1-2"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P1_3,                  "A:NTS Dn P1-3"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P1_4,                  "A:NTS Dn P1-4"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P1_5,                  "A:NTS Dn P1-5"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P1_6,                  "A:NTS Dn P1-6"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P1_7,                  "A:NTS Dn P1-7"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P1_8,                  "A:NTS Dn P1-8"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P2_1,                  "A:NTS Dn P2-1"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P2_2,                  "A:NTS Dn P2-2"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P2_3,                  "A:NTS Dn P2-3"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P2_4,                  "A:NTS Dn P2-4"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P2_5,                  "A:NTS Dn P2-5"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P2_6,                  "A:NTS Dn P2-6"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P2_7,                  "A:NTS Dn P2-7"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P2_8,                  "A:NTS Dn P2-8"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P3_1,                  "A:NTS Dn P3-1"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P3_2,                  "A:NTS Dn P3-2"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P3_3,                  "A:NTS Dn P3-3"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P3_4,                  "A:NTS Dn P3-4"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P3_5,                  "A:NTS Dn P3-5"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P3_6,                  "A:NTS Dn P3-6"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P3_7,                  "A:NTS Dn P3-7"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P3_8,                  "A:NTS Dn P3-8"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P4_1,                  "A:NTS Dn P4-1"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P4_2,                  "A:NTS Dn P4-2"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P4_3,                  "A:NTS Dn P4-3"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P4_4,                  "A:NTS Dn P4-4"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P4_5,                  "A:NTS Dn P4-5"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P4_6,                  "A:NTS Dn P4-6"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P4_7,                  "A:NTS Dn P4-7"                  ) \
   ALM_ENTRY( ALM__NTS_DN_P4_8,                  "A:NTS Dn P4-8"                  ) \
   ALM_ENTRY( ALM__NTS_INVALID_P1,               "A:NTS Invalid P1"               ) \
   ALM_ENTRY( ALM__NTS_INVALID_P2,               "A:NTS Invalid P2"               ) \
   ALM_ENTRY( ALM__NTS_INVALID_P3,               "A:NTS Invalid P3"               ) \
   ALM_ENTRY( ALM__NTS_INVALID_P4,               "A:NTS Invalid P4"               ) \
   ALM_ENTRY( ALM__ESTOP_CLASS_OP,               "A:ES Class Op"                  ) \
   ALM_ENTRY( ALM__ESTOP_STOP_TIMEOUT,           "A:ES Stop Timeout"              ) \
   ALM_ENTRY( ALM__ESTOP_MOVE_TIMEOUT,           "A:ES Move Timeout"              ) \
   ALM_ENTRY( ALM__ESTOP_INVALID_INSP,           "A:ES Inv Insp"                  ) \
   ALM_ENTRY( ALM__ESTOP_RECALL_DEST,            "A:ES Recall Dest."              ) \
   ALM_ENTRY( ALM__STOP_AT_NEXT_FLOOR,           "A:ES Stop At Next"              ) \
   ALM_ENTRY( ALM__ESTOP_EQ,                     "A:ES Earthquake"                ) \
   ALM_ENTRY( ALM__ESTOP_FLOOD,                  "A:ES Flood"                     ) \
   ALM_ENTRY( ALM__STOP_NO_DZ,                   "A:Stop No DZ"                   ) \
   ALM_ENTRY( ALM__RELEVELING,                   "A:Releveling"                   ) \
   ALM_ENTRY( ALM__DEFAULT_PARAM_1BIT,           "A:Defaulting 1-Bit"             ) \
   ALM_ENTRY( ALM__DEFAULT_PARAM_8BIT,           "A:Defaulting 8-Bit"             ) \
   ALM_ENTRY( ALM__DEFAULT_PARAM_16BIT,          "A:Defaulting 16-Bit"            ) \
   ALM_ENTRY( ALM__DEFAULT_PARAM_24BIT,          "A:Defaulting 24-Bit"            ) \
   ALM_ENTRY( ALM__DEFAULT_PARAM_32BIT,          "A:Defaulting 32-Bit"            ) \
   ALM_ENTRY( ALM__RECALL_INVALID_DOOR,          "A:Recall Inv Door"              ) \
   ALM_ENTRY( ALM__RECALL_INVALID_FLOOR,         "A:Recall Inv Floor"             ) \
   ALM_ENTRY( ALM__RECALL_INVALID_OPENING,       "A:Recall Inv Opening"           ) \
   ALM_ENTRY( ALM__WDT_DISABLED_MRA,             "A:MRA WDT Disabled"             ) \
   ALM_ENTRY( ALM__WDT_DISABLED_MRB,             "A:MRB WDT Disabled"             ) \
   ALM_ENTRY( ALM__WDT_DISABLED_CTA,             "A:CTA WDT Disabled"             ) \
   ALM_ENTRY( ALM__WDT_DISABLED_CTB,             "A:CTB WDT Disabled"             ) \
   ALM_ENTRY( ALM__WDT_DISABLED_COPA,            "A:COPA WDT Disabled"            ) \
   ALM_ENTRY( ALM__WDT_DISABLED_COPB,            "A:COPB WDT Disabled"            ) \
   ALM_ENTRY( ALM__MR_CAN_RESET_1,               "A:MR CAN Rst 1"                 ) \
   ALM_ENTRY( ALM__MR_CAN_RESET_2,               "A:MR CAN Rst 2"                 ) \
   ALM_ENTRY( ALM__MR_CAN_RESET_3,               "A:MR CAN Rst 3"                 ) \
   ALM_ENTRY( ALM__MR_CAN_RESET_4,               "A:MR CAN Rst 4"                 ) \
   ALM_ENTRY( ALM__CT_CAN_RESET_1,               "A:CT CAN Rst 1"                 ) \
   ALM_ENTRY( ALM__CT_CAN_RESET_2,               "A:CT CAN Rst 2"                 ) \
   ALM_ENTRY( ALM__CT_CAN_RESET_3,               "A:CT CAN Rst 3"                 ) \
   ALM_ENTRY( ALM__CT_CAN_RESET_4,               "A:CT CAN Rst 4"                 ) \
   ALM_ENTRY( ALM__COP_CAN_RESET_1,              "A:COP CAN Rst 1"                ) \
   ALM_ENTRY( ALM__COP_CAN_RESET_2,              "A:COP CAN Rst 2"                ) \
   ALM_ENTRY( ALM__COP_CAN_RESET_3,              "A:COP CAN Rst 3"                ) \
   ALM_ENTRY( ALM__COP_CAN_RESET_4,              "A:COP CAN Rst 4"                ) \
   ALM_ENTRY( ALM__DRIVE_RESET,                  "A:Drive Rst"                    ) \
   ALM_ENTRY( ALM__DRIVE_RESET_LIMIT,            "A:Drive Rst Limit"              ) \
   ALM_ENTRY( ALM__FULLY_LOADED,                 "A:Fully Loaded"                 ) \
   ALM_ENTRY( ALM__REMOTE_PU_1BIT,               "A:Remote PU 1-Bit"              ) \
   ALM_ENTRY( ALM__REMOTE_PU_8BIT,               "A:Remote PU 8-Bit"              ) \
   ALM_ENTRY( ALM__REMOTE_PU_16BIT,              "A:Remote PU 16-Bit"             ) \
   ALM_ENTRY( ALM__REMOTE_PU_24BIT,              "A:Remote PU 24-Bit"             ) \
   ALM_ENTRY( ALM__REMOTE_PU_32BIT,              "A:Remote PU 32-Bit"             ) \
   ALM_ENTRY( ALM__REMOTE_PU_MAG,                "A:Remote PU Mag"                ) \
   ALM_ENTRY( ALM__REMOTE_PU_KEB,                "A:Remote PU KEB"                ) \
   ALM_ENTRY( ALM__INVALID_MAN_RUN_DOOR,         "A:Inv Man Run Door"             ) \
   ALM_ENTRY( ALM__INVALID_MAN_RUN_LOCK,         "A:Inv Man Run Lock"             ) \
   ALM_ENTRY( ALM__INVALID_MAN_RUN_ARM,          "A:Inv Man Run Arm"              ) \
   ALM_ENTRY( ALM__INVALID_MAN_RUN_DCBF,         "A:NA"                           ) \
   ALM_ENTRY( ALM__INVALID_MAN_RUN_DCBR,         "A:NA"                           ) \
   ALM_ENTRY( ALM__INVALID_MAN_RUN_DOBF,         "A:Inv Man Run DOBF"             ) \
   ALM_ENTRY( ALM__INVALID_MAN_RUN_DOBR,         "A:Inv Man Run DOBR"             ) \
   ALM_ENTRY( ALM__INVALID_MAN_RUN_HA,           "A:Inv Man Run HA"               ) \
   ALM_ENTRY( ALM__INVALID_MAN_RUN_CT_EN,        "A:Inv Man Run CT En"            ) \
   ALM_ENTRY( ALM__IDLE_DIRECTION_TIMEOUT,       "A:Idle Dir Timeout"             ) \
   ALM_ENTRY( ALM__CPLD_OFFLINE_MR,              "A:CPLD Offline MR"              ) \
   ALM_ENTRY( ALM__CPLD_OFFLINE_CT,              "A:CPLD Offline CT"              ) \
   ALM_ENTRY( ALM__CPLD_OFFLINE_COP,             "A:CPLD Offline COP"             ) \
   ALM_ENTRY( ALM__NO_DEST_STOP,                 "A:No Dest Stop"                 ) \
   ALM_ENTRY( ALM__FLOOD_SWITCH,                 "A:Flood Switch"                 ) \
   ALM_ENTRY( ALM__REMOTE_PU_BACKUP,             "A:Remote PU Backup"             ) \
   ALM_ENTRY( ALM__UNUSED131,                    "A:NA"                           ) \
   ALM_ENTRY( ALM__UNUSED132,                    "A:NA"                           ) \
   ALM_ENTRY( ALM__UNUSED133,                    "A:NA"                           ) \
   ALM_ENTRY( ALM__UNUSED134,                    "A:NA"                           ) \
   ALM_ENTRY( ALM__UNUSED135,                    "A:NA"                           ) \
   ALM_ENTRY( ALM__UNUSED136,                    "A:NA"                           ) \
   ALM_ENTRY( ALM__UNUSED137,                    "A:NA"                           ) \
   ALM_ENTRY( ALM__UNUSED138,                    "A:NA"                           ) \
   ALM_ENTRY( ALM__UNUSED139,                    "A:NA"                           ) \
   ALM_ENTRY( ALM__UNUSED140,                    "A:NA"                           ) \
   ALM_ENTRY( ALM__UNUSED141,                    "A:NA"                           ) \
   ALM_ENTRY( ALM__UNUSED142,                    "A:NA"                           ) \
   ALM_ENTRY( ALM__UNUSED143,                    "A:NA"                           ) \
   ALM_ENTRY( ALM__LWD_LOAD_LEARN,               "A:LWD Load Learn"               ) \
   ALM_ENTRY( ALM__LWD_RECALIBRATE,              "A:LWD Recalibrate"              ) \
   ALM_ENTRY( ALM__MODE_CHANGE,                  "A:Mode Changed"                 ) \
   ALM_ENTRY( ALM__RIS1_OFFLINE,                 "A:RIS1 Offline"                 ) \
   ALM_ENTRY( ALM__RIS1_UNK,                     "A:RIS1 Unk"                     ) \
   ALM_ENTRY( ALM__RIS1_POR_RST,                 "A:RIS1 POR Rst"                 ) \
   ALM_ENTRY( ALM__RIS1_WDT_RST,                 "A:RIS1 WDT Rst"                 ) \
   ALM_ENTRY( ALM__RIS1_BOD_RST,                 "A:RIS1 BOD Rst"                 ) \
   ALM_ENTRY( ALM__RIS1_GRP_COM,                 "A:RIS1 Group Net"               ) \
   ALM_ENTRY( ALM__RIS1_HALL_COM,                "A:RIS1 Hall Net"                ) \
   ALM_ENTRY( ALM__RIS1_CAR_COM,                 "A:RIS1 Car Net"                 ) \
   ALM_ENTRY( ALM__RIS1_MST_COM,                 "A:RIS1 Mst Net"                 ) \
   ALM_ENTRY( ALM__RIS1_SLV_COM,                 "A:RS1 Slv Net"                  ) \
   ALM_ENTRY( ALM__RIS1_ADDRESS,                 "A:RIS1 DIP"                     ) \
   ALM_ENTRY( ALM__RIS1_BUS_RST_1,               "A:RIS1 Bus Rst 1"               ) \
   ALM_ENTRY( ALM__RIS1_BUS_RST_2,               "A:RIS1 Bus Rst 2"               ) \
   ALM_ENTRY( ALM__RIS1_BUS_INV_MSG_1,           "A:RIS1 Inv Msg 1"               ) \
   ALM_ENTRY( ALM__RIS1_BUS_INV_MSG_2,           "A:RIS1 Inv Msg 2"               ) \
   ALM_ENTRY( ALM__RIS2_OFFLINE,                 "A:RIS2 Offline"                 ) \
   ALM_ENTRY( ALM__RIS2_UNK,                     "A:RIS2 Unk"                     ) \
   ALM_ENTRY( ALM__RIS2_POR_RST,                 "A:RIS2 POR Rst"                 ) \
   ALM_ENTRY( ALM__RIS2_WDT_RST,                 "A:RIS2 WDT Rst"                 ) \
   ALM_ENTRY( ALM__RIS2_BOD_RST,                 "A:RIS2 BOD Rst"                 ) \
   ALM_ENTRY( ALM__RIS2_GRP_COM,                 "A:RIS2 Group Net"               ) \
   ALM_ENTRY( ALM__RIS2_HALL_COM,                "A:RIS2 Hall Net"                ) \
   ALM_ENTRY( ALM__RIS2_CAR_COM,                 "A:RIS2 Car Net"                 ) \
   ALM_ENTRY( ALM__RIS2_MST_COM,                 "A:RIS2 Mst Net"                 ) \
   ALM_ENTRY( ALM__RIS2_SLV_COM,                 "A:RS1 Slv Net"                  ) \
   ALM_ENTRY( ALM__RIS2_ADDRESS,                 "A:RIS2 DIP"                     ) \
   ALM_ENTRY( ALM__RIS2_BUS_RST_1,               "A:RIS2 Bus Rst 1"               ) \
   ALM_ENTRY( ALM__RIS2_BUS_RST_2,               "A:RIS2 Bus Rst 2"               ) \
   ALM_ENTRY( ALM__RIS2_BUS_INV_MSG_1,           "A:RIS2 Inv Msg 1"               ) \
   ALM_ENTRY( ALM__RIS2_BUS_INV_MSG_2,           "A:RIS2 Inv Msg 2"               ) \
   ALM_ENTRY( ALM__RIS3_OFFLINE,                 "A:RIS3 Offline"                 ) \
   ALM_ENTRY( ALM__RIS3_UNK,                     "A:RIS3 Unk"                     ) \
   ALM_ENTRY( ALM__RIS3_POR_RST,                 "A:RIS3 POR Rst"                 ) \
   ALM_ENTRY( ALM__RIS3_WDT_RST,                 "A:RIS3 WDT Rst"                 ) \
   ALM_ENTRY( ALM__RIS3_BOD_RST,                 "A:RIS3 BOD Rst"                 ) \
   ALM_ENTRY( ALM__RIS3_GRP_COM,                 "A:RIS3 Group Net"               ) \
   ALM_ENTRY( ALM__RIS3_HALL_COM,                "A:RIS3 Hall Net"                ) \
   ALM_ENTRY( ALM__RIS3_CAR_COM,                 "A:RIS3 Car Net"                 ) \
   ALM_ENTRY( ALM__RIS3_MST_COM,                 "A:RIS3 Mst Net"                 ) \
   ALM_ENTRY( ALM__RIS3_SLV_COM,                 "A:RS1 Slv Net"                  ) \
   ALM_ENTRY( ALM__RIS3_ADDRESS,                 "A:RIS3 DIP"                     ) \
   ALM_ENTRY( ALM__RIS3_BUS_RST_1,               "A:RIS3 Bus Rst 1"               ) \
   ALM_ENTRY( ALM__RIS3_BUS_RST_2,               "A:RIS3 Bus Rst 2"               ) \
   ALM_ENTRY( ALM__RIS3_BUS_INV_MSG_1,           "A:RIS3 Inv Msg 1"               ) \
   ALM_ENTRY( ALM__RIS3_BUS_INV_MSG_2,           "A:RIS3 Inv Msg 2"               ) \
   ALM_ENTRY( ALM__RIS4_OFFLINE,                 "A:RIS4 Offline"                 ) \
   ALM_ENTRY( ALM__RIS4_UNK,                     "A:RIS4 Unk"                     ) \
   ALM_ENTRY( ALM__RIS4_POR_RST,                 "A:RIS4 POR Rst"                 ) \
   ALM_ENTRY( ALM__RIS4_WDT_RST,                 "A:RIS4 WDT Rst"                 ) \
   ALM_ENTRY( ALM__RIS4_BOD_RST,                 "A:RIS4 BOD Rst"                 ) \
   ALM_ENTRY( ALM__RIS4_GRP_COM,                 "A:RIS4 Group Net"               ) \
   ALM_ENTRY( ALM__RIS4_HALL_COM,                "A:RIS4 Hall Net"                ) \
   ALM_ENTRY( ALM__RIS4_CAR_COM,                 "A:RIS4 Car Net"                 ) \
   ALM_ENTRY( ALM__RIS4_MST_COM,                 "A:RIS4 Mst Net"                 ) \
   ALM_ENTRY( ALM__RIS4_SLV_COM,                 "A:RS1 Slv Net"                  ) \
   ALM_ENTRY( ALM__RIS4_ADDRESS,                 "A:RIS4 DIP"                     ) \
   ALM_ENTRY( ALM__RIS4_BUS_RST_1,               "A:RIS4 Bus Rst 1"               ) \
   ALM_ENTRY( ALM__RIS4_BUS_RST_2,               "A:RIS4 Bus Rst 2"               ) \
   ALM_ENTRY( ALM__RIS4_BUS_INV_MSG_1,           "A:RIS4 Inv Msg 1"               ) \
   ALM_ENTRY( ALM__RIS4_BUS_INV_MSG_2,           "A:RIS4 Inv Msg 2"               ) \
   ALM_ENTRY( ALM__DISPATCH_TIMEOUT_C1,          "A:Dispatch T/O C1"              ) \
   ALM_ENTRY( ALM__DISPATCH_TIMEOUT_C2,          "A:Dispatch T/O C2"              ) \
   ALM_ENTRY( ALM__DISPATCH_TIMEOUT_C3,          "A:Dispatch T/O C3"              ) \
   ALM_ENTRY( ALM__DISPATCH_TIMEOUT_C4,          "A:Dispatch T/O C4"              ) \
   ALM_ENTRY( ALM__DISPATCH_TIMEOUT_C5,          "A:Dispatch T/O C5"              ) \
   ALM_ENTRY( ALM__DISPATCH_TIMEOUT_C6,          "A:Dispatch T/O C6"              ) \
   ALM_ENTRY( ALM__DISPATCH_TIMEOUT_C7,          "A:Dispatch T/O C7"              ) \
   ALM_ENTRY( ALM__DISPATCH_TIMEOUT_C8,          "A:Dispatch T/O C8"              ) \
   ALM_ENTRY( ALM__DISPATCH_TIMEOUT_X1,          "A:Dispatch T/O X1"              ) \
   ALM_ENTRY( ALM__DISPATCH_TIMEOUT_X2,          "A:Dispatch T/O X2"              ) \
   ALM_ENTRY( ALM__DISPATCH_TIMEOUT_X3,          "A:Dispatch T/O X3"              ) \
   ALM_ENTRY( ALM__DISPATCH_TIMEOUT_X4,          "A:Dispatch T/O X4"              ) \
   ALM_ENTRY( ALM__DISPATCH_TIMEOUT_X5,          "A:Dispatch T/O X5"              ) \
   ALM_ENTRY( ALM__DISPATCH_TIMEOUT_X6,          "A:Dispatch T/O X6"              ) \
   ALM_ENTRY( ALM__DISPATCH_TIMEOUT_X7,          "A:Dispatch T/O X7"              ) \
   ALM_ENTRY( ALM__DISPATCH_TIMEOUT_X8,          "A:Dispatch T/O X8"              ) \
   ALM_ENTRY( ALM__XREG_OFFLINE_1,               "A:XREG Offline 1"               ) \
   ALM_ENTRY( ALM__XREG_OFFLINE_2,               "A:XREG Offline 2"               ) \
   ALM_ENTRY( ALM__XREG_OFFLINE_3,               "A:XREG Offline 3"               ) \
   ALM_ENTRY( ALM__XREG_OFFLINE_4,               "A:XREG Offline 4"               ) \
   ALM_ENTRY( ALM__XREG_OFFLINE_5,               "A:XREG Offline 5"               ) \
   ALM_ENTRY( ALM__XREG_OFFLINE_6,               "A:XREG Offline 6"               ) \
   ALM_ENTRY( ALM__XREG_OFFLINE_7,               "A:XREG Offline 7"               ) \
   ALM_ENTRY( ALM__XREG_OFFLINE_8,               "A:XREG Offline 8"               ) \
   ALM_ENTRY( ALM__UNUSED_231,                   "A:NA"                           ) \
   ALM_ENTRY( ALM__RUN_TIME_FAULT,               "A:Mod_Fault Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_FAULTAPP,            "A:Mod_FaultApp Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_HEARTBEAT,           "A:Mod_Heartbeat Runtime"        ) \
   ALM_ENTRY( ALM__RUN_TIME_WATCHDOG,            "A:Mod_Watchdog Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_INPUTS,              "A:Mod_Inputs Runtime"           ) \
   ALM_ENTRY( ALM__RUN_TIME_CAN,                 "A:Mod_CAN Runtime"              ) \
   ALM_ENTRY( ALM__RUN_TIME_UART,                "A:Mod_UART Runtime"             ) \
   ALM_ENTRY( ALM__RUN_TIME_SDATA,               "A:Mod_SDATA Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_PARAM_EEPROM,        "A:Mod_EEPROM Runtime"           ) \
   ALM_ENTRY( ALM__RUN_TIME_PARAM_APP,           "A:Mod_ParamApp Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_PARAM_MASTER,        "A:Mod_ParamMaster Runtime"      ) \
   ALM_ENTRY( ALM__RUN_TIME_PARAM_SLAVE,         "A:Mod_ParamSlave Runtime"       ) \
   ALM_ENTRY( ALM__RUN_TIME_MODE_OPER,           "A:Mod_ModeOper Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_OPER_AUTO,           "A:Mod_OperAuto Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_OPER_MANUAL,         "A:Mod_OperManual Runtime"       ) \
   ALM_ENTRY( ALM__RUN_TIME_OPER_LEARN,          "A:Mod_OperLearn Runtime"        ) \
   ALM_ENTRY( ALM__RUN_TIME_FIRE,                "A:Mod_Fire Runtime"             ) \
   ALM_ENTRY( ALM__RUN_TIME_FLOOD,               "A:Mod_flood Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_SABBATH,             "A:Mod_Sabbath Runtime"          ) \
   ALM_ENTRY( ALM__RUN_TIME_CARCALLS,            "A:Mod_CarCalls Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_DISPATCH,            "A:Mod_Dispatch Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_EARTHQUAKE,          "A:Mod_Earthquake Runtime"       ) \
   ALM_ENTRY( ALM__RUN_TIME_CAPTURE,             "A:Mod_Capture Runtime"          ) \
   ALM_ENTRY( ALM__RUN_TIME_CARTOLOBBY,          "A:Mod_CarToLobby Runtime"       ) \
   ALM_ENTRY( ALM__RUN_TIME_MAXRUNTIME,          "A:Mod_MaxRunTime Runtime"       ) \
   ALM_ENTRY( ALM__RUN_TIME_RELEVELING,          "A:Mod_Releveling Runtime"       ) \
   ALM_ENTRY( ALM__RUN_TIME_DOORS,               "A:Mod_Doors Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_INPUTMAPPING,        "A:Mod_InputMapping Runtime"     ) \
   ALM_ENTRY( ALM__RUN_TIME_OUTPUTMAPPING,       "A:Mod_OutputMapping Runtime"    ) \
   ALM_ENTRY( ALM__RUN_TIME_RTC,                 "A:Mod_RTC Runtime"              ) \
   ALM_ENTRY( ALM__RUN_TIME_ALARMAPP,            "A:Mod_AlarmApp Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_TEMPTEST,            "A:Mod_TempTest Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_POS,                 "A:Mod_Position Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_MOTION,              "A:Mod_Motion Runtime"           ) \
   ALM_ENTRY( ALM__RUN_TIME_BRAKE,               "A:Mod_Brake Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_DRIVE,               "A:Mod_Drive Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_SAFETY,              "A:Mod_Safety Runtime"           ) \
   ALM_ENTRY( ALM__RUN_TIME_FRAM,                "A:Mod_FRAM Runtime"             ) \
   ALM_ENTRY( ALM__RUN_TIME_RUNTIMER,            "A:Mod_RunTimer Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_SDATA_CTRL,          "A:Mod_SDATA_Ctrl Runtime"       ) \
   ALM_ENTRY( ALM__RUN_TIME_FPGA,                "A:Mod_FPGA Runtime"             ) \
   ALM_ENTRY( ALM__RUN_TIME_ACCEPTANCE,          "A:Mod_Acceptance Runtime"       ) \
   ALM_ENTRY( ALM__RUN_TIME_RUNLOG,              "A:Mod_Runlog Runtime"           ) \
   ALM_ENTRY( ALM__RUN_TIME_ETS,                 "A:Mod_ETS Runtime"              ) \
   ALM_ENTRY( ALM__RUN_TIME_CARCALLSECURE,       "A:Mod_CarCallSecure Runtime"    ) \
   ALM_ENTRY( ALM__RUN_TIME_EMS,                 "A:Mod_EMS Runtime"              ) \
   ALM_ENTRY( ALM__RUN_TIME_RESCUE,              "A:Mod_Rescue Runtime"           ) \
   ALM_ENTRY( ALM__MRA_MOD_45,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_46,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_47,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_48,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_49,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_50,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_51,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_52,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_53,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_54,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_55,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_56,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_57,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_58,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_59,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_60,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_61,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_62,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_63,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRA_MOD_64,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_FAULT_MRB,           "A:Mod_Fault Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_FAULTAPP_MRB,        "A:Mod_Faultapp Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_HEARTBEAT_MRB,       "A:Mod_Heartbeat Runtime"        ) \
   ALM_ENTRY( ALM__RUN_TIME_WATCHDOG_MRB,        "A:Mod_Watchdog Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_CAN_MRB,             "A:Mod_CAN Runtime"              ) \
   ALM_ENTRY( ALM__RUN_TIME_UART_MRB,            "A:Mod_UART Runtime"             ) \
   ALM_ENTRY( ALM__RUN_TIME_PARAM_EEPROM_MRB,    "A:Mod_Parameeprom Runtime"      ) \
   ALM_ENTRY( ALM__RUN_TIME_PARMAPP_MRB,         "A:Mod_ParamApp Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_PARAM_MRB,           "A:Mod_Param Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_SDATA_MRB,           "A:Mod_SDATA Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_GROUPNET,            "A:Mod_GroupNet Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_AUXNET_MRB,          "A:Mod_Auxnet Runtime"           ) \
   ALM_ENTRY( ALM__RUN_TIME_LCD,                 "A:Mod_Lcd Runtime"              ) \
   ALM_ENTRY( ALM__RUN_TIME_BUTTONS,             "A:Mod_Buttons Runtime"          ) \
   ALM_ENTRY( ALM__RUN_TIME_UI,                  "A:Mod_UI Runtime"               ) \
   ALM_ENTRY( ALM__RUN_TIME_CE,                  "A:Mod_CE Runtime"               ) \
   ALM_ENTRY( ALM__RUN_TIME_RTC_MRB,             "A:Mod_RTC Runtime"              ) \
   ALM_ENTRY( ALM__RUN_TIME_ALARMAPP_MRB,        "A:Mod_AlarmApp Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_OUTPUTS_MRB,         "A:Mod_Outputs Runtime"          ) \
   ALM_ENTRY( ALM__RUN_TIME_LW,                  "A:Mod_Lw Runtime"               ) \
   ALM_ENTRY( ALM__RUN_TIME_RUNLOG_MRB,          "A:Mod_Runlog Runtime"           ) \
   ALM_ENTRY( ALM__RUN_TIME_DISPATCHING,         "A:Mod_Dispatch Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_EPOWER_MASTER,       "A:Mod_Epower Runtime"           ) \
   ALM_ENTRY( ALM__RUN_TIME_LANTERNMONITOR,      "A:Mod_Lantern Runtime"          ) \
   ALM_ENTRY( ALM__RUN_TIME_XREG,                "A:Mod_XREG Runtime"             ) \
   ALM_ENTRY( ALM__MRB_MOD_26,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_27,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_28,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_29,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_30,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_31,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_32,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_33,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_34,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_35,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_36,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_37,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_38,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_39,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_40,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_41,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_42,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_43,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_44,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_45,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_46,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_47,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_48,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_49,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_50,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_51,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_52,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_53,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_54,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_55,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_56,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_57,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_58,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_59,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_60,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_61,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_62,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_63,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__MRB_MOD_64,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_FAULT_CTA,           "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_FAULTAPP_CTA,        "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_HEARTBEAT_CTA,       "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_WD_CTA,              "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_INPUTS_CTA,          "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_CAN_CTA,             "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_UART_CTA,            "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_SDATA_CTA,           "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_PARAMEEPROM_CTA,     "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_PARAMAPP_CTA,        "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_PARAM_CTA,           "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_PARAM_MASTER_CTA,    "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_FPGA_CTA,            "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_RTC_CTA,             "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_ALARM_APP_CTA,       "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_POS_CTA,             "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_SAFETY_CTA,          "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_17,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_18,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_19,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_20,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_21,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_22,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_23,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_24,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_25,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_26,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_27,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_28,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_29,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_30,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_31,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_32,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_33,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_34,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_35,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_36,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_37,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_38,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_39,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_40,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_41,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_42,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_43,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_44,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_45,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_46,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_47,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_48,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_49,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_50,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_51,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_52,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_53,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_54,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_55,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_56,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_57,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_58,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_59,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_60,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_61,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_62,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_63,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTA_MOD_64,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_FAULT_CTB,           "A:Mod_Fault Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_FAULTAPP_CTB,        "A:Mod_FaultApp Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_HEARTBEAT_CTB,       "A:Mod_Heartbeat Runtime"        ) \
   ALM_ENTRY( ALM__RUN_TIME_WD_CTB,              "A:Mod_WD Runtime"               ) \
   ALM_ENTRY( ALM__RUN_TIME_CAN_CTB,             "A:Mod_CAN Runtime"              ) \
   ALM_ENTRY( ALM__RUN_TIME_PARAMEEPROM_CTB,     "A:Mod_ParamEEPROM Runtime"      ) \
   ALM_ENTRY( ALM__RUN_TIME_PARAMAPP_CTB,        "A:Mod_ParamApp Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_PARAMCTB,            "A:Mod_Param Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_UART_CTB,            "A:Mod_UART Runtime"             ) \
   ALM_ENTRY( ALM__RUN_TIME_SDATA_CTB,           "A:Mod_SDATA Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_POS_CTB,             "A:Mod_Position Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_LCD_CTB,             "A:Mod_LCD Runtime"              ) \
   ALM_ENTRY( ALM__RUN_TIME_BUTTONS_CTB,         "A:Mod_Buttons Runtime"          ) \
   ALM_ENTRY( ALM__RUN_TIME_UI_CTB,              "A:Mod_UI Runtime"               ) \
   ALM_ENTRY( ALM__RUN_TIME_NTS,                 "A:Mod_NTS Runtime"              ) \
   ALM_ENTRY( ALM__RUN_TIME_RTC_CTB,             "A:Mod_RTC Runtime"              ) \
   ALM_ENTRY( ALM__RUN_TIME_ALARM_CTB,           "A:Mod_Alarm Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_OUTPUTS_CTB,         "A:Mod_Outputs Runtime"          ) \
   ALM_ENTRY( ALM__RUN_TIME_AUXNET_CTB,          "A:Mod_Auxnet Runtime"           ) \
   ALM_ENTRY( ALM__RUN_TIME_LW_CTB,              "A:Mod_LW Runtime"               ) \
   ALM_ENTRY( ALM__RUN_TIME_CE_CTB,              "A:Mod_CE Runtime"               ) \
   ALM_ENTRY( ALM__CTB_MOD_22,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_23,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_24,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_25,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_26,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_27,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_28,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_29,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_30,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_31,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_32,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_33,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_34,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_35,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_36,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_37,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_38,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_39,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_40,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_41,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_42,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_43,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_44,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_45,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_46,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_47,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_48,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_49,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_50,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_51,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_52,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_53,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_54,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_55,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_56,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_57,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_58,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_59,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_60,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_61,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_62,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_63,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CTB_MOD_64,                   "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_FAULT_COPA,          "A:Mod_Fault Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_FAULTAPP_COPA,       "A:Mod_Faultapp Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_HEARTBEAT_COPA,      "A:Mod_Heartbeat Runtime"        ) \
   ALM_ENTRY( ALM__RUN_TIME_WD_COPA,             "A:Mod_WD Runtime"               ) \
   ALM_ENTRY( ALM__RUN_TIME_INPUTS_COPA,         "A:Mod_Inputs Runtime"           ) \
   ALM_ENTRY( ALM__RUN_TIME_CAN_COPA,            "A:Mod_CAN Runtime"              ) \
   ALM_ENTRY( ALM__RUN_TIME_UART_COPA,           "A:Mod_UART Runtime"             ) \
   ALM_ENTRY( ALM__RUN_TIME_SDATA_COPA,          "A:Mod_SDATA Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_PARAMEEPORM_COPA,    "A:Mod_ParamEEPROM Runtime"      ) \
   ALM_ENTRY( ALM__RUN_TIME_PARAMAPP_COPA,       "A:Mod_ParamApp Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_ALARM_COPA,          "A:Mod_Alarm Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_RTC_COPA,            "A:Mod_RTC Runtime"              ) \
   ALM_ENTRY( ALM__RUN_TIME_FPGA_COPA,           "A:Mod_FPGA Runtime"             ) \
   ALM_ENTRY( ALM__RUN_TIME_SAFETY_COPA,         "A:Mod_Safety Runtime"           ) \
   ALM_ENTRY( ALM__RUN_TIME_POS_COPA,            "A:Mod_Pos Runtime"              ) \
   ALM_ENTRY( ALM__COPA_MOD_16,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_17,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_18,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_19,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_20,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_21,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_22,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_23,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_24,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_25,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_26,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_27,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_28,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_29,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_30,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_31,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_32,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_33,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_34,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_35,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_36,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_37,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_38,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_39,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_40,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_41,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_42,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_43,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_44,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_45,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_46,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_47,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_48,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_49,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_50,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_51,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_52,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_53,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_54,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_55,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_56,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_57,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_58,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_59,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_60,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_61,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_62,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_63,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPA_MOD_64,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__RUN_TIME_FAULT_COPB,          "A:Mod_Fault Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_FAULTAPP_COPB,       "A:Mod_FaultApp Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_HEARTBEAT_COPB,      "A:Mod_Heartbeat Runtime"        ) \
   ALM_ENTRY( ALM__RUN_TIME_WD_COPB,             "A:Mod_WD Runtime"               ) \
   ALM_ENTRY( ALM__RUN_TIME_CAN_COPB,            "A:Mod_CAN Runtime"              ) \
   ALM_ENTRY( ALM__RUN_TIME_PARAMEEPROM_COPB,    "A:Mod_ParamEEPROM Runtime"      ) \
   ALM_ENTRY( ALM__RUN_TIME_PARAMAPP_COPB,       "A:Mod_ParamApp Runtime"         ) \
   ALM_ENTRY( ALM__RUN_TIME_PARAMC_COPB,         "A:Mod_Param Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_UART_COPB,           "A:Mod_UART_Runtime"             ) \
   ALM_ENTRY( ALM__RUN_TIME_SDATA_COPB,          "A:Mod_SDATA Runtime"            ) \
   ALM_ENTRY( ALM__RUN_TIME_LCD_COPB,            "A:Mod_LCD Runtime"              ) \
   ALM_ENTRY( ALM__RUN_TIME_BUTTONS_COPB,        "A:Mod_Buttons Runtime"          ) \
   ALM_ENTRY( ALM__RUN_TIME_UI_COPB,             "A:Mod_UI Runtime"               ) \
   ALM_ENTRY( ALM__RUN_TIME_RTC_COPB,            "A:Mod_RTC Runtime"              ) \
   ALM_ENTRY( ALM__RUIN_TIME_ALARM_COPB,         "A:Mod_Alarm Ruintime"           ) \
   ALM_ENTRY( ALM__RUN_TIME_OUTPUTS_COPB,        "A:Mod_Outputs Runtime"          ) \
   ALM_ENTRY( ALM__UNUSED572,                    "A:N/A"                          ) \
   ALM_ENTRY( ALM__RUN_TIME_AUXNET_COPB,         "A:Mod_Auxnet Runtime"           ) \
   ALM_ENTRY( ALM__RUN_TIME_CE_COPB,             "A:Mod_CE Runtime"               ) \
   ALM_ENTRY( ALM__COPB_MOD_20,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_21,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_22,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_23,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_24,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_25,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_26,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_27,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_28,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_29,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_30,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_31,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_32,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_33,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_34,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_35,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_36,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_37,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_38,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_39,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_40,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_41,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_42,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_43,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_44,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_45,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_46,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_47,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_48,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_49,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_50,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_51,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_52,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_53,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_54,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_55,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_56,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_57,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_58,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_59,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_60,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_61,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_62,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_63,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__COPB_MOD_64,                  "A:INVALID Mod Alarm"            ) \
   ALM_ENTRY( ALM__CAR_OFFLINE_1,                "A:Car Offline 1"                ) \
   ALM_ENTRY( ALM__CAR_OFFLINE_2,                "A:Car Offline 2"                ) \
   ALM_ENTRY( ALM__CAR_OFFLINE_3,                "A:Car Offline 3"                ) \
   ALM_ENTRY( ALM__CAR_OFFLINE_4,                "A:Car Offline 4"                ) \
   ALM_ENTRY( ALM__CAR_OFFLINE_5,                "A:Car Offline 5"                ) \
   ALM_ENTRY( ALM__CAR_OFFLINE_6,                "A:Car Offline 6"                ) \
   ALM_ENTRY( ALM__CAR_OFFLINE_7,                "A:Car Offline 7"                ) \
   ALM_ENTRY( ALM__CAR_OFFLINE_8,                "A:Car Offline 8"                ) \
   ALM_ENTRY( ALM__DDM_OFFLINE,                  "A:DDM Offline"                  ) \
   ALM_ENTRY( ALM__DOOR_OPEN_TEST,               "A:Door Open In Motion"          ) \
   ALM_ENTRY( ALM__FRAM_REDUNDANCY,              "A:FRAM Redundancy"              ) \
   ALM_ENTRY( ALM__DO_DURING_RUN,                "A:DO During Run"                ) \
   ALM_ENTRY( ALM__IN_DEST_DZ_DURING_RUN,        "A:In Dest DZ During Run"        ) \
   ALM_ENTRY( ALM__DUPLICATE_MR_501,             "A:Dupl. MR 501"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_MR_502,             "A:Dupl. MR 502"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_MR_503,             "A:Dupl. MR 503"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_MR_504,             "A:Dupl. MR 504"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_MR_505,             "A:Dupl. MR 505"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_MR_506,             "A:Dupl. MR 506"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_MR_507,             "A:Dupl. MR 507"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_MR_508,             "A:Dupl. MR 508"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_501,             "A:Dupl. CT 501"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_502,             "A:Dupl. CT 502"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_503,             "A:Dupl. CT 503"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_504,             "A:Dupl. CT 504"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_505,             "A:Dupl. CT 505"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_506,             "A:Dupl. CT 506"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_507,             "A:Dupl. CT 507"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_508,             "A:Dupl. CT 508"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_509,             "A:Dupl. CT 509"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_510,             "A:Dupl. CT 510"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_511,             "A:Dupl. CT 511"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_512,             "A:Dupl. CT 512"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_513,             "A:Dupl. CT 513"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_514,             "A:Dupl. CT 514"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_515,             "A:Dupl. CT 515"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_516,             "A:Dupl. CT 516"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_501,            "A:Dupl. COP 501"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_502,            "A:Dupl. COP 502"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_503,            "A:Dupl. COP 503"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_504,            "A:Dupl. COP 504"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_505,            "A:Dupl. COP 505"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_506,            "A:Dupl. COP 506"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_507,            "A:Dupl. COP 507"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_508,            "A:Dupl. COP 508"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_509,            "A:Dupl. COP 509"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_510,            "A:Dupl. COP 510"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_511,            "A:Dupl. COP 511"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_512,            "A:Dupl. COP 512"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_513,            "A:Dupl. COP 513"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_514,            "A:Dupl. COP 514"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_515,            "A:Dupl. COP 515"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_516,            "A:Dupl. COP 516"                ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS1_501,           "A:Dupl. RIS1 501"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS1_502,           "A:Dupl. RIS1 502"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS1_503,           "A:Dupl. RIS1 503"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS1_504,           "A:Dupl. RIS1 504"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS1_505,           "A:Dupl. RIS1 505"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS1_506,           "A:Dupl. RIS1 506"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS1_507,           "A:Dupl. RIS1 507"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS1_508,           "A:Dupl. RIS1 508"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS2_501,           "A:Dupl. RIS2 501"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS2_502,           "A:Dupl. RIS2 502"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS2_503,           "A:Dupl. RIS2 503"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS2_504,           "A:Dupl. RIS2 504"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS2_505,           "A:Dupl. RIS2 505"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS2_506,           "A:Dupl. RIS2 506"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS2_507,           "A:Dupl. RIS2 507"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS2_508,           "A:Dupl. RIS2 508"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS3_501,           "A:Dupl. RIS3 501"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS3_502,           "A:Dupl. RIS3 502"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS3_503,           "A:Dupl. RIS3 503"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS3_504,           "A:Dupl. RIS3 504"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS3_505,           "A:Dupl. RIS3 505"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS3_506,           "A:Dupl. RIS3 506"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS3_507,           "A:Dupl. RIS3 507"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS3_508,           "A:Dupl. RIS3 508"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS4_501,           "A:Dupl. RIS4 501"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS4_502,           "A:Dupl. RIS4 502"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS4_503,           "A:Dupl. RIS4 503"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS4_504,           "A:Dupl. RIS4 504"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS4_505,           "A:Dupl. RIS4 505"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS4_506,           "A:Dupl. RIS4 506"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS4_507,           "A:Dupl. RIS4 507"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS4_508,           "A:Dupl. RIS4 508"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP1_501,           "A:Dupl. EXP1 501"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP1_502,           "A:Dupl. EXP1 502"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP1_503,           "A:Dupl. EXP1 503"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP1_504,           "A:Dupl. EXP1 504"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP1_505,           "A:Dupl. EXP1 505"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP1_506,           "A:Dupl. EXP1 506"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP1_507,           "A:Dupl. EXP1 507"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP1_508,           "A:Dupl. EXP1 508"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP2_501,           "A:Dupl. EXP2 501"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP2_502,           "A:Dupl. EXP2 502"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP2_503,           "A:Dupl. EXP2 503"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP2_504,           "A:Dupl. EXP2 504"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP2_505,           "A:Dupl. EXP2 505"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP2_506,           "A:Dupl. EXP2 506"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP2_507,           "A:Dupl. EXP2 507"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP2_508,           "A:Dupl. EXP2 508"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP3_501,           "A:Dupl. EXP3 501"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP3_502,           "A:Dupl. EXP3 502"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP3_503,           "A:Dupl. EXP3 503"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP3_504,           "A:Dupl. EXP3 504"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP3_505,           "A:Dupl. EXP3 505"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP3_506,           "A:Dupl. EXP3 506"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP3_507,           "A:Dupl. EXP3 507"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP3_508,           "A:Dupl. EXP3 508"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP4_501,           "A:Dupl. EXP4 501"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP4_502,           "A:Dupl. EXP4 502"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP4_503,           "A:Dupl. EXP4 503"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP4_504,           "A:Dupl. EXP4 504"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP4_505,           "A:Dupl. EXP4 505"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP4_506,           "A:Dupl. EXP4 506"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP4_507,           "A:Dupl. EXP4 507"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP4_508,           "A:Dupl. EXP4 508"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP5_501,           "A:Dupl. EXP5 501"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP5_502,           "A:Dupl. EXP5 502"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP5_503,           "A:Dupl. EXP5 503"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP5_504,           "A:Dupl. EXP5 504"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP5_505,           "A:Dupl. EXP5 505"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP5_506,           "A:Dupl. EXP5 506"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP5_507,           "A:Dupl. EXP5 507"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP5_508,           "A:Dupl. EXP5 508"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP6_501,           "A:Dupl. EXP6 501"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP6_502,           "A:Dupl. EXP6 502"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP6_503,           "A:Dupl. EXP6 503"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP6_504,           "A:Dupl. EXP6 504"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP6_505,           "A:Dupl. EXP6 505"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP6_506,           "A:Dupl. EXP6 506"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP6_507,           "A:Dupl. EXP6 507"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP6_508,           "A:Dupl. EXP6 508"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP7_501,           "A:Dupl. EXP7 501"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP7_502,           "A:Dupl. EXP7 502"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP7_503,           "A:Dupl. EXP7 503"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP7_504,           "A:Dupl. EXP7 504"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP7_505,           "A:Dupl. EXP7 505"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP7_506,           "A:Dupl. EXP7 506"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP7_507,           "A:Dupl. EXP7 507"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP7_508,           "A:Dupl. EXP7 508"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP8_501,           "A:Dupl. EXP8 501"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP8_502,           "A:Dupl. EXP8 502"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP8_503,           "A:Dupl. EXP8 503"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP8_504,           "A:Dupl. EXP8 504"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP8_505,           "A:Dupl. EXP8 505"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP8_506,           "A:Dupl. EXP8 506"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP8_507,           "A:Dupl. EXP8 507"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP8_508,           "A:Dupl. EXP8 508"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP9_501,           "A:Dupl. EXP9 501"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP9_502,           "A:Dupl. EXP9 502"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP9_503,           "A:Dupl. EXP9 503"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP9_504,           "A:Dupl. EXP9 504"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP9_505,           "A:Dupl. EXP9 505"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP9_506,           "A:Dupl. EXP9 506"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP9_507,           "A:Dupl. EXP9 507"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP9_508,           "A:Dupl. EXP9 508"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP10_501,          "A:Dupl. EXP10 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP10_502,          "A:Dupl. EXP10 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP10_503,          "A:Dupl. EXP10 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP10_504,          "A:Dupl. EXP10 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP10_505,          "A:Dupl. EXP10 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP10_506,          "A:Dupl. EXP10 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP10_507,          "A:Dupl. EXP10 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP10_508,          "A:Dupl. EXP10 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP11_501,          "A:Dupl. EXP11 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP11_502,          "A:Dupl. EXP11 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP11_503,          "A:Dupl. EXP11 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP11_504,          "A:Dupl. EXP11 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP11_505,          "A:Dupl. EXP11 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP11_506,          "A:Dupl. EXP11 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP11_507,          "A:Dupl. EXP11 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP11_508,          "A:Dupl. EXP11 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP12_501,          "A:Dupl. EXP12 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP12_502,          "A:Dupl. EXP12 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP12_503,          "A:Dupl. EXP12 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP12_504,          "A:Dupl. EXP12 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP12_505,          "A:Dupl. EXP12 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP12_506,          "A:Dupl. EXP12 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP12_507,          "A:Dupl. EXP12 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP12_508,          "A:Dupl. EXP12 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP13_501,          "A:Dupl. EXP13 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP13_502,          "A:Dupl. EXP13 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP13_503,          "A:Dupl. EXP13 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP13_504,          "A:Dupl. EXP13 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP13_505,          "A:Dupl. EXP13 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP13_506,          "A:Dupl. EXP13 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP13_507,          "A:Dupl. EXP13 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP13_508,          "A:Dupl. EXP13 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP14_501,          "A:Dupl. EXP14 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP14_502,          "A:Dupl. EXP14 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP14_503,          "A:Dupl. EXP14 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP14_504,          "A:Dupl. EXP14 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP14_505,          "A:Dupl. EXP14 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP14_506,          "A:Dupl. EXP14 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP14_507,          "A:Dupl. EXP14 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP14_508,          "A:Dupl. EXP14 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP15_501,          "A:Dupl. EXP15 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP15_502,          "A:Dupl. EXP15 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP15_503,          "A:Dupl. EXP15 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP15_504,          "A:Dupl. EXP15 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP15_505,          "A:Dupl. EXP15 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP15_506,          "A:Dupl. EXP15 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP15_507,          "A:Dupl. EXP15 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP15_508,          "A:Dupl. EXP15 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP16_501,          "A:Dupl. EXP16 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP16_502,          "A:Dupl. EXP16 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP16_503,          "A:Dupl. EXP16 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP16_504,          "A:Dupl. EXP16 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP16_505,          "A:Dupl. EXP16 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP16_506,          "A:Dupl. EXP16 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP16_507,          "A:Dupl. EXP16 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP16_508,          "A:Dupl. EXP16 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP17_501,          "A:Dupl. EXP17 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP17_502,          "A:Dupl. EXP17 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP17_503,          "A:Dupl. EXP17 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP17_504,          "A:Dupl. EXP17 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP17_505,          "A:Dupl. EXP17 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP17_506,          "A:Dupl. EXP17 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP17_507,          "A:Dupl. EXP17 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP17_508,          "A:Dupl. EXP17 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP18_501,          "A:Dupl. EXP18 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP18_502,          "A:Dupl. EXP18 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP18_503,          "A:Dupl. EXP18 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP18_504,          "A:Dupl. EXP18 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP18_505,          "A:Dupl. EXP18 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP18_506,          "A:Dupl. EXP18 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP18_507,          "A:Dupl. EXP18 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP18_508,          "A:Dupl. EXP18 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP19_501,          "A:Dupl. EXP19 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP19_502,          "A:Dupl. EXP19 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP19_503,          "A:Dupl. EXP19 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP19_504,          "A:Dupl. EXP19 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP19_505,          "A:Dupl. EXP19 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP19_506,          "A:Dupl. EXP19 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP19_507,          "A:Dupl. EXP19 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP19_508,          "A:Dupl. EXP19 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP20_501,          "A:Dupl. EXP20 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP20_502,          "A:Dupl. EXP20 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP20_503,          "A:Dupl. EXP20 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP20_504,          "A:Dupl. EXP20 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP20_505,          "A:Dupl. EXP20 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP20_506,          "A:Dupl. EXP20 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP20_507,          "A:Dupl. EXP20 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP20_508,          "A:Dupl. EXP20 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP21_501,          "A:Dupl. EXP21 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP21_502,          "A:Dupl. EXP21 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP21_503,          "A:Dupl. EXP21 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP21_504,          "A:Dupl. EXP21 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP21_505,          "A:Dupl. EXP21 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP21_506,          "A:Dupl. EXP21 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP21_507,          "A:Dupl. EXP21 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP21_508,          "A:Dupl. EXP21 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP22_501,          "A:Dupl. EXP22 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP22_502,          "A:Dupl. EXP22 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP22_503,          "A:Dupl. EXP22 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP22_504,          "A:Dupl. EXP22 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP22_505,          "A:Dupl. EXP22 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP22_506,          "A:Dupl. EXP22 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP22_507,          "A:Dupl. EXP22 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP22_508,          "A:Dupl. EXP22 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP23_501,          "A:Dupl. EXP23 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP23_502,          "A:Dupl. EXP23 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP23_503,          "A:Dupl. EXP23 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP23_504,          "A:Dupl. EXP23 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP23_505,          "A:Dupl. EXP23 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP23_506,          "A:Dupl. EXP23 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP23_507,          "A:Dupl. EXP23 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP23_508,          "A:Dupl. EXP23 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP24_501,          "A:Dupl. EXP24 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP24_502,          "A:Dupl. EXP24 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP24_503,          "A:Dupl. EXP24 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP24_504,          "A:Dupl. EXP24 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP24_505,          "A:Dupl. EXP24 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP24_506,          "A:Dupl. EXP24 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP24_507,          "A:Dupl. EXP24 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP24_508,          "A:Dupl. EXP24 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP25_501,          "A:Dupl. EXP25 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP25_502,          "A:Dupl. EXP25 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP25_503,          "A:Dupl. EXP25 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP25_504,          "A:Dupl. EXP25 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP25_505,          "A:Dupl. EXP25 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP25_506,          "A:Dupl. EXP25 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP25_507,          "A:Dupl. EXP25 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP25_508,          "A:Dupl. EXP25 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP26_501,          "A:Dupl. EXP26 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP26_502,          "A:Dupl. EXP26 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP26_503,          "A:Dupl. EXP26 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP26_504,          "A:Dupl. EXP26 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP26_505,          "A:Dupl. EXP26 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP26_506,          "A:Dupl. EXP26 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP26_507,          "A:Dupl. EXP26 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP26_508,          "A:Dupl. EXP26 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP27_501,          "A:Dupl. EXP27 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP27_502,          "A:Dupl. EXP27 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP27_503,          "A:Dupl. EXP27 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP27_504,          "A:Dupl. EXP27 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP27_505,          "A:Dupl. EXP27 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP27_506,          "A:Dupl. EXP27 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP27_507,          "A:Dupl. EXP27 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP27_508,          "A:Dupl. EXP27 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP28_501,          "A:Dupl. EXP28 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP28_502,          "A:Dupl. EXP28 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP28_503,          "A:Dupl. EXP28 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP28_504,          "A:Dupl. EXP28 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP28_505,          "A:Dupl. EXP28 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP28_506,          "A:Dupl. EXP28 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP28_507,          "A:Dupl. EXP28 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP28_508,          "A:Dupl. EXP28 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP29_501,          "A:Dupl. EXP29 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP29_502,          "A:Dupl. EXP29 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP29_503,          "A:Dupl. EXP29 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP29_504,          "A:Dupl. EXP29 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP29_505,          "A:Dupl. EXP29 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP29_506,          "A:Dupl. EXP29 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP29_507,          "A:Dupl. EXP29 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP29_508,          "A:Dupl. EXP29 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP30_501,          "A:Dupl. EXP30 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP30_502,          "A:Dupl. EXP30 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP30_503,          "A:Dupl. EXP30 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP30_504,          "A:Dupl. EXP30 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP30_505,          "A:Dupl. EXP30 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP30_506,          "A:Dupl. EXP30 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP30_507,          "A:Dupl. EXP30 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP30_508,          "A:Dupl. EXP30 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP31_501,          "A:Dupl. EXP31 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP31_502,          "A:Dupl. EXP31 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP31_503,          "A:Dupl. EXP31 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP31_504,          "A:Dupl. EXP31 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP31_505,          "A:Dupl. EXP31 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP31_506,          "A:Dupl. EXP31 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP31_507,          "A:Dupl. EXP31 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP31_508,          "A:Dupl. EXP31 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP32_501,          "A:Dupl. EXP32 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP32_502,          "A:Dupl. EXP32 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP32_503,          "A:Dupl. EXP32 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP32_504,          "A:Dupl. EXP32 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP32_505,          "A:Dupl. EXP32 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP32_506,          "A:Dupl. EXP32 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP32_507,          "A:Dupl. EXP32 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP32_508,          "A:Dupl. EXP32 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP33_501,          "A:Dupl. EXP33 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP33_502,          "A:Dupl. EXP33 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP33_503,          "A:Dupl. EXP33 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP33_504,          "A:Dupl. EXP33 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP33_505,          "A:Dupl. EXP33 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP33_506,          "A:Dupl. EXP33 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP33_507,          "A:Dupl. EXP33 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP33_508,          "A:Dupl. EXP33 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP34_501,          "A:Dupl. EXP34 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP34_502,          "A:Dupl. EXP34 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP34_503,          "A:Dupl. EXP34 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP34_504,          "A:Dupl. EXP34 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP34_505,          "A:Dupl. EXP34 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP34_506,          "A:Dupl. EXP34 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP34_507,          "A:Dupl. EXP34 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP34_508,          "A:Dupl. EXP34 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP35_501,          "A:Dupl. EXP35 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP35_502,          "A:Dupl. EXP35 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP35_503,          "A:Dupl. EXP35 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP35_504,          "A:Dupl. EXP35 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP35_505,          "A:Dupl. EXP35 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP35_506,          "A:Dupl. EXP35 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP35_507,          "A:Dupl. EXP35 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP35_508,          "A:Dupl. EXP35 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP36_501,          "A:Dupl. EXP36 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP36_502,          "A:Dupl. EXP36 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP36_503,          "A:Dupl. EXP36 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP36_504,          "A:Dupl. EXP36 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP36_505,          "A:Dupl. EXP36 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP36_506,          "A:Dupl. EXP36 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP36_507,          "A:Dupl. EXP36 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP36_508,          "A:Dupl. EXP36 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP37_501,          "A:Dupl. EXP37 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP37_502,          "A:Dupl. EXP37 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP37_503,          "A:Dupl. EXP37 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP37_504,          "A:Dupl. EXP37 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP37_505,          "A:Dupl. EXP37 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP37_506,          "A:Dupl. EXP37 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP37_507,          "A:Dupl. EXP37 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP37_508,          "A:Dupl. EXP37 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP38_501,          "A:Dupl. EXP38 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP38_502,          "A:Dupl. EXP38 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP38_503,          "A:Dupl. EXP38 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP38_504,          "A:Dupl. EXP38 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP38_505,          "A:Dupl. EXP38 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP38_506,          "A:Dupl. EXP38 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP38_507,          "A:Dupl. EXP38 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP38_508,          "A:Dupl. EXP38 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP39_501,          "A:Dupl. EXP39 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP39_502,          "A:Dupl. EXP39 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP39_503,          "A:Dupl. EXP39 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP39_504,          "A:Dupl. EXP39 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP39_505,          "A:Dupl. EXP39 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP39_506,          "A:Dupl. EXP39 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP39_507,          "A:Dupl. EXP39 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP39_508,          "A:Dupl. EXP39 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP40_501,          "A:Dupl. EXP40 501"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP40_502,          "A:Dupl. EXP40 502"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP40_503,          "A:Dupl. EXP40 503"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP40_504,          "A:Dupl. EXP40 504"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP40_505,          "A:Dupl. EXP40 505"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP40_506,          "A:Dupl. EXP40 506"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP40_507,          "A:Dupl. EXP40 507"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP40_508,          "A:Dupl. EXP40 508"              ) \
   ALM_ENTRY( ALM__DUPLICATE_MR_601,             "A:Dupl. MR 601"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_MR_602,             "A:Dupl. MR 602"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_MR_603,             "A:Dupl. MR 603"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_MR_604,             "A:Dupl. MR 604"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_MR_605,             "A:Dupl. MR 605"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_MR_606,             "A:Dupl. MR 606"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_MR_607,             "A:Dupl. MR 607"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_MR_608,             "A:Dupl. MR 608"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_601,             "A:Dupl. CT 601"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_602,             "A:Dupl. CT 602"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_603,             "A:Dupl. CT 603"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_604,             "A:Dupl. CT 604"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_605,             "A:Dupl. CT 605"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_606,             "A:Dupl. CT 606"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_607,             "A:Dupl. CT 607"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_608,             "A:Dupl. CT 608"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_609,             "A:Dupl. CT 609"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_610,             "A:Dupl. CT 610"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_611,             "A:Dupl. CT 611"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_612,             "A:Dupl. CT 612"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_613,             "A:Dupl. CT 613"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_614,             "A:Dupl. CT 614"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_615,             "A:Dupl. CT 615"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_CT_616,             "A:Dupl. CT 616"                 ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_601,            "A:Dupl. COP 601"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_602,            "A:Dupl. COP 602"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_603,            "A:Dupl. COP 603"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_604,            "A:Dupl. COP 604"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_605,            "A:Dupl. COP 605"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_606,            "A:Dupl. COP 606"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_607,            "A:Dupl. COP 607"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_608,            "A:Dupl. COP 608"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_609,            "A:Dupl. COP 609"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_610,            "A:Dupl. COP 610"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_611,            "A:Dupl. COP 611"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_612,            "A:Dupl. COP 612"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_613,            "A:Dupl. COP 613"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_614,            "A:Dupl. COP 614"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_615,            "A:Dupl. COP 615"                ) \
   ALM_ENTRY( ALM__DUPLICATE_COP_616,            "A:Dupl. COP 616"                ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS1_601,           "A:Dupl. RIS1 601"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS1_602,           "A:Dupl. RIS1 602"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS1_603,           "A:Dupl. RIS1 603"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS1_604,           "A:Dupl. RIS1 604"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS1_605,           "A:Dupl. RIS1 605"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS1_606,           "A:Dupl. RIS1 606"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS1_607,           "A:Dupl. RIS1 607"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS1_608,           "A:Dupl. RIS1 608"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS2_601,           "A:Dupl. RIS2 601"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS2_602,           "A:Dupl. RIS2 602"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS2_603,           "A:Dupl. RIS2 603"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS2_604,           "A:Dupl. RIS2 604"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS2_605,           "A:Dupl. RIS2 605"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS2_606,           "A:Dupl. RIS2 606"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS2_607,           "A:Dupl. RIS2 607"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS2_608,           "A:Dupl. RIS2 608"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS3_601,           "A:Dupl. RIS3 601"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS3_602,           "A:Dupl. RIS3 602"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS3_603,           "A:Dupl. RIS3 603"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS3_604,           "A:Dupl. RIS3 604"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS3_605,           "A:Dupl. RIS3 605"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS3_606,           "A:Dupl. RIS3 606"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS3_607,           "A:Dupl. RIS3 607"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS3_608,           "A:Dupl. RIS3 608"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS4_601,           "A:Dupl. RIS4 601"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS4_602,           "A:Dupl. RIS4 602"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS4_603,           "A:Dupl. RIS4 603"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS4_604,           "A:Dupl. RIS4 604"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS4_605,           "A:Dupl. RIS4 605"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS4_606,           "A:Dupl. RIS4 606"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS4_607,           "A:Dupl. RIS4 607"               ) \
   ALM_ENTRY( ALM__DUPLICATE_RIS4_608,           "A:Dupl. RIS4 608"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP1_601,           "A:Dupl. EXP1 601"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP1_602,           "A:Dupl. EXP1 602"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP1_603,           "A:Dupl. EXP1 603"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP1_604,           "A:Dupl. EXP1 604"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP1_605,           "A:Dupl. EXP1 605"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP1_606,           "A:Dupl. EXP1 606"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP1_607,           "A:Dupl. EXP1 607"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP1_608,           "A:Dupl. EXP1 608"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP2_601,           "A:Dupl. EXP2 601"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP2_602,           "A:Dupl. EXP2 602"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP2_603,           "A:Dupl. EXP2 603"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP2_604,           "A:Dupl. EXP2 604"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP2_605,           "A:Dupl. EXP2 605"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP2_606,           "A:Dupl. EXP2 606"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP2_607,           "A:Dupl. EXP2 607"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP2_608,           "A:Dupl. EXP2 608"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP3_601,           "A:Dupl. EXP3 601"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP3_602,           "A:Dupl. EXP3 602"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP3_603,           "A:Dupl. EXP3 603"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP3_604,           "A:Dupl. EXP3 604"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP3_605,           "A:Dupl. EXP3 605"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP3_606,           "A:Dupl. EXP3 606"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP3_607,           "A:Dupl. EXP3 607"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP3_608,           "A:Dupl. EXP3 608"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP4_601,           "A:Dupl. EXP4 601"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP4_602,           "A:Dupl. EXP4 602"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP4_603,           "A:Dupl. EXP4 603"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP4_604,           "A:Dupl. EXP4 604"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP4_605,           "A:Dupl. EXP4 605"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP4_606,           "A:Dupl. EXP4 606"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP4_607,           "A:Dupl. EXP4 607"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP4_608,           "A:Dupl. EXP4 608"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP5_601,           "A:Dupl. EXP5 601"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP5_602,           "A:Dupl. EXP5 602"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP5_603,           "A:Dupl. EXP5 603"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP5_604,           "A:Dupl. EXP5 604"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP5_605,           "A:Dupl. EXP5 605"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP5_606,           "A:Dupl. EXP5 606"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP5_607,           "A:Dupl. EXP5 607"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP5_608,           "A:Dupl. EXP5 608"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP6_601,           "A:Dupl. EXP6 601"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP6_602,           "A:Dupl. EXP6 602"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP6_603,           "A:Dupl. EXP6 603"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP6_604,           "A:Dupl. EXP6 604"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP6_605,           "A:Dupl. EXP6 605"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP6_606,           "A:Dupl. EXP6 606"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP6_607,           "A:Dupl. EXP6 607"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP6_608,           "A:Dupl. EXP6 608"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP7_601,           "A:Dupl. EXP7 601"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP7_602,           "A:Dupl. EXP7 602"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP7_603,           "A:Dupl. EXP7 603"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP7_604,           "A:Dupl. EXP7 604"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP7_605,           "A:Dupl. EXP7 605"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP7_606,           "A:Dupl. EXP7 606"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP7_607,           "A:Dupl. EXP7 607"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP7_608,           "A:Dupl. EXP7 608"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP8_601,           "A:Dupl. EXP8 601"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP8_602,           "A:Dupl. EXP8 602"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP8_603,           "A:Dupl. EXP8 603"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP8_604,           "A:Dupl. EXP8 604"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP8_605,           "A:Dupl. EXP8 605"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP8_606,           "A:Dupl. EXP8 606"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP8_607,           "A:Dupl. EXP8 607"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP8_608,           "A:Dupl. EXP8 608"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP9_601,           "A:Dupl. EXP9 601"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP9_602,           "A:Dupl. EXP9 602"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP9_603,           "A:Dupl. EXP9 603"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP9_604,           "A:Dupl. EXP9 604"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP9_605,           "A:Dupl. EXP9 605"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP9_606,           "A:Dupl. EXP9 606"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP9_607,           "A:Dupl. EXP9 607"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP9_608,           "A:Dupl. EXP9 608"               ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP10_601,          "A:Dupl. EXP10 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP10_602,          "A:Dupl. EXP10 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP10_603,          "A:Dupl. EXP10 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP10_604,          "A:Dupl. EXP10 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP10_605,          "A:Dupl. EXP10 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP10_606,          "A:Dupl. EXP10 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP10_607,          "A:Dupl. EXP10 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP10_608,          "A:Dupl. EXP10 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP11_601,          "A:Dupl. EXP11 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP11_602,          "A:Dupl. EXP11 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP11_603,          "A:Dupl. EXP11 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP11_604,          "A:Dupl. EXP11 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP11_605,          "A:Dupl. EXP11 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP11_606,          "A:Dupl. EXP11 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP11_607,          "A:Dupl. EXP11 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP11_608,          "A:Dupl. EXP11 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP12_601,          "A:Dupl. EXP12 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP12_602,          "A:Dupl. EXP12 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP12_603,          "A:Dupl. EXP12 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP12_604,          "A:Dupl. EXP12 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP12_605,          "A:Dupl. EXP12 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP12_606,          "A:Dupl. EXP12 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP12_607,          "A:Dupl. EXP12 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP12_608,          "A:Dupl. EXP12 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP13_601,          "A:Dupl. EXP13 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP13_602,          "A:Dupl. EXP13 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP13_603,          "A:Dupl. EXP13 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP13_604,          "A:Dupl. EXP13 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP13_605,          "A:Dupl. EXP13 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP13_606,          "A:Dupl. EXP13 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP13_607,          "A:Dupl. EXP13 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP13_608,          "A:Dupl. EXP13 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP14_601,          "A:Dupl. EXP14 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP14_602,          "A:Dupl. EXP14 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP14_603,          "A:Dupl. EXP14 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP14_604,          "A:Dupl. EXP14 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP14_605,          "A:Dupl. EXP14 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP14_606,          "A:Dupl. EXP14 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP14_607,          "A:Dupl. EXP14 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP14_608,          "A:Dupl. EXP14 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP15_601,          "A:Dupl. EXP15 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP15_602,          "A:Dupl. EXP15 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP15_603,          "A:Dupl. EXP15 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP15_604,          "A:Dupl. EXP15 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP15_605,          "A:Dupl. EXP15 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP15_606,          "A:Dupl. EXP15 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP15_607,          "A:Dupl. EXP15 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP15_608,          "A:Dupl. EXP15 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP16_601,          "A:Dupl. EXP16 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP16_602,          "A:Dupl. EXP16 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP16_603,          "A:Dupl. EXP16 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP16_604,          "A:Dupl. EXP16 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP16_605,          "A:Dupl. EXP16 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP16_606,          "A:Dupl. EXP16 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP16_607,          "A:Dupl. EXP16 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP16_608,          "A:Dupl. EXP16 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP17_601,          "A:Dupl. EXP17 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP17_602,          "A:Dupl. EXP17 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP17_603,          "A:Dupl. EXP17 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP17_604,          "A:Dupl. EXP17 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP17_605,          "A:Dupl. EXP17 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP17_606,          "A:Dupl. EXP17 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP17_607,          "A:Dupl. EXP17 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP17_608,          "A:Dupl. EXP17 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP18_601,          "A:Dupl. EXP18 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP18_602,          "A:Dupl. EXP18 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP18_603,          "A:Dupl. EXP18 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP18_604,          "A:Dupl. EXP18 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP18_605,          "A:Dupl. EXP18 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP18_606,          "A:Dupl. EXP18 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP18_607,          "A:Dupl. EXP18 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP18_608,          "A:Dupl. EXP18 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP19_601,          "A:Dupl. EXP19 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP19_602,          "A:Dupl. EXP19 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP19_603,          "A:Dupl. EXP19 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP19_604,          "A:Dupl. EXP19 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP19_605,          "A:Dupl. EXP19 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP19_606,          "A:Dupl. EXP19 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP19_607,          "A:Dupl. EXP19 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP19_608,          "A:Dupl. EXP19 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP20_601,          "A:Dupl. EXP20 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP20_602,          "A:Dupl. EXP20 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP20_603,          "A:Dupl. EXP20 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP20_604,          "A:Dupl. EXP20 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP20_605,          "A:Dupl. EXP20 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP20_606,          "A:Dupl. EXP20 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP20_607,          "A:Dupl. EXP20 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP20_608,          "A:Dupl. EXP20 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP21_601,          "A:Dupl. EXP21 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP21_602,          "A:Dupl. EXP21 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP21_603,          "A:Dupl. EXP21 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP21_604,          "A:Dupl. EXP21 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP21_605,          "A:Dupl. EXP21 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP21_606,          "A:Dupl. EXP21 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP21_607,          "A:Dupl. EXP21 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP21_608,          "A:Dupl. EXP21 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP22_601,          "A:Dupl. EXP22 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP22_602,          "A:Dupl. EXP22 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP22_603,          "A:Dupl. EXP22 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP22_604,          "A:Dupl. EXP22 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP22_605,          "A:Dupl. EXP22 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP22_606,          "A:Dupl. EXP22 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP22_607,          "A:Dupl. EXP22 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP22_608,          "A:Dupl. EXP22 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP23_601,          "A:Dupl. EXP23 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP23_602,          "A:Dupl. EXP23 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP23_603,          "A:Dupl. EXP23 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP23_604,          "A:Dupl. EXP23 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP23_605,          "A:Dupl. EXP23 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP23_606,          "A:Dupl. EXP23 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP23_607,          "A:Dupl. EXP23 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP23_608,          "A:Dupl. EXP23 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP24_601,          "A:Dupl. EXP24 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP24_602,          "A:Dupl. EXP24 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP24_603,          "A:Dupl. EXP24 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP24_604,          "A:Dupl. EXP24 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP24_605,          "A:Dupl. EXP24 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP24_606,          "A:Dupl. EXP24 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP24_607,          "A:Dupl. EXP24 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP24_608,          "A:Dupl. EXP24 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP25_601,          "A:Dupl. EXP25 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP25_602,          "A:Dupl. EXP25 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP25_603,          "A:Dupl. EXP25 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP25_604,          "A:Dupl. EXP25 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP25_605,          "A:Dupl. EXP25 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP25_606,          "A:Dupl. EXP25 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP25_607,          "A:Dupl. EXP25 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP25_608,          "A:Dupl. EXP25 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP26_601,          "A:Dupl. EXP26 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP26_602,          "A:Dupl. EXP26 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP26_603,          "A:Dupl. EXP26 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP26_604,          "A:Dupl. EXP26 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP26_605,          "A:Dupl. EXP26 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP26_606,          "A:Dupl. EXP26 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP26_607,          "A:Dupl. EXP26 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP26_608,          "A:Dupl. EXP26 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP27_601,          "A:Dupl. EXP27 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP27_602,          "A:Dupl. EXP27 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP27_603,          "A:Dupl. EXP27 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP27_604,          "A:Dupl. EXP27 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP27_605,          "A:Dupl. EXP27 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP27_606,          "A:Dupl. EXP27 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP27_607,          "A:Dupl. EXP27 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP27_608,          "A:Dupl. EXP27 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP28_601,          "A:Dupl. EXP28 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP28_602,          "A:Dupl. EXP28 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP28_603,          "A:Dupl. EXP28 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP28_604,          "A:Dupl. EXP28 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP28_605,          "A:Dupl. EXP28 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP28_606,          "A:Dupl. EXP28 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP28_607,          "A:Dupl. EXP28 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP28_608,          "A:Dupl. EXP28 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP29_601,          "A:Dupl. EXP29 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP29_602,          "A:Dupl. EXP29 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP29_603,          "A:Dupl. EXP29 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP29_604,          "A:Dupl. EXP29 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP29_605,          "A:Dupl. EXP29 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP29_606,          "A:Dupl. EXP29 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP29_607,          "A:Dupl. EXP29 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP29_608,          "A:Dupl. EXP29 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP30_601,          "A:Dupl. EXP30 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP30_602,          "A:Dupl. EXP30 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP30_603,          "A:Dupl. EXP30 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP30_604,          "A:Dupl. EXP30 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP30_605,          "A:Dupl. EXP30 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP30_606,          "A:Dupl. EXP30 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP30_607,          "A:Dupl. EXP30 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP30_608,          "A:Dupl. EXP30 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP31_601,          "A:Dupl. EXP31 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP31_602,          "A:Dupl. EXP31 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP31_603,          "A:Dupl. EXP31 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP31_604,          "A:Dupl. EXP31 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP31_605,          "A:Dupl. EXP31 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP31_606,          "A:Dupl. EXP31 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP31_607,          "A:Dupl. EXP31 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP31_608,          "A:Dupl. EXP31 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP32_601,          "A:Dupl. EXP32 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP32_602,          "A:Dupl. EXP32 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP32_603,          "A:Dupl. EXP32 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP32_604,          "A:Dupl. EXP32 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP32_605,          "A:Dupl. EXP32 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP32_606,          "A:Dupl. EXP32 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP32_607,          "A:Dupl. EXP32 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP32_608,          "A:Dupl. EXP32 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP33_601,          "A:Dupl. EXP33 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP33_602,          "A:Dupl. EXP33 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP33_603,          "A:Dupl. EXP33 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP33_604,          "A:Dupl. EXP33 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP33_605,          "A:Dupl. EXP33 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP33_606,          "A:Dupl. EXP33 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP33_607,          "A:Dupl. EXP33 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP33_608,          "A:Dupl. EXP33 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP34_601,          "A:Dupl. EXP34 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP34_602,          "A:Dupl. EXP34 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP34_603,          "A:Dupl. EXP34 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP34_604,          "A:Dupl. EXP34 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP34_605,          "A:Dupl. EXP34 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP34_606,          "A:Dupl. EXP34 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP34_607,          "A:Dupl. EXP34 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP34_608,          "A:Dupl. EXP34 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP35_601,          "A:Dupl. EXP35 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP35_602,          "A:Dupl. EXP35 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP35_603,          "A:Dupl. EXP35 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP35_604,          "A:Dupl. EXP35 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP35_605,          "A:Dupl. EXP35 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP35_606,          "A:Dupl. EXP35 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP35_607,          "A:Dupl. EXP35 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP35_608,          "A:Dupl. EXP35 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP36_601,          "A:Dupl. EXP36 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP36_602,          "A:Dupl. EXP36 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP36_603,          "A:Dupl. EXP36 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP36_604,          "A:Dupl. EXP36 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP36_605,          "A:Dupl. EXP36 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP36_606,          "A:Dupl. EXP36 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP36_607,          "A:Dupl. EXP36 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP36_608,          "A:Dupl. EXP36 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP37_601,          "A:Dupl. EXP37 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP37_602,          "A:Dupl. EXP37 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP37_603,          "A:Dupl. EXP37 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP37_604,          "A:Dupl. EXP37 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP37_605,          "A:Dupl. EXP37 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP37_606,          "A:Dupl. EXP37 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP37_607,          "A:Dupl. EXP37 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP37_608,          "A:Dupl. EXP37 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP38_601,          "A:Dupl. EXP38 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP38_602,          "A:Dupl. EXP38 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP38_603,          "A:Dupl. EXP38 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP38_604,          "A:Dupl. EXP38 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP38_605,          "A:Dupl. EXP38 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP38_606,          "A:Dupl. EXP38 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP38_607,          "A:Dupl. EXP38 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP38_608,          "A:Dupl. EXP38 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP39_601,          "A:Dupl. EXP39 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP39_602,          "A:Dupl. EXP39 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP39_603,          "A:Dupl. EXP39 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP39_604,          "A:Dupl. EXP39 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP39_605,          "A:Dupl. EXP39 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP39_606,          "A:Dupl. EXP39 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP39_607,          "A:Dupl. EXP39 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP39_608,          "A:Dupl. EXP39 608"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP40_601,          "A:Dupl. EXP40 601"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP40_602,          "A:Dupl. EXP40 602"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP40_603,          "A:Dupl. EXP40 603"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP40_604,          "A:Dupl. EXP40 604"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP40_605,          "A:Dupl. EXP40 605"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP40_606,          "A:Dupl. EXP40 606"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP40_607,          "A:Dupl. EXP40 607"              ) \
   ALM_ENTRY( ALM__DUPLICATE_EXP40_608,          "A:Dupl. EXP40 608"              ) \
   ALM_ENTRY( ALM__LWD_OFFLINE,                  "A:LWD Offline"                  ) \
   ALM_ENTRY( ALM__DL20_OFFLINE_CT,              "A:DL20 Offline CT"              ) \
   ALM_ENTRY( ALM__DL20_OFFLINE_COP,             "A:DL20 Offline COP"             ) \
   ALM_ENTRY( ALM__CPLD_OVF_MR,                  "A:CPLD OVF MR"                  ) \
   ALM_ENTRY( ALM__CPLD_OVF_CT,                  "A:CPLD OVF CT"                  ) \
   ALM_ENTRY( ALM__CPLD_OVF_COP,                 "A:CPLD OVF COP"                 ) \
   ALM_ENTRY( ALM__FIRE_KEY_MAIN,                "A:Fire Key Main"                ) \
   ALM_ENTRY( ALM__FIRE_KEY_REMOTE,              "A:Fire Key Remote"              ) \
   ALM_ENTRY( ALM__FIRE_SMOKE_MAIN,              "A:Fire Smoke Main"              ) \
   ALM_ENTRY( ALM__FIRE_SMOKE_ALT,               "A:Fire Smoke Alt"               ) \
   ALM_ENTRY( ALM__FIRE_SMOKE_MR,                "A:Fire Smoke MR"                ) \
   ALM_ENTRY( ALM__FIRE_SMOKE_HA,                "A:Fire Smoke HA"                ) \
   ALM_ENTRY( ALM__FIRE_SMOKE_SAVED,             "A:Fire Smoke Latched"           ) \
   ALM_ENTRY( ALM__FIRE_SMOKE_PIT,               "A:Fire Smoke Pit"               ) \
   ALM_ENTRY( ALM__FIRE_SMOKE_MR_2,              "A:Fire Smoke MR 2"              ) \
   ALM_ENTRY( ALM__FIRE_SMOKE_HA_2,              "A:Fire Smoke HA 2"              ) \
   ALM_ENTRY( ALM__NEED_TO_RESET_MR,             "A:NEED TO RST MR"               ) \
   ALM_ENTRY( ALM__NEED_TO_RESET_CT,             "A:NEED TO RST CT"               ) \
   ALM_ENTRY( ALM__NEED_TO_RESET_COP,            "A:NEED TO RST COP"              ) \
   ALM_ENTRY( ALM__UNINTENDED_MOV_TEST_ACTIVE,   "A:Unint. Mov. Test Active"      ) \
   ALM_ENTRY( ALM__DUPAR_COP_OFFLINE,            "A:Dupar COP Offline"            ) \
   ALM_ENTRY( ALM__RIS1_HB_OFFLINE,              "A:RIS1 HB Offline"              ) \
   ALM_ENTRY( ALM__RIS2_HB_OFFLINE,              "A:RIS2 HB Offline"              ) \
   ALM_ENTRY( ALM__RIS3_HB_OFFLINE,              "A:RIS3 HB Offline"              ) \
   ALM_ENTRY( ALM__RIS4_HB_OFFLINE,              "A:RIS4 HB Offline"              ) \
   ALM_ENTRY( ALM__SHIELD_UNKNOWN,               "A:Shield Unknown"               ) \
   ALM_ENTRY( ALM__SHIELD_POR_RESET,             "A:Shield POR Rst"               ) \
   ALM_ENTRY( ALM__SHIELD_BOD_RESET,             "A:Shield BOD Rst"               ) \
   ALM_ENTRY( ALM__SHIELD_WDT_RESET,             "A:Shield WDT Rst"               ) \
   ALM_ENTRY( ALM__SHIELD_COM_GROUP,             "A:Shield COM Group"             ) \
   ALM_ENTRY( ALM__SHIELD_COM_RPI,               "A:Shield COM RPi"               ) \
   ALM_ENTRY( ALM__SHIELD_FAILED_RTC,            "A:Shield Failed RTC"            ) \
   ALM_ENTRY( ALM__SHIELD_UART_OVF_TX,           "A:Shield UART OVF TX"           ) \
   ALM_ENTRY( ALM__SHIELD_UART_OVF_RX,           "A:Shield UART OVF RX"           ) \
   ALM_ENTRY( ALM__SHIELD_CAN_OVF_TX,            "A:Shield CAN OVF TX"            ) \
   ALM_ENTRY( ALM__SHIELD_CAN_OVF_RX,            "A:Shield CAN OVF RX"            ) \
   ALM_ENTRY( ALM__SHIELD_CAN_BUS_RST,           "A:Shield CAN Bus Rst"           ) \
   ALM_ENTRY( ALM__VIP_TIMEOUT,                  "A:VIP Timeout"                  ) \
   ALM_ENTRY( ALM__FIRE_VIRTUAL_REMOTE_RECALL,   "A:Fire Virtual Remote Recall"   ) \
   ALM_ENTRY( ALM__EMS2_NOT_AT_RECALL,           "A:EMS2 Not At Recall"           )

/* Macros for accessing table elements */
#define EXPAND_ALARM_TABLE_AS_ENUMERATION(A, B) A,
#define EXPAND_ALARM_TABLE_AS_STRING_ARRAY(A, B) B,

#endif