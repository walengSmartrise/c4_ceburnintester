/*
 * watchdog.h
 *
 *  Created on: May 22, 2016
 *      Author: sean
 */

#ifndef WATCHDOG_H_
#define WATCHDOG_H_

void watchdog_feed();

void watchdog_init();


#endif /* WATCHDOG_H_ */
