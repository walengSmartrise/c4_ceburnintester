#ifndef OPERATION_H_
#define OPERATION_H_

#include "sys.h"
#define INVALID_FLOOR (255)
enum en_mocmd
{
   MOCMD__EMERG_STOP,
   MOCMD__NORMAL_STOP,
   MOCMD__QUICK_STOP,
   MOCMD__RUN_UP,
   MOCMD__RUN_DN,
   MOCMD__GOTO_DEST,

   NUM_MOCMD
};
enum en_mode_learn
{
   MODE_L__UNKNOWN,
   MODE_L__INVALID,
   MODE_L__NONE,
   MODE_L__GOTO_TERM_HA,
   MODE_L__UNUSED4,
   MODE_L__UNUSED5,
   MODE_L__BYP_TERM_LIMIT_HA,
   MODE_L__READY_HA,
   MODE_L__UNUSED8,
   MODE_L__UNUSED9,
   MODE_L__LEARNING_UHA,
   MODE_L__LEARNING_DHA,
   MODE_L__UNUSED12,
   MODE_L__UNUSED_LEARNING_DNTS,
   MODE_L__COMPLETE,

   NUM_MODE_LEARN
};

enum en_mode_manual
{
   MODE_M__UNKNOWN,
   MODE_M__INVALID,
   MODE_M__NONE,
   MODE_M__INSP_CT,
   MODE_M__INSP_IC,
   MODE_M__INSP_HA,
   MODE_M__INSP_MR,
   MODE_M__INSP_PIT,
   MODE_M__INSP_LND,
   MODE_M__CONSTRUCTION,
   MODE_M__INSP_HA_TOP,
   MODE_M__INSP_HA_BOTTOM,

   NUM_MODE_MANUAL
};

enum en_mode_auto
{
   MODE_A__UNKNOWN,
   MODE_A__NONE,
   MODE_A__NORMAL,
   MODE_A__FIRE1,
   MODE_A__FIRE2,
   MODE_A__EMS1,
   MODE_A__EMS2,
   MODE_A__ATTENDANT,
   MODE_A__INDP_SRV,
   MODE_A__SEISMC,
   MODE_A__CW_DRAIL,
   MODE_A__SABBATH,
   MODE_A__EPOWER,
   MODE_A__EVAC,
   MODE_A__OOS,
   MODE_A__BATT_LOW, // TODO replace unused
   MODE_A__BATT_RESQ,
   MODE_A__PRSN_TRSPT1,
   MODE_A__PRSN_TRSPT2,
   MODE_A__UNUSED19,
   MODE_A__WANDERGUARD,
   MODE_A__HUGS,
   MODE_A__CAR_SW,
   MODE_A__TEST,
   MODE_A__WIND_OP,
   MODE_A__FLOOD_OP,
   MODE_A__SWING,
   MODE_A__CUSTOM,
   MODE_A__ACTIVE_SHOOTER_MODE,
   MODE_A__MARSHAL_MODE,
   MODE_A__VIP_MODE,
   MODE_A__EQ_HOISTWAY_SCAN,

   NUM_MODE_AUTO
};

enum en_classop
{
   CLASSOP__UNKNOWN,
   CLASSOP__MANUAL,
   CLASSOP__SEMI_AUTO,
   CLASSOP__AUTO,

   NUM_CLASSOP
};

struct st_operation
{
   enum en_classop eClassOfOperation;
   enum en_mocmd eCommand;
   enum en_mode_learn eLearnMode;
   enum en_mode_auto eAutoMode;
   enum en_mode_manual eManualMode;
   uint32_t ulRequestedDest;
   uint32_t ulPosLimit_UP;
   uint32_t ulPosLimit_DN;
   uint16_t uwSpeedLimit;
   uint8_t ucCurrentFloor;
   uint8_t ucDestinationFloor;
   uint8_t bBypTermLimits;
};

enum en_classop GetOperation_ClassOfOp( void );
enum en_mocmd GetOperation_MotionCmd( void );
enum en_mode_learn GetOperation_LearnMode( void );
enum en_mode_auto GetOperation_AutoMode( void );
enum en_mode_manual GetOperation_ManualMode( void );
uint32_t GetOperation_RequestedDestination( void );
uint32_t GetOperation_PositionLimit_UP( void );
uint32_t GetOperation_PositionLimit_DN( void );
uint16_t GetOperation_SpeedLimit( void );
uint8_t GetOperation_CurrentFloor( void );
uint8_t GetOperation_DestinationFloor( void );
uint8_t GetOperation_BypassTermLimits( void );

void SetOperation_ClassOfOp( enum en_classop     );
void SetOperation_MotionCmd( enum en_mocmd       );
void SetOperation_LearnMode( enum en_mode_learn  );
void SetOperation_AutoMode( enum en_mode_auto   );
void SetOperation_ManualMode( enum en_mode_manual );
void SetOperation_RequestedDestination( uint32_t );
void SetOperation_PositionLimit_UP( uint32_t );
void SetOperation_PositionLimit_DN( uint32_t );
void SetOperation_SpeedLimit( uint16_t );
void SetOperation_CurrentFloor( uint8_t  );
void SetOperation_DestinationFloor( uint8_t  );
void SetOperation_BypassTermLimits( uint8_t  );

#endif
