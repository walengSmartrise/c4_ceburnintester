/******************************************************************************
 *
 * @file     param_edit_protocol.h
 * @brief    Header file for MR SRU deployment
 * @version  V1.00
 * @date     28, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef PARAM_EDIT_PROTOCOL_H
#define PARAM_EDIT_PROTOCOL_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "sys.h"
#include "sys_drive.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define PEP_BACKUP_CHUNKS_PER_BLOCK       (11)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   PEP__1Bit,
   PEP__8Bit,
   PEP__16Bit,
   PEP__24Bit,
   PEP__32Bit,
   PEP__Magnetek,
   PEP__KEB,
   PEP__Backup,

   NUM_PEP = 16 // MAX
}en_param_edit_protocol;

typedef struct
{
   en_drive_commands eCommand;
   uint16_t uwID;
   int32_t iValue;

   uint16_t uwCountdown_ms;
} st_drive_edit_cmd;
typedef struct
{
   uint8_t bWriteBit;
   en_param_edit_protocol eProtocol;
   uint8_t ucBlockIndex;// For PEP__Backup only
   uint8_t ucChunkIndex;// For PEP__Backup only
   uint16_t uwParamIndex;
   uint32_t uiData;
} st_param_edit_req;
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void PEP_SetCRCDirtyBit( uint8_t ucBlockIndex );
void PEP_ClrCRCDirtyBit( uint8_t ucBlockIndex );
uint8_t PEP_GetCRCDirtyBit( uint8_t ucBlockIndex );
void PEP_ClearCRCResendCounter( uint8_t ucBlockIndex );
void PEP_UpdateCRCDirtyBits( uint16_t uwRunPeriod_ms );

void GetEditDriveParameterCommand( st_drive_edit_cmd * pstDriveEditCmd );
uint8_t SetEditDriveParameterCommand( st_drive_edit_cmd * pstDriveEditCmd, uint8_t bSuppressCounterRefresh );
void UpdateEditDriveParameterRequest( struct st_module *pstThisModule );

uint8_t UnloadDatagram_ParamEditRequest( un_sdata_datagram *punDatagram, uint8_t bEnableWrite );
uint8_t LoadDatagram_ParamEditResponse( un_sdata_datagram *punDatagram );

void LoadDatagram_ParamEditRequest( un_sdata_datagram *punDatagram, st_param_edit_req *pstParamEditReq );
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
