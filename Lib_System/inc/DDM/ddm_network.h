/******************************************************************************
 *
 * @file     network.h
 * @brief    Network header files
 * @version  V1.00
 * @date     15, Sept 2016
 *
 * @note   For network, nodes, and datagram definitions
 *
 ******************************************************************************/

#ifndef DDM_NETWORK_H
#define DDM_NETWORK_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "chip.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   Nodes
 *----------------------------------------------------------------------------*/
typedef enum
{
   DDM_NODE__A,
   DDM_NODE__B,

   NUM_DDM_NODES,

   DDM_NODE__UNK,
}en_ddm_nodes;

/*----------------------------------------------------------------------------
   Datagram Definitions
 *----------------------------------------------------------------------------*/
typedef enum
{
   /* KEEP IN ORDER - START */
   DG_DDMA__ParamSlaveRequest,
   DG_DDMA__Alarm,
   /* KEEP IN ORDER - END */
   DG_DDMA__ExpInputs1,
   DG_DDMA__ExpInputs2,
   DG_DDMA__ExpInputs3,
   DG_DDMA__ExpInputs4,
   DG_DDMA__ExpInputs5,
   DG_DDMA__ExpStatus1,
   DG_DDMA__ExpStatus2,
   DG_DDMA__ExpStatus3,
   DG_DDMA__ExpStatus4,
   DG_DDMA__ExpStatus5,
   DG_DDMA__Debug,

   NUM_DDMA_DATAGRAMS
} en_ddma_datagrams;

typedef enum
{
   /* KEEP IN ORDER - START */
   DG_DDMB__ParamMaster_ParamValues,
   DG_DDMB__ParamMaster_BlockCRCs1,
   DG_DDMB__ParamMaster_BlockCRCs2,
   DG_DDMB__ParamMaster_BlockCRCs3,
   DG_DDMB__ParamMaster_BlockCRCs4,
   DG_DDMB__ParamMaster_BlockCRCs5,
   DG_DDMB__ParamMaster_BlockCRCs6,
   DG_DDMB__ParamMaster_BlockCRCs7,
   DG_DDMB__ParamMaster_BlockCRCs8,
   DG_DDMB__ParamMaster_BlockCRCs9,
   DG_DDMB__ParamMaster_BlockCRCs10,
   DG_DDMB__ParamMaster_BlockCRCs11,
   DG_DDMB__ParamMaster_BlockCRCs12,
   DG_DDMB__ParamMaster_BlockCRCs13,
   DG_DDMB__ParamMaster_BlockCRCs14,
   DG_DDMB__ParamMaster_BlockCRCs15,
   DG_DDMB__ParamMaster_BlockCRCs16,
   DG_DDMB__ParamMaster_BlockCRCs17,
   DG_DDMB__ParamMaster_BlockCRCs18,
   DG_DDMB__ParamMaster_BlockCRCs19,
   DG_DDMB__ParamMaster_BlockCRCs20,
   DG_DDMB__ParamMaster_BlockCRCs21,
   DG_DDMB__ParamMaster_BlockCRCs22,
   DG_DDMB__ParamMaster_BlockCRCs23,
   DG_DDMB__ParamMaster_BlockCRCs24,
   DG_DDMB__ParamMaster_BlockCRCs25,
   DG_DDMB__ParamMaster_BlockCRCs26,
   DG_DDMB__ParamMaster_BlockCRCs27,
   DG_DDMB__ParamMaster_BlockCRCs28,
   DG_DDMB__ParamMaster_BlockCRCs29,
   DG_DDMB__ParamMaster_BlockCRCs30,
   DG_DDMB__ParamMaster_BlockCRCs31,
   DG_DDMB__ParamMaster_BlockCRCs32,
   DG_DDMB__ParamMaster_BlockCRCs33,
   DG_DDMB__ParamMaster_BlockCRCs34,
   DG_DDMB__ParamMaster_BlockCRCs35,
   DG_DDMB__ParamMaster_BlockCRCs36,
   DG_DDMB__ParamMaster_BlockCRCs37,
   DG_DDMB__ParamMaster_BlockCRCs38,
   DG_DDMB__ParamMaster_BlockCRCs39,
   DG_DDMB__ParamMaster_BlockCRCs40,
   DG_DDMB__ParamMaster_BlockCRCs41,
   DG_DDMB__ParamMaster_BlockCRCs42,
   DG_DDMB__ParamMaster_BlockCRCs43,
   DG_DDMB__ParamMaster_BlockCRCs44,
   DG_DDMB__ParamMaster_BlockCRCs45,
   DG_DDMB__ParamMaster_BlockCRCs46,
   DG_DDMB__ParamMaster_BlockCRCs47,
   DG_DDMB__ParamMaster_BlockCRCs48,
   DG_DDMB__ParamMaster_BlockCRCs49,
   DG_DDMB__ParamMaster_BlockCRCs50,
   DG_DDMB__ParamMaster_BlockCRCs51,
   DG_DDMB__ParamMaster_BlockCRCs52,
   DG_DDMB__ParamMaster_BlockCRCs53,
   DG_DDMB__ParamMaster_BlockCRCs54,
   DG_DDMB__ParamMaster_BlockCRCs55,
   DG_DDMB__ParamMaster_BlockCRCs56,
   DG_DDMB__ParamMaster_BlockCRCs57,
   DG_DDMB__ParamMaster_BlockCRCs58,
   DG_DDMB__ParamMaster_BlockCRCs59,
   DG_DDMB__ParamMaster_BlockCRCs60,
   DG_DDMB__Alarm,
   /* KEEP IN ORDER - END */
   DG_DDMB__SyncTime,
   DG_DDMB__Debug,

   NUM_DDMB_DATAGRAMS
} en_ddmb_datagrams;

/*----------------------------------------------------------------------------
   Datagrams to expansion master boards sent in order to maintain communication
 *----------------------------------------------------------------------------*/
typedef enum
{
   DG_DDMA_TO_EXP__ExpOutputs1,
   DG_DDMA_TO_EXP__ExpOutputs2,
   DG_DDMA_TO_EXP__ExpOutputs3,
   DG_DDMA_TO_EXP__ExpOutputs4,
   DG_DDMA_TO_EXP__ExpOutputs5,

   NUM_DDMA_TO_EXP_DATAGRAMS
} en_ddma_to_exp_datagrams;

/*----------------------------------------------------------------------------

   SData Network Definitions

 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/


#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
1 *----------------------------------------------------------------------------*/


