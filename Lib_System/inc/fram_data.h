/******************************************************************************
 *
 * @file     fram_data.h
 * @brief    FRAM data files
 * @version  V1.00
 * @date     15, Sept 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef FRAM_DATA_H
#define FRAM_DATA_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "chip.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
//Bit order of emergency bitmap stored in fram on MRA board
enum en_emergency_bitmap //Max 32
{
   EmergencyBF_FirePhaseI_RecallToAltFloor,
   EmergencyBF_FirePhaseI_FlashFireHat,
   EmergencyBF_FirePhaseI_ArmReset,
   EmergencyBF_FirePhaseI_Active,
   EmergencyBF_FirePhaseII_Active,
   EmergencyBF_CW_Recall_Complete,
   EmergencyBF_CW_Derail_InProgress,
   EmergencyBF_EQ_Seismic_InProgress,

   //todo str
   EmergencyBF_EMS_Phase1_Active,// car has recalled to floor atleast once
   EmergencyBF_EMS_Phase2_Active,

   EmergencyBF_EQ_AutoMode_InProgress,

   Num_EmergenyBF
};

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/


#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
1 *----------------------------------------------------------------------------*/
