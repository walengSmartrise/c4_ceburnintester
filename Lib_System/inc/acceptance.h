#ifndef ACCEPTANCE_SMARTRISE
#define ACCEPTANCE_SMARTRISE

#include "acceptance_FSM.h"
#include <stdint.h>


#define NTS_COUNT_OFFSET (400)

typedef enum
{
   NO_ACTIVE_ACCEPTANCE_TEST,
   ACCEPTANCE_TEST_NTS,
   ACCEPTANCE_TEST_ETS,
   ACCEPTANCE_TEST_ETSL,
   ACCEPTANCE_TEST_TOP_LIMIT,
   ACCEPTANCE_TEST_BOTTOM_LIMIT,
   ACCEPTANCE_TEST_REDUNDANCY_CPLD,
   ACCEPTANCE_TEST_REDUNDANCY_MCU,
   ACCEPTANCE_TEST_ASC_OR_DESC_OVERSPEED,
   ACCEPTANCE_TEST_INSPECTION_SPEED,
   ACCEPTANCE_TEST_LEVELING_SPEED,
   ACCEPTANCE_TEST_BOTTOM_FINAL_LIMIT,
   ACCEPTANCE_TEST_TOP_FINAL_LIMIT,
   ACCEPTANCE_TEST_COUNTERWEIGHT_BUFFER,
   ACCEPTANCE_TEST_CAR_BUFFER,
   ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_FRONT,
   ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_REAR,
   ACCEPTANCE_TEST_BRAKEBOARD_FEEDBACK,
   ACCEPTANCE_TEST_SLIDE_DISTANCE,
   ACCEPTANCE_TEST_EBRAKE_SLIDE,
   NUM_ACCEPTANCE_TEST
} AcceptanceTest;

typedef enum
{
    ACCEPTANCE_TEST_NOT_RUN,
    ACCEPTANCE_TEST_PASS,
    ACCEPTANCE_TEST_FAIL,
    ACCEPTANCE_TEST_IN_PROGRESS,
    ACCEPTANCE_TEST_INCOMPLETE,
    ACCEPTANCE_TEST_COMPLETE,
    NUM_ACCEPTANCE_TEST_STATUS
}AcceptanceTestStatus;

AcceptanceTestStatus GetAcceptanceTestStatus(AcceptanceTest);
void SetAcceptanceTestStatus(AcceptanceTest, AcceptanceTestStatus);
uint8_t GetActiveAcceptanceTestState( void );

AcceptanceTest GetActiveAcceptanceTest( void );
void SetActiveAcceptanceTest( AcceptanceTest );

uint16_t GetAcceptance_SlideDistance( void );


// Function for each acceptance test
uint8_t AcceptanceNTS( void );
uint8_t AcceptanceETS( void );
uint8_t AcceptanceETSL( void );
uint8_t AcceptanceTopLimit( void );
uint8_t AcceptanceBottomLimit( void );
uint8_t AcceptanceRedundancyCPLD( void );
uint8_t AcceptanceRedundancyMCU( void );
uint8_t AcceptanceAscOrDescOverspeed(void);
uint8_t AcceptanceCarOverspeed(void);
uint8_t AcceptanceInspectionSpeed(void);
uint8_t AcceptanceLevelingSpeed(void);
uint8_t AcceptanceBottomFinalLimit(void);
uint8_t AcceptanceTopFinalLimit(void);
uint8_t AcceptanceCounterweightBuffer(void);
uint8_t AcceptanceCarBuffer(void);
uint8_t AcceptanceUnintendedMovementFront(void);
uint8_t AcceptanceUnintendedMovementRear(void);
uint8_t AcceptanceOverspeed( void );
uint8_t AcceptanceBrakeBoardFeedback( void );
uint8_t AcceptanceSlideDistance( void );
uint8_t AcceptanceEBrkSlide( void );

void ResetAcceptanceNTS( void );
void ResetAcceptanceETS( void );
void ResetAcceptanceETSL( void );
void ResetAcceptanceTopLimit( void );
void ResetAcceptanceBottomLimit( void );
void ResetAcceptanceRedundancyCPLD( void );
void ResetAcceptanceRedundancyMCU( void );
void ResetAcceptanceAscOrDescOverspeed(void);
void ResetAcceptanceCarOverspeed(void);
void ResetAcceptanceInspectionSpeed(void);
void ResetAcceptanceLevelingSpeed(void);
void ResetAcceptanceBottomFinalLimit(void);
void ResetAcceptanceTopFinalLimit(void);
void ResetAcceptanceCounterweightBuffer(void);
void ResetAcceptanceCarBuffer(void);
void ResetAcceptanceUnintendedMovementFront(void);
void ResetAcceptanceUnintendedMovementRear(void);
void ResetAcceptanceOverspeed( void );
void ResetAcceptanceBrakeBoardFeedback( void );
void ResetAcceptanceSlideDistance( void );
void ResetAcceptanceEBrkSlide( void );

#endif
