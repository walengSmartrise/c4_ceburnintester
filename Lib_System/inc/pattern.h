#ifndef _PATTERN_H_
#define _PATTERN_H_

#include "motion.h"
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define MAX_PATTERN_SIZE_BYTES                (2000)
/* Parameter Bounds. Match with decel.h */
#define MAX_JERK                    (25.0f)           /* ft/s3 */
#define MIN_JERK                    (0.3f)            /* ft/s3 */
#define MAX_ACCEL                   (8.0f)            /* ft/s2 */
#define MIN_ACCEL                   (1.0f)            /* ft/s2 */
#define MAX_JERK_X10                (MAX_JERK*10.0f)  /* 0.1 ft/s3 */
#define MIN_JERK_X10                (MIN_JERK*10.0f)  /* 0.1 ft/s3 */
#define MAX_ACCEL_X10               (MAX_ACCEL*10.0f) /* 0.1 ft/s2 */
#define MIN_ACCEL_X10               (MIN_ACCEL*10.0f) /* 0.1 ft/s2 */
#define MAX_LEVELING_DISTANCE_5MM   (TWO_FEET/10)     /* x5mm */

#define MIN_PATTERN_RESOLUTION__MS  (5)
#define MAX_PATTERN_RESOLUTION__MS  (20)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/* Values that will change between pattern profiles */
typedef struct
{
      float fAccel;
      float fJerkInAccel;
      float fJerkOutAccel;
      float fDecel;
      float fJerkInDecel; /* This parameter is JerkOutDecel in all non-motion areas of code */
      float fJerkOutDecel; /* This parameter is JerkInDecel in all non-motion areas of code */
      uint16_t uwLevelingSpeed_FPM;
      uint16_t uwLevelingDistance;

      float fFullPatternMinAccelDistance_FT;
      float fShortPatternMinAccelDistance_FT;
      float fShortPatternMinAccelSpeed_FPS;
      uint32_t uiMaxAccelTime;

      float fFullPatternMinDecelDistance_FT;
      float fShortPatternMinDecelDistance_FT;
      float fShortPatternMinDecelSpeed_FPS;
      uint32_t uiMaxDecelTime;

      uint32_t uiShortPatternMinAccelDistance;
      uint32_t uiShortPatternMinDecelDistance;
} Curve_Parameters;
/* Values that will change between runs */
typedef struct
{
      uint32_t uiEndTime_JerkInAccel;
      uint32_t uiEndTime_ConstantAccel;
      uint32_t uiEndTime_JerkOutAccel;

      uint32_t uiEndTime_JerkInDecel;
      uint32_t uiEndTime_ConstantDecel;
      uint32_t uiEndTime_JerkOutDecel;

      uint32_t uiRampUpDistance;
      uint32_t uiSlowdownDistance;
      uint32_t uiRSLDecelDistance;

      uint32_t uiStartPosition_Accel;
      uint32_t uiStartPosition_Cruise;
      uint32_t uiStartPosition_Decel;
      uint32_t uiStartPosition_AddedAccel;
      uint32_t uiStartPosition_RSL;

      uint32_t uiPatternAccelTime; // Time in pattern counts required for acceleration
      uint32_t uiPatternDecelTime; // Time in pattern counts required for deceleration (not including leveling)
      uint32_t uiAddedAccelTime;   // Time in pattern counts required to reach new top run speed
      uint32_t uiRSLTime;          // Time in pattern counts required to reach reduced run speed
      uint8_t ucStartOfRunFloor;   // Floor of car at start of run

      /* Added accel */
      uint32_t uiEndTime_JerkInAccel2;
      uint32_t uiEndTime_ConstantAccel2;
      uint32_t uiEndTime_JerkOutAccel2;

      /* Reduced Speed Limit */
      uint32_t uiEndTime_JerkInDecel2;
      uint32_t uiEndTime_ConstantDecel2;
      uint32_t uiEndTime_JerkOutDecel2;

      uint16_t uwMaxRunSpeed_FPM;  // The max speed reached

} Run_Parameters;

/* Values for speed calc */
typedef struct
{
      enum en_patterns ePatternProfile;
      float fEndTime_JerkInAccel;
      float fEndTime_ConstantAccel;
      float fEndTime_JerkOutAccel;

      // Used in midrun speed calculations - in feet per second
      float fEndSpeed_JerkInAccel;
      float fEndSpeed_ConstantAccel;
      float fEndSpeed_JerkOutAccel;

      // Used in midrun speed calculations - in feet per sec2
      float fMaxAccel;

} Accel_Vars;
typedef struct
{
      enum en_patterns ePatternProfile;

      float fEndTime_JerkInDecel;
      float fEndTime_ConstantDecel;
      float fEndTime_JerkOutDecel;

      // Used in midrun speed calculations - in feet per second
      float fStartSpeed_JerkInDecel;//Leveling speed
      float fEndSpeed_JerkInDecel;
      float fEndSpeed_ConstantDecel;
      float fEndSpeed_JerkOutDecel;
      // Used in midrun speed calculations - in feet per sec2
      float fMaxDecel;

} Decel_Vars;
/* Timers */
typedef struct
{
      uint32_t uiPreflightCounter_ms; // Time preflight has been active
      uint32_t auiStartTimers[NUM_MOTION_START_SEQUENCE];
      uint32_t auiStopTimers[NUM_MOTION_STOP_SEQUENCE];
      uint32_t uiAccelDelayTimeout_ms;
} Motion_Timers;

/* Flags */
typedef struct
{
      uint8_t bDriveHWEnable;
      uint8_t bPickB2;
      uint8_t bPickM;
      uint8_t bPickEBrake;
      uint8_t bPickDrive;
      uint8_t bPickBrake;
      uint8_t bPreflight;
} Motion_Flags;

/* Control */
typedef struct
{
      uint32_t uiNTSDecelTime;// For NTSD speed control
      uint32_t uiRunTimeCounter;// For manual run speed control
      uint32_t uiPrevTime; // For auto run pattern control
      uint32_t uiPrevTime_Added; // For added run pattern control
      uint32_t uiPrevTime_RSLDecel; // For reduced spd decel pattern control

      int16_t wEndOfRunSpeed_FPM;

      int16_t wLastSpeedCMD_FPM;//For tracking speed jumps

      uint8_t bRequestRejected;
      uint32_t uiRejectedDest; // Records the last rejected dest request to prevent spamming mod_motion with midflight changes
      enum en_motion_state eMotionState;
      enum en_motion_stop_sequence eStopSequence;
      enum en_motion_start_sequence eStartSequence;
      uint8_t bLeveling;// Flag signaling the car has transitioned to leveling speed
      uint8_t bQuickStop;
      uint8_t bAcceptanceBypassDecel;//Set by acceptance test for ETS/NTS checks
      uint8_t bAcceptanceTriggerOverspeed;//Set by acceptance test for overspeed test
      uint8_t bReleveling;// Flag signaling car is performing a releveling run
      uint8_t bSpeedLimitChanged; // Flag signaling speed limit has changed during a run
      uint8_t bResetFieldTimer;  // Flag signaling the the reset timer should be reset

      Run_Parameters stRunParameters;

      Accel_Vars stAccelVars;
      Decel_Vars stDecelVars;
      Accel_Vars stAddedAccelVars;
      Decel_Vars stRSLDecelVars;

      Motion_Flags stFlags;
      Motion_Timers stTimers;

      uint16_t uwMaxSpeedOfCurrentRun_FPM; // For debugging
      uint8_t ucSubAlarm_Plus1; // For debugging
      float flContractSpeed_FPS; // Max speed in fps
      uint16_t uwContractSpeed_FPM; // Max speed in fpm
      float flPatternTimeRes_sec; //Resolution of generated pattern in seconds

      uint8_t uwMinAccelSpeed_FPM; //The minimum speed allowed in an normal mode accel pattern (to prevent car stalling after rollback)
      uint8_t ucMinRelevelSpeed_FPM; //The minimum speed allowed in an releveling accel pattern (to prevent car stalling after rollback)

      uint32_t auiAccelPattern_Position[MAX_PATTERN_SIZE_BYTES];
      uint32_t auiAddedPattern_Position[MAX_PATTERN_SIZE_BYTES]; /* This array is used for both added accel ramp up and reduced speed limit ramp down */
      uint32_t auiDecelPattern_Position[MAX_PATTERN_SIZE_BYTES];

      uint32_t uiLastStopPos_05mm;
} Motion_Control;
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern Curve_Parameters * gpstCurveParameters;
extern Motion_Control * gpstMotionCtrl;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
uint8_t Pattern_UpdateParameters( Motion_Profile eProfile );
void Pattern_UpdateCurveLimits( Motion_Profile eProfile );
float Pattern_GetMaxAccelSpeed_FPS( uint32_t uiDistance );
float Pattern_GetMaxAddedAccelSpeed_FPS( uint32_t uiDistance );
uint32_t Pattern_CalculateRampUpDistance( uint16_t uwSpeed );
uint32_t Pattern_CalculateSlowdownDistance( uint16_t uwSpeed );
uint32_t Pattern_CalculateAddedRampUpDistance( uint16_t uwStartSpeed_FPM, uint16_t uwEndSpeed_FPM );
uint32_t Pattern_CalculateReducedSpdDistance( uint16_t uwStartSpeed_FPM, uint16_t uwEndSpeed_FPM );

void Pattern_GenerateAccelRun( float flMaxSpeed_FPS );
void Pattern_GenerateDecelRun( float flMaxSpeed_FPS );
void Pattern_GenerateAddedAccelRun( float flMaxSpeed_FPS );
void Pattern_GenerateReducedSpdRun( float flStartSpeed_FPS, float flEndSpeed_FPS);

uint16_t Pattern_GetAccelCommandSpeed(uint32_t uiTimeIndex);
uint16_t Pattern_GetDecelCommandSpeed(uint32_t uiTimeIndex);
uint16_t Pattern_GetAddedAccelCommandSpeed(uint32_t uiTimeIndex);
uint16_t Pattern_GetReducedSpdCommandSpeed(uint32_t uiTimeIndex);

uint32_t Pattern_GetAccelPosition(uint32_t uiTimeIndex);
uint32_t Pattern_GetDecelPosition(uint32_t uiTimeIndex);
uint32_t Pattern_GetAddedAccelPosition(uint32_t uiTimeIndex);
uint32_t Pattern_GetReducedSpdPosition(uint32_t uiTimeIndex);

uint32_t Pattern_GetMinShortRunDistance_Counts();

uint8_t Pattern_CheckForInvalidSCurve(Motion_Profile eProfile);
/*----------------------------------------------------------------------------
   TODO MOVE:
   mod_Motion.c only functions below
 *----------------------------------------------------------------------------*/

#endif
