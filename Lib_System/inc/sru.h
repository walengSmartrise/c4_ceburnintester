/******************************************************************************
 *
 * @file     sru.h
 * @brief    Header file for MR SRU deployment
 * @version  V1.00
 * @date     28, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef SRU_H
#define SRU_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

typedef enum
{
   enSRU_DEPLOYMENT__Invalid0,
   enSRU_DEPLOYMENT__COP,
   enSRU_DEPLOYMENT__CT,
   enSRU_DEPLOYMENT__Invalid3,
   enSRU_DEPLOYMENT__MR,
   enSRU_DEPLOYMENT__HIO,
   enSRU_DEPLOYMENT__CN,
   enSRU_DEPLOYMENT__RIS,

   SRU_NUM_DEPLOYMENT_TYPES
}en_sru_deployments;

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
