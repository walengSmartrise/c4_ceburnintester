/******************************************************************************
 *
 * @file     sys.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/
#ifndef SYS_H
#define SYS_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include "chip.h"
#include <stdint.h>
#include <sys_drive.h>
#include "faults.h"
#include "alarms.h"
#include "param.h"
#include "can.h"
#include "network.h"
#include "sys_io.h"
#include "debug.h"
#include "rtc.h"
#include "subfaults.h"
#include "network2.h"
#include "hallboard.h"
#include "riser.h"
#include "operation.h"
#include "subalarms.h"
#include "version.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define MASTER_PAYMENT_PASSCODE              (116578)
/*-----------------------------------------------------------------------------

   MACROS

 -----------------------------------------------------------------------------*/
#ifndef NULL
#define NULL    ((void *)0)
#endif

#define BITMAP8_SIZE( x )            ( ( ( ( x ) - 1 ) / 8 ) + 1 )
#define NUM_BITS_PER_BITMAP8       (8)
#define BITMAP32_SIZE( x )          ( ( ( ( x ) - 1 ) / 32 ) + 1 )
#define NUM_BITS_PER_BITMAP32       (32)
#define NUM_ARRAY_ELEMENTS( x )     ( sizeof( x ) / sizeof ( x[ 0 ] ) )
#define NUM_BITS_PER_BITMAP24       (24)
#define NUM_BITS_PER_BITMAP16       (16)

#define NUM_BYTES_PER_BITMAP32      (4)

/* x = MASK, y = value to check */
#define CHECK_SET_BITMASK_U8( x, y )      ( ( (uint8_t) x & (uint8_t) y) ==  (uint8_t) x )
#define CHECK_CLR_BITMASK_U8( x, y )      ( ( (uint8_t) x & ~( (uint8_t) y) ) ==  (uint8_t) x )
#define CHECK_SET_BITMASK_U16( x, y )      ( ( (uint16_t) x & (uint16_t) y ) ==  (uint16_t) x )
#define CHECK_CLR_BITMASK_U16( x, y )      ( ( (uint16_t) x & ~(uint16_t) y ) ==  (uint16_t) x )


/* Get bit*/
#define GET_BITFLAG_U8( bitmask, pos )         ((uint8_t) bitmask >> pos & 1 )
#define GET_BITFLAG_U16( bitmask, pos )         ((uint16_t) bitmask >> pos & 1 )
#define GET_BITFLAG_U32( bitmask, pos )         ((uint32_t) bitmask >> pos & 1 )

/* Get byte */
#define GET_BYTE_U32( data, pos )                     (((uint32_t) data >> (8*pos)) & 0xFF) //pos = [0,3]
#define GET_BYTE_U16( data, pos )                     (((uint16_t) data >> (8*pos)) & 0xFF) //pos = [0,1]

/* Encode CAN ID */
#define GET_EXT_CAN_MSG_ID( dgID, dstID, srcID, netID )      ( (1 << 30)       \
                                                             | ((dgID & 0xFF) << 21) \
                                                             | ((dstID & 0x7FFF)) \
                                                             | ((srcID & 0x0F) << 14) \
                                                             | ((netID & 0x07) << 18) \
                                                             )

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define HALL_BOARDS_MAX_NUM_FLOORS             (128)

#define MAX_NUM_FLOORS             (96)

/* Standard software typically assumes a max floor limit of 64 floors */
#define MAX_NUM_FLOORS_REDUCED             (64)

#define MIN_NUM_FLOORS             (2)

#define MAX_CAR_SPEED              (1600)//(2000)
#define MAX_CAR_SPEED_OVERSPEED    (2400)//Max car speed for supporting overspeed testing for 1600 cars
#define MAX_CAR_DECEL_FT_PER_MIN2  (100.0f)
#define MAX_INSP_SPEED             (150)
#define MIN_LEARN_SPEED            (10U)
#define MIN_CONTRACT_SPEED         (10U)
#define MAX_TERMINAL_SPEED         (30U)

#define MAX_LEVELING_SPEED         (20U)
#define MIN_LEVELING_SPEED         (1U)

#define MAX_NTSD_SPEED         (20U)
#define MIN_NTSD_SPEED         (1U)

#define MAX_MINIMUM_ACCEL_SPEED     (25U)
#define MIN_MINIMUM_ACCEL_SPEED     (1U)

/* Battery rescue will have a fixed minimum accel speed for assessing drive's chosen direction */
#define BATTERY_RESCUE_MIN_ACCEL_SPEED     (5U)

#define HALF_INCH       (25.4)
#define QUARTER_INCH    (13)
#define ONE_INCH        (51)
// This is the position count for the given length. CEDES is in 0.5mm
// counts.
#define ONE_INCHS       ( 50.8f  )
#define ONE_HALF_INCHS  ( 76.2f  )
#define TWO_INCHES      ( 101.6f )
#define TWO_HALF_INCHES ( 127.0f )
#define THREE_INCHES    ( 152.4f )
#define SIX_INCHES      ( 2 * THREE_INCHES )
#define ONE_FEET        ( 12 * ONE_INCHS )
#define TWO_FEET        ( 2 * ONE_FEET )
#define FIVE_FEET       ( 3048 )
#define TEN_FEET        (ONE_FEET * 10)
// The internal EEPROM on the 4078 is arranged in pages of 64 bytes each.
#define EEPROM_INT__UINT8s_PER_PAGE  (64)
#define EEPROM_INT__UINT16s_PER_PAGE  (32)
#define EEPROM_INT__UINT32s_PER_PAGE  (16)

#define EEPROM_INT__BYTES_PER_PAGE  (64)

// Device has 4032 bytes of storage (not 4096 which would be 4K) so there are
// only 63 pages (not 64).
#define EEPROM_INT__NUM_PAGES  (63)

#define MAX_VISIBLE_CALLS (5)//Enter Car call UI

// Number of fault log items stored on the MR FRAM chip. Also used for the alarm log.
#define NUM_FAULTLOG_ITEMS                  (32U)

#define MAX_BRAKE_VOLTAGE_DC       (255U)
#define MINIMUM_FLOOR_SPACING                (ONE_INCH*7)  // 2*Deadzone Size + DZ Magnet Size
//Group shared setting
#define MAX_GROUP_CARS (8)

// Offline delay for core boards (MR, CT, COP)
#define MIN_OFFLINE_DELAY_5MS             (200U)


// Number of ETS trip points
#define NUM_ETS_TRIP_POINTS            (8U)
#define MIN_ETS_TRIP_SPEED             (75)
// Number of nts trip points
#define NUM_NTS_TRIP_POINTS            (8U)
#define MIN_NTS_TRIP_SPEED             (50)
// Scaling of saved NTS positions to CEDES counts
#define NTS_POSITION_SCALING  (10)

#define MAX_24BIT_VALUE    (0xFFFFFF)

#define DEFAULT_DATAGRAM_RESEND_RATE_1MS     (10000)

#define ENABLE_FRAM_SSP_MODIFICATION            (0) // Test option for offloading FRAM SPI communication to hardware.
#define INCREASED_PRIORITY_FOR_BITBANGED_CPLD_SPI     (0) // Test option give CPLD SPI communication the highest interrupt priority to address reliability issues seen with CPLD communication.

#define INVALID_LANDING    (255)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
Port & Pin
 -----------------------------------------------------------------------------*/


typedef struct
{
   uint8_t port;
   uint8_t pin;
}PortAndPin;
typedef struct
{
   uint8_t port;
   uint8_t pin;
   uint8_t invert;
}PortAndPin_Inv;
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
typedef enum
{
   enMCU_Unknown,
   enMCUA_SRU_Base,
   enMCUB_SRU_UI,
   enMCUC_Hydro,
   enMCUD_Traction,
   enMCUE_SRU_IO,
   enMCUF_Input,
}enum_sys_mcu_ids;

typedef enum
{
   enMCU_Unknown15 = 0xF,
   enMCU_MR_Base = 0x0,
   enMCU_MR_UI = 0x2,
   enMCU_CT_Base = 0x5,
   enMCU_CT_UI = 0x7,
   enMCU_COP_Base = 0x4,
   enMCU_COP_UI = 0x6,
   enMCU_IO_Exp = 0xA,
   enMCU_Input = 0x0B,
}enum_sys_mcu_ids2;


enum enum_sys_tick_count_resolutions
{
   enTickCount_1us,
   enTickCount_1ms,
   enTickCount_100ms,
   enTickCount_500ms,
   enTickCount_1s,

   NUM_SYS_TICK_COUNTERS
} ;

/*-----------------------------------------------------------------------------
   CEDES Positioning System
 -----------------------------------------------------------------------------*/

struct st_cedes_frame
{
   uint32_t uiPosCount;
   int16_t wVelocity;
   uint16_t uwStatus;
   uint8_t ucError;
   uint8_t ucClip;
   uint8_t ucClipOffset;
};

/*-----------------------------------------------------------------------------
   CAN
 -----------------------------------------------------------------------------*/
enum en_sys_cans
{
   enCAN_1 = 1,
   enCAN_2 = 2,

   NUM_SYS_CANS
};

enum en_sys_can_bauds
{
   enCAN_BAUD_25k = 25000,
   enCAN_BAUD_50k = 50000,
   enCAN_BAUD_100k = 100000,
   enCAN_BAUD_125k = 125000,
   enCAN_BAUD_250k = 250000,
   enCAN_BAUD_500k = 500000,
   enCAN_BAUD_1M = 1000000,

   NUM_SYS_CAN_BAUDS
};

struct st_can
{
   uint32_t ulBAUD;   // use enum en_sys_can_bauds
   uint8_t  ucIndex;  // use enum en_sys_cans
   uint8_t  ucFlags;  // 4= TRC Board
};

struct st_can_msg               /*!< Message structure */
{
   uint32_t ulID;               /*!< Message Identifier. If 30th-bit is set, this is 29-bit ID, othewise 11-bit ID */
   uint32_t ulType;             /*!< Message Type. which can include: - CAN_REMOTE_MSG type*/
   uint32_t ulDLC;              /*!< Message Data Length: 0~8 */
   uint8_t  aucData[8];          /*!< Message Data */
};

/*-----------------------------------------------------------------------------
   Module
 -----------------------------------------------------------------------------*/
//-----------------------------------------------------------------------------
struct st_module
{
   uint32_t (*pfnInit)( struct st_module *pstThisModule );
   uint32_t (*pfnRun)( struct st_module *pstThisModule );

   uint16_t uwInitialDelay_1ms;
   uint16_t uwRunPeriod_1ms;

   int32_t iTimeOfNextRun_1ms;

   // Keeps track of the longest time it took to execute the module's pfnRun()
   // function. Every time the pfnRun() is called, it is timed to see how long
   // it takes to return. This value (in microseconds) is updated whenever the
   // call takes longer than the previous record. Note that if an interrupt(s)
   // occurs while pfnRun() is executing, that will make pfnRun() appear to
   // take longer to execute since the ISR(s) execution time will be added.
   int32_t iLongestRun_1us;

   float fAverageRun_1us;
   float fWindowTotal;
   uint8_t ucAverageWindow;
   uint8_t bTimeViolation;
};

//-----------------------------------------------------------------------------
struct st_module_control
{
   uint32_t uiNumModules;

   uint32_t uiCurrModIndex;

   // Keeps track of which module's pfnRun() function took the longest to
   // execute. Keeps track of the module index and the length of time in
   // microseconds. Module run times are continually monitored and these values
   // are updated as needed.
   uint32_t uiModuleWithLongestRunTime_ModIndex;
   int32_t iModuleWithLongestRunTime_1us;

   // Keeps track of the longest time it took to cycle through calling all the
   // modules', pfnRun() functions. Any module that needs to be serviced on a
   // period shorter than this value should either put multiple entries in the
   // module service array pastModules or consider using an interrupt.
   int32_t iLongestSuperLoopCycleTime_1us;

   struct st_module * (* pastModules)[];

   uint8_t enMCU_ID;
   uint8_t enDeployment;
};


typedef union
{
      float af32[ 2 ];
      uint32_t aui32[ 2 ];
      uint16_t auw16[ 4 ];
      uint8_t auc8[ 8 ];
      int32_t ai32[ 2 ];
      int16_t aw16[ 4 ];
      int8_t ac8[ 8 ];
} un_sdata_datagram;

//-----------------------------------------------------------------------------
// Contains all the information about one network's shared data.
struct st_sdata_control
{
   un_sdata_datagram *paunDatagrams;

   uint8_t *paucDatagramDirtyBits;

   int32_t *paiLastSendCount_1ms;
   int32_t *paiResendRate_1ms;

   uint32_t uiNumDatagrams;

   uint16_t uwRoundRobinIndex;
   uint16_t uwLastSentIndex;

   int32_t iLastCountHB_1ms;//for tracking need to send HB

   uint16_t uwHeartbeatInterval_1ms;

   uint8_t bDisableResend;// For disabling check of paiResendRate_1ms for all datagrams
   uint8_t bDisableHeartbeat;// Disabling minimum packet send rate
};
struct st_sdata_local
{
   struct st_can stCAN_Config;
   uint8_t ucNetworkID;
   uint8_t ucLocalNodeID;
   uint8_t ucDestNodeID;
   uint8_t ucNumNodes;
};
/*-----------------------------------------------------------------------------
   Position
 -----------------------------------------------------------------------------*/
typedef struct
{
   uint8_t  Data[CAN_MSG_MAX_DATA_LEN];/*!< Message Data */
   uint8_t bNewMsg;
} st_position_frame;

typedef struct
{
   un_sdata_datagram unDatagram;/*!< Message Data */
   uint8_t bNewMsg;
} st_position_frame2;
/*-----------------------------------------------------------------------------
   CRC
 -----------------------------------------------------------------------------*/
enum crc_mode
{
   enCRC_MODE__CCITT,
   enCRC_MODE__16_BIT,
   enCRC_MODE__32_BIT,

   NUM_CRC_MODE
};

/*-----------------------------------------------------------------------------
   Parameters
 -----------------------------------------------------------------------------*/

// These are all the nodes that maintain a copy of the parameters.
enum en_param_nodes
{
   enPARAM_NODE__TRACTION,
   enPARAM_NODE__MRA,
   enPARAM_NODE__MRB,
   enPARAM_NODE__CTA,
   enPARAM_NODE__CTB,
   enPARAM_NODE__COPA,
   enPARAM_NODE__COPB,

   NUM_PARAM_NODES
};

//-----------------------------------------------------------------------------
struct st_param_and_value
{
   uint32_t uiValue;
   uint16_t uwParamIndex;
   uint8_t ucBlockIndex;
   uint8_t ucCountdown;
};
//-----------------------------------------------------------------------------
struct st_parameter_block
{
//   uint16_t uwNumParameters;

   uint8_t ueBlockType;
};

//-----------------------------------------------------------------------------
struct st_param_chunk_req
{
      uint8_t ucBlock_Plus1;
      uint8_t ucChunk_Plus1; //[0,14]
      uint8_t ucCountdown;//for priority
};
//-----------------------------------------------------------------------------
struct st_param_block_info
{
   // Information about the individual parameter blocks.
   const struct st_parameter_block * (* pastParamBlocks)[];

   uint8_t ucNumBlocks;  // total number of blocks

   // Parameter blocks are sized so that one block fits in one 64-byte internal
   // EEPROM page. The first few pages of the EEPROM are reserved for temporary
   // storage while updating a block and for possible furture use. This field
   // holds the first EEPROM page used for block storage. Block 00 is stored at
   // this EEPROM page, Block 01 is stored at the next higher page, etc.
   uint8_t ucEEPROMI_Page__Block00;

   // This indicates the EEPROM page used for temporary storage while updating
   // a block. Because the EEPROM does page oriented writes, a loss of power
   // during a write can wipe out a 64-byte page. For this reason, before
   // updating a page, the old page value is copied to a temporary page first.
   // The original page is then updated and the temporary page erased. This
   // ensures that a loss of power at any point will not result in data loss.
   uint8_t ucEEPROMI_Page__Temp;
};

//-----------------------------------------------------------------------------
struct st_param_control
{
   // Points to structure containing information about the parameter blocks.
   // How many blocks there are, how many parameter in a particular block,
   // how they are stored in the internal EEPROM, etc.
   struct st_param_block_info *pstParamBlockInfo;

   uint8_t *paucParamBlocksInRAM;

   uint32_t *pauiEEPROM_CRCs;

   uint32_t *pauiMaster_CRCs;

   // Master keep track of all the request for parameters form the slaves.
   //struct st_param_and_value *pastSlaveRequestedParam;
   struct st_param_and_value astSlaveRequestedParam[ NUM_PARAM_NODES ];

   // Master log of all request for chunk
   struct st_param_chunk_req astSlaveRequestedChunk[ NUM_PARAM_NODES ];

   struct st_param_chunk_req stSlaveReceivedChunk;
   // When a slave node wants to change the value of a parameter, it loads the
   // request into one of these structures.
   struct st_param_and_value astParmEditRequest[ NUM_PARAM_NODES ];


   // Used to hold parameter block, index, and value for operations by various
   // functions.
   struct st_param_and_value stParamAndValue_Arg;
   struct st_param_and_value stParamAndValue_Ret;

   struct st_param_and_value stRecentlyUpdatedParam;

   struct st_param_and_value stRequestedParam;
   struct st_param_and_value stReceivedParam;

   struct st_param_and_value stParam_MasterRoundRobin_NetAB;
   struct st_param_and_value stParam_MasterRoundRobin_NetCar;
   struct st_param_and_value stParam_MasterRoundRobin_NetControl;

   // Parameter slaves will compare their CRCs received master CRCs
   // the first out of date block will be stored here.
   uint8_t ucOutOfDateBlock_IndexPlus1;

   uint8_t ucRequestedChunk; //[0,14]
   uint8_t ucRecievedChunk; //[0,14]

   uint8_t ucParamNodeType;  // enum en_param_nodes

   uint8_t bRAM_CopyValid;

   // Only node acting as parameter master (MRA) sets this to non-zero
   uint8_t bMaster;
};

/*-----------------------------------------------------------------------------
   Call and Dispatching
 -----------------------------------------------------------------------------*/

struct st_autoModeRules {
   uint8_t bIgnoreCarCallSecurity: 1;
   uint8_t bIgnoreHallCallSecurity: 1;
   uint8_t bIgnoreCarCall_F: 1;
   uint8_t bIgnoreCarCall_R: 1;
   uint8_t bIgnoreHallCall: 1;
   uint8_t bAllowedOutsideDoorZone: 1; // Suppress correction runs when ON
   uint8_t bParkingEnabled: 1;
   uint8_t bAutoDoorOpen: 1; // Automatically open doors at end of run
   uint8_t bIgnoreDCB: 1; // Suppress DCB when ON
   uint8_t bDoorHold: 1;  // Door held open until commanded to close
   uint8_t bForceDoorsOpenOrClosed: 1; // Doors will return to the last full open or close state they were in. Overrides bAutoDoorOpen.
};

/*-----------------------------------------------------------------------------
   Faults
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 * Runtime Data
 *----------------------------------------------------------------------------*/

enum en_Capture_Mode {
   CAPTURE_OFF,
   CAPTURE_ON,  // Car capture request has been received car will stop taking hall calls and clear out requests.
   CAPTURE_IDLE // Car has been successfully captured
};

enum en_door_ui_ctrl
{
   DOOR_UI_CTRL__NONE,
   DOOR_UI_CTRL__CLOSE_F,      // Close Front Door
   DOOR_UI_CTRL__OPEN_F,      // Open Front Door
   DOOR_UI_CTRL__CLOSE_R = 4,       // Close Rear Door
   DOOR_UI_CTRL__OPEN_R  = 8,      // Open Rear Door
   DOOR_UI_CTRL__CLOSE_B = 5,      // Close Both Door
   DOOR_UI_CTRL__OPEN_B = 10,      // Open Both Door
   DOOR_UI_CTRL__NUDGE_F = 16,
   DOOR_UI_CTRL__NUDGE_R = 32,

   NUM_DOOR_UI_CTRL
};
enum en_doors {
   DOOR_FRONT,
   DOOR_REAR,
   NUM_OF_DOORS,
   DOOR_ANY
};
enum en_door_commands
{
   DOOR_COMMAND__NONE,
   DOOR_COMMAND__OPEN_UI_REQUEST,
   DOOR_COMMAND__OPEN_IN_CAR_REQUEST,
   DOOR_COMMAND__OPEN_ADA_MODE,
   DOOR_COMMAND__OPEN_SABBATH_MODE,
   DOOR_COMMAND__OPEN_HALL_REQUEST,
   DOOR_COMMAND__OPEN_HOLD_REQUEST, /* Holds door open without dwell timer */
   DOOR_COMMAND__OPEN_CONSTANT_PRESSURE,
   DOOR_COMMAND__CLOSE,
   DOOR_COMMAND__CLOSE_CONSTANT_PRESSURE,
   DOOR_COMMAND__NUDGE,
   DOOR_COMMAND__NUDGE_CONSTANT_PRESSURE,
   DOOR_COMMAND__FAULT,
   DOOR_COMMAND__OPEN_HOLD_DWELL_REQUEST, /* Predefined dwell time */

   DOOR_COMMAND__OPEN_LOBBY_REQUEST, /* Open doors longer at lobby floor */

   NUM_OF_DOOR_COMMANDS,
};
enum en_door_states
{
   DOOR__UNKNOWN,

   DOOR__CLOSED,
   DOOR__OPENING,
   DOOR__OPEN,
   DOOR__PARTIALLY_OPEN,
   DOOR__CLOSING,
   DOOR__NUDGING,

   NUM_DOOR_STATES
};
typedef enum
{
   UI_DOOR__UNKNOWN,

   UI_DOOR__CLOSED,
   UI_DOOR__CLOSED_WITH_DC,

   UI_DOOR__OPENING,
   UI_DOOR__OPENING_WITH_GSW,
   UI_DOOR__OPENING_WITH_PHE,

   UI_DOOR__OPEN,
   UI_DOOR__OPEN_WITH_DO,
   UI_DOOR__OPEN_WITH_PHE,
   UI_DOOR__OPEN_WITH_PHE_DO,

   UI_DOOR__PARTIALLY_OPEN,
   UI_DOOR__PARTIALLY_OPEN_WITH_PHE,

   UI_DOOR__CLOSING,
   UI_DOOR__CLOSING_WITH_GSW,
   UI_DOOR__CLOSING_WITH_PHE,

   UI_DOOR__NUDGING,

   NUM_UI_DOOR_STATES
}en_ui_door_states;

typedef enum
{
   DOOR_TYPE__AUTOMATIC,
   DOOR_TYPE__FREIGHT,
   DOOR_TYPE__MANUAL,
   DOOR_TYPE__SWING,

   NUM_DOOR_TYPES
}en_door_type;

enum en_cw_states
{
   CW__UNKNOWN,
   CW__OOS,
   CW__GOING_TO_CLSOEST_DZ,

   NUM_CW_STATES
};

enum en_fire_2_states
{
   FIRE_II__UNKNOWN,
   FIRE_II__OFF,
   FIRE_II__HOLD,
   FIRE_II__ON,

   NUM_FIRE_II_STATES
};


enum en_automatic_state
{
   AUTO_UNKNOWN,
   AUTO_STOPPED,
   AUTO_MOVING,
   AUTO_IDLE,
   AUTO_CORRECTION,
   AUTO_NUMBER_OF_MODES
};

enum en_learn_states
{
   LEARN__NOT_ON_LEARN,
   LEARN__READY,
   LEARN__LEARNING,
   LEARN__COMPLETE,

   NUM_LEARN_STATES
};

enum en_recall_states
{
   RECALL__UNKNOWN,
   RECALL__MOVING,
   RECALL__STOPPED,
   RECALL__RECALL_FINISHED,

   NUM_RECALL_STATES
};

typedef enum
{
   VIP_STATE__INACTIVE,
   VIP_STATE__CAPTURING,
   VIP_STATE__ACCEPTING_HC,
   VIP_STATE__ACCEPTING_CC,
   VIP_STATE__WAIT_FOR_IDLE,
   NUM_VIP_STATES
}en_vip_states;

typedef enum
{
   SWING__UNKNOWN,
   SWING__CAPTURING,
   SWING__ON,

   NUM_SWING_STATES
}en_swing_states;

typedef enum
{
   EMS2__UNKNOWN,
   EMS2__INACTIVE,
   EMS2__ACTIVE,
   NUM_EMS2_STATES
} en_ems2_states;

typedef enum
{
   EMS1__UNKNOWN,
   EMS1__INACTIVE,
   EMS1__ACTIVE,

   NUM_EMS1_STATES
} en_ems1_states;
//-------------------------

//-----------------
// Enum that defines all the possible NTS faults
typedef enum
{
   NTS_FAULT__UNKNOWN,
   NTS_FAULT__NONE,
   NTS_FAULT__CAMERA_FAULT,
   NTS_FAULT__INVALID_CONTRACT_SPPED,//todo rm

   NUM_NTS_FAULTS
} NTS_Faults;

// Enum that defines all the possible NTS alarms
typedef enum
{
   NTS_ALM__NONE,
   NTS_ALARM__INCOMPLETE_LEARN,
   NTS_ALARM__INVALID_LEARN,
   NTS_ALARM__NEED_TO_RELEARN,

   NUM_NTS_ALARMS
} NTS_Alarms;

// The states for the NTS state machine
typedef enum
{
   NTS__FAULT,
   NTS__ALARM,
   NTS__LEARNING,//
   NTS__READY,
   NTS__RUNNING,
   NTS__TRIPPED,

   NUM_NTS_STATES
} NTS_State;
/*----------------------------------------------------------------------------
   Brake
 *----------------------------------------------------------------------------*/
enum en_brake_states
{
   BRAKE_STATE__UNK,/* Output zero */

   BRAKE_STATE__IDLE, /* Output zero */
   BRAKE_STATE__GENERATE_RAMP,/* Beyond this point: Output NONzero */
   BRAKE_STATE__RAMP_P,
   BRAKE_STATE__PICK,
   BRAKE_STATE__RAMP_H,
   BRAKE_STATE__HOLD,
   BRAKE_STATE__RAMP_S,

   BRAKE_STATE__ERR,/* Output zero */
   NUM_BRAKE_STATES
};

/*----------------------------------------------------------------------------
 *
 *    Structures
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
   Operation
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
   Motion
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
   Postiion
 *----------------------------------------------------------------------------*/

struct st_position_b
{
   uint32_t ulPosCount;
   uint16_t uwStatus;
   int16_t wVelocity;
   uint8_t ucError;
   uint8_t ucClip;
   uint8_t ucClipOffset;
};
/*----------------------------------------------------------------------------
   Brake
 *----------------------------------------------------------------------------*/
struct st_brake
{
   /* From MRA to BRK */
   uint8_t ucCommandByte; /* 0=Drop Brake, 1=Lift Brake */
   uint8_t ucRampUpTime_10ms; /* 2.55 sec Max */
   uint8_t ucRampDownTime_10ms; /* 2.55 sec Max */
   uint8_t ucPickTime_10ms; /* 2.55 sec Max */
   uint8_t ucPickVoltageCommand; /* 255VDC Max */
   uint8_t ucHoldVoltageCommand; /* 255VDC Max */
   /* From BRK to MRA*/
   enum en_brake_states eState;
   enum en_brake_faults eError;
   uint8_t ucVoltageFeedback;
   uint8_t bBPS_Feedback; // Raw brake feedback
   uint8_t ucMeasuredPickVoltage;
   uint8_t ucMeasuredHoldVoltage;
   uint8_t bBrakeVoltage_240VAC;//AC voltage supplied to board (0 = 120, 1 = 240)
   /* On MRA */
   uint8_t ucFP_PickVoltage;
   uint8_t ucFP_HoldVoltage;
   uint8_t ucFP_RelVoltage;
   uint8_t ucFP_RampTime_10ms;
   uint8_t ucFP_PickTime_10ms;
   uint8_t bBPS; // Processed (possibly inverted) brake feedback
   uint16_t uwOfflineCounter_1ms;
};

//-------------------------
struct st_io_replay_item
{
      uint32_t uiInputBitmap[BITMAP32_SIZE(NUM_INPUT_FUNCTIONS)];
      uint32_t uiOutputBitmap[BITMAP32_SIZE(NUM_OUTPUT_FUNCTIONS)];
};
/*----------------------------------------------------------------------------

         Faults

 *----------------------------------------------------------------------------*/
typedef struct
{
      uint32_t ulPosition;
      uint32_t ulTimestamp;
      int16_t wSpeed;
      en_faults eFaultNum;

      int16_t wEncoderSpeed;
      int16_t wCommandSpeed;
      uint8_t ucCurrentFloor;
      uint8_t ucDestinationFloor;
} st_fault_data;
typedef struct
{
      en_faults aeActiveFaultNum[NUM_FAULT_NODES];

      uint8_t bActiveFault;
      en_faults eFaultNumber;//holds first active fault
      //node where first active fault was detected.
      enum en_fault_nodes eActiveNode_Plus1;
} st_fault;

/*----------------------------------------------------------------------------
   Alarms
 *----------------------------------------------------------------------------*/
typedef struct
{
      en_alarms aeActiveAlarmNum[NUM_ALARM_NODES];

      uint8_t bActiveAlarm;
      en_alarms eAlarmNumber;//holds first active alarm
      //node where first active alarm was detected.
      en_alarm_nodes eActiveNode_Plus1;
} st_alarm;
typedef struct
{
      uint32_t ulPosition;
      uint32_t ulTimestamp;
      int16_t wSpeed;
      en_alarms eAlarmNum;
} st_alarm_data;

/*----------------------------------------------------------------------------
   Emergency Power
 *----------------------------------------------------------------------------*/
/* Group master dispatcher, emergency power state machine states */
enum enEmergencyPower {
   EP_OFF, // Car is not on emergency power
   EP_ON, // Car is on emergency power, but generator is not spun up yet. Cars should not move.
   EP_RECALL, // Generator is spun up, cars can be lowered one at a time to the recall floor
   EP_RUN_CAR, // All cars have been recalled and are at bottom floor with doors open, allow a certain number of them to run
   EP_PRETRANSFER, // Car is transfering from generator power to main line power, stop all cars in preparation of this transition
   EP_NUM_OF_MODES
};

/* Commands from group dispatching car to each of the group cars telling them what to do on emergency power. Each is always recieving one of these commands (resent at a regular interval) */
enum enEmergencyPowerCommands {
   EPC__OFF, // Emergency power is not active, cars behave normally
   EPC__RECALL, // Recall to the lobby, hold doors open and fault
   EPC__RUN_AUTO, // Car is allowed to take car calls and hall calls
   EPC__OOS, // Car should fault wherever it is
   EPC__PRETRANSFER, // Car should stop and the nearest landing, open its doors and fault.
   EPC__NUM_OF_COMMANDS
};
/*----------------------------------------------------------------------------
   Load Weigher
 *----------------------------------------------------------------------------*/
typedef enum
{
   LW_SELECT__DISCRETE,
   LW_SELECT__MR,
   LW_SELECT__CT,
   NUM_LW_SELECT
}enum_lw_select;

/*----------------------------------------------------------------------------
   View Debug Data
 *----------------------------------------------------------------------------*/
typedef enum
{
   VDD__NONE,
   VDD__MR_CAN1,
   VDD__MR_CAN2,
   VDD__MR_CAN3,
   VDD__MR_CAN4,
   VDD__MR_A_NET,
   VDD__MR_B_NET,
   VDD__MR_RS485,
   VDD__CT_CAN1,
   VDD__CT_CAN2,
   VDD__CT_CAN3,
   VDD__CT_CAN4,
   VDD__CT_A_NET,
   VDD__CT_B_NET,
   VDD__CT_RS485,
   VDD__COP_CAN1,
   VDD__COP_CAN2,
   VDD__COP_CAN3,
   VDD__COP_CAN4,
   VDD__COP_A_NET,
   VDD__COP_B_NET,
   VDD__COP_RS485,
   VDD__RUN_SIGNALS,
   VDD__LAST_STOP_POS,
   VDD__MRA_VERSION,
   VDD__MRB_VERSION,
   VDD__CTA_VERSION,
   VDD__CTB_VERSION,
   VDD__COPA_VERSION,
   VDD__COPB_VERSION,
   VDD__DIR_CHANGE_COUNT,
   VDD__RIS1_CAN1,
   VDD__RIS2_CAN1,
   VDD__RIS3_CAN1,
   VDD__RIS4_CAN1,
   VDD__RIS1_CAN2,
   VDD__RIS2_CAN2,
   VDD__RIS3_CAN2,
   VDD__RIS4_CAN2,
   VDD__DESTINATION_1,
   VDD__DESTINATION_2,
   VDD__IDLE_TIME,
   VDD__DRIVE_SPD_FB,
   VDD__DOOR_DATA_F,
   VDD__DOOR_DATA_R,
   VDD__CPLD_MR,
   VDD__CPLD_CT,
   VDD__CPLD_COP,

   NUM_VDD
} en_view_debug_data;
typedef enum
{
   RSF__GO_TO_DEST,       // Destination Determined
   RSF__DOOR,             // Car Doors All Closed
   RSF__LOCK,             // Locks All Closed
   RSF__RUN,              // Run Flag
   RSF__HW_ENABLE,        // DSD Drive HW Enable
   RSF__PICK_M,           // M Contactor Command
   RSF__MMC,              // M Contactor Feedback
   RSF__PICK_DRIVE,       // Drive Command
   RSF__MBC,              // B Contactor Feedback
   RSF__SERIAL_SPEED_REG, // Serial Speed Reg Rls
   RSF__BRAKE_PICK,       // Primary Brake Pick
   RSF__BPS,              // Primary BPS
   RSF__EBRAKE_PICK,      // Secondary Brake Pick
   RSF__EBPS,             // Secondary BPS
   RSF__COMMAND,          // Command Speed Nonzero
   RSF__FEEDBACK,         // Camera Speed Nonzero

   NUM_RSF,
   MAX_RSF = 16
} en_run_signal_flags;

/*----------------------------------------------------------------------------
   UART Status
 *----------------------------------------------------------------------------*/
enum enum_uart_status
{
   UART_STATUS__PACKET_RECEIVED,
   UART_STATUS__PACKET_TRANSMITTED,
   UART_STATUS__RCV_CHECKSUM,
   UART_STATUS__RCV_OVERFLOW1,
   UART_STATUS__RCV_OVERFLOW2,

   NUM_UART_STATUS_BYTES
};

/*----------------------------------------------------------------------------
   enPARAM8__LoadWeigherSelect options
 *----------------------------------------------------------------------------*/
typedef enum
{
   LWS__DISCRETE,
   LWS__MR,
   LWS__CT,

   NUM_LWS
} en_lws;

typedef enum
{
   LOAD_FLAG__LIGHT_LOAD,
   LOAD_FLAG__FULL_LOAD,
   LOAD_FLAG__OVER_LOAD,

   NUM_LOAD_FLAG
} en_load_flag_bits;
/* LWD calibration states */
typedef enum
{
   LWD_CALIB_STATE__NONE,
   LWD_CALIB_STATE__CAPTURE,
   LWD_CALIB_STATE__MOVE_TO_BOTTOM,
   LWD_CALIB_STATE__LEARN_FLOOR,
   LWD_CALIB_STATE__MOVE_TO_NEXT,
   LWD_CALIB_STATE__COMPLETE,

   NUM_LWD_CALIB_STATES
} en_lwd_calib_state;

typedef enum
{
   DISPATCH_MODE__NONE,
   DISPATCH_MODE__LOBBY,
   DISPATCH_MODE__UP,
   DISPATCH_MODE__DOWN,

   NUM_DISPATCH_MODES
} en_dispatch_mode;

typedef enum
{
   VIRTUAL_SYSTEM_INPUT__UNUSED0,
   NUM_VIRTUAL_SYTEM_INPUTS,
} en_virtual_system_inputs;
/*----------------------------------------------------------------------------
   Type conversions
 *----------------------------------------------------------------------------*/
typedef union
{
   float f;
   uint8_t auc[4];
   uint16_t auw[2];
   uint32_t ui;
   int8_t ac[4];
   int16_t aw[2];
   int32_t i;
}un_type_conv;
/*----------------------------------------------------------------------------
   Run time violation checks. Sets while module's
   runtime violation should be monitored
 *----------------------------------------------------------------------------*/
typedef enum
{
   MRC__ALL,
   MRC__MRA_START,
   MRC__MRA_END = MRC__MRA_START+80,

   MRC__MRB_START,
   MRC__MRB_END = MRC__MRB_START+40,

   MRC__CTA_START,
   MRC__CTA_END = MRC__CTA_START+30,

   MRC__CTB_START,
   MRC__CTB_END = MRC__CTB_START+30,

   MRC__COPA_START,
   MRC__COPA_END = MRC__COPA_START+30,

   MRC__COPB_START,
   MRC__COPB_END = MRC__COPB_START+30,

   NUM_MRC
}en_module_runtime_check;

/*----------------------------------------------------------------------------
   Possible recall door commands used in Auto_Recall().
   Specified what the door behavior will be after the car is recalled to a floor
 *----------------------------------------------------------------------------*/
typedef enum
{
   RECALL_DOOR_COMMAND__OPEN_NEITHER_DOOR,
   RECALL_DOOR_COMMAND__OPEN_FRONT_DOOR,
   RECALL_DOOR_COMMAND__OPEN_REAR_DOOR,
   RECALL_DOOR_COMMAND__OPEN_BOTH_DOORS,
   RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR,

   NUM_RECALL_DOOR_COMMAND
}en_recall_door_command;
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

extern struct st_module gstSys_Mod;
extern struct st_module gstMod_ParamEEPROM;
extern struct st_module gstMod_RTC;

extern struct st_param_block_info gstSys_ParamBlockInfo;

extern struct st_param_control gstParamControl;

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
   Internal EEPROM
 -----------------------------------------------------------------------------*/
void EEPROMI__Init( void );
int EEPROMI__Read( uint_fast8_t ucPage, void * pvPageData );
int EEPROMI__Write( uint_fast8_t ucPage, void * pvPageData );

/*-----------------------------------------------------------------------------
   Parameters
 -----------------------------------------------------------------------------*/
int Sys_Param_IsValid( int iBlockIndex, int iParamIndex );
uint8_t Sys_ParamChunk_IsValid( uint8_t ucBlockIndex, uint8_t ucChunk );
enum en_param_block_types Sys_Param_BlockType( int iBlockIndex );

void Param_WriteValue( struct st_param_and_value *pstParamAndValue );

uint32_t Param_ReadValue( struct st_param_and_value *pstParamAndValue );

uint32_t Param_ReadChunk( uint8_t ucBlockIndex,
                          uint8_t ucChunkIndex );//DON"T USE

void Param_WriteChunk( uint8_t ucBlockIndex,
                       uint8_t ucChunkIndex,
                       uint32_t ulData );//Dont use

uint32_t Param_GetBlockCRC_RAM( uint8_t ucBlockIndex );

uint32_t Param_GetBlockCRC_EEPROM( struct st_param_control *pstParamControl );

uint32_t Param_GetBlockCRC_Master( uint8_t ucBlockIndex );

uint32_t Param_PutBlockCRC_EEPROM( struct st_param_control *pstParamControl,
                                   uint8_t ucBlockIndex,
                                   uint32_t uiCRC
                                 );

uint32_t Param_PutBlockCRC_Master( struct st_param_control *pstParamControl,
                                   uint8_t ucBlockIndex,
                                   uint32_t uiCRC
                                 );

int Param_BlockToEEPROM_Page( struct st_param_block_info *pstParamBlockInfo, uint8_t ucBlockIndex );

void Param_CopyBlock_RAM_to_EEPROM( struct st_param_control *pstParamControl );

int Param_ParametersAreValid( void );// CALL ONLY IN MOD_PARAM_APP. USE gbFlashParamsReady otherwise

int Param_NumBlocks( void );

int Param_NumParams( int iBlockIndex );

void Param_NextParam( struct st_param_and_value *pstParam, uint_fast8_t bStayInBlock );

void Param_LookForOutOfDateBlock( struct st_param_control *pstParamControl );

void Param_SaveParameter( struct st_param_and_value *pstParamAndValue );

void Param_SaveParameterFromNode( struct st_param_and_value *pstParamAndValue,
                                  uint_fast8_t ucParamNodeType_SavingNode
                                );

void Param_WriteChunk( uint8_t ucBlockIndex,
      uint8_t ucChunkIndex,
      uint32_t ulData );

uint32_t Param_ReadChunk( uint8_t ucBlockIndex,
                          uint8_t ucChunkIndex );

uint32_t Param_ReadChunk_PEP( uint8_t ucBlockIndex,
                              uint8_t ucChunkIndex );

void UpdateBlockCRC( struct st_param_control *pstParamControl );// ONLY USE WITH MOD_PARAM FILES
/*-----------------------------------------------------------------------------
   Shared Data
 -----------------------------------------------------------------------------*/
int SDATA_DirtyBit_Get( struct st_sdata_control *pstSData_Control,
                        uint32_t uiDatagramIndex
                      );

void SDATA_DirtyBit_Set( struct st_sdata_control *pstSData_Control,
                         uint32_t uiDatagramIndex
                      );

void SDATA_DirtyBit_Clr( struct st_sdata_control *pstSData_Control,
                         uint32_t uiDatagramIndex
                      );

int SDATA_CreateDatagrams( struct st_sdata_control *pstSData_Control,
                           uint32_t uiNumDatagrams
                         );

int SDATA_WriteDatagram( struct st_sdata_control *pstSData_Control,
                         uint32_t uiDatagramIndex,
                         un_sdata_datagram *punDatagram
                       );

int SDATA_ReadDatagram( struct st_sdata_control *pstSData_Control,
                        uint32_t uiDatagramIndex,
                        un_sdata_datagram *punDatagram
                      );

uint16_t SDATA_GetNextDatagramToSendIndex_Plus1( struct st_sdata_control *pstSData_Control );

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
enum_sys_mcu_ids Sys_GetMCU_ID( void );

int32_t Sys_TickCount_Compare( int32_t iArg_TickCount,
                               enum enum_sys_tick_count_resolutions enResolution
                             );
int32_t Sys_GetTickCount( enum enum_sys_tick_count_resolutions enResolution );

void Sys_Init( void );




void Mod_InitAllModules( struct st_module_control * pstModuleControl );
void Mod_RunOneModule( struct st_module_control * pstModuleControl );

uint8_t  Sys_MCU_Pin_In( uint32_t uiPort, uint32_t uiPin );
void     Sys_MCU_Pin_Out( uint32_t uiPort, uint32_t uiPin, uint32_t bValue );
void     Sys_MCU_Pin_SetDir( uint32_t uiPort, uint32_t uiPin );
uint32_t Sys_MCU_Port_In( uint32_t uiPort );
void     Sys_MCU_Port_Out( uint32_t uiPort, uint32_t uiWriteEnableMask, uint32_t uiValue );

void    Sys_Bit_Set( void *pvBlock, uint32_t uiBitIndex, uint8_t bSet );
uint8_t Sys_Bit_Get( void * const pvBlock, uint32_t uiBitIndex );
void    Sys_Bit_Set_Array32( uint32_t * aulArray, uint8_t ucBitToSet, uint8_t ucArraySize, uint8_t bValue );
uint8_t Sys_Bit_Get_Array32( uint32_t * aulArray, uint8_t ucBitToSet, uint8_t ucArraySize );

void Sys_Byte_Set_Array32( uint32_t * aulArray, uint8_t ucByteToSet, uint8_t ucArraySize, uint8_t ucValue );
uint8_t Sys_Byte_Get_Array32( uint32_t * aulArray, uint8_t ucByteToSet, uint8_t ucArraySize );
int Sys_Shutdown( void );

#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
