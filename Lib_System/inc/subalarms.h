/******************************************************************************
 *
 * @file     alarms.h
 * @brief    System Alarms Header
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/
#ifndef _SUBALARMS_H_
#define _SUBALARMS_H_
/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

//-----------------------------------------------------------------------------
//Subalarm definitions
enum en_subalarm_default
{
   SUBALARM_DEFAULT__0,
   SUBALARM_DEFAULT__1,
   SUBALARM_DEFAULT__2,
   SUBALARM_DEFAULT__3,
   SUBALARM_DEFAULT__4,
   SUBALARM_DEFAULT__5,
   SUBALARM_DEFAULT__6,
   SUBALARM_DEFAULT__7,

   NUM_SUBALARM_DEFAULT
};
//-----------------------------------------------
enum en_subalarm_invaild_nts
{
   SUBALARM_INVALID_NTS__P1_VEL1,
   SUBALARM_INVALID_NTS__P1_VEL2,
   SUBALARM_INVALID_NTS__P1_VEL3,
   SUBALARM_INVALID_NTS__P1_VEL4,
   SUBALARM_INVALID_NTS__P1_VEL5,
   SUBALARM_INVALID_NTS__P1_VEL6,
   SUBALARM_INVALID_NTS__P1_VEL7,
   SUBALARM_INVALID_NTS__P1_VEL8,

   SUBALARM_INVALID_NTS__P2_VEL1,
   SUBALARM_INVALID_NTS__P2_VEL2,
   SUBALARM_INVALID_NTS__P2_VEL3,
   SUBALARM_INVALID_NTS__P2_VEL4,
   SUBALARM_INVALID_NTS__P2_VEL5,
   SUBALARM_INVALID_NTS__P2_VEL6,
   SUBALARM_INVALID_NTS__P2_VEL7,
   SUBALARM_INVALID_NTS__P2_VEL8,

   SUBALARM_INVALID_NTS__P3_VEL1,
   SUBALARM_INVALID_NTS__P3_VEL2,
   SUBALARM_INVALID_NTS__P3_VEL3,
   SUBALARM_INVALID_NTS__P3_VEL4,
   SUBALARM_INVALID_NTS__P3_VEL5,
   SUBALARM_INVALID_NTS__P3_VEL6,
   SUBALARM_INVALID_NTS__P3_VEL7,
   SUBALARM_INVALID_NTS__P3_VEL8,

   SUBALARM_INVALID_NTS__P4_VEL1,
   SUBALARM_INVALID_NTS__P4_VEL2,
   SUBALARM_INVALID_NTS__P4_VEL3,
   SUBALARM_INVALID_NTS__P4_VEL4,
   SUBALARM_INVALID_NTS__P4_VEL5,
   SUBALARM_INVALID_NTS__P4_VEL6,
   SUBALARM_INVALID_NTS__P4_VEL7,
   SUBALARM_INVALID_NTS__P4_VEL8,

   SUBALARM_INVALID_NTS__P1_POS1,
   SUBALARM_INVALID_NTS__P1_POS2,
   SUBALARM_INVALID_NTS__P1_POS3,
   SUBALARM_INVALID_NTS__P1_POS4,
   SUBALARM_INVALID_NTS__P1_POS5,
   SUBALARM_INVALID_NTS__P1_POS6,
   SUBALARM_INVALID_NTS__P1_POS7,
   SUBALARM_INVALID_NTS__P1_POS8,

   SUBALARM_INVALID_NTS__P2_POS1,
   SUBALARM_INVALID_NTS__P2_POS2,
   SUBALARM_INVALID_NTS__P2_POS3,
   SUBALARM_INVALID_NTS__P2_POS4,
   SUBALARM_INVALID_NTS__P2_POS5,
   SUBALARM_INVALID_NTS__P2_POS6,
   SUBALARM_INVALID_NTS__P2_POS7,
   SUBALARM_INVALID_NTS__P2_POS8,

   SUBALARM_INVALID_NTS__P3_POS1,
   SUBALARM_INVALID_NTS__P3_POS2,
   SUBALARM_INVALID_NTS__P3_POS3,
   SUBALARM_INVALID_NTS__P3_POS4,
   SUBALARM_INVALID_NTS__P3_POS5,
   SUBALARM_INVALID_NTS__P3_POS6,
   SUBALARM_INVALID_NTS__P3_POS7,
   SUBALARM_INVALID_NTS__P3_POS8,

   SUBALARM_INVALID_NTS__P4_POS1,
   SUBALARM_INVALID_NTS__P4_POS2,
   SUBALARM_INVALID_NTS__P4_POS3,
   SUBALARM_INVALID_NTS__P4_POS4,
   SUBALARM_INVALID_NTS__P4_POS5,
   SUBALARM_INVALID_NTS__P4_POS6,
   SUBALARM_INVALID_NTS__P4_POS7,
   SUBALARM_INVALID_NTS__P4_POS8,

   NUM_SUBALARM_INVALID_NTS
};

enum en_subalarm_sys_debug
{
   SUBALARM_SYS_DEBUG__ACCEL_CMD_T, // Pattern accel calc exceed time bounds
   SUBALARM_SYS_DEBUG__ACCEL_CMD_V, // Pattern accel calc exceed speed bounds
   SUBALARM_SYS_DEBUG__A_ACCEL_CMD_T, // Pattern added accel calc exceed time bounds
   SUBALARM_SYS_DEBUG__A_ACCEL_CMD_V, // Pattern added accel calc exceed speed bounds
   SUBALARM_SYS_DEBUG__DECEL_CMD_T,// Pattern decel calc exceed time bounds
   SUBALARM_SYS_DEBUG__DECEL_CMD_V,// Pattern decel calc exceed speed bounds

   SUBALARM_SYS_DEBUG__CAN_MRA_1,
   SUBALARM_SYS_DEBUG__CAN_MRA_2,
   SUBALARM_SYS_DEBUG__CAN_MRB_1,
   SUBALARM_SYS_DEBUG__CAN_MRB_2,
   SUBALARM_SYS_DEBUG__CAN_CTA_1,
   SUBALARM_SYS_DEBUG__CAN_CTA_2,
   SUBALARM_SYS_DEBUG__CAN_CTB_1,
   SUBALARM_SYS_DEBUG__CAN_CTB_2,
   SUBALARM_SYS_DEBUG__CAN_COPA_1,
   SUBALARM_SYS_DEBUG__CAN_COPA_2,
   SUBALARM_SYS_DEBUG__CAN_COPB_1,
   SUBALARM_SYS_DEBUG__CAN_COPB_2,
   SUBALARM_SYS_DEBUG__DEFAULTING_1BIT,
   SUBALARM_SYS_DEBUG__DEFAULTING_8BIT,
   SUBALARM_SYS_DEBUG__DEFAULTING_16BIT,
   SUBALARM_SYS_DEBUG__DEFAULTING_24BIT,
   SUBALARM_SYS_DEBUG__DEFAULTING_32BIT,
   SUBALARM_SYS_DEBUG__RSL_DECEL_CMD_T,// Pattern reduced speed decel calc exceed time bounds
   SUBALARM_SYS_DEBUG__RSL_DECEL_CMD_V,// Pattern reduced speed decel calc exceed speed bounds
   NUM_SUBALARM_SYS_DEBUG
};

enum en_subalarm_oos
{
   SUBALARM_OOS__START_OF_FAULTS,
   SUBALARM_OOS__HOURLY_LIMIT = 255,

   NUM_SUBALARM_OOS = 255
};

enum en_subalarm_invalid_manual_run
{
   SUBALARM_INVALID_MANUAL_RUN__CAR_DOOR,
   SUBALARM_INVALID_MANUAL_RUN__HALL_DOOR,
   SUBALARM_INVALID_MANUAL_RUN__ARM_CONTROL,
   SUBALARM_INVALID_MANUAL_RUN__DCB_F,
   SUBALARM_INVALID_MANUAL_RUN__DCB_R,
   SUBALARM_INVALID_MANUAL_RUN__DOB_F,
   SUBALARM_INVALID_MANUAL_RUN__DOB_R,
   SUBALARM_INVALID_MANUAL_RUN__HA_OPENING,
   SUBALARM_INVALID_MANUAL_RUN__CT_ENABLE,

   NUM_SUBALARM_INVALID_MANUAL_RUN
};

enum en_subalarm_can_bus_reset
{
  SUBALARM_CAN_BUS_RESET__MR_1,
  SUBALARM_CAN_BUS_RESET__MR_2,
  SUBALARM_CAN_BUS_RESET__MR_3,
  SUBALARM_CAN_BUS_RESET__MR_4,
  SUBALARM_CAN_BUS_RESET__CT_1,
  SUBALARM_CAN_BUS_RESET__CT_2,
  SUBALARM_CAN_BUS_RESET__CT_3,
  SUBALARM_CAN_BUS_RESET__CT_4,
  SUBALARM_CAN_BUS_RESET__COP_1,
  SUBALARM_CAN_BUS_RESET__COP_2,
  SUBALARM_CAN_BUS_RESET__COP_3,
  SUBALARM_CAN_BUS_RESET__COP_4,

  NUM_SUBALARM_CAN_BUS_RESET
};

typedef enum
{
   SUBALARM_INVALID_RECALL_DEST__FLOOR,
   SUBALARM_INVALID_RECALL_DEST__DOOR,
   SUBALARM_INVALID_RECALL_DEST__OPENING,
} en_subalarm_invalid_recall_dest;

//-----------------------------------------------

enum en_subalarm_ris_exp // en_ris_exp_error + 1
{
   SUBF_RIS_EXP_ALARM__OFFLINE,
   SUBF_RIS_EXP_ALARM__REPORTED_Start,
   NUM_SUBALARM_RIS_EXP_FAULT = SUBF_RIS_EXP_ALARM__REPORTED_Start + NUM_RIS_EXP_ERROR,
};
//-----------------------------------------------
typedef enum
{
   SUBA_OP_MODE__StartManualModes,

   SUBA_OP_MODE__StartAutoModes = SUBA_OP_MODE__StartManualModes + 20,

   NUM_SUBA_OP_MODE
}en_subalarm_op_mode;

//-----------------------------------------------
typedef enum
{
   SUBA_ESTOP__ClassOp,
   SUBA_ESTOP__AutoStopTimeout,
   SUBA_ESTOP__AutoMotionTimeout,
   SUBA_ESTOP__InvalidInsp,
   SUBA_ESTOP__RecallMissedDest,
   SUBA_ESTOP__StopAtNextLanding,
   SUBA_ESTOP__EQ,
   SUBA_ESTOP__FLOOD,

   NUM_SUBA_ESTOP
}en_subalarm_estop;
/*----------------------------------------------------------------------------

   MACROs for expansion/alignment of fault info

 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif


