#ifndef FPGA_SMARTRISE_ENGINEERING
#define FPGA_SMARTRISE_ENGINEERING

#include "sys.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define FPGA_REDUNDANCY_DEBOUNCE_5MS      (200)
#define FPGA_BYTE_EXTRACT_DATA(x)         ( (x & 0x7E) >> 1 ) // First and last bit of every FPGA data byte is dropped
#define FPGA_DATA_BITS_PER_BYTE           (6) // First and last bit of every FPGA data byte is dropped

#define FPGA_ENABLE_NEW_FAULTS            (1)

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   FPGA_LOC__MRA,
   FPGA_LOC__CTA,
   FPGA_LOC__COPA,

   NUM_FPGA_LOC
} en_fpga_locations;

/*
   Data Format (MR):
      SOF       (8’hFF);                                                             -- Start of Frame
      DATA0     (1’b0, relayStatus_RGP, relayControl_RGP, relayStatus_SFM, relayControl_SFM, relayStatus_SFP, relayControl_SFP, Parity0)
      DATA1     (1’b0, relayStatus_RGBM, relayControlMcu_RGB, relayStatus_RGBP, relayControl_RGB, relayStatus_RGM, relayControl_RGM, Parity1)
      DATA2     (1’b0, access_BottomUp, access_TopDown, access_TopUp, inspection_MachineRoom, inspection_Landing, inspection_Pit, Parity2)
      DATA3     (1’b0, lock_RearMiddle, lock_RearTop, bypass_CarDoors, bypass_HallDoors, CPLD_MM, access_BottomDown, Parity3)
      DATA4     (1’b0, nonBypass_Governor, CPLD_120V, lock_FrontBottom, lock_FrontMiddle, lock_FrontTop, lock_RearBottom, Parity4)
      DATA5     (1’b0, nonBypass_MachineRoom, nonBypass_Hoistway, nonBypass_BottomFinalLimit, nonBypass_TopFinalLimit, nonBypass_Buffer, nonBypass_Pit, Parity5)
      DATA6     (1’b0, DIP_B_6, DIP_B_5, DIP_B_4, DIP_B_3, DIP_B_2, DIP_B_1, Parity6)
      DATA7     (1’b0, w_preflightCommandToMCU[2:0], NTS_Status, DIP_B_8, DIP_B_7, Parity7)
      DATA8     (1’b0, debugPreflightError[3:0], w_preflightStatus[1:0], Parity8)
      DATA9     (1’b0, 6'b00_0000, Parity9)                       -- unused
      DATA10    (1’b0, status_System[5:0], Parity10)
      DATA11    (1’b0, 6'b01_0001, Parity11)                    -- CPLD version = 11 hex
      DATA12    (1’b0, 6'b00_0000, Parity12)                    -- unused
      BCC       (BCC)                                                                 -- BCC byte
      EOF       (8’h80);                                                             -- End of Frame

   Data Format (CT):
      SOF       (8’hFF);                                                             -- Start of Frame
      DATA0     (1’b0, GSWR, GSWF, SAFE4, SAFE3, SAFE2, SAFE1, Parity0)
      DATA1     (1’b0, DIP_B_4, DIP_B_3, DIP_B_2, DIP_B_1, DZR, DZF, Parity1)
      DATA2     (1’b0, 2'b00, DIP_B_8, DIP_B_7, DIP_B_6, DIP_B_5, Parity2)
      DATA3     (1’b0, 6'b00_0000, Parity3)
      DATA4     (1’b0, 6'b00_0000, Parity4)
      DATA5     (1’b0, 6'b00_0000, Parity5)
      DATA6     (1’b0, 6'b00_0000, Parity6)
      DATA7     (1’b0, w_preflightCommandToMCU[2:0], 3'b000, Parity7)
      DATA8     (1’b0, debugPreflightError[3:0], w_preflightStatus[1:0], Parity8)
      DATA9     (1’b0, 6'b00_0000, Parity9)                       -- unused
      DATA10    (1’b0, status_System[5:0], Parity10)
      DATA11    (1’b0, 6'b02_0001, Parity11)                    -- CPLD version = 21 hex
      DATA12    (1’b0, 6'b00_0000, Parity12)                    -- unused
      BCC       (BCC)                                                                 -- BCC byte
      EOF       (8’h80);                                                             -- End of Frame

   Data Format (COP):
      SOF       (8’hFF);                                                             -- Start of Frame
      DATA0     (1’b0, DIP_B_2, DIP_B_1, SAFE4, SAFE3, SAFE2, SAFE1, Parity0)
      DATA1     (1’b0, DIP_B_8, DIP_B_7, DIP_B_6, DIP_B_5, DIP_B_4, DIP_B_3, Parity1)
      DATA2     (1’b0, 6'b00_0000, Parity2)
      DATA3     (1’b0, 6'b00_0000, Parity3)
      DATA4     (1’b0, 6'b00_0000, Parity4)
      DATA5     (1’b0, 6'b00_0000, Parity5)
      DATA6     (1’b0, 6'b00_0000, Parity6)
      DATA7     (1’b0, 6'b00_0000, Parity7)
      DATA8     (1’b0, debugPreflightError[3:0], w_preflightStatus[1:0], Parity8)
      DATA9     (1’b0, 6'b00_0000, Parity9)                       -- unused
      DATA10    (1’b0, status_System[5:0], Parity10)
      DATA11    (1’b0, 6'b03_0001, Parity11)                    -- CPLD version = 31 hex
      DATA12    (1’b0, 6'b00_0000, Parity12)                    -- unused
      BCC       (BCC)                                                                 -- BCC byte
      EOF       (8’h80);                                                             -- End of Frame
*/
typedef enum
{
   FPGA_BYTE__DATA0,
   FPGA_BYTE__DATA1,
   FPGA_BYTE__DATA2,
   FPGA_BYTE__DATA3,
   FPGA_BYTE__DATA4,
   FPGA_BYTE__DATA5,
   FPGA_BYTE__DATA6,
   FPGA_BYTE__DATA7,
   FPGA_BYTE__DATA8,
   FPGA_BYTE__DATA9,
   FPGA_BYTE__DATA10,
   FPGA_BYTE__DATA11,
   FPGA_BYTE__DATA12,

   NUM_FPGA_BYTES
} en_fpga_bytes;

typedef enum
{
   FPGA_BITS_MR_V2__RELAY_C_SFP,
   FPGA_BITS_MR_V2__RELAY_M_SFP,
   FPGA_BITS_MR_V2__RELAY_C_SFM,
   FPGA_BITS_MR_V2__RELAY_M_SFM,
   FPGA_BITS_MR_V2__RELAY_C_EB1,
   FPGA_BITS_MR_V2__RELAY_M_EB1,

   FPGA_BITS_MR_V2__RELAY_C_EB2,
   FPGA_BITS_MR_V2__RELAY_M_EB2,
   FPGA_BITS_MR_V2__RELAY_C_EB3,
   FPGA_BITS_MR_V2__RELAY_M_EB3,
   FPGA_BITS_MR_V2__RELAY_C_EB4,
   FPGA_BITS_MR_V2__RELAY_M_EB4,

   FPGA_BITS_MR_V2__INSP_PIT,
   FPGA_BITS_MR_V2__INSP_LAND,
   FPGA_BITS_MR_V2__INSP_MR,
   FPGA_BITS_MR_V2__ATU,
   FPGA_BITS_MR_V2__ATD,
   FPGA_BITS_MR_V2__ABU,

   FPGA_BITS_MR_V2__ABD,
   FPGA_BITS_MR_V2__MM,
   FPGA_BITS_MR_V2__BYP_H,
   FPGA_BITS_MR_V2__BYP_C,
   FPGA_BITS_MR_V2__LRT,
   FPGA_BITS_MR_V2__LRM,

   FPGA_BITS_MR_V2__LRB,
   FPGA_BITS_MR_V2__LFT,
   FPGA_BITS_MR_V2__LFM,
   FPGA_BITS_MR_V2__LFB,
   FPGA_BITS_MR_V2__120VAC,
   FPGA_BITS_MR_V2__GOV,

   FPGA_BITS_MR_V2__PIT,
   FPGA_BITS_MR_V2__BUF,
   FPGA_BITS_MR_V2__TFL,
   FPGA_BITS_MR_V2__BFL,
   FPGA_BITS_MR_V2__SFH,
   FPGA_BITS_MR_V2__SFM,

   FPGA_BITS_MR_V2__DIP_B1,
   FPGA_BITS_MR_V2__DIP_B2,
   FPGA_BITS_MR_V2__DIP_B3,
   FPGA_BITS_MR_V2__DIP_B4,
   FPGA_BITS_MR_V2__DIP_B5,
   FPGA_BITS_MR_V2__DIP_B6,

   FPGA_BITS_MR_V2__DIP_B7,
   FPGA_BITS_MR_V2__DIP_B8,
   FPGA_BITS_MR_V2__NTS_STATUS,
   END_OF_INPUTS__FPGA_BITS_MR_V2,
   FPGA_BITS_MR_V2__PREFLIGHT_CMD0 = END_OF_INPUTS__FPGA_BITS_MR_V2,
   FPGA_BITS_MR_V2__PREFLIGHT_CMD1,
   FPGA_BITS_MR_V2__PREFLIGHT_CMD2,

   FPGA_BITS_MR_V2__PREFLIGHT_STATUS0,
   FPGA_BITS_MR_V2__PREFLIGHT_STATUS1,
   FPGA_BITS_MR_V2__UNUSED50,
   FPGA_BITS_MR_V2__UNUSED51,
   FPGA_BITS_MR_V2__UNUSED52,
   FPGA_BITS_MR_V2__UNUSED53,

   FPGA_BITS_MR_V2__UNUSED54,
   FPGA_BITS_MR_V2__UNUSED55,
   FPGA_BITS_MR_V2__UNUSED56,
   FPGA_BITS_MR_V2__UNUSED57,
   FPGA_BITS_MR_V2__UNUSED58,
   FPGA_BITS_MR_V2__UNUSED59,

   FPGA_BITS_MR_V2__FAULT0,
   FPGA_BITS_MR_V2__FAULT1,
   FPGA_BITS_MR_V2__FAULT2,
   FPGA_BITS_MR_V2__FAULT3,
   FPGA_BITS_MR_V2__FAULT4,
   FPGA_BITS_MR_V2__FAULT5,

   FPGA_BITS_MR_V2__VERSION0,
   FPGA_BITS_MR_V2__VERSION1,
   FPGA_BITS_MR_V2__VERSION2,
   FPGA_BITS_MR_V2__VERSION3,
   FPGA_BITS_MR_V2__VERSION4,
   FPGA_BITS_MR_V2__VERSION5,

   FPGA_BITS_MR_V2__UNUSED72,
   FPGA_BITS_MR_V2__UNUSED73,
   FPGA_BITS_MR_V2__UNUSED74,
   FPGA_BITS_MR_V2__UNUSED75,
   FPGA_BITS_MR_V2__UNUSED76,
   FPGA_BITS_MR_V2__UNUSED77,

   NUM_FPGA_BITS_MR_V2
} en_fpga_bits_mr_v2;

typedef enum
{
   FPGA_BITS_MR_V1__RELAY_M_EB3,
   FPGA_BITS_MR_V1__RELAY_M_EB1,
   FPGA_BITS_MR_V1__RELAY_M_SFP,
   FPGA_BITS_MR_V1__RELAY_C_EB3,
   FPGA_BITS_MR_V1__RELAY_C_EB1,
   FPGA_BITS_MR_V1__RELAY_C_SFP,

   FPGA_BITS_MR_V1__INSP_MR,
   FPGA_BITS_MR_V1__INSP_LAND,
   FPGA_BITS_MR_V1__RELAY_C_EB2,
   FPGA_BITS_MR_V1__RELAY_C_SFM,
   FPGA_BITS_MR_V1__RELAY_M_EB2,
   FPGA_BITS_MR_V1__RELAY_M_SFM,

   FPGA_BITS_MR_V1__UNUSED12,
   FPGA_BITS_MR_V1__MM,
   FPGA_BITS_MR_V1__SFM,
   FPGA_BITS_MR_V1__SFH,
   FPGA_BITS_MR_V1__PIT,
   FPGA_BITS_MR_V1__INSP_PIT,

   FPGA_BITS_MR_V1__ATU,
   FPGA_BITS_MR_V1__ATD,
   FPGA_BITS_MR_V1__ABU,
   FPGA_BITS_MR_V1__ABD,
   FPGA_BITS_MR_V1__BYP_C,
   FPGA_BITS_MR_V1__BYP_H,

   FPGA_BITS_MR_V1__LRM,
   FPGA_BITS_MR_V1__LRT,
   FPGA_BITS_MR_V1__LFB,
   FPGA_BITS_MR_V1__LFM,
   FPGA_BITS_MR_V1__LFT,
   FPGA_BITS_MR_V1__UNUSED29,

   FPGA_BITS_MR_V1__UNUSED30,
   FPGA_BITS_MR_V1__UNUSED31,
   FPGA_BITS_MR_V1__UNUSED32,
   FPGA_BITS_MR_V1__UNUSED33,
   FPGA_BITS_MR_V1__UNUSED34,
   FPGA_BITS_MR_V1__LRB,

   NUM_FPGA_BITS_MR_V1
} en_fpga_bits_mr_v1;

typedef enum
{
   FPGA_BITS_CT__CT_SW,
   FPGA_BITS_CT__ESC_HATCH,
   FPGA_BITS_CT__CAR_SAFE,
   FPGA_BITS_CT__CT_INSP,
   FPGA_BITS_CT__GSWF,
   FPGA_BITS_CT__GSWR,

   FPGA_BITS_CT__DZF,
   FPGA_BITS_CT__DZR,
   FPGA_BITS_CT__DIP_B1,
   FPGA_BITS_CT__DIP_B2,
   FPGA_BITS_CT__DIP_B3,
   FPGA_BITS_CT__DIP_B4,

   FPGA_BITS_CT__DIP_B5,
   FPGA_BITS_CT__DIP_B6,
   FPGA_BITS_CT__DIP_B7,
   FPGA_BITS_CT__DIP_B8,
   END_OF_INPUTS__FPGA_BITS_CT,
   NUM_FPGA_BITS_CT = END_OF_INPUTS__FPGA_BITS_CT,
} en_fpga_bits_ct;

typedef enum
{
   FPGA_BITS_COP__HA_INSP,
   FPGA_BITS_COP__IC_ST,
   FPGA_BITS_COP__FSS,
   FPGA_BITS_COP__IC_INSP,
   FPGA_BITS_COP__DIP_B1,
   FPGA_BITS_COP__DIP_B2,

   FPGA_BITS_COP__DIP_B3,
   FPGA_BITS_COP__DIP_B4,
   FPGA_BITS_COP__DIP_B5,
   FPGA_BITS_COP__DIP_B6,
   FPGA_BITS_COP__DIP_B7,
   FPGA_BITS_COP__DIP_B8,
   END_OF_INPUTS__FPGA_BITS_COP,
   NUM_FPGA_BITS_COP = END_OF_INPUTS__FPGA_BITS_COP,
} en_fpga_bits_cop;
typedef enum
{
  PREFLIGHT_CMD__INACTIVE,
  PREFLIGHT_CMD__UNUSED1,
  PREFLIGHT_CMD__UNUSED2,
  PREFLIGHT_CMD__UNUSED3,
  PREFLIGHT_CMD__PICK_BYPASS,
  PREFLIGHT_CMD__DROP_GRIPPER,
  PREFLIGHT_CMD__PICK_GRIPPER,
  PREFLIGHT_CMD__DROP_BYPASS,

  NUM_PREFLIGHT_CMD
} PreflightMCU_Command;

typedef enum
{
    PREFLIGHT_STATUS__INACTIVE,
    PREFLIGHT_STATUS__ACTIVE,
    PREFLIGHT_STATUS__PASS,
    PREFLIGHT_STATUS__FAIL,

    NUM_PREFLIGHT_STATUS
}PreflightStatus;

/* Debugging field for preflight errors */
typedef enum
{
   PREFLIGHT_ERROR__NONE,
   PREFLIGHT_ERROR__C_PICK_BYP,
   PREFLIGHT_ERROR__M_PICK_BYP,
   PREFLIGHT_ERROR__M_DROP_GRIP,
   PREFLIGHT_ERROR__C_DROP_GRIP,
   PREFLIGHT_ERROR__C_PICK_GRIP,
   PREFLIGHT_ERROR__M_PICK_GRIP,
   PREFLIGHT_ERROR__M_DROP_BYP,
   PREFLIGHT_ERROR__C_DROP_BYP,
   PREFLIGHT_ERROR__COMPARISON,

   NUM_PREFLIGHT_ERRORS
}PreflightErrors;

typedef enum
{
   FPGA_FAULT_MR_V2__NONE,
   FPGA_FAULT_MR_V2__STARTUP_MR,
   FPGA_FAULT_MR_V2__STARTUP_CT,
   FPGA_FAULT_MR_V2__STARTUP_COP,
   FPGA_FAULT_MR_V2__UNINTENDED_MOV,
   FPGA_FAULT_MR_V2__COMM_LOSS_CT,
   FPGA_FAULT_MR_V2__COMM_LOSS_COP,
   FPGA_FAULT_MR_V2__120VAC_LOSS,
   FPGA_FAULT_MR_V2__GOVERNOR_LOSS,
   FPGA_FAULT_MR_V2__CAR_BYPASS_SW,
   FPGA_FAULT_MR_V2__HALL_BYPASS_SW,
   FPGA_FAULT_MR_V2__SFM_LOSS,
   FPGA_FAULT_MR_V2__SFH_LOSS,
   FPGA_FAULT_MR_V2__PIT_LOSS,
   FPGA_FAULT_MR_V2__BUF_LOSS,
   FPGA_FAULT_MR_V2__TFL_LOSS,
   FPGA_FAULT_MR_V2__BFL_LOSS,
   FPGA_FAULT_MR_V2__CT_SW_LOSS,
   FPGA_FAULT_MR_V2__ESC_HATCH_LOSS,
   FPGA_FAULT_MR_V2__CAR_SAFE_LOSS,
   FPGA_FAULT_MR_V2__IC_STOP_SW,
   FPGA_FAULT_MR_V2__FIRE_STOP_SW,
   FPGA_FAULT_MR_V2__INSPECTION,
   FPGA_FAULT_MR_V2__ACCESS,
   FPGA_FAULT_MR_V2__LOCK_FRONT_TOP,
   FPGA_FAULT_MR_V2__LOCK_FRONT_MID,
   FPGA_FAULT_MR_V2__LOCK_FRONT_BOT,
   FPGA_FAULT_MR_V2__LOCK_REAR_TOP,
   FPGA_FAULT_MR_V2__LOCK_REAR_MID,
   FPGA_FAULT_MR_V2__LOCK_REAR_BOT,
   FPGA_FAULT_MR_V2__GATESWITCH_FRONT,
   FPGA_FAULT_MR_V2__GATESWITCH_REAR,
   FPGA_FAULT_MR_V2__PF_PIT_INSP,
   FPGA_FAULT_MR_V2__PF_LND_INSP,
   FPGA_FAULT_MR_V2__PF_BFL,
   FPGA_FAULT_MR_V2__PF_TFL,
   FPGA_FAULT_MR_V2__PF_BUF,
   FPGA_FAULT_MR_V2__PF_PIT,
   FPGA_FAULT_MR_V2__PF_GOV,
   FPGA_FAULT_MR_V2__PF_SFH,
   FPGA_FAULT_MR_V2__PF_SFM,
   FPGA_FAULT_MR_V2__PF_LFT,
   FPGA_FAULT_MR_V2__PF_LFM,
   FPGA_FAULT_MR_V2__PF_LFB,
   FPGA_FAULT_MR_V2__PF_LRT,
   FPGA_FAULT_MR_V2__PF_LRM,
   FPGA_FAULT_MR_V2__PF_LRB,
   FPGA_FAULT_MR_V2__PF_BYP_H,
   FPGA_FAULT_MR_V2__PF_BYP_C,
   FPGA_FAULT_MR_V2__PF_MR_INSP,
   FPGA_FAULT_MR_V2__PF_CPLD_PICK_BYPASS,
   FPGA_FAULT_MR_V2__PF_MCU_PICK_BYPASS,
   FPGA_FAULT_MR_V2__PF_MCU_DROP_GRIPPER,
   FPGA_FAULT_MR_V2__PF_CPLD_DROP_GRIPPER,
   FPGA_FAULT_MR_V2__PF_CPLD_PICK_GRIPPER,
   FPGA_FAULT_MR_V2__PF_MCU_PICK_GRIPPER,
   FPGA_FAULT_MR_V2__PF_MCU_DROP_BYPASS,
   FPGA_FAULT_MR_V2__PF_CPLD_DROP_BYPASS,

   NUM_FPGA_FAULTS_MR_V2

}FPGA_Fault_MR_V2;
typedef enum
{
   FPGA_FAULT_MR_V1_None,
   FPGA_FAULT_MR_V1_InStartup,
   FPGA_FAULT_MR_V1_UnintendedMovement,
   FPGA_FAULT_MR_V1_Governor,
   FPGA_FAULT_MR_V1_Redundancy,
   FPGA_FAULT_MR_V1_CommLoss,
   FPGA_FAULT_MR_V1_NonBypass,
   FPGA_FAULT_MR_V1_InCar,
   FPGA_FAULT_MR_V1_Inspection,
   FPGA_FAULT_MR_V1_SFHFeedback,
   FPGA_FAULT_MR_V1_RopeGripperFeedback,
   FPGA_FAULT_MR_V1_Access,
   FPGA_FAULT_MR_V1_Locks,
   FPGA_FAULT_MR_V1_Doors,
   FPGA_FAULT_MR_V1_BypassSwitches,
   FPGA_FAULT_MR_V1_Preflight,

   NUM_FPGA_FAULTS_MR_V1
}FPGA_Fault_MR_V1;

typedef enum
{
   FPGA_FAULT_CT__NONE,
   FPGA_FAULT_CT__STARTUP,
   FPGA_FAULT_CT__PF_CT_SW,
   FPGA_FAULT_CT__PF_ESC_HATCH,
   FPGA_FAULT_CT__PF_CAR_SAFE,
   FPGA_FAULT_CT__PF_CT_INSP,
   FPGA_FAULT_CT__PF_GSWF,
   FPGA_FAULT_CT__PF_GSWR,
   FPGA_FAULT_CT__PF_DZF,
   FPGA_FAULT_CT__PF_DZR,

   NUM_FPGA_FAULTS_CT
}FPGA_Fault_CT;

typedef enum
{
   FPGA_FAULT_COP__NONE,
   FPGA_FAULT_COP__STARTUP,
   FPGA_FAULT_COP__PF_HA_INSP,
   FPGA_FAULT_COP__PF_IC_ST,
   FPGA_FAULT_COP__PF_FSS,
   FPGA_FAULT_COP__PF_IC_INSP,

   NUM_FPGA_FAULTS_COP
}FPGA_Fault_COP;

typedef struct
{
    uint8_t aucData[NUM_FPGA_BYTES];
}FPGA_Packet;

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

// Same for all MR and SRU boards. This sets up the FPGA
void FPGA_Init_V1_V2( void );
void FPGA_Init_V3( void );

/*----------------------------------------------------------------------------
   Buffer overflow flag access functions
 *----------------------------------------------------------------------------*/
uint8_t FPGA_GetBufferOverflowFlag(void);
void FPGA_ClrBufferOverflowFlag(void);
/*----------------------------------------------------------------------------
   Access functions for the most recently unloaded packets.
 *----------------------------------------------------------------------------*/
void FPGA_SetRecentPacket( FPGA_Packet *pstPacket, en_fpga_locations eLoc );
void FPGA_GetRecentPacket( FPGA_Packet *pstPacket, en_fpga_locations eLoc );
void FPGA_UnloadUIData( uint32_t uiData, uint8_t ucIndex, en_fpga_locations eLoc );
uint8_t FPGA_GetFault( en_fpga_locations eLoc );
PreflightStatus FPGA_GetPreflightStatus( en_fpga_locations eLoc );
PreflightMCU_Command FPGA_GetPreflightCommand( en_fpga_locations eLoc );
PreflightErrors FPGA_GetPreflightErrors( en_fpga_locations eLoc );
uint8_t FPGA_GetVersion( en_fpga_locations eLoc );
uint8_t FPGA_GetDataBit( en_fpga_locations eLoc, uint8_t ucBit );
uint8_t FPGA_GetDataRange( en_fpga_locations eLoc, uint8_t ucBit_Start, uint8_t ucBit_End );
/*----------------------------------------------------------------------------
   Checks if a full FPGA packet has been recieved. If so, unloads
   it from the ring buffer and passes it to a global access buffer
 *---------------------------------------------------------------------------*/
uint8_t FPGA_UnloadData( en_fpga_locations eLoc );

#endif
