#ifndef _POSITION_H_
#define _POSITION_H_

#include "sys.h"
#include "network2.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define POS_FWD_DG_ID                        ( 2 )
#define POS_FWD_SRC_ID                       (3)//( SYS_NODE__CTA )
#define POS_FWD_DST_ID                       ( 0x7FFF )
#define POS_FWD_NET_ID                       ( SDATA_NET__CAR )
#define POS_FWD_MSG_ID                       ( GET_EXT_CAN_MSG_ID(POS_FWD_DG_ID, POS_FWD_DST_ID, POS_FWD_SRC_ID, POS_FWD_NET_ID ))


#define POS_FWD_CAN_ID                       (LPC_CAN1)
#define POS_FWD_PACKET_SIZE_BYTES            (8U)


#define CEDES_OFFLINE_LIMIT_1MS     (200U)
#define CEDES_INVALID_VELOCITY      (0x7FFF)
#define CEDES_INVALID_POSITION      (0xFFFFFF)
#define CEDES_SPEED_TO_FPM          (5.08f) // mm/s to fpm
//#define CEDES_OFFLINE_LIMIT_1MS     (100U)
#define CEDES_ERROR_LIMIT               (8U)
#define CEDES_POS_ERROR_LIMIT           (10U) /* Used for C4's redundant check for consecutive invalid position values from the CEDES camera */
/* Position offset between two cedes camera channels */
#define CHANNEL_POS_COUNT_OFFSET        (45U)

#define SCALE_AB_POSITION_UPDATE_RATE    (20)

#define HALF_MM_PER_FT     (609.6f)     /* To convert from feet to Cedes pos counts */
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
enum en_position_fault
{
   POSITION_FAULT__NONE,
   POSITION_FAULT__COMM_TIMEOUT,
   POSITION_FAULT__POSITION_REPORTED_FAULT,

   NUM_POSITION_FAULTS
};


struct st_position
{
   uint32_t ulPosCount;
   int16_t wVelocity;
};
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void SetPosition_PositionCount( uint32_t uiPositionCount );
void SetPosition_Velocity( int16_t wVelocity );

uint32_t GetPosition_PositionCount();
int16_t GetPosition_Velocity();

uint8_t CheckIfInDoorZone(void);

#endif
