/******************************************************************************
 *
 * @file     contributors.h
 * @brief    People who contributed to the C4 project
 * @version  V1.00
 * @date     6, July 2018
 *
 * @note
 *
 ******************************************************************************/
#ifndef _CONTRIBUTORS_H_
#define _CONTRIBUTORS_H_
/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define FLAG_SEQUENCE_HOLD_TIME_5MS         (1000)
#define FLAG_SEQUENCE_RESET_LIMIT_5MS       (6000)
#define FLAG_SEQUENCE_PRESS_LIMIT            (3)
#define FLAG_SEQUENCE_FLASH_RATE_5MS         (60)
#define FLAG_SEQUENCE_CYCLES_TO_FLASH        (6)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

typedef enum
{
   C4C__KEITH_SONEDA,
   C4C__SAM_NEEL,
   C4C__SEAN_ALLING,
   C4C__RYAN_PAVUSKO,
   C4C__SANG_HO,
   C4C__WALENG_VANG,
   C4C__BEN_KOOP,

   NUM_C4C
}en_c4_contributors;

/* For simplicity only those who worked on the project longer
 * than a year or made significant contributions to the code
 * base were added */

/* Table Definition - Limit 3 characters per person */
#define C4C_TABLE(C4C_ENTRY)  \
   C4C_ENTRY( C4C__KEITH_SONEDA,             " KS" ) \
   C4C_ENTRY( C4C__SAM_NEEL,                 " SN" ) \
   C4C_ENTRY( C4C__SEAN_ALLING,              " SA" ) \
   C4C_ENTRY( C4C__RYAN_PAVUSKO,             " RP" ) \
   C4C_ENTRY( C4C__SANG_HO,                  " SH" ) \
   C4C_ENTRY( C4C__WALENG_VANG,              " WV" ) \
   C4C_ENTRY( C4C__BEN_KOOP,                 " BK" )

/* Macros for accessing table elements */
#define EXPAND_C4C_TABLE_AS_ENUMERATION(A, B) A,
#define EXPAND_C4C_TABLE_AS_STRING_ARRAY(A, B) B,
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif

