/******************************************************************************
 *
 * @file     faults.h
 * @brief    System Faults Header
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/
#ifndef _SUBFAULTS_H_
#define _SUBFAULTS_H_
/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
    Subfault Enumerations - LIMIT 254

    For per board faults (Board Reset, Watchdog Reset, FaultApp, etc)
    use ENUM: enum en_fault_nodes
 *----------------------------------------------------------------------------*/
//-----------------------------------------------
enum en_subfault_default
{
   SUBF_DEFAULT__1,
   SUBF_DEFAULT__2,
   SUBF_DEFAULT__3,
   SUBF_DEFAULT__4,
   SUBF_DEFAULT__5,
   SUBF_DEFAULT__6,
   SUBF_DEFAULT__7,
   SUBF_DEFAULT__8,
   NUM_SUBF_DEFAULT
};
//-----------------------------------------------
enum en_subfault_none
{
   SUBF_NONE__1,

   NUM_SUBF_NONE
};

//-----------------------------------------------

//-----------------------------------------------
enum en_subfault_board_reset
{
   SUBF_BOARD_RESET__POR,
   SUBF_BOARD_RESET__WDT,

   NUM_SUBF_BOARD_RESET,
};

//-----------------------------------------------
enum en_subfault_gsw_open
{
   SUBF__GSW_F,
   SUBF__GSW_R,

   NUM_SUBF_GSW
};

//-----------------------------------------------
enum en_subfault_lock_jumper
{
   SUBF__JUMPER_ON_LOCKS_F,
   SUBF__JUMPER_ON_LOCKS_R,

   NUM_SUBF_JUMPER_ON_LOCKS
};

//-----------------------------------------------
enum en_subfault_fuse_blown
{
   SUBF__FUSE_BLOWN_ZC,
   SUBF__FUSE_BLOWN_ZM,
   SUBF__FUSE_BLOWN_ZP,
   SUBF__FUSE_BLOWN_ZH,
   SUBF__FUSE_BLOWN_EB,

   NUM_SUBF_FUSE_BLOWN
};

//-----------------------------------------------
enum en_subfault_safety_string
{
   SUBF__SAFETY_STRING_ZC,//todo rm
   SUBF__SAFETY_STRING_ZM,
   SUBF__SAFETY_STRING_ZP,//todo rm
   SUBF__SAFETY_STRING_ZH,
   SUBF__SAFETY_STRING_PIT,
   SUBF__SAFETY_STRING_BUF,
   SUBF__SAFETY_STRING_TFL,
   SUBF__SAFETY_STRING_BFL,

   SUBF__SAFETY_STRING_CTSW,
   SUBF__SAFETY_STRING_ESC_HATCH_SW,
   SUBF__SAFETY_STRING_CAR_SAFETIES,
   NUM_SUBF_SAFETY_STRING
};

//-----------------------------------------------
enum en_subfault_hall_locks
{
   SUBF__HALL_LOCKS_LTF,
   SUBF__HALL_LOCKS_LMF,
   SUBF__HALL_LOCKS_LBF,
   SUBF__HALL_LOCKS_LTR,
   SUBF__HALL_LOCKS_LMR,
   SUBF__HALL_LOCKS_LBR,

   NUM_SUBF_HALL_LOCKS
};

//-----------------------------------------------
enum en_subfault_invalid_speed
{
  SUBF__INVALID_SPD_CONTRACT,//0x01
  SUBF__INVALID_SPD_INSPECT,
  SUBF__INVALID_SPD_LEARN,
  SUBF__INVALID_SPD_TERMINAL,
  SUBF__INVALID_SPD_LEVEL,
  SUBF__INVALID_SPD_NTSD,

  NUM_SUBF__INVALID_SPD,
};

// For FAULT__MOTION subfaults will be the start sequence
// mode that failed, enum en_motion_start_sequence.
//-----------------------------------------------
enum en_subfault_bps
{
   SUBF__INVALID_BPS_RUN_ON,
   SUBF__INVALID_BPS_STOPPED,

   NUM_SUBF__INVALID_BPS,
};

//-----------------------------------------------
enum en_cedes_error // fault reported by cedes
{
   CEDES_ERROR__READ_FAIL,
   CEDES_ERROR__ALIGN_CLOSE,
   CEDES_ERROR__ALIGN_FAR,
   CEDES_ERROR__ALIGN_LEFT,
   CEDES_ERROR__ALIGN_RIGHT,
   CEDES_ERROR__INTERNAL,
   CEDES_ERROR__COMMUNICATION,
   CEDES_ERROR__CROSS1_POS,
   CEDES_ERROR__CROSS1_VEL,
   CEDES_ERROR__CROSS1_BOTH,
   CEDES_ERROR__CROSS2_POS,
   CEDES_ERROR__CROSS2_VEL,
   CEDES_ERROR__CROSS2_BOTH,
   // TODO add
   NUM_CEDES_ERRORS
};
enum en_subfault_cedes
{
   SUBF_CEDES_FAULT__OFFLINE,
   SUBF_CEDES_FAULT__CRC,
   SUBF_CEDES_FAULT__REPORTED_Start,

   NUM_SUBF_CEDES_FAULT = SUBF_CEDES_FAULT__REPORTED_Start + NUM_CEDES_ERRORS,
   SUBF_CEDES_FAULT__UNKNOWN,
};
//-----------------------------------------------
enum en_drive_fault//todo rm
{
   DRIVE_FAULT__NONE,
   DRIVE_FAULT__COMM_TIMEOUT,
   DRIVE_FAULT__DRIVE_REPORTED_FAULT,

   NUM_DRIVE_FAULTS
};

enum en_drive_error_dsd
{
   DRIVE_ERROR_DSD__NONE,
   DRIVE_ERROR_DSD__NOT_READY,
   DRIVE_ERROR_DSD__TACH_OVERSPEED_F97,
   DRIVE_ERROR_DSD__TACH_LOSS_F98,
   DRIVE_ERROR_DSD__TACH_REV_F99,
   DRIVE_ERROR_DSD__OVERLOAD_F400,
   DRIVE_ERROR_DSD__FIELD_CURR_F401,
   DRIVE_ERROR_DSD__CONTACTOR_F402,
   DRIVE_ERROR_DSD__CEMF_F407_8,
   DRIVE_ERROR_DSD__ESTOP_F405,
   DRIVE_ERROR_DSD__LOOP_F900,
   DRIVE_ERROR_DSD__PCU_IST_F901,
   DRIVE_ERROR_DSD__LINE_SYNC_F903,
   DRIVE_ERROR_DSD__LINE_LOW_F904,
   DRIVE_ERROR_DSD__FIELD_LOSS_F905,
   DRIVE_ERROR_DSD__LINE_DROOP_F406,
   DRIVE_ERROR_DSD__SERIAL_COM,

   NUM_DRIVE_ERROR_DSD
};
enum en_drive_error_hpv
{
   DRIVE_ERROR_HPV__NONE,
   // See Magnetek Serial Protocol Customer Interface Document P.46
   DRIVE_ERROR_HPV__UNK,
   NUM_DRIVE_ERROR_HPV = 252
};
enum en_drive_error_keb
{
   DRIVE_ERROR_KEB__NONE,
   //See Inverter Status DG02 P.235 in KEB manual
   DRIVE_ERROR_KEB__UNK = 251,
   NUM_DRIVE_ERROR_KEB = 252
};
enum en_drive_error_m1k
{
   DRIVE_ERROR_M1K__NONE,
   // See Magnetek Serial Protocol Customer Interface Document P.46
   DRIVE_ERROR_M1K__UNK,
   NUM_DRIVE_ERROR_M1K = 252
};
enum en_subfault_drive // reported drive fault + 1
{
   SUBF_DRIVE_FAULT__OFFLINE,
   SUBF_DRIVE_FAULT__REPORTED_Start,
   SUBF_DRIVE_FAULT__DSD_HW_EN_LOSS = 254,
   SUBF_DRIVE_FAULT__SPD_REG_LOSS = 255,
   NUM_SUBF_DRIVE_FAULT = 255,
};

//-----------------------------------------------

enum en_brake_faults
{
   BRAKE_ERROR__UNK,
   BRAKE_ERROR__NONE,
   BRAKE_ERROR__RESET_POR,
   BRAKE_ERROR__RESET_WDT,
   BRAKE_ERROR__COM,   // com loss with C4 system
   BRAKE_ERROR__GATE_FAULT, // Gate driver IC signaling faulted or not ready
   BRAKE_ERROR__MOSFET_FAILURE, // Ripple check has failed
   BRAKE_ERROR__CAN_BUS_OFFLINE,
   BRAKE_ERROR__DUPLICATE_ADDR,//Multiple brakeboards on network with same DIP addressing
   BRAKE_ERROR__RESET_BOD,
   BRAKE_ERROR__AC_LOSS,  // Loss of 120AC source (only for 20A brake)
   BRAKE_ERROR__OVERHEAT,

   NUM_BRAKE_ERRORS
};
enum en_subfault_brake // reported brake fault + 1
{
   SUBF_BRAKE_FAULT__OFFLINE,
   SUBF_BRAKE_FAULT__REPORTED_Start,
   NUM_SUBF_BRAKE_FAULT = SUBF_BRAKE_FAULT__REPORTED_Start + NUM_BRAKE_ERRORS,
};

//-----------------------------------------------
enum en_subfault_contactor_feedback
{
   SUBF_CONTACTOR_FEEDBACK__M,
   SUBF_CONTACTOR_FEEDBACK__B,
   SUBF_CONTACTOR_FEEDBACK__B2,

   NUM_SUBF_CONTACTOR_FAULT,
};

//-----------------------------------------------
enum en_subfault_relay_feedback
{
   SUBF_RELAY_FEEDBACK__SFH,
   SUBF_RELAY_FEEDBACK__RGP,
   SUBF_RELAY_FEEDBACK__DZP,
   SUBF_RELAY_FEEDBACK__RGM,
   SUBF_RELAY_FEEDBACK__DZM,
   SUBF_RELAY_FEEDBACK__SF1,
   SUBF_RELAY_FEEDBACK__SF2,

   NUM_SUBF_RELAY_FEEDBACK,
};

//-----------------------------------------------
enum en_subfault_can_bus
{
   SUBF_CAN_BUS__RESET_1,
   SUBF_CAN_BUS__LATCHED_1,// reset limit exceeded. should latch until board reset
   SUBF_CAN_BUS__RESET_2,
   SUBF_CAN_BUS__LATCHED_2,// reset limit exceeded. should latch until board reset

   NUM_SUBF_CAN_BUS,
};

//-----------------------------------------------
enum en_fpga_error
{
   FPGA_ERROR__UNK,
   FPGA_ERROR__NONE,
   FPGA_ERROR__RESET,
   // TODO add
   NUM_FPGA_ERROR
};
enum en_subfault_fpga_fault
{
   SUBF_FPGA_FAULT__OFFLINE,
   SUBF_FPGA_FAULT__CRC,
   SUBF_FPGA_FAULT__REPORTED_Start,
   NUM_SUBF_FPGA_FAULT = SUBF_FPGA_FAULT__REPORTED_Start + NUM_FPGA_ERROR,
};

//-----------------------------------------------

enum en_subfault_fram_fault
{
   SUBF_FRAM_FAULT__TX_TIMEOUT,
   SUBF_FRAM_FAULT__DEFAULTING,
   SUBF_FRAM_FAULT__CORRUPT,
   SUBF_FRAM_FAULT__INVALID_DEFAULTING,
   NUM_SUBF_FRAM_FAULT
};

//-----------------------------------------------

enum en_subfault_door_fault
{
   SUBF_DOOR_FAULT__1,
   SUBF_DOOR_FAULT__DO_FAIL,
   SUBF_DOOR_FAULT__DC_FAIL,
   SUBF_DOOR_FAULT__DPM_FAIL,
//todo

   NUM_SUBF_DOOR_FAULT
};
//-----------------------------------------------
enum en_subfault_need_to_rst
{
   SUBF_NEED_TO_RESET__CONTRACT_SPD,
   SUBF_NEED_TO_RESET__REAR_DOORS,
   SUBF_NEED_TO_RESET__NUM_FLOORS,
   SUBF_NEED_TO_RESET__DRIVE_SELECT,
   SUBF_NEED_TO_RESET__OPENING_MAP_F,
   SUBF_NEED_TO_RESET__OPENING_MAP_R,
   NUM_SUBF_NEED_TO_RESET
};
//-----------------------------------------------
enum en_subfault_invalid_scurve
{
   SUBF_INVALID_SCURVE__ACCEL,
   SUBF_INVALID_SCURVE__DECEL,
   SUBF_INVALID_SCURVE__ADDED_ACCEL,
   SUBF_INVALID_SCURVE__ZERO_SPEED,//If in auto, and max run speed is zero, fault
   SUBF_INVALID_SCURVE__RSL,
   /* Below is the theoretical size check, above is the per run check */
   SUBF_INVALID_SCURVE__PROFILE1,
   SUBF_INVALID_SCURVE__PROFILE2,
   SUBF_INVALID_SCURVE__PROFILE3,
   SUBF_INVALID_SCURVE__PROFILE4,
  NUM_SUBF_INVALID_SCURVE
};
//-----------------------------------------------
enum en_subfault_rgb_feedback
{
   SUBF_RGB_FEEDBACK__EB3_Inv0,//EB3 Monitoring reports low, when control is high
   SUBF_RGB_FEEDBACK__EB3_Inv1,//EB3 Monitoring reports high, when control is low
   SUBF_RGB_FEEDBACK__EB4_Inv0,//EB4 Monitoring reports low, when control is high
   SUBF_RGB_FEEDBACK__EB4_Inv1,//EB4 Monitoring reports high, when control is low
   SUBF_RGB_FEEDBACK__EB3_High,
   SUBF_RGB_FEEDBACK__EB4_High,

   NUM_SUBF_RGB_FEEDBACK
};
//-----------------------------------------------
enum en_subfault_invaild_ets
{
   SUBF_INVALID_ETS__P1_VEL1,
   SUBF_INVALID_ETS__P1_VEL2,
   SUBF_INVALID_ETS__P1_VEL3,
   SUBF_INVALID_ETS__P1_VEL4,
   SUBF_INVALID_ETS__P1_VEL5,
   SUBF_INVALID_ETS__P1_VEL6,
   SUBF_INVALID_ETS__P1_VEL7,
   SUBF_INVALID_ETS__P1_VEL8,

   SUBF_INVALID_ETS__P2_VEL1,
   SUBF_INVALID_ETS__P2_VEL2,
   SUBF_INVALID_ETS__P2_VEL3,
   SUBF_INVALID_ETS__P2_VEL4,
   SUBF_INVALID_ETS__P2_VEL5,
   SUBF_INVALID_ETS__P2_VEL6,
   SUBF_INVALID_ETS__P2_VEL7,
   SUBF_INVALID_ETS__P2_VEL8,

   SUBF_INVALID_ETS__P3_VEL1,
   SUBF_INVALID_ETS__P3_VEL2,
   SUBF_INVALID_ETS__P3_VEL3,
   SUBF_INVALID_ETS__P3_VEL4,
   SUBF_INVALID_ETS__P3_VEL5,
   SUBF_INVALID_ETS__P3_VEL6,
   SUBF_INVALID_ETS__P3_VEL7,
   SUBF_INVALID_ETS__P3_VEL8,

   SUBF_INVALID_ETS__P4_VEL1,
   SUBF_INVALID_ETS__P4_VEL2,
   SUBF_INVALID_ETS__P4_VEL3,
   SUBF_INVALID_ETS__P4_VEL4,
   SUBF_INVALID_ETS__P4_VEL5,
   SUBF_INVALID_ETS__P4_VEL6,
   SUBF_INVALID_ETS__P4_VEL7,
   SUBF_INVALID_ETS__P4_VEL8,

   SUBF_INVALID_ETS__P1_POS1,
   SUBF_INVALID_ETS__P1_POS2,
   SUBF_INVALID_ETS__P1_POS3,
   SUBF_INVALID_ETS__P1_POS4,
   SUBF_INVALID_ETS__P1_POS5,
   SUBF_INVALID_ETS__P1_POS6,
   SUBF_INVALID_ETS__P1_POS7,
   SUBF_INVALID_ETS__P1_POS8,

   SUBF_INVALID_ETS__P2_POS1,
   SUBF_INVALID_ETS__P2_POS2,
   SUBF_INVALID_ETS__P2_POS3,
   SUBF_INVALID_ETS__P2_POS4,
   SUBF_INVALID_ETS__P2_POS5,
   SUBF_INVALID_ETS__P2_POS6,
   SUBF_INVALID_ETS__P2_POS7,
   SUBF_INVALID_ETS__P2_POS8,

   SUBF_INVALID_ETS__P3_POS1,
   SUBF_INVALID_ETS__P3_POS2,
   SUBF_INVALID_ETS__P3_POS3,
   SUBF_INVALID_ETS__P3_POS4,
   SUBF_INVALID_ETS__P3_POS5,
   SUBF_INVALID_ETS__P3_POS6,
   SUBF_INVALID_ETS__P3_POS7,
   SUBF_INVALID_ETS__P3_POS8,

   SUBF_INVALID_ETS__P4_POS1,
   SUBF_INVALID_ETS__P4_POS2,
   SUBF_INVALID_ETS__P4_POS3,
   SUBF_INVALID_ETS__P4_POS4,
   SUBF_INVALID_ETS__P4_POS5,
   SUBF_INVALID_ETS__P4_POS6,
   SUBF_INVALID_ETS__P4_POS7,
   SUBF_INVALID_ETS__P4_POS8,

   NUM_SUBF_INVALID_ETS
};
//-----------------------------------------------
enum en_subfault_mr_safety
{
   SUBF_MR_SAFETY__ZMIN,
   SUBF_MR_SAFETY__GOV,
   SUBF_MR_SAFETY__LATCHED,

   NUM_SUBF_MR_SAFETY
};
//-----------------------------------------------
enum en_subfault_governor
{
   SUBF_GOVERNOR__ACTIVE = 1,
   SUBF_GOVERNOR__LATCHED,

   NUM_SUBF_GOVERNOR
};
//-----------------------------------------------
enum en_subfault_fpga_ebrake
{
   SUBF_FPGA_EBRAKE__ACTIVE = 1,
   SUBF_FPGA_EBRAKE__LATCHED,

   NUM_SUBF_FPGA_EBRAKE
};
//-----------------------------------------------
enum en_subfault_b_cont
{
   SUBF_B_CONT__LOW_SW,
   SUBF_B_CONT__LOW_HW,
   SUBF_B_CONT__HIGH_SW,
   SUBF_B_CONT__HIGH_HW,

   NUM_SUBF_B_CONT
};
//-----------------------------------------------
enum en_subfault_dc_hw_enable
{
   SUBF_DC_HW_ENABLE__LOW,
   SUBF_DC_HW_ENABLE__HIGH,

   NUM_SUBF_DC_HW_ENABLE
};
//-----------------------------------------------
enum en_subfault_rescue
{
   SUBF_RESCUE__START,
   SUBF_RESCUE__DZ,
   SUBF_RESCUE__NoRecall,

   NUM_SUBF_RESCUE
};
//-----------------------------------------------
enum en_subfault_board_offline
{
   SUBF_BOARD_OFFLINE__MRA_BY_CTA,
   SUBF_BOARD_OFFLINE__MRA_BY_COPA,
   SUBF_BOARD_OFFLINE__MRA_BY_MRB,
   SUBF_BOARD_OFFLINE__CTA_BY_MRA,
   SUBF_BOARD_OFFLINE__CTA_BY_COPA,
   SUBF_BOARD_OFFLINE__CTA_BY_CTB,
   SUBF_BOARD_OFFLINE__COPA_BY_MRA,
   SUBF_BOARD_OFFLINE__COPA_BY_CTA,
   SUBF_BOARD_OFFLINE__COPA_BY_COPB,
   SUBF_BOARD_OFFLINE__MRB,
   SUBF_BOARD_OFFLINE__CTB,
   SUBF_BOARD_OFFLINE__COPB,

   NUM_SUBF_BOARD_OFFLINE
};
//-----------------------------------------------
typedef enum
{
   HALLBOARD_ERROR__UNK,
   HALLBOARD_ERROR__NONE,
   HALLBOARD_ERROR__RESET_POR,
   HALLBOARD_ERROR__RESET_WDT,
   HALLBOARD_ERROR__RESET_BOD,
   HALLBOARD_ERROR__COM_SYS,   //com loss with RIS or MRB
   HALLBOARD_ERROR__DIP, // Multiple HB with same DIPs

   /* Below requires POR */
   HALLBOARD_ERROR__POR_TYPE_START,
   HALLBOARD_ERROR__CAN_BUS_OFFLINE = HALLBOARD_ERROR__POR_TYPE_START,//offline limit reached.

   NUM_HALLBOARD_ERROR,
} en_hallboard_error;
//-----------------------------------------------
typedef enum
{
   SUBF_INV_SETTING__PARKING_FLOOR,
   SUBF_INV_SETTING__FIRE_MAIN_FLOOR,
   SUBF_INV_SETTING__FIRE_ALT_FLOOR,

   NUM_SUBF_INV_SETTING
}en_subf_invalid_setting;
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
#define EXPAND(A) A,
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif

