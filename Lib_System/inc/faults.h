#ifndef FAULTS_H
#define FAULTS_H

/* NOTE: FILE UPDATED VIA LIB_SYSTEM - SUPPORT - FAULTS, 
 * Edit SystemFaults.xlsx then save as tab delimited (SystemFaults.txt)  
 * Then run Update_Faults.bat to automatically update this file */

/* Includes */
#include "sys.h"

/* Defines */
#define MAX_LATCHED_FAULTS             ( 32 )
#define FAULT_LATCH_PERIOD_1MS         ( 3000U )
#define BOARD_RESET_FAULT_TIMER_MS     ( 10000 )
/* Update rate for fault packets when the processor is not faulted, and when it is faulted */
#define FAULT_RESEND_RATE_INACTIVE_1MS ( 2000 )
#define FAULT_RESEND_RATE_ACTIVE_1MS   ( 500 )

// List of system faults
typedef enum
{
   FLT__NONE,                          // FaultNum: 0
   FLT__GOV,                           // FaultNum: 1
   FLT__GOV_L,                         // FaultNum: 2
   FLT__EB1_DROP,                      // FaultNum: 3
   FLT__EB1_DROP_L,                    // FaultNum: 4
   FLT__UNINTENDED_MOV,                // FaultNum: 5
   FLT__UNINTENDED_MOV_L,              // FaultNum: 6
   FLT__TRACTION_LOSS,                 // FaultNum: 7
   FLT__TRACTION_LOSS_L,               // FaultNum: 8
   FLT__SPEED_DEV,                     // FaultNum: 9
   FLT__IC_STOP_SW,                    // FaultNum: 10
   FLT__REDUNDANCY_LRB,                // FaultNum: 11
   FLT__REDUNDANCY_LRM,                // FaultNum: 12
   FLT__REDUNDANCY_LRT,                // FaultNum: 13
   FLT__REDUNDANCY_LFB,                // FaultNum: 14
   FLT__REDUNDANCY_LFM,                // FaultNum: 15
   FLT__REDUNDANCY_LFT,                // FaultNum: 16
   FLT__REDUNDANCY_ATU,                // FaultNum: 17
   FLT__REDUNDANCY_ATD,                // FaultNum: 18
   FLT__REDUNDANCY_ABU,                // FaultNum: 19
   FLT__REDUNDANCY_ABD,                // FaultNum: 20
   FLT__REDUNDANCY_CAR_BYP,            // FaultNum: 21
   FLT__REDUNDANCY_HA_BYP,             // FaultNum: 22
   FLT__REDUNDANCY_MM,                 // FaultNum: 23
   FLT__REDUNDANCY_SFM,                // FaultNum: 24
   FLT__REDUNDANCY_SFH,                // FaultNum: 25
   FLT__REDUNDANCY_PIT,                // FaultNum: 26
   FLT__REDUNDANCY_IP_INSP,            // FaultNum: 27
   FLT__REDUNDANCY_MR_INSP,            // FaultNum: 28
   FLT__REDUNDANCY_IL_INSP,            // FaultNum: 29
   FLT__REDUNDANCY_C_EB2,              // FaultNum: 30
   FLT__REDUNDANCY_C_SFM,              // FaultNum: 31
   FLT__REDUNDANCY_M_EB2,              // FaultNum: 32
   FLT__REDUNDANCY_M_SFM,              // FaultNum: 33
   FLT__REDUNDANCY_M_EB3,              // FaultNum: 34
   FLT__REDUNDANCY_M_EB1,              // FaultNum: 35
   FLT__REDUNDANCY_M_SFP,              // FaultNum: 36
   FLT__REDUNDANCY_C_EB3,              // FaultNum: 37
   FLT__REDUNDANCY_C_EB1,              // FaultNum: 38
   FLT__REDUNDANCY_C_SFP,              // FaultNum: 39
   FLT__REDUNDANCY_GSWR,               // FaultNum: 40
   FLT__REDUNDANCY_GSWF,               // FaultNum: 41
   FLT__REDUNDANCY_CT_INSP,            // FaultNum: 42
   FLT__REDUNDANCY_CT_STOP,            // FaultNum: 43
   FLT__REDUNDANCY_ESC_HATCH,          // FaultNum: 44
   FLT__REDUNDANCY_CAR_SAFE,           // FaultNum: 45
   FLT__REDUNDANCY_FSS,                // FaultNum: 46
   FLT__REDUNDANCY_IC_STOP,            // FaultNum: 47
   FLT__REDUNDANCY_IC_INSP,            // FaultNum: 48
   FLT__REDUNDANCY_HA_INSP,            // FaultNum: 49
   FLT__SFP_STUCK_LO,                  // FaultNum: 50
   FLT__SFP_STUCK_HI,                  // FaultNum: 51
   FLT__SFP_DROPPED,                   // FaultNum: 52
   FLT__EB3_STUCK_LO,                  // FaultNum: 53
   FLT__EB3_STUCK_HI,                  // FaultNum: 54
   FLT__EB4_STUCK_LO,                  // FaultNum: 55
   FLT__EB4_STUCK_HI,                  // FaultNum: 56
   FLT__EB1_STUCK,                     // FaultNum: 57
   FLT__M_CONT_STUCK_HI,               // FaultNum: 58
   FLT__M_CONT_STUCK_LO,               // FaultNum: 59
   FLT__B2_CONT_STUCK_HI,              // FaultNum: 60
   FLT__B2_CONT_STUCK_LO,              // FaultNum: 61
   FLT__HA_BYPASS,                     // FaultNum: 62
   FLT__CAR_BYPASS,                    // FaultNum: 63
   FLT__OVERSPEED_GENERAL,             // FaultNum: 64
   FLT__OVERSPEED_GENERAL_L,           // FaultNum: 65
   FLT__OVERSPEED_INSPECTION,          // FaultNum: 66
   FLT__OVERSPEED_DOOR_GSWF,           // FaultNum: 67
   FLT__OVERSPEED_DOORS_LFT,           // FaultNum: 68
   FLT__OVERSPEED_DOORS_LFM,           // FaultNum: 69
   FLT__OVERSPEED_DOORS_LFB,           // FaultNum: 70
   FLT__OVERSPEED_DOORS_GSWR,          // FaultNum: 71
   FLT__OVERSPEED_DOORS_LRT,           // FaultNum: 72
   FLT__OVERSPEED_DOORS_LRM,           // FaultNum: 73
   FLT__OVERSPEED_DOORS_LRB,           // FaultNum: 74
   FLT__FLOOD_OOS,                     // FaultNum: 75
   FLT__DOOR_INVALID,                  // FaultNum: 76
   FLT__CPU_STOP_SW_MRA,               // FaultNum: 77
   FLT__CPU_STOP_SW_MRB,               // FaultNum: 78
   FLT__CPU_STOP_SW_CTA,               // FaultNum: 79
   FLT__CPU_STOP_SW_CTB,               // FaultNum: 80
   FLT__CPU_STOP_SW_COPA,              // FaultNum: 81
   FLT__CPU_STOP_SW_COPB,              // FaultNum: 82
   FLT__NEED_TO_CYCLE_PWR_MR,          // FaultNum: 83
   FLT__INVALID_NUM_FLOORS,            // FaultNum: 84
   FLT__INVALID_CONTRACT_SPD,          // FaultNum: 85
   FLT__INVALID_INSP_SPD,              // FaultNum: 86
   FLT__INVALID_LEARN_SPD,             // FaultNum: 87
   FLT__INVALID_TERM_SPD,              // FaultNum: 88
   FLT__INVALID_LEVEL_SPD,             // FaultNum: 89
   FLT__INVALID_NTSD_SPD,              // FaultNum: 90
   FLT__NEED_TO_LEARN,                 // FaultNum: 91
   FLT__INVALID_ETS_1,                 // FaultNum: 92
   FLT__INVALID_ETS_2,                 // FaultNum: 93
   FLT__INVALID_ETS_3,                 // FaultNum: 94
   FLT__INVALID_ETS_4,                 // FaultNum: 95
   FLT__AT_FLOOR_NO_DZ,                // FaultNum: 96
   FLT__FIRE_STOP_SW,                  // FaultNum: 97
   FLT__DOOR_F_JUMPER_GSW,             // FaultNum: 98
   FLT__DOOR_F_JUMPER_LOCK,            // FaultNum: 99
   FLT__DOOR_F_LOCKS_OPEN,             // FaultNum: 100
   FLT__DOOR_F_GSW_OPEN,               // FaultNum: 101
   FLT__DOOR_F_FAIL_OPEN,              // FaultNum: 102
   FLT__DOOR_F_FAIL_CLOSE,             // FaultNum: 103
   FLT__DOOR_F_FAIL_NUDGE,             // FaultNum: 104
   FLT__DOOR_F_STALLED,                // FaultNum: 105
   FLT__DOOR_F_LOST_SIGNAL,            // FaultNum: 106
   FLT__DOOR_R_JUMPER_GSW,             // FaultNum: 107
   FLT__DOOR_R_JUMPER_LOCK,            // FaultNum: 108
   FLT__DOOR_R_LOCKS_OPEN,             // FaultNum: 109
   FLT__DOOR_R_GSW_OPEN,               // FaultNum: 110
   FLT__DOOR_R_FAIL_OPEN,              // FaultNum: 111
   FLT__DOOR_R_FAIL_CLOSE,             // FaultNum: 112
   FLT__DOOR_R_FAIL_NUDGE,             // FaultNum: 113
   FLT__DOOR_R_STALLED,                // FaultNum: 114
   FLT__DOOR_R_LOST_SIGNAL,            // FaultNum: 115
   FLT__MAX_RUNTIME,                   // FaultNum: 116
   FLT__EB_BYPASSED,                   // FaultNum: 117
   FLT__PARAM_QUEUE_MRA,               // FaultNum: 118
   FLT__PARAM_QUEUE_MRB,               // FaultNum: 119
   FLT__PARAM_QUEUE_CTA,               // FaultNum: 120
   FLT__PARAM_QUEUE_CTB,               // FaultNum: 121
   FLT__PARAM_QUEUE_COPA,              // FaultNum: 122
   FLT__PARAM_QUEUE_COPB,              // FaultNum: 123
   FLT__OFFLINE_MRA_BY_CTA,            // FaultNum: 124
   FLT__OFFLINE_MRA_BY_COPA,           // FaultNum: 125
   FLT__OFFLINE_MRA_BY_MRB,            // FaultNum: 126
   FLT__OFFLINE_CTA_BY_MRA,            // FaultNum: 127
   FLT__OFFLINE_CTA_BY_COPA,           // FaultNum: 128
   FLT__OFFLINE_CTA_BY_CTB,            // FaultNum: 129
   FLT__OFFLINE_COPA_BY_MRA,           // FaultNum: 130
   FLT__OFFLINE_COPA_BY_CTA,           // FaultNum: 131
   FLT__OFFLINE_COPA_BY_COPB,          // FaultNum: 132
   FLT__OFFLINE_MRB_BY_MRA,            // FaultNum: 133
   FLT__OFFLINE_CTB_BY_CTA,            // FaultNum: 134
   FLT__OFFLINE_COPB_BY_COPA,          // FaultNum: 135
   FLT__BOARD_RESET_MRA,               // FaultNum: 136
   FLT__BOARD_RESET_MRB,               // FaultNum: 137
   FLT__BOARD_RESET_CTA,               // FaultNum: 138
   FLT__BOARD_RESET_CTB,               // FaultNum: 139
   FLT__BOARD_RESET_COPA,              // FaultNum: 140
   FLT__BOARD_RESET_COPB,              // FaultNum: 141
   FLT__WDT_RESET_MRA,                 // FaultNum: 142
   FLT__WDT_RESET_MRB,                 // FaultNum: 143
   FLT__WDT_RESET_CTA,                 // FaultNum: 144
   FLT__WDT_RESET_CTB,                 // FaultNum: 145
   FLT__WDT_RESET_COPA,                // FaultNum: 146
   FLT__WDT_RESET_COPB,                // FaultNum: 147
   FLT__BOD_RESET_MRA,                 // FaultNum: 148
   FLT__BOD_RESET_MRB,                 // FaultNum: 149
   FLT__BOD_RESET_CTA,                 // FaultNum: 150
   FLT__BOD_RESET_CTB,                 // FaultNum: 151
   FLT__BOD_RESET_COPA,                // FaultNum: 152
   FLT__BOD_RESET_COPB,                // FaultNum: 153
   FLT__SAFETY_STR_SFH,                // FaultNum: 154
   FLT__SAFETY_STR_SFM,                // FaultNum: 155
   FLT__SAFETY_STR_PIT,                // FaultNum: 156
   FLT__SAFETY_STR_BUF,                // FaultNum: 157
   FLT__SAFETY_STR_TFL,                // FaultNum: 158
   FLT__SAFETY_STR_BFL,                // FaultNum: 159
   FLT__SAFETY_STR_CT_SW,              // FaultNum: 160
   FLT__SAFETY_STR_ESC_HATCH,          // FaultNum: 161
   FLT__SAFETY_STR_CAR_SAFE,           // FaultNum: 162
   FLT__LTF_OPEN,                      // FaultNum: 163
   FLT__LMF_OPEN,                      // FaultNum: 164
   FLT__LBF_OPEN,                      // FaultNum: 165
   FLT__LTR_OPEN,                      // FaultNum: 166
   FLT__LMR_OPEN,                      // FaultNum: 167
   FLT__LBR_OPEN,                      // FaultNum: 168
   FLT__GSWF_OPEN,                     // FaultNum: 169
   FLT__GSWR_OPEN,                     // FaultNum: 170
   FLT__FRAM_DEFAULTING,               // FaultNum: 171
   FLT__FRAM_TIMEOUT,                  // FaultNum: 172
   FLT__FRAM_DEFAULT_FAIL,             // FaultNum: 173
   FLT__120VAC,                        // FaultNum: 174
   FLT__MOTION_INVALID_CMD,            // FaultNum: 175
   FLT__MOTION_PREPARE_RUN,            // FaultNum: 176
   FLT__MOTION_DRIVE_ENABLE,           // FaultNum: 177
   FLT__MOTION_PICK_M,                 // FaultNum: 178
   FLT__MOTION_SPEED_REG,              // FaultNum: 179
   FLT__MOTION_PICK_B2,                // FaultNum: 180
   FLT__MOTION_LIFT_BRAKE,             // FaultNum: 181
   FLT__MOTION_ACCEL_DELAY,            // FaultNum: 182
   FLT__MOTION_RAMP_TO_ZERO,           // FaultNum: 183
   FLT__MOTION_HOLD_ZERO,              // FaultNum: 184
   FLT__MOTION_CHECK_BPS,              // FaultNum: 185
   FLT__MOTION_DEENERGIZE,             // FaultNum: 186
   FLT__MOTION_DROP_M,                 // FaultNum: 187
   FLT__MOTION_PREFLIGHT,              // FaultNum: 188
   FLT__BPS_STUCK_CLOSED,              // FaultNum: 189
   FLT__BPS_STUCK_OPEN,                // FaultNum: 190
   FLT__EB2_DROPPED,                   // FaultNum: 191
   FLT__EB2_STUCK,                     // FaultNum: 192
   FLT__BRAKE_OFFLINE,                 // FaultNum: 193
   FLT__BRAKE_UNK,                     // FaultNum: 194
   FLT__BRAKE_POR_RST,                 // FaultNum: 195
   FLT__BRAKE_WDT_RST,                 // FaultNum: 196
   FLT__BRAKE_COM,                     // FaultNum: 197
   FLT__BRAKE_UNUSED5,                 // FaultNum: 198
   FLT__BRAKE_MOSFET,                  // FaultNum: 199
   FLT__BRAKE_BUS_OFFLINE,             // FaultNum: 200
   FLT__BRAKE_DIP,                     // FaultNum: 201
   FLT__BRAKE_BOD_RST,                 // FaultNum: 202
   FLT__BRAKE_AC_LOSS,                 // FaultNum: 203
   FLT__EBRAKE_OFFLINE,                // FaultNum: 204
   FLT__EBRAKE_UNK,                    // FaultNum: 205
   FLT__EBRAKE_POR_RST,                // FaultNum: 206
   FLT__EBRAKE_WDT_RST,                // FaultNum: 207
   FLT__EBRAKE_COM,                    // FaultNum: 208
   FLT__EBRAKE_UNUSED5,                // FaultNum: 209
   FLT__EBRAKE_MOSFET,                 // FaultNum: 210
   FLT__EBRAKE_BUS_OFFLINE,            // FaultNum: 211
   FLT__EBRAKE_DIP,                    // FaultNum: 212
   FLT__EBRAKE_BOD_RST,                // FaultNum: 213
   FLT__EBRAKE_AC_LOSS,                // FaultNum: 214
   FLT__CPLD_STARTUP,                  // FaultNum: 215
   FLT__CPLD_UNINT_MOV,                // FaultNum: 216
   FLT__CPLD_GOV,                      // FaultNum: 217
   FLT__CPLD_REDUND,                   // FaultNum: 218
   FLT__CPLD_COM_LOSS,                 // FaultNum: 219
   FLT__CPLD_NON_BYP,                  // FaultNum: 220
   FLT__CPLD_IN_CAR,                   // FaultNum: 221
   FLT__CPLD_INSP,                     // FaultNum: 222
   FLT__CPLD_SFH,                      // FaultNum: 223
   FLT__CPLD_GRIPPER,                  // FaultNum: 224
   FLT__CPLD_ACCESS,                   // FaultNum: 225
   FLT__CPLD_LOCKS,                    // FaultNum: 226
   FLT__CPLD_DOORS,                    // FaultNum: 227
   FLT__CPLD_BYPASS_SW,                // FaultNum: 228
   FLT__CPLD_PREFLIGHT,                // FaultNum: 229
   FLT__RISER_OFFLINE_1,               // FaultNum: 230
   FLT__RISER_OFFLINE_2,               // FaultNum: 231
   FLT__RISER_OFFLINE_3,               // FaultNum: 232
   FLT__RISER_OFFLINE_4,               // FaultNum: 233
   FLT__DZ_STUCK_HI,                   // FaultNum: 234
   FLT__POSITION_LIMIT,                // FaultNum: 235
   FLT__INVALID_MANUAL_RUN,            // FaultNum: 236
   FLT__INVALID_ACCEL_CURVE,           // FaultNum: 237
   FLT__INVALID_DECEL_CURVE,           // FaultNum: 238
   FLT__INVALID_ADDED_CURVE,           // FaultNum: 239
   FLT__INVALID_RSL_CURVE,             // FaultNum: 240
   FLT__INVALID_CURVE_P1,              // FaultNum: 241
   FLT__INVALID_CURVE_P2,              // FaultNum: 242
   FLT__INVALID_CURVE_P3,              // FaultNum: 243
   FLT__INVALID_CURVE_P4,              // FaultNum: 244
   FLT__SFM_STUCK,                     // FaultNum: 245
   FLT__OVERLOADED,                    // FaultNum: 246
   FLT__PREFLIGHT_MR,                  // FaultNum: 247
   FLT__PREFLIGHT_CT,                  // FaultNum: 248
   FLT__PREFLIGHT_COP,                 // FaultNum: 249
   FLT__PARAM_SYNC_MRB,                // FaultNum: 250
   FLT__PARAM_SYNC_CTA,                // FaultNum: 251
   FLT__PARAM_SYNC_CTB,                // FaultNum: 252
   FLT__SCURVE_UPDATING,               // FaultNum: 253
   FLT__REGEN_FAULT,                   // FaultNum: 254
   FLT__OVERSPEED_CONST,               // FaultNum: 255
   FLT__EBPS_STUCK_CLOSED,             // FaultNum: 256
   FLT__EBPS_STUCK_OPEN,               // FaultNum: 257
   FLT__INVALID_DIP_SW_B2,             // FaultNum: 258
   FLT__INVALID_DIP_SW_B3,             // FaultNum: 259
   FLT__INVALID_DIP_SW_B4,             // FaultNum: 260
   FLT__INVALID_DIP_SW_B8,             // FaultNum: 261
   FLT__INVALID_DIP_SW_A6,             // FaultNum: 262
   FLT__INSP_IC_KEY_REQD,              // FaultNum: 263
   FLT__SPEED_REG_HI,                  // FaultNum: 264
   FLT__B_CONT_HI_HW,                  // FaultNum: 265
   FLT__SPEED_REG_LO,                  // FaultNum: 266
   FLT__B_CONT_LO_HW,                  // FaultNum: 267
   FLT__DC_HW_ENABLE,                  // FaultNum: 268
   FLT__EXP_COM_1,                     // FaultNum: 269
   FLT__EXP_COM_2,                     // FaultNum: 270
   FLT__EXP_COM_3,                     // FaultNum: 271
   FLT__EXP_COM_4,                     // FaultNum: 272
   FLT__EXP_COM_5,                     // FaultNum: 273
   FLT__EXP_COM_6,                     // FaultNum: 274
   FLT__EXP_COM_7,                     // FaultNum: 275
   FLT__EXP_COM_8,                     // FaultNum: 276
   FLT__EXP_COM_9,                     // FaultNum: 277
   FLT__EXP_COM_10,                    // FaultNum: 278
   FLT__EXP_COM_11,                    // FaultNum: 279
   FLT__EXP_COM_12,                    // FaultNum: 280
   FLT__EXP_COM_13,                    // FaultNum: 281
   FLT__EXP_COM_14,                    // FaultNum: 282
   FLT__EXP_COM_15,                    // FaultNum: 283
   FLT__EXP_DIP_1,                     // FaultNum: 284
   FLT__EXP_DIP_2,                     // FaultNum: 285
   FLT__EXP_DIP_3,                     // FaultNum: 286
   FLT__EXP_DIP_4,                     // FaultNum: 287
   FLT__EXP_DIP_5,                     // FaultNum: 288
   FLT__EXP_DIP_6,                     // FaultNum: 289
   FLT__EXP_DIP_7,                     // FaultNum: 290
   FLT__EXP_DIP_8,                     // FaultNum: 291
   FLT__EXP_DIP_9,                     // FaultNum: 292
   FLT__EXP_DIP_10,                    // FaultNum: 293
   FLT__EXP_DIP_11,                    // FaultNum: 294
   FLT__EXP_DIP_12,                    // FaultNum: 295
   FLT__EXP_DIP_13,                    // FaultNum: 296
   FLT__EXP_DIP_14,                    // FaultNum: 297
   FLT__EXP_DIP_15,                    // FaultNum: 298
   FLT__INVALID_HALLMASK,              // FaultNum: 299
   FLT__OOS_LIMIT,                     // FaultNum: 300
   FLT__DUPLICATE_GROUP_ID,            // FaultNum: 301
   FLT__RESCUE_START,                  // FaultNum: 302
   FLT__RESCUE_IN_DZ,                  // FaultNum: 303
   FLT__RESCUE_INVALID,                // FaultNum: 304
   FLT__UNUSED305,                     // FaultNum: 305
   FLT__CEDES1_OFFLINE,                // FaultNum: 306
   FLT__CEDES1_READ_FAIL,              // FaultNum: 307
   FLT__CEDES1_ALIGN_CLOSE,            // FaultNum: 308
   FLT__CEDES1_ALIGN_FAR,              // FaultNum: 309
   FLT__CEDES1_ALIGN_LEFT,             // FaultNum: 310
   FLT__CEDES1_ALIGN_RIGHT,            // FaultNum: 311
   FLT__CEDES1_INTERNAL,               // FaultNum: 312
   FLT__CEDES1_COMM,                   // FaultNum: 313
   FLT__CEDES1_CROSS1_POS,             // FaultNum: 314
   FLT__CEDES1_CROSS1_VEL,             // FaultNum: 315
   FLT__CEDES1_CROSS1_BOTH,            // FaultNum: 316
   FLT__CEDES1_CROSS2_POS,             // FaultNum: 317
   FLT__CEDES1_CROSS2_VEL,             // FaultNum: 318
   FLT__CEDES1_CROSS2_BOTH,            // FaultNum: 319
   FLT__CEDES2_OFFLINE,                // FaultNum: 320
   FLT__CEDES2_READ_FAIL,              // FaultNum: 321
   FLT__CEDES2_ALIGN_CLOSE,            // FaultNum: 322
   FLT__CEDES2_ALIGN_FAR,              // FaultNum: 323
   FLT__CEDES2_ALIGN_LEFT,             // FaultNum: 324
   FLT__CEDES2_ALIGN_RIGHT,            // FaultNum: 325
   FLT__CEDES2_INTERNAL,               // FaultNum: 326
   FLT__CEDES2_COMM,                   // FaultNum: 327
   FLT__CEDES2_CROSS1_POS,             // FaultNum: 328
   FLT__CEDES2_CROSS1_VEL,             // FaultNum: 329
   FLT__CEDES2_CROSS1_BOTH,            // FaultNum: 330
   FLT__CEDES2_CROSS2_POS,             // FaultNum: 331
   FLT__CEDES2_CROSS2_VEL,             // FaultNum: 332
   FLT__CEDES2_CROSS2_BOTH,            // FaultNum: 333
   FLT__EPOWER,                        // FaultNum: 334
   FLT__INVALID_PARKING,               // FaultNum: 335
   FLT__INVALID_FIRE_MAIN,             // FaultNum: 336
   FLT__INVALID_FIRE_ALT,              // FaultNum: 337
   FLT__CPLD_OFFLINE_MR,               // FaultNum: 338
   FLT__CPLD_OFFLINE_CT,               // FaultNum: 339
   FLT__CPLD_OFFLINE_COP,              // FaultNum: 340
   FLT__DATAGRAM_EXPIRED,              // FaultNum: 341
   FLT__DRIVE_OFFLINE,                 // FaultNum: 342
   FLT__DSD_NOT_RDY,                   // FaultNum: 343
   FLT__DSD_OVERSPEED,                 // FaultNum: 344
   FLT__DSD_TACH_LOSS,                 // FaultNum: 345
   FLT__DSD_TACH_REV,                  // FaultNum: 346
   FLT__DSD_OVERLOAD,                  // FaultNum: 347
   FLT__DSD_FIELD_CURR,                // FaultNum: 348
   FLT__DSD_CONTACT,                   // FaultNum: 349
   FLT__DSD_CEMF,                      // FaultNum: 350
   FLT__DSD_ESTOP,                     // FaultNum: 351
   FLT__DSD_LOOP,                      // FaultNum: 352
   FLT__DSD_PCU,                       // FaultNum: 353
   FLT__DSD_LINE_SYNC,                 // FaultNum: 354
   FLT__DSD_LINE_LO,                   // FaultNum: 355
   FLT__DSD_FIELD_LOSS,                // FaultNum: 356
   FLT__DSD_LINE_DROOP,                // FaultNum: 357
   FLT__DSD_COMM,                      // FaultNum: 358
   FLT__MAG_FLT_1,                     // FaultNum: 359
   FLT__MAG_FLT_2,                     // FaultNum: 360
   FLT__MAG_FLT_3,                     // FaultNum: 361
   FLT__MAG_FLT_4,                     // FaultNum: 362
   FLT__MAG_FLT_5,                     // FaultNum: 363
   FLT__MAG_FLT_6,                     // FaultNum: 364
   FLT__MAG_FLT_7,                     // FaultNum: 365
   FLT__MAG_FLT_8,                     // FaultNum: 366
   FLT__MAG_FLT_9,                     // FaultNum: 367
   FLT__MAG_FLT_10,                    // FaultNum: 368
   FLT__MAG_FLT_11,                    // FaultNum: 369
   FLT__MAG_FLT_12,                    // FaultNum: 370
   FLT__MAG_FLT_13,                    // FaultNum: 371
   FLT__MAG_FLT_14,                    // FaultNum: 372
   FLT__MAG_FLT_15,                    // FaultNum: 373
   FLT__MAG_FLT_16,                    // FaultNum: 374
   FLT__MAG_FLT_17,                    // FaultNum: 375
   FLT__MAG_FLT_18,                    // FaultNum: 376
   FLT__MAG_FLT_19,                    // FaultNum: 377
   FLT__MAG_FLT_20,                    // FaultNum: 378
   FLT__MAG_FLT_21,                    // FaultNum: 379
   FLT__MAG_FLT_22,                    // FaultNum: 380
   FLT__MAG_FLT_23,                    // FaultNum: 381
   FLT__MAG_FLT_24,                    // FaultNum: 382
   FLT__MAG_FLT_25,                    // FaultNum: 383
   FLT__MAG_FLT_26,                    // FaultNum: 384
   FLT__MAG_FLT_27,                    // FaultNum: 385
   FLT__MAG_FLT_28,                    // FaultNum: 386
   FLT__MAG_FLT_29,                    // FaultNum: 387
   FLT__MAG_FLT_30,                    // FaultNum: 388
   FLT__MAG_FLT_31,                    // FaultNum: 389
   FLT__MAG_FLT_32,                    // FaultNum: 390
   FLT__MAG_FLT_33,                    // FaultNum: 391
   FLT__MAG_FLT_34,                    // FaultNum: 392
   FLT__MAG_FLT_35,                    // FaultNum: 393
   FLT__MAG_FLT_36,                    // FaultNum: 394
   FLT__MAG_FLT_37,                    // FaultNum: 395
   FLT__MAG_FLT_38,                    // FaultNum: 396
   FLT__MAG_FLT_39,                    // FaultNum: 397
   FLT__MAG_FLT_40,                    // FaultNum: 398
   FLT__MAG_FLT_41,                    // FaultNum: 399
   FLT__MAG_FLT_42,                    // FaultNum: 400
   FLT__MAG_FLT_43,                    // FaultNum: 401
   FLT__MAG_FLT_44,                    // FaultNum: 402
   FLT__MAG_FLT_45,                    // FaultNum: 403
   FLT__MAG_FLT_46,                    // FaultNum: 404
   FLT__MAG_FLT_47,                    // FaultNum: 405
   FLT__MAG_FLT_48,                    // FaultNum: 406
   FLT__MAG_FLT_49,                    // FaultNum: 407
   FLT__MAG_FLT_50,                    // FaultNum: 408
   FLT__MAG_FLT_51,                    // FaultNum: 409
   FLT__MAG_FLT_52,                    // FaultNum: 410
   FLT__MAG_FLT_53,                    // FaultNum: 411
   FLT__MAG_FLT_54,                    // FaultNum: 412
   FLT__MAG_FLT_55,                    // FaultNum: 413
   FLT__MAG_FLT_56,                    // FaultNum: 414
   FLT__MAG_FLT_57,                    // FaultNum: 415
   FLT__MAG_FLT_58,                    // FaultNum: 416
   FLT__MAG_FLT_59,                    // FaultNum: 417
   FLT__MAG_FLT_60,                    // FaultNum: 418
   FLT__MAG_FLT_61,                    // FaultNum: 419
   FLT__MAG_FLT_62,                    // FaultNum: 420
   FLT__MAG_FLT_63,                    // FaultNum: 421
   FLT__MAG_FLT_64,                    // FaultNum: 422
   FLT__MAG_FLT_65,                    // FaultNum: 423
   FLT__MAG_FLT_66,                    // FaultNum: 424
   FLT__MAG_FLT_67,                    // FaultNum: 425
   FLT__MAG_FLT_68,                    // FaultNum: 426
   FLT__MAG_FLT_69,                    // FaultNum: 427
   FLT__MAG_FLT_70,                    // FaultNum: 428
   FLT__MAG_FLT_71,                    // FaultNum: 429
   FLT__MAG_FLT_72,                    // FaultNum: 430
   FLT__KEB_FLT_1,                     // FaultNum: 431
   FLT__KEB_FLT_2,                     // FaultNum: 432
   FLT__KEB_FLT_3,                     // FaultNum: 433
   FLT__KEB_FLT_4,                     // FaultNum: 434
   FLT__KEB_FLT_5,                     // FaultNum: 435
   FLT__KEB_FLT_6,                     // FaultNum: 436
   FLT__KEB_FLT_7,                     // FaultNum: 437
   FLT__KEB_FLT_8,                     // FaultNum: 438
   FLT__KEB_FLT_9,                     // FaultNum: 439
   FLT__KEB_FLT_10,                    // FaultNum: 440
   FLT__KEB_FLT_11,                    // FaultNum: 441
   FLT__KEB_FLT_12,                    // FaultNum: 442
   FLT__KEB_FLT_13,                    // FaultNum: 443
   FLT__KEB_FLT_14,                    // FaultNum: 444
   FLT__KEB_FLT_15,                    // FaultNum: 445
   FLT__KEB_FLT_16,                    // FaultNum: 446
   FLT__KEB_FLT_17,                    // FaultNum: 447
   FLT__KEB_FLT_18,                    // FaultNum: 448
   FLT__KEB_FLT_19,                    // FaultNum: 449
   FLT__KEB_FLT_20,                    // FaultNum: 450
   FLT__KEB_FLT_21,                    // FaultNum: 451
   FLT__KEB_FLT_22,                    // FaultNum: 452
   FLT__KEB_FLT_23,                    // FaultNum: 453
   FLT__KEB_FLT_24,                    // FaultNum: 454
   FLT__KEB_FLT_25,                    // FaultNum: 455
   FLT__KEB_FLT_26,                    // FaultNum: 456
   FLT__KEB_FLT_27,                    // FaultNum: 457
   FLT__KEB_FLT_28,                    // FaultNum: 458
   FLT__KEB_FLT_29,                    // FaultNum: 459
   FLT__KEB_FLT_30,                    // FaultNum: 460
   FLT__KEB_FLT_31,                    // FaultNum: 461
   FLT__KEB_FLT_32,                    // FaultNum: 462
   FLT__KEB_FLT_33,                    // FaultNum: 463
   FLT__KEB_FLT_34,                    // FaultNum: 464
   FLT__KEB_FLT_35,                    // FaultNum: 465
   FLT__KEB_FLT_36,                    // FaultNum: 466
   FLT__KEB_FLT_37,                    // FaultNum: 467
   FLT__KEB_FLT_38,                    // FaultNum: 468
   FLT__KEB_FLT_39,                    // FaultNum: 469
   FLT__KEB_FLT_40,                    // FaultNum: 470
   FLT__KEB_FLT_41,                    // FaultNum: 471
   FLT__KEB_FLT_42,                    // FaultNum: 472
   FLT__KEB_FLT_43,                    // FaultNum: 473
   FLT__KEB_FLT_44,                    // FaultNum: 474
   FLT__KEB_FLT_45,                    // FaultNum: 475
   FLT__KEB_FLT_46,                    // FaultNum: 476
   FLT__KEB_FLT_47,                    // FaultNum: 477
   FLT__KEB_FLT_48,                    // FaultNum: 478
   FLT__KEB_FLT_49,                    // FaultNum: 479
   FLT__KEB_FLT_50,                    // FaultNum: 480
   FLT__KEB_FLT_51,                    // FaultNum: 481
   FLT__KEB_FLT_52,                    // FaultNum: 482
   FLT__KEB_FLT_53,                    // FaultNum: 483
   FLT__KEB_FLT_54,                    // FaultNum: 484
   FLT__KEB_FLT_55,                    // FaultNum: 485
   FLT__KEB_FLT_56,                    // FaultNum: 486
   FLT__KEB_FLT_57,                    // FaultNum: 487
   FLT__KEB_FLT_58,                    // FaultNum: 488
   FLT__KEB_FLT_59,                    // FaultNum: 489
   FLT__KEB_FLT_60,                    // FaultNum: 490
   FLT__KEB_FLT_61,                    // FaultNum: 491
   FLT__KEB_FLT_62,                    // FaultNum: 492
   FLT__KEB_FLT_63,                    // FaultNum: 493
   FLT__KEB_FLT_64,                    // FaultNum: 494
   FLT__KEB_FLT_65,                    // FaultNum: 495
   FLT__KEB_FLT_66,                    // FaultNum: 496
   FLT__KEB_FLT_67,                    // FaultNum: 497
   FLT__KEB_FLT_68,                    // FaultNum: 498
   FLT__KEB_FLT_69,                    // FaultNum: 499
   FLT__KEB_FLT_70,                    // FaultNum: 500
   FLT__KEB_FLT_71,                    // FaultNum: 501
   FLT__KEB_FLT_72,                    // FaultNum: 502
   FLT__KEB_FLT_73,                    // FaultNum: 503
   FLT__KEB_FLT_74,                    // FaultNum: 504
   FLT__KEB_FLT_75,                    // FaultNum: 505
   FLT__KEB_FLT_76,                    // FaultNum: 506
   FLT__KEB_FLT_77,                    // FaultNum: 507
   FLT__KEB_FLT_78,                    // FaultNum: 508
   FLT__KEB_FLT_79,                    // FaultNum: 509
   FLT__KEB_FLT_80,                    // FaultNum: 510
   FLT__KEB_FLT_81,                    // FaultNum: 511
   FLT__KEB_FLT_82,                    // FaultNum: 512
   FLT__KEB_FLT_83,                    // FaultNum: 513
   FLT__KEB_FLT_84,                    // FaultNum: 514
   FLT__KEB_FLT_85,                    // FaultNum: 515
   FLT__KEB_FLT_86,                    // FaultNum: 516
   FLT__KEB_FLT_87,                    // FaultNum: 517
   FLT__KEB_FLT_88,                    // FaultNum: 518
   FLT__KEB_FLT_89,                    // FaultNum: 519
   FLT__KEB_FLT_90,                    // FaultNum: 520
   FLT__KEB_FLT_91,                    // FaultNum: 521
   FLT__KEB_FLT_92,                    // FaultNum: 522
   FLT__KEB_FLT_93,                    // FaultNum: 523
   FLT__KEB_FLT_94,                    // FaultNum: 524
   FLT__KEB_FLT_95,                    // FaultNum: 525
   FLT__KEB_FLT_96,                    // FaultNum: 526
   FLT__KEB_FLT_97,                    // FaultNum: 527
   FLT__KEB_FLT_98,                    // FaultNum: 528
   FLT__KEB_FLT_99,                    // FaultNum: 529
   FLT__KEB_FLT_100,                   // FaultNum: 530
   FLT__KEB_FLT_101,                   // FaultNum: 531
   FLT__KEB_FLT_102,                   // FaultNum: 532
   FLT__KEB_FLT_103,                   // FaultNum: 533
   FLT__KEB_FLT_104,                   // FaultNum: 534
   FLT__KEB_FLT_105,                   // FaultNum: 535
   FLT__KEB_FLT_106,                   // FaultNum: 536
   FLT__KEB_FLT_107,                   // FaultNum: 537
   FLT__KEB_FLT_108,                   // FaultNum: 538
   FLT__KEB_FLT_109,                   // FaultNum: 539
   FLT__KEB_FLT_110,                   // FaultNum: 540
   FLT__KEB_FLT_111,                   // FaultNum: 541
   FLT__KEB_FLT_112,                   // FaultNum: 542
   FLT__KEB_FLT_113,                   // FaultNum: 543
   FLT__KEB_FLT_114,                   // FaultNum: 544
   FLT__KEB_FLT_115,                   // FaultNum: 545
   FLT__KEB_FLT_116,                   // FaultNum: 546
   FLT__KEB_FLT_117,                   // FaultNum: 547
   FLT__KEB_FLT_118,                   // FaultNum: 548
   FLT__KEB_FLT_119,                   // FaultNum: 549
   FLT__KEB_FLT_120,                   // FaultNum: 550
   FLT__KEB_FLT_121,                   // FaultNum: 551
   FLT__KEB_FLT_122,                   // FaultNum: 552
   FLT__KEB_FLT_123,                   // FaultNum: 553
   FLT__KEB_FLT_124,                   // FaultNum: 554
   FLT__KEB_FLT_125,                   // FaultNum: 555
   FLT__KEB_FLT_126,                   // FaultNum: 556
   FLT__KEB_FLT_127,                   // FaultNum: 557
   FLT__KEB_FLT_128,                   // FaultNum: 558
   FLT__KEB_FLT_129,                   // FaultNum: 559
   FLT__KEB_FLT_130,                   // FaultNum: 560
   FLT__KEB_FLT_131,                   // FaultNum: 561
   FLT__KEB_FLT_132,                   // FaultNum: 562
   FLT__KEB_FLT_133,                   // FaultNum: 563
   FLT__KEB_FLT_134,                   // FaultNum: 564
   FLT__KEB_FLT_135,                   // FaultNum: 565
   FLT__KEB_FLT_136,                   // FaultNum: 566
   FLT__KEB_FLT_137,                   // FaultNum: 567
   FLT__KEB_FLT_138,                   // FaultNum: 568
   FLT__KEB_FLT_139,                   // FaultNum: 569
   FLT__KEB_FLT_140,                   // FaultNum: 570
   FLT__KEB_FLT_141,                   // FaultNum: 571
   FLT__KEB_FLT_142,                   // FaultNum: 572
   FLT__KEB_FLT_143,                   // FaultNum: 573
   FLT__KEB_FLT_144,                   // FaultNum: 574
   FLT__KEB_FLT_145,                   // FaultNum: 575
   FLT__KEB_FLT_146,                   // FaultNum: 576
   FLT__KEB_FLT_147,                   // FaultNum: 577
   FLT__KEB_FLT_148,                   // FaultNum: 578
   FLT__KEB_FLT_149,                   // FaultNum: 579
   FLT__KEB_FLT_150,                   // FaultNum: 580
   FLT__KEB_FLT_151,                   // FaultNum: 581
   FLT__KEB_FLT_152,                   // FaultNum: 582
   FLT__KEB_FLT_153,                   // FaultNum: 583
   FLT__KEB_FLT_154,                   // FaultNum: 584
   FLT__KEB_FLT_155,                   // FaultNum: 585
   FLT__KEB_FLT_156,                   // FaultNum: 586
   FLT__KEB_FLT_157,                   // FaultNum: 587
   FLT__KEB_FLT_158,                   // FaultNum: 588
   FLT__KEB_FLT_159,                   // FaultNum: 589
   FLT__KEB_FLT_160,                   // FaultNum: 590
   FLT__KEB_FLT_161,                   // FaultNum: 591
   FLT__KEB_FLT_162,                   // FaultNum: 592
   FLT__KEB_FLT_163,                   // FaultNum: 593
   FLT__KEB_FLT_164,                   // FaultNum: 594
   FLT__KEB_FLT_165,                   // FaultNum: 595
   FLT__KEB_FLT_166,                   // FaultNum: 596
   FLT__KEB_FLT_167,                   // FaultNum: 597
   FLT__KEB_FLT_168,                   // FaultNum: 598
   FLT__KEB_FLT_169,                   // FaultNum: 599
   FLT__KEB_FLT_170,                   // FaultNum: 600
   FLT__KEB_FLT_171,                   // FaultNum: 601
   FLT__KEB_FLT_172,                   // FaultNum: 602
   FLT__KEB_FLT_173,                   // FaultNum: 603
   FLT__KEB_FLT_174,                   // FaultNum: 604
   FLT__KEB_FLT_175,                   // FaultNum: 605
   FLT__KEB_FLT_176,                   // FaultNum: 606
   FLT__KEB_FLT_177,                   // FaultNum: 607
   FLT__KEB_FLT_178,                   // FaultNum: 608
   FLT__KEB_FLT_179,                   // FaultNum: 609
   FLT__KEB_FLT_180,                   // FaultNum: 610
   FLT__KEB_FLT_181,                   // FaultNum: 611
   FLT__KEB_FLT_182,                   // FaultNum: 612
   FLT__KEB_FLT_183,                   // FaultNum: 613
   FLT__KEB_FLT_184,                   // FaultNum: 614
   FLT__KEB_FLT_185,                   // FaultNum: 615
   FLT__KEB_FLT_186,                   // FaultNum: 616
   FLT__KEB_FLT_187,                   // FaultNum: 617
   FLT__KEB_FLT_188,                   // FaultNum: 618
   FLT__KEB_FLT_189,                   // FaultNum: 619
   FLT__KEB_FLT_190,                   // FaultNum: 620
   FLT__KEB_FLT_191,                   // FaultNum: 621
   FLT__KEB_FLT_192,                   // FaultNum: 622
   FLT__KEB_FLT_193,                   // FaultNum: 623
   FLT__KEB_FLT_194,                   // FaultNum: 624
   FLT__KEB_FLT_195,                   // FaultNum: 625
   FLT__KEB_FLT_196,                   // FaultNum: 626
   FLT__KEB_FLT_197,                   // FaultNum: 627
   FLT__KEB_FLT_198,                   // FaultNum: 628
   FLT__KEB_FLT_199,                   // FaultNum: 629
   FLT__KEB_FLT_200,                   // FaultNum: 630
   FLT__KEB_FLT_201,                   // FaultNum: 631
   FLT__KEB_FLT_202,                   // FaultNum: 632
   FLT__KEB_FLT_203,                   // FaultNum: 633
   FLT__KEB_FLT_204,                   // FaultNum: 634
   FLT__KEB_FLT_205,                   // FaultNum: 635
   FLT__KEB_FLT_206,                   // FaultNum: 636
   FLT__KEB_FLT_207,                   // FaultNum: 637
   FLT__KEB_FLT_208,                   // FaultNum: 638
   FLT__KEB_FLT_209,                   // FaultNum: 639
   FLT__KEB_FLT_210,                   // FaultNum: 640
   FLT__KEB_FLT_211,                   // FaultNum: 641
   FLT__KEB_FLT_212,                   // FaultNum: 642
   FLT__KEB_FLT_213,                   // FaultNum: 643
   FLT__KEB_FLT_214,                   // FaultNum: 644
   FLT__KEB_FLT_215,                   // FaultNum: 645
   FLT__KEB_FLT_216,                   // FaultNum: 646
   FLT__KEB_FLT_217,                   // FaultNum: 647
   FLT__KEB_FLT_218,                   // FaultNum: 648
   FLT__KEB_FLT_219,                   // FaultNum: 649
   FLT__KEB_FLT_220,                   // FaultNum: 650
   FLT__KEB_FLT_221,                   // FaultNum: 651
   FLT__KEB_FLT_222,                   // FaultNum: 652
   FLT__KEB_FLT_223,                   // FaultNum: 653
   FLT__INVALID_LAND_OFF,              // FaultNum: 654
   FLT__PAYMENT_PASSCODE,              // FaultNum: 655
   FLT__BATTERY_FAULT,                 // FaultNum: 656
   FLT__INVALID_ETSL_1,                // FaultNum: 657
   FLT__INVALID_ETSL_2,                // FaultNum: 658
   FLT__INVALID_ETSL_3,                // FaultNum: 659
   FLT__INVALID_ETSL_4,                // FaultNum: 660
   FLT__UNUSED661,                     // FaultNum: 661
   FLT__UNUSED662,                     // FaultNum: 662
   FLT__CEDES3_OFFLINE,                // FaultNum: 663
   FLT__CEDES3_READ_FAIL,              // FaultNum: 664
   FLT__CEDES3_ALIGN_CLOSE,            // FaultNum: 665
   FLT__CEDES3_ALIGN_FAR,              // FaultNum: 666
   FLT__CEDES3_ALIGN_LEFT,             // FaultNum: 667
   FLT__CEDES3_ALIGN_RIGHT,            // FaultNum: 668
   FLT__CEDES3_INTERNAL,               // FaultNum: 669
   FLT__CEDES3_COMM,                   // FaultNum: 670
   FLT__CEDES3_CROSS1_POS,             // FaultNum: 671
   FLT__CEDES3_CROSS1_VEL,             // FaultNum: 672
   FLT__CEDES3_CROSS1_BOTH,            // FaultNum: 673
   FLT__CEDES3_CROSS2_POS,             // FaultNum: 674
   FLT__CEDES3_CROSS2_VEL,             // FaultNum: 675
   FLT__CEDES3_CROSS2_BOTH,            // FaultNum: 676
   FLT__RSBUFFER_P1,                   // FaultNum: 677
   FLT__RSBUFFER_P2,                   // FaultNum: 678
   FLT__RSBUFFER_P3,                   // FaultNum: 679
   FLT__RSBUFFER_P4,                   // FaultNum: 680
   FLT__OVERSPEED_UETS_1,              // FaultNum: 681
   FLT__OVERSPEED_UETS_2,              // FaultNum: 682
   FLT__OVERSPEED_UETS_3,              // FaultNum: 683
   FLT__OVERSPEED_UETS_4,              // FaultNum: 684
   FLT__OVERSPEED_UETS_5,              // FaultNum: 685
   FLT__OVERSPEED_UETS_6,              // FaultNum: 686
   FLT__OVERSPEED_UETS_7,              // FaultNum: 687
   FLT__OVERSPEED_UETS_8,              // FaultNum: 688
   FLT__OVERSPEED_DETS_1,              // FaultNum: 689
   FLT__OVERSPEED_DETS_2,              // FaultNum: 690
   FLT__OVERSPEED_DETS_3,              // FaultNum: 691
   FLT__OVERSPEED_DETS_4,              // FaultNum: 692
   FLT__OVERSPEED_DETS_5,              // FaultNum: 693
   FLT__OVERSPEED_DETS_6,              // FaultNum: 694
   FLT__OVERSPEED_DETS_7,              // FaultNum: 695
   FLT__OVERSPEED_DETS_8,              // FaultNum: 696
   FLT__OVERSPEED_UETSL_1,             // FaultNum: 697
   FLT__OVERSPEED_UETSL_2,             // FaultNum: 698
   FLT__OVERSPEED_UETSL_3,             // FaultNum: 699
   FLT__OVERSPEED_UETSL_4,             // FaultNum: 700
   FLT__OVERSPEED_UETSL_5,             // FaultNum: 701
   FLT__OVERSPEED_UETSL_6,             // FaultNum: 702
   FLT__OVERSPEED_UETSL_7,             // FaultNum: 703
   FLT__OVERSPEED_UETSL_8,             // FaultNum: 704
   FLT__OVERSPEED_DETSL_1,             // FaultNum: 705
   FLT__OVERSPEED_DETSL_2,             // FaultNum: 706
   FLT__OVERSPEED_DETSL_3,             // FaultNum: 707
   FLT__OVERSPEED_DETSL_4,             // FaultNum: 708
   FLT__OVERSPEED_DETSL_5,             // FaultNum: 709
   FLT__OVERSPEED_DETSL_6,             // FaultNum: 710
   FLT__OVERSPEED_DETSL_7,             // FaultNum: 711
   FLT__OVERSPEED_DETSL_8,             // FaultNum: 712
   FLT__FAULT_INPUT,                   // FaultNum: 713
   FLT__DRIVE_FLT_UNKNOWN,             // FaultNum: 714
   FLT__FRAM_DATA_CORRUPTION,          // FaultNum: 715
   FLT__MAX_RUNS_PER_MINUTE,           // FaultNum: 716
   FLT__NEED_TO_CYCLE_PWR_CT,          // FaultNum: 717
   FLT__NEED_TO_CYCLE_PWR_COP,         // FaultNum: 718
   FLT__TCL_F_OPEN,                    // FaultNum: 719
   FLT__MCL_F_OPEN,                    // FaultNum: 720
   FLT__BCL_F_OPEN,                    // FaultNum: 721
   FLT__TCL_R_OPEN,                    // FaultNum: 722
   FLT__MCL_R_OPEN,                    // FaultNum: 723
   FLT__BCL_R_OPEN,                    // FaultNum: 724
   FLT__INVALID_EPWR_SPD,              // FaultNum: 725
   FLT__INVALID_ACCESS_SPD,            // FaultNum: 726
   FLT__UNINTENDED_LCK_AND_GSW,        // FaultNum: 727
   FLT__DPMF_OPEN,                     // FaultNum: 728
   FLT__DPMR_OPEN,                     // FaultNum: 729
   FLT__CPLD_2_MR_STARTUP,             // FaultNum: 730
   FLT__CPLD_2_CT_STARTUP,             // FaultNum: 731
   FLT__CPLD_2_COP_STARTUP,            // FaultNum: 732
   FLT__CPLD_2_UNINT_MOV,              // FaultNum: 733
   FLT__CPLD_2_CT_COM_LOSS,            // FaultNum: 734
   FLT__CPLD_2_COP_COM_LOSS,           // FaultNum: 735
   FLT__CPLD_2_120VAC,                 // FaultNum: 736
   FLT__CPLD_2_GOVERNOR,               // FaultNum: 737
   FLT__CPLD_2_CAR_BYP,                // FaultNum: 738
   FLT__CPLD_2_HALL_BYP,               // FaultNum: 739
   FLT__CPLD_2_SFM_LOSS,               // FaultNum: 740
   FLT__CPLD_2_SFH_LOSS,               // FaultNum: 741
   FLT__CPLD_2_PIT_LOSS,               // FaultNum: 742
   FLT__CPLD_2_BUF_LOSS,               // FaultNum: 743
   FLT__CPLD_2_TFL_LOSS,               // FaultNum: 744
   FLT__CPLD_2_BFL_LOSS,               // FaultNum: 745
   FLT__CPLD_2_CT_SW_LOSS,             // FaultNum: 746
   FLT__CPLD_2_ESC_HATCH_LOSS,         // FaultNum: 747
   FLT__CPLD_2_CAR_SAFE_LOSS,          // FaultNum: 748
   FLT__CPLD_2_IC_STOP,                // FaultNum: 749
   FLT__CPLD_2_FIRE_STOP,              // FaultNum: 750
   FLT__CPLD_2_INSP,                   // FaultNum: 751
   FLT__CPLD_2_ACCESS,                 // FaultNum: 752
   FLT__CPLD_2_LFT,                    // FaultNum: 753
   FLT__CPLD_2_LFM,                    // FaultNum: 754
   FLT__CPLD_2_LFB,                    // FaultNum: 755
   FLT__CPLD_2_LRT,                    // FaultNum: 756
   FLT__CPLD_2_LRM,                    // FaultNum: 757
   FLT__CPLD_2_LRB,                    // FaultNum: 758
   FLT__CPLD_2_GSWF,                   // FaultNum: 759
   FLT__CPLD_2_GSWR,                   // FaultNum: 760
   FLT__PF_PIT_INSP,                   // FaultNum: 761
   FLT__PF_LND_INSP,                   // FaultNum: 762
   FLT__PF_BFL,                        // FaultNum: 763
   FLT__PF_TFL,                        // FaultNum: 764
   FLT__PF_BUF,                        // FaultNum: 765
   FLT__PF_PIT,                        // FaultNum: 766
   FLT__PF_GOV,                        // FaultNum: 767
   FLT__PF_SFH,                        // FaultNum: 768
   FLT__PF_SFM,                        // FaultNum: 769
   FLT__PF_LFT,                        // FaultNum: 770
   FLT__PF_LFM,                        // FaultNum: 771
   FLT__PF_LFB,                        // FaultNum: 772
   FLT__PF_LRT,                        // FaultNum: 773
   FLT__PF_LRM,                        // FaultNum: 774
   FLT__PF_LRB,                        // FaultNum: 775
   FLT__PF_BYP_H,                      // FaultNum: 776
   FLT__PF_BYP_C,                      // FaultNum: 777
   FLT__PF_MR_INSP,                    // FaultNum: 778
   FLT__PF_CPLD_PICK_BYP,              // FaultNum: 779
   FLT__PF_MCU_PICK_BYP,               // FaultNum: 780
   FLT__PF_MCU_DROP_GRIP,              // FaultNum: 781
   FLT__PF_CPLD_DROP_GRIP,             // FaultNum: 782
   FLT__PF_CPLD_PICK_GRIP,             // FaultNum: 783
   FLT__PF_MCU_PICK_GRIP,              // FaultNum: 784
   FLT__PF_MCU_DROP_BYP,               // FaultNum: 785
   FLT__PF_CPLD_DROP_BYP,              // FaultNum: 786
   FLT__CPLD_MR_UNKNOWN,               // FaultNum: 787
   FLT__PF_CT_SW,                      // FaultNum: 788
   FLT__PF_ESC_HATCH,                  // FaultNum: 789
   FLT__PF_CAR_SAFE,                   // FaultNum: 790
   FLT__PF_CT_INSP,                    // FaultNum: 791
   FLT__PF_GSWF,                       // FaultNum: 792
   FLT__PF_GSWR,                       // FaultNum: 793
   FLT__PF_DZF,                        // FaultNum: 794
   FLT__PF_DZR,                        // FaultNum: 795
   FLT__CPLD_CT_UNKNOWN,               // FaultNum: 796
   FLT__PF_HA_INSP,                    // FaultNum: 797
   FLT__PF_IC_STOP,                    // FaultNum: 798
   FLT__PF_FSS,                        // FaultNum: 799
   FLT__PF_IC_INSP,                    // FaultNum: 800
   FLT__CPLD_COP_UNKNOWN,              // FaultNum: 801
   FLT__BRAKE_OVERHEAT,                // FaultNum: 802
   FLT__EBRAKE_OVERHEAT,               // FaultNum: 803
   FLT__MOTION_PICK_B1,                // FaultNum: 804
   FLT__OVERSPEED_DOOR_DPM_F,          // FaultNum: 805
   FLT__OVERSPEED_DOOR_DPM_R,          // FaultNum: 806
   FLT__EQ,                            // FaultNum: 807
   FLT__PHE_TEST_FAILED,               // FaultNum: 808
   FLT__MOTION_PREPARE_GSWF,           // FaultNum: 809
   FLT__MOTION_PREPARE_LFT,            // FaultNum: 810
   FLT__MOTION_PREPARE_LFM,            // FaultNum: 811
   FLT__MOTION_PREPARE_DPM_F,          // FaultNum: 812
   FLT__MOTION_PREPARE_LFB,            // FaultNum: 813
   FLT__MOTION_PREPARE_GSWR,           // FaultNum: 814
   FLT__MOTION_PREPARE_LRT,            // FaultNum: 815
   FLT__MOTION_PREPARE_LRM,            // FaultNum: 816
   FLT__MOTION_PREPARE_LRB,            // FaultNum: 817
   FLT__MOTION_PREPARE_DPM_R,          // FaultNum: 818
   FLT__MOTION_ACCEL_GSWF,             // FaultNum: 819
   FLT__MOTION_ACCEL_LFT,              // FaultNum: 820
   FLT__MOTION_ACCEL_LFM,              // FaultNum: 821
   FLT__MOTION_ACCEL_LFB,              // FaultNum: 822
   FLT__MOTION_ACCEL_DPM_F,            // FaultNum: 823
   FLT__MOTION_ACCEL_GSWR,             // FaultNum: 824
   FLT__MOTION_ACCEL_LRT,              // FaultNum: 825
   FLT__MOTION_ACCEL_LRM,              // FaultNum: 826
   FLT__MOTION_ACCEL_LRB,              // FaultNum: 827
   FLT__MOTION_ACCEL_DPM_R,            // FaultNum: 828
   FLT__MOTION_PREPARE_DCL_F,          // FaultNum: 829
   FLT__MOTION_PREPARE_DCL_R,          // FaultNum: 830
   FLT__MOTION_PREPARE_DOL_F,          // FaultNum: 831
   FLT__MOTION_PREPARE_DOL_R,          // FaultNum: 832
   FLT__MOTION_ACCEL_DCL_F,            // FaultNum: 833
   FLT__MOTION_ACCEL_DCL_R,            // FaultNum: 834
   FLT__MOTION_ACCEL_DOL_F,            // FaultNum: 835
   FLT__MOTION_ACCEL_DOL_R,            // FaultNum: 836
   FLT__VALVE_UNKNOWN,                 // FaultNum: 837
   FLT__VALVE_POR_RST,                 // FaultNum: 838
   FLT__VALVE_WDT_RST,                 // FaultNum: 839
   FLT__VALVE_BOD_RST,                 // FaultNum: 840
   FLT__VALVE_COMM_LOSS,               // FaultNum: 841
   FLT__VALVE_LOW_DN,                  // FaultNum: 842
   FLT__VALVE_LOW_UP,                  // FaultNum: 843
   FLT__VALVE_HIGH_DN,                 // FaultNum: 844
   FLT__VALVE_HIGH_UP,                 // FaultNum: 845
   FLT__VALVE_START_MOTOR,             // FaultNum: 846
   FLT__VALVE_CAN_BUS_RST,             // FaultNum: 847
   FLT__SOFT_STARTER_UNKNOWN,          // FaultNum: 848
   FLT__SOFT_STARTER_POR_RST,          // FaultNum: 849
   FLT__SOFT_STARTER_WDT_RST,          // FaultNum: 850
   FLT__SOFT_STARTER_BOD_RST,          // FaultNum: 851
   FLT__SOFT_STARTER_COMM_LOSS,        // FaultNum: 852
   FLT__SOFT_STARTER_OVERCURRENT,      // FaultNum: 853
   FLT__SOFT_STARTER_OVERVOLTAGE,      // FaultNum: 854
   FLT__SOFT_STARTER_UNDERVOLTAGE,     // FaultNum: 855
   FLT__SOFT_STARTER_PHASE_MISSING,    // FaultNum: 856
   FLT__SOFT_STARTER_PHASE_SEQUENCE,   // FaultNum: 857
   FLT__SOFT_STARTER_CAN_BUS_RST,      // FaultNum: 858
   FLT__VALVE_OFFLINE,                 // FaultNum: 859
   FLT__SOFT_STARTER_OFFLINE,          // FaultNum: 860

   NUM_FAULTS // Total: 861
} en_faults;

/* Fault that will be stored to FRAM */
enum en_latching_safety_faults
{
   LSF__GOV,
   LSF__EB1_DROPPED,
   LSF__UNINTENDED_MOVEMENT,
   LSF__TRACTION_LOSS,
   LSF__OVERSPEED,

   NUM_LATCHING_SAFETY_FAULTS /* Must be less than 32! */};

// These are all the nodes that maintain a copy of the faults.
enum en_fault_nodes
{
   enFAULT_NODE__CPLD,
   enFAULT_NODE__MRA,
   enFAULT_NODE__MRB,
   enFAULT_NODE__CTA,
   enFAULT_NODE__CTB,
   enFAULT_NODE__COPA,
   enFAULT_NODE__COPB,

   NUM_FAULT_NODES,

   enFAULT_NODE__UNK,
};

/* FAULT_ENTRY(A, B, C, D, E, F, G) FORMAT:
 *    A = Unique Fault Number ID
 *    ------------------
 *    Fault Function Fields:
 *    B = en_faultentry_oos
 *    C = en_faultentry_con
 *    D = en_faultentry_cleartype
 *    E = en_faultentry_clearcalls
 *    F = en_faultentry_priority
 *    ------------------
 *    G = String
 * */

// OOS   If ON, the fault will contribute to the OOS limit.
typedef enum
{
   FT_OOS_OFF,
   FT_OOS__ON,
} en_faultentry_oos;
// Construction  If ON, the fault will be checked during construction operation.
typedef enum
{
   FT_CON_OFF,
   FT_CON__ON,
} en_faultentry_con;
typedef enum
{
   FT_CLR_NORM, // Self clears after 3 seconds,
   FT_CLR_LTCH, // Latches & persists after reset, (NOTE: see enum en_latching_safety_faults and GetLatchedFaultIndex_Plus1)
   FT_CLR_RSET, // Requires reset to clear
   FT_CLR_DIP1, // Requires DIP A1 and reset to clear,
} en_faultentry_cleartype;
typedef enum
{
   FT_CC_OFF,
   FT_CC__ON,
} en_faultentry_clearcalls;
typedef enum
{
   FT_PRI_000,// latching
   FT_PRI_001,//
   FT_PRI_002,// board hardware
   FT_PRI_003,// board config
   FT_PRI_004,// system hardware (const)
   FT_PRI_005,// system hardware (insp)
   FT_PRI_006,// Run (High)
   FT_PRI_007,// Run (Low)
   FT_PRI_008,
   FT_PRI_009,
   FT_PRI_010,
   FT_PRI_011,
   FT_PRI_012,
   FT_PRI_013,
   FT_PRI_014,
   FT_PRI_015,
   FT_PRI_016,
   FT_PRI_017,
   FT_PRI_018,
   FT_PRI_019,
   NUM_FT_PRI = 255
} en_faultentry_priority; /* Lowest value is highest priority */

/* Fault Table Definition */
#define FAULT_TABLE(FT_ENTRY)  \
   FT_ENTRY( FLT__NONE,                         FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "No Fault"                 ) \
   FT_ENTRY( FLT__GOV,                          FT_OOS__ON, FT_CON_OFF, FT_CLR_LTCH, FT_CC__ON, FT_PRI_000, "Governor"                 ) \
   FT_ENTRY( FLT__GOV_L,                        FT_OOS__ON, FT_CON_OFF, FT_CLR_LTCH, FT_CC__ON, FT_PRI_000, "Governor (L)"             ) \
   FT_ENTRY( FLT__EB1_DROP,                     FT_OOS__ON, FT_CON_OFF, FT_CLR_LTCH, FT_CC__ON, FT_PRI_001, "EB1 Drop"                 ) \
   FT_ENTRY( FLT__EB1_DROP_L,                   FT_OOS__ON, FT_CON_OFF, FT_CLR_LTCH, FT_CC__ON, FT_PRI_002, "EB1 Drop (L)"             ) \
   FT_ENTRY( FLT__UNINTENDED_MOV,               FT_OOS__ON, FT_CON_OFF, FT_CLR_LTCH, FT_CC__ON, FT_PRI_000, "Unintended Move"          ) \
   FT_ENTRY( FLT__UNINTENDED_MOV_L,             FT_OOS__ON, FT_CON_OFF, FT_CLR_LTCH, FT_CC__ON, FT_PRI_000, "Unintended Move (L)"      ) \
   FT_ENTRY( FLT__TRACTION_LOSS,                FT_OOS__ON, FT_CON_OFF, FT_CLR_LTCH, FT_CC__ON, FT_PRI_000, "Traction Loss"            ) \
   FT_ENTRY( FLT__TRACTION_LOSS_L,              FT_OOS__ON, FT_CON_OFF, FT_CLR_LTCH, FT_CC__ON, FT_PRI_000, "Traction Loss (L)"        ) \
   FT_ENTRY( FLT__SPEED_DEV,                    FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "Speed Dev"                ) \
   FT_ENTRY( FLT__IC_STOP_SW,                   FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "IC Stop Sw"               ) \
   FT_ENTRY( FLT__REDUNDANCY_LRB,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. LRB"               ) \
   FT_ENTRY( FLT__REDUNDANCY_LRM,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. LRM"               ) \
   FT_ENTRY( FLT__REDUNDANCY_LRT,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. LRT"               ) \
   FT_ENTRY( FLT__REDUNDANCY_LFB,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. LFB"               ) \
   FT_ENTRY( FLT__REDUNDANCY_LFM,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. LFM"               ) \
   FT_ENTRY( FLT__REDUNDANCY_LFT,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. LFT"               ) \
   FT_ENTRY( FLT__REDUNDANCY_ATU,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. ATU"               ) \
   FT_ENTRY( FLT__REDUNDANCY_ATD,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. ATD"               ) \
   FT_ENTRY( FLT__REDUNDANCY_ABU,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. ABU"               ) \
   FT_ENTRY( FLT__REDUNDANCY_ABD,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. ABD"               ) \
   FT_ENTRY( FLT__REDUNDANCY_CAR_BYP,           FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. Car Byp"           ) \
   FT_ENTRY( FLT__REDUNDANCY_HA_BYP,            FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. HA Byp"            ) \
   FT_ENTRY( FLT__REDUNDANCY_MM,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. MM"                ) \
   FT_ENTRY( FLT__REDUNDANCY_SFM,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. SFM"               ) \
   FT_ENTRY( FLT__REDUNDANCY_SFH,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. SFH"               ) \
   FT_ENTRY( FLT__REDUNDANCY_PIT,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. PIT"               ) \
   FT_ENTRY( FLT__REDUNDANCY_IP_INSP,           FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. IP Insp"           ) \
   FT_ENTRY( FLT__REDUNDANCY_MR_INSP,           FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. MR Insp"           ) \
   FT_ENTRY( FLT__REDUNDANCY_IL_INSP,           FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. IL Insp"           ) \
   FT_ENTRY( FLT__REDUNDANCY_C_EB2,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. C EB2"             ) \
   FT_ENTRY( FLT__REDUNDANCY_C_SFM,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. C SFM"             ) \
   FT_ENTRY( FLT__REDUNDANCY_M_EB2,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. M EB2"             ) \
   FT_ENTRY( FLT__REDUNDANCY_M_SFM,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. M SFM"             ) \
   FT_ENTRY( FLT__REDUNDANCY_M_EB3,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. M EB3"             ) \
   FT_ENTRY( FLT__REDUNDANCY_M_EB1,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. M EB1"             ) \
   FT_ENTRY( FLT__REDUNDANCY_M_SFP,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. M SFP"             ) \
   FT_ENTRY( FLT__REDUNDANCY_C_EB3,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. C EB3"             ) \
   FT_ENTRY( FLT__REDUNDANCY_C_EB1,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. C EB1"             ) \
   FT_ENTRY( FLT__REDUNDANCY_C_SFP,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Redun. C SFP"             ) \
   FT_ENTRY( FLT__REDUNDANCY_GSWR,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Redun. GSWR"              ) \
   FT_ENTRY( FLT__REDUNDANCY_GSWF,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Redun. GSWF"              ) \
   FT_ENTRY( FLT__REDUNDANCY_CT_INSP,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Redun. CT Insp"           ) \
   FT_ENTRY( FLT__REDUNDANCY_CT_STOP,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Redun. CT Stop Sw"        ) \
   FT_ENTRY( FLT__REDUNDANCY_ESC_HATCH,         FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Redun. Esc Hatch"         ) \
   FT_ENTRY( FLT__REDUNDANCY_CAR_SAFE,          FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Redun. Car Safety"        ) \
   FT_ENTRY( FLT__REDUNDANCY_FSS,               FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Redun. Fire Stop Sw"      ) \
   FT_ENTRY( FLT__REDUNDANCY_IC_STOP,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Redun. IC Stop"           ) \
   FT_ENTRY( FLT__REDUNDANCY_IC_INSP,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Redun. IC Insp"           ) \
   FT_ENTRY( FLT__REDUNDANCY_HA_INSP,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Redun. HA Insp"           ) \
   FT_ENTRY( FLT__SFP_STUCK_LO,                 FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "SFP Stuck Lo"             ) \
   FT_ENTRY( FLT__SFP_STUCK_HI,                 FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "SFP Stuck Hi"             ) \
   FT_ENTRY( FLT__SFP_DROPPED,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "SFP Drop"                 ) \
   FT_ENTRY( FLT__EB3_STUCK_LO,                 FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "EB3 Stuck Lo"             ) \
   FT_ENTRY( FLT__EB3_STUCK_HI,                 FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "EB3 Stuck Hi"             ) \
   FT_ENTRY( FLT__EB4_STUCK_LO,                 FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "EB4 Stuck Lo"             ) \
   FT_ENTRY( FLT__EB4_STUCK_HI,                 FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "EB4 Stuck Hi"             ) \
   FT_ENTRY( FLT__EB1_STUCK,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "EB1 Stuck"                ) \
   FT_ENTRY( FLT__M_CONT_STUCK_HI,              FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "M Cont. Stuck Hi"         ) \
   FT_ENTRY( FLT__M_CONT_STUCK_LO,              FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "M Cont. Stuck Lo"         ) \
   FT_ENTRY( FLT__B2_CONT_STUCK_HI,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "B2 Cont. Stuck Hi"        ) \
   FT_ENTRY( FLT__B2_CONT_STUCK_LO,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "B2 Cont. Stuck Lo"        ) \
   FT_ENTRY( FLT__HA_BYPASS,                    FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "HA Bypass Sw"             ) \
   FT_ENTRY( FLT__CAR_BYPASS,                   FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Car Bypass Sw"            ) \
   FT_ENTRY( FLT__OVERSPEED_GENERAL,            FT_OOS__ON, FT_CON_OFF, FT_CLR_LTCH, FT_CC__ON, FT_PRI_000, "General OVSP"             ) \
   FT_ENTRY( FLT__OVERSPEED_GENERAL_L,          FT_OOS__ON, FT_CON_OFF, FT_CLR_LTCH, FT_CC__ON, FT_PRI_001, "General OVSP (L)"         ) \
   FT_ENTRY( FLT__OVERSPEED_INSPECTION,         FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Insp OVSP"                ) \
   FT_ENTRY( FLT__OVERSPEED_DOOR_GSWF,          FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Door OVSP GSWF"           ) \
   FT_ENTRY( FLT__OVERSPEED_DOORS_LFT,          FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Door OVSP LFT"            ) \
   FT_ENTRY( FLT__OVERSPEED_DOORS_LFM,          FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Door OVSP LFM"            ) \
   FT_ENTRY( FLT__OVERSPEED_DOORS_LFB,          FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Door OVSP LFB"            ) \
   FT_ENTRY( FLT__OVERSPEED_DOORS_GSWR,         FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Door OVSP GSWR"           ) \
   FT_ENTRY( FLT__OVERSPEED_DOORS_LRT,          FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Door OVSP LRT"            ) \
   FT_ENTRY( FLT__OVERSPEED_DOORS_LRM,          FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Door OVSP LRM"            ) \
   FT_ENTRY( FLT__OVERSPEED_DOORS_LRB,          FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Door OVSP LRB"            ) \
   FT_ENTRY( FLT__FLOOD_OOS,                    FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Flood OOS"                ) \
   FT_ENTRY( FLT__DOOR_INVALID,                 FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Door Invalid"             ) \
   FT_ENTRY( FLT__CPU_STOP_SW_MRA,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "MRA CPU Stop Sw"          ) \
   FT_ENTRY( FLT__CPU_STOP_SW_MRB,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "MRB CPU Stop Sw"          ) \
   FT_ENTRY( FLT__CPU_STOP_SW_CTA,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "CTA CPU Stop Sw"          ) \
   FT_ENTRY( FLT__CPU_STOP_SW_CTB,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "CTB CPU Stop Sw"          ) \
   FT_ENTRY( FLT__CPU_STOP_SW_COPA,             FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "COPA CPU Stop Sw"         ) \
   FT_ENTRY( FLT__CPU_STOP_SW_COPB,             FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "COPB CPU Stop Sw"         ) \
   FT_ENTRY( FLT__NEED_TO_CYCLE_PWR_MR,         FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_003, "Need To Cycle Pwr MR"     ) \
   FT_ENTRY( FLT__INVALID_NUM_FLOORS,           FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_003, "Inv. Num Floors"          ) \
   FT_ENTRY( FLT__INVALID_CONTRACT_SPD,         FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_003, "Inv. Contract Spd"        ) \
   FT_ENTRY( FLT__INVALID_INSP_SPD,             FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_003, "Inv. Insp Spd"            ) \
   FT_ENTRY( FLT__INVALID_LEARN_SPD,            FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_003, "Inv. Learn Spd"           ) \
   FT_ENTRY( FLT__INVALID_TERM_SPD,             FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_003, "Inv. Term Spd"            ) \
   FT_ENTRY( FLT__INVALID_LEVEL_SPD,            FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_003, "Inv. Level Spd"           ) \
   FT_ENTRY( FLT__INVALID_NTSD_SPD,             FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_003, "Inv. NTSD Spd"            ) \
   FT_ENTRY( FLT__NEED_TO_LEARN,                FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Need To Learn"            ) \
   FT_ENTRY( FLT__INVALID_ETS_1,                FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Inv. ETS 1"               ) \
   FT_ENTRY( FLT__INVALID_ETS_2,                FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Inv. ETS 2"               ) \
   FT_ENTRY( FLT__INVALID_ETS_3,                FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Inv. ETS 3"               ) \
   FT_ENTRY( FLT__INVALID_ETS_4,                FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Inv. ETS 4"               ) \
   FT_ENTRY( FLT__AT_FLOOR_NO_DZ,               FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "At Floor No DZ"           ) \
   FT_ENTRY( FLT__FIRE_STOP_SW,                 FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "Fire Stop Switch"         ) \
   FT_ENTRY( FLT__DOOR_F_JUMPER_GSW,            FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "Door F Jumper GSW"        ) \
   FT_ENTRY( FLT__DOOR_F_JUMPER_LOCK,           FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "Door F Jumper Lock"       ) \
   FT_ENTRY( FLT__DOOR_F_LOCKS_OPEN,            FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "Door F Locks Open"        ) \
   FT_ENTRY( FLT__DOOR_F_GSW_OPEN,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "Door F GSW Open"          ) \
   FT_ENTRY( FLT__DOOR_F_FAIL_OPEN,             FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "Door F Fail Open"         ) \
   FT_ENTRY( FLT__DOOR_F_FAIL_CLOSE,            FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "Door F Fail Close"        ) \
   FT_ENTRY( FLT__DOOR_F_FAIL_NUDGE,            FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "Door F Fail Nudge"        ) \
   FT_ENTRY( FLT__DOOR_F_STALLED,               FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "Door F Stalled"           ) \
   FT_ENTRY( FLT__DOOR_F_LOST_SIGNAL,           FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "Door F Lost Signal"       ) \
   FT_ENTRY( FLT__DOOR_R_JUMPER_GSW,            FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "Door R Jumper GSW"        ) \
   FT_ENTRY( FLT__DOOR_R_JUMPER_LOCK,           FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "Door R Jumper Lock"       ) \
   FT_ENTRY( FLT__DOOR_R_LOCKS_OPEN,            FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "Door R Locks Open"        ) \
   FT_ENTRY( FLT__DOOR_R_GSW_OPEN,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "Door R GSW Open"          ) \
   FT_ENTRY( FLT__DOOR_R_FAIL_OPEN,             FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "Door R Fail Open"         ) \
   FT_ENTRY( FLT__DOOR_R_FAIL_CLOSE,            FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "Door R Fail Close"        ) \
   FT_ENTRY( FLT__DOOR_R_FAIL_NUDGE,            FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "Door R Fail Nudge"        ) \
   FT_ENTRY( FLT__DOOR_R_STALLED,               FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "Door R Stalled"           ) \
   FT_ENTRY( FLT__DOOR_R_LOST_SIGNAL,           FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_005, "Door R Lost Signal"       ) \
   FT_ENTRY( FLT__MAX_RUNTIME,                  FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Max Runtime"              ) \
   FT_ENTRY( FLT__EB_BYPASSED,                  FT_OOS__ON, FT_CON_OFF, FT_CLR_RSET, FT_CC__ON, NUM_FT_PRI, "EB Byp"                   ) \
   FT_ENTRY( FLT__PARAM_QUEUE_MRA,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "MRA Param OVF"            ) \
   FT_ENTRY( FLT__PARAM_QUEUE_MRB,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "MRB Param OVF"            ) \
   FT_ENTRY( FLT__PARAM_QUEUE_CTA,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "CTA Param OVF"            ) \
   FT_ENTRY( FLT__PARAM_QUEUE_CTB,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "CTB Param OVF"            ) \
   FT_ENTRY( FLT__PARAM_QUEUE_COPA,             FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "COPA Param OVF"           ) \
   FT_ENTRY( FLT__PARAM_QUEUE_COPB,             FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "COPB Param OVF"           ) \
   FT_ENTRY( FLT__OFFLINE_MRA_BY_CTA,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MRA Offline (CTA)"        ) \
   FT_ENTRY( FLT__OFFLINE_MRA_BY_COPA,          FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MRA Offline (COPA)"       ) \
   FT_ENTRY( FLT__OFFLINE_MRA_BY_MRB,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MRA Offline (MRB)"        ) \
   FT_ENTRY( FLT__OFFLINE_CTA_BY_MRA,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CTA Offline (MRA)"        ) \
   FT_ENTRY( FLT__OFFLINE_CTA_BY_COPA,          FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CTA Offline (COPA)"       ) \
   FT_ENTRY( FLT__OFFLINE_CTA_BY_CTB,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CTA Offline (CTB)"        ) \
   FT_ENTRY( FLT__OFFLINE_COPA_BY_MRA,          FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "COPA Offline (MRA)"       ) \
   FT_ENTRY( FLT__OFFLINE_COPA_BY_CTA,          FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "COPA Offline (CTA)"       ) \
   FT_ENTRY( FLT__OFFLINE_COPA_BY_COPB,         FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "COPA Offline (COPB)"      ) \
   FT_ENTRY( FLT__OFFLINE_MRB_BY_MRA,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MRB Offline (MRA)"        ) \
   FT_ENTRY( FLT__OFFLINE_CTB_BY_CTA,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CTB Offline (CTA)"        ) \
   FT_ENTRY( FLT__OFFLINE_COPB_BY_COPA,         FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "COPB Offline (COPA)"      ) \
   FT_ENTRY( FLT__BOARD_RESET_MRA,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "MRA Board Rst"            ) \
   FT_ENTRY( FLT__BOARD_RESET_MRB,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "MRB Board Rst"            ) \
   FT_ENTRY( FLT__BOARD_RESET_CTA,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "CTA Board Rst"            ) \
   FT_ENTRY( FLT__BOARD_RESET_CTB,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "CTB Board Rst"            ) \
   FT_ENTRY( FLT__BOARD_RESET_COPA,             FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "COPA Board Rst"           ) \
   FT_ENTRY( FLT__BOARD_RESET_COPB,             FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "COPB Board Rst"           ) \
   FT_ENTRY( FLT__WDT_RESET_MRA,                FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "MRA WDT Rst"              ) \
   FT_ENTRY( FLT__WDT_RESET_MRB,                FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "MRB WDT Rst"              ) \
   FT_ENTRY( FLT__WDT_RESET_CTA,                FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "CTA WDT Rst"              ) \
   FT_ENTRY( FLT__WDT_RESET_CTB,                FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "CTB WDT Rst"              ) \
   FT_ENTRY( FLT__WDT_RESET_COPA,               FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "COPA WDT Rst"             ) \
   FT_ENTRY( FLT__WDT_RESET_COPB,               FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "COPB WDT Rst"             ) \
   FT_ENTRY( FLT__BOD_RESET_MRA,                FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "MRA BOD Rst"              ) \
   FT_ENTRY( FLT__BOD_RESET_MRB,                FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "MRB BOD Rst"              ) \
   FT_ENTRY( FLT__BOD_RESET_CTA,                FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "CTA BOD Rst"              ) \
   FT_ENTRY( FLT__BOD_RESET_CTB,                FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "CTB BOD Rst"              ) \
   FT_ENTRY( FLT__BOD_RESET_COPA,               FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "COPA BOD Rst"             ) \
   FT_ENTRY( FLT__BOD_RESET_COPB,               FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "COPB BOD Rst"             ) \
   FT_ENTRY( FLT__SAFETY_STR_SFH,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "SS SFH"                   ) \
   FT_ENTRY( FLT__SAFETY_STR_SFM,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "SS SFM"                   ) \
   FT_ENTRY( FLT__SAFETY_STR_PIT,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "SS PIT"                   ) \
   FT_ENTRY( FLT__SAFETY_STR_BUF,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "SS BUF"                   ) \
   FT_ENTRY( FLT__SAFETY_STR_TFL,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "SS TFL"                   ) \
   FT_ENTRY( FLT__SAFETY_STR_BFL,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "SS BFL"                   ) \
   FT_ENTRY( FLT__SAFETY_STR_CT_SW,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "SS CT Stop Sw"            ) \
   FT_ENTRY( FLT__SAFETY_STR_ESC_HATCH,         FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "SS Esc Hatch"             ) \
   FT_ENTRY( FLT__SAFETY_STR_CAR_SAFE,          FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "SS Car Safeties"          ) \
   FT_ENTRY( FLT__LTF_OPEN,                     FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_004, "LFT Open"                 ) \
   FT_ENTRY( FLT__LMF_OPEN,                     FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_004, "LFM Open"                 ) \
   FT_ENTRY( FLT__LBF_OPEN,                     FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_004, "LFB Open"                 ) \
   FT_ENTRY( FLT__LTR_OPEN,                     FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_004, "LRT Open"                 ) \
   FT_ENTRY( FLT__LMR_OPEN,                     FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_004, "LRM Open"                 ) \
   FT_ENTRY( FLT__LBR_OPEN,                     FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_004, "LRB Open"                 ) \
   FT_ENTRY( FLT__GSWF_OPEN,                    FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_006, "GSWF Open"                ) \
   FT_ENTRY( FLT__GSWR_OPEN,                    FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_006, "GSWR Open"                ) \
   FT_ENTRY( FLT__FRAM_DEFAULTING,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_002, "FRAM Defaulting"          ) \
   FT_ENTRY( FLT__FRAM_TIMEOUT,                 FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_002, "FRAM Timeout"             ) \
   FT_ENTRY( FLT__FRAM_DEFAULT_FAIL,            FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_002, "FRAM Default Fail"        ) \
   FT_ENTRY( FLT__120VAC,                       FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_002, "120VAC Loss"              ) \
   FT_ENTRY( FLT__MOTION_INVALID_CMD,           FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Mo. Inv. Cmd"             ) \
   FT_ENTRY( FLT__MOTION_PREPARE_RUN,           FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Prepare Run"          ) \
   FT_ENTRY( FLT__MOTION_DRIVE_ENABLE,          FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Mo. Drive Enable"         ) \
   FT_ENTRY( FLT__MOTION_PICK_M,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Mo. Pick M"               ) \
   FT_ENTRY( FLT__MOTION_SPEED_REG,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Mo. Speed Reg"            ) \
   FT_ENTRY( FLT__MOTION_PICK_B2,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Mo. Pick B2"              ) \
   FT_ENTRY( FLT__MOTION_LIFT_BRAKE,            FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Mo. Lift Brake"           ) \
   FT_ENTRY( FLT__MOTION_ACCEL_DELAY,           FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Mo. Accel Delay"          ) \
   FT_ENTRY( FLT__MOTION_RAMP_TO_ZERO,          FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Mo. Ramp To Zero"         ) \
   FT_ENTRY( FLT__MOTION_HOLD_ZERO,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Mo. Hold Zero"            ) \
   FT_ENTRY( FLT__MOTION_CHECK_BPS,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Mo. Check BPS"            ) \
   FT_ENTRY( FLT__MOTION_DEENERGIZE,            FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Mo. Deenergize"           ) \
   FT_ENTRY( FLT__MOTION_DROP_M,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Mo. Drop M"               ) \
   FT_ENTRY( FLT__MOTION_PREFLIGHT,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Mo. Preflight"            ) \
   FT_ENTRY( FLT__BPS_STUCK_CLOSED,             FT_OOS__ON, FT_CON_OFF, FT_CLR_RSET, FT_CC__ON, NUM_FT_PRI, "BPS Stuck Closed"         ) \
   FT_ENTRY( FLT__BPS_STUCK_OPEN,               FT_OOS__ON, FT_CON_OFF, FT_CLR_RSET, FT_CC__ON, NUM_FT_PRI, "BPS Stuck Open"           ) \
   FT_ENTRY( FLT__EB2_DROPPED,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "EB2 Drop"                 ) \
   FT_ENTRY( FLT__EB2_STUCK,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "EB2 Stuck"                ) \
   FT_ENTRY( FLT__BRAKE_OFFLINE,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Brake Offline"            ) \
   FT_ENTRY( FLT__BRAKE_UNK,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Brake Unk."               ) \
   FT_ENTRY( FLT__BRAKE_POR_RST,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Brake POR Rst"            ) \
   FT_ENTRY( FLT__BRAKE_WDT_RST,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Brake WDT Rst"            ) \
   FT_ENTRY( FLT__BRAKE_COM,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Brake Comm Loss"          ) \
   FT_ENTRY( FLT__BRAKE_UNUSED5,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Brake Gate Flt"           ) \
   FT_ENTRY( FLT__BRAKE_MOSFET,                 FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Brake MOSFET"             ) \
   FT_ENTRY( FLT__BRAKE_BUS_OFFLINE,            FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Brake Bus Off"            ) \
   FT_ENTRY( FLT__BRAKE_DIP,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Brake DIP"                ) \
   FT_ENTRY( FLT__BRAKE_BOD_RST,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Brake BOD Rst"            ) \
   FT_ENTRY( FLT__BRAKE_AC_LOSS,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Brake AC Loss"            ) \
   FT_ENTRY( FLT__EBRAKE_OFFLINE,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "EBrake Offline"           ) \
   FT_ENTRY( FLT__EBRAKE_UNK,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "EBrake Unk."              ) \
   FT_ENTRY( FLT__EBRAKE_POR_RST,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "EBrake POR Rst"           ) \
   FT_ENTRY( FLT__EBRAKE_WDT_RST,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "EBrake WDT Rst"           ) \
   FT_ENTRY( FLT__EBRAKE_COM,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "EBrake Comm Loss"         ) \
   FT_ENTRY( FLT__EBRAKE_UNUSED5,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "EBrake Gate Flt"          ) \
   FT_ENTRY( FLT__EBRAKE_MOSFET,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "EBrake MOSFET"            ) \
   FT_ENTRY( FLT__EBRAKE_BUS_OFFLINE,           FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "EBrake Bus Off"           ) \
   FT_ENTRY( FLT__EBRAKE_DIP,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "EBrake DIP"               ) \
   FT_ENTRY( FLT__EBRAKE_BOD_RST,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "EBrake BOD Rst"           ) \
   FT_ENTRY( FLT__EBRAKE_AC_LOSS,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "EBrake AC Loss"           ) \
   FT_ENTRY( FLT__CPLD_STARTUP,                 FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Startup"             ) \
   FT_ENTRY( FLT__CPLD_UNINT_MOV,               FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Unint Mov"           ) \
   FT_ENTRY( FLT__CPLD_GOV,                     FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Governor"            ) \
   FT_ENTRY( FLT__CPLD_REDUND,                  FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Redundancy"          ) \
   FT_ENTRY( FLT__CPLD_COM_LOSS,                FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Comm Loss"           ) \
   FT_ENTRY( FLT__CPLD_NON_BYP,                 FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Non Bypass"          ) \
   FT_ENTRY( FLT__CPLD_IN_CAR,                  FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_004, "CPLD In Car"              ) \
   FT_ENTRY( FLT__CPLD_INSP,                    FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Insp."               ) \
   FT_ENTRY( FLT__CPLD_SFH,                     FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD SFH"                 ) \
   FT_ENTRY( FLT__CPLD_GRIPPER,                 FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Gripper"             ) \
   FT_ENTRY( FLT__CPLD_ACCESS,                  FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Access"              ) \
   FT_ENTRY( FLT__CPLD_LOCKS,                   FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_004, "CPLD Locks"               ) \
   FT_ENTRY( FLT__CPLD_DOORS,                   FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_004, "CPLD Doors"               ) \
   FT_ENTRY( FLT__CPLD_BYPASS_SW,               FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Bypass Sw"           ) \
   FT_ENTRY( FLT__CPLD_PREFLIGHT,               FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Preflight"           ) \
   FT_ENTRY( FLT__RISER_OFFLINE_1,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Riser Offline 1"          ) \
   FT_ENTRY( FLT__RISER_OFFLINE_2,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Riser Offline 2"          ) \
   FT_ENTRY( FLT__RISER_OFFLINE_3,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Riser Offline 3"          ) \
   FT_ENTRY( FLT__RISER_OFFLINE_4,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Riser Offline 4"          ) \
   FT_ENTRY( FLT__DZ_STUCK_HI,                  FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "DZ Stuck Hi"              ) \
   FT_ENTRY( FLT__POSITION_LIMIT,               FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Position Limit"           ) \
   FT_ENTRY( FLT__INVALID_MANUAL_RUN,           FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Inv. Manual Run"          ) \
   FT_ENTRY( FLT__INVALID_ACCEL_CURVE,          FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Inv. Accel Curve"         ) \
   FT_ENTRY( FLT__INVALID_DECEL_CURVE,          FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Inv. Decel Curve"         ) \
   FT_ENTRY( FLT__INVALID_ADDED_CURVE,          FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Inv. Added Curve"         ) \
   FT_ENTRY( FLT__INVALID_RSL_CURVE,            FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Inv. RSL Curve"           ) \
   FT_ENTRY( FLT__INVALID_CURVE_P1,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Inv. Profile 1"           ) \
   FT_ENTRY( FLT__INVALID_CURVE_P2,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Inv. Profile 2"           ) \
   FT_ENTRY( FLT__INVALID_CURVE_P3,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Inv. Profile 3"           ) \
   FT_ENTRY( FLT__INVALID_CURVE_P4,             FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Inv. Profile 4"           ) \
   FT_ENTRY( FLT__SFM_STUCK,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "SFM Stuck"                ) \
   FT_ENTRY( FLT__OVERLOADED,                   FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Car Overloaded"           ) \
   FT_ENTRY( FLT__PREFLIGHT_MR,                 FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "MR Preflight"             ) \
   FT_ENTRY( FLT__PREFLIGHT_CT,                 FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "CT Preflight"             ) \
   FT_ENTRY( FLT__PREFLIGHT_COP,                FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "COP Preflight"            ) \
   FT_ENTRY( FLT__PARAM_SYNC_MRB,               FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "MRB Param Sync"           ) \
   FT_ENTRY( FLT__PARAM_SYNC_CTA,               FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "CTA Param Sync"           ) \
   FT_ENTRY( FLT__PARAM_SYNC_CTB,               FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "CTB Param Sync"           ) \
   FT_ENTRY( FLT__SCURVE_UPDATING,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "S-Curve Update"           ) \
   FT_ENTRY( FLT__REGEN_FAULT,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Regen Fault"              ) \
   FT_ENTRY( FLT__OVERSPEED_CONST,              FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Constr. OVSP"             ) \
   FT_ENTRY( FLT__EBPS_STUCK_CLOSED,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EBPS Stuck Closed"        ) \
   FT_ENTRY( FLT__EBPS_STUCK_OPEN,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EBPS Stuck Open"          ) \
   FT_ENTRY( FLT__INVALID_DIP_SW_B2,            FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Inv. DIP B2"              ) \
   FT_ENTRY( FLT__INVALID_DIP_SW_B3,            FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Inv. DIP B3"              ) \
   FT_ENTRY( FLT__INVALID_DIP_SW_B4,            FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Inv. DIP B4"              ) \
   FT_ENTRY( FLT__INVALID_DIP_SW_B8,            FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Inv. DIP B8"              ) \
   FT_ENTRY( FLT__INVALID_DIP_SW_A6,            FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Inv. DIP A6"              ) \
   FT_ENTRY( FLT__INSP_IC_KEY_REQD,             FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "CT Insp Reqs IC Insp"     ) \
   FT_ENTRY( FLT__SPEED_REG_HI,                 FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "Speed Reg Hi"             ) \
   FT_ENTRY( FLT__B_CONT_HI_HW,                 FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "B Cont. Hi HW"            ) \
   FT_ENTRY( FLT__SPEED_REG_LO,                 FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "Speed Reg Lo"             ) \
   FT_ENTRY( FLT__B_CONT_LO_HW,                 FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "B Cont. Lo HW"            ) \
   FT_ENTRY( FLT__DC_HW_ENABLE,                 FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "DC HW En."                ) \
   FT_ENTRY( FLT__EXP_COM_1,                    FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP 1-8 Comm"             ) \
   FT_ENTRY( FLT__EXP_COM_2,                    FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP 9-16 Comm"            ) \
   FT_ENTRY( FLT__EXP_COM_3,                    FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP 17-24 Comm"           ) \
   FT_ENTRY( FLT__EXP_COM_4,                    FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP 25-32 Comm"           ) \
   FT_ENTRY( FLT__EXP_COM_5,                    FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP 33-40 Comm"           ) \
   FT_ENTRY( FLT__EXP_COM_6,                    FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP 41-48 Comm"           ) \
   FT_ENTRY( FLT__EXP_COM_7,                    FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP 49-56 Comm"           ) \
   FT_ENTRY( FLT__EXP_COM_8,                    FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP 57-64 Comm"           ) \
   FT_ENTRY( FLT__EXP_COM_9,                    FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP 65-72 Comm"           ) \
   FT_ENTRY( FLT__EXP_COM_10,                   FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP 73-80 Comm"           ) \
   FT_ENTRY( FLT__EXP_COM_11,                   FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP 81-88  Comm"          ) \
   FT_ENTRY( FLT__EXP_COM_12,                   FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP 89-96 Comm"           ) \
   FT_ENTRY( FLT__EXP_COM_13,                   FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP 97-104 Comm"          ) \
   FT_ENTRY( FLT__EXP_COM_14,                   FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP 105-112 Comm"         ) \
   FT_ENTRY( FLT__EXP_COM_15,                   FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP 113-120 Comm"         ) \
   FT_ENTRY( FLT__EXP_DIP_1,                    FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP DIP 1"                ) \
   FT_ENTRY( FLT__EXP_DIP_2,                    FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP DIP 2"                ) \
   FT_ENTRY( FLT__EXP_DIP_3,                    FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP DIP 3"                ) \
   FT_ENTRY( FLT__EXP_DIP_4,                    FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP DIP 4"                ) \
   FT_ENTRY( FLT__EXP_DIP_5,                    FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP DIP 5"                ) \
   FT_ENTRY( FLT__EXP_DIP_6,                    FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP DIP 6"                ) \
   FT_ENTRY( FLT__EXP_DIP_7,                    FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP DIP 7"                ) \
   FT_ENTRY( FLT__EXP_DIP_8,                    FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP DIP 8"                ) \
   FT_ENTRY( FLT__EXP_DIP_9,                    FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP DIP 9"                ) \
   FT_ENTRY( FLT__EXP_DIP_10,                   FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP DIP 10"               ) \
   FT_ENTRY( FLT__EXP_DIP_11,                   FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP DIP 11"               ) \
   FT_ENTRY( FLT__EXP_DIP_12,                   FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP DIP 12"               ) \
   FT_ENTRY( FLT__EXP_DIP_13,                   FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP DIP 13"               ) \
   FT_ENTRY( FLT__EXP_DIP_14,                   FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP DIP 14"               ) \
   FT_ENTRY( FLT__EXP_DIP_15,                   FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EXP DIP 15"               ) \
   FT_ENTRY( FLT__INVALID_HALLMASK,             FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Inv. Hall Mask"           ) \
   FT_ENTRY( FLT__OOS_LIMIT,                    FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "OOS Fault"                ) \
   FT_ENTRY( FLT__DUPLICATE_GROUP_ID,           FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Inv. Group ID"            ) \
   FT_ENTRY( FLT__RESCUE_START,                 FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Rescue Start"             ) \
   FT_ENTRY( FLT__RESCUE_IN_DZ,                 FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Rescue In DZ"             ) \
   FT_ENTRY( FLT__RESCUE_INVALID,               FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Rescue Invalid"           ) \
   FT_ENTRY( FLT__UNUSED305,                    FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "MR Safety"                ) \
   FT_ENTRY( FLT__CEDES1_OFFLINE,               FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES1 Offline"           ) \
   FT_ENTRY( FLT__CEDES1_READ_FAIL,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES1 Read"              ) \
   FT_ENTRY( FLT__CEDES1_ALIGN_CLOSE,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES1 Close"             ) \
   FT_ENTRY( FLT__CEDES1_ALIGN_FAR,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES1 Far"               ) \
   FT_ENTRY( FLT__CEDES1_ALIGN_LEFT,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES1 Left"              ) \
   FT_ENTRY( FLT__CEDES1_ALIGN_RIGHT,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES1 Right"             ) \
   FT_ENTRY( FLT__CEDES1_INTERNAL,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES1 Internal"          ) \
   FT_ENTRY( FLT__CEDES1_COMM,                  FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES1 Comm."             ) \
   FT_ENTRY( FLT__CEDES1_CROSS1_POS,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES1 X1 Pos"            ) \
   FT_ENTRY( FLT__CEDES1_CROSS1_VEL,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES1 X1 Vel"            ) \
   FT_ENTRY( FLT__CEDES1_CROSS1_BOTH,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES1 X1 Both"           ) \
   FT_ENTRY( FLT__CEDES1_CROSS2_POS,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES1 X2 Pos"            ) \
   FT_ENTRY( FLT__CEDES1_CROSS2_VEL,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES1 X2 Vel"            ) \
   FT_ENTRY( FLT__CEDES1_CROSS2_BOTH,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES1 X2 Both"           ) \
   FT_ENTRY( FLT__CEDES2_OFFLINE,               FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES2 Offline"           ) \
   FT_ENTRY( FLT__CEDES2_READ_FAIL,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES2 Read"              ) \
   FT_ENTRY( FLT__CEDES2_ALIGN_CLOSE,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES2 Close"             ) \
   FT_ENTRY( FLT__CEDES2_ALIGN_FAR,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES2 Far"               ) \
   FT_ENTRY( FLT__CEDES2_ALIGN_LEFT,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES2 Left"              ) \
   FT_ENTRY( FLT__CEDES2_ALIGN_RIGHT,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES2 Right"             ) \
   FT_ENTRY( FLT__CEDES2_INTERNAL,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES2 Internal"          ) \
   FT_ENTRY( FLT__CEDES2_COMM,                  FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES2 Comm."             ) \
   FT_ENTRY( FLT__CEDES2_CROSS1_POS,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES2 X1 Pos"            ) \
   FT_ENTRY( FLT__CEDES2_CROSS1_VEL,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES2 X1 Vel"            ) \
   FT_ENTRY( FLT__CEDES2_CROSS1_BOTH,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES2 X1 Both"           ) \
   FT_ENTRY( FLT__CEDES2_CROSS2_POS,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES2 X2 Pos"            ) \
   FT_ENTRY( FLT__CEDES2_CROSS2_VEL,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES2 X2 Vel"            ) \
   FT_ENTRY( FLT__CEDES2_CROSS2_BOTH,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES2 X2 Both"           ) \
   FT_ENTRY( FLT__EPOWER,                       FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "E-Power OOS"              ) \
   FT_ENTRY( FLT__INVALID_PARKING,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Inv. Parking"             ) \
   FT_ENTRY( FLT__INVALID_FIRE_MAIN,            FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Inv. Fire Main"           ) \
   FT_ENTRY( FLT__INVALID_FIRE_ALT,             FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Inv. Fire Alt"            ) \
   FT_ENTRY( FLT__CPLD_OFFLINE_MR,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "MR CPLD Offline"          ) \
   FT_ENTRY( FLT__CPLD_OFFLINE_CT,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "CT CPLD Offline"          ) \
   FT_ENTRY( FLT__CPLD_OFFLINE_COP,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "COP CPLD Offline"         ) \
   FT_ENTRY( FLT__DATAGRAM_EXPIRED,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "DG Expired"               ) \
   FT_ENTRY( FLT__DRIVE_OFFLINE,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Drive Offline"            ) \
   FT_ENTRY( FLT__DSD_NOT_RDY,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "DSD Not Rdy"              ) \
   FT_ENTRY( FLT__DSD_OVERSPEED,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "DSD OVSP"                 ) \
   FT_ENTRY( FLT__DSD_TACH_LOSS,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "DSD Tach Loss"            ) \
   FT_ENTRY( FLT__DSD_TACH_REV,                 FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "DSD Tach Rev"             ) \
   FT_ENTRY( FLT__DSD_OVERLOAD,                 FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "DSD Overload"             ) \
   FT_ENTRY( FLT__DSD_FIELD_CURR,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "DSD Field Curr"           ) \
   FT_ENTRY( FLT__DSD_CONTACT,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "DSD Contact."             ) \
   FT_ENTRY( FLT__DSD_CEMF,                     FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "DSD CEMF"                 ) \
   FT_ENTRY( FLT__DSD_ESTOP,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "DSD Estop"                ) \
   FT_ENTRY( FLT__DSD_LOOP,                     FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "DSD Loop"                 ) \
   FT_ENTRY( FLT__DSD_PCU,                      FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "DSD PCU"                  ) \
   FT_ENTRY( FLT__DSD_LINE_SYNC,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "DSD Line Sync"            ) \
   FT_ENTRY( FLT__DSD_LINE_LO,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "DSD Line Lo"              ) \
   FT_ENTRY( FLT__DSD_FIELD_LOSS,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "DSD Field Loss"           ) \
   FT_ENTRY( FLT__DSD_LINE_DROOP,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "DSD Line Droop"           ) \
   FT_ENTRY( FLT__DSD_COMM,                     FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "DSD Comm."                ) \
   FT_ENTRY( FLT__MAG_FLT_1,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG1"                     ) \
   FT_ENTRY( FLT__MAG_FLT_2,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG2"                     ) \
   FT_ENTRY( FLT__MAG_FLT_3,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG3"                     ) \
   FT_ENTRY( FLT__MAG_FLT_4,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG4"                     ) \
   FT_ENTRY( FLT__MAG_FLT_5,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG5"                     ) \
   FT_ENTRY( FLT__MAG_FLT_6,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG6"                     ) \
   FT_ENTRY( FLT__MAG_FLT_7,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG7"                     ) \
   FT_ENTRY( FLT__MAG_FLT_8,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG8"                     ) \
   FT_ENTRY( FLT__MAG_FLT_9,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG9"                     ) \
   FT_ENTRY( FLT__MAG_FLT_10,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG10"                    ) \
   FT_ENTRY( FLT__MAG_FLT_11,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG11"                    ) \
   FT_ENTRY( FLT__MAG_FLT_12,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG12"                    ) \
   FT_ENTRY( FLT__MAG_FLT_13,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG13"                    ) \
   FT_ENTRY( FLT__MAG_FLT_14,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG14"                    ) \
   FT_ENTRY( FLT__MAG_FLT_15,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG15"                    ) \
   FT_ENTRY( FLT__MAG_FLT_16,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG16"                    ) \
   FT_ENTRY( FLT__MAG_FLT_17,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG17"                    ) \
   FT_ENTRY( FLT__MAG_FLT_18,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG18"                    ) \
   FT_ENTRY( FLT__MAG_FLT_19,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG19"                    ) \
   FT_ENTRY( FLT__MAG_FLT_20,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG20"                    ) \
   FT_ENTRY( FLT__MAG_FLT_21,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG21"                    ) \
   FT_ENTRY( FLT__MAG_FLT_22,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG22"                    ) \
   FT_ENTRY( FLT__MAG_FLT_23,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG23"                    ) \
   FT_ENTRY( FLT__MAG_FLT_24,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG24"                    ) \
   FT_ENTRY( FLT__MAG_FLT_25,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG25"                    ) \
   FT_ENTRY( FLT__MAG_FLT_26,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG26"                    ) \
   FT_ENTRY( FLT__MAG_FLT_27,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG27"                    ) \
   FT_ENTRY( FLT__MAG_FLT_28,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG28"                    ) \
   FT_ENTRY( FLT__MAG_FLT_29,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG29"                    ) \
   FT_ENTRY( FLT__MAG_FLT_30,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG30"                    ) \
   FT_ENTRY( FLT__MAG_FLT_31,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG31"                    ) \
   FT_ENTRY( FLT__MAG_FLT_32,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG32"                    ) \
   FT_ENTRY( FLT__MAG_FLT_33,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG33"                    ) \
   FT_ENTRY( FLT__MAG_FLT_34,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG34"                    ) \
   FT_ENTRY( FLT__MAG_FLT_35,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG35"                    ) \
   FT_ENTRY( FLT__MAG_FLT_36,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG36"                    ) \
   FT_ENTRY( FLT__MAG_FLT_37,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG37"                    ) \
   FT_ENTRY( FLT__MAG_FLT_38,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG38"                    ) \
   FT_ENTRY( FLT__MAG_FLT_39,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG39"                    ) \
   FT_ENTRY( FLT__MAG_FLT_40,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG40"                    ) \
   FT_ENTRY( FLT__MAG_FLT_41,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG41"                    ) \
   FT_ENTRY( FLT__MAG_FLT_42,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG42"                    ) \
   FT_ENTRY( FLT__MAG_FLT_43,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG43"                    ) \
   FT_ENTRY( FLT__MAG_FLT_44,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG44"                    ) \
   FT_ENTRY( FLT__MAG_FLT_45,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG45"                    ) \
   FT_ENTRY( FLT__MAG_FLT_46,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG46"                    ) \
   FT_ENTRY( FLT__MAG_FLT_47,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG47"                    ) \
   FT_ENTRY( FLT__MAG_FLT_48,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG48"                    ) \
   FT_ENTRY( FLT__MAG_FLT_49,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG49"                    ) \
   FT_ENTRY( FLT__MAG_FLT_50,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG50"                    ) \
   FT_ENTRY( FLT__MAG_FLT_51,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG51"                    ) \
   FT_ENTRY( FLT__MAG_FLT_52,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG52"                    ) \
   FT_ENTRY( FLT__MAG_FLT_53,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG53"                    ) \
   FT_ENTRY( FLT__MAG_FLT_54,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG54"                    ) \
   FT_ENTRY( FLT__MAG_FLT_55,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG55"                    ) \
   FT_ENTRY( FLT__MAG_FLT_56,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG56"                    ) \
   FT_ENTRY( FLT__MAG_FLT_57,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG57"                    ) \
   FT_ENTRY( FLT__MAG_FLT_58,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG58"                    ) \
   FT_ENTRY( FLT__MAG_FLT_59,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG59"                    ) \
   FT_ENTRY( FLT__MAG_FLT_60,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG60"                    ) \
   FT_ENTRY( FLT__MAG_FLT_61,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG61"                    ) \
   FT_ENTRY( FLT__MAG_FLT_62,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG62"                    ) \
   FT_ENTRY( FLT__MAG_FLT_63,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG63"                    ) \
   FT_ENTRY( FLT__MAG_FLT_64,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG64"                    ) \
   FT_ENTRY( FLT__MAG_FLT_65,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG65"                    ) \
   FT_ENTRY( FLT__MAG_FLT_66,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG66"                    ) \
   FT_ENTRY( FLT__MAG_FLT_67,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG67"                    ) \
   FT_ENTRY( FLT__MAG_FLT_68,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG68"                    ) \
   FT_ENTRY( FLT__MAG_FLT_69,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG69"                    ) \
   FT_ENTRY( FLT__MAG_FLT_70,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG70"                    ) \
   FT_ENTRY( FLT__MAG_FLT_71,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG71"                    ) \
   FT_ENTRY( FLT__MAG_FLT_72,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "MAG72"                    ) \
   FT_ENTRY( FLT__KEB_FLT_1,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB1-OVER VOLTAGE"        ) \
   FT_ENTRY( FLT__KEB_FLT_2,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB2-UNDER VOLTAGE"       ) \
   FT_ENTRY( FLT__KEB_FLT_3,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB3-INPUT PH. FAIL"      ) \
   FT_ENTRY( FLT__KEB_FLT_4,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB4-OVER CURRENT"        ) \
   FT_ENTRY( FLT__KEB_FLT_5,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB5-OUTPUT PH. FAIL"     ) \
   FT_ENTRY( FLT__KEB_FLT_6,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB6-OVHT INT"            ) \
   FT_ENTRY( FLT__KEB_FLT_7,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB7-NO OVHT INT"         ) \
   FT_ENTRY( FLT__KEB_FLT_8,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB8-OVHT PWR MOD"        ) \
   FT_ENTRY( FLT__KEB_FLT_9,                    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB9-MTR OVHT"            ) \
   FT_ENTRY( FLT__KEB_FLT_10,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB10"                    ) \
   FT_ENTRY( FLT__KEB_FLT_11,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB11-NO MTR OVHT"        ) \
   FT_ENTRY( FLT__KEB_FLT_12,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB12-POWER UNIT"         ) \
   FT_ENTRY( FLT__KEB_FLT_13,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB13-POW NOT RDY"        ) \
   FT_ENTRY( FLT__KEB_FLT_14,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB14"                    ) \
   FT_ENTRY( FLT__KEB_FLT_15,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB15-CHARGE RELAY"       ) \
   FT_ENTRY( FLT__KEB_FLT_16,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB16-OVERLOAD"           ) \
   FT_ENTRY( FLT__KEB_FLT_17,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB17-NO OVERLOAD"        ) \
   FT_ENTRY( FLT__KEB_FLT_18,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB18-HSP5 SER COM"       ) \
   FT_ENTRY( FLT__KEB_FLT_19,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB19-OVLD LOW SPD"       ) \
   FT_ENTRY( FLT__KEB_FLT_20,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB20-NO OVLD LOW SP"     ) \
   FT_ENTRY( FLT__KEB_FLT_21,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB21"                    ) \
   FT_ENTRY( FLT__KEB_FLT_22,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB22"                    ) \
   FT_ENTRY( FLT__KEB_FLT_23,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB23-BUS SYNC"           ) \
   FT_ENTRY( FLT__KEB_FLT_24,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB24-MAX ACCEL"          ) \
   FT_ENTRY( FLT__KEB_FLT_25,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB25-SPD.CTRL LIM"       ) \
   FT_ENTRY( FLT__KEB_FLT_26,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB26"                    ) \
   FT_ENTRY( FLT__KEB_FLT_27,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB27"                    ) \
   FT_ENTRY( FLT__KEB_FLT_28,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB28"                    ) \
   FT_ENTRY( FLT__KEB_FLT_29,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB29"                    ) \
   FT_ENTRY( FLT__KEB_FLT_30,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB30-MTR PROTECT"        ) \
   FT_ENTRY( FLT__KEB_FLT_31,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB31-EXTERNAL FLT"       ) \
   FT_ENTRY( FLT__KEB_FLT_32,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB32-ENCODER 1"          ) \
   FT_ENTRY( FLT__KEB_FLT_33,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB33"                    ) \
   FT_ENTRY( FLT__KEB_FLT_34,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB34-ENCODER 2"          ) \
   FT_ENTRY( FLT__KEB_FLT_35,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB35"                    ) \
   FT_ENTRY( FLT__KEB_FLT_36,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB36-NO OVHT POWMOD"     ) \
   FT_ENTRY( FLT__KEB_FLT_37,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB37"                    ) \
   FT_ENTRY( FLT__KEB_FLT_38,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB38"                    ) \
   FT_ENTRY( FLT__KEB_FLT_39,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB39-ERROR SET"          ) \
   FT_ENTRY( FLT__KEB_FLT_40,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB40"                    ) \
   FT_ENTRY( FLT__KEB_FLT_41,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB41"                    ) \
   FT_ENTRY( FLT__KEB_FLT_42,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB42"                    ) \
   FT_ENTRY( FLT__KEB_FLT_43,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB43"                    ) \
   FT_ENTRY( FLT__KEB_FLT_44,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB44-SF LIM F"           ) \
   FT_ENTRY( FLT__KEB_FLT_45,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB45-SF LIM R"           ) \
   FT_ENTRY( FLT__KEB_FLT_46,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB46-PROT ROTATE F"      ) \
   FT_ENTRY( FLT__KEB_FLT_47,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB47-PROT ROTATE R"      ) \
   FT_ENTRY( FLT__KEB_FLT_48,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB48"                    ) \
   FT_ENTRY( FLT__KEB_FLT_49,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB49-PWRCODE INV"        ) \
   FT_ENTRY( FLT__KEB_FLT_50,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB50-PWRUNIT CHNG"       ) \
   FT_ENTRY( FLT__KEB_FLT_51,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB51-DRIVER RELAY"       ) \
   FT_ENTRY( FLT__KEB_FLT_52,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB52-ENCODER CARD"       ) \
   FT_ENTRY( FLT__KEB_FLT_53,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB53-IN ERR DETECT"      ) \
   FT_ENTRY( FLT__KEB_FLT_54,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB54-CNTR OVERRUN1"      ) \
   FT_ENTRY( FLT__KEB_FLT_55,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB55-CNTR OVERRUN2"      ) \
   FT_ENTRY( FLT__KEB_FLT_56,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB56-LOW MTR CUR"        ) \
   FT_ENTRY( FLT__KEB_FLT_57,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB57-INIT MFC"           ) \
   FT_ENTRY( FLT__KEB_FLT_58,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB58-OVERSPEED"          ) \
   FT_ENTRY( FLT__KEB_FLT_59,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB59-CARD CHANGE"        ) \
   FT_ENTRY( FLT__KEB_FLT_60,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB60-CALC MTRDATA"       ) \
   FT_ENTRY( FLT__KEB_FLT_61,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB61"                    ) \
   FT_ENTRY( FLT__KEB_FLT_62,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB62"                    ) \
   FT_ENTRY( FLT__KEB_FLT_63,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB63"                    ) \
   FT_ENTRY( FLT__KEB_FLT_64,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB64-UP ACCEL"           ) \
   FT_ENTRY( FLT__KEB_FLT_65,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB65-UP DECEL"           ) \
   FT_ENTRY( FLT__KEB_FLT_66,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB66-UP CSNT SPD"        ) \
   FT_ENTRY( FLT__KEB_FLT_67,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB67-DN ACCEL"           ) \
   FT_ENTRY( FLT__KEB_FLT_68,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB68-DN DECEL"           ) \
   FT_ENTRY( FLT__KEB_FLT_69,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB69-DN CST SPD"         ) \
   FT_ENTRY( FLT__KEB_FLT_70,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB70-NO DIRECTION"       ) \
   FT_ENTRY( FLT__KEB_FLT_71,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB71-STALL"              ) \
   FT_ENTRY( FLT__KEB_FLT_72,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB720-LA STOP"           ) \
   FT_ENTRY( FLT__KEB_FLT_73,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB73-LD STOP"            ) \
   FT_ENTRY( FLT__KEB_FLT_74,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB74-SPD SRCH"           ) \
   FT_ENTRY( FLT__KEB_FLT_75,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB75-DC BRAKE"           ) \
   FT_ENTRY( FLT__KEB_FLT_76,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB76-BASE BLCK"          ) \
   FT_ENTRY( FLT__KEB_FLT_77,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB77-LOW SPD"            ) \
   FT_ENTRY( FLT__KEB_FLT_78,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB78-PWR OFF"            ) \
   FT_ENTRY( FLT__KEB_FLT_79,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB79-QUICK STOP"         ) \
   FT_ENTRY( FLT__KEB_FLT_80,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB80-HW CUR LIMIT"       ) \
   FT_ENTRY( FLT__KEB_FLT_81,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB81-ACTIVE REF"         ) \
   FT_ENTRY( FLT__KEB_FLT_82,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB82-CALC MTRDATA"       ) \
   FT_ENTRY( FLT__KEB_FLT_83,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB83-POSITIONING"        ) \
   FT_ENTRY( FLT__KEB_FLT_84,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB84-LOW SPD/POW"        ) \
   FT_ENTRY( FLT__KEB_FLT_85,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB85-CLOSING BRK"        ) \
   FT_ENTRY( FLT__KEB_FLT_86,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB86-OPENING BRK"        ) \
   FT_ENTRY( FLT__KEB_FLT_87,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB87-STOP OVHEAT"        ) \
   FT_ENTRY( FLT__KEB_FLT_88,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB88-NO OVHT POW "       ) \
   FT_ENTRY( FLT__KEB_FLT_89,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB89-STOP OVHT POW"      ) \
   FT_ENTRY( FLT__KEB_FLT_90,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB90-STOP EXT FLT"       ) \
   FT_ENTRY( FLT__KEB_FLT_91,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB91-NO DRV OVH"         ) \
   FT_ENTRY( FLT__KEB_FLT_92,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB92-NO STP OVH IN"      ) \
   FT_ENTRY( FLT__KEB_FLT_93,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB93-STOP BUS"           ) \
   FT_ENTRY( FLT__KEB_FLT_94,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB94-STOP PROT F"        ) \
   FT_ENTRY( FLT__KEB_FLT_95,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB95-STOP PROT R"        ) \
   FT_ENTRY( FLT__KEB_FLT_96,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB96-STOP DRVE OVH"      ) \
   FT_ENTRY( FLT__KEB_FLT_97,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB97-STOP MTR PRCT"      ) \
   FT_ENTRY( FLT__KEB_FLT_98,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB98-NO STEP OL"         ) \
   FT_ENTRY( FLT__KEB_FLT_99,                   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB99-STOP OVL"           ) \
   FT_ENTRY( FLT__KEB_FLT_100,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB100-STOP OVL2"         ) \
   FT_ENTRY( FLT__KEB_FLT_101,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB101- NO STOP OVL2"     ) \
   FT_ENTRY( FLT__KEB_FLT_102,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB102-STOP SET"          ) \
   FT_ENTRY( FLT__KEB_FLT_103,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB103-STOP BUS SYNC"     ) \
   FT_ENTRY( FLT__KEB_FLT_104,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB104-SF LIM FWD"        ) \
   FT_ENTRY( FLT__KEB_FLT_105,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB105-SF LIM RVSE"       ) \
   FT_ENTRY( FLT__KEB_FLT_106,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB106-STOP MAX ACC"      ) \
   FT_ENTRY( FLT__KEB_FLT_107,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB107-STOP SPD LIM"      ) \
   FT_ENTRY( FLT__KEB_FLT_108,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB108"                   ) \
   FT_ENTRY( FLT__KEB_FLT_109,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB109"                   ) \
   FT_ENTRY( FLT__KEB_FLT_110,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB110"                   ) \
   FT_ENTRY( FLT__KEB_FLT_111,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB111"                   ) \
   FT_ENTRY( FLT__KEB_FLT_112,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB112"                   ) \
   FT_ENTRY( FLT__KEB_FLT_113,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB113"                   ) \
   FT_ENTRY( FLT__KEB_FLT_114,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB114"                   ) \
   FT_ENTRY( FLT__KEB_FLT_115,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB115"                   ) \
   FT_ENTRY( FLT__KEB_FLT_116,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB116"                   ) \
   FT_ENTRY( FLT__KEB_FLT_117,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB117"                   ) \
   FT_ENTRY( FLT__KEB_FLT_118,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB118"                   ) \
   FT_ENTRY( FLT__KEB_FLT_119,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB119"                   ) \
   FT_ENTRY( FLT__KEB_FLT_120,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB120"                   ) \
   FT_ENTRY( FLT__KEB_FLT_121,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB121-READY POS"         ) \
   FT_ENTRY( FLT__KEB_FLT_122,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB122-POS ACTIVE"        ) \
   FT_ENTRY( FLT__KEB_FLT_123,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB123-POS. NOT ACC"      ) \
   FT_ENTRY( FLT__KEB_FLT_124,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB124-PROT R.FWD"        ) \
   FT_ENTRY( FLT__KEB_FLT_125,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB125-PROT R.REV"        ) \
   FT_ENTRY( FLT__KEB_FLT_126,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB126-POS NOT ACC"       ) \
   FT_ENTRY( FLT__KEB_FLT_127,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB127-CALC CMPLTE"       ) \
   FT_ENTRY( FLT__KEB_FLT_128,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB128-REF FOUND"         ) \
   FT_ENTRY( FLT__KEB_FLT_129,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB129"                   ) \
   FT_ENTRY( FLT__KEB_FLT_130,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB130"                   ) \
   FT_ENTRY( FLT__KEB_FLT_131,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB131"                   ) \
   FT_ENTRY( FLT__KEB_FLT_132,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB132"                   ) \
   FT_ENTRY( FLT__KEB_FLT_133,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB133"                   ) \
   FT_ENTRY( FLT__KEB_FLT_134,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB134"                   ) \
   FT_ENTRY( FLT__KEB_FLT_135,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB135"                   ) \
   FT_ENTRY( FLT__KEB_FLT_136,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB136"                   ) \
   FT_ENTRY( FLT__KEB_FLT_137,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB137"                   ) \
   FT_ENTRY( FLT__KEB_FLT_138,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB138"                   ) \
   FT_ENTRY( FLT__KEB_FLT_139,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB139"                   ) \
   FT_ENTRY( FLT__KEB_FLT_140,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB140"                   ) \
   FT_ENTRY( FLT__KEB_FLT_141,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB141"                   ) \
   FT_ENTRY( FLT__KEB_FLT_142,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB142"                   ) \
   FT_ENTRY( FLT__KEB_FLT_143,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB143"                   ) \
   FT_ENTRY( FLT__KEB_FLT_144,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB144"                   ) \
   FT_ENTRY( FLT__KEB_FLT_145,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB145"                   ) \
   FT_ENTRY( FLT__KEB_FLT_146,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB146"                   ) \
   FT_ENTRY( FLT__KEB_FLT_147,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB147"                   ) \
   FT_ENTRY( FLT__KEB_FLT_148,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB148"                   ) \
   FT_ENTRY( FLT__KEB_FLT_149,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB149"                   ) \
   FT_ENTRY( FLT__KEB_FLT_150,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB150-M.CONT FAIL"       ) \
   FT_ENTRY( FLT__KEB_FLT_151,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB151-BRK SW FAIL"       ) \
   FT_ENTRY( FLT__KEB_FLT_152,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB152-SPD FOLLOW"        ) \
   FT_ENTRY( FLT__KEB_FLT_153,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB153-SPD SELECT"        ) \
   FT_ENTRY( FLT__KEB_FLT_154,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB154-ETS IN. FAIL"      ) \
   FT_ENTRY( FLT__KEB_FLT_155,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB155-ETS OVRSPD"        ) \
   FT_ENTRY( FLT__KEB_FLT_156,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB156-NTS IN. FAIL"      ) \
   FT_ENTRY( FLT__KEB_FLT_157,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB157-SIGNAL FAIL"       ) \
   FT_ENTRY( FLT__KEB_FLT_158,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB158-UNIN. MVMT"        ) \
   FT_ENTRY( FLT__KEB_FLT_159,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB159-SC FAULT RST"      ) \
   FT_ENTRY( FLT__KEB_FLT_160,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB160-ESD IN. FAIL"      ) \
   FT_ENTRY( FLT__KEB_FLT_161,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB161-DIR SEL FAIL"      ) \
   FT_ENTRY( FLT__KEB_FLT_162,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB162-DRV EN SW OFF"     ) \
   FT_ENTRY( FLT__KEB_FLT_163,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB163-FIELD BUS WD"      ) \
   FT_ENTRY( FLT__KEB_FLT_164,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB164-COM POS"           ) \
   FT_ENTRY( FLT__KEB_FLT_165,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB165-EXCESS ACL"        ) \
   FT_ENTRY( FLT__KEB_FLT_166,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB166-SER.CMD.SPD"       ) \
   FT_ENTRY( FLT__KEB_FLT_167,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB167"                   ) \
   FT_ENTRY( FLT__KEB_FLT_168,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB168"                   ) \
   FT_ENTRY( FLT__KEB_FLT_169,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB169"                   ) \
   FT_ENTRY( FLT__KEB_FLT_170,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB170-UPS MODE"          ) \
   FT_ENTRY( FLT__KEB_FLT_171,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB171-REDUCE TRQ"        ) \
   FT_ENTRY( FLT__KEB_FLT_172,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB172-EPOW PROF"         ) \
   FT_ENTRY( FLT__KEB_FLT_173,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB173-EPOW GEN SPD"      ) \
   FT_ENTRY( FLT__KEB_FLT_174,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB174-EQ SPEED"          ) \
   FT_ENTRY( FLT__KEB_FLT_175,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB175-EMG SLOWDOWN"      ) \
   FT_ENTRY( FLT__KEB_FLT_176,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB176"                   ) \
   FT_ENTRY( FLT__KEB_FLT_177,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB177"                   ) \
   FT_ENTRY( FLT__KEB_FLT_178,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB178"                   ) \
   FT_ENTRY( FLT__KEB_FLT_179,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB179"                   ) \
   FT_ENTRY( FLT__KEB_FLT_180,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB180"                   ) \
   FT_ENTRY( FLT__KEB_FLT_181,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB181"                   ) \
   FT_ENTRY( FLT__KEB_FLT_182,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB182"                   ) \
   FT_ENTRY( FLT__KEB_FLT_183,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB183"                   ) \
   FT_ENTRY( FLT__KEB_FLT_184,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB184"                   ) \
   FT_ENTRY( FLT__KEB_FLT_185,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB185"                   ) \
   FT_ENTRY( FLT__KEB_FLT_186,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB186"                   ) \
   FT_ENTRY( FLT__KEB_FLT_187,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB187"                   ) \
   FT_ENTRY( FLT__KEB_FLT_188,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB188"                   ) \
   FT_ENTRY( FLT__KEB_FLT_189,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB189"                   ) \
   FT_ENTRY( FLT__KEB_FLT_190,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB190"                   ) \
   FT_ENTRY( FLT__KEB_FLT_191,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB191"                   ) \
   FT_ENTRY( FLT__KEB_FLT_192,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB192"                   ) \
   FT_ENTRY( FLT__KEB_FLT_193,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB193"                   ) \
   FT_ENTRY( FLT__KEB_FLT_194,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB194"                   ) \
   FT_ENTRY( FLT__KEB_FLT_195,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB195"                   ) \
   FT_ENTRY( FLT__KEB_FLT_196,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB196"                   ) \
   FT_ENTRY( FLT__KEB_FLT_197,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB197"                   ) \
   FT_ENTRY( FLT__KEB_FLT_198,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB198"                   ) \
   FT_ENTRY( FLT__KEB_FLT_199,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB199"                   ) \
   FT_ENTRY( FLT__KEB_FLT_200,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB200-NO COM E.CARD"     ) \
   FT_ENTRY( FLT__KEB_FLT_201,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB201-E.CARD COM OK"     ) \
   FT_ENTRY( FLT__KEB_FLT_202,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB202-ENCODER UNDEF"     ) \
   FT_ENTRY( FLT__KEB_FLT_203,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB203"                   ) \
   FT_ENTRY( FLT__KEB_FLT_204,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB204"                   ) \
   FT_ENTRY( FLT__KEB_FLT_205,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB205"                   ) \
   FT_ENTRY( FLT__KEB_FLT_206,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB206-NO COM TO ENC"     ) \
   FT_ENTRY( FLT__KEB_FLT_207,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB207-INC COUNT DEV"     ) \
   FT_ENTRY( FLT__KEB_FLT_208,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB208-EN.PPR LE01"       ) \
   FT_ENTRY( FLT__KEB_FLT_209,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB209-ID WRONG"          ) \
   FT_ENTRY( FLT__KEB_FLT_210,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB210"                   ) \
   FT_ENTRY( FLT__KEB_FLT_211,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB211"                   ) \
   FT_ENTRY( FLT__KEB_FLT_212,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB212"                   ) \
   FT_ENTRY( FLT__KEB_FLT_213,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB213-ENC. OVHT"         ) \
   FT_ENTRY( FLT__KEB_FLT_214,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB214-ENC. OVRSPD"       ) \
   FT_ENTRY( FLT__KEB_FLT_215,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB215-ENC. LOW VOLT"     ) \
   FT_ENTRY( FLT__KEB_FLT_216,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB216-INTERNAL ENC."     ) \
   FT_ENTRY( FLT__KEB_FLT_217,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB217-ENC.FRMATING"      ) \
   FT_ENTRY( FLT__KEB_FLT_218,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB218"                   ) \
   FT_ENTRY( FLT__KEB_FLT_219,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB219"                   ) \
   FT_ENTRY( FLT__KEB_FLT_220,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB220"                   ) \
   FT_ENTRY( FLT__KEB_FLT_221,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB221-NEW ENC."          ) \
   FT_ENTRY( FLT__KEB_FLT_222,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB222-UNDEF ENC."        ) \
   FT_ENTRY( FLT__KEB_FLT_223,                  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "KEB223-ENC.IN BSY"        ) \
   FT_ENTRY( FLT__INVALID_LAND_OFF,             FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_003, "Inv. Land Off"            ) \
   FT_ENTRY( FLT__PAYMENT_PASSCODE,             FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Payment Passcode"         ) \
   FT_ENTRY( FLT__BATTERY_FAULT,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_007, "Battery Check Fail"       ) \
   FT_ENTRY( FLT__INVALID_ETSL_1,               FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Inv. ETSL 1"              ) \
   FT_ENTRY( FLT__INVALID_ETSL_2,               FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Inv. ETSL 2"              ) \
   FT_ENTRY( FLT__INVALID_ETSL_3,               FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Inv. ETSL 3"              ) \
   FT_ENTRY( FLT__INVALID_ETSL_4,               FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Inv. ETSL 4"              ) \
   FT_ENTRY( FLT__UNUSED661,                    FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "UNUSED661"                ) \
   FT_ENTRY( FLT__UNUSED662,                    FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "UNUSED662"                ) \
   FT_ENTRY( FLT__CEDES3_OFFLINE,               FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES3 Offline"           ) \
   FT_ENTRY( FLT__CEDES3_READ_FAIL,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES3 Read"              ) \
   FT_ENTRY( FLT__CEDES3_ALIGN_CLOSE,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES3 Close"             ) \
   FT_ENTRY( FLT__CEDES3_ALIGN_FAR,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES3 Far"               ) \
   FT_ENTRY( FLT__CEDES3_ALIGN_LEFT,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES3 Left"              ) \
   FT_ENTRY( FLT__CEDES3_ALIGN_RIGHT,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES3 Right"             ) \
   FT_ENTRY( FLT__CEDES3_INTERNAL,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES3 Internal"          ) \
   FT_ENTRY( FLT__CEDES3_COMM,                  FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES3 Comm."             ) \
   FT_ENTRY( FLT__CEDES3_CROSS1_POS,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES3 X1 Pos"            ) \
   FT_ENTRY( FLT__CEDES3_CROSS1_VEL,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES3 X1 Vel"            ) \
   FT_ENTRY( FLT__CEDES3_CROSS1_BOTH,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES3 X1 Both"           ) \
   FT_ENTRY( FLT__CEDES3_CROSS2_POS,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES3 X2 Pos"            ) \
   FT_ENTRY( FLT__CEDES3_CROSS2_VEL,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES3 X2 Vel"            ) \
   FT_ENTRY( FLT__CEDES3_CROSS2_BOTH,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_005, "CEDES3 X2 Both"           ) \
   FT_ENTRY( FLT__RSBUFFER_P1,                  FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "RS Buffer P1"             ) \
   FT_ENTRY( FLT__RSBUFFER_P2,                  FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "RS Buffer P2"             ) \
   FT_ENTRY( FLT__RSBUFFER_P3,                  FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "RS Buffer P3"             ) \
   FT_ENTRY( FLT__RSBUFFER_P4,                  FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "RS Buffer P4"             ) \
   FT_ENTRY( FLT__OVERSPEED_UETS_1,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "UETS OVSP 1"              ) \
   FT_ENTRY( FLT__OVERSPEED_UETS_2,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "UETS OVSP 2"              ) \
   FT_ENTRY( FLT__OVERSPEED_UETS_3,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "UETS OVSP 3"              ) \
   FT_ENTRY( FLT__OVERSPEED_UETS_4,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "UETS OVSP 4"              ) \
   FT_ENTRY( FLT__OVERSPEED_UETS_5,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "UETS OVSP 5"              ) \
   FT_ENTRY( FLT__OVERSPEED_UETS_6,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "UETS OVSP 6"              ) \
   FT_ENTRY( FLT__OVERSPEED_UETS_7,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "UETS OVSP 7"              ) \
   FT_ENTRY( FLT__OVERSPEED_UETS_8,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "UETS OVSP 8"              ) \
   FT_ENTRY( FLT__OVERSPEED_DETS_1,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "DETS OVSP 1"              ) \
   FT_ENTRY( FLT__OVERSPEED_DETS_2,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "DETS OVSP 2"              ) \
   FT_ENTRY( FLT__OVERSPEED_DETS_3,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "DETS OVSP 3"              ) \
   FT_ENTRY( FLT__OVERSPEED_DETS_4,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "DETS OVSP 4"              ) \
   FT_ENTRY( FLT__OVERSPEED_DETS_5,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "DETS OVSP 5"              ) \
   FT_ENTRY( FLT__OVERSPEED_DETS_6,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "DETS OVSP 6"              ) \
   FT_ENTRY( FLT__OVERSPEED_DETS_7,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "DETS OVSP 7"              ) \
   FT_ENTRY( FLT__OVERSPEED_DETS_8,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "DETS OVSP 8"              ) \
   FT_ENTRY( FLT__OVERSPEED_UETSL_1,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "UETSL OVSP 1"             ) \
   FT_ENTRY( FLT__OVERSPEED_UETSL_2,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "UETSL OVSP 2"             ) \
   FT_ENTRY( FLT__OVERSPEED_UETSL_3,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "UETSL OVSP 3"             ) \
   FT_ENTRY( FLT__OVERSPEED_UETSL_4,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "UETSL OVSP 4"             ) \
   FT_ENTRY( FLT__OVERSPEED_UETSL_5,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "UETSL OVSP 5"             ) \
   FT_ENTRY( FLT__OVERSPEED_UETSL_6,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "UETSL OVSP 6"             ) \
   FT_ENTRY( FLT__OVERSPEED_UETSL_7,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "UETSL OVSP 7"             ) \
   FT_ENTRY( FLT__OVERSPEED_UETSL_8,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "UETSL OVSP 8"             ) \
   FT_ENTRY( FLT__OVERSPEED_DETSL_1,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "DETSL OVSP 1"             ) \
   FT_ENTRY( FLT__OVERSPEED_DETSL_2,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "DETSL OVSP 2"             ) \
   FT_ENTRY( FLT__OVERSPEED_DETSL_3,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "DETSL OVSP 3"             ) \
   FT_ENTRY( FLT__OVERSPEED_DETSL_4,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "DETSL OVSP 4"             ) \
   FT_ENTRY( FLT__OVERSPEED_DETSL_5,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "DETSL OVSP 5"             ) \
   FT_ENTRY( FLT__OVERSPEED_DETSL_6,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "DETSL OVSP 6"             ) \
   FT_ENTRY( FLT__OVERSPEED_DETSL_7,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "DETSL OVSP 7"             ) \
   FT_ENTRY( FLT__OVERSPEED_DETSL_8,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "DETSL OVSP 8"             ) \
   FT_ENTRY( FLT__FAULT_INPUT,                  FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "FAULT INPUT"              ) \
   FT_ENTRY( FLT__DRIVE_FLT_UNKNOWN,            FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Drive Fault (UNK)"        ) \
   FT_ENTRY( FLT__FRAM_DATA_CORRUPTION,         FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_007, "FRAM Data Corrupt"        ) \
   FT_ENTRY( FLT__MAX_RUNS_PER_MINUTE,          FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Max Runs Per Minute"      ) \
   FT_ENTRY( FLT__NEED_TO_CYCLE_PWR_CT,         FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_003, "Need To Cycle Pwr CT"     ) \
   FT_ENTRY( FLT__NEED_TO_CYCLE_PWR_COP,        FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_003, "Need To Cycle Pwr COP"    ) \
   FT_ENTRY( FLT__TCL_F_OPEN,                   FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_004, "Front TCL Open"           ) \
   FT_ENTRY( FLT__MCL_F_OPEN,                   FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_004, "Front MCL Open"           ) \
   FT_ENTRY( FLT__BCL_F_OPEN,                   FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_004, "Front BCL Open"           ) \
   FT_ENTRY( FLT__TCL_R_OPEN,                   FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_004, "Rear TCL Open"            ) \
   FT_ENTRY( FLT__MCL_R_OPEN,                   FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_004, "Rear MCL Open"            ) \
   FT_ENTRY( FLT__BCL_R_OPEN,                   FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_004, "Rear BCL Open"            ) \
   FT_ENTRY( FLT__INVALID_EPWR_SPD,             FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_003, "Inv. EPWR Spd"            ) \
   FT_ENTRY( FLT__INVALID_ACCESS_SPD,           FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, FT_PRI_003, "Inv. ACCESS Spd"          ) \
   FT_ENTRY( FLT__UNINTENDED_LCK_AND_GSW,       FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_001, "Unint. LCK And GSW"       ) \
   FT_ENTRY( FLT__DPMF_OPEN,                    FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_006, "DPMF Open"                ) \
   FT_ENTRY( FLT__DPMR_OPEN,                    FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC_OFF, FT_PRI_006, "DPMR Open"                ) \
   FT_ENTRY( FLT__CPLD_2_MR_STARTUP,            FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD MR Startup"          ) \
   FT_ENTRY( FLT__CPLD_2_CT_STARTUP,            FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD CT Startup"          ) \
   FT_ENTRY( FLT__CPLD_2_COP_STARTUP,           FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD COP Startup"         ) \
   FT_ENTRY( FLT__CPLD_2_UNINT_MOV,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Unint Mov"           ) \
   FT_ENTRY( FLT__CPLD_2_CT_COM_LOSS,           FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD CT Comm"             ) \
   FT_ENTRY( FLT__CPLD_2_COP_COM_LOSS,          FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD COP Comm"            ) \
   FT_ENTRY( FLT__CPLD_2_120VAC,                FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD 120 VAC"             ) \
   FT_ENTRY( FLT__CPLD_2_GOVERNOR,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Gov"                 ) \
   FT_ENTRY( FLT__CPLD_2_CAR_BYP,               FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Car Byp"             ) \
   FT_ENTRY( FLT__CPLD_2_HALL_BYP,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Hall Byp"            ) \
   FT_ENTRY( FLT__CPLD_2_SFM_LOSS,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD SFM"                 ) \
   FT_ENTRY( FLT__CPLD_2_SFH_LOSS,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD SFH"                 ) \
   FT_ENTRY( FLT__CPLD_2_PIT_LOSS,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD PIT"                 ) \
   FT_ENTRY( FLT__CPLD_2_BUF_LOSS,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD BUF"                 ) \
   FT_ENTRY( FLT__CPLD_2_TFL_LOSS,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD TFL"                 ) \
   FT_ENTRY( FLT__CPLD_2_BFL_LOSS,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD BFL"                 ) \
   FT_ENTRY( FLT__CPLD_2_CT_SW_LOSS,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD CT SW"               ) \
   FT_ENTRY( FLT__CPLD_2_ESC_HATCH_LOSS,        FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Esc Hatch"           ) \
   FT_ENTRY( FLT__CPLD_2_CAR_SAFE_LOSS,         FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Car Safety"          ) \
   FT_ENTRY( FLT__CPLD_2_IC_STOP,               FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD IC Stop"             ) \
   FT_ENTRY( FLT__CPLD_2_FIRE_STOP,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Fire Stop"           ) \
   FT_ENTRY( FLT__CPLD_2_INSP,                  FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Insp."               ) \
   FT_ENTRY( FLT__CPLD_2_ACCESS,                FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD Access"              ) \
   FT_ENTRY( FLT__CPLD_2_LFT,                   FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD LFT"                 ) \
   FT_ENTRY( FLT__CPLD_2_LFM,                   FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD LFM"                 ) \
   FT_ENTRY( FLT__CPLD_2_LFB,                   FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD LFB"                 ) \
   FT_ENTRY( FLT__CPLD_2_LRT,                   FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD LRT"                 ) \
   FT_ENTRY( FLT__CPLD_2_LRM,                   FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD LRM"                 ) \
   FT_ENTRY( FLT__CPLD_2_LRB,                   FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD LRB"                 ) \
   FT_ENTRY( FLT__CPLD_2_GSWF,                  FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD GSWF"                ) \
   FT_ENTRY( FLT__CPLD_2_GSWR,                  FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD GSWR"                ) \
   FT_ENTRY( FLT__PF_PIT_INSP,                  FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF Pit Insp"              ) \
   FT_ENTRY( FLT__PF_LND_INSP,                  FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF Lnd Insp"              ) \
   FT_ENTRY( FLT__PF_BFL,                       FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF BFL"                   ) \
   FT_ENTRY( FLT__PF_TFL,                       FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF TFL"                   ) \
   FT_ENTRY( FLT__PF_BUF,                       FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF BUF"                   ) \
   FT_ENTRY( FLT__PF_PIT,                       FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF PIT"                   ) \
   FT_ENTRY( FLT__PF_GOV,                       FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF GOV"                   ) \
   FT_ENTRY( FLT__PF_SFH,                       FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF SFH"                   ) \
   FT_ENTRY( FLT__PF_SFM,                       FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF SFM"                   ) \
   FT_ENTRY( FLT__PF_LFT,                       FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF LFT"                   ) \
   FT_ENTRY( FLT__PF_LFM,                       FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF LFM"                   ) \
   FT_ENTRY( FLT__PF_LFB,                       FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF LFB"                   ) \
   FT_ENTRY( FLT__PF_LRT,                       FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF LRT"                   ) \
   FT_ENTRY( FLT__PF_LRM,                       FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF LRM"                   ) \
   FT_ENTRY( FLT__PF_LRB,                       FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF LRB"                   ) \
   FT_ENTRY( FLT__PF_BYP_H,                     FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF Car Byp"               ) \
   FT_ENTRY( FLT__PF_BYP_C,                     FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF Hall Byp"              ) \
   FT_ENTRY( FLT__PF_MR_INSP,                   FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF MR Insp"               ) \
   FT_ENTRY( FLT__PF_CPLD_PICK_BYP,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF C Pick Byp"            ) \
   FT_ENTRY( FLT__PF_MCU_PICK_BYP,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF M Pick Byp"            ) \
   FT_ENTRY( FLT__PF_MCU_DROP_GRIP,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF M Drop Grip"           ) \
   FT_ENTRY( FLT__PF_CPLD_DROP_GRIP,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF C Drop Grip"           ) \
   FT_ENTRY( FLT__PF_CPLD_PICK_GRIP,            FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF C Pick Grip"           ) \
   FT_ENTRY( FLT__PF_MCU_PICK_GRIP,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF M Pick Grip"           ) \
   FT_ENTRY( FLT__PF_MCU_DROP_BYP,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF M Drop Byp"            ) \
   FT_ENTRY( FLT__PF_CPLD_DROP_BYP,             FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF C Drop Byp"            ) \
   FT_ENTRY( FLT__CPLD_MR_UNKNOWN,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD MR Unk."             ) \
   FT_ENTRY( FLT__PF_CT_SW,                     FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF CT Sw"                 ) \
   FT_ENTRY( FLT__PF_ESC_HATCH,                 FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF Esc Hatch"             ) \
   FT_ENTRY( FLT__PF_CAR_SAFE,                  FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF Car Safety"            ) \
   FT_ENTRY( FLT__PF_CT_INSP,                   FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF CT Insp"               ) \
   FT_ENTRY( FLT__PF_GSWF,                      FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF GSWF"                  ) \
   FT_ENTRY( FLT__PF_GSWR,                      FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF GSWR"                  ) \
   FT_ENTRY( FLT__PF_DZF,                       FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF DZF"                   ) \
   FT_ENTRY( FLT__PF_DZR,                       FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF DZR"                   ) \
   FT_ENTRY( FLT__CPLD_CT_UNKNOWN,              FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD CT Unk"              ) \
   FT_ENTRY( FLT__PF_HA_INSP,                   FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF HA Insp"               ) \
   FT_ENTRY( FLT__PF_IC_STOP,                   FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF IC Stop"               ) \
   FT_ENTRY( FLT__PF_FSS,                       FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF FSS"                   ) \
   FT_ENTRY( FLT__PF_IC_INSP,                   FT_OOS__ON, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "PF IC Insp"               ) \
   FT_ENTRY( FLT__CPLD_COP_UNKNOWN,             FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "CPLD COP Unk"             ) \
   FT_ENTRY( FLT__BRAKE_OVERHEAT,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Brake Overheat"           ) \
   FT_ENTRY( FLT__EBRAKE_OVERHEAT,              FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "EBrake Overheat"          ) \
   FT_ENTRY( FLT__MOTION_PICK_B1,               FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Mo. Pick B1 "             ) \
   FT_ENTRY( FLT__OVERSPEED_DOOR_DPM_F,         FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Door OVSP DPM-F"          ) \
   FT_ENTRY( FLT__OVERSPEED_DOOR_DPM_R,         FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "Door OVSP DPM-R"          ) \
   FT_ENTRY( FLT__EQ,                           FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "EQ"                       ) \
   FT_ENTRY( FLT__PHE_TEST_FAILED,              FT_OOS_OFF, FT_CON_OFF, FT_CLR_NORM, FT_CC__ON, NUM_FT_PRI, "PHE Test Fail"            ) \
   FT_ENTRY( FLT__MOTION_PREPARE_GSWF,          FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Prepare GSWF Open"    ) \
   FT_ENTRY( FLT__MOTION_PREPARE_LFT,           FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Prepare LFT Open"     ) \
   FT_ENTRY( FLT__MOTION_PREPARE_LFM,           FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Prepare LFM Open"     ) \
   FT_ENTRY( FLT__MOTION_PREPARE_DPM_F,         FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Prepare DPM F Open"   ) \
   FT_ENTRY( FLT__MOTION_PREPARE_LFB,           FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Prepare LFB Open"     ) \
   FT_ENTRY( FLT__MOTION_PREPARE_GSWR,          FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Prepare GSWR Open"    ) \
   FT_ENTRY( FLT__MOTION_PREPARE_LRT,           FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Prepare LRT Open"     ) \
   FT_ENTRY( FLT__MOTION_PREPARE_LRM,           FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Prepare LRM Open"     ) \
   FT_ENTRY( FLT__MOTION_PREPARE_LRB,           FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Prepare LRB Open"     ) \
   FT_ENTRY( FLT__MOTION_PREPARE_DPM_R,         FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Prepare DPM R Open"   ) \
   FT_ENTRY( FLT__MOTION_ACCEL_GSWF,            FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Accel GSWF Open"      ) \
   FT_ENTRY( FLT__MOTION_ACCEL_LFT,             FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Accel LFT Open"       ) \
   FT_ENTRY( FLT__MOTION_ACCEL_LFM,             FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Accel LFM Open"       ) \
   FT_ENTRY( FLT__MOTION_ACCEL_LFB,             FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Accel LFB Open"       ) \
   FT_ENTRY( FLT__MOTION_ACCEL_DPM_F,           FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Accel DPM F Open"     ) \
   FT_ENTRY( FLT__MOTION_ACCEL_GSWR,            FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Accel GSWR Open"      ) \
   FT_ENTRY( FLT__MOTION_ACCEL_LRT,             FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Accel LRT Open"       ) \
   FT_ENTRY( FLT__MOTION_ACCEL_LRM,             FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Accel LRM Open"       ) \
   FT_ENTRY( FLT__MOTION_ACCEL_LRB,             FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Accel LRB Open"       ) \
   FT_ENTRY( FLT__MOTION_ACCEL_DPM_R,           FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Accel DPM R Open"     ) \
   FT_ENTRY( FLT__MOTION_PREPARE_DCL_F,         FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Prepare DCL F"        ) \
   FT_ENTRY( FLT__MOTION_PREPARE_DCL_R,         FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Prepare DCL R"        ) \
   FT_ENTRY( FLT__MOTION_PREPARE_DOL_F,         FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Prepare DOL F"        ) \
   FT_ENTRY( FLT__MOTION_PREPARE_DOL_R,         FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Prepare DOL R"        ) \
   FT_ENTRY( FLT__MOTION_ACCEL_DCL_F,           FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Accel DCL F"          ) \
   FT_ENTRY( FLT__MOTION_ACCEL_DCL_R,           FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Accel DCL R"          ) \
   FT_ENTRY( FLT__MOTION_ACCEL_DOL_F,           FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Accel DOL F"          ) \
   FT_ENTRY( FLT__MOTION_ACCEL_DOL_R,           FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC_OFF, NUM_FT_PRI, "Mo. Accel DOL R"          ) \
   FT_ENTRY( FLT__VALVE_UNKNOWN,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Valve Unk"                ) \
   FT_ENTRY( FLT__VALVE_POR_RST,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Valve POR Rst"            ) \
   FT_ENTRY( FLT__VALVE_WDT_RST,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Valve WDT Rst"            ) \
   FT_ENTRY( FLT__VALVE_BOD_RST,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Valve Bod Rst"            ) \
   FT_ENTRY( FLT__VALVE_COMM_LOSS,              FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Valve Comm Loss"          ) \
   FT_ENTRY( FLT__VALVE_LOW_DN,                 FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Valve Low Dn"             ) \
   FT_ENTRY( FLT__VALVE_LOW_UP,                 FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Valve Low Up"             ) \
   FT_ENTRY( FLT__VALVE_HIGH_DN,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Valve High Dn"            ) \
   FT_ENTRY( FLT__VALVE_HIGH_UP,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Valve High Up"            ) \
   FT_ENTRY( FLT__VALVE_START_MOTOR,            FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Valve SM"                 ) \
   FT_ENTRY( FLT__VALVE_CAN_BUS_RST,            FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Valve CAN Bus Rst"        ) \
   FT_ENTRY( FLT__SOFT_STARTER_UNKNOWN,         FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Soft Start Unk"           ) \
   FT_ENTRY( FLT__SOFT_STARTER_POR_RST,         FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Soft Start POR Rst"       ) \
   FT_ENTRY( FLT__SOFT_STARTER_WDT_RST,         FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Soft Start WDT Rst"       ) \
   FT_ENTRY( FLT__SOFT_STARTER_BOD_RST,         FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Soft Start BOD Rst"       ) \
   FT_ENTRY( FLT__SOFT_STARTER_COMM_LOSS,       FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Soft Start Comm Loss"     ) \
   FT_ENTRY( FLT__SOFT_STARTER_OVERCURRENT,     FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Soft Start OC"            ) \
   FT_ENTRY( FLT__SOFT_STARTER_OVERVOLTAGE,     FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Soft Start OVV"           ) \
   FT_ENTRY( FLT__SOFT_STARTER_UNDERVOLTAGE,    FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Soft Start UNDV"          ) \
   FT_ENTRY( FLT__SOFT_STARTER_PHASE_MISSING,   FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Soft Start Phase Miss"    ) \
   FT_ENTRY( FLT__SOFT_STARTER_PHASE_SEQUENCE,  FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Soft Start Phase Seq"     ) \
   FT_ENTRY( FLT__SOFT_STARTER_CAN_BUS_RST,     FT_OOS_OFF, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Soft Start CAN Bus Rst"   ) \
   FT_ENTRY( FLT__VALVE_OFFLINE,                FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Valve Offline"            ) \
   FT_ENTRY( FLT__SOFT_STARTER_OFFLINE,         FT_OOS__ON, FT_CON__ON, FT_CLR_NORM, FT_CC__ON, FT_PRI_004, "Soft Start Offline"       )

/* Macros for accessing table elements */
#define EXPAND_FAULT_TABLE_AS_ENUMERATION(A, B, C, D, E, F, G) A, 
#define EXPAND_FAULT_TABLE_AS_OOS_ARRAY(A, B, C, D, E, F, G) B, 
#define EXPAND_FAULT_TABLE_AS_CONSTRUCTION_ARRAY(A, B, C, D, E, F, G) C, 
#define EXPAND_FAULT_TABLE_AS_RESET_ARRAY(A, B, C, D, E, F, G) D, 
#define EXPAND_FAULT_TABLE_AS_CC_ARRAY(A, B, C, D, E, F, G) E, 
#define EXPAND_FAULT_TABLE_AS_PRIORITY_ARRAY(A, B, C, D, E, F, G) F, 
#define EXPAND_FAULT_TABLE_AS_STRING_ARRAY(A, B, C, D, E, F, G) G, 

#endif