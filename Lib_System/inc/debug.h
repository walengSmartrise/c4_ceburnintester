/******************************************************************************
 *
 * @file     sys_private.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     21, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef _DEBUG_H_
#define _DEBUG_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "sys.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

uint8_t Debug_CAN_GetUtilization(uint8_t ucNet);
void Debug_CAN_MonitorUtilization(void);
void DebugPackets(uint16_t uwDatagramToSend_Plus1, uint8_t ucNet);
void Debug_CallRate();
void Debug_ViewPackets(CAN_MSG_T *pstMsg);
#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
