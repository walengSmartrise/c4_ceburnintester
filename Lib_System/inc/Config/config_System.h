#ifndef CONFIG_SYSTEM_H
#define CONFIG_SYSTEM_H
#include <stdint.h>

typedef enum
{
    DEFAULT_ALL_IDLE,
    DEFAULT_ALL_ONE_BIT,
    DEFAULT_ALL_EIGHT_BIT,
    DEFAULT_ALL_SIXTEEN_BIT,
    DEFAULT_ALL_TWENTYFOUR_BIT,
    DEFAULT_ALL_THIRTYTWO_BIT,
    DEFAULT_ALL_COMPLETE
}DefaultAll_FSM;


DefaultAll_FSM GetDefaultAllState(void);
void SetDefaultAllState(DefaultAll_FSM);

uint32_t GetDefaultAllParameterIndex( void );
void IncDefaultAllParameterIndex( void );
void ResetdefaultAllParameterIndex( void );
void SetDefaultAllParameterIndex(uint32_t);


uint8_t DefaultAll( uint8_t *pucState, uint32_t *puiIndex, enum_default_cmd eCommand );

#endif
