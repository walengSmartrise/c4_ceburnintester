#ifndef DEFAULT_ALL_
#define DEFAULT_ALL_
#include "param.h"
#include "param1Bit.h"
#include "param8Bit.h"
#include "param16Bit.h"
#include "param24Bit.h"
#include "param32Bit.h"
#include "config_System.h"
// These structs hold all information about a parameter
typedef struct {
        enum en_1bit_params paramNumber;
        uint8_t paramDefault;
}Param_1BitConfig;

typedef struct {
        enum en_8bit_params paramNumber;
        uint8_t paramDefault;
}Param_8BitConfig;

typedef struct {
        enum en_16bit_params paramNumber;
        uint16_t paramDefault;
}Param_16BitConfig;

typedef struct {
        enum en_24bit_params paramNumber;
        uint32_t paramDefault;
}Param_24BitConfig;

typedef struct {
        enum en_32bit_params paramNumber;
        uint32_t paramDefault;
}Param_32BitConfig;

// Return: !0 when complete, 0 if not complete
uint8_t DefaultAll_1Bit( enum_default_cmd eCommand );
uint8_t DefaultAll_8Bit( enum_default_cmd eCommand );
uint8_t DefaultAll_16Bit( enum_default_cmd eCommand );
uint8_t DefaultAll_24Bit( enum_default_cmd eCommand );
uint8_t DefaultAll_32Bit( enum_default_cmd eCommand );

#endif
