/******************************************************************************
 *
 * @file     sru.h
 * @brief    Header file for MR SRU deployment
 * @version  V1.00
 * @date     28, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef HALLBOARD_H
#define HALLBOARD_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "sys.h"
#include "subfaults.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define NEW_HALL_BOARD_MAX_FLOOR_LIMIT                ( 128 )
#define MAX_NUM_HALLBOARDS_OLD                        ( 512 )  // Max number of HB supported (by 10-dip version)
#define MAX_NUM_HALLBOARDS                            ( 1024 ) // Max number of HB supported (by 12-dip version)
#define NUM_OF_HALLBOARD_BLOCKS                       ( MAX_NUM_HALLBOARDS/NEW_HALL_BOARD_MAX_FLOOR_LIMIT ) // Number of unique hallboard groupings enabled by per car HallMask bitmap param
#define NUM_OF_HALLBOARD_DIPS_OLD                     ( 9 )
#define NUM_OF_HALLBOARD_DIPS                         ( 11 )

#define FIRST_HALLBOARD_FUNCTION_DIP_OLD              ( 7 ) // 10 DIP version
#define FIRST_HALLBOARD_FUNCTION_DIP                  ( 8 ) // 12 DIP version
#define NUM_HALLBOARD_FUNCTION_DIPS                   ( 3 ) // Number of DIP switches defining the hall board's function


#define MAX_NUM_HALL_LANTERNS                         ( 512 ) // Max number of Hall Lanterns supported (by 12-dip version)
#define NUM_OF_HALL_LANTERN_BLOCKS                    ( MAX_NUM_HALL_LANTERNS/NEW_HALL_BOARD_MAX_FLOOR_LIMIT ) // Number of unique hall lanterns groupings enabled by per car HallLanternMask bitmap param

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/* Hall UI requests sent by MRB, CTB, COPB when user is on
 * a board status page for hall boards, hall lanterns, or
 * hall security */
typedef enum
{
   HALL_UI_REQ__NONE,
   HALL_UI_REQ__HALL_BOARD__START,
   HALL_UI_REQ__HALL_BOARD__END = HALL_UI_REQ__HALL_BOARD__START+MAX_NUM_HALLBOARDS-1,
   HALL_UI_REQ__HALL_LANTERN__START,
   HALL_UI_REQ__HALL_LANTERN__END = HALL_UI_REQ__HALL_LANTERN__START+MAX_NUM_HALL_LANTERNS-1,
   HALL_UI_REQ__HALL_SECURITY__START,
   HALL_UI_REQ__HALL_SECURITY__END = HALL_UI_REQ__HALL_SECURITY__START+NEW_HALL_BOARD_MAX_FLOOR_LIMIT-1,

   NUM_HALL_UI_REQ,

   LIMIT_HALL_UI_REQ = 0xFFFF
} en_hall_ui_req;


typedef enum
{
   HC_DIR__DOWN,
   HC_DIR__UP,
} en_hc_dir;
typedef enum
{
   HB_COM_STATE__INACTIVE,
   HB_COM_STATE__0,
   HB_COM_STATE__25,
   HB_COM_STATE__50,
   HB_COM_STATE__75,
   HB_COM_STATE__100,

   NUM_HB_COM_STATE
} en_hb_com_state;
typedef enum
{
   HB_IO_SHIFT__BTN_UP,
   HB_IO_SHIFT__BTN_DN,
   HB_IO_SHIFT__LMP_UP,
   HB_IO_SHIFT__LMP_DN,
}en_hb_io_shifts;

typedef struct
{
      en_hallboard_error eError : 4;
      en_hb_com_state eCommState : 4;
      uint8_t ucBF_RawIO : 4; // see en_hb_io_shifts
      uint8_t ucSourceNode : 4; // Source riser board index plus 1. For tracking duplicate HB DIPs from diff risers
} st_hallboard_status; /* Per address hall board statuses */

typedef enum
{
   HC_DIR_PLUS1__NONE,
   HC_DIR_PLUS1__DN,
   HC_DIR_PLUS1__UP,

   NUM_HC_DIR_PLUS1
} en_hc_dir_plus1;
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
