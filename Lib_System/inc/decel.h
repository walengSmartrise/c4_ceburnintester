/******************************************************************************
 *
 * @file     decel.h
 * @brief    Decel curve functions
 * @version  V1.00
 * @date     11, July 2018
 *
 * @note     This is pattern.c with a reduced memory requirement for
 *           use in ETSL/NTS point generation.
 *
 ******************************************************************************/
#ifndef _DECEL_H_
#define _DECEL_H_

#include "motion.h"
#include "sys.h"
#include "pattern.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/* Values that will change between pattern profiles */
typedef struct
{
      float fDecel;
      float fJerkInDecel; /* This parameter is JerkOutDecel in all non-motion areas of code */
      float fJerkOutDecel; /* This parameter is JerkInDecel in all non-motion areas of code */
      uint16_t uwLevelingSpeed_FPM;
      uint16_t uwLevelingDistance;

      float fFullPatternMinDecelDistance_FT;
      float fShortPatternMinDecelDistance_FT;
      float fShortPatternMinDecelSpeed_FPS;
      uint32_t uiMaxDecelTime;

      uint32_t uiShortPatternMinDecelDistance;
} Decel_Curve_Parameters;
/* Values that will change between runs */
typedef struct
{
      uint32_t uiEndTime_JerkInDecel;
      uint32_t uiEndTime_ConstantDecel;
      uint32_t uiEndTime_JerkOutDecel;

      uint32_t uiSlowdownDistance;

      uint32_t uiPatternDecelTime;

} Decel_Run_Parameters;

/* Values for speed calc */
typedef struct
{
      enum en_patterns ePatternProfile;

      float fEndTime_JerkInDecel;
      float fEndTime_ConstantDecel;
      float fEndTime_JerkOutDecel;

      // Used in midrun speed calculations - in feet per second
      float fStartSpeed_JerkInDecel;//Leveling speed
      float fEndSpeed_JerkInDecel;
      float fEndSpeed_ConstantDecel;
      float fEndSpeed_JerkOutDecel;
      // Used in midrun speed calculations - in feet per sec2
      float fMaxDecel;

} Decel2_Vars;

/* Control */
typedef struct
{
      Decel_Run_Parameters stRunParameters;

      Decel2_Vars stDecelVars;

      float flContractSpeed_FPS; // Max speed in fps
      uint16_t uwContractSpeed_FPM; // Max speed in fpm
      float flPatternTimeRes_sec; //Resolution of generated pattern in seconds

      uint32_t auiDecelPattern_Position[MAX_PATTERN_SIZE_BYTES];
} Decel_Motion_Control;
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern Decel_Curve_Parameters * gpstDecelCurveParameters;
extern Decel_Motion_Control * gpstDecelMotionCtrl;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
uint8_t Decel_UpdateParameters( Motion_Profile eProfile );
void Decel_UpdateCurveLimits( Motion_Profile eProfile );

void Decel_GenerateDecelRun( float flMaxSpeed_FPS );

uint16_t Decel_GetDecelCommandSpeed(uint32_t uiTimeIndex);

uint32_t Decel_GetDecelPosition(uint32_t uiTimeIndex);

uint8_t Decel_CheckForInvalidSCurve(Motion_Profile eProfile);

#endif
