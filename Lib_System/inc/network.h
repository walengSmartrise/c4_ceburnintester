/******************************************************************************
 *
 * @file     network.h
 * @brief    Network header files
 * @version  V1.00
 * @date     15, Sept 2016
 *
 * @note   For network, nodes, and datagram definitions
 *
 ******************************************************************************/

#ifndef NETWORK_H
#define NETWORK_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "chip.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

#define NUM_DEBUGSTATES_PER_PACKET     ( 7U )
#define NUM_DEBUGSTATES_PACKETS        (2)

#define PACKET_UPDATE_RATE_5MS               (100U)
#define DISABLED_DATAGRAM_RESEND_RATE_MS     (0x7FFFFFFF)

/*----------------------------------------------------------------------------
   Network ID MACROS & SHIFTS
 *----------------------------------------------------------------------------*/
#define EXTRACT_HALLNET_NET_ID(x)      (( x >> 25 ) & 0x0F )
#define EXTRACT_HALLNET_SRC_ID(x)      (( x >> 20 ) & 0x1F )
#define EXTRACT_HALLNET_DEST_ID(x)     (( x >> 15 ) & 0x1F ) // Repurposed for extended HB dips SR-1060G
#define EXTRACT_HALLNET_DATAGRAM_ID(x) (( x >> 10 ) & 0x1F )
#define EXTRACT_HALLNET_DIP_ID(x)      (( x >> 0  ) & 0x3FF )
enum en_hallnet_can_id_shift
{
   HALLNET_CAN_ID_SHIFT__DIP  = 0,
   HALLNET_CAN_ID_SHIFT__DG   = 10,
   HALLNET_CAN_ID_SHIFT__DEST = 15, // Repurposed for extended HB dips SR-1060G
   HALLNET_CAN_ID_SHIFT__SRC  = 20,
   HALLNET_CAN_ID_SHIFT__NET  = 25,
   HALLNET_CAN_ID_SHIFT__EXT  = 30,
};


/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
enum en_debug_states
{
   DEBUGSTATES_MotionState,
   DEBUGSTATES_MotionStart,
   DEBUGSTATES_MotionStop,
   DEBUGSTATES_MotionPattern,
   DEBUGSTATES_NTSState,
   DEBUGSTATES_NTSAlarm,
   DEBUGSTATES_NTSFault,
   DEBUGSTATES_AutoState,
   DEBUGSTATES_FloorLearnState,
   DEBUGSTATES_Recall,
   DEBUGSTATES_Counterweight,
   DEBUGSTATES_FireSrv,
   DEBUGSTATES_FireSrv2,
   //Door State? or only low priority

   NUM_DEBUGSTATES
};

/*----------------------------------------------------------------------------
 * Shared Data -- CN AB Network
 *----------------------------------------------------------------------------*/
enum en_ab_cna_datagrams
{
   /* KEEP IN ORDER - START */
   DG_AB_CNA__FaultSlaveRequest,
   DG_AB_CNA__FaultUpdate,
   DG_AB_CNA__ParamValues,
   DG_AB_CNA__ParamMaster_BlockCRCs,
   /* KEEP IN ORDER - END */
   DG_AB_CNA__LocalInputs,
   DG_AB_CNA__DIP_Switches,

   NUM_AB_CNA_DATAGRAMS
};

enum en_ab_cnb_datagrams
{
   /* KEEP IN ORDER - START */
   DG_AB_CNB__ParamSlaveRequest,
   DG_AB_CNB__FaultSlaveRequest,
   /* KEEP IN ORDER - END */
   DG_AB_CNB__DIP_Switches,

   NUM_AB_CNB_DATAGRAMS
};

enum en_car_net_cna_datagrams /* CNA1-CNA8 boards */
{
   DG_CarNet_EXP1__LocalInputs,
   DG_CarNet_EXP2__LocalInputs,
   DG_CarNet_EXP3__LocalInputs,
   DG_CarNet_EXP4__LocalInputs,
   DG_CarNet_EXP5__LocalInputs,
   DG_CarNet_EXP6__LocalInputs,

//   DG_CarNet_CNA1__FaultActive,
   NUM_CarNet_CNA_DATAGRAMS
};


/*----------------------------------------------------------------------------
 * Shared Data -- Ctrl Network
 *----------------------------------------------------------------------------*/
enum en_ctrl_net_mra_datagrams
{
   DG_CtrlNet_MRA__BRK1,
   DG_CtrlNet_MRA__BRK2,

   NUM_CtrlNet_MRA_DATAGRAMS
};

enum en_ctrl_net_trc_datagrams
{
   DG_CtrlNet_TRC__Brake1,
   DG_CtrlNet_TRC__Brake2,

   NUM_CtrlNet_TRC_DATAGRAMS
};

enum en_ctrl_net_brk_datagrams
{
   DG_CtrlNet_BRK1_Data,
   DG_CtrlNet_BRK2_Data,

   NUM_CtrlNet_BRK_DATAGRAMS
};

/*----------------------------------------------------------------------------
 * Shared Data -- Hall Network
 *----------------------------------------------------------------------------*/
#if 1
enum en_hall_net_hio_datagrams
{
   DG_HallNet_HIO__unused_1,

   NUM_HallNet_HIO_DATAGRAMS
};


enum en_hall_net_ris_datagrams
{
   /* Support for 512 unique HB addresses */
   DG_HallNet_RIS__HallLamps_0,
   DG_HallNet_RIS__HallLamps_1,
   DG_HallNet_RIS__HallLamps_2,
   DG_HallNet_RIS__HallLamps_3,
   DG_HallNet_RIS__HallLamps_4,
   DG_HallNet_RIS__HallLamps_5,
   DG_HallNet_RIS__HallLamps_6,
   DG_HallNet_RIS__HallLamps_7,
   DG_HallNet_RIS__HallLamps_8,
   DG_HallNet_RIS__HallLamps_9,
   DG_HallNet_RIS__HallLamps_10,
   DG_HallNet_RIS__HallLamps_11,
   DG_HallNet_RIS__HallLamps_12,
   DG_HallNet_RIS__HallLamps_13,
   DG_HallNet_RIS__HallLamps_14,
   DG_HallNet_RIS__HallLamps_15,

   /* Increased support for 1024 unique HB addresses */
   DG_HallNet_RIS__HallLamps_16,
   DG_HallNet_RIS__HallLamps_17,
   DG_HallNet_RIS__HallLamps_18,
   DG_HallNet_RIS__HallLamps_19,

   DG_HallNet_RIS__HallLamps_20,
   DG_HallNet_RIS__HallLamps_21,
   DG_HallNet_RIS__HallLamps_22,
   DG_HallNet_RIS__HallLamps_23,
   DG_HallNet_RIS__HallLamps_24,
   DG_HallNet_RIS__HallLamps_25,
   DG_HallNet_RIS__HallLamps_26,
   DG_HallNet_RIS__HallLamps_27,
   DG_HallNet_RIS__HallLamps_28,
   DG_HallNet_RIS__HallLamps_29,

   DG_HallNet_RIS__HallLamps_30,
   DG_HallNet_RIS__HallLamps_31,

   NUM_HallNet_RIS_DATAGRAMS
};
enum en_hall_net_hb_datagrams
{
   DG_HallNet_HB__Status,

   NUM_HallNet_HB_DATAGRAMS
};
#else
enum en_hall_net_hio_datagrams
{
   DG_HallNet_HIO__unused_1,

   NUM_HallNet_HIO_DATAGRAMS
};

enum en_hall_net_ris_datagrams
{
   DG_HallNet_RIS__LAMP_COMMANDS_1,
   DG_HallNet_RIS__LAMP_COMMANDS_2,
   DG_HallNet_RIS__LAMP_COMMANDS_3,
   DG_HallNet_RIS__LAMP_COMMANDS_4,

   NUM_HallNet_RIS_DATAGRAMS
};

enum en_hall_net_mrb_datagrams
{
//   DG_HallNet_MRB__unused_1,// interferes with receiving from HB with DIP 0

   DG_HallNet_MRB__Start,

   DG_HallNet_MRB__End = DG_HallNet_MRB__Start+23-1,
   DG_HallNet_MRB__Message, // Edited for CE Board
   DG_HallNet_MRB__Label,
   DG_HallNet_MRB__DiscretePI,
   NUM_HallNet_MRB_DATAGRAMS
};

enum en_hall_net_hb_datagrams
{
//   DG_HallNet_HB__unused_1, // interferes with receiving from HB with DIP 0
   DG_HallNet_HB__Start,

   DG_HallNet_HB__End = DG_HallNet_HB__Start+23-1,
   NUM_HallNet_HB_DATAGRAMS
};

enum en_hall_net_ce_datagrams
{
//   DG_HallNet_CE__unused_1, // interferes with receiving from HB with DIP 0

   DG_HallNet_CE__Message, // Edited for CE Board
   DG_HallNet_CE__Label,
   DG_HallNet_CE__DiscretePI,
   NUM_HallNet_CE_DATAGRAMS
};

#endif

/*----------------------------------------------------------------------------
 * Shared Data -- IO Network
 *----------------------------------------------------------------------------*/
enum en_io_net_master_datagrams
{
   DG_IONet_Master_Outputs,

   NUM_IONet_Master_DATAGRAMS
};
enum en_io_net_slave_datagrams
{
   DG_IONet_Slave_Inputs1,
   DG_IONet_Slave_Inputs2,
   DG_IONet_Slave_Inputs3,
   DG_IONet_Slave_Inputs4,
   DG_IONet_Slave_Inputs5,
   DG_IONet_Slave_Inputs6,
   DG_IONet_Slave_Inputs7,
   NUM_IONet_Slave_DATAGRAMS
};
/*----------------------------------------------------------------------------
 * Shared Data -- Aux Network
 *----------------------------------------------------------------------------*/
enum en_aux_net_mrb_datagrams
{
   DG_AuxNet_MRB__Message,
   DG_AuxNet_MRB__Label,
   DG_AuxNet_MRB__DiscretePI,
   DG_AuxNet_MRB__LoadWeigher,
   DG_AuxNet_MRB__HallLamps_0,
   DG_AuxNet_MRB__HallLamps_1,
   DG_AuxNet_MRB__HallLamps_2,
   DG_AuxNet_MRB__HallLamps_3,
   DG_AuxNet_MRB__HallLamps_4,
   DG_AuxNet_MRB__HallLamps_5,
   DG_AuxNet_MRB__HallLamps_6,
   DG_AuxNet_MRB__HallLamps_7,
   DG_AuxNet_MRB__HallLamps_8,
   DG_AuxNet_MRB__HallLamps_9,
   DG_AuxNet_MRB__HallLamps_10,
   DG_AuxNet_MRB__HallLamps_11,
   DG_AuxNet_MRB__HallLamps_12,
   DG_AuxNet_MRB__HallLamps_13,
   DG_AuxNet_MRB__HallLamps_14,
   DG_AuxNet_MRB__HallLamps_15,
   NUM_AuxNet_MRB_DATAGRAMS
};
enum en_aux_net_lw_datagrams
{
   DG_AuxNet_LW__Response,

   NUM_AuxNet_LW_DATAGRAMS
};
enum en_aux_net_ctb_datagrams
{
   DG_AuxNet_CTB__Message,
   DG_AuxNet_CTB__Label,
   DG_AuxNet_CTB__DiscretePI,
   DG_AuxNet_CTB__LoadWeigher,
   NUM_AuxNet_CTB_DATAGRAMS
};
enum en_aux_net_copb_datagrams
{
   DG_AuxNet_COPB__Message,
   DG_AuxNet_COPB__Label,
   DG_AuxNet_COPB__DiscretePI,
   DG_AuxNet_COPB__CED_FloorInfo,
   DG_AuxNet_COPB__CED_Priority,
   DG_AuxNet_COPB__CED_Secondary,
   DG_AuxNet_COPB__CED_Audio,
   DG_AuxNet_COPB__CED_Flash,
   DG_AuxNet_COPB__CED_Dest,
   DG_AuxNet_COPB__CED_Label,
   DG_AuxNet_COPB__CED_Config,
   NUM_AuxNet_COPB_DATAGRAMS
};

enum en_aux_net_copb_dupar_datagrams
{
	DG_AuxNet_COPB__LatchedCarCalls_F0,
	DG_AuxNet_COPB__LatchedCarCalls_F1,
	DG_AuxNet_COPB__LatchedCarCalls_R0,
	DG_AuxNet_COPB__LatchedCarCalls_R1,
	DG_AuxNet_COPB__SecureBitmap_F0,
	DG_AuxNet_COPB__SecureBitmap_F1,
	DG_AuxNet_COPB__SecureBitmap_R0,
	DG_AuxNet_COPB__SecureBitmap_R1,
	DG_AuxNet_COPB__CarOperation,
	DG_AuxNet_COPB__EmergencyStatus,

	NUM_AuxNet__COPB_DUPAR_DATAGRAMS
};

enum en_aux_net_dupar_request_datagrams
{
	DG_AuxNet_DUPAR__Request,

	NUM_AuxNet_DUPAR_REQUEST_DATAGRAMS
};

typedef enum
{
   DG_AuxNet_SHIELD__CarOperation,
   DG_AuxNet_SHIELD__CallResponse,
   DG_AuxNet_SHIELD__Timeouts,
   DG_AuxNet_SHIELD__ButtonState,
   DG_AuxNet_SHIELD__OpMode,

   NUM_AuxNet_SHIELD_DATAGRAMS
} en_aux_net_shield_datagrams;
/*----------------------------------------------------------------------------
 * Shared Data -- Netwotk IDs
 *----------------------------------------------------------------------------*/
enum en_car_net_nodes
{
   CAR_NET__MRA,
   CAR_NET__CTA,
   CAR_NET__CNA,
   CAR_NET__COPA,
   CAR_NET__TRC,
   NUM_CAR_NET_NODES
};
enum en_ctrl_net_nodes
{
   CTRL_NET__MRA,
   CTRL_NET__TRC,
   CTRL_NET__BRK,

   NUM_CTRL_NET_NODES
};
enum en_group_net_nodes
{
   GROUP_NET__CAR_1,
   GROUP_NET__CAR_2,
   GROUP_NET__CAR_3,
   GROUP_NET__CAR_4,
   GROUP_NET__CAR_5,
   GROUP_NET__CAR_6,
   GROUP_NET__CAR_7,
   GROUP_NET__CAR_8,
   GROUP_NET__RIS_1,
   GROUP_NET__RIS_2,
   GROUP_NET__RIS_3, /* GROUP_NET__RIS_3 is the security riser. HBs take security inputs */
   GROUP_NET__RIS_4,
   GROUP_NET__SHIELD,
   GROUP_NET__XREG,
   GROUP_NET__DDM,

   NUM_GROUP_NET_NODES
};// LIMITED TO 14
enum en_hall_net_nodes /* Must Match Order of gastSData_Nodes[] and gaucPerNodeNumDatagrams[] */
{
   HALL_NET__RIS,
   HALL_NET__MRB,
   HALL_NET__HB,

   NUM_HALL_NET_NODES
};
enum en_io_net_nodes
{
   IO_NET__MASTER,
   IO_NET__SLAVE,

   NUM_IO_NET_NODES
};
enum en_mr_aux_net_nodes
{
   MR_AUX_NET__MRB,
   MR_AUX_NET__LW, // Value must match CT_AUX_NET__LW

   NUM_MR_AUX_NET_NODES
};

enum en_ct_aux_net_nodes
{
   CT_AUX_NET__CTB,
   CT_AUX_NET__LW, // Value must match MR_AUX_NET__LW

   NUM_CT_AUX_NET_NODES
};
enum en_cop_aux_net_nodes
{
   COP_AUX_NET__COPB,

   NUM_COP_AUX_NET_NODES
};


typedef enum
{
   SHIELD_AUX_NET__SHIELD,
   SHIELD_AUX_NET__KIOSK,

   NUM_SHIELD_AUX_NET_NODES
} en_shield_aux_net_nodes;
/*----------------------------------------------------------------------------

   SData Network Definitions

 *----------------------------------------------------------------------------*/
enum en_sdata_networks
{
   SDATA_NET__AB,
   SDATA_NET__CAR = 1,
   SDATA_NET__CONTROL,
   SDATA_NET__GROUP,
   SDATA_NET__HALL,
   SDATA_NET__IO, // Only for forwarding between daisy chained IO boards
   SDATA_NET__AUX,
   NUM_SDATA_NETWORKS
};
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/


#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
1 *----------------------------------------------------------------------------*/
