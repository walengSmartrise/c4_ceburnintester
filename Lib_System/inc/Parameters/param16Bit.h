#ifndef SMARTRISE_PARAMETERS_16BIT
#define SMARTRISE_PARAMETERS_16BIT

/* NOTE: FILE UPDATED VIA LIB_SYSTEM - SUPPORT,
 * Edit SystemParameters.xlsx run Update_Parameters.bat
 * to automatically update this file. */

enum en_16bit_params
{
   enPARAM16__MR_Inputs_501,                  // 16-0000
   enPARAM16__MR_Inputs_502,                  // 16-0001
   enPARAM16__MR_Inputs_503,                  // 16-0002
   enPARAM16__MR_Inputs_504,                  // 16-0003
   enPARAM16__MR_Inputs_505,                  // 16-0004
   enPARAM16__MR_Inputs_506,                  // 16-0005
   enPARAM16__MR_Inputs_507,                  // 16-0006
   enPARAM16__MR_Inputs_508,                  // 16-0007
   enPARAM16__CT_Inputs_501,                  // 16-0008
   enPARAM16__CT_Inputs_502,                  // 16-0009
   enPARAM16__CT_Inputs_503,                  // 16-0010
   enPARAM16__CT_Inputs_504,                  // 16-0011
   enPARAM16__CT_Inputs_505,                  // 16-0012
   enPARAM16__CT_Inputs_506,                  // 16-0013
   enPARAM16__CT_Inputs_507,                  // 16-0014
   enPARAM16__CT_Inputs_508,                  // 16-0015
   enPARAM16__CT_Inputs_509,                  // 16-0016
   enPARAM16__CT_Inputs_510,                  // 16-0017
   enPARAM16__CT_Inputs_511,                  // 16-0018
   enPARAM16__CT_Inputs_512,                  // 16-0019
   enPARAM16__CT_Inputs_513,                  // 16-0020
   enPARAM16__CT_Inputs_514,                  // 16-0021
   enPARAM16__CT_Inputs_515,                  // 16-0022
   enPARAM16__CT_Inputs_516,                  // 16-0023
   enPARAM16__COP_Inputs_501,                 // 16-0024
   enPARAM16__COP_Inputs_502,                 // 16-0025
   enPARAM16__COP_Inputs_503,                 // 16-0026
   enPARAM16__COP_Inputs_504,                 // 16-0027
   enPARAM16__COP_Inputs_505,                 // 16-0028
   enPARAM16__COP_Inputs_506,                 // 16-0029
   enPARAM16__COP_Inputs_507,                 // 16-0030
   enPARAM16__COP_Inputs_508,                 // 16-0031
   enPARAM16__COP_Inputs_509,                 // 16-0032
   enPARAM16__COP_Inputs_510,                 // 16-0033
   enPARAM16__COP_Inputs_511,                 // 16-0034
   enPARAM16__COP_Inputs_512,                 // 16-0035
   enPARAM16__COP_Inputs_513,                 // 16-0036
   enPARAM16__COP_Inputs_514,                 // 16-0037
   enPARAM16__COP_Inputs_515,                 // 16-0038
   enPARAM16__COP_Inputs_516,                 // 16-0039
   enPARAM16__RIS1_Inputs_501,                // 16-0040
   enPARAM16__RIS1_Inputs_502,                // 16-0041
   enPARAM16__RIS1_Inputs_503,                // 16-0042
   enPARAM16__RIS1_Inputs_504,                // 16-0043
   enPARAM16__RIS1_Inputs_505,                // 16-0044
   enPARAM16__RIS1_Inputs_506,                // 16-0045
   enPARAM16__RIS1_Inputs_507,                // 16-0046
   enPARAM16__RIS1_Inputs_508,                // 16-0047
   enPARAM16__RIS2_Inputs_501,                // 16-0048
   enPARAM16__RIS2_Inputs_502,                // 16-0049
   enPARAM16__RIS2_Inputs_503,                // 16-0050
   enPARAM16__RIS2_Inputs_504,                // 16-0051
   enPARAM16__RIS2_Inputs_505,                // 16-0052
   enPARAM16__RIS2_Inputs_506,                // 16-0053
   enPARAM16__RIS2_Inputs_507,                // 16-0054
   enPARAM16__RIS2_Inputs_508,                // 16-0055
   enPARAM16__RIS3_Inputs_501,                // 16-0056
   enPARAM16__RIS3_Inputs_502,                // 16-0057
   enPARAM16__RIS3_Inputs_503,                // 16-0058
   enPARAM16__RIS3_Inputs_504,                // 16-0059
   enPARAM16__RIS3_Inputs_505,                // 16-0060
   enPARAM16__RIS3_Inputs_506,                // 16-0061
   enPARAM16__RIS3_Inputs_507,                // 16-0062
   enPARAM16__RIS3_Inputs_508,                // 16-0063
   enPARAM16__RIS4_Inputs_501,                // 16-0064
   enPARAM16__RIS4_Inputs_502,                // 16-0065
   enPARAM16__RIS4_Inputs_503,                // 16-0066
   enPARAM16__RIS4_Inputs_504,                // 16-0067
   enPARAM16__RIS4_Inputs_505,                // 16-0068
   enPARAM16__RIS4_Inputs_506,                // 16-0069
   enPARAM16__RIS4_Inputs_507,                // 16-0070
   enPARAM16__RIS4_Inputs_508,                // 16-0071
   enPARAM16__EXP01_Inputs_501,               // 16-0072
   enPARAM16__EXP01_Inputs_502,               // 16-0073
   enPARAM16__EXP01_Inputs_503,               // 16-0074
   enPARAM16__EXP01_Inputs_504,               // 16-0075
   enPARAM16__EXP01_Inputs_505,               // 16-0076
   enPARAM16__EXP01_Inputs_506,               // 16-0077
   enPARAM16__EXP01_Inputs_507,               // 16-0078
   enPARAM16__EXP01_Inputs_508,               // 16-0079
   enPARAM16__EXP02_Inputs_501,               // 16-0080
   enPARAM16__EXP02_Inputs_502,               // 16-0081
   enPARAM16__EXP02_Inputs_503,               // 16-0082
   enPARAM16__EXP02_Inputs_504,               // 16-0083
   enPARAM16__EXP02_Inputs_505,               // 16-0084
   enPARAM16__EXP02_Inputs_506,               // 16-0085
   enPARAM16__EXP02_Inputs_507,               // 16-0086
   enPARAM16__EXP02_Inputs_508,               // 16-0087
   enPARAM16__EXP03_Inputs_501,               // 16-0088
   enPARAM16__EXP03_Inputs_502,               // 16-0089
   enPARAM16__EXP03_Inputs_503,               // 16-0090
   enPARAM16__EXP03_Inputs_504,               // 16-0091
   enPARAM16__EXP03_Inputs_505,               // 16-0092
   enPARAM16__EXP03_Inputs_506,               // 16-0093
   enPARAM16__EXP03_Inputs_507,               // 16-0094
   enPARAM16__EXP03_Inputs_508,               // 16-0095
   enPARAM16__EXP04_Inputs_501,               // 16-0096
   enPARAM16__EXP04_Inputs_502,               // 16-0097
   enPARAM16__EXP04_Inputs_503,               // 16-0098
   enPARAM16__EXP04_Inputs_504,               // 16-0099
   enPARAM16__EXP04_Inputs_505,               // 16-0100
   enPARAM16__EXP04_Inputs_506,               // 16-0101
   enPARAM16__EXP04_Inputs_507,               // 16-0102
   enPARAM16__EXP04_Inputs_508,               // 16-0103
   enPARAM16__EXP05_Inputs_501,               // 16-0104
   enPARAM16__EXP05_Inputs_502,               // 16-0105
   enPARAM16__EXP05_Inputs_503,               // 16-0106
   enPARAM16__EXP05_Inputs_504,               // 16-0107
   enPARAM16__EXP05_Inputs_505,               // 16-0108
   enPARAM16__EXP05_Inputs_506,               // 16-0109
   enPARAM16__EXP05_Inputs_507,               // 16-0110
   enPARAM16__EXP05_Inputs_508,               // 16-0111
   enPARAM16__EXP06_Inputs_501,               // 16-0112
   enPARAM16__EXP06_Inputs_502,               // 16-0113
   enPARAM16__EXP06_Inputs_503,               // 16-0114
   enPARAM16__EXP06_Inputs_504,               // 16-0115
   enPARAM16__EXP06_Inputs_505,               // 16-0116
   enPARAM16__EXP06_Inputs_506,               // 16-0117
   enPARAM16__EXP06_Inputs_507,               // 16-0118
   enPARAM16__EXP06_Inputs_508,               // 16-0119
   enPARAM16__EXP07_Inputs_501,               // 16-0120
   enPARAM16__EXP07_Inputs_502,               // 16-0121
   enPARAM16__EXP07_Inputs_503,               // 16-0122
   enPARAM16__EXP07_Inputs_504,               // 16-0123
   enPARAM16__EXP07_Inputs_505,               // 16-0124
   enPARAM16__EXP07_Inputs_506,               // 16-0125
   enPARAM16__EXP07_Inputs_507,               // 16-0126
   enPARAM16__EXP07_Inputs_508,               // 16-0127
   enPARAM16__EXP08_Inputs_501,               // 16-0128
   enPARAM16__EXP08_Inputs_502,               // 16-0129
   enPARAM16__EXP08_Inputs_503,               // 16-0130
   enPARAM16__EXP08_Inputs_504,               // 16-0131
   enPARAM16__EXP08_Inputs_505,               // 16-0132
   enPARAM16__EXP08_Inputs_506,               // 16-0133
   enPARAM16__EXP08_Inputs_507,               // 16-0134
   enPARAM16__EXP08_Inputs_508,               // 16-0135
   enPARAM16__EXP09_Inputs_501,               // 16-0136
   enPARAM16__EXP09_Inputs_502,               // 16-0137
   enPARAM16__EXP09_Inputs_503,               // 16-0138
   enPARAM16__EXP09_Inputs_504,               // 16-0139
   enPARAM16__EXP09_Inputs_505,               // 16-0140
   enPARAM16__EXP09_Inputs_506,               // 16-0141
   enPARAM16__EXP09_Inputs_507,               // 16-0142
   enPARAM16__EXP09_Inputs_508,               // 16-0143
   enPARAM16__EXP10_Inputs_501,               // 16-0144
   enPARAM16__EXP10_Inputs_502,               // 16-0145
   enPARAM16__EXP10_Inputs_503,               // 16-0146
   enPARAM16__EXP10_Inputs_504,               // 16-0147
   enPARAM16__EXP10_Inputs_505,               // 16-0148
   enPARAM16__EXP10_Inputs_506,               // 16-0149
   enPARAM16__EXP10_Inputs_507,               // 16-0150
   enPARAM16__EXP10_Inputs_508,               // 16-0151
   enPARAM16__EXP11_Inputs_501,               // 16-0152
   enPARAM16__EXP11_Inputs_502,               // 16-0153
   enPARAM16__EXP11_Inputs_503,               // 16-0154
   enPARAM16__EXP11_Inputs_504,               // 16-0155
   enPARAM16__EXP11_Inputs_505,               // 16-0156
   enPARAM16__EXP11_Inputs_506,               // 16-0157
   enPARAM16__EXP11_Inputs_507,               // 16-0158
   enPARAM16__EXP11_Inputs_508,               // 16-0159
   enPARAM16__EXP12_Inputs_501,               // 16-0160
   enPARAM16__EXP12_Inputs_502,               // 16-0161
   enPARAM16__EXP12_Inputs_503,               // 16-0162
   enPARAM16__EXP12_Inputs_504,               // 16-0163
   enPARAM16__EXP12_Inputs_505,               // 16-0164
   enPARAM16__EXP12_Inputs_506,               // 16-0165
   enPARAM16__EXP12_Inputs_507,               // 16-0166
   enPARAM16__EXP12_Inputs_508,               // 16-0167
   enPARAM16__EXP13_Inputs_501,               // 16-0168
   enPARAM16__EXP13_Inputs_502,               // 16-0169
   enPARAM16__EXP13_Inputs_503,               // 16-0170
   enPARAM16__EXP13_Inputs_504,               // 16-0171
   enPARAM16__EXP13_Inputs_505,               // 16-0172
   enPARAM16__EXP13_Inputs_506,               // 16-0173
   enPARAM16__EXP13_Inputs_507,               // 16-0174
   enPARAM16__EXP13_Inputs_508,               // 16-0175
   enPARAM16__EXP14_Inputs_501,               // 16-0176
   enPARAM16__EXP14_Inputs_502,               // 16-0177
   enPARAM16__EXP14_Inputs_503,               // 16-0178
   enPARAM16__EXP14_Inputs_504,               // 16-0179
   enPARAM16__EXP14_Inputs_505,               // 16-0180
   enPARAM16__EXP14_Inputs_506,               // 16-0181
   enPARAM16__EXP14_Inputs_507,               // 16-0182
   enPARAM16__EXP14_Inputs_508,               // 16-0183
   enPARAM16__EXP15_Inputs_501,               // 16-0184
   enPARAM16__EXP15_Inputs_502,               // 16-0185
   enPARAM16__EXP15_Inputs_503,               // 16-0186
   enPARAM16__EXP15_Inputs_504,               // 16-0187
   enPARAM16__EXP15_Inputs_505,               // 16-0188
   enPARAM16__EXP15_Inputs_506,               // 16-0189
   enPARAM16__EXP15_Inputs_507,               // 16-0190
   enPARAM16__EXP15_Inputs_508,               // 16-0191
   enPARAM16__EXP16_Inputs_501,               // 16-0192
   enPARAM16__EXP16_Inputs_502,               // 16-0193
   enPARAM16__EXP16_Inputs_503,               // 16-0194
   enPARAM16__EXP16_Inputs_504,               // 16-0195
   enPARAM16__EXP16_Inputs_505,               // 16-0196
   enPARAM16__EXP16_Inputs_506,               // 16-0197
   enPARAM16__EXP16_Inputs_507,               // 16-0198
   enPARAM16__EXP16_Inputs_508,               // 16-0199
   enPARAM16__EXP17_Inputs_501,               // 16-0200
   enPARAM16__EXP17_Inputs_502,               // 16-0201
   enPARAM16__EXP17_Inputs_503,               // 16-0202
   enPARAM16__EXP17_Inputs_504,               // 16-0203
   enPARAM16__EXP17_Inputs_505,               // 16-0204
   enPARAM16__EXP17_Inputs_506,               // 16-0205
   enPARAM16__EXP17_Inputs_507,               // 16-0206
   enPARAM16__EXP17_Inputs_508,               // 16-0207
   enPARAM16__EXP18_Inputs_501,               // 16-0208
   enPARAM16__EXP18_Inputs_502,               // 16-0209
   enPARAM16__EXP18_Inputs_503,               // 16-0210
   enPARAM16__EXP18_Inputs_504,               // 16-0211
   enPARAM16__EXP18_Inputs_505,               // 16-0212
   enPARAM16__EXP18_Inputs_506,               // 16-0213
   enPARAM16__EXP18_Inputs_507,               // 16-0214
   enPARAM16__EXP18_Inputs_508,               // 16-0215
   enPARAM16__EXP19_Inputs_501,               // 16-0216
   enPARAM16__EXP19_Inputs_502,               // 16-0217
   enPARAM16__EXP19_Inputs_503,               // 16-0218
   enPARAM16__EXP19_Inputs_504,               // 16-0219
   enPARAM16__EXP19_Inputs_505,               // 16-0220
   enPARAM16__EXP19_Inputs_506,               // 16-0221
   enPARAM16__EXP19_Inputs_507,               // 16-0222
   enPARAM16__EXP19_Inputs_508,               // 16-0223
   enPARAM16__EXP20_Inputs_501,               // 16-0224
   enPARAM16__EXP20_Inputs_502,               // 16-0225
   enPARAM16__EXP20_Inputs_503,               // 16-0226
   enPARAM16__EXP20_Inputs_504,               // 16-0227
   enPARAM16__EXP20_Inputs_505,               // 16-0228
   enPARAM16__EXP20_Inputs_506,               // 16-0229
   enPARAM16__EXP20_Inputs_507,               // 16-0230
   enPARAM16__EXP20_Inputs_508,               // 16-0231
   enPARAM16__EXP21_Inputs_501,               // 16-0232
   enPARAM16__EXP21_Inputs_502,               // 16-0233
   enPARAM16__EXP21_Inputs_503,               // 16-0234
   enPARAM16__EXP21_Inputs_504,               // 16-0235
   enPARAM16__EXP21_Inputs_505,               // 16-0236
   enPARAM16__EXP21_Inputs_506,               // 16-0237
   enPARAM16__EXP21_Inputs_507,               // 16-0238
   enPARAM16__EXP21_Inputs_508,               // 16-0239
   enPARAM16__EXP22_Inputs_501,               // 16-0240
   enPARAM16__EXP22_Inputs_502,               // 16-0241
   enPARAM16__EXP22_Inputs_503,               // 16-0242
   enPARAM16__EXP22_Inputs_504,               // 16-0243
   enPARAM16__EXP22_Inputs_505,               // 16-0244
   enPARAM16__EXP22_Inputs_506,               // 16-0245
   enPARAM16__EXP22_Inputs_507,               // 16-0246
   enPARAM16__EXP22_Inputs_508,               // 16-0247
   enPARAM16__EXP23_Inputs_501,               // 16-0248
   enPARAM16__EXP23_Inputs_502,               // 16-0249
   enPARAM16__EXP23_Inputs_503,               // 16-0250
   enPARAM16__EXP23_Inputs_504,               // 16-0251
   enPARAM16__EXP23_Inputs_505,               // 16-0252
   enPARAM16__EXP23_Inputs_506,               // 16-0253
   enPARAM16__EXP23_Inputs_507,               // 16-0254
   enPARAM16__EXP23_Inputs_508,               // 16-0255
   enPARAM16__EXP24_Inputs_501,               // 16-0256
   enPARAM16__EXP24_Inputs_502,               // 16-0257
   enPARAM16__EXP24_Inputs_503,               // 16-0258
   enPARAM16__EXP24_Inputs_504,               // 16-0259
   enPARAM16__EXP24_Inputs_505,               // 16-0260
   enPARAM16__EXP24_Inputs_506,               // 16-0261
   enPARAM16__EXP24_Inputs_507,               // 16-0262
   enPARAM16__EXP24_Inputs_508,               // 16-0263
   enPARAM16__EXP25_Inputs_501,               // 16-0264
   enPARAM16__EXP25_Inputs_502,               // 16-0265
   enPARAM16__EXP25_Inputs_503,               // 16-0266
   enPARAM16__EXP25_Inputs_504,               // 16-0267
   enPARAM16__EXP25_Inputs_505,               // 16-0268
   enPARAM16__EXP25_Inputs_506,               // 16-0269
   enPARAM16__EXP25_Inputs_507,               // 16-0270
   enPARAM16__EXP25_Inputs_508,               // 16-0271
   enPARAM16__EXP26_Inputs_501,               // 16-0272
   enPARAM16__EXP26_Inputs_502,               // 16-0273
   enPARAM16__EXP26_Inputs_503,               // 16-0274
   enPARAM16__EXP26_Inputs_504,               // 16-0275
   enPARAM16__EXP26_Inputs_505,               // 16-0276
   enPARAM16__EXP26_Inputs_506,               // 16-0277
   enPARAM16__EXP26_Inputs_507,               // 16-0278
   enPARAM16__EXP26_Inputs_508,               // 16-0279
   enPARAM16__EXP27_Inputs_501,               // 16-0280
   enPARAM16__EXP27_Inputs_502,               // 16-0281
   enPARAM16__EXP27_Inputs_503,               // 16-0282
   enPARAM16__EXP27_Inputs_504,               // 16-0283
   enPARAM16__EXP27_Inputs_505,               // 16-0284
   enPARAM16__EXP27_Inputs_506,               // 16-0285
   enPARAM16__EXP27_Inputs_507,               // 16-0286
   enPARAM16__EXP27_Inputs_508,               // 16-0287
   enPARAM16__EXP28_Inputs_501,               // 16-0288
   enPARAM16__EXP28_Inputs_502,               // 16-0289
   enPARAM16__EXP28_Inputs_503,               // 16-0290
   enPARAM16__EXP28_Inputs_504,               // 16-0291
   enPARAM16__EXP28_Inputs_505,               // 16-0292
   enPARAM16__EXP28_Inputs_506,               // 16-0293
   enPARAM16__EXP28_Inputs_507,               // 16-0294
   enPARAM16__EXP28_Inputs_508,               // 16-0295
   enPARAM16__EXP29_Inputs_501,               // 16-0296
   enPARAM16__EXP29_Inputs_502,               // 16-0297
   enPARAM16__EXP29_Inputs_503,               // 16-0298
   enPARAM16__EXP29_Inputs_504,               // 16-0299
   enPARAM16__EXP29_Inputs_505,               // 16-0300
   enPARAM16__EXP29_Inputs_506,               // 16-0301
   enPARAM16__EXP29_Inputs_507,               // 16-0302
   enPARAM16__EXP29_Inputs_508,               // 16-0303
   enPARAM16__EXP30_Inputs_501,               // 16-0304
   enPARAM16__EXP30_Inputs_502,               // 16-0305
   enPARAM16__EXP30_Inputs_503,               // 16-0306
   enPARAM16__EXP30_Inputs_504,               // 16-0307
   enPARAM16__EXP30_Inputs_505,               // 16-0308
   enPARAM16__EXP30_Inputs_506,               // 16-0309
   enPARAM16__EXP30_Inputs_507,               // 16-0310
   enPARAM16__EXP30_Inputs_508,               // 16-0311
   enPARAM16__EXP31_Inputs_501,               // 16-0312
   enPARAM16__EXP31_Inputs_502,               // 16-0313
   enPARAM16__EXP31_Inputs_503,               // 16-0314
   enPARAM16__EXP31_Inputs_504,               // 16-0315
   enPARAM16__EXP31_Inputs_505,               // 16-0316
   enPARAM16__EXP31_Inputs_506,               // 16-0317
   enPARAM16__EXP31_Inputs_507,               // 16-0318
   enPARAM16__EXP31_Inputs_508,               // 16-0319
   enPARAM16__EXP32_Inputs_501,               // 16-0320
   enPARAM16__EXP32_Inputs_502,               // 16-0321
   enPARAM16__EXP32_Inputs_503,               // 16-0322
   enPARAM16__EXP32_Inputs_504,               // 16-0323
   enPARAM16__EXP32_Inputs_505,               // 16-0324
   enPARAM16__EXP32_Inputs_506,               // 16-0325
   enPARAM16__EXP32_Inputs_507,               // 16-0326
   enPARAM16__EXP32_Inputs_508,               // 16-0327
   enPARAM16__EXP33_Inputs_501,               // 16-0328
   enPARAM16__EXP33_Inputs_502,               // 16-0329
   enPARAM16__EXP33_Inputs_503,               // 16-0330
   enPARAM16__EXP33_Inputs_504,               // 16-0331
   enPARAM16__EXP33_Inputs_505,               // 16-0332
   enPARAM16__EXP33_Inputs_506,               // 16-0333
   enPARAM16__EXP33_Inputs_507,               // 16-0334
   enPARAM16__EXP33_Inputs_508,               // 16-0335
   enPARAM16__EXP34_Inputs_501,               // 16-0336
   enPARAM16__EXP34_Inputs_502,               // 16-0337
   enPARAM16__EXP34_Inputs_503,               // 16-0338
   enPARAM16__EXP34_Inputs_504,               // 16-0339
   enPARAM16__EXP34_Inputs_505,               // 16-0340
   enPARAM16__EXP34_Inputs_506,               // 16-0341
   enPARAM16__EXP34_Inputs_507,               // 16-0342
   enPARAM16__EXP34_Inputs_508,               // 16-0343
   enPARAM16__EXP35_Inputs_501,               // 16-0344
   enPARAM16__EXP35_Inputs_502,               // 16-0345
   enPARAM16__EXP35_Inputs_503,               // 16-0346
   enPARAM16__EXP35_Inputs_504,               // 16-0347
   enPARAM16__EXP35_Inputs_505,               // 16-0348
   enPARAM16__EXP35_Inputs_506,               // 16-0349
   enPARAM16__EXP35_Inputs_507,               // 16-0350
   enPARAM16__EXP35_Inputs_508,               // 16-0351
   enPARAM16__EXP36_Inputs_501,               // 16-0352
   enPARAM16__EXP36_Inputs_502,               // 16-0353
   enPARAM16__EXP36_Inputs_503,               // 16-0354
   enPARAM16__EXP36_Inputs_504,               // 16-0355
   enPARAM16__EXP36_Inputs_505,               // 16-0356
   enPARAM16__EXP36_Inputs_506,               // 16-0357
   enPARAM16__EXP36_Inputs_507,               // 16-0358
   enPARAM16__EXP36_Inputs_508,               // 16-0359
   enPARAM16__EXP37_Inputs_501,               // 16-0360
   enPARAM16__EXP37_Inputs_502,               // 16-0361
   enPARAM16__EXP37_Inputs_503,               // 16-0362
   enPARAM16__EXP37_Inputs_504,               // 16-0363
   enPARAM16__EXP37_Inputs_505,               // 16-0364
   enPARAM16__EXP37_Inputs_506,               // 16-0365
   enPARAM16__EXP37_Inputs_507,               // 16-0366
   enPARAM16__EXP37_Inputs_508,               // 16-0367
   enPARAM16__EXP38_Inputs_501,               // 16-0368
   enPARAM16__EXP38_Inputs_502,               // 16-0369
   enPARAM16__EXP38_Inputs_503,               // 16-0370
   enPARAM16__EXP38_Inputs_504,               // 16-0371
   enPARAM16__EXP38_Inputs_505,               // 16-0372
   enPARAM16__EXP38_Inputs_506,               // 16-0373
   enPARAM16__EXP38_Inputs_507,               // 16-0374
   enPARAM16__EXP38_Inputs_508,               // 16-0375
   enPARAM16__EXP39_Inputs_501,               // 16-0376
   enPARAM16__EXP39_Inputs_502,               // 16-0377
   enPARAM16__EXP39_Inputs_503,               // 16-0378
   enPARAM16__EXP39_Inputs_504,               // 16-0379
   enPARAM16__EXP39_Inputs_505,               // 16-0380
   enPARAM16__EXP39_Inputs_506,               // 16-0381
   enPARAM16__EXP39_Inputs_507,               // 16-0382
   enPARAM16__EXP39_Inputs_508,               // 16-0383
   enPARAM16__EXP40_Inputs_501,               // 16-0384
   enPARAM16__EXP40_Inputs_502,               // 16-0385
   enPARAM16__EXP40_Inputs_503,               // 16-0386
   enPARAM16__EXP40_Inputs_504,               // 16-0387
   enPARAM16__EXP40_Inputs_505,               // 16-0388
   enPARAM16__EXP40_Inputs_506,               // 16-0389
   enPARAM16__EXP40_Inputs_507,               // 16-0390
   enPARAM16__EXP40_Inputs_508,               // 16-0391
   enPARAM16__MR_Outputs_601,                 // 16-0392
   enPARAM16__MR_Outputs_602,                 // 16-0393
   enPARAM16__MR_Outputs_603,                 // 16-0394
   enPARAM16__MR_Outputs_604,                 // 16-0395
   enPARAM16__MR_Outputs_605,                 // 16-0396
   enPARAM16__MR_Outputs_606,                 // 16-0397
   enPARAM16__MR_Outputs_607,                 // 16-0398
   enPARAM16__MR_Outputs_608,                 // 16-0399
   enPARAM16__CT_Outputs_601,                 // 16-0400
   enPARAM16__CT_Outputs_602,                 // 16-0401
   enPARAM16__CT_Outputs_603,                 // 16-0402
   enPARAM16__CT_Outputs_604,                 // 16-0403
   enPARAM16__CT_Outputs_605,                 // 16-0404
   enPARAM16__CT_Outputs_606,                 // 16-0405
   enPARAM16__CT_Outputs_607,                 // 16-0406
   enPARAM16__CT_Outputs_608,                 // 16-0407
   enPARAM16__CT_Outputs_609,                 // 16-0408
   enPARAM16__CT_Outputs_610,                 // 16-0409
   enPARAM16__CT_Outputs_611,                 // 16-0410
   enPARAM16__CT_Outputs_612,                 // 16-0411
   enPARAM16__CT_Outputs_613,                 // 16-0412
   enPARAM16__CT_Outputs_614,                 // 16-0413
   enPARAM16__CT_Outputs_615,                 // 16-0414
   enPARAM16__CT_Outputs_616,                 // 16-0415
   enPARAM16__COP_Outputs_601,                // 16-0416
   enPARAM16__COP_Outputs_602,                // 16-0417
   enPARAM16__COP_Outputs_603,                // 16-0418
   enPARAM16__COP_Outputs_604,                // 16-0419
   enPARAM16__COP_Outputs_605,                // 16-0420
   enPARAM16__COP_Outputs_606,                // 16-0421
   enPARAM16__COP_Outputs_607,                // 16-0422
   enPARAM16__COP_Outputs_608,                // 16-0423
   enPARAM16__COP_Outputs_609,                // 16-0424
   enPARAM16__COP_Outputs_610,                // 16-0425
   enPARAM16__COP_Outputs_611,                // 16-0426
   enPARAM16__COP_Outputs_612,                // 16-0427
   enPARAM16__COP_Outputs_613,                // 16-0428
   enPARAM16__COP_Outputs_614,                // 16-0429
   enPARAM16__COP_Outputs_615,                // 16-0430
   enPARAM16__COP_Outputs_616,                // 16-0431
   enPARAM16__RIS1_Outputs_601,               // 16-0432
   enPARAM16__RIS1_Outputs_602,               // 16-0433
   enPARAM16__RIS1_Outputs_603,               // 16-0434
   enPARAM16__RIS1_Outputs_604,               // 16-0435
   enPARAM16__RIS1_Outputs_605,               // 16-0436
   enPARAM16__RIS1_Outputs_606,               // 16-0437
   enPARAM16__RIS1_Outputs_607,               // 16-0438
   enPARAM16__RIS1_Outputs_608,               // 16-0439
   enPARAM16__RIS2_Outputs_601,               // 16-0440
   enPARAM16__RIS2_Outputs_602,               // 16-0441
   enPARAM16__RIS2_Outputs_603,               // 16-0442
   enPARAM16__RIS2_Outputs_604,               // 16-0443
   enPARAM16__RIS2_Outputs_605,               // 16-0444
   enPARAM16__RIS2_Outputs_606,               // 16-0445
   enPARAM16__RIS2_Outputs_607,               // 16-0446
   enPARAM16__RIS2_Outputs_608,               // 16-0447
   enPARAM16__RIS3_Outputs_601,               // 16-0448
   enPARAM16__RIS3_Outputs_602,               // 16-0449
   enPARAM16__RIS3_Outputs_603,               // 16-0450
   enPARAM16__RIS3_Outputs_604,               // 16-0451
   enPARAM16__RIS3_Outputs_605,               // 16-0452
   enPARAM16__RIS3_Outputs_606,               // 16-0453
   enPARAM16__RIS3_Outputs_607,               // 16-0454
   enPARAM16__RIS3_Outputs_608,               // 16-0455
   enPARAM16__RIS4_Outputs_601,               // 16-0456
   enPARAM16__RIS4_Outputs_602,               // 16-0457
   enPARAM16__RIS4_Outputs_603,               // 16-0458
   enPARAM16__RIS4_Outputs_604,               // 16-0459
   enPARAM16__RIS4_Outputs_605,               // 16-0460
   enPARAM16__RIS4_Outputs_606,               // 16-0461
   enPARAM16__RIS4_Outputs_607,               // 16-0462
   enPARAM16__RIS4_Outputs_608,               // 16-0463
   enPARAM16__EXP01_Outputs_601,              // 16-0464
   enPARAM16__EXP01_Outputs_602,              // 16-0465
   enPARAM16__EXP01_Outputs_603,              // 16-0466
   enPARAM16__EXP01_Outputs_604,              // 16-0467
   enPARAM16__EXP01_Outputs_605,              // 16-0468
   enPARAM16__EXP01_Outputs_606,              // 16-0469
   enPARAM16__EXP01_Outputs_607,              // 16-0470
   enPARAM16__EXP01_Outputs_608,              // 16-0471
   enPARAM16__EXP02_Outputs_601,              // 16-0472
   enPARAM16__EXP02_Outputs_602,              // 16-0473
   enPARAM16__EXP02_Outputs_603,              // 16-0474
   enPARAM16__EXP02_Outputs_604,              // 16-0475
   enPARAM16__EXP02_Outputs_605,              // 16-0476
   enPARAM16__EXP02_Outputs_606,              // 16-0477
   enPARAM16__EXP02_Outputs_607,              // 16-0478
   enPARAM16__EXP02_Outputs_608,              // 16-0479
   enPARAM16__EXP03_Outputs_601,              // 16-0480
   enPARAM16__EXP03_Outputs_602,              // 16-0481
   enPARAM16__EXP03_Outputs_603,              // 16-0482
   enPARAM16__EXP03_Outputs_604,              // 16-0483
   enPARAM16__EXP03_Outputs_605,              // 16-0484
   enPARAM16__EXP03_Outputs_606,              // 16-0485
   enPARAM16__EXP03_Outputs_607,              // 16-0486
   enPARAM16__EXP03_Outputs_608,              // 16-0487
   enPARAM16__EXP04_Outputs_601,              // 16-0488
   enPARAM16__EXP04_Outputs_602,              // 16-0489
   enPARAM16__EXP04_Outputs_603,              // 16-0490
   enPARAM16__EXP04_Outputs_604,              // 16-0491
   enPARAM16__EXP04_Outputs_605,              // 16-0492
   enPARAM16__EXP04_Outputs_606,              // 16-0493
   enPARAM16__EXP04_Outputs_607,              // 16-0494
   enPARAM16__EXP04_Outputs_608,              // 16-0495
   enPARAM16__EXP05_Outputs_601,              // 16-0496
   enPARAM16__EXP05_Outputs_602,              // 16-0497
   enPARAM16__EXP05_Outputs_603,              // 16-0498
   enPARAM16__EXP05_Outputs_604,              // 16-0499
   enPARAM16__EXP05_Outputs_605,              // 16-0500
   enPARAM16__EXP05_Outputs_606,              // 16-0501
   enPARAM16__EXP05_Outputs_607,              // 16-0502
   enPARAM16__EXP05_Outputs_608,              // 16-0503
   enPARAM16__EXP06_Outputs_601,              // 16-0504
   enPARAM16__EXP06_Outputs_602,              // 16-0505
   enPARAM16__EXP06_Outputs_603,              // 16-0506
   enPARAM16__EXP06_Outputs_604,              // 16-0507
   enPARAM16__EXP06_Outputs_605,              // 16-0508
   enPARAM16__EXP06_Outputs_606,              // 16-0509
   enPARAM16__EXP06_Outputs_607,              // 16-0510
   enPARAM16__EXP06_Outputs_608,              // 16-0511
   enPARAM16__EXP07_Outputs_601,              // 16-0512
   enPARAM16__EXP07_Outputs_602,              // 16-0513
   enPARAM16__EXP07_Outputs_603,              // 16-0514
   enPARAM16__EXP07_Outputs_604,              // 16-0515
   enPARAM16__EXP07_Outputs_605,              // 16-0516
   enPARAM16__EXP07_Outputs_606,              // 16-0517
   enPARAM16__EXP07_Outputs_607,              // 16-0518
   enPARAM16__EXP07_Outputs_608,              // 16-0519
   enPARAM16__EXP08_Outputs_601,              // 16-0520
   enPARAM16__EXP08_Outputs_602,              // 16-0521
   enPARAM16__EXP08_Outputs_603,              // 16-0522
   enPARAM16__EXP08_Outputs_604,              // 16-0523
   enPARAM16__EXP08_Outputs_605,              // 16-0524
   enPARAM16__EXP08_Outputs_606,              // 16-0525
   enPARAM16__EXP08_Outputs_607,              // 16-0526
   enPARAM16__EXP08_Outputs_608,              // 16-0527
   enPARAM16__EXP09_Outputs_601,              // 16-0528
   enPARAM16__EXP09_Outputs_602,              // 16-0529
   enPARAM16__EXP09_Outputs_603,              // 16-0530
   enPARAM16__EXP09_Outputs_604,              // 16-0531
   enPARAM16__EXP09_Outputs_605,              // 16-0532
   enPARAM16__EXP09_Outputs_606,              // 16-0533
   enPARAM16__EXP09_Outputs_607,              // 16-0534
   enPARAM16__EXP09_Outputs_608,              // 16-0535
   enPARAM16__EXP10_Outputs_601,              // 16-0536
   enPARAM16__EXP10_Outputs_602,              // 16-0537
   enPARAM16__EXP10_Outputs_603,              // 16-0538
   enPARAM16__EXP10_Outputs_604,              // 16-0539
   enPARAM16__EXP10_Outputs_605,              // 16-0540
   enPARAM16__EXP10_Outputs_606,              // 16-0541
   enPARAM16__EXP10_Outputs_607,              // 16-0542
   enPARAM16__EXP10_Outputs_608,              // 16-0543
   enPARAM16__EXP11_Outputs_601,              // 16-0544
   enPARAM16__EXP11_Outputs_602,              // 16-0545
   enPARAM16__EXP11_Outputs_603,              // 16-0546
   enPARAM16__EXP11_Outputs_604,              // 16-0547
   enPARAM16__EXP11_Outputs_605,              // 16-0548
   enPARAM16__EXP11_Outputs_606,              // 16-0549
   enPARAM16__EXP11_Outputs_607,              // 16-0550
   enPARAM16__EXP11_Outputs_608,              // 16-0551
   enPARAM16__EXP12_Outputs_601,              // 16-0552
   enPARAM16__EXP12_Outputs_602,              // 16-0553
   enPARAM16__EXP12_Outputs_603,              // 16-0554
   enPARAM16__EXP12_Outputs_604,              // 16-0555
   enPARAM16__EXP12_Outputs_605,              // 16-0556
   enPARAM16__EXP12_Outputs_606,              // 16-0557
   enPARAM16__EXP12_Outputs_607,              // 16-0558
   enPARAM16__EXP12_Outputs_608,              // 16-0559
   enPARAM16__EXP13_Outputs_601,              // 16-0560
   enPARAM16__EXP13_Outputs_602,              // 16-0561
   enPARAM16__EXP13_Outputs_603,              // 16-0562
   enPARAM16__EXP13_Outputs_604,              // 16-0563
   enPARAM16__EXP13_Outputs_605,              // 16-0564
   enPARAM16__EXP13_Outputs_606,              // 16-0565
   enPARAM16__EXP13_Outputs_607,              // 16-0566
   enPARAM16__EXP13_Outputs_608,              // 16-0567
   enPARAM16__EXP14_Outputs_601,              // 16-0568
   enPARAM16__EXP14_Outputs_602,              // 16-0569
   enPARAM16__EXP14_Outputs_603,              // 16-0570
   enPARAM16__EXP14_Outputs_604,              // 16-0571
   enPARAM16__EXP14_Outputs_605,              // 16-0572
   enPARAM16__EXP14_Outputs_606,              // 16-0573
   enPARAM16__EXP14_Outputs_607,              // 16-0574
   enPARAM16__EXP14_Outputs_608,              // 16-0575
   enPARAM16__EXP15_Outputs_601,              // 16-0576
   enPARAM16__EXP15_Outputs_602,              // 16-0577
   enPARAM16__EXP15_Outputs_603,              // 16-0578
   enPARAM16__EXP15_Outputs_604,              // 16-0579
   enPARAM16__EXP15_Outputs_605,              // 16-0580
   enPARAM16__EXP15_Outputs_606,              // 16-0581
   enPARAM16__EXP15_Outputs_607,              // 16-0582
   enPARAM16__EXP15_Outputs_608,              // 16-0583
   enPARAM16__EXP16_Outputs_601,              // 16-0584
   enPARAM16__EXP16_Outputs_602,              // 16-0585
   enPARAM16__EXP16_Outputs_603,              // 16-0586
   enPARAM16__EXP16_Outputs_604,              // 16-0587
   enPARAM16__EXP16_Outputs_605,              // 16-0588
   enPARAM16__EXP16_Outputs_606,              // 16-0589
   enPARAM16__EXP16_Outputs_607,              // 16-0590
   enPARAM16__EXP16_Outputs_608,              // 16-0591
   enPARAM16__EXP17_Outputs_601,              // 16-0592
   enPARAM16__EXP17_Outputs_602,              // 16-0593
   enPARAM16__EXP17_Outputs_603,              // 16-0594
   enPARAM16__EXP17_Outputs_604,              // 16-0595
   enPARAM16__EXP17_Outputs_605,              // 16-0596
   enPARAM16__EXP17_Outputs_606,              // 16-0597
   enPARAM16__EXP17_Outputs_607,              // 16-0598
   enPARAM16__EXP17_Outputs_608,              // 16-0599
   enPARAM16__EXP18_Outputs_601,              // 16-0600
   enPARAM16__EXP18_Outputs_602,              // 16-0601
   enPARAM16__EXP18_Outputs_603,              // 16-0602
   enPARAM16__EXP18_Outputs_604,              // 16-0603
   enPARAM16__EXP18_Outputs_605,              // 16-0604
   enPARAM16__EXP18_Outputs_606,              // 16-0605
   enPARAM16__EXP18_Outputs_607,              // 16-0606
   enPARAM16__EXP18_Outputs_608,              // 16-0607
   enPARAM16__EXP19_Outputs_601,              // 16-0608
   enPARAM16__EXP19_Outputs_602,              // 16-0609
   enPARAM16__EXP19_Outputs_603,              // 16-0610
   enPARAM16__EXP19_Outputs_604,              // 16-0611
   enPARAM16__EXP19_Outputs_605,              // 16-0612
   enPARAM16__EXP19_Outputs_606,              // 16-0613
   enPARAM16__EXP19_Outputs_607,              // 16-0614
   enPARAM16__EXP19_Outputs_608,              // 16-0615
   enPARAM16__EXP20_Outputs_601,              // 16-0616
   enPARAM16__EXP20_Outputs_602,              // 16-0617
   enPARAM16__EXP20_Outputs_603,              // 16-0618
   enPARAM16__EXP20_Outputs_604,              // 16-0619
   enPARAM16__EXP20_Outputs_605,              // 16-0620
   enPARAM16__EXP20_Outputs_606,              // 16-0621
   enPARAM16__EXP20_Outputs_607,              // 16-0622
   enPARAM16__EXP20_Outputs_608,              // 16-0623
   enPARAM16__EXP21_Outputs_601,              // 16-0624
   enPARAM16__EXP21_Outputs_602,              // 16-0625
   enPARAM16__EXP21_Outputs_603,              // 16-0626
   enPARAM16__EXP21_Outputs_604,              // 16-0627
   enPARAM16__EXP21_Outputs_605,              // 16-0628
   enPARAM16__EXP21_Outputs_606,              // 16-0629
   enPARAM16__EXP21_Outputs_607,              // 16-0630
   enPARAM16__EXP21_Outputs_608,              // 16-0631
   enPARAM16__EXP22_Outputs_601,              // 16-0632
   enPARAM16__EXP22_Outputs_602,              // 16-0633
   enPARAM16__EXP22_Outputs_603,              // 16-0634
   enPARAM16__EXP22_Outputs_604,              // 16-0635
   enPARAM16__EXP22_Outputs_605,              // 16-0636
   enPARAM16__EXP22_Outputs_606,              // 16-0637
   enPARAM16__EXP22_Outputs_607,              // 16-0638
   enPARAM16__EXP22_Outputs_608,              // 16-0639
   enPARAM16__EXP23_Outputs_601,              // 16-0640
   enPARAM16__EXP23_Outputs_602,              // 16-0641
   enPARAM16__EXP23_Outputs_603,              // 16-0642
   enPARAM16__EXP23_Outputs_604,              // 16-0643
   enPARAM16__EXP23_Outputs_605,              // 16-0644
   enPARAM16__EXP23_Outputs_606,              // 16-0645
   enPARAM16__EXP23_Outputs_607,              // 16-0646
   enPARAM16__EXP23_Outputs_608,              // 16-0647
   enPARAM16__EXP24_Outputs_601,              // 16-0648
   enPARAM16__EXP24_Outputs_602,              // 16-0649
   enPARAM16__EXP24_Outputs_603,              // 16-0650
   enPARAM16__EXP24_Outputs_604,              // 16-0651
   enPARAM16__EXP24_Outputs_605,              // 16-0652
   enPARAM16__EXP24_Outputs_606,              // 16-0653
   enPARAM16__EXP24_Outputs_607,              // 16-0654
   enPARAM16__EXP24_Outputs_608,              // 16-0655
   enPARAM16__EXP25_Outputs_601,              // 16-0656
   enPARAM16__EXP25_Outputs_602,              // 16-0657
   enPARAM16__EXP25_Outputs_603,              // 16-0658
   enPARAM16__EXP25_Outputs_604,              // 16-0659
   enPARAM16__EXP25_Outputs_605,              // 16-0660
   enPARAM16__EXP25_Outputs_606,              // 16-0661
   enPARAM16__EXP25_Outputs_607,              // 16-0662
   enPARAM16__EXP25_Outputs_608,              // 16-0663
   enPARAM16__EXP26_Outputs_601,              // 16-0664
   enPARAM16__EXP26_Outputs_602,              // 16-0665
   enPARAM16__EXP26_Outputs_603,              // 16-0666
   enPARAM16__EXP26_Outputs_604,              // 16-0667
   enPARAM16__EXP26_Outputs_605,              // 16-0668
   enPARAM16__EXP26_Outputs_606,              // 16-0669
   enPARAM16__EXP26_Outputs_607,              // 16-0670
   enPARAM16__EXP26_Outputs_608,              // 16-0671
   enPARAM16__EXP27_Outputs_601,              // 16-0672
   enPARAM16__EXP27_Outputs_602,              // 16-0673
   enPARAM16__EXP27_Outputs_603,              // 16-0674
   enPARAM16__EXP27_Outputs_604,              // 16-0675
   enPARAM16__EXP27_Outputs_605,              // 16-0676
   enPARAM16__EXP27_Outputs_606,              // 16-0677
   enPARAM16__EXP27_Outputs_607,              // 16-0678
   enPARAM16__EXP27_Outputs_608,              // 16-0679
   enPARAM16__EXP28_Outputs_601,              // 16-0680
   enPARAM16__EXP28_Outputs_602,              // 16-0681
   enPARAM16__EXP28_Outputs_603,              // 16-0682
   enPARAM16__EXP28_Outputs_604,              // 16-0683
   enPARAM16__EXP28_Outputs_605,              // 16-0684
   enPARAM16__EXP28_Outputs_606,              // 16-0685
   enPARAM16__EXP28_Outputs_607,              // 16-0686
   enPARAM16__EXP28_Outputs_608,              // 16-0687
   enPARAM16__EXP29_Outputs_601,              // 16-0688
   enPARAM16__EXP29_Outputs_602,              // 16-0689
   enPARAM16__EXP29_Outputs_603,              // 16-0690
   enPARAM16__EXP29_Outputs_604,              // 16-0691
   enPARAM16__EXP29_Outputs_605,              // 16-0692
   enPARAM16__EXP29_Outputs_606,              // 16-0693
   enPARAM16__EXP29_Outputs_607,              // 16-0694
   enPARAM16__EXP29_Outputs_608,              // 16-0695
   enPARAM16__EXP30_Outputs_601,              // 16-0696
   enPARAM16__EXP30_Outputs_602,              // 16-0697
   enPARAM16__EXP30_Outputs_603,              // 16-0698
   enPARAM16__EXP30_Outputs_604,              // 16-0699
   enPARAM16__EXP30_Outputs_605,              // 16-0700
   enPARAM16__EXP30_Outputs_606,              // 16-0701
   enPARAM16__EXP30_Outputs_607,              // 16-0702
   enPARAM16__EXP30_Outputs_608,              // 16-0703
   enPARAM16__EXP31_Outputs_601,              // 16-0704
   enPARAM16__EXP31_Outputs_602,              // 16-0705
   enPARAM16__EXP31_Outputs_603,              // 16-0706
   enPARAM16__EXP31_Outputs_604,              // 16-0707
   enPARAM16__EXP31_Outputs_605,              // 16-0708
   enPARAM16__EXP31_Outputs_606,              // 16-0709
   enPARAM16__EXP31_Outputs_607,              // 16-0710
   enPARAM16__EXP31_Outputs_608,              // 16-0711
   enPARAM16__EXP32_Outputs_601,              // 16-0712
   enPARAM16__EXP32_Outputs_602,              // 16-0713
   enPARAM16__EXP32_Outputs_603,              // 16-0714
   enPARAM16__EXP32_Outputs_604,              // 16-0715
   enPARAM16__EXP32_Outputs_605,              // 16-0716
   enPARAM16__EXP32_Outputs_606,              // 16-0717
   enPARAM16__EXP32_Outputs_607,              // 16-0718
   enPARAM16__EXP32_Outputs_608,              // 16-0719
   enPARAM16__EXP33_Outputs_601,              // 16-0720
   enPARAM16__EXP33_Outputs_602,              // 16-0721
   enPARAM16__EXP33_Outputs_603,              // 16-0722
   enPARAM16__EXP33_Outputs_604,              // 16-0723
   enPARAM16__EXP33_Outputs_605,              // 16-0724
   enPARAM16__EXP33_Outputs_606,              // 16-0725
   enPARAM16__EXP33_Outputs_607,              // 16-0726
   enPARAM16__EXP33_Outputs_608,              // 16-0727
   enPARAM16__EXP34_Outputs_601,              // 16-0728
   enPARAM16__EXP34_Outputs_602,              // 16-0729
   enPARAM16__EXP34_Outputs_603,              // 16-0730
   enPARAM16__EXP34_Outputs_604,              // 16-0731
   enPARAM16__EXP34_Outputs_605,              // 16-0732
   enPARAM16__EXP34_Outputs_606,              // 16-0733
   enPARAM16__EXP34_Outputs_607,              // 16-0734
   enPARAM16__EXP34_Outputs_608,              // 16-0735
   enPARAM16__EXP35_Outputs_601,              // 16-0736
   enPARAM16__EXP35_Outputs_602,              // 16-0737
   enPARAM16__EXP35_Outputs_603,              // 16-0738
   enPARAM16__EXP35_Outputs_604,              // 16-0739
   enPARAM16__EXP35_Outputs_605,              // 16-0740
   enPARAM16__EXP35_Outputs_606,              // 16-0741
   enPARAM16__EXP35_Outputs_607,              // 16-0742
   enPARAM16__EXP35_Outputs_608,              // 16-0743
   enPARAM16__EXP36_Outputs_601,              // 16-0744
   enPARAM16__EXP36_Outputs_602,              // 16-0745
   enPARAM16__EXP36_Outputs_603,              // 16-0746
   enPARAM16__EXP36_Outputs_604,              // 16-0747
   enPARAM16__EXP36_Outputs_605,              // 16-0748
   enPARAM16__EXP36_Outputs_606,              // 16-0749
   enPARAM16__EXP36_Outputs_607,              // 16-0750
   enPARAM16__EXP36_Outputs_608,              // 16-0751
   enPARAM16__EXP37_Outputs_601,              // 16-0752
   enPARAM16__EXP37_Outputs_602,              // 16-0753
   enPARAM16__EXP37_Outputs_603,              // 16-0754
   enPARAM16__EXP37_Outputs_604,              // 16-0755
   enPARAM16__EXP37_Outputs_605,              // 16-0756
   enPARAM16__EXP37_Outputs_606,              // 16-0757
   enPARAM16__EXP37_Outputs_607,              // 16-0758
   enPARAM16__EXP37_Outputs_608,              // 16-0759
   enPARAM16__EXP38_Outputs_601,              // 16-0760
   enPARAM16__EXP38_Outputs_602,              // 16-0761
   enPARAM16__EXP38_Outputs_603,              // 16-0762
   enPARAM16__EXP38_Outputs_604,              // 16-0763
   enPARAM16__EXP38_Outputs_605,              // 16-0764
   enPARAM16__EXP38_Outputs_606,              // 16-0765
   enPARAM16__EXP38_Outputs_607,              // 16-0766
   enPARAM16__EXP38_Outputs_608,              // 16-0767
   enPARAM16__EXP39_Outputs_601,              // 16-0768
   enPARAM16__EXP39_Outputs_602,              // 16-0769
   enPARAM16__EXP39_Outputs_603,              // 16-0770
   enPARAM16__EXP39_Outputs_604,              // 16-0771
   enPARAM16__EXP39_Outputs_605,              // 16-0772
   enPARAM16__EXP39_Outputs_606,              // 16-0773
   enPARAM16__EXP39_Outputs_607,              // 16-0774
   enPARAM16__EXP39_Outputs_608,              // 16-0775
   enPARAM16__EXP40_Outputs_601,              // 16-0776
   enPARAM16__EXP40_Outputs_602,              // 16-0777
   enPARAM16__EXP40_Outputs_603,              // 16-0778
   enPARAM16__EXP40_Outputs_604,              // 16-0779
   enPARAM16__EXP40_Outputs_605,              // 16-0780
   enPARAM16__EXP40_Outputs_606,              // 16-0781
   enPARAM16__EXP40_Outputs_607,              // 16-0782
   enPARAM16__EXP40_Outputs_608,              // 16-0783
   enPARAM16__NTS_VEL_P1_0,                   // 16-0784
   enPARAM16__NTS_VEL_P1_1,                   // 16-0785
   enPARAM16__NTS_VEL_P1_2,                   // 16-0786
   enPARAM16__NTS_VEL_P1_3,                   // 16-0787
   enPARAM16__NTS_VEL_P1_4,                   // 16-0788
   enPARAM16__NTS_VEL_P1_5,                   // 16-0789
   enPARAM16__NTS_VEL_P1_6,                   // 16-0790
   enPARAM16__NTS_VEL_P1_7,                   // 16-0791
   enPARAM16__NTS_VEL_P2_0,                   // 16-0792
   enPARAM16__NTS_VEL_P2_1,                   // 16-0793
   enPARAM16__NTS_VEL_P2_2,                   // 16-0794
   enPARAM16__NTS_VEL_P2_3,                   // 16-0795
   enPARAM16__NTS_VEL_P2_4,                   // 16-0796
   enPARAM16__NTS_VEL_P2_5,                   // 16-0797
   enPARAM16__NTS_VEL_P2_6,                   // 16-0798
   enPARAM16__NTS_VEL_P2_7,                   // 16-0799
   enPARAM16__NTS_VEL_P3_0,                   // 16-0800
   enPARAM16__NTS_VEL_P3_1,                   // 16-0801
   enPARAM16__NTS_VEL_P3_2,                   // 16-0802
   enPARAM16__NTS_VEL_P3_3,                   // 16-0803
   enPARAM16__NTS_VEL_P3_4,                   // 16-0804
   enPARAM16__NTS_VEL_P3_5,                   // 16-0805
   enPARAM16__NTS_VEL_P3_6,                   // 16-0806
   enPARAM16__NTS_VEL_P3_7,                   // 16-0807
   enPARAM16__NTS_VEL_P4_0,                   // 16-0808
   enPARAM16__NTS_VEL_P4_1,                   // 16-0809
   enPARAM16__NTS_VEL_P4_2,                   // 16-0810
   enPARAM16__NTS_VEL_P4_3,                   // 16-0811
   enPARAM16__NTS_VEL_P4_4,                   // 16-0812
   enPARAM16__NTS_VEL_P4_5,                   // 16-0813
   enPARAM16__NTS_VEL_P4_6,                   // 16-0814
   enPARAM16__NTS_VEL_P4_7,                   // 16-0815
   enPARAM16__NTS_POS_P1_0,                   // 16-0816
   enPARAM16__NTS_POS_P1_1,                   // 16-0817
   enPARAM16__NTS_POS_P1_2,                   // 16-0818
   enPARAM16__NTS_POS_P1_3,                   // 16-0819
   enPARAM16__NTS_POS_P1_4,                   // 16-0820
   enPARAM16__NTS_POS_P1_5,                   // 16-0821
   enPARAM16__NTS_POS_P1_6,                   // 16-0822
   enPARAM16__NTS_POS_P1_7,                   // 16-0823
   enPARAM16__NTS_POS_P2_0,                   // 16-0824
   enPARAM16__NTS_POS_P2_1,                   // 16-0825
   enPARAM16__NTS_POS_P2_2,                   // 16-0826
   enPARAM16__NTS_POS_P2_3,                   // 16-0827
   enPARAM16__NTS_POS_P2_4,                   // 16-0828
   enPARAM16__NTS_POS_P2_5,                   // 16-0829
   enPARAM16__NTS_POS_P2_6,                   // 16-0830
   enPARAM16__NTS_POS_P2_7,                   // 16-0831
   enPARAM16__NTS_POS_P3_0,                   // 16-0832
   enPARAM16__NTS_POS_P3_1,                   // 16-0833
   enPARAM16__NTS_POS_P3_2,                   // 16-0834
   enPARAM16__NTS_POS_P3_3,                   // 16-0835
   enPARAM16__NTS_POS_P3_4,                   // 16-0836
   enPARAM16__NTS_POS_P3_5,                   // 16-0837
   enPARAM16__NTS_POS_P3_6,                   // 16-0838
   enPARAM16__NTS_POS_P3_7,                   // 16-0839
   enPARAM16__NTS_POS_P4_0,                   // 16-0840
   enPARAM16__NTS_POS_P4_1,                   // 16-0841
   enPARAM16__NTS_POS_P4_2,                   // 16-0842
   enPARAM16__NTS_POS_P4_3,                   // 16-0843
   enPARAM16__NTS_POS_P4_4,                   // 16-0844
   enPARAM16__NTS_POS_P4_5,                   // 16-0845
   enPARAM16__NTS_POS_P4_6,                   // 16-0846
   enPARAM16__NTS_POS_P4_7,                   // 16-0847
   enPARAM16__UNUSED_16BIT_848,               // 16-0848
   enPARAM16__UNUSED_16BIT_849,               // 16-0849
   enPARAM16__UNUSED_16BIT_850,               // 16-0850
   enPARAM16__UNUSED_16BIT_851,               // 16-0851
   enPARAM16__UNUSED_16BIT_852,               // 16-0852
   enPARAM16__UNUSED_16BIT_853,               // 16-0853
   enPARAM16__UNUSED_16BIT_854,               // 16-0854
   enPARAM16__UNUSED_16BIT_855,               // 16-0855
   enPARAM16__UNUSED_16BIT_856,               // 16-0856
   enPARAM16__UNUSED_16BIT_857,               // 16-0857
   enPARAM16__UNUSED_16BIT_858,               // 16-0858
   enPARAM16__UNUSED_16BIT_859,               // 16-0859
   enPARAM16__UNUSED_16BIT_860,               // 16-0860
   enPARAM16__UNUSED_16BIT_861,               // 16-0861
   enPARAM16__Acceptance_AscOrDescSpeed,      // 16-0862
   enPARAM16__UNUSED_16BIT_863,               // 16-0863
   enPARAM16__Acceptance_BufferSpeed,         // 16-0864
   enPARAM16__Acceptance_SlideDistance,       // 16-0865
   enPARAM16__Acceptance_EBrk_SlideDistance,  // 16-0866
   enPARAM16__UNUSED_16BIT_867,               // 16-0867
   enPARAM16__UNUSED_16BIT_868,               // 16-0868
   enPARAM16__UNUSED_16BIT_869,               // 16-0869
   enPARAM16__UNUSED_16BIT_870,               // 16-0870
   enPARAM16__UNUSED_16BIT_871,               // 16-0871
   enPARAM16__ContractSpeed,                  // 16-0872
   enPARAM16__InspectionSpeed,                // 16-0873
   enPARAM16__LearnSpeed,                     // 16-0874
   enPARAM16__InspectionTerminalSpeed,        // 16-0875
   enPARAM16__LockClipTime_10ms,              // 16-0876
   enPARAM16__MinAccelSpeed,                  // 16-0877
   enPARAM16__EPowerSpeed_fpm,                // 16-0878
   enPARAM16__UNUSED_16BIT_879,               // 16-0879
   enPARAM16__BrakePickDelay_Insp_ms,         // 16-0880
   enPARAM16__BrakePickDelay_Auto_ms,         // 16-0881
   enPARAM16__AccelDelay_ms,                  // 16-0882
   enPARAM16__AccelDelay_Insp_ms,             // 16-0883
   enPARAM16__UNUSED_16BIT_884,               // 16-0884
   enPARAM16__BrakeDropDelay_Auto_1ms,        // 16-0885
   enPARAM16__BrakeDropDelay_Insp_1ms,        // 16-0886
   enPARAM16__DriveDropDelay_Auto_1ms,        // 16-0887
   enPARAM16__DriveDropDelay_Insp_1ms,        // 16-0888
   enPARAM16__MotorDropDelay_Auto_1ms,        // 16-0889
   enPARAM16__MotorDropDelay_Insp_1ms,        // 16-0890
   enPARAM16__EBrakeDropDelay_Auto_1ms,       // 16-0891
   enPARAM16__EBrakeDropDelay_Insp_1ms,       // 16-0892
   enPARAM16__B2DropDelay_Auto_1ms,           // 16-0893
   enPARAM16__B2DropDelay_Insp_1ms,           // 16-0894
   enPARAM16__UNUSED_16BIT_895,               // 16-0895
   enPARAM16__UNUSED_16BIT_896,               // 16-0896
   enPARAM16__SoftLimitDistance_UP,           // 16-0897
   enPARAM16__SoftLimitDistance_DN,           // 16-0898
   enPARAM16__UNUSED_16BIT_899,               // 16-0899
   enPARAM16__UNUSED_16BIT_900,               // 16-0900
   enPARAM16__UNUSED_16BIT_901,               // 16-0901
   enPARAM16__SpeedDev_Threshold,             // 16-0902
   enPARAM16__SpeedDev_Timeout_10ms,          // 16-0903
   enPARAM16__SpeedDev_OffsetPercent,         // 16-0904
   enPARAM16__TractionLoss_Threshold,         // 16-0905
   enPARAM16__TractionLoss_Timeout_10ms,      // 16-0906
   enPARAM16__TractionLoss_OffsetPercent,     // 16-0907
   enPARAM16__LevelingSpeed,                  // 16-0908
   enPARAM16__UNUSED_16BIT_909,               // 16-0909
   enPARAM16__PreOpeningDistance,             // 16-0910
   enPARAM16__UNUSED_16BIT_911,               // 16-0911
   enPARAM16__UNUSED_16BIT_912,               // 16-0912
   enPARAM16__UNUSED_16BIT_913,               // 16-0913
   enPARAM16__UNUSED_16BIT_914,               // 16-0914
   enPARAM16__UNUSED_16BIT_915,               // 16-0915
   enPARAM16__UNUSED_16BIT_916,               // 16-0916
   enPARAM16__UNUSED_16BIT_917,               // 16-0917
   enPARAM16__UNUSED_16BIT_918,               // 16-0918
   enPARAM16__UNUSED_16BIT_919,               // 16-0919
   enPARAM16__UNUSED_16BIT_920,               // 16-0920
   enPARAM16__UNUSED_16BIT_921,               // 16-0921
   enPARAM16__UNUSED_16BIT_922,               // 16-0922
   enPARAM16__UNUSED_16BIT_923,               // 16-0923
   enPARAM16__Time_Violation_1ms,             // 16-0924
   enPARAM16__UNUSED_16BIT_925,               // 16-0925
   enPARAM16__ETSL_CameraOffset_05mm,         // 16-0926
   enPARAM16__BufferDistance_05mm,            // 16-0927
   enPARAM16__SecuredCheckInBitmap_F0,        // 16-0928
   enPARAM16__SecuredCheckInBitmap_F1,        // 16-0929
   enPARAM16__SecuredCheckInBitmap_F2,        // 16-0930
   enPARAM16__SecuredCheckInBitmap_F3,        // 16-0931
   enPARAM16__SecuredCheckInBitmap_F4,        // 16-0932
   enPARAM16__SecuredCheckInBitmap_F5,        // 16-0933
   enPARAM16__SecuredCheckInBitmap_R0,        // 16-0934
   enPARAM16__SecuredCheckInBitmap_R1,        // 16-0935
   enPARAM16__SecuredCheckInBitmap_R2,        // 16-0936
   enPARAM16__SecuredCheckInBitmap_R3,        // 16-0937
   enPARAM16__SecuredCheckInBitmap_R4,        // 16-0938
   enPARAM16__SecuredCheckInBitmap_R5,        // 16-0939
   enPARAM16__HallSecureMap_0,                // 16-0940
   enPARAM16__HallSecureMap_1,                // 16-0941
   enPARAM16__HallSecureMap_2,                // 16-0942
   enPARAM16__HallSecureMap_3,                // 16-0943
   enPARAM16__HallSecureMap_4,                // 16-0944
   enPARAM16__HallSecureMap_5,                // 16-0945
   enPARAM16__SwingDoorOpening_F_0,           // 16-0946
   enPARAM16__SwingDoorOpening_F_1,           // 16-0947
   enPARAM16__SwingDoorOpening_F_2,           // 16-0948
   enPARAM16__SwingDoorOpening_F_3,           // 16-0949
   enPARAM16__SwingDoorOpening_F_4,           // 16-0950
   enPARAM16__SwingDoorOpening_F_5,           // 16-0951
   enPARAM16__SwingDoorOpening_R_0,           // 16-0952
   enPARAM16__SwingDoorOpening_R_1,           // 16-0953
   enPARAM16__SwingDoorOpening_R_2,           // 16-0954
   enPARAM16__SwingDoorOpening_R_3,           // 16-0955
   enPARAM16__SwingDoorOpening_R_4,           // 16-0956
   enPARAM16__SwingDoorOpening_R_5,           // 16-0957
   enPARAM16__ShortFloorOpening_0,            // 16-0958
   enPARAM16__ShortFloorOpening_1,            // 16-0959
   enPARAM16__ShortFloorOpening_2,            // 16-0960
   enPARAM16__ShortFloorOpening_3,            // 16-0961
   enPARAM16__ShortFloorOpening_4,            // 16-0962
   enPARAM16__ShortFloorOpening_5,            // 16-0963
   enPARAM16__MedValveMaxRunDist_in,          // 16-0964
   enPARAM16__LowValveMaxRunDist_in,          // 16-0965
   enPARAM16__LevelValveMaxRunDist_in,        // 16-0966
   enPARAM16__Speed1_THOLD_fpm,               // 16-0967
   enPARAM16__Speed2_THOLD_fpm,               // 16-0968
   enPARAM16__Speed3_THOLD_fpm,               // 16-0969
   enPARAM16__Speed4_THOLD_fpm,               // 16-0970
   enPARAM16__Speed5_THOLD_fpm,               // 16-0971
   enPARAM16__Speed6_THOLD_fpm,               // 16-0972
   enPARAM16__Speed7_THOLD_fpm,               // 16-0973
   enPARAM16__Speed8_THOLD_fpm,               // 16-0974
   enPARAM16__Speed9_THOLD_fpm,               // 16-0975
   enPARAM16__Speed10_THOLD_fpm,              // 16-0976
   enPARAM16__Speed11_THOLD_fpm,              // 16-0977
   enPARAM16__Speed12_THOLD_fpm,              // 16-0978
   enPARAM16__Speed13_THOLD_fpm,              // 16-0979
   enPARAM16__Speed14_THOLD_fpm,              // 16-0980
   enPARAM16__Speed15_THOLD_fpm,              // 16-0981
   enPARAM16__UNUSED_16BIT_982,               // 16-0982
   enPARAM16__AccessCodeFloor_1F,             // 16-0983
   enPARAM16__AccessCodeFloor_2F,             // 16-0984
   enPARAM16__AccessCodeFloor_3F,             // 16-0985
   enPARAM16__AccessCodeFloor_4F,             // 16-0986
   enPARAM16__AccessCodeFloor_5F,             // 16-0987
   enPARAM16__AccessCodeFloor_6F,             // 16-0988
   enPARAM16__AccessCodeFloor_7F,             // 16-0989
   enPARAM16__AccessCodeFloor_8F,             // 16-0990
   enPARAM16__AccessCodeFloor_1R,             // 16-0991
   enPARAM16__AccessCodeFloor_2R,             // 16-0992
   enPARAM16__AccessCodeFloor_3R,             // 16-0993
   enPARAM16__AccessCodeFloor_4R,             // 16-0994
   enPARAM16__AccessCodeFloor_5R,             // 16-0995
   enPARAM16__AccessCodeFloor_6R,             // 16-0996
   enPARAM16__AccessCodeFloor_7R,             // 16-0997
   enPARAM16__AccessCodeFloor_8R,             // 16-0998
   enPARAM16__Weekday_StartTime,              // 16-0999
   enPARAM16__Weekday_EndTime,                // 16-1000
   enPARAM16__Weekend_StartTime,              // 16-1001
   enPARAM16__Weekend_EndTime,                // 16-1002
   enPARAM16__Speed1_SlowdownDist_UP_05mm,    // 16-1003
   enPARAM16__Speed2_SlowdownDist_UP_05mm,    // 16-1004
   enPARAM16__Speed3_SlowdownDist_UP_05mm,    // 16-1005
   enPARAM16__Speed4_SlowdownDist_UP_05mm,    // 16-1006
   enPARAM16__Speed5_SlowdownDist_UP_05mm,    // 16-1007
   enPARAM16__Speed6_SlowdownDist_UP_05mm,    // 16-1008
   enPARAM16__Speed7_SlowdownDist_UP_05mm,    // 16-1009
   enPARAM16__Speed8_SlowdownDist_UP_05mm,    // 16-1010
   enPARAM16__Speed9_SlowdownDist_UP_05mm,    // 16-1011
   enPARAM16__Speed10_SlowdownDist_UP_05mm,   // 16-1012
   enPARAM16__Speed11_SlowdownDist_UP_05mm,   // 16-1013
   enPARAM16__Speed12_SlowdownDist_UP_05mm,   // 16-1014
   enPARAM16__Speed13_SlowdownDist_UP_05mm,   // 16-1015
   enPARAM16__Speed14_SlowdownDist_UP_05mm,   // 16-1016
   enPARAM16__Speed15_SlowdownDist_UP_05mm,   // 16-1017
   enPARAM16__Speed16_SlowdownDist_UP_05mm,   // 16-1018
   enPARAM16__Speed1_SlowdownDist_DN_05mm,    // 16-1019
   enPARAM16__Speed2_SlowdownDist_DN_05mm,    // 16-1020
   enPARAM16__Speed3_SlowdownDist_DN_05mm,    // 16-1021
   enPARAM16__Speed4_SlowdownDist_DN_05mm,    // 16-1022
   enPARAM16__Speed5_SlowdownDist_DN_05mm,    // 16-1023
   enPARAM16__Speed6_SlowdownDist_DN_05mm,    // 16-1024
   enPARAM16__Speed7_SlowdownDist_DN_05mm,    // 16-1025
   enPARAM16__Speed8_SlowdownDist_DN_05mm,    // 16-1026
   enPARAM16__Speed9_SlowdownDist_DN_05mm,    // 16-1027
   enPARAM16__Speed10_SlowdownDist_DN_05mm,   // 16-1028
   enPARAM16__Speed11_SlowdownDist_DN_05mm,   // 16-1029
   enPARAM16__Speed12_SlowdownDist_DN_05mm,   // 16-1030
   enPARAM16__Speed13_SlowdownDist_DN_05mm,   // 16-1031
   enPARAM16__Speed14_SlowdownDist_DN_05mm,   // 16-1032
   enPARAM16__Speed15_SlowdownDist_DN_05mm,   // 16-1033
   enPARAM16__Speed16_SlowdownDist_DN_05mm,   // 16-1034
   enPARAM16__UNUSED_16BIT_1035,              // 16-1035
   enPARAM16__UNUSED_16BIT_1036,              // 16-1036
   enPARAM16__UNUSED_16BIT_1037,              // 16-1037
   enPARAM16__UNUSED_16BIT_1038,              // 16-1038
   enPARAM16__UNUSED_16BIT_1039,              // 16-1039
   enPARAM16__UNUSED_16BIT_1040,              // 16-1040
   enPARAM16__UNUSED_16BIT_1041,              // 16-1041
   enPARAM16__UNUSED_16BIT_1042,              // 16-1042
   enPARAM16__UNUSED_16BIT_1043,              // 16-1043
   enPARAM16__UNUSED_16BIT_1044,              // 16-1044
   enPARAM16__UNUSED_16BIT_1045,              // 16-1045
   enPARAM16__UNUSED_16BIT_1046,              // 16-1046
   enPARAM16__UNUSED_16BIT_1047,              // 16-1047
   enPARAM16__UNUSED_16BIT_1048,              // 16-1048
   enPARAM16__UNUSED_16BIT_1049,              // 16-1049
   enPARAM16__UNUSED_16BIT_1050,              // 16-1050
   enPARAM16__UNUSED_16BIT_1051,              // 16-1051
   enPARAM16__UNUSED_16BIT_1052,              // 16-1052
   enPARAM16__UNUSED_16BIT_1053,              // 16-1053
   enPARAM16__UNUSED_16BIT_1054,              // 16-1054
   enPARAM16__UNUSED_16BIT_1055,              // 16-1055
   enPARAM16__UNUSED_16BIT_1056,              // 16-1056
   enPARAM16__UNUSED_16BIT_1057,              // 16-1057
   enPARAM16__UNUSED_16BIT_1058,              // 16-1058
   enPARAM16__UNUSED_16BIT_1059,              // 16-1059
   enPARAM16__UNUSED_16BIT_1060,              // 16-1060
   enPARAM16__UNUSED_16BIT_1061,              // 16-1061
   enPARAM16__UNUSED_16BIT_1062,              // 16-1062
   enPARAM16__UNUSED_16BIT_1063,              // 16-1063
   enPARAM16__UNUSED_16BIT_1064,              // 16-1064
   enPARAM16__UNUSED_16BIT_1065,              // 16-1065
   enPARAM16__UNUSED_16BIT_1066,              // 16-1066
   enPARAM16__UNUSED_16BIT_1067,              // 16-1067
   enPARAM16__UNUSED_16BIT_1068,              // 16-1068
   enPARAM16__UNUSED_16BIT_1069,              // 16-1069
   enPARAM16__UNUSED_16BIT_1070,              // 16-1070
   enPARAM16__UNUSED_16BIT_1071,              // 16-1071
   enPARAM16__UNUSED_16BIT_1072,              // 16-1072
   enPARAM16__UNUSED_16BIT_1073,              // 16-1073
   enPARAM16__UNUSED_16BIT_1074,              // 16-1074
   enPARAM16__UNUSED_16BIT_1075,              // 16-1075
   enPARAM16__UNUSED_16BIT_1076,              // 16-1076
   enPARAM16__UNUSED_16BIT_1077,              // 16-1077
   enPARAM16__UNUSED_16BIT_1078,              // 16-1078
   enPARAM16__UNUSED_16BIT_1079,              // 16-1079
   enPARAM16__UNUSED_16BIT_1080,              // 16-1080
   enPARAM16__UNUSED_16BIT_1081,              // 16-1081
   enPARAM16__UNUSED_16BIT_1082,              // 16-1082
   enPARAM16__UNUSED_16BIT_1083,              // 16-1083
   enPARAM16__UNUSED_16BIT_1084,              // 16-1084
   enPARAM16__UNUSED_16BIT_1085,              // 16-1085
   enPARAM16__UNUSED_16BIT_1086,              // 16-1086
   enPARAM16__UNUSED_16BIT_1087,              // 16-1087
   enPARAM16__UNUSED_16BIT_1088,              // 16-1088
   enPARAM16__UNUSED_16BIT_1089,              // 16-1089
   enPARAM16__UNUSED_16BIT_1090,              // 16-1090
   enPARAM16__UNUSED_16BIT_1091,              // 16-1091
   enPARAM16__UNUSED_16BIT_1092,              // 16-1092
   enPARAM16__UNUSED_16BIT_1093,              // 16-1093
   enPARAM16__UNUSED_16BIT_1094,              // 16-1094
   enPARAM16__UNUSED_16BIT_1095,              // 16-1095
   enPARAM16__UNUSED_16BIT_1096,              // 16-1096
   enPARAM16__UNUSED_16BIT_1097,              // 16-1097
   enPARAM16__UNUSED_16BIT_1098,              // 16-1098
   enPARAM16__UNUSED_16BIT_1099,              // 16-1099
   enPARAM16__UNUSED_16BIT_1100,              // 16-1100
   enPARAM16__UNUSED_16BIT_1101,              // 16-1101
   enPARAM16__UNUSED_16BIT_1102,              // 16-1102
   enPARAM16__UNUSED_16BIT_1103,              // 16-1103
   enPARAM16__UNUSED_16BIT_1104,              // 16-1104
   enPARAM16__UNUSED_16BIT_1105,              // 16-1105
   enPARAM16__UNUSED_16BIT_1106,              // 16-1106
   enPARAM16__UNUSED_16BIT_1107,              // 16-1107
   enPARAM16__UNUSED_16BIT_1108,              // 16-1108
   enPARAM16__UNUSED_16BIT_1109,              // 16-1109
   enPARAM16__UNUSED_16BIT_1110,              // 16-1110
   enPARAM16__UNUSED_16BIT_1111,              // 16-1111
   enPARAM16__UNUSED_16BIT_1112,              // 16-1112
   enPARAM16__UNUSED_16BIT_1113,              // 16-1113
   enPARAM16__UNUSED_16BIT_1114,              // 16-1114
   enPARAM16__UNUSED_16BIT_1115,              // 16-1115
   enPARAM16__UNUSED_16BIT_1116,              // 16-1116
   enPARAM16__UNUSED_16BIT_1117,              // 16-1117
   enPARAM16__UNUSED_16BIT_1118,              // 16-1118
   enPARAM16__UNUSED_16BIT_1119,              // 16-1119
   enPARAM16__UNUSED_16BIT_1120,              // 16-1120
   enPARAM16__UNUSED_16BIT_1121,              // 16-1121
   enPARAM16__UNUSED_16BIT_1122,              // 16-1122
   enPARAM16__UNUSED_16BIT_1123,              // 16-1123
   enPARAM16__UNUSED_16BIT_1124,              // 16-1124
   enPARAM16__UNUSED_16BIT_1125,              // 16-1125
   enPARAM16__UNUSED_16BIT_1126,              // 16-1126
   enPARAM16__UNUSED_16BIT_1127,              // 16-1127
   enPARAM16__UNUSED_16BIT_1128,              // 16-1128
   enPARAM16__UNUSED_16BIT_1129,              // 16-1129
   enPARAM16__UNUSED_16BIT_1130,              // 16-1130
   enPARAM16__UNUSED_16BIT_1131,              // 16-1131
   enPARAM16__UNUSED_16BIT_1132,              // 16-1132
   enPARAM16__UNUSED_16BIT_1133,              // 16-1133
   enPARAM16__UNUSED_16BIT_1134,              // 16-1134
   enPARAM16__UNUSED_16BIT_1135,              // 16-1135
   enPARAM16__UNUSED_16BIT_1136,              // 16-1136
   enPARAM16__UNUSED_16BIT_1137,              // 16-1137
   enPARAM16__UNUSED_16BIT_1138,              // 16-1138
   enPARAM16__UNUSED_16BIT_1139,              // 16-1139
   enPARAM16__UNUSED_16BIT_1140,              // 16-1140
   enPARAM16__UNUSED_16BIT_1141,              // 16-1141
   enPARAM16__UNUSED_16BIT_1142,              // 16-1142
   enPARAM16__UNUSED_16BIT_1143,              // 16-1143
   enPARAM16__UNUSED_16BIT_1144,              // 16-1144
   enPARAM16__UNUSED_16BIT_1145,              // 16-1145
   enPARAM16__UNUSED_16BIT_1146,              // 16-1146
   enPARAM16__UNUSED_16BIT_1147,              // 16-1147
   enPARAM16__UNUSED_16BIT_1148,              // 16-1148
   enPARAM16__UNUSED_16BIT_1149,              // 16-1149
   enPARAM16__UNUSED_16BIT_1150,              // 16-1150
   enPARAM16__UNUSED_16BIT_1151,              // 16-1151
   enPARAM16__UNUSED_16BIT_1152,              // 16-1152
   enPARAM16__UNUSED_16BIT_1153,              // 16-1153
   enPARAM16__UNUSED_16BIT_1154,              // 16-1154
   enPARAM16__UNUSED_16BIT_1155,              // 16-1155
   enPARAM16__UNUSED_16BIT_1156,              // 16-1156
   enPARAM16__UNUSED_16BIT_1157,              // 16-1157
   enPARAM16__UNUSED_16BIT_1158,              // 16-1158
   enPARAM16__UNUSED_16BIT_1159,              // 16-1159

   NUM_16BIT_PARAMS
};

#define PARAM_STRINGS_16BIT(PARAM_STR_16BIT)  \
   PARAM_STR_16BIT( MR_Inputs_501,                   "MR IN 1"                         ) \
   PARAM_STR_16BIT( MR_Inputs_502,                   "MR IN 2"                         ) \
   PARAM_STR_16BIT( MR_Inputs_503,                   "MR IN 3"                         ) \
   PARAM_STR_16BIT( MR_Inputs_504,                   "MR IN 4"                         ) \
   PARAM_STR_16BIT( MR_Inputs_505,                   "MR IN 5"                         ) \
   PARAM_STR_16BIT( MR_Inputs_506,                   "MR IN 6"                         ) \
   PARAM_STR_16BIT( MR_Inputs_507,                   "MR IN 7"                         ) \
   PARAM_STR_16BIT( MR_Inputs_508,                   "MR IN 8"                         ) \
   PARAM_STR_16BIT( CT_Inputs_501,                   "CT IN 1"                         ) \
   PARAM_STR_16BIT( CT_Inputs_502,                   "CT IN 2"                         ) \
   PARAM_STR_16BIT( CT_Inputs_503,                   "CT IN 3"                         ) \
   PARAM_STR_16BIT( CT_Inputs_504,                   "CT IN 4"                         ) \
   PARAM_STR_16BIT( CT_Inputs_505,                   "CT IN 5"                         ) \
   PARAM_STR_16BIT( CT_Inputs_506,                   "CT IN 6"                         ) \
   PARAM_STR_16BIT( CT_Inputs_507,                   "CT IN 7"                         ) \
   PARAM_STR_16BIT( CT_Inputs_508,                   "CT IN 8"                         ) \
   PARAM_STR_16BIT( CT_Inputs_509,                   "CT IN 9"                         ) \
   PARAM_STR_16BIT( CT_Inputs_510,                   "CT IN 10"                        ) \
   PARAM_STR_16BIT( CT_Inputs_511,                   "CT IN 11"                        ) \
   PARAM_STR_16BIT( CT_Inputs_512,                   "CT IN 12"                        ) \
   PARAM_STR_16BIT( CT_Inputs_513,                   "CT IN 13"                        ) \
   PARAM_STR_16BIT( CT_Inputs_514,                   "CT IN 14"                        ) \
   PARAM_STR_16BIT( CT_Inputs_515,                   "CT IN 15"                        ) \
   PARAM_STR_16BIT( CT_Inputs_516,                   "CT IN 16"                        ) \
   PARAM_STR_16BIT( COP_Inputs_501,                  "COP IN 1"                        ) \
   PARAM_STR_16BIT( COP_Inputs_502,                  "COP IN 2"                        ) \
   PARAM_STR_16BIT( COP_Inputs_503,                  "COP IN 3"                        ) \
   PARAM_STR_16BIT( COP_Inputs_504,                  "COP IN 4"                        ) \
   PARAM_STR_16BIT( COP_Inputs_505,                  "COP IN 5"                        ) \
   PARAM_STR_16BIT( COP_Inputs_506,                  "COP IN 6"                        ) \
   PARAM_STR_16BIT( COP_Inputs_507,                  "COP IN 7"                        ) \
   PARAM_STR_16BIT( COP_Inputs_508,                  "COP IN 8"                        ) \
   PARAM_STR_16BIT( COP_Inputs_509,                  "COP IN 9"                        ) \
   PARAM_STR_16BIT( COP_Inputs_510,                  "COP IN 10"                       ) \
   PARAM_STR_16BIT( COP_Inputs_511,                  "COP IN 11"                       ) \
   PARAM_STR_16BIT( COP_Inputs_512,                  "COP IN 12"                       ) \
   PARAM_STR_16BIT( COP_Inputs_513,                  "COP IN 13"                       ) \
   PARAM_STR_16BIT( COP_Inputs_514,                  "COP IN 14"                       ) \
   PARAM_STR_16BIT( COP_Inputs_515,                  "COP IN 15"                       ) \
   PARAM_STR_16BIT( COP_Inputs_516,                  "COP IN 16"                       ) \
   PARAM_STR_16BIT( RIS1_Inputs_501,                 "RIS1 IN 1"                       ) \
   PARAM_STR_16BIT( RIS1_Inputs_502,                 "RIS1 IN 2"                       ) \
   PARAM_STR_16BIT( RIS1_Inputs_503,                 "RIS1 IN 3"                       ) \
   PARAM_STR_16BIT( RIS1_Inputs_504,                 "RIS1 IN 4"                       ) \
   PARAM_STR_16BIT( RIS1_Inputs_505,                 "RIS1 IN 5"                       ) \
   PARAM_STR_16BIT( RIS1_Inputs_506,                 "RIS1 IN 6"                       ) \
   PARAM_STR_16BIT( RIS1_Inputs_507,                 "RIS1 IN 7"                       ) \
   PARAM_STR_16BIT( RIS1_Inputs_508,                 "RIS1 IN 8"                       ) \
   PARAM_STR_16BIT( RIS2_Inputs_501,                 "RIS2 IN 1"                       ) \
   PARAM_STR_16BIT( RIS2_Inputs_502,                 "RIS2 IN 2"                       ) \
   PARAM_STR_16BIT( RIS2_Inputs_503,                 "RIS2 IN 3"                       ) \
   PARAM_STR_16BIT( RIS2_Inputs_504,                 "RIS2 IN 4"                       ) \
   PARAM_STR_16BIT( RIS2_Inputs_505,                 "RIS2 IN 5"                       ) \
   PARAM_STR_16BIT( RIS2_Inputs_506,                 "RIS2 IN 6"                       ) \
   PARAM_STR_16BIT( RIS2_Inputs_507,                 "RIS2 IN 7"                       ) \
   PARAM_STR_16BIT( RIS2_Inputs_508,                 "RIS2 IN 8"                       ) \
   PARAM_STR_16BIT( RIS3_Inputs_501,                 "RIS3 IN 1"                       ) \
   PARAM_STR_16BIT( RIS3_Inputs_502,                 "RIS3 IN 2"                       ) \
   PARAM_STR_16BIT( RIS3_Inputs_503,                 "RIS3 IN 3"                       ) \
   PARAM_STR_16BIT( RIS3_Inputs_504,                 "RIS3 IN 4"                       ) \
   PARAM_STR_16BIT( RIS3_Inputs_505,                 "RIS3 IN 5"                       ) \
   PARAM_STR_16BIT( RIS3_Inputs_506,                 "RIS3 IN 6"                       ) \
   PARAM_STR_16BIT( RIS3_Inputs_507,                 "RIS3 IN 7"                       ) \
   PARAM_STR_16BIT( RIS3_Inputs_508,                 "RIS3 IN 8"                       ) \
   PARAM_STR_16BIT( RIS4_Inputs_501,                 "RIS4 IN 1"                       ) \
   PARAM_STR_16BIT( RIS4_Inputs_502,                 "RIS4 IN 2"                       ) \
   PARAM_STR_16BIT( RIS4_Inputs_503,                 "RIS4 IN 3"                       ) \
   PARAM_STR_16BIT( RIS4_Inputs_504,                 "RIS4 IN 4"                       ) \
   PARAM_STR_16BIT( RIS4_Inputs_505,                 "RIS4 IN 5"                       ) \
   PARAM_STR_16BIT( RIS4_Inputs_506,                 "RIS4 IN 6"                       ) \
   PARAM_STR_16BIT( RIS4_Inputs_507,                 "RIS4 IN 7"                       ) \
   PARAM_STR_16BIT( RIS4_Inputs_508,                 "RIS4 IN 8"                       ) \
   PARAM_STR_16BIT( EXP01_Inputs_501,                "EXP01 IN 1"                      ) \
   PARAM_STR_16BIT( EXP01_Inputs_502,                "EXP01 IN 2"                      ) \
   PARAM_STR_16BIT( EXP01_Inputs_503,                "EXP01 IN 3"                      ) \
   PARAM_STR_16BIT( EXP01_Inputs_504,                "EXP01 IN 4"                      ) \
   PARAM_STR_16BIT( EXP01_Inputs_505,                "EXP01 IN 5"                      ) \
   PARAM_STR_16BIT( EXP01_Inputs_506,                "EXP01 IN 6"                      ) \
   PARAM_STR_16BIT( EXP01_Inputs_507,                "EXP01 IN 7"                      ) \
   PARAM_STR_16BIT( EXP01_Inputs_508,                "EXP01 IN 8"                      ) \
   PARAM_STR_16BIT( EXP02_Inputs_501,                "EXP02 IN 1"                      ) \
   PARAM_STR_16BIT( EXP02_Inputs_502,                "EXP02 IN 2"                      ) \
   PARAM_STR_16BIT( EXP02_Inputs_503,                "EXP02 IN 3"                      ) \
   PARAM_STR_16BIT( EXP02_Inputs_504,                "EXP02 IN 4"                      ) \
   PARAM_STR_16BIT( EXP02_Inputs_505,                "EXP02 IN 5"                      ) \
   PARAM_STR_16BIT( EXP02_Inputs_506,                "EXP02 IN 6"                      ) \
   PARAM_STR_16BIT( EXP02_Inputs_507,                "EXP02 IN 7"                      ) \
   PARAM_STR_16BIT( EXP02_Inputs_508,                "EXP02 IN 8"                      ) \
   PARAM_STR_16BIT( EXP03_Inputs_501,                "EXP03 IN 1"                      ) \
   PARAM_STR_16BIT( EXP03_Inputs_502,                "EXP03 IN 2"                      ) \
   PARAM_STR_16BIT( EXP03_Inputs_503,                "EXP03 IN 3"                      ) \
   PARAM_STR_16BIT( EXP03_Inputs_504,                "EXP03 IN 4"                      ) \
   PARAM_STR_16BIT( EXP03_Inputs_505,                "EXP03 IN 5"                      ) \
   PARAM_STR_16BIT( EXP03_Inputs_506,                "EXP03 IN 6"                      ) \
   PARAM_STR_16BIT( EXP03_Inputs_507,                "EXP03 IN 7"                      ) \
   PARAM_STR_16BIT( EXP03_Inputs_508,                "EXP03 IN 8"                      ) \
   PARAM_STR_16BIT( EXP04_Inputs_501,                "EXP04 IN 1"                      ) \
   PARAM_STR_16BIT( EXP04_Inputs_502,                "EXP04 IN 2"                      ) \
   PARAM_STR_16BIT( EXP04_Inputs_503,                "EXP04 IN 3"                      ) \
   PARAM_STR_16BIT( EXP04_Inputs_504,                "EXP04 IN 4"                      ) \
   PARAM_STR_16BIT( EXP04_Inputs_505,                "EXP04 IN 5"                      ) \
   PARAM_STR_16BIT( EXP04_Inputs_506,                "EXP04 IN 6"                      ) \
   PARAM_STR_16BIT( EXP04_Inputs_507,                "EXP04 IN 7"                      ) \
   PARAM_STR_16BIT( EXP04_Inputs_508,                "EXP04 IN 8"                      ) \
   PARAM_STR_16BIT( EXP05_Inputs_501,                "EXP05 IN 1"                      ) \
   PARAM_STR_16BIT( EXP05_Inputs_502,                "EXP05 IN 2"                      ) \
   PARAM_STR_16BIT( EXP05_Inputs_503,                "EXP05 IN 3"                      ) \
   PARAM_STR_16BIT( EXP05_Inputs_504,                "EXP05 IN 4"                      ) \
   PARAM_STR_16BIT( EXP05_Inputs_505,                "EXP05 IN 5"                      ) \
   PARAM_STR_16BIT( EXP05_Inputs_506,                "EXP05 IN 6"                      ) \
   PARAM_STR_16BIT( EXP05_Inputs_507,                "EXP05 IN 7"                      ) \
   PARAM_STR_16BIT( EXP05_Inputs_508,                "EXP05 IN 8"                      ) \
   PARAM_STR_16BIT( EXP06_Inputs_501,                "EXP06 IN 1"                      ) \
   PARAM_STR_16BIT( EXP06_Inputs_502,                "EXP06 IN 2"                      ) \
   PARAM_STR_16BIT( EXP06_Inputs_503,                "EXP06 IN 3"                      ) \
   PARAM_STR_16BIT( EXP06_Inputs_504,                "EXP06 IN 4"                      ) \
   PARAM_STR_16BIT( EXP06_Inputs_505,                "EXP06 IN 5"                      ) \
   PARAM_STR_16BIT( EXP06_Inputs_506,                "EXP06 IN 6"                      ) \
   PARAM_STR_16BIT( EXP06_Inputs_507,                "EXP06 IN 7"                      ) \
   PARAM_STR_16BIT( EXP06_Inputs_508,                "EXP06 IN 8"                      ) \
   PARAM_STR_16BIT( EXP07_Inputs_501,                "EXP07 IN 1"                      ) \
   PARAM_STR_16BIT( EXP07_Inputs_502,                "EXP07 IN 2"                      ) \
   PARAM_STR_16BIT( EXP07_Inputs_503,                "EXP07 IN 3"                      ) \
   PARAM_STR_16BIT( EXP07_Inputs_504,                "EXP07 IN 4"                      ) \
   PARAM_STR_16BIT( EXP07_Inputs_505,                "EXP07 IN 5"                      ) \
   PARAM_STR_16BIT( EXP07_Inputs_506,                "EXP07 IN 6"                      ) \
   PARAM_STR_16BIT( EXP07_Inputs_507,                "EXP07 IN 7"                      ) \
   PARAM_STR_16BIT( EXP07_Inputs_508,                "EXP07 IN 8"                      ) \
   PARAM_STR_16BIT( EXP08_Inputs_501,                "EXP08 IN 1"                      ) \
   PARAM_STR_16BIT( EXP08_Inputs_502,                "EXP08 IN 2"                      ) \
   PARAM_STR_16BIT( EXP08_Inputs_503,                "EXP08 IN 3"                      ) \
   PARAM_STR_16BIT( EXP08_Inputs_504,                "EXP08 IN 4"                      ) \
   PARAM_STR_16BIT( EXP08_Inputs_505,                "EXP08 IN 5"                      ) \
   PARAM_STR_16BIT( EXP08_Inputs_506,                "EXP08 IN 6"                      ) \
   PARAM_STR_16BIT( EXP08_Inputs_507,                "EXP08 IN 7"                      ) \
   PARAM_STR_16BIT( EXP08_Inputs_508,                "EXP08 IN 8"                      ) \
   PARAM_STR_16BIT( EXP09_Inputs_501,                "EXP09 IN 1"                      ) \
   PARAM_STR_16BIT( EXP09_Inputs_502,                "EXP09 IN 2"                      ) \
   PARAM_STR_16BIT( EXP09_Inputs_503,                "EXP09 IN 3"                      ) \
   PARAM_STR_16BIT( EXP09_Inputs_504,                "EXP09 IN 4"                      ) \
   PARAM_STR_16BIT( EXP09_Inputs_505,                "EXP09 IN 5"                      ) \
   PARAM_STR_16BIT( EXP09_Inputs_506,                "EXP09 IN 6"                      ) \
   PARAM_STR_16BIT( EXP09_Inputs_507,                "EXP09 IN 7"                      ) \
   PARAM_STR_16BIT( EXP09_Inputs_508,                "EXP09 IN 8"                      ) \
   PARAM_STR_16BIT( EXP10_Inputs_501,                "EXP10 IN 1"                      ) \
   PARAM_STR_16BIT( EXP10_Inputs_502,                "EXP10 IN 2"                      ) \
   PARAM_STR_16BIT( EXP10_Inputs_503,                "EXP10 IN 3"                      ) \
   PARAM_STR_16BIT( EXP10_Inputs_504,                "EXP10 IN 4"                      ) \
   PARAM_STR_16BIT( EXP10_Inputs_505,                "EXP10 IN 5"                      ) \
   PARAM_STR_16BIT( EXP10_Inputs_506,                "EXP10 IN 6"                      ) \
   PARAM_STR_16BIT( EXP10_Inputs_507,                "EXP10 IN 7"                      ) \
   PARAM_STR_16BIT( EXP10_Inputs_508,                "EXP10 IN 8"                      ) \
   PARAM_STR_16BIT( EXP11_Inputs_501,                "EXP11 IN 1"                      ) \
   PARAM_STR_16BIT( EXP11_Inputs_502,                "EXP11 IN 2"                      ) \
   PARAM_STR_16BIT( EXP11_Inputs_503,                "EXP11 IN 3"                      ) \
   PARAM_STR_16BIT( EXP11_Inputs_504,                "EXP11 IN 4"                      ) \
   PARAM_STR_16BIT( EXP11_Inputs_505,                "EXP11 IN 5"                      ) \
   PARAM_STR_16BIT( EXP11_Inputs_506,                "EXP11 IN 6"                      ) \
   PARAM_STR_16BIT( EXP11_Inputs_507,                "EXP11 IN 7"                      ) \
   PARAM_STR_16BIT( EXP11_Inputs_508,                "EXP11 IN 8"                      ) \
   PARAM_STR_16BIT( EXP12_Inputs_501,                "EXP12 IN 1"                      ) \
   PARAM_STR_16BIT( EXP12_Inputs_502,                "EXP12 IN 2"                      ) \
   PARAM_STR_16BIT( EXP12_Inputs_503,                "EXP12 IN 3"                      ) \
   PARAM_STR_16BIT( EXP12_Inputs_504,                "EXP12 IN 4"                      ) \
   PARAM_STR_16BIT( EXP12_Inputs_505,                "EXP12 IN 5"                      ) \
   PARAM_STR_16BIT( EXP12_Inputs_506,                "EXP12 IN 6"                      ) \
   PARAM_STR_16BIT( EXP12_Inputs_507,                "EXP12 IN 7"                      ) \
   PARAM_STR_16BIT( EXP12_Inputs_508,                "EXP12 IN 8"                      ) \
   PARAM_STR_16BIT( EXP13_Inputs_501,                "EXP13 IN 1"                      ) \
   PARAM_STR_16BIT( EXP13_Inputs_502,                "EXP13 IN 2"                      ) \
   PARAM_STR_16BIT( EXP13_Inputs_503,                "EXP13 IN 3"                      ) \
   PARAM_STR_16BIT( EXP13_Inputs_504,                "EXP13 IN 4"                      ) \
   PARAM_STR_16BIT( EXP13_Inputs_505,                "EXP13 IN 5"                      ) \
   PARAM_STR_16BIT( EXP13_Inputs_506,                "EXP13 IN 6"                      ) \
   PARAM_STR_16BIT( EXP13_Inputs_507,                "EXP13 IN 7"                      ) \
   PARAM_STR_16BIT( EXP13_Inputs_508,                "EXP13 IN 8"                      ) \
   PARAM_STR_16BIT( EXP14_Inputs_501,                "EXP14 IN 1"                      ) \
   PARAM_STR_16BIT( EXP14_Inputs_502,                "EXP14 IN 2"                      ) \
   PARAM_STR_16BIT( EXP14_Inputs_503,                "EXP14 IN 3"                      ) \
   PARAM_STR_16BIT( EXP14_Inputs_504,                "EXP14 IN 4"                      ) \
   PARAM_STR_16BIT( EXP14_Inputs_505,                "EXP14 IN 5"                      ) \
   PARAM_STR_16BIT( EXP14_Inputs_506,                "EXP14 IN 6"                      ) \
   PARAM_STR_16BIT( EXP14_Inputs_507,                "EXP14 IN 7"                      ) \
   PARAM_STR_16BIT( EXP14_Inputs_508,                "EXP14 IN 8"                      ) \
   PARAM_STR_16BIT( EXP15_Inputs_501,                "EXP15 IN 1"                      ) \
   PARAM_STR_16BIT( EXP15_Inputs_502,                "EXP15 IN 2"                      ) \
   PARAM_STR_16BIT( EXP15_Inputs_503,                "EXP15 IN 3"                      ) \
   PARAM_STR_16BIT( EXP15_Inputs_504,                "EXP15 IN 4"                      ) \
   PARAM_STR_16BIT( EXP15_Inputs_505,                "EXP15 IN 5"                      ) \
   PARAM_STR_16BIT( EXP15_Inputs_506,                "EXP15 IN 6"                      ) \
   PARAM_STR_16BIT( EXP15_Inputs_507,                "EXP15 IN 7"                      ) \
   PARAM_STR_16BIT( EXP15_Inputs_508,                "EXP15 IN 8"                      ) \
   PARAM_STR_16BIT( EXP16_Inputs_501,                "EXP16 IN 1"                      ) \
   PARAM_STR_16BIT( EXP16_Inputs_502,                "EXP16 IN 2"                      ) \
   PARAM_STR_16BIT( EXP16_Inputs_503,                "EXP16 IN 3"                      ) \
   PARAM_STR_16BIT( EXP16_Inputs_504,                "EXP16 IN 4"                      ) \
   PARAM_STR_16BIT( EXP16_Inputs_505,                "EXP16 IN 5"                      ) \
   PARAM_STR_16BIT( EXP16_Inputs_506,                "EXP16 IN 6"                      ) \
   PARAM_STR_16BIT( EXP16_Inputs_507,                "EXP16 IN 7"                      ) \
   PARAM_STR_16BIT( EXP16_Inputs_508,                "EXP16 IN 8"                      ) \
   PARAM_STR_16BIT( EXP17_Inputs_501,                "EXP17 IN 1"                      ) \
   PARAM_STR_16BIT( EXP17_Inputs_502,                "EXP17 IN 2"                      ) \
   PARAM_STR_16BIT( EXP17_Inputs_503,                "EXP17 IN 3"                      ) \
   PARAM_STR_16BIT( EXP17_Inputs_504,                "EXP17 IN 4"                      ) \
   PARAM_STR_16BIT( EXP17_Inputs_505,                "EXP17 IN 5"                      ) \
   PARAM_STR_16BIT( EXP17_Inputs_506,                "EXP17 IN 6"                      ) \
   PARAM_STR_16BIT( EXP17_Inputs_507,                "EXP17 IN 7"                      ) \
   PARAM_STR_16BIT( EXP17_Inputs_508,                "EXP17 IN 8"                      ) \
   PARAM_STR_16BIT( EXP18_Inputs_501,                "EXP18 IN 1"                      ) \
   PARAM_STR_16BIT( EXP18_Inputs_502,                "EXP18 IN 2"                      ) \
   PARAM_STR_16BIT( EXP18_Inputs_503,                "EXP18 IN 3"                      ) \
   PARAM_STR_16BIT( EXP18_Inputs_504,                "EXP18 IN 4"                      ) \
   PARAM_STR_16BIT( EXP18_Inputs_505,                "EXP18 IN 5"                      ) \
   PARAM_STR_16BIT( EXP18_Inputs_506,                "EXP18 IN 6"                      ) \
   PARAM_STR_16BIT( EXP18_Inputs_507,                "EXP18 IN 7"                      ) \
   PARAM_STR_16BIT( EXP18_Inputs_508,                "EXP18 IN 8"                      ) \
   PARAM_STR_16BIT( EXP19_Inputs_501,                "EXP19 IN 1"                      ) \
   PARAM_STR_16BIT( EXP19_Inputs_502,                "EXP19 IN 2"                      ) \
   PARAM_STR_16BIT( EXP19_Inputs_503,                "EXP19 IN 3"                      ) \
   PARAM_STR_16BIT( EXP19_Inputs_504,                "EXP19 IN 4"                      ) \
   PARAM_STR_16BIT( EXP19_Inputs_505,                "EXP19 IN 5"                      ) \
   PARAM_STR_16BIT( EXP19_Inputs_506,                "EXP19 IN 6"                      ) \
   PARAM_STR_16BIT( EXP19_Inputs_507,                "EXP19 IN 7"                      ) \
   PARAM_STR_16BIT( EXP19_Inputs_508,                "EXP19 IN 8"                      ) \
   PARAM_STR_16BIT( EXP20_Inputs_501,                "EXP20 IN 1"                      ) \
   PARAM_STR_16BIT( EXP20_Inputs_502,                "EXP20 IN 2"                      ) \
   PARAM_STR_16BIT( EXP20_Inputs_503,                "EXP20 IN 3"                      ) \
   PARAM_STR_16BIT( EXP20_Inputs_504,                "EXP20 IN 4"                      ) \
   PARAM_STR_16BIT( EXP20_Inputs_505,                "EXP20 IN 5"                      ) \
   PARAM_STR_16BIT( EXP20_Inputs_506,                "EXP20 IN 6"                      ) \
   PARAM_STR_16BIT( EXP20_Inputs_507,                "EXP20 IN 7"                      ) \
   PARAM_STR_16BIT( EXP20_Inputs_508,                "EXP20 IN 8"                      ) \
   PARAM_STR_16BIT( EXP21_Inputs_501,                "EXP21 IN 1"                      ) \
   PARAM_STR_16BIT( EXP21_Inputs_502,                "EXP21 IN 2"                      ) \
   PARAM_STR_16BIT( EXP21_Inputs_503,                "EXP21 IN 3"                      ) \
   PARAM_STR_16BIT( EXP21_Inputs_504,                "EXP21 IN 4"                      ) \
   PARAM_STR_16BIT( EXP21_Inputs_505,                "EXP21 IN 5"                      ) \
   PARAM_STR_16BIT( EXP21_Inputs_506,                "EXP21 IN 6"                      ) \
   PARAM_STR_16BIT( EXP21_Inputs_507,                "EXP21 IN 7"                      ) \
   PARAM_STR_16BIT( EXP21_Inputs_508,                "EXP21 IN 8"                      ) \
   PARAM_STR_16BIT( EXP22_Inputs_501,                "EXP22 IN 1"                      ) \
   PARAM_STR_16BIT( EXP22_Inputs_502,                "EXP22 IN 2"                      ) \
   PARAM_STR_16BIT( EXP22_Inputs_503,                "EXP22 IN 3"                      ) \
   PARAM_STR_16BIT( EXP22_Inputs_504,                "EXP22 IN 4"                      ) \
   PARAM_STR_16BIT( EXP22_Inputs_505,                "EXP22 IN 5"                      ) \
   PARAM_STR_16BIT( EXP22_Inputs_506,                "EXP22 IN 6"                      ) \
   PARAM_STR_16BIT( EXP22_Inputs_507,                "EXP22 IN 7"                      ) \
   PARAM_STR_16BIT( EXP22_Inputs_508,                "EXP22 IN 8"                      ) \
   PARAM_STR_16BIT( EXP23_Inputs_501,                "EXP23 IN 1"                      ) \
   PARAM_STR_16BIT( EXP23_Inputs_502,                "EXP23 IN 2"                      ) \
   PARAM_STR_16BIT( EXP23_Inputs_503,                "EXP23 IN 3"                      ) \
   PARAM_STR_16BIT( EXP23_Inputs_504,                "EXP23 IN 4"                      ) \
   PARAM_STR_16BIT( EXP23_Inputs_505,                "EXP23 IN 5"                      ) \
   PARAM_STR_16BIT( EXP23_Inputs_506,                "EXP23 IN 6"                      ) \
   PARAM_STR_16BIT( EXP23_Inputs_507,                "EXP23 IN 7"                      ) \
   PARAM_STR_16BIT( EXP23_Inputs_508,                "EXP23 IN 8"                      ) \
   PARAM_STR_16BIT( EXP24_Inputs_501,                "EXP24 IN 1"                      ) \
   PARAM_STR_16BIT( EXP24_Inputs_502,                "EXP24 IN 2"                      ) \
   PARAM_STR_16BIT( EXP24_Inputs_503,                "EXP24 IN 3"                      ) \
   PARAM_STR_16BIT( EXP24_Inputs_504,                "EXP24 IN 4"                      ) \
   PARAM_STR_16BIT( EXP24_Inputs_505,                "EXP24 IN 5"                      ) \
   PARAM_STR_16BIT( EXP24_Inputs_506,                "EXP24 IN 6"                      ) \
   PARAM_STR_16BIT( EXP24_Inputs_507,                "EXP24 IN 7"                      ) \
   PARAM_STR_16BIT( EXP24_Inputs_508,                "EXP24 IN 8"                      ) \
   PARAM_STR_16BIT( EXP25_Inputs_501,                "EXP25 IN 1"                      ) \
   PARAM_STR_16BIT( EXP25_Inputs_502,                "EXP25 IN 2"                      ) \
   PARAM_STR_16BIT( EXP25_Inputs_503,                "EXP25 IN 3"                      ) \
   PARAM_STR_16BIT( EXP25_Inputs_504,                "EXP25 IN 4"                      ) \
   PARAM_STR_16BIT( EXP25_Inputs_505,                "EXP25 IN 5"                      ) \
   PARAM_STR_16BIT( EXP25_Inputs_506,                "EXP25 IN 6"                      ) \
   PARAM_STR_16BIT( EXP25_Inputs_507,                "EXP25 IN 7"                      ) \
   PARAM_STR_16BIT( EXP25_Inputs_508,                "EXP25 IN 8"                      ) \
   PARAM_STR_16BIT( EXP26_Inputs_501,                "EXP26 IN 1"                      ) \
   PARAM_STR_16BIT( EXP26_Inputs_502,                "EXP26 IN 2"                      ) \
   PARAM_STR_16BIT( EXP26_Inputs_503,                "EXP26 IN 3"                      ) \
   PARAM_STR_16BIT( EXP26_Inputs_504,                "EXP26 IN 4"                      ) \
   PARAM_STR_16BIT( EXP26_Inputs_505,                "EXP26 IN 5"                      ) \
   PARAM_STR_16BIT( EXP26_Inputs_506,                "EXP26 IN 6"                      ) \
   PARAM_STR_16BIT( EXP26_Inputs_507,                "EXP26 IN 7"                      ) \
   PARAM_STR_16BIT( EXP26_Inputs_508,                "EXP26 IN 8"                      ) \
   PARAM_STR_16BIT( EXP27_Inputs_501,                "EXP27 IN 1"                      ) \
   PARAM_STR_16BIT( EXP27_Inputs_502,                "EXP27 IN 2"                      ) \
   PARAM_STR_16BIT( EXP27_Inputs_503,                "EXP27 IN 3"                      ) \
   PARAM_STR_16BIT( EXP27_Inputs_504,                "EXP27 IN 4"                      ) \
   PARAM_STR_16BIT( EXP27_Inputs_505,                "EXP27 IN 5"                      ) \
   PARAM_STR_16BIT( EXP27_Inputs_506,                "EXP27 IN 6"                      ) \
   PARAM_STR_16BIT( EXP27_Inputs_507,                "EXP27 IN 7"                      ) \
   PARAM_STR_16BIT( EXP27_Inputs_508,                "EXP27 IN 8"                      ) \
   PARAM_STR_16BIT( EXP28_Inputs_501,                "EXP28 IN 1"                      ) \
   PARAM_STR_16BIT( EXP28_Inputs_502,                "EXP28 IN 2"                      ) \
   PARAM_STR_16BIT( EXP28_Inputs_503,                "EXP28 IN 3"                      ) \
   PARAM_STR_16BIT( EXP28_Inputs_504,                "EXP28 IN 4"                      ) \
   PARAM_STR_16BIT( EXP28_Inputs_505,                "EXP28 IN 5"                      ) \
   PARAM_STR_16BIT( EXP28_Inputs_506,                "EXP28 IN 6"                      ) \
   PARAM_STR_16BIT( EXP28_Inputs_507,                "EXP28 IN 7"                      ) \
   PARAM_STR_16BIT( EXP28_Inputs_508,                "EXP28 IN 8"                      ) \
   PARAM_STR_16BIT( EXP29_Inputs_501,                "EXP29 IN 1"                      ) \
   PARAM_STR_16BIT( EXP29_Inputs_502,                "EXP29 IN 2"                      ) \
   PARAM_STR_16BIT( EXP29_Inputs_503,                "EXP29 IN 3"                      ) \
   PARAM_STR_16BIT( EXP29_Inputs_504,                "EXP29 IN 4"                      ) \
   PARAM_STR_16BIT( EXP29_Inputs_505,                "EXP29 IN 5"                      ) \
   PARAM_STR_16BIT( EXP29_Inputs_506,                "EXP29 IN 6"                      ) \
   PARAM_STR_16BIT( EXP29_Inputs_507,                "EXP29 IN 7"                      ) \
   PARAM_STR_16BIT( EXP29_Inputs_508,                "EXP29 IN 8"                      ) \
   PARAM_STR_16BIT( EXP30_Inputs_501,                "EXP30 IN 1"                      ) \
   PARAM_STR_16BIT( EXP30_Inputs_502,                "EXP30 IN 2"                      ) \
   PARAM_STR_16BIT( EXP30_Inputs_503,                "EXP30 IN 3"                      ) \
   PARAM_STR_16BIT( EXP30_Inputs_504,                "EXP30 IN 4"                      ) \
   PARAM_STR_16BIT( EXP30_Inputs_505,                "EXP30 IN 5"                      ) \
   PARAM_STR_16BIT( EXP30_Inputs_506,                "EXP30 IN 6"                      ) \
   PARAM_STR_16BIT( EXP30_Inputs_507,                "EXP30 IN 7"                      ) \
   PARAM_STR_16BIT( EXP30_Inputs_508,                "EXP30 IN 8"                      ) \
   PARAM_STR_16BIT( EXP31_Inputs_501,                "EXP31 IN 1"                      ) \
   PARAM_STR_16BIT( EXP31_Inputs_502,                "EXP31 IN 2"                      ) \
   PARAM_STR_16BIT( EXP31_Inputs_503,                "EXP31 IN 3"                      ) \
   PARAM_STR_16BIT( EXP31_Inputs_504,                "EXP31 IN 4"                      ) \
   PARAM_STR_16BIT( EXP31_Inputs_505,                "EXP31 IN 5"                      ) \
   PARAM_STR_16BIT( EXP31_Inputs_506,                "EXP31 IN 6"                      ) \
   PARAM_STR_16BIT( EXP31_Inputs_507,                "EXP31 IN 7"                      ) \
   PARAM_STR_16BIT( EXP31_Inputs_508,                "EXP31 IN 8"                      ) \
   PARAM_STR_16BIT( EXP32_Inputs_501,                "EXP32 IN 1"                      ) \
   PARAM_STR_16BIT( EXP32_Inputs_502,                "EXP32 IN 2"                      ) \
   PARAM_STR_16BIT( EXP32_Inputs_503,                "EXP32 IN 3"                      ) \
   PARAM_STR_16BIT( EXP32_Inputs_504,                "EXP32 IN 4"                      ) \
   PARAM_STR_16BIT( EXP32_Inputs_505,                "EXP32 IN 5"                      ) \
   PARAM_STR_16BIT( EXP32_Inputs_506,                "EXP32 IN 6"                      ) \
   PARAM_STR_16BIT( EXP32_Inputs_507,                "EXP32 IN 7"                      ) \
   PARAM_STR_16BIT( EXP32_Inputs_508,                "EXP32 IN 8"                      ) \
   PARAM_STR_16BIT( EXP33_Inputs_501,                "EXP33 IN 1"                      ) \
   PARAM_STR_16BIT( EXP33_Inputs_502,                "EXP33 IN 2"                      ) \
   PARAM_STR_16BIT( EXP33_Inputs_503,                "EXP33 IN 3"                      ) \
   PARAM_STR_16BIT( EXP33_Inputs_504,                "EXP33 IN 4"                      ) \
   PARAM_STR_16BIT( EXP33_Inputs_505,                "EXP33 IN 5"                      ) \
   PARAM_STR_16BIT( EXP33_Inputs_506,                "EXP33 IN 6"                      ) \
   PARAM_STR_16BIT( EXP33_Inputs_507,                "EXP33 IN 7"                      ) \
   PARAM_STR_16BIT( EXP33_Inputs_508,                "EXP33 IN 8"                      ) \
   PARAM_STR_16BIT( EXP34_Inputs_501,                "EXP34 IN 1"                      ) \
   PARAM_STR_16BIT( EXP34_Inputs_502,                "EXP34 IN 2"                      ) \
   PARAM_STR_16BIT( EXP34_Inputs_503,                "EXP34 IN 3"                      ) \
   PARAM_STR_16BIT( EXP34_Inputs_504,                "EXP34 IN 4"                      ) \
   PARAM_STR_16BIT( EXP34_Inputs_505,                "EXP34 IN 5"                      ) \
   PARAM_STR_16BIT( EXP34_Inputs_506,                "EXP34 IN 6"                      ) \
   PARAM_STR_16BIT( EXP34_Inputs_507,                "EXP34 IN 7"                      ) \
   PARAM_STR_16BIT( EXP34_Inputs_508,                "EXP34 IN 8"                      ) \
   PARAM_STR_16BIT( EXP35_Inputs_501,                "EXP35 IN 1"                      ) \
   PARAM_STR_16BIT( EXP35_Inputs_502,                "EXP35 IN 2"                      ) \
   PARAM_STR_16BIT( EXP35_Inputs_503,                "EXP35 IN 3"                      ) \
   PARAM_STR_16BIT( EXP35_Inputs_504,                "EXP35 IN 4"                      ) \
   PARAM_STR_16BIT( EXP35_Inputs_505,                "EXP35 IN 5"                      ) \
   PARAM_STR_16BIT( EXP35_Inputs_506,                "EXP35 IN 6"                      ) \
   PARAM_STR_16BIT( EXP35_Inputs_507,                "EXP35 IN 7"                      ) \
   PARAM_STR_16BIT( EXP35_Inputs_508,                "EXP35 IN 8"                      ) \
   PARAM_STR_16BIT( EXP36_Inputs_501,                "EXP36 IN 1"                      ) \
   PARAM_STR_16BIT( EXP36_Inputs_502,                "EXP36 IN 2"                      ) \
   PARAM_STR_16BIT( EXP36_Inputs_503,                "EXP36 IN 3"                      ) \
   PARAM_STR_16BIT( EXP36_Inputs_504,                "EXP36 IN 4"                      ) \
   PARAM_STR_16BIT( EXP36_Inputs_505,                "EXP36 IN 5"                      ) \
   PARAM_STR_16BIT( EXP36_Inputs_506,                "EXP36 IN 6"                      ) \
   PARAM_STR_16BIT( EXP36_Inputs_507,                "EXP36 IN 7"                      ) \
   PARAM_STR_16BIT( EXP36_Inputs_508,                "EXP36 IN 8"                      ) \
   PARAM_STR_16BIT( EXP37_Inputs_501,                "EXP37 IN 1"                      ) \
   PARAM_STR_16BIT( EXP37_Inputs_502,                "EXP37 IN 2"                      ) \
   PARAM_STR_16BIT( EXP37_Inputs_503,                "EXP37 IN 3"                      ) \
   PARAM_STR_16BIT( EXP37_Inputs_504,                "EXP37 IN 4"                      ) \
   PARAM_STR_16BIT( EXP37_Inputs_505,                "EXP37 IN 5"                      ) \
   PARAM_STR_16BIT( EXP37_Inputs_506,                "EXP37 IN 6"                      ) \
   PARAM_STR_16BIT( EXP37_Inputs_507,                "EXP37 IN 7"                      ) \
   PARAM_STR_16BIT( EXP37_Inputs_508,                "EXP37 IN 8"                      ) \
   PARAM_STR_16BIT( EXP38_Inputs_501,                "EXP38 IN 1"                      ) \
   PARAM_STR_16BIT( EXP38_Inputs_502,                "EXP38 IN 2"                      ) \
   PARAM_STR_16BIT( EXP38_Inputs_503,                "EXP38 IN 3"                      ) \
   PARAM_STR_16BIT( EXP38_Inputs_504,                "EXP38 IN 4"                      ) \
   PARAM_STR_16BIT( EXP38_Inputs_505,                "EXP38 IN 5"                      ) \
   PARAM_STR_16BIT( EXP38_Inputs_506,                "EXP38 IN 6"                      ) \
   PARAM_STR_16BIT( EXP38_Inputs_507,                "EXP38 IN 7"                      ) \
   PARAM_STR_16BIT( EXP38_Inputs_508,                "EXP38 IN 8"                      ) \
   PARAM_STR_16BIT( EXP39_Inputs_501,                "EXP39 IN 1"                      ) \
   PARAM_STR_16BIT( EXP39_Inputs_502,                "EXP39 IN 2"                      ) \
   PARAM_STR_16BIT( EXP39_Inputs_503,                "EXP39 IN 3"                      ) \
   PARAM_STR_16BIT( EXP39_Inputs_504,                "EXP39 IN 4"                      ) \
   PARAM_STR_16BIT( EXP39_Inputs_505,                "EXP39 IN 5"                      ) \
   PARAM_STR_16BIT( EXP39_Inputs_506,                "EXP39 IN 6"                      ) \
   PARAM_STR_16BIT( EXP39_Inputs_507,                "EXP39 IN 7"                      ) \
   PARAM_STR_16BIT( EXP39_Inputs_508,                "EXP39 IN 8"                      ) \
   PARAM_STR_16BIT( EXP40_Inputs_501,                "EXP40 IN 1"                      ) \
   PARAM_STR_16BIT( EXP40_Inputs_502,                "EXP40 IN 2"                      ) \
   PARAM_STR_16BIT( EXP40_Inputs_503,                "EXP40 IN 3"                      ) \
   PARAM_STR_16BIT( EXP40_Inputs_504,                "EXP40 IN 4"                      ) \
   PARAM_STR_16BIT( EXP40_Inputs_505,                "EXP40 IN 5"                      ) \
   PARAM_STR_16BIT( EXP40_Inputs_506,                "EXP40 IN 6"                      ) \
   PARAM_STR_16BIT( EXP40_Inputs_507,                "EXP40 IN 7"                      ) \
   PARAM_STR_16BIT( EXP40_Inputs_508,                "EXP40 IN 8"                      ) \
   PARAM_STR_16BIT( MR_Outputs_601,                  "MR OUT 1"                        ) \
   PARAM_STR_16BIT( MR_Outputs_602,                  "MR OUT 2"                        ) \
   PARAM_STR_16BIT( MR_Outputs_603,                  "MR OUT 3"                        ) \
   PARAM_STR_16BIT( MR_Outputs_604,                  "MR OUT 4"                        ) \
   PARAM_STR_16BIT( MR_Outputs_605,                  "MR OUT 5"                        ) \
   PARAM_STR_16BIT( MR_Outputs_606,                  "MR OUT 6"                        ) \
   PARAM_STR_16BIT( MR_Outputs_607,                  "MR OUT 7"                        ) \
   PARAM_STR_16BIT( MR_Outputs_608,                  "MR OUT 8"                        ) \
   PARAM_STR_16BIT( CT_Outputs_601,                  "CT OUT 1"                        ) \
   PARAM_STR_16BIT( CT_Outputs_602,                  "CT OUT 2"                        ) \
   PARAM_STR_16BIT( CT_Outputs_603,                  "CT OUT 3"                        ) \
   PARAM_STR_16BIT( CT_Outputs_604,                  "CT OUT 4"                        ) \
   PARAM_STR_16BIT( CT_Outputs_605,                  "CT OUT 5"                        ) \
   PARAM_STR_16BIT( CT_Outputs_606,                  "CT OUT 6"                        ) \
   PARAM_STR_16BIT( CT_Outputs_607,                  "CT OUT 7"                        ) \
   PARAM_STR_16BIT( CT_Outputs_608,                  "CT OUT 8"                        ) \
   PARAM_STR_16BIT( CT_Outputs_609,                  "CT OUT 9"                        ) \
   PARAM_STR_16BIT( CT_Outputs_610,                  "CT OUT 10"                       ) \
   PARAM_STR_16BIT( CT_Outputs_611,                  "CT OUT 11"                       ) \
   PARAM_STR_16BIT( CT_Outputs_612,                  "CT OUT 12"                       ) \
   PARAM_STR_16BIT( CT_Outputs_613,                  "CT OUT 13"                       ) \
   PARAM_STR_16BIT( CT_Outputs_614,                  "CT OUT 14"                       ) \
   PARAM_STR_16BIT( CT_Outputs_615,                  "CT OUT 15"                       ) \
   PARAM_STR_16BIT( CT_Outputs_616,                  "CT OUT 16"                       ) \
   PARAM_STR_16BIT( COP_Outputs_601,                 "COP OUT 1"                       ) \
   PARAM_STR_16BIT( COP_Outputs_602,                 "COP OUT 2"                       ) \
   PARAM_STR_16BIT( COP_Outputs_603,                 "COP OUT 3"                       ) \
   PARAM_STR_16BIT( COP_Outputs_604,                 "COP OUT 4"                       ) \
   PARAM_STR_16BIT( COP_Outputs_605,                 "COP OUT 5"                       ) \
   PARAM_STR_16BIT( COP_Outputs_606,                 "COP OUT 6"                       ) \
   PARAM_STR_16BIT( COP_Outputs_607,                 "COP OUT 7"                       ) \
   PARAM_STR_16BIT( COP_Outputs_608,                 "COP OUT 8"                       ) \
   PARAM_STR_16BIT( COP_Outputs_609,                 "COP OUT 9"                       ) \
   PARAM_STR_16BIT( COP_Outputs_610,                 "COP OUT 10"                      ) \
   PARAM_STR_16BIT( COP_Outputs_611,                 "COP OUT 11"                      ) \
   PARAM_STR_16BIT( COP_Outputs_612,                 "COP OUT 12"                      ) \
   PARAM_STR_16BIT( COP_Outputs_613,                 "COP OUT 13"                      ) \
   PARAM_STR_16BIT( COP_Outputs_614,                 "COP OUT 14"                      ) \
   PARAM_STR_16BIT( COP_Outputs_615,                 "COP OUT 15"                      ) \
   PARAM_STR_16BIT( COP_Outputs_616,                 "COP OUT 16"                      ) \
   PARAM_STR_16BIT( RIS1_Outputs_601,                "RIS1 OUT 1"                      ) \
   PARAM_STR_16BIT( RIS1_Outputs_602,                "RIS1 OUT 2"                      ) \
   PARAM_STR_16BIT( RIS1_Outputs_603,                "RIS1 OUT 3"                      ) \
   PARAM_STR_16BIT( RIS1_Outputs_604,                "RIS1 OUT 4"                      ) \
   PARAM_STR_16BIT( RIS1_Outputs_605,                "RIS1 OUT 5"                      ) \
   PARAM_STR_16BIT( RIS1_Outputs_606,                "RIS1 OUT 6"                      ) \
   PARAM_STR_16BIT( RIS1_Outputs_607,                "RIS1 OUT 7"                      ) \
   PARAM_STR_16BIT( RIS1_Outputs_608,                "RIS1 OUT 8"                      ) \
   PARAM_STR_16BIT( RIS2_Outputs_601,                "RIS2 OUT 1"                      ) \
   PARAM_STR_16BIT( RIS2_Outputs_602,                "RIS2 OUT 2"                      ) \
   PARAM_STR_16BIT( RIS2_Outputs_603,                "RIS2 OUT 3"                      ) \
   PARAM_STR_16BIT( RIS2_Outputs_604,                "RIS2 OUT 4"                      ) \
   PARAM_STR_16BIT( RIS2_Outputs_605,                "RIS2 OUT 5"                      ) \
   PARAM_STR_16BIT( RIS2_Outputs_606,                "RIS2 OUT 6"                      ) \
   PARAM_STR_16BIT( RIS2_Outputs_607,                "RIS2 OUT 7"                      ) \
   PARAM_STR_16BIT( RIS2_Outputs_608,                "RIS2 OUT 8"                      ) \
   PARAM_STR_16BIT( RIS3_Outputs_601,                "RIS3 OUT 1"                      ) \
   PARAM_STR_16BIT( RIS3_Outputs_602,                "RIS3 OUT 2"                      ) \
   PARAM_STR_16BIT( RIS3_Outputs_603,                "RIS3 OUT 3"                      ) \
   PARAM_STR_16BIT( RIS3_Outputs_604,                "RIS3 OUT 4"                      ) \
   PARAM_STR_16BIT( RIS3_Outputs_605,                "RIS3 OUT 5"                      ) \
   PARAM_STR_16BIT( RIS3_Outputs_606,                "RIS3 OUT 6"                      ) \
   PARAM_STR_16BIT( RIS3_Outputs_607,                "RIS3 OUT 7"                      ) \
   PARAM_STR_16BIT( RIS3_Outputs_608,                "RIS3 OUT 8"                      ) \
   PARAM_STR_16BIT( RIS4_Outputs_601,                "RIS4 OUT 1"                      ) \
   PARAM_STR_16BIT( RIS4_Outputs_602,                "RIS4 OUT 2"                      ) \
   PARAM_STR_16BIT( RIS4_Outputs_603,                "RIS4 OUT 3"                      ) \
   PARAM_STR_16BIT( RIS4_Outputs_604,                "RIS4 OUT 4"                      ) \
   PARAM_STR_16BIT( RIS4_Outputs_605,                "RIS4 OUT 5"                      ) \
   PARAM_STR_16BIT( RIS4_Outputs_606,                "RIS4 OUT 6"                      ) \
   PARAM_STR_16BIT( RIS4_Outputs_607,                "RIS4 OUT 7"                      ) \
   PARAM_STR_16BIT( RIS4_Outputs_608,                "RIS4 OUT 8"                      ) \
   PARAM_STR_16BIT( EXP01_Outputs_601,               "EXP01 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP01_Outputs_602,               "EXP01 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP01_Outputs_603,               "EXP01 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP01_Outputs_604,               "EXP01 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP01_Outputs_605,               "EXP01 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP01_Outputs_606,               "EXP01 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP01_Outputs_607,               "EXP01 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP01_Outputs_608,               "EXP01 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP02_Outputs_601,               "EXP02 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP02_Outputs_602,               "EXP02 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP02_Outputs_603,               "EXP02 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP02_Outputs_604,               "EXP02 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP02_Outputs_605,               "EXP02 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP02_Outputs_606,               "EXP02 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP02_Outputs_607,               "EXP02 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP02_Outputs_608,               "EXP02 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP03_Outputs_601,               "EXP03 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP03_Outputs_602,               "EXP03 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP03_Outputs_603,               "EXP03 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP03_Outputs_604,               "EXP03 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP03_Outputs_605,               "EXP03 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP03_Outputs_606,               "EXP03 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP03_Outputs_607,               "EXP03 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP03_Outputs_608,               "EXP03 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP04_Outputs_601,               "EXP04 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP04_Outputs_602,               "EXP04 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP04_Outputs_603,               "EXP04 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP04_Outputs_604,               "EXP04 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP04_Outputs_605,               "EXP04 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP04_Outputs_606,               "EXP04 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP04_Outputs_607,               "EXP04 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP04_Outputs_608,               "EXP04 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP05_Outputs_601,               "EXP05 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP05_Outputs_602,               "EXP05 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP05_Outputs_603,               "EXP05 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP05_Outputs_604,               "EXP05 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP05_Outputs_605,               "EXP05 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP05_Outputs_606,               "EXP05 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP05_Outputs_607,               "EXP05 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP05_Outputs_608,               "EXP05 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP06_Outputs_601,               "EXP06 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP06_Outputs_602,               "EXP06 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP06_Outputs_603,               "EXP06 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP06_Outputs_604,               "EXP06 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP06_Outputs_605,               "EXP06 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP06_Outputs_606,               "EXP06 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP06_Outputs_607,               "EXP06 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP06_Outputs_608,               "EXP06 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP07_Outputs_601,               "EXP07 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP07_Outputs_602,               "EXP07 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP07_Outputs_603,               "EXP07 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP07_Outputs_604,               "EXP07 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP07_Outputs_605,               "EXP07 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP07_Outputs_606,               "EXP07 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP07_Outputs_607,               "EXP07 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP07_Outputs_608,               "EXP07 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP08_Outputs_601,               "EXP08 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP08_Outputs_602,               "EXP08 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP08_Outputs_603,               "EXP08 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP08_Outputs_604,               "EXP08 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP08_Outputs_605,               "EXP08 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP08_Outputs_606,               "EXP08 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP08_Outputs_607,               "EXP08 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP08_Outputs_608,               "EXP08 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP09_Outputs_601,               "EXP09 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP09_Outputs_602,               "EXP09 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP09_Outputs_603,               "EXP09 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP09_Outputs_604,               "EXP09 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP09_Outputs_605,               "EXP09 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP09_Outputs_606,               "EXP09 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP09_Outputs_607,               "EXP09 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP09_Outputs_608,               "EXP09 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP10_Outputs_601,               "EXP10 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP10_Outputs_602,               "EXP10 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP10_Outputs_603,               "EXP10 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP10_Outputs_604,               "EXP10 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP10_Outputs_605,               "EXP10 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP10_Outputs_606,               "EXP10 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP10_Outputs_607,               "EXP10 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP10_Outputs_608,               "EXP10 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP11_Outputs_601,               "EXP11 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP11_Outputs_602,               "EXP11 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP11_Outputs_603,               "EXP11 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP11_Outputs_604,               "EXP11 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP11_Outputs_605,               "EXP11 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP11_Outputs_606,               "EXP11 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP11_Outputs_607,               "EXP11 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP11_Outputs_608,               "EXP11 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP12_Outputs_601,               "EXP12 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP12_Outputs_602,               "EXP12 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP12_Outputs_603,               "EXP12 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP12_Outputs_604,               "EXP12 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP12_Outputs_605,               "EXP12 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP12_Outputs_606,               "EXP12 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP12_Outputs_607,               "EXP12 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP12_Outputs_608,               "EXP12 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP13_Outputs_601,               "EXP13 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP13_Outputs_602,               "EXP13 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP13_Outputs_603,               "EXP13 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP13_Outputs_604,               "EXP13 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP13_Outputs_605,               "EXP13 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP13_Outputs_606,               "EXP13 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP13_Outputs_607,               "EXP13 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP13_Outputs_608,               "EXP13 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP14_Outputs_601,               "EXP14 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP14_Outputs_602,               "EXP14 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP14_Outputs_603,               "EXP14 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP14_Outputs_604,               "EXP14 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP14_Outputs_605,               "EXP14 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP14_Outputs_606,               "EXP14 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP14_Outputs_607,               "EXP14 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP14_Outputs_608,               "EXP14 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP15_Outputs_601,               "EXP15 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP15_Outputs_602,               "EXP15 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP15_Outputs_603,               "EXP15 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP15_Outputs_604,               "EXP15 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP15_Outputs_605,               "EXP15 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP15_Outputs_606,               "EXP15 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP15_Outputs_607,               "EXP15 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP15_Outputs_608,               "EXP15 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP16_Outputs_601,               "EXP16 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP16_Outputs_602,               "EXP16 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP16_Outputs_603,               "EXP16 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP16_Outputs_604,               "EXP16 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP16_Outputs_605,               "EXP16 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP16_Outputs_606,               "EXP16 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP16_Outputs_607,               "EXP16 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP16_Outputs_608,               "EXP16 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP17_Outputs_601,               "EXP17 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP17_Outputs_602,               "EXP17 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP17_Outputs_603,               "EXP17 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP17_Outputs_604,               "EXP17 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP17_Outputs_605,               "EXP17 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP17_Outputs_606,               "EXP17 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP17_Outputs_607,               "EXP17 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP17_Outputs_608,               "EXP17 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP18_Outputs_601,               "EXP18 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP18_Outputs_602,               "EXP18 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP18_Outputs_603,               "EXP18 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP18_Outputs_604,               "EXP18 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP18_Outputs_605,               "EXP18 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP18_Outputs_606,               "EXP18 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP18_Outputs_607,               "EXP18 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP18_Outputs_608,               "EXP18 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP19_Outputs_601,               "EXP19 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP19_Outputs_602,               "EXP19 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP19_Outputs_603,               "EXP19 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP19_Outputs_604,               "EXP19 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP19_Outputs_605,               "EXP19 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP19_Outputs_606,               "EXP19 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP19_Outputs_607,               "EXP19 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP19_Outputs_608,               "EXP19 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP20_Outputs_601,               "EXP20 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP20_Outputs_602,               "EXP20 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP20_Outputs_603,               "EXP20 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP20_Outputs_604,               "EXP20 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP20_Outputs_605,               "EXP20 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP20_Outputs_606,               "EXP20 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP20_Outputs_607,               "EXP20 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP20_Outputs_608,               "EXP20 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP21_Outputs_601,               "EXP21 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP21_Outputs_602,               "EXP21 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP21_Outputs_603,               "EXP21 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP21_Outputs_604,               "EXP21 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP21_Outputs_605,               "EXP21 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP21_Outputs_606,               "EXP21 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP21_Outputs_607,               "EXP21 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP21_Outputs_608,               "EXP21 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP22_Outputs_601,               "EXP22 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP22_Outputs_602,               "EXP22 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP22_Outputs_603,               "EXP22 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP22_Outputs_604,               "EXP22 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP22_Outputs_605,               "EXP22 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP22_Outputs_606,               "EXP22 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP22_Outputs_607,               "EXP22 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP22_Outputs_608,               "EXP22 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP23_Outputs_601,               "EXP23 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP23_Outputs_602,               "EXP23 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP23_Outputs_603,               "EXP23 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP23_Outputs_604,               "EXP23 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP23_Outputs_605,               "EXP23 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP23_Outputs_606,               "EXP23 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP23_Outputs_607,               "EXP23 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP23_Outputs_608,               "EXP23 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP24_Outputs_601,               "EXP24 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP24_Outputs_602,               "EXP24 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP24_Outputs_603,               "EXP24 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP24_Outputs_604,               "EXP24 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP24_Outputs_605,               "EXP24 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP24_Outputs_606,               "EXP24 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP24_Outputs_607,               "EXP24 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP24_Outputs_608,               "EXP24 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP25_Outputs_601,               "EXP25 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP25_Outputs_602,               "EXP25 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP25_Outputs_603,               "EXP25 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP25_Outputs_604,               "EXP25 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP25_Outputs_605,               "EXP25 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP25_Outputs_606,               "EXP25 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP25_Outputs_607,               "EXP25 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP25_Outputs_608,               "EXP25 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP26_Outputs_601,               "EXP26 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP26_Outputs_602,               "EXP26 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP26_Outputs_603,               "EXP26 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP26_Outputs_604,               "EXP26 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP26_Outputs_605,               "EXP26 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP26_Outputs_606,               "EXP26 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP26_Outputs_607,               "EXP26 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP26_Outputs_608,               "EXP26 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP27_Outputs_601,               "EXP27 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP27_Outputs_602,               "EXP27 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP27_Outputs_603,               "EXP27 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP27_Outputs_604,               "EXP27 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP27_Outputs_605,               "EXP27 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP27_Outputs_606,               "EXP27 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP27_Outputs_607,               "EXP27 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP27_Outputs_608,               "EXP27 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP28_Outputs_601,               "EXP28 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP28_Outputs_602,               "EXP28 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP28_Outputs_603,               "EXP28 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP28_Outputs_604,               "EXP28 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP28_Outputs_605,               "EXP28 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP28_Outputs_606,               "EXP28 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP28_Outputs_607,               "EXP28 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP28_Outputs_608,               "EXP28 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP29_Outputs_601,               "EXP29 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP29_Outputs_602,               "EXP29 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP29_Outputs_603,               "EXP29 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP29_Outputs_604,               "EXP29 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP29_Outputs_605,               "EXP29 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP29_Outputs_606,               "EXP29 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP29_Outputs_607,               "EXP29 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP29_Outputs_608,               "EXP29 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP30_Outputs_601,               "EXP30 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP30_Outputs_602,               "EXP30 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP30_Outputs_603,               "EXP30 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP30_Outputs_604,               "EXP30 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP30_Outputs_605,               "EXP30 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP30_Outputs_606,               "EXP30 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP30_Outputs_607,               "EXP30 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP30_Outputs_608,               "EXP30 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP31_Outputs_601,               "EXP31 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP31_Outputs_602,               "EXP31 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP31_Outputs_603,               "EXP31 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP31_Outputs_604,               "EXP31 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP31_Outputs_605,               "EXP31 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP31_Outputs_606,               "EXP31 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP31_Outputs_607,               "EXP31 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP31_Outputs_608,               "EXP31 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP32_Outputs_601,               "EXP32 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP32_Outputs_602,               "EXP32 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP32_Outputs_603,               "EXP32 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP32_Outputs_604,               "EXP32 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP32_Outputs_605,               "EXP32 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP32_Outputs_606,               "EXP32 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP32_Outputs_607,               "EXP32 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP32_Outputs_608,               "EXP32 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP33_Outputs_601,               "EXP33 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP33_Outputs_602,               "EXP33 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP33_Outputs_603,               "EXP33 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP33_Outputs_604,               "EXP33 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP33_Outputs_605,               "EXP33 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP33_Outputs_606,               "EXP33 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP33_Outputs_607,               "EXP33 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP33_Outputs_608,               "EXP33 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP34_Outputs_601,               "EXP34 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP34_Outputs_602,               "EXP34 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP34_Outputs_603,               "EXP34 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP34_Outputs_604,               "EXP34 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP34_Outputs_605,               "EXP34 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP34_Outputs_606,               "EXP34 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP34_Outputs_607,               "EXP34 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP34_Outputs_608,               "EXP34 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP35_Outputs_601,               "EXP35 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP35_Outputs_602,               "EXP35 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP35_Outputs_603,               "EXP35 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP35_Outputs_604,               "EXP35 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP35_Outputs_605,               "EXP35 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP35_Outputs_606,               "EXP35 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP35_Outputs_607,               "EXP35 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP35_Outputs_608,               "EXP35 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP36_Outputs_601,               "EXP36 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP36_Outputs_602,               "EXP36 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP36_Outputs_603,               "EXP36 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP36_Outputs_604,               "EXP36 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP36_Outputs_605,               "EXP36 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP36_Outputs_606,               "EXP36 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP36_Outputs_607,               "EXP36 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP36_Outputs_608,               "EXP36 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP37_Outputs_601,               "EXP37 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP37_Outputs_602,               "EXP37 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP37_Outputs_603,               "EXP37 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP37_Outputs_604,               "EXP37 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP37_Outputs_605,               "EXP37 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP37_Outputs_606,               "EXP37 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP37_Outputs_607,               "EXP37 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP37_Outputs_608,               "EXP37 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP38_Outputs_601,               "EXP38 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP38_Outputs_602,               "EXP38 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP38_Outputs_603,               "EXP38 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP38_Outputs_604,               "EXP38 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP38_Outputs_605,               "EXP38 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP38_Outputs_606,               "EXP38 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP38_Outputs_607,               "EXP38 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP38_Outputs_608,               "EXP38 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP39_Outputs_601,               "EXP39 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP39_Outputs_602,               "EXP39 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP39_Outputs_603,               "EXP39 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP39_Outputs_604,               "EXP39 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP39_Outputs_605,               "EXP39 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP39_Outputs_606,               "EXP39 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP39_Outputs_607,               "EXP39 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP39_Outputs_608,               "EXP39 OUT 8"                     ) \
   PARAM_STR_16BIT( EXP40_Outputs_601,               "EXP40 OUT 1"                     ) \
   PARAM_STR_16BIT( EXP40_Outputs_602,               "EXP40 OUT 2"                     ) \
   PARAM_STR_16BIT( EXP40_Outputs_603,               "EXP40 OUT 3"                     ) \
   PARAM_STR_16BIT( EXP40_Outputs_604,               "EXP40 OUT 4"                     ) \
   PARAM_STR_16BIT( EXP40_Outputs_605,               "EXP40 OUT 5"                     ) \
   PARAM_STR_16BIT( EXP40_Outputs_606,               "EXP40 OUT 6"                     ) \
   PARAM_STR_16BIT( EXP40_Outputs_607,               "EXP40 OUT 7"                     ) \
   PARAM_STR_16BIT( EXP40_Outputs_608,               "EXP40 OUT 8"                     ) \
   PARAM_STR_16BIT( NTS_VEL_P1_0,                    "NTS VEL P1-0"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P1_1,                    "NTS VEL P1-1"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P1_2,                    "NTS VEL P1-2"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P1_3,                    "NTS VEL P1-3"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P1_4,                    "NTS VEL P1-4"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P1_5,                    "NTS VEL P1-5"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P1_6,                    "NTS VEL P1-6"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P1_7,                    "NTS VEL P1-7"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P2_0,                    "NTS VEL P2-0"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P2_1,                    "NTS VEL P2-1"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P2_2,                    "NTS VEL P2-2"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P2_3,                    "NTS VEL P2-3"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P2_4,                    "NTS VEL P2-4"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P2_5,                    "NTS VEL P2-5"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P2_6,                    "NTS VEL P2-6"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P2_7,                    "NTS VEL P2-7"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P3_0,                    "NTS VEL P3-0"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P3_1,                    "NTS VEL P3-1"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P3_2,                    "NTS VEL P3-2"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P3_3,                    "NTS VEL P3-3"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P3_4,                    "NTS VEL P3-4"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P3_5,                    "NTS VEL P3-5"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P3_6,                    "NTS VEL P3-6"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P3_7,                    "NTS VEL P3-7"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P4_0,                    "NTS VEL P4-0"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P4_1,                    "NTS VEL P4-1"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P4_2,                    "NTS VEL P4-2"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P4_3,                    "NTS VEL P4-3"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P4_4,                    "NTS VEL P4-4"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P4_5,                    "NTS VEL P4-5"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P4_6,                    "NTS VEL P4-6"                    ) \
   PARAM_STR_16BIT( NTS_VEL_P4_7,                    "NTS VEL P4-7"                    ) \
   PARAM_STR_16BIT( NTS_POS_P1_0,                    "NTS POS P1-0"                    ) \
   PARAM_STR_16BIT( NTS_POS_P1_1,                    "NTS POS P1-1"                    ) \
   PARAM_STR_16BIT( NTS_POS_P1_2,                    "NTS POS P1-2"                    ) \
   PARAM_STR_16BIT( NTS_POS_P1_3,                    "NTS POS P1-3"                    ) \
   PARAM_STR_16BIT( NTS_POS_P1_4,                    "NTS POS P1-4"                    ) \
   PARAM_STR_16BIT( NTS_POS_P1_5,                    "NTS POS P1-5"                    ) \
   PARAM_STR_16BIT( NTS_POS_P1_6,                    "NTS POS P1-6"                    ) \
   PARAM_STR_16BIT( NTS_POS_P1_7,                    "NTS POS P1-7"                    ) \
   PARAM_STR_16BIT( NTS_POS_P2_0,                    "NTS POS P2-0"                    ) \
   PARAM_STR_16BIT( NTS_POS_P2_1,                    "NTS POS P2-1"                    ) \
   PARAM_STR_16BIT( NTS_POS_P2_2,                    "NTS POS P2-2"                    ) \
   PARAM_STR_16BIT( NTS_POS_P2_3,                    "NTS POS P2-3"                    ) \
   PARAM_STR_16BIT( NTS_POS_P2_4,                    "NTS POS P2-4"                    ) \
   PARAM_STR_16BIT( NTS_POS_P2_5,                    "NTS POS P2-5"                    ) \
   PARAM_STR_16BIT( NTS_POS_P2_6,                    "NTS POS P2-6"                    ) \
   PARAM_STR_16BIT( NTS_POS_P2_7,                    "NTS POS P2-7"                    ) \
   PARAM_STR_16BIT( NTS_POS_P3_0,                    "NTS POS P3-0"                    ) \
   PARAM_STR_16BIT( NTS_POS_P3_1,                    "NTS POS P3-1"                    ) \
   PARAM_STR_16BIT( NTS_POS_P3_2,                    "NTS POS P3-2"                    ) \
   PARAM_STR_16BIT( NTS_POS_P3_3,                    "NTS POS P3-3"                    ) \
   PARAM_STR_16BIT( NTS_POS_P3_4,                    "NTS POS P3-4"                    ) \
   PARAM_STR_16BIT( NTS_POS_P3_5,                    "NTS POS P3-5"                    ) \
   PARAM_STR_16BIT( NTS_POS_P3_6,                    "NTS POS P3-6"                    ) \
   PARAM_STR_16BIT( NTS_POS_P3_7,                    "NTS POS P3-7"                    ) \
   PARAM_STR_16BIT( NTS_POS_P4_0,                    "NTS POS P4-0"                    ) \
   PARAM_STR_16BIT( NTS_POS_P4_1,                    "NTS POS P4-1"                    ) \
   PARAM_STR_16BIT( NTS_POS_P4_2,                    "NTS POS P4-2"                    ) \
   PARAM_STR_16BIT( NTS_POS_P4_3,                    "NTS POS P4-3"                    ) \
   PARAM_STR_16BIT( NTS_POS_P4_4,                    "NTS POS P4-4"                    ) \
   PARAM_STR_16BIT( NTS_POS_P4_5,                    "NTS POS P4-5"                    ) \
   PARAM_STR_16BIT( NTS_POS_P4_6,                    "NTS POS P4-6"                    ) \
   PARAM_STR_16BIT( NTS_POS_P4_7,                    "NTS POS P4-7"                    ) \
   PARAM_STR_16BIT( UNUSED_16BIT_848,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_849,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_850,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_851,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_852,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_853,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_854,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_855,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_856,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_857,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_858,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_859,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_860,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_861,                "NA"                              ) \
   PARAM_STR_16BIT( Acceptance_AscOrDescSpeed,       "Acceptance A/D SPD "             ) \
   PARAM_STR_16BIT( UNUSED_16BIT_863,                "NA"                              ) \
   PARAM_STR_16BIT( Acceptance_BufferSpeed,          "Acceptance Buffer SPD "          ) \
   PARAM_STR_16BIT( Acceptance_SlideDistance,        "Acceptance Slide Distance"       ) \
   PARAM_STR_16BIT( Acceptance_EBrk_SlideDistance,   "Acceptance_EBrk_SlideDistance"   ) \
   PARAM_STR_16BIT( UNUSED_16BIT_867,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_868,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_869,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_870,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_871,                "NA"                              ) \
   PARAM_STR_16BIT( ContractSpeed,                   "Contract SPD "                   ) \
   PARAM_STR_16BIT( InspectionSpeed,                 "Inspection SPD "                 ) \
   PARAM_STR_16BIT( LearnSpeed,                      "Learn SPD "                      ) \
   PARAM_STR_16BIT( InspectionTerminalSpeed,         "Inspection Terminal SPD "        ) \
   PARAM_STR_16BIT( LockClipTime_10ms,               "LockClipTime (10 ms)"            ) \
   PARAM_STR_16BIT( MinAccelSpeed,                   "Min Accel SPD "                  ) \
   PARAM_STR_16BIT( EPowerSpeed_fpm,                 "EPower SPD fpm"                  ) \
   PARAM_STR_16BIT( UNUSED_16BIT_879,                "NA"                              ) \
   PARAM_STR_16BIT( BrakePickDelay_Insp_ms,          "BrakePickDelay Insp (ms)"        ) \
   PARAM_STR_16BIT( BrakePickDelay_Auto_ms,          "BrakePickDelay Auto (ms)"        ) \
   PARAM_STR_16BIT( AccelDelay_ms,                   "AccelDelay Auto (ms)"            ) \
   PARAM_STR_16BIT( AccelDelay_Insp_ms,              "AccelDelay Insp (ms)"            ) \
   PARAM_STR_16BIT( UNUSED_16BIT_884,                "NA"                              ) \
   PARAM_STR_16BIT( BrakeDropDelay_Auto_1ms,         "BrakeDropDelay Auto (ms)"        ) \
   PARAM_STR_16BIT( BrakeDropDelay_Insp_1ms,         "BrakeDropDelay Insp (ms)"        ) \
   PARAM_STR_16BIT( DriveDropDelay_Auto_1ms,         "DriveDropDelay Auto (ms)"        ) \
   PARAM_STR_16BIT( DriveDropDelay_Insp_1ms,         "DriveDropDelay Insp (ms)"        ) \
   PARAM_STR_16BIT( MotorDropDelay_Auto_1ms,         "MotorDropDelay Auto (ms)"        ) \
   PARAM_STR_16BIT( MotorDropDelay_Insp_1ms,         "MotorDropDelay Insp (ms)"        ) \
   PARAM_STR_16BIT( EBrakeDropDelay_Auto_1ms,        "EBrakeDropDelay Auto (ms)"       ) \
   PARAM_STR_16BIT( EBrakeDropDelay_Insp_1ms,        "EBrakeDropDelay Insp (ms)"       ) \
   PARAM_STR_16BIT( B2DropDelay_Auto_1ms,            "B2DropDelay Auto (ms)"           ) \
   PARAM_STR_16BIT( B2DropDelay_Insp_1ms,            "B2DropDelay Insp (ms)"           ) \
   PARAM_STR_16BIT( UNUSED_16BIT_895,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_896,                "NA"                              ) \
   PARAM_STR_16BIT( SoftLimitDistance_UP,            "Soft Limit Distance Up (ft)"     ) \
   PARAM_STR_16BIT( SoftLimitDistance_DN,            "Soft Limit Distance Down (ft)"   ) \
   PARAM_STR_16BIT( UNUSED_16BIT_899,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_900,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_901,                "NA"                              ) \
   PARAM_STR_16BIT( SpeedDev_Threshold,              "SPD Dev Threshold"               ) \
   PARAM_STR_16BIT( SpeedDev_Timeout_10ms,           "SPD Dev Timeout (10 ms)"         ) \
   PARAM_STR_16BIT( SpeedDev_OffsetPercent,          "SPD Dev Percent"                 ) \
   PARAM_STR_16BIT( TractionLoss_Threshold,          "Traction Loss Threshold"         ) \
   PARAM_STR_16BIT( TractionLoss_Timeout_10ms,       "Traction Loss Timeout (10 ms)"   ) \
   PARAM_STR_16BIT( TractionLoss_OffsetPercent,      "Traction Loss Percent"           ) \
   PARAM_STR_16BIT( LevelingSpeed,                   "Leveling SPD "                   ) \
   PARAM_STR_16BIT( UNUSED_16BIT_909,                "NA"                              ) \
   PARAM_STR_16BIT( PreOpeningDistance,              "PreOpeningDistance"              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_911,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_912,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_913,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_914,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_915,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_916,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_917,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_918,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_919,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_920,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_921,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_922,                "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_923,                "NA"                              ) \
   PARAM_STR_16BIT( Time_Violation_1ms,              "Module Time Violation (ms)"      ) \
   PARAM_STR_16BIT( UNUSED_16BIT_925,                "NA"                              ) \
   PARAM_STR_16BIT( ETSL_CameraOffset_05mm,          "ETSL Camera Offset"              ) \
   PARAM_STR_16BIT( BufferDistance_05mm,             "BufferDistance_05mm"             ) \
   PARAM_STR_16BIT( SecuredCheckInBitmap_F0,         "Front Check In Security 0"       ) \
   PARAM_STR_16BIT( SecuredCheckInBitmap_F1,         "Front Check In Security 1"       ) \
   PARAM_STR_16BIT( SecuredCheckInBitmap_F2,         "Front Check In Security 2"       ) \
   PARAM_STR_16BIT( SecuredCheckInBitmap_F3,         "Front Check In Security 3"       ) \
   PARAM_STR_16BIT( SecuredCheckInBitmap_F4,         "Front Check In Security 4"       ) \
   PARAM_STR_16BIT( SecuredCheckInBitmap_F5,         "Front Check In Security 5"       ) \
   PARAM_STR_16BIT( SecuredCheckInBitmap_R0,         "Rear Check In Security 0"        ) \
   PARAM_STR_16BIT( SecuredCheckInBitmap_R1,         "Rear Check In Security 1"        ) \
   PARAM_STR_16BIT( SecuredCheckInBitmap_R2,         "Rear Check In Security 2"        ) \
   PARAM_STR_16BIT( SecuredCheckInBitmap_R3,         "Rear Check In Security 3"        ) \
   PARAM_STR_16BIT( SecuredCheckInBitmap_R4,         "Rear Check In Security 4"        ) \
   PARAM_STR_16BIT( SecuredCheckInBitmap_R5,         "Rear Check In Security 5"        ) \
   PARAM_STR_16BIT( HallSecureMap_0,                 "Hall Secure Map 0"               ) \
   PARAM_STR_16BIT( HallSecureMap_1,                 "Hall Secure Map 1"               ) \
   PARAM_STR_16BIT( HallSecureMap_2,                 "Hall Secure Map 2"               ) \
   PARAM_STR_16BIT( HallSecureMap_3,                 "Hall Secure Map 3"               ) \
   PARAM_STR_16BIT( HallSecureMap_4,                 "Hall Secure Map 4"               ) \
   PARAM_STR_16BIT( HallSecureMap_5,                 "Hall Secure Map 5"               ) \
   PARAM_STR_16BIT( SwingDoorOpening_F_0,            "Swing Door Opening F 0"          ) \
   PARAM_STR_16BIT( SwingDoorOpening_F_1,            "Swing Door Opening F 1"          ) \
   PARAM_STR_16BIT( SwingDoorOpening_F_2,            "Swing Door Opening F 2"          ) \
   PARAM_STR_16BIT( SwingDoorOpening_F_3,            "Swing Door Opening F 3"          ) \
   PARAM_STR_16BIT( SwingDoorOpening_F_4,            "Swing Door Opening F 4"          ) \
   PARAM_STR_16BIT( SwingDoorOpening_F_5,            "Swing Door Opening F 5"          ) \
   PARAM_STR_16BIT( SwingDoorOpening_R_0,            "Swing Door Opening R 0"          ) \
   PARAM_STR_16BIT( SwingDoorOpening_R_1,            "Swing Door Opening R 1"          ) \
   PARAM_STR_16BIT( SwingDoorOpening_R_2,            "Swing Door Opening R 2"          ) \
   PARAM_STR_16BIT( SwingDoorOpening_R_3,            "Swing Door Opening R 3"          ) \
   PARAM_STR_16BIT( SwingDoorOpening_R_4,            "Swing Door Opening R 4"          ) \
   PARAM_STR_16BIT( SwingDoorOpening_R_5,            "Swing Door Opening R 5"          ) \
   PARAM_STR_16BIT( ShortFloorOpening_0,             "NA"                              ) \
   PARAM_STR_16BIT( ShortFloorOpening_1,             "NA"                              ) \
   PARAM_STR_16BIT( ShortFloorOpening_2,             "NA"                              ) \
   PARAM_STR_16BIT( ShortFloorOpening_3,             "NA"                              ) \
   PARAM_STR_16BIT( ShortFloorOpening_4,             "NA"                              ) \
   PARAM_STR_16BIT( ShortFloorOpening_5,             "NA"                              ) \
   PARAM_STR_16BIT( MedValveMaxRunDist_in,           "MED Valve Max Run Dist (in)"     ) \
   PARAM_STR_16BIT( LowValveMaxRunDist_in,           "LOW Valve Max Run Dist (in)"     ) \
   PARAM_STR_16BIT( LevelValveMaxRunDist_in,         "LEVEL Valve Max Run Dist (in)"   ) \
   PARAM_STR_16BIT( Speed1_THOLD_fpm,                "Speed1_THOLD_fpm"                ) \
   PARAM_STR_16BIT( Speed2_THOLD_fpm,                "Speed2_THOLD_fpm"                ) \
   PARAM_STR_16BIT( Speed3_THOLD_fpm,                "Speed3_THOLD_fpm"                ) \
   PARAM_STR_16BIT( Speed4_THOLD_fpm,                "Speed4_THOLD_fpm"                ) \
   PARAM_STR_16BIT( Speed5_THOLD_fpm,                "Speed5_THOLD_fpm"                ) \
   PARAM_STR_16BIT( Speed6_THOLD_fpm,                "Speed6_THOLD_fpm"                ) \
   PARAM_STR_16BIT( Speed7_THOLD_fpm,                "Speed7_THOLD_fpm"                ) \
   PARAM_STR_16BIT( Speed8_THOLD_fpm,                "Speed8_THOLD_fpm"                ) \
   PARAM_STR_16BIT( Speed9_THOLD_fpm,                "Speed9_THOLD_fpm"                ) \
   PARAM_STR_16BIT( Speed10_THOLD_fpm,               "Speed10_THOLD_fpm"               ) \
   PARAM_STR_16BIT( Speed11_THOLD_fpm,               "Speed11_THOLD_fpm"               ) \
   PARAM_STR_16BIT( Speed12_THOLD_fpm,               "Speed12_THOLD_fpm"               ) \
   PARAM_STR_16BIT( Speed13_THOLD_fpm,               "Speed13_THOLD_fpm"               ) \
   PARAM_STR_16BIT( Speed14_THOLD_fpm,               "Speed14_THOLD_fpm"               ) \
   PARAM_STR_16BIT( Speed15_THOLD_fpm,               "Speed15_THOLD_fpm"               ) \
   PARAM_STR_16BIT( UNUSED_16BIT_982,                "NA"                              ) \
   PARAM_STR_16BIT( AccessCodeFloor_1F,              "Access Code Floor 1F"            ) \
   PARAM_STR_16BIT( AccessCodeFloor_2F,              "Access Code Floor 2F"            ) \
   PARAM_STR_16BIT( AccessCodeFloor_3F,              "Access Code Floor 3F"            ) \
   PARAM_STR_16BIT( AccessCodeFloor_4F,              "Access Code Floor 4F"            ) \
   PARAM_STR_16BIT( AccessCodeFloor_5F,              "Access Code Floor 5F"            ) \
   PARAM_STR_16BIT( AccessCodeFloor_6F,              "Access Code Floor 6F"            ) \
   PARAM_STR_16BIT( AccessCodeFloor_7F,              "Access Code Floor 7F"            ) \
   PARAM_STR_16BIT( AccessCodeFloor_8F,              "Access Code Floor 8F"            ) \
   PARAM_STR_16BIT( AccessCodeFloor_1R,              "Access Code Floor 1R"            ) \
   PARAM_STR_16BIT( AccessCodeFloor_2R,              "Access Code Floor 2R"            ) \
   PARAM_STR_16BIT( AccessCodeFloor_3R,              "Access Code Floor 3R"            ) \
   PARAM_STR_16BIT( AccessCodeFloor_4R,              "Access Code Floor 4R"            ) \
   PARAM_STR_16BIT( AccessCodeFloor_5R,              "Access Code Floor 5R"            ) \
   PARAM_STR_16BIT( AccessCodeFloor_6R,              "Access Code Floor 6R"            ) \
   PARAM_STR_16BIT( AccessCodeFloor_7R,              "Access Code Floor 7R"            ) \
   PARAM_STR_16BIT( AccessCodeFloor_8R,              "Access Code Floor 8R"            ) \
   PARAM_STR_16BIT( Weekday_StartTime,               "Weekday Start Time"              ) \
   PARAM_STR_16BIT( Weekday_EndTime,                 "Weekday End Time"                ) \
   PARAM_STR_16BIT( Weekend_StartTime,               "Weekend Start Time"              ) \
   PARAM_STR_16BIT( Weekend_EndTime,                 "Weekend End Time"                ) \
   PARAM_STR_16BIT( Speed1_SlowdownDist_UP_05mm,     "Speed1_SlowdownDist_UP_05mm"     ) \
   PARAM_STR_16BIT( Speed2_SlowdownDist_UP_05mm,     "Speed2_SlowdownDist_UP_05mm"     ) \
   PARAM_STR_16BIT( Speed3_SlowdownDist_UP_05mm,     "Speed3_SlowdownDist_UP_05mm"     ) \
   PARAM_STR_16BIT( Speed4_SlowdownDist_UP_05mm,     "Speed4_SlowdownDist_UP_05mm"     ) \
   PARAM_STR_16BIT( Speed5_SlowdownDist_UP_05mm,     "Speed5_SlowdownDist_UP_05mm"     ) \
   PARAM_STR_16BIT( Speed6_SlowdownDist_UP_05mm,     "Speed6_SlowdownDist_UP_05mm"     ) \
   PARAM_STR_16BIT( Speed7_SlowdownDist_UP_05mm,     "Speed7_SlowdownDist_UP_05mm"     ) \
   PARAM_STR_16BIT( Speed8_SlowdownDist_UP_05mm,     "Speed8_SlowdownDist_UP_05mm"     ) \
   PARAM_STR_16BIT( Speed9_SlowdownDist_UP_05mm,     "Speed9_SlowdownDist_UP_05mm"     ) \
   PARAM_STR_16BIT( Speed10_SlowdownDist_UP_05mm,    "Speed10_SlowdownDist_UP_05mm"    ) \
   PARAM_STR_16BIT( Speed11_SlowdownDist_UP_05mm,    "Speed11_SlowdownDist_UP_05mm"    ) \
   PARAM_STR_16BIT( Speed12_SlowdownDist_UP_05mm,    "Speed12_SlowdownDist_UP_05mm"    ) \
   PARAM_STR_16BIT( Speed13_SlowdownDist_UP_05mm,    "Speed13_SlowdownDist_UP_05mm"    ) \
   PARAM_STR_16BIT( Speed14_SlowdownDist_UP_05mm,    "Speed14_SlowdownDist_UP_05mm"    ) \
   PARAM_STR_16BIT( Speed15_SlowdownDist_UP_05mm,    "Speed15_SlowdownDist_UP_05mm"    ) \
   PARAM_STR_16BIT( Speed16_SlowdownDist_UP_05mm,    "Speed16_SlowdownDist_UP_05mm"    ) \
   PARAM_STR_16BIT( Speed1_SlowdownDist_DN_05mm,     "Speed1_SlowdownDist_DN_05mm"     ) \
   PARAM_STR_16BIT( Speed2_SlowdownDist_DN_05mm,     "Speed2_SlowdownDist_DN_05mm"     ) \
   PARAM_STR_16BIT( Speed3_SlowdownDist_DN_05mm,     "Speed3_SlowdownDist_DN_05mm"     ) \
   PARAM_STR_16BIT( Speed4_SlowdownDist_DN_05mm,     "Speed4_SlowdownDist_DN_05mm"     ) \
   PARAM_STR_16BIT( Speed5_SlowdownDist_DN_05mm,     "Speed5_SlowdownDist_DN_05mm"     ) \
   PARAM_STR_16BIT( Speed6_SlowdownDist_DN_05mm,     "Speed6_SlowdownDist_DN_05mm"     ) \
   PARAM_STR_16BIT( Speed7_SlowdownDist_DN_05mm,     "Speed7_SlowdownDist_DN_05mm"     ) \
   PARAM_STR_16BIT( Speed8_SlowdownDist_DN_05mm,     "Speed8_SlowdownDist_DN_05mm"     ) \
   PARAM_STR_16BIT( Speed9_SlowdownDist_DN_05mm,     "Speed9_SlowdownDist_DN_05mm"     ) \
   PARAM_STR_16BIT( Speed10_SlowdownDist_DN_05mm,    "Speed10_SlowdownDist_DN_05mm"    ) \
   PARAM_STR_16BIT( Speed11_SlowdownDist_DN_05mm,    "Speed11_SlowdownDist_DN_05mm"    ) \
   PARAM_STR_16BIT( Speed12_SlowdownDist_DN_05mm,    "Speed12_SlowdownDist_DN_05mm"    ) \
   PARAM_STR_16BIT( Speed13_SlowdownDist_DN_05mm,    "Speed13_SlowdownDist_DN_05mm"    ) \
   PARAM_STR_16BIT( Speed14_SlowdownDist_DN_05mm,    "Speed14_SlowdownDist_DN_05mm"    ) \
   PARAM_STR_16BIT( Speed15_SlowdownDist_DN_05mm,    "Speed15_SlowdownDist_DN_05mm"    ) \
   PARAM_STR_16BIT( Speed16_SlowdownDist_DN_05mm,    "Speed16_SlowdownDist_DN_05mm"    ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1035,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1036,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1037,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1038,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1039,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1040,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1041,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1042,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1043,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1044,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1045,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1046,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1047,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1048,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1049,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1050,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1051,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1052,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1053,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1054,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1055,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1056,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1057,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1058,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1059,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1060,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1061,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1062,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1063,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1064,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1065,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1066,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1067,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1068,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1069,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1070,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1071,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1072,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1073,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1074,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1075,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1076,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1077,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1078,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1079,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1080,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1081,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1082,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1083,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1084,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1085,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1086,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1087,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1088,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1089,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1090,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1091,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1092,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1093,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1094,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1095,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1096,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1097,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1098,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1099,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1100,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1101,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1102,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1103,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1104,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1105,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1106,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1107,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1108,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1109,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1110,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1111,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1112,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1113,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1114,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1115,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1116,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1117,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1118,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1119,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1120,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1121,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1122,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1123,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1124,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1125,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1126,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1127,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1128,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1129,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1130,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1131,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1132,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1133,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1134,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1135,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1136,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1137,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1138,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1139,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1140,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1141,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1142,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1143,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1144,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1145,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1146,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1147,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1148,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1149,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1150,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1151,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1152,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1153,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1154,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1155,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1156,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1157,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1158,               "NA"                              ) \
   PARAM_STR_16BIT( UNUSED_16BIT_1159,               "NA"                              )

/* PARAM_DEFAULT_ENTRY_16BIT(A, B) FORMAT:
 *    A = Parameter Byte Map Index 
 *    B = (GRP1) Default All 8-Bit Map
 *    C = (GRP2) Default Learned Floors 8-Bit Map
 *    D = (GRP3) Default S-Curve 8-Bit Map
 *    E = (GRP4) Default Run Timers 8-Bit Map
 *    F = (GRP5) Default (UNUSED) 8-Bit Map
 *    G = (GRP6) Default (UNUSED) 8-Bit Map
 *    H = (GRP7) Default (UNUSED) 8-Bit Map
 *    I = (GRP8) Default (UNUSED) 8-Bit Map
 * */
/* Parameter Default Table Definition */
#define PARAM_DEFAULT_TABLE_16BIT(PARAM_DEFAULT_ENTRY_16BIT)  \
   PARAM_DEFAULT_ENTRY_16BIT( 0     , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 1     , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 2     , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 3     , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 4     , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 5     , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 6     , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 7     , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 8     , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 9     , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 10    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 11    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 12    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 13    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 14    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 15    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 16    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 17    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 18    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 19    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 20    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 21    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 22    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 23    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 24    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 25    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 26    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 27    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 28    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 29    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 30    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 31    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 32    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 33    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 34    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 35    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 36    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 37    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 38    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 39    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 40    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 41    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 42    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 43    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 44    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 45    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 46    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 47    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 48    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 49    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 50    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 51    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 52    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 53    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 54    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 55    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 56    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 57    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 58    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 59    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 60    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 61    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 62    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 63    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 64    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 65    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 66    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 67    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 68    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 69    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 70    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 71    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 72    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 73    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 74    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 75    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 76    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 77    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 78    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 79    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 80    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 81    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 82    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 83    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 84    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 85    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 86    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 87    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 88    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 89    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 90    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 91    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 92    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 93    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 94    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 95    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 96    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 97    , 0   , 0   , 0   , 0   , 255 , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 98    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 99    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 100   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 101   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 102   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 103   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 104   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 105   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 106   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 107   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 108   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 109   , 223 , 0   , 32  , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 110   , 16  , 0   , 0   , 239 , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 111   , 128 , 0   , 0   , 127 , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 112   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 113   , 239 , 0   , 16  , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 114   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 115   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 116   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 117   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 118   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 119   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 120   , 15  , 0   , 240 , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 121   , 0   , 0   , 255 , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 122   , 192 , 0   , 63  , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 123   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 124   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 125   , 7   , 0   , 248 , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 126   , 0   , 0   , 255 , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 127   , 0   , 0   , 255 , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 128   , 0   , 0   , 255 , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 129   , 248 , 0   , 7   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 130   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 131   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 132   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 133   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 134   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 135   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 136   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 137   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 138   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 139   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 140   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 141   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 142   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 143   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_16BIT( 144   , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    )

/* Macros for accessing table elements */
#define EXPAND_PARAM_STRING_TABLE_16BIT_AS_ARRAY(A, B) B,

#define EXPAND_PARAM_DEFAULT_TABLE_16BIT_AS_DEFAULT_GRP_1(A, B, C, D, E, F, G, H, I) B,
#define EXPAND_PARAM_DEFAULT_TABLE_16BIT_AS_DEFAULT_GRP_2(A, B, C, D, E, F, G, H, I) C,
#define EXPAND_PARAM_DEFAULT_TABLE_16BIT_AS_DEFAULT_GRP_3(A, B, C, D, E, F, G, H, I) D,
#define EXPAND_PARAM_DEFAULT_TABLE_16BIT_AS_DEFAULT_GRP_4(A, B, C, D, E, F, G, H, I) E,
#define EXPAND_PARAM_DEFAULT_TABLE_16BIT_AS_DEFAULT_GRP_5(A, B, C, D, E, F, G, H, I) F,
#define EXPAND_PARAM_DEFAULT_TABLE_16BIT_AS_DEFAULT_GRP_6(A, B, C, D, E, F, G, H, I) G,
#define EXPAND_PARAM_DEFAULT_TABLE_16BIT_AS_DEFAULT_GRP_7(A, B, C, D, E, F, G, H, I) H,
#define EXPAND_PARAM_DEFAULT_TABLE_16BIT_AS_DEFAULT_GRP_8(A, B, C, D, E, F, G, H, I) I,

#endif