#ifndef SMARTRISE_PARAMETERS_32BIT
#define SMARTRISE_PARAMETERS_32BIT

/* NOTE: FILE UPDATED VIA LIB_SYSTEM - SUPPORT,
 * Edit SystemParameters.xlsx run Update_Parameters.bat
 * to automatically update this file. */

enum en_32bit_params
{
   enPARAM32__OpeningBitmapF_0,          // 32-0000
   enPARAM32__OpeningBitmapF_1,          // 32-0001
   enPARAM32__OpeningBitmapF_2,          // 32-0002
   enPARAM32__UNUSED_32BIT_3,            // 32-0003
   enPARAM32__OpeningBitmapR_0,          // 32-0004
   enPARAM32__OpeningBitmapR_1,          // 32-0005
   enPARAM32__OpeningBitmapR_2,          // 32-0006
   enPARAM32__UNUSED_32BIT_7,            // 32-0007
   enPARAM32__SecureKeyedBitmapF_0,      // 32-0008
   enPARAM32__SecureKeyedBitmapF_1,      // 32-0009
   enPARAM32__SecureKeyedBitmapF_2,      // 32-0010
   enPARAM32__UNUSED_32BIT_11,           // 32-0011
   enPARAM32__SecureKeyedBitmapR_0,      // 32-0012
   enPARAM32__SecureKeyedBitmapR_1,      // 32-0013
   enPARAM32__SecureKeyedBitmapR_2,      // 32-0014
   enPARAM32__UNUSED_32BIT_15,           // 32-0015
   enPARAM32__SecureTimedBitmapF_0,      // 32-0016
   enPARAM32__SecureTimedBitmapF_1,      // 32-0017
   enPARAM32__SecureTimedBitmapF_2,      // 32-0018
   enPARAM32__UNUSED_32BIT_19,           // 32-0019
   enPARAM32__SecureTimedBitmapR_0,      // 32-0020
   enPARAM32__SecureTimedBitmapR_1,      // 32-0021
   enPARAM32__SecureTimedBitmapR_2,      // 32-0022
   enPARAM32__SabbathDestinationUp_0,    // 32-0023
   enPARAM32__SabbathDestinationUp_1,    // 32-0024
   enPARAM32__SabbathDestinationUp_2,    // 32-0025
   enPARAM32__SabbathDestinationDown_0,  // 32-0026
   enPARAM32__SabbathDestinationDown_1,  // 32-0027
   enPARAM32__SabbathDestinationDown_2,  // 32-0028
   enPARAM32__UNUSED_32BIT_29,           // 32-0029
   enPARAM32__UNUSED_32BIT_30,           // 32-0030
   enPARAM32__UNUSED_32BIT_31,           // 32-0031
   enPARAM32__WanderGuardMask0,          // 32-0032
   enPARAM32__WanderGuardMask1,          // 32-0033
   enPARAM32__WanderGuardMask2,          // 32-0034
   enPARAM32__UNUSED_32BIT_35,           // 32-0035
   enPARAM32__SabbathOpeningF_0,         // 32-0036
   enPARAM32__SabbathOpeningF_1,         // 32-0037
   enPARAM32__SabbathOpeningF_2,         // 32-0038
   enPARAM32__SabbathOpeningR_0,         // 32-0039
   enPARAM32__SabbathOpeningR_1,         // 32-0040
   enPARAM32__SabbathOpeningR_2,         // 32-0041

   NUM_32BIT_PARAMS
};

#define PARAM_STRINGS_32BIT(PARAM_STR_32BIT)  \
   PARAM_STR_32BIT( OpeningBitmapF_0,           "Front Opening Map 0"           ) \
   PARAM_STR_32BIT( OpeningBitmapF_1,           "Front Opening Map 1"           ) \
   PARAM_STR_32BIT( OpeningBitmapF_2,           "Front Opening Map 2"           ) \
   PARAM_STR_32BIT( UNUSED_32BIT_3,             "NA"                            ) \
   PARAM_STR_32BIT( OpeningBitmapR_0,           "Rear Opening Map 0"            ) \
   PARAM_STR_32BIT( OpeningBitmapR_1,           "Rear Opening Map 1"            ) \
   PARAM_STR_32BIT( OpeningBitmapR_2,           "Rear Opening Map 2"            ) \
   PARAM_STR_32BIT( UNUSED_32BIT_7,             "NA"                            ) \
   PARAM_STR_32BIT( SecureKeyedBitmapF_0,       "Front Security Map 0"          ) \
   PARAM_STR_32BIT( SecureKeyedBitmapF_1,       "Front Security Map 1"          ) \
   PARAM_STR_32BIT( SecureKeyedBitmapF_2,       "Front Security Map 2"          ) \
   PARAM_STR_32BIT( UNUSED_32BIT_11,            "NA"                            ) \
   PARAM_STR_32BIT( SecureKeyedBitmapR_0,       "Rear Security Map 0"           ) \
   PARAM_STR_32BIT( SecureKeyedBitmapR_1,       "Rear Security Map 1"           ) \
   PARAM_STR_32BIT( SecureKeyedBitmapR_2,       "Rear Security Map 2"           ) \
   PARAM_STR_32BIT( UNUSED_32BIT_15,            "NA"                            ) \
   PARAM_STR_32BIT( SecureTimedBitmapF_0,       "SecureTimedBitmapF 0"          ) \
   PARAM_STR_32BIT( SecureTimedBitmapF_1,       "SecureTimedBitmapF 1"          ) \
   PARAM_STR_32BIT( SecureTimedBitmapF_2,       "SecureTimedBitmapF 2"          ) \
   PARAM_STR_32BIT( UNUSED_32BIT_19,            "NA"                            ) \
   PARAM_STR_32BIT( SecureTimedBitmapR_0,       "SecureTimedBitmapR 0"          ) \
   PARAM_STR_32BIT( SecureTimedBitmapR_1,       "SecureTimedBitmapR 1"          ) \
   PARAM_STR_32BIT( SecureTimedBitmapR_2,       "SecureTimedBitmapR 2"          ) \
   PARAM_STR_32BIT( SabbathDestinationUp_0,     "Sabbath Up Destinations 0"     ) \
   PARAM_STR_32BIT( SabbathDestinationUp_1,     "Sabbath Up Destinations 1"     ) \
   PARAM_STR_32BIT( SabbathDestinationUp_2,     "Sabbath Up Destinations 2"     ) \
   PARAM_STR_32BIT( SabbathDestinationDown_0,   "Sabbath Down Destinations 0"   ) \
   PARAM_STR_32BIT( SabbathDestinationDown_1,   "Sabbath Down Destinations 1"   ) \
   PARAM_STR_32BIT( SabbathDestinationDown_2,   "Sabbath Down Destinations 2"   ) \
   PARAM_STR_32BIT( UNUSED_32BIT_29,            "NA"                            ) \
   PARAM_STR_32BIT( UNUSED_32BIT_30,            "NA"                            ) \
   PARAM_STR_32BIT( UNUSED_32BIT_31,            "NA"                            ) \
   PARAM_STR_32BIT( WanderGuardMask0,           "WanderGuardMask0"              ) \
   PARAM_STR_32BIT( WanderGuardMask1,           "WanderGuardMask1"              ) \
   PARAM_STR_32BIT( WanderGuardMask2,           "WanderGuardMask2"              ) \
   PARAM_STR_32BIT( UNUSED_32BIT_35,            "NA"                            ) \
   PARAM_STR_32BIT( SabbathOpeningF_0,          "Sabbath Front Opening 0"       ) \
   PARAM_STR_32BIT( SabbathOpeningF_1,          "Sabbath Front Opening 1"       ) \
   PARAM_STR_32BIT( SabbathOpeningF_2,          "Sabbath Front Opening 2"       ) \
   PARAM_STR_32BIT( SabbathOpeningR_0,          "Sabbath Rear Opening 0"        ) \
   PARAM_STR_32BIT( SabbathOpeningR_1,          "Sabbath Rear Opening 1"        ) \
   PARAM_STR_32BIT( SabbathOpeningR_2,          "Sabbath Rear Opening 2"        )

/* PARAM_DEFAULT_ENTRY_32BIT(A, B) FORMAT:
 *    A = Parameter Byte Map Index 
 *    B = (GRP1) Default All 8-Bit Map
 *    C = (GRP2) Default Learned Floors 8-Bit Map
 *    D = (GRP3) Default S-Curve 8-Bit Map
 *    E = (GRP4) Default Run Timers 8-Bit Map
 *    F = (GRP5) Default (UNUSED) 8-Bit Map
 *    G = (GRP6) Default (UNUSED) 8-Bit Map
 *    H = (GRP7) Default (UNUSED) 8-Bit Map
 *    I = (GRP8) Default (UNUSED) 8-Bit Map
 * */
/* Parameter Default Table Definition */
#define PARAM_DEFAULT_TABLE_32BIT(PARAM_DEFAULT_ENTRY_32BIT)  \
   PARAM_DEFAULT_ENTRY_32BIT( 0     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_32BIT( 1     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_32BIT( 2     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_32BIT( 3     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_32BIT( 4     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_32BIT( 5     , 3   , 0   , 0   , 0   , 0   , 0   , 0   , 0    )

/* Macros for accessing table elements */
#define EXPAND_PARAM_STRING_TABLE_32BIT_AS_ARRAY(A, B) B,

#define EXPAND_PARAM_DEFAULT_TABLE_32BIT_AS_DEFAULT_GRP_1(A, B, C, D, E, F, G, H, I) B,
#define EXPAND_PARAM_DEFAULT_TABLE_32BIT_AS_DEFAULT_GRP_2(A, B, C, D, E, F, G, H, I) C,
#define EXPAND_PARAM_DEFAULT_TABLE_32BIT_AS_DEFAULT_GRP_3(A, B, C, D, E, F, G, H, I) D,
#define EXPAND_PARAM_DEFAULT_TABLE_32BIT_AS_DEFAULT_GRP_4(A, B, C, D, E, F, G, H, I) E,
#define EXPAND_PARAM_DEFAULT_TABLE_32BIT_AS_DEFAULT_GRP_5(A, B, C, D, E, F, G, H, I) F,
#define EXPAND_PARAM_DEFAULT_TABLE_32BIT_AS_DEFAULT_GRP_6(A, B, C, D, E, F, G, H, I) G,
#define EXPAND_PARAM_DEFAULT_TABLE_32BIT_AS_DEFAULT_GRP_7(A, B, C, D, E, F, G, H, I) H,
#define EXPAND_PARAM_DEFAULT_TABLE_32BIT_AS_DEFAULT_GRP_8(A, B, C, D, E, F, G, H, I) I,

#endif