#ifndef SMARTRISE_PARAMETERS_8BIT
#define SMARTRISE_PARAMETERS_8BIT

/* NOTE: FILE UPDATED VIA LIB_SYSTEM - SUPPORT,
 * Edit SystemParameters.xlsx run Update_Parameters.bat
 * to automatically update this file. */

enum en_8bit_params
{
   enPARAM8__UNUSED_8BIT_0,                       // 08-0000
   enPARAM8__DoorDwellTime_1s,                    // 08-0001
   enPARAM8__DoorStuckTime_1s,                    // 08-0002
   enPARAM8__DoorNudgeTime_1s,                    // 08-0003
   enPARAM8__DoorDwellHallTime_1s,                // 08-0004
   enPARAM8__DoorDwellADATime_1s,                 // 08-0005
   enPARAM8__DoorDwellHoldTime_1s,                // 08-0006
   enPARAM8__DoorDwellSabbathTimer_1s,            // 08-0007
   enPARAM8__DoorJumperTimeout_100ms,             // 08-0008
   enPARAM8__Swing_Contacts_Timeout_1s,           // 08-0009
   enPARAM8__Swing_GSW_Locks_Timeout_1s,          // 08-0010
   enPARAM8__LobbyDwellTime_1s,                   // 08-0011
   enPARAM8__DoorTypeSelect_F,                    // 08-0012
   enPARAM8__DoorTypeSelect_R,                    // 08-0013
   enPARAM8__DoorCloseBuzzer_100ms,               // 08-0014
   enPARAM8__SabbathClosingBuzzer_100ms,          // 08-0015
   enPARAM8__UNUSED_8BIT_16,                      // 08-0016
   enPARAM8__P1_Accel_x10,                        // 08-0017
   enPARAM8__P1_JerkInAccel_x10,                  // 08-0018
   enPARAM8__P1_JerkOutAccel_x10,                 // 08-0019
   enPARAM8__P1_Decel_x10,                        // 08-0020
   enPARAM8__P1_JerkInDecel_x10,                  // 08-0021
   enPARAM8__P1_JerkOutDecel_x10,                 // 08-0022
   enPARAM8__QuickStopDecel_x10,                  // 08-0023
   enPARAM8__P1_LevelingDistance_5mm,             // 08-0024
   enPARAM8__P2_Accel_x10,                        // 08-0025
   enPARAM8__P2_JerkInAccel_x10,                  // 08-0026
   enPARAM8__P2_JerkOutAccel_x10,                 // 08-0027
   enPARAM8__P2_Decel_x10,                        // 08-0028
   enPARAM8__P2_JerkInDecel_x10,                  // 08-0029
   enPARAM8__P2_JerkOutDecel_x10,                 // 08-0030
   enPARAM8__P2_LevelingDistance_5mm,             // 08-0031
   enPARAM8__P3_Accel_x10,                        // 08-0032
   enPARAM8__P3_JerkInAccel_x10,                  // 08-0033
   enPARAM8__P3_JerkOutAccel_x10,                 // 08-0034
   enPARAM8__P3_Decel_x10,                        // 08-0035
   enPARAM8__P3_JerkInDecel_x10,                  // 08-0036
   enPARAM8__P3_JerkOutDecel_x10,                 // 08-0037
   enPARAM8__P3_LevelingDistance_5mm,             // 08-0038
   enPARAM8__P4_Accel_x10,                        // 08-0039
   enPARAM8__P4_JerkInAccel_x10,                  // 08-0040
   enPARAM8__P4_JerkOutAccel_x10,                 // 08-0041
   enPARAM8__P4_Decel_x10,                        // 08-0042
   enPARAM8__P4_JerkInDecel_x10,                  // 08-0043
   enPARAM8__P4_JerkOutDecel_x10,                 // 08-0044
   enPARAM8__P4_LevelingDistance_5mm,             // 08-0045
   enPARAM8__LevelingDecel_01fps,                 // 08-0046
   enPARAM8__NTSD_Speed,                          // 08-0047
   enPARAM8__Time_Violation_Rate,                 // 08-0048
   enPARAM8__Acceptance_ETSL_Point,               // 08-0049
   enPARAM8__CCDirectionChange_50ms,              // 08-0050
   enPARAM8__VIP_CarCall_Timer_1s,                // 08-0051
   enPARAM8__Viscosity_CyclesAllowed,             // 08-0052
   enPARAM8__Viscosity_RunTime_1min,              // 08-0053
   enPARAM8__Viscosity_RestTime_1min,             // 08-0054
   enPARAM8__JackResync_Frequency_1hr,            // 08-0055
   enPARAM8__JackResync_StartTime_15min,          // 08-0056
   enPARAM8__JackResync_Duration_1s,              // 08-0057
   enPARAM8__UNUSED_8BIT_58,                      // 08-0058
   enPARAM8__UNUSED_8BIT_59,                      // 08-0059
   enPARAM8__UNUSED_8BIT_60,                      // 08-0060
   enPARAM8__UNUSED_8BIT_61,                      // 08-0061
   enPARAM8__UNUSED_8BIT_62,                      // 08-0062
   enPARAM8__UNUSED_8BIT_63,                      // 08-0063
   enPARAM8__UNUSED_8BIT_64,                      // 08-0064
   enPARAM8__UNUSED_8BIT_65,                      // 08-0065
   enPARAM8__UNUSED_8BIT_66,                      // 08-0066
   enPARAM8__UNUSED_8BIT_67,                      // 08-0067
   enPARAM8__UNUSED_8BIT_68,                      // 08-0068
   enPARAM8__UNUSED_8BIT_69,                      // 08-0069
   enPARAM8__UNUSED_8BIT_70,                      // 08-0070
   enPARAM8__UNUSED_8BIT_71,                      // 08-0071
   enPARAM8__UNUSED_8BIT_72,                      // 08-0072
   enPARAM8__UNUSED_8BIT_73,                      // 08-0073
   enPARAM8__UNUSED_8BIT_74,                      // 08-0074
   enPARAM8__UNUSED_8BIT_75,                      // 08-0075
   enPARAM8__UNUSED_8BIT_76,                      // 08-0076
   enPARAM8__UNUSED_8BIT_77,                      // 08-0077
   enPARAM8__UNUSED_8BIT_78,                      // 08-0078
   enPARAM8__UNUSED_8BIT_79,                      // 08-0079
   enPARAM8__UNUSED_8BIT_80,                      // 08-0080
   enPARAM8__UNUSED_8BIT_81,                      // 08-0081
   enPARAM8__UNUSED_8BIT_82,                      // 08-0082
   enPARAM8__UNUSED_8BIT_83,                      // 08-0083
   enPARAM8__UNUSED_8BIT_84,                      // 08-0084
   enPARAM8__UNUSED_8BIT_85,                      // 08-0085
   enPARAM8__UNUSED_8BIT_86,                      // 08-0086
   enPARAM8__UNUSED_8BIT_87,                      // 08-0087
   enPARAM8__UNUSED_8BIT_88,                      // 08-0088
   enPARAM8__UNUSED_8BIT_89,                      // 08-0089
   enPARAM8__UNUSED_8BIT_90,                      // 08-0090
   enPARAM8__UNUSED_8BIT_91,                      // 08-0091
   enPARAM8__NumFloors,                           // 08-0092
   enPARAM8__CarStabilityDelay_50ms,              // 08-0093
   enPARAM8__HA_AllowedDist_Top_FT,               // 08-0094
   enPARAM8__HA_TopFloor,                         // 08-0095
   enPARAM8__HA_BottomFloor,                      // 08-0096
   enPARAM8__HA_TopOpening,                       // 08-0097
   enPARAM8__HA_BottomOpening,                    // 08-0098
   enPARAM8__BrakePickVoltage,                    // 08-0099
   enPARAM8__BrakeHoldVoltage,                    // 08-0100
   enPARAM8__BrakeRampTime_Auto_10ms,             // 08-0101
   enPARAM8__BrakePickDelay_10ms,                 // 08-0102
   enPARAM8__BrakeRelevelVoltage,                 // 08-0103
   enPARAM8__SecBrakePickVoltage,                 // 08-0104
   enPARAM8__SecBrakeHoldVoltage,                 // 08-0105
   enPARAM8__SecBrakeRampTime_10ms,               // 08-0106
   enPARAM8__SecBrakePickDelay_10ms,              // 08-0107
   enPARAM8__SecBrakeRelevelVoltage,              // 08-0108
   enPARAM8__BrakeRampTime_Insp_10ms,             // 08-0109
   enPARAM8__HA_AllowedDist_Bot_FT,               // 08-0110
   enPARAM8__FireRecallFloorM,                    // 08-0111
   enPARAM8__FireRecallFloorA,                    // 08-0112
   enPARAM8__OverrideParkingFloor,                // 08-0113
   enPARAM8__Parking_Timer_1sec,                  // 08-0114
   enPARAM8__FanAndLight_1s,                      // 08-0115
   enPARAM8__InspectionOverspeedDebounceLimit,    // 08-0116
   enPARAM8__DoorOpenOverspeedDebounceLimit,      // 08-0117
   enPARAM8__ETSOverspeedDebounceLimit,           // 08-0118
   enPARAM8__SFP_DebounceLimit,                   // 08-0119
   enPARAM8__RateToSendParameters_5ms,            // 08-0120
   enPARAM8__GroupCarIndex,                       // 08-0121
   enPARAM8__CarToLobbyFloor,                     // 08-0122
   enPARAM8__ResendDriveTimer_5ms,                // 08-0123
   enPARAM8__OfflineCtrlTimer_5ms,                // 08-0124
   enPARAM8__Debug_RunLogScaling,                 // 08-0125
   enPARAM8__ResendBrakeTimer_5ms,                // 08-0126
   enPARAM8__MotionRes_1ms,                       // 08-0127
   enPARAM8__ETS_OffsetFromNTS_5mm,               // 08-0128
   enPARAM8__EPowerPriorityCar,                   // 08-0129
   enPARAM8__DriveSelect,                         // 08-0130
   enPARAM8__Max_Runtime_1sec,                    // 08-0131
   enPARAM8__LWD_TorqueOffset,                    // 08-0132
   enPARAM8__LWD_TorqueScaling,                   // 08-0133
   enPARAM8__VIP_HC_TransitionDelay_50ms,         // 08-0134
   enPARAM8__LoadWeigherSelect,                   // 08-0135
   enPARAM8__GeneralOverspeedDebounceLimit,       // 08-0136
   enPARAM8__TimeoutLockAndCAM_100ms,             // 08-0137
   enPARAM8__AccessCode_CCB_Time_1s,              // 08-0138
   enPARAM8__DebounceNTS,                         // 08-0139
   enPARAM8__RelevelingDelay_50ms,                // 08-0140
   enPARAM8__AN_MaxOpensWithoutPHE,               // 08-0141
   enPARAM8__NumResendRunLog,                     // 08-0142
   enPARAM8__AutoRescueSpeed_fpm,                 // 08-0143
   enPARAM8__AccelDelay_Rescue_100ms,             // 08-0144
   enPARAM8__GroupPriority,                       // 08-0145
   enPARAM8__UNUSED_8BIT_146,                     // 08-0146
   enPARAM8__ShortProfileMinDist_ft,              // 08-0147
   enPARAM8__DoorHourlyFaultLimit,                // 08-0148
   enPARAM8__BPS_Timeout_100ms,                   // 08-0149
   enPARAM8__BPS2_Timeout_100ms,                  // 08-0150
   enPARAM8__TimeViolationModule,                 // 08-0151
   enPARAM8__MedValveSpeed_FPM,                   // 08-0152
   enPARAM8__LowValveSpeed_FPM,                   // 08-0153
   enPARAM8__UNUSED_8BIT_154,                     // 08-0154
   enPARAM8__UNUSED_8BIT_155,                     // 08-0155
   enPARAM8__RelevelOffsetUp_05mm,                // 08-0156
   enPARAM8__RelevelOffsetDown_05mm,              // 08-0157
   enPARAM8__RelevelingDistance_05mm,             // 08-0158
   enPARAM8__ConstructionOverspeedDebounce_10ms,  // 08-0159
   enPARAM8__HourlyFaultLimit,                    // 08-0160
   enPARAM8__Swing_IdleTime_1s,                   // 08-0161
   enPARAM8__OOS_DesiredFloor,                    // 08-0162
   enPARAM8__EMS1_ExitDelay_1s,                   // 08-0163
   enPARAM8__EMS2_ExitDelay_1s,                   // 08-0164
   enPARAM8__FLOOD_NumFloors_Plus1,               // 08-0165
   enPARAM8__ATTD_BuzzerTime_100ms,               // 08-0166
   enPARAM8__ATTD_DispatchTimeout_1s,             // 08-0167
   enPARAM8__ArrivalUpdateTime_sec,               // 08-0168
   enPARAM8__DestOffsetUp_05mm,                   // 08-0169
   enPARAM8__DestOffsetDown_05mm,                 // 08-0170
   enPARAM8__DEBUG_KEB_BaudRate,                  // 08-0171
   enPARAM8__DEBUG_TestRunsDwellTime_s,           // 08-0172
   enPARAM8__CPLDOfflineTimeout_10ms,             // 08-0173
   enPARAM8__GroupLandingOffset,                  // 08-0174
   enPARAM8__DispatchTimeout_1s,                  // 08-0175
   enPARAM8__DispatchOffline_1s,                  // 08-0176
   enPARAM8__NumXRegCars,                         // 08-0177
   enPARAM8__LinkedHallMask_1,                    // 08-0178
   enPARAM8__LinkedHallMask_2,                    // 08-0179
   enPARAM8__LinkedHallMask_3,                    // 08-0180
   enPARAM8__LinkedHallMask_4,                    // 08-0181
   enPARAM8__ETSL_ODL_10ms,                       // 08-0182
   enPARAM8__RatedBufferSpd_10fpm,                // 08-0183
   enPARAM8__MRFanTimer_min,                      // 08-0184
   enPARAM8__DoorCheckTime_100ms,                 // 08-0185
   enPARAM8__NumEPowerCars,                       // 08-0186
   enPARAM8__DoorOpeningTime_100ms,               // 08-0187
   enPARAM8__DSD_PretorqueDelay_50ms,             // 08-0188
   enPARAM8__DirChangeDelay_1s,                   // 08-0189
   enPARAM8__CCB_RecentPressTimer_100ms,          // 08-0190
   enPARAM8__DEBUG_NumInvalidDrivePackets,        // 08-0191
   enPARAM8__XREG_DestinationTimeout_10sec,       // 08-0192
   enPARAM8__XREG_DestinationOffline_10sec,       // 08-0193
   enPARAM8__MotionDirStage_Plus1,                // 08-0194
   enPARAM8__MinRelevelSpeed,                     // 08-0195
   enPARAM8__MaxStartsPerMinute,                  // 08-0196
   enPARAM8__ArvLanternFloor_1,                   // 08-0197
   enPARAM8__ArvLanternFloor_2,                   // 08-0198
   enPARAM8__ArvLanternFloor_3,                   // 08-0199
   enPARAM8__ArvLanternFloor_4,                   // 08-0200
   enPARAM8__ArvLanternFloor_5,                   // 08-0201
   enPARAM8__CheckInFloor,                        // 08-0202
   enPARAM8__MoveIdleCarTimer_10min,              // 08-0203
   enPARAM8__AN_MaxCarCallsPer_250lb,             // 08-0204
   enPARAM8__LWD_MonthlyCalibrationHour,          // 08-0205
   enPARAM8__LWD_MonthlyCalibrationDay,           // 08-0206
   enPARAM8__AccessSpeed_fpm,                     // 08-0207
   enPARAM8__HallSecurityMask,                    // 08-0208
   enPARAM8__HallCallMask,                        // 08-0209
   enPARAM8__HallMedicalMask,                     // 08-0210
   enPARAM8__HallRearDoorMask,                    // 08-0211
   enPARAM8__SwingCallMask,                       // 08-0212
   enPARAM8__HallLanternMask,                     // 08-0213
   enPARAM8__HallLanternRearMask,                 // 08-0214
   enPARAM8__DynamicParkingLanding_1_Plus1,       // 08-0215
   enPARAM8__DynamicParkingLanding_2_Plus1,       // 08-0216
   enPARAM8__DynamicParkingLanding_3_Plus1,       // 08-0217
   enPARAM8__DynamicParkingLanding_4_Plus1,       // 08-0218
   enPARAM8__DynamicParkingLanding_5_Plus1,       // 08-0219
   enPARAM8__DynamicParkingLanding_6_Plus1,       // 08-0220
   enPARAM8__DynamicParkingLanding_7_Plus1,       // 08-0221
   enPARAM8__DynamicParkingLanding_8_Plus1,       // 08-0222
   enPARAM8__AN_MaxCarCallsLightLoad,             // 08-0223
   enPARAM8__ATTD_FireRecallDelay_1s,             // 08-0224
   enPARAM8__EQ_Hoistway_Scan_Speed,              // 08-0225
   enPARAM8__SoftStarter_RampUpTime_100ms,        // 08-0226
   enPARAM8__SoftStarter_VMax_Percentage,         // 08-0227
   enPARAM8__SoftStarter_OvercurrentLimit_Amp,    // 08-0228
   enPARAM8__SoftStarter_OvertempLimit_Celsius,   // 08-0229
   enPARAM8__EPowerNumInterCars,                  // 08-0230
   enPARAM8__UNUSED_8BIT_231,                     // 08-0231
   enPARAM8__UNUSED_8BIT_232,                     // 08-0232
   enPARAM8__UNUSED_8BIT_233,                     // 08-0233
   enPARAM8__UNUSED_8BIT_234,                     // 08-0234
   enPARAM8__UNUSED_8BIT_235,                     // 08-0235
   enPARAM8__UNUSED_8BIT_236,                     // 08-0236
   enPARAM8__UNUSED_8BIT_237,                     // 08-0237
   enPARAM8__UNUSED_8BIT_238,                     // 08-0238
   enPARAM8__UNUSED_8BIT_239,                     // 08-0239
   enPARAM8__UNUSED_8BIT_240,                     // 08-0240
   enPARAM8__ValveTypeSelect,                     // 08-0241
   enPARAM8__VIP_Idle_Time_1s,                    // 08-0242
   enPARAM8__UNUSED_8BIT_243,                     // 08-0243
   enPARAM8__UNUSED_8BIT_244,                     // 08-0244
   enPARAM8__UNUSED_8BIT_245,                     // 08-0245
   enPARAM8__UNUSED_8BIT_246,                     // 08-0246
   enPARAM8__UNUSED_8BIT_247,                     // 08-0247
   enPARAM8__UNUSED_8BIT_248,                     // 08-0248
   enPARAM8__UNUSED_8BIT_249,                     // 08-0249
   enPARAM8__UNUSED_8BIT_250,                     // 08-0250
   enPARAM8__UNUSED_8BIT_251,                     // 08-0251
   enPARAM8__UNUSED_8BIT_252,                     // 08-0252
   enPARAM8__UNUSED_8BIT_253,                     // 08-0253
   enPARAM8__UNUSED_8BIT_254,                     // 08-0254
   enPARAM8__UNUSED_8BIT_255,                     // 08-0255
   enPARAM8__UNUSED_8BIT_256,                     // 08-0256
   enPARAM8__UNUSED_8BIT_257,                     // 08-0257
   enPARAM8__UNUSED_8BIT_258,                     // 08-0258
   enPARAM8__UNUSED_8BIT_259,                     // 08-0259
   enPARAM8__UNUSED_8BIT_260,                     // 08-0260
   enPARAM8__UNUSED_8BIT_261,                     // 08-0261
   enPARAM8__UNUSED_8BIT_262,                     // 08-0262
   enPARAM8__UNUSED_8BIT_263,                     // 08-0263
   enPARAM8__UNUSED_8BIT_264,                     // 08-0264
   enPARAM8__UNUSED_8BIT_265,                     // 08-0265
   enPARAM8__UNUSED_8BIT_266,                     // 08-0266
   enPARAM8__UNUSED_8BIT_267,                     // 08-0267
   enPARAM8__UNUSED_8BIT_268,                     // 08-0268
   enPARAM8__UNUSED_8BIT_269,                     // 08-0269
   enPARAM8__UNUSED_8BIT_270,                     // 08-0270
   enPARAM8__UNUSED_8BIT_271,                     // 08-0271
   enPARAM8__UNUSED_8BIT_272,                     // 08-0272
   enPARAM8__UNUSED_8BIT_273,                     // 08-0273
   enPARAM8__UNUSED_8BIT_274,                     // 08-0274
   enPARAM8__UNUSED_8BIT_275,                     // 08-0275
   enPARAM8__UNUSED_8BIT_276,                     // 08-0276
   enPARAM8__UNUSED_8BIT_277,                     // 08-0277
   enPARAM8__UNUSED_8BIT_278,                     // 08-0278
   enPARAM8__UNUSED_8BIT_279,                     // 08-0279
   enPARAM8__UNUSED_8BIT_280,                     // 08-0280
   enPARAM8__UNUSED_8BIT_281,                     // 08-0281
   enPARAM8__UNUSED_8BIT_282,                     // 08-0282
   enPARAM8__UNUSED_8BIT_283,                     // 08-0283
   enPARAM8__UNUSED_8BIT_284,                     // 08-0284
   enPARAM8__UNUSED_8BIT_285,                     // 08-0285
   enPARAM8__UNUSED_8BIT_286,                     // 08-0286
   enPARAM8__UNUSED_8BIT_287,                     // 08-0287
   enPARAM8__UNUSED_8BIT_288,                     // 08-0288
   enPARAM8__UNUSED_8BIT_289,                     // 08-0289

   NUM_8BIT_PARAMS
};

#define PARAM_STRINGS_8BIT(PARAM_STR_8BIT)  \
   PARAM_STR_8BIT( UNUSED_8BIT_0,                        "NA"                                ) \
   PARAM_STR_8BIT( DoorDwellTime_1s,                     "DR Dwell Time 1s"                  ) \
   PARAM_STR_8BIT( DoorStuckTime_1s,                     "DR Stuck Time 1s"                  ) \
   PARAM_STR_8BIT( DoorNudgeTime_1s,                     "DR Nudge Time 1s"                  ) \
   PARAM_STR_8BIT( DoorDwellHallTime_1s,                 "DR Dwell Hall Time 1s"             ) \
   PARAM_STR_8BIT( DoorDwellADATime_1s,                  "DR Dwell ADATime 1s"               ) \
   PARAM_STR_8BIT( DoorDwellHoldTime_1s,                 "DR Dwell Hold Time 1s"             ) \
   PARAM_STR_8BIT( DoorDwellSabbathTimer_1s,             "DR Dwell Sabbath Time 1s"          ) \
   PARAM_STR_8BIT( DoorJumperTimeout_100ms,              "DR Jumper Timeout 100ms"           ) \
   PARAM_STR_8BIT( Swing_Contacts_Timeout_1s,            "FDR Contacts Timeout 1s"           ) \
   PARAM_STR_8BIT( Swing_GSW_Locks_Timeout_1s,           "FDR GSW Locks Timeout 1s"          ) \
   PARAM_STR_8BIT( LobbyDwellTime_1s,                    "Lobby Dwell Time 1s"               ) \
   PARAM_STR_8BIT( DoorTypeSelect_F,                     "Door Type Select Front"            ) \
   PARAM_STR_8BIT( DoorTypeSelect_R,                     "Door Type Select Rear"             ) \
   PARAM_STR_8BIT( DoorCloseBuzzer_100ms,                "Door Close Buzzer 100ms"           ) \
   PARAM_STR_8BIT( SabbathClosingBuzzer_100ms,           "Sabbath Closing Buzzer 100ms"      ) \
   PARAM_STR_8BIT( UNUSED_8BIT_16,                       "NA"                                ) \
   PARAM_STR_8BIT( P1_Accel_x10,                         "Normal Accel"                      ) \
   PARAM_STR_8BIT( P1_JerkInAccel_x10,                   "Normal Jerk In Accel"              ) \
   PARAM_STR_8BIT( P1_JerkOutAccel_x10,                  "Normal Jerk Out Accel"             ) \
   PARAM_STR_8BIT( P1_Decel_x10,                         "Normal Decel"                      ) \
   PARAM_STR_8BIT( P1_JerkInDecel_x10,                   "Normal Jerk In Decel"              ) \
   PARAM_STR_8BIT( P1_JerkOutDecel_x10,                  "Normal Jerk Out Decel"             ) \
   PARAM_STR_8BIT( QuickStopDecel_x10,                   "Quick Stop Decel"                  ) \
   PARAM_STR_8BIT( P1_LevelingDistance_5mm,              "P1 LevelingDistance 5mm"           ) \
   PARAM_STR_8BIT( P2_Accel_x10,                         "Insp. Accel"                       ) \
   PARAM_STR_8BIT( P2_JerkInAccel_x10,                   "Insp. Jerk In Accel"               ) \
   PARAM_STR_8BIT( P2_JerkOutAccel_x10,                  "Insp. Jerk Out Accel"              ) \
   PARAM_STR_8BIT( P2_Decel_x10,                         "Insp. Decel"                       ) \
   PARAM_STR_8BIT( P2_JerkInDecel_x10,                   "Insp. Jerk Out Decel"              ) \
   PARAM_STR_8BIT( P2_JerkOutDecel_x10,                  "Insp. Jerk In Decel"               ) \
   PARAM_STR_8BIT( P2_LevelingDistance_5mm,              "Insp. Leveling Distance"           ) \
   PARAM_STR_8BIT( P3_Accel_x10,                         "EP Accel"                          ) \
   PARAM_STR_8BIT( P3_JerkInAccel_x10,                   "EP Jerk In Accel"                  ) \
   PARAM_STR_8BIT( P3_JerkOutAccel_x10,                  "EP Jerk Out Accel"                 ) \
   PARAM_STR_8BIT( P3_Decel_x10,                         "EP Decel"                          ) \
   PARAM_STR_8BIT( P3_JerkInDecel_x10,                   "EP Jerk In Decel"                  ) \
   PARAM_STR_8BIT( P3_JerkOutDecel_x10,                  "EP Jerk Out Decel"                 ) \
   PARAM_STR_8BIT( P3_LevelingDistance_5mm,              "EP Leveling Distance"              ) \
   PARAM_STR_8BIT( P4_Accel_x10,                         "Short Accel"                       ) \
   PARAM_STR_8BIT( P4_JerkInAccel_x10,                   "Short Jerk In Accel"               ) \
   PARAM_STR_8BIT( P4_JerkOutAccel_x10,                  "Short Jerk Out Accel"              ) \
   PARAM_STR_8BIT( P4_Decel_x10,                         "Short Decel"                       ) \
   PARAM_STR_8BIT( P4_JerkInDecel_x10,                   "Short Jerk In Decel"               ) \
   PARAM_STR_8BIT( P4_JerkOutDecel_x10,                  "Short Jerk Out Decel"              ) \
   PARAM_STR_8BIT( P4_LevelingDistance_5mm,              "Short Leveling Distance"           ) \
   PARAM_STR_8BIT( LevelingDecel_01fps,                  "LevelingDecel 01fps"               ) \
   PARAM_STR_8BIT( NTSD_Speed,                           "NTSD Speed"                        ) \
   PARAM_STR_8BIT( Time_Violation_Rate,                  "Time Violation Rate"               ) \
   PARAM_STR_8BIT( Acceptance_ETSL_Point,                "Acceptance ETSL Point"             ) \
   PARAM_STR_8BIT( CCDirectionChange_50ms,               "CC Dir. Change (50ms)"             ) \
   PARAM_STR_8BIT( VIP_CarCall_Timer_1s,                 "VIP CarCall Timer (1s)"            ) \
   PARAM_STR_8BIT( Viscosity_CyclesAllowed,              "Viscosity_CyclesAllowed"           ) \
   PARAM_STR_8BIT( Viscosity_RunTime_1min,               "Viscosity_RunTime_1min"            ) \
   PARAM_STR_8BIT( Viscosity_RestTime_1min,              "Viscosity_RestTime_1min"           ) \
   PARAM_STR_8BIT( JackResync_Frequency_1hr,             "JackResync_Frequency_1hr"          ) \
   PARAM_STR_8BIT( JackResync_StartTime_15min,           "JackResync_StartTime_15min"        ) \
   PARAM_STR_8BIT( JackResync_Duration_1s,               "JackResync_Duration_1s"            ) \
   PARAM_STR_8BIT( UNUSED_8BIT_58,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_59,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_60,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_61,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_62,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_63,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_64,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_65,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_66,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_67,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_68,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_69,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_70,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_71,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_72,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_73,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_74,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_75,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_76,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_77,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_78,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_79,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_80,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_81,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_82,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_83,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_84,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_85,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_86,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_87,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_88,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_89,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_90,                       "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_91,                       "NA"                                ) \
   PARAM_STR_8BIT( NumFloors,                            "Number of FLRs"                    ) \
   PARAM_STR_8BIT( CarStabilityDelay_50ms,               "Car Stability Delay (50ms)"        ) \
   PARAM_STR_8BIT( HA_AllowedDist_Top_FT,                "HA Top Allowed Distance"           ) \
   PARAM_STR_8BIT( HA_TopFloor,                          "HA Top FLR "                       ) \
   PARAM_STR_8BIT( HA_BottomFloor,                       "HA BottomFLR "                     ) \
   PARAM_STR_8BIT( HA_TopOpening,                        "HA Top Opening"                    ) \
   PARAM_STR_8BIT( HA_BottomOpening,                     "HA Bottom Opening"                 ) \
   PARAM_STR_8BIT( BrakePickVoltage,                     "Brake Pick Voltage"                ) \
   PARAM_STR_8BIT( BrakeHoldVoltage,                     "Brake Hold Voltage"                ) \
   PARAM_STR_8BIT( BrakeRampTime_Auto_10ms,              "Brake Ramp Time Auto"              ) \
   PARAM_STR_8BIT( BrakePickDelay_10ms,                  "Brake Pick Delay"                  ) \
   PARAM_STR_8BIT( BrakeRelevelVoltage,                  "Brake Relevel Voltage"             ) \
   PARAM_STR_8BIT( SecBrakePickVoltage,                  "Secondary Brake Pick Voltage"      ) \
   PARAM_STR_8BIT( SecBrakeHoldVoltage,                  "Secondary Brake Hold Voltage"      ) \
   PARAM_STR_8BIT( SecBrakeRampTime_10ms,                "Secondary Brake Ramp Time"         ) \
   PARAM_STR_8BIT( SecBrakePickDelay_10ms,               "Secondary Brake Pick Delay"        ) \
   PARAM_STR_8BIT( SecBrakeRelevelVoltage,               "Secondary Brake Relevel Voltage"   ) \
   PARAM_STR_8BIT( BrakeRampTime_Insp_10ms,              "Brake Ramp Time Inspection"        ) \
   PARAM_STR_8BIT( HA_AllowedDist_Bot_FT,                "HA Bottom Allowed Distance"        ) \
   PARAM_STR_8BIT( FireRecallFloorM,                     "Fire Main Recall FLR "             ) \
   PARAM_STR_8BIT( FireRecallFloorA,                     "Fire Alternate Recall FLR "        ) \
   PARAM_STR_8BIT( OverrideParkingFloor,                 "Parking FLR "                      ) \
   PARAM_STR_8BIT( Parking_Timer_1sec,                   "Parking Timer"                     ) \
   PARAM_STR_8BIT( FanAndLight_1s,                       "Fan And Light Timer"               ) \
   PARAM_STR_8BIT( InspectionOverspeedDebounceLimit,     "Inspection OVSP Debounce Limit"    ) \
   PARAM_STR_8BIT( DoorOpenOverspeedDebounceLimit,       "DR Open OVSP Debounce Limit"       ) \
   PARAM_STR_8BIT( ETSOverspeedDebounceLimit,            "ETS OVSP Debounce Limit"           ) \
   PARAM_STR_8BIT( SFP_DebounceLimit,                    "SFP Debounce Limit"                ) \
   PARAM_STR_8BIT( RateToSendParameters_5ms,             "Rate To Send Parameters"           ) \
   PARAM_STR_8BIT( GroupCarIndex,                        "Group Car Index"                   ) \
   PARAM_STR_8BIT( CarToLobbyFloor,                      "Car To Lobby FLR "                 ) \
   PARAM_STR_8BIT( ResendDriveTimer_5ms,                 "Drive Resend Timer"                ) \
   PARAM_STR_8BIT( OfflineCtrlTimer_5ms,                 "OfflineCtrlTimer"                  ) \
   PARAM_STR_8BIT( Debug_RunLogScaling,                  "Run Log Scaling"                   ) \
   PARAM_STR_8BIT( ResendBrakeTimer_5ms,                 "Resend Brake Timer"                ) \
   PARAM_STR_8BIT( MotionRes_1ms,                        "Motion Resolution"                 ) \
   PARAM_STR_8BIT( ETS_OffsetFromNTS_5mm,                "ETS Offset From NTS"               ) \
   PARAM_STR_8BIT( EPowerPriorityCar,                    "Epower Priority Car"               ) \
   PARAM_STR_8BIT( DriveSelect,                          "Drive Select"                      ) \
   PARAM_STR_8BIT( Max_Runtime_1sec,                     "Max Runtime (1s)"                  ) \
   PARAM_STR_8BIT( LWD_TorqueOffset,                     "LWD Torque Offset"                 ) \
   PARAM_STR_8BIT( LWD_TorqueScaling,                    "LWD Torque Scaling"                ) \
   PARAM_STR_8BIT( VIP_HC_TransitionDelay_50ms,          "VIP_HC_TransitionDelay_50ms"       ) \
   PARAM_STR_8BIT( LoadWeigherSelect,                    "LoadWeigherSelect"                 ) \
   PARAM_STR_8BIT( GeneralOverspeedDebounceLimit,        "General OVSP Debounce Limit"       ) \
   PARAM_STR_8BIT( TimeoutLockAndCAM_100ms,              "Timeout Lock and CAM (100ms)"      ) \
   PARAM_STR_8BIT( AccessCode_CCB_Time_1s,               "AccessCode CCB Time (1s)"          ) \
   PARAM_STR_8BIT( DebounceNTS,                          "NTS Debounce"                      ) \
   PARAM_STR_8BIT( RelevelingDelay_50ms,                 "Releveling Delay (50ms)"           ) \
   PARAM_STR_8BIT( AN_MaxOpensWithoutPHE,                "AN Max Opens Without PHE"          ) \
   PARAM_STR_8BIT( NumResendRunLog,                      "NumResendRunLog"                   ) \
   PARAM_STR_8BIT( AutoRescueSpeed_fpm,                  "Auto Rescue Spd (fpm)"             ) \
   PARAM_STR_8BIT( AccelDelay_Rescue_100ms,              "AccelDelay Rescue (100ms)"         ) \
   PARAM_STR_8BIT( GroupPriority,                        "Group Priority"                    ) \
   PARAM_STR_8BIT( UNUSED_8BIT_146,                      "NA"                                ) \
   PARAM_STR_8BIT( ShortProfileMinDist_ft,               "Short Profile Minimum Distance"    ) \
   PARAM_STR_8BIT( DoorHourlyFaultLimit,                 "DR Hourly Fault Limit"             ) \
   PARAM_STR_8BIT( BPS_Timeout_100ms,                    "BPS Timeout (100ms)"               ) \
   PARAM_STR_8BIT( BPS2_Timeout_100ms,                   "BPS2 Timeout (100ms)"              ) \
   PARAM_STR_8BIT( TimeViolationModule,                  "Time Violation Module"             ) \
   PARAM_STR_8BIT( MedValveSpeed_FPM,                    "MedValveSpeed (fpm)"               ) \
   PARAM_STR_8BIT( LowValveSpeed_FPM,                    "LowValveSpeed (fpm)"               ) \
   PARAM_STR_8BIT( UNUSED_8BIT_154,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_155,                      "NA"                                ) \
   PARAM_STR_8BIT( RelevelOffsetUp_05mm,                 "Relevel Offset Up 0.5mm"           ) \
   PARAM_STR_8BIT( RelevelOffsetDown_05mm,               "Relevel Offset Down 0.5mm"         ) \
   PARAM_STR_8BIT( RelevelingDistance_05mm,              "Releveling Zone Size"              ) \
   PARAM_STR_8BIT( ConstructionOverspeedDebounce_10ms,   "Construction OVSP Debounce"        ) \
   PARAM_STR_8BIT( HourlyFaultLimit,                     "HourlyFaultLimit"                  ) \
   PARAM_STR_8BIT( Swing_IdleTime_1s,                    "Swing IdleTime 1s"                 ) \
   PARAM_STR_8BIT( OOS_DesiredFloor,                     "OOS DesiredFLR "                   ) \
   PARAM_STR_8BIT( EMS1_ExitDelay_1s,                    "EMS1 Exit Delay"                   ) \
   PARAM_STR_8BIT( EMS2_ExitDelay_1s,                    "EMS2 Exit Delay"                   ) \
   PARAM_STR_8BIT( FLOOD_NumFloors_Plus1,                "Number of Flood FLRs"              ) \
   PARAM_STR_8BIT( ATTD_BuzzerTime_100ms,                "Attendant Buzzer Duration"         ) \
   PARAM_STR_8BIT( ATTD_DispatchTimeout_1s,              "Attendant Dispatch Timeout (1s)"   ) \
   PARAM_STR_8BIT( ArrivalUpdateTime_sec,                "Arrival Lantern Update Time"       ) \
   PARAM_STR_8BIT( DestOffsetUp_05mm,                    "Dest. Offset Up 0.5mm"             ) \
   PARAM_STR_8BIT( DestOffsetDown_05mm,                  "Dest. Offset Down 0.5mm"           ) \
   PARAM_STR_8BIT( DEBUG_KEB_BaudRate,                   "Debug KEB Baud Rate"               ) \
   PARAM_STR_8BIT( DEBUG_TestRunsDwellTime_s,            "Test Runs Dwell Time"              ) \
   PARAM_STR_8BIT( CPLDOfflineTimeout_10ms,              "CPLD Offline Timeout"              ) \
   PARAM_STR_8BIT( GroupLandingOffset,                   "Group Landing Offset"              ) \
   PARAM_STR_8BIT( DispatchTimeout_1s,                   "Dispatch Timeout 1s"               ) \
   PARAM_STR_8BIT( DispatchOffline_1s,                   "Dispatch Offline 1s"               ) \
   PARAM_STR_8BIT( NumXRegCars,                          "NumXRegCars"                       ) \
   PARAM_STR_8BIT( LinkedHallMask_1,                     "Linked Hall Mask 1"                ) \
   PARAM_STR_8BIT( LinkedHallMask_2,                     "Linked Hall Mask 2"                ) \
   PARAM_STR_8BIT( LinkedHallMask_3,                     "Linked Hall Mask 3"                ) \
   PARAM_STR_8BIT( LinkedHallMask_4,                     "Linked Hall Mask 4"                ) \
   PARAM_STR_8BIT( ETSL_ODL_10ms,                        "ETSL OVSP Debounce Limit"          ) \
   PARAM_STR_8BIT( RatedBufferSpd_10fpm,                 "RatedBufferSpd 10fpm"              ) \
   PARAM_STR_8BIT( MRFanTimer_min,                       "MR Fan Timer (min)"                ) \
   PARAM_STR_8BIT( DoorCheckTime_100ms,                  "Door Check Time 100ms"             ) \
   PARAM_STR_8BIT( NumEPowerCars,                        "NumEPCars"                         ) \
   PARAM_STR_8BIT( DoorOpeningTime_100ms,                "DR Opening Time (100ms)"           ) \
   PARAM_STR_8BIT( DSD_PretorqueDelay_50ms,              "DSD Pretorque Delay (50ms)"        ) \
   PARAM_STR_8BIT( DirChangeDelay_1s,                    "Dir. Change Delay (1s)"            ) \
   PARAM_STR_8BIT( CCB_RecentPressTimer_100ms,           "CCB Recent Press Timer (100ms)"    ) \
   PARAM_STR_8BIT( DEBUG_NumInvalidDrivePackets,         "Debug NumInvalidDrivePackets"      ) \
   PARAM_STR_8BIT( XREG_DestinationTimeout_10sec,        "XREG Dest. Timeout (10s)"          ) \
   PARAM_STR_8BIT( XREG_DestinationOffline_10sec,        "XREG Dest. Offline (10s)"          ) \
   PARAM_STR_8BIT( MotionDirStage_Plus1,                 "Motion Direction Stage Plus1"      ) \
   PARAM_STR_8BIT( MinRelevelSpeed,                      "Min Relevel Speed"                 ) \
   PARAM_STR_8BIT( MaxStartsPerMinute,                   "Max Starts Per Minute"             ) \
   PARAM_STR_8BIT( ArvLanternFloor_1,                    "Arv Lantern FLR 1"                 ) \
   PARAM_STR_8BIT( ArvLanternFloor_2,                    "Arv Lantern FLR 2"                 ) \
   PARAM_STR_8BIT( ArvLanternFloor_3,                    "Arv Lantern FLR 3"                 ) \
   PARAM_STR_8BIT( ArvLanternFloor_4,                    "Arv Lantern FLR 4"                 ) \
   PARAM_STR_8BIT( ArvLanternFloor_5,                    "Arv Lantern FLR 5"                 ) \
   PARAM_STR_8BIT( CheckInFloor,                         "Check In Floor"                    ) \
   PARAM_STR_8BIT( MoveIdleCarTimer_10min,               "Move Idle Car Timer (10min)"       ) \
   PARAM_STR_8BIT( AN_MaxCarCallsPer_250lb,              "Max Car Calls Per 250lb"           ) \
   PARAM_STR_8BIT( LWD_MonthlyCalibrationHour,           "LWD Monthly Calibration Hour "     ) \
   PARAM_STR_8BIT( LWD_MonthlyCalibrationDay,            "LWD Monthly Calibration Day "      ) \
   PARAM_STR_8BIT( AccessSpeed_fpm,                      "Access Speed (fpm)"                ) \
   PARAM_STR_8BIT( HallSecurityMask,                     "Hall Security Mask"                ) \
   PARAM_STR_8BIT( HallCallMask,                         "Hall Call Mask"                    ) \
   PARAM_STR_8BIT( HallMedicalMask,                      "Hall Medical Mask"                 ) \
   PARAM_STR_8BIT( HallRearDoorMask,                     "Hall Rear Door Mask"               ) \
   PARAM_STR_8BIT( SwingCallMask,                        "Swing Call Mask"                   ) \
   PARAM_STR_8BIT( HallLanternMask,                      "Hall Lantern Mask"                 ) \
   PARAM_STR_8BIT( HallLanternRearMask,                  "Rear Lantern Mask"                 ) \
   PARAM_STR_8BIT( DynamicParkingLanding_1_Plus1,        "DynamicParkingLanding_1_Plus1"     ) \
   PARAM_STR_8BIT( DynamicParkingLanding_2_Plus1,        "DynamicParkingLanding_2_Plus1"     ) \
   PARAM_STR_8BIT( DynamicParkingLanding_3_Plus1,        "DynamicParkingLanding_3_Plus1"     ) \
   PARAM_STR_8BIT( DynamicParkingLanding_4_Plus1,        "DynamicParkingLanding_4_Plus1"     ) \
   PARAM_STR_8BIT( DynamicParkingLanding_5_Plus1,        "DynamicParkingLanding_5_Plus1"     ) \
   PARAM_STR_8BIT( DynamicParkingLanding_6_Plus1,        "DynamicParkingLanding_6_Plus1"     ) \
   PARAM_STR_8BIT( DynamicParkingLanding_7_Plus1,        "DynamicParkingLanding_7_Plus1"     ) \
   PARAM_STR_8BIT( DynamicParkingLanding_8_Plus1,        "DynamicParkingLanding_8_Plus1"     ) \
   PARAM_STR_8BIT( AN_MaxCarCallsLightLoad,              "Max Car Calls Light Load"          ) \
   PARAM_STR_8BIT( ATTD_FireRecallDelay_1s,              "ATTD Fire Recall Delay (1s)"       ) \
   PARAM_STR_8BIT( EQ_Hoistway_Scan_Speed,               "EQ Hoistway Scan Speed"            ) \
   PARAM_STR_8BIT( SoftStarter_RampUpTime_100ms,         "SS Ramp Up Time 100ms"             ) \
   PARAM_STR_8BIT( SoftStarter_VMax_Percentage,          "SS Vmax VAC %"                     ) \
   PARAM_STR_8BIT( SoftStarter_OvercurrentLimit_Amp,     "SS OVC (A)"                        ) \
   PARAM_STR_8BIT( SoftStarter_OvertempLimit_Celsius,    "SS OVT (C)"                        ) \
   PARAM_STR_8BIT( EPowerNumInterCars,                   "Maximum EP Group Cars"             ) \
   PARAM_STR_8BIT( UNUSED_8BIT_231,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_232,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_233,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_234,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_235,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_236,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_237,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_238,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_239,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_240,                      "NA"                                ) \
   PARAM_STR_8BIT( ValveTypeSelect,                      "Valve Type Select"                 ) \
   PARAM_STR_8BIT( VIP_Idle_Time_1s,                     "Vip Idle Time 1s"                  ) \
   PARAM_STR_8BIT( UNUSED_8BIT_243,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_244,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_245,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_246,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_247,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_248,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_249,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_250,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_251,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_252,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_253,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_254,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_255,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_256,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_257,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_258,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_259,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_260,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_261,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_262,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_263,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_264,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_265,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_266,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_267,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_268,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_269,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_270,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_271,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_272,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_273,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_274,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_275,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_276,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_277,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_278,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_279,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_280,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_281,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_282,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_283,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_284,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_285,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_286,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_287,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_288,                      "NA"                                ) \
   PARAM_STR_8BIT( UNUSED_8BIT_289,                      "NA"                                )

/* PARAM_DEFAULT_ENTRY_8BIT(A, B) FORMAT:
 *    A = Parameter Byte Map Index 
 *    B = (GRP1) Default All 8-Bit Map
 *    C = (GRP2) Default Learned Floors 8-Bit Map
 *    D = (GRP3) Default S-Curve 8-Bit Map
 *    E = (GRP4) Default Run Timers 8-Bit Map
 *    F = (GRP5) Default (UNUSED) 8-Bit Map
 *    G = (GRP6) Default (UNUSED) 8-Bit Map
 *    H = (GRP7) Default (UNUSED) 8-Bit Map
 *    I = (GRP8) Default (UNUSED) 8-Bit Map
 * */
/* Parameter Default Table Definition */
#define PARAM_DEFAULT_TABLE_8BIT(PARAM_DEFAULT_ENTRY_8BIT)  \
   PARAM_DEFAULT_ENTRY_8BIT( 0     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 1     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 2     , 129 , 0   , 126 , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 3     , 0   , 0   , 255 , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 4     , 0   , 0   , 255 , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 5     , 128 , 0   , 127 , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 6     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 7     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 8     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 9     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 10    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 11    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 12    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 13    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 14    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 15    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 16    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 17    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 18    , 246 , 0   , 8   , 1   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 19    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 20    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 21    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 22    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 23    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 24    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 25    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 26    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 27    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 28    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 29    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 30    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 31    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 32    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 33    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 34    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 35    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_8BIT( 36    , 3   , 0   , 0   , 0   , 0   , 0   , 0   , 0    )

/* Macros for accessing table elements */
#define EXPAND_PARAM_STRING_TABLE_8BIT_AS_ARRAY(A, B) B,

#define EXPAND_PARAM_DEFAULT_TABLE_8BIT_AS_DEFAULT_GRP_1(A, B, C, D, E, F, G, H, I) B,
#define EXPAND_PARAM_DEFAULT_TABLE_8BIT_AS_DEFAULT_GRP_2(A, B, C, D, E, F, G, H, I) C,
#define EXPAND_PARAM_DEFAULT_TABLE_8BIT_AS_DEFAULT_GRP_3(A, B, C, D, E, F, G, H, I) D,
#define EXPAND_PARAM_DEFAULT_TABLE_8BIT_AS_DEFAULT_GRP_4(A, B, C, D, E, F, G, H, I) E,
#define EXPAND_PARAM_DEFAULT_TABLE_8BIT_AS_DEFAULT_GRP_5(A, B, C, D, E, F, G, H, I) F,
#define EXPAND_PARAM_DEFAULT_TABLE_8BIT_AS_DEFAULT_GRP_6(A, B, C, D, E, F, G, H, I) G,
#define EXPAND_PARAM_DEFAULT_TABLE_8BIT_AS_DEFAULT_GRP_7(A, B, C, D, E, F, G, H, I) H,
#define EXPAND_PARAM_DEFAULT_TABLE_8BIT_AS_DEFAULT_GRP_8(A, B, C, D, E, F, G, H, I) I,

#endif