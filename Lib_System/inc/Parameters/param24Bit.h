#ifndef SMARTRISE_PARAMETERS_24BIT
#define SMARTRISE_PARAMETERS_24BIT

/* NOTE: FILE UPDATED VIA LIB_SYSTEM - SUPPORT,
 * Edit SystemParameters.xlsx run Update_Parameters.bat
 * to automatically update this file. */

enum en_24bit_params
{
   enPARAM24__PI_0,                      // 24-0000
   enPARAM24__PI_1,                      // 24-0001
   enPARAM24__PI_2,                      // 24-0002
   enPARAM24__PI_3,                      // 24-0003
   enPARAM24__PI_4,                      // 24-0004
   enPARAM24__PI_5,                      // 24-0005
   enPARAM24__PI_6,                      // 24-0006
   enPARAM24__PI_7,                      // 24-0007
   enPARAM24__PI_8,                      // 24-0008
   enPARAM24__PI_9,                      // 24-0009
   enPARAM24__PI_10,                     // 24-0010
   enPARAM24__PI_11,                     // 24-0011
   enPARAM24__PI_12,                     // 24-0012
   enPARAM24__PI_13,                     // 24-0013
   enPARAM24__PI_14,                     // 24-0014
   enPARAM24__PI_15,                     // 24-0015
   enPARAM24__PI_16,                     // 24-0016
   enPARAM24__PI_17,                     // 24-0017
   enPARAM24__PI_18,                     // 24-0018
   enPARAM24__PI_19,                     // 24-0019
   enPARAM24__PI_20,                     // 24-0020
   enPARAM24__PI_21,                     // 24-0021
   enPARAM24__PI_22,                     // 24-0022
   enPARAM24__PI_23,                     // 24-0023
   enPARAM24__PI_24,                     // 24-0024
   enPARAM24__PI_25,                     // 24-0025
   enPARAM24__PI_26,                     // 24-0026
   enPARAM24__PI_27,                     // 24-0027
   enPARAM24__PI_28,                     // 24-0028
   enPARAM24__PI_29,                     // 24-0029
   enPARAM24__PI_30,                     // 24-0030
   enPARAM24__PI_31,                     // 24-0031
   enPARAM24__PI_32,                     // 24-0032
   enPARAM24__PI_33,                     // 24-0033
   enPARAM24__PI_34,                     // 24-0034
   enPARAM24__PI_35,                     // 24-0035
   enPARAM24__PI_36,                     // 24-0036
   enPARAM24__PI_37,                     // 24-0037
   enPARAM24__PI_38,                     // 24-0038
   enPARAM24__PI_39,                     // 24-0039
   enPARAM24__PI_40,                     // 24-0040
   enPARAM24__PI_41,                     // 24-0041
   enPARAM24__PI_42,                     // 24-0042
   enPARAM24__PI_43,                     // 24-0043
   enPARAM24__PI_44,                     // 24-0044
   enPARAM24__PI_45,                     // 24-0045
   enPARAM24__PI_46,                     // 24-0046
   enPARAM24__PI_47,                     // 24-0047
   enPARAM24__PI_48,                     // 24-0048
   enPARAM24__PI_49,                     // 24-0049
   enPARAM24__PI_50,                     // 24-0050
   enPARAM24__PI_51,                     // 24-0051
   enPARAM24__PI_52,                     // 24-0052
   enPARAM24__PI_53,                     // 24-0053
   enPARAM24__PI_54,                     // 24-0054
   enPARAM24__PI_55,                     // 24-0055
   enPARAM24__PI_56,                     // 24-0056
   enPARAM24__PI_57,                     // 24-0057
   enPARAM24__PI_58,                     // 24-0058
   enPARAM24__PI_59,                     // 24-0059
   enPARAM24__PI_60,                     // 24-0060
   enPARAM24__PI_61,                     // 24-0061
   enPARAM24__PI_62,                     // 24-0062
   enPARAM24__PI_63,                     // 24-0063
   enPARAM24__PI_64,                     // 24-0064
   enPARAM24__PI_65,                     // 24-0065
   enPARAM24__PI_66,                     // 24-0066
   enPARAM24__PI_67,                     // 24-0067
   enPARAM24__PI_68,                     // 24-0068
   enPARAM24__PI_69,                     // 24-0069
   enPARAM24__PI_70,                     // 24-0070
   enPARAM24__PI_71,                     // 24-0071
   enPARAM24__PI_72,                     // 24-0072
   enPARAM24__PI_73,                     // 24-0073
   enPARAM24__PI_74,                     // 24-0074
   enPARAM24__PI_75,                     // 24-0075
   enPARAM24__PI_76,                     // 24-0076
   enPARAM24__PI_77,                     // 24-0077
   enPARAM24__PI_78,                     // 24-0078
   enPARAM24__PI_79,                     // 24-0079
   enPARAM24__PI_80,                     // 24-0080
   enPARAM24__PI_81,                     // 24-0081
   enPARAM24__PI_82,                     // 24-0082
   enPARAM24__PI_83,                     // 24-0083
   enPARAM24__PI_84,                     // 24-0084
   enPARAM24__PI_85,                     // 24-0085
   enPARAM24__PI_86,                     // 24-0086
   enPARAM24__PI_87,                     // 24-0087
   enPARAM24__PI_88,                     // 24-0088
   enPARAM24__PI_89,                     // 24-0089
   enPARAM24__PI_90,                     // 24-0090
   enPARAM24__PI_91,                     // 24-0091
   enPARAM24__PI_92,                     // 24-0092
   enPARAM24__PI_93,                     // 24-0093
   enPARAM24__PI_94,                     // 24-0094
   enPARAM24__PI_95,                     // 24-0095
   enPARAM24__LearnedFloor_0,            // 24-0096
   enPARAM24__LearnedFloor_1,            // 24-0097
   enPARAM24__LearnedFloor_2,            // 24-0098
   enPARAM24__LearnedFloor_3,            // 24-0099
   enPARAM24__LearnedFloor_4,            // 24-0100
   enPARAM24__LearnedFloor_5,            // 24-0101
   enPARAM24__LearnedFloor_6,            // 24-0102
   enPARAM24__LearnedFloor_7,            // 24-0103
   enPARAM24__LearnedFloor_8,            // 24-0104
   enPARAM24__LearnedFloor_9,            // 24-0105
   enPARAM24__LearnedFloor_10,           // 24-0106
   enPARAM24__LearnedFloor_11,           // 24-0107
   enPARAM24__LearnedFloor_12,           // 24-0108
   enPARAM24__LearnedFloor_13,           // 24-0109
   enPARAM24__LearnedFloor_14,           // 24-0110
   enPARAM24__LearnedFloor_15,           // 24-0111
   enPARAM24__LearnedFloor_16,           // 24-0112
   enPARAM24__LearnedFloor_17,           // 24-0113
   enPARAM24__LearnedFloor_18,           // 24-0114
   enPARAM24__LearnedFloor_19,           // 24-0115
   enPARAM24__LearnedFloor_20,           // 24-0116
   enPARAM24__LearnedFloor_21,           // 24-0117
   enPARAM24__LearnedFloor_22,           // 24-0118
   enPARAM24__LearnedFloor_23,           // 24-0119
   enPARAM24__LearnedFloor_24,           // 24-0120
   enPARAM24__LearnedFloor_25,           // 24-0121
   enPARAM24__LearnedFloor_26,           // 24-0122
   enPARAM24__LearnedFloor_27,           // 24-0123
   enPARAM24__LearnedFloor_28,           // 24-0124
   enPARAM24__LearnedFloor_29,           // 24-0125
   enPARAM24__LearnedFloor_30,           // 24-0126
   enPARAM24__LearnedFloor_31,           // 24-0127
   enPARAM24__LearnedFloor_32,           // 24-0128
   enPARAM24__LearnedFloor_33,           // 24-0129
   enPARAM24__LearnedFloor_34,           // 24-0130
   enPARAM24__LearnedFloor_35,           // 24-0131
   enPARAM24__LearnedFloor_36,           // 24-0132
   enPARAM24__LearnedFloor_37,           // 24-0133
   enPARAM24__LearnedFloor_38,           // 24-0134
   enPARAM24__LearnedFloor_39,           // 24-0135
   enPARAM24__LearnedFloor_40,           // 24-0136
   enPARAM24__LearnedFloor_41,           // 24-0137
   enPARAM24__LearnedFloor_42,           // 24-0138
   enPARAM24__LearnedFloor_43,           // 24-0139
   enPARAM24__LearnedFloor_44,           // 24-0140
   enPARAM24__LearnedFloor_45,           // 24-0141
   enPARAM24__LearnedFloor_46,           // 24-0142
   enPARAM24__LearnedFloor_47,           // 24-0143
   enPARAM24__LearnedFloor_48,           // 24-0144
   enPARAM24__LearnedFloor_49,           // 24-0145
   enPARAM24__LearnedFloor_50,           // 24-0146
   enPARAM24__LearnedFloor_51,           // 24-0147
   enPARAM24__LearnedFloor_52,           // 24-0148
   enPARAM24__LearnedFloor_53,           // 24-0149
   enPARAM24__LearnedFloor_54,           // 24-0150
   enPARAM24__LearnedFloor_55,           // 24-0151
   enPARAM24__LearnedFloor_56,           // 24-0152
   enPARAM24__LearnedFloor_57,           // 24-0153
   enPARAM24__LearnedFloor_58,           // 24-0154
   enPARAM24__LearnedFloor_59,           // 24-0155
   enPARAM24__LearnedFloor_60,           // 24-0156
   enPARAM24__LearnedFloor_61,           // 24-0157
   enPARAM24__LearnedFloor_62,           // 24-0158
   enPARAM24__LearnedFloor_63,           // 24-0159
   enPARAM24__LearnedFloor_64,           // 24-0160
   enPARAM24__LearnedFloor_65,           // 24-0161
   enPARAM24__LearnedFloor_66,           // 24-0162
   enPARAM24__LearnedFloor_67,           // 24-0163
   enPARAM24__LearnedFloor_68,           // 24-0164
   enPARAM24__LearnedFloor_69,           // 24-0165
   enPARAM24__LearnedFloor_70,           // 24-0166
   enPARAM24__LearnedFloor_71,           // 24-0167
   enPARAM24__LearnedFloor_72,           // 24-0168
   enPARAM24__LearnedFloor_73,           // 24-0169
   enPARAM24__LearnedFloor_74,           // 24-0170
   enPARAM24__LearnedFloor_75,           // 24-0171
   enPARAM24__LearnedFloor_76,           // 24-0172
   enPARAM24__LearnedFloor_77,           // 24-0173
   enPARAM24__LearnedFloor_78,           // 24-0174
   enPARAM24__LearnedFloor_79,           // 24-0175
   enPARAM24__LearnedFloor_80,           // 24-0176
   enPARAM24__LearnedFloor_81,           // 24-0177
   enPARAM24__LearnedFloor_82,           // 24-0178
   enPARAM24__LearnedFloor_83,           // 24-0179
   enPARAM24__LearnedFloor_84,           // 24-0180
   enPARAM24__LearnedFloor_85,           // 24-0181
   enPARAM24__LearnedFloor_86,           // 24-0182
   enPARAM24__LearnedFloor_87,           // 24-0183
   enPARAM24__LearnedFloor_88,           // 24-0184
   enPARAM24__LearnedFloor_89,           // 24-0185
   enPARAM24__LearnedFloor_90,           // 24-0186
   enPARAM24__LearnedFloor_91,           // 24-0187
   enPARAM24__LearnedFloor_92,           // 24-0188
   enPARAM24__LearnedFloor_93,           // 24-0189
   enPARAM24__LearnedFloor_94,           // 24-0190
   enPARAM24__LearnedFloor_95,           // 24-0191
   enPARAM24__COUNTER_WEIGHT_MID_POINT,  // 24-0192
   enPARAM24__Sabbath_Start_Time,        // 24-0193
   enPARAM24__Sabbath_End_Time,          // 24-0194
   enPARAM24__JobID,                     // 24-0195
   enPARAM24__PaymentPasscode,           // 24-0196
   enPARAM24__UNUSED_24BIT_197,          // 24-0197
   enPARAM24__UNUSED_24BIT_198,          // 24-0198
   enPARAM24__UNUSED_24BIT_199,          // 24-0199
   enPARAM24__UNUSED_24BIT_200,          // 24-0200
   enPARAM24__UNUSED_24BIT_201,          // 24-0201
   enPARAM24__UNUSED_24BIT_202,          // 24-0202
   enPARAM24__UNUSED_24BIT_203,          // 24-0203
   enPARAM24__UNUSED_24BIT_204,          // 24-0204
   enPARAM24__UNUSED_24BIT_205,          // 24-0205
   enPARAM24__UNUSED_24BIT_206,          // 24-0206
   enPARAM24__UNUSED_24BIT_207,          // 24-0207
   enPARAM24__UNUSED_24BIT_208,          // 24-0208

   NUM_24BIT_PARAMS
};

#define PARAM_STRINGS_24BIT(PARAM_STR_24BIT)  \
   PARAM_STR_24BIT( PI_0,                       "PI_0"                       ) \
   PARAM_STR_24BIT( PI_1,                       "PI_1"                       ) \
   PARAM_STR_24BIT( PI_2,                       "PI_2"                       ) \
   PARAM_STR_24BIT( PI_3,                       "PI_3"                       ) \
   PARAM_STR_24BIT( PI_4,                       "PI_4"                       ) \
   PARAM_STR_24BIT( PI_5,                       "PI_5"                       ) \
   PARAM_STR_24BIT( PI_6,                       "PI_6"                       ) \
   PARAM_STR_24BIT( PI_7,                       "PI_7"                       ) \
   PARAM_STR_24BIT( PI_8,                       "PI_8"                       ) \
   PARAM_STR_24BIT( PI_9,                       "PI_9"                       ) \
   PARAM_STR_24BIT( PI_10,                      "PI_10"                      ) \
   PARAM_STR_24BIT( PI_11,                      "PI_11"                      ) \
   PARAM_STR_24BIT( PI_12,                      "PI_12"                      ) \
   PARAM_STR_24BIT( PI_13,                      "PI_13"                      ) \
   PARAM_STR_24BIT( PI_14,                      "PI_14"                      ) \
   PARAM_STR_24BIT( PI_15,                      "PI_15"                      ) \
   PARAM_STR_24BIT( PI_16,                      "PI_16"                      ) \
   PARAM_STR_24BIT( PI_17,                      "PI_17"                      ) \
   PARAM_STR_24BIT( PI_18,                      "PI_18"                      ) \
   PARAM_STR_24BIT( PI_19,                      "PI_19"                      ) \
   PARAM_STR_24BIT( PI_20,                      "PI_20"                      ) \
   PARAM_STR_24BIT( PI_21,                      "PI_21"                      ) \
   PARAM_STR_24BIT( PI_22,                      "PI_22"                      ) \
   PARAM_STR_24BIT( PI_23,                      "PI_23"                      ) \
   PARAM_STR_24BIT( PI_24,                      "PI_24"                      ) \
   PARAM_STR_24BIT( PI_25,                      "PI_25"                      ) \
   PARAM_STR_24BIT( PI_26,                      "PI_26"                      ) \
   PARAM_STR_24BIT( PI_27,                      "PI_27"                      ) \
   PARAM_STR_24BIT( PI_28,                      "PI_28"                      ) \
   PARAM_STR_24BIT( PI_29,                      "PI_29"                      ) \
   PARAM_STR_24BIT( PI_30,                      "PI_30"                      ) \
   PARAM_STR_24BIT( PI_31,                      "PI_31"                      ) \
   PARAM_STR_24BIT( PI_32,                      "PI_32"                      ) \
   PARAM_STR_24BIT( PI_33,                      "PI_33"                      ) \
   PARAM_STR_24BIT( PI_34,                      "PI_34"                      ) \
   PARAM_STR_24BIT( PI_35,                      "PI_35"                      ) \
   PARAM_STR_24BIT( PI_36,                      "PI_36"                      ) \
   PARAM_STR_24BIT( PI_37,                      "PI_37"                      ) \
   PARAM_STR_24BIT( PI_38,                      "PI_38"                      ) \
   PARAM_STR_24BIT( PI_39,                      "PI_39"                      ) \
   PARAM_STR_24BIT( PI_40,                      "PI_40"                      ) \
   PARAM_STR_24BIT( PI_41,                      "PI_41"                      ) \
   PARAM_STR_24BIT( PI_42,                      "PI_42"                      ) \
   PARAM_STR_24BIT( PI_43,                      "PI_43"                      ) \
   PARAM_STR_24BIT( PI_44,                      "PI_44"                      ) \
   PARAM_STR_24BIT( PI_45,                      "PI_45"                      ) \
   PARAM_STR_24BIT( PI_46,                      "PI_46"                      ) \
   PARAM_STR_24BIT( PI_47,                      "PI_47"                      ) \
   PARAM_STR_24BIT( PI_48,                      "PI_48"                      ) \
   PARAM_STR_24BIT( PI_49,                      "PI_49"                      ) \
   PARAM_STR_24BIT( PI_50,                      "PI_50"                      ) \
   PARAM_STR_24BIT( PI_51,                      "PI_51"                      ) \
   PARAM_STR_24BIT( PI_52,                      "PI_52"                      ) \
   PARAM_STR_24BIT( PI_53,                      "PI_53"                      ) \
   PARAM_STR_24BIT( PI_54,                      "PI_54"                      ) \
   PARAM_STR_24BIT( PI_55,                      "PI_55"                      ) \
   PARAM_STR_24BIT( PI_56,                      "PI_56"                      ) \
   PARAM_STR_24BIT( PI_57,                      "PI_57"                      ) \
   PARAM_STR_24BIT( PI_58,                      "PI_58"                      ) \
   PARAM_STR_24BIT( PI_59,                      "PI_59"                      ) \
   PARAM_STR_24BIT( PI_60,                      "PI_60"                      ) \
   PARAM_STR_24BIT( PI_61,                      "PI_61"                      ) \
   PARAM_STR_24BIT( PI_62,                      "PI_62"                      ) \
   PARAM_STR_24BIT( PI_63,                      "PI_63"                      ) \
   PARAM_STR_24BIT( PI_64,                      "PI_64"                      ) \
   PARAM_STR_24BIT( PI_65,                      "PI_65"                      ) \
   PARAM_STR_24BIT( PI_66,                      "PI_66"                      ) \
   PARAM_STR_24BIT( PI_67,                      "PI_67"                      ) \
   PARAM_STR_24BIT( PI_68,                      "PI_68"                      ) \
   PARAM_STR_24BIT( PI_69,                      "PI_69"                      ) \
   PARAM_STR_24BIT( PI_70,                      "PI_70"                      ) \
   PARAM_STR_24BIT( PI_71,                      "PI_71"                      ) \
   PARAM_STR_24BIT( PI_72,                      "PI_72"                      ) \
   PARAM_STR_24BIT( PI_73,                      "PI_73"                      ) \
   PARAM_STR_24BIT( PI_74,                      "PI_74"                      ) \
   PARAM_STR_24BIT( PI_75,                      "PI_75"                      ) \
   PARAM_STR_24BIT( PI_76,                      "PI_76"                      ) \
   PARAM_STR_24BIT( PI_77,                      "PI_77"                      ) \
   PARAM_STR_24BIT( PI_78,                      "PI_78"                      ) \
   PARAM_STR_24BIT( PI_79,                      "PI_79"                      ) \
   PARAM_STR_24BIT( PI_80,                      "PI_80"                      ) \
   PARAM_STR_24BIT( PI_81,                      "PI_81"                      ) \
   PARAM_STR_24BIT( PI_82,                      "PI_82"                      ) \
   PARAM_STR_24BIT( PI_83,                      "PI_83"                      ) \
   PARAM_STR_24BIT( PI_84,                      "PI_84"                      ) \
   PARAM_STR_24BIT( PI_85,                      "PI_85"                      ) \
   PARAM_STR_24BIT( PI_86,                      "PI_86"                      ) \
   PARAM_STR_24BIT( PI_87,                      "PI_87"                      ) \
   PARAM_STR_24BIT( PI_88,                      "PI_88"                      ) \
   PARAM_STR_24BIT( PI_89,                      "PI_89"                      ) \
   PARAM_STR_24BIT( PI_90,                      "PI_90"                      ) \
   PARAM_STR_24BIT( PI_91,                      "PI_91"                      ) \
   PARAM_STR_24BIT( PI_92,                      "PI_92"                      ) \
   PARAM_STR_24BIT( PI_93,                      "PI_93"                      ) \
   PARAM_STR_24BIT( PI_94,                      "PI_94"                      ) \
   PARAM_STR_24BIT( PI_95,                      "PI_95"                      ) \
   PARAM_STR_24BIT( LearnedFloor_0,             "LRN FLR 0"                  ) \
   PARAM_STR_24BIT( LearnedFloor_1,             "LRN FLR 1"                  ) \
   PARAM_STR_24BIT( LearnedFloor_2,             "LRN FLR 2"                  ) \
   PARAM_STR_24BIT( LearnedFloor_3,             "LRN FLR 3"                  ) \
   PARAM_STR_24BIT( LearnedFloor_4,             "LRN FLR 4"                  ) \
   PARAM_STR_24BIT( LearnedFloor_5,             "LRN FLR 5"                  ) \
   PARAM_STR_24BIT( LearnedFloor_6,             "LRN FLR 6"                  ) \
   PARAM_STR_24BIT( LearnedFloor_7,             "LRN FLR 7"                  ) \
   PARAM_STR_24BIT( LearnedFloor_8,             "LRN FLR 8"                  ) \
   PARAM_STR_24BIT( LearnedFloor_9,             "LRN FLR 9"                  ) \
   PARAM_STR_24BIT( LearnedFloor_10,            "LRN FLR 10"                 ) \
   PARAM_STR_24BIT( LearnedFloor_11,            "LRN FLR 11"                 ) \
   PARAM_STR_24BIT( LearnedFloor_12,            "LRN FLR 12"                 ) \
   PARAM_STR_24BIT( LearnedFloor_13,            "LRN FLR 13"                 ) \
   PARAM_STR_24BIT( LearnedFloor_14,            "LRN FLR 14"                 ) \
   PARAM_STR_24BIT( LearnedFloor_15,            "LRN FLR 15"                 ) \
   PARAM_STR_24BIT( LearnedFloor_16,            "LRN FLR 16"                 ) \
   PARAM_STR_24BIT( LearnedFloor_17,            "LRN FLR 17"                 ) \
   PARAM_STR_24BIT( LearnedFloor_18,            "LRN FLR 18"                 ) \
   PARAM_STR_24BIT( LearnedFloor_19,            "LRN FLR 19"                 ) \
   PARAM_STR_24BIT( LearnedFloor_20,            "LRN FLR 20"                 ) \
   PARAM_STR_24BIT( LearnedFloor_21,            "LRN FLR 21"                 ) \
   PARAM_STR_24BIT( LearnedFloor_22,            "LRN FLR 22"                 ) \
   PARAM_STR_24BIT( LearnedFloor_23,            "LRN FLR 23"                 ) \
   PARAM_STR_24BIT( LearnedFloor_24,            "LRN FLR 24"                 ) \
   PARAM_STR_24BIT( LearnedFloor_25,            "LRN FLR 25"                 ) \
   PARAM_STR_24BIT( LearnedFloor_26,            "LRN FLR 26"                 ) \
   PARAM_STR_24BIT( LearnedFloor_27,            "LRN FLR 27"                 ) \
   PARAM_STR_24BIT( LearnedFloor_28,            "LRN FLR 28"                 ) \
   PARAM_STR_24BIT( LearnedFloor_29,            "LRN FLR 29"                 ) \
   PARAM_STR_24BIT( LearnedFloor_30,            "LRN FLR 30"                 ) \
   PARAM_STR_24BIT( LearnedFloor_31,            "LRN FLR 31"                 ) \
   PARAM_STR_24BIT( LearnedFloor_32,            "LRN FLR 32"                 ) \
   PARAM_STR_24BIT( LearnedFloor_33,            "LRN FLR 33"                 ) \
   PARAM_STR_24BIT( LearnedFloor_34,            "LRN FLR 34"                 ) \
   PARAM_STR_24BIT( LearnedFloor_35,            "LRN FLR 35"                 ) \
   PARAM_STR_24BIT( LearnedFloor_36,            "LRN FLR 36"                 ) \
   PARAM_STR_24BIT( LearnedFloor_37,            "LRN FLR 37"                 ) \
   PARAM_STR_24BIT( LearnedFloor_38,            "LRN FLR 38"                 ) \
   PARAM_STR_24BIT( LearnedFloor_39,            "LRN FLR 39"                 ) \
   PARAM_STR_24BIT( LearnedFloor_40,            "LRN FLR 40"                 ) \
   PARAM_STR_24BIT( LearnedFloor_41,            "LRN FLR 41"                 ) \
   PARAM_STR_24BIT( LearnedFloor_42,            "LRN FLR 42"                 ) \
   PARAM_STR_24BIT( LearnedFloor_43,            "LRN FLR 43"                 ) \
   PARAM_STR_24BIT( LearnedFloor_44,            "LRN FLR 44"                 ) \
   PARAM_STR_24BIT( LearnedFloor_45,            "LRN FLR 45"                 ) \
   PARAM_STR_24BIT( LearnedFloor_46,            "LRN FLR 46"                 ) \
   PARAM_STR_24BIT( LearnedFloor_47,            "LRN FLR 47"                 ) \
   PARAM_STR_24BIT( LearnedFloor_48,            "LRN FLR 48"                 ) \
   PARAM_STR_24BIT( LearnedFloor_49,            "LRN FLR 49"                 ) \
   PARAM_STR_24BIT( LearnedFloor_50,            "LRN FLR 50"                 ) \
   PARAM_STR_24BIT( LearnedFloor_51,            "LRN FLR 51"                 ) \
   PARAM_STR_24BIT( LearnedFloor_52,            "LRN FLR 52"                 ) \
   PARAM_STR_24BIT( LearnedFloor_53,            "LRN FLR 53"                 ) \
   PARAM_STR_24BIT( LearnedFloor_54,            "LRN FLR 54"                 ) \
   PARAM_STR_24BIT( LearnedFloor_55,            "LRN FLR 55"                 ) \
   PARAM_STR_24BIT( LearnedFloor_56,            "LRN FLR 56"                 ) \
   PARAM_STR_24BIT( LearnedFloor_57,            "LRN FLR 57"                 ) \
   PARAM_STR_24BIT( LearnedFloor_58,            "LRN FLR 58"                 ) \
   PARAM_STR_24BIT( LearnedFloor_59,            "LRN FLR 59"                 ) \
   PARAM_STR_24BIT( LearnedFloor_60,            "LRN FLR 60"                 ) \
   PARAM_STR_24BIT( LearnedFloor_61,            "LRN FLR 61"                 ) \
   PARAM_STR_24BIT( LearnedFloor_62,            "LRN FLR 62"                 ) \
   PARAM_STR_24BIT( LearnedFloor_63,            "LRN FLR 63"                 ) \
   PARAM_STR_24BIT( LearnedFloor_64,            "LRN FLR 64"                 ) \
   PARAM_STR_24BIT( LearnedFloor_65,            "LRN FLR 65"                 ) \
   PARAM_STR_24BIT( LearnedFloor_66,            "LRN FLR 66"                 ) \
   PARAM_STR_24BIT( LearnedFloor_67,            "LRN FLR 67"                 ) \
   PARAM_STR_24BIT( LearnedFloor_68,            "LRN FLR 68"                 ) \
   PARAM_STR_24BIT( LearnedFloor_69,            "LRN FLR 69"                 ) \
   PARAM_STR_24BIT( LearnedFloor_70,            "LRN FLR 70"                 ) \
   PARAM_STR_24BIT( LearnedFloor_71,            "LRN FLR 71"                 ) \
   PARAM_STR_24BIT( LearnedFloor_72,            "LRN FLR 72"                 ) \
   PARAM_STR_24BIT( LearnedFloor_73,            "LRN FLR 73"                 ) \
   PARAM_STR_24BIT( LearnedFloor_74,            "LRN FLR 74"                 ) \
   PARAM_STR_24BIT( LearnedFloor_75,            "LRN FLR 75"                 ) \
   PARAM_STR_24BIT( LearnedFloor_76,            "LRN FLR 76"                 ) \
   PARAM_STR_24BIT( LearnedFloor_77,            "LRN FLR 77"                 ) \
   PARAM_STR_24BIT( LearnedFloor_78,            "LRN FLR 78"                 ) \
   PARAM_STR_24BIT( LearnedFloor_79,            "LRN FLR 79"                 ) \
   PARAM_STR_24BIT( LearnedFloor_80,            "LRN FLR 80"                 ) \
   PARAM_STR_24BIT( LearnedFloor_81,            "LRN FLR 81"                 ) \
   PARAM_STR_24BIT( LearnedFloor_82,            "LRN FLR 82"                 ) \
   PARAM_STR_24BIT( LearnedFloor_83,            "LRN FLR 83"                 ) \
   PARAM_STR_24BIT( LearnedFloor_84,            "LRN FLR 84"                 ) \
   PARAM_STR_24BIT( LearnedFloor_85,            "LRN FLR 85"                 ) \
   PARAM_STR_24BIT( LearnedFloor_86,            "LRN FLR 86"                 ) \
   PARAM_STR_24BIT( LearnedFloor_87,            "LRN FLR 87"                 ) \
   PARAM_STR_24BIT( LearnedFloor_88,            "LRN FLR 88"                 ) \
   PARAM_STR_24BIT( LearnedFloor_89,            "LRN FLR 89"                 ) \
   PARAM_STR_24BIT( LearnedFloor_90,            "LRN FLR 90"                 ) \
   PARAM_STR_24BIT( LearnedFloor_91,            "LRN FLR 91"                 ) \
   PARAM_STR_24BIT( LearnedFloor_92,            "LRN FLR 92"                 ) \
   PARAM_STR_24BIT( LearnedFloor_93,            "LRN FLR 93"                 ) \
   PARAM_STR_24BIT( LearnedFloor_94,            "LRN FLR 94"                 ) \
   PARAM_STR_24BIT( LearnedFloor_95,            "LRN FLR 95"                 ) \
   PARAM_STR_24BIT( COUNTER_WEIGHT_MID_POINT,   "COUNTER_WEIGHT_MID_POINT"   ) \
   PARAM_STR_24BIT( Sabbath_Start_Time,         "Sabbath_Start_Time"         ) \
   PARAM_STR_24BIT( Sabbath_End_Time,           "Sabbath_End_Time"           ) \
   PARAM_STR_24BIT( JobID,                      "Job ID"                     ) \
   PARAM_STR_24BIT( PaymentPasscode,            "PaymentPasscode"            ) \
   PARAM_STR_24BIT( UNUSED_24BIT_197,           "NA"                         ) \
   PARAM_STR_24BIT( UNUSED_24BIT_198,           "NA"                         ) \
   PARAM_STR_24BIT( UNUSED_24BIT_199,           "NA"                         ) \
   PARAM_STR_24BIT( UNUSED_24BIT_200,           "NA"                         ) \
   PARAM_STR_24BIT( UNUSED_24BIT_201,           "NA"                         ) \
   PARAM_STR_24BIT( UNUSED_24BIT_202,           "NA"                         ) \
   PARAM_STR_24BIT( UNUSED_24BIT_203,           "NA"                         ) \
   PARAM_STR_24BIT( UNUSED_24BIT_204,           "NA"                         ) \
   PARAM_STR_24BIT( UNUSED_24BIT_205,           "NA"                         ) \
   PARAM_STR_24BIT( UNUSED_24BIT_206,           "NA"                         ) \
   PARAM_STR_24BIT( UNUSED_24BIT_207,           "NA"                         ) \
   PARAM_STR_24BIT( UNUSED_24BIT_208,           "NA"                         )

/* PARAM_DEFAULT_ENTRY_24BIT(A, B) FORMAT:
 *    A = Parameter Byte Map Index 
 *    B = (GRP1) Default All 8-Bit Map
 *    C = (GRP2) Default Learned Floors 8-Bit Map
 *    D = (GRP3) Default S-Curve 8-Bit Map
 *    E = (GRP4) Default Run Timers 8-Bit Map
 *    F = (GRP5) Default (UNUSED) 8-Bit Map
 *    G = (GRP6) Default (UNUSED) 8-Bit Map
 *    H = (GRP7) Default (UNUSED) 8-Bit Map
 *    I = (GRP8) Default (UNUSED) 8-Bit Map
 * */
/* Parameter Default Table Definition */
#define PARAM_DEFAULT_TABLE_24BIT(PARAM_DEFAULT_ENTRY_24BIT)  \
   PARAM_DEFAULT_ENTRY_24BIT( 0     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 1     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 2     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 3     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 4     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 5     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 6     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 7     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 8     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 9     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 10    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 11    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 12    , 0   , 255 , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 13    , 0   , 255 , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 14    , 0   , 255 , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 15    , 0   , 255 , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 16    , 0   , 255 , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 17    , 0   , 255 , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 18    , 0   , 255 , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 19    , 0   , 255 , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 20    , 0   , 255 , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 21    , 0   , 255 , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 22    , 0   , 255 , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 23    , 0   , 255 , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 24    , 254 , 1   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 25    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_24BIT( 26    , 1   , 0   , 0   , 0   , 0   , 0   , 0   , 0    )

/* Macros for accessing table elements */
#define EXPAND_PARAM_STRING_TABLE_24BIT_AS_ARRAY(A, B) B,

#define EXPAND_PARAM_DEFAULT_TABLE_24BIT_AS_DEFAULT_GRP_1(A, B, C, D, E, F, G, H, I) B,
#define EXPAND_PARAM_DEFAULT_TABLE_24BIT_AS_DEFAULT_GRP_2(A, B, C, D, E, F, G, H, I) C,
#define EXPAND_PARAM_DEFAULT_TABLE_24BIT_AS_DEFAULT_GRP_3(A, B, C, D, E, F, G, H, I) D,
#define EXPAND_PARAM_DEFAULT_TABLE_24BIT_AS_DEFAULT_GRP_4(A, B, C, D, E, F, G, H, I) E,
#define EXPAND_PARAM_DEFAULT_TABLE_24BIT_AS_DEFAULT_GRP_5(A, B, C, D, E, F, G, H, I) F,
#define EXPAND_PARAM_DEFAULT_TABLE_24BIT_AS_DEFAULT_GRP_6(A, B, C, D, E, F, G, H, I) G,
#define EXPAND_PARAM_DEFAULT_TABLE_24BIT_AS_DEFAULT_GRP_7(A, B, C, D, E, F, G, H, I) H,
#define EXPAND_PARAM_DEFAULT_TABLE_24BIT_AS_DEFAULT_GRP_8(A, B, C, D, E, F, G, H, I) I,

#endif