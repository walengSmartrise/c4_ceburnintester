/******************************************************************************
 *
 * @file     param.h
 * @brief    Parameters Header
 * @version  V1.00
 * @date     17, August 2016
 *
 * @note
 *
 ******************************************************************************/
#ifndef PARAM_H
#define PARAM_H
/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "param1Bit.h"
#include "param8Bit.h"
#include "param16Bit.h"
#include "param24Bit.h"
#include "param32Bit.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define BYTES_PER_PARAM_CHUNK                   ( 4 )
#define START_OF_CHUNK_BYTES_IN_BLOCK           ( 4 )
#define SIZE__PARAM_SAVE_QUEUE                  ( 64 )
#define TIMEOUT_PARAM_SAVE_10MS                 ( 3000 ) // [0, 65535]
#define NUM_OF_CHUNKS_PER_BLOCK                 ( 15U )//( 15U )
#define PARAM_CHUNK_REQ_COUNTDOWN               ( 5U )
#define NUM_TIMES_TO_RESEND_CHUNK_REQ           ( 3U )
#define NUM_TIMES_TO_RESEND_PARAM_SAVE_REQ      ( 1U )
#define NUM_TIMES_TO_RESEND_PARAM_CRC           ( 3U )

#define PARAM_SYNC_FAULT_DEBOUNCE_LIMIT   (1U)

#define NUM_PARAM_BLOCKS (NUM_PARAM_EEPROMI_PAGES - enEEPROMI_PAGE__BLOCK_00)
//-----------------------------------------------------------------------------
// For param.h
//-----------------------------------------------------------------------------
#define STARTING_BLOCK_U32         ( 0 )
#define STARTING_BLOCK_U24         ( 3 )
#define STARTING_BLOCK_U16         ( 14 )
#define STARTING_BLOCK_U8          ( 54 )
#define STARTING_BLOCK_BIT         ( 59 )
//-----------------------------------------------------------------------------
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

typedef enum
{
   DEFAULT_CMD__NONE,
   DEFAULT_CMD__ALL,
   DEFAULT_CMD__FLOORS,
   DEFAULT_CMD__SCURVE,
   DEFAULT_CMD__RUN_TIMERS,
   DEFAULT_CMD__IO,
   DEFAULT_CMD__GRP6,
   DEFAULT_CMD__GRP7,
   DEFAULT_CMD__GRP8,
   DEFAULT_CMD__FACTORY,

   NUM_DEFAULT_CMD
}enum_default_cmd;
/*----------------------------------------------------------------------------

            Don't change below here

 *----------------------------------------------------------------------------*/
/* Buffer */
enum en_param_faults
{
   PARAM_FAULT__NONE,
   PARAM_FAULT__OVERFLOW,
   PARAM_FAULT__TIMEOUT,
   PARAM_FAULT__PROTECTED,
   PARAM_FAULT__LOCKEDUP,

   NUM_PARAM_FAULT
};

enum en_param_datagram_message_types
{
   enPARAM_DG__NULL,  // no action requested, no data being sent

   enPARAM_DG__REQ_FOR_DEV_SIG,  // request for device signature
   enPARAM_DG__REQ_FOR_BLOCK_CRC,  // request for block CRC
   enPARAM_DG__REQ_FOR_PARAM_VALUE,  // request for parameter value
   enPARAM_DG__REQ_TO_DEFAULT_BLOCK,  // request to default a block
   enPARAM_DG__REQ_TO_SET_A_PARAM,  // request to set a parameter's value
   enPARAM_DG__SENDING_DEV_SIG,  // sending device signature
   enPARAM_DG__SENDING_BLOCK_CRC,  // sending block CRC
   enPARAM_DG__SENDING_BLOCK_DFLT_COUNT,  // sending block default count
   enPARAM_DG__SENDING_PARAM_VALUE,  // sending parameter value

   enPARAM_DG__REQ_FOR_PARAM_CHUNK,  // request for parameter block
   enPARAM_DG__SENDING_PARAM_CHUNK,  // sending parameter block

   NUM_PARAM_DG_TYPES

};


//-----------------------------------------------------------------------------
enum en_param_block_types
{
   enPARAM_BLOCK_TYPE__UNKNOWN,

   enPARAM_BLOCK_TYPE__BIT,
   enPARAM_BLOCK_TYPE__UINT8,
   enPARAM_BLOCK_TYPE__UINT16,
   enPARAM_BLOCK_TYPE__UINT24,
   enPARAM_BLOCK_TYPE__UINT32,
};
//-----------------------------------------------------------------------------
enum en_param_elements_per_type // for default param enable mask
{
   enPARAM_NUM_PER_TYPE__BIT = 58*8,
   enPARAM_NUM_PER_TYPE__UINT8 = 58,
   enPARAM_NUM_PER_TYPE__UINT16 = 29,
   enPARAM_NUM_PER_TYPE__UINT24 = 19,
   enPARAM_NUM_PER_TYPE__UINT32 = 14,
};

//-----------------------------------------------------------------------------
enum en_param_eepromi_page_names
{
   enEEPROMI_PAGE__TEMP,
   enEEPROMI_PAGE__RESERVED1,
   enEEPROMI_PAGE__RESERVED2,
   enEEPROMI_PAGE__BLOCK_00,
   enEEPROMI_PAGE__BLOCK_01,
   enEEPROMI_PAGE__BLOCK_02,
   enEEPROMI_PAGE__BLOCK_03,
   enEEPROMI_PAGE__BLOCK_04,
   enEEPROMI_PAGE__BLOCK_05,
   enEEPROMI_PAGE__BLOCK_06,
   enEEPROMI_PAGE__BLOCK_07,
   enEEPROMI_PAGE__BLOCK_08,
   enEEPROMI_PAGE__BLOCK_09,
   enEEPROMI_PAGE__BLOCK_10,
   enEEPROMI_PAGE__BLOCK_11,
   enEEPROMI_PAGE__BLOCK_12,
   enEEPROMI_PAGE__BLOCK_13,
   enEEPROMI_PAGE__BLOCK_14,
   enEEPROMI_PAGE__BLOCK_15,
   enEEPROMI_PAGE__BLOCK_16,
   enEEPROMI_PAGE__BLOCK_17,
   enEEPROMI_PAGE__BLOCK_18,
   enEEPROMI_PAGE__BLOCK_19,
   enEEPROMI_PAGE__BLOCK_20,
   enEEPROMI_PAGE__BLOCK_21,
   enEEPROMI_PAGE__BLOCK_22,
   enEEPROMI_PAGE__BLOCK_23,
   enEEPROMI_PAGE__BLOCK_24,
   enEEPROMI_PAGE__BLOCK_25,
   enEEPROMI_PAGE__BLOCK_26,
   enEEPROMI_PAGE__BLOCK_27,
   enEEPROMI_PAGE__BLOCK_28,
   enEEPROMI_PAGE__BLOCK_29,
   enEEPROMI_PAGE__BLOCK_30,
   enEEPROMI_PAGE__BLOCK_31,
   enEEPROMI_PAGE__BLOCK_32,
   enEEPROMI_PAGE__BLOCK_33,
   enEEPROMI_PAGE__BLOCK_34,
   enEEPROMI_PAGE__BLOCK_35,
   enEEPROMI_PAGE__BLOCK_36,
   enEEPROMI_PAGE__BLOCK_37,
   enEEPROMI_PAGE__BLOCK_38,
   enEEPROMI_PAGE__BLOCK_39,
   enEEPROMI_PAGE__BLOCK_40,
   enEEPROMI_PAGE__BLOCK_41,
   enEEPROMI_PAGE__BLOCK_42,
   enEEPROMI_PAGE__BLOCK_43,
   enEEPROMI_PAGE__BLOCK_44,
   enEEPROMI_PAGE__BLOCK_45,
   enEEPROMI_PAGE__BLOCK_46,
   enEEPROMI_PAGE__BLOCK_47,
   enEEPROMI_PAGE__BLOCK_48,
   enEEPROMI_PAGE__BLOCK_49,
   enEEPROMI_PAGE__BLOCK_50,
   enEEPROMI_PAGE__BLOCK_51,
   enEEPROMI_PAGE__BLOCK_52,
   enEEPROMI_PAGE__BLOCK_53,
   enEEPROMI_PAGE__BLOCK_54,
   enEEPROMI_PAGE__BLOCK_55,
   enEEPROMI_PAGE__BLOCK_56,
   enEEPROMI_PAGE__BLOCK_57,
   enEEPROMI_PAGE__BLOCK_58,
   enEEPROMI_PAGE__BLOCK_59,

   NUM_PARAM_EEPROMI_PAGES
};
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

uint8_t Param_ReadValue_1Bit( enum en_1bit_params enParamNum );
uint8_t Param_ReadValue_8Bit( enum en_8bit_params enParamNum );
uint16_t Param_ReadValue_16Bit( enum en_16bit_params enParamNum );
uint32_t Param_ReadValue_24Bit( enum en_24bit_params enParamNum );
uint32_t Param_ReadValue_32Bit( enum en_32bit_params enParamNum );

uint32_t Param_ReadValue_ByType( enum en_param_block_types eParamType, uint16_t uwParameterIndex );

uint8_t Param_WriteValue_1Bit( enum en_1bit_params enParamNum, uint8_t bValue );
uint8_t Param_WriteValue_8Bit( enum en_8bit_params enParamNum, uint8_t ucValue );
uint8_t Param_WriteValue_16Bit( enum en_16bit_params enParamNum, uint16_t uwValue );
uint8_t Param_WriteValue_24Bit( enum en_24bit_params enParamNum, uint32_t ulValue );
uint8_t Param_WriteValue_32Bit( enum en_32bit_params enParamNum, uint32_t ulValue );

uint8_t Param_WriteValue_ByType( enum en_param_block_types eParamType, uint16_t uwParamIndex, uint32_t uiParamValue );

uint8_t Param_GetBlockIndex_1Bit( enum en_1bit_params enParamNum );
uint8_t Param_GetBlockIndex_8Bit( enum en_8bit_params enParamNum );
uint8_t Param_GetBlockIndex_16Bit( enum en_16bit_params enParamNum );
uint8_t Param_GetBlockIndex_24Bit( enum en_24bit_params enParamNum );
uint8_t Param_GetBlockIndex_32Bit( enum en_32bit_params enParamNum );

uint16_t Param_GetParamIndex_1Bit( enum en_1bit_params enParamNum );
uint16_t Param_GetParamIndex_8Bit( enum en_8bit_params enParamNum );
uint16_t Param_GetParamIndex_16Bit( enum en_16bit_params enParamNum );
uint16_t Param_GetParamIndex_24Bit( enum en_24bit_params enParamNum );
uint16_t Param_GetParamIndex_32Bit( enum en_32bit_params enParamNum );

uint8_t ParamBuff_GetFault( void );

uint8_t Param_EmptySlotsInBuffer(void);
void ParamBuff_ClrFault( void );
void Param_ServiceBuffer( void );
void Param_SetMaster(void);
void Param_InitRingBuffers(void);
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif


