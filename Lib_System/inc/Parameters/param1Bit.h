#ifndef SMARTRISE_PARAMETERS_1BIT
#define SMARTRISE_PARAMETERS_1BIT

/* NOTE: FILE UPDATED VIA LIB_SYSTEM - SUPPORT,
 * Edit SystemParameters.xlsx run Update_Parameters.bat
 * to automatically update this file. */

enum en_1bit_params
{
   enPARAM1__FireRecallDoorM,                         // 01-0000
   enPARAM1__FireRecallDoorA,                         // 01-0001
   enPARAM1__Fire__Smoke_Main_UseAltFloor,            // 01-0002
   enPARAM1__Fire__Smoke_Alt_UseAltFloor,             // 01-0003
   enPARAM1__Fire__Smoke_MachineRoom_UseAltFloor,     // 01-0004
   enPARAM1__Fire__Smoke_Hoistway_UseAltFloor,        // 01-0005
   enPARAM1__Fire__Smoke_Main_FlashFireHat,           // 01-0006
   enPARAM1__Fire__Smoke_Alt_FlashFireHat,            // 01-0007
   enPARAM1__Fire__Smoke_MachineRoom_FlashFireHat,    // 01-0008
   enPARAM1__Fire__Smoke_Hoistway_FlashFireHat,       // 01-0009
   enPARAM1__Fire__Smoke_Main_ShuntOnRecall,          // 01-0010
   enPARAM1__Fire__Smoke_Alt_ShuntOnRecall,           // 01-0011
   enPARAM1__Fire__Smoke_MachineRoom_ShuntOnRecall,   // 01-0012
   enPARAM1__Fire__Smoke_Hoistway_ShuntOnRecall,      // 01-0013
   enPARAM1__Fire__ResetToExitPhase1,                 // 01-0014
   enPARAM1__Fire__DisableDoorRestrictorOnPhase_2,    // 01-0015
   enPARAM1__Fire__Phase2SwingReopenDisabled,         // 01-0016
   enPARAM1__Fire__Group3TypeHoldSwitch,              // 01-0017
   enPARAM1__Fire__IgnoreLocksJumpedOnPhase2,         // 01-0018
   enPARAM1__Fire__FireStopSwitchKillsDoorOperator,   // 01-0019
   enPARAM1__Fire__DOL_toExitPhase2,                  // 01-0020
   enPARAM1__UNUSED_1BIT_021,                         // 01-0021
   enPARAM1__Fire__OkToStopOutsideDoorZone,           // 01-0022
   enPARAM1__Fire__AllowResetWithActiveSmoke,         // 01-0023
   enPARAM1__Fire__HatFlashIgnoreOrder,               // 01-0024
   enPARAM1__Fire__MomentaryDoorCloseButton,          // 01-0025
   enPARAM1__Fire__FlashLobbyFireLamp,                // 01-0026
   enPARAM1__Fire__RemoteAndMainToOverrideSmoke,      // 01-0027
   enPARAM1__Fire__EnablePHE_OnPhase2,                // 01-0028
   enPARAM1__Fire__DoorOpenOnHold,                    // 01-0029
   enPARAM1__VIP_Priority_Dispatching,                // 01-0030
   enPARAM1__Fire__Smoke_Pit_FlashFireHat,            // 01-0031
   enPARAM1__Fire__Smoke_Pit_ShuntOnRecall,           // 01-0032
   enPARAM1__NumDoors,                                // 01-0033
   enPARAM1__BypassTermLimits,                        // 01-0034
   enPARAM1__EBrakeOnOverspeed,                       // 01-0035
   enPARAM1__Fire__Smoke_Pit_UseAltFloor,             // 01-0036
   enPARAM1__Enable_Pit_Inspection,                   // 01-0037
   enPARAM1__Enable_Landing_Inspection,               // 01-0038
   enPARAM1__ImproveMaxSpeed,                         // 01-0039
   enPARAM1__DisableBypassICStop,                     // 01-0040
   enPARAM1__RelevelEnabled,                          // 01-0041
   enPARAM1__EnableEarthQuake,                        // 01-0042
   enPARAM1__EnableMidFlightDestinationChange,        // 01-0043
   enPARAM1__DisableBrakeboard,                       // 01-0044
   enPARAM1__DzStuckHighTesting,                      // 01-0045
   enPARAM1__LearnBrake,                              // 01-0046
   enPARAM1__DEBUG_TransmitRunLog,                    // 01-0047
   enPARAM1__Enable_Freight_Doors,                    // 01-0048
   enPARAM1__Enable_Freight_DCM,                      // 01-0049
   enPARAM1__Enable_Freight_AutoClose,                // 01-0050
   enPARAM1__LearnBrake2,                             // 01-0051
   enPARAM1__TestUnintendedMovement,                  // 01-0052
   enPARAM1__EnableEmergencyDispatch,                 // 01-0053
   enPARAM1__BPS_PRIM_NC,                             // 01-0054
   enPARAM1__BPS_SEC_NC,                              // 01-0055
   enPARAM1__DEBUG_RunTerminalToTerminal_R,           // 01-0056
   enPARAM1__DisableCEDESOffline,                     // 01-0057
   enPARAM1__DisableAutoDriveReset,                   // 01-0058
   enPARAM1__EnableSecondaryBrake,                    // 01-0059
   enPARAM1__EnableRiserAlarms,                       // 01-0060
   enPARAM1__DEBUG_RunTerminalToTerminal,             // 01-0061
   enPARAM1__DEBUG_RunFloorToFloor,                   // 01-0062
   enPARAM1__DEBUG_DisableUpdateNTS,                  // 01-0063
   enPARAM1__DEBUG_DisablePreflight,                  // 01-0064
   enPARAM1__Ind_Srv_OverridesSecurity,               // 01-0065
   enPARAM1__LWD_EnableWifi,                          // 01-0066
   enPARAM1__Invert_NTS_Stop,                         // 01-0067
   enPARAM1__LWD_AutoRecalibrate,                     // 01-0068
   enPARAM1__EnableSpeedDevControl,                   // 01-0069
   enPARAM1__LWD_TriggerRecalibrate,                  // 01-0070
   enPARAM1__LWD_TriggerLoadLearn,                    // 01-0071
   enPARAM1__EnableConstructionRunBox,                // 01-0072
   enPARAM1__DisableConstructionOverspeed,            // 01-0073
   enPARAM1__DEBUG_RunTerminalToTerminal_F,           // 01-0074
   enPARAM1__ICInspectionRequiredForCT,               // 01-0075
   enPARAM1__Door_DC_ON_RUN,                          // 01-0076
   enPARAM1__DEBUG_RunFloorToFloor_R,                 // 01-0077
   enPARAM1__Debug_LWD,                               // 01-0078
   enPARAM1__OOS_RearOpening,                         // 01-0079
   enPARAM1__OOS_Disable,                             // 01-0080
   enPARAM1__OOS_SetDoorOpen,                         // 01-0081
   enPARAM1__Swing_CallsEnable,                       // 01-0082
   enPARAM1__Swing_StayInGroup,                       // 01-0083
   enPARAM1__DoorLocksJumpedOnDOL,                    // 01-0084
   enPARAM1__NC_INPUT_CustomMode,                     // 01-0085
   enPARAM1__CustomMode_IgnoreCarCallSecurity,        // 01-0086
   enPARAM1__CustomMode_IgnoreHallCallSecurity,       // 01-0087
   enPARAM1__CustomMode_AllowedOutsideDoorZone,       // 01-0088
   enPARAM1__CustomMode_ParkingEnabled,               // 01-0089
   enPARAM1__CustomMode_IgnoredCarCall_F,             // 01-0090
   enPARAM1__CustomMode_IgnoredCarCall_R,             // 01-0091
   enPARAM1__CustomMode_IgnoreHallCall,               // 01-0092
   enPARAM1__CustomMode_AutoDoorOpen,                 // 01-0093
   enPARAM1__CustomMode_DoorHold,                     // 01-0094
   enPARAM1__CustomMode_IgnoreDCB,                    // 01-0095
   enPARAM1__CustomMode_ForceDoorsOpenOrClosed,       // 01-0096
   enPARAM1__EMS_AllowPh2WithoutPh1,                  // 01-0097
   enPARAM1__EMS_ExitPh2AtAnyFloor,                   // 01-0098
   enPARAM1__DEBUG_RunFloorToFloor_F,                 // 01-0099
   enPARAM1__EMS_FireOverridesEMS1,                   // 01-0100
   enPARAM1__BPSStuckOpenDropsEBrake,                 // 01-0101
   enPARAM1__FLOOD_OverrideFire,                      // 01-0102
   enPARAM1__FLOOD_OkayToRun,                         // 01-0103
   enPARAM1__ATTD_DirWithCCB,                         // 01-0104
   enPARAM1__Rescue_RecTrvDir,                        // 01-0105
   enPARAM1__CC_Acknowledge,                          // 01-0106
   enPARAM1__DEBUG_MonitorCarDirection,               // 01-0107
   enPARAM1__DC_On_DoorCloseState,                    // 01-0108
   enPARAM1__DO_On_DoorOpenState,                     // 01-0109
   enPARAM1__EnableRandomCarCallRuns_R,               // 01-0110
   enPARAM1__DisableBPS_StopSeq,                      // 01-0111
   enPARAM1__DisableBPS_StuckHigh,                    // 01-0112
   enPARAM1__DisableBPS_StuckLow,                     // 01-0113
   enPARAM1__EnableRandomCarCallRuns,                 // 01-0114
   enPARAM1__CT_Stop_SW_Kills_Doors,                  // 01-0115
   enPARAM1__DisableIdleTravelArrows,                 // 01-0116
   enPARAM1__DisableBrakeOverheat,                    // 01-0117
   enPARAM1__DisableDoorsOnHA,                        // 01-0118
   enPARAM1__EnableLoadLearn,                         // 01-0119
   enPARAM1__EnableIMotionDoors,                      // 01-0120
   enPARAM1__EnableDSDFullField,                      // 01-0121
   enPARAM1__StopSeq_DisableRampZero,                 // 01-0122
   enPARAM1__StopSeq_DisableHoldZero,                 // 01-0123
   enPARAM1__DEBUG_IncreaseMRBSendRate,               // 01-0124
   enPARAM1__Debug_FastGroupResend,                   // 01-0125
   enPARAM1__EnablePreflightTestDIP,                  // 01-0126
   enPARAM1__DisableEPower,                           // 01-0127
   enPARAM1__EnableUIDriveEdit,                       // 01-0128
   enPARAM1__EnableOpModeAlarm,                       // 01-0129
   enPARAM1__EnableStopAtNextAlarm,                   // 01-0130
   enPARAM1__BypassFireSrv,                           // 01-0131
   enPARAM1__ParkingWithDoorOpen,                     // 01-0132
   enPARAM1__EnableLatchesCC,                         // 01-0133
   enPARAM1__NoDemandDoorsOpen,                       // 01-0134
   enPARAM1__EnableCPLDOffline,                       // 01-0135
   enPARAM1__DebounceLatchedFault,                    // 01-0136
   enPARAM1__EnableOldFRAM,                           // 01-0137
   enPARAM1__EnableHallSecurity,                      // 01-0138
   enPARAM1__Sabbath_KeyEnable_Only,                  // 01-0139
   enPARAM1__Sabbath_KeyOrTimer_Enable,               // 01-0140
   enPARAM1__Sabbath_TimerEnable_Only,                // 01-0141
   enPARAM1__Doors_NudgeBuzzerOnly,                   // 01-0142
   enPARAM1__Doors_NudgeNoBuzzer,                     // 01-0143
   enPARAM1__Enable3DigitPI,                          // 01-0144
   enPARAM1__DefaultFRAM,                             // 01-0145
   enPARAM1__Enable_Dynamic_Parking,                  // 01-0146
   enPARAM1__Enable_CEDES2,                           // 01-0147
   enPARAM1__Enable_ETSL,                             // 01-0148
   enPARAM1__DisableCE_FloorPlus1,                    // 01-0149
   enPARAM1__EnableEStopAlarms,                       // 01-0150
   enPARAM1__EnableInspDoorOpenOutOfDZ,               // 01-0151
   enPARAM1__DSD_EarlyFieldEnable,                    // 01-0152
   enPARAM1__DisableNonTerminalNTS,                   // 01-0153
   enPARAM1__TestTrcLoss,                             // 01-0154
   enPARAM1__DisableInvertKEBSpeed,                   // 01-0155
   enPARAM1__DuparCOP_Enable,                         // 01-0156
   enPARAM1__EnableRegenOnEP,                         // 01-0157
   enPARAM1__EBrakeOnETSandETSL,                      // 01-0158
   enPARAM1__EnableOpenDoorsAlarm,                    // 01-0159
   enPARAM1__CarToLobbyExpress,                       // 01-0160
   enPARAM1__DoubleChimeOnDown,                       // 01-0161
   enPARAM1__DisableBPS2_StuckHigh,                   // 01-0162
   enPARAM1__DisableBPS2_StuckLow,                    // 01-0163
   enPARAM1__Enable_Janus_RS_Fixture,                 // 01-0164
   enPARAM1__LearnOpeningTime,                        // 01-0165
   enPARAM1__EPowerPretransferStall,                  // 01-0166
   enPARAM1__XREG_EnableInMotionAssignment,           // 01-0167
   enPARAM1__XREG_PriorityFromArrivalDir,             // 01-0168
   enPARAM1__FRAM_EnableAlarms,                       // 01-0169
   enPARAM1__DisableLatchingBrakeFault,               // 01-0170
   enPARAM1__DisablePIOOS,                            // 01-0171
   enPARAM1__InMotionOpeningAlarm,                    // 01-0172
   enPARAM1__DisableDOBSecuredFloor,                  // 01-0173
   enPARAM1__ReducedMaxSpeed,                         // 01-0174
   enPARAM1__ArvLanternDoor_1,                        // 01-0175
   enPARAM1__ArvLanternDoor_2,                        // 01-0176
   enPARAM1__ArvLanternDoor_3,                        // 01-0177
   enPARAM1__ArvLanternDoor_4,                        // 01-0178
   enPARAM1__ArvLanternDoor_5,                        // 01-0179
   enPARAM1__BContactsNC,                             // 01-0180
   enPARAM1__EnableAltMachineRoom,                    // 01-0181
   enPARAM1__Fire__Smoke_MachineRoom2_FlashFireHat,   // 01-0182
   enPARAM1__Fire__Smoke_Hoistway2_FlashFireHat,      // 01-0183
   enPARAM1__Fire__Smoke_MachineRoom2_UseAltFloor,    // 01-0184
   enPARAM1__Fire__Smoke_Hoistway2_UseAltFloor,       // 01-0185
   enPARAM1__Fire__Smoke_MachineRoom2_ShuntOnRecall,  // 01-0186
   enPARAM1__Fire__Smoke_Hoistway2_ShuntOnRecall,     // 01-0187
   enPARAM1__EnableClearCC,                           // 01-0188
   enPARAM1__EnableDualPHE_Test,                      // 01-0189
   enPARAM1__EnablePretorqueTest,                     // 01-0190
   enPARAM1__SuppressReopenOnGSW,                     // 01-0191
   enPARAM1__EnableCheckInFloor,                      // 01-0192
   enPARAM1__EnablePassingLobbyDO,                    // 01-0193
   enPARAM1__EnableNeverDropHallCalls,                // 01-0194
   enPARAM1__EnableExtHallBoards,                     // 01-0195
   enPARAM1__DisableNoDestStop,                       // 01-0196
   enPARAM1__DisableSabbathReleveling,                // 01-0197
   enPARAM1__CW_Derail_NO,                            // 01-0198
   enPARAM1__EnableBoardRTC,                          // 01-0199
   enPARAM1__EnableCPLD_V2,                           // 01-0200
   enPARAM1__EnableCPLD_V3,                           // 01-0201
   enPARAM1__DisableDestLossStop,                     // 01-0202
   enPARAM1__Fire__RecallToMainAfterFirePhase2,       // 01-0203
   enPARAM1__EnableDL20_CT,                           // 01-0204
   enPARAM1__EnableDL20_COP,                          // 01-0205
   enPARAM1__DisableDL20_Buzzer,                      // 01-0206
   enPARAM1__Door_RetiringCAM,                        // 01-0207
   enPARAM1__Door_FixedHallCAM,                       // 01-0208
   enPARAM1__Door_HallClosedReqForCAM,                // 01-0209
   enPARAM1__EnableEX51_CT,                           // 01-0210
   enPARAM1__EnableEX51_COP,                          // 01-0211
   enPARAM1__EnableBrakeV2,                           // 01-0212
   enPARAM1__DynamicParkingDO_1,                      // 01-0213
   enPARAM1__DynamicParkingDO_2,                      // 01-0214
   enPARAM1__DynamicParkingDO_3,                      // 01-0215
   enPARAM1__DynamicParkingDO_4,                      // 01-0216
   enPARAM1__DynamicParkingDO_5,                      // 01-0217
   enPARAM1__DynamicParkingDO_6,                      // 01-0218
   enPARAM1__DynamicParkingDO_7,                      // 01-0219
   enPARAM1__DynamicParkingDO_8,                      // 01-0220
   enPARAM1__Fire2SwingReopen,                        // 01-0221
   enPARAM1__FreightTestPHE,                          // 01-0222
   enPARAM1__SabbathDisableLWD,                       // 01-0223
   enPARAM1__EnableShieldAlarms,                      // 01-0224
   enPARAM1__EnableExtFloorLimit,                     // 01-0225
   enPARAM1__EnableCE_V2,                             // 01-0226
   enPARAM1__Fire__DisableLatchSmokes,                // 01-0227
   enPARAM1__Fire__DisableLatchLobbyKey,              // 01-0228
   enPARAM1__Fire__DisableLatchMainRecallFloor,       // 01-0229
   enPARAM1__DISA_CPLD_OVF_ALARM,                     // 01-0230
   enPARAM1__Fire__ResetOnTransitionFire1,            // 01-0231
   enPARAM1__AN_ClrReverseDirCC,                      // 01-0232
   enPARAM1__EnableVIPTimeoutAlarm,                   // 01-0233
   enPARAM1__Sabbath_EnableExtBuzzer,                 // 01-0234
   enPARAM1__Disable_VirtualInputs,                   // 01-0235
   enPARAM1__Ind_Srv_IgnoreFrontCCB,                  // 01-0236
   enPARAM1__DISA_DoorJumperCheck,                    // 01-0237
   enPARAM1__NudgeWithoutOnwardDemand,                // 01-0238
   enPARAM1__EQ_OldJobSupport,                        // 01-0239
   enPARAM1__DISA_CAM_ON_HA,                          // 01-0240
   enPARAM1__Disable_Rear_DOB,                        // 01-0241
   enPARAM1__Sabbath_NudgeDoors,                      // 01-0242
   enPARAM1__EnableC4SoftStarter,                     // 01-0243
   enPARAM1__ENA_DAD_FAULT_RESEND,                    // 01-0244
   enPARAM1__EnableRandomCarCallRuns_F,               // 01-0245
   enPARAM1__EQ_Buzzer,                               // 01-0246
   enPARAM1__MLT_Fire1_CloseDoors,                    // 01-0247
   enPARAM1__FlashFireHatOnLowOil,                    // 01-0248
   enPARAM1__JackResync_IgnoreCalls,                  // 01-0249
   enPARAM1__UNUSED_1BIT_250,                         // 01-0250
   enPARAM1__UNUSED_1BIT_251,                         // 01-0251
   enPARAM1__UNUSED_1BIT_252,                         // 01-0252
   enPARAM1__UNUSED_1BIT_253,                         // 01-0253
   enPARAM1__UNUSED_1BIT_254,                         // 01-0254
   enPARAM1__UNUSED_1BIT_255,                         // 01-0255
   enPARAM1__UNUSED_1BIT_256,                         // 01-0256
   enPARAM1__UNUSED_1BIT_257,                         // 01-0257
   enPARAM1__UNUSED_1BIT_258,                         // 01-0258
   enPARAM1__UNUSED_1BIT_259,                         // 01-0259
   enPARAM1__UNUSED_1BIT_260,                         // 01-0260
   enPARAM1__UNUSED_1BIT_261,                         // 01-0261
   enPARAM1__UNUSED_1BIT_262,                         // 01-0262
   enPARAM1__UNUSED_1BIT_263,                         // 01-0263
   enPARAM1__UNUSED_1BIT_264,                         // 01-0264
   enPARAM1__UNUSED_1BIT_265,                         // 01-0265
   enPARAM1__UNUSED_1BIT_266,                         // 01-0266
   enPARAM1__UNUSED_1BIT_267,                         // 01-0267
   enPARAM1__UNUSED_1BIT_268,                         // 01-0268
   enPARAM1__UNUSED_1BIT_269,                         // 01-0269
   enPARAM1__UNUSED_1BIT_270,                         // 01-0270
   enPARAM1__UNUSED_1BIT_271,                         // 01-0271
   enPARAM1__UNUSED_1BIT_272,                         // 01-0272
   enPARAM1__UNUSED_1BIT_273,                         // 01-0273
   enPARAM1__UNUSED_1BIT_274,                         // 01-0274
   enPARAM1__UNUSED_1BIT_275,                         // 01-0275
   enPARAM1__UNUSED_1BIT_276,                         // 01-0276
   enPARAM1__UNUSED_1BIT_277,                         // 01-0277
   enPARAM1__UNUSED_1BIT_278,                         // 01-0278
   enPARAM1__UNUSED_1BIT_279,                         // 01-0279
   enPARAM1__UNUSED_1BIT_280,                         // 01-0280
   enPARAM1__UNUSED_1BIT_281,                         // 01-0281
   enPARAM1__UNUSED_1BIT_282,                         // 01-0282
   enPARAM1__UNUSED_1BIT_283,                         // 01-0283
   enPARAM1__UNUSED_1BIT_284,                         // 01-0284
   enPARAM1__UNUSED_1BIT_285,                         // 01-0285
   enPARAM1__UNUSED_1BIT_286,                         // 01-0286
   enPARAM1__UNUSED_1BIT_287,                         // 01-0287
   enPARAM1__UNUSED_1BIT_288,                         // 01-0288
   enPARAM1__UNUSED_1BIT_289,                         // 01-0289
   enPARAM1__UNUSED_1BIT_290,                         // 01-0290
   enPARAM1__UNUSED_1BIT_291,                         // 01-0291
   enPARAM1__UNUSED_1BIT_292,                         // 01-0292
   enPARAM1__UNUSED_1BIT_293,                         // 01-0293
   enPARAM1__UNUSED_1BIT_294,                         // 01-0294
   enPARAM1__UNUSED_1BIT_295,                         // 01-0295
   enPARAM1__UNUSED_1BIT_296,                         // 01-0296
   enPARAM1__UNUSED_1BIT_297,                         // 01-0297
   enPARAM1__UNUSED_1BIT_298,                         // 01-0298
   enPARAM1__UNUSED_1BIT_299,                         // 01-0299
   enPARAM1__UNUSED_1BIT_300,                         // 01-0300
   enPARAM1__UNUSED_1BIT_301,                         // 01-0301
   enPARAM1__UNUSED_1BIT_302,                         // 01-0302
   enPARAM1__UNUSED_1BIT_303,                         // 01-0303
   enPARAM1__UNUSED_1BIT_304,                         // 01-0304
   enPARAM1__UNUSED_1BIT_305,                         // 01-0305
   enPARAM1__UNUSED_1BIT_306,                         // 01-0306
   enPARAM1__UNUSED_1BIT_307,                         // 01-0307
   enPARAM1__UNUSED_1BIT_308,                         // 01-0308
   enPARAM1__UNUSED_1BIT_309,                         // 01-0309
   enPARAM1__UNUSED_1BIT_310,                         // 01-0310
   enPARAM1__UNUSED_1BIT_311,                         // 01-0311
   enPARAM1__UNUSED_1BIT_312,                         // 01-0312
   enPARAM1__UNUSED_1BIT_313,                         // 01-0313
   enPARAM1__UNUSED_1BIT_314,                         // 01-0314
   enPARAM1__UNUSED_1BIT_315,                         // 01-0315
   enPARAM1__UNUSED_1BIT_316,                         // 01-0316
   enPARAM1__UNUSED_1BIT_317,                         // 01-0317
   enPARAM1__UNUSED_1BIT_318,                         // 01-0318
   enPARAM1__UNUSED_1BIT_319,                         // 01-0319
   enPARAM1__UNUSED_1BIT_320,                         // 01-0320
   enPARAM1__UNUSED_1BIT_321,                         // 01-0321
   enPARAM1__UNUSED_1BIT_322,                         // 01-0322
   enPARAM1__UNUSED_1BIT_323,                         // 01-0323
   enPARAM1__UNUSED_1BIT_324,                         // 01-0324
   enPARAM1__UNUSED_1BIT_325,                         // 01-0325
   enPARAM1__UNUSED_1BIT_326,                         // 01-0326
   enPARAM1__UNUSED_1BIT_327,                         // 01-0327
   enPARAM1__UNUSED_1BIT_328,                         // 01-0328
   enPARAM1__UNUSED_1BIT_329,                         // 01-0329
   enPARAM1__UNUSED_1BIT_330,                         // 01-0330
   enPARAM1__UNUSED_1BIT_331,                         // 01-0331
   enPARAM1__UNUSED_1BIT_332,                         // 01-0332
   enPARAM1__UNUSED_1BIT_333,                         // 01-0333
   enPARAM1__UNUSED_1BIT_334,                         // 01-0334
   enPARAM1__UNUSED_1BIT_335,                         // 01-0335
   enPARAM1__UNUSED_1BIT_336,                         // 01-0336
   enPARAM1__UNUSED_1BIT_337,                         // 01-0337
   enPARAM1__UNUSED_1BIT_338,                         // 01-0338
   enPARAM1__UNUSED_1BIT_339,                         // 01-0339
   enPARAM1__UNUSED_1BIT_340,                         // 01-0340
   enPARAM1__UNUSED_1BIT_341,                         // 01-0341
   enPARAM1__UNUSED_1BIT_342,                         // 01-0342
   enPARAM1__UNUSED_1BIT_343,                         // 01-0343
   enPARAM1__UNUSED_1BIT_344,                         // 01-0344
   enPARAM1__UNUSED_1BIT_345,                         // 01-0345
   enPARAM1__UNUSED_1BIT_346,                         // 01-0346
   enPARAM1__UNUSED_1BIT_347,                         // 01-0347
   enPARAM1__UNUSED_1BIT_348,                         // 01-0348
   enPARAM1__UNUSED_1BIT_349,                         // 01-0349
   enPARAM1__UNUSED_1BIT_350,                         // 01-0350
   enPARAM1__UNUSED_1BIT_351,                         // 01-0351
   enPARAM1__UNUSED_1BIT_352,                         // 01-0352
   enPARAM1__UNUSED_1BIT_353,                         // 01-0353
   enPARAM1__UNUSED_1BIT_354,                         // 01-0354
   enPARAM1__UNUSED_1BIT_355,                         // 01-0355
   enPARAM1__UNUSED_1BIT_356,                         // 01-0356
   enPARAM1__UNUSED_1BIT_357,                         // 01-0357
   enPARAM1__UNUSED_1BIT_358,                         // 01-0358
   enPARAM1__UNUSED_1BIT_359,                         // 01-0359
   enPARAM1__UNUSED_1BIT_360,                         // 01-0360
   enPARAM1__UNUSED_1BIT_361,                         // 01-0361
   enPARAM1__UNUSED_1BIT_362,                         // 01-0362
   enPARAM1__UNUSED_1BIT_363,                         // 01-0363
   enPARAM1__UNUSED_1BIT_364,                         // 01-0364
   enPARAM1__UNUSED_1BIT_365,                         // 01-0365
   enPARAM1__UNUSED_1BIT_366,                         // 01-0366
   enPARAM1__UNUSED_1BIT_367,                         // 01-0367
   enPARAM1__UNUSED_1BIT_368,                         // 01-0368
   enPARAM1__UNUSED_1BIT_369,                         // 01-0369
   enPARAM1__UNUSED_1BIT_370,                         // 01-0370
   enPARAM1__UNUSED_1BIT_371,                         // 01-0371
   enPARAM1__UNUSED_1BIT_372,                         // 01-0372
   enPARAM1__UNUSED_1BIT_373,                         // 01-0373
   enPARAM1__UNUSED_1BIT_374,                         // 01-0374
   enPARAM1__UNUSED_1BIT_375,                         // 01-0375
   enPARAM1__UNUSED_1BIT_376,                         // 01-0376
   enPARAM1__UNUSED_1BIT_377,                         // 01-0377
   enPARAM1__UNUSED_1BIT_378,                         // 01-0378
   enPARAM1__UNUSED_1BIT_379,                         // 01-0379
   enPARAM1__UNUSED_1BIT_380,                         // 01-0380
   enPARAM1__UNUSED_1BIT_381,                         // 01-0381
   enPARAM1__UNUSED_1BIT_382,                         // 01-0382
   enPARAM1__UNUSED_1BIT_383,                         // 01-0383
   enPARAM1__UNUSED_1BIT_384,                         // 01-0384
   enPARAM1__UNUSED_1BIT_385,                         // 01-0385
   enPARAM1__UNUSED_1BIT_386,                         // 01-0386
   enPARAM1__UNUSED_1BIT_387,                         // 01-0387
   enPARAM1__UNUSED_1BIT_388,                         // 01-0388
   enPARAM1__UNUSED_1BIT_389,                         // 01-0389
   enPARAM1__UNUSED_1BIT_390,                         // 01-0390
   enPARAM1__UNUSED_1BIT_391,                         // 01-0391
   enPARAM1__UNUSED_1BIT_392,                         // 01-0392
   enPARAM1__UNUSED_1BIT_393,                         // 01-0393
   enPARAM1__UNUSED_1BIT_394,                         // 01-0394
   enPARAM1__UNUSED_1BIT_395,                         // 01-0395
   enPARAM1__UNUSED_1BIT_396,                         // 01-0396
   enPARAM1__UNUSED_1BIT_397,                         // 01-0397
   enPARAM1__UNUSED_1BIT_398,                         // 01-0398
   enPARAM1__UNUSED_1BIT_399,                         // 01-0399
   enPARAM1__UNUSED_1BIT_400,                         // 01-0400
   enPARAM1__UNUSED_1BIT_401,                         // 01-0401
   enPARAM1__UNUSED_1BIT_402,                         // 01-0402
   enPARAM1__UNUSED_1BIT_403,                         // 01-0403
   enPARAM1__UNUSED_1BIT_404,                         // 01-0404
   enPARAM1__UNUSED_1BIT_405,                         // 01-0405
   enPARAM1__UNUSED_1BIT_406,                         // 01-0406
   enPARAM1__UNUSED_1BIT_407,                         // 01-0407
   enPARAM1__UNUSED_1BIT_408,                         // 01-0408
   enPARAM1__UNUSED_1BIT_409,                         // 01-0409
   enPARAM1__UNUSED_1BIT_410,                         // 01-0410
   enPARAM1__UNUSED_1BIT_411,                         // 01-0411
   enPARAM1__UNUSED_1BIT_412,                         // 01-0412
   enPARAM1__UNUSED_1BIT_413,                         // 01-0413
   enPARAM1__UNUSED_1BIT_414,                         // 01-0414
   enPARAM1__UNUSED_1BIT_415,                         // 01-0415
   enPARAM1__UNUSED_1BIT_416,                         // 01-0416
   enPARAM1__UNUSED_1BIT_417,                         // 01-0417
   enPARAM1__UNUSED_1BIT_418,                         // 01-0418
   enPARAM1__UNUSED_1BIT_419,                         // 01-0419
   enPARAM1__UNUSED_1BIT_420,                         // 01-0420
   enPARAM1__UNUSED_1BIT_421,                         // 01-0421
   enPARAM1__UNUSED_1BIT_422,                         // 01-0422
   enPARAM1__UNUSED_1BIT_423,                         // 01-0423
   enPARAM1__UNUSED_1BIT_424,                         // 01-0424
   enPARAM1__UNUSED_1BIT_425,                         // 01-0425
   enPARAM1__UNUSED_1BIT_426,                         // 01-0426
   enPARAM1__UNUSED_1BIT_427,                         // 01-0427
   enPARAM1__UNUSED_1BIT_428,                         // 01-0428
   enPARAM1__UNUSED_1BIT_429,                         // 01-0429
   enPARAM1__UNUSED_1BIT_430,                         // 01-0430
   enPARAM1__UNUSED_1BIT_431,                         // 01-0431
   enPARAM1__UNUSED_1BIT_432,                         // 01-0432
   enPARAM1__UNUSED_1BIT_433,                         // 01-0433
   enPARAM1__UNUSED_1BIT_434,                         // 01-0434
   enPARAM1__UNUSED_1BIT_435,                         // 01-0435
   enPARAM1__UNUSED_1BIT_436,                         // 01-0436
   enPARAM1__UNUSED_1BIT_437,                         // 01-0437
   enPARAM1__UNUSED_1BIT_438,                         // 01-0438
   enPARAM1__UNUSED_1BIT_439,                         // 01-0439
   enPARAM1__UNUSED_1BIT_440,                         // 01-0440
   enPARAM1__UNUSED_1BIT_441,                         // 01-0441
   enPARAM1__UNUSED_1BIT_442,                         // 01-0442
   enPARAM1__UNUSED_1BIT_443,                         // 01-0443
   enPARAM1__UNUSED_1BIT_444,                         // 01-0444
   enPARAM1__UNUSED_1BIT_445,                         // 01-0445
   enPARAM1__UNUSED_1BIT_446,                         // 01-0446
   enPARAM1__UNUSED_1BIT_447,                         // 01-0447
   enPARAM1__UNUSED_1BIT_448,                         // 01-0448
   enPARAM1__UNUSED_1BIT_449,                         // 01-0449
   enPARAM1__UNUSED_1BIT_450,                         // 01-0450
   enPARAM1__UNUSED_1BIT_451,                         // 01-0451
   enPARAM1__UNUSED_1BIT_452,                         // 01-0452
   enPARAM1__UNUSED_1BIT_453,                         // 01-0453
   enPARAM1__UNUSED_1BIT_454,                         // 01-0454
   enPARAM1__UNUSED_1BIT_455,                         // 01-0455
   enPARAM1__UNUSED_1BIT_456,                         // 01-0456
   enPARAM1__UNUSED_1BIT_457,                         // 01-0457
   enPARAM1__UNUSED_1BIT_458,                         // 01-0458
   enPARAM1__UNUSED_1BIT_459,                         // 01-0459
   enPARAM1__UNUSED_1BIT_460,                         // 01-0460
   enPARAM1__UNUSED_1BIT_461,                         // 01-0461
   enPARAM1__UNUSED_1BIT_462,                         // 01-0462
   enPARAM1__UNUSED_1BIT_463,                         // 01-0463

   NUM_1BIT_PARAMS
};

#define PARAM_STRINGS_1BIT(PARAM_STR_1BIT)  \
   PARAM_STR_1BIT( FireRecallDoorM,                          "Fire Main Use Rear DR "                   ) \
   PARAM_STR_1BIT( FireRecallDoorA,                          "Fire Alt Use Rear DR "                    ) \
   PARAM_STR_1BIT( Fire__Smoke_Main_UseAltFloor,             "Fire Main Use Alt FLR "                   ) \
   PARAM_STR_1BIT( Fire__Smoke_Alt_UseAltFloor,              "Fire Alt Use Alt FLR "                    ) \
   PARAM_STR_1BIT( Fire__Smoke_MachineRoom_UseAltFloor,      "Fire MR Use Alt FLR "                     ) \
   PARAM_STR_1BIT( Fire__Smoke_Hoistway_UseAltFloor,         "Fire HW Use Alt FLR "                     ) \
   PARAM_STR_1BIT( Fire__Smoke_Main_FlashFireHat,            "Fire Main Flash Fire Hat"                 ) \
   PARAM_STR_1BIT( Fire__Smoke_Alt_FlashFireHat,             "Fire Alt Flash Fire Hat"                  ) \
   PARAM_STR_1BIT( Fire__Smoke_MachineRoom_FlashFireHat,     "Fire MR Flash Fire Hat"                   ) \
   PARAM_STR_1BIT( Fire__Smoke_Hoistway_FlashFireHat,        "Fire HW Flash Fire Hat"                   ) \
   PARAM_STR_1BIT( Fire__Smoke_Main_ShuntOnRecall,           "Fire Main Shunt On Recall"                ) \
   PARAM_STR_1BIT( Fire__Smoke_Alt_ShuntOnRecall,            "Fire Alt Shunt On Recall"                 ) \
   PARAM_STR_1BIT( Fire__Smoke_MachineRoom_ShuntOnRecall,    "Fire MR Shunt On Recall"                  ) \
   PARAM_STR_1BIT( Fire__Smoke_Hoistway_ShuntOnRecall,       "Fire HW Shunt On Recall"                  ) \
   PARAM_STR_1BIT( Fire__ResetToExitPhase1,                  "Fire Reset To Exit Phase1"                ) \
   PARAM_STR_1BIT( Fire__DisableDoorRestrictorOnPhase_2,     "Fire DISA DR Restrictor Phase2"           ) \
   PARAM_STR_1BIT( Fire__Phase2SwingReopenDisabled,          "Fire Phase2 Swing Reopen DISA "           ) \
   PARAM_STR_1BIT( Fire__Group3TypeHoldSwitch,               "Fire Group3 Hold Switch"                  ) \
   PARAM_STR_1BIT( Fire__IgnoreLocksJumpedOnPhase2,          "Fire Ignore Locks Jumped On Phase2"       ) \
   PARAM_STR_1BIT( Fire__FireStopSwitchKillsDoorOperator,    "Fire Stop Switch Kills DR Operator"       ) \
   PARAM_STR_1BIT( Fire__DOL_toExitPhase2,                   "Fire DOL To Exit Phase2"                  ) \
   PARAM_STR_1BIT( UNUSED_1BIT_021,                          "NA"                                       ) \
   PARAM_STR_1BIT( Fire__OkToStopOutsideDoorZone,            "Fire Ok To Stop Outside DZ"               ) \
   PARAM_STR_1BIT( Fire__AllowResetWithActiveSmoke,          "Fire Allow Reset With Active Smoke"       ) \
   PARAM_STR_1BIT( Fire__HatFlashIgnoreOrder,                "Fire Hat Flash Ignore Order"              ) \
   PARAM_STR_1BIT( Fire__MomentaryDoorCloseButton,           "Fire Momentary DCB"                       ) \
   PARAM_STR_1BIT( Fire__FlashLobbyFireLamp,                 "Fire Flash Lobby Lamp"                    ) \
   PARAM_STR_1BIT( Fire__RemoteAndMainToOverrideSmoke,       "Fire Remote And Main To Override Smoke"   ) \
   PARAM_STR_1BIT( Fire__EnablePHE_OnPhase2,                 "Fire ENA PHE On Phase2"                   ) \
   PARAM_STR_1BIT( Fire__DoorOpenOnHold,                     "Fire DR Open On Hold"                     ) \
   PARAM_STR_1BIT( VIP_Priority_Dispatching,                 "VIP Priority Dispatching"                 ) \
   PARAM_STR_1BIT( Fire__Smoke_Pit_FlashFireHat,             "Fire Pit Flash Fire Hat"                  ) \
   PARAM_STR_1BIT( Fire__Smoke_Pit_ShuntOnRecall,            "Fire Pit Shunt On Recall"                 ) \
   PARAM_STR_1BIT( NumDoors,                                 "ENA Rear Doors"                           ) \
   PARAM_STR_1BIT( BypassTermLimits,                         "BYP  Term Limits"                         ) \
   PARAM_STR_1BIT( EBrakeOnOverspeed,                        "EBrake On OVSP"                           ) \
   PARAM_STR_1BIT( Fire__Smoke_Pit_UseAltFloor,              "Fire Pit Use Alt FLR "                    ) \
   PARAM_STR_1BIT( Enable_Pit_Inspection,                    "ENA Pit Insp."                            ) \
   PARAM_STR_1BIT( Enable_Landing_Inspection,                "ENA Landing Insp."                        ) \
   PARAM_STR_1BIT( ImproveMaxSpeed,                          "Improved Max SPD"                         ) \
   PARAM_STR_1BIT( DisableBypassICStop,                      "DISA BYP IC Stop"                         ) \
   PARAM_STR_1BIT( RelevelEnabled,                           "ENA Releveling"                           ) \
   PARAM_STR_1BIT( EnableEarthQuake,                         "ENA EQ"                                   ) \
   PARAM_STR_1BIT( EnableMidFlightDestinationChange,         "ENA Midflight Destination Change"         ) \
   PARAM_STR_1BIT( DisableBrakeboard,                        "DISA Brake Faults"                        ) \
   PARAM_STR_1BIT( DzStuckHighTesting,                       "DZ Stuck High Test"                       ) \
   PARAM_STR_1BIT( LearnBrake,                               "Learn Brake"                              ) \
   PARAM_STR_1BIT( DEBUG_TransmitRunLog,                     "Transmit Run Log"                         ) \
   PARAM_STR_1BIT( Enable_Freight_Doors,                     "ENA Freight Doors"                        ) \
   PARAM_STR_1BIT( Enable_Freight_DCM,                       "ENA FDR DCM"                              ) \
   PARAM_STR_1BIT( Enable_Freight_AutoClose,                 "ENA FDR Auto Close"                       ) \
   PARAM_STR_1BIT( LearnBrake2,                              "Learn Brake 2"                            ) \
   PARAM_STR_1BIT( TestUnintendedMovement,                   "TestUnintendedMovement"                   ) \
   PARAM_STR_1BIT( EnableEmergencyDispatch,                  "ENA Emergency Dispatch"                   ) \
   PARAM_STR_1BIT( BPS_PRIM_NC,                              "Primary BPS NC"                           ) \
   PARAM_STR_1BIT( BPS_SEC_NC,                               "Secondary BPS NC"                         ) \
   PARAM_STR_1BIT( DEBUG_RunTerminalToTerminal_R,            "Auto Runs Terminal To Terminal R"         ) \
   PARAM_STR_1BIT( DisableCEDESOffline,                      "DISA CEDES Faults"                        ) \
   PARAM_STR_1BIT( DisableAutoDriveReset,                    "DISA Auto Drive Reset"                    ) \
   PARAM_STR_1BIT( EnableSecondaryBrake,                     "ENA Secondary Brake"                      ) \
   PARAM_STR_1BIT( EnableRiserAlarms,                        "ENA Riser Alarms"                         ) \
   PARAM_STR_1BIT( DEBUG_RunTerminalToTerminal,              "Auto Runs Terminal To Terminal"           ) \
   PARAM_STR_1BIT( DEBUG_RunFloorToFloor,                    "Auto Runs FLR To FLR "                    ) \
   PARAM_STR_1BIT( DEBUG_DisableUpdateNTS,                   "DISA NTS Update"                          ) \
   PARAM_STR_1BIT( DEBUG_DisablePreflight,                   "DISA Preflight"                           ) \
   PARAM_STR_1BIT( Ind_Srv_OverridesSecurity,                "Independent Srv. Byp. Security"           ) \
   PARAM_STR_1BIT( LWD_EnableWifi,                           "LWD ENA WiFi"                             ) \
   PARAM_STR_1BIT( Invert_NTS_Stop,                          "Invert NTS Stop"                          ) \
   PARAM_STR_1BIT( LWD_AutoRecalibrate,                      "LWD Auto Recalibrate"                     ) \
   PARAM_STR_1BIT( EnableSpeedDevControl,                    "ENA SPD Dev Control"                      ) \
   PARAM_STR_1BIT( LWD_TriggerRecalibrate,                   "LWD Trigger Recalibrate"                  ) \
   PARAM_STR_1BIT( LWD_TriggerLoadLearn,                     "LWD Trigger Load Learn"                   ) \
   PARAM_STR_1BIT( EnableConstructionRunBox,                 "ENA Construction Run Box"                 ) \
   PARAM_STR_1BIT( DisableConstructionOverspeed,             "DISA Construction OVSP"                   ) \
   PARAM_STR_1BIT( DEBUG_RunTerminalToTerminal_F,            "Auto Runs Terminal To Terminal F"         ) \
   PARAM_STR_1BIT( ICInspectionRequiredForCT,                "IC Insp.Req For CT"                       ) \
   PARAM_STR_1BIT( Door_DC_ON_RUN,                           "DR DC On Run"                             ) \
   PARAM_STR_1BIT( DEBUG_RunFloorToFloor_R,                  "Auto Runs FLR To FLR R"                   ) \
   PARAM_STR_1BIT( Debug_LWD,                                "Debug LWD"                                ) \
   PARAM_STR_1BIT( OOS_RearOpening,                          "OOS Rear Opening"                         ) \
   PARAM_STR_1BIT( OOS_Disable,                              "DISA OOS"                                 ) \
   PARAM_STR_1BIT( OOS_SetDoorOpen,                          "OOS SetDR Open"                           ) \
   PARAM_STR_1BIT( Swing_CallsEnable,                        "Swing Calls ENA "                         ) \
   PARAM_STR_1BIT( Swing_StayInGroup,                        "Swing Stay In Group"                      ) \
   PARAM_STR_1BIT( DoorLocksJumpedOnDOL,                     "Locks Jumped On DOL"                      ) \
   PARAM_STR_1BIT( NC_INPUT_CustomMode,                      "NC INPUT CustomMode"                      ) \
   PARAM_STR_1BIT( CustomMode_IgnoreCarCallSecurity,         "CustomMode IgnoreCarCallSecurity"         ) \
   PARAM_STR_1BIT( CustomMode_IgnoreHallCallSecurity,        "CustomMode IgnoreHallCallSecurity"        ) \
   PARAM_STR_1BIT( CustomMode_AllowedOutsideDoorZone,        "CustomMode AllowedOutsideDR Zone"         ) \
   PARAM_STR_1BIT( CustomMode_ParkingEnabled,                "CustomMode ParkingEnabled"                ) \
   PARAM_STR_1BIT( CustomMode_IgnoredCarCall_F,              "CustomMode IgnoredCarCall F"              ) \
   PARAM_STR_1BIT( CustomMode_IgnoredCarCall_R,              "CustomMode IgnoredCarCall R"              ) \
   PARAM_STR_1BIT( CustomMode_IgnoreHallCall,                "CustomMode IgnoreHallCall"                ) \
   PARAM_STR_1BIT( CustomMode_AutoDoorOpen,                  "CustomMode AutoDR Open"                   ) \
   PARAM_STR_1BIT( CustomMode_DoorHold,                      "CustomMode DR Hold"                       ) \
   PARAM_STR_1BIT( CustomMode_IgnoreDCB,                     "CustomMode IgnoreDCB"                     ) \
   PARAM_STR_1BIT( CustomMode_ForceDoorsOpenOrClosed,        "CustomMode ForceDoorsOpenOrClosed"        ) \
   PARAM_STR_1BIT( EMS_AllowPh2WithoutPh1,                   "EMS Allow Ph2 Without Ph1"                ) \
   PARAM_STR_1BIT( EMS_ExitPh2AtAnyFloor,                    "EMS Exit Ph2 At Any FLR "                 ) \
   PARAM_STR_1BIT( DEBUG_RunFloorToFloor_F,                  "Auto Runs FLR To FLR F"                   ) \
   PARAM_STR_1BIT( EMS_FireOverridesEMS1,                    "Fire Overrides EMS Ph1"                   ) \
   PARAM_STR_1BIT( BPSStuckOpenDropsEBrake,                  "BPSStuckOpenDropsEBrake"                  ) \
   PARAM_STR_1BIT( FLOOD_OverrideFire,                       "Flood Override Fire"                      ) \
   PARAM_STR_1BIT( FLOOD_OkayToRun,                          "Flood Okay To Run"                        ) \
   PARAM_STR_1BIT( ATTD_DirWithCCB,                          "Attendant Direction With CCB"             ) \
   PARAM_STR_1BIT( Rescue_RecTrvDir,                         "Rescue Rec Trv Dir"                       ) \
   PARAM_STR_1BIT( CC_Acknowledge,                           "CC Acknowledge"                           ) \
   PARAM_STR_1BIT( DEBUG_MonitorCarDirection,                "DEBUG MonitorCarDirection"                ) \
   PARAM_STR_1BIT( DC_On_DoorCloseState,                     "DR DC On Closed State"                    ) \
   PARAM_STR_1BIT( DO_On_DoorOpenState,                      "DR DO On Opened State"                    ) \
   PARAM_STR_1BIT( EnableRandomCarCallRuns_R,                "Run Random Runs R"                        ) \
   PARAM_STR_1BIT( DisableBPS_StopSeq,                       "DISA BPS StopSeq"                         ) \
   PARAM_STR_1BIT( DisableBPS_StuckHigh,                     "DISA BPS Stuck Active"                    ) \
   PARAM_STR_1BIT( DisableBPS_StuckLow,                      "DISA BPS Stuck Inactive"                  ) \
   PARAM_STR_1BIT( EnableRandomCarCallRuns,                  "Run Random Runs"                          ) \
   PARAM_STR_1BIT( CT_Stop_SW_Kills_Doors,                   "CT ST SW Kills Doors"                     ) \
   PARAM_STR_1BIT( DisableIdleTravelArrows,                  "DISA IdleTravelArrows"                    ) \
   PARAM_STR_1BIT( DisableBrakeOverheat,                     "DISA Brake Overheat"                      ) \
   PARAM_STR_1BIT( DisableDoorsOnHA,                         "DISA DoorsOnHA"                           ) \
   PARAM_STR_1BIT( EnableLoadLearn,                          "ENA LoadLearn"                            ) \
   PARAM_STR_1BIT( EnableIMotionDoors,                       "ENA IMotionDR "                           ) \
   PARAM_STR_1BIT( EnableDSDFullField,                       "ENA DSD Full Field"                       ) \
   PARAM_STR_1BIT( StopSeq_DisableRampZero,                  "StopSeq DISA RampZero"                    ) \
   PARAM_STR_1BIT( StopSeq_DisableHoldZero,                  "StopSeq DISA HoldZero"                    ) \
   PARAM_STR_1BIT( DEBUG_IncreaseMRBSendRate,                "IncreaseMRBSendRate"                      ) \
   PARAM_STR_1BIT( Debug_FastGroupResend,                    "Debug FastGroupResend"                    ) \
   PARAM_STR_1BIT( EnablePreflightTestDIP,                   "ENA PreflightTestDIP"                     ) \
   PARAM_STR_1BIT( DisableEPower,                            "DISA E-Power"                             ) \
   PARAM_STR_1BIT( EnableUIDriveEdit,                        "ENA UIDriveEdit"                          ) \
   PARAM_STR_1BIT( EnableOpModeAlarm,                        "ENA OpModeAlarm"                          ) \
   PARAM_STR_1BIT( EnableStopAtNextAlarm,                    "ENA StopAtNextAlarm"                      ) \
   PARAM_STR_1BIT( BypassFireSrv,                            "BYP FireSrv"                              ) \
   PARAM_STR_1BIT( ParkingWithDoorOpen,                      "ParkingWithDR Open"                       ) \
   PARAM_STR_1BIT( EnableLatchesCC,                          "ENA LatchesCC"                            ) \
   PARAM_STR_1BIT( NoDemandDoorsOpen,                        "NoDemandDoorsOpen"                        ) \
   PARAM_STR_1BIT( EnableCPLDOffline,                        "ENA CPLDOffline"                          ) \
   PARAM_STR_1BIT( DebounceLatchedFault,                     "DebounceLatchedFault"                     ) \
   PARAM_STR_1BIT( EnableOldFRAM,                            "ENA OldFRAM"                              ) \
   PARAM_STR_1BIT( EnableHallSecurity,                       "ENA HallSecurity"                         ) \
   PARAM_STR_1BIT( Sabbath_KeyEnable_Only,                   "Sabbath Key Only ENA "                    ) \
   PARAM_STR_1BIT( Sabbath_KeyOrTimer_Enable,                "Sabbath KeyOrTimer ENA "                  ) \
   PARAM_STR_1BIT( Sabbath_TimerEnable_Only,                 "Sabbath Timer Only ENA "                  ) \
   PARAM_STR_1BIT( Doors_NudgeBuzzerOnly,                    "Buzzer Only On Nudge"                     ) \
   PARAM_STR_1BIT( Doors_NudgeNoBuzzer,                      "Nudge No Buzzer"                          ) \
   PARAM_STR_1BIT( Enable3DigitPI,                           "3 Digit PI"                               ) \
   PARAM_STR_1BIT( DefaultFRAM,                              "DefaultFRAM"                              ) \
   PARAM_STR_1BIT( Enable_Dynamic_Parking,                   "ENA DynamicParking"                       ) \
   PARAM_STR_1BIT( Enable_CEDES2,                            "ENA CEDES2"                               ) \
   PARAM_STR_1BIT( Enable_ETSL,                              "ENA ETSL"                                 ) \
   PARAM_STR_1BIT( DisableCE_FloorPlus1,                     "DISA CE FlrPlus1"                         ) \
   PARAM_STR_1BIT( EnableEStopAlarms,                        "ENA EStopAlarms"                          ) \
   PARAM_STR_1BIT( EnableInspDoorOpenOutOfDZ,                "ENA Insp DO Out Of DZ"                    ) \
   PARAM_STR_1BIT( DSD_EarlyFieldEnable,                     "DSD Early Field ENA "                     ) \
   PARAM_STR_1BIT( DisableNonTerminalNTS,                    "DISA NonTerminalNTS"                      ) \
   PARAM_STR_1BIT( TestTrcLoss,                              "TestTrcLoss"                              ) \
   PARAM_STR_1BIT( DisableInvertKEBSpeed,                    "DISA InvertKEB SPD "                      ) \
   PARAM_STR_1BIT( DuparCOP_Enable,                          "ENA DuparCOP"                             ) \
   PARAM_STR_1BIT( EnableRegenOnEP,                          "ENA RegenOnEP"                            ) \
   PARAM_STR_1BIT( EBrakeOnETSandETSL,                       "EBrake on ETS/ETSL"                       ) \
   PARAM_STR_1BIT( EnableOpenDoorsAlarm,                     "ENA Open Doors Alarm"                     ) \
   PARAM_STR_1BIT( CarToLobbyExpress,                        "Car To Lobby Express"                     ) \
   PARAM_STR_1BIT( DoubleChimeOnDown,                        "Double Chime On Down"                     ) \
   PARAM_STR_1BIT( DisableBPS2_StuckHigh,                    "DISA BPS2 Stuck Active"                   ) \
   PARAM_STR_1BIT( DisableBPS2_StuckLow,                     "DISA BPS2 Stuck Inactive"                 ) \
   PARAM_STR_1BIT( Enable_Janus_RS_Fixture,                  "ENA Janus RS Fixture"                     ) \
   PARAM_STR_1BIT( LearnOpeningTime,                         "Learn Opening Time"                       ) \
   PARAM_STR_1BIT( EPowerPretransferStall,                   "EPWR Pretransfer Stall"                   ) \
   PARAM_STR_1BIT( XREG_EnableInMotionAssignment,            "XREG ENA In Motion Assignment"            ) \
   PARAM_STR_1BIT( XREG_PriorityFromArrivalDir,              "XREG Priority From Arrival Dir"           ) \
   PARAM_STR_1BIT( FRAM_EnableAlarms,                        "FRAM ENA Alarms"                          ) \
   PARAM_STR_1BIT( DisableLatchingBrakeFault,                "DISA Latching Brake Flt"                  ) \
   PARAM_STR_1BIT( DisablePIOOS,                             "DISA PI OOS"                              ) \
   PARAM_STR_1BIT( InMotionOpeningAlarm,                     "In Motion Opening Alarm"                  ) \
   PARAM_STR_1BIT( DisableDOBSecuredFloor,                   "DISA DOB Secured FLR "                    ) \
   PARAM_STR_1BIT( ReducedMaxSpeed,                          "Reduced Max SPD"                          ) \
   PARAM_STR_1BIT( ArvLanternDoor_1,                         "Arv Lantern DR 1"                         ) \
   PARAM_STR_1BIT( ArvLanternDoor_2,                         "Arv Lantern DR 2"                         ) \
   PARAM_STR_1BIT( ArvLanternDoor_3,                         "Arv Lantern DR 3"                         ) \
   PARAM_STR_1BIT( ArvLanternDoor_4,                         "Arv Lantern DR 4"                         ) \
   PARAM_STR_1BIT( ArvLanternDoor_5,                         "Arv Lantern DR 5"                         ) \
   PARAM_STR_1BIT( BContactsNC,                              "B Cont. NC"                               ) \
   PARAM_STR_1BIT( EnableAltMachineRoom,                     "Enable Alt MR"                            ) \
   PARAM_STR_1BIT( Fire__Smoke_MachineRoom2_FlashFireHat,    "Fire MR 2 Flash Fire Hat"                 ) \
   PARAM_STR_1BIT( Fire__Smoke_Hoistway2_FlashFireHat,       "Fire HW 2 Flash Fire Hat"                 ) \
   PARAM_STR_1BIT( Fire__Smoke_MachineRoom2_UseAltFloor,     "Fire MR 2 Use Alt FLR "                   ) \
   PARAM_STR_1BIT( Fire__Smoke_Hoistway2_UseAltFloor,        "Fire HW 2 Use Alt FLR "                   ) \
   PARAM_STR_1BIT( Fire__Smoke_MachineRoom2_ShuntOnRecall,   "Fire MR 2 Shunt On Recall"                ) \
   PARAM_STR_1BIT( Fire__Smoke_Hoistway2_ShuntOnRecall,      "Fire HW 2 Shunt On Recall"                ) \
   PARAM_STR_1BIT( EnableClearCC,                            "En. Clear Car Call"                       ) \
   PARAM_STR_1BIT( EnableDualPHE_Test,                       "ENA Dual PHE Test"                        ) \
   PARAM_STR_1BIT( EnablePretorqueTest,                      "EnablePretorqueTest"                      ) \
   PARAM_STR_1BIT( SuppressReopenOnGSW,                      "SuppressReopenOnGSW"                      ) \
   PARAM_STR_1BIT( EnableCheckInFloor,                       "ENA Check In Floor"                       ) \
   PARAM_STR_1BIT( EnablePassingLobbyDO,                     "ENA Passing Lobby DO"                     ) \
   PARAM_STR_1BIT( EnableNeverDropHallCalls,                 "ENA Never Drop Hall Calls"                ) \
   PARAM_STR_1BIT( EnableExtHallBoards,                      "ENA Ext. Hall Boards"                     ) \
   PARAM_STR_1BIT( DisableNoDestStop,                        "DISA No Dest Stop"                        ) \
   PARAM_STR_1BIT( DisableSabbathReleveling,                 "DISA Sabbath Releveling"                  ) \
   PARAM_STR_1BIT( CW_Derail_NO,                             "CW Derail NO"                             ) \
   PARAM_STR_1BIT( EnableBoardRTC,                           "ENA Board RTC"                            ) \
   PARAM_STR_1BIT( EnableCPLD_V2,                            "ENA CPLD V2"                              ) \
   PARAM_STR_1BIT( EnableCPLD_V3,                            "ENA CPLD V3"                              ) \
   PARAM_STR_1BIT( DisableDestLossStop,                      "DISA Dest Loss Stop"                      ) \
   PARAM_STR_1BIT( Fire__RecallToMainAfterFirePhase2,        "Fire Recall to Main After Phase 2"        ) \
   PARAM_STR_1BIT( EnableDL20_CT,                            "ENA DL20 CT"                              ) \
   PARAM_STR_1BIT( EnableDL20_COP,                           "ENA DL20 COP"                             ) \
   PARAM_STR_1BIT( DisableDL20_Buzzer,                       "DISA DL20 Buzzer"                         ) \
   PARAM_STR_1BIT( Door_RetiringCAM,                         "Door Retiring CAM"                        ) \
   PARAM_STR_1BIT( Door_FixedHallCAM,                        "Fixed Hall CAM"                           ) \
   PARAM_STR_1BIT( Door_HallClosedReqForCAM,                 "Hall Closed Req for CAM"                  ) \
   PARAM_STR_1BIT( EnableEX51_CT,                            "ENA EX51 CT"                              ) \
   PARAM_STR_1BIT( EnableEX51_COP,                           "ENA EX51 COP"                             ) \
   PARAM_STR_1BIT( EnableBrakeV2,                            "ENA Brake V2"                             ) \
   PARAM_STR_1BIT( DynamicParkingDO_1,                       "DynamicParkingDO_1"                       ) \
   PARAM_STR_1BIT( DynamicParkingDO_2,                       "DynamicParkingDO_2"                       ) \
   PARAM_STR_1BIT( DynamicParkingDO_3,                       "DynamicParkingDO_3"                       ) \
   PARAM_STR_1BIT( DynamicParkingDO_4,                       "DynamicParkingDO_4"                       ) \
   PARAM_STR_1BIT( DynamicParkingDO_5,                       "DynamicParkingDO_5"                       ) \
   PARAM_STR_1BIT( DynamicParkingDO_6,                       "DynamicParkingDO_6"                       ) \
   PARAM_STR_1BIT( DynamicParkingDO_7,                       "DynamicParkingDO_7"                       ) \
   PARAM_STR_1BIT( DynamicParkingDO_8,                       "DynamicParkingDO_8"                       ) \
   PARAM_STR_1BIT( Fire2SwingReopen,                         "Fire2 Swing Reopen"                       ) \
   PARAM_STR_1BIT( FreightTestPHE,                           "Freight Test PHE"                         ) \
   PARAM_STR_1BIT( SabbathDisableLWD,                        "Sabbath Disable LWD"                      ) \
   PARAM_STR_1BIT( EnableShieldAlarms,                       "ENA Shield Alarms"                        ) \
   PARAM_STR_1BIT( EnableExtFloorLimit,                      "ENA Ext Floor Limit"                      ) \
   PARAM_STR_1BIT( EnableCE_V2,                              "ENA CE V2"                                ) \
   PARAM_STR_1BIT( Fire__DisableLatchSmokes,                 "Fire DISA Latch Smokes"                   ) \
   PARAM_STR_1BIT( Fire__DisableLatchLobbyKey,               "Fire DISA Latch Lobby Key"                ) \
   PARAM_STR_1BIT( Fire__DisableLatchMainRecallFloor,        "Fire DISA Latch Main Recall"              ) \
   PARAM_STR_1BIT( DISA_CPLD_OVF_ALARM,                      "DISA_CPLD_OVF_ALARM"                      ) \
   PARAM_STR_1BIT( Fire__ResetOnTransitionFire1,             "Fire Reset On Transition"                 ) \
   PARAM_STR_1BIT( AN_ClrReverseDirCC,                       "AN ClrReverseDirCC"                       ) \
   PARAM_STR_1BIT( EnableVIPTimeoutAlarm,                    "ENA VIP T/O Alarm"                        ) \
   PARAM_STR_1BIT( Sabbath_EnableExtBuzzer,                  "Sabbath ENA Ext Buzzer"                   ) \
   PARAM_STR_1BIT( Disable_VirtualInputs,                    "Disable Virtual Input"                    ) \
   PARAM_STR_1BIT( Ind_Srv_IgnoreFrontCCB,                   "Independent Srv. Ignore Front CCB"        ) \
   PARAM_STR_1BIT( DISA_DoorJumperCheck,                     "DISA_DoorJumperCheck"                     ) \
   PARAM_STR_1BIT( NudgeWithoutOnwardDemand,                 "Nudge Without Onward Demand"              ) \
   PARAM_STR_1BIT( EQ_OldJobSupport,                         "EQ Old Job Support"                       ) \
   PARAM_STR_1BIT( DISA_CAM_ON_HA,                           "DISA CAM ON HA"                           ) \
   PARAM_STR_1BIT( Disable_Rear_DOB,                         "Disable Rear DOB"                         ) \
   PARAM_STR_1BIT( Sabbath_NudgeDoors,                       "Sabbath Nudge Doors"                      ) \
   PARAM_STR_1BIT( EnableC4SoftStarter,                      "ENA C4 Soft Starter"                      ) \
   PARAM_STR_1BIT( ENA_DAD_FAULT_RESEND,                     "ENA DAD Flt Resend"                       ) \
   PARAM_STR_1BIT( EnableRandomCarCallRuns_F,                "Run Random Runs F"                        ) \
   PARAM_STR_1BIT( EQ_Buzzer,                                "EQ Buzzer"                                ) \
   PARAM_STR_1BIT( MLT_Fire1_CloseDoors,                     "MLT_Fire1_DC"                             ) \
   PARAM_STR_1BIT( FlashFireHatOnLowOil,                     "FlashFireHatLowOil"                       ) \
   PARAM_STR_1BIT( JackResync_IgnoreCalls,                   "JackResync_IgnoreCalls"                   ) \
   PARAM_STR_1BIT( UNUSED_1BIT_250,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_251,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_252,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_253,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_254,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_255,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_256,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_257,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_258,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_259,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_260,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_261,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_262,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_263,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_264,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_265,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_266,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_267,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_268,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_269,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_270,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_271,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_272,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_273,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_274,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_275,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_276,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_277,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_278,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_279,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_280,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_281,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_282,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_283,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_284,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_285,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_286,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_287,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_288,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_289,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_290,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_291,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_292,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_293,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_294,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_295,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_296,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_297,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_298,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_299,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_300,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_301,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_302,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_303,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_304,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_305,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_306,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_307,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_308,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_309,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_310,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_311,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_312,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_313,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_314,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_315,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_316,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_317,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_318,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_319,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_320,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_321,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_322,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_323,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_324,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_325,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_326,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_327,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_328,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_329,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_330,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_331,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_332,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_333,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_334,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_335,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_336,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_337,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_338,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_339,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_340,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_341,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_342,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_343,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_344,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_345,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_346,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_347,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_348,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_349,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_350,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_351,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_352,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_353,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_354,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_355,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_356,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_357,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_358,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_359,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_360,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_361,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_362,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_363,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_364,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_365,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_366,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_367,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_368,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_369,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_370,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_371,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_372,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_373,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_374,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_375,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_376,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_377,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_378,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_379,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_380,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_381,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_382,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_383,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_384,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_385,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_386,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_387,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_388,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_389,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_390,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_391,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_392,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_393,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_394,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_395,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_396,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_397,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_398,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_399,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_400,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_401,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_402,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_403,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_404,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_405,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_406,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_407,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_408,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_409,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_410,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_411,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_412,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_413,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_414,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_415,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_416,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_417,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_418,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_419,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_420,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_421,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_422,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_423,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_424,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_425,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_426,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_427,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_428,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_429,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_430,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_431,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_432,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_433,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_434,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_435,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_436,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_437,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_438,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_439,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_440,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_441,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_442,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_443,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_444,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_445,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_446,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_447,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_448,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_449,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_450,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_451,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_452,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_453,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_454,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_455,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_456,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_457,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_458,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_459,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_460,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_461,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_462,                          "NA"                                       ) \
   PARAM_STR_1BIT( UNUSED_1BIT_463,                          "NA"                                       )

/* PARAM_DEFAULT_ENTRY_1BIT(A, B) FORMAT:
 *    A = Parameter Byte Map Index 
 *    B = (GRP1) Default All 8-Bit Map
 *    C = (GRP2) Default Learned Floors 8-Bit Map
 *    D = (GRP3) Default S-Curve 8-Bit Map
 *    E = (GRP4) Default Run Timers 8-Bit Map
 *    F = (GRP5) Default (UNUSED) 8-Bit Map
 *    G = (GRP6) Default (UNUSED) 8-Bit Map
 *    H = (GRP7) Default (UNUSED) 8-Bit Map
 *    I = (GRP8) Default (UNUSED) 8-Bit Map
 * */
/* Parameter Default Table Definition */
#define PARAM_DEFAULT_TABLE_1BIT(PARAM_DEFAULT_ENTRY_1BIT)  \
   PARAM_DEFAULT_ENTRY_1BIT( 0     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 1     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 2     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 3     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 4     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 5     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 6     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 7     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 8     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 9     , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 10    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 11    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 12    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 13    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 14    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 15    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 16    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 17    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 18    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 19    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 20    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 21    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 22    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 23    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 24    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 25    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 26    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 27    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 28    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 29    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 30    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 31    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 32    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 33    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 34    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 35    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 36    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 37    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 38    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 39    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 40    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 41    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 42    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 43    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 44    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 45    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 46    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 47    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 48    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 49    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 50    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 51    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 52    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 53    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 54    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 55    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 56    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    ) \
   PARAM_DEFAULT_ENTRY_1BIT( 57    , 255 , 0   , 0   , 0   , 0   , 0   , 0   , 0    )

/* Macros for accessing table elements */
#define EXPAND_PARAM_STRING_TABLE_1BIT_AS_ARRAY(A, B) B,

#define EXPAND_PARAM_DEFAULT_TABLE_1BIT_AS_DEFAULT_GRP_1(A, B, C, D, E, F, G, H, I) B,
#define EXPAND_PARAM_DEFAULT_TABLE_1BIT_AS_DEFAULT_GRP_2(A, B, C, D, E, F, G, H, I) C,
#define EXPAND_PARAM_DEFAULT_TABLE_1BIT_AS_DEFAULT_GRP_3(A, B, C, D, E, F, G, H, I) D,
#define EXPAND_PARAM_DEFAULT_TABLE_1BIT_AS_DEFAULT_GRP_4(A, B, C, D, E, F, G, H, I) E,
#define EXPAND_PARAM_DEFAULT_TABLE_1BIT_AS_DEFAULT_GRP_5(A, B, C, D, E, F, G, H, I) F,
#define EXPAND_PARAM_DEFAULT_TABLE_1BIT_AS_DEFAULT_GRP_6(A, B, C, D, E, F, G, H, I) G,
#define EXPAND_PARAM_DEFAULT_TABLE_1BIT_AS_DEFAULT_GRP_7(A, B, C, D, E, F, G, H, I) H,
#define EXPAND_PARAM_DEFAULT_TABLE_1BIT_AS_DEFAULT_GRP_8(A, B, C, D, E, F, G, H, I) I,

#endif