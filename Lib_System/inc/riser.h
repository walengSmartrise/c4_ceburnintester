/******************************************************************************
 *
 * @file     sru.h
 * @brief    Header file for MR SRU deployment
 * @version  V1.00
 * @date     28, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef RISER_H
#define RISER_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "subfaults.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define RISER_OFFLINE_COUNT_MS__FAULTED                  ( 30000 ) // Offline
#define RISER_OFFLINE_COUNT_MS__UNUSED                   ( 0xFFFF ) // Marks board as not used
// See enum en_group_net_nodes: (GROUP_NET__RIS_4-GROUP_NET__RIS_1+1)
#define MAX_NUM_RISER_BOARDS                             ( 4 ) // Max number of RIS supported

// Max number of master expansion boards supported by the system
#define NUM_EXP_MASTER_BOARDS                            ( 5 )

// Max number of boards supported per master expansion board (including the master)
#define NUM_EXP_BOARDS_PER_MASTER                        ( 8 )

// Number of characters each riser sends marking its software version
#define NUM_RIS_VERSION_CHARACTERS                       ( 4 )

// Riser 3 functions as a security riser, meaning any hall boards connected to it act as security inputs.
#define SECURITY_RISER_INDEX                             ( 2 )
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
  ERROR_RIS_EXP__UNKNOWN, // Board error state is unknown
  ERROR_RIS_EXP__NONE,
  ERROR_RIS_EXP__RESET_POR, // Board powering on after normal power cycle reset
  ERROR_RIS_EXP__RESET_WDT, // Board powering on after watch dog timer reset
  ERROR_RIS_EXP__RESET_BOD, // Board power on after brown out detection triggered reset
  ERROR_RIS_EXP__COM_GROUP,
  ERROR_RIS_EXP__COM_HALL, // Communication lost with hall network
  ERROR_RIS_EXP__COM_CAR, // Communication lost with car network
  ERROR_RIS_EXP__COM_MASTER, // Communication lost with master expansion board by slave expansion board
  ERROR_RIS_EXP__DRIVER_FLT, // Input driver circuit faulted
  ERROR_RIS_EXP__ADDRESS, // Two boards with same DIP address
  ERROR_RIS_EXP__BUS_RESET_CAN1, // CAN1 Bus has reset due to excessive line errors
  ERROR_RIS_EXP__BUS_RESET_CAN2, // CAN2 Bus has reset due to excessive line errors
  ERROR_RIS_EXP__COM_SLAVE, // Communication lost with slave expansion board by master expansion board
  ERROR_RIS_EXP__HALL_BOARD_OFFLINE, // Communication with a hall board previously on the network has been lost

  NUM_RIS_EXP_ERROR,
  MAX_RIS_EXP_ERROR = 16 // Limit to 16 due to transmit encoding
} en_ris_exp_error;

typedef struct
{
      uint8_t aucInputs[MAX_NUM_RISER_BOARDS];
      en_ris_exp_error aeError[MAX_NUM_RISER_BOARDS];

      uint8_t aucOutputs[MAX_NUM_RISER_BOARDS];
      uint16_t auwOfflineCounter_ms[MAX_NUM_RISER_BOARDS];
      char aaucVersion[MAX_NUM_RISER_BOARDS][NUM_RIS_VERSION_CHARACTERS];
} st_riser_board_data;

typedef struct
{
      uint8_t aucInputs[NUM_EXP_BOARDS_PER_MASTER];
      uint8_t aucOutputs[NUM_EXP_BOARDS_PER_MASTER];
      en_ris_exp_error aeError[NUM_EXP_BOARDS_PER_MASTER];
      uint16_t auwOfflineCounter_ms[NUM_EXP_BOARDS_PER_MASTER];

      uint8_t ucBF_OnlineFlags;
} st_exp_data;

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
