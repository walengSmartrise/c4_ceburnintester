/******************************************************************************
 *
 * @file     lifo.h
 * @brief    LIFO log header file
 * @version  V1.00
 * @date     12/19/16
 * @author   Keith Soneda
 *
 * @note    lifo.c
 *
 ******************************************************************************/

#ifndef _LIFO_H
#define _LIFO_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include "chip.h"
#include <stdint.h>
#include <string.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
uint8_t LIFO_Init(RINGBUFF_T *LIFO, void *buffer, int itemSize, int count);
uint8_t LIFO_GetElement(RINGBUFF_T *LIFO, void *data, uint8_t ucIndex );
uint8_t LIFO_Insert(RINGBUFF_T *LIFO, const void *data);

#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
