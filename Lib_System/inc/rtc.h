/******************************************************************************
 *
 * @file     sys_private.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     21, March 2016
 *
 * @note    For mod_rtc.c
 *
 ******************************************************************************/

#ifndef _RTC_H
#define _RTC_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   RTC_DOW__SUNDAY,
   RTC_DOW__MONDAY,
   RTC_DOW__TUESDAY,
   RTC_DOW__WEDNESDAY,
   RTC_DOW__THURSDAY,
   RTC_DOW__FRIDAY,
   RTC_DOW__SATURDAY,

   NUM_RTC_DOW
} st_rtc_day_of_week;
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/* Functions accessible only from MRB SRU */
void RTC_InitPeripheral( void );
void RTC_WritePeripheralTime_Unix( uint32_t ulUnixTime );
uint32_t RTC_ReadPeripheralTime_Unix( void );

/* Functions accessible from any main SRU processor (MR,CT,COP)*/
uint32_t RTC_GetSyncTime();
void RTC_SetSyncTime( uint32_t ulSyncTime_1s );
uint32_t RTC_GetLocalTime();
uint32_t RTC_GetTimeOfDay_TotalSeconds(void);
uint8_t RTC_GetTimeOfDay_Sec(void);
uint8_t RTC_GetTimeOfDay_Min(void);
uint8_t RTC_GetTimeOfDay_Hour(void);
st_rtc_day_of_week RTC_GetDayOfWeek(void);
uint8_t RTC_GetMonthOfTheYear(void);
uint8_t RTC_GetDayOfTheMonth(void);
uint16_t RTC_GetYear(void);

#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
