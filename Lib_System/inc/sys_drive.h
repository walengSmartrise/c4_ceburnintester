/******************************************************************************
 *
 * @file     sru.h
 * @brief    Header file for MR SRU deployment
 * @version  V1.00
 * @date     28, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef SYS_DRIVE_H
#define SYS_DRIVE_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "subfaults.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

// Drive
#define DRIVE_BUFFER_SIZE (40U)//Extended for KEB F5 Service 49 packets

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   DRIVE_CMD__NONE,
   DRIVE_CMD__READ,
   DRIVE_CMD__WRITE,
   NUM_DRIVE_CMD
}en_drive_commands;

enum en_drive_type
{
   enDriveType__HPV,
   enDriveType__KEB,
   enDriveType__DSD,
   NUM_DRIVE_TYPES,
//   enDriveType__DELTA,

};

enum en_drive_state
{
   enDriveState__OFFLINE,
   enDriveState__INITIALIZING,
   enDriveState__RUNNING,

   NUM_DRIVE_STATES
};
struct st_drive_interface
{
   void ( *pfnLoadTxBuffer )( void );
   void ( *pfnUnloadRxBuffer )( void );

   void ( *pfnTransmit)( void );
   void ( *pfnReceive )( void );
};

struct st_buffer
{
   uint32_t uiIndex;
   uint8_t aucData[ DRIVE_BUFFER_SIZE ];
   uint8_t ucBytesLeft;
   uint8_t ucState;
   uint8_t ucChecksum;
};

struct st_drive_param
{
   uint16_t * puwParamIDs;
   uint8_t ucIndex;
};
struct st_drive
{
   struct st_drive_interface stInterface;
   struct st_drive_param stParamWrite;
   struct st_drive_param stParamRead;
   struct st_buffer stRxBuffer;
   struct st_buffer stTxBuffer;
   uint32_t uiOfflineTime_5ms;
   int16_t wSpeedFeedback;
   enum en_drive_type eType;
   enum en_drive_state eState;
   enum en_drive_fault eFault;

   enum en_drive_error_dsd eFault_DSD;/* Stores reported faults */
   enum en_drive_error_hpv eFault_HPV;/* Stores reported faults */
   enum en_drive_error_keb eFault_KEB;/* Stores reported faults */
   enum en_drive_error_m1k eFault_M1K;/* Stores reported faults */
   uint8_t bSpeedRegRelease;
   uint8_t bTriggerDriveReset;
   uint8_t aucUnloadBuffer[DRIVE_BUFFER_SIZE];
   int16_t wPretorquePercentage; // Signed percentage of max pretorque [-100,100]

   uint8_t ucResetCounter;
   uint8_t bLearnMotor;

   uint8_t ucBadPacketCountdown;// TODO rm Test option. When nonzero, will send bad packets to the drive

};
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
