#ifndef _MOTION_H
#define _MOTION_H

#include "sys.h"

enum en_motion_start_sequence
{
   SEQUENCE_START__PREPARE_TO_RUN,
   SEQUENCE_START__DRIVE_HW_ENABLE,
   SEQUENCE_START__PICK_M,
   SEQUENCE_START__MOTOR_ENERGIZE,
   SEQUENCE_START__PICK_B2,
   SEQUENCE_START__LIFT_EBRAKE, // UNUSED
   SEQUENCE_START__LIFT_BRAKE, // Not part of start sequence
   SEQUENCE_START__ACCEL_DELAY,
   SEQUENCE_START__DSD_PRETORQUE,

   NUM_MOTION_START_SEQUENCE
};

enum en_motion_stop_sequence
{
   SEQUENCE_STOP__RAMP_TO_ZERO,
   SEQUENCE_STOP__HOLD_ZERO,
   SEQUENCE_STOP__DROP_BRAKE,
   SEQUENCE_STOP__CHECK_BPS,
   SEQUENCE_STOP__DELAY_DEENERGIZE,
   SEQUENCE_STOP__MOTOR_DEENERGIZE,// Unused
   SEQUENCE_STOP__DELAY_DROP_M,
   SEQUENCE_STOP__DROP_M,
   SEQUENCE_STOP__PREFLIGHT,
   SEQUENCE_STOP__END,

   NUM_MOTION_STOP_SEQUENCE,
};

enum en_motion_state
{
   MOTION__STOPPED,
   MOTION__MANUAL_RUN,
   MOTION__START_SEQUENCE,
   MOTION__ACCELERATING,
   MOTION__CRUISING,
   MOTION__DECELERATING,
   MOTION__STOP_SEQUENCE,
   MOTION__RSL_DECEL,
   MOTION__REC_TRV_DIR,

   NUM_MOTION_STATES
};
enum en_patterns
{
   PATTERN__NONE,
   PATTERN__VERY_SHORT,
   PATTERN__SHORT,
   PATTERN__FULL,

   NUM_PATTERNS
};
enum direction_enum
{
   DIR__NONE = 0,
   DIR__DN = -1,
   DIR__UP = 1,
};

typedef enum
{
   MOTION_PROFILE__1, // Normal Profile
   MOTION_PROFILE__2, // Inspection Profile
   MOTION_PROFILE__3, // E-Power Profile
   MOTION_PROFILE__4, // Very Short Profile

   NUM_MOTION_PROFILES
} Motion_Profile;

struct st_motion
{
   uint8_t bRunFlag;
   int8_t cDirection;
   int16_t wSpeedCMD;
   uint32_t ulActualDest;
   Motion_Profile eProfile;
};

void SetMotion_RunFlag( uint8_t bRunFlag );
void SetMotion_Direction( enum direction_enum eDirection );
void SetMotion_SpeedCommand( int16_t wSpeed_FPM );
void SetMotion_Destination( uint32_t ulDestination );
void SetMotion_Profile( Motion_Profile eProfile );

uint8_t GetMotion_RunFlag();
enum direction_enum GetMotion_Direction();
int16_t GetMotion_SpeedCommand();
uint32_t GetMotion_Destination();
Motion_Profile GetMotion_Profile();

#endif
