/******************************************************************************
 *
 * @file     network.h
 * @brief    Network header files
 * @version  V1.00
 * @date     15, Sept 2016
 *
 * @note   For network, nodes, and datagram definitions
 *
 ******************************************************************************/

#ifndef NETWORK2_H
#define NETWORK2_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "chip.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Shared Data -- MRA
 *----------------------------------------------------------------------------*/
typedef enum
{
   DG_MRA__Fault1,
   DG_MRA__Fault2,
   DG_MRA__Alarm1,
   DG_MRA__Alarm2,
   DG_MRA__DebugStates,
   DG_MRA__Debug,

   /*Operation*/
   DG_MRA__Operation1,
   DG_MRA__Operation2,
   DG_MRA__Operation3,
   DG_MRA__Operation4,

   /*Car Calls*/
   DG_MRA__CarCalls1,
   DG_MRA__CarCalls2,
   DG_MRA__CarCalls3,
   /*Input Mapping*/
   DG_MRA__MappedInputs1,
   DG_MRA__MappedInputs2,
   DG_MRA__MappedInputs3,
   DG_MRA__MappedInputs4,

   /*Output mapping*/
   DG_MRA__MappedOutputs1,
   DG_MRA__MappedOutputs2,
   DG_MRA__MappedOutputs3,
   DG_MRA__MappedOutputs4,
   DG_MRA__RiserBoardOutputs,
   DG_MRA__LocalOutputs1,
   DG_MRA__LocalOutputs2,
   DG_MRA__LocalOutputs3,
   DG_MRA__LocalOutputs4,

   DG_MRA__ParamMaster_ParamValues2,
   DG_MRA__ParamMaster_ParamValues3,
   DG_MRA__ParamMaster_ParamValues4,
   DG_MRA__ParamMaster_ParamValues5,
   DG_MRA__ParamMaster_ParamValues6,
   DG_MRA__Motion,

   DG_MRA__LoggedFaults1,
   DG_MRA__LoggedFaults2,
   DG_MRA__UNUSED34,
   DG_MRA__UNUSED35,
   DG_MRA__Group_Data,
   DG_MRA__Debug_PatternData,
   DG_MRA__Run_Log,
   DG_MRA__BrakeData,
   DG_MRA__EBrakeData,
   DG_MRA__CPLD_Fault1,
   DG_MRA__CPLD_Fault2,

   DG_MRA__ParamMaster_BlockCRCs1,
   DG_MRA__ParamMaster_BlockCRCs2,
   DG_MRA__ParamMaster_BlockCRCs3,
   DG_MRA__ParamMaster_BlockCRCs4,
   DG_MRA__ParamMaster_BlockCRCs5,
   DG_MRA__ParamMaster_BlockCRCs6,
   DG_MRA__ParamMaster_BlockCRCs7,
   DG_MRA__ParamMaster_BlockCRCs8,
   DG_MRA__ParamMaster_BlockCRCs9,
   DG_MRA__ParamMaster_BlockCRCs10,
   DG_MRA__ParamMaster_BlockCRCs11,
   DG_MRA__ParamMaster_BlockCRCs12,
   DG_MRA__ParamMaster_BlockCRCs13,
   DG_MRA__ParamMaster_BlockCRCs14,
   DG_MRA__ParamMaster_BlockCRCs15,
   DG_MRA__ParamMaster_BlockCRCs16,
   DG_MRA__ParamMaster_BlockCRCs17,
   DG_MRA__ParamMaster_BlockCRCs18,
   DG_MRA__ParamMaster_BlockCRCs19,
   DG_MRA__ParamMaster_BlockCRCs20,
   DG_MRA__ParamMaster_BlockCRCs21,
   DG_MRA__ParamMaster_BlockCRCs22,
   DG_MRA__ParamMaster_BlockCRCs23,
   DG_MRA__ParamMaster_BlockCRCs24,
   DG_MRA__ParamMaster_BlockCRCs25,
   DG_MRA__ParamMaster_BlockCRCs26,
   DG_MRA__ParamMaster_BlockCRCs27,
   DG_MRA__ParamMaster_BlockCRCs28,
   DG_MRA__ParamMaster_BlockCRCs29,
   DG_MRA__ParamMaster_BlockCRCs30,
   DG_MRA__ParamMaster_BlockCRCs31,
   DG_MRA__ParamMaster_BlockCRCs32,
   DG_MRA__ParamMaster_BlockCRCs33,
   DG_MRA__ParamMaster_BlockCRCs34,
   DG_MRA__ParamMaster_BlockCRCs35,
   DG_MRA__ParamMaster_BlockCRCs36,
   DG_MRA__ParamMaster_BlockCRCs37,
   DG_MRA__ParamMaster_BlockCRCs38,
   DG_MRA__ParamMaster_BlockCRCs39,
   DG_MRA__ParamMaster_BlockCRCs40,
   DG_MRA__ParamMaster_BlockCRCs41,
   DG_MRA__ParamMaster_BlockCRCs42,
   DG_MRA__ParamMaster_BlockCRCs43,
   DG_MRA__ParamMaster_BlockCRCs44,
   DG_MRA__ParamMaster_BlockCRCs45,
   DG_MRA__ParamMaster_BlockCRCs46,
   DG_MRA__ParamMaster_BlockCRCs47,
   DG_MRA__ParamMaster_BlockCRCs48,
   DG_MRA__ParamMaster_BlockCRCs49,
   DG_MRA__ParamMaster_BlockCRCs50,
   DG_MRA__ParamMaster_BlockCRCs51,
   DG_MRA__ParamMaster_BlockCRCs52,
   DG_MRA__ParamMaster_BlockCRCs53,
   DG_MRA__ParamMaster_BlockCRCs54,
   DG_MRA__ParamMaster_BlockCRCs55,
   DG_MRA__ParamMaster_BlockCRCs56,
   DG_MRA__ParamMaster_BlockCRCs57,
   DG_MRA__ParamMaster_BlockCRCs58,
   DG_MRA__ParamMaster_BlockCRCs59,
   DG_MRA__ParamMaster_BlockCRCs60,
   DG_MRA__LocalOutputs5,
   DG_MRA__LocalOutputs6,

   DG_MRA__DefaultAll,
   DG_MRA__DriveData,

   DG_MRA__ParamSync_GUI,

   DG_MRA__LoggedFaults3,
   DG_MRA__CPLD_Fault3,
   DG_MRA__Fault3,

   DG_MRA__CarCallSecurity_F0,
   DG_MRA__CarCallSecurity_F1,
   DG_MRA__CarCallSecurity_R0,
   DG_MRA__CarCallSecurity_R1,

   DG_MRA__CPLD_Data0,
   DG_MRA__CPLD_Data1,

   DG_MRA__LocalOutputs7,
   DG_MRA__LocalOutputs8,

   NUM_MRA_DATAGRAMS
} en_mra_datagrams;

typedef enum
{
   /* KEEP IN ORDER - START */
   DG_MRB__ParamSlaveRequest,
   /* KEEP IN ORDER - END */
   DG_MRB__Fault1,
   DG_MRB__Fault2,
   DG_MRB__Alarm1,
   DG_MRB__Alarm2,
   DG_MRB__UI_Req,
   /* Group dependent */
   DG_MRB__GroupInputs,
   DG_MRB__HallCalls,
   DG_MRB__DriveParameter,
   DG_MRB__SyncTime,

   /* Load Weigher */
   DG_MRB__LW,

   DG_MRB__RemoteCommands,
   DG_MRB__HallStatus,
   DG_MRB__Debug,
   DG_MRB__UI_Req_2,
   DG_MRB__LW_2,
   DG_MRB__Fault3,

   NUM_MRB_DATAGRAMS
} en_mrb_datagrams;

/*----------------------------------------------------------------------------
 * Shared Data -- CT
 *----------------------------------------------------------------------------*/
typedef enum
{
   /* KEEP IN ORDER - START */
   DG_CTA__ParamSlaveRequest,
   /* KEEP IN ORDER - END */
   DG_CTA__Fault1,
   DG_CTA__Fault2,
   DG_CTA__Alarm1,
   DG_CTA__Alarm2,
   DG_CTA__LocalInputs,
   DG_CTA__Debug,
   DG_CTA__SysPosition,
   DG_CTA__Fault3,
   DG_CTA__CPLD_Data0,
   DG_CTA__CPLD_Data1,

   NUM_CTA_DATAGRAMS
} en_cta_datagrams;

typedef enum
{
   /* KEEP IN ORDER - START */
   DG_CTB__ParamSlaveRequest,
   /* KEEP IN ORDER - END */
   DG_CTB__Fault1,
   DG_CTB__Fault2,
   DG_CTB__Alarm1,
   DG_CTB__Alarm2,
   DG_CTB__UI_Req,
   DG_CTB__DebugStates,
   /* ICEP */
   DG_CTB__UNUSED7,
   /* Load Weigher */
   DG_CTB__LW,
   DG_CTB__Debug,
   DG_CTB__UI_Req_2,
   DG_CTB__LW_2,
   DG_CTB__Fault3,
   NUM_CTB_DATAGRAMS
} en_ctb_datagrams;
/*----------------------------------------------------------------------------
 * Shared Data -- COP
 *----------------------------------------------------------------------------*/
typedef enum
{
   DG_COPA__ParamSlaveRequest,

   DG_COPA__Fault1,
   DG_COPA__Fault2,
   DG_COPA__Alarm1,
   DG_COPA__Alarm2,
   DG_COPA__LocalInputs,
   DG_COPA__Debug,
   DG_COPA__Fault3,
   DG_COPA__CPLD_Data0,
   DG_COPA__CPLD_Data1,

   NUM_COPA_DATAGRAMS
} en_copa_datagrams;

typedef enum
{
   DG_COPB__ParamSlaveRequest,

   DG_COPB__Fault1,
   DG_COPB__Fault2,
   DG_COPB__Alarm1,
   DG_COPB__Alarm2,
   DG_COPB__UI_Req,
   DG_COPB__Debug,
   DG_COPB__UNUSED7,
   DG_COPB__UI_Req_2,
   DG_COPB__Fault3,

   NUM_COPB_DATAGRAMS
} en_copb_datagrams;

/*----------------------------------------------------------------------------
 * Shared Data -- Expansion IO
 *----------------------------------------------------------------------------*/
typedef enum
{
   DG_EXP__LocalInputs1,
   DG_EXP__LocalInputs2,
   DG_EXP__LocalInputs3,
   DG_EXP__LocalInputs4,
   DG_EXP__LocalInputs5,
   DG_EXP__LocalInputs6,
   DG_EXP__LocalInputs7,
   DG_EXP__LocalInputs8,
   DG_EXP__LocalInputs9,
   DG_EXP__LocalInputs10,
   DG_EXP__LocalInputs11,
   DG_EXP__LocalInputs12,
   DG_EXP__LocalInputs13,
   DG_EXP__LocalInputs14,
   DG_EXP__LocalInputs15,
   DG_EXP__LocalInputs16,
   DG_EXP__DipComStatus1,
   DG_EXP__DipComStatus2,
   DG_EXP__DipComStatus3,
   DG_EXP__DipComStatus4,
   DG_EXP__DipComStatus5,
   DG_EXP__DipComStatus6,
   DG_EXP__DipComStatus7,
   DG_EXP__DipComStatus8,

   NUM_EXP_DATAGRAMS = 24,
} en_exp_datagrams;

/*----------------------------------------------------------------------------
 * Shared Data -- Netwotk IDs
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------

   SData Network Definitions

 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/


#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
1 *----------------------------------------------------------------------------*/


