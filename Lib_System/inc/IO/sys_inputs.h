/******************************************************************************
 *
 * @file     sys_inputs.h
 * @brief    input header files
 * @version  V1.00
 * @date     13, July 2017
 *
 * @note   For defining system inputs shared between boards
 *
 ******************************************************************************/

#ifndef _SYS_INPUTS_H
#define _SYS_INPUTS_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "chip.h"
#include "sru.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
   Inputs
 -----------------------------------------------------------------------------*/
enum en_input_functions
{
   enIN_ATU,
   enIN_ATD,
   enIN_ABU,
   enIN_ABD,

   enIN_LTF,
   enIN_LMF,
   enIN_LBF,
   enIN_LTR,
   enIN_LMR,
   enIN_LBR,

   enIN_ZMIN,
   enIN_ZHIN,

   enIN_BYPH,
   enIN_BYPC,

   enIN_MRTR,

   enIN_MM,
   enIN_GOV,

   enIN_SAFE_FUSE,
   enIN_EB_FUSE,

   enIN_MRUP,
   enIN_MRDN,

   enIN_CRGB,
   enIN_CRGP,
   enIN_CSFP,

   enIN_MRGM,
   enIN_MSFM,
   enIN_MRGP,
   enIN_MRGBP,
   enIN_MSFP,

   enIN_MRGBM,

   enIN_M120VAC,
   enIN_CPLD_AUX,

   enIN_MMC,
   enIN_CONST_EN,
   enIN_MBC2,

   enIN_CAPT,

   enIN_CW_DERAIL,

   enIN_BAT_STATUS,

   enIN_PIT,
   enIN_BUF,
   enIN_TFL,
   enIN_BFL,
   /* Reserve the first 41 entries for the machine room board inputs
    * Keep the same order as in en_ext_inputs from Lib_MRA sru_a.h */

   enIN_CTTR,
   enIN_ICTR,
   enIN_HATR,
   enIN_ILTR,
   enIN_IPTR,

   enIN_CTDN,
   enIN_CTUP,
   enIN_ICDN,
   enIN_ICUP,
   enIN_ILDN,//TODO move to unfixed
   enIN_ILUP,//TODO move to unfixed
   enIN_IPDN,//TODO move to unfixed
   enIN_IPUP,//TODO move to unfixed

   enIN_CTEN,

   enIN_FIRE_STOP_SW,
   enIN_ICST,
   enIN_GSWR,
   enIN_GSWF,
   enIN_DZ_F,
   enIN_DZ_R,
   enIN_CONST_DN,
   enIN_CONST_UP,
   enIN_CT_SW,
   enIN_ESC_HATCH,
   enIN_CAR_SAFE,
   enIN_MBC,
   enIN_Reserved = 68,
   /* Add inputs below this line */
   START_INPUTS_NONFIXED,
   enIN_UETS = START_INPUTS_NONFIXED,// TODO rm
   enIN_DETS,// TODO rm
   enIN_DOB_F,
   enIN_DCB_F,
   enIN_DOL_F,
   enIN_DCL_F,
   enIN_PHE_F,
   enIN_INDP,
   enIN_FIRE_II_ON,
   enIN_FIRE_II_OFF,
   enIN_FIRE_II_HOLD,
   enIN_FIRE_II_CNCL,

   enIN_FIRE_RECALL_OFF,
   enIN_FIRE_RECALL_RESET,
   enIN_SMOKE_HA,
   enIN_SMOKE_MR,
   enIN_SMOKE_MAIN,
   enIN_SMOKE_ALT,
   enIN_SEISMIC,

   enIN_FAN_AND_LIGHT,

   enIN_CAR_TO_LOBBY,

   enIN_DPM_F,

   enIN_DOB_R,
   enIN_DCB_R,
   enIN_DOL_R,
   enIN_DCL_R,
   enIN_PHE_R,
   enIN_DPM_R,

   enIN_REGEN_FLT,
   enIN_OVER_LOAD,
   enIN_FULL_LOAD,
   enIN_LIGHT_LOAD,
   enIN_SABBATH,
   enIN_SECURITY_ENABLE_ALL_CAR_CALLS,
   enIN_REMOTE_FIRE_KEY_SWITCH,
   enIN_OOS,
   enIN_FLOOD,
   enIN_SWING_ENABLE_KEY,
   enIN_CUSTOM,
   enIN_EMS2_ON,
   enIN_UNUSED_EMS2_OFF,
   enIN_ATTD_ON,
   enIN_ATTD_UP,
   enIN_ATTD_DN,
   enIN_ATTD_BYP,
   enIN_WANDER_GUARD,
   enIN_BATTERY_POWER,
   enIN_EP_ON,
   enIN_EP_UP_TO_SPEED,
   enIN_EP_AUTO_SELECT,
   enIN_EP_SELECT_1,
   enIN_EP_SELECT_2,
   enIN_EP_SELECT_3,
   enIN_EP_SELECT_4,
   enIN_EP_SELECT_5,
   enIN_EP_SELECT_6,
   enIN_EP_SELECT_7,
   enIN_EP_SELECT_8,
   enIN_AUTO_RESCUE,
   enIN_DoorHold_F,
   enIN_DoorHold_R,
   enIN_RecTrvOn,
   enIN_RecTrvDir,
   enIN_EnableAllHC,
   enIN_SMOKE_PIT,
   enIN_EP_PRETRANSFER,
   enIN_MANUAL_PICK,
   enIN_BATTERY_FLT,
   enIN_FAULT,
   enIN_PARKING_OFF,
   enIN_PhoneFailActive,
   enIN_PhoneFailReset,
   enIN_DSD_RunEngaged,
   enIN_SMOKE_MR_2,
   enIN_SMOKE_HA_2,
   enIN_TCL_F,
   enIN_MCL_F,
   enIN_BCL_F,
   enIN_TCL_R,
   enIN_MCL_R,
   enIN_BCL_R,
   enIN_SafetyEdge_F,
   enIN_SafetyEdge_R,
   enIN_TLOSS_RESET,
   enIN_PASSING_CHIME_DISABLE,
   enIN_PHE_F2,
   enIN_PHE_R2,
   enIN_DISABLE_ALL_HC,
   enIN_ENABLE_LOBBY_PEAK,
   enIN_ENABLE_UP_PEAK,
   enIN_ENABLE_DOWN_PEAK,
   enIN_VALVE_FLT,
   enIN_BRAKE1_BPS,
   enIN_BRAKE2_BPS,
   enIN_MARSHAL_MODE,
   enIN_ACTIVE_SHOOTER_MODE,
   enIN_DISTRESS_BUTTON,
   enIN_DISTRESS_ACKNOWLEDGE,
   enIN_EQ_HOISTWAY_SCAN,
   enIN_ENABLE_ALL_CC_FRONT,
   enIN_ENABLE_ALL_CC_REAR,
   enIN_MOTOR_OVERHEAT, //(Hydro Only) N/C input signals motor overheat.
   enIN_LOW_OIL, //(Hydro Only) N/C input signals low oil.
   enIN_LOW_PRESSURE, //(Hydro Only) N/C input signals low pressure.
   enIN_VISCOSITY, //(Hydro Only) N/C input signals cold oil.

   NUM_INPUT_FUNCTIONS,
   enIN_UNUSED,

   /* This limit is based on the 3 datagram addresses
    * allocated on the Car and Group net for mapped inputs */
   MAX_INPUT_FUNCTIONS = 192
};

/*-----------------------------------------------------------------------------
   Input Groups
      Inputs marked 'Dedicated' do not have an assigned terminal and will not be checked.
      Inputs marked 'Fixed' are checked in input mapping, but not changed.
      Inputs marked 'Unfixed' are checked in input mapping, and can be changed.
 -----------------------------------------------------------------------------*/
typedef enum
{
   INPUT_GROUP__NONE,
   INPUT_GROUP__AUTO_OP,
   INPUT_GROUP__DOORS_F,
   INPUT_GROUP__DOORS_R,
   INPUT_GROUP__FIRE,
   INPUT_GROUP__INSP,
   INPUT_GROUP__CTRL,
   INPUT_GROUP__SAFETY,
   INPUT_GROUP__CCB_F,
   INPUT_GROUP__CC_ENABLE_F,
   INPUT_GROUP__CCB_R,
   INPUT_GROUP__CC_ENABLE_R,
   INPUT_GROUP__EPWR,
   NUM_INPUT_GROUPS
}enum_input_groups;

typedef enum
{
   /* Fixed Inputs */
   /* Unfixed Inputs */
   CIG_AUTO_OPER__INDP,
   CIG_AUTO_OPER__CAR_TO_LOBBY,
   CIG_AUTO_OPER__LIGHT_LOAD,
   CIG_AUTO_OPER__SABBATH,
   CIG_AUTO_OPER__ENABLE_ALL_CC,
   CIG_AUTO_OPER__SWING,
   CIG_AUTO_OPER__CUSTOM,
   CIG_AUTO_OPER__EMS2_ON,
   CIG_AUTO_OPER__PARKING_OFF,
   CIG_AUTO_OPER__ATTD_ON,
   CIG_AUTO_OPER__ATTD_UP,
   CIG_AUTO_OPER__ATTD_DN,
   CIG_AUTO_OPER__ATTD_BYP,
   CIG_AUTO_OPER__WANDER_GUARD,
   CIG_AUTO_OPER__ENABLE_ALL_HC,
   CIG_AUTO_OPER__PASSING_CHIME_DISABLE,
   CIG_AUTO_OPER__DISABLE_ALL_HC,
   CIG_AUTO_OPER__ENABLE_LOBBY_PEAK,
   CIG_AUTO_OPER__ENABLE_UP_PEAK,
   CIG_AUTO_OPER__ENABLE_DOWN_PEAK,
   CIG_AUTO_OPER__MARSHAL_MODE,
   CIG_AUTO_OPER__ACTIVE_SHOOTER_MODE,
   CIG_AUTO_OPER__DISTRESS_BUTTON,
   CIG_AUTO_OPER__DISTRESS_ACKNOWLEDGE,
   CIG_AUTO_OPER__ENABLE_ALL_CC_FRONT,
   CIG_AUTO_OPER__ENABLE_ALL_CC_REAR,

   NUM_CIG_AUTO_OPER
} enum_cig_auto_oper;
typedef enum
{
   /* Fixed Inputs */
   CIG_DOORS__GSW,
   CIG_DOORS__DZ,
   /* Unfixed Inputs */
   CIG_DOORS__DPM,
   CIG_DOORS__DCL,
   CIG_DOORS__DOL,
   CIG_DOORS__PHE,
   CIG_DOORS__DCB,
   CIG_DOORS__DOB,
   CIG_DOORS__HOLD,
   CIG_DOORS__SE,
   CIG_DOORS__PHE_ALT,

   CIG_DOORS__TCL,
   CIG_DOORS__MCL,
   CIG_DOORS__BCL,

   NUM_CIG_DOORS
} enum_cig_doors;
typedef enum
{
   /* Fixed Inputs */
   /* Unfixed Inputs */
   CIG_FIRE_EQ__SMOKE_HA,
   CIG_FIRE_EQ__SMOKE_MR,
   CIG_FIRE_EQ__SMOKE_MAIN,
   CIG_FIRE_EQ__SMOKE_ALT,
   CIG_FIRE_EQ__SEISMIC,
   CIG_FIRE_EQ__FIRE_REMOTE_KEY_SW,
   CIG_FIRE_EQ__FIRE_RECALL_OFF,
   CIG_FIRE_EQ__FIRE_RECALL_RESET,
   CIG_FIRE_EQ__FIRE_II_ON,
   CIG_FIRE_EQ__FIRE_II_OFF,
   CIG_FIRE_EQ__FIRE_II_HOLD,
   CIG_FIRE_EQ__FIRE_II_CNCL,
   CIG_FIRE_EQ__SMOKE_PIT,
   CIG_FIRE_EQ__SMOKE_MR_2,
   CIG_FIRE_EQ__SMOKE_HA_2,
   CIG_FIRE_EQ__EQ_HOISTWAY_SCAN,

   NUM_CIG_FIRE_EQ
} enum_cig_fire_eq;

typedef enum
{
   /* Fixed Inputs */
   CIG_INSP__IP_TR,
   CIG_INSP__IL_TR,
   CIG_INSP__UNUSED2,
   CIG_INSP__UNUSED3,
   CIG_INSP__CT_UP,
   CIG_INSP__CT_DN,
   CIG_INSP__IC_UP,
   CIG_INSP__IC_DN,
   CIG_INSP__CT_EN,
   /* Unfixed Inputs */
   CIG_INSP__IP_UP,
   CIG_INSP__IP_DN,
   CIG_INSP__IL_UP,
   CIG_INSP__IL_DN,

   NUM_CIG_INSP
} enum_cig_insp;

typedef enum
{
   /* Fixed Inputs */
   CIG_CTRL__UNUSED0,
   /* Unfixed Inputs */
   CIG_CTRL__FAN_AND_LIGHT,
   CIG_CTRL__REGEN_FLT,
   CIG_CTRL__OOS,
   CIG_CTRL__BATTERY_POWER,
   CIG_CTRL__AUTO_RESCUE,
   CIG_CTRL__REC_TRV_ON,
   CIG_CTRL__REC_TRV_DIR,
   CIG_CTRL__MANUAL_PICK,
   CIG_CTRL__BATTERY_FLT,
   CIG_CTRL__FAULT,
   CIG_CTRL__DSD_RUN_ENGAGED,
   CIG_CTRL__VALVE_FLT,
   CIG_CTRL__BRAKE1_BPS,
   CIG_CTRL__BRAKE2_BPS,

   NUM_CIG_CTRL
} enum_cig_ctrl;

typedef enum
{
   /* Fixed Inputs */
   /* Unfixed Inputs */
   CIG_SAFETY__OVER_LOAD,
   CIG_SAFETY__FULL_LOAD,
   CIG_SAFETY__FLOOD,
   CIG_SAFETY__PHONE_FAIL_ACTIVE,
   CIG_SAFETY__PHONE_FAIL_RESET,
   CIG_SAFETY__TLOSS_RESET,
   CIG_SAFETY__MOTOR_OVERHEAT, //(Hydro Only) N/C input signals motor overheat.
   CIG_SAFETY__LOW_OIL, //(Hydro Only) N/C input signals low oil.
   CIG_SAFETY__LOW_PRESSURE, //(Hydro Only) N/C input signals low pressure.
   CIG_SAFETY__VISCOSITY, //(Hydro Only) N/C input signals cold oil.

   NUM_CIG_SAFETY
} enum_cig_safety;

typedef enum
{
   /* Fixed Inputs */
   /* Unfixed Inputs */
   CIG_EPWR__ON,
   CIG_EPWR__UP_TO_SPEED,
   CIG_EPWR__AUTO_SELECT,
   CIG_EPWR__SELECT_1,
   CIG_EPWR__SELECT_2,
   CIG_EPWR__SELECT_3,
   CIG_EPWR__SELECT_4,
   CIG_EPWR__SELECT_5,
   CIG_EPWR__SELECT_6,
   CIG_EPWR__SELECT_7,
   CIG_EPWR__SELECT_8,
   CIG_EPWR__PRETRANSFER,

   NUM_CIG_EPWR
} enum_cig_epwr;

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
enum en_input_functions SysIO_GetInputFunctionFromCIG( enum_input_groups eCIG, uint8_t ucIndex );
uint8_t SysIO_GetSizeOfCIG( enum_input_groups eCIG );
uint16_t SysIO_GetFixedTerminalFromCIG_Plus1( enum_input_groups eCIG, uint8_t ucIndex );
const char * SysIO_GetInputGroupString( enum_input_groups eCIG );
const char * SysIO_GetInputGroupString_ByFunctionIndex( enum_input_groups eCIG, uint8_t ucIndex );
#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/


