/******************************************************************************
 *
 * @file     sys_outputs.h
 * @brief    output header files
 * @version  V1.00
 * @date     13, July 2017
 *
 * @note   For defining system outputs shared between boards
 *
 ******************************************************************************/

#ifndef _SYS_OUTPUTS_H
#define _SYS_OUTPUTS_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "chip.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   I/O
 -----------------------------------------------------------------------------*/
enum en_output_functions
{
   UNUSED_OUTPUT,
   START_OUTPUTS_NONFIXED,
   enOUT_DO_F = START_OUTPUTS_NONFIXED,
   enOUT_DC_F,
   enOUT_NDG_F,
   enOUT_LIGHTFAN,
   enOUT_TRV_UP,
   enOUT_TRV_DN,
   enOUT_ARV_UP_F,
   enOUT_ARV_DN_F,
   enOUT_BUZZER,
   enOUT_CHIME,
   enOUT_LMP_INSP,
   enOUT_LMP_FIRE,
   enOUT_LMP_EQ,
   enOUT_UNUSED_BRAKE_PICK,

   enOUT_CAR_TO_LOBBY,
   enOUT_IN_SERVICE,
   enOUT_FIRE_LOBBY_LAMP,

   enOUT_GATE_RELEASE_F,
   enOUT_DCP_F,

   enOUT_DO_R,
   enOUT_DC_R,
   enOUT_NDG_R,
   enOUT_GATE_RELEASE_R,
   enOUT_DCP_R,

   enOUT_REGEN_ENABLE,
   enOUT_REGEN_RESET,
   enOUT_LMP_EPWR,
   enOUT_DRV_HW_ENABLE,
   enOUT_FIRE_I_ACTIVE,
   enOUT_FIRE_SHUNT,
   enOUT_LMP_EMS,
   enOUT_LMP_ATTD_ABOVE,
   enOUT_LMP_ATTD_BELOW,
   enOUT_LMP_FLOOD,
   enOUT_CAM_F,
   enOUT_CAM_R,
   enOUT_DOL_F,
   enOUT_DOL_R,
   enOUT_LMP_INDP_SRV,
   enOUT_ARV_UP_R,
   enOUT_ARV_DN_R,
   enOUT_DOOR_RESTRICTOR_F,
   enOUT_DOOR_RESTRICTOR_R,
   enOUT_IN_USE,
   enOUT_ACCELERATING,
   enOUT_DECELERATING,
   enOUT_DoorHold_F,
   enOUT_DoorHold_R,
   enOUT_LMP_Select1,
   enOUT_LMP_Select2,
   enOUT_LMP_Select3,
   enOUT_LMP_Select4,
   enOUT_LMP_Select5,
   enOUT_LMP_Select6,
   enOUT_LMP_Select7,
   enOUT_LMP_Select8,
   enOUT_RecTrvDir,
   enOUT_BatteryPwr,
   enOUT_Overload,
   enOUT_SAFETY_RESCUE,
   enOUT_LMP_SABBATH,
   enOUT_At_Recall,
   enOUT_LMP_Parking,
   enOUT_PhoneFailLamp,
   enOUT_PhoneFailBuzzer,
   enOUT_ARV_UP_1,
   enOUT_ARV_DN_1,
   enOUT_ARV_UP_2,
   enOUT_ARV_DN_2,
   enOUT_ARV_UP_3,
   enOUT_ARV_DN_3,
   enOUT_ARV_UP_4,
   enOUT_ARV_DN_4,
   enOUT_ARV_UP_5,
   enOUT_ARV_DN_5,

   enOUT_FreightDoor_Buzzer_F,
   enOUT_FreightDoor_Buzzer_R,
   enOUT_FreightDoor_DCM_F,
   enOUT_FreightDoor_DCM_R,
   enOUT_FreightDoor_Test_F,
   enOUT_FreightDoor_Test_R,

   enOUT_FIRE_II_ACTIVE,

   enOUT_AUTO_RESCUE,

   enOUT_MR_FAN,
   enOUT_START_MOTOR,
   enOUT_VALVE_INSP,
   enOUT_VALVE_HIGH_UP,
   enOUT_VALVE_HIGH_DOWN,
   enOUT_VALVE_MED_UP,
   enOUT_VALVE_MED_DOWN,
   enOUT_VALVE_LOW_UP,
   enOUT_VALVE_LOW_DOWN,
   enOUT_VALVE_LEVEL_UP,
   enOUT_VALVE_LEVEL_DOWN,
   enOUT_BRAKE1_PICK,
   enOUT_BRAKE2_PICK,

   enOUT_FIRE_II_HOLD,
   enOUT_DISTRESS_LAMP,
   enOUT_DISTRESS_BUZZER,

   enOUT_CC_ACKNOWLEDGE,

   enOUT_EQ_SLOW_LAMP,
   enOUT_LMP_SEISMIC_STATUS,

   NUM_OUTPUT_FUNCTIONS,

   /* This limit is based on the 2 datagram addresses
    * allocated on the Car and Group net for mapped outputs */
   MAX_OUTPUT_FUNCTIONS = 128
};
/*-----------------------------------------------------------------------------
   Output Groups
 -----------------------------------------------------------------------------*/
typedef enum
{
   OUTPUT_GROUP__NONE,
   OUTPUT_GROUP__AUTO_OPER,
   OUTPUT_GROUP__DOORS_F,
   OUTPUT_GROUP__DOORS_R,
   OUTPUT_GROUP__FIRE_EQ,
   OUTPUT_GROUP__INSP,
   OUTPUT_GROUP__CTRL,
   OUTPUT_GROUP__SAFETY,
   OUTPUT_GROUP__CCL_F,
   OUTPUT_GROUP__CCL_R,
   OUTPUT_GROUP__EPWR,

   NUM_OUTPUT_GROUPS
}  enum_output_groups;


typedef enum
{
   COG_AUTO_OPER__TRV_UP,
   COG_AUTO_OPER__TRV_DN,
   COG_AUTO_OPER__UNUSED2,
   COG_AUTO_OPER__LMP_PARKING,
   COG_AUTO_OPER__BUZZER,
   COG_AUTO_OPER__CHIME,
   COG_AUTO_OPER__CAR_TO_LOBBY,
   COG_AUTO_OPER__IN_SERV,
   COG_AUTO_OPER__OVERLOAD,
   COG_AUTO_OPER__LMP_EMS,
   COG_AUTO_OPER__LMP_ATTD_ABOVE,
   COG_AUTO_OPER__LMP_ATTD_BELOW,
   COG_AUTO_OPER__LMP_INDP_SRV,
   COG_AUTO_OPER__IN_USE,
   COG_AUTO_OPER__ACCELERATING,
   COG_AUTO_OPER__DECELERATING,
   COG_AUTO_OPER__LMP_SABBATH,
   COG_AUTO_OPER__AT_RECALL,
   COG_AUTO_OPER__ARV_UP_1,
   COG_AUTO_OPER__ARV_DN_1,
   COG_AUTO_OPER__ARV_UP_2,
   COG_AUTO_OPER__ARV_DN_2,
   COG_AUTO_OPER__ARV_UP_3,
   COG_AUTO_OPER__ARV_DN_3,
   COG_AUTO_OPER__ARV_UP_4,
   COG_AUTO_OPER__ARV_DN_4,
   COG_AUTO_OPER__ARV_UP_5,
   COG_AUTO_OPER__ARV_DN_5,
   COG_AUTO_OPER__DISTRESS_LAMP,
   COG_AUTO_OPER__DISTRESS_BUZZER,
   COG_AUTO_OPER__CC_ACKNOWLEDGE,

   NUM_COG_AUTO_OPER
} enum_cog_auto_oper;
typedef enum
{
   COG_DOORS__DO,
   COG_DOORS__DC,
   COG_DOORS__NDG,
   COG_DOORS__GATE_RELEASE,
   COG_DOORS__DCP,
   COG_DOORS__CAM,
   COG_DOORS__DOL,
   COG_DOORS__RESTRICTOR,
   COG_DOORS__ARV_UP,
   COG_DOORS__ARV_DN,
   COG_DOORS__HOLD,

   COG_DOORS__BUZZER,
   COG_DOORS__DCM,
   COG_DOORS__TEST,

   NUM_COG_DOORS
} enum_cog_doors;
typedef enum
{
   COG_FIRE_EQ__LMP_FIRE,
   COG_FIRE_EQ__LMP_EQ,
   COG_FIRE_EQ__LMP_FIRE_LOBBY,
   COG_FIRE_EQ__FIRE_I_ACTIVE,
   COG_FIRE_EQ__FIRE_SHUNT,
   COG_FIRE_EQ__FIRE_II_ACTIVE,
   COG_FIRE_EQ__FIRE_II_HOLD,
   COG_FIRE_EQ__EQ_SLOW_LAMP,
   COG_FIRE_EQ__LMP_SEISMIC_STATUS,

   NUM_COG_FIRE_EQ
} enum_cog_fire_eq;
typedef enum
{
   COG_INSP__LMP_INSP,

   NUM_COG_INSP
} enum_cog_insp;
typedef enum
{
   COG_CTRL__LIGHT_FAN,
   COG_CTRL__REGEN_EN,
   COG_CTRL__REGEN_RST,
   COG_CTRL__DRV_HW_ENABLE,
   COG_CTRL__REC_TRV_ENABLE,
   COG_CTRL__BATTERY_PWR,
   COG_CTRL__SAFETY_RESCUE,
   COG_CTRL__AUTO_RESCUE,
   COG_CTRL__MR_FAN,
   COG_CTRL__START_MOTOR,
   COG_CTRL__VALVE_INSP,
   COG_CTRL__VALVE_HIGH_UP,
   COG_CTRL__VALVE_HIGH_DOWN,
   COG_CTRL__VALVE_MID_UP,
   COG_CTRL__VALVE_MID_DOWN,
   COG_CTRL__VALVE_LOW_UP,
   COG_CTRL__VALVE_LOW_DOWN,
   COG_CTRL__VALVE_LEVEL_UP,
   COG_CTRL__VALVE_LEVEL_DOWN,
   COG_CTRL__BRAKE1_PICK,
   COG_CTRL__BRAKE2_PICK,

   NUM_COG_CTRL
} enum_cog_ctrl;
typedef enum
{
   COG_SAFETY__LMP_FLOOD,
   COG_SAFETY__PHONE_FAIL_LAMP,
   COG_SAFETY__PHONE_FAIL_BUZZER,

   NUM_COG_SAFETY
} enum_cog_safety;

typedef enum
{
   COG_EPWR__LMP_EPWR,
   COG_EPWR__LMP_SELECT1,
   COG_EPWR__LMP_SELECT2,
   COG_EPWR__LMP_SELECT3,
   COG_EPWR__LMP_SELECT4,
   COG_EPWR__LMP_SELECT5,
   COG_EPWR__LMP_SELECT6,
   COG_EPWR__LMP_SELECT7,
   COG_EPWR__LMP_SELECT8,

   NUM_COG_EPWR
} enum_cog_epwr;
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
enum en_output_functions SysIO_GetOutputFunctionFromCOG( enum_output_groups eCOG, uint8_t ucIndex );
uint8_t SysIO_GetSizeOfCOG( enum_output_groups eCOG );
const char * SysIO_GetOutputGroupString( enum_output_groups eCOG );
const char * SysIO_GetOutputGroupString_ByFunctionIndex( enum_output_groups eCOG, uint8_t ucIndex );
#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
