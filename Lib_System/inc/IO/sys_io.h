/******************************************************************************
 *
 * @file     sys_io.h
 * @brief    input/output header files
 * @version  V1.00
 * @date     15, Sept 2016
 *
 * @note   For defining system inputs/outputs shared between boards
 *
 ******************************************************************************/

#ifndef _SYS_IO_H
#define _SYS_IO_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "chip.h"
#include "sys_inputs.h"
#include "sys_outputs.h"
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   I/O Boards
 -----------------------------------------------------------------------------*/
enum en_io_boards
{
   IO_BOARDS__UNUSED,
   IO_BOARDS__MRA,
   IO_BOARDS__CTA,
   IO_BOARDS__COP,
   IO_BOARDS__RISER1,
   IO_BOARDS__RISER2,
   IO_BOARDS__RISER3,
   IO_BOARDS__RISER4,
   IO_BOARDS__EXP1,
   IO_BOARDS__EXP2,
   IO_BOARDS__EXP3,
   IO_BOARDS__EXP4,
   IO_BOARDS__EXP5,
   IO_BOARDS__EXP6,
   IO_BOARDS__EXP7,
   IO_BOARDS__EXP8,

   IO_BOARDS__EXP9,
   IO_BOARDS__EXP10,
   IO_BOARDS__EXP11,
   IO_BOARDS__EXP12,
   IO_BOARDS__EXP13,
   IO_BOARDS__EXP14,
   IO_BOARDS__EXP15,
   IO_BOARDS__EXP16,

   IO_BOARDS__EXP17,
   IO_BOARDS__EXP18,
   IO_BOARDS__EXP19,
   IO_BOARDS__EXP20,
   IO_BOARDS__EXP21,
   IO_BOARDS__EXP22,
   IO_BOARDS__EXP23,
   IO_BOARDS__EXP24,

   IO_BOARDS__EXP25,
   IO_BOARDS__EXP26,
   IO_BOARDS__EXP27,
   IO_BOARDS__EXP28,
   IO_BOARDS__EXP29,
   IO_BOARDS__EXP30,
   IO_BOARDS__EXP31,
   IO_BOARDS__EXP32,

   IO_BOARDS__EXP33,
   IO_BOARDS__EXP34,
   IO_BOARDS__EXP35,
   IO_BOARDS__EXP36,
   IO_BOARDS__EXP37,
   IO_BOARDS__EXP38,
   IO_BOARDS__EXP39,
   IO_BOARDS__EXP40,

   MAX__IO_BOARDS,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define NUM_IO_BOARDS_PER_MASTER             ( 8 )
#define MAX_NUM_RIS_BOARDS                   ( IO_BOARDS__RISER4 - IO_BOARDS__RISER1 + 1 )
#define MAX_NUM_EXP_BOARDS                   ( IO_BOARDS__EXP40 - IO_BOARDS__EXP1 + 1 )

#define MIN_TERMINALS_PER_IO_BOARD           ( 8 )
#define TERMINAL_INDEX_START__MRA            ( 0 )
#define TERMINAL_INDEX_START__CTA            ( TERMINAL_INDEX_START__MRA +   MIN_TERMINALS_PER_IO_BOARD )
#define TERMINAL_INDEX_START__COP            ( TERMINAL_INDEX_START__CTA + 2*MIN_TERMINALS_PER_IO_BOARD )
#define TERMINAL_INDEX_START__RIS            ( TERMINAL_INDEX_START__COP + 2*MIN_TERMINALS_PER_IO_BOARD )
#define TERMINAL_INDEX_START__EXP            ( TERMINAL_INDEX_START__RIS + MAX_NUM_RIS_BOARDS*MIN_TERMINALS_PER_IO_BOARD )
#define TERMINAL_INDEX_END__EXP              ( TERMINAL_INDEX_START__EXP + MAX_NUM_EXP_BOARDS*MIN_TERMINALS_PER_IO_BOARD - 1 )
#define MAX_NUM_IO_TERMINALS                 ( TERMINAL_INDEX_END__EXP + 1 )

#define TERMINAL_INDEX_START__EXP1           ( TERMINAL_INDEX_START__RIS + MAX_NUM_RIS_BOARDS*MIN_TERMINALS_PER_IO_BOARD )
#define TERMINAL_INDEX_START__EXP2           ( TERMINAL_INDEX_START__EXP1 + NUM_IO_BOARDS_PER_MASTER*MIN_TERMINALS_PER_IO_BOARD )
#define TERMINAL_INDEX_START__EXP3           ( TERMINAL_INDEX_START__EXP2 + NUM_IO_BOARDS_PER_MASTER*MIN_TERMINALS_PER_IO_BOARD )
#define TERMINAL_INDEX_START__EXP4           ( TERMINAL_INDEX_START__EXP3 + NUM_IO_BOARDS_PER_MASTER*MIN_TERMINALS_PER_IO_BOARD )
#define TERMINAL_INDEX_START__EXP5           ( TERMINAL_INDEX_START__EXP4 + NUM_IO_BOARDS_PER_MASTER*MIN_TERMINALS_PER_IO_BOARD )

#define MASK_INVERT_FIXED_TERMINAL_PLUS1     (0x8000)
#define MASK_IO_PARAM_TERMINAL_INVERSION        (0x0080)

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/


#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
1 *----------------------------------------------------------------------------*/
