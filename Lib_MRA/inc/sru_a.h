/******************************************************************************
 *
 * @file     sru_a.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef SRU_A_H
#define SRU_A_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "board.h"
#include "sru.h"
#include <stdint.h>
#include "shared_data.h"
#include "fpga_api.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
enum en_sru_id_pins
{
   enSRU_ID_PIN1,
   enSRU_ID_PIN2,
   enSRU_ID_PIN3,
   enSRU_ID_PIN4,

   SRU_NUM_ID_PINS
};
typedef enum
{
   enSRU_Input_501,
   enSRU_Input_502,
   enSRU_Input_503,
   enSRU_Input_504,
   enSRU_Input_505,
   enSRU_Input_506,
   enSRU_Input_507,
   enSRU_Input_508,

   SRU_NUM_INPUTS
} en_sru_inputs;

typedef enum
{
   eEXT_Input_ATU,
   eEXT_Input_ATD,
   eEXT_Input_ABU,
   eEXT_Input_ABD,

   eEXT_Input_LTF,
   eEXT_Input_LMF,
   eEXT_Input_LBF,
   eEXT_Input_LTR,
   eEXT_Input_LMR,
   eEXT_Input_LBR,

   eEXT_Input_ZMIN,
   eEXT_Input_ZHIN,

   eEXT_Input_BYPH,
   eEXT_Input_BYPC,
   eEXT_Input_MRTR,

   eEXT_Input_MM,
   eEXT_Input_GOV,

   eEXT_Input_SAFE_FUSE,
   eEXT_Input_EB_FUSE,

   eEXT_Input_MRUP,
   eEXT_Input_MRDN,

   eEXT_Input_CRGB,
   eEXT_Input_CRGP,
   eEXT_Input_CSFP,

   eEXT_Input_MRGM,
   eEXT_Input_MSFM,
   eEXT_Input_MRGP,
   eEXT_Input_MRGBP,
   eEXT_Input_MSFP,

   eEXT_Input_MRGBM,

   eEXT_Input_M120VAC,
   eEXT_Input_CPLD_AUX,

   eEXT_Input_MMC,
   eEXT_Input_MDC,
   eEXT_Input_MBC2,

   eEXT_Input_CAPT,

   eEXT_Input_CW_DERAIL,

   eEXT_Input_BAT_STATUS,

   eEXT_Input_PIT,
   eEXT_Input_BUF,
   eEXT_Input_TFL,
   eEXT_Input_BFL,
   eEXT_Input_MBC,
   eEXT_Input_CUP,
   eEXT_Input_CDN,

   EXT_NUM_INPUTS
} en_ext_inputs;

typedef enum
{
   ePREFLIGHT_INPUT_1,
   ePREFLIGHT_INPUT_2,

   NUM_PREFLIGHT_INPUTS,
} en_preflight_input;

typedef enum
{
   eRESET_BUTTON_CW,
   eRESET_BUTTON_TRC_LOSS,
   eRESET_BUTTON_E_BRAKE,

   NUM_RESET_BUTTONS
} en_reset_button;


typedef enum
{
   eExt_Output_CRGM,
   eExt_Output_CSFM,
   eExt_Output_MCUA_X,
   eExt_Output_CRGBM,
   EXT_NUM_OUTPUTS
} en_ext_outputs;

typedef enum
{
#if !ENABLE_FRAM_SSP_MODIFICATION
   eFRAM_Output_SSEL,
#endif
   eFRAM_Output_nWP,

   NUM_FRAM_OUTPUTS
} en_fram_outputs;

typedef enum
{
   enSRU_LED_Heartbeat,
   enSRU_LED_Fault,
   enSRU_LED_Alarm,

   enSRU_LED_Capture,
   enSRU_LED_SafetyZoneC,

   SRU_NUM_LEDS
} en_sru_leds;

enum en_sru_dip_switches
{
   enSRU_DIP_A1,
   enSRU_DIP_A2,
   enSRU_DIP_A3,
   enSRU_DIP_A4,
   enSRU_DIP_A5,
   enSRU_DIP_A6,
   enSRU_DIP_A7,
   enSRU_DIP_A8,

   enSRU_DIP_B1,
   enSRU_DIP_B2,
   enSRU_DIP_B3,
   enSRU_DIP_B4,
   enSRU_DIP_B5,
   enSRU_DIP_B6,
   enSRU_DIP_B7,
   enSRU_DIP_B8,
   SRU_NUM_DIP_SWITCHES
};

enum en_sru_dip_banks
{
   enSRU_DIP_BANK_A,

   enSRU_DIP_BANK_B,

   enSRU_NUM_DIP_BANKS,
};



/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

void MCU_A_Init( void );

uint8_t SRU_Read_MCU_ID();

uint8_t SRU_Read_WatchDogEnable();

uint8_t SRU_Read_BoardTypeIndicator();

uint8_t SRU_Read_Input( en_sru_inputs enSRU_Input );

uint8_t SRU_Read_ExtInput( en_ext_inputs enEXT_Input );
uint8_t SRU_Read_RawExtInput( en_ext_inputs enEXT_Input );

void SRU_Write_ExtOutput( en_ext_outputs enSRU_Output, uint8_t bValue );

uint8_t SRU_Read_ExtOutput( en_ext_outputs enEXT_Output );

void SRU_Write_LED( en_sru_leds enSRU_LED, uint8_t bValue );

uint8_t SRU_Read_LED( en_sru_leds enSRU_LED );

void SRU_Write_FRAM_SSEL( uint8_t bValue );
void SRU_Write_FRAM_nWP( uint8_t bValue );

uint8_t SRU_Read_DIP_Switch( enum en_sru_dip_switches enSRU_DIP_Switch );

uint8_t SRU_Read_DIP_Bank( enum en_sru_dip_banks enSRU_DIP_Bank );

uint8_t SRU_Read_PreflightInput( en_preflight_input ePreflightInput );

uint8_t SRU_Read_ResetButton( en_reset_button eResetButton );

en_sru_deployments GetSRU_Deployment( void );
void SetSRU_Deployment( void );



/**************************************************************************************************
                                        CAN API
 *************************************************************************************************/
void CAN1_Init( void );
void CAN2_Init_BrakeV1( void );
void CAN2_Init_BrakeV2( void );
Status CAN1_UnloadFromRB(CAN_MSG_T *pstRxMsg);
Status CAN1_LoadToRB(CAN_MSG_T *pstRxMsg);
Status CAN2_UnloadFromRB(CAN_MSG_T *pstRxMsg);
Status CAN2_LoadToRB(CAN_MSG_T *pstRxMsg);
Status CAN1_FillHardwareBuffer();
Status CAN2_FillHardwareBuffer();


/**************************************************************************************************
                                        UART API
 *************************************************************************************************/
void UART_AB_Init( void );
uint8_t UART_AB_ReadByte(uint8_t *pucByte);
uint32_t UART_AB_SendRB(const void *data, int bytes);


/**************************************************************************************************
                                        SPI API
 *************************************************************************************************/
void SPI_Init( void );





#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
