#include "sru_a.h"
#include "sys.h"
#include <string.h>
#define MRA_CAR_NET              (LPC_CAN1)
#define MRA_CAR_NET_BAUD         (enCAN_BAUD_125k)
#define MRA_CTL_NET              (LPC_CAN2)
#define MRA_CTL_NET_BAUD_V1      (enCAN_BAUD_25k)
#define MRA_CTL_NET_BAUD_V2      (enCAN_BAUD_125k)

#define CAN_INTERRUPT_ENABLE_REGISTER_BITMASK         (CAN_IER_RIE)
//--------------------------------------------------------
// Ring buffer of can messages
// This is accessed outside of this module.
extern RINGBUFF_T RxRingBuffer_CAN1;
extern RINGBUFF_T RxRingBuffer_CAN2;
extern RINGBUFF_T TxRingBuffer_CAN1;
extern RINGBUFF_T TxRingBuffer_CAN2;

#define RING_BUFFER_SIZE (32)
static CAN_MSG_T RxBuffer_CAN1[RING_BUFFER_SIZE];
static CAN_MSG_T RxBuffer_CAN2[RING_BUFFER_SIZE];
static CAN_MSG_T TxBuffer_CAN1[RING_BUFFER_SIZE];
static CAN_MSG_T TxBuffer_CAN2[RING_BUFFER_SIZE];
//--------------------------------------------------------
static void Init_CAN_RingBuffers( void )
{
    // Init the Rx Ring buffer.
    RingBuffer_Init(&RxRingBuffer_CAN1, RxBuffer_CAN1, sizeof(RxBuffer_CAN1[0]), RING_BUFFER_SIZE);
    RingBuffer_Init(&RxRingBuffer_CAN2, RxBuffer_CAN2, sizeof(RxBuffer_CAN2[0]), RING_BUFFER_SIZE);

    // Init the Tx Ring buffer.
    RingBuffer_Init(&TxRingBuffer_CAN1, TxBuffer_CAN1, sizeof(TxBuffer_CAN1[0]), RING_BUFFER_SIZE);
    RingBuffer_Init(&TxRingBuffer_CAN2, TxBuffer_CAN2, sizeof(TxBuffer_CAN2[0]), RING_BUFFER_SIZE);
}
//--------------------------------------------------------
/* CAN 1 starts up first */
void CAN1_Init( void )
{
   Chip_CAN_Init      (MRA_CAR_NET, LPC_CANAF, LPC_CANAF_RAM);
   Chip_CAN_SetBitRate(MRA_CAR_NET, MRA_CAR_NET_BAUD);
   Chip_CAN_EnableInt (MRA_CAR_NET, CAN_IER_RIE);
   Chip_CAN_SetAFMode (LPC_CANAF,CAN_AF_BYBASS_MODE);
   NVIC_EnableIRQ(CAN_IRQn);
   Init_CAN_RingBuffers();
}
/* CAN 2 now starts up after parameters are read */
void CAN2_Init_BrakeV1( void )
{
   Chip_CAN_Init      (MRA_CTL_NET, LPC_CANAF, LPC_CANAF_RAM);
   Chip_CAN_SetBitRate(MRA_CTL_NET, MRA_CTL_NET_BAUD_V1);
   Chip_CAN_EnableInt (MRA_CTL_NET, CAN_IER_RIE);
   Chip_CAN_SetAFMode (LPC_CANAF,CAN_AF_BYBASS_MODE);
}
void CAN2_Init_BrakeV2( void )
{
   Chip_CAN_Init      (MRA_CTL_NET, LPC_CANAF, LPC_CANAF_RAM);
   Chip_CAN_SetBitRate(MRA_CTL_NET, MRA_CTL_NET_BAUD_V2);
   Chip_CAN_EnableInt (MRA_CTL_NET, CAN_IER_RIE);
   Chip_CAN_SetAFMode (LPC_CANAF,CAN_AF_BYBASS_MODE);
}

Status CAN1_UnloadFromRB(CAN_MSG_T *pstRxMsg)
{
   Status bReturn = ERROR;
   if(!RingBuffer_IsEmpty( &RxRingBuffer_CAN1 ) )
   {
      RingBuffer_Pop(&RxRingBuffer_CAN1, pstRxMsg);
      bReturn = SUCCESS;
   }
   return bReturn;
}

Status CAN1_LoadToRB(CAN_MSG_T *pstRxMsg)
{
   Status bReturn = ERROR;
   if(RingBuffer_Insert(&TxRingBuffer_CAN1, pstRxMsg))
   {
      bReturn = SUCCESS;
   }
   return bReturn;
}

Status CAN2_UnloadFromRB(CAN_MSG_T *pstRxMsg)
{
   Status bReturn = ERROR;
   if(!RingBuffer_IsEmpty( &RxRingBuffer_CAN2 ) )
   {
      RingBuffer_Pop(&RxRingBuffer_CAN2, pstRxMsg);
      bReturn = SUCCESS;
   }
   return bReturn;
}

Status CAN2_LoadToRB(CAN_MSG_T *pstRxMsg)
{
   Status bReturn = ERROR;
   if(RingBuffer_Insert(&TxRingBuffer_CAN2, pstRxMsg))
   {
      bReturn = SUCCESS;
   }
   return bReturn;
}
/*----------------------------------------------------------------------------
   If theres is data in the TxRingBuffer, it will be loaded into any hardware buffers
 *----------------------------------------------------------------------------*/
Status CAN1_FillHardwareBuffer()
{
   Status bReturn = ERROR;
   for(uint8_t i = 0; i < 2; i++)
   {
      CAN_BUFFER_ID_T TxBuf = CAN_GetFreeTxBuf_Pr1(LPC_CAN1);
      if(TxBuf >= CAN_BUFFER_LAST)
      {
         i = 3;
         break;
      }
      else
      {
         CAN_MSG_T stTxMsg;
         if(!RingBuffer_IsEmpty( &TxRingBuffer_CAN1 ) )
         {
            RingBuffer_Pop(&TxRingBuffer_CAN1, &stTxMsg);
            bReturn = Chip_CAN_Send(LPC_CAN1, TxBuf, &stTxMsg);
         }
      }
   }
   return bReturn;
}
Status CAN2_FillHardwareBuffer()
{
   Status bReturn = ERROR;
   for(uint8_t i = 0; i < 2; i++)
   {
      CAN_BUFFER_ID_T TxBuf = CAN_GetFreeTxBuf_Pr1(LPC_CAN2);
      if(TxBuf >= CAN_BUFFER_LAST)
      {
         i = 3;
         break;
      }
      else
      {
         CAN_MSG_T stTxMsg;
         if(!RingBuffer_IsEmpty( &TxRingBuffer_CAN2 ) )
         {
            RingBuffer_Pop(&TxRingBuffer_CAN2, &stTxMsg);
            bReturn = Chip_CAN_Send(LPC_CAN2, TxBuf, &stTxMsg);
         }
      }
   }
   return bReturn;
}


