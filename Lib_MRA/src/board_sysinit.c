#include "board.h"
#include "sru.h"
#include "sys.h"
/* The System initialization code is called prior to the application and
   initializes the board for run-time operation. Board initialization
   includes clock setup and default pin muxing configuration. */

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

#define IOCON_D_INPUT      (IOCON_FUNC0 | IOCON_HYS_EN)
#define IOCON_D_OUTPUT     (IOCON_FUNC0 | IOCON_HYS_EN)
#define IOCON_D_LED        (IOCON_FUNC0 | IOCON_MODE_PULLUP | IOCON_HYS_EN)
#define IOCON_D_MCU_ID_PIN (IOCON_FUNC0 | IOCON_MODE_PULLUP)
#define IOCON_D_STARTUP_INPUT (IOCON_FUNC0)
#define IOCON_D_SSP0          (IOCON_FUNC5)
#define IOCON_D_SSEL0       (IOCON_FUNC0)

#define IOCON_A_INPUT  (IOCON_FUNC0 | IOCON_FILT_DIS | IOCON_DIGMODE_EN)
#define IOCON_A_OUTPUT (IOCON_FUNC0 | IOCON_FILT_DIS | IOCON_DIGMODE_EN)
#define IOCON_A_LED    (IOCON_FUNC0 | IOCON_MODE_PULLUP | IOCON_FILT_DIS | IOCON_DIGMODE_EN)

#define IOCON_W_INPUT (IOCON_FUNC0 | IOCON_DIGMODE_EN | IOCON_HYS_EN)
#define IOCON_W_SSP1  (IOCON_FUNC2 | IOCON_DIGMODE_EN | IOCON_HYS_EN)

#define IOCON_U_INPUT (IOCON_FUNC0 )

#define IOCON_I_INPUT (IOCON_FUNC0 )
#define IOCON_I_OUTPUT (IOCON_FUNC0 )

/* Pin muxing configuration */
static const PINMUX_GRP_T pinmuxing[] =
{
    //Inputs
    {2, 2 ,   (IOCON_D_STARTUP_INPUT)                                   },// PortAndPin_Input_WDEnable
    {2, 12,   (IOCON_D_STARTUP_INPUT)                                   },// PortAndPin_Input_MR_Select

    {0, 10,   (IOCON_D_MCU_ID_PIN)                                   },// PortAndPin_InputsID
    {0, 11,   (IOCON_D_MCU_ID_PIN)                                   },// PortAndPin_InputsID
    {0, 17,   (IOCON_D_MCU_ID_PIN)                                   },// PortAndPin_InputsID
    {0, 18,   (IOCON_D_MCU_ID_PIN)                                   },// PortAndPin_InputsID

    {1, 0,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {1, 1,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {1, 4,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {1, 8,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {1, 9,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {1,10,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {1,14,    (IOCON_W_INPUT)                                   },// PortAndPin_DIPs
    {1,15,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs

    {3,26,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {0,31,    (IOCON_U_INPUT)                                   },// PortAndPin_DIPs
    {0,27,    (IOCON_I_INPUT)                                   },// PortAndPin_DIPs
    {0,28,    (IOCON_I_INPUT)                                   },// PortAndPin_DIPs
    {1,30,    (IOCON_A_INPUT)                                   },// PortAndPin_DIPs
    {2,11,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {3,24,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {3,25,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs

    {4, 0,    (IOCON_D_INPUT)                                   },// PortAndPin_Inputs
    {1,22,    (IOCON_D_INPUT)                                   },// PortAndPin_Inputs
    {0,14,    (IOCON_D_INPUT)                                   },// PortAndPin_Inputs
    {1,19,    (IOCON_D_INPUT)                                   },// PortAndPin_Inputs
    {1,18,    (IOCON_D_INPUT)                                   },// PortAndPin_Inputs
    {3,23,    (IOCON_D_INPUT)                                   },// PortAndPin_Inputs
    {0,30,    (IOCON_U_INPUT)                                   },// PortAndPin_Inputs
    {0,29 ,   (IOCON_U_INPUT)                                   },// PortAndPin_Inputs

    {4,5 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_ATU
    {4,6 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_ATD
    {0,21 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_ABU
    {0,20 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_ABD

    {4,12 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_LTF
    {4,11 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_LMF
    {2,7 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_LBF
    {4,10 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_LTR
    {2,8 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_LMR
    {2,9 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_LBR

    {3,3 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_ZMIN
    {5,0 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_ZHIN

    {1,28 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_BYPH
    {1,29 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_BYPC
    {4,3 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_MRTR

    {2,13 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_MM
    {4,4 ,   (IOCON_D_INPUT)                                   },// enEXT_Input_GOV

    {1,25 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_PREFLIGHT_1
    {4,1 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_PREFLIGHT_2

    {1,31 ,   (IOCON_A_INPUT)                                   },// eEXT_Input_ZMOUT
    {0,12 ,   (IOCON_A_INPUT)                                   },// eEXT_Input_ZHOUT

    {4,15 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_MRUP
    {4,14 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_MRDN

    {1,26 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_CRGB
    {1,27 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_CRGP
    {4,2 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_CSFP

    {0,24 ,   (IOCON_A_INPUT)                                   },// eEXT_Input_MRGM
    {0,23 ,   (IOCON_A_INPUT)                                   },// eEXT_Input_MSFM
    {3,6 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_MRGP
    {3,7 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_MRGBP
    {5,1 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_MSFP

    {2,3 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_MRGBM

    {0,13 ,   (IOCON_A_INPUT)                                   },// eEXT_Input_M120VAC
    {2,6,   (IOCON_D_INPUT)                                   },// eEXT_Input_CPLD_AUX
    {4,24 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_CW_RESET
    {4,30 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_TRC_LOSS_RESET
    {4,31 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_E_BRAKE_RESET


    {4,13 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_MMC
    {2,12,   (IOCON_D_INPUT)                                   },// eEXT_Input_MDC
    {2,1 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_MBC2

    {1,17 ,   (IOCON_W_INPUT)                                   },// eEXT_Input_CAPT
    {1,16 ,   (IOCON_W_INPUT)                                   },// eEXT_Input_CW_DERAIL
    {3,4 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_BAT_STATUS

    {4,9 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_PIT
    {4,8 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_BUF
    {0,19 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_TFL
    {4,7 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_BFL
    {2,0 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_MBC
    {0,2 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_CUP
    {0,3 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_CDN

    //Outputs
    {0,25 ,   (IOCON_A_OUTPUT)                                   },// eExt_Output_CRGM
    {3,5 ,   (IOCON_D_OUTPUT)                                   },// eExt_Output_CSFM
    {2,6 ,   (IOCON_D_OUTPUT)                                   },// eExt_Output_MCUA_X
    {2,4 ,   (IOCON_D_OUTPUT)                                   },// eExt_Output_CRGBM

    {3,2 ,   (IOCON_D_LED)                                   },// enSRU_LED_Heartbeat
    {3,1 ,   (IOCON_D_LED)                                   },// enSRU_LED_Fault
    {3,0 ,   (IOCON_D_LED)                                   },// enSRU_LED_Alarm
    {4,25 ,   (IOCON_D_LED)                                   },// enSRU_LED_Capture
    {0,26 ,   (IOCON_A_LED)                                   },// enSRU_LED_SafetyZoneC

    {5, 3 ,   (IOCON_I_OUTPUT)                                   },// eFRAM_Output_nWP

    // COM
    {0,15 ,   ((IOCON_FUNC1 | IOCON_HYS_EN                     )) },// U1_TXD
    {0,16 ,   ((IOCON_FUNC1 | IOCON_HYS_EN                     )) },// U1_RXD
    {0,22 ,   ((IOCON_FUNC1 | IOCON_HYS_EN | IOCON_MODE_PULLUP )) },// U1_RTS

    {4,28 ,   (IOCON_FUNC2 | IOCON_HYS_EN                     ) },// U3_TXD
    {4,29 ,   (IOCON_FUNC2 | IOCON_HYS_EN                     ) },// U3_RXD

    {0,1 ,   ((IOCON_FUNC1 | IOCON_HYS_EN | IOCON_MODE_PULLUP )) },// CAN_TD1
    {0,0 ,   ((IOCON_FUNC1 | IOCON_HYS_EN | IOCON_MODE_PULLUP )) },// CAN_RD1

    {0,5 ,   ((IOCON_FUNC2 | IOCON_HYS_EN | IOCON_MODE_PULLUP )) },// CAN_TD2
    {0,4 ,   ((IOCON_FUNC2 | IOCON_HYS_EN | IOCON_MODE_PULLUP )) },// CAN_RD2

    {1,24 ,   (IOCON_FUNC5)                                  },// SSP0_MOSI
    {1,23 ,   (IOCON_FUNC5)                                  },// SSP0_MISO
    {1,20 ,   (IOCON_FUNC5)                                  },// SSP0_SCK
#if ENABLE_FRAM_SSP_MODIFICATION
    {1,21 ,   (IOCON_FUNC3)                                  },// SSP0_SSEL
#else
    {1,21 ,   (IOCON_FUNC0)                                  },// SSP0_SSEL
#endif

    {0,9 ,   (IOCON_FUNC2 | IOCON_DIGMODE_EN)                },// SSP1_MOSI cpld
    {0,8 ,   (IOCON_FUNC2 | IOCON_DIGMODE_EN)                },// SSP1_MISO
    {0,7 ,   (IOCON_FUNC2 | IOCON_DIGMODE_EN)                },// SSP1_SCK
    {0,6 ,   (IOCON_FUNC2)                                   },// SSP1_SSEL

};

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
/* Sets up system pin muxing */
void Board_SetupMuxing(void)
{
   // Reset the IOCON register
   Chip_SYSCTL_PeriphReset(SYSCTL_RESET_IOCON);

   /* Setup system level pin muxing */
   Chip_IOCON_SetPinMuxing(LPC_IOCON, pinmuxing, sizeof(pinmuxing) / sizeof(PINMUX_GRP_T));
}

/* Setup system clocking */
void Board_SetupClocking(void)
{
   // Use the internal system clock
   //Chip_SetupIrcClocking();
   Chip_SetupXtalClocking();
}


/* Set up and initialize hardware prior to call to main */
void Board_SystemInit(void)
{
   Board_SetupMuxing();
   Board_SetupClocking();
}

