/******************************************************************************
 *
 * @file     mcu_a.c
 * @brief    Support for MR A on SR-3032J
 * @version  V1.00
 * @date     21, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_a.h"

#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/


static en_sru_deployments eSRU_Deployment = enSRU_DEPLOYMENT__Invalid0;

static const PortAndPin PortAndPin_Input_WDEnable = {2,  2};

static const PortAndPin PortAndPin_Input_MR_Select = {2, 12};

static const PortAndPin PortAndPin_InputsID[SRU_NUM_ID_PINS] =
{
      {0,  18 },
      {0,  17 },
      {0,  11 },
      {0,  10 },
};

static const PortAndPin PortAndPin_DIPs[SRU_NUM_DIP_SWITCHES] =
{
   {1,  0 },
   {1,  1 },
   {1,  4 },
   {1,  8 },
   {1,  9 },
   {1, 10 },
   {1, 14 },
   {1, 15 },
   // B dips
   {3, 26 },
   {0, 31 },
   {0, 27 },
   {0, 28 },
   {1, 30 },
   {2, 11 },
   {3, 24 },
   {3, 25 },
};

static const PortAndPin PortAndPin_Inputs[ SRU_NUM_INPUTS ] =
{
   {4,  0},  // enSRU_Input_501
   {1, 22}, // enSRU_Input_502
   {0, 14},  // enSRU_Input_503
   {1, 19},  // enSRU_Input_504

   {1, 18},  // enSRU_Input_505
   {3, 23},  // enSRU_Input_506
   {0, 30},  // enSRU_Input_507
   {0, 29},  // enSRU_Input_508
};

static const PortAndPin_Inv PortAndPinInv_ExtInputs[ EXT_NUM_INPUTS ] =
{
      {4,5,0},//eEXT_Input_ATU,
      {4,6,0},//eEXT_Input_ATD,
      {0,21,0},//eEXT_Input_ABU,
      {0,20,0},//eEXT_Input_ABD,

      {4,12,0},//eEXT_Input_LTF,
      {4,11,0},//eEXT_Input_LMF,
      {2,7,0},//eEXT_Input_LBF,
      {4,10,0},//eEXT_Input_LTR,
      {2,8,0},//eEXT_Input_LMR,
      {2,9,0},//eEXT_Input_LBR,

      {3,3,0},//eEXT_Input_ZMIN,
      {5,0,0},//eEXT_Input_ZHIN,

      {1,28,0},//eEXT_Input_BYPH,
      {1,29,0},//eEXT_Input_BYPC,
      {4, 3,1},//eEXT_Input_MRTR,

      {2,13,1},//eEXT_Input_MM,
      {4,4, 0},//eEXT_Input_GOV,

      {1,31,0},//eEXT_Input_F2_OK
      {0,12,0},//eEXT_Input_F3_OK

      {4,15,0},//eEXT_Input_MRUP,
      {4,14,0},//eEXT_Input_MRDN,

      {1,26,0},//eEXT_Input_CRGB,
      {1,27,0},//eEXT_Input_CRGP,
      {4,2,0},//eEXT_Input_CSFP,

      {0,24,1},//eEXT_Input_MRGM,
      {0,23,1},//eEXT_Input_MSFM,
      {3,6,1},//eEXT_Input_MRGP,
      {3,7,1},//eEXT_Input_MRGBP,
      {5,1,1},//eEXT_Input_MSFP,

      {2,3,1},//eEXT_Input_MRGBM,

      {0,13,0},//eEXT_Input_M120VAC,
      {2,6,0},//eEXT_Input_CPLD_AUX,

      {4,13,1},//eEXT_Input_MMC,
      {2,12,1},//eEXT_Input_MDC,
      {2,1,1},//eEXT_Input_MBC2,

      {1,17,0},//eEXT_Input_CAPT,

      {1,16,0},//eEXT_Input_CW_DERAIL,

      {3,4,0},//eEXT_Input_BAT_STATUS,

      {4, 9, 0},//eEXT_Input_PIT,
      {4, 8, 0},//eEXT_Input_BUF,
      {0,19, 0},//eEXT_Input_TFL,
      {4, 7, 0},//eEXT_Input_BFL,
      {2,0,1},//eEXT_Input_MBC,
      {0,2,1},//eEXT_Input_CUP,
      {0,3,1},//eEXT_Input_CDN,

};

static const PortAndPin PortAndPin_PreflightInputs[ NUM_PREFLIGHT_INPUTS ] =
{
      {1, 25},//ePREFLIGHT_INPUT_1,
      {4,  1},//ePREFLIGHT_INPUT_2,
};

static const PortAndPin PortAndPin_ResetButtons[ NUM_RESET_BUTTONS ] =
{
      {4,24},//eRESET_BUTTON_CW,
      {4,30},//eRESET_BUTTON_TRC_LOSS,
      {4,31},//eRESET_BUTTON_E_BRAKE,
};

//------------------------------------------------------------
static const PortAndPin PortAndPin_ExtOutputs[ EXT_NUM_OUTPUTS ] =
{
   {0, 25},  // eExt_Output_CRGM
   {3,  5},  // eExt_Output_CSFM
   {2,  6},  // eExt_Output_MCUA_X
   {2,  4},  // eExt_Output_CRGBM
};
//------------------------------------------------------------
static const PortAndPin PortAndPin_FRAM_Outputs[ NUM_FRAM_OUTPUTS ] =
{
#if !ENABLE_FRAM_SSP_MODIFICATION
   {1, 21},  // eFRAM_Output_SSEL
#endif
   {5,  3},  // eFRAM_Output_nWP
};
//------------------------------------------------------------
static const PortAndPin PortAndPin_LEDs[ SRU_NUM_LEDS ] =
{
      {3, 2},  // enSRU_LED_Heartbeat
      {3, 1},  // enSRU_LED_Fault
      {3, 0},   // enSRU_LED_Alarm

      {4, 25},   // enSRU_LED_Capture

      {0, 26}   // enSRU_LED_SafetyZoneC
};

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Returns 4 bit hardcoded MCU ID
-----------------------------------------------------------------------------*/
uint8_t SRU_Read_MCU_ID()
{
  uint8_t ucValue = 0;
  for(uint8_t i = 0; i < SRU_NUM_ID_PINS; i++)
  {
     uint8_t uiPort = PortAndPin_InputsID[i].port;
     uint8_t uiPin  = PortAndPin_InputsID[i].pin;
     uint8_t bValue = Chip_GPIO_GetPinState(LPC_GPIO, uiPort, uiPin);
     ucValue |= (bValue << i);
  }
  return ucValue;
}
/*-----------------------------------------------------------------------------
if pin is high, watchdog is enabled
-----------------------------------------------------------------------------*/
uint8_t SRU_Read_WatchDogEnable()
{
  uint8_t bValue = 0;

   uint8_t uiPort = PortAndPin_Input_WDEnable.port;
   uint8_t uiPin  = PortAndPin_Input_WDEnable.pin;

   bValue = Chip_GPIO_GetPinState(LPC_GPIO, uiPort, uiPin);

  return bValue;
}
/*-----------------------------------------------------------------------------
Returns 1 if MR, 0 if CT/COP
-----------------------------------------------------------------------------*/
uint8_t SRU_Read_BoardTypeIndicator()
{
  uint8_t bValue = 0;

   uint8_t uiPort = PortAndPin_Input_MR_Select.port;
   uint8_t uiPin  = PortAndPin_Input_MR_Select.pin;

   bValue = Chip_GPIO_GetPinState(LPC_GPIO, uiPort, uiPin);

  return bValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_DIP_Switch( enum en_sru_dip_switches enSRU_DIP_Switch )
{
   uint8_t bValue = 0;

   if( enSRU_DIP_Switch < SRU_NUM_DIP_SWITCHES )
   {
      uint8_t ucPin = PortAndPin_DIPs[enSRU_DIP_Switch].pin;
      uint8_t ucPort = PortAndPin_DIPs[enSRU_DIP_Switch].port;
      bValue = !Chip_GPIO_GetPinState(LPC_GPIO,ucPort,ucPin);
   }

   return bValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_DIP_Bank( enum en_sru_dip_banks enSRU_DIP_Bank )
{
   uint8_t ucValue = 0;

   if ( enSRU_DIP_Bank == enSRU_DIP_BANK_A )
   {
      for(uint8_t i = 0; i <= enSRU_DIP_A8; i++)
      {
         uint8_t ucPin = PortAndPin_DIPs[i].pin;
         uint8_t ucPort = PortAndPin_DIPs[i].port;
         uint8_t bValue = !Chip_GPIO_GetPinState(LPC_GPIO,ucPort,ucPin);
         ucValue |= (bValue << i);
      }
   }
   else if( enSRU_DIP_Bank == enSRU_DIP_BANK_B )
   {
      for(uint8_t i = enSRU_DIP_B1; i <= enSRU_DIP_B8; i++)
      {
         uint8_t ucPin = PortAndPin_DIPs[i].pin;
         uint8_t ucPort = PortAndPin_DIPs[i].port;
         uint8_t bValue = !Chip_GPIO_GetPinState(LPC_GPIO,ucPort,ucPin);
         ucValue |= (bValue << i);
      }
   }

   return ucValue;
}
/*-----------------------------------------------------------------------------
returns state of general gpio status
 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_Input( en_sru_inputs enSRU_Input )
{
   uint8_t bValue = 0;

   if ( enSRU_Input < SRU_NUM_INPUTS )
   {
      uint8_t uiPort = PortAndPin_Inputs[ enSRU_Input ].port;
      uint8_t uiPin  = PortAndPin_Inputs[ enSRU_Input ].pin;

      // Invert the value returned by Sys_MCU_Pin_In() because when an input is
      // powered, the port pin is low and vice versa.
      bValue = !Chip_GPIO_GetPinState(LPC_GPIO, uiPort, uiPin);
   }

   return bValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_ExtInput( en_ext_inputs enEXT_Input )
{
   uint8_t bValue = 0;

   if ( enEXT_Input < EXT_NUM_INPUTS )
   {
      uint8_t uiPort = PortAndPinInv_ExtInputs[ enEXT_Input ].port;
      uint8_t uiPin  = PortAndPinInv_ExtInputs[ enEXT_Input ].pin;
      uint8_t bInv = PortAndPinInv_ExtInputs[ enEXT_Input ].invert;

      // Invert the value returned by Sys_MCU_Pin_In() because when an input is
      // powered, the port pin is low and vice versa.
      bValue = Chip_GPIO_GetPinState(LPC_GPIO, uiPort, uiPin) ^ bInv;
   }

   return bValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_RawExtInput( en_ext_inputs enEXT_Input )
{
   uint8_t bValue = 0;

   if ( enEXT_Input < EXT_NUM_INPUTS )
   {
      uint8_t uiPort = PortAndPinInv_ExtInputs[ enEXT_Input ].port;
      uint8_t uiPin  = PortAndPinInv_ExtInputs[ enEXT_Input ].pin;

      // Invert the value returned by Sys_MCU_Pin_In() because when an input is
      // powered, the port pin is low and vice versa.
      bValue = Chip_GPIO_GetPinState(LPC_GPIO, uiPort, uiPin);
   }

   return bValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_PreflightInput( en_preflight_input ePreflightInput )
{
   uint8_t bValue = 0;

   if ( ePreflightInput < NUM_PREFLIGHT_INPUTS )
   {
      uint8_t uiPort = PortAndPin_PreflightInputs[ ePreflightInput ].port;
      uint8_t uiPin  = PortAndPin_PreflightInputs[ ePreflightInput ].pin;

      // Invert the value returned by Sys_MCU_Pin_In() because when an input is
      // powered, the port pin is low and vice versa.
      bValue = Chip_GPIO_GetPinState(LPC_GPIO, uiPort, uiPin);
   }

   return bValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_ResetButton( en_reset_button eResetButton )
{
   uint8_t bValue = 0;

   if ( eResetButton < NUM_RESET_BUTTONS )
   {
      uint8_t uiPort = PortAndPin_ResetButtons[ eResetButton ].port;
      uint8_t uiPin  = PortAndPin_ResetButtons[ eResetButton ].pin;

      // Invert the value returned by Sys_MCU_Pin_In() because when an input is
      // powered, the port pin is low and vice versa.
      bValue = !Chip_GPIO_GetPinState(LPC_GPIO, uiPort, uiPin);
   }

   return bValue;
}

/*-----------------------------------------------------------------------------
   LED write
 -----------------------------------------------------------------------------*/
void SRU_Write_LED( en_sru_leds enSRU_LED, uint8_t bValue )
{
   if ( enSRU_LED < SRU_NUM_LEDS )
   {
      uint8_t uiPort = PortAndPin_LEDs[ enSRU_LED ].port;
      uint8_t uiPin  = PortAndPin_LEDs[ enSRU_LED ].pin;

      // The port pins that control the LEDs must be pulled low to turn on the
      // LED so we need to invert bValue to get the desired behavior.
      Chip_GPIO_SetPinState(LPC_GPIO , uiPort, uiPin, !bValue);
   }
}
/*-----------------------------------------------------------------------------
   LED Read
 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_LED( en_sru_leds enSRU_LED )
{
   uint8_t bReturn = 0;
   if ( enSRU_LED < SRU_NUM_LEDS )
   {
      uint8_t uiPort = PortAndPin_LEDs[ enSRU_LED ].port;
      uint8_t uiPin  = PortAndPin_LEDs[ enSRU_LED ].pin;

      // The port pins that control the LEDs must be pulled low to turn on the
      // LED so we need to invert bValue to get the desired behavior.
      bReturn = Chip_GPIO_GetPinState(LPC_GPIO , uiPort, uiPin);
   }
   return bReturn;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SRU_Write_ExtOutput( en_ext_outputs enEXT_Output, uint8_t bValue )
{
   if ( enEXT_Output < EXT_NUM_OUTPUTS )
   {
      uint8_t uiPort = PortAndPin_ExtOutputs[ enEXT_Output ].port;
      uint8_t uiPin  = PortAndPin_ExtOutputs[ enEXT_Output ].pin;
      Chip_GPIO_SetPinState(LPC_GPIO, uiPort, uiPin, bValue);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_ExtOutput( en_ext_outputs enEXT_Output )
{
   uint8_t bValue = 0;
   if ( enEXT_Output < EXT_NUM_OUTPUTS )
   {
      uint8_t uiPort = PortAndPin_ExtOutputs[ enEXT_Output ].port;
      uint8_t uiPin = PortAndPin_ExtOutputs[ enEXT_Output ].pin;
      bValue = Chip_GPIO_GetPinState( LPC_GPIO, uiPort, uiPin );
   }

   return bValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SRU_Write_FRAM_SSEL( uint8_t bValue )
{
#if !ENABLE_FRAM_SSP_MODIFICATION
   uint8_t uiPort = PortAndPin_FRAM_Outputs[ eFRAM_Output_SSEL ].port;
   uint8_t uiPin  = PortAndPin_FRAM_Outputs[ eFRAM_Output_SSEL ].pin;
   Chip_GPIO_SetPinState(LPC_GPIO, uiPort, uiPin, bValue);
#endif
}
/* Control the FRAM write protection pin */
void SRU_Write_FRAM_nWP( uint8_t bValue )
{

   uint8_t uiPort = PortAndPin_FRAM_Outputs[ eFRAM_Output_nWP ].port;
   uint8_t uiPin  = PortAndPin_FRAM_Outputs[ eFRAM_Output_nWP ].pin;
   Chip_GPIO_SetPinState(LPC_GPIO, uiPort, uiPin, bValue);
}
/*-----------------------------------------------------------------------------
   Set bit to indicate pin direction (0 = input, 1 = output).
 -----------------------------------------------------------------------------*/
static void ConfigureGPIO_DirectionOutputs( void )
{
    // Setup output pins
    for (uint32_t i = 0; i < EXT_NUM_OUTPUTS; i++ )
    {
        Chip_GPIO_SetDir(LPC_GPIO, PortAndPin_ExtOutputs[i].port, PortAndPin_ExtOutputs[i].pin,true);
    }
    // Setup led outputs
    for (uint32_t i = 0; i < SRU_NUM_LEDS; i++)
    {
        Chip_GPIO_SetDir(LPC_GPIO, PortAndPin_LEDs[i].port, PortAndPin_LEDs[i].pin,true);
    }
    // Setup FRAM output
    for (uint32_t i = 0; i < NUM_FRAM_OUTPUTS; i++)
    {
       Chip_GPIO_SetDir(LPC_GPIO, PortAndPin_FRAM_Outputs[i].port, PortAndPin_FRAM_Outputs[i].pin,true);
    }

    // Turn off all outputs.
    for ( uint32_t i = 0; i < EXT_NUM_OUTPUTS; ++i )
    {
       SRU_Write_ExtOutput( i, 0 );
    }
    for ( uint32_t i = 0; i < SRU_NUM_LEDS; ++i )
    {
       SRU_Write_LED( i, 0 );
    }
    SRU_Write_FRAM_SSEL(0);
    SRU_Write_FRAM_nWP(0);
}

static void ConfigureGPIO_Direction( void )
{
    ConfigureGPIO_DirectionOutputs();
}

/*-----------------------------------------------------------------------------
   Verify the 4 ID pins connected to the MCU identify this MCU as part of the
   SRU Base board (MCU A). If not, loop forever flashing all LEDs.
 -----------------------------------------------------------------------------*/
#define TOGGLE_LED_TIMEOUT (0xFFFFF)
void VerifyBoardID( void )
{
   uint8_t ucMCU_ID = SRU_Read_MCU_ID() & 0xE;// Ignore least significant bit
   if ( ucMCU_ID !=  enMCU_MR_Base )
   {
      while ( 1 )
      {
         static uint32_t uiCounter, bState;

         if( uiCounter >= TOGGLE_LED_TIMEOUT )
         {
            uiCounter = 0;

            bState = ( bState )? 0: 1;

            SRU_Write_LED( enSRU_LED_Heartbeat, bState );
            SRU_Write_LED( enSRU_LED_Alarm,     bState );
            SRU_Write_LED( enSRU_LED_Fault,     bState );
         }
         else
         {
            uiCounter++;
         }
      }
   }
}

en_sru_deployments GetSRU_Deployment( void )
{
     return eSRU_Deployment;
}

void SetSRU_Deployment( void )
{
   uint8_t ucID = SRU_Read_MCU_ID() & 0xE;// Ignore least significant bit
   eSRU_Deployment = enSRU_DEPLOYMENT__Invalid0;
   if(ucID == enMCU_MR_Base)
    {
       eSRU_Deployment = enSRU_DEPLOYMENT__MR;
    }
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void MCU_A_Init( void )
{
   ConfigureGPIO_Direction();

   VerifyBoardID();

   // Set the global deployment variable. This is used to set up some
   // of the other board specific features.
   SetSRU_Deployment();
   CAN1_Init();
   UART_AB_Init();
//   FPGA_Init(); // Moved to mod_fpga.c
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
