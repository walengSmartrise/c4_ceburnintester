#include "shared_data_cta.h"

/***************************************************************************************************
    DG_CTA__ParamSlaveRequest,
 ***************************************************************************************************/
//DATAGRAM_ID_151
uint8_t GetParamSlaveRequest_MessageType_CTA()
{
   return shared_data[DATAGRAM_ID_151].data.data.auc8[0];
}
uint8_t GetParamSlaveRequest_BlockIndex_CTA()
{
   return shared_data[DATAGRAM_ID_151].data.data.auc8[1];
}
uint8_t GetParamSlaveRequest_ChunkIndex_CTA()
{
   return shared_data[DATAGRAM_ID_151].data.data.auc8[2];
}
uint8_t GetParamSlaveRequest_NodeID_CTA()
{
   return shared_data[DATAGRAM_ID_151].data.data.auc8[3];
}

uint16_t GetParamSlaveRequest_ParameterIndex_CTA()
{
   return shared_data[DATAGRAM_ID_151].data.data.auw16[1];
}
uint32_t GetParamSlaveRequest_ParamValue_CTA()
{
   return shared_data[DATAGRAM_ID_151].data.data.aui32[1];
}
/***************************************************************************************************
    DG_CTA__Fault1,
 ***************************************************************************************************/
//DATAGRAM_ID_9
uint16_t GetFault1_Fault_CTA()
{
   return shared_data[DATAGRAM_ID_9].data.data.auw16[0];
}
uint32_t GetFault1_Timestamp_CTA()
{
   return shared_data[DATAGRAM_ID_9].data.data.aui32[1];
}

/***************************************************************************************************
    DG_CTA__Fault2,
 ***************************************************************************************************/
//DATAGRAM_ID_10
uint16_t GetFault2_Fault_CTA()
{
   return shared_data[DATAGRAM_ID_10].data.data.auw16[0];
}
uint16_t GetFault2_Speed_CTA()
{
   return shared_data[DATAGRAM_ID_10].data.data.auw16[1];
}
uint32_t GetFault2_Position_CTA()
{
   return shared_data[DATAGRAM_ID_10].data.data.aui32[1];
}

/***************************************************************************************************
    DG_CTA__Fault3,
 ***************************************************************************************************/
//DATAGRAM_ID_25
uint16_t GetFault3_Fault_CTA(void)
{
   return shared_data[DATAGRAM_ID_25].data.data.auw16[0];
}
uint8_t GetFault3_CurrentFloor_CTA(void)
{
   return shared_data[DATAGRAM_ID_25].data.data.auc8[2];
}
uint8_t GetFault3_DestinationFloor_CTA(void)
{
   return shared_data[DATAGRAM_ID_25].data.data.auc8[3];
}
uint16_t GetFault3_CommandSpeed_CTA(void)
{
   return shared_data[DATAGRAM_ID_25].data.data.auw16[2];
}
uint16_t GetFault3_EncoderSpeed_CTA(void)
{
   return shared_data[DATAGRAM_ID_25].data.data.auw16[3];
}

/***************************************************************************************************
    DG_CTA__Alarm1,
 ***************************************************************************************************/
//DATAGRAM_ID_88
uint16_t GetAlarm1_Alarm_CTA()
{
   return shared_data[DATAGRAM_ID_88].data.data.auw16[0];
}
uint32_t GetAlarm1_Timestamp_CTA()
{
   return shared_data[DATAGRAM_ID_88].data.data.aui32[1];
}

/***************************************************************************************************
    DG_CTA__Alarm2,
 ***************************************************************************************************/
//DATAGRAM_ID_89
uint16_t GetAlarm2_Alarm_CTA()
{
   return shared_data[DATAGRAM_ID_89].data.data.auw16[0];
}
uint16_t GetAlarm2_Speed_CTA()
{
   return shared_data[DATAGRAM_ID_89].data.data.auw16[1];
}
uint32_t GetAlarm2_Position_CTA()
{
   return shared_data[DATAGRAM_ID_89].data.data.aui32[1];
}

/***************************************************************************************************
    DG_CTA__LocalInputs,
 ***************************************************************************************************/
//DATAGRAM_ID_26
uint32_t GetLocalInputs_Inputs_CTA()
{
   return shared_data[DATAGRAM_ID_26].data.data.aui32[0];
}
uint8_t GetPreflightState_CTA()
{
   return shared_data[DATAGRAM_ID_26].data.data.auc8[4];
}

/***************************************************************************************************
    DG_CTA__Debug,
 ***************************************************************************************************/
//DATAGRAM_ID_170
uint8_t GetDebugData_CTA_DL20_PhoneFailureFlag(void)
{
   return ( shared_data[DATAGRAM_ID_170].data.data.auc8[0] >> 0 ) & 1;
}
uint8_t GetDebugData_CTA_DL20_OOSFlag(void)
{
   return ( shared_data[DATAGRAM_ID_170].data.data.auc8[0] >> 1 ) & 1;
}
uint8_t GetDebugData_CTA_CAN_Utilization(void)
{
   return shared_data[DATAGRAM_ID_170].data.data.auc8[5];
}
uint16_t GetDebugData_CTA_NetworkError(void)
{
   return shared_data[DATAGRAM_ID_170].data.data.auw16[3];
}
uint32_t GetDebugData_CTA_Data(void)
{
   return shared_data[DATAGRAM_ID_170].data.data.aui32[1];
}

/***************************************************************************************************
    DG_CTA__SysPosition,
 ***************************************************************************************************/
//DATAGRAM_ID_2
uint32_t GetSysPosition_Position_CTA()
{
   return shared_data[DATAGRAM_ID_2].data.data.aui32[0] & MAX_24BIT_VALUE;
}
int16_t GetSysPosition_Velocity_CTA()
{
   return shared_data[DATAGRAM_ID_2].data.data.auw16[2];
}

/***************************************************************************************************
    DG_CTA__CPLD_Data0,
 ***************************************************************************************************/
//DATAGRAM_ID_234
uint32_t GetCPLD_Data0_CTA(void)
{
   return shared_data[DATAGRAM_ID_234].data.data.aui32[0];
}
uint32_t GetCPLD_Data1_CTA(void)
{
   return shared_data[DATAGRAM_ID_234].data.data.aui32[1];
}
/***************************************************************************************************
    DG_CTA__CPLD_Data1,
 ***************************************************************************************************/
//DATAGRAM_ID_235
uint32_t GetCPLD_Data2_CTA(void)
{
   return shared_data[DATAGRAM_ID_235].data.data.aui32[0];
}
uint32_t GetCPLD_Data3_CTA(void)
{
   return shared_data[DATAGRAM_ID_235].data.data.aui32[1];
}
