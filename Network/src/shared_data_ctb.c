#include "shared_data_ctb.h"

/***************************************************************************************************
    DG_CTB__ParamSlaveRequest,
 ***************************************************************************************************/
//DATAGRAM_ID_152
uint8_t GetParamSlaveRequest_MessageType_CTB()
{
   return shared_data[DATAGRAM_ID_152].data.data.auc8[0];
}
uint8_t GetParamSlaveRequest_BlockIndex_CTB()
{
   return shared_data[DATAGRAM_ID_152].data.data.auc8[1];
}
uint8_t GetParamSlaveRequest_ChunkIndex_CTB()
{
   return shared_data[DATAGRAM_ID_152].data.data.auc8[2];
}
uint8_t GetParamSlaveRequest_NodeID_CTB()
{
   return shared_data[DATAGRAM_ID_152].data.data.auc8[3];
}

uint16_t GetParamSlaveRequest_ParameterIndex_CTB()
{
   return shared_data[DATAGRAM_ID_152].data.data.auw16[1];
}
uint32_t GetParamSlaveRequest_ParamValue_CTB()
{
   return shared_data[DATAGRAM_ID_152].data.data.aui32[1];
}
/***************************************************************************************************
    DG_CTB__Fault1,
 ***************************************************************************************************/
//DATAGRAM_ID_11
uint16_t GetFault1_Fault_CTB()
{
   return shared_data[DATAGRAM_ID_11].data.data.auw16[0];
}
uint32_t GetFault1_Timestamp_CTB()
{
   return shared_data[DATAGRAM_ID_11].data.data.aui32[1];
}

/***************************************************************************************************
    DG_CTB__Fault2,
 ***************************************************************************************************/
//DATAGRAM_ID_12
uint16_t GetFault2_Fault_CTB()
{
   return shared_data[DATAGRAM_ID_12].data.data.auw16[0];
}
uint16_t GetFault2_Speed_CTB()
{
   return shared_data[DATAGRAM_ID_12].data.data.auw16[1];
}
uint32_t GetFault2_Position_CTB()
{
   return shared_data[DATAGRAM_ID_12].data.data.aui32[1];
}

/***************************************************************************************************
    DG_CTB__Fault3,
 ***************************************************************************************************/
//DATAGRAM_ID_73
uint16_t GetFault3_Fault_CTB(void)
{
   return shared_data[DATAGRAM_ID_73].data.data.auw16[0];
}
uint8_t GetFault3_CurrentFloor_CTB(void)
{
   return shared_data[DATAGRAM_ID_73].data.data.auc8[2];
}
uint8_t GetFault3_DestinationFloor_CTB(void)
{
   return shared_data[DATAGRAM_ID_73].data.data.auc8[3];
}
uint16_t GetFault3_CommandSpeed_CTB(void)
{
   return shared_data[DATAGRAM_ID_73].data.data.auw16[2];
}
uint16_t GetFault3_EncoderSpeed_CTB(void)
{
   return shared_data[DATAGRAM_ID_73].data.data.auw16[3];
}

/***************************************************************************************************
    DG_CTB__Alarm1,
 ***************************************************************************************************/
//DATAGRAM_ID_90
uint16_t GetAlarm1_Alarm_CTB()
{
   return shared_data[DATAGRAM_ID_90].data.data.auw16[0];
}
uint32_t GetAlarm1_Timestamp_CTB()
{
   return shared_data[DATAGRAM_ID_90].data.data.aui32[1];
}

/***************************************************************************************************
    DG_CTB__Alarm2,
 ***************************************************************************************************/
//DATAGRAM_ID_91
uint16_t GetAlarm2_Alarm_CTB()
{
   return shared_data[DATAGRAM_ID_91].data.data.auw16[0];
}
uint16_t GetAlarm2_Speed_CTB()
{
   return shared_data[DATAGRAM_ID_91].data.data.auw16[1];
}
uint32_t GetAlarm2_Position_CTB()
{
   return shared_data[DATAGRAM_ID_91].data.data.aui32[1];
}

/***************************************************************************************************
    DG_CTB__UI_Req,
 ***************************************************************************************************/
//DATAGRAM_ID_156
uint8_t GetUIRequest_AcceptanceTest_CTB()
{
   return shared_data[DATAGRAM_ID_156].data.data.auc8[0];
}
uint8_t GetUIRequest_FaultLog_CTB()
{
   return shared_data[DATAGRAM_ID_156].data.data.auc8[1];
}
uint8_t GetUIRequest_DoorControl_CTB()
{
   return shared_data[DATAGRAM_ID_156].data.data.auc8[2];
}
uint8_t GetUIRequest_CarCall_F_CTB()
{
   return shared_data[DATAGRAM_ID_156].data.data.auc8[3];
}
uint16_t GetUIRequest_HallStatusIndex_CTB()
{
   return shared_data[DATAGRAM_ID_156].data.data.auw16[2];
}
uint8_t GetUIRequest_DefaultCommand_CTB()
{
   return shared_data[DATAGRAM_ID_156].data.data.auc8[6];
}
uint8_t GetUIRequest_ViewDebugData_CTB(void)
{
   return shared_data[DATAGRAM_ID_156].data.data.auc8[7];
}

/***************************************************************************************************
    DG_CTB__DebugStates,
 ***************************************************************************************************/
//DATAGRAM_ID_172
uint8_t GetDebugStates_NTSState_CTB()
{
   return shared_data[DATAGRAM_ID_172].data.data.auc8[0];
}
uint8_t GetDebugStates_NTSAlarm_CTB()
{
   return shared_data[DATAGRAM_ID_172].data.data.auc8[1];
}
uint8_t GetDebugStates_NTSFault_CTB()
{
   return shared_data[DATAGRAM_ID_172].data.data.auc8[2];
}
uint8_t GetNTS_StopFlag_CTB()
{
   return shared_data[DATAGRAM_ID_172].data.data.auc8[3];
}

/***************************************************************************************************
    DG_CTB__UNUSED7,
 ***************************************************************************************************/
//DATAGRAM_ID_174

/***************************************************************************************************
    DG_CTB__LW,
 ***************************************************************************************************/
//DATAGRAM_ID_192
int8_t GetLW_AverageTorquePercent_CTB(void)
{
   return shared_data[DATAGRAM_ID_192].data.data.auc8[0];
}
uint8_t GetLW_OfflineStatus_CTB(void)
{
   return shared_data[DATAGRAM_ID_192].data.data.auc8[1];
}
uint8_t GetLW_ReceiveCounter_CTB(void)
{
   return shared_data[DATAGRAM_ID_192].data.data.auc8[2];
}
int8_t GetLW_RawTorquePercent_CTB(void)
{
   return shared_data[DATAGRAM_ID_192].data.data.auc8[3];
}
uint8_t GetLW_GetError_CTB(void)
{
   return shared_data[DATAGRAM_ID_192].data.data.auc8[4];
}
uint8_t GetLW_GetInCarWeight_CTB(void)
{
   return shared_data[DATAGRAM_ID_192].data.data.auc8[5];
}
uint8_t GetLW_GetLoadFlag_CTB(void)
{
   return shared_data[DATAGRAM_ID_192].data.data.auc8[6];
}
uint8_t GetLW_GetCalibrationState_CTB(void)
{
   return shared_data[DATAGRAM_ID_192].data.data.auc8[7];
}
/***************************************************************************************************
    DG_CTB__Debug,
 ***************************************************************************************************/
//DATAGRAM_ID_218
uint8_t GetDebugData_CTB_CAN_Utilization(void)
{
   return shared_data[DATAGRAM_ID_218].data.data.auc8[5];
}
uint16_t GetDebugData_CTB_NetworkError(void)
{
   return shared_data[DATAGRAM_ID_218].data.data.auw16[3];
}
uint32_t GetDebugData_CTB_Data(void)
{
   return shared_data[DATAGRAM_ID_218].data.data.aui32[1];
}

/***************************************************************************************************
    DG_CTB__UI_Req_2,
 ***************************************************************************************************/
//DATAGRAM_ID_223
uint8_t GetUIRequest_CarCall_R_CTB(void)
{
   return shared_data[DATAGRAM_ID_223].data.data.auc8[0];
}
/***************************************************************************************************
    DG_CTB__LW_2,
 ***************************************************************************************************/
//DATAGRAM_ID_226
uint8_t GetLW_UIReq_Request_CTB(void)
{
   return shared_data[DATAGRAM_ID_226].data.data.auc8[0];
}
uint8_t GetLW_UIReq_Command_CTB(void)
{
   return shared_data[DATAGRAM_ID_226].data.data.auc8[1];
}
float GetLW_UIReq_Data_CTB(void)
{
   return shared_data[DATAGRAM_ID_226].data.data.af32[1];
}
