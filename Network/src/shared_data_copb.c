#include "shared_data_copb.h"

/***************************************************************************************************
    DG_COPB__ParamSlaveRequest,
 ***************************************************************************************************/
//DATAGRAM_ID_154
uint8_t GetParamSlaveRequest_MessageType_COPB()
{
   return shared_data[DATAGRAM_ID_154].data.data.auc8[0];
}
uint8_t GetParamSlaveRequest_BlockIndex_COPB()
{
   return shared_data[DATAGRAM_ID_154].data.data.auc8[1];
}
uint8_t GetParamSlaveRequest_ChunkIndex_COPB()
{
   return shared_data[DATAGRAM_ID_154].data.data.auc8[2];
}
uint8_t GetParamSlaveRequest_NodeID_COPB()
{
   return shared_data[DATAGRAM_ID_154].data.data.auc8[3];
}

uint16_t GetParamSlaveRequest_ParameterIndex_COPB()
{
   return shared_data[DATAGRAM_ID_154].data.data.auw16[1];
}
uint32_t GetParamSlaveRequest_ParamValue_COPB()
{
   return shared_data[DATAGRAM_ID_154].data.data.aui32[1];
}
/***************************************************************************************************
    DG_COPB__Fault1,
 ***************************************************************************************************/
//DATAGRAM_ID_15
uint16_t GetFault1_Fault_COPB()
{
   return shared_data[DATAGRAM_ID_15].data.data.auw16[0];
}
uint32_t GetFault1_Timestamp_COPB()
{
   return shared_data[DATAGRAM_ID_15].data.data.aui32[1];
}

/***************************************************************************************************
    DG_COPB__Fault2,
 ***************************************************************************************************/
//DATAGRAM_ID_16
uint16_t GetFault2_Fault_COPB()
{
   return shared_data[DATAGRAM_ID_16].data.data.auw16[0];
}
uint16_t GetFault2_Speed_COPB()
{
   return shared_data[DATAGRAM_ID_16].data.data.auw16[1];
}
uint32_t GetFault2_Position_COPB()
{
   return shared_data[DATAGRAM_ID_16].data.data.aui32[1];
}

/***************************************************************************************************
    DG_COPB__Fault3,
 ***************************************************************************************************/
//DATAGRAM_ID_76
uint16_t GetFault3_Fault_COPB(void)
{
   return shared_data[DATAGRAM_ID_76].data.data.auw16[0];
}
uint8_t GetFault3_CurrentFloor_COPB(void)
{
   return shared_data[DATAGRAM_ID_76].data.data.auc8[2];
}
uint8_t GetFault3_DestinationFloor_COPB(void)
{
   return shared_data[DATAGRAM_ID_76].data.data.auc8[3];
}
uint16_t GetFault3_CommandSpeed_COPB(void)
{
   return shared_data[DATAGRAM_ID_76].data.data.auw16[2];
}
uint16_t GetFault3_EncoderSpeed_COPB(void)
{
   return shared_data[DATAGRAM_ID_76].data.data.auw16[3];
}

/***************************************************************************************************
    DG_COPB__Alarm1,
 ***************************************************************************************************/
//DATAGRAM_ID_94
uint16_t GetAlarm1_Alarm_COPB()
{
   return shared_data[DATAGRAM_ID_94].data.data.auw16[0];
}
uint32_t GetAlarm1_Timestamp_COPB()
{
   return shared_data[DATAGRAM_ID_94].data.data.aui32[1];
}

/***************************************************************************************************
    DG_COPB__Alarm2,
 ***************************************************************************************************/
//DATAGRAM_ID_95
uint16_t GetAlarm2_Alarm_COPB()
{
   return shared_data[DATAGRAM_ID_95].data.data.auw16[0];
}
uint16_t GetAlarm2_Speed_COPB()
{
   return shared_data[DATAGRAM_ID_95].data.data.auw16[1];
}
uint32_t GetAlarm2_Position_COPB()
{
   return shared_data[DATAGRAM_ID_95].data.data.aui32[1];
}

/***************************************************************************************************
    DG_COPB__UI_Req,
 ***************************************************************************************************/
//DATAGRAM_ID_157
uint8_t GetUIRequest_AcceptanceTest_COPB()
{
   return shared_data[DATAGRAM_ID_157].data.data.auc8[0];
}
uint8_t GetUIRequest_FaultLog_COPB()
{
   return shared_data[DATAGRAM_ID_157].data.data.auc8[1];
}
uint8_t GetUIRequest_DoorControl_COPB()
{
   return shared_data[DATAGRAM_ID_157].data.data.auc8[2];
}
uint8_t GetUIRequest_CarCall_F_COPB()
{
   return shared_data[DATAGRAM_ID_157].data.data.auc8[3];
}
uint16_t GetUIRequest_HallStatusIndex_COPB()
{
   return shared_data[DATAGRAM_ID_157].data.data.auw16[2];
}
uint8_t GetUIRequest_DefaultCommand_COPB()
{
   return shared_data[DATAGRAM_ID_157].data.data.auc8[6];
}
uint8_t GetUIRequest_ViewDebugData_COPB(void)
{
   return shared_data[DATAGRAM_ID_157].data.data.auc8[7];
}

/***************************************************************************************************
    DG_COPB__Debug,
 ***************************************************************************************************/
//DATAGRAM_ID_219
uint8_t GetDebugData_COPB_CAN_Utilization(void)
{
   return shared_data[DATAGRAM_ID_219].data.data.auc8[5];
}
uint16_t GetDebugData_COPB_NetworkError(void)
{
   return shared_data[DATAGRAM_ID_219].data.data.auw16[3];
}
uint32_t GetDebugData_COPB_Data(void)
{
   return shared_data[DATAGRAM_ID_219].data.data.aui32[1];
}
/***************************************************************************************************
    DG_COPB__UNUSED7,
 ***************************************************************************************************/
//DATAGRAM_ID_221

/***************************************************************************************************
    DG_COPB__UI_Req_2,
 ***************************************************************************************************/
//DATAGRAM_ID_224
uint8_t GetUIRequest_CarCall_R_COPB(void)
{
   return shared_data[DATAGRAM_ID_224].data.data.auc8[0];
}
