
#include "shared_data_exp.h"

/***************************************************************************************************
    DG_EXP__LocalInputs1,
 ***************************************************************************************************/
//DATAGRAM_ID_29
uint8_t GetExpansionInputs1_LocalInputs1()
{
   return shared_data[DATAGRAM_ID_29].data.data.auc8[0];
}
uint8_t GetExpansionInputs1_LocalInputs2()
{
   return shared_data[DATAGRAM_ID_29].data.data.auc8[1];
}
uint8_t GetExpansionInputs1_LocalInputs3()
{
   return shared_data[DATAGRAM_ID_29].data.data.auc8[2];
}
uint8_t GetExpansionInputs1_LocalInputs4()
{
   return shared_data[DATAGRAM_ID_29].data.data.auc8[3];
}
uint8_t GetExpansionInputs1_LocalInputs5()
{
   return shared_data[DATAGRAM_ID_29].data.data.auc8[4];
}
uint8_t GetExpansionInputs1_LocalInputs6()
{
   return shared_data[DATAGRAM_ID_29].data.data.auc8[5];
}
uint8_t GetExpansionInputs1_LocalInputs7()
{
   return shared_data[DATAGRAM_ID_29].data.data.auc8[6];
}
uint8_t GetExpansionInputs1_LocalInputs8()
{
   return shared_data[DATAGRAM_ID_29].data.data.auc8[7];
}
/***************************************************************************************************
    DG_EXP__LocalInputs2,
 ***************************************************************************************************/
//DATAGRAM_ID_30
uint8_t GetExpansionInputs2_LocalInputs1()
{
   return shared_data[DATAGRAM_ID_30].data.data.auc8[0];
}
uint8_t GetExpansionInputs2_LocalInputs2()
{
   return shared_data[DATAGRAM_ID_30].data.data.auc8[1];
}
uint8_t GetExpansionInputs2_LocalInputs3()
{
   return shared_data[DATAGRAM_ID_30].data.data.auc8[2];
}
uint8_t GetExpansionInputs2_LocalInputs4()
{
   return shared_data[DATAGRAM_ID_30].data.data.auc8[3];
}
uint8_t GetExpansionInputs2_LocalInputs5()
{
   return shared_data[DATAGRAM_ID_30].data.data.auc8[4];
}
uint8_t GetExpansionInputs2_LocalInputs6()
{
   return shared_data[DATAGRAM_ID_30].data.data.auc8[5];
}
uint8_t GetExpansionInputs2_LocalInputs7()
{
   return shared_data[DATAGRAM_ID_30].data.data.auc8[6];
}
uint8_t GetExpansionInputs2_LocalInputs8()
{
   return shared_data[DATAGRAM_ID_30].data.data.auc8[7];
}

/***************************************************************************************************
    DG_EXP__LocalInputs3,
 ***************************************************************************************************/
//DATAGRAM_ID_31
uint8_t GetExpansionInputs3_LocalInputs1()
{
   return shared_data[DATAGRAM_ID_31].data.data.auc8[0];
}
uint8_t GetExpansionInputs3_LocalInputs2()
{
   return shared_data[DATAGRAM_ID_31].data.data.auc8[1];
}
uint8_t GetExpansionInputs3_LocalInputs3()
{
   return shared_data[DATAGRAM_ID_31].data.data.auc8[2];
}
uint8_t GetExpansionInputs3_LocalInputs4()
{
   return shared_data[DATAGRAM_ID_31].data.data.auc8[3];
}
uint8_t GetExpansionInputs3_LocalInputs5()
{
   return shared_data[DATAGRAM_ID_31].data.data.auc8[4];
}
uint8_t GetExpansionInputs3_LocalInputs6()
{
   return shared_data[DATAGRAM_ID_31].data.data.auc8[5];
}
uint8_t GetExpansionInputs3_LocalInputs7()
{
   return shared_data[DATAGRAM_ID_31].data.data.auc8[6];
}
uint8_t GetExpansionInputs3_LocalInputs8()
{
   return shared_data[DATAGRAM_ID_31].data.data.auc8[7];
}

/***************************************************************************************************
    DG_EXP__LocalInputs4,
 ***************************************************************************************************/
//DATAGRAM_ID_32
uint8_t GetExpansionInputs4_LocalInputs1()
{
   return shared_data[DATAGRAM_ID_32].data.data.auc8[0];
}
uint8_t GetExpansionInputs4_LocalInputs2()
{
   return shared_data[DATAGRAM_ID_32].data.data.auc8[1];
}
uint8_t GetExpansionInputs4_LocalInputs3()
{
   return shared_data[DATAGRAM_ID_32].data.data.auc8[2];
}
uint8_t GetExpansionInputs4_LocalInputs4()
{
   return shared_data[DATAGRAM_ID_32].data.data.auc8[3];
}
uint8_t GetExpansionInputs4_LocalInputs5()
{
   return shared_data[DATAGRAM_ID_32].data.data.auc8[4];
}
uint8_t GetExpansionInputs4_LocalInputs6()
{
   return shared_data[DATAGRAM_ID_32].data.data.auc8[5];
}
uint8_t GetExpansionInputs4_LocalInputs7()
{
   return shared_data[DATAGRAM_ID_32].data.data.auc8[6];
}
uint8_t GetExpansionInputs4_LocalInputs8()
{
   return shared_data[DATAGRAM_ID_32].data.data.auc8[7];
}
/***************************************************************************************************
    DG_EXP__LocalInputs5,
 ***************************************************************************************************/
//DATAGRAM_ID_33
uint8_t GetExpansionInputs5_LocalInputs1()
{
   return shared_data[DATAGRAM_ID_33].data.data.auc8[0];
}
uint8_t GetExpansionInputs5_LocalInputs2()
{
   return shared_data[DATAGRAM_ID_33].data.data.auc8[1];
}
uint8_t GetExpansionInputs5_LocalInputs3()
{
   return shared_data[DATAGRAM_ID_33].data.data.auc8[2];
}
uint8_t GetExpansionInputs5_LocalInputs4()
{
   return shared_data[DATAGRAM_ID_33].data.data.auc8[3];
}
uint8_t GetExpansionInputs5_LocalInputs5()
{
   return shared_data[DATAGRAM_ID_33].data.data.auc8[4];
}
uint8_t GetExpansionInputs5_LocalInputs6()
{
   return shared_data[DATAGRAM_ID_33].data.data.auc8[5];
}
uint8_t GetExpansionInputs5_LocalInputs7()
{
   return shared_data[DATAGRAM_ID_33].data.data.auc8[6];
}
uint8_t GetExpansionInputs5_LocalInputs8()
{
   return shared_data[DATAGRAM_ID_33].data.data.auc8[7];
}
/***************************************************************************************************
    DG_EXP__LocalInputs6,
 ***************************************************************************************************/
//DATAGRAM_ID_34
uint8_t GetExpansionInputs6_LocalInputs1()
{
   return shared_data[DATAGRAM_ID_34].data.data.auc8[0];
}
uint8_t GetExpansionInputs6_LocalInputs2()
{
   return shared_data[DATAGRAM_ID_34].data.data.auc8[1];
}
uint8_t GetExpansionInputs6_LocalInputs3()
{
   return shared_data[DATAGRAM_ID_34].data.data.auc8[2];
}
uint8_t GetExpansionInputs6_LocalInputs4()
{
   return shared_data[DATAGRAM_ID_34].data.data.auc8[3];
}
uint8_t GetExpansionInputs6_LocalInputs5()
{
   return shared_data[DATAGRAM_ID_34].data.data.auc8[4];
}
uint8_t GetExpansionInputs6_LocalInputs6()
{
   return shared_data[DATAGRAM_ID_34].data.data.auc8[5];
}
uint8_t GetExpansionInputs6_LocalInputs7()
{
   return shared_data[DATAGRAM_ID_34].data.data.auc8[6];
}
uint8_t GetExpansionInputs6_LocalInputs8()
{
   return shared_data[DATAGRAM_ID_34].data.data.auc8[7];
}
/***************************************************************************************************
    DG_EXP__LocalInputs7,
 ***************************************************************************************************/
//DATAGRAM_ID_35
uint8_t GetExpansionInputs7_LocalInputs1()
{
   return shared_data[DATAGRAM_ID_35].data.data.auc8[0];
}
uint8_t GetExpansionInputs7_LocalInputs2()
{
   return shared_data[DATAGRAM_ID_35].data.data.auc8[1];
}
uint8_t GetExpansionInputs7_LocalInputs3()
{
   return shared_data[DATAGRAM_ID_35].data.data.auc8[2];
}
uint8_t GetExpansionInputs7_LocalInputs4()
{
   return shared_data[DATAGRAM_ID_35].data.data.auc8[3];
}
uint8_t GetExpansionInputs7_LocalInputs5()
{
   return shared_data[DATAGRAM_ID_35].data.data.auc8[4];
}
uint8_t GetExpansionInputs7_LocalInputs6()
{
   return shared_data[DATAGRAM_ID_35].data.data.auc8[5];
}
uint8_t GetExpansionInputs7_LocalInputs7()
{
   return shared_data[DATAGRAM_ID_35].data.data.auc8[6];
}
uint8_t GetExpansionInputs7_LocalInputs8()
{
   return shared_data[DATAGRAM_ID_35].data.data.auc8[7];
}

/***************************************************************************************************
    DG_EXP__DipComStatus1,
 ***************************************************************************************************/
//DATAGRAM_ID_198
uint8_t GetExpansionDipBank1()
{
   return shared_data[DATAGRAM_ID_198].data.data.auc8[0];
}
uint8_t GetExpansionDipFault1()
{
   return shared_data[DATAGRAM_ID_198].data.data.auc8[1];
}
uint8_t GetExpansionComFault1()
{
   return shared_data[DATAGRAM_ID_198].data.data.auc8[2];
}

/***************************************************************************************************
    DG_EXP__DipComStatus2,
 ***************************************************************************************************/
//DATAGRAM_ID_199
uint8_t GetExpansionDipBank2()
{
   return shared_data[DATAGRAM_ID_199].data.data.auc8[0];
}
uint8_t GetExpansionDipFault2()
{
   return shared_data[DATAGRAM_ID_199].data.data.auc8[1];
}
uint8_t GetExpansionComFault2()
{
   return shared_data[DATAGRAM_ID_199].data.data.auc8[2];
}
/***************************************************************************************************
    DG_EXP__DipComStatus3,
 ***************************************************************************************************/
//DATAGRAM_ID_200
uint8_t GetExpansionDipBank3()
{
   return shared_data[DATAGRAM_ID_200].data.data.auc8[0];
}
uint8_t GetExpansionDipFault3()
{
   return shared_data[DATAGRAM_ID_200].data.data.auc8[1];
}
uint8_t GetExpansionComFault3()
{
   return shared_data[DATAGRAM_ID_200].data.data.auc8[2];
}
/***************************************************************************************************
    DG_EXP__DipComStatus4,
 ***************************************************************************************************/
//DATAGRAM_ID_201
uint8_t GetExpansionDipBank4()
{
   return shared_data[DATAGRAM_ID_201].data.data.auc8[0];
}
uint8_t GetExpansionDipFault4()
{
   return shared_data[DATAGRAM_ID_201].data.data.auc8[1];
}
uint8_t GetExpansionComFault4()
{
   return shared_data[DATAGRAM_ID_201].data.data.auc8[2];
}
/***************************************************************************************************
    DG_EXP__DipComStatus5,
 ***************************************************************************************************/
//DATAGRAM_ID_202
uint8_t GetExpansionDipBank5()
{
   return shared_data[DATAGRAM_ID_202].data.data.auc8[0];
}
uint8_t GetExpansionDipFault5()
{
   return shared_data[DATAGRAM_ID_202].data.data.auc8[1];
}
uint8_t GetExpansionComFault5()
{
   return shared_data[DATAGRAM_ID_202].data.data.auc8[2];
}
/***************************************************************************************************
    DG_EXP__DipComStatus6,
 ***************************************************************************************************/
//DATAGRAM_ID_203
uint8_t GetExpansionDipBank6()
{
   return shared_data[DATAGRAM_ID_203].data.data.auc8[0];
}
uint8_t GetExpansionDipFault6()
{
   return shared_data[DATAGRAM_ID_203].data.data.auc8[1];
}
uint8_t GetExpansionComFault6()
{
   return shared_data[DATAGRAM_ID_203].data.data.auc8[2];
}
/***************************************************************************************************
    DG_EXP__DipComStatus7,
 ***************************************************************************************************/
//DATAGRAM_ID_204
uint8_t GetExpansionDipBank7()
{
   return shared_data[DATAGRAM_ID_204].data.data.auc8[0];
}
uint8_t GetExpansionDipFault7()
{
   return shared_data[DATAGRAM_ID_204].data.data.auc8[1];
}
uint8_t GetExpansionComFault7()
{
   return shared_data[DATAGRAM_ID_204].data.data.auc8[2];
}
/***************************************************************************************************
    DG_EXP__DipComStatus8,
 ***************************************************************************************************/
//DATAGRAM_ID_205
uint8_t GetExpansionDipBank8()
{
   return shared_data[DATAGRAM_ID_205].data.data.auc8[0];
}
uint8_t GetExpansionDipFault8()
{
   return shared_data[DATAGRAM_ID_205].data.data.auc8[1];
}
uint8_t GetExpansionComFault8()
{
   return shared_data[DATAGRAM_ID_205].data.data.auc8[2];
}


/***************************************************************************************************
    DG_EXP__DipComStatus9,
 ***************************************************************************************************/
//DATAGRAM_ID_206
uint8_t GetExpansionDipBank9()
{
   return shared_data[DATAGRAM_ID_206].data.data.auc8[0];
}
uint8_t GetExpansionDipFault9()
{
   return shared_data[DATAGRAM_ID_206].data.data.auc8[1];
}
uint8_t GetExpansionComFault9()
{
   return shared_data[DATAGRAM_ID_206].data.data.auc8[2];
}

/***************************************************************************************************
    DG_EXP__DipComStatus10,
 ***************************************************************************************************/
//DATAGRAM_ID_207
uint8_t GetExpansionDipBank10()
{
   return shared_data[DATAGRAM_ID_207].data.data.auc8[0];
}
uint8_t GetExpansionDipFault10()
{
   return shared_data[DATAGRAM_ID_207].data.data.auc8[1];
}
uint8_t GetExpansionComFault10()
{
   return shared_data[DATAGRAM_ID_207].data.data.auc8[2];
}
/***************************************************************************************************
    DG_EXP__DipComStatus11,
 ***************************************************************************************************/
//DATAGRAM_ID_208
uint8_t GetExpansionDipBank11()
{
   return shared_data[DATAGRAM_ID_208].data.data.auc8[0];
}
uint8_t GetExpansionDipFault11()
{
   return shared_data[DATAGRAM_ID_208].data.data.auc8[1];
}
uint8_t GetExpansionComFault11()
{
   return shared_data[DATAGRAM_ID_208].data.data.auc8[2];
}
/***************************************************************************************************
    DG_EXP__DipComStatus12,
 ***************************************************************************************************/
//DATAGRAM_ID_209
uint8_t GetExpansionDipBank12()
{
   return shared_data[DATAGRAM_ID_209].data.data.auc8[0];
}
uint8_t GetExpansionDipFault12()
{
   return shared_data[DATAGRAM_ID_209].data.data.auc8[1];
}
uint8_t GetExpansionComFault12()
{
   return shared_data[DATAGRAM_ID_209].data.data.auc8[2];
}
/***************************************************************************************************
    DG_EXP__DipComStatus13,
 ***************************************************************************************************/
//DATAGRAM_ID_210
uint8_t GetExpansionDipBank13()
{
   return shared_data[DATAGRAM_ID_210].data.data.auc8[0];
}
uint8_t GetExpansionDipFault13()
{
   return shared_data[DATAGRAM_ID_210].data.data.auc8[1];
}
uint8_t GetExpansionComFault13()
{
   return shared_data[DATAGRAM_ID_210].data.data.auc8[2];
}
/***************************************************************************************************
    DG_EXP__DipComStatus14,
 ***************************************************************************************************/
//DATAGRAM_ID_211
uint8_t GetExpansionDipBank14()
{
   return shared_data[DATAGRAM_ID_211].data.data.auc8[0];
}
uint8_t GetExpansionDipFault14()
{
   return shared_data[DATAGRAM_ID_211].data.data.auc8[1];
}
uint8_t GetExpansionComFault14()
{
   return shared_data[DATAGRAM_ID_211].data.data.auc8[2];
}
/***************************************************************************************************
    DG_EXP__DipComStatus15,
 ***************************************************************************************************/
//DATAGRAM_ID_212
uint8_t GetExpansionDipBank15()
{
   return shared_data[DATAGRAM_ID_212].data.data.auc8[0];
}
uint8_t GetExpansionDipFault15()
{
   return shared_data[DATAGRAM_ID_212].data.data.auc8[1];
}
uint8_t GetExpansionComFault15()
{
   return shared_data[DATAGRAM_ID_212].data.data.auc8[2];
}
/***************************************************************************************************
    DG_EXP__DipComStatus16,
 ***************************************************************************************************/
//DATAGRAM_ID_213
uint8_t GetExpansionDipBank16()
{
   return shared_data[DATAGRAM_ID_213].data.data.auc8[0];
}
uint8_t GetExpansionDipFault16()
{
   return shared_data[DATAGRAM_ID_213].data.data.auc8[1];
}
uint8_t GetExpansionComFault16()
{
   return shared_data[DATAGRAM_ID_213].data.data.auc8[2];
}
/***************************************************************************************************
    DG_EXP__LocalInputs1 to DG_EXP__LocalInputs5,
    DG_EXP__DipComStatus1 to DG_EXP__DipComStatus16,
    DG_MRA__LocalOutputs2 to DG_MRA__LocalOutputs6
 ***************************************************************************************************/
//DATAGRAM_ID_29 to DATAGRAM_ID_33
//DATAGRAM_ID_198 to DATAGRAM_ID_213
//DATAGRAM_ID_55 to DATAGRAM_ID_59
uint8_t GetExpansionInputs( uint8_t ucMasterIndex, uint8_t ucBoardIndex )
{
   uint8_t ucInputs = 0;
   if( ( ucMasterIndex < 7 )
    && ( ucBoardIndex < NUM_EXP_BOARDS_PER_MASTER ) )
   {
      ucInputs = shared_data[DATAGRAM_ID_29+ucMasterIndex].data.data.auc8[ucBoardIndex];
   }
   return ucInputs;
}
uint8_t GetExpansionOutputs( uint8_t ucMasterIndex, uint8_t ucBoardIndex )
{
   uint8_t ucOutputs = 0;
   if( ( ucMasterIndex < 7 )
    && ( ucBoardIndex < NUM_EXP_BOARDS_PER_MASTER ) )
   {
      ucOutputs = shared_data[DATAGRAM_ID_55+ucMasterIndex].data.data.auc8[ucBoardIndex];
   }
   return ucOutputs;
}
uint8_t GetExpansionOnlineFlag( uint8_t ucMasterIndex, uint8_t ucBoardIndex )
{
   uint8_t bOnline = 0;
   if( ( ucMasterIndex < 7 )
    && ( ucBoardIndex < NUM_EXP_BOARDS_PER_MASTER ) )
   {
      bOnline = Sys_Bit_Get( &shared_data[DATAGRAM_ID_198+ucMasterIndex].data.data.auc8[3], ucBoardIndex );
   }
   return bOnline;
}
uint8_t GetExpansionError( uint8_t ucMasterIndex, uint8_t ucBoardIndex )
{
   uint8_t ucError = 0;
   if( ( ucMasterIndex < 7 )
    && ( ucBoardIndex < NUM_EXP_BOARDS_PER_MASTER ) )
   {
      uint8_t ucByteIndex = 4 + ucBoardIndex/2;
      uint8_t ucShift = 4 * (ucBoardIndex%2);
      ucError = ( shared_data[DATAGRAM_ID_198+ucMasterIndex].data.data.auc8[ucByteIndex] >> ucShift ) & 0xF;
   }
   return ucError;
}
