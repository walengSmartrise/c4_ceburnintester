
#include "shared_data_copa.h"

/***************************************************************************************************
    DG_COPA__ParamSlaveRequest,
 ***************************************************************************************************/
//DATAGRAM_ID_153
uint8_t GetParamSlaveRequest_MessageType_COPA()
{
   return shared_data[DATAGRAM_ID_153].data.data.auc8[0];
}
uint8_t GetParamSlaveRequest_BlockIndex_COPA()
{
   return shared_data[DATAGRAM_ID_153].data.data.auc8[1];
}
uint8_t GetParamSlaveRequest_ChunkIndex_COPA()
{
   return shared_data[DATAGRAM_ID_153].data.data.auc8[2];
}
uint8_t GetParamSlaveRequest_NodeID_COPA()
{
   return shared_data[DATAGRAM_ID_153].data.data.auc8[3];
}

uint16_t GetParamSlaveRequest_ParameterIndex_COPA()
{
   return shared_data[DATAGRAM_ID_153].data.data.auw16[1];
}
uint32_t GetParamSlaveRequest_ParamValue_COPA()
{
   return shared_data[DATAGRAM_ID_153].data.data.aui32[1];
}
/***************************************************************************************************
    DG_COPA__Fault1,
 ***************************************************************************************************/
//DATAGRAM_ID_13
uint16_t GetFault1_Fault_COPA()
{
   return shared_data[DATAGRAM_ID_13].data.data.auw16[0];
}
uint32_t GetFault1_Timestamp_COPA()
{
   return shared_data[DATAGRAM_ID_13].data.data.aui32[1];
}

/***************************************************************************************************
    DG_COPA__Fault2,
 ***************************************************************************************************/
//DATAGRAM_ID_14
uint16_t GetFault2_Fault_COPA()
{
   return shared_data[DATAGRAM_ID_14].data.data.auw16[0];
}
uint16_t GetFault2_Speed_COPA()
{
   return shared_data[DATAGRAM_ID_14].data.data.auw16[1];
}
uint32_t GetFault2_Position_COPA()
{
   return shared_data[DATAGRAM_ID_14].data.data.aui32[1];
}

/***************************************************************************************************
    DG_COPA__Fault3,
 ***************************************************************************************************/
//DATAGRAM_ID_75
uint16_t GetFault3_Fault_COPA(void)
{
   return shared_data[DATAGRAM_ID_75].data.data.auw16[0];
}
uint8_t GetFault3_CurrentFloor_COPA(void)
{
   return shared_data[DATAGRAM_ID_75].data.data.auc8[2];
}
uint8_t GetFault3_DestinationFloor_COPA(void)
{
   return shared_data[DATAGRAM_ID_75].data.data.auc8[3];
}
uint16_t GetFault3_CommandSpeed_COPA(void)
{
   return shared_data[DATAGRAM_ID_75].data.data.auw16[2];
}
uint16_t GetFault3_EncoderSpeed_COPA(void)
{
   return shared_data[DATAGRAM_ID_75].data.data.auw16[3];
}

/***************************************************************************************************
    DG_COPA__Alarm1,
 ***************************************************************************************************/
//DATAGRAM_ID_92
uint16_t GetAlarm1_Alarm_COPA()
{
   return shared_data[DATAGRAM_ID_92].data.data.auw16[0];
}
uint32_t GetAlarm1_Timestamp_COPA()
{
   return shared_data[DATAGRAM_ID_92].data.data.aui32[1];
}

/***************************************************************************************************
    DG_COPA__Alarm2,
 ***************************************************************************************************/
//DATAGRAM_ID_93
uint16_t GetAlarm2_Alarm_COPA()
{
   return shared_data[DATAGRAM_ID_93].data.data.auw16[0];
}
uint16_t GetAlarm2_Speed_COPA()
{
   return shared_data[DATAGRAM_ID_93].data.data.auw16[1];
}
uint32_t GetAlarm2_Position_COPA()
{
   return shared_data[DATAGRAM_ID_93].data.data.aui32[1];
}

/***************************************************************************************************
    DG_COPA__LocalInputs,
 ***************************************************************************************************/
//DATAGRAM_ID_27
uint32_t GetLocalInputs_Inputs_COPA()
{
   return shared_data[DATAGRAM_ID_27].data.data.aui32[0];
}
uint8_t GetPreflightState_COPA()
{
   return shared_data[DATAGRAM_ID_27].data.data.auc8[4];
}

/***************************************************************************************************
    DG_COPA__Debug,
 ***************************************************************************************************/
//DATAGRAM_ID_176
uint8_t GetDebugData_COPA_DL20_PhoneFailureFlag(void)
{
   return ( shared_data[DATAGRAM_ID_176].data.data.auc8[0] >> 0 ) & 1;
}
uint8_t GetDebugData_COPA_DL20_OOSFlag(void)
{
   return ( shared_data[DATAGRAM_ID_176].data.data.auc8[0] >> 1 ) & 1;
}
uint8_t GetDebugData_COPA_CAN_Utilization(void)
{
   return shared_data[DATAGRAM_ID_176].data.data.auc8[5];
}
uint16_t GetDebugData_COPA_NetworkError(void)
{
   return shared_data[DATAGRAM_ID_176].data.data.auw16[3];
}
uint32_t GetDebugData_COPA_Data(void)
{
   return shared_data[DATAGRAM_ID_176].data.data.aui32[1];
}

/***************************************************************************************************
    DG_COPA__CPLD_Data0,
 ***************************************************************************************************/
//DATAGRAM_ID_236
uint32_t GetCPLD_Data0_COPA(void)
{
   return shared_data[DATAGRAM_ID_236].data.data.aui32[0];
}
uint32_t GetCPLD_Data1_COPA(void)
{
   return shared_data[DATAGRAM_ID_236].data.data.aui32[1];
}
/***************************************************************************************************
    DG_COPA__CPLD_Data1,
 ***************************************************************************************************/
//DATAGRAM_ID_237
uint32_t GetCPLD_Data2_COPA(void)
{
   return shared_data[DATAGRAM_ID_237].data.data.aui32[0];
}
uint32_t GetCPLD_Data3_COPA(void)
{
   return shared_data[DATAGRAM_ID_237].data.data.aui32[1];
}
