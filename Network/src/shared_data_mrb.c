#include "shared_data_mrb.h"

/***************************************************************************************************
    DG_MRB__ParamSlaveRequest,
 ***************************************************************************************************/
//DATAGRAM_ID_150
uint8_t GetParamSlaveRequest_MessageType_MRB()
{
   return shared_data[DATAGRAM_ID_150].data.data.auc8[0];
}
uint8_t GetParamSlaveRequest_BlockIndex_MRB()
{
   return shared_data[DATAGRAM_ID_150].data.data.auc8[1];
}
uint8_t GetParamSlaveRequest_ChunkIndex_MRB()
{
   return shared_data[DATAGRAM_ID_150].data.data.auc8[2];
}
uint8_t GetParamSlaveRequest_NodeID_MRB()
{
   return shared_data[DATAGRAM_ID_150].data.data.auc8[3];
}

uint16_t GetParamSlaveRequest_ParameterIndex_MRB()
{
   return shared_data[DATAGRAM_ID_150].data.data.auw16[1];
}
uint32_t GetParamSlaveRequest_ParamValue_MRB()
{
   return shared_data[DATAGRAM_ID_150].data.data.aui32[1];
}
/***************************************************************************************************
    DG_MRB__Fault1,
 ***************************************************************************************************/
//DATAGRAM_ID_7
uint16_t GetFault1_Fault_MRB()
{
   return shared_data[DATAGRAM_ID_7].data.data.auw16[0];
}
uint32_t GetFault1_Timestamp_MRB()
{
   return shared_data[DATAGRAM_ID_7].data.data.aui32[1];
}

/***************************************************************************************************
    DG_MRB__Fault2,
 ***************************************************************************************************/
//DATAGRAM_ID_8
uint16_t GetFault2_Fault_MRB()
{
   return shared_data[DATAGRAM_ID_8].data.data.auw16[0];
}
uint16_t GetFault2_Speed_MRB()
{
   return shared_data[DATAGRAM_ID_8].data.data.auw16[1];
}
uint32_t GetFault2_Position_MRB()
{
   return shared_data[DATAGRAM_ID_8].data.data.aui32[1];
}

/***************************************************************************************************
    DG_MRB__Fault3,
 ***************************************************************************************************/
//DATAGRAM_ID_24
uint16_t GetFault3_Fault_MRB(void)
{
   return shared_data[DATAGRAM_ID_24].data.data.auw16[0];
}
uint8_t GetFault3_CurrentFloor_MRB(void)
{
   return shared_data[DATAGRAM_ID_24].data.data.auc8[2];
}
uint8_t GetFault3_DestinationFloor_MRB(void)
{
   return shared_data[DATAGRAM_ID_24].data.data.auc8[3];
}
uint16_t GetFault3_CommandSpeed_MRB(void)
{
   return shared_data[DATAGRAM_ID_24].data.data.auw16[2];
}
uint16_t GetFault3_EncoderSpeed_MRB(void)
{
   return shared_data[DATAGRAM_ID_24].data.data.auw16[3];
}

/***************************************************************************************************
    DG_MRB__Alarm1,
 ***************************************************************************************************/
//DATAGRAM_ID_86
uint16_t GetAlarm1_Alarm_MRB()
{
   return shared_data[DATAGRAM_ID_86].data.data.auw16[0];
}
uint32_t GetAlarm1_Timestamp_MRB()
{
   return shared_data[DATAGRAM_ID_86].data.data.aui32[1];
}

/***************************************************************************************************
    DG_MRB__Alarm2,
 ***************************************************************************************************/
//DATAGRAM_ID_87
uint16_t GetAlarm2_Alarm_MRB()
{
   return shared_data[DATAGRAM_ID_87].data.data.auw16[0];
}
uint16_t GetAlarm2_Speed_MRB()
{
   return shared_data[DATAGRAM_ID_87].data.data.auw16[1];
}
uint32_t GetAlarm2_Position_MRB()
{
   return shared_data[DATAGRAM_ID_87].data.data.aui32[1];
}

/***************************************************************************************************
    DG_MRB__UI_Req,
 ***************************************************************************************************/
//DATAGRAM_ID_155
uint8_t GetUIRequest_AcceptanceTest_MRB()
{
   return shared_data[DATAGRAM_ID_155].data.data.auc8[0];
}
uint8_t GetUIRequest_FaultLog_MRB()
{
   return shared_data[DATAGRAM_ID_155].data.data.auc8[1];
}
uint8_t GetUIRequest_DoorControl_MRB()
{
   return shared_data[DATAGRAM_ID_155].data.data.auc8[2];
}
uint8_t GetUIRequest_CarCall_F_MRB()
{
   return shared_data[DATAGRAM_ID_155].data.data.auc8[3];
}
//uint8_t GetUIRequest_ADA_Request_MRB()
//{
//   return shared_data[DATAGRAM_ID_155].data.data.auc8[4];
//}
uint8_t GetUIRequest_AcceptanceTestStatus_MRB()
{
   return shared_data[DATAGRAM_ID_155].data.data.auc8[6];
}
uint8_t GetUIRequest_DefaultCommand_MRB()
{
   return shared_data[DATAGRAM_ID_155].data.data.auc8[7];
}
uint8_t GetUIRequest_ViewDebugData_MRB(void)
{
   return shared_data[DATAGRAM_ID_155].data.data.auc8[5];
}

/***************************************************************************************************
    DG_MRB__GroupInputs,
 ***************************************************************************************************/
//DATAGRAM_ID_28
uint8_t GetGroupInputs_Riser1_MRB()
{
   return shared_data[DATAGRAM_ID_28].data.data.auc8[0];
}
uint8_t GetGroupInputs_Riser2_MRB()
{
   return shared_data[DATAGRAM_ID_28].data.data.auc8[1];
}
uint8_t GetGroupInputs_Riser3_MRB()
{
   return shared_data[DATAGRAM_ID_28].data.data.auc8[2];
}
uint8_t GetGroupInputs_Riser4_MRB()
{
   return shared_data[DATAGRAM_ID_28].data.data.auc8[3];
}
uint8_t Dispatch_GetEmergencyDispatchFlag_MRB(void)
{
   return shared_data[DATAGRAM_ID_28].data.data.auc8[4] & 1;
}
/***************************************************************************************************
    DG_MRB__HallCalls,
 ***************************************************************************************************/
//DATAGRAM_ID_74
uint8_t GetHallCallFrontUp_FloorPlus1()
{
   return shared_data[DATAGRAM_ID_74].data.data.auc8[0];
}
uint8_t GetHallCallRearUp_FloorPlus1()
{
   return shared_data[DATAGRAM_ID_74].data.data.auc8[1];
}
uint8_t GetHallCallFrontDn_FloorPlus1()
{
   return shared_data[DATAGRAM_ID_74].data.data.auc8[2];
}
uint8_t GetHallCallRearDn_FloorPlus1()
{
   return shared_data[DATAGRAM_ID_74].data.data.auc8[3];
}
uint8_t GetNearestHallCall_Up()
{
  return shared_data[DATAGRAM_ID_74].data.data.auc8[4];
}
uint8_t GetNearestHallCall_Down()
{
  return shared_data[DATAGRAM_ID_74].data.data.auc8[5];
}
uint8_t GetActiveMedicalCallFloor_MRB()
{
   return shared_data[DATAGRAM_ID_74].data.data.auc8[6];
}
uint8_t GetActiveSwingCallFlag_MRB()
{
   return ( shared_data[DATAGRAM_ID_74].data.data.auc8[7] >> 1 ) & 1;
}
uint8_t GetVIP_FloorPlus1_MRB(void)
{
   return shared_data[DATAGRAM_ID_74].data.data.auc8[7];
}

/***************************************************************************************************
    DG_MRB__DriveParameter, (<- Feedback via DG_MRA__DriveData)
 ***************************************************************************************************/
//DATAGRAM_ID_81
uint8_t GetDriveParameterCommand()
{
   return shared_data[DATAGRAM_ID_81].data.data.auc8[0];
}
uint16_t GetDriveParameterID()
{
   return shared_data[DATAGRAM_ID_81].data.data.auw16[1];
}
uint32_t GetDriveParameterValue()
{
   return shared_data[DATAGRAM_ID_81].data.data.aui32[1];
}

/***************************************************************************************************
    DG_MRB__SyncTime,
 ***************************************************************************************************/
//DATAGRAM_ID_168
uint32_t GetSyncTime_Time_MRB()
{
   return shared_data[DATAGRAM_ID_168].data.data.aui32[1];
}
/***************************************************************************************************
    DG_MRB__LW,
 ***************************************************************************************************/
//DATAGRAM_ID_191
int8_t GetLW_AverageTorquePercent_MRB(void)
{
   return shared_data[DATAGRAM_ID_191].data.data.auc8[0];
}
uint8_t GetLW_OfflineStatus_MRB(void)
{
   return shared_data[DATAGRAM_ID_191].data.data.auc8[1];
}
uint8_t GetLW_ReceiveCounter_MRB(void)
{
   return shared_data[DATAGRAM_ID_191].data.data.auc8[2];
}
int8_t GetLW_RawTorquePercent_MRB(void)
{
   return shared_data[DATAGRAM_ID_191].data.data.auc8[3];
}
uint8_t GetLW_GetError_MRB(void)
{
   return shared_data[DATAGRAM_ID_191].data.data.auc8[4];
}
uint8_t GetLW_GetInCarWeight_MRB(void)
{
   return shared_data[DATAGRAM_ID_191].data.data.auc8[5];
}
uint8_t GetLW_GetLoadFlag_MRB(void)
{
   return shared_data[DATAGRAM_ID_191].data.data.auc8[6];
}
uint8_t GetLW_GetCalibrationState_MRB(void)
{
   return shared_data[DATAGRAM_ID_191].data.data.auc8[7];
}

/***************************************************************************************************
    DG_MRB__RemoteCommands,
 ***************************************************************************************************/
//DATAGRAM_ID_216
uint8_t GetEmergencyPowerCommand()
{
   return shared_data[DATAGRAM_ID_216].data.data.auc8[0];
}
uint8_t GetDynamicParkingFloor_Plus1_MRB(void)
{
  return shared_data[DATAGRAM_ID_216].data.data.auc8[1] & 0x7F;
}
uint8_t GetDynamicParkingWithDoorsOpen_MRB(void)
{
  return ( shared_data[DATAGRAM_ID_216].data.data.auc8[1] >> 7 ) & 1;
}
uint8_t GetMasterDispatcherFlag_MRB(void)
{
  return shared_data[DATAGRAM_ID_216].data.data.auc8[2] & 1;
}
uint8_t GetRemoteData_Key(void)
{
  return shared_data[DATAGRAM_ID_216].data.data.auc8[3];
}
uint32_t GetRemoteData_Value(void)
{
   return shared_data[DATAGRAM_ID_216].data.data.aui32[1];
}

/***************************************************************************************************
    DG_MRB__HallStatus,
 ***************************************************************************************************/
//DATAGRAM_ID_195
uint16_t GetHallStatus_IndexPlus1(void)
{
   return shared_data[DATAGRAM_ID_195].data.data.auw16[0];
}
uint8_t GetHallStatus_IOBitmap(void)
{
  return shared_data[DATAGRAM_ID_195].data.data.auc8[2];
}
uint8_t GetHallStatus_Error(void)
{
  return shared_data[DATAGRAM_ID_195].data.data.auc8[3];
}
uint8_t GetHallStatus_Comm(void)
{
  return shared_data[DATAGRAM_ID_195].data.data.auc8[4];
}
uint8_t GetHallStatus_Source(void)
{
  return shared_data[DATAGRAM_ID_195].data.data.auc8[5];
}

/***************************************************************************************************
    DG_MRB__Debug,
 ***************************************************************************************************/
//DATAGRAM_ID_217
uint8_t GetDebugData_MRB_CAN_Utilization(void)
{
   return shared_data[DATAGRAM_ID_217].data.data.auc8[5];
}
uint16_t GetDebugData_MRB_NetworkError(void)
{
   return shared_data[DATAGRAM_ID_217].data.data.auw16[3];
}
uint32_t GetDebugData_MRB_Data(void)
{
   return shared_data[DATAGRAM_ID_217].data.data.aui32[1];
}

/***************************************************************************************************
    DG_MRB__UI_Req_2,
 ***************************************************************************************************/
//DATAGRAM_ID_222
uint8_t GetUIRequest_CarCall_R_MRB(void)
{
   return shared_data[DATAGRAM_ID_222].data.data.auc8[0];
}
uint16_t GetUIRequest_ADA_Request_MRB(void)
{
   return shared_data[DATAGRAM_ID_222].data.data.auw16[1];
}
/***************************************************************************************************
    DG_MRB__LW_2,
 ***************************************************************************************************/
//DATAGRAM_ID_225
uint8_t GetLW_UIReq_Request_MRB(void)
{
   return shared_data[DATAGRAM_ID_225].data.data.auc8[0];
}
uint8_t GetLW_UIReq_Command_MRB(void)
{
   return shared_data[DATAGRAM_ID_225].data.data.auc8[1];
}
float GetLW_UIReq_Data_MRB(void)
{
   return shared_data[DATAGRAM_ID_225].data.data.af32[1];
}
