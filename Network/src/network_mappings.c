/******************************************************************************
 *
 * @file     main.c
 * @brief    Program start point and main loop.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note    Mappings of local datagram IDs to global shared data datagram IDs
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "network2.h"
#include "datagrams.h"
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Shared Data -- MRA
 *----------------------------------------------------------------------------*/
static const Datagram_ID aucDatagramMap_MRA[NUM_MRA_DATAGRAMS] =
{
   DATAGRAM_ID_5,//DG_MRA__Fault1,
   DATAGRAM_ID_6,//DG_MRA__Fault2,
   DATAGRAM_ID_84, //DG_MRA__Alarm1
   DATAGRAM_ID_85, //DG_MRA__Alarm1
   DATAGRAM_ID_163,//DG_MRA__DebugStates
   DATAGRAM_ID_164,//DG_MRA__Debug

///*Operation*/
   DATAGRAM_ID_17,//DG_MRA__Operation1,
   DATAGRAM_ID_18,//DG_MRA__Operation2,
   DATAGRAM_ID_19,//DG_MRA__Operation3,
   DATAGRAM_ID_20,//DG_MRA__Operation4,
///*Car Calls*/
   DATAGRAM_ID_70, //DG_MRA__CarCalls1
   DATAGRAM_ID_71, //DG_MRA__CarCalls2
   DATAGRAM_ID_72, //DG_MRA__CarCalls3
///*Input Mapping*/
   DATAGRAM_ID_45, // DG_MRA__MappedInputs1
   DATAGRAM_ID_46, // DG_MRA__MappedInputs2
   DATAGRAM_ID_47, // DG_MRA__MappedInputs3
   DATAGRAM_ID_48, // DG_MRA__MappedInputs4
//
///*Output mapping*/
   DATAGRAM_ID_49, // DG_MRA__MappedOutputs1
   DATAGRAM_ID_50, // DG_MRA__MappedOutputs2
   DATAGRAM_ID_51, // DG_MRA__MappedOutputs3
   DATAGRAM_ID_52, // DG_MRA__MappedOutputs4
   DATAGRAM_ID_53, // DG_MRA__RiserBoardOutputs
   DATAGRAM_ID_54, // DG_MRA__LocalOutputs1
   DATAGRAM_ID_55, // DG_MRA__LocalOutputs2
   DATAGRAM_ID_56, // DG_MRA__LocalOutputs3
   DATAGRAM_ID_57, // DG_MRA__LocalOutputs4

   DATAGRAM_ID_181,//DG_MRA__ParamMaster_ParamValues2,
   DATAGRAM_ID_182,//DG_MRA__ParamMaster_ParamValues3,
   DATAGRAM_ID_183,//DG_MRA__ParamMaster_ParamValues4,
   DATAGRAM_ID_184,//DG_MRA__ParamMaster_ParamValues5,
   DATAGRAM_ID_185,//DG_MRA__ParamMaster_ParamValues6,
   DATAGRAM_ID_21,//DG_MRA__Motion,

   DATAGRAM_ID_161,//DG_MRA__LoggedFaults1,
   DATAGRAM_ID_162,//DG_MRA__LoggedFaults2,
   DATAGRAM_ID_186,//DG_MRA__UNUSED34
   DATAGRAM_ID_187,//DG_MRA__UNUSED35
   DATAGRAM_ID_188,//DG_MRA__Group_Data
   DATAGRAM_ID_189,//DG_MRA__Debug_PatternData
   DATAGRAM_ID_190,//DG_MRA__Run_Log
   DATAGRAM_ID_193,//DG_MRA__BrakeData,
   DATAGRAM_ID_194,//DG_MRA__EBrakeData,

   DATAGRAM_ID_3,//DG_MRA__CPLD_Fault1,
   DATAGRAM_ID_4,//DG_MRA__CPLD_Fault2,

   DATAGRAM_ID_96,//DG_MRA__ParamMaster_BlockCRCs,
   DATAGRAM_ID_97, //DG_MRA__ParamMaster_BlockCRCs2
   DATAGRAM_ID_98, //DG_MRA__ParamMaster_BlockCRCs3
   DATAGRAM_ID_99, //DG_MRA__ParamMaster_BlockCRCs4

   DATAGRAM_ID_100, //DG_MRA__ParamMaster_BlockCRCs5
   DATAGRAM_ID_101,
   DATAGRAM_ID_102,
   DATAGRAM_ID_103,
   DATAGRAM_ID_104,
   DATAGRAM_ID_105,
   DATAGRAM_ID_106,
   DATAGRAM_ID_107,
   DATAGRAM_ID_108,
   DATAGRAM_ID_109, //DG_MRA__ParamMaster_BlockCRCs14

   DATAGRAM_ID_110, //DG_MRA__ParamMaster_BlockCRCs15
   DATAGRAM_ID_111,
   DATAGRAM_ID_112,
   DATAGRAM_ID_113,
   DATAGRAM_ID_114,
   DATAGRAM_ID_115,
   DATAGRAM_ID_116,
   DATAGRAM_ID_117,
   DATAGRAM_ID_118,
   DATAGRAM_ID_119, //DG_MRA__ParamMaster_BlockCRCs24

   DATAGRAM_ID_120,//DG_MRA__ParamMaster_BlockCRCs25
   DATAGRAM_ID_121,
   DATAGRAM_ID_122,
   DATAGRAM_ID_123,
   DATAGRAM_ID_124,
   DATAGRAM_ID_125,
   DATAGRAM_ID_126,
   DATAGRAM_ID_127,
   DATAGRAM_ID_128,
   DATAGRAM_ID_129,//DG_MRA__ParamMaster_BlockCRCs34

   DATAGRAM_ID_130,//DG_MRA__ParamMaster_BlockCRCs35
   DATAGRAM_ID_131,
   DATAGRAM_ID_132,
   DATAGRAM_ID_133,
   DATAGRAM_ID_134,
   DATAGRAM_ID_135,
   DATAGRAM_ID_136,
   DATAGRAM_ID_137,
   DATAGRAM_ID_138,
   DATAGRAM_ID_139,//DG_MRA__ParamMaster_BlockCRCs44

   DATAGRAM_ID_140,//DG_MRA__ParamMaster_BlockCRCs45
   DATAGRAM_ID_141,
   DATAGRAM_ID_142,
   DATAGRAM_ID_143,
   DATAGRAM_ID_144, //DG_MRA__ParamMaster_BlockCRCs
   DATAGRAM_ID_145, //DG_MRA__ParamMaster_BlockCRCs
   DATAGRAM_ID_146, //DG_MRA__ParamMaster_BlockCRCs
   DATAGRAM_ID_147, //DG_MRA__ParamMaster_BlockCRCs
   DATAGRAM_ID_148, //DG_MRA__ParamMaster_BlockCRCs53
   DATAGRAM_ID_149, //DG_MRA__ParamMaster_BlockCRCs54

   DATAGRAM_ID_158,//DG_MRA__ParamMaster_BlockCRCs55
   DATAGRAM_ID_159,//DG_MRA__ParamMaster_BlockCRCs56
   DATAGRAM_ID_160,//DG_MRA__ParamMaster_BlockCRCs57,
   DATAGRAM_ID_180,//DG_MRA__ParamMaster_BlockCRCs58,
   DATAGRAM_ID_196,//DG_MRA__ParamMaster_BlockCRCs59,
   DATAGRAM_ID_197,//DG_MRA__ParamMaster_BlockCRCs60,
   DATAGRAM_ID_58,//DG_MRA__LocalOutputs5,
   DATAGRAM_ID_59,//DG_MRA__LocalOutputs6,

   DATAGRAM_ID_214, // DG_MRA__DefaultAll,
   DATAGRAM_ID_215, // DG_MRA__DriveData,
   DATAGRAM_ID_220, // DG_MRA__ParamSync_GUI,
   DATAGRAM_ID_227, // DG_MRA__LoggedFaults3,
   DATAGRAM_ID_22,  // DG_MRA__CPLD_Fault3,
   DATAGRAM_ID_23,  // DG_MRA__Fault3,

   DATAGRAM_ID_228,//DG_MRA__CarCallSecurity_F0
   DATAGRAM_ID_229,//DG_MRA__CarCallSecurity_F1
   DATAGRAM_ID_230,//DG_MRA__CarCallSecurity_R0
   DATAGRAM_ID_231,//DG_MRA__CarCallSecurity_R1

   DATAGRAM_ID_232,//DG_MRA__CPLD_Data0,
   DATAGRAM_ID_233,//DG_MRA__CPLD_Data1,

   DATAGRAM_ID_60,//DG_MRA__LocalOutputs7,
   DATAGRAM_ID_61,//DG_MRA__LocalOutputs8,

};

static const Datagram_ID aucDatagramMap_MRB[NUM_MRB_DATAGRAMS] =
{
   DATAGRAM_ID_150, //DG_MRB__ParamSlaveRequest
   DATAGRAM_ID_7,//DG_MRB__Fault1,
   DATAGRAM_ID_8,//DG_MRB__Fault2,
   DATAGRAM_ID_86, //DG_MRB__Alarm1
   DATAGRAM_ID_87, //DG_MRB__Alarm1
   DATAGRAM_ID_155,//DG_MRB__UI_Req
   DATAGRAM_ID_28, // DG_MRB__GroupInputs
   DATAGRAM_ID_74, //DG_MRB__HallCalls
   DATAGRAM_ID_81, //DG_MRB__DriveParameter
   DATAGRAM_ID_168,//DG_MRB__SyncTime
   DATAGRAM_ID_191,//DG_MRB__LW
   DATAGRAM_ID_216,//DG_MRB__RemoteCommands
   DATAGRAM_ID_195,//DG_MRB__HallStatus
   DATAGRAM_ID_217,//DG_MRB__Debug
   DATAGRAM_ID_222,//DG_MRB__UI_Req_2
   DATAGRAM_ID_225,//DG_MRB__LW_2
   DATAGRAM_ID_24, // DG_MRB__Fault3,
};

/*----------------------------------------------------------------------------
 * Shared Data -- CT
 *----------------------------------------------------------------------------*/
static const Datagram_ID aucDatagramMap_CTA[NUM_CTA_DATAGRAMS] =
{
   DATAGRAM_ID_151, //DG_CTA__ParamSlaveRequest
   DATAGRAM_ID_9,//DG_CTA__Fault1,
   DATAGRAM_ID_10,//DG_CTA__Fault2,
   DATAGRAM_ID_88, //DG_CTA__Alarm1
   DATAGRAM_ID_89, //DG_CTA__Alarm1
   DATAGRAM_ID_26, // DG_CTA__LocalInputs
   DATAGRAM_ID_170,//DG_CTA__Debug
   DATAGRAM_ID_2,//DG_CTA__SysPosition,
   DATAGRAM_ID_25, // DG_CTA__Fault3,
   DATAGRAM_ID_234,//DG_CTA__CPLD_Data0,
   DATAGRAM_ID_235,//DG_CTA__CPLD_Data1,
};

static const Datagram_ID aucDatagramMap_CTB[NUM_CTB_DATAGRAMS] =
{
   DATAGRAM_ID_152, //DG_CTB__ParamSlaveRequest
   DATAGRAM_ID_11,//DG_CTB__Fault1,
   DATAGRAM_ID_12,//DG_CTB__Fault2,
   DATAGRAM_ID_90, //DG_CTB__Alarm1
   DATAGRAM_ID_91, //DG_CTB__Alarm2
   DATAGRAM_ID_156,//DG_CTB__UI_Req
   DATAGRAM_ID_172,//DG_CTB__DebugStates
   DATAGRAM_ID_174,//DG_CTB__UNUSED7,
   DATAGRAM_ID_192,//DG_CTB__LW
   DATAGRAM_ID_218,//DG_CTB__Debug
   DATAGRAM_ID_223,//DG_CTB__UI_Req_2
   DATAGRAM_ID_226,//DG_CTB__LW_2
   DATAGRAM_ID_73, // DG_CTB__Fault3,
};
/*----------------------------------------------------------------------------
 * Shared Data -- COP
 *----------------------------------------------------------------------------*/
static const Datagram_ID aucDatagramMap_COPA[NUM_COPA_DATAGRAMS] =
{
   DATAGRAM_ID_153,//DG_COPA__ParamSlaveRequest,
   DATAGRAM_ID_13,//DG_COPA__Fault1,
   DATAGRAM_ID_14,//DG_COPA__Fault2,
   DATAGRAM_ID_92, //DG_COPA__Alarm1
   DATAGRAM_ID_93, //DG_COPA__Alarm2
   DATAGRAM_ID_27, // DG_COPA__LocalInputs
   DATAGRAM_ID_176,//DG_COPA__Debug,
   DATAGRAM_ID_75, // DG_COPA__Fault3,
   DATAGRAM_ID_236,//DG_COPA__CPLD_Data0,
   DATAGRAM_ID_237,//DG_COPA__CPLD_Data1,
};

static const Datagram_ID aucDatagramMap_COPB[NUM_COPB_DATAGRAMS] =
{
   DATAGRAM_ID_154,//DG_COPB__ParamSlaveRequest,
   DATAGRAM_ID_15,//DG_COPB__Fault1,
   DATAGRAM_ID_16,//DG_COPB__Fault2,
   DATAGRAM_ID_94,//DG_COPB__Alarm1,
   DATAGRAM_ID_95,//DG_COPB__Alarm2,
   DATAGRAM_ID_157,//DG_COPB__UI_Req
   DATAGRAM_ID_219,//DG_COPB__Debug
   DATAGRAM_ID_221,//DG_COPB__UNUSED7
   DATAGRAM_ID_224,//DG_COPB__UI_Req_2
   DATAGRAM_ID_76, // DG_COPB__Fault3,
};
/*----------------------------------------------------------------------------
 * Shared Data -- EXP
 *----------------------------------------------------------------------------*/
static const Datagram_ID aucDatagramMap_EXP[NUM_EXP_DATAGRAMS] =
{
      DATAGRAM_ID_29,//DG_EXP__LocalInputs1,
      DATAGRAM_ID_30,//DG_EXP__LocalInputs2,
      DATAGRAM_ID_31,//DG_EXP__LocalInputs3,
      DATAGRAM_ID_32,//DG_EXP__LocalInputs4,
      DATAGRAM_ID_33,//DG_EXP__LocalInputs5,
      DATAGRAM_ID_34,//DG_EXP__LocalInputs6,
      DATAGRAM_ID_35,//DG_EXP__LocalInputs7,
      DATAGRAM_ID_36,//DG_EXP__LocalInputs8,

      DATAGRAM_ID_37,//DG_EXP__LocalInputs9,
      DATAGRAM_ID_38,//DG_EXP__LocalInputs10,
      DATAGRAM_ID_39,//DG_EXP__LocalInputs11,
      DATAGRAM_ID_40,//DG_EXP__LocalInputs12,
      DATAGRAM_ID_41,//DG_EXP__LocalInputs13,
      DATAGRAM_ID_42,//DG_EXP__LocalInputs14,
      DATAGRAM_ID_43,//DG_EXP__LocalInputs15,
      DATAGRAM_ID_44,//DG_EXP__LocalInputs16,

	  DATAGRAM_ID_198, // DG_EXP__DipComStatus1
	  DATAGRAM_ID_199, // DG_EXP__DipComStatus2
	  DATAGRAM_ID_200, // DG_EXP__DipComStatus3
	  DATAGRAM_ID_201, // DG_EXP__DipComStatus4
	  DATAGRAM_ID_202, // DG_EXP__DipComStatus5
	  DATAGRAM_ID_203, // DG_EXP__DipComStatus6
	  DATAGRAM_ID_204, // DG_EXP__DipComStatus7
	  DATAGRAM_ID_205, // DG_EXP__DipComStatus8
};


/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   Retrieve network layer datagram ID from local datagram ID
 *----------------------------------------------------------------------------*/
Datagram_ID GetNetworkDatagramID_AnyBoard( uint8_t ucLocalDatagramID )
{
   Datagram_ID eReturn = 255;
   Sys_Nodes eNodeID = GetSystemNodeID();
   switch(eNodeID)
   {
      case SYS_NODE__MRA:
         eReturn = GetNetworkDatagramID_MRA(ucLocalDatagramID);
         break;
      case SYS_NODE__MRB:
         eReturn = GetNetworkDatagramID_MRB(ucLocalDatagramID);
         break;
      case SYS_NODE__CTA:
         eReturn = GetNetworkDatagramID_CTA(ucLocalDatagramID);
         break;
      case SYS_NODE__CTB:
         eReturn = GetNetworkDatagramID_CTB(ucLocalDatagramID);
         break;
      case SYS_NODE__COPA:
         eReturn = GetNetworkDatagramID_COPA(ucLocalDatagramID);
         break;
      case SYS_NODE__COPB:
         eReturn = GetNetworkDatagramID_COPB(ucLocalDatagramID);
         break;
      case SYS_NODE__EXP:
         eReturn = GetNetworkDatagramID_EXP(ucLocalDatagramID);
         break;
      default:
         break;
   }
   return eReturn;
}
Datagram_ID GetNetworkDatagramID_MRA( en_mra_datagrams eLocalID )
{
   Datagram_ID eID = NUM_DATAGRAM_IDs;
   if( eLocalID < NUM_MRA_DATAGRAMS )
   {
      eID = aucDatagramMap_MRA[eLocalID];
   }
   return eID;
}
Datagram_ID GetNetworkDatagramID_MRB( en_mrb_datagrams eLocalID )
{
   Datagram_ID eID = NUM_DATAGRAM_IDs;
   if( eLocalID < NUM_MRB_DATAGRAMS )
   {
      eID = aucDatagramMap_MRB[eLocalID];
   }
   return eID;
}
Datagram_ID GetNetworkDatagramID_CTA( en_cta_datagrams eLocalID )
{
   Datagram_ID eID = NUM_DATAGRAM_IDs;
   if( eLocalID < NUM_CTA_DATAGRAMS )
   {
      eID = aucDatagramMap_CTA[eLocalID];
   }
   return eID;
}
Datagram_ID GetNetworkDatagramID_CTB( en_ctb_datagrams eLocalID )
{
   Datagram_ID eID = NUM_DATAGRAM_IDs;
   if( eLocalID < NUM_CTB_DATAGRAMS )
   {
      eID = aucDatagramMap_CTB[eLocalID];
   }
   return eID;
}
Datagram_ID GetNetworkDatagramID_COPA( en_copa_datagrams eLocalID )
{
   Datagram_ID eID = NUM_DATAGRAM_IDs;
   if( eLocalID < NUM_COPA_DATAGRAMS )
   {
      eID = aucDatagramMap_COPA[eLocalID];
   }
   return eID;
}
Datagram_ID GetNetworkDatagramID_COPB( en_copb_datagrams eLocalID )
{
   Datagram_ID eID = NUM_DATAGRAM_IDs;
   if( eLocalID < NUM_COPB_DATAGRAMS )
   {
      eID = aucDatagramMap_COPB[eLocalID];
   }
   return eID;
}
Datagram_ID GetNetworkDatagramID_EXP( en_exp_datagrams eLocalID )
{
   Datagram_ID eID = NUM_DATAGRAM_IDs;
   if( eLocalID < NUM_EXP_DATAGRAMS )
   {
      eID = aucDatagramMap_EXP[eLocalID];
   }
   return eID;
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

