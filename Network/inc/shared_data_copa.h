#ifndef _SHARED_DATA_COPA_H_
#define _SHARED_DATA_COPA_H_

#include "datagrams.h"
#include "sys.h"

/***************************************************************************************************
    DG_COPA__ParamSlaveRequest,
 ***************************************************************************************************/
//DATAGRAM_ID_153
uint8_t GetParamSlaveRequest_MessageType_COPA();
uint8_t GetParamSlaveRequest_BlockIndex_COPA();
uint8_t GetParamSlaveRequest_ChunkIndex_COPA();
uint8_t GetParamSlaveRequest_NodeID_COPA();
uint16_t GetParamSlaveRequest_ParameterIndex_COPA();
uint32_t GetParamSlaveRequest_ParamValue_COPA();

/***************************************************************************************************
    DG_COPA__Fault1,
 ***************************************************************************************************/
//DATAGRAM_ID_13
uint16_t GetFault1_Fault_COPA();
uint32_t GetFault1_Timestamp_COPA();

/***************************************************************************************************
    DG_COPA__Fault2,
 ***************************************************************************************************/
//DATAGRAM_ID_14
uint16_t GetFault2_Fault_COPA();
uint16_t GetFault2_Speed_COPA();
uint32_t GetFault2_Position_COPA();

/***************************************************************************************************
    DG_COPA__Fault3,
 ***************************************************************************************************/
//DATAGRAM_ID_75
uint16_t GetFault3_Fault_COPA(void);
uint8_t GetFault3_CurrentFloor_COPA(void);
uint8_t GetFault3_DestinationFloor_COPA(void);
uint16_t GetFault3_CommandSpeed_COPA(void);
uint16_t GetFault3_EncoderSpeed_COPA(void);

/***************************************************************************************************
    DG_COPA__Alarm1,
 ***************************************************************************************************/
//DATAGRAM_ID_92
uint16_t GetAlarm1_Alarm_COPA();
uint32_t GetAlarm1_Timestamp_COPA();

/***************************************************************************************************
    DG_COPA__Alarm2,
 ***************************************************************************************************/
//DATAGRAM_ID_93
uint16_t GetAlarm2_Alarm_COPA();
uint16_t GetAlarm2_Speed_COPA();
uint32_t GetAlarm2_Position_COPA();

/***************************************************************************************************
    DG_COPA__LocalInputs,
 ***************************************************************************************************/
//DATAGRAM_ID_27
uint32_t GetLocalInputs_Inputs_COPA();
uint8_t GetPreflightState_COPA();

/***************************************************************************************************
    DG_COPA__DebugStates,
 ***************************************************************************************************/
//DATAGRAM_ID_175

/***************************************************************************************************
    DG_COPA__Debug,
 ***************************************************************************************************/
//DATAGRAM_ID_176
uint8_t GetDebugData_COPA_DL20_PhoneFailureFlag(void);
uint8_t GetDebugData_COPA_DL20_OOSFlag(void);
uint8_t GetDebugData_COPA_CAN_Utilization(void);
uint16_t GetDebugData_COPA_NetworkError(void);
uint32_t GetDebugData_COPA_Data(void);

/***************************************************************************************************
    DG_COPA__CPLD_Data0,
 ***************************************************************************************************/
//DATAGRAM_ID_236
uint32_t GetCPLD_Data0_COPA(void);
uint32_t GetCPLD_Data1_COPA(void);
/***************************************************************************************************
    DG_COPA__CPLD_Data1,
 ***************************************************************************************************/
//DATAGRAM_ID_237
uint32_t GetCPLD_Data2_COPA(void);
uint32_t GetCPLD_Data3_COPA(void);

#endif
