#ifndef _SHARED_DATA_MRB_H_
#define _SHARED_DATA_MRB_H_

#include "datagrams.h"
#include "sys.h"

/***************************************************************************************************
    DG_MRB__ParamSlaveRequest,
 ***************************************************************************************************/
//DATAGRAM_ID_150
uint8_t GetParamSlaveRequest_MessageType_MRB();
uint8_t GetParamSlaveRequest_BlockIndex_MRB();
uint8_t GetParamSlaveRequest_ChunkIndex_MRB();
uint8_t GetParamSlaveRequest_NodeID_MRB();
uint16_t GetParamSlaveRequest_ParameterIndex_MRB();
uint32_t GetParamSlaveRequest_ParamValue_MRB();

/***************************************************************************************************
    DG_MRB__Fault1,
 ***************************************************************************************************/
//DATAGRAM_ID_7
uint16_t GetFault1_Fault_MRB();
uint32_t GetFault1_Timestamp_MRB();

/***************************************************************************************************
    DG_MRB__Fault2,
 ***************************************************************************************************/
//DATAGRAM_ID_8
uint16_t GetFault2_Fault_MRB();
uint16_t GetFault2_Speed_MRB();
uint32_t GetFault2_Position_MRB();

/***************************************************************************************************
    DG_MRB__Fault3,
 ***************************************************************************************************/
//DATAGRAM_ID_24
uint16_t GetFault3_Fault_MRB(void);
uint8_t GetFault3_CurrentFloor_MRB(void);
uint8_t GetFault3_DestinationFloor_MRB(void);
uint16_t GetFault3_CommandSpeed_MRB(void);
uint16_t GetFault3_EncoderSpeed_MRB(void);

/***************************************************************************************************
    DG_MRB__Alarm1,
 ***************************************************************************************************/
//DATAGRAM_ID_86
uint16_t GetAlarm1_Alarm_MRB();
uint32_t GetAlarm1_Timestamp_MRB();

/***************************************************************************************************
    DG_MRB__Alarm2,
 ***************************************************************************************************/
//DATAGRAM_ID_87
uint16_t GetAlarm2_Alarm_MRB();
uint16_t GetAlarm2_Speed_MRB();
uint32_t GetAlarm2_Position_MRB();

/***************************************************************************************************
    DG_MRB__UI_Req,
 ***************************************************************************************************/
//DATAGRAM_ID_155
uint8_t GetUIRequest_AcceptanceTest_MRB();
uint8_t GetUIRequest_FaultLog_MRB();
uint8_t GetUIRequest_DoorControl_MRB();
uint8_t GetUIRequest_CarCall_F_MRB();
//uint8_t GetUIRequest_ADA_Request_MRB();
uint8_t GetUIRequest_AcceptanceTestStatus_MRB();
uint8_t GetUIRequest_DefaultCommand_MRB();
uint8_t GetUIRequest_ViewDebugData_MRB(void);

/***************************************************************************************************
    DG_MRB__GroupInputs,
 ***************************************************************************************************/
//DATAGRAM_ID_28
uint8_t GetGroupInputs_Riser1_MRB();
uint8_t GetGroupInputs_Riser2_MRB();
uint8_t GetGroupInputs_Riser3_MRB();
uint8_t GetGroupInputs_Riser4_MRB();
uint8_t Dispatch_GetEmergencyDispatchFlag_MRB(void);

/***************************************************************************************************
    DG_MRB__HallCalls,
 ***************************************************************************************************/
//DATAGRAM_ID_74
uint8_t GetHallCallFrontUp_FloorPlus1();
uint8_t GetHallCallRearUp_FloorPlus1();
uint8_t GetHallCallFrontDn_FloorPlus1();
uint8_t GetHallCallRearDn_FloorPlus1();
uint8_t GetNearestHallCall_Up();
uint8_t GetNearestHallCall_Down();
uint8_t GetActiveMedicalCallFloor_MRB();
uint8_t GetActiveSwingCallFlag_MRB();
uint8_t GetVIP_FloorPlus1_MRB(void);

/***************************************************************************************************
    DG_MRB__DriveParameter,
 ***************************************************************************************************/
//DATAGRAM_ID_81
uint8_t GetDriveParameterCommand();
uint16_t GetDriveParameterID();
uint32_t GetDriveParameterValue();

/***************************************************************************************************
    DG_MRB__SyncTime,
 ***************************************************************************************************/
//DATAGRAM_ID_168
uint32_t GetSyncTime_Time_MRB();

/***************************************************************************************************
    DG_MRB__LW,
 ***************************************************************************************************/
//DATAGRAM_ID_191
int8_t GetLW_AverageTorquePercent_MRB(void);
uint8_t GetLW_OfflineStatus_MRB(void);
uint8_t GetLW_ReceiveCounter_MRB(void);
int8_t GetLW_RawTorquePercent_MRB(void);
uint8_t GetLW_GetError_MRB(void);
uint8_t GetLW_GetInCarWeight_MRB(void);
uint8_t GetLW_GetLoadFlag_MRB(void);
uint8_t GetLW_GetCalibrationState_MRB(void);

/***************************************************************************************************
    DG_MRB__RemoteCommands,
 ***************************************************************************************************/
//DATAGRAM_ID_216
uint8_t GetEmergencyPowerCommand();
uint8_t GetDynamicParkingFloor_Plus1_MRB(void);
uint8_t GetDynamicParkingWithDoorsOpen_MRB(void);
uint8_t GetMasterDispatcherFlag_MRB(void);
uint8_t GetRemoteData_Key(void);
uint32_t GetRemoteData_Value(void);

/***************************************************************************************************
    DG_MRB__HallStatus,
 ***************************************************************************************************/
//DATAGRAM_ID_195
uint16_t GetHallStatus_IndexPlus1(void);
uint8_t GetHallStatus_IOBitmap(void);
uint8_t GetHallStatus_Error(void);
uint8_t GetHallStatus_Comm(void);
uint8_t GetHallStatus_Source(void);

/***************************************************************************************************
    DG_MRB__Debug,
 ***************************************************************************************************/
//DATAGRAM_ID_217
uint8_t GetDebugData_MRB_CAN_Utilization(void);
uint16_t GetDebugData_MRB_NetworkError(void);
uint32_t GetDebugData_MRB_Data(void);

/***************************************************************************************************
    DG_MRB__UI_Req_2,
 ***************************************************************************************************/
//DATAGRAM_ID_222
uint8_t GetUIRequest_CarCall_R_MRB(void);
uint16_t GetUIRequest_ADA_Request_MRB(void);

/***************************************************************************************************
    DG_MRB__LW_2,
 ***************************************************************************************************/
//DATAGRAM_ID_225
uint8_t GetLW_UIReq_Request_MRB(void);
uint8_t GetLW_UIReq_Command_MRB(void);
float GetLW_UIReq_Data_MRB(void);

#endif

