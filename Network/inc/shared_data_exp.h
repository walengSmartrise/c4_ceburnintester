#ifndef _SHARED_DATA_EXP_H_
#define _SHARED_DATA_EXP_H_

#include "datagrams.h"
#include "sys.h"

/***************************************************************************************************
    DG_EXP__LocalInputs1,
 ***************************************************************************************************/
//DATAGRAM_ID_29
uint8_t GetExpansionInputs1_LocalInputs1();
uint8_t GetExpansionInputs1_LocalInputs2();
uint8_t GetExpansionInputs1_LocalInputs3();
uint8_t GetExpansionInputs1_LocalInputs4();
uint8_t GetExpansionInputs1_LocalInputs5();
uint8_t GetExpansionInputs1_LocalInputs6();
uint8_t GetExpansionInputs1_LocalInputs7();
uint8_t GetExpansionInputs1_LocalInputs8();
/***************************************************************************************************
    DG_EXP__LocalInputs2,
 ***************************************************************************************************/
//DATAGRAM_ID_30
uint8_t GetExpansionInputs2_LocalInputs1();
uint8_t GetExpansionInputs2_LocalInputs2();
uint8_t GetExpansionInputs2_LocalInputs3();
uint8_t GetExpansionInputs2_LocalInputs4();
uint8_t GetExpansionInputs2_LocalInputs5();
uint8_t GetExpansionInputs2_LocalInputs6();
uint8_t GetExpansionInputs2_LocalInputs7();
uint8_t GetExpansionInputs2_LocalInputs8();

/***************************************************************************************************
    DG_EXP__LocalInputs3,
 ***************************************************************************************************/
//DATAGRAM_ID_31
uint8_t GetExpansionInputs3_LocalInputs1();
uint8_t GetExpansionInputs3_LocalInputs2();
uint8_t GetExpansionInputs3_LocalInputs3();
uint8_t GetExpansionInputs3_LocalInputs4();
uint8_t GetExpansionInputs3_LocalInputs5();
uint8_t GetExpansionInputs3_LocalInputs6();
uint8_t GetExpansionInputs3_LocalInputs7();
uint8_t GetExpansionInputs3_LocalInputs8();

/***************************************************************************************************
    DG_EXP__LocalInputs4,
 ***************************************************************************************************/
//DATAGRAM_ID_32
uint8_t GetExpansionInputs4_LocalInputs1();
uint8_t GetExpansionInputs4_LocalInputs2();
uint8_t GetExpansionInputs4_LocalInputs3();
uint8_t GetExpansionInputs4_LocalInputs4();
uint8_t GetExpansionInputs4_LocalInputs5();
uint8_t GetExpansionInputs4_LocalInputs6();
uint8_t GetExpansionInputs4_LocalInputs7();
uint8_t GetExpansionInputs4_LocalInputs8();
/***************************************************************************************************
    DG_EXP__LocalInputs5,
 ***************************************************************************************************/
//DATAGRAM_ID_33
uint8_t GetExpansionInputs5_LocalInputs1();
uint8_t GetExpansionInputs5_LocalInputs2();
uint8_t GetExpansionInputs5_LocalInputs3();
uint8_t GetExpansionInputs5_LocalInputs4();
uint8_t GetExpansionInputs5_LocalInputs5();
uint8_t GetExpansionInputs5_LocalInputs6();
uint8_t GetExpansionInputs5_LocalInputs7();
uint8_t GetExpansionInputs5_LocalInputs8();
/***************************************************************************************************
    DG_EXP__LocalInputs6,
 ***************************************************************************************************/
//DATAGRAM_ID_34
uint8_t GetExpansionInputs6_LocalInputs1();
uint8_t GetExpansionInputs6_LocalInputs2();
uint8_t GetExpansionInputs6_LocalInputs3();
uint8_t GetExpansionInputs6_LocalInputs4();
uint8_t GetExpansionInputs6_LocalInputs5();
uint8_t GetExpansionInputs6_LocalInputs6();
uint8_t GetExpansionInputs6_LocalInputs7();
uint8_t GetExpansionInputs6_LocalInputs8();
/***************************************************************************************************
    DG_EXP__LocalInputs7,
 ***************************************************************************************************/
//DATAGRAM_ID_35
uint8_t GetExpansionInputs7_LocalInputs1();
uint8_t GetExpansionInputs7_LocalInputs2();
uint8_t GetExpansionInputs7_LocalInputs3();
uint8_t GetExpansionInputs7_LocalInputs4();
uint8_t GetExpansionInputs7_LocalInputs5();
uint8_t GetExpansionInputs7_LocalInputs6();
uint8_t GetExpansionInputs7_LocalInputs7();
uint8_t GetExpansionInputs7_LocalInputs8();

/***************************************************************************************************
    DG_EXP__DipComStatus1,
 ***************************************************************************************************/
//DATAGRAM_ID_198
uint8_t GetExpansionDipBank1();
uint8_t GetExpansionDipFault1();
uint8_t GetExpansionComFault1();

/***************************************************************************************************
    DG_EXP__DipComStatus2,
 ***************************************************************************************************/
//DATAGRAM_ID_199
uint8_t GetExpansionDipBank2();
uint8_t GetExpansionDipFault2();
uint8_t GetExpansionComFault2();
/***************************************************************************************************
    DG_EXP__DipComStatus3,
 ***************************************************************************************************/
//DATAGRAM_ID_200
uint8_t GetExpansionDipBank3();
uint8_t GetExpansionDipFault3();
uint8_t GetExpansionComFault3();
/***************************************************************************************************
    DG_EXP__DipComStatus4,
 ***************************************************************************************************/
//DATAGRAM_ID_201
uint8_t GetExpansionDipBank4();
uint8_t GetExpansionDipFault4();
uint8_t GetExpansionComFault4();
/***************************************************************************************************
    DG_EXP__DipComStatus5,
 ***************************************************************************************************/
//DATAGRAM_ID_202
uint8_t GetExpansionDipBank5();
uint8_t GetExpansionDipFault5();
uint8_t GetExpansionComFault5();
/***************************************************************************************************
    DG_EXP__DipComStatus6,
 ***************************************************************************************************/
//DATAGRAM_ID_203
uint8_t GetExpansionDipBank6();
uint8_t GetExpansionDipFault6();
uint8_t GetExpansionComFault6();
/***************************************************************************************************
    DG_EXP__DipComStatus7,
 ***************************************************************************************************/
//DATAGRAM_ID_204
uint8_t GetExpansionDipBank7();
uint8_t GetExpansionDipFault7();
uint8_t GetExpansionComFault7();
/***************************************************************************************************
    DG_EXP__DipComStatus8,
 ***************************************************************************************************/
//DATAGRAM_ID_205
uint8_t GetExpansionDipBank8();
uint8_t GetExpansionDipFault8();
uint8_t GetExpansionComFault8();
/***************************************************************************************************
    DG_EXP__DipComStatus9,
 ***************************************************************************************************/
//DATAGRAM_ID_206
uint8_t GetExpansionDipBank9();
uint8_t GetExpansionDipFault9();
uint8_t GetExpansionComFault9();
/***************************************************************************************************
    DG_EXP__DipComStatus10,
 ***************************************************************************************************/
//DATAGRAM_ID_207
uint8_t GetExpansionDipBank10();
uint8_t GetExpansionDipFault10();
uint8_t GetExpansionComFault10();
/***************************************************************************************************
    DG_EXP__DipComStatus11,
 ***************************************************************************************************/
//DATAGRAM_ID_208
uint8_t GetExpansionDipBank11();
uint8_t GetExpansionDipFault11();
uint8_t GetExpansionComFault11();
/***************************************************************************************************
    DG_EXP__DipComStatus12,
 ***************************************************************************************************/
//DATAGRAM_ID_209
uint8_t GetExpansionDipBank12();
uint8_t GetExpansionDipFault12();
uint8_t GetExpansionComFault12();
/***************************************************************************************************
    DG_EXP__DipComStatus13,
 ***************************************************************************************************/
//DATAGRAM_ID_210
uint8_t GetExpansionDipBank13();
uint8_t GetExpansionDipFault13();
uint8_t GetExpansionComFault13();
/***************************************************************************************************
    DG_EXP__DipComStatus14,
 ***************************************************************************************************/
//DATAGRAM_ID_211
uint8_t GetExpansionDipBank14();
uint8_t GetExpansionDipFault14();
uint8_t GetExpansionComFault14();
/***************************************************************************************************
    DG_EXP__DipComStatus15,
 ***************************************************************************************************/
//DATAGRAM_ID_212
uint8_t GetExpansionDipBank15();
uint8_t GetExpansionDipFault15();
uint8_t GetExpansionComFault15();
/***************************************************************************************************
    DG_EXP__DipComStatus16,
 ***************************************************************************************************/
//DATAGRAM_ID_213
uint8_t GetExpansionDipBank16();
uint8_t GetExpansionDipFault16();
uint8_t GetExpansionComFault16();
/***************************************************************************************************
    DG_EXP__LocalInputs1 to DG_EXP__LocalInputs5,
    DG_EXP__DipComStatus1 to DG_EXP__DipComStatus16,
    DG_MRA__LocalOutputs2 to DG_MRA__LocalOutputs6
 ***************************************************************************************************/
//DATAGRAM_ID_29 to DATAGRAM_ID_33
//DATAGRAM_ID_198 to DATAGRAM_ID_213
//DATAGRAM_ID_55 to DATAGRAM_ID_59
uint8_t GetExpansionInputs( uint8_t ucMasterIndex, uint8_t ucBoardIndex );
uint8_t GetExpansionOutputs( uint8_t ucMasterIndex, uint8_t ucBoardIndex );
uint8_t GetExpansionOnlineFlag( uint8_t ucMasterIndex, uint8_t ucBoardIndex );
uint8_t GetExpansionError( uint8_t ucMasterIndex, uint8_t ucBoardIndex );
#endif
