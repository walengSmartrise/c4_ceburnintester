#ifndef _SHARED_DATA_CTA_H_
#define _SHARED_DATA_CTA_H_

#include "datagrams.h"
#include "sys.h"

/***************************************************************************************************
    DG_CTA__ParamSlaveRequest,
 ***************************************************************************************************/
//DATAGRAM_ID_151
uint8_t GetParamSlaveRequest_MessageType_CTA();
uint8_t GetParamSlaveRequest_BlockIndex_CTA();
uint8_t GetParamSlaveRequest_ChunkIndex_CTA();
uint8_t GetParamSlaveRequest_NodeID_CTA();
uint16_t GetParamSlaveRequest_ParameterIndex_CTA();
uint32_t GetParamSlaveRequest_ParamValue_CTA();

/***************************************************************************************************
    DG_CTA__Fault1,
 ***************************************************************************************************/
//DATAGRAM_ID_9
uint16_t GetFault1_Fault_CTA();
uint32_t GetFault1_Timestamp_CTA();

/***************************************************************************************************
    DG_CTA__Fault2,
 ***************************************************************************************************/
//DATAGRAM_ID_10
uint16_t GetFault2_Fault_CTA();
uint16_t GetFault2_Speed_CTA();
uint32_t GetFault2_Position_CTA();

/***************************************************************************************************
    DG_CTA__Fault3,
 ***************************************************************************************************/
//DATAGRAM_ID_25
uint16_t GetFault3_Fault_CTA(void);
uint8_t GetFault3_CurrentFloor_CTA(void);
uint8_t GetFault3_DestinationFloor_CTA(void);
uint16_t GetFault3_CommandSpeed_CTA(void);
uint16_t GetFault3_EncoderSpeed_CTA(void);

/***************************************************************************************************
    DG_CTA__Alarm1,
 ***************************************************************************************************/
//DATAGRAM_ID_88
uint16_t GetAlarm1_Alarm_CTA();
uint32_t GetAlarm1_Timestamp_CTA();

/***************************************************************************************************
    DG_CTA__Alarm2,
 ***************************************************************************************************/
//DATAGRAM_ID_89
uint16_t GetAlarm2_Alarm_CTA();
uint16_t GetAlarm2_Speed_CTA();
uint32_t GetAlarm2_Position_CTA();

/***************************************************************************************************
    DG_CTA__LocalInputs,
 ***************************************************************************************************/
//DATAGRAM_ID_26
uint32_t GetLocalInputs_Inputs_CTA();
uint8_t GetPreflightState_CTA();


/***************************************************************************************************
    DG_CTA__Debug,
 ***************************************************************************************************/
//DATAGRAM_ID_170
uint8_t GetDebugData_CTA_DL20_PhoneFailureFlag(void);
uint8_t GetDebugData_CTA_DL20_OOSFlag(void);
uint8_t GetDebugData_CTA_CAN_Utilization(void);
uint16_t GetDebugData_CTA_NetworkError(void);
uint32_t GetDebugData_CTA_Data(void);

/***************************************************************************************************
    DG_CTA__SysPosition,
 ***************************************************************************************************/
//DATAGRAM_ID_2
uint32_t GetSysPosition_Position_CTA();
int16_t GetSysPosition_Velocity_CTA();

/***************************************************************************************************
    DG_CTA__CPLD_Data0,
 ***************************************************************************************************/
//DATAGRAM_ID_234
uint32_t GetCPLD_Data0_CTA(void);
uint32_t GetCPLD_Data1_CTA(void);
/***************************************************************************************************
    DG_CTA__CPLD_Data1,
 ***************************************************************************************************/
//DATAGRAM_ID_235
uint32_t GetCPLD_Data2_CTA(void);
uint32_t GetCPLD_Data3_CTA(void);
#endif
