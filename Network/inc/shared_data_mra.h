#ifndef SHARED_DATA_MRA_H_
#define SHARED_DATA_MRA_H_

#include "datagrams.h"
#include "sys.h"

/***************************************************************************************************
    DG_MRA__ParamMaster_ParamValues1,
 ***************************************************************************************************/
//DATAGRAM_ID_180
//uint8_t GetParamMasterValues_MessageType_MRA();
//uint8_t GetParamMasterValues_BlockIndex_MRA();
//uint8_t GetParamMasterValues_ChunkIndex_MRA();
//uint32_t GetParamMasterValues_DataChunk_MRA();
/***************************************************************************************************
    DG_MRA__ParamMaster_ParamValues2,
 ***************************************************************************************************/
//DATAGRAM_ID_181
uint8_t GetParamMasterValues2_MessageType_MRA();
uint8_t GetParamMasterValues2_BlockIndex_MRA();
uint8_t GetParamMasterValues2_ChunkIndex_MRA();
uint32_t GetParamMasterValues2_DataChunk_MRA();
/***************************************************************************************************
    DG_MRA__ParamMaster_ParamValues3
 ***************************************************************************************************/
//DATAGRAM_ID_182
uint8_t GetParamMasterValues3_MessageType_MRA();
uint8_t GetParamMasterValues3_BlockIndex_MRA();
uint8_t GetParamMasterValues3_ChunkIndex_MRA();
uint32_t GetParamMasterValues3_DataChunk_MRA();
/***************************************************************************************************
    DG_MRA__ParamMaster_ParamValues4
 ***************************************************************************************************/
//DATAGRAM_ID_183
uint8_t GetParamMasterValues4_MessageType_MRA();
uint8_t GetParamMasterValues4_BlockIndex_MRA();
uint8_t GetParamMasterValues4_ChunkIndex_MRA();
uint32_t GetParamMasterValues4_DataChunk_MRA();
/***************************************************************************************************
    DG_MRA__ParamMaster_ParamValues5
 ***************************************************************************************************/
//DATAGRAM_ID_184
uint8_t GetParamMasterValues5_MessageType_MRA();
uint8_t GetParamMasterValues5_BlockIndex_MRA();
uint8_t GetParamMasterValues5_ChunkIndex_MRA();
uint32_t GetParamMasterValues5_DataChunk_MRA();
/***************************************************************************************************
    DG_MRA__ParamMaster_ParamValues6
 ***************************************************************************************************/
//DATAGRAM_ID_185
uint8_t GetParamMasterValues6_MessageType_MRA();
uint8_t GetParamMasterValues6_BlockIndex_MRA();
uint8_t GetParamMasterValues6_ChunkIndex_MRA();
uint32_t GetParamMasterValues6_DataChunk_MRA();
/***************************************************************************************************
    DG_MRA__ParamMaster_BlockCRCs,
 ***************************************************************************************************/
//DATAGRAM_ID_96
uint8_t GetParamMasterCRCs_MessageType_MRA( uint8_t ucBlockIndex );
uint8_t GetParamMasterCRCs_BlockIndex_MRA( uint8_t ucBlockIndex );
uint32_t GetParamMasterCRCs_CRC_MRA( uint8_t ucBlockIndex );
/***************************************************************************************************
    DG_MRA__Fault1,
 ***************************************************************************************************/
//DATAGRAM_ID_5
uint16_t GetFault1_Fault_MRA();
uint32_t GetFault1_Timestamp_MRA();

/***************************************************************************************************
    DG_MRA__Fault2,
 ***************************************************************************************************/
//DATAGRAM_ID_6
uint16_t GetFault2_Fault_MRA();
uint16_t GetFault2_Speed_MRA();
uint32_t GetFault2_Position_MRA();

/***************************************************************************************************
    DG_MRA__Fault3,
 ***************************************************************************************************/
//DATAGRAM_ID_23
uint16_t GetFault3_Fault_MRA(void);
uint8_t GetFault3_CurrentFloor_MRA(void);
uint8_t GetFault3_DestinationFloor_MRA(void);
uint16_t GetFault3_CommandSpeed_MRA(void);
uint16_t GetFault3_EncoderSpeed_MRA(void);

/***************************************************************************************************
    DG_MRA__Alarm1,
 ***************************************************************************************************/
//DATAGRAM_ID_84
uint16_t GetAlarm1_Alarm_MRA();
uint32_t GetAlarm1_Timestamp_MRA();

/***************************************************************************************************
    DG_MRA__Alarm2,
 ***************************************************************************************************/
//DATAGRAM_ID_85
uint16_t GetAlarm2_Alarm_MRA();
uint16_t GetAlarm2_Speed_MRA();
uint32_t GetAlarm2_Position_MRA();


/***************************************************************************************************
    DG_MRA__DefaultAll,
 ***************************************************************************************************/
//DATAGRAM_ID_214

uint32_t GetDefaultAllParameterIndex_MRA();
uint32_t GetDefaultAllState_MRA();

/***************************************************************************************************
    DG_MRA__DriveData, (-> Feedback for DG_MRB__DriveParameter)
 ***************************************************************************************************/
//DATAGRAM_ID_215
uint8_t GetDriveData_Command();
uint16_t GetDriveData_ParameterID();
uint32_t GetDriveData_ParameterValue();

/***************************************************************************************************
    DG_MRA__DebugStates,
 ***************************************************************************************************/
//DATAGRAM_ID_163
uint8_t GetDebugStates_OperAutoState_MRA();
uint8_t GetDebugStates_OperLearnState_MRA();
uint8_t GetDebugStates_RecallState_MRA();
uint8_t GetDebugStates_AutoCWState_MRA();
uint8_t GetDebugStates_FireServiceState_MRA();
uint8_t GetDebugStates_FireService2State_MRA();
uint8_t GetDebugStates_AcceptanceTestStatus_MRA();
uint8_t GetDebugStates_AcceptanceTestState_MRA();
/***************************************************************************************************
    DG_MRA__Debug,
 ***************************************************************************************************/
//DATAGRAM_ID_164
uint8_t GetDebug_MotionStartState_MRA();
uint8_t GetDebug_MotionStopState_MRA();
uint8_t GetDebug_MotionState_MRA();
uint8_t GetDebug_MotionPatternState_MRA();
uint8_t GetDebugData_MRA_CAN_Utilization();
int16_t GetDebug_DriveSpeedFeedback_MRA();
uint16_t GetDebugData_MRA_NetworkError();
uint32_t GetDebugData_MRA_Data();

/***************************************************************************************************
    DG_MRA__Operation1,
 ***************************************************************************************************/
//DATAGRAM_ID_17
uint8_t GetOperation1_CurrentFloor_MRA();
uint8_t GetOperation1_MotionCMD_MRA();
uint8_t GetOperation1_InDestinationDZ_MRA();
uint8_t GetOperation1_PreflightFlag_MRA();
uint32_t GetOperation1_RequestedDestination_MRA();
uint8_t GetOperation1_DestinationFloor_MRA();
/***************************************************************************************************
    DG_MRA__Operation2,
 ***************************************************************************************************/
//DATAGRAM_ID_18
uint8_t GetOperation2_ClassOfOperation_MRA( void );
uint8_t GetOperation2_AutoMode_MRA( void );
uint8_t GetOperation2_ManualMode_MRA( void );
uint8_t GetOperation2_LearnMode_MRA( void );
uint16_t GetOperation2_SpeedLimit_MRA( void );
uint16_t GetOperation2_EmergencyBitmap_MRA( void );

/***************************************************************************************************
    DG_MRA__Operation3,
 ***************************************************************************************************/
//DATAGRAM_ID_19
uint32_t GetOperation3_PositionLimitUp_MR();
uint8_t GetOperation3_BypassTermLimits_MR();
uint32_t GetOperation3_PositionLimitDown_MR();
uint8_t GetOperation3_CaptureMode_MR();
uint8_t GetPHE2InputProgrammed_F2();
uint8_t GetPHE2InputProgrammed_R2();
uint8_t GetSEInputProgrammed_F();
uint8_t GetSEInputProgrammed_R();

/***************************************************************************************************
    DG_MRA__Operation4,
 ***************************************************************************************************/
//DATAGRAM_ID_20
uint8_t GetOperation4_FrontDoorState( void );
uint8_t GetOperation4_RearDoorState( void );
uint8_t GetOperation4_BootloaderFlag( void );
uint8_t GetOperation4_ParamSync( void );
uint32_t GetOperation4_RunCounter( void );
/***************************************************************************************************
    DG_MRA__CarCalls1,
 ***************************************************************************************************/
//DATAGRAM_ID_70
uint32_t GetCarCalls1_Front_MRA( void );
uint32_t GetCarCalls1_Rear_MRA( void );

/***************************************************************************************************
    DG_MRA__CarCalls2,
 ***************************************************************************************************/
//DATAGRAM_ID_71
uint32_t GetCarCalls2_Front_MRA( void );
uint32_t GetCarCalls2_Rear_MRA( void );

/***************************************************************************************************
    DG_MRA__CarCalls3,
 ***************************************************************************************************/
//DATAGRAM_ID_72
uint32_t GetCarCalls3_Front_MRA( void );
uint32_t GetCarCalls3_Rear_MRA( void );

/***************************************************************************************************
    DG_MRA__MappedInputs1,
 ***************************************************************************************************/
//DATAGRAM_ID_45
uint32_t GetMappedInputs1_InputBitmap1_MRA( void );
uint32_t GetMappedInputs1_InputBitmap2_MRA( void );
/***************************************************************************************************
    DG_MRA__MappedInputs2,
 ***************************************************************************************************/
//DATAGRAM_ID_46
uint32_t GetMappedInputs2_InputBitmap3_MRA( void );
uint32_t GetMappedInputs2_InputBitmap4_MRA( void );
/***************************************************************************************************
    DG_MRA__MappedInputs3,
 ***************************************************************************************************/
//DATAGRAM_ID_47
uint32_t GetMappedInputs3_InputBitmap5_MRA( void );
uint32_t GetMappedInputs3_InputBitmap6_MRA( void );
/***************************************************************************************************
    DG_MRA__MappedInputs4,
 ***************************************************************************************************/
//DATAGRAM_ID_48
uint32_t GetMappedInputs4_InputBitmap7_MRA( void );
uint32_t GetMappedInputs4_InputBitmap8_MRA( void );

/***************************************************************************************************
    DG_MRA__MappedOutputs1,
 ***************************************************************************************************/
//DATAGRAM_ID_49
uint32_t GetMappedOutputs1_OutputBitmap1_MRA( void );
uint32_t GetMappedOutputs1_OutputBitmap2_MRA( void );

/***************************************************************************************************
    DG_MRA__MappedOutputs2,
 ***************************************************************************************************/
//DATAGRAM_ID_50
uint32_t GetMappedOutputs2_OutputBitmap3_MRA( void );
uint32_t GetMappedOutputs2_OutputBitmap4_MRA( void );

/***************************************************************************************************
    DG_MRA__MappedOutputs3,
 ***************************************************************************************************/
//DATAGRAM_ID_51
uint32_t GetMappedOutputs3_OutputBitmap5_MRA( void );
uint32_t GetMappedOutputs3_OutputBitmap6_MRA( void );

/***************************************************************************************************
    DG_MRA__MappedOutputs4,
 ***************************************************************************************************/
//DATAGRAM_ID_52
uint32_t GetMappedOutputs4_OutputBitmap7_MRA( void );
uint32_t GetMappedOutputs4_OutputBitmap8_MRA( void );

/***************************************************************************************************
    DG_MRA__RiserBoardOutputs,
 ***************************************************************************************************/
//DATAGRAM_ID_53
uint8_t GetRiserBoardOutputs_Output1_MRA( void );
uint8_t GetRiserBoardOutputs_Output2_MRA( void );
uint8_t GetRiserBoardOutputs_Output3_MRA( void );
uint8_t GetRiserBoardOutputs_Output4_MRA( void );
uint8_t GetRiserBoardOutputs_Output5_MRA( void );
uint8_t GetRiserBoardOutputs_Output6_MRA( void );
uint8_t GetRiserBoardOutputs_Output7_MRA( void );
uint8_t GetRiserBoardOutputs_Output8_MRA( void );

/***************************************************************************************************
    DG_MRA__LocalOutputs1,
 ***************************************************************************************************/
//DATAGRAM_ID_54
uint16_t GetLocalOutputs1_Cartop_MRA( void );
uint16_t GetLocalOutputs1_CarOperatingPanel1_MRA( void );
uint16_t GetLocalOutputs1_CarOperatingPanel2_MRA( void );
uint8_t GetLocalOutputs1_MachineRoom_MRA( void );
uint8_t GetLocalOutputs1_Riser1_MRA( void );

/***************************************************************************************************
    DG_MRA__LocalOutputs2,
 ***************************************************************************************************/
//DATAGRAM_ID_55
uint8_t GetLocalOutputs2_Output1_MRA( void );
uint8_t GetLocalOutputs2_Output2_MRA( void );
uint8_t GetLocalOutputs2_Output3_MRA( void );
uint8_t GetLocalOutputs2_Output4_MRA( void );
uint8_t GetLocalOutputs2_Output5_MRA( void );
uint8_t GetLocalOutputs2_Output6_MRA( void );
uint8_t GetLocalOutputs2_Output7_MRA( void );
uint8_t GetLocalOutputs2_Output8_MRA( void );

/***************************************************************************************************
    DG_MRA__LocalOutputs3,
 ***************************************************************************************************/
//DATAGRAM_ID_56
uint8_t GetLocalOutputs3_Output1_MRA( void );
uint8_t GetLocalOutputs3_Output2_MRA( void );
uint8_t GetLocalOutputs3_Output3_MRA( void );
uint8_t GetLocalOutputs3_Output4_MRA( void );
uint8_t GetLocalOutputs3_Output5_MRA( void );
uint8_t GetLocalOutputs3_Output6_MRA( void );
uint8_t GetLocalOutputs3_Output7_MRA( void );
uint8_t GetLocalOutputs3_Output8_MRA( void );

/***************************************************************************************************
    DG_MRA__LocalOutputs4,
 ***************************************************************************************************/
//DATAGRAM_ID_57
uint8_t GetLocalOutputs4_Output1_MRA( void );
uint8_t GetLocalOutputs4_Output2_MRA( void );
uint8_t GetLocalOutputs4_Output3_MRA( void );
uint8_t GetLocalOutputs4_Output4_MRA( void );
uint8_t GetLocalOutputs4_Output5_MRA( void );
uint8_t GetLocalOutputs4_Output6_MRA( void );
uint8_t GetLocalOutputs4_Output7_MRA( void );
uint8_t GetLocalOutputs4_Output8_MRA( void );
/***************************************************************************************************
    DG_MRA__Motion,
 ***************************************************************************************************/
//DATAGRAM_ID_21
uint8_t GetMotion_RunFlag_MRA();
int8_t GetMotion_Direction_MRA();
int16_t GetMotion_SpeedCMD_MRA();
uint32_t GetMotion_Destination_MRA();
uint8_t GetMotion_Profile_MRA();

/***************************************************************************************************
    DG_MRA__LoggedFaults1,
 ***************************************************************************************************/
//DATAGRAM_ID_161
uint8_t GetLoggedFaults1_RequestIndex_MRA();
uint16_t GetLoggedFaults1_FaultNumber_MRA();
uint32_t GetLoggedFaults1_Timestamp_MRA();

/***************************************************************************************************
    DG_MRA__LoggedFaults2,
 ***************************************************************************************************/
//DATAGRAM_ID_162
uint8_t GetLoggedFaults2_RequestIndex_MRA();
int16_t GetLoggedFaults2_Speed_MRA();
uint32_t GetLoggedFaults2_Position_MRA();
/***************************************************************************************************
    DG_MRA__LoggedFaults3,
 ***************************************************************************************************/
//DATAGRAM_ID_227
uint8_t GetLoggedFaults3_RequestIndex_MRA(void);
uint8_t GetLoggedFaults3_CurrentFloor_MRA(void);
uint8_t GetLoggedFaults3_DestinationFloor_MRA(void);
int16_t GetLoggedFaults3_CommandSpeed_MRA(void);
int16_t GetLoggedFaults3_EncoderSpeed_MRA(void);

/***************************************************************************************************
    DG_MRA__UNUSED34,
 ***************************************************************************************************/
//DATAGRAM_ID_186
/***************************************************************************************************
    DG_MRA__UNUSED35,
 ***************************************************************************************************/
// DATAGRAM_ID_187
/***************************************************************************************************
    DG_MRA__Group_Data,
 ***************************************************************************************************/
// DATAGRAM_ID_188
uint8_t GetGroup_DispatchData( void );
uint8_t GetGroup_DispatchNextFloor( void );
uint8_t GetGroup_EPower_CommandFeedback( void );
uint8_t GetGroup_EPower_CarRecalledFlag( void );
uint8_t GetGroup_ReadyToPark( void );
uint8_t GetGroup_VIP_CarReady( void );
uint8_t GetGroup_VIP_CarCapturing( void );
uint8_t GetGroup_VIP_State( void );

/***************************************************************************************************
    DG_MRA__Debug_PatternData,
 ***************************************************************************************************/
// DATAGRAM_ID_189
uint32_t GetDebugPatternData_RampUpDistance();
uint32_t GetDebugPatternData_SlowdownDistance();
uint8_t GetDebug_ActiveAcceptanceTest();
uint8_t GetDebug_GetMaxFloorToFloorTime_sec(void);

/***************************************************************************************************
    DG_MRA__Run_Log,
 ***************************************************************************************************/
// DATAGRAM_ID_190
uint32_t GetRunLog_StartCommand();
uint32_t GetRunLog_ExpectedSize();

int32_t GetRunLog_Timestamp();
uint8_t GetRunLog_TimestampIndex();

int16_t GetRunLog_CommandSpeed();
int16_t GetRunLog_CarSpeed();
uint32_t GetRunLog_CarPosition();
uint8_t GetRunLog_AlignmentByte();
void GetRunLog_Datagram(un_sdata_datagram *punDatagram );

/***************************************************************************************************
    DG_MRA__BrakeData,
 ***************************************************************************************************/
// DATAGRAM_ID_193
uint8_t GetBrakeData_State();
uint8_t GetBrakeData_Error();
uint8_t GetBrakeData_VoltageFeedback();
uint8_t GetBrakeData_BPS();
uint8_t GetBrakeData_BrakeVoltage();
/***************************************************************************************************
    DG_MRA__EBrakeData,
 ***************************************************************************************************/
// DATAGRAM_ID_194
uint8_t GetEBrakeData_State();
uint8_t GetEBrakeData_Error();
uint8_t GetEBrakeData_VoltageFeedback();
uint8_t GetEBrakeData_BPS();
uint8_t GetEBrakeData_BrakeVoltage();

/***************************************************************************************************
    DG_MRA__CPLD_Fault1,
 ***************************************************************************************************/
//DATAGRAM_ID_3
uint16_t GetCPLDFault1_Fault_MRA();
uint32_t GetCPLDFault1_Timestamp_MRA();

/***************************************************************************************************
    DG_MRA__CPLD_Fault2,
 ***************************************************************************************************/
//DATAGRAM_ID_4
uint16_t GetCPLDFault2_Fault_MRA();
uint16_t GetCPLDFault2_Speed_MRA();
uint32_t GetCPLDFault2_Position_MRA();

/***************************************************************************************************
    DG_MRA__CPLD_Fault3,
 ***************************************************************************************************/
//DATAGRAM_ID_22
uint16_t GetCPLDFault3_Fault_MRA(void);
uint8_t GetCPLDFault3_CurrentFloor_MRA(void);
uint8_t GetCPLDFault3_DestinationFloor_MRA(void);
uint16_t GetCPLDFault3_CommandSpeed_MRA(void);
uint16_t GetCPLDFault3_EncoderSpeed_MRA(void);

/***************************************************************************************************
    DG_MRA__LocalOutputs5,
 ***************************************************************************************************/
//DATAGRAM_ID_58
uint8_t GetLocalOutputs5_Output1_MRA( void );
uint8_t GetLocalOutputs5_Output2_MRA( void );
uint8_t GetLocalOutputs5_Output3_MRA( void );
uint8_t GetLocalOutputs5_Output4_MRA( void );
uint8_t GetLocalOutputs5_Output5_MRA( void );
uint8_t GetLocalOutputs5_Output6_MRA( void );
uint8_t GetLocalOutputs5_Output7_MRA( void );
uint8_t GetLocalOutputs5_Output8_MRA( void );

/***************************************************************************************************
    DG_MRA__LocalOutputs6,
 ***************************************************************************************************/
//DATAGRAM_ID_59
uint8_t GetLocalOutputs6_Output1_MRA( void );
uint8_t GetLocalOutputs6_Output2_MRA( void );
uint8_t GetLocalOutputs6_Output3_MRA( void );
uint8_t GetLocalOutputs6_Output4_MRA( void );
uint8_t GetLocalOutputs6_Output5_MRA( void );
uint8_t GetLocalOutputs6_Output6_MRA( void );
uint8_t GetLocalOutputs6_Output7_MRA( void );
uint8_t GetLocalOutputs6_Output8_MRA( void );

/***************************************************************************************************
    DG_MRA__CarCallSecurity_F0,
 ***************************************************************************************************/
//DATAGRAM_ID_228
uint32_t GetCarCallSecurity_F0(void);
uint32_t GetCarCallSecurity_F1(void);

/***************************************************************************************************
    DG_MRA__CarCallSecurity_F1,
 ***************************************************************************************************/
//DATAGRAM_ID_229
uint32_t GetCarCallSecurity_F2(void);
/***************************************************************************************************
    DG_MRA__CarCallSecurity_R0,
 ***************************************************************************************************/
//DATAGRAM_ID_230
uint32_t GetCarCallSecurity_R0(void);
uint32_t GetCarCallSecurity_R1(void);

/***************************************************************************************************
    DG_MRA__CarCallSecurity_R1,
 ***************************************************************************************************/
//DATAGRAM_ID_231
uint32_t GetCarCallSecurity_R2(void);

/***************************************************************************************************
    DG_MRA__CPLD_Data0,
 ***************************************************************************************************/
//DATAGRAM_ID_232
uint32_t GetCPLD_Data0_MRA(void);
uint32_t GetCPLD_Data1_MRA(void);

/***************************************************************************************************
    DG_MRA__CPLD_Data1,
 ***************************************************************************************************/
//DATAGRAM_ID_233
uint32_t GetCPLD_Data2_MRA(void);
uint32_t GetCPLD_Data3_MRA(void);

/***************************************************************************************************
    DG_MRA__LocalOutputs7,
 ***************************************************************************************************/
//DATAGRAM_ID_60 //Marshal Out 1-64 F
uint8_t GetLocalOutputs7_Output1_MRA( void );
uint8_t GetLocalOutputs7_Output2_MRA( void );
uint8_t GetLocalOutputs7_Output3_MRA( void );
uint8_t GetLocalOutputs7_Output4_MRA( void );
uint8_t GetLocalOutputs7_Output5_MRA( void );
uint8_t GetLocalOutputs7_Output6_MRA( void );
uint8_t GetLocalOutputs7_Output7_MRA( void );
uint8_t GetLocalOutputs7_Output8_MRA( void );

/***************************************************************************************************
    DG_MRA__LocalOutputs8,
 ***************************************************************************************************/
//DATAGRAM_ID_61 //Marshal Out 1-64 R
uint8_t GetLocalOutputs8_Output1_MRA( void );
uint8_t GetLocalOutputs8_Output2_MRA( void );
uint8_t GetLocalOutputs8_Output3_MRA( void );
uint8_t GetLocalOutputs8_Output4_MRA( void );
uint8_t GetLocalOutputs8_Output5_MRA( void );
uint8_t GetLocalOutputs8_Output6_MRA( void );
uint8_t GetLocalOutputs8_Output7_MRA( void );
uint8_t GetLocalOutputs8_Output8_MRA( void );


#endif
