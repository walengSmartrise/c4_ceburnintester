#ifndef _SHARED_DATA_COPB_H_
#define _SHARED_DATA_COPB_H_

#include "datagrams.h"
#include "sys.h"

/***************************************************************************************************
    DG_COPB__ParamSlaveRequest,
 ***************************************************************************************************/
//DATAGRAM_ID_154
uint8_t GetParamSlaveRequest_MessageType_COPB();
uint8_t GetParamSlaveRequest_BlockIndex_COPB();
uint8_t GetParamSlaveRequest_ChunkIndex_COPB();
uint8_t GetParamSlaveRequest_NodeID_COPB();
uint16_t GetParamSlaveRequest_ParameterIndex_COPB();
uint32_t GetParamSlaveRequest_ParamValue_COPB();

/***************************************************************************************************
    DG_COPB__Fault1,
 ***************************************************************************************************/
//DATAGRAM_ID_15
uint16_t GetFault1_Fault_COPB();
uint32_t GetFault1_Timestamp_COPB();

/***************************************************************************************************
    DG_COPB__Fault2,
 ***************************************************************************************************/
//DATAGRAM_ID_16
uint16_t GetFault2_Fault_COPB();
uint16_t GetFault2_Speed_COPB();
uint32_t GetFault2_Position_COPB();

/***************************************************************************************************
    DG_COPB__Fault3,
 ***************************************************************************************************/
//DATAGRAM_ID_76
uint16_t GetFault3_Fault_COPB(void);
uint8_t GetFault3_CurrentFloor_COPB(void);
uint8_t GetFault3_DestinationFloor_COPB(void);
uint16_t GetFault3_CommandSpeed_COPB(void);
uint16_t GetFault3_EncoderSpeed_COPB(void);

/***************************************************************************************************
    DG_COPB__Alarm1,
 ***************************************************************************************************/
//DATAGRAM_ID_94
uint16_t GetAlarm1_Alarm_COPB();
uint32_t GetAlarm1_Timestamp_COPB();

/***************************************************************************************************
    DG_COPB__Alarm2,
 ***************************************************************************************************/
//DATAGRAM_ID_95
uint16_t GetAlarm2_Alarm_COPB();
uint16_t GetAlarm2_Speed_COPB();
uint32_t GetAlarm2_Position_COPB();

/***************************************************************************************************
    DG_COPB__UI_Req,
 ***************************************************************************************************/
//DATAGRAM_ID_157
uint8_t GetUIRequest_AcceptanceTest_COPB();
uint8_t GetUIRequest_FaultLog_COPB();
uint8_t GetUIRequest_DoorControl_COPB();
uint8_t GetUIRequest_CarCall_F_COPB();
uint16_t GetUIRequest_HallStatusIndex_COPB();
uint8_t GetUIRequest_DefaultCommand_COPB();
uint8_t GetUIRequest_ViewDebugData_COPB(void);

/***************************************************************************************************
    DG_COPB__Debug,
 ***************************************************************************************************/
//DATAGRAM_ID_219
uint8_t GetDebugData_COPB_CAN_Utilization(void);
uint16_t GetDebugData_COPB_NetworkError(void);
uint32_t GetDebugData_COPB_Data(void);

/***************************************************************************************************
    DG_COPB__UNUSED7,
 ***************************************************************************************************/

/***************************************************************************************************
    DG_COPB__UI_Req_2,
 ***************************************************************************************************/
//DATAGRAM_ID_224
uint8_t GetUIRequest_CarCall_R_COPB(void);

#endif
