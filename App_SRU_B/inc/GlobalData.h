
#ifndef GLOBAL_DATA_H
#define GLOBAL_DATA_H

#include <stdint.h>
#include "sys.h"
#include "sru.h"
#include "sru_b.h"
#include "position.h"
#include "shared_data.h"
#include "fram_data.h"

#define LOWER_RELEVELING_THRESHHOLD (13)
#define UPPER_RELEVELING_THRESHHOLD (100)

uint8_t GetFP_FlashParamsReady();
void SetFP_FlashParamsReady();
uint8_t GetFP_NumFloors(void);
void SetFP_NumFloors(uint8_t ucFloors);
uint8_t GetFP_RearDoors(void);
void SetFP_RearDoors(uint8_t bDoor);
en_door_type GetFP_DoorType( enum en_doors eDoor );
void SetFP_DoorType( en_door_type enDoorType, enum en_doors eDoor );
uint8_t GetFP_FreightDoors(void);
void SetFP_FreightDoors(uint8_t bDoor);
uint16_t GetFP_ContractSpeed( void );
void SetFP_ContractSpeed( uint16_t uwSpeed );
void SetInputBitMap(uint32_t ulBitmap, uint8_t ucIndex);
void SetOutputBitMap(uint32_t ulBitmap, uint8_t ucIndex);
uint32_t GetInputBitMap(uint8_t ucIndex);
uint32_t GetOutputBitMap(uint8_t ucIndex);
uint8_t GetInputValue(enum en_input_functions enInput);
uint8_t GetOutputValue(enum en_output_functions enOutput);

uint32_t GetPositionReference(void);

void SetNTSPosition(uint32_t ulPosition);
uint32_t GetNTSPostion();
void SetNTSVelocity(int16_t wVelocity);
int16_t GetNTSVelocity();

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetUIRequest_DoorControl(enum en_door_ui_ctrl ucControl, en_sru_deployments eSRU_SourceDeployment);
enum en_door_ui_ctrl GetUIRequest_DoorControl();

void SetUIRequest_FaultLog(uint8_t bRequestLog, en_sru_deployments eSRU_SourceDeployment);
uint8_t GetUIRequest_FaultLog();

//Call Que
void InitCarCallQue ( void );
uint8_t SetCarCall (uint8_t ucFloorIndex, uint8_t ucDoorIndex);
uint8_t GetCarCallRequest ( enum en_doors eDoor );

void SetBitMap_LatchedCCB(uint32_t ulCarCalls, uint8_t ucIndex, uint8_t bFront);
uint32_t GetBitMap_LatchedCCB(uint8_t ucIndex, uint8_t bFront);
uint8_t GetLatchedCCB(uint8_t ucIndex, uint8_t bRear);


void SetStructPosition_NTS(struct st_position_b *pstPosition);
void GetStructPosition_NTS(struct st_position_b *pstPosition);


void SetArrivalUpDn(uint8_t ucArrivalUpDn);
uint8_t GetArrivalUpDn();

void SetModState_MotionState(uint8_t ucState );
uint8_t GetModState_MotionState();
void SetModState_MotionPattern(uint8_t ucMotionPattern );
uint8_t GetModState_MotionPattern();
void SetModState_MotionStop(uint8_t ucMotionStop );
uint8_t GetModState_MotionStop();
void SetModState_MotionStart(uint8_t ucMotionStart );
uint8_t GetModState_MotionStart();
void SetModState_AutoState(uint8_t ucState );
uint8_t GetModState_AutoState();
void SetModState_FloorLearnState(uint8_t ucState );
uint8_t GetModState_FloorLearnState();
void SetModState_FireSrv(uint8_t ucState );
uint8_t GetModState_FireSrv();
void SetModState_FireSrv2(uint8_t ucState );
uint8_t GetModState_FireSrv2();
void SetModState_Counterweight(uint8_t ucState );
uint8_t GetModState_Counterweight();
void SetModState_Recall(uint8_t ucState );
uint8_t GetModState_Recall();
/*-----------------------------------------------------------------------------
Doors
 -----------------------------------------------------------------------------*/
enum en_door_states GetDoorState_Front();
enum en_door_states GetDoorState_Rear();

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetAcceptanceTest_Plus1( uint32_t uiIndex_Plus1 );
uint32_t GetAcceptanceTest_Plus1();
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetFaultLogElement(st_fault_data *pstFault, uint8_t ucIndex_Plus1);
void GetFaultLogElement(st_fault_data *pstFault, uint8_t ucIndex_Plus1);
en_faults GetFaultLog_FaultNumber(uint8_t ucIndex_Plus1);
void SetAlarmLogElement(st_alarm_data *pstAlarm, uint8_t ucIndex_Plus1);
void GetAlarmLogElement(st_alarm_data *pstAlarm, uint8_t ucIndex_Plus1);
en_alarms GetAlarmLog_AlarmNumber(uint8_t ucIndex_Plus1);
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint16_t GetEmergencyBitmap();
uint8_t GetEmergencyBit( enum en_emergency_bitmap enEmergencyBF );

/*-----------------------------------------------------------------------------
   UI Req - Default CMD
 ----------------------------------------------------------------------------*/
void SetUIRequest_DefaultCommand( enum_default_cmd eCMD );
enum_default_cmd GetUIRequest_DefaultCommand();
/*-----------------------------------------------------------------------------
   Returns 1 if valid opening
 ----------------------------------------------------------------------------*/
uint8_t GetFloorOpening_Front( uint8_t ucFloor );
uint8_t GetFloorOpening_Rear( uint8_t ucFloor );

/*-----------------------------------------------------------------------------
   Create a localized version of the chime output to avoid missing the passing
   chime when running at high speeds
 -----------------------------------------------------------------------------*/
void UpdateLocalChimeSignal( uint16_t uwRunPeriod_ms );
uint8_t GetLocalChimeSignal(void);
/*----------------------------------------------------------------------------
   Flag signaling that a packet from an new style hall board has been received. Pre SR1060G

   This means the backwards compatible support code should be bypassed and the
   increased floor and hall board address limits should be used.
 *----------------------------------------------------------------------------*/
uint8_t GetHallBoard_MaxNumberOfFloors(void);
uint16_t GetHallBoard_MaxNumberOfBoards(void);
uint8_t GetHallBoard_NumberOfDIPs(void);
/*-----------------------------------------------------------------------------
   Requests for hall board, hall lantern or hall security status will
   be encoded in the same value.
   See en_hall_ui_req for the encoding.
 -----------------------------------------------------------------------------*/
void SetUIRequest_HallStatusIndex_Plus1( uint16_t uwIndex_Plus1 );
uint16_t GetUIRequest_HallStatusIndex_Plus1(void);
uint8_t GetHallStatusResponse_IO(void);
uint8_t GetHallStatusResponse_Error(void);
uint8_t GetHallStatusResponse_Comm(void);
uint8_t GetHallStatusResponse_Source(void);

/*----------------------------------------------------------------------------
For GetGroup_DispatchData()
   b0:    Any Door Open
   b1-b2: Priority_Plus1
   b3:    Hall Calls Disabled
   b4:    In Slowdown
   b5:    Suppress Reopen
   b6:    Car Out Of Group
   b7:    bIdleDirection
 *----------------------------------------------------------------------------*/
uint8_t HallCallsDisabled ( void );
uint8_t CheckIf_CarOutOfGroup ();
uint8_t GetCarPriority();
uint8_t CheckIf_IdleDirection(void);
uint8_t CheckIf_InSlowdown(void);
uint8_t CheckIf_SuppressReopen(void);

/*-----------------------------------------------------------------------------
   For spoofing displayed car mode as normal operation when car is allowed to run on e-power
 -----------------------------------------------------------------------------*/
uint8_t CheckIf_EPowerCarInNormal(void);

/*-----------------------------------------------------------------------------
   View Debug Data Requests
 ----------------------------------------------------------------------------*/
en_view_debug_data GetUIRequest_ViewDebugDataCommand(void);
/*-----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
uint8_t GetOverLoadFlag(void);
uint8_t GetFullLoadFlag(void);
uint8_t GetLightLoadFlag(void);
/*-----------------------------------------------------------------------------
   Check if car is inside virtual dead zone
 ----------------------------------------------------------------------------*/
uint8_t InsideDeadZone( void );
/*-----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
void UpdateBypassInCarStopSwitchFlag(void);
uint8_t GetBypassInCarStopSwitchFlag(void);
#endif // GLOBAL_DATA_H
