/******************************************************************************
 *
 * @file     ui.h
 * @brief    UI Header File
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef UI_H
#define UI_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "sru_b.h"
#include "buttons.h"
#include <stdint.h>
#include "lcd.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
enum en_number_decimal_formats
{
   enNumberDecimalPoints_None,
   enNumberDecimalPoints_One,
   enNumberDecimalPoints_Two,
   enNumberDecimalPoints_Three,
};

enum en_save_state
{
   SAVE_STATE__UNK,
   SAVE_STATE__SAVED,//aka idle
   SAVE_STATE__CLEARING,
   SAVE_STATE__SAVING,

   NUM_SAVE_STATE,
};
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
struct st_set_flag_menu
{
   uint8_t ucCursorColumn;

   char * psTitle;
   char * psActive;

   // Keep bit-fields together within 1-byte groups
   uint8_t bFlag:1;
   uint8_t bFinished:1;
   uint8_t bSaving:1;
   uint8_t bInvalid:1;
};
//-----------------------------------------------------------------------------

struct st_date_edit_menu
{
   char * psTitle;

   uint16_t ucYear;        
   uint8_t ucMonth;
   uint8_t ucDay;

   // Keep bit-fields together within 1-byte groups
   uint8_t bSaving:1;
   uint8_t bInvalid:1;
};

struct st_time_edit_menu
{
   char * psTitle;

   uint8_t ucHour;
   uint8_t ucMinutes;

   // Keep bit-fields together within 1-byte groups
   uint8_t bSaving:1;
   uint8_t bInvalid:1;
};

struct st_param_edit_menu
{
      char * ((*ucaPTRChoiceList)[]);
      char * psTitle;
      char * psUnit;

      uint32_t ulValue_Saved;
      uint32_t ulValue_Edited;
      uint32_t ulValue_Max; /* Sets parameter's max allowed value. Should be initialized if less than the max value for the selected paramter size  */
      uint16_t ulValue_Min;
      uint16_t uwParamIndex_Start;
      uint16_t uwParamIndex_End;
      uint16_t uwParamIndex_Current;
      uint8_t ucCursorColumn;
      uint8_t ucFieldIndex;
      uint8_t ucFieldSize_Index;
      uint8_t ucFieldSize_Value;
      uint8_t ucValueOffset; // Adds an integer offset to the displayed value

      enum en_param_block_types enParamType;
      enum en_number_decimal_formats  enDecimalFormat;

      // Keep bit-fields together within 1-byte groups
      uint8_t bSaving:1;
      uint8_t bInvalid:1;
      /* Doesn't work for 24-bit integer type
       * or enDecimalFormat != enNumberDecimalPoints_None */
      uint8_t bSignedInteger:1;
};

struct st_runtime_signal_edit_menu
{
      char * psTitle;
      char * pasValueStrings;

      uint32_t ulValue_Saved;
      uint32_t ulValue_Edited;
      uint32_t ulValue_Limit;
      uint8_t ucCursorColumn;
      uint8_t ucFieldIndex;
      uint8_t ucFieldSize_Index;
      uint8_t ucFieldSize_Value;

      uint32_t (*pfnGetSignal) (void);
      void (*pfnSetSignal) (uint32_t);

      uint8_t *pucSignal;
      uint8_t ucSignalSize;

      enum en_param_block_types enParamType;
      enum en_number_decimal_formats  enDecimalFormat;

      // Keep bit-fields together within 1-byte groups
      uint8_t bSaving:1;
      uint8_t bInvalid:1;
};

struct st_ui_parameter_edit
{
      uint32_t ulValue_Saved;
      uint32_t ulValue_Edited;
      uint32_t ulMask;

      uint16_t uwParamIndex_Current;
      uint16_t uwParamIndex_Start;
      uint16_t uwParamIndex_End;

      enum en_param_block_types enParamType;

      uint8_t ucCursorColumn;

      uint8_t ucFieldIndex;
      uint8_t ucFieldSize_Index;
      uint8_t ucFieldSize_Value;

      uint8_t ucShift; // if data is shifted into larger parameter size

      // Keep bit-fields together within 1-byte groups
      uint8_t bSaving:1;
      uint8_t bInvalid:1;
};

struct FloorIncrement
{
   uint8_t ucIncrement1: 4;
   uint8_t ucIncrement2: 4;
   uint8_t ucIncrement3: 4;
   uint8_t ucIncrement4: 4;
};

enum en_ui_screen_types
{
   enUI_STYPE__FREEFORM,
   enUI_STYPE__MENU,
   enUI_STYPE__PARAMETER,
};

enum en_parm_edit_format
{
   enEDIT_FORMAT__HEX,
   enEDIT_FORMAT__DEC,
   enEDIT_FORMAT__BIN,

   NUM_EDIT_FORMATS
};

enum en_parm_edit_fields
{
   enFIELD__NONE,  // this is the field when screen is first entered
   enFIELD__BOARD,
   enFIELD__PIN,
   enFIELD__BLOCK,
   enFIELD__INDEX,
   enFIELD__VALUE,
   enFIELD__SAVE,
   enFIELD__DATA_TYPE,//for debug memory
   enFIELD__MCU,
   enFIELD__IO_GROUP,
   enFIELD__IO_FUNCT,
   enFIELD__FLOOR,
   enFIELD__BIT_INDEX,
   NUM_PARM_EDIT_FIELDS,
};

enum en_field_action
{
   enACTION__NONE,

   enACTION__EDIT,
   enACTION__ENTER_FROM_RIGHT,
   enACTION__ENTER_FROM_LEFT,
   enACTION__EXIT_TO_LEFT,
   enACTION__EXIT_TO_RIGHT,

   NUM_FIELD_ACTIONS
};
enum en_setup_format
{
   enSETUP_FORMAT__GENERIC,
   enSETUP_FORMAT__PI_LABEL,
   enSETUP_FORMAT__SPEED,
   enSETUP_FORMAT__VOLTAGE,
   enSETUP_FORMAT__DELAY,
   enSETUP_FORMAT__NTS_TRIP,
   enSETUP_FORMAT__NTS_POS,
   enSETUP_FORMAT__NTS_VEL,
   enSETUP_FORMAT__LEARNED,
   enSETUP_FORMAT__CC_BL,
   enSETUP_FORMAT__IO,
   NUM_SETUP_FORMATS
};

//-----------------------------------------------------------------------------
struct st_ui_generic_screen
{
   struct st_ui_generic_screen *pstUGS_Previous;
   void *pvReference;

   const uint8_t ucType;
};

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
struct st_ui_screen__freeform
{
   void (*pfnDraw)( void );
};

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
struct st_ui_menu_item
{
   char *psTitle;
   struct st_ui_generic_screen *pstUGS_Next;
};

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
struct st_ui_screen__menu
{
   char *psTitle;

   struct st_ui_menu_item * (* pastMenuItems)[];

   uint8_t ucNumItems;
   uint8_t ucTopItem;
   uint8_t ucCurrentItem;
};

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
struct st_ui_control
{
   struct st_ui_generic_screen *pstUGS_Current;
};

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

extern struct st_ui_generic_screen gstUGS_HomeScreen;
extern struct st_ui_generic_screen gstUGS_MainMenu;
extern struct st_ui_generic_screen gstUGS_Menu_Status;
extern struct st_ui_generic_screen gstUGS_AboutScreen;
extern struct st_ui_generic_screen gstUGS_Menu_Inputs;
extern struct st_ui_generic_screen gstUGS_Menu_Outputs;
extern struct st_ui_generic_screen gstUGS_Menu_Faults;
extern struct st_ui_generic_screen gstUGS_Menu_Setup;
extern struct st_ui_generic_screen gstUGS_ActiveFaults;
extern struct st_ui_generic_screen gstUGS_PopUpFaults;
extern struct st_ui_generic_screen gstUGS_LoggedFaults;
extern struct st_ui_generic_screen gstUGS_Faults_ClearLog;

extern struct st_ui_generic_screen gstUGS_PopUpAlarms;
extern struct st_ui_generic_screen gstUGS_LoggedAlarms;
extern struct st_ui_generic_screen gstUGS_Alarms_ClearLog;

extern struct st_ui_generic_screen gstUGS_Menu_ParamEdit;
extern struct st_ui_generic_screen gstUGS_DecimalParams;
extern struct st_ui_generic_screen gstUGS_HexParams;

extern struct st_ui_generic_screen gstUGS_Setup_PI;
extern struct st_ui_generic_screen gstUGS_NTS_Pos1;
extern struct st_ui_generic_screen gstUGS_NTS_Pos3;
extern struct st_ui_generic_screen gstUGS_NTS_Pos4;
extern struct st_ui_generic_screen gstUGS_NTS_Vel1;
extern struct st_ui_generic_screen gstUGS_NTS_Vel3;
extern struct st_ui_generic_screen gstUGS_NTS_Vel4;

//gstUGS_NTS_Pos

extern struct st_ui_generic_screen gstUGS_Status_Inputs;
extern struct st_ui_generic_screen gstUGS_Status_Inputs_Functions;
extern struct st_ui_generic_screen gstUGS_Status_Inputs_Inspection;
extern struct st_ui_generic_screen gstUGS_Status_Inputs_Locks;
extern struct st_ui_generic_screen gstUGS_Status_Inputs_Hoistway;
extern struct st_ui_generic_screen gstUGS_Status_Inputs_Safety;
extern struct st_ui_generic_screen gstUGS_Status_Inputs_Relays;
extern struct st_ui_generic_screen gstUGS_Status_Inputs_Misc;
extern struct st_ui_generic_screen gstUGS_Status_Inputs_Doors;
extern struct st_ui_generic_screen gstUGS_Status_Inputs_Contactors;
extern struct st_ui_generic_screen gstUGS_Status_Inputs_Auto;
extern struct st_ui_generic_screen gstUGS_Status_Inputs_FireEQ;
extern struct st_ui_generic_screen gstUGS_Status_Inputs_EPower;
extern struct st_ui_generic_screen gstUGS_Status_Inputs_Unfixed;
extern struct st_ui_generic_screen gstUGS_Status_Inputs_Control;
extern struct st_ui_generic_screen gstUGS_Status_Inputs_SafetyOperations;

extern struct st_ui_generic_screen gstUGS_Status_Outputs_AutoOper;
extern struct st_ui_generic_screen gstUGS_Status_Outputs_Doors_F;
extern struct st_ui_generic_screen gstUGS_Status_Outputs_Doors_R;
extern struct st_ui_generic_screen gstUGS_Status_Outputs_FireEQ;
extern struct st_ui_generic_screen gstUGS_Status_Outputs_EPower;
extern struct st_ui_generic_screen gstUGS_Status_Outputs_Insp;
extern struct st_ui_generic_screen gstUGS_Status_Outputs_CTRL;
extern struct st_ui_generic_screen gstUGS_Status_Outputs_Contactors ;
extern struct st_ui_generic_screen gstUGS_Status_Outputs_Safety;
extern struct st_ui_generic_screen gstUGS_Status_Outputs_CCL_F;
extern struct st_ui_generic_screen gstUGS_Status_Outputs_CCL_R ;

extern struct st_ui_generic_screen gstUGS_Menu_Outputs;
extern struct st_ui_generic_screen gstUGS_Status_Outputs_Functions;

extern struct st_ui_generic_screen gstUGS_Setup_Inputs_Boards;
extern struct st_ui_generic_screen gstUGS_Setup_Outputs_Boards;
extern struct st_ui_generic_screen gstUGS_Setup_Inputs_MR;
extern struct st_ui_generic_screen gstUGS_Setup_Inputs_CT;
extern struct st_ui_generic_screen gstUGS_Setup_Inputs_COP;
extern struct st_ui_generic_screen gstUGS_Setup_Inputs_RIS;
extern struct st_ui_generic_screen gstUGS_Setup_Inputs_EXP1;
extern struct st_ui_generic_screen gstUGS_Setup_Inputs_EXP2;
extern struct st_ui_generic_screen gstUGS_Setup_Inputs_EXP3;
extern struct st_ui_generic_screen gstUGS_Setup_Inputs_EXP4;
extern struct st_ui_generic_screen gstUGS_Setup_Inputs_EXP5;
extern struct st_ui_generic_screen gstUGS_Setup_Outputs_MR;
extern struct st_ui_generic_screen gstUGS_Setup_Outputs_CT;
extern struct st_ui_generic_screen gstUGS_Setup_Outputs_COP;
extern struct st_ui_generic_screen gstUGS_Setup_Outputs_RIS;
extern struct st_ui_generic_screen gstUGS_Setup_Outputs_EXP1;
extern struct st_ui_generic_screen gstUGS_Setup_Outputs_EXP2;
extern struct st_ui_generic_screen gstUGS_Setup_Outputs_EXP3;
extern struct st_ui_generic_screen gstUGS_Setup_Outputs_EXP4;
extern struct st_ui_generic_screen gstUGS_Setup_Outputs_EXP5;

extern struct st_ui_generic_screen gstUGS_Menu_Setup_Speeds;
extern struct st_ui_generic_screen gstUGS_Setup_Preflight;
extern struct st_ui_generic_screen gstUGS_Speeds_Contract;
extern struct st_ui_generic_screen gstUGS_Speeds_Inspection;
extern struct st_ui_generic_screen gstUGS_Speeds_Learn;
extern struct st_ui_generic_screen gstUGS_Speeds_Terminal;
extern struct st_ui_generic_screen gstUGS_Speeds_Leveling;
extern struct st_ui_generic_screen gstUGS_Speeds_NTSD;
extern struct st_ui_generic_screen gstUGS_Speeds_MinAccel;

extern struct st_ui_generic_screen gstUGS_Menu_Setup_Misc;
extern struct st_ui_generic_screen gstUGS_Setup_NTS;
extern struct st_ui_generic_screen gstUGS_NTS_Pos;
extern struct st_ui_generic_screen gstUGS_NTS_Vel;

extern struct st_ui_generic_screen gstUGS_Misc_BypassTermLimits;
extern struct st_ui_generic_screen gstUGS_Floor_NumberOfFloors;
extern struct st_ui_generic_screen gstUGS_Misc_NumberOfDoors;

extern struct st_ui_generic_screen gstUGS_Menu_Setup_Brakes;
extern struct st_ui_generic_screen gstUGS_Menu_Setup_Brakes2;

extern struct st_ui_generic_screen gstUGS_Brakes_DelayPick;
extern struct st_ui_generic_screen gstUGS_Brakes_RampTime1;
extern struct st_ui_generic_screen gstUGS_Brakes_RampTime2;
extern struct st_ui_generic_screen gstUGS_Brakes_VoltagePick;
extern struct st_ui_generic_screen gstUGS_Brakes_VoltageHold;
extern struct st_ui_generic_screen gstUGS_Brakes_VoltageRelevel;
extern struct st_ui_generic_screen gstUGS_Brakes_BPS_NC;

extern struct st_ui_generic_screen gstUGS_Brakes2_DelayPick;
extern struct st_ui_generic_screen gstUGS_Brakes2_RampTime;
extern struct st_ui_generic_screen gstUGS_Brakes2_VoltagePick;
extern struct st_ui_generic_screen gstUGS_Brakes2_VoltageHold;
extern struct st_ui_generic_screen gstUGS_Brakes2_VoltageRelevel;
extern struct st_ui_generic_screen gstUGS_Brakes2_BPS_NC;
extern struct st_ui_generic_screen gstUGS_Brakes2_Enable;

extern struct st_ui_generic_screen gstUGS_Menu_Setup_Floors;
extern struct st_ui_generic_screen gstUGS_Floors_Learned;
extern struct st_ui_generic_screen gstUGS_Setup_Floors_TooHighTooLow;
extern struct st_ui_generic_screen gstUGS_Setup_Floors_Sabbath_Opening_F;
extern struct st_ui_generic_screen gstUGS_Setup_Floors_Sabbath_Opening_R;
extern struct st_ui_generic_screen gstUGS_Setup_Floors_StoreFloorLevel;
extern struct st_ui_generic_screen gstUGS_Setup_Floors_ShortFloorOpening;

extern struct st_ui_generic_screen gstUGS_AccessCode_Floor_1F;
extern struct st_ui_generic_screen gstUGS_AccessCode_Floor_2F;
extern struct st_ui_generic_screen gstUGS_AccessCode_Floor_3F;
extern struct st_ui_generic_screen gstUGS_AccessCode_Floor_4F;
extern struct st_ui_generic_screen gstUGS_AccessCode_Floor_5F;
extern struct st_ui_generic_screen gstUGS_AccessCode_Floor_6F;
extern struct st_ui_generic_screen gstUGS_AccessCode_Floor_7F;
extern struct st_ui_generic_screen gstUGS_AccessCode_Floor_8F;

extern struct st_ui_generic_screen gstUGS_AccessCode_Floor_1R;
extern struct st_ui_generic_screen gstUGS_AccessCode_Floor_2R;
extern struct st_ui_generic_screen gstUGS_AccessCode_Floor_3R;
extern struct st_ui_generic_screen gstUGS_AccessCode_Floor_4R;
extern struct st_ui_generic_screen gstUGS_AccessCode_Floor_5R;
extern struct st_ui_generic_screen gstUGS_AccessCode_Floor_6R;
extern struct st_ui_generic_screen gstUGS_AccessCode_Floor_7R;
extern struct st_ui_generic_screen gstUGS_AccessCode_Floor_8R;

extern struct st_ui_generic_screen gstUGS_Setup_Floors_AccessCode;
extern struct st_ui_generic_screen gstUGS_Setup_Floors_AccessCode_Front;
extern struct st_ui_generic_screen gstUGS_Setup_Floors_AccessCode_Rear;
extern struct st_ui_generic_screen gstUGS_AccessCode_CCBTime;


extern struct st_ui_generic_screen gstUGS_Menu_Setup_NTS;
extern struct st_ui_generic_screen gstUGS_NTS_TripThreshold;
extern struct st_ui_generic_screen gstUGS_NTS_Pos_Up;
extern struct st_ui_generic_screen gstUGS_NTS_Pos_Down;
extern struct st_ui_generic_screen gstUGS_NTS_Vel_Up;
extern struct st_ui_generic_screen gstUGS_NTS_Vel_Down;

extern struct st_ui_generic_screen gstUGS_Menu_Debug;
extern struct st_ui_generic_screen gstUGS_Menu_Setup_CarCalls;
extern struct st_ui_generic_screen gstUGS_CC_Latch_F;
extern struct st_ui_generic_screen gstUGS_CC_Latch_R;
extern struct st_ui_generic_screen gstUGS_HC_Latch;
extern struct st_ui_generic_screen gstUGS_Debug_ViewNetworkPackets;

extern struct st_ui_generic_screen gstUGS_Menu_ModuleStatus;
extern struct st_ui_generic_screen gstUGS_Debug_FloorLearnStatus;
extern struct st_ui_generic_screen gstUGS_Debug_MotionStatus;
extern struct st_ui_generic_screen gstUGS_Debug_AutoStatus;
extern struct st_ui_generic_screen gstUGS_Debug_FireStatus;
extern struct st_ui_generic_screen gstUGS_Debug_ViewDebugData;

extern struct st_ui_generic_screen gstUGS_Debug_RecallStatus;
extern struct st_ui_generic_screen gstUGS_Debug_CWStatus;
extern struct st_ui_generic_screen gstUGS_Debug_AcceptanceTest;


extern struct st_ui_generic_screen gstUGS_Menu_Setup_Hoistway;

extern struct st_ui_generic_screen gstUGS_Menu_Setup_Doors;
extern struct st_ui_generic_screen gstUGS_Door_DwellTimer;
extern struct st_ui_generic_screen gstUGS_Door_StuckTimer;
extern struct st_ui_generic_screen gstUGS_Door_NudgeTimer;
extern struct st_ui_generic_screen gstUGS_Door_SabbathDwell;

extern struct st_ui_generic_screen gstUGS_HoistWay_Distance_Top;
extern struct st_ui_generic_screen gstUGS_HoistWay_Distance_Bottom;
extern struct st_ui_generic_screen gstUGS_HoistWay_TopFloor;
extern struct st_ui_generic_screen gstUGS_HoistWay_BottomFloor;
extern struct st_ui_generic_screen gstUGS_HoistWay_TopOpening;
extern struct st_ui_generic_screen gstUGS_HoistWay_BottomOpening;

extern struct st_ui_generic_screen gstUGS_Menu_Scurve;
extern struct st_ui_generic_screen gstUGS_Menu_Scurve_P1;
extern struct st_ui_generic_screen gstUGS_Menu_Scurve_P2;
extern struct st_ui_generic_screen gstUGS_Menu_Scurve_P3;
extern struct st_ui_generic_screen gstUGS_Menu_Scurve_P4;

extern struct st_ui_generic_screen gstUGS_SCurve_QuickStopDecel;
extern struct st_ui_generic_screen gstUGS_SCurve_LevelingDecel;
extern struct st_ui_generic_screen gstUGS_SCurve_InspectionTerminalSpeed;
extern struct st_ui_generic_screen gstUGS_SCurve_SoftLimitDistUp;
extern struct st_ui_generic_screen gstUGS_SCurve_SoftLimitDistDown;

extern struct st_ui_generic_screen gstUGS_SCurve_P1_Accel;
extern struct st_ui_generic_screen gstUGS_SCurve_P1_Decel;
extern struct st_ui_generic_screen gstUGS_SCurve_P1_AccelJerkIn;
extern struct st_ui_generic_screen gstUGS_SCurve_P1_AccelJerkOut;
extern struct st_ui_generic_screen gstUGS_SCurve_P1_DecelJerkIn;
extern struct st_ui_generic_screen gstUGS_SCurve_P1_DecelJerkOut;
extern struct st_ui_generic_screen gstUGS_SCurve_P1_LevelingDistance;

extern struct st_ui_generic_screen gstUGS_SCurve_P2_Accel;
extern struct st_ui_generic_screen gstUGS_SCurve_P2_Decel;
extern struct st_ui_generic_screen gstUGS_SCurve_P2_AccelJerkIn;
extern struct st_ui_generic_screen gstUGS_SCurve_P2_AccelJerkOut;

extern struct st_ui_generic_screen gstUGS_SCurve_P3_Accel;
extern struct st_ui_generic_screen gstUGS_SCurve_P3_Decel;
extern struct st_ui_generic_screen gstUGS_SCurve_P3_AccelJerkIn;
extern struct st_ui_generic_screen gstUGS_SCurve_P3_AccelJerkOut;
extern struct st_ui_generic_screen gstUGS_SCurve_P3_DecelJerkIn;
extern struct st_ui_generic_screen gstUGS_SCurve_P3_DecelJerkOut;
extern struct st_ui_generic_screen gstUGS_SCurve_P3_LevelingDistance;

extern struct st_ui_generic_screen gstUGS_SCurve_P4_Accel;
extern struct st_ui_generic_screen gstUGS_SCurve_P4_Decel;
extern struct st_ui_generic_screen gstUGS_SCurve_P4_AccelJerkIn;
extern struct st_ui_generic_screen gstUGS_SCurve_P4_AccelJerkOut;
extern struct st_ui_generic_screen gstUGS_SCurve_P4_DecelJerkIn;
extern struct st_ui_generic_screen gstUGS_SCurve_P4_DecelJerkOut;
extern struct st_ui_generic_screen gstUGS_SCurve_P4_LevelingDistance;

extern struct st_ui_generic_screen gstUGS_Menu_Setup_Fire;
extern struct st_ui_generic_screen gstUGS_Fire_Defaults;
extern struct st_ui_generic_screen gstUGS_Fire_MRecall;
extern struct st_ui_generic_screen gstUGS_Fire_ARecall;
extern struct st_ui_generic_screen gstUGS_Fire_SmokeU;
extern struct st_ui_generic_screen gstUGS_Fire_SmokeL;
extern struct st_ui_generic_screen gstUGS_Fire_SmokeM;
extern struct st_ui_generic_screen gstUGS_Fire_SmokeH;

extern struct st_ui_generic_screen gstUGS_Fire_MainRecall;
extern struct st_ui_generic_screen gstUGS_Fire_AltRecall;
extern struct st_ui_generic_screen gstUGS_Fire_MainSmoke;
extern struct st_ui_generic_screen gstUGS_Fire_AltSmoke;
extern struct st_ui_generic_screen gstUGS_Fire_HoistwaySmoke;
extern struct st_ui_generic_screen gstUGS_Fire_MRSmoke;

extern struct st_ui_generic_screen gstUGS_Fire_SmokeConfig;
extern struct st_ui_generic_screen gstUGS_Fire_MainRecall_Floor;
extern struct st_ui_generic_screen gstUGS_Fire_MainRecall_Opening;

extern struct st_ui_generic_screen gstUGS_Fire_AltRecall_Floor;
extern struct st_ui_generic_screen gstUGS_Fire_AltRecall_Opening;

//---------------------------------------------------------------
// MAin Smoke
extern struct st_ui_generic_screen gstUGS_Fire_MainSmoke_MainOrAlt;
extern struct st_ui_generic_screen gstUGS_Fire_MainSmoke_FlashFireHat;
extern struct st_ui_generic_screen gstUGS_Fire_MainSmoke_ShuntTrip;
//---------------------------------------------------------------
// Alt Smoke
extern struct st_ui_generic_screen gstUGS_Fire_AltSmoke_MainOrAlt;
extern struct st_ui_generic_screen gstUGS_Fire_AltSmoke_FlashFireHat;
extern struct st_ui_generic_screen gstUGS_Fire_AltSmoke_ShuntTrip;
//---------------------------------------------------------------
// Hoistway Smoke
extern struct st_ui_generic_screen gstUGS_Fire_HoistwaySmoke_MainOrAlt;
extern struct st_ui_generic_screen gstUGS_Fire_HoistwaySmoke_FlashFireHat;
extern struct st_ui_generic_screen gstUGS_Fire_HoistwaySmoke_ShuntTrip;
//---------------------------------------------------------------
// MR Smoke
extern struct st_ui_generic_screen gstUGS_Fire_MRSmoke_MainOrAlt;
extern struct st_ui_generic_screen gstUGS_Fire_MRSmoke_FlashFireHat;
extern struct st_ui_generic_screen gstUGS_Fire_MRSmoke_ShuntTrip;

//---------------------------------------------------------------
// PIT Smoke
extern struct st_ui_generic_screen gstUGS_Fire_PITSmoke_MainOrAlt;
extern struct st_ui_generic_screen gstUGS_Fire_PITSmoke_FlashFireHat;
extern struct st_ui_generic_screen gstUGS_Fire_PITSmoke_ShuntTrip;
extern struct st_ui_generic_screen gstUGS_Fire_PITSmoke;

extern struct st_ui_generic_screen gstUGS_Menu_Setup_RTC;
extern struct st_ui_generic_screen gstUGS_Doors_Control;

extern struct st_ui_generic_screen gstUGS_Menu_Setup_Timers;
extern struct st_ui_generic_screen gstUGS_Menu_Setup_StartTimers;
extern struct st_ui_generic_screen gstUGS_Menu_Setup_StopTimers;
extern struct st_ui_generic_screen gstUGS_Delay_AccelDelay_Auto;
extern struct st_ui_generic_screen gstUGS_Delay_AccelDelay_Insp;
extern struct st_ui_generic_screen gstUGS_Delay_BrakePick_Auto;
extern struct st_ui_generic_screen gstUGS_Delay_BrakePick_Insp;

extern struct st_ui_generic_screen gstUGS_Delay_BrakeDropA;
extern struct st_ui_generic_screen gstUGS_Delay_BrakeDropI;

extern struct st_ui_generic_screen gstUGS_Delay_B2DropA;
extern struct st_ui_generic_screen gstUGS_Delay_B2DropI;
extern struct st_ui_generic_screen gstUGS_Delay_EBrakeDropA;
extern struct st_ui_generic_screen gstUGS_Delay_EBrakeDropI;

extern struct st_ui_generic_screen gstUGS_Delay_MotorDropA;
extern struct st_ui_generic_screen gstUGS_Delay_MotorDropI;

extern struct st_ui_generic_screen gstUGS_Delay_DriveDropA;
extern struct st_ui_generic_screen gstUGS_Delay_DriveDropI;

extern struct st_ui_generic_screen gstUGS_EmergencyBitmap;

extern struct st_ui_generic_screen gstUGS_PatternData;

extern struct st_ui_generic_screen gstUGS_Menu_Alarms;
extern struct st_ui_generic_screen gstUGS_ActiveAlarms;

extern struct st_ui_generic_screen gstUGS_Menu_Safety;
extern struct st_ui_generic_screen gstUGS_Menu_SpeedDev;
extern struct st_ui_generic_screen gstUGS_SpeedDev_Threshold;
extern struct st_ui_generic_screen gstUGS_SpeedDev_Timeout;
extern struct st_ui_generic_screen gstUGS_SpeedDev_Offset;
extern struct st_ui_generic_screen gstUGS_Menu_TractionLoss;
extern struct st_ui_generic_screen gstUGS_TractionLoss_Threshold;
extern struct st_ui_generic_screen gstUGS_TractionLoss_Timeout;
extern struct st_ui_generic_screen gstUGS_TractionLoss_Offset;

extern struct st_ui_generic_screen gstUGS_Menu_LockClip;
extern struct st_ui_generic_screen gstUGS_Menu_InspectionODL;
extern struct st_ui_generic_screen gstUGS_Menu_DoorOpenODL;
extern struct st_ui_generic_screen gstUGS_Menu_ETS_ODL;
extern struct st_ui_generic_screen gstUGS_Menu_SFP_DL;
extern struct st_ui_generic_screen gstUGS_Menu_GeneralODL;
extern struct st_ui_generic_screen gstUGS_Menu_NTS_ODL;

extern struct st_ui_generic_screen gstUGS_LoadWeigher;
extern struct st_ui_generic_screen gstUGS_BrakeStatus;
extern struct st_ui_generic_screen gstUGS_EBrakeStatus;

extern struct st_ui_generic_screen gstUGS_LoadWeigher_Select;
extern struct st_ui_generic_screen gstUGS_LWD_EnableWifi;
extern struct st_ui_generic_screen gstUGS_LWD_Debug;
extern struct st_ui_generic_screen gstUGS_LWD_AutoRecalibrate;
extern struct st_ui_generic_screen gstUGS_LWD_TriggerRecalibrate;
extern struct st_ui_generic_screen gstUGS_LWD_TriggerLoadLearn;
extern struct st_ui_generic_screen gstUGS_LWD_TorqueScaling;
extern struct st_ui_generic_screen gstUGS_LWD_TorqueOffset;
extern struct st_ui_generic_screen gstUGS_LWD_MonthlyCalibrationHour;
extern struct st_ui_generic_screen gstUGS_LWD_MonthlyCalibrationDay;
extern struct st_ui_generic_screen gstUGS_LWD_Req;

extern struct st_ui_generic_screen gstUGS_Menu_Setup_LoadWeigher;
extern struct st_ui_generic_screen gstUGS_Group_CarIndex;
extern struct st_ui_generic_screen gstUGS_Group_LandingOffset;
extern struct st_ui_generic_screen gstUGS_Group_DispatchTimeout;
extern struct st_ui_generic_screen gstUGS_Group_DispatchOffline;
extern struct st_ui_generic_screen gstUGS_Group_NumXRegCars;
extern struct st_ui_generic_screen gstUGS_Group_XREG_DestTimeout;
extern struct st_ui_generic_screen gstUGS_Group_XREG_DestOfflineTimer;
extern struct st_ui_generic_screen gstUGS_Menu_Setup_Group;

extern struct st_ui_generic_screen gstUGS_Floors_RelevelZoneSize;
extern struct st_ui_generic_screen gstUGS_Floors_EnableRelevel;
extern struct st_ui_generic_screen gstUGS_Floors_RelevelDelay;

extern struct st_ui_generic_screen gstUGS_Menu_DriveSetup;
extern struct st_ui_generic_screen gstUGS_Setup_DriveSelect;
extern struct st_ui_generic_screen gstUGS_DriveSetup_EnableUIDriveEdit;

extern struct st_ui_generic_screen gstUGS_DisableBPS_StopSeq;
extern struct st_ui_generic_screen gstUGS_DisableBPS_StuckHigh;
extern struct st_ui_generic_screen gstUGS_DisableBPS_StuckLow;

extern struct st_ui_generic_screen gstUGS_Setup_InvertInputs_Boards;

extern struct st_ui_generic_screen gstUGS_Setup_InvertInputs_MR;
extern struct st_ui_generic_screen gstUGS_Setup_InvertInputs_CT;
extern struct st_ui_generic_screen gstUGS_Setup_InvertInputs_COP;
extern struct st_ui_generic_screen gstUGS_Setup_InvertInputs_RIS;
extern struct st_ui_generic_screen gstUGS_Setup_InvertInputs_EXP1;
extern struct st_ui_generic_screen gstUGS_Setup_InvertInputs_EXP2;
extern struct st_ui_generic_screen gstUGS_Setup_InvertInputs_EXP3;
extern struct st_ui_generic_screen gstUGS_Setup_InvertInputs_EXP4;
extern struct st_ui_generic_screen gstUGS_Setup_InvertInputs_EXP5;

extern struct st_ui_generic_screen gstUGS_Setup_Floors_Security_F;
extern struct st_ui_generic_screen gstUGS_Setup_Floors_Security_R;
extern struct st_ui_generic_screen gstUGS_Setup_Floors_Opening_F;
extern struct st_ui_generic_screen gstUGS_Setup_Floors_Opening_R;

extern struct st_ui_generic_screen gstUGS_Setup_Floors_TimedCCSecurity;
extern struct st_ui_generic_screen gstUGS_Setup_Floors_TimedCCSecurity_Start_MF;
extern struct st_ui_generic_screen gstUGS_Setup_Floors_TimedCCSecurity_Stop_MF;
extern struct st_ui_generic_screen gstUGS_Setup_Floors_TimedCCSecurity_Start_SS;
extern struct st_ui_generic_screen gstUGS_Setup_Floors_TimedCCSecurity_Stop_SS;
extern struct st_ui_generic_screen gstUGS_Setup_Floors_TimedCCSecurity_EnablePerFloor_F;
extern struct st_ui_generic_screen gstUGS_Setup_Floors_TimedCCSecurity_EnablePerFloor_R;

extern struct st_ui_generic_screen gstUGS_Door_DwellHallTimer;
extern struct st_ui_generic_screen gstUGS_Door_DwellADATimer;
extern struct st_ui_generic_screen gstUGS_Door_PreOpeningDistance;

extern struct st_ui_generic_screen gstUGS_SCurve_ShortRunMinDist;

extern struct st_ui_generic_screen gstUGS_Misc_ParkingTimer;
extern struct st_ui_generic_screen gstUGS_Misc_ParkingFloor;
extern struct st_ui_generic_screen gstUGS_Misc_ParkingDoorOpen;

extern struct st_ui_generic_screen gstUGS_EQ_Enable;
extern struct st_ui_generic_screen gstUGS_EQ_CounterweightPos;
extern struct st_ui_generic_screen gstUGS_Menu_Setup_EQ;

extern struct st_ui_generic_screen gstUGS_Menu_SetupIO;

extern struct st_ui_generic_screen gstUGS_Menu_SetupAllBrakes;

extern struct st_ui_generic_screen gstUGS_Misc_EnableConstructionBox;
extern struct st_ui_generic_screen gstUGS_Misc_OOS_Disable;
extern struct st_ui_generic_screen gstUGS_Misc_OOS_HourlyLimit;
extern struct st_ui_generic_screen gstUGS_Misc_MaxRuntime;

extern struct st_ui_generic_screen gstUGS_Safety_EBrakeOnOverspeed;
extern struct st_ui_generic_screen gstUGS_Safety_DisableConstOverspeed;

extern struct st_ui_generic_screen gstUGS_Door_DC_On_Run;
extern struct st_ui_generic_screen gstUGS_Door_DC_On_Close;
extern struct st_ui_generic_screen gstUGS_Door_DO_On_Open;
extern struct st_ui_generic_screen gstUGS_Door_DisableOnCTStop;
extern struct st_ui_generic_screen gstUGS_Door_DisableOnHA;
extern struct st_ui_generic_screen gstUGS_Door_IMotionDoors;

extern struct st_ui_generic_screen gstUGS_Misc_ICInspReqForCT;
extern struct st_ui_generic_screen gstUGS_Misc_DisableIdleTravelArrow;
extern struct st_ui_generic_screen gstUGS_Misc_UIDriveEdit;
extern struct st_ui_generic_screen gstUGS_Misc_EnableLatchesCC;
extern struct st_ui_generic_screen gstUGS_Misc_FanAndLightTimer;
extern struct st_ui_generic_screen gstUGS_Misc_FanAndLightTimer2;
extern struct st_ui_generic_screen gstUGS_Misc_CarToLobbyFloor;
extern struct st_ui_generic_screen gstUGS_Misc_ArrivalUpdateTime;
extern struct st_ui_generic_screen gstUGS_Misc_EnableOldFRAM;
extern struct st_ui_generic_screen gstUGS_Misc_EnableHallSecurity;
extern struct st_ui_generic_screen gstUGS_Misc_EnablePitInsp;
extern struct st_ui_generic_screen gstUGS_Misc_EnableLandInsp;

extern struct st_ui_generic_screen gstUGS_Speeds_Test_AscDescSpeed;
extern struct st_ui_generic_screen gstUGS_Speeds_Test_CarOverspeed;
extern struct st_ui_generic_screen gstUGS_Speeds_Test_BufferSpeed;

extern struct st_ui_generic_screen gstUGS_Menu_Setup_Parking;
extern struct st_ui_generic_screen gstUGS_Menu_Setup_OOS;

extern struct st_ui_generic_screen gstUGS_Flood_NumFloors;
extern struct st_ui_generic_screen gstUGS_Flood_OkayToRun;
extern struct st_ui_generic_screen gstUGS_Flood_OverrideFire;
extern struct st_ui_generic_screen gstUGS_Menu_Setup_Flood;

extern struct st_ui_generic_screen gstUGS_Sabbath_KeyEnable;
extern struct st_ui_generic_screen gstUGS_Sabbath_TimerEnable;
extern struct st_ui_generic_screen gstUGS_Sabbath_KeyOrTimer_Enable;
extern struct st_ui_generic_screen gstUGS_Sabbath_Start_Time;
extern struct st_ui_generic_screen gstUGS_Sabbath_End_Time;
extern struct st_ui_generic_screen gstUGS_Sabbath_Door_Dwell_Timer;
extern struct st_ui_generic_screen gstUGS_Sabbath_FloorsOpening_F;
extern struct st_ui_generic_screen gstUGS_Sabbath_FloorsOpening_R;
extern struct st_ui_generic_screen gstUGS_Sabbath_Destinations_Up;
extern struct st_ui_generic_screen gstUGS_Sabbath_Destinations_Down;
extern struct st_ui_generic_screen gstUGS_Menu_Setup_Sabbath;

extern struct st_ui_generic_screen gstUGS_EMS_AllowPh2WithoutPh1;
extern struct st_ui_generic_screen gstUGS_EMS_ExitPh2AnyFloor;
extern struct st_ui_generic_screen gstUGS_EMS_FireOverridesEMS1;
extern struct st_ui_generic_screen gstUGS_EMS_EMS1_ExitDelay;
extern struct st_ui_generic_screen gstUGS_EMS_EMS2_ExitDelay;
extern struct st_ui_generic_screen gstUGS_Menu_Setup_EMS;

extern struct st_ui_generic_screen gstUGS_Swing_CallsEnable;
extern struct st_ui_generic_screen gstUGS_Swing_StayInGroup;
extern struct st_ui_generic_screen gstUGS_Swing_IdleTimer;
extern struct st_ui_generic_screen gstUGS_Menu_Setup_Swing;

//extern struct st_ui_generic_screen gstUGS_ATTD_DirWithCCB;
extern struct st_ui_generic_screen gstUGS_ATTD_DispatchTimeout;
extern struct st_ui_generic_screen gstUGS_ATTD_BuzzerTime;
extern struct st_ui_generic_screen gstUGS_Menu_Setup_ATTD;

extern struct st_ui_generic_screen gstUGS_Door_DwellHoldTimer;
extern struct st_ui_generic_screen gstUGS_Door_NoDemandDoorsOpen;
extern struct st_ui_generic_screen gstUGS_Door_JumperTimeout;
extern struct st_ui_generic_screen gstUGS_Door_JumperOnDOL;
extern struct st_ui_generic_screen gstUGS_Door_HourlyFaultLimit;
extern struct st_ui_generic_screen gstUGS_Door_NudgeBuzzerOnly;
extern struct st_ui_generic_screen gstUGS_Door_NudgeNoBuzzer;

extern struct st_ui_generic_screen gstUGS_Safety_ETS_Offset;

extern struct st_ui_generic_screen gstUGS_Misc_Enable3DigitPI;

extern struct st_ui_generic_screen gstUGS_Misc_PaymentPasscode;

extern struct st_ui_generic_screen gstUGS_Menu_ETSL;
extern struct st_ui_generic_screen gstUGS_Safety_ETSL_ODL;
extern struct st_ui_generic_screen gstUGS_Safety_Enable_ETSL;
extern struct st_ui_generic_screen gstUGS_Safety_RatedBufferSpd;
extern struct st_ui_generic_screen gstUGS_Safety_BufferDistance;
extern struct st_ui_generic_screen gstUGS_Safety_SlideDistance;
extern struct st_ui_generic_screen gstUGS_ETSL_CameraOffset;

extern struct st_ui_generic_screen gstUGS_DisableRampZero;
extern struct st_ui_generic_screen gstUGS_DisableHoldZero;

extern struct st_ui_generic_screen gstUGS_Misc_DynamicParking;

extern struct st_ui_generic_screen gstUGS_Construction_ODL;

extern struct st_ui_generic_screen gstUGS_ExpansionStatus1;
extern struct st_ui_generic_screen gstUGS_ExpansionStatus2;
extern struct st_ui_generic_screen gstUGS_ExpansionStatus3;
extern struct st_ui_generic_screen gstUGS_ExpansionStatus4;
extern struct st_ui_generic_screen gstUGS_ExpansionStatus5;
extern struct st_ui_generic_screen gstUGS_ExpansionStatus6;
extern struct st_ui_generic_screen gstUGS_ExpansionStatus7;
extern struct st_ui_generic_screen gstUGS_Menu_ExpansionStatus;

extern struct st_ui_generic_screen gstUGS_HallBoardStatus;
extern struct st_ui_generic_screen gstUGS_HallLanternStatus;
extern struct st_ui_generic_screen gstUGS_HallSecurityStatus;

extern struct st_ui_generic_screen gstUGS_ClockStatus;

extern struct st_ui_generic_screen gstUGS_Menu_Default;
extern struct st_ui_generic_screen gstUGS_Default_Other;
extern struct st_ui_generic_screen gstUGS_Default_Floors;
extern struct st_ui_generic_screen gstUGS_Default_SCurve;
extern struct st_ui_generic_screen gstUGS_Default_RunTimers;
extern struct st_ui_generic_screen gstUGS_Default_IO;
extern struct st_ui_generic_screen gstUGS_Default_Factory;
extern struct st_ui_generic_screen gstUGS_Default_FRAM;

extern struct st_ui_generic_screen gstUGS_DisableBPS2_StopSeq;
extern struct st_ui_generic_screen gstUGS_DisableBPS2_StuckHigh;
extern struct st_ui_generic_screen gstUGS_DisableBPS2_StuckLow;

extern struct st_ui_generic_screen gstUGS_BPS_Timeout;
extern struct st_ui_generic_screen gstUGS_BPS2_Timeout;

extern struct st_ui_generic_screen gstUGS_DisableLatchingBrakeFault;

extern struct st_ui_generic_screen gstUGS_Speeds_MinRelevel;

extern struct st_ui_generic_screen gstUGS_OOS_MaxStartsPerMinute;

extern struct st_ui_generic_screen gstUGS_EPower_NumCars;
extern struct st_ui_generic_screen gstUGS_EPower_PriorityCar;
extern struct st_ui_generic_screen gstUGS_EPower_PretransferStall;
extern struct st_ui_generic_screen gstUGS_Menu_Setup_EPower;

extern struct st_ui_generic_screen gstUGS_Door_OpeningTime;

extern struct st_ui_generic_screen gstUGS_DirChangeDelay;

extern struct st_ui_generic_screen gstUGS_OOS_DisablePIOOS;

extern struct st_ui_generic_screen gstUGS_Speeds_EPowerSpeed;
extern struct st_ui_generic_screen gstUGS_Speeds_AccessSpeed;

extern struct st_ui_generic_screen gstUGS_IdleCarDirTimeout;

extern struct st_ui_generic_screen gstUGS_Fire_AltMachineRoom;
extern struct st_ui_generic_screen gstUGS_Fire_EnableAltMachineRoom;
//---------------------------------------------------------------
// Hoistway 2 Smoke
extern struct st_ui_generic_screen gstUGS_Fire_Hoistway2Smoke;
extern struct st_ui_generic_screen gstUGS_Fire_Hoistway2Smoke_MainOrAlt;
extern struct st_ui_generic_screen gstUGS_Fire_Hoistway2Smoke_FlashFireHat;
extern struct st_ui_generic_screen gstUGS_Fire_Hoistway2Smoke_ShuntTrip;
//---------------------------------------------------------------
// MR 2 Smoke
extern struct st_ui_generic_screen gstUGS_Fire_MR2Smoke;
extern struct st_ui_generic_screen gstUGS_Fire_MR2Smoke_MainOrAlt;
extern struct st_ui_generic_screen gstUGS_Fire_MR2Smoke_FlashFireHat;
extern struct st_ui_generic_screen gstUGS_Fire_MR2Smoke_ShuntTrip;

extern struct st_ui_generic_screen gstUGS_SCurve_DestOffsetUp;
extern struct st_ui_generic_screen gstUGS_SCurve_DestOffsetDown;
extern struct st_ui_generic_screen gstUGS_SCurve_RelevelOffsetUp;
extern struct st_ui_generic_screen gstUGS_SCurve_RelevelOffsetDown;

extern struct st_ui_generic_screen gstUGS_Door_CheckTime;
extern struct st_ui_generic_screen gstUGS_Door_LobbyDwellTimer;

extern struct st_ui_generic_screen gstUGS_Group_HallSecurityMask;
extern struct st_ui_generic_screen gstUGS_Group_HallSecurityMap;
extern struct st_ui_generic_screen gstUGS_Group_HallCallMask;
extern struct st_ui_generic_screen gstUGS_Group_HallMedicalMask;
extern struct st_ui_generic_screen gstUGS_Group_HallRearDoorMask;
extern struct st_ui_generic_screen gstUGS_Group_SwingCallMask;
extern struct st_ui_generic_screen gstUGS_Group_LinkedHallMask_1;
extern struct st_ui_generic_screen gstUGS_Group_LinkedHallMask_2;
extern struct st_ui_generic_screen gstUGS_Group_LinkedHallMask_3;
extern struct st_ui_generic_screen gstUGS_Group_LinkedHallMask_4;

extern struct st_ui_generic_screen gstUGS_CPLD_Status_MR;
extern struct st_ui_generic_screen gstUGS_CPLD_Status_CT;
extern struct st_ui_generic_screen gstUGS_CPLD_Status_COP;
extern struct st_ui_generic_screen gstUGS_Menu_CPLD_Status;

extern struct st_ui_generic_screen gstUGS_Door_TimeoutLockAndCAM;

extern struct st_ui_generic_screen gstUGS_Door_DoorTypeSelect_F;
extern struct st_ui_generic_screen gstUGS_Door_DoorTypeSelect_R;
extern struct st_ui_generic_screen gstUGS_Door_SwingDoorBitmap_F;
extern struct st_ui_generic_screen gstUGS_Door_SwingDoorBitmap_R;
extern struct st_ui_generic_screen gstUGS_Door_TimeoutLockAndCAM;
extern struct st_ui_generic_screen gstUGS_Door_RetiringCAM;
extern struct st_ui_generic_screen gstUGS_Door_FixedCAM;
extern struct st_ui_generic_screen gstUGS_Door_Swing_GSW_Lock_Timeout;
extern struct st_ui_generic_screen gstUGS_Door_Swing_Contacts_Timeout;
extern struct st_ui_generic_screen gstUGS_Door_Disable_Rear_DOB;

extern struct st_ui_control gstUI_Control;

extern uint8_t gbParamSaveInProgress;//5-25

extern uint8_t gbResetSaveState;

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

extern void gEdit_Parameter(struct st_ui_parameter_edit * pstParamEdit);

extern void Print_AccessDeniedScreen( const char * psDenialReason );

void LCD_WritePositionInFeet( uint32_t ulCount_500UM );
void LCD_WritePositionInFeet_Shortened( uint32_t ulCount_500UM );
void LCD_WriteDistanceInInches( uint32_t ulCount_500UM, uint8_t bInchesOnly );

void PrevScreen( void );
void NextScreen( struct st_ui_generic_screen *pstUGS_Next );

void Draw_Menu( struct st_ui_screen__menu * pstMenuScreen );
void UI_ParamEditSceenTemplate( struct st_param_edit_menu * pstParamEditMenu );

uint8_t Param_PrintParamString_1Bit( enum en_1bit_params enParamNum);
uint8_t Param_PrintParamString_8Bit( enum en_8bit_params enParamNum);
uint8_t Param_PrintParamString_16Bit( enum en_16bit_params enParamNum);
uint8_t Param_PrintParamString_24Bit( enum en_24bit_params enParamNum);
uint8_t Param_PrintParamString_32Bit( enum en_32bit_params enParamNum);

uint8_t Get_FireCodeDefault(void);

void PrintFaultString( en_faults eFaultNum );
char const * const GetAlarmString( en_alarms eAlarmNum );
void PrintAlarmString( en_alarms eAlarmNum );
void PrintMovingText( char const * const psString );//one per screen
void UI_UpdateMovingTextVariables(struct st_ui_control *pstUI_Control);

void UI_UpdateNewPageFlag(struct st_ui_control *pstUI_Control);
uint8_t UI_GetNewPageFlag();

void UI_PrintDoorSymbol( enum en_door_states eState,
                         uint8_t bPHE, uint8_t bGSW,
                         uint8_t bDO, uint8_t bDC );

void UI_ParamEditScreenTemplate_EditByBit_PerFloor(struct st_param_edit_menu * pstParamEditMenu);
void UI_ParamEditScreenTemplate_EditByBit_PerLanding(struct st_param_edit_menu * pstParamEditMenu);
void UI_ParamEditScreenTemplate_EditByBit_PerHallMaskFunction(struct st_param_edit_menu * pstParamEditMenu);
#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
