#ifndef SMARTRISTE_NTS_CT
#define SMARTRISE_NTS_CT

uint32_t NTS_CT_Init( struct st_module *pstThisModule );
void NTS_CT_Run( void );

extern uint8_t NTS_GetState_CT( void );
extern uint8_t NTS_GetFault_CT( void );
extern uint8_t NTS_GetAlarm_CT( void );
uint8_t NTS_GetStopFlag();
#endif
