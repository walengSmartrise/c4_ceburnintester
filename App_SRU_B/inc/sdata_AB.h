#ifndef SDATA_AB_H
#define SDATA_AB_H


// TODO: Give a better name to this structure
typedef struct
{
    struct st_sdata_control * pstSData_AB_A;
    struct st_sdata_control * pstSData_AB_B;
    uint8_t aucPerNodeNumDatagrams[2];
}SData_Config;


void SharedData_AB_MR_Init( SData_Config* );
void SharedData_AB_CT_Init( SData_Config* );
void SharedData_AB_CN_Init( SData_Config* );
void SharedData_AB_COP_Init( SData_Config* );


void LoadData_MR( void );
void LoadData_CT( void );
void LoadData_CN( void );
void LoadData_COP( void );

void UnloadData_MR( void );
void UnloadData_CT( void );
void UnloadData_CN( void );
void UnloadData_COP( void );

#endif
