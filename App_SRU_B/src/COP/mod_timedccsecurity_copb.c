/******************************************************************************
 *
 * @file
 * @brief
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include <stdint.h>
#include <stdlib.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

static uint32_t GetTimedCCSecurityParamTime( uint16_t param );
static uint8_t GetTimedCCSecurityDayOfWeek( uint8_t ucDayofWeek );
static uint8_t GetCarCallAndVirtualEnable( uint8_t ucFloorIndex, uint8_t bRearDoor );

static uint32_t auiBF_TimedCarCallSecurity[NUM_OF_DOORS][ BITMAP32_SIZE ( MAX_NUM_FLOORS ) ];

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_TimedCarCallSecurity =
{
   .pfnInit = Init,
   .pfnRun = Run,
};


/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 1000;
   pstThisModule->uwRunPeriod_1ms = 200;

   auiBF_TimedCarCallSecurity[DOOR_FRONT][0] = 0;
   auiBF_TimedCarCallSecurity[DOOR_FRONT][1] = 0;
   auiBF_TimedCarCallSecurity[DOOR_FRONT][2] = 0;

   auiBF_TimedCarCallSecurity[DOOR_REAR][0] = 0;
   auiBF_TimedCarCallSecurity[DOOR_REAR][1] = 0;
   auiBF_TimedCarCallSecurity[DOOR_REAR][2] = 0;

   return 0;
}

static uint8_t FloorSecured( uint8_t ucFloorIndex, uint8_t bRearDoor)
{
   uint8_t ucDayofWeek = RTC_GetDayOfWeek();
   uint32_t ucTimeofDay = RTC_GetTimeOfDay_TotalSeconds();
   uint8_t bSecured = 1;
   uint8_t ucMapAddress = (bRearDoor)?enPARAM32__SecureKeyedBitmapR_0:enPARAM32__SecureKeyedBitmapF_0;
   uint8_t ucMapAddress_TimedCCS = (bRearDoor)?enPARAM32__SecureTimedBitmapR_0:enPARAM32__SecureTimedBitmapF_0;
   ucMapAddress += (ucFloorIndex / 32);
   ucMapAddress_TimedCCS += (ucFloorIndex / 32);

   uint8_t bTimedCCSecurityEnableFlag = 0;

   if( ( ucTimeofDay > GetTimedCCSecurityParamTime(enPARAM16__Weekday_StartTime) )
         && ( ucTimeofDay < GetTimedCCSecurityParamTime(enPARAM16__Weekday_EndTime) )
         && ( GetTimedCCSecurityDayOfWeek(ucDayofWeek) == 1 ) )
   {
      bTimedCCSecurityEnableFlag = 1;
   }
   else if( ( ( ucTimeofDay > GetTimedCCSecurityParamTime(enPARAM16__Weekend_StartTime) )
         && ( ucTimeofDay < GetTimedCCSecurityParamTime(enPARAM16__Weekend_EndTime) )
         && ( GetTimedCCSecurityDayOfWeek(ucDayofWeek) == 2 ) ) )
   {
      bTimedCCSecurityEnableFlag = 1;
   }

   if (
           ( ( ( (Param_ReadValue_32Bit( ucMapAddress ) >> (ucFloorIndex%32) ) & 1 ) == 0) &&
           ( ( ( ( (Param_ReadValue_32Bit( ucMapAddress_TimedCCS ) >> (ucFloorIndex%32) ) & 1 ) == 0) || !bTimedCCSecurityEnableFlag ) ) ) ||
           GetInputValue( enIN_SECURITY_ENABLE_ALL_CAR_CALLS) ||
           GetCarCallAndVirtualEnable( ucFloorIndex, bRearDoor )
        )
   {
      bSecured = 0;
   }

   return bSecured;
}
/*-----------------------------------------------------------------------------

   Converts Timed Car Call Security time to seconds

 -----------------------------------------------------------------------------*/
static uint32_t GetTimedCCSecurityParamTime( uint16_t param )
{
   uint32_t timeInHours = Param_ReadValue_16Bit( param );

   uint32_t hourInSeconds = timeInHours / 100;

   uint32_t minuteInSeconds = timeInHours - (hourInSeconds * 100);

   uint32_t timeInSeconds = ( hourInSeconds * 3600 ) + ( minuteInSeconds * 60 );

   return timeInSeconds;
}
/*-----------------------------------------------------------------------------

   Determines if the day is a weekday or weekend

 -----------------------------------------------------------------------------*/
static uint8_t GetTimedCCSecurityDayOfWeek( uint8_t ucDayofWeek )
{
   //Monday through Friday
   if( ( ucDayofWeek > RTC_DOW__SUNDAY ) && ( ucDayofWeek < RTC_DOW__SATURDAY ) )
   {
      return 1;
   }
   //Saturday and Sunday
   else if( (ucDayofWeek == RTC_DOW__SUNDAY ) || ( ucDayofWeek == RTC_DOW__SATURDAY ) )
   {
      return 2;
   }
   else
   {
      return 0;
   }
}
/*-----------------------------------------------------------------------------

   Get Timed Car Call Security Bitmap

 -----------------------------------------------------------------------------*/
uint32_t GetTimedCarCallSecurityBitmap(uint8_t bRear, uint8_t ucBitmapIndex)
{
   return auiBF_TimedCarCallSecurity[bRear][ucBitmapIndex];
}
/*-----------------------------------------------------------------------------

   Get Timed Car Call Security Bitmap

 -----------------------------------------------------------------------------*/
static uint8_t GetCarCallAndVirtualEnable( uint8_t ucFloorIndex, uint8_t bRearDoor )
{
   uint8_t bValue = 0;

   if(!bRearDoor)
   {
      if( (ucFloorIndex/32) == 0 )
      {
         bValue = ( GetCarCallSecurity_F0() >> ucFloorIndex%32 ) & 1;
      }
      else if( (ucFloorIndex/32) == 1 )
      {
         bValue = ( GetCarCallSecurity_F1() >> ucFloorIndex%32 ) & 1;
      }
      else if( (ucFloorIndex/32) == 2 )
      {
         bValue = ( GetCarCallSecurity_F2() >> ucFloorIndex%32 ) & 1;
      }
   }
   else if( bRearDoor && GetFP_RearDoors() )
   {
      if( (ucFloorIndex/32) == 0 )
      {
         bValue = ( GetCarCallSecurity_R0() >> ucFloorIndex%32 ) & 1;
      }
      else if( (ucFloorIndex/32) == 1 )
      {
         bValue = ( GetCarCallSecurity_R1() >> ucFloorIndex%32 ) & 1;
      }
      else if( (ucFloorIndex/32) == 2 )
      {
         bValue = ( GetCarCallSecurity_R2() >> ucFloorIndex%32 ) & 1;
      }
   }

   return bValue;
}
/*-----------------------------------------------------------------------------

   Scan all CCBs and latch them if on MODE_A__NORMAL.
   Update CCLs

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   uint8_t ucFloorIndex;

   for ( ucFloorIndex = 0; ucFloorIndex < GetFP_NumFloors(); ucFloorIndex++ )
   {
      Sys_Bit_Set( &auiBF_TimedCarCallSecurity[DOOR_FRONT][ucFloorIndex / 32], ucFloorIndex, FloorSecured( ucFloorIndex , DOOR_FRONT ) );

      if ( GetFP_RearDoors() )
      {
         Sys_Bit_Set( &auiBF_TimedCarCallSecurity[DOOR_REAR][ucFloorIndex / 32], ucFloorIndex, FloorSecured( ucFloorIndex , DOOR_REAR ) );
      }
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
