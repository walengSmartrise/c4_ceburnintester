#include "mod.h"
#include "board.h"
#include <stdio.h>
#include "sru.h"
#include "sru_b.h"
#include "sys.h"

//------------------------------------------------------------------------------
static struct st_module * gastModules_COPB[] =
{
  &gstSys_Mod,  // must include the system module in addition to app modules
  &gstMod_Fault,
  &gstMod_FaultApp,
  &gstMod_Heartbeat,
  &gstMod_Watchdog,

  &gstMod_CAN,
  &gstMod_ParamEEPROM,
  &gstMod_ParamApp,
  &gstMod_ParamCOPB,
  &gstMod_UART,

  &gstMod_SData,
  &gstMod_LCD,
  &gstMod_Buttons,
  &gstMod_UI,
  &gstMod_RTC,

  &gstMod_AlarmApp,
  &gstMod_LocalOutputs,
  &gstMod_SData_AuxNet,
  &gstMod_CEboards,

  &gstMod_Dupar,

  &gstMod_TimedCarCallSecurity,
};

static struct st_module_control gstModuleControl_COPB =
{
   .uiNumModules = sizeof(gastModules_COPB) / sizeof(gastModules_COPB[ 0 ]),

   .pastModules = &gastModules_COPB,

   .enMCU_ID = enMCUB_SRU_UI,

   .enDeployment = enSRU_DEPLOYMENT__COP,
};
static struct st_module * gastModules_COPB_Pre[] =
{
         &gstSys_Mod,  // must include the system module in addition to app modules

         &gstMod_ParamEEPROM,
         &gstMod_ParamApp,
         &gstMod_ParamCOPB,
};

static struct st_module_control gstModuleControl_COPB_Pre =
{
   .uiNumModules = sizeof(gastModules_COPB_Pre) / sizeof(gastModules_COPB_Pre[ 0 ]),

   .pastModules = &gastModules_COPB_Pre,

   .enMCU_ID = enMCUB_SRU_UI,

   .enDeployment = enSRU_DEPLOYMENT__COP,
};
/**
 * @brief   main routine for systick example
 * @return  Function should not exit.
 */
int main_COP( void )
{
     __enable_irq();

     // These are the modules that need to be started before the main application.
     // Right now this is just parameters. This is because most modules require
     // parameters and the parameters are not valid till they are loaded into RAM
     Mod_InitAllModules( &gstModuleControl_COPB_Pre );
     while(!GetFP_FlashParamsReady())
     {
        Mod_RunOneModule( &gstModuleControl_COPB_Pre );
     }

     Mod_InitAllModules( &gstModuleControl_COPB );

    // This is really a while(1) loop. Sys_Shutdown() always returns 0. The
    // function is being provided in anticipation of a future time when this
    // application might be running on a PC simulator and we would need a
    // mechanism to terminate this application.
    while ( !Sys_Shutdown() )
    {
        Mod_RunOneModule( &gstModuleControl_COPB );

        uint8_t ucCurrModIndex = gstModuleControl_COPB.uiCurrModIndex;
        uint8_t ucNumOfMods = gstModuleControl_COPB.uiNumModules;

        struct st_module * (* pastModules)[] = gstModuleControl_COPB.pastModules;
        struct st_module * pstCurrentModule = (*pastModules)[ ucCurrModIndex ];

        if( ( pstCurrentModule->bTimeViolation )
         && ( ucCurrModIndex < ucNumOfMods )
         && ( ( Param_ReadValue_8Bit(enPARAM8__TimeViolationModule) == MRC__ALL )
           || ( ( Param_ReadValue_8Bit(enPARAM8__TimeViolationModule)-MRC__COPB_START  ) == ucCurrModIndex ) ) )
        {
           SetAlarm( ALM__RUN_TIME_FAULT_COPB + ucCurrModIndex - 1);
        }
    }

    return 0;
}
