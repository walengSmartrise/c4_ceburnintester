/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief
 * @version  V1.00
 * @date     27, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "sru.h"
#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "sdata_AB.h"
#include "GlobalData.h"
#include "operation.h"
#include "motion.h"
#include "position.h"
#include <string.h>
#include "fpga_api.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void LoadData_COP( void )
{
   un_sdata_datagram unDatagram;
   //--------------------------------------
   memset(&unDatagram, 0, sizeof(un_sdata_datagram));
   unDatagram.auc8[0] = GetAcceptanceTest_Plus1();
   unDatagram.auc8[1] = GetUIRequest_FaultLog();
   unDatagram.auc8[2] = GetUIRequest_DoorControl();
   unDatagram.auc8[3] = GetCarCallRequest(DOOR_FRONT);
   unDatagram.auw16[2] = GetUIRequest_HallStatusIndex_Plus1();
   unDatagram.auc8[6] = GetUIRequest_DefaultCommand();
   unDatagram.auc8[7] = GetUIRequest_ViewDebugData();
   SDATA_WriteDatagram( &gstSData_LocalData,
                        DG_COPB__UI_Req,
                        &unDatagram
                      );
   /* Load the outbound debug datagram to the RX buffer */
   uint8_t ucNetDatagramID = GetNetworkDatagramID_AnyBoard(DG_COPB__UI_Req);
   InsertSharedDatagram_FromPacketSource(&unDatagram, ucNetDatagramID);
   //--------------------------------------
   memset(&unDatagram, 0, sizeof(un_sdata_datagram));
   unDatagram.auc8[0] = GetCarCallRequest(DOOR_REAR);
   SDATA_WriteDatagram( &gstSData_LocalData,
                        DG_COPB__UI_Req_2,
                        &unDatagram
                      );
   /* Load the outbound debug datagram to the RX buffer */
   ucNetDatagramID = GetNetworkDatagramID_AnyBoard(DG_COPB__UI_Req_2);
   InsertSharedDatagram_FromPacketSource(&unDatagram, ucNetDatagramID);

   //--------------------------------------
   memset(&unDatagram, 0, sizeof(un_sdata_datagram));
   switch( GetUIRequest_ViewDebugDataCommand() )
   {
      case VDD__COP_CAN3:
         unDatagram.auc8[5] = Debug_CAN_GetUtilization(0);
         unDatagram.auw16[3] = GetDebugBusOfflineCounter_CAN1();
         break;
      case VDD__COP_CAN4:
         unDatagram.auc8[5] = Debug_CAN_GetUtilization(1);
         unDatagram.auw16[3] = GetDebugBusOfflineCounter_CAN2();
         break;
      case VDD__COP_B_NET:
         unDatagram.auw16[3] = UART_GetRxErrorCount();
         break;
      case VDD__COPB_VERSION:
         unDatagram.auc8[4] = *(pasVersion+0);
         unDatagram.auc8[5] = *(pasVersion+1);
         unDatagram.auc8[6] = *(pasVersion+2);
         unDatagram.auc8[7] = *(pasVersion+3);
         break;
      default: break;
   }

   SDATA_WriteDatagram( &gstSData_LocalData,
                        DG_COPB__Debug,
                        &unDatagram
                       );
   /* Load the outbound debug datagram to the RX buffer */
   ucNetDatagramID = GetNetworkDatagramID_AnyBoard(DG_COPB__Debug);
   InsertSharedDatagram_FromPacketSource(&unDatagram, ucNetDatagramID);
}
/*-----------------------------------------------------------------------------
Unloads fault log updates stored on the MRA FRAM for display on the C4 UI
 -----------------------------------------------------------------------------*/
static void UnloadDatagram_LoggedFaults(void)
{
   if( ( GetLoggedFaults1_RequestIndex_MRA() == GetLoggedFaults2_RequestIndex_MRA() )
    && ( GetLoggedFaults1_RequestIndex_MRA() == GetLoggedFaults3_RequestIndex_MRA() ) )
   {
      uint8_t ucRequestedIndex_Plus1 = GetLoggedFaults1_RequestIndex_MRA();
      if( ucRequestedIndex_Plus1 > NUM_FAULTLOG_ITEMS )
      {
         st_alarm_data stAlarm;
         stAlarm.eAlarmNum = GetLoggedFaults1_FaultNumber_MRA();
         stAlarm.ulTimestamp = GetLoggedFaults1_Timestamp_MRA();
         stAlarm.wSpeed = GetLoggedFaults2_Speed_MRA();
         stAlarm.ulPosition = GetLoggedFaults2_Position_MRA();
         ucRequestedIndex_Plus1 -= NUM_FAULTLOG_ITEMS;
         SetAlarmLogElement(&stAlarm, ucRequestedIndex_Plus1);
      }
      else
      {
         st_fault_data stFault;
         stFault.eFaultNum = GetLoggedFaults1_FaultNumber_MRA();
         stFault.ulTimestamp = GetLoggedFaults1_Timestamp_MRA();
         stFault.wSpeed = GetLoggedFaults2_Speed_MRA();
         stFault.ulPosition = GetLoggedFaults2_Position_MRA();
         stFault.wEncoderSpeed = GetLoggedFaults3_EncoderSpeed_MRA();
         stFault.wCommandSpeed = GetLoggedFaults3_CommandSpeed_MRA();
         stFault.ucCurrentFloor = GetLoggedFaults3_CurrentFloor_MRA();
         stFault.ucDestinationFloor = GetLoggedFaults3_DestinationFloor_MRA();
         SetFaultLogElement(&stFault, ucRequestedIndex_Plus1);
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_DebugDatagrams_MRA()
{
   static uint8_t ucOldStartState;
   uint8_t ucNewStartState = GetDebug_MotionStartState_MRA();
   if(ucOldStartState != ucNewStartState)
   {
      ucOldStartState = ucNewStartState;
   }
   static uint8_t ucOldState;
   uint8_t ucNewState = GetDebug_MotionState_MRA();
   if(ucOldState != ucNewState)
   {
      ucOldState = ucNewState;
   }
   SetModState_MotionState(GetDebug_MotionState_MRA());
   SetModState_MotionStart(GetDebug_MotionStartState_MRA());
   SetModState_MotionStop(GetDebug_MotionStopState_MRA());
   SetModState_MotionPattern(GetDebug_MotionPatternState_MRA());

   SetModState_AutoState(GetDebugStates_OperAutoState_MRA());
   SetModState_FloorLearnState(GetDebugStates_OperLearnState_MRA());
   SetModState_Recall(GetDebugStates_RecallState_MRA());
   SetModState_Counterweight(GetDebugStates_AutoCWState_MRA());
   SetModState_FireSrv(GetDebugStates_FireServiceState_MRA());
   SetModState_FireSrv2(GetDebugStates_FireService2State_MRA());
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_CPLD_Data_MR(void)
{
   // DG_MRA__CPLD_Data0
   if( GetDatagramDirtyBit(DATAGRAM_ID_232) )
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_232);
      uint32_t uiData = GetCPLD_Data0_MRA();
      FPGA_UnloadUIData(uiData, 0, FPGA_LOC__MRA);
      uiData = GetCPLD_Data1_MRA();
      FPGA_UnloadUIData(uiData, 1, FPGA_LOC__MRA);
   }
   // DG_MRA__CPLD_Data1
   if( GetDatagramDirtyBit(DATAGRAM_ID_233) )
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_233);
      uint32_t uiData = GetCPLD_Data2_MRA();
      FPGA_UnloadUIData(uiData, 2, FPGA_LOC__MRA);
      uiData = GetCPLD_Data3_MRA();
      FPGA_UnloadUIData(uiData, 3, FPGA_LOC__MRA);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_MRA()
{
   SetOperation_ClassOfOp(GetOperation2_ClassOfOperation_MRA());
   SetOperation_LearnMode(GetOperation2_LearnMode_MRA());
   SetOperation_AutoMode(GetOperation2_AutoMode_MRA());
   SetOperation_ManualMode(GetOperation2_ManualMode_MRA());
   SetOperation_CurrentFloor(GetOperation1_CurrentFloor_MRA());
   SetOperation_DestinationFloor(GetOperation1_DestinationFloor_MRA());
   SetOperation_RequestedDestination(GetOperation1_RequestedDestination_MRA());
   SetOperation_MotionCmd(GetOperation1_MotionCMD_MRA());

   SetOperation_SpeedLimit(GetOperation2_SpeedLimit_MRA());
   SetOperation_BypassTermLimits(GetOperation3_BypassTermLimits_MR());
   SetOperation_PositionLimit_UP(GetOperation3_PositionLimitUp_MR());
   SetOperation_PositionLimit_DN(GetOperation3_PositionLimitDown_MR());
   //-------------------------------------
   SetBitMap_LatchedCCB(GetCarCalls1_Front_MRA(), 0, DOOR_FRONT);
   SetBitMap_LatchedCCB(GetCarCalls1_Rear_MRA(), 0, DOOR_REAR);

   SetBitMap_LatchedCCB(GetCarCalls2_Front_MRA(), 1, DOOR_FRONT);
   SetBitMap_LatchedCCB(GetCarCalls2_Rear_MRA(), 1, DOOR_REAR);

   SetBitMap_LatchedCCB(GetCarCalls3_Front_MRA(), 2, DOOR_FRONT);
   SetBitMap_LatchedCCB(GetCarCalls3_Rear_MRA(), 2, DOOR_REAR);
   //-------------------------------------
   /* Mapped Inputs */
   SetInputBitMap(GetMappedInputs1_InputBitmap1_MRA(), 0);
   SetInputBitMap(GetMappedInputs1_InputBitmap1_MRA(), 0);
   SetInputBitMap(GetMappedInputs1_InputBitmap2_MRA(), 1);

   SetInputBitMap(GetMappedInputs2_InputBitmap3_MRA(), 2);
   SetInputBitMap(GetMappedInputs2_InputBitmap4_MRA(), 3);

   SetInputBitMap(GetMappedInputs3_InputBitmap5_MRA(), 4);
   SetInputBitMap(GetMappedInputs3_InputBitmap6_MRA(), 5);

   SetInputBitMap(GetMappedInputs4_InputBitmap7_MRA(), 6);
   SetInputBitMap(GetMappedInputs4_InputBitmap8_MRA(), 7);
   //-------------------------------------
   SetOutputBitMap(GetMappedOutputs1_OutputBitmap1_MRA(), 0);
   SetOutputBitMap(GetMappedOutputs1_OutputBitmap2_MRA(), 1);

   SetOutputBitMap(GetMappedOutputs2_OutputBitmap3_MRA(), 2);
   SetOutputBitMap(GetMappedOutputs2_OutputBitmap4_MRA(), 3);

   SetOutputBitMap(GetMappedOutputs3_OutputBitmap5_MRA(), 4);
   SetOutputBitMap(GetMappedOutputs3_OutputBitmap6_MRA(), 5);

   SetOutputBitMap(GetMappedOutputs4_OutputBitmap7_MRA(), 6);
   SetOutputBitMap(GetMappedOutputs4_OutputBitmap8_MRA(), 7);

   //-------------------------------------
   // TODO add COP2 check
   SetLocalOutputs_ThisNode(GetLocalOutputs1_CarOperatingPanel1_MRA());

   //-------------------------------------
   SetMotion_Destination(GetMotion_Destination_MRA());
   SetMotion_Direction(GetMotion_Direction_MRA());
   SetMotion_RunFlag(GetMotion_RunFlag_MRA());
   SetMotion_SpeedCommand(GetMotion_SpeedCMD_MRA());
   SetMotion_Profile(GetMotion_Profile_MRA());

   //-------------------------------------
   UnloadDatagram_LoggedFaults();

   //-------------------------------------
   Unload_DebugDatagrams_MRA();

   Unload_CPLD_Data_MR();
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_CPLD_Data_CT(void)
{
   // DG_CTA__CPLD_Data0
   if( GetDatagramDirtyBit(DATAGRAM_ID_234) )
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_234);
      uint32_t uiData = GetCPLD_Data0_CTA();
      FPGA_UnloadUIData(uiData, 0, FPGA_LOC__CTA);
      uiData = GetCPLD_Data1_CTA();
      FPGA_UnloadUIData(uiData, 1, FPGA_LOC__CTA);
   }
   // DG_CTA__CPLD_Data1
   if( GetDatagramDirtyBit(DATAGRAM_ID_235) )
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_235);
      uint32_t uiData = GetCPLD_Data2_CTA();
      FPGA_UnloadUIData(uiData, 2, FPGA_LOC__CTA);
      uiData = GetCPLD_Data3_CTA();
      FPGA_UnloadUIData(uiData, 3, FPGA_LOC__CTA);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_CPLD_Data_COP(void)
{
   // DG_COPA__CPLD_Data0
   if( GetDatagramDirtyBit(DATAGRAM_ID_236) )
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_236);
      uint32_t uiData = GetCPLD_Data0_COPA();
      FPGA_UnloadUIData(uiData, 0, FPGA_LOC__COPA);
      uiData = GetCPLD_Data1_COPA();
      FPGA_UnloadUIData(uiData, 1, FPGA_LOC__COPA);
   }
   // DG_COPA__CPLD_Data1
   if( GetDatagramDirtyBit(DATAGRAM_ID_237) )
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_237);
      uint32_t uiData = GetCPLD_Data2_COPA();
      FPGA_UnloadUIData(uiData, 2, FPGA_LOC__COPA);
      uiData = GetCPLD_Data3_COPA();
      FPGA_UnloadUIData(uiData, 3, FPGA_LOC__COPA);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UnloadData_COP( void )
{
   Unload_MRA();

   //-------------------------------------
   RTC_SetSyncTime(GetSyncTime_Time_MRB());

   //-------------------------------------
   SetPosition_PositionCount(GetSysPosition_Position_CTA());
   SetPosition_Velocity(GetSysPosition_Velocity_CTA());

   //-------------------------------------
   //todo move
   UpdateBypassInCarStopSwitchFlag();
   uint8_t bBypassInCarStopSwitch = GetBypassInCarStopSwitchFlag();
   SRU_Write_CPLD_Output( enSRU_CPLD_OUTPUTS_X2, bBypassInCarStopSwitch );

   Unload_CPLD_Data_CT();
   Unload_CPLD_Data_COP();
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
