#include "mod.h"
#include "sru.h"
#include "sru_b.h"
#include "sys.h"
#include "operation.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define SMARTRISE_DUPAR_ID (0x534420)

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

static struct st_sdata_control gstSData_AuxNetDupar_COPB;
static struct st_sdata_local gstSData_Config;
// Max number of floors as 96.
static uint32_t auiLatchedCarCalls[NUM_OF_DOORS][BITMAP32_SIZE(96)];
static uint32_t auiSecureBitmap[NUM_OF_DOORS][BITMAP32_SIZE(96)];

struct st_module gstMod_Dupar =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

struct st_dupar_data_buffer
{
   uint8_t bArrivalF_DN;
   uint8_t bArrivalF_UP;
   uint8_t bArrivalR_DN;
   uint8_t bArrivalR_UP;
   uint8_t bTravel_UP;
   uint8_t bTravel_DN;
   uint8_t bStrobe;
   uint8_t bChime;
   uint8_t bFire;
   uint8_t bEMS;

   uint8_t ucCurrentLanding;
   uint8_t ucDestinationLanding;

   enum en_door_states enDoor_Front;
   enum en_door_states enDoor_Rear;

   enum en_mode_auto enAutoMode;
   enum en_mode_manual enManualMode;
};

static struct st_dupar_data_buffer stData;

static CAN_MSG_T gstCAN_MSG_Tx;

#define OFFLINE_COUNTER_LIMIT__10MS       (300) // Reported DUPAR resend rate is 100ms.
static uint16_t uwOfflineCounter_10ms;
/*-----------------------------------------------------------------------------

Byte 0: Elevator Position

Byte 1:
           - Bit0: Front arrival gong up
          - Bit1: Front arrival gong down
          - Bit2: Front arrival lantern up
          - Bit3: Front arrival lantern down
           - Bit4: Travel arrow up
          - Bit5: Travel arrow down
          - Bit6: Elevator on inspection
          - Bit7: Front buzzer

Byte 2:
            - Bit0: Rear arrival gong up
          - Bit1: Rear arrival gong down
          - Bit2: Rear arrival lantern up
          - Bit3: Rear arrival lantern down
           - Bit4: Fire service
          - Bit5: Flash fire hat indicator
          - Bit6: Medical service
          - Bit7: Rear buzzer

Byte 3:
          - Bit0: Emergency Power
          - Bit1: Earthquake operation
          - Bit2: Independent service
          - Bit3: Elevator overload
          - Bit4: Out of service
          - Bit5: N/A
          - Bit6: N/A
          - Bit7: N/A

 -----------------------------------------------------------------------------*/
void SharedData_AuxNetDupar_Init()
{
   int iError;
   int i;

   gstSData_Config.ucLocalNodeID = COP_AUX_NET__COPB;

   gstSData_Config.ucNetworkID = SDATA_NET__AUX;

   gstSData_Config.ucNumNodes = NUM_COP_AUX_NET_NODES;

   gstSData_Config.ucDestNodeID = 0x1F;

   for ( i = 0; i < gstSData_Config.ucNumNodes; ++i )
   {
    iError = SDATA_CreateDatagrams( &gstSData_AuxNetDupar_COPB,
          NUM_AuxNet__COPB_DUPAR_DATAGRAMS );

    if ( iError )
    {
      while ( 1 )
      {
         //SDA REVIEW: issue a fault or some other kind of error recovery
         ;  // TODO: unable to allocate shared data
      }
    }
   }

}
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 50;
   pstThisModule->uwRunPeriod_1ms    = 10;

   for( uint8_t i = 0; i < NUM_AuxNet__COPB_DUPAR_DATAGRAMS; i++ )
   {
     gstSData_AuxNetDupar_COPB.paiResendRate_1ms[ i ] = 100;
   }


   return 0;
}

static void Update_CarCalls( void )
{
   auiLatchedCarCalls[0][0] = GetBitMap_LatchedCCB(0,0);
   auiLatchedCarCalls[0][1] = GetBitMap_LatchedCCB(1,0);
   auiLatchedCarCalls[0][2] = GetBitMap_LatchedCCB(2,0);

   auiLatchedCarCalls[1][0] = GetBitMap_LatchedCCB(0,1);
   auiLatchedCarCalls[1][1] = GetBitMap_LatchedCCB(1,1);
   auiLatchedCarCalls[1][2] = GetBitMap_LatchedCCB(2,1);
}

static void Update_Security( void )
{
   auiSecureBitmap[DOOR_FRONT][0] = ~GetCarCallSecurity_F0() & ( Param_ReadValue_32Bit(enPARAM32__SecureKeyedBitmapF_0) | GetTimedCarCallSecurityBitmap(DOOR_FRONT,0) );
   auiSecureBitmap[DOOR_FRONT][1] = ~GetCarCallSecurity_F1() & ( Param_ReadValue_32Bit(enPARAM32__SecureKeyedBitmapF_1) | GetTimedCarCallSecurityBitmap(DOOR_FRONT,1) );
   auiSecureBitmap[DOOR_FRONT][2] = ~GetCarCallSecurity_F2() & ( Param_ReadValue_32Bit(enPARAM32__SecureKeyedBitmapF_2) | GetTimedCarCallSecurityBitmap(DOOR_FRONT,2) );

   auiSecureBitmap[DOOR_REAR][0] = ~GetCarCallSecurity_R0() & ( Param_ReadValue_32Bit(enPARAM32__SecureKeyedBitmapR_0) | GetTimedCarCallSecurityBitmap(DOOR_REAR,0) );
   auiSecureBitmap[DOOR_REAR][1] = ~GetCarCallSecurity_R1() & ( Param_ReadValue_32Bit(enPARAM32__SecureKeyedBitmapR_1) | GetTimedCarCallSecurityBitmap(DOOR_REAR,1) );
   auiSecureBitmap[DOOR_REAR][2] = ~GetCarCallSecurity_R1() & ( Param_ReadValue_32Bit(enPARAM32__SecureKeyedBitmapR_2) | GetTimedCarCallSecurityBitmap(DOOR_REAR,2) );
}

static void Update_Operation( void )
{
   stData.bArrivalF_UP = GetOutputValue(enOUT_ARV_UP_F);
   stData.bArrivalF_DN = GetOutputValue(enOUT_ARV_DN_F);
   stData.bArrivalR_UP = GetOutputValue(enOUT_ARV_UP_R);
   stData.bArrivalR_DN = GetOutputValue(enOUT_ARV_DN_R);

   if(GetMotion_Direction() > 0)
   {
      stData.bTravel_UP = 1;
      stData.bTravel_DN = 0;
   }
   else if(GetMotion_Direction() < 0)
   {
      stData.bTravel_UP = 0;
      stData.bTravel_DN = 1;
   }

   if(GetOperation_AutoMode() == MODE_A__FIRE1 || GetOperation_AutoMode() == MODE_A__FIRE2)
   {
      stData.bFire = 1;
   }
   else
   {
      stData.bFire = 0;
   }
   if(GetOperation_AutoMode() == MODE_A__EMS1 || GetOperation_AutoMode() == MODE_A__EMS2)
   {
      stData.bEMS = 1;
   }
   else
   {
      stData.bEMS = 0;
   }

   stData.enDoor_Front = GetDoorState_Front();
   stData.enDoor_Rear = GetDoorState_Rear();

   stData.bChime = GetLocalChimeSignal();

   stData.ucCurrentLanding = GetOperation_CurrentFloor();
   stData.ucDestinationLanding = GetOperation_DestinationFloor();

   stData.enAutoMode = GetOperation_AutoMode();
   stData.enManualMode = GetOperation_ManualMode();
}

static void Update_DuparData( void )
{
   Update_CarCalls();
   Update_Security();
   Update_Operation();
}

void UnloadPanelData(CAN_MSG_T *pstRxMsg)
{
   uint8_t ucDestination;
   uint8_t bRearCall;

   uint8_t bDoorOpen_F = pstRxMsg->Data[2];
   uint8_t bDoorOpen_R = pstRxMsg->Data[5];

   uint8_t bDoorClose_F = pstRxMsg->Data[3];
   uint8_t bDoorClose_R = pstRxMsg->Data[6];

   uint8_t bDoorHold_F = pstRxMsg->Data[4];
   uint8_t bDoorHold_R = pstRxMsg->Data[7];

   ucDestination = pstRxMsg->Data[0] & 0x7F;
   bRearCall = pstRxMsg->Data[0] >> 7;

   if(ucDestination & bRearCall)
   {
      uint8_t ucFloor = ucDestination-1;
      uint8_t bSecured = Sys_Bit_Get(&auiSecureBitmap[DOOR_REAR][0], ucFloor);
      if(!bSecured)
      {
         SetCarCall(ucFloor, DOOR_REAR);
      }
   }
   else if(ucDestination)
   {
      uint8_t ucFloor = ucDestination-1;
      uint8_t bSecured = Sys_Bit_Get(&auiSecureBitmap[DOOR_FRONT][0], ucFloor);
      if(!bSecured)
      {
         SetCarCall(ucFloor, DOOR_FRONT);
      }
   }

   if(bDoorOpen_F)
   {
      SetUIRequest_DoorControl(DOOR_UI_CTRL__OPEN_F, GetSRU_Deployment());
   }
   if(bDoorOpen_R)
   {
      SetUIRequest_DoorControl(DOOR_UI_CTRL__OPEN_R, GetSRU_Deployment());
   }
   if(bDoorClose_F)
   {
      SetUIRequest_DoorControl(DOOR_UI_CTRL__CLOSE_F, GetSRU_Deployment());
   }
   if(bDoorClose_R)
   {
      SetUIRequest_DoorControl(DOOR_UI_CTRL__CLOSE_F, GetSRU_Deployment());
   }
   if(bDoorHold_F)
   {
      SetUIRequest_DoorControl(DOOR_UI_CTRL__OPEN_F, GetSRU_Deployment());
   }
   if(bDoorHold_R)
   {
      SetUIRequest_DoorControl(DOOR_UI_CTRL__OPEN_R, GetSRU_Deployment());
   }

   uwOfflineCounter_10ms = 0;
}

static void LoadLatchedCarCalls( void )
{
   un_sdata_datagram unDatagram;
   unDatagram.aui32[0] = auiLatchedCarCalls[0][0];
   unDatagram.aui32[1] = auiLatchedCarCalls[0][1];

   SDATA_WriteDatagram( &gstSData_AuxNetDupar_COPB,
                   DG_AuxNet_COPB__LatchedCarCalls_F0,
                    &unDatagram );

   unDatagram.aui32[0] = auiLatchedCarCalls[0][2];
   unDatagram.aui32[0] = 0;

   SDATA_WriteDatagram( &gstSData_AuxNetDupar_COPB,
                   DG_AuxNet_COPB__LatchedCarCalls_F1,
                    &unDatagram );

   unDatagram.aui32[0] = auiLatchedCarCalls[1][0];
   unDatagram.aui32[1] = auiLatchedCarCalls[1][1];

   SDATA_WriteDatagram( &gstSData_AuxNetDupar_COPB,
                   DG_AuxNet_COPB__LatchedCarCalls_R0,
                    &unDatagram );

   unDatagram.aui32[0] = auiLatchedCarCalls[1][2];
   unDatagram.aui32[1] = 0;

   SDATA_WriteDatagram( &gstSData_AuxNetDupar_COPB,
                   DG_AuxNet_COPB__LatchedCarCalls_R1,
                    &unDatagram );

}

static void LoadSecurity( void )
{
   un_sdata_datagram unDatagram;
   unDatagram.aui32[0] = auiSecureBitmap[0][0];
   unDatagram.aui32[1] = auiSecureBitmap[0][1];

   SDATA_WriteDatagram( &gstSData_AuxNetDupar_COPB,
                   DG_AuxNet_COPB__SecureBitmap_F0,
                    &unDatagram );

   unDatagram.aui32[0] = auiSecureBitmap[0][2];
   unDatagram.aui32[1] = 0;
   SDATA_WriteDatagram( &gstSData_AuxNetDupar_COPB,
                   DG_AuxNet_COPB__SecureBitmap_F1,
                    &unDatagram );

   unDatagram.aui32[0] = auiSecureBitmap[1][0];
   unDatagram.aui32[1] = auiSecureBitmap[1][1];

   SDATA_WriteDatagram( &gstSData_AuxNetDupar_COPB,
                   DG_AuxNet_COPB__SecureBitmap_R0,
                    &unDatagram );

   unDatagram.aui32[0] = auiSecureBitmap[1][2];
   unDatagram.aui32[1] = 0;
   SDATA_WriteDatagram( &gstSData_AuxNetDupar_COPB,
                   DG_AuxNet_COPB__SecureBitmap_R1,
                    &unDatagram );

}

static uint8_t GetModeOfOperation( void )
{
   if(stData.enAutoMode == MODE_A__NORMAL)
   {
      return (1 << 7);
   }
   else if(stData.enAutoMode == MODE_A__SWING)
   {
      return (1 << 6);
   }
   else if(stData.enAutoMode == MODE_A__SABBATH)
   {
      return (1 << 5);
   }
   else if(stData.enAutoMode == MODE_A__INDP_SRV)
   {
      return (1 << 4);
   }
   else if(stData.enAutoMode == MODE_A__EPOWER)
   {
      return (1 << 2);
   }
   else if(stData.enAutoMode == MODE_A__SEISMC)
   {
      return 1;
   }
   else if(stData.enAutoMode == MODE_A__NONE)
   {
      if(stData.enManualMode >= MODE_M__INSP_CT)
      {
         return (1 << 1);
      }
      else
      {
         return 0;
      }
   }
   else
   {
      return 0;
   }
}

static void LoadOperation( void )
{
   un_sdata_datagram unDatagram;
   unDatagram.auw16[0] = GetPosition_Velocity();

   unDatagram.auc8[2] = stData.ucCurrentLanding + 1;
   unDatagram.auc8[3] = stData.ucDestinationLanding + 1;

   unDatagram.auc8[4] = (stData.bChime << 7) |
                  (stData.bTravel_UP << 6) |
                  (stData.bTravel_DN << 5) |
                  (stData.bArrivalF_UP << 4) |
                  (stData.bArrivalF_DN << 3) |
                  (stData.bArrivalR_UP << 2) |
                  (stData.bArrivalR_DN << 1) |
                  0;

   unDatagram.auc8[5] = GetModeOfOperation();
   unDatagram.auc8[6] = stData.enDoor_Front;
   unDatagram.auc8[7] = stData.enDoor_Rear;

   SDATA_WriteDatagram( &gstSData_AuxNetDupar_COPB,
                   DG_AuxNet_COPB__CarOperation,
                   &unDatagram );

}

static void LoadEmergencyStatus()
{
   un_sdata_datagram unDatagram;
   unDatagram.aui32[0] = 0;
   unDatagram.aui32[1] = 0;

   if(stData.enAutoMode == MODE_A__FIRE1)
   {
      unDatagram.auc8[0] = 0x1;
   }
   else if(stData.enAutoMode == MODE_A__FIRE2)
   {
      unDatagram.auc8[0] = 0x2;
   }

   if(stData.bFire)
   {
      if( GetEmergencyBit( EmergencyBF_FirePhaseI_RecallToAltFloor ) )
      {
         unDatagram.auc8[1] = 0x2;
      }
      else
      {
         unDatagram.auc8[1] = 0x1;
      }
   }

   if(stData.enAutoMode == MODE_A__EMS1)
   {
      unDatagram.auc8[2] = 0x1;
   }
   else if(stData.enAutoMode == MODE_A__EMS2)
   {
      unDatagram.auc8[2] = 0x2;
   }

   SDATA_WriteDatagram( &gstSData_AuxNetDupar_COPB,
                   DG_AuxNet_COPB__EmergencyStatus,
                    &unDatagram );
}

static void LoadData( void )
{
   LoadLatchedCarCalls();
   LoadSecurity();
   LoadOperation();
   LoadEmergencyStatus();
}

static void SetDatagramMessageID_COP(uint8_t ucLocalDatagramID)
{
   gstCAN_MSG_Tx.DLC = 8;

   gstCAN_MSG_Tx.ID = ( 1 << 30 ) |
                  ( SMARTRISE_DUPAR_ID ) |
                  ( ucLocalDatagramID );

}

static void Transmit( void )
{
   un_sdata_datagram unDatagram;
   int iError = 1;
   uint8_t ucDatagramID;
   uint16_t uwDatagramID_Plus1 = 0;

   uwDatagramID_Plus1 = SDATA_GetNextDatagramToSendIndex_Plus1(
         &gstSData_AuxNetDupar_COPB);
   if(uwDatagramID_Plus1)
   {
     ucDatagramID = uwDatagramID_Plus1 - 1;
     if ( ucDatagramID < NUM_AuxNet__COPB_DUPAR_DATAGRAMS )
     {
       SDATA_ReadDatagram( &gstSData_AuxNetDupar_COPB, ucDatagramID,
              &unDatagram );

       memcpy( gstCAN_MSG_Tx.Data, unDatagram.auc8,
              sizeof( unDatagram.auc8 ) );

       SetDatagramMessageID_COP(ucDatagramID);

       iError =  !CAN1_LoadToRB( &gstCAN_MSG_Tx );

       if ( iError )
       {
         SDATA_DirtyBit_Set(  &gstSData_AuxNetDupar_COPB, ucDatagramID );

         if ( gstSData_AuxNetDupar_COPB.uwLastSentIndex )
         {
            --gstSData_AuxNetDupar_COPB.uwLastSentIndex;
         }
         else
         {
             gstSData_AuxNetDupar_COPB.uwLastSentIndex =
                   gstSData_AuxNetDupar_COPB.uiNumDatagrams - 1;
         }
       }
     }
   }
}


static void CheckForPanelOffline(void)
{
   if( uwOfflineCounter_10ms >= OFFLINE_COUNTER_LIMIT__10MS )
   {
      SetAlarm(ALM__DUPAR_COP_OFFLINE);
   }
   else
   {
      uwOfflineCounter_10ms++;
   }
}
static uint32_t Run( struct st_module *pstThisModule )
{
   if(Param_ReadValue_1Bit(enPARAM1__DuparCOP_Enable))
   {
      Update_DuparData();
      LoadData();
      Transmit();
      CheckForPanelOffline();
   }

   return 0;
}
