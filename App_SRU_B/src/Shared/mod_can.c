/******************************************************************************
 *
 * @file
 * @brief
 * @version  V1.00
 * @date
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "sru_b.h"
#include "sys.h"

#include <string.h>
#include "GlobalData.h"
#include "ring_buffer.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_CAN =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define LIMIT_UNLOAD_CAN_CYCLES       (16)

#define EXTRACT_NET_ID(x)      (( x >> 25 ) & 0x0F )
#define EXTRACT_SRC_ID(x)      (( x >> 20 ) & 0x1F )
#define EXTRACT_DEST_ID(x)     (( x >> 15 ) & 0x1F )
#define EXTRACT_DATAGRAM_ID(x) (( x >> 10 ) & 0x1F )

#define DUPAR_SMARTRISE_ID ((0x534410) | (1 << 30))

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *-----------------------------------------------------------------------------*/

st_position_frame channel1_0x11;
st_position_frame channel1_0x12;
static uint16_t uwBusErrorCounter_CAN1;
static uint16_t uwBusErrorCounter_CAN2;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Returns bus error counter
 -----------------------------------------------------------------------------*/
uint16_t GetDebugBusOfflineCounter_CAN1()
{
   return uwBusErrorCounter_CAN1;
}
uint16_t GetDebugBusOfflineCounter_CAN2()
{
   return uwBusErrorCounter_CAN2;
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = 5; // Should be as fast as the fastest producer.

   return 0;
}

static void UnloadCAN_CTB( void )
{
    CAN_MSG_T tmp;

    // Read data from ring buffer till it is empty
    // Devices on CAN1: LWD
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN1_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
          enum en_sdata_networks eNet = EXTRACT_NET_ID(tmp.ID);
          uint8_t ucSource = EXTRACT_SRC_ID(tmp.ID);
          uint8_t ucDest = EXTRACT_DEST_ID(tmp.ID);
          uint8_t ucDatagramID = EXTRACT_DATAGRAM_ID(tmp.ID);
          if( ( eNet == SDATA_NET__AUX )
           && ( ucDest == 0x1F ) // All nodes
           && ( ucSource == MR_AUX_NET__LW )
           && ( ucDatagramID == DG_AuxNet_LW__Response ) )
          {
             un_sdata_datagram unDatagram;
             memcpy( unDatagram.auc8, tmp.Data, sizeof( tmp.Data ) );
             LoadWeigher_UnloadDatagram(&unDatagram);
          }
       }
    }

    // Read data from ring buffer till it is empty
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN2_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
           switch ( tmp.ID )
           {
               case 0x11:
                   channel1_0x11.bNewMsg = 1;
                   for ( uint8_t i = 0; i < tmp.DLC; i++ )
                   {
                       channel1_0x11.Data[i] = tmp.Data[i];
                   }
                   break;

               case 0x12:
                   channel1_0x12.bNewMsg = 1;
                   for ( uint8_t i = 0; i < tmp.DLC; i++ )
                   {
                       channel1_0x12.Data[i] = tmp.Data[i];
                   }
                   break;

               default:
//                  SetAlarm( ALARM__SYS_DEBUG, SUBALARM_SYS_DEBUG__CAN_CTB_2 );
                   break;
           }
       }
    }
}
static void UnloadCAN_COPB( void )
{
    CAN_MSG_T tmp;

    // Read data from ring buffer till it is empty
    // Devices on CAN1: Dupar COP
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN1_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
          un_sdata_datagram unDatagram;
          memcpy( unDatagram.auc8, tmp.Data, sizeof( tmp.Data ) );
           switch ( tmp.ID )
           {
              case DUPAR_SMARTRISE_ID:
                 UnloadPanelData(&tmp);
                 break;
              default:
//                  SetAlarm( ALARM__SYS_DEBUG, SUBALARM_SYS_DEBUG__CAN_COPB_1 );
                 break;
           }
       }
    }
}
/*-----------------------------------------------------------------------------
   Check for CAN Bus offline and reset if offline
 -----------------------------------------------------------------------------*/
static void CheckFor_CANBusOffline()
{
   if( Chip_CAN_GetGlobalStatus( LPC_CAN1 ) & CAN_GSR_BS ) // Bus offline if 1
   {
      Chip_CAN_SetMode( LPC_CAN1, CAN_RESET_MODE, ENABLE );
      __NOP();
      __NOP();
      __NOP();
      Chip_CAN_SetMode( LPC_CAN1, CAN_RESET_MODE, DISABLE );
      en_alarms eAlarm = (GetSRU_Deployment() == enSRU_DEPLOYMENT__CT)
                       ? ALM__CT_CAN_RESET_3:ALM__COP_CAN_RESET_3;
      SetAlarm(eAlarm);
   }

   if( Chip_CAN_GetGlobalStatus( LPC_CAN2 ) & CAN_GSR_BS ) // Bus offline if 1
   {
      Chip_CAN_SetMode( LPC_CAN2, CAN_RESET_MODE, ENABLE );
      __NOP();
      __NOP();
      __NOP();
      Chip_CAN_SetMode( LPC_CAN2, CAN_RESET_MODE, DISABLE );
      en_alarms eAlarm = (GetSRU_Deployment() == enSRU_DEPLOYMENT__CT)
                       ? ALM__CT_CAN_RESET_4:ALM__COP_CAN_RESET_4;
      SetAlarm(eAlarm);
   }
}
/*-----------------------------------------------------------------------------
   Updates the CAN bus error counters viewed via View Debug Data menus
 -----------------------------------------------------------------------------*/
static void UpdateDebugBusErrorCounter()
{
   static uint8_t ucLastCountRx_CAN1;
   static uint8_t ucLastCountTx_CAN1;
   static uint8_t ucLastCountRx_CAN2;
   static uint8_t ucLastCountTx_CAN2;
   uint32_t uiStatus = Chip_CAN_GetGlobalStatus( LPC_CAN1 );
   uint8_t ucCountRx = (uiStatus >> 16) & 0xFF;
   uint8_t ucCountTx = (uiStatus >> 24) & 0xFF;
   if( ( ucCountRx > ucLastCountRx_CAN1 )
    || ( ucCountTx > ucLastCountTx_CAN1 ))
   {
       uwBusErrorCounter_CAN1++;
   }
   ucLastCountRx_CAN1 = ucCountRx;
   ucLastCountTx_CAN1 = ucCountTx;

   uiStatus = Chip_CAN_GetGlobalStatus( LPC_CAN2 );
   ucCountRx = (uiStatus >> 16) & 0xFF;
   ucCountTx = (uiStatus >> 24) & 0xFF;
   if( ( ucCountRx > ucLastCountRx_CAN2 )
    || ( ucCountTx > ucLastCountTx_CAN2 ))
   {
       uwBusErrorCounter_CAN2++;
   }
   ucLastCountRx_CAN2 = ucCountRx;
   ucLastCountTx_CAN2 = ucCountTx;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
    // This is the only place the can periphs are init.
    switch(GetSRU_Deployment())
    {
        case enSRU_DEPLOYMENT__CT:
            UnloadCAN_CTB();
            break;
        case enSRU_DEPLOYMENT__COP:
            UnloadCAN_COPB();
            break;
        default:
            break;
    }
    UpdateDebugBusErrorCounter();
    CheckFor_CANBusOffline();

    return 0;
}

