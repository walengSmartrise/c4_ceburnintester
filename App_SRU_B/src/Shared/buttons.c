/******************************************************************************
 *
 * @file     buttons.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     23, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "buttons.h"

#include "sru_b.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static uint8_t gucCurrentKeyPress;

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
enum en_keypresses Button_GetKeypress( void )
{
   enum en_keypresses enCurrentKeyPress = gucCurrentKeyPress;

   // Consume the keypress by reading it.
   gucCurrentKeyPress = enKEYPRESS_NONE;

   return enCurrentKeyPress;
}

/*-----------------------------------------------------------------------------

   Receives a buttom press that was read from the hardware and converts it to
   an application level keypress.

 -----------------------------------------------------------------------------*/
void Button_PutKeypress( enum en_sru_buttons enSRU_Button )
{
   // Don't overwrite a keypress that hasn't been consumed yet.
   if ( gucCurrentKeyPress == enKEYPRESS_NONE )
   {
      uint8_t ucKeyPress;

      switch( enSRU_Button )
      {
         case enSRU_BUTTON_UP: ucKeyPress = enKEYPRESS_UP; break;
         case enSRU_BUTTON_DOWN: ucKeyPress = enKEYPRESS_DOWN; break;
         case enSRU_BUTTON_LEFT: ucKeyPress = enKEYPRESS_LEFT; break;
         case enSRU_BUTTON_RIGHT: ucKeyPress = enKEYPRESS_RIGHT; break;
         case enSRU_BUTTON_ENTER: ucKeyPress = enKEYPRESS_ENTER; break;

         default: ucKeyPress = enKEYPRESS_NONE;
      }

      gucCurrentKeyPress = ucKeyPress;
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
