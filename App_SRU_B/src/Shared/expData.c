/******************************************************************************
 * @author   Keith Soneda
 * @version  V1.00
 * @date     6, Sept 2018
 *
 * @note    Tracks the online state of master I/O boards
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "sru.h"
#include "sys.h"

#include <stdint.h>
#include <string.h>
#include "riser.h"
#include "expData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint16_t auwMasterOfflineTimers_ms[7] =
{
      RISER_OFFLINE_COUNT_MS__UNUSED,
      RISER_OFFLINE_COUNT_MS__UNUSED,
      RISER_OFFLINE_COUNT_MS__UNUSED,
      RISER_OFFLINE_COUNT_MS__UNUSED,
      RISER_OFFLINE_COUNT_MS__UNUSED,
      RISER_OFFLINE_COUNT_MS__UNUSED,
      RISER_OFFLINE_COUNT_MS__UNUSED,
};

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void ExpData_UpdateMasterOfflineTimers( uint16_t uwRunPeriod_1ms )
{
   for(uint8_t i = 0; i < 7; i++)
   {
      /* Clear timer of new packet from an IO master has been received */
      if( GetDatagramDirtyBit(DATAGRAM_ID_29+i) || GetDatagramDirtyBit(DATAGRAM_ID_198+i) )
      {
         ClrDatagramDirtyBit(DATAGRAM_ID_29+i);
         ClrDatagramDirtyBit(DATAGRAM_ID_198+i);
         auwMasterOfflineTimers_ms[i] = 0;
      }
      else if( auwMasterOfflineTimers_ms[i] < MASTER_IO_OFFLINE_LIMIT_MS )
      {
         auwMasterOfflineTimers_ms[i] += uwRunPeriod_1ms;
      }
   }
}
uint8_t ExpData_GetOnlineFlag( uint8_t ucMasterIndex, uint8_t ucBoardIndex )
{
   uint8_t bOnline = 0;
   if( auwMasterOfflineTimers_ms[ucMasterIndex] < MASTER_IO_OFFLINE_LIMIT_MS )
   {
      bOnline = GetExpansionOnlineFlag(ucMasterIndex, ucBoardIndex);
   }
   return bOnline;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
