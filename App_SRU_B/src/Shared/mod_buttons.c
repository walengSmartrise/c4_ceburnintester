/******************************************************************************
 *
 * @file     mod_buttons.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     23, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "buttons.h"
#include "sru_b.h"
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Buttons =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{

    //SDA REVIEW: Why is this comment here. it can be deleted.
   // I tried running this module every 100 ms and it worked pretty well but
   // was a liitle unresponsive if you pressed a key really quickly.
   pstThisModule->uwInitialDelay_1ms = 50;
   pstThisModule->uwRunPeriod_1ms = 50;

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static uint8_t aucButtonHeldCountup_100ms[ SRU_NUM_BUTTONS ];
   static uint8_t abButtonDown_Old[ SRU_NUM_BUTTONS ];

   for ( int iButtonIndex = 0; iButtonIndex < SRU_NUM_BUTTONS; ++iButtonIndex )
   {
      uint_fast8_t bButtonDown = SRU_Read_Button( iButtonIndex );

      // Check if button just changed position -- was pressed or released.
      if ( abButtonDown_Old[ iButtonIndex ] != bButtonDown )
      {
         abButtonDown_Old[ iButtonIndex ] = bButtonDown;

         aucButtonHeldCountup_100ms[ iButtonIndex ] = 0;

         if ( bButtonDown )  // is button currently pressed down?
         {
            Button_PutKeypress( iButtonIndex );
         }
      }
      else  // button is in same position as 50 ns ago
      {
         if ( bButtonDown )  // is button currently pressed down?
         {
            // Check if button has been held for at least 0.75 seconds (15 * 50ms).
            if ( aucButtonHeldCountup_100ms[ iButtonIndex ] < 15 )
            {
               ++aucButtonHeldCountup_100ms[ iButtonIndex ];
            }
            else
            {
               // Button has been held for at least 750 ms. Log the keypress
               // and bump the held countup down to 14. This will cause the key
               // to be relogged on every 2nd call of Run() until it is released
               // giving a repeat cycle of 100 ms or the apprearance of being
               // pressed 10 time a second if held for more than 0.75 seconds.
               aucButtonHeldCountup_100ms[ iButtonIndex ] = 14;

               Button_PutKeypress( iButtonIndex );
            }
         }
      }
   }


   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
