/******************************************************************************
 *
 * @file     mod_can_test.c
 * @brief
 * @version  V1.00
 * @date     30, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "sru.h"
#include "sru_b.h"
#include "sys.h"
#include "NTS_CT.h"
#include "decel.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run ( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_NTS =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
NTS access functions
 *----------------------------------------------------------------------------*/
//SDA REVIEW: multiple return statment
uint8_t NTS_GetState( void )
{
   if(GetSRU_Deployment() == enSRU_DEPLOYMENT__CT)
   {
      return NTS_GetState_CT();
   }
   return 0;
}
//SDA REVIEW: multiple return statment
uint8_t NTS_GetAlarm( void )
{
   if(GetSRU_Deployment() == enSRU_DEPLOYMENT__CT)
   {
      return NTS_GetAlarm_CT();
   }
   return 0;
}
//SDA REVIEW: multiple return statment
uint8_t NTS_GetFault( void )
{
   if(GetSRU_Deployment() == enSRU_DEPLOYMENT__CT)
   {
      return NTS_GetFault_CT();
   }
   return 0;

}
/*--------------------------------------------------------------------------
   For both the up and down directions:
      Takes 10 evenly spaced time index points from the overall ramp down time.
      These points are offset by half of their difference.
      Finds the speed and absolute position at those points and updates system parameters.

--------------------------------------------------------------------------*/
static void UpdateNTSTripPoints( Motion_Profile eProfile )
{
   if(!Param_ReadValue_1Bit(enPARAM1__DEBUG_DisableUpdateNTS))
   {
      Motion_Profile eSavedProfile = GetMotion_Profile();
      SetMotion_Profile(eProfile);
      //Calculate the decel pattern for max speed to get the slowdown distance
      if( eProfile == MOTION_PROFILE__3 )
      {
         Decel_GenerateDecelRun(Param_ReadValue_16Bit(enPARAM16__EPowerSpeed_fpm)/60.0f);
      }
      else
      {
         Decel_GenerateDecelRun(gpstDecelMotionCtrl->flContractSpeed_FPS);
      }
      uint32_t uiTimeBetweenTripPoints = gpstDecelMotionCtrl->stRunParameters.uiPatternDecelTime/NUM_NTS_TRIP_POINTS;
      uint32_t uiTimeOffset =  uiTimeBetweenTripPoints/2;
      for(uint8_t i = 0; i < NUM_NTS_TRIP_POINTS; i++)
      {
         uint32_t uiTimeIndex = (uiTimeBetweenTripPoints * i) + uiTimeOffset;
         uint32_t uiRelativePosition = Decel_GetDecelPosition(uiTimeIndex) + ( gpstDecelCurveParameters + eProfile )->uwLevelingDistance;

         uint16_t uwTripSpeed = Decel_GetDecelCommandSpeed(uiTimeIndex);

         if( eProfile == MOTION_PROFILE__1 )
         {
            Param_WriteValue_16Bit(enPARAM16__NTS_POS_P1_0 + i, uiRelativePosition/NTS_POSITION_SCALING);
            Param_WriteValue_16Bit(enPARAM16__NTS_VEL_P1_0 + i, uwTripSpeed);
         }
         else if( eProfile == MOTION_PROFILE__2 )
         {
            Param_WriteValue_16Bit(enPARAM16__NTS_POS_P2_0 + i, uiRelativePosition/NTS_POSITION_SCALING);
            Param_WriteValue_16Bit(enPARAM16__NTS_VEL_P2_0 + i, uwTripSpeed);
         }
         else if( eProfile == MOTION_PROFILE__3 )
         {
            Param_WriteValue_16Bit(enPARAM16__NTS_POS_P3_0 + i, uiRelativePosition/NTS_POSITION_SCALING);
            Param_WriteValue_16Bit(enPARAM16__NTS_VEL_P3_0 + i, uwTripSpeed);
         }
         else if( eProfile == MOTION_PROFILE__4 )
         {
            Param_WriteValue_16Bit(enPARAM16__NTS_POS_P4_0 + i, uiRelativePosition/NTS_POSITION_SCALING);
            Param_WriteValue_16Bit(enPARAM16__NTS_VEL_P4_0 + i, uwTripSpeed);
         }
      }
      SetMotion_Profile(eSavedProfile);
      Decel_GenerateDecelRun(gpstDecelMotionCtrl->flContractSpeed_FPS);
   }
}
/*-----------------------------------------------------------------------------
   Update curve parameters & ETS/NTS points
 -----------------------------------------------------------------------------*/
#define SCURVE_UPDATING_HOLD_TIME_5MS     (1750)
static void UpdateMotionProfiles(uint8_t bStartup)
{
   static uint16_t ucCountdown_5ms;
   for( Motion_Profile i = 0; i < NUM_MOTION_PROFILES; ++i )
   {
      if( Decel_UpdateParameters( i ) )
      {
         Decel_UpdateCurveLimits( i );
         if( !Decel_CheckForInvalidSCurve( i ) && ( i != MOTION_PROFILE__2 ) )
         {
            UpdateNTSTripPoints( i );
         }
         ucCountdown_5ms += SCURVE_UPDATING_HOLD_TIME_5MS;
      }
   }

   /* TODO rm after reducing parameter sync time. This is only an issue when changing contract speed for acceptance tests */
   if(bStartup)
   {
      ucCountdown_5ms = 0;
   }
   else if(ucCountdown_5ms)
   {
      ucCountdown_5ms--;
      SetFault(FLT__SCURVE_UPDATING);
   }
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   // Execute the init depending on the deployment selected.
   UpdateMotionProfiles(1);
   NTS_CT_Init(pstThisModule);

   // This is the time the module will init and also how often it will run.
   pstThisModule->uwInitialDelay_1ms = 200;
   pstThisModule->uwRunPeriod_1ms = 5;

   return 0;
}

/*-----------------------------------------------------------------------------


 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   /* Original run rate was 25ms. UpdateMotionProfiles needs higher update rate to increase resposiveness so scaling occurs here */
   UpdateMotionProfiles(0);

   static uint8_t ucCounter_5ms;
   if(++ucCounter_5ms >= 5)
   {
      ucCounter_5ms = 0;
      NTS_CT_Run();
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
