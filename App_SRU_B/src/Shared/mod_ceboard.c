/******************************************************************************
 *
 * @file
 * @brief
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_b.h"
#include "sru.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
#include "motion.h"
#include "contributors.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_module gstMod_CEboards =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define CED_TARGET__IN_CAR                  (0x00)
#define CED_TARGET__HALL_DISPLAY            (0xFE)
#define CED_TARGET__ALL_DISPLAY             (0xFD)
#define CED_TARGET__FLOOR_INFO              (0xFF)
#define CED_TIMES_TO_PLAY_CAR_ID_CODE       (2)
#define CED_DISPLAY_FLASH_TIME_MS           (3000)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
struct st_ce_data_buffer
{
   uint8_t bArrivalF_DN;
   uint8_t bArrivalF_UP;
   uint8_t bArrivalR_DN;
   uint8_t bArrivalR_UP;
   uint8_t bTravel_UP;
   uint8_t bTravel_DN;
   uint8_t bStrobe;
   uint8_t bChime;
   uint8_t bFire;
   uint8_t ucMessageNum;
   uint8_t ucSlot;
   /* Current Floor */
   uint8_t ucCurrentCharL;//left most PI char
   uint8_t ucCurrentCharM;//mid PI char
   uint8_t ucCurrentCharR;//right most PI char
   /* Destination Floor */
   uint8_t ucDestCharL;//left most PI char
   uint8_t ucDestCharM;//mid PI char
   uint8_t ucDestCharR;//right most PI char

};
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ce_data_buffer stCE_DataBuffer;

/*----------------------------------------------------------------------------
Mess
Numb  ASCII level comment
0  [[[   x  no message
1   FM   3  Fire Main
2   FA   2  Fire Alternate
3   ND   1  Nudging
4   IN   1  Independent Service
5   OL   1  Over load
6   EP   1  Emergency Power
7   IS   1  Inspection Service
8   SS   1  Seismic Sensor
 *----------------------------------------------------------------------------*/
enum en_ce_messages
{
   CE_MSG__NONE,
   CE_MSG__FIRE_MAIN,//Enabled by DIP5, MF
   CE_MSG__FIRE_ALT,//Enabled by DIP5, AF
   CE_MSG__NUDGE,//Enabled by DIP5, DN
   CE_MSG__INDP_SRV,//Enabled by DIP5, NI
   CE_MSG__OVERLOAD,//Enabled by DIP5, LO
   CE_MSG__EPOWER,//Enabled by DIP5, PE
   CE_MSG__INSP,//Enabled by DIP5, SI
   CE_MSG__SEISMIC,//Enabled by DIP5, SS
   CE_MSG__OOS,//Enabled by DIP5, OS
   CE_MSG__FF2,//Enabled by DIP5, F2

   NUM_CE_MSG
};
/*Message levels From CE documentation*/
static const uint8_t gaucMsgLevel[NUM_CE_MSG] =
{
      0,
      3,
      2,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
};

/* Message strings from CE documentation */
static const char * gpas_MsgStrings[NUM_CE_MSG] =
{
      "[[[",
      "FM",
      "FA",
      "ND",
      "IN",
      "OL",
      "EP",
      "IS",
      "SS",
      "OS",
      "F2",
};
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

   Egg: In car check for seeing if the elevator you're in is Smartrise C4.

   Press DOB & DCB together 3 times in succession, then hold both in for 3 seconds.
 -----------------------------------------------------------------------------*/
static uint8_t bSmartriseFlag;
static char aucSmartriseString[4];
static void UpdateSmartriseFlag(void)
{
   static char const * const pasStrings[ NUM_C4C ] =
   {
      C4C_TABLE(EXPAND_C4C_TABLE_AS_STRING_ARRAY)
   };
   static uint8_t bLastActive;
   static uint8_t ucToggleCounter;
   static uint32_t uiResetTimer_5ms;
   static uint16_t uwHoldTimer_5ms;
   static uint8_t ucState;
   if( GetOperation_AutoMode() == MODE_A__NORMAL )
   {
      if(bSmartriseFlag)
      {
         if(++uwHoldTimer_5ms >= FLAG_SEQUENCE_FLASH_RATE_5MS)
         {
            uwHoldTimer_5ms = 0;
            bLastActive ^= 1;
            ucToggleCounter = (bLastActive) ? ucToggleCounter+1:ucToggleCounter;
         }

         if(!ucState)
         {
            if(bLastActive)
            {
               aucSmartriseString[2] = 'R';
               aucSmartriseString[1] = 'S';
               aucSmartriseString[0] = ' ';
            }
            else
            {
               aucSmartriseString[2] = ' ';
               aucSmartriseString[1] = ' ';
               aucSmartriseString[0] = ' ';
            }

            if(ucToggleCounter >= FLAG_SEQUENCE_CYCLES_TO_FLASH)
            {
               ucToggleCounter = 0;
               ucState = 1;
               uiResetTimer_5ms = 0;
            }
         }
         else if(ucState < NUM_C4C+1)
         {
            if(bLastActive)
            {
               aucSmartriseString[2] = *(pasStrings[ucState-1]+2);
               aucSmartriseString[1] = *(pasStrings[ucState-1]+1);
               aucSmartriseString[0] = *(pasStrings[ucState-1]+0);
            }
            else
            {
               aucSmartriseString[2] = ' ';
               aucSmartriseString[1] = ' ';
               aucSmartriseString[0] = ' ';
            }

            if(ucToggleCounter >= FLAG_SEQUENCE_CYCLES_TO_FLASH/2)
            {
               ucToggleCounter = 0;
               ucState++;
            }
         }
         else
         {
            ucState = 0;
            bSmartriseFlag = 0;
         }
      }
      else
      {
         uint8_t bActive = GetInputValue(enIN_DOB_F) && GetInputValue(enIN_DCB_F);
         bActive |= GetInputValue(enIN_DOB_R) && GetInputValue(enIN_DCB_R);
         if( bActive )
         {
            uiResetTimer_5ms = 0;
            if(!bLastActive)
            {
               ucToggleCounter++;
            }
            else if( ucToggleCounter > FLAG_SEQUENCE_PRESS_LIMIT )
            {
               if( ++uwHoldTimer_5ms >= FLAG_SEQUENCE_HOLD_TIME_5MS )
               {
                  bSmartriseFlag = 1;
                  uiResetTimer_5ms = 0;
                  ucToggleCounter = 0;
                  uwHoldTimer_5ms = 0;
                  ucState = 0;
               }
            }
         }
         else
         {
            uwHoldTimer_5ms = 0;
         }
         bLastActive = bActive;

         if(++uiResetTimer_5ms >= FLAG_SEQUENCE_RESET_LIMIT_5MS)
         {
            uiResetTimer_5ms = 0;
            ucToggleCounter = 0;
            uwHoldTimer_5ms = 0;
         }
      }
   }
}
/*-----------------------------------------------------------------------------
(*ps) = PI Left Char
(*ps+1) = PI Right Char
 -----------------------------------------------------------------------------*/
char * Get_PI_Label( uint8_t ucFloor )
{
   static char aucChar[4];
   if(bSmartriseFlag)
   {
      return &aucSmartriseString[0];
   }
   else
   {
      uint32_t ulData = Param_ReadValue_24Bit(enPARAM24__PI_0 + ucFloor);
      aucChar[2] = ulData & 0xFF;
      aucChar[1] = ulData >> 8 & 0xFF;
      aucChar[0] = ulData >> 16 & 0xFF;
      aucChar[3] = 0;
      return &aucChar[0];
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define DOUBLE_CHIME_ARRIVAL_DOWN_ON_TIME_5MS         (100)
#define DOUBLE_CHIME_ARRIVAL_DOWN_OFF_TIME_5MS        (100)
static void Update_ArrivalArrows(void)
{
   static uint8_t aucDoubleChimeDownCounter_ON_5ms[NUM_OF_DOORS];
   static uint8_t aucDoubleChimeDownCounter_OFF_5ms[NUM_OF_DOORS];
   stCE_DataBuffer.bArrivalF_UP = GetOutputValue(enOUT_ARV_UP_F);
   stCE_DataBuffer.bArrivalR_UP = GetOutputValue(enOUT_ARV_UP_R);
   if( Param_ReadValue_1Bit(enPARAM1__DoubleChimeOnDown) )
   {
      uint8_t bArrivalF_DN = 0;
      uint8_t bArrivalR_DN = 0;
      if( GetOutputValue(enOUT_ARV_DN_F) )
      {
         if( aucDoubleChimeDownCounter_ON_5ms[DOOR_FRONT] >= DOUBLE_CHIME_ARRIVAL_DOWN_ON_TIME_5MS )
         {
            if( aucDoubleChimeDownCounter_OFF_5ms[DOOR_FRONT] >= DOUBLE_CHIME_ARRIVAL_DOWN_OFF_TIME_5MS )
            {
               bArrivalF_DN = 1;
            }
            else
            {
               aucDoubleChimeDownCounter_OFF_5ms[DOOR_FRONT]++;
            }
         }
         else
         {
            aucDoubleChimeDownCounter_ON_5ms[DOOR_FRONT]++;
            bArrivalF_DN = 1;
         }
      }
      else
      {
         aucDoubleChimeDownCounter_ON_5ms[DOOR_FRONT] = 0;
         aucDoubleChimeDownCounter_OFF_5ms[DOOR_FRONT] = 0;
      }

      if( GetOutputValue(enOUT_ARV_DN_R) )
      {
         if( aucDoubleChimeDownCounter_ON_5ms[DOOR_REAR] >= DOUBLE_CHIME_ARRIVAL_DOWN_ON_TIME_5MS )
         {
            if( aucDoubleChimeDownCounter_OFF_5ms[DOOR_REAR] >= DOUBLE_CHIME_ARRIVAL_DOWN_OFF_TIME_5MS )
            {
               bArrivalR_DN = 1;
            }
            else
            {
               aucDoubleChimeDownCounter_OFF_5ms[DOOR_REAR]++;
            }
         }
         else
         {
            aucDoubleChimeDownCounter_ON_5ms[DOOR_REAR]++;
            bArrivalR_DN = 1;
         }
      }
      else
      {
         aucDoubleChimeDownCounter_ON_5ms[DOOR_REAR] = 0;
         aucDoubleChimeDownCounter_OFF_5ms[DOOR_REAR] = 0;
      }
      stCE_DataBuffer.bArrivalF_DN = bArrivalF_DN;
      stCE_DataBuffer.bArrivalR_DN = bArrivalR_DN;
   }
   else
   {
      stCE_DataBuffer.bArrivalF_DN = GetOutputValue(enOUT_ARV_DN_F);
      stCE_DataBuffer.bArrivalR_DN = GetOutputValue(enOUT_ARV_DN_R);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_TravelArrows(void)
{
   stCE_DataBuffer.bTravel_UP =1;
//   if(GetMotion_Direction() > 0)
//   {
//     stCE_DataBuffer.bTravel_UP = 1;
//     stCE_DataBuffer.bTravel_DN = 0;
//   }
//   else if(GetMotion_Direction() < 0)
//   {
//     stCE_DataBuffer.bTravel_UP = 0;
//     stCE_DataBuffer.bTravel_DN = 1;
//   }
//   else if( !Param_ReadValue_1Bit(enPARAM1__DisableIdleTravelArrows) )
//   {
//      if( GetOutputValue(enOUT_ARV_UP_F) || GetOutputValue(enOUT_ARV_UP_R) )
//      {
//        stCE_DataBuffer.bTravel_UP = 1;
//        stCE_DataBuffer.bTravel_DN = 0;
//      }
//      else if( GetOutputValue(enOUT_ARV_DN_F) || GetOutputValue(enOUT_ARV_DN_R) )
//      {
//        stCE_DataBuffer.bTravel_UP = 0;
//        stCE_DataBuffer.bTravel_DN = 1;
//      }
//      else
//      {
//         stCE_DataBuffer.bTravel_UP = 0;
//         stCE_DataBuffer.bTravel_DN = 0;
//      }
//   }
//   else
//   {
//      stCE_DataBuffer.bTravel_UP = 0;
//      stCE_DataBuffer.bTravel_DN = 0;
//   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_FireLamp(void)
{
   if(GetOperation_AutoMode() == MODE_A__FIRE1 || GetOperation_AutoMode() == MODE_A__FIRE2)
   {
      stCE_DataBuffer.bFire = 1;
   }
   else
   {
      stCE_DataBuffer.bFire = 0;
   }
}
/*-----------------------------------------------------------------------------
   For annunciator of floor levels
   Holds strobe high for 1 sec
 -----------------------------------------------------------------------------*/
static void Update_StrobeLamp(void)
{
#if 1 // Fix for annunciator inconsistency
   static uint16_t uwCountdown_5ms;
   static uint8_t bLastDoorOpen;
   uint8_t bDZ = GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R);
   uint8_t bDoorOpen = ( GetDoorState_Front() == DOOR__OPENING )
                    || ( GetDoorState_Front() == DOOR__OPEN )
                    || ( GetDoorState_Rear() == DOOR__OPENING )
                    || ( GetDoorState_Rear() == DOOR__OPEN );
   uint8_t bArrivalLantern = GetOutputValue(enOUT_ARV_UP_F)
                          || GetOutputValue(enOUT_ARV_DN_F)
                          || GetOutputValue(enOUT_ARV_UP_R)
                          || GetOutputValue(enOUT_ARV_DN_R);
   if( !Param_ReadValue_1Bit(enPARAM1__DisableCE_FloorPlus1) )
   {
      if( bArrivalLantern )
      {
         stCE_DataBuffer.ucSlot = GetOperation_DestinationFloor()+1;
      }
      else
      {
         stCE_DataBuffer.ucSlot = GetOperation_CurrentFloor()+1;
      }
   }
   else
   {
      if( bArrivalLantern )
      {
         stCE_DataBuffer.ucSlot = GetOperation_DestinationFloor();
      }
      else
      {
         stCE_DataBuffer.ucSlot = GetOperation_CurrentFloor();
      }
   }

   if( bDoorOpen
    && !bLastDoorOpen )
   {
      uwCountdown_5ms = 400;
   }

   if( bDZ
    && uwCountdown_5ms
    && GetOperation_CurrentFloor() == GetOperation_DestinationFloor()
    && bDoorOpen )
   {
      stCE_DataBuffer.bStrobe = 1;
   }
   else if( bDZ
         && bArrivalLantern
         && bDoorOpen
         && GetOperation_CurrentFloor() == GetOperation_DestinationFloor() )
   {
      stCE_DataBuffer.bStrobe = 1;
   }
   else
   {
      uwCountdown_5ms = 0;
      stCE_DataBuffer.bStrobe = 0;
   }

   if( uwCountdown_5ms )
   {
      uwCountdown_5ms--;
   }
   bLastDoorOpen = bDoorOpen;
#else
   static uint16_t uwCountdown_5ms;
   static uint8_t bLastDZ;
   uint8_t bDZ = GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R);
   uint8_t bDoorOpen = ( GetDoorState_Front() == DOOR__OPENING )
                    || ( GetDoorState_Front() == DOOR__OPEN )
                    || ( GetDoorState_Rear() == DOOR__OPENING )
                    || ( GetDoorState_Rear() == DOOR__OPEN );
   uint8_t bArrivalLantern = GetOutputValue(enOUT_ARV_UP_F)
                          || GetOutputValue(enOUT_ARV_DN_F)
                          || GetOutputValue(enOUT_ARV_UP_R)
                          || GetOutputValue(enOUT_ARV_DN_R);
   if( !Param_ReadValue_1Bit(enPARAM1__DisableCE_FloorPlus1) )
   {
      if( bArrivalLantern )
      {
         stCE_DataBuffer.ucSlot = GetOperation_DestinationFloor()+1;
      }
      else
      {
         stCE_DataBuffer.ucSlot = GetOperation_CurrentFloor()+1;
      }
   }
   else
   {
      if( bArrivalLantern )
      {
         stCE_DataBuffer.ucSlot = GetOperation_DestinationFloor();
      }
      else
      {
         stCE_DataBuffer.ucSlot = GetOperation_CurrentFloor();
      }
   }

   if( bDZ
    && !bLastDZ )
   {
      uwCountdown_5ms = 300;
   }

   if( bDZ
    && uwCountdown_5ms
    && GetOperation_CurrentFloor() == GetOperation_DestinationFloor()
    && bDoorOpen )
   {
      stCE_DataBuffer.bStrobe = 1;
   }
   else if( bArrivalLantern && bDoorOpen )
   {
      stCE_DataBuffer.bStrobe = 1;
   }
   else
   {
      uwCountdown_5ms = 0;
      stCE_DataBuffer.bStrobe = 0;
   }

   if( uwCountdown_5ms )
   {
      uwCountdown_5ms--;
   }
   bLastDZ = bDZ;
#endif
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_Message(void)
{
   if( ( Param_ReadValue_1Bit(enPARAM1__EnableCE_V2) )
    && ( GetOperation_AutoMode() == MODE_A__FIRE2 ) )
   {
      stCE_DataBuffer.ucMessageNum = CE_MSG__FF2;
   }
   else if( ( GetOperation_AutoMode() == MODE_A__FIRE1 )
         || ( GetOperation_AutoMode() == MODE_A__FIRE2 ) )
   {
      if( GetEmergencyBit( EmergencyBF_FirePhaseI_RecallToAltFloor ) )
      {
         stCE_DataBuffer.ucMessageNum = CE_MSG__FIRE_ALT;
      }
      else
      {
         stCE_DataBuffer.ucMessageNum = CE_MSG__FIRE_MAIN;
      }
   }
   else if( ( GetDoorState_Front() == DOOR__NUDGING )
         || ( GetDoorState_Rear() == DOOR__NUDGING ) )
   {
      stCE_DataBuffer.ucMessageNum = CE_MSG__NUDGE;
   }
   else if( GetOutputValue(enOUT_Overload) )
   {
      stCE_DataBuffer.ucMessageNum = CE_MSG__OVERLOAD;
   }
   else if(GetOperation_AutoMode() == MODE_A__INDP_SRV)
   {
      stCE_DataBuffer.ucMessageNum = CE_MSG__INDP_SRV;
   }
   else if( ( GetOperation_AutoMode() == MODE_A__EPOWER )
         && ( !CheckIf_EPowerCarInNormal() ) )
   {
      stCE_DataBuffer.ucMessageNum = CE_MSG__EPOWER;
   }
   else if(GetOperation_ManualMode() >= MODE_M__INSP_CT && GetOperation_ManualMode() < NUM_MODE_MANUAL)
   {
      stCE_DataBuffer.ucMessageNum = CE_MSG__INSP;
   }
   else if (GetOperation_AutoMode() == MODE_A__SEISMC)
   {
      stCE_DataBuffer.ucMessageNum = CE_MSG__SEISMIC;
   }
   else if( ( Param_ReadValue_1Bit(enPARAM1__EnableCE_V2) )
         && ( !Param_ReadValue_1Bit(enPARAM1__DisablePIOOS) )
         && ( ( GetOperation_AutoMode() == MODE_A__OOS ) ||
              ( ( GetOperation_AutoMode() == MODE_A__NORMAL ) && ( HallCallsDisabled() || ( GetOperation3_CaptureMode_MR() == CAPTURE_IDLE ) ) ) ) )
   {
      stCE_DataBuffer.ucMessageNum = CE_MSG__OOS;
   }
   else
   {
      stCE_DataBuffer.ucMessageNum = CE_MSG__NONE;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define OOS_STATUS_TOGGLE_RATE_5MS     (200)
static void Update_PI(void)
{
   /* Simulate a flashing OOS display if in OOS mode, if in normal and car is captured or faulted */
   static uint8_t bOOS;
   static uint8_t bCR;
   static uint8_t bVIP;
   static uint8_t ucToggleCounter_5ms;
   if( ( GetOperation_AutoMode() == MODE_A__OOS )
    || ( ( GetOperation_AutoMode() == MODE_A__NORMAL ) && ( !Param_ReadValue_1Bit(enPARAM1__DisablePIOOS) ) &&
         ( gstFault.bActiveFault || ( HallCallsDisabled() && ( GetOperation3_CaptureMode_MR() == CAPTURE_IDLE ) ) ) ) )
   {
      if(++ucToggleCounter_5ms >= OOS_STATUS_TOGGLE_RATE_5MS)
      {
         bOOS = !bOOS;
         ucToggleCounter_5ms = 0;
      }
   }
   else if(GetOperation_AutoMode() == MODE_A__ACTIVE_SHOOTER_MODE)
   {
      if(++ucToggleCounter_5ms >= OOS_STATUS_TOGGLE_RATE_5MS)
      {
         bCR = !bCR;
         ucToggleCounter_5ms = 0;
      }
   }
   else if( !GetGroup_VIP_CarCapturing() && ( GetOperation_AutoMode() == MODE_A__VIP_MODE ) )
   {
      if(++ucToggleCounter_5ms >= OOS_STATUS_TOGGLE_RATE_5MS)
      {
         bVIP = !bVIP;
         ucToggleCounter_5ms = 0;
      }
   }
   else
   {
      ucToggleCounter_5ms = 0;
      bOOS = 0;
      bCR = 0;
      bVIP = 0;
   }
   if(bOOS && !stCE_DataBuffer.ucMessageNum)
   {
      stCE_DataBuffer.ucCurrentCharL = 'O';
      stCE_DataBuffer.ucCurrentCharM = 'O';
      stCE_DataBuffer.ucCurrentCharR = 'S';
   }
   else if(bCR && !stCE_DataBuffer.ucMessageNum)
   {
      stCE_DataBuffer.ucCurrentCharL = ' ';
      stCE_DataBuffer.ucCurrentCharM = 'C';
      stCE_DataBuffer.ucCurrentCharR = 'R';
   }
   else if(bVIP && !stCE_DataBuffer.ucMessageNum)
   {
      stCE_DataBuffer.ucCurrentCharL = ' ';
      stCE_DataBuffer.ucCurrentCharM = 'V';
      stCE_DataBuffer.ucCurrentCharR = 'P';
   }
   else
   {
      char * ps = Get_PI_Label(GetOperation_CurrentFloor());
      stCE_DataBuffer.ucCurrentCharL = *(ps+0);
      stCE_DataBuffer.ucCurrentCharM = *(ps+1);
      stCE_DataBuffer.ucCurrentCharR = *(ps+2);

      if(stCE_DataBuffer.ucCurrentCharL == 0x20)
      {
         stCE_DataBuffer.ucCurrentCharL = 0x3B;
      }
      if(stCE_DataBuffer.ucCurrentCharM == 0x20)
      {
         stCE_DataBuffer.ucCurrentCharM = 0x3B;
      }
      if(stCE_DataBuffer.ucCurrentCharR == 0x20)
      {
         stCE_DataBuffer.ucCurrentCharR = 0x3B;
      }
   }
}
static void Update_Chime()
{
   stCE_DataBuffer.bChime = GetLocalChimeSignal();
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_CEDisplayData( void )
{
   Update_TravelArrows();
   Update_ArrivalArrows();
   Update_FireLamp();
   Update_StrobeLamp();
   Update_PI();
   Update_Message();
   Update_Chime();
}
/*-----------------------------------------------------------------------------
   CAN ID: 0x501

   Byte 0: Bitmap
            Bit 7-3: Reserved
            Bit 2: Play strobe
            Bit 1: Arrival DN arrow
            Bit 0: Arrival UP arrow
   Byte 1: Scan slot (must match CAN ID 0x502 Byte 6 Field)
   Byte 2: Message number (0 if no message. 1-63 valid)
   Byte 3: Bitmap
            Bit 7: Rear Arrival DN arrow
            Bit 6: Rear Arrival UP arrow
            Bit 5: Mess level HIGH bit
            Bit 4: Mess level LOW bit
            Bit 3: Passing Chime
            Bit 2: Reserved
            Bit 1: Traveling DN arrow
            Bit 0: Traveling UP arrow
   Byte 4: Reserved
   Byte 5: Reserved
   Byte 6: Reserved
   Byte 7: Reserved
-----------------------------------------------------------------------------*/
static void LoadDatagram_CE_Message()
{
   un_sdata_datagram unDatagram;
   memset(&unDatagram, 0, sizeof(un_sdata_datagram));
   unDatagram.auc8[0] = stCE_DataBuffer.bArrivalF_UP
                     | (stCE_DataBuffer.bArrivalF_DN << 1)
                     | (stCE_DataBuffer.bStrobe << 2)
                     | (stCE_DataBuffer.bFire << 6)
                     ;
   unDatagram.auc8[1] = stCE_DataBuffer.ucSlot;
   unDatagram.auc8[2] = stCE_DataBuffer.ucMessageNum; //No message
   unDatagram.auc8[3] = stCE_DataBuffer.bTravel_UP
                     | (stCE_DataBuffer.bTravel_DN << 1)
                     | (stCE_DataBuffer.bChime << 3)
                     | (gaucMsgLevel[stCE_DataBuffer.ucMessageNum] << 4)
                     | (stCE_DataBuffer.bArrivalR_UP << 6)
                     | (stCE_DataBuffer.bArrivalR_DN << 7);

    if( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT )
    {
       SDATA_WriteDatagram( gpastSData_Nodes_CT_AuxNet[CT_AUX_NET__CTB],
                            DG_AuxNet_CTB__Message,
                            &unDatagram
                            );
    }
    /* COP */
    else
    {
       SDATA_WriteDatagram( gpastSData_Nodes_COP_AuxNet[COP_AUX_NET__COPB],
                            DG_AuxNet_COPB__Message,
                            &unDatagram
                            );
    }
}
/*-----------------------------------------------------------------------------
   CAN ID: 0x502

   Byte 0: (N/A) ASCII Floor Char, msb
   Byte 1: ASCII Floor Char, mid
   Byte 2: ASCII Floor Char, lsb
   Byte 3: ASCII Message Char, msb
   Byte 4: ASCII Message Char, mid
   Byte 5: ASCII Message Char, lsb
   Byte 6: Lantern Position (match Scan Slot Byte 1 in ID 0x501 message)
   Byte 7: UNUSED
-----------------------------------------------------------------------------*/
static void LoadDatagram_CE_Label()
{
   un_sdata_datagram unDatagram;
   memset(&unDatagram, 0, sizeof(un_sdata_datagram));
   if(Param_ReadValue_1Bit(enPARAM1__Enable3DigitPI))
   {
      unDatagram.auc8[0] = stCE_DataBuffer.ucCurrentCharL;
      unDatagram.auc8[1] = stCE_DataBuffer.ucCurrentCharM;
      unDatagram.auc8[2] = stCE_DataBuffer.ucCurrentCharR;
   }
   else
   {
      unDatagram.auc8[0] = 0x3B;//";"
      unDatagram.auc8[1] = stCE_DataBuffer.ucCurrentCharM;
      unDatagram.auc8[2] = stCE_DataBuffer.ucCurrentCharR;
   }
   unDatagram.auc8[3] = *(gpas_MsgStrings[stCE_DataBuffer.ucMessageNum]+2);//No message
   unDatagram.auc8[4] = *(gpas_MsgStrings[stCE_DataBuffer.ucMessageNum]+0);//No message
   unDatagram.auc8[5] = *(gpas_MsgStrings[stCE_DataBuffer.ucMessageNum]+1);//No message
   unDatagram.auc8[6] = stCE_DataBuffer.ucSlot;
   unDatagram.auc8[7] = 0;

   if( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT )
   {
      SDATA_WriteDatagram( gpastSData_Nodes_CT_AuxNet[CT_AUX_NET__CTB],
                           DG_AuxNet_CTB__Label,
                           &unDatagram
                           );
   }
   /* COP */
   else
   {
      SDATA_WriteDatagram( gpastSData_Nodes_COP_AuxNet[COP_AUX_NET__COPB],
                           DG_AuxNet_COPB__Label,
                           &unDatagram
                           );
   }
}
/*-----------------------------------------------------------------------------
   CAN ID: 0x503

   Byte 0:
   Byte 1:
   Byte 2:
   Byte 3: PI Indux Plus 1
   Byte 4: Msg Number
   Byte 5: TravelArrows
   Byte 6:
   Byte 7:
-----------------------------------------------------------------------------*/
static void LoadDatagram_DiscretePI()
{
   un_sdata_datagram unDatagram;
   memset(&unDatagram, 0, sizeof(un_sdata_datagram));

   unDatagram.auc8[3] = GetOperation_CurrentFloor()+1;
   unDatagram.auc8[4] = stCE_DataBuffer.ucMessageNum;
   unDatagram.auc8[5] = (stCE_DataBuffer.bTravel_DN << 1) | stCE_DataBuffer.bTravel_UP;
   unDatagram.auc8[6] = (stCE_DataBuffer.bArrivalF_DN << 1) | stCE_DataBuffer.bArrivalF_UP;
   unDatagram.auc8[7] = (stCE_DataBuffer.bArrivalR_DN << 1) | stCE_DataBuffer.bArrivalR_UP;

   if( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT )
   {
      SDATA_WriteDatagram( gpastSData_Nodes_CT_AuxNet[CT_AUX_NET__CTB],
                           DG_AuxNet_CTB__DiscretePI,
                           &unDatagram );
   }
   else   /* COP */
   {
      SDATA_WriteDatagram( gpastSData_Nodes_COP_AuxNet[COP_AUX_NET__COPB],
                           DG_AuxNet_COPB__DiscretePI,
                           &unDatagram );
   }
}
/*-----------------------------------------------------------------------------
Floor Information (0x580)
Byte 1
   Floor Number.  A floor number of 0 is not a valid floor position and should only be
   used when translating from a source that does not provide numeric position data.
Byte 2
   Floor marking character (leftmost) [the Micro Comm converter does not support this character]
Byte 3
   Floor marking character
Byte 4
   Floor marking character
Byte 5
   Floor marking character (rightmost)
Byte 6
   Bit 0 – Front in-car arrow up
   Bit 1 – Front in-car arrow down
   Bit 2 – Rear in-car arrow up
   Bit 3 – Rear in-car arrow down
   Bit 4 – Travel arrow up
   Bit 5 – Travel arrow down
   Bit 6 – Passing chime
   Bit 7 – Unused
Byte 7
   Bit 0 – Front arrival arrow up
   Bit 1 – Front arrival arrow down
   Bit 2 – Front gong up
   Bit 3 – Front gong down
   Bit 4 – Rear arrival arrow up
   Bit 5 – Rear arrival arrow down
   Bit 6 – Rear gong up
   Bit 7 – Rear gong down
Byte 8
   Target display number  (This is not used by most controllers.
   Just leave this set to 0xFF for basic controller operation).
   If this is used, it only applies to data located in Byte 7.
 -----------------------------------------------------------------------------*/
static void LoadDatagram_CED_FloorInfo()
{
   static un_sdata_datagram unDatagram;
   unDatagram.auc8[0] = GetOperation_CurrentFloor()+1;
   unDatagram.auc8[1] = 0x20;// ' '
   unDatagram.auc8[2] = stCE_DataBuffer.ucCurrentCharL;
   unDatagram.auc8[3] = stCE_DataBuffer.ucCurrentCharM;
   unDatagram.auc8[4] = stCE_DataBuffer.ucCurrentCharR;
   unDatagram.auc8[5] = ( stCE_DataBuffer.bTravel_UP       << 4 )
                      | ( stCE_DataBuffer.bTravel_DN       << 5 )
                      | ( stCE_DataBuffer.bChime           << 6 );
   unDatagram.auc8[6] = ( stCE_DataBuffer.bArrivalF_UP     << 0 )
                      | ( stCE_DataBuffer.bArrivalF_DN     << 1 )
                      | ( stCE_DataBuffer.bArrivalR_UP     << 4 )
                      | ( stCE_DataBuffer.bArrivalR_DN     << 5 );
   unDatagram.auc8[7] = CED_TARGET__ALL_DISPLAY;

   if( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP )
   {
      SDATA_WriteDatagram( gpastSData_Nodes_COP_AuxNet[COP_AUX_NET__COPB],
                           DG_AuxNet_COPB__CED_FloorInfo,
                           &unDatagram );
   }
}
/*-----------------------------------------------------------------------------
Priority Message (0x581)
Byte 1
   Priority message number.  If the priority message number is non-zero, the display should show the associated message marking data unless it is filtered by the message status flags.  If the priority message number is zero, no message is active.
Byte 2
   Fire and Message status flags
   Bit 0 – Message can be filtered and not shown by the display
   Bit 1 – Fire return to the main floor, blank all other displays.
   Bit 2 – Fire return to the alternate floor, blank all other displays.
   Bit 3 – Fire warning lamp activated
   Bit 4 – Fire buzzer activated
   Bit 5 – Nudging buzzer activated
   Bit 6 – unused
   Bit 7 – unused
Byte 3
   Door signal flags [the Micro Comm converter does not support this byte]
   (If multiple Front bits or multiple Rear bits are set, only the lowest bit in each group should be used.  If no bits are set, displays should behave as if the closed bit is set.)
   Bit 0 – Front door opening
   Bit 1 – Front door open
   Bit 2 – Front door closing
   Bit 3 – Front door closed
   Bit 4 – Rear door opening
   Bit 5 – Rear door open
   Bit 6 – Rear door closing
   Bit 7 – Rear door closed
Byte 4
   Reserved
Byte 5
   Message marking character (leftmost) [the Micro Comm converter does not support this character]
Byte 6
   Message marking character
Byte 7
   Message marking character
Byte 8
   Message marking character (rightmost)
 -----------------------------------------------------------------------------*/
static void LoadDatagram_CED_Priority()
{
   static un_sdata_datagram unDatagram;
   uint8_t bBuzzer_Nudge = ( GetOutputValue(enOUT_BUZZER)
                       && ( GetOutputValue(enOUT_NDG_F) || GetOutputValue(enOUT_NDG_R) ) );
   uint8_t bBuzzer_Fire = ( GetOutputValue(enOUT_BUZZER)
                       && ( GetOutputValue(enOUT_LMP_FIRE) ) );
   uint8_t bFire_ReturnAlt = stCE_DataBuffer.ucMessageNum == CE_MSG__FIRE_ALT;
   uint8_t bFire_ReturnMain = stCE_DataBuffer.ucMessageNum == CE_MSG__FIRE_MAIN;
   unDatagram.auc8[0] = stCE_DataBuffer.ucMessageNum;
   unDatagram.auc8[1] = ( 0                                << 0 )//Message can be filtered and not shown by the display
                      | ( bFire_ReturnMain                 << 1 )
                      | ( bFire_ReturnAlt                  << 2 )
                      | ( GetOutputValue(enOUT_LMP_FIRE)   << 3 )
                      | ( bBuzzer_Fire                     << 4 )
                      | ( bBuzzer_Nudge                    << 5 );
   unDatagram.auc8[2] = ( (GetDoorState_Front() == DOOR__OPENING)   << 0 )
                      | ( (GetDoorState_Front() == DOOR__OPEN)      << 1 )
                      | ( (GetDoorState_Front() == DOOR__CLOSING)   << 2 )
                      | ( (GetDoorState_Front() == DOOR__CLOSED)    << 3 )
                      | ( (GetDoorState_Rear()  == DOOR__OPENING)   << 4 )
                      | ( (GetDoorState_Rear()  == DOOR__OPEN)      << 5 )
                      | ( (GetDoorState_Rear()  == DOOR__CLOSING)   << 6 )
                      | ( (GetDoorState_Rear()  == DOOR__CLOSED)    << 7 );
   unDatagram.auc8[4] = 0x20;// ' '
   unDatagram.auc8[5] = 0x20;// ' '
   unDatagram.auc8[6] = *(gpas_MsgStrings[stCE_DataBuffer.ucMessageNum]+0);
   unDatagram.auc8[7] = *(gpas_MsgStrings[stCE_DataBuffer.ucMessageNum]+1);

   if( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP )
   {
      SDATA_WriteDatagram( gpastSData_Nodes_COP_AuxNet[COP_AUX_NET__COPB],
                           DG_AuxNet_COPB__CED_Priority,
                           &unDatagram );
   }
}
/*-----------------------------------------------------------------------------
Secondary Message (0x582)
Byte 1
   Command
   0x00 – Add secondary message
   0x01 – Remove secondary message
Byte 2
   Message number.  Valid message numbers are 1-255.  If a Remove command is received with a message number of 0, all secondary messages should be removed.  An Add command with a message number of 0 should be ignored.
Byte 3
   Reserved
Byte 4
   Reserved
Byte 5
   Message marking character (leftmost) [the Micro Comm converter does not support this character]
Byte 6
   Message marking character [the Micro Comm converter does not support this character]
Byte 7
   Message marking character
Byte 8
   Message marking character (rightmost)
 -----------------------------------------------------------------------------*/
static void LoadDatagram_CED_Secondary()
{
   /* UNUSED */
   static un_sdata_datagram unDatagram;
   unDatagram.auc8[0] = 0x01;
   if( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP )
   {
      SDATA_WriteDatagram( gpastSData_Nodes_COP_AuxNet[COP_AUX_NET__COPB],
                           DG_AuxNet_COPB__CED_Secondary,
                           &unDatagram );
   }
}
/*-----------------------------------------------------------------------------
Audio Message (0x583)
Byte 1
   Target display number
Byte 2
   Target flags
      Bit 0 – Front displays are targeted
      Bit 1 – Rear displays are targeted
Byte 3
   Voice activation flags
      Bit 0 – Play floor position announcement (from last received floor information packet)
      Bit 1 – Play arrow announcement (from last received floor information packet.  Display decides which arrows to use)
      Bit 2 – Announce all active destinations (ignored in non-destination mode)
      Bit 3 – Announce all active handicap destinations (ignored in non-destination mode)
Byte 4
   Number of times to play car identification tone (when in non-destination mode, treat as 0)
Byte 5
   Car identification tone number (0 to 255 are all valid tone numbers)
Byte 6-8
   Unused
 -----------------------------------------------------------------------------*/
static void LoadDatagram_CED_Audio()
{
   static un_sdata_datagram unDatagram;

   unDatagram.auc8[0] = CED_TARGET__ALL_DISPLAY;
   unDatagram.auc8[1] = 0; /* Assume front displays */
   unDatagram.auc8[2] = GetInputValue(enIN_DZ_F) && ( GetOperation_CurrentFloor() == GetOperation_DestinationFloor() );
   unDatagram.auc8[3] = CED_TIMES_TO_PLAY_CAR_ID_CODE;
   unDatagram.auc8[4] = Param_ReadValue_8Bit(enPARAM8__GroupCarIndex);
   if( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP )
   {
      SDATA_WriteDatagram( gpastSData_Nodes_COP_AuxNet[COP_AUX_NET__COPB],
                           DG_AuxNet_COPB__CED_Secondary,
                           &unDatagram );
   }
}
/*-----------------------------------------------------------------------------
Flash Control (0x584)
Byte 1
   Flash control flags
      Bit 0 – Message flash control
      Bit 1 – Destination flash control (ignored in non-destination mode)
      Bit 2 – Car label flash control (ignored in non-destination mode) [the Micro Comm converter does not support this flag]
Byte 2-8
   Unused
 -----------------------------------------------------------------------------*/
static void LoadDatagram_CED_Flash()
{
   static uint32_t uiFlashTimer_ms;
   static uint8_t ucActiveBit;//Bit to flash
   static un_sdata_datagram unDatagram;
   /* Cycle through each display type */
   if( uiFlashTimer_ms >= CED_DISPLAY_FLASH_TIME_MS )
   {
      uiFlashTimer_ms = 0;
      if(++ucActiveBit >= 3) // Valid bits [0,2]
      {
         ucActiveBit = 0;
      }
   }
   else
   {
      uiFlashTimer_ms += MOD_RUN_PERIOD_CE_BOARD_1MS;
   }

   unDatagram.auc8[0] = ( (ucActiveBit == 0) << 0 )
                      | ( (ucActiveBit == 1) << 1 )
                      | ( (ucActiveBit == 2) << 2 );

   if( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP )
   {
      SDATA_WriteDatagram( gpastSData_Nodes_COP_AuxNet[COP_AUX_NET__COPB],
                           DG_AuxNet_COPB__CED_Flash,
                           &unDatagram );
   }
}
/*-----------------------------------------------------------------------------
Destination Command (0x585)
Byte 1
   Target display number
Byte 2
   Target and destination flags
   Bit 0 – Front displays are targeted
   Bit 1 – Rear displays are targeted
   Bit 2 – Handicap destination
   Bit 3 – Up Directional Call
   Bit 4 – Down Directional Call
Byte 3
   Command
   0x00 – Add or stop flashing destination
   0x01 – Remove destination
   0x02 – Flash destination (destination will be added if it does not exist)
   0x03 – Remove all destinations
   0x04 – Flash all destinations (only affects currently active destinations)
   0x05 – Stop flashing all destinations
Byte 4
   Destination number.  This number is used to order a list of destinations.
   Generally, the destination number used with a marking will be the same floor
   number used with that marking in the floor information packet, but this is not required.
   0 to 255 are all valid destination numbers.
Byte 5
   Destination marking character (leftmost).  The destination marking characters are only used by the Add and Flash commands.  For all other commands, these characters are ignored.
Byte 6
   Destination marking character
Byte 7
   Destination marking character
Byte 8
   Destination marking character (rightmost)
 -----------------------------------------------------------------------------*/
static void LoadDatagram_CED_Destination()
{
   static un_sdata_datagram unDatagram;
   if( GetOperation_ClassOfOp() == CLASSOP__AUTO )
   {
      unDatagram.auc8[0] = CED_TARGET__ALL_DISPLAY;
      unDatagram.auc8[1] = 0x03  //Front and rear displays
                        | ( GetOutputValue(enOUT_TRV_UP) << 3 )
                        | ( GetOutputValue(enOUT_TRV_DN) << 4 );
      if( ( GetOperation_DestinationFloor() != INVALID_FLOOR )
       && ( GetOperation_DestinationFloor() != GetOperation_CurrentFloor() ) )
      {
         unDatagram.auc8[2] = 0x00;//Add destination
         unDatagram.auc8[3] = GetOperation_DestinationFloor()+1;
      }
      else if( ( GetOperation_CurrentFloor() == GetOperation_DestinationFloor() )
            && ( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R) )
            && !GetMotion_RunFlag() )
      {
         unDatagram.auc8[2] = 0x01;//Remove destination
         unDatagram.auc8[3] = GetOperation_CurrentFloor()+1;
      }
   }
   else
   {
      unDatagram.auc8[0] = CED_TARGET__ALL_DISPLAY;
      unDatagram.auc8[1] = 0x03  //Front and rear displays
                        | ( GetOutputValue(enOUT_TRV_UP) << 3 )
                        | ( GetOutputValue(enOUT_TRV_DN) << 4 );
      unDatagram.auc8[2] = 0x03;//Remove all destinations
      unDatagram.auc8[3] = 0;
   }

   unDatagram.auc8[4] = 0x20; //' '
   unDatagram.auc8[5] = stCE_DataBuffer.ucDestCharL;
   unDatagram.auc8[6] = stCE_DataBuffer.ucDestCharM;
   unDatagram.auc8[7] = stCE_DataBuffer.ucDestCharR;
   if( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP )
   {
      SDATA_WriteDatagram( gpastSData_Nodes_COP_AuxNet[COP_AUX_NET__COPB],
                           DG_AuxNet_COPB__CED_Dest,
                           &unDatagram );
   }
}
/*-----------------------------------------------------------------------------
Car Label (0x586)
Byte 1
   Target display number
Byte 2
   Target flags
      Bit 0 – Front displays are targeted
      Bit 1 – Rear displays are targeted
Byte 3
   Car label status
      0x00 – Display decides
      0x01 – Show car label
      0x02 – Flash car label
Byte 4
   Car label marking character (leftmost)
Byte 5
   Car label marking character
Byte 6
   Car label marking character
Byte 7
   Car label marking character (rightmost)
Byte 8
   Unused
 -----------------------------------------------------------------------------*/
static void LoadDatagram_CED_Label()
{
   static un_sdata_datagram unDatagram;
   unDatagram.auc8[0] = CED_TARGET__ALL_DISPLAY;
   unDatagram.auc8[1] = 0x03;  // Front and rear displays
   unDatagram.auc8[2] = 0x00;  // Display decides

   // TODO add car label

   if( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP )
   {
      SDATA_WriteDatagram( gpastSData_Nodes_COP_AuxNet[COP_AUX_NET__COPB],
                           DG_AuxNet_COPB__CED_Label,
                           &unDatagram );
   }
}
/*-----------------------------------------------------------------------------
Display Configuration Parameters (0x587)
Byte 1
   Target display number
Byte 2
   Target flags
      Bit 0 – Front displays are targeted
      Bit 1 – Rear displays are targeted
Byte 3
   Bit 0 – Destination mode [Default: set]
      When this bit is set, the display will operate in destination mode (if possible) and process all packets.  When this bit is cleared, the display will operate in non-destination mode and will only show floor and message information and will ignore the Destination Command packet, Car Label packet, and certain bits within the Audio Activation and Flash Control packets.  If a display does not have any destination capabilities it should always operate as if this bit is cleared.
   Bit 1 – Double Gong [Default: set]
      When this bit is set, a gong unit should sound twice when the gong down bit is triggered.
   Bit 2 - Display-Controlled Flashing [Default: clear]
      If a controller does not want to synchronize display flashing with the Flash Control packet, it should set this bit to allow the display to perform message, destination, and car label flashing.  If this bit is not set and the controller does not send any Flash Control packets, messages, destinations, and car labels will not flash.
Byte 4-8
   Unused
 -----------------------------------------------------------------------------*/
static void LoadDatagram_CED_Config()
{
   static un_sdata_datagram unDatagram;
   unDatagram.auc8[0] = CED_TARGET__ALL_DISPLAY;
   unDatagram.auc8[1] = 0x03;  //Front and rear displays
   unDatagram.auc8[2] = ( 1 << 0 )//Destination mode ON
                      | ( 1 << 1 )//Double gong OFF;
                      | ( 0 << 2 );//Display controlled flashing OFF

   if( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP )
   {
      SDATA_WriteDatagram( gpastSData_Nodes_COP_AuxNet[COP_AUX_NET__COPB],
                           DG_AuxNet_COPB__CED_Config,
                           &unDatagram );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData( void )
{
   /* CE Display - MICROCOM (Chip ID: XP 345 3) */
   LoadDatagram_CE_Message();
   LoadDatagram_CE_Label();
   /* Discrete PI */
   LoadDatagram_DiscretePI();
   /* CE Display - ElevCAN (Chip ID: XP 424 1)
    * NOTE: Set DIP2 on baseboard for 50KHZ CAN at startup, and no others. */
   LoadDatagram_CED_FloorInfo();
   LoadDatagram_CED_Priority();
   LoadDatagram_CED_Secondary();
   LoadDatagram_CED_Audio();
   LoadDatagram_CED_Flash();
   LoadDatagram_CED_Destination();
   LoadDatagram_CED_Label();
   LoadDatagram_CED_Config();
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Init_CEBoardData()
{
   stCE_DataBuffer.bArrivalF_DN = 0;
   stCE_DataBuffer.bArrivalF_UP = 0;
   stCE_DataBuffer.bArrivalR_DN = 0;
   stCE_DataBuffer.bArrivalR_UP = 0;
   stCE_DataBuffer.bChime = 0;
   stCE_DataBuffer.ucMessageNum = 0; // No message
   stCE_DataBuffer.bStrobe = 0;
   stCE_DataBuffer.bTravel_UP = 0; // turn this on?
   stCE_DataBuffer.bTravel_DN = 0;
   stCE_DataBuffer.ucSlot = 0;
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10000;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_CE_BOARD_1MS;
   Init_CEBoardData();
   return 0;
}

/*-----------------------------------------------------------------------------
   NOTES:
      Applies to chips labeled "P 345"
      Set DIP3 to enable CAN 50K Baudrate. Otherwise, CAN is at 100K.
      Set DIP5 to enable CE message display
      Runs on CAN3 on SRU/MR boards.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   UpdateSmartriseFlag();
   UpdateLocalChimeSignal(pstThisModule->uwRunPeriod_1ms);
   Update_CEDisplayData(); // update this to send only the travel arrow
   LoadData();
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
