/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     26, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sru_b.h"
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init(struct st_module *pstThisModule);
static uint32_t Run(struct st_module *pstThisModule);

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_SData_AuxNet =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_sdata_control gstSData_AuxNet_CTB;
static struct st_sdata_control gstSData_AuxNet_LW;
static struct st_sdata_control gstSData_AuxNet_COPB;

struct st_sdata_control * gpastSData_Nodes_CT_AuxNet[ NUM_CT_AUX_NET_NODES ] =
{
   &gstSData_AuxNet_CTB,//MR_AUX_NET__MRB,
   &gstSData_AuxNet_LW,//MR_AUX_NET__LW,
};


uint8_t gaucPerNodeNumDatagrams_CT_AuxNet[ NUM_CT_AUX_NET_NODES ] =
{
   NUM_AuxNet_CTB_DATAGRAMS,
   NUM_AuxNet_LW_DATAGRAMS,
};
struct st_sdata_control * gpastSData_Nodes_COP_AuxNet[ NUM_COP_AUX_NET_NODES ] =
{
   &gstSData_AuxNet_COPB,//COP_AUX_NET__COPB
};


uint8_t gaucPerNodeNumDatagrams_COP_AuxNet[ NUM_COP_AUX_NET_NODES ] =
{
   NUM_AuxNet_COPB_DATAGRAMS,
};
static struct st_sdata_local gstSData_Config;

static CAN_MSG_T gstCAN_MSG_Tx;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SharedData_AuxNet_Init()
{
   int iError;
   int i;

   //--------------------------------------------------------
   if( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT )
   {
      gstSData_Config.ucLocalNodeID = CT_AUX_NET__CTB;

      gstSData_Config.ucNetworkID = SDATA_NET__AUX;

      gstSData_Config.ucNumNodes = NUM_CT_AUX_NET_NODES;

      gstSData_Config.ucDestNodeID = 0x1F;
      for ( i = 0; i < gstSData_Config.ucNumNodes; ++i )
      {
         iError = SDATA_CreateDatagrams( gpastSData_Nodes_CT_AuxNet[ i ],
                                         gaucPerNodeNumDatagrams_CT_AuxNet[ i ] );

         if ( iError )
         {
            while ( 1 )
            {
                //SDA REVIEW: issue a fault or some other kind of error recovery
               ;  // TODO: unable to allocate shared data
            }
         }
      }
   }
   /* COPB */
   else
   {
      gstSData_Config.ucLocalNodeID = COP_AUX_NET__COPB;

      gstSData_Config.ucNetworkID = SDATA_NET__AUX;

      gstSData_Config.ucNumNodes = NUM_COP_AUX_NET_NODES;

      gstSData_Config.ucDestNodeID = 0x1F;

      for ( i = 0; i < gstSData_Config.ucNumNodes; ++i )
      {
         iError = SDATA_CreateDatagrams( gpastSData_Nodes_COP_AuxNet[ i ],
                                         gaucPerNodeNumDatagrams_COP_AuxNet[ i ] );

         if ( iError )
         {
            while ( 1 )
            {
                //SDA REVIEW: issue a fault or some other kind of error recovery
               ;  // TODO: unable to allocate shared data
            }
         }
      }
   }
   gstCAN_MSG_Tx.DLC = 8;
}
/*-----------------------------------------------------------------------------
Takes local datagram ID and assigns corresponding CAN
 network message ID to the outbound packet
 -----------------------------------------------------------------------------*/
static void SetDatagramMessageID_CT(uint8_t ucLocalDatagramID)
{
   gstCAN_MSG_Tx.DLC = 8;
   if ( ucLocalDatagramID == DG_AuxNet_CTB__Message )
   {
      gstCAN_MSG_Tx.ID = 0x501;
      gstCAN_MSG_Tx.DLC = 4;
   }
   else if( ucLocalDatagramID == DG_AuxNet_CTB__Label )
   {
      gstCAN_MSG_Tx.ID = 0x502;
      gstCAN_MSG_Tx.DLC = 7;
   }
   else if( ucLocalDatagramID == DG_AuxNet_CTB__DiscretePI )
   {
      gstCAN_MSG_Tx.ID = 0x503;
   }
   else if( ucLocalDatagramID == DG_AuxNet_CTB__LoadWeigher )
   {
      gstCAN_MSG_Tx.ID = CAN_EXTEND_ID_USAGE;
      gstCAN_MSG_Tx.ID |= ( SDATA_NET__AUX & 0x0F ) << 25;
      gstCAN_MSG_Tx.ID |= ( CT_AUX_NET__CTB & 0x1F ) << 20;
      gstCAN_MSG_Tx.ID |= ( 0x1F & 0x1F ) << 15;
      gstCAN_MSG_Tx.ID |= ( DG_AuxNet_CTB__LoadWeigher & 0x1F ) << 10;
   }
   else
   {
      gstCAN_MSG_Tx.ID = ( 1 << 30 ) | ( ucLocalDatagramID << 10 )
               | ( gstSData_Config.ucDestNodeID << 15 )
               | ( gstSData_Config.ucLocalNodeID << 20 )
               | ( gstSData_Config.ucNetworkID << 25 );

   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Transmit_CT(void)
{
   un_sdata_datagram unDatagram;
   int iError = 1;
   uint8_t ucNodeID;
   uint8_t ucDatagramID;
   uint16_t uwDatagramID_Plus1 = 0;
   ucNodeID = gstSData_Config.ucLocalNodeID;

   uwDatagramID_Plus1 = SDATA_GetNextDatagramToSendIndex_Plus1(
            gpastSData_Nodes_CT_AuxNet[ ucNodeID ] );
   if(uwDatagramID_Plus1)
   {
      ucDatagramID = uwDatagramID_Plus1 - 1;
      if ( ucDatagramID < gaucPerNodeNumDatagrams_CT_AuxNet[ ucNodeID ] )
      {
         SDATA_ReadDatagram( gpastSData_Nodes_CT_AuxNet[ ucNodeID ], ucDatagramID,
                  &unDatagram );

         memcpy( gstCAN_MSG_Tx.Data, unDatagram.auc8,
                  sizeof( unDatagram.auc8 ) );

         SetDatagramMessageID_CT(ucDatagramID);
         iError =  !CAN1_LoadToRB( &gstCAN_MSG_Tx );
//         CAN_BUFFER_ID_T   TxBuf = Chip_CAN_GetFreeTxBuf(LPC_CAN1);
//         iError =  !Sys_CAN_Send( LPC_CAN1, TxBuf, &gstCAN_MSG_Tx);
//         iError =  !Chip_CAN_Send( LPC_CAN1, TxBuf, &gstCAN_MSG_Tx);
         if ( iError )
         {
            SDATA_DirtyBit_Set( gpastSData_Nodes_CT_AuxNet[ ucNodeID ], ucDatagramID );

            if ( gpastSData_Nodes_CT_AuxNet[ ucNodeID ]->uwLastSentIndex )
            {
               --gpastSData_Nodes_CT_AuxNet[ ucNodeID ]->uwLastSentIndex;
            }
            else
            {
               gpastSData_Nodes_CT_AuxNet[ ucNodeID ]->uwLastSentIndex =
                        gpastSData_Nodes_CT_AuxNet[ ucNodeID ]->uiNumDatagrams - 1;
            }
         }
      }
   }
}
/*-----------------------------------------------------------------------------
Takes local datagram ID and assigns corresponding CAN
 network message ID to the outbound packet
 -----------------------------------------------------------------------------*/
static void SetDatagramMessageID_COP(uint8_t ucLocalDatagramID)
{
   gstCAN_MSG_Tx.DLC = 8;
   if ( ucLocalDatagramID == DG_AuxNet_COPB__Message )
   {
      gstCAN_MSG_Tx.ID = 0x501;
      gstCAN_MSG_Tx.DLC = 4;
   }
   else if( ucLocalDatagramID == DG_AuxNet_COPB__Label )
   {
      gstCAN_MSG_Tx.ID = 0x502;
      gstCAN_MSG_Tx.DLC = 7;
   }
   else if( ucLocalDatagramID == DG_AuxNet_COPB__DiscretePI )
   {
      gstCAN_MSG_Tx.ID = 0x503;
   }
   else if( ucLocalDatagramID == DG_AuxNet_COPB__CED_FloorInfo )
   {
      gstCAN_MSG_Tx.ID = 0x580;
   }
   else if( ucLocalDatagramID == DG_AuxNet_COPB__CED_Priority )
   {
      gstCAN_MSG_Tx.ID = 0x581;
   }
   else if( ucLocalDatagramID == DG_AuxNet_COPB__CED_Secondary )
   {
      gstCAN_MSG_Tx.ID = 0x582;
   }
   else if( ucLocalDatagramID == DG_AuxNet_COPB__CED_Audio )
   {
      gstCAN_MSG_Tx.ID = 0x583;
   }
   else if( ucLocalDatagramID == DG_AuxNet_COPB__CED_Flash )
   {
      gstCAN_MSG_Tx.ID = 0x584;
   }
   else if( ucLocalDatagramID == DG_AuxNet_COPB__CED_Dest )
   {
      gstCAN_MSG_Tx.ID = 0x585;
   }
   else if( ucLocalDatagramID == DG_AuxNet_COPB__CED_Label )
   {
      gstCAN_MSG_Tx.ID = 0x586;
   }
   else if( ucLocalDatagramID == DG_AuxNet_COPB__CED_Config )
   {
      gstCAN_MSG_Tx.ID = 0x587;
   }
   else
   {
      gstCAN_MSG_Tx.ID = ( 1 << 30 ) | ( ucLocalDatagramID << 10 )
               | ( gstSData_Config.ucDestNodeID << 15 )
               | ( gstSData_Config.ucLocalNodeID << 20 )
               | ( gstSData_Config.ucNetworkID << 25 );

   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Transmit_COP(void)
{
   un_sdata_datagram unDatagram;
   int iError = 1;
   uint8_t ucNodeID;
   uint8_t ucDatagramID;
   uint16_t uwDatagramID_Plus1 = 0;
   ucNodeID = gstSData_Config.ucLocalNodeID;

   uwDatagramID_Plus1 = SDATA_GetNextDatagramToSendIndex_Plus1(
            gpastSData_Nodes_COP_AuxNet[ ucNodeID ] );
   if(uwDatagramID_Plus1)
   {
      ucDatagramID = uwDatagramID_Plus1 - 1;
      if ( ucDatagramID < gaucPerNodeNumDatagrams_COP_AuxNet[ ucNodeID ] )
      {
         SDATA_ReadDatagram( gpastSData_Nodes_COP_AuxNet[ ucNodeID ], ucDatagramID,
                  &unDatagram );

         memcpy( gstCAN_MSG_Tx.Data, unDatagram.auc8,
                  sizeof( unDatagram.auc8 ) );

         SetDatagramMessageID_COP(ucDatagramID);

         iError =  !CAN1_LoadToRB( &gstCAN_MSG_Tx );

         if ( iError )
         {
            SDATA_DirtyBit_Set( gpastSData_Nodes_COP_AuxNet[ ucNodeID ], ucDatagramID );

            if ( gpastSData_Nodes_COP_AuxNet[ ucNodeID ]->uwLastSentIndex )
            {
               --gpastSData_Nodes_COP_AuxNet[ ucNodeID ]->uwLastSentIndex;
            }
            else
            {
               gpastSData_Nodes_COP_AuxNet[ ucNodeID ]->uwLastSentIndex =
                        gpastSData_Nodes_COP_AuxNet[ ucNodeID ]->uiNumDatagrams - 1;
            }
         }
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Transmit()
{
   if( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT )
   {
      Transmit_CT();
   }
   else
   {
      Transmit_COP();
   }
}

/*-----------------------------------------------------------------------------

 Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init(struct st_module *pstThisModule)
{
   pstThisModule->uwInitialDelay_1ms = 5;
   pstThisModule->uwRunPeriod_1ms = 5;

   //------------------------------------------------

  // Set resend rates for each datagram in msec (Default 1 sec)
   if( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT )
   {
      *(gpastSData_Nodes_CT_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_CTB__Message) = 250;
      *(gpastSData_Nodes_CT_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_CTB__Label) = 250;
      *(gpastSData_Nodes_CT_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_CTB__DiscretePI) = 250;
      *(gpastSData_Nodes_CT_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_CTB__LoadWeigher) = IGNORED_DATAGRAM;
   }
   /* COP */
   else
   {
      *(gpastSData_Nodes_COP_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_COPB__Message) = 250;
      *(gpastSData_Nodes_COP_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_COPB__Label) = 250;
      *(gpastSData_Nodes_COP_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_COPB__DiscretePI) = 250;
      *(gpastSData_Nodes_COP_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_COPB__CED_FloorInfo) = 500;
      *(gpastSData_Nodes_COP_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_COPB__CED_Priority) = 500;
      *(gpastSData_Nodes_COP_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_COPB__CED_Secondary) = 5000;
      *(gpastSData_Nodes_COP_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_COPB__CED_Audio) = DISABLED_DATAGRAM_RESEND_RATE_MS;
      *(gpastSData_Nodes_COP_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_COPB__CED_Flash) = DISABLED_DATAGRAM_RESEND_RATE_MS;
      *(gpastSData_Nodes_COP_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_COPB__CED_Dest) = 1000;
      *(gpastSData_Nodes_COP_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_COPB__CED_Label) = 5000;
      *(gpastSData_Nodes_COP_AuxNet[gstSData_Config.ucLocalNodeID]->paiResendRate_1ms + DG_AuxNet_COPB__CED_Config) = 30000;
   }

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run(struct st_module *pstThisModule)
{
   Transmit();

   CAN1_FillHardwareBuffer();
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

