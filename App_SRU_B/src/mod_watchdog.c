/*
 * mod_watchdog.c
 *
 *  Created on: May 21, 2016
 *      Author: sean
 */


/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "watchdog.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Watchdog =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t bWatchdogDisabled;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 1;
   pstThisModule->uwRunPeriod_1ms = 1;

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static uint32_t uiTimeToHoldAlarm_ms = 1000;//1sec
   static uint8_t bFirst = 1;
   if(bFirst)
   {
      bFirst = 0;
      if(SRU_Read_WatchDogEnable())
      {
         bWatchdogDisabled = 0;
         watchdog_init();
      }
      else
      {
         bWatchdogDisabled = 1;
      }
   }
   else
   {
      if(bWatchdogDisabled && uiTimeToHoldAlarm_ms)
      {
         uiTimeToHoldAlarm_ms--;
         en_alarms eAlarm = (GetSRU_Deployment() == enSRU_DEPLOYMENT__CT)
                          ? ALM__WDT_DISABLED_CTB:ALM__WDT_DISABLED_COPB;
         SetAlarm(eAlarm);
      }
      if(!GetOperation4_BootloaderFlag())
      {
         watchdog_feed();
      }
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

