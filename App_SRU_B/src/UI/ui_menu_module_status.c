/******************************************************************************
 *
 * @file     ui_menu_setup.c
 * @brief    Setup Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "mod.h"
#include "sru.h"
#include <string.h>
#include <math.h>
#include "lcd.h"
#include "motion.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Debug_FloorLearnStatus( void );
static void UI_FFS_Debug_MotionStatus( void );
static void UI_FFS_Debug_AutoStatus( void );
static void UI_FFS_Debug_FireStatus( void );
static void UI_FFS_Debug_CWStatus( void );
static void UI_FFS_Debug_RecallStatus( void );

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_ui_menu_item gstMI_FloorLearnStatus =
{
   .psTitle = "Floor Learn Status",
   .pstUGS_Next = &gstUGS_Debug_FloorLearnStatus,
};
static struct st_ui_menu_item gstMI_MotionStatus =
{
   .psTitle = "Motion Status",
   .pstUGS_Next = &gstUGS_Debug_MotionStatus,
};
static struct st_ui_menu_item gstMI_AutoStatus =
{
   .psTitle = "Auto Status",
   .pstUGS_Next = &gstUGS_Debug_AutoStatus,
};
static struct st_ui_menu_item gstMI_FireStatus =
{
   .psTitle = "Fire Status",
   .pstUGS_Next = &gstUGS_Debug_FireStatus,
};
static struct st_ui_menu_item gstMI_CWStatus =
{
   .psTitle = "Counterweight Status",
   .pstUGS_Next = &gstUGS_Debug_CWStatus,
};
static struct st_ui_menu_item gstMI_RecallStatus =
{
   .psTitle = "Recall Status",
   .pstUGS_Next = &gstUGS_Debug_RecallStatus,
};
static struct st_ui_menu_item gstMI_PatternData =
{
   .psTitle = "Pattern Data",
   .pstUGS_Next = &gstUGS_PatternData,
};

static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_MotionStatus,
   &gstMI_PatternData,
   &gstMI_AutoStatus,
   &gstMI_RecallStatus,
   &gstMI_FireStatus,
   &gstMI_CWStatus,
   &gstMI_FloorLearnStatus,

};
static struct st_ui_screen__menu gstMenu =
{
   .psTitle = "Module Status",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};

//---------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Debug_FloorLearnStatus =
{
   .pfnDraw = UI_FFS_Debug_FloorLearnStatus,
};
//---------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Debug_MotionStatus =
{
   .pfnDraw = UI_FFS_Debug_MotionStatus,
};
//---------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Debug_AutoStatus =
{
   .pfnDraw = UI_FFS_Debug_AutoStatus,
};
//---------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Debug_FireStatus =
{
   .pfnDraw = UI_FFS_Debug_FireStatus,
};
//---------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Debug_CWStatus =
{
   .pfnDraw = UI_FFS_Debug_CWStatus,
};
//---------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Debug_RecallStatus =
{
   .pfnDraw = UI_FFS_Debug_RecallStatus,
};

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static char const * const gpasFloorLearnStates[6] =
{
      "Not On Learn",
      "Ready",
      "Learning",
      "Complete",
};

//-------------------------------------------
static char const * const gpasMotionStatus[NUM_MOTION_STATES]=
{
      "Stopped",
      "Manual Run",
      "Start Sequence",
      "Accelerating",
      "Cruising",
      "Decelerating",
      "Stop Sequence",
      "RSL Decel",
      "Rec Trv Dir",
};
static char const * const gpasMotionPattern[NUM_PATTERNS]=
{
      "None",
      "Very Short",
      "Short",
      "Full",
};
static char const * const gpasMotionStart[NUM_MOTION_START_SEQUENCE] =
{
      "PrepareToRun",
      "Drive HW Enable",
      "Pick M Cont.",
      "Motor Energize",
      "Pick B2 Cont.",
      "Lift EBrake",
      "Lift Brake",
      "Accel Delay",
      "DSD Pretorque",

};
static char const * const gpasMotionStop[NUM_MOTION_STOP_SEQUENCE]=
{
      "Ramp To Zero",
      "Hold Zero",
      "Drop Brake",
      "Check BPS",
      "Delay Deenergize",
      "Motor Deenergize",
      "Delay Drop M",
      "Drop M Cont.",
      "Preflight",
      "End",
};
//-------------------------------------------
static char const * const gpasAutoStates[AUTO_NUMBER_OF_MODES] =
{
      "Unknown",
      "Stopped",
      "Moving",
      "Idle",
      "Correction",
};
//-------------------------------------------
static char const * const gpasFireSrv1States[3] =
{
      "Off",
      "Fire I",
      "Fire II",

};
static char const * const gpasFireSrv2States[4] =
{
      "Unknown",
      "Off",
      "Hold",
      "On",

};
static char const * const gpasCWStates[NUM_CW_STATES] =
{
      "Unknown",
      "OOS",
      "GoingToNearestDZ",
};
static char const * const gpasRecallStates[NUM_RECALL_STATES] =
{
      "Unknown",
      "Moving",
      "Stopped",
      "Recall Finished",
};

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Menu_ModuleStatus =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu,
};
//----------------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Debug_FloorLearnStatus =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Debug_FloorLearnStatus,
};

//----------------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Debug_MotionStatus =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Debug_MotionStatus,
};
//----------------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Debug_AutoStatus =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Debug_AutoStatus,
};
//----------------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Debug_FireStatus =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Debug_FireStatus,
};
//----------------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Debug_CWStatus =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Debug_CWStatus,
};
//----------------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Debug_RecallStatus =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Debug_RecallStatus,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Debug_FloorLearnStatus( void )
{
   static uint8_t ucScrollY;
   LCD_Char_Clear();

   //-----------------------------------------
   LCD_Char_GotoXY(0,0);
   LCD_Char_WriteString("Floor Learn Status");
   LCD_Char_GotoXY(0,1);
   LCD_Char_WriteString("State: ");
   uint8_t ucState = GetModState_FloorLearnState();
   if(ucState < NUM_LEARN_STATES)
   {
      LCD_Char_WriteString(gpasFloorLearnStates[ucState]);
   }

   //-----------------------------------------
   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen( );
   }
   else if(enKeypress == enKEYPRESS_UP)
   {
      if(ucScrollY)
      {
        ucScrollY--;
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

static void UI_FFS_Debug_MotionStatus( void )
{
   static uint8_t ucScrollY;
   LCD_Char_Clear();

   //-----------------------------------------
   uint8_t ucIndex, ucState;
   for(ucIndex = 0; ucIndex < 4; ucIndex++)
   {
      uint8_t ucLine = ucScrollY+ucIndex;
      switch(ucLine)
      {
         case 0:
          LCD_Char_GotoXY(0,0-ucScrollY);
         LCD_Char_WriteString("Motion Status");
         break;
         case 1:
          LCD_Char_GotoXY(0,1-ucScrollY);
          LCD_Char_WriteString("State:  ");

          ucState = GetModState_MotionState();
          if(ucState < NUM_MOTION_STATES)
          {
             LCD_Char_WriteString(gpasMotionStatus[ucState]);
          }
          break;
         case 2:
          LCD_Char_GotoXY(0,2-ucScrollY);
          LCD_Char_WriteString("Start:  ");
          ucState = GetModState_MotionStart();
          if(ucState < NUM_MOTION_START_SEQUENCE)
          {
             LCD_Char_WriteString(gpasMotionStart[ucState]);
          }
            break;

         case 3:
          LCD_Char_GotoXY(0,3-ucScrollY);
          LCD_Char_WriteString("Stop:   ");
          ucState = GetModState_MotionStop();
          if(ucState < NUM_MOTION_STOP_SEQUENCE)
          {
             LCD_Char_WriteString(gpasMotionStop[ucState]);
          }
          break;
         case 4:
          LCD_Char_GotoXY(0,4-ucScrollY);
          LCD_Char_WriteString("Pattern:");
          ucState = GetModState_MotionPattern();
          if(ucState < NUM_PATTERNS)
          {
             LCD_Char_WriteString(gpasMotionPattern[ucState]);
          }
          break;
         default:
            break;
      }
   }
   //-----------------------------------------
   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen( );
   }
   else if(enKeypress == enKEYPRESS_UP)
   {
      if(ucScrollY)
      {
        ucScrollY--;
      }
   }
   else if(enKeypress == enKEYPRESS_DOWN)
   {
      if(!ucScrollY)
      {
        ucScrollY++;
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Debug_AutoStatus( void )
{
   static uint8_t ucScrollY;
   LCD_Char_Clear();

   //-----------------------------------------
   uint8_t ucIndex, ucState;
   for(ucIndex = 0; ucIndex < 4; ucIndex++)
   {
      uint8_t ucLine = ucScrollY+ucIndex;
      switch(ucLine)
      {
         case 0:
          LCD_Char_GotoXY(0,0);
         LCD_Char_WriteString("Auto Operation Status");

         case 1:
          LCD_Char_GotoXY(0,1);
          LCD_Char_WriteString("State: ");
          ucState = GetModState_AutoState();
          if(ucState < AUTO_NUMBER_OF_MODES)
          {
             LCD_Char_WriteString(gpasAutoStates[ucState]);
          }
         default:
            break;
      }
   }
   //-----------------------------------------
   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen( );
   }
   else if(enKeypress == enKEYPRESS_UP)
   {
      if(ucScrollY)
      {
        ucScrollY--;
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Debug_FireStatus( void )
{
   static uint8_t ucScrollY;
   LCD_Char_Clear();

   //-----------------------------------------
   uint8_t ucIndex, ucState;
   for(ucIndex = 0; ucIndex < 4; ucIndex++)
   {
      uint8_t ucLine = ucScrollY+ucIndex;
      switch(ucLine)
      {
         case 0:
          LCD_Char_GotoXY(0,0);
         LCD_Char_WriteString("Fire Status");

         case 1:
          LCD_Char_GotoXY(0,1);
          LCD_Char_WriteString("Active: ");
          ucState = GetModState_FireSrv();
          if(ucState < 3)
          {
             LCD_Char_WriteString(gpasFireSrv1States[ucState]);
          }

         case 2:
          LCD_Char_GotoXY(0,2);
          LCD_Char_WriteString("FireII:  ");
          ucState = GetModState_FireSrv2();
          if(ucState < NUM_FIRE_II_STATES)
          {
             LCD_Char_WriteString(gpasFireSrv2States[ucState]);
          }

         default:
            break;
      }
   }
   //-----------------------------------------
   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen( );
   }
   else if(enKeypress == enKEYPRESS_UP)
   {
      if(ucScrollY)
      {
        ucScrollY--;
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Debug_CWStatus( void )
{
   static uint8_t ucScrollY;
   LCD_Char_Clear();

   //-----------------------------------------
   uint8_t ucIndex, ucState;
   for(ucIndex = 0; ucIndex < 4; ucIndex++)
   {
      uint8_t ucLine = ucScrollY+ucIndex;
      switch(ucLine)
      {
         case 0:
          LCD_Char_GotoXY(0,0);
         LCD_Char_WriteString("Counterweight Status");

         case 1:
          LCD_Char_GotoXY(0,1);
          LCD_Char_WriteString("State: ");
          ucState = GetModState_Counterweight();
          if(ucState < NUM_CW_STATES)
          {
             LCD_Char_WriteString(gpasCWStates[ucState]);
          }

         default:
            break;
      }
   }
   //-----------------------------------------
   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen( );
   }
   else if(enKeypress == enKEYPRESS_UP)
   {
      if(ucScrollY)
      {
        ucScrollY--;
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Debug_RecallStatus( void )
{
   static uint8_t ucScrollY;
   LCD_Char_Clear();

   //-----------------------------------------
   uint8_t ucState;
   LCD_Char_GotoXY(0,0);
   LCD_Char_WriteString("Recall Status");
   LCD_Char_GotoXY(0,1);
   LCD_Char_WriteString("State: ");
   ucState = GetModState_Recall();
   if(ucState < NUM_RECALL_STATES)
   {
      LCD_Char_WriteString(gpasRecallStates[ucState]);
   }
   //-----------------------------------------
   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen( );
   }
   else if(enKeypress == enKEYPRESS_UP)
   {
      if(ucScrollY)
      {
        ucScrollY--;
      }
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
