
/******************************************************************************
 *
 * @file     ui_menu_io.c
 * @brief    I/O Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "GlobalData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Outputs_Functions_AutoOper( void );
static void UI_FFS_Outputs_Functions_Doors_F( void );
static void UI_FFS_Outputs_Functions_Doors_R( void );
static void UI_FFS_Outputs_Functions_FireEQ( void );
static void UI_FFS_Outputs_Functions_EPower( void );
static void UI_FFS_Outputs_Functions_Insp( void );
static void UI_FFS_Outputs_Functions_CTRL( void );
static void UI_FFS_Outputs_Functions_Safety( void );
static void UI_FFS_Outputs_Functions_CCL_F( void );
static void UI_FFS_Outputs_Functions_CCL_R( void );

//static void UI_FFS_Outputs_Functions_All( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
//------------------------------------------------------
/* functions */

static struct st_ui_menu_item gstMI_Functions_AutoOper =
{
   .psTitle = "Auto Operation",
   .pstUGS_Next = &gstUGS_Status_Outputs_AutoOper,
};

static struct st_ui_menu_item gstMI_Functions_Doors_F =
{
   .psTitle = "Front Doors",
   .pstUGS_Next = &gstUGS_Status_Outputs_Doors_F,
};

static struct st_ui_menu_item gstMI_Functions_Doors_R =
{
   .psTitle = "Rear Doors",
   .pstUGS_Next = &gstUGS_Status_Outputs_Doors_R,
};

static struct st_ui_menu_item gstMI_Functions_FireEQ =
{
   .psTitle = "Fire EQ ",
   .pstUGS_Next = &gstUGS_Status_Outputs_FireEQ,
};

static struct st_ui_menu_item gstMI_Functions_EPower =
{
   .psTitle = "E-Power ",
   .pstUGS_Next = &gstUGS_Status_Outputs_EPower,
};

static struct st_ui_menu_item gstMI_Functions_Insp =
{
   .psTitle = "Inspection",
   .pstUGS_Next = &gstUGS_Status_Outputs_Insp,
};

static struct st_ui_menu_item gstMI_Functions_CTRL =
{
   .psTitle = "Controller",
   .pstUGS_Next = &gstUGS_Status_Outputs_CTRL,
};

/* Movable */
static struct st_ui_menu_item gstMI_Functions_Safety =
{
   .psTitle = "Safety",
   .pstUGS_Next = &gstUGS_Status_Outputs_Safety,
};

static struct st_ui_menu_item gstMI_Functions_CCL_F =
{
   .psTitle = "CCL Front",
   .pstUGS_Next = &gstUGS_Status_Outputs_CCL_F,
};

/* Movable */
static struct st_ui_menu_item gstMI_Functions_CCL_R =
{
   .psTitle = "CCL Rear",
   .pstUGS_Next = &gstUGS_Status_Outputs_CCL_R ,
};
static struct st_ui_menu_item * gastMenuItems_Functions_Outputs[] =
{
   &gstMI_Functions_AutoOper,
   &gstMI_Functions_Doors_F,
   &gstMI_Functions_Doors_R,
   &gstMI_Functions_FireEQ,
   &gstMI_Functions_EPower,
   &gstMI_Functions_Insp,
   &gstMI_Functions_CTRL,
   &gstMI_Functions_Safety,
   &gstMI_Functions_CCL_F,
   &gstMI_Functions_CCL_R,
};
static struct st_ui_screen__menu gstMenu_Functions_Output =
{
   .psTitle = "Outputs By Function",
   .pastMenuItems = &gastMenuItems_Functions_Outputs,
   .ucNumItems = sizeof(gastMenuItems_Functions_Outputs) / sizeof(gastMenuItems_Functions_Outputs[ 0 ]),
};

static struct st_ui_screen__freeform gstFFS_Status_Outputs_Functions_AutoOper =
{
      .pfnDraw = UI_FFS_Outputs_Functions_AutoOper,
};
static struct st_ui_screen__freeform gstFFS_Status_Outputs_Functions_Doors_F =
{
      .pfnDraw = UI_FFS_Outputs_Functions_Doors_F,
};
static struct st_ui_screen__freeform gstFFS_Status_Outputs_Functions_Doors_R =
{
      .pfnDraw = UI_FFS_Outputs_Functions_Doors_R,
};
static struct st_ui_screen__freeform gstFFS_Status_Outputs_Functions_FireEQ =
{
      .pfnDraw = UI_FFS_Outputs_Functions_FireEQ,
};
static struct st_ui_screen__freeform gstFFS_Status_Outputs_Functions_EPower =
{
      .pfnDraw = UI_FFS_Outputs_Functions_EPower,
};
static struct st_ui_screen__freeform gstFFS_Status_Outputs_Functions_Insp =
{
      .pfnDraw = UI_FFS_Outputs_Functions_Insp,
};
static struct st_ui_screen__freeform gstFFS_Status_Outputs_Functions_CTRL =
{
      .pfnDraw = UI_FFS_Outputs_Functions_CTRL,
};
static struct st_ui_screen__freeform gstFFS_Status_Outputs_Functions_Safety =
{
      .pfnDraw = UI_FFS_Outputs_Functions_Safety,
};
static struct st_ui_screen__freeform gstFFS_Status_Outputs_Functions_CCL_F =
{
      .pfnDraw = UI_FFS_Outputs_Functions_CCL_F,
};
static struct st_ui_screen__freeform gstFFS_Status_Outputs_Functions_CCL_R =
{
      .pfnDraw = UI_FFS_Outputs_Functions_CCL_R,
};
//------------------------------------------------------
//static struct st_ui_screen__freeform gstFFS_Status_Outputs_Functions =
//{
//   .pfnDraw = UI_FFS_Outputs_Functions_All,
//};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/


struct st_ui_generic_screen gstUGS_Status_Outputs_Functions =
{
   .ucType = enUI_STYPE__MENU,  // Change To menu
   .pvReference = &gstMenu_Functions_Output,
   .pstUGS_Previous = &gstUGS_Menu_Status,
};

struct st_ui_generic_screen gstUGS_Status_Outputs_AutoOper =
{
  .ucType = enUI_STYPE__FREEFORM,
  .pvReference = &gstFFS_Status_Outputs_Functions_AutoOper,
  .pstUGS_Previous = &gstUGS_Status_Outputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Outputs_Doors_F =
 {
  .ucType = enUI_STYPE__FREEFORM,
  .pvReference = &gstFFS_Status_Outputs_Functions_Doors_F,
  .pstUGS_Previous = &gstUGS_Status_Outputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Outputs_Doors_R =
 {
  .ucType = enUI_STYPE__FREEFORM,
  .pvReference = &gstFFS_Status_Outputs_Functions_Doors_R,
  .pstUGS_Previous = &gstUGS_Status_Outputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Outputs_FireEQ =
 {
  .ucType = enUI_STYPE__FREEFORM,
  .pvReference = &gstFFS_Status_Outputs_Functions_FireEQ ,
  .pstUGS_Previous = &gstUGS_Status_Outputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Outputs_EPower =
 {
  .ucType = enUI_STYPE__FREEFORM,
  .pvReference = &gstFFS_Status_Outputs_Functions_EPower ,
  .pstUGS_Previous = &gstUGS_Status_Outputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Outputs_Insp =
 {
  .ucType = enUI_STYPE__FREEFORM,
  .pvReference = &gstFFS_Status_Outputs_Functions_Insp,
  .pstUGS_Previous = &gstUGS_Status_Outputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Outputs_CTRL =
 {
  .ucType = enUI_STYPE__FREEFORM,
  .pvReference = &gstFFS_Status_Outputs_Functions_CTRL,
  .pstUGS_Previous = &gstUGS_Status_Outputs_Functions
};

struct st_ui_generic_screen gstUGS_Status_Outputs_Safety =
 {
  .ucType = enUI_STYPE__FREEFORM,
  .pvReference = &gstFFS_Status_Outputs_Functions_Safety,
  .pstUGS_Previous = &gstUGS_Status_Outputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Outputs_CCL_F =
 {
  .ucType = enUI_STYPE__FREEFORM,
  .pvReference = &gstFFS_Status_Outputs_Functions_CCL_F,
  .pstUGS_Previous = &gstUGS_Status_Outputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Outputs_CCL_R  =
 {
  .ucType = enUI_STYPE__FREEFORM,
  .pvReference = &gstFFS_Status_Outputs_Functions_CCL_R,
  .pstUGS_Previous = &gstUGS_Status_Outputs_Functions
};


/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void Print_Outputs(uint8_t ucCursor, enum_output_groups eCOG, uint8_t ucSizeCOG)
{
   uint8_t ucIndex;
   for(ucIndex = 0; ucIndex < 3; ucIndex++)
   {
      uint8_t ucBitPos = ucCursor + ucIndex;
      if(ucBitPos <= ucSizeCOG)
      {
         LCD_Char_GotoXY( 0, 1+ucIndex );
         LCD_Char_WriteString("[");
         uint8_t bStatus = GetOutputValue( SysIO_GetOutputFunctionFromCOG(eCOG,ucBitPos) );
         const char * ps = (bStatus) ? "X":" ";
         LCD_Char_WriteString(ps);
         LCD_Char_WriteString("] ");
         LCD_Char_WriteString(SysIO_GetOutputGroupString_ByFunctionIndex(eCOG,ucBitPos));
      }
   }
}
static void Display_Status(enum_output_groups eCOG, uint8_t ucSizeCOG)
{

   static uint8_t ucCursorY = 0;
   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper(SysIO_GetOutputGroupString(eCOG));

   Print_Outputs(ucCursorY, eCOG, ucSizeCOG);

   enum en_keypresses enKeypress = Button_GetKeypress();
   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      int8_t cLimit = ucSizeCOG - 3;
      cLimit = (cLimit < 0) ? 0:cLimit;
      if(ucCursorY < cLimit)
      {
         ucCursorY++;
      }

   }
   if(ucCursorY > ucSizeCOG - 3) ucCursorY = 0;
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_Outputs_Functions_AutoOper( void )
{
   uint8_t ucSizeOfCOG = SysIO_GetSizeOfCOG(OUTPUT_GROUP__AUTO_OPER);
   Display_Status(OUTPUT_GROUP__AUTO_OPER,ucSizeOfCOG);
}
static void UI_FFS_Outputs_Functions_Doors_F( void )
{
   uint8_t ucSizeOfCOG = SysIO_GetSizeOfCOG(OUTPUT_GROUP__DOORS_F);
   Display_Status(OUTPUT_GROUP__DOORS_F,ucSizeOfCOG);
}
static void UI_FFS_Outputs_Functions_Doors_R( void )
{
   uint8_t ucSizeOfCOG = SysIO_GetSizeOfCOG(OUTPUT_GROUP__DOORS_R);
   Display_Status(OUTPUT_GROUP__DOORS_R,ucSizeOfCOG);
}
static void UI_FFS_Outputs_Functions_FireEQ( void )
{
   uint8_t ucSizeOfCOG = SysIO_GetSizeOfCOG(OUTPUT_GROUP__FIRE_EQ);
   Display_Status(OUTPUT_GROUP__FIRE_EQ,ucSizeOfCOG);
}
static void UI_FFS_Outputs_Functions_EPower( void )
{
   uint8_t ucSizeOfCOG = SysIO_GetSizeOfCOG(OUTPUT_GROUP__EPWR);
   Display_Status(OUTPUT_GROUP__EPWR,ucSizeOfCOG);
}
static void UI_FFS_Outputs_Functions_Insp( void )
{
   uint8_t ucSizeOfCOG = SysIO_GetSizeOfCOG(OUTPUT_GROUP__INSP);
   Display_Status(OUTPUT_GROUP__INSP,ucSizeOfCOG);
}
static void UI_FFS_Outputs_Functions_CTRL( void )
{
   uint8_t ucSizeOfCOG = SysIO_GetSizeOfCOG(OUTPUT_GROUP__CTRL);
   Display_Status(OUTPUT_GROUP__CTRL,ucSizeOfCOG);
}
static void UI_FFS_Outputs_Functions_Safety( void )
{
   uint8_t ucSizeOfCOG = SysIO_GetSizeOfCOG(OUTPUT_GROUP__SAFETY);
   Display_Status(OUTPUT_GROUP__SAFETY,ucSizeOfCOG);
}
static void UI_FFS_Outputs_Functions_CCL_F( void )
{
   uint8_t ucSizeOfCOG = SysIO_GetSizeOfCOG(OUTPUT_GROUP__CCL_F);
   Display_Status(OUTPUT_GROUP__CCL_F,ucSizeOfCOG);
}
static void UI_FFS_Outputs_Functions_CCL_R( void )
{
   uint8_t ucSizeOfCOG = SysIO_GetSizeOfCOG(OUTPUT_GROUP__CCL_R);
   Display_Status(OUTPUT_GROUP__CCL_R,ucSizeOfCOG);
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
