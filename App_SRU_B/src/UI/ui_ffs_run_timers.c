/******************************************************************************
 *
 * @file     ui_ffs_doors.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Delay_AccelDelay_Auto(void);
static void UI_FFS_Delay_AccelDelay_Insp(void);
static void UI_FFS_Delay_BrakePick_Auto();
static void UI_FFS_Delay_BrakePick_Insp();

static void UI_FFS_Delay_BrakeDropA(void);
static void UI_FFS_Delay_BrakeDropI(void);
static void UI_FFS_Delay_EBrakeDropA(void);
static void UI_FFS_Delay_EBrakeDropI(void);
static void UI_FFS_Delay_B2DropA(void);
static void UI_FFS_Delay_B2DropI(void);

static void UI_FFS_Delay_DriveDropA(void);
static void UI_FFS_Delay_DriveDropI(void);
static void UI_FFS_Delay_MotorDropA(void);
static void UI_FFS_Delay_MotorDropI(void);

static void UI_FFS_DisableRampZero(void);
static void UI_FFS_DisableHoldZero(void);
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
//---------------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Delay_AccelDelay_Auto =
{
   .pfnDraw = UI_FFS_Delay_AccelDelay_Auto,
};
static struct st_ui_screen__freeform gstFFS_Delay_AccelDelay_Insp =
{
   .pfnDraw = UI_FFS_Delay_AccelDelay_Insp,
};

static struct st_ui_screen__freeform gstFFS_Delay_BrakePick_Auto =
{
   .pfnDraw = UI_FFS_Delay_BrakePick_Auto,
};
static struct st_ui_screen__freeform gstFFS_Delay_BrakePick_Insp =
{
   .pfnDraw = UI_FFS_Delay_BrakePick_Insp,
};

static struct st_ui_screen__freeform gstFFS_Delay_BrakeDropA =
{
   .pfnDraw = UI_FFS_Delay_BrakeDropA,
};
static struct st_ui_screen__freeform gstFFS_Delay_BrakeDropI =
{
   .pfnDraw = UI_FFS_Delay_BrakeDropI,
};
static struct st_ui_screen__freeform gstFFS_Delay_B2DropA =
{
   .pfnDraw = UI_FFS_Delay_B2DropA,
};
static struct st_ui_screen__freeform gstFFS_Delay_B2DropI =
{
   .pfnDraw = UI_FFS_Delay_B2DropI,
};

static struct st_ui_screen__freeform gstFFS_Delay_EBrakeDropA =
{
   .pfnDraw = UI_FFS_Delay_EBrakeDropA,
};
static struct st_ui_screen__freeform gstFFS_Delay_EBrakeDropI =
{
   .pfnDraw = UI_FFS_Delay_EBrakeDropI,
};

static struct st_ui_screen__freeform gstFFS_Delay_DriveDropA =
{
   .pfnDraw = UI_FFS_Delay_DriveDropA,
};
static struct st_ui_screen__freeform gstFFS_Delay_DriveDropI =
{
   .pfnDraw = UI_FFS_Delay_DriveDropI,
};

static struct st_ui_screen__freeform gstFFS_Delay_MotorDropA =
{
   .pfnDraw = UI_FFS_Delay_MotorDropA,
};
static struct st_ui_screen__freeform gstFFS_Delay_MotorDropI =
{
   .pfnDraw = UI_FFS_Delay_MotorDropI,
};
static struct st_ui_screen__freeform gstFFS_DisableRampZero =
{
   .pfnDraw = UI_FFS_DisableRampZero,
};
static struct st_ui_screen__freeform gstFFS_DisableHoldZero =
{
   .pfnDraw = UI_FFS_DisableHoldZero,
};
//---------------------------------------------------------------

static struct st_ui_menu_item gstMI_Timers_AccelDelay_Auto =
{
   .psTitle = "Accel Delay (Auto)",
   .pstUGS_Next = &gstUGS_Delay_AccelDelay_Auto,
};
static struct st_ui_menu_item gstMI_Timers_AccelDelay_Insp =
{
   .psTitle = "Accel Delay (Insp)",
   .pstUGS_Next = &gstUGS_Delay_AccelDelay_Insp,
};
static struct st_ui_menu_item gstMI_Timers_BrakePick_Auto =
{
   .psTitle = "Brake Pick (Auto)",
   .pstUGS_Next = &gstUGS_Delay_BrakePick_Auto,
};
static struct st_ui_menu_item gstMI_Timers_BrakePick_Insp =
{
   .psTitle = "Brake Pick (Insp)",
   .pstUGS_Next = &gstUGS_Delay_BrakePick_Insp,
};
static struct st_ui_menu_item * gastMenuItems_StartTimers[] =
{
   &gstMI_Timers_AccelDelay_Auto,
   &gstMI_Timers_AccelDelay_Insp,
   &gstMI_Timers_BrakePick_Auto,
   &gstMI_Timers_BrakePick_Insp,
};
static struct st_ui_screen__menu gstMenu_StartTimers =
{
   .psTitle = "Start Timers",
   .pastMenuItems = &gastMenuItems_StartTimers,
   .ucNumItems = sizeof(gastMenuItems_StartTimers) / sizeof(gastMenuItems_StartTimers[ 0 ]),
};
//-------------------------------------------------

static struct st_ui_menu_item gstMI_Timers_BrakeDropA =
{
   .psTitle = "Brake Drop (Auto)",
   .pstUGS_Next = &gstUGS_Delay_BrakeDropA,
};
static struct st_ui_menu_item gstMI_Timers_BrakeDropI =
{
   .psTitle = "Brake Drop (Insp)",
   .pstUGS_Next = &gstUGS_Delay_BrakeDropI,
};
static struct st_ui_menu_item gstMI_Timers_EBrakeDropA =
{
   .psTitle = "EBrake Drop (Auto)",
   .pstUGS_Next = &gstUGS_Delay_EBrakeDropA,
};
static struct st_ui_menu_item gstMI_Timers_EBrakeDropI =
{
   .psTitle = "EBrake Drop (Insp)",
   .pstUGS_Next = &gstUGS_Delay_EBrakeDropI,
};
static struct st_ui_menu_item gstMI_Timers_B2DropA =
{
   .psTitle = "B2 Drop (Auto)",
   .pstUGS_Next = &gstUGS_Delay_B2DropA,
};
static struct st_ui_menu_item gstMI_Timers_B2DropI =
{
   .psTitle = "B2 Drop (Insp)",
   .pstUGS_Next = &gstUGS_Delay_B2DropI,
};
static struct st_ui_menu_item gstMI_Timers_MotorDropA =
{
   .psTitle = "Motor Drop (Auto)",
   .pstUGS_Next = &gstUGS_Delay_MotorDropA,
};
static struct st_ui_menu_item gstMI_Timers_MotorDropI =
{
   .psTitle = "Motor Drop (Insp)",
   .pstUGS_Next = &gstUGS_Delay_MotorDropI,
};
static struct st_ui_menu_item gstMI_Timers_DriveDropA =
{
   .psTitle = "Drive Drop (Auto)",
   .pstUGS_Next = &gstUGS_Delay_DriveDropA,
};
static struct st_ui_menu_item gstMI_Timers_DriveDropI =
{
   .psTitle = "Drive Drop (Insp)",
   .pstUGS_Next = &gstUGS_Delay_DriveDropI,
};
static struct st_ui_menu_item gstMI_DisableRampZero =
{
   .psTitle = "Dis. Ramp Zero",
   .pstUGS_Next = &gstUGS_DisableRampZero,
};
static struct st_ui_menu_item gstMI_DisableHoldZero =
{
   .psTitle = "Dis. Hold Zero",
   .pstUGS_Next = &gstUGS_DisableHoldZero,
};
static struct st_ui_menu_item * gastMenuItems_StopTimers[] =
{
      &gstMI_Timers_BrakeDropA,
      &gstMI_Timers_BrakeDropI,

      &gstMI_Timers_DriveDropA,
      &gstMI_Timers_DriveDropI,
      &gstMI_Timers_MotorDropA,
      &gstMI_Timers_MotorDropI,

      &gstMI_Timers_B2DropA,
      &gstMI_Timers_B2DropI,
      &gstMI_Timers_EBrakeDropA,
      &gstMI_Timers_EBrakeDropI,
      &gstMI_DisableRampZero,
      &gstMI_DisableHoldZero,
};
static struct st_ui_screen__menu gstMenu_StopTimers =
{
   .psTitle = "Stop Timers",
   .pastMenuItems = &gastMenuItems_StopTimers,
   .ucNumItems = sizeof(gastMenuItems_StopTimers) / sizeof(gastMenuItems_StopTimers[ 0 ]),
};
//---------------------------------------
static struct st_ui_menu_item gstMI_Timers_StartTimers =
{
   .psTitle = "Start Timers",
   .pstUGS_Next = &gstUGS_Menu_Setup_StartTimers,
};
static struct st_ui_menu_item gstMI_Timers_StopTimers =
{
   .psTitle = "Stop Timers",
   .pstUGS_Next = &gstUGS_Menu_Setup_StopTimers,
};
static struct st_ui_menu_item * gastMenuItems_Timers[] =
{
      &gstMI_Timers_StartTimers,
      &gstMI_Timers_StopTimers,
};

static struct st_ui_screen__menu gstMenu_Timers =
{
   .psTitle = "Timers",
   .pastMenuItems = &gastMenuItems_Timers,
   .ucNumItems = sizeof(gastMenuItems_Timers) / sizeof(gastMenuItems_Timers[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_Delay_AccelDelay_Auto =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Delay_AccelDelay_Auto,
};
struct st_ui_generic_screen gstUGS_Delay_AccelDelay_Insp =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Delay_AccelDelay_Insp,
};
struct st_ui_generic_screen gstUGS_Delay_BrakePick_Auto =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Delay_BrakePick_Auto,
};

struct st_ui_generic_screen gstUGS_Delay_BrakePick_Insp =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Delay_BrakePick_Insp,
};

struct st_ui_generic_screen gstUGS_Delay_BrakeDropA =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Delay_BrakeDropA,
};
struct st_ui_generic_screen gstUGS_Delay_BrakeDropI =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Delay_BrakeDropI,
};

struct st_ui_generic_screen gstUGS_Delay_B2DropA =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Delay_B2DropA,
};
struct st_ui_generic_screen gstUGS_Delay_B2DropI =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Delay_B2DropI,
};

struct st_ui_generic_screen gstUGS_Delay_EBrakeDropA =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Delay_EBrakeDropA,
};
struct st_ui_generic_screen gstUGS_Delay_EBrakeDropI =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Delay_EBrakeDropI,
};

struct st_ui_generic_screen gstUGS_Delay_MotorDropA =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Delay_MotorDropA,
};
struct st_ui_generic_screen gstUGS_Delay_MotorDropI =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Delay_MotorDropI,
};
struct st_ui_generic_screen gstUGS_Delay_DriveDropA =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Delay_DriveDropA,
};
struct st_ui_generic_screen gstUGS_Delay_DriveDropI =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Delay_DriveDropI,
};
struct st_ui_generic_screen gstUGS_DisableRampZero =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DisableRampZero,
};
struct st_ui_generic_screen gstUGS_DisableHoldZero =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DisableHoldZero,
};
//-----------------------------------------
struct st_ui_generic_screen gstUGS_Menu_Setup_StartTimers =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_StartTimers,
};
struct st_ui_generic_screen gstUGS_Menu_Setup_StopTimers =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_StopTimers,
};
struct st_ui_generic_screen gstUGS_Menu_Setup_Timers =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Timers,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Delay_AccelDelay_Auto( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__AccelDelay_ms;
   stParamEdit.uwParamIndex_End = enPARAM16__AccelDelay_ms;
   stParamEdit.psTitle = "Accel Delay (Auto)";
   stParamEdit.psUnit = " msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Delay_AccelDelay_Insp( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__AccelDelay_Insp_ms;
   stParamEdit.uwParamIndex_End = enPARAM16__AccelDelay_Insp_ms;
   stParamEdit.psTitle = "Accel Delay (Insp)";
   stParamEdit.psUnit = " msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Delay_BrakePick_Auto( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__BrakePickDelay_Auto_ms;
   stParamEdit.uwParamIndex_End = enPARAM16__BrakePickDelay_Auto_ms;
   stParamEdit.psTitle = "Brake Pick Delay (Auto)";
   stParamEdit.psUnit = " msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Delay_BrakePick_Insp( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__BrakePickDelay_Insp_ms;
   stParamEdit.uwParamIndex_End = enPARAM16__BrakePickDelay_Insp_ms;
   stParamEdit.psTitle = "Brake Pick Delay (Insp)";
   stParamEdit.psUnit = " msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Delay_BrakeDropA( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__BrakeDropDelay_Auto_1ms;
   stParamEdit.uwParamIndex_End = enPARAM16__BrakeDropDelay_Auto_1ms;
   stParamEdit.psTitle = "Brake Drop Delay (Auto)";
   stParamEdit.psUnit = " msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Delay_BrakeDropI( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__BrakeDropDelay_Insp_1ms;
   stParamEdit.uwParamIndex_End = enPARAM16__BrakeDropDelay_Insp_1ms;
   stParamEdit.psTitle = "Brake Drop Delay (Insp)";
   stParamEdit.psUnit = " msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define MIN_EBRAKE_DROP_TIME__MS                   (1000)
static void UI_FFS_Delay_EBrakeDropA( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__EBrakeDropDelay_Auto_1ms;
   stParamEdit.uwParamIndex_End = enPARAM16__EBrakeDropDelay_Auto_1ms;
   stParamEdit.psTitle = "EBrake Drop Delay (Auto)";
   stParamEdit.psUnit = " msec";
   stParamEdit.ulValue_Min = MIN_EBRAKE_DROP_TIME__MS;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Delay_EBrakeDropI( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__EBrakeDropDelay_Insp_1ms;
   stParamEdit.uwParamIndex_End = enPARAM16__EBrakeDropDelay_Insp_1ms;
   stParamEdit.psTitle = "EBrake Drop Delay (Insp)";
   stParamEdit.psUnit = " msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Delay_B2DropA( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__B2DropDelay_Auto_1ms;
   stParamEdit.uwParamIndex_End = enPARAM16__B2DropDelay_Auto_1ms;
   stParamEdit.psTitle = "B2 Drop Delay (Auto)";
   stParamEdit.psUnit = " msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Delay_B2DropI( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__B2DropDelay_Insp_1ms;
   stParamEdit.uwParamIndex_End = enPARAM16__B2DropDelay_Insp_1ms;
   stParamEdit.psTitle = "B2 Drop Delay (Insp)";
   stParamEdit.psUnit = " msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Delay_DriveDropA( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__DriveDropDelay_Auto_1ms;
   stParamEdit.uwParamIndex_End = enPARAM16__DriveDropDelay_Auto_1ms;
   stParamEdit.psTitle = "Drive Drop Delay (Auto)";
   stParamEdit.psUnit = " msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Delay_DriveDropI( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__DriveDropDelay_Insp_1ms;
   stParamEdit.uwParamIndex_End = enPARAM16__DriveDropDelay_Insp_1ms;
   stParamEdit.psTitle = "Drive Drop Delay (Insp)";
   stParamEdit.psUnit = " msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Delay_MotorDropA( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__MotorDropDelay_Auto_1ms;
   stParamEdit.uwParamIndex_End = enPARAM16__MotorDropDelay_Auto_1ms;
   stParamEdit.psTitle = "Motor Drop Delay (Auto)";
   stParamEdit.psUnit = " msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Delay_MotorDropI( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__MotorDropDelay_Insp_1ms;
   stParamEdit.uwParamIndex_End = enPARAM16__MotorDropDelay_Insp_1ms;
   stParamEdit.psTitle = "Motor Drop Delay (Insp)";
   stParamEdit.psUnit = " msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_DisableRampZero( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__StopSeq_DisableRampZero;
   stParamEdit.uwParamIndex_End = enPARAM1__StopSeq_DisableRampZero;
   stParamEdit.psTitle = "Disable Ramp Zero";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_DisableHoldZero( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__StopSeq_DisableHoldZero;
   stParamEdit.uwParamIndex_End = enPARAM1__StopSeq_DisableHoldZero;
   stParamEdit.psTitle = "Disable Hold Zero";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

