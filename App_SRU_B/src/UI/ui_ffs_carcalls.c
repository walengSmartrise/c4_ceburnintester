/******************************************************************************
 *
 * @file     ui_ffs_carcalls.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include "operation.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_CC_Latch_F( void );
static void UI_FFS_CC_Latch_R( void );
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_CC_Latch_F =
{
   .pfnDraw = UI_FFS_CC_Latch_F,
};
static struct st_ui_screen__freeform gstFFS_CC_Latch_R =
{
   .pfnDraw = UI_FFS_CC_Latch_R,
};
static uint8_t bRearDoor;
static uint8_t gucDebugLatchCC_XmtCountdown;  // clear request after 150ms in ui_mod.c
static uint8_t gucDebugLatchCC_Request_Plus1;
static uint8_t bRearRequest;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_CC_Latch_F =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_CC_Latch_F,
};
struct st_ui_generic_screen gstUGS_CC_Latch_R =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_CC_Latch_R,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define LATCH_CC_REQUEST_DELAY_5MS        (30U)
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
static void UI_FFS_CC_Latch( void )
 -----------------------------------------------------------------------------*/
void Update_CC_LatchRequest( void )
{
   if( gucDebugLatchCC_Request_Plus1 )
   {
      uint8_t bDoor = bRearRequest;
      uint8_t ucFloor_Plus1 = gucDebugLatchCC_Request_Plus1;
      if(gucDebugLatchCC_XmtCountdown)
      {
         gucDebugLatchCC_XmtCountdown--;
      }
      else
      {
         gucDebugLatchCC_Request_Plus1 = 0;
      }
      if( ucFloor_Plus1 )
      {
         SetCarCall( ucFloor_Plus1-1, bDoor );
      }
   }
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void ShowLatchedCalls( void )
{
   uint8_t i;
   uint8_t j;
   static uint8_t ucCCB_Index;
   static uint8_t ucLatchArrayIndex;
   static uint8_t aucTempLatchedCallsIndex_Plus1[ MAX_VISIBLE_CALLS ];

   for ( i = 0; i < 10; ++i )  // only process 10 CCBs per call
   {
      if ( ucCCB_Index >= GetFP_NumFloors() )
      {
         ucCCB_Index = 0;

         ucLatchArrayIndex = 0;

         // Double buffering is to prevent display from flickering every time
         // ucLatchArrayIndex is reset to 0.
         for ( j = 0; j < MAX_VISIBLE_CALLS; ++j )
         {
            gaucLatchedCallsIndex_Plus1[ j ] = aucTempLatchedCallsIndex_Plus1[ j ];
            aucTempLatchedCallsIndex_Plus1[ j ] = 0;
         }
      }

      if ( ucLatchArrayIndex < MAX_VISIBLE_CALLS )
      {
         if(GetLatchedCCB(ucCCB_Index, bRearDoor))
         {
            aucTempLatchedCallsIndex_Plus1[ ucLatchArrayIndex++ ] = ucCCB_Index + 1;
         }
      }

      ++ucCCB_Index;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ClearScreenAndDrawFixedText( void )
{
   char * ps;

   LCD_Char_Clear();

   LCD_Char_GotoXY( 0, 1 );
   ps = "Floor";
   LCD_Char_WriteStringUpper( ps );
   LCD_Char_GotoXY( 2, 3 );
   ps = "*";
   LCD_Char_WriteStringUpper( ps );

   if ( gaucLatchedCallsIndex_Plus1[ 0 ] )
   {
      LCD_Char_GotoXY( 5, 2 );
      ps = "----Latched----";
      LCD_Char_WriteStringUpper( ps );
   }
}
/*-----------------------------------------------------------------------------
eDir = DIR__NONE for checkf if current floor is valid
-----------------------------------------------------------------------------*/
static uint8_t GetNextOpening(uint8_t ucFloorIndex, enum direction_enum eDir, uint8_t bRear)
{
   uint8_t ucNextFloor = INVALID_FLOOR;
   if( eDir == DIR__UP )
   {
      ucNextFloor = ucFloorIndex;
      if( ucFloorIndex < GetFP_NumFloors()-1 )
      {
         for(uint8_t ucFloor = ucFloorIndex+1; ucFloor < GetFP_NumFloors(); ucFloor++)
         {
            uint8_t ucMapIndex = ucFloor / 32;
            uint8_t ucBitIndex = ucFloor % 32;
            uint32_t uiOpeningMask = (bRear) ? Param_ReadValue_32Bit( enPARAM32__OpeningBitmapR_0+ucMapIndex )
                                              :Param_ReadValue_32Bit( enPARAM32__OpeningBitmapF_0+ucMapIndex );
            if(Sys_Bit_Get(&uiOpeningMask, ucBitIndex ))
            {
               ucNextFloor = ucFloor;
               break;
            }
         }
      }
   }
   else if( eDir == DIR__DN )
   {
      ucNextFloor = ucFloorIndex;
      if(ucFloorIndex)
      {
         for(int8_t cFloor = ucFloorIndex-1; cFloor >= 0; cFloor--)
         {
            uint8_t ucMapIndex = cFloor / 32;
            uint8_t ucBitIndex = cFloor % 32;
            uint32_t uiOpeningMask = (bRear) ? Param_ReadValue_32Bit( enPARAM32__OpeningBitmapR_0+ucMapIndex )
                                              :Param_ReadValue_32Bit( enPARAM32__OpeningBitmapF_0+ucMapIndex );
            if(Sys_Bit_Get(&uiOpeningMask, ucBitIndex ))
            {
               ucNextFloor = cFloor;
               break;
            }
         }
      }
   }
   else if( ( eDir == DIR__NONE )
         && ( ucFloorIndex < GetFP_NumFloors() ) )
   {
      uint8_t ucMapIndex = ucFloorIndex / 32;
      uint8_t ucBitIndex = ucFloorIndex % 32;
      uint32_t uiOpeningMask = (bRear) ? Param_ReadValue_32Bit( enPARAM32__OpeningBitmapR_0+ucMapIndex )
                                        :Param_ReadValue_32Bit( enPARAM32__OpeningBitmapF_0+ucMapIndex );
      if(Sys_Bit_Get(&uiOpeningMask, ucBitIndex ))
      {
         ucNextFloor = ucFloorIndex;
      }
   }

   return ucNextFloor;
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void DrawScreen_EnterCarCalls( void )
{
   char * ps;
   uint8_t i;
   static uint8_t ucFloorIndex;

   ClearScreenAndDrawFixedText();

   int16_t wNewFloorIndex = GetNextOpening(ucFloorIndex, DIR__NONE, bRearDoor);
   if( wNewFloorIndex == INVALID_FLOOR )
   {
      wNewFloorIndex = GetNextOpening(0, DIR__UP, bRearDoor);
   }

   enum en_keypresses enKeypress = Button_GetKeypress();
   if ( enKeypress )
   {
      if ( enKeypress == enKEYPRESS_LEFT )
      {
         PrevScreen();
      }
      else if ( enKeypress == enKEYPRESS_UP )
      {
         wNewFloorIndex = GetNextOpening(ucFloorIndex, DIR__UP, bRearDoor);
      }
      else if ( enKeypress == enKEYPRESS_DOWN )
      {
         wNewFloorIndex = GetNextOpening(ucFloorIndex, DIR__DN, bRearDoor);
      }
      else if ( enKeypress == enKEYPRESS_ENTER )
      {
         gucDebugLatchCC_Request_Plus1 = wNewFloorIndex + 1;
         bRearRequest = bRearDoor;
         gucDebugLatchCC_XmtCountdown = LATCH_CC_REQUEST_DELAY_5MS;
      }
   }

   ucFloorIndex = wNewFloorIndex;

   LCD_Char_GotoXY( 0, 0 );

   ps = "Enter Car Call: ";
   LCD_Char_WriteStringUpper( ps );

   LCD_Char_GotoXY( 15, 0 );
   LCD_Char_WriteStringUpper( "[" );
   char * psPI = Get_PI_Label( GetOperation_CurrentFloor() );
   LCD_Char_WriteStringUpper( psPI );
   LCD_Char_WriteStringUpper( "]" );

   ShowLatchedCalls();

   for ( i = 0; i < MAX_VISIBLE_CALLS; ++i )
   {
      if ( gaucLatchedCallsIndex_Plus1[ i ] == 0 )
      {
         break;
      }
      else
      {
         LCD_Char_GotoXY( 5 + (i * 3), 3 );
         psPI = Get_PI_Label( gaucLatchedCallsIndex_Plus1[i] - 1 );
         LCD_Char_WriteStringUpper( psPI );
      }
   }

   LCD_Char_GotoXY( 1, 2 );
   psPI = Get_PI_Label( ucFloorIndex );
   LCD_Char_WriteStringUpper( psPI );


   LCD_Char_GotoXY( 6, 1 );
   LCD_Char_WriteInteger_MinLength(ucFloorIndex+1,1);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Edit_Latched_CCs( void )
{
   if((GetOperation_ClassOfOp() != CLASSOP__AUTO))
   {
      LCD_Char_Clear();
      LCD_Char_GotoXY(1, 2);
      LCD_Char_WriteString("Invalid: Move Car");
      LCD_Char_GotoXY(1, 3);
      LCD_Char_WriteString("to Auto Operation");

      enum en_keypresses enKeypress = Button_GetKeypress();
      if ( enKeypress != enKEYPRESS_NONE)
      {
         PrevScreen();
      }
   }
   else if( bRearDoor && !GetFP_RearDoors() )
   {
      PrevScreen();
   }
   else
   {
      DrawScreen_EnterCarCalls();

   }
}

static void TEST_EnterFloorToFloor()
{
   static uint16_t uwDelay = 0;
   static uint8_t ucRandomRun;
   static uint8_t bUp = 1;
   uint8_t bEnterCall = 1;
   if( ( GetSRU_Deployment() == enSRU_DEPLOYMENT__MR )
    && ( Param_ReadValue_1Bit( enPARAM1__DEBUG_RunFloorToFloor ) ) )
   {
      for(uint8_t i = 0; i < MAX_VISIBLE_CALLS; i++)
      {
         if(gaucLatchedCallsIndex_Plus1[i])
         {
            bEnterCall = 0;
         }
      }
      if( gucDebugLatchCC_Request_Plus1 || GetMotion_RunFlag() )
      {
         bEnterCall = 0;
      }
      if(bEnterCall)
      {
         if(++uwDelay >= (Param_ReadValue_8Bit(enPARAM8__DEBUG_TestRunsDwellTime_s)*33.3)) // Scale for 30ms runtime
         {
            uwDelay = 0;
            gucDebugLatchCC_Request_Plus1 = (bUp) ? (GetOperation_CurrentFloor() + 1):(GetOperation_CurrentFloor() - 1);
            gucDebugLatchCC_Request_Plus1 %= GetFP_NumFloors();
            gucDebugLatchCC_Request_Plus1 +=1;
            bRearRequest = bRearDoor;
            gucDebugLatchCC_XmtCountdown = 5;
            if( bUp && ( gucDebugLatchCC_Request_Plus1 >= GetFP_NumFloors() ) )
            {
               bUp =0;
            }
            else if( !bUp && ( gucDebugLatchCC_Request_Plus1 <= 1 ) )
            {
               bUp =1;
            }
         }
      }
      else
      {
         uwDelay = 0;
      }
   }
   else if( ( GetSRU_Deployment() == enSRU_DEPLOYMENT__MR )
    && ( Param_ReadValue_1Bit( enPARAM1__EnableRandomCarCallRuns ) ) )
   {
      for(uint8_t i = 0; i < MAX_VISIBLE_CALLS; i++)
      {
         if(gaucLatchedCallsIndex_Plus1[i])
         {
            bEnterCall = 0;
         }
      }
      if( gucDebugLatchCC_Request_Plus1 || GetMotion_RunFlag() )
      {
         bEnterCall = 0;
      }
      if(bEnterCall)
      {
         if(++uwDelay >= (Param_ReadValue_8Bit(enPARAM8__DEBUG_TestRunsDwellTime_s)*33.3)) // Scale for 30ms runtime
         {
            ucRandomRun +=1;
            uint8_t ucNewFloor = GetOperation_CurrentFloor()+ucRandomRun;
            if(!bUp)
            {
               ucNewFloor = GetOperation_CurrentFloor()-ucRandomRun;
            }
            ucNewFloor %= GetFP_NumFloors();
            uint8_t bRear = 0;
            if(GetFloorOpening_Front(ucNewFloor))
            {
               uwDelay = 0;
               bUp = !bUp;
               gucDebugLatchCC_Request_Plus1 = ucNewFloor;
               gucDebugLatchCC_Request_Plus1 +=1;
               bRearRequest = bRear;
               gucDebugLatchCC_XmtCountdown = 5;
            }
            else if(GetFloorOpening_Rear(ucNewFloor))
            {
               bRear = 1;
               uwDelay = 0;
               bUp = !bUp;
               gucDebugLatchCC_Request_Plus1 = ucNewFloor;
               gucDebugLatchCC_Request_Plus1 +=1;
               bRearRequest = bRear;
               gucDebugLatchCC_XmtCountdown = 5;
            }
         }
      }
      else
      {
         uwDelay = 0;
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_CC_Latch_F( void )
{
   bRearDoor = 0;
   Edit_Latched_CCs();

   TEST_EnterFloorToFloor();
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_CC_Latch_R( void )
{
   bRearDoor = 1;
   Edit_Latched_CCs();

   TEST_EnterFloorToFloor();
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
