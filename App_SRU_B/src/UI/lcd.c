/******************************************************************************
 *
 * @file     lcd.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     22, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "lcd.h"

#include "sru_b.h"
#include <stdint.h>
#include <string.h>
#include <math.h>
#include "sys.h"

/******************************************************************************
 *
 * @file     lcd.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     22, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "lcd.h"

#include <stdint.h>
#include <string.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

//SDA REVIEW: can this be static
enum lcd_char_update_state
{
   LCD_CHAR_UPDATE_STATE__FIND_ROW_COLUMN_TO_UPDATE,
   LCD_CHAR_UPDATE_STATE__MOVE_TO_ROW_COLUMN,
   LCD_CHAR_UPDATE_STATE__WRITE_CHAR,

   NUM_LCD_CHAR_UPDATE_STATE
};

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static uint8_t gucCurrentX;
static uint8_t gucCurrentY;

static uint8_t gaaucLCD_Char_DisplayBuffer[ LCD_CHAR_NUM_ROWS ][ LCD_CHAR_NUM_COLUMNS ];

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static inline void Write_E( uint_fast8_t const bState )
{
//   Sys_MCU_Pin_Out( 0, 30, bState );
   SRU_Write_LCD(enSRU_LCD_OUTPUT__EN, bState);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static inline void Write_RW( uint_fast8_t const bState )
{
//   Sys_MCU_Pin_Out( 0, 29, bState );
   SRU_Write_LCD(enSRU_LCD_OUTPUT__RW, bState);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static inline void Write_RS( uint_fast8_t const bState )
{
//   Sys_MCU_Pin_Out( 0, 26, bState );
   SRU_Write_LCD(enSRU_LCD_OUTPUT__RS, bState);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LCD_Delay( int iDelay )
{
   int iCount = 0;

   for (iCount = 0; iCount < iDelay; iCount++)
   {
      __NOP();
      __NOP();
      __NOP();

   }
}

#define EXTRACT_BIT(data,bit) ( ((data>>bit) & 0x01) ? true : false)

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
//TODO: Break this function into two pieces that get called every other time.
//      This will allow getting rid of the LCD_Delay() busy loop.
void LCD_Write( uint_fast8_t const bData, uint8_t const ucDataValue )
{
   Write_RW( 0 );

   Write_E( 0 );

   Write_RS( bData );

   // Data sheet indicates a 40 ns delay should come here but the LCD does not
   // seem to need it. Could be for backwards compatibility with older models.
   LCD_Delay( 10 );

   Write_E( 1 );

   SRU_Write_LCD(enSRU_LCD_OUTPUT__DATA0, EXTRACT_BIT(ucDataValue,0));
   SRU_Write_LCD(enSRU_LCD_OUTPUT__DATA1, EXTRACT_BIT(ucDataValue,1));
   SRU_Write_LCD(enSRU_LCD_OUTPUT__DATA2, EXTRACT_BIT(ucDataValue,2));
   SRU_Write_LCD(enSRU_LCD_OUTPUT__DATA3, EXTRACT_BIT(ucDataValue,3));
   SRU_Write_LCD(enSRU_LCD_OUTPUT__DATA4, EXTRACT_BIT(ucDataValue,4));
   SRU_Write_LCD(enSRU_LCD_OUTPUT__DATA5, EXTRACT_BIT(ucDataValue,5));
   SRU_Write_LCD(enSRU_LCD_OUTPUT__DATA6, EXTRACT_BIT(ucDataValue,6));
   SRU_Write_LCD(enSRU_LCD_OUTPUT__DATA7, EXTRACT_BIT(ucDataValue,7));

   // This delay was found through trail and error. May need to increase if it
   // does not work for all LCD displays.
   LCD_Delay( 100 );

   Write_E( 0 );
}

/*--------------------------------------------------------------------------*/

void LCD_Char_AdvanceX( uint_fast8_t const ucAdvanceAmount )
{
   if ( ( gucCurrentX + ucAdvanceAmount ) < LCD_CHAR_NUM_COLUMNS )
   {
      gucCurrentX += ucAdvanceAmount;
   }
}
/*--------------------------------------------------------------------------*/
void LCD_Char_AdvanceY( uint_fast8_t const ucAdvanceAmount )
{
   if ( ( gucCurrentY + ucAdvanceAmount ) < LCD_CHAR_NUM_ROWS )
   {
      gucCurrentY += ucAdvanceAmount;
   }
}
/*--------------------------------------------------------------------------*/
void LCD_Char_GoToStartOfNextRow()
{
   if ( ( gucCurrentY + 1 ) < LCD_CHAR_NUM_ROWS )
   {
      gucCurrentY += 1;
      gucCurrentX = 0;
   }
}
/*--------------------------------------------------------------------------*/

void LCD_Char_GotoXY( uint_fast8_t const ucX, uint_fast8_t const ucY )
{
   if ( ucX < LCD_CHAR_NUM_COLUMNS )
   {
      gucCurrentX = ucX;
   }

   if ( ucY < LCD_CHAR_NUM_ROWS )
   {
      gucCurrentY = ucY;
   }
}

/*--------------------------------------------------------------------------*/

void LCD_Char_WriteChar( char const cData )
{
   if ( gucCurrentX < LCD_CHAR_NUM_COLUMNS )
   {
      gaaucLCD_Char_DisplayBuffer[ gucCurrentY ][ gucCurrentX ] = cData;

      ++gucCurrentX;
   }
}

/*--------------------------------------------------------------------------*/

void LCD_Char_WriteString( char const * const ps )
{
   if ( ps )
   {
      uint_fast8_t i;
      uint_fast8_t ucMaxLength;

      ucMaxLength = LCD_CHAR_NUM_COLUMNS - gucCurrentX;

      for ( i = 0; i < ucMaxLength; ++i )
      {
         if ( ps[ i ] != '\0' )
         {
            gaaucLCD_Char_DisplayBuffer[ gucCurrentY ][ gucCurrentX++ ] = ps[ i ];
         }
         else
         {
            break;
         }
      }
   }
}

/*--------------------------------------------------------------------------*/

static void LCD_Char_WriteStringMax( char const * const ps, uint_fast8_t len )
{
   if ( ps )
   {
      uint_fast8_t i;
      uint_fast8_t ucMaxLength;

      ucMaxLength = LCD_CHAR_NUM_COLUMNS - gucCurrentX;

      if ( ucMaxLength > len )
      {
         ucMaxLength = len;
      }

      for ( i = 0; i < ucMaxLength; ++i )
      {
         if ( ps[ i ] != '\0' )
         {
            gaaucLCD_Char_DisplayBuffer[ gucCurrentY ][ gucCurrentX++ ] = ps[ i ];
         }
         else
         {
            break;
         }
      }
   }
}

/*--------------------------------------------------------------------------*/

void LCD_Char_WriteStringUpper( char const * const ps )
{
   if ( ps )
   {
      uint_fast8_t i;
      uint_fast8_t ucMaxLength;

      ucMaxLength = LCD_CHAR_NUM_COLUMNS - gucCurrentX;

      for ( i = 0; i < ucMaxLength; ++i )
      {
         if ( ps[ i ] != '\0' )
         {
            if (    ( ps[ i ] >= 'a' )
                 && ( ps[ i ] <= 'z' )
                )
            {
               gaaucLCD_Char_DisplayBuffer[ gucCurrentY ][ gucCurrentX++ ] = ( ps[ i ] - 'a' ) + 'A';
            }
            else
            {
               gaaucLCD_Char_DisplayBuffer[ gucCurrentY ][ gucCurrentX++ ] = ps[ i ];
            }
         }
         else
         {
            break;
         }
      }
   }
}

/*--------------------------------------------------------------------------*/

void LCD_Char_WriteStringUpper_Centered( char const * const ps )
{
   uint8_t ucLength = strlen(ps);
   gucCurrentX = LCD_CHAR_NUM_COLUMNS/2 - ucLength/2;
   LCD_Char_WriteStringUpper(ps);
}
/*--------------------------------------------------------------------------*/

void LCD_Char_WriteString_Centered( char const * const ps )
{
   uint8_t ucLength = strlen(ps);
   gucCurrentX = LCD_CHAR_NUM_COLUMNS/2 - ucLength/2;
   LCD_Char_WriteString(ps);
}
/*--------------------------------------------------------------------------*/

static void IntToString( uint32_t ul , char * ps, uint_fast8_t ucMinLength )
{
   /* UINT32_MAX = 4294967295. 10 characters, plus NULL character */
   char sNumString[11];
   uint_fast8_t ucLength = 0;

   if ( ucMinLength > 10 )
   {
      ucMinLength = 10;
   }

   if ( ul == 0 )
   {
      sNumString[ ucLength++ ] = '0';

      if ( ucMinLength )
      {
         --ucMinLength;
      }
   }

   while ( ( ul ) || ( ucMinLength ) )
   {
      if ( ul )
      {
         sNumString[ ucLength++ ] = ( char ) ( ( ul % 10 ) + '0' );

         ul /= 10;
      }
      else
      {
         sNumString[ ucLength++ ] = '0';
      }

      if ( ucMinLength )
      {
         --ucMinLength;
      }
   }


   /* Reverse it and add NULL character */
   while ( ucLength )
   {
      *ps++ = sNumString[ --ucLength  ];
   };

   *ps = '\0';
}

/*--------------------------------------------------------------------------*/

void LCD_Char_WriteInteger( uint32_t ul )
{
   /* UINT32_MAX = 4294967295. 10 characters, plus NULL character */
   char sNumString[ 11 ];

   IntToString( ul, &( sNumString[ 0 ] ), 0 );

   LCD_Char_WriteString( &( sNumString[ 0 ] ) );
}

/*--------------------------------------------------------------------------*/

void LCD_Char_WriteSignedInteger( int32_t iValue )
{
   if ( iValue < 0 )
   {
      LCD_Char_WriteChar( '-' );

      iValue *= -1;
   }

   LCD_Char_WriteInteger( iValue );
}

/*--------------------------------------------------------------------------*/

void LCD_Char_WriteHex_8( uint8_t uc )
{
   char sNumString[ 3 ];

   sNumString[ 0 ] = (char) ( ( uc >> 4 ) & 0x0f );

   sNumString[ 0 ] = sNumString[ 0 ] + ( (sNumString[ 0 ] < 10) ? '0' : ( 'A' - 10 ) );

   sNumString[ 1 ] = (char) ( uc & 0x0f );

   sNumString[ 1 ] = sNumString[ 1 ] + ( (sNumString[ 1 ] < 10) ? '0' : ( 'A' - 10 ) );

   LCD_Char_WriteString( &sNumString[ 0 ] );
}

/*--------------------------------------------------------------------------*/

void LCD_Char_WriteHex_ExactLength( uint32_t ul, uint_fast8_t ucExactLength )
{
   while ( ucExactLength )
   {
      uint8_t uc = (uint8_t) (ul >> (4 * (ucExactLength - 1)));

      uc &= 0x0f;

      uc = (uc < 10) ? (uc + '0') : (uc - 10 + 'A');

      LCD_Char_WriteChar( uc );

      --ucExactLength;
   }
}

/*--------------------------------------------------------------------------*/

void LCD_Char_WriteInteger_MinLength( uint32_t ul, uint_fast8_t ucMinLength )
{
   /* UINT32_MAX = 4294967295. 10 characters, plus NULL character */
   char sNumString[11];

   IntToString( ul, &( sNumString[ 0 ] ), ucMinLength );

   LCD_Char_WriteString( &( sNumString[ 0 ] ) );
}

/*--------------------------------------------------------------------------*/

void LCD_Char_WriteInteger_ExactLength( uint32_t ul, uint_fast8_t ucExactLength )
{
   /* UINT32_MAX = 4294967295. 10 characters, plus NULL character */
   char sNumString[11];

   IntToString( ul, &( sNumString[ 0 ] ), ucExactLength );

   LCD_Char_WriteStringMax( &( sNumString[ 0 ] ), ucExactLength );
}

/*--------------------------------------------------------------------------*/

void LCD_Char_Clear( void )
{
   memset( &( gaaucLCD_Char_DisplayBuffer[ 0 ][ 0 ] ),
           ' ',
           sizeof( gaaucLCD_Char_DisplayBuffer )
          );

   gucCurrentY = 0;
   gucCurrentX = 0;
}

/*--------------------------------------------------------------------------*/

void LCD_Char_ClearRow( void )
{
   memset( &( gaaucLCD_Char_DisplayBuffer[ gucCurrentY ][ 0 ] ),
           ' ',
           LCD_CHAR_NUM_COLUMNS
          );
}

/*--------------------------------------------------------------------------*/
void LCD_Char_Update( void )
{
   static enum lcd_char_update_state eLCD_Char_UpdateState;
   static uint8_t aaucLCD_Char_DisplayBuffer_Old[ LCD_CHAR_NUM_ROWS ][ LCD_CHAR_NUM_COLUMNS ];
   static uint8_t ucRow;
   static uint8_t ucColumn;
   uint8_t uc;

   if ( eLCD_Char_UpdateState == LCD_CHAR_UPDATE_STATE__FIND_ROW_COLUMN_TO_UPDATE )
   {
      while ( ucColumn < LCD_CHAR_NUM_COLUMNS )
      {
         if ( gaaucLCD_Char_DisplayBuffer[ ucRow ][ ucColumn ] != aaucLCD_Char_DisplayBuffer_Old[ ucRow ][ ucColumn ] )
         {
            eLCD_Char_UpdateState = LCD_CHAR_UPDATE_STATE__MOVE_TO_ROW_COLUMN;
            break;
         }
         else
         {
            ++ucColumn;
         }
      }

      if ( eLCD_Char_UpdateState == LCD_CHAR_UPDATE_STATE__FIND_ROW_COLUMN_TO_UPDATE )
      {
         ucColumn = 0;

         ++ucRow;

         if ( ucRow >= LCD_CHAR_NUM_ROWS )
         {
            ucRow = 0;
         }
      }
   }

   if ( eLCD_Char_UpdateState == LCD_CHAR_UPDATE_STATE__MOVE_TO_ROW_COLUMN )
   {
      if (ucRow == 0)
      {
         uc = ucColumn;
      }
      else if (ucRow == 1)
      {
         uc = 0x40 + ucColumn;
      }
      else if (ucRow == 2)
      {
         uc = 0x14 + ucColumn;
      }
      else
      {
         uc = 0x54 + ucColumn;
      }

      LCD_Write( 0, ( 0x80 | uc ) );

      eLCD_Char_UpdateState = LCD_CHAR_UPDATE_STATE__WRITE_CHAR;
   }
   else if ( eLCD_Char_UpdateState == LCD_CHAR_UPDATE_STATE__WRITE_CHAR )
   {
      uc = gaaucLCD_Char_DisplayBuffer[ ucRow ][ ucColumn ];

      aaucLCD_Char_DisplayBuffer_Old[ucRow][ ucColumn ] = uc;

      LCD_Write( 1, uc );

      eLCD_Char_UpdateState = LCD_CHAR_UPDATE_STATE__FIND_ROW_COLUMN_TO_UPDATE;
   }
   else
   {
      eLCD_Char_UpdateState = LCD_CHAR_UPDATE_STATE__FIND_ROW_COLUMN_TO_UPDATE;
   }
}

/*--------------------------------------------------------------------------*/
void LCD_Char_WriteBin_16( uint16_t uw )
{
   char sBin[ 21 ];
   char sBinSend[ 21 ];

   uint8_t i, j, ucBit;
   uint8_t ucShift = 0;
   for( i = 0; i < 4; i++)
   {
      for( j = 0; j < 4; j++)
      {
         ucBit = ( uw >> (i*4 + j) ) & 0x01;
         sBin[ i*4 + j + ucShift ] = (ucBit == 1) ? '1':'0';
         if((j == 3) && (i < 3))
         {
            ucShift++;
            sBin[ i*4 + j + ucShift ] = ' ';
         }
      }
   }

   /* Reverse it and add NULL character */
   for( i = 0; i < 20; i++ )
   {
      sBinSend[i] = sBin[ 19 - i ];
   }

   sBinSend[20] = '\0';

   LCD_Char_WriteString( &sBinSend[1] );
}

/*--------------------------------------------------------------------------*/
void LCD_Char_WriteFloat_ExactLength( float f, uint8_t ucIntegerDigits, uint8_t ucFractionDigits )
{
   if(f < 0) {
      LCD_Char_WriteChar('-');
   } else {
      LCD_Char_WriteChar('+');
   }
   if(ucIntegerDigits) {
      for(int8_t i = ucIntegerDigits-1; i >= 0; i--) {
          uint32_t uiPower = powf(10, i);
          uint32_t uiValue = f;
          uiValue /= uiPower;
          uiValue %= 10;
          uint8_t uc = uiValue + '0';
          LCD_Char_WriteChar( uc );
      }
   }
   else {
      LCD_Char_WriteChar('0');
   }
   if(ucFractionDigits) {
      LCD_Char_WriteChar('.');
      for(int8_t i = 1; i <= ucFractionDigits; i++) {
          uint32_t uiPower = powf(10, i);
          uint32_t uiValue = f*uiPower;
          uiValue %= 10;
          uint8_t uc = uiValue + '0';
          LCD_Char_WriteChar( uc );
      }
   }
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
