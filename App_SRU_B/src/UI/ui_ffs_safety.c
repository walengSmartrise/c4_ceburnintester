/******************************************************************************
 *
 * @file     ui_ffs_safety.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_SpeedDev_Threshold( void );
static void UI_FFS_SpeedDev_Timeout( void );
static void UI_FFS_SpeedDev_Offset( void );

static void UI_FFS_TractionLoss_Threshold( void );
static void UI_FFS_TractionLoss_Timeout( void );
static void UI_FFS_TractionLoss_Offset( void );

static void UI_FFS_LockClip( void );
static void UI_FFS_InspectionODL( void );
static void UI_FFS_DoorOpenODL( void );
static void UI_FFS_ETS_ODL( void );
static void UI_FFS_SFP_DL( void );
static void UI_FFS_GeneralODL( void );
static void UI_FFS_NTS_ODL( void );
static void UI_FFS_EBRAKE_ON_OVERSPEED();
static void UI_FFS_DisableConstOverspeed();
static void UI_FFS_ETS_Offset();

static void UI_FFS_ETSL_ODL( void );
static void UI_FFS_Enable_ETSL( void );
static void UI_FFS_RatedBufferSpd( void );
static void UI_FFS_BufferDistance( void );
static void UI_FFS_ETSL_CameraOffset( void );
static void UI_FFS_SlideDistance( void );

static void UI_FFS_Construction_ODL( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

//---------------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_SpeedDev_Threshold =
{
   .pfnDraw = UI_FFS_SpeedDev_Threshold,
};
static struct st_ui_screen__freeform gstFFS_SpeedDev_Timeout =
{
   .pfnDraw = UI_FFS_SpeedDev_Timeout,
};
static struct st_ui_screen__freeform gstFFS_SpeedDev_Offset =
{
   .pfnDraw = UI_FFS_SpeedDev_Offset,
};
//---------------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_TractionLoss_Threshold =
{
   .pfnDraw = UI_FFS_TractionLoss_Threshold,
};
static struct st_ui_screen__freeform gstFFS_TractionLoss_Timeout =
{
   .pfnDraw = UI_FFS_TractionLoss_Timeout,
};
static struct st_ui_screen__freeform gstFFS_TractionLoss_Offset =
{
   .pfnDraw = UI_FFS_TractionLoss_Offset,
};

static struct st_ui_screen__freeform gstFFS_LockClip =
{
   .pfnDraw = UI_FFS_LockClip,
};
static struct st_ui_screen__freeform gstFFS_InspectionODL =
{
   .pfnDraw = UI_FFS_InspectionODL,
};
static struct st_ui_screen__freeform gstFFS_DoorOpenODL =
{
   .pfnDraw = UI_FFS_DoorOpenODL,
};

static struct st_ui_screen__freeform gstFFS_ETS_ODL =
{
   .pfnDraw = UI_FFS_ETS_ODL,
};
static struct st_ui_screen__freeform gstFFS_SFP_DL =
{
   .pfnDraw = UI_FFS_SFP_DL,
};
static struct st_ui_screen__freeform gstFFS_GeneralODL =
{
   .pfnDraw = UI_FFS_GeneralODL,
};
static struct st_ui_screen__freeform gstFFS_NTS_ODL =
{
   .pfnDraw = UI_FFS_NTS_ODL,
};
static struct st_ui_screen__freeform gstFFS_EBRAKE_ON_OVERSPEED =
{
   .pfnDraw = UI_FFS_EBRAKE_ON_OVERSPEED,
};
static struct st_ui_screen__freeform gstFFS_DisableConstOverspeed =
{
   .pfnDraw = UI_FFS_DisableConstOverspeed,
};
static struct st_ui_screen__freeform gstFFS_ETS_Offset =
{
   .pfnDraw = UI_FFS_ETS_Offset,
};
static struct st_ui_screen__freeform gstFFS_ETSL_ODL =
{
   .pfnDraw = UI_FFS_ETSL_ODL,
};
static struct st_ui_screen__freeform gstFFS_Enable_ETSL =
{
   .pfnDraw = UI_FFS_Enable_ETSL,
};
static struct st_ui_screen__freeform gstFFS_RatedBufferSpd =
{
   .pfnDraw = UI_FFS_RatedBufferSpd,
};
static struct st_ui_screen__freeform gstFFS_BufferDistance =
{
   .pfnDraw = UI_FFS_BufferDistance,
};
static struct st_ui_screen__freeform gstFFS_ETSL_CameraOffset =
{
   .pfnDraw = UI_FFS_ETSL_CameraOffset,
};
static struct st_ui_screen__freeform gstFFS_SlideDistance =
{
   .pfnDraw = UI_FFS_SlideDistance,
};
static struct st_ui_screen__freeform gstFFS_Construction_ODL =
{
   .pfnDraw = UI_FFS_Construction_ODL,
};
//------------------------------------------------------------------
static struct st_ui_menu_item gstMI_SpeedDev_Threshold =
{
   .psTitle = "Threshold",
   .pstUGS_Next = &gstUGS_SpeedDev_Threshold,
};
static struct st_ui_menu_item gstMI_SpeedDev_Timeout =
{
   .psTitle = "Timeout",
   .pstUGS_Next = &gstUGS_SpeedDev_Timeout,
};
static struct st_ui_menu_item gstMI_SpeedDev_Offset =
{
   .psTitle = "Offset",
   .pstUGS_Next = &gstUGS_SpeedDev_Offset,
};
static struct st_ui_menu_item * gastMenuItems_SpeedDev[] =
{
   &gstMI_SpeedDev_Threshold,
   &gstMI_SpeedDev_Timeout,
   &gstMI_SpeedDev_Offset,
};
static struct st_ui_screen__menu gstMenu_SpeedDev =
{
   .psTitle = "Speed Deviation",
   .pastMenuItems = &gastMenuItems_SpeedDev,
   .ucNumItems = sizeof(gastMenuItems_SpeedDev) / sizeof(gastMenuItems_SpeedDev[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_TractionLoss_Threshold =
{
   .psTitle = "Threshold",
   .pstUGS_Next = &gstUGS_TractionLoss_Threshold,
};
static struct st_ui_menu_item gstMI_TractionLoss_Timeout =
{
   .psTitle = "Timeout",
   .pstUGS_Next = &gstUGS_TractionLoss_Timeout,
};
static struct st_ui_menu_item gstMI_TractionLoss_Offset =
{
   .psTitle = "Offset",
   .pstUGS_Next = &gstUGS_TractionLoss_Offset,
};
static struct st_ui_menu_item * gastMenuItems_TractionLoss[] =
{
   &gstMI_TractionLoss_Threshold,
   &gstMI_TractionLoss_Timeout,
   &gstMI_TractionLoss_Offset,
};
static struct st_ui_screen__menu gstMenu_TractionLoss =
{
   .psTitle = "Traction Loss",
   .pastMenuItems = &gastMenuItems_TractionLoss,
   .ucNumItems = sizeof(gastMenuItems_TractionLoss) / sizeof(gastMenuItems_TractionLoss[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Safety_SpeedDev =
{
   .psTitle = "Speed Deviation",
   .pstUGS_Next = &gstUGS_Menu_SpeedDev,
};
static struct st_ui_menu_item gstMI_Safety_TractionLoss =
{
   .psTitle = "Traction Loss",
   .pstUGS_Next = &gstUGS_Menu_TractionLoss,
};

static struct st_ui_menu_item gstMI_Safety_LockClip =
{
   .psTitle = "Lock Clip",
   .pstUGS_Next = &gstUGS_Menu_LockClip,
};
static struct st_ui_menu_item gstMI_Safety_InspectionODL =
{
   .psTitle = "Inspection ODL",
   .pstUGS_Next = &gstUGS_Menu_InspectionODL,
};
static struct st_ui_menu_item gstMI_Safety_DoorOpenODL =
{
   .psTitle = "Door Open ODL",
   .pstUGS_Next = &gstUGS_Menu_DoorOpenODL,
};
static struct st_ui_menu_item gstMI_Safety_ETS_ODL =
{
   .psTitle = "ETS ODL",
   .pstUGS_Next = &gstUGS_Menu_ETS_ODL,
};
static struct st_ui_menu_item gstMI_Safety_SFP_DL =
{
   .psTitle = "SFP Debounce Limit",
   .pstUGS_Next = &gstUGS_Menu_SFP_DL,
};
static struct st_ui_menu_item gstMI_Safety_GeneralODL =
{
   .psTitle = "General ODL",
   .pstUGS_Next = &gstUGS_Menu_GeneralODL ,
};
static struct st_ui_menu_item gstMI_Safety_NTS_ODL =
{
   .psTitle = "NTS ODL",
   .pstUGS_Next = &gstUGS_Menu_NTS_ODL,
};
static struct st_ui_menu_item gstMI_Safety_EBrakeOnOverspeed =
{
   .psTitle = "EBrake On Overspeed",
   .pstUGS_Next = &gstUGS_Safety_EBrakeOnOverspeed,
};
static struct st_ui_menu_item gstMI_Safety_DisableConstOverspeed =
{
   .psTitle = "Dis. Const. Overspeed",
   .pstUGS_Next = &gstUGS_Safety_DisableConstOverspeed,
};
static struct st_ui_menu_item gstMI_ETS_Offset =
{
   .psTitle = "ETS Offset",
   .pstUGS_Next = &gstUGS_Safety_ETS_Offset,
};
static struct st_ui_menu_item gstMI_ETSL =
{
   .psTitle = "ETSL",
   .pstUGS_Next = &gstUGS_Menu_ETSL,
};
static struct st_ui_menu_item gstMI_Construction_ODL =
{
   .psTitle = "Const. ODL",
   .pstUGS_Next = &gstUGS_Construction_ODL,
};
static struct st_ui_menu_item * gastMenuItems_Safety[] =
{
   &gstMI_Safety_SpeedDev,
   &gstMI_Safety_TractionLoss,

   &gstMI_Safety_LockClip,
   &gstMI_Safety_InspectionODL,  // ODL = Overspeed Debounce Limit
   &gstMI_Safety_DoorOpenODL,
   &gstMI_Safety_ETS_ODL,
   &gstMI_Safety_SFP_DL,
   &gstMI_Safety_GeneralODL,

   &gstMI_Safety_NTS_ODL,
   &gstMI_Construction_ODL,
   &gstMI_Safety_EBrakeOnOverspeed,
   &gstMI_Safety_DisableConstOverspeed,
   &gstMI_ETS_Offset,
   &gstMI_ETSL,
};
static struct st_ui_screen__menu gstMenu_Safety =
{
   .psTitle = "Safety",
   .pastMenuItems = &gastMenuItems_Safety,
   .ucNumItems = sizeof(gastMenuItems_Safety) / sizeof(gastMenuItems_Safety[ 0 ]),
};
//--------------------------------------------
static struct st_ui_menu_item gstMI_Safety_Enable_ETSL =
{
   .psTitle = "Enable ETSL",
   .pstUGS_Next = &gstUGS_Safety_Enable_ETSL,
};
static struct st_ui_menu_item gstMI_Safety_ETSL_ODL =
{
   .psTitle = "ETSL ODL",
   .pstUGS_Next = &gstUGS_Safety_ETSL_ODL,
};
static struct st_ui_menu_item gstMI_Safety_RatedBufferSpd =
{
   .psTitle = "Rated Buffer Speed",
   .pstUGS_Next = &gstUGS_Safety_RatedBufferSpd,
};
static struct st_ui_menu_item gstMI_Safety_BufferDistance =
{
   .psTitle = "Buffer Distance",
   .pstUGS_Next = &gstUGS_Safety_BufferDistance,
};
static struct st_ui_menu_item gstMI_Safety_SlideDistance =
{
   .psTitle = "Slide Distance",
   .pstUGS_Next = &gstUGS_Safety_SlideDistance,
};
static struct st_ui_menu_item gstMI_ETSL_CameraOffset =
{
   .psTitle = "Camera Offset",
   .pstUGS_Next = &gstUGS_ETSL_CameraOffset,
};
static struct st_ui_menu_item * gastMenuItems_ETSL[] =
{
      &gstMI_Safety_Enable_ETSL,
      &gstMI_Safety_ETSL_ODL,
      &gstMI_Safety_RatedBufferSpd,
      &gstMI_Safety_BufferDistance,
      &gstMI_Safety_SlideDistance,
      &gstMI_ETSL_CameraOffset,
};
static struct st_ui_screen__menu gstMenu_ETSL =
{
   .psTitle = "ETSL",
   .pastMenuItems = &gastMenuItems_ETSL,
   .ucNumItems = sizeof(gastMenuItems_ETSL) / sizeof(gastMenuItems_ETSL[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
//-----------------------------------------------------
struct st_ui_generic_screen gstUGS_SpeedDev_Threshold =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SpeedDev_Threshold,
};
struct st_ui_generic_screen gstUGS_SpeedDev_Timeout =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SpeedDev_Timeout,
};
struct st_ui_generic_screen gstUGS_SpeedDev_Offset =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SpeedDev_Offset,
};
//-----------------------------------------------------
struct st_ui_generic_screen gstUGS_TractionLoss_Threshold =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_TractionLoss_Threshold,
};
struct st_ui_generic_screen gstUGS_TractionLoss_Timeout =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_TractionLoss_Timeout,
};
struct st_ui_generic_screen gstUGS_TractionLoss_Offset =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_TractionLoss_Offset,
};

struct st_ui_generic_screen gstUGS_Menu_LockClip =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_LockClip,
};
struct st_ui_generic_screen gstUGS_Menu_InspectionODL =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_InspectionODL,
};
struct st_ui_generic_screen gstUGS_Menu_DoorOpenODL =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DoorOpenODL,
};
struct st_ui_generic_screen gstUGS_Menu_ETS_ODL =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ETS_ODL,
};
struct st_ui_generic_screen gstUGS_Menu_SFP_DL =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SFP_DL,
};
struct st_ui_generic_screen gstUGS_Menu_GeneralODL =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_GeneralODL,
};
struct st_ui_generic_screen gstUGS_Menu_NTS_ODL =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_NTS_ODL,
};
struct st_ui_generic_screen gstUGS_Safety_EBrakeOnOverspeed =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_EBRAKE_ON_OVERSPEED,
};
struct st_ui_generic_screen gstUGS_Safety_DisableConstOverspeed =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DisableConstOverspeed,
};
struct st_ui_generic_screen gstUGS_Safety_ETS_Offset =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ETS_Offset,
};
struct st_ui_generic_screen gstUGS_Safety_ETSL_ODL =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ETSL_ODL,
};
struct st_ui_generic_screen gstUGS_Safety_Enable_ETSL =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Enable_ETSL,
};
struct st_ui_generic_screen gstUGS_Safety_RatedBufferSpd =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_RatedBufferSpd,
};
struct st_ui_generic_screen gstUGS_Safety_BufferDistance =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_BufferDistance,
};
struct st_ui_generic_screen gstUGS_ETSL_CameraOffset =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ETSL_CameraOffset,
};
struct st_ui_generic_screen gstUGS_Safety_SlideDistance =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SlideDistance,
};
struct st_ui_generic_screen gstUGS_Construction_ODL =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Construction_ODL,
};
//---------------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_SpeedDev =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_SpeedDev,
};
struct st_ui_generic_screen gstUGS_Menu_TractionLoss =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_TractionLoss,
};
struct st_ui_generic_screen gstUGS_Menu_ETSL =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_ETSL,
};
struct st_ui_generic_screen gstUGS_Menu_Safety =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Safety,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SpeedDev_Threshold( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM16__SpeedDev_Threshold;
   stParamEdit.uwParamIndex_End = enPARAM16__SpeedDev_Threshold;
   stParamEdit.psTitle = "Threshold";
   stParamEdit.psUnit = " fpm";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SpeedDev_Timeout( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM16__SpeedDev_Timeout_10ms;
   stParamEdit.uwParamIndex_End = enPARAM16__SpeedDev_Timeout_10ms;
   stParamEdit.psTitle = "Timeout";
   stParamEdit.psUnit = "0 msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SpeedDev_Offset( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM16__SpeedDev_OffsetPercent;
   stParamEdit.uwParamIndex_End = enPARAM16__SpeedDev_OffsetPercent;
   stParamEdit.psTitle = "Offset";
   stParamEdit.psUnit = " %";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_TractionLoss_Threshold( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM16__TractionLoss_Threshold;
   stParamEdit.uwParamIndex_End = enPARAM16__TractionLoss_Threshold;
   stParamEdit.psTitle = "Threshold";
   stParamEdit.psUnit = " fpm";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_TractionLoss_Timeout( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM16__TractionLoss_Timeout_10ms;
   stParamEdit.uwParamIndex_End = enPARAM16__TractionLoss_Timeout_10ms;
   stParamEdit.psTitle = "Timeout";
   stParamEdit.psUnit = "0 msec";
   stParamEdit.ucFieldSize_Value = 3;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_TractionLoss_Offset( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM16__TractionLoss_OffsetPercent;
   stParamEdit.uwParamIndex_End = enPARAM16__TractionLoss_OffsetPercent;
   stParamEdit.psTitle = "Offset";
   stParamEdit.psUnit = " %";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_LockClip( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM16__LockClipTime_10ms;
   stParamEdit.uwParamIndex_End = enPARAM16__LockClipTime_10ms;
   stParamEdit.psTitle = "Lock Clip";
   stParamEdit.psUnit = "0 msec";
   stParamEdit.ucFieldSize_Value = 3;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_InspectionODL( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM8__InspectionOverspeedDebounceLimit;
   stParamEdit.uwParamIndex_End = enPARAM8__InspectionOverspeedDebounceLimit;
   stParamEdit.psTitle = "Inspection ODL";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_DoorOpenODL( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM8__DoorOpenOverspeedDebounceLimit;
   stParamEdit.uwParamIndex_End = enPARAM8__DoorOpenOverspeedDebounceLimit;
   stParamEdit.psTitle = "Door Open ODL";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_ETS_ODL( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM8__ETSOverspeedDebounceLimit;
   stParamEdit.uwParamIndex_End = enPARAM8__ETSOverspeedDebounceLimit;
   stParamEdit.psTitle = "ETS ODL";
   stParamEdit.psUnit = " ";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SFP_DL( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM8__SFP_DebounceLimit;
   stParamEdit.uwParamIndex_End = enPARAM8__SFP_DebounceLimit;
   stParamEdit.psTitle = "SFP DL";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_GeneralODL( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM8__GeneralOverspeedDebounceLimit;
   stParamEdit.uwParamIndex_End = enPARAM8__GeneralOverspeedDebounceLimit;
   stParamEdit.psTitle = "General ODL";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_NTS_ODL( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM8__DebounceNTS;
   stParamEdit.uwParamIndex_End = enPARAM8__DebounceNTS;
   stParamEdit.psTitle = "NTS ODL";
   stParamEdit.psUnit = " ";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_EBRAKE_ON_OVERSPEED( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__EBrakeOnOverspeed;
   stParamEdit.uwParamIndex_End = enPARAM1__EBrakeOnOverspeed;
   stParamEdit.psTitle = "EBrake On Overspeed";
   stParamEdit.psUnit = " ";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_DisableConstOverspeed( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__DisableConstructionOverspeed;
   stParamEdit.uwParamIndex_End = enPARAM1__DisableConstructionOverspeed;
   stParamEdit.psTitle = "Dis. Const. Overspeed";
   stParamEdit.psUnit = " ";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_ETS_Offset( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__ETS_OffsetFromNTS_5mm;
   stParamEdit.uwParamIndex_End = enPARAM8__ETS_OffsetFromNTS_5mm;
   stParamEdit.psTitle = "ETS Offset";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Enable_ETSL( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM1__Enable_ETSL;
   stParamEdit.uwParamIndex_End = enPARAM1__Enable_ETSL;
   stParamEdit.psTitle = "Enable ETSL";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_ETSL_ODL( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM8__ETSL_ODL_10ms;
   stParamEdit.uwParamIndex_End = enPARAM8__ETSL_ODL_10ms;
   stParamEdit.psTitle = "ETSL ODL";
   stParamEdit.psUnit = "0 msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_RatedBufferSpd( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM8__RatedBufferSpd_10fpm;
   stParamEdit.uwParamIndex_End = enPARAM8__RatedBufferSpd_10fpm;
   stParamEdit.psTitle = "Rated Buffer Speed";
   stParamEdit.psUnit = "0 fpm";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_BufferDistance( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM16__BufferDistance_05mm;
   stParamEdit.uwParamIndex_End = enPARAM16__BufferDistance_05mm;
   stParamEdit.psTitle = "Buffer Distance";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_SlideDistance( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM16__Acceptance_SlideDistance;
   stParamEdit.uwParamIndex_End = enPARAM16__Acceptance_SlideDistance;
   stParamEdit.psTitle = "Slide Distance";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_ETSL_CameraOffset( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_None;
   stParamEdit.uwParamIndex_Start = enPARAM16__ETSL_CameraOffset_05mm;
   stParamEdit.uwParamIndex_End = enPARAM16__ETSL_CameraOffset_05mm;
   stParamEdit.psTitle = "Camera Offset";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Construction_ODL( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__ConstructionOverspeedDebounce_10ms;
   stParamEdit.uwParamIndex_End = enPARAM8__ConstructionOverspeedDebounce_10ms;
   stParamEdit.psTitle = "Const. Overspeed Debounce";
   stParamEdit.psUnit = "0 msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
