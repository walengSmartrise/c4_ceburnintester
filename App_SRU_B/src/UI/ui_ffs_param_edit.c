/******************************************************************************
 *
 * @file     ui_ffs_param_edit.c
 * @brief    Debug parameter edit UI pages
 * @version  V1.00
 * @date     13, Feb 2018
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "math.h"
#include "mod.h"
#include <stdint.h>
#include "sys.h"
#include "param.h"
#include "config_System.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_DecimalParams( void );
static void UI_FFS_HexParams( void );

static void SavingParamsScreen( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_ui_screen__freeform gstFFS_DecimalParams =
{
   .pfnDraw = UI_FFS_DecimalParams,
};
static struct st_ui_screen__freeform gstFFS_HexParams =
{
   .pfnDraw = UI_FFS_HexParams,
};
//---------------------------------------------------------------------
static struct st_ui_menu_item gstMI_HexParameters =
{
   .psTitle = "Hex Format",
   .pstUGS_Next = &gstUGS_HexParams,
};
static struct st_ui_menu_item gstMI_DecimalParameters =
{
   .psTitle = "Decimal Format",
   .pstUGS_Next = &gstUGS_DecimalParams,
};
static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_HexParameters,
   &gstMI_DecimalParameters,
};
static struct st_ui_screen__menu gstMenu =
{
   .psTitle = "Parameter Edit",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};
//---------------------------------------------------------------------

static uint8_t gucFieldIndex;

static int16_t gwCurrentParamIndex;

static int8_t gcCurrentBitTypeIndex = 1; /* Valid values are 1, 8, 16, 24, 32 */

static uint32_t guiCurrentParam_EditedValue;
static uint32_t guiCurrentParam_SavedValue;

static uint32_t guiMaxValue;

// Number of digits in the editable field of the parameter value. For example,
// when editing a 16-bit parameter in hex format, there are 4 value digits.
static uint8_t gucNumValueDigits;

// Number of bytes used to store the parameter value. This value is used,
// among other things, to determine the max value the parameter can hold.
static uint8_t gucNumBytes;

static uint8_t gucCursorColumn;

static int8_t gbEditsMade;

static int8_t gbValidParamSelected;

static uint8_t gucEditFormat;
static uint8_t gucSetupFormat;//enum en_setup_format

static uint8_t ucbSaveFlag = 0;

static enum en_param_block_types genBlockType;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_DecimalParams =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DecimalParams,
};
struct st_ui_generic_screen gstUGS_HexParams =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_HexParams,
};

//---------------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_ParamEdit =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*
 * Description: This function uses the BitType and enParamIndex to find the block
 * 				index
 * Parameters: iBitType is the either 1,8,16,24 or 36 bit param type.
 * 				wParamIndex is the paramIndex of that param type. no long use blocks to check
 * Return: Returns the Block Index
 */
int8_t getBlockIndex(int8_t iBitType, int16_t wParamIndex)
{
	int8_t iBlockIndex = 0; 	// assum invalid
	switch(iBitType)
	{
		case 1:
			iBlockIndex = STARTING_BLOCK_BIT + wParamIndex/enPARAM_NUM_PER_TYPE__BIT;
			break;
		case 8:
			iBlockIndex = STARTING_BLOCK_U8 + wParamIndex/enPARAM_NUM_PER_TYPE__UINT8;
			break;
		case 16:
			iBlockIndex = STARTING_BLOCK_U16 + wParamIndex/enPARAM_NUM_PER_TYPE__UINT16;
			break;
		case 24:
			iBlockIndex = STARTING_BLOCK_U24 + wParamIndex/enPARAM_NUM_PER_TYPE__UINT24;
			break;
		case 32:
			iBlockIndex = STARTING_BLOCK_U32 + wParamIndex/enPARAM_NUM_PER_TYPE__UINT32;
			break;
	}
	return iBlockIndex;
}

/*
 * Description: This function uses the BitType and enParamIndex to find the block
 * 				index
 * Parameters: iBitType is the either 1,8,16,24 or 36 bit param type.
 * 				wParamIndex is the paramIndex of that param type. no long use blocks to check
 * Return: Returns the Block Index
 */
int16_t getParamIndex(int8_t iBitType, int16_t wParamIndex)
{
	int16_t iParamIndex = 0; 	// assum invalid
	switch(iBitType)
	{
		case 1:
			iParamIndex = wParamIndex%enPARAM_NUM_PER_TYPE__BIT;
			break;
		case 8:
			iParamIndex = wParamIndex%enPARAM_NUM_PER_TYPE__UINT8;
			break;
		case 16:
			iParamIndex = wParamIndex%enPARAM_NUM_PER_TYPE__UINT16;
			break;
		case 24:
			iParamIndex = wParamIndex%enPARAM_NUM_PER_TYPE__UINT24;
			break;
		case 32:
			iParamIndex = wParamIndex%enPARAM_NUM_PER_TYPE__UINT32;
			break;
	}
	return iParamIndex;
}


/*
 * Description: This function checks to see if the ParamIndex is within the
 * 				bounds of its BitType. For Ex:if iBitType = 1 bit param then it needs
 * 				to be less than 2320 to be valid.
 * Parameters: iBitType is the either 1,8,16,24 or 36 bit param type.
 * 				wParamIndex is the paramIndex of that param type. no long use blocks to check
 * Return: 1 if param is valid, else return a 0.
 */
int8_t IsParamValid(int8_t iBitType, int16_t wParamIndex)
{
	int8_t isValid = 0; 	// assum invalid
	switch(iBitType)
		{
		case 1:
			if(wParamIndex < NUM_1BIT_PARAMS)isValid = 1;
			break;
		case 8:
			if(wParamIndex < NUM_8BIT_PARAMS)isValid = 1;
			break;
		case 16:
			if(wParamIndex < NUM_16BIT_PARAMS)isValid = 1;
			break;
		case 24:
			if(wParamIndex < NUM_24BIT_PARAMS)isValid = 1;
			break;
		case 32:
			if(wParamIndex < NUM_32BIT_PARAMS)isValid = 1;
			break;
		}
	return isValid;
}

/*
 * Description: This function checks which BlockType we are in. 1,8,16,24, or 32
 * Parameters: None
 * Return: enumeration of Block Type base on currentBitType on Screen
 */
enum en_param_block_types getBlockType()
{
	switch(gcCurrentBitTypeIndex)
	{
	case 1:
		return enPARAM_BLOCK_TYPE__BIT;
	case 8:
		return enPARAM_BLOCK_TYPE__UINT8;
	case 16:
		return enPARAM_BLOCK_TYPE__UINT16;
	case 24:
		return enPARAM_BLOCK_TYPE__UINT24;
	case 32:
		return enPARAM_BLOCK_TYPE__UINT32;
	default :
		return enPARAM_BLOCK_TYPE__UNKNOWN;
	}
}
int Get_MaxParamIndex()
{
	int iNumParams;
	switch( genBlockType )
	      {
				 case enPARAM_BLOCK_TYPE__UINT32: iNumParams = NUM_32BIT_PARAMS; break;
		         case enPARAM_BLOCK_TYPE__BIT: iNumParams = NUM_1BIT_PARAMS; break;
		         case enPARAM_BLOCK_TYPE__UINT8: iNumParams = NUM_8BIT_PARAMS; break;
		         case enPARAM_BLOCK_TYPE__UINT16: iNumParams = NUM_16BIT_PARAMS; break;
		         case enPARAM_BLOCK_TYPE__UINT24: iNumParams = NUM_24BIT_PARAMS; break;
		         default: iNumParams = NUM_32BIT_PARAMS; break;
	      }
	return iNumParams;
}

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void Print_Parameter_Name(void)
{
   uint16_t uwIndex;
   LCD_Char_GotoXY(0,1);
   uwIndex = gwCurrentParamIndex;
   switch(genBlockType)
   {
      case enPARAM_BLOCK_TYPE__BIT:
         //ucBlockStart = STARTING_BLOCK_BIT;
         //uwIndex = gwCurrentParamIndex + enPARAM_NUM_PER_TYPE__BIT*(gcCurrentBitTypeIndex - ucBlockStart);

         if(gwCurrentParamIndex >= NUM_1BIT_PARAMS)
         {
            uwIndex = NUM_1BIT_PARAMS;
         }
         Param_PrintParamString_1Bit(uwIndex);
         break;

      case enPARAM_BLOCK_TYPE__UINT8:
//         ucBlockStart = STARTING_BLOCK_U8;
//         uwIndex = gwCurrentParamIndex + enPARAM_NUM_PER_TYPE__UINT8*(gcCurrentBitTypeIndex - ucBlockStart);
         if(gwCurrentParamIndex >= NUM_8BIT_PARAMS)
         {
            uwIndex = NUM_8BIT_PARAMS;
         }
         Param_PrintParamString_8Bit(uwIndex);
         break;

      case enPARAM_BLOCK_TYPE__UINT16:
//         ucBlockStart = STARTING_BLOCK_U16;
//         uwIndex = gwCurrentParamIndex + enPARAM_NUM_PER_TYPE__UINT16*(gcCurrentBitTypeIndex - ucBlockStart);
         if(gwCurrentParamIndex >= NUM_16BIT_PARAMS)
         {
            uwIndex = NUM_16BIT_PARAMS;
         }
         Param_PrintParamString_16Bit(uwIndex);
         break;

      case enPARAM_BLOCK_TYPE__UINT24:
//         ucBlockStart = STARTING_BLOCK_U24;
//         uwIndex = gwCurrentParamIndex + enPARAM_NUM_PER_TYPE__UINT24*(gcCurrentBitTypeIndex - ucBlockStart);
         if(gwCurrentParamIndex >= NUM_24BIT_PARAMS)
         {
            uwIndex = NUM_24BIT_PARAMS;
         }
         Param_PrintParamString_24Bit(uwIndex);
         break;

      case enPARAM_BLOCK_TYPE__UINT32:
//         ucBlockStart = STARTING_BLOCK_U32;
//         uwIndex = gwCurrentParamIndex + enPARAM_NUM_PER_TYPE__UINT32*(gcCurrentBitTypeIndex - ucBlockStart);
         if(gwCurrentParamIndex >= NUM_32BIT_PARAMS)
         {
            uwIndex = NUM_32BIT_PARAMS;
         }
         Param_PrintParamString_32Bit(uwIndex);
         break;

      default:
         break;
   }
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static uint32_t GetParamInfo( int8_t cBitTypeIndex, int16_t wParamIndex )
{
   uint8_t bSelectionChanged = 0;
   static int16_t wParamIndex_Old = -1;  // force to initially appear changed
   static int8_t cBlockIndex_Old = -1;  // force to initially appear changed

   //------------------------------------------------------------------
   // 9/14 hard faulting occurs when gwCurrentParamIndex exceeds limit for current param type
   if(( gcCurrentBitTypeIndex != 1 )
   && ( gcCurrentBitTypeIndex != 8 )
   && ( gcCurrentBitTypeIndex != 16 )
   && ( gcCurrentBitTypeIndex != 24 )
   && ( gcCurrentBitTypeIndex != 32 ) )
   {
      gcCurrentBitTypeIndex = 1;
      bSelectionChanged = 1;
   }

   if ( gwCurrentParamIndex < 0 )
   {
      gwCurrentParamIndex = 0;
   }
   else if ( gwCurrentParamIndex >= Get_MaxParamIndex()-1)
   {
      gwCurrentParamIndex = Get_MaxParamIndex()-1;
   }

   //------------------------------------------------------------------
   if ( cBlockIndex_Old != cBitTypeIndex )
   {
      cBlockIndex_Old = cBitTypeIndex;
      bSelectionChanged = 1;
   }

   if ( wParamIndex_Old != wParamIndex )
   {
      wParamIndex_Old = wParamIndex;
      bSelectionChanged = 1;
   }

   if ( bSelectionChanged )
   {
      gbEditsMade = 0;

      gbValidParamSelected = IsParamValid( cBitTypeIndex, wParamIndex );
      genBlockType = getBlockType();
      if ( gbValidParamSelected )
      {
         //genBlockType = Sys_Param_BlockType( cBlockIndex );
//    	  genBlockType = getBlockType();
         switch ( genBlockType )
         {
            case enPARAM_BLOCK_TYPE__UINT32:
               gucNumBytes = 4;
               break;


            case enPARAM_BLOCK_TYPE__UINT16:
               gucNumBytes = 2;
               break;

            case enPARAM_BLOCK_TYPE__UINT24:
               gucNumBytes = 3;
               break;

            default:
               gucNumBytes = 1;
               break;
         }

         switch ( gucNumBytes )
         {
            case 2: guiMaxValue = 0xffff; break;
            case 3: guiMaxValue = 0xffffff; break;
            case 4: guiMaxValue = 0xffffffff; break;
            default: guiMaxValue = 0xff;
            break;
         }
      }
   }

   struct st_param_and_value stParamAndValue;

   stParamAndValue.ucBlockIndex = getBlockIndex(cBitTypeIndex,wParamIndex);
   stParamAndValue.uwParamIndex = getParamIndex(cBitTypeIndex,wParamIndex);
   guiCurrentParam_SavedValue = Param_ReadValue( &stParamAndValue );

   if ( !gbEditsMade )
   {
      guiCurrentParam_EditedValue = guiCurrentParam_SavedValue;
   }

   return 0;
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void SaveParameter( void )
{
   struct st_param_and_value stParamAndValue;

   stParamAndValue.ucBlockIndex = getBlockIndex(gcCurrentBitTypeIndex,gwCurrentParamIndex);
   stParamAndValue.uwParamIndex = getParamIndex(gcCurrentBitTypeIndex,gwCurrentParamIndex);
   stParamAndValue.uiValue = guiCurrentParam_EditedValue;

   SaveAndVerifyParameter( &stParamAndValue );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Save( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t bSaveInProgress;

   if (    !gbEditsMade
        || (guiCurrentParam_EditedValue == guiCurrentParam_SavedValue)
      )
   {
      bSaveInProgress = 0;
      ucbSaveFlag = 0;

      gbEditsMade = 0;
      enReturnValue = enACTION__EXIT_TO_LEFT;
   }
   else
   {
      gucCursorColumn = 19;

      if ( enFieldAction == enACTION__EDIT )
      {
         if ( enKeypress == enKEYPRESS_ENTER )
         {
            bSaveInProgress = 1;
            ucbSaveFlag = 1;

         }
         else if ( enKeypress == enKEYPRESS_LEFT )
         {
            bSaveInProgress = 0;
            ucbSaveFlag = 0;

            enReturnValue = enACTION__EXIT_TO_LEFT;
         }
      }

      // Keep transmitting save request until we receive confimation that new
      // value was saved.
      if ( bSaveInProgress )
      {
         SaveParameter();
      }
   }

   return enReturnValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_BinaryValue( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   enum en_field_action enReturnValue = enACTION__NONE;

   int32_t uiCurrentParam_EditedValue = guiCurrentParam_EditedValue;

   gucCursorColumn = 10;

   if ( enFieldAction == enACTION__EDIT )
   {
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
         case enKEYPRESS_DOWN:
            if ( uiCurrentParam_EditedValue == 0 )
            {
               uiCurrentParam_EditedValue = 1;
            }
            else
            {
               uiCurrentParam_EditedValue = 0;
            }

            gbEditsMade = 1;
            break;

         case enKEYPRESS_LEFT:
            enReturnValue = enACTION__EXIT_TO_LEFT;
            break;

         case enKEYPRESS_RIGHT:
            enReturnValue = enACTION__EXIT_TO_RIGHT;
            break;

         default:
            break;
      }
   }

   guiCurrentParam_EditedValue = uiCurrentParam_EditedValue;

   return enReturnValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_DecOrHexValue( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex;

   int64_t llCurrentParam_EditedValue = guiCurrentParam_EditedValue;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = gucNumValueDigits - 1;
      }
   }
   else  // enACTION__EDIT
   {
      //------------------------------------------------------------------
      if ( gucNumValueDigits == 0 )
      {
         llCurrentParam_EditedValue = 0;

         enReturnValue = enACTION__EXIT_TO_LEFT;
      }
      else
      {
         int32_t uiDigitWeight = (gucNumValueDigits - 1) - ucCursorIndex;

         if ( gucEditFormat == enEDIT_FORMAT__DEC )
         {
            uiDigitWeight = pow( 10, uiDigitWeight );  // 10 to the power of uiDigitWeight

            //------------------------------------------------------------------
            gucCursorColumn = 9 + ucCursorIndex;
         }
         else
         {
            uiDigitWeight = pow( 16, uiDigitWeight );  // 16 to the power of uiDigitWeight

            //------------------------------------------------------------------
            gucCursorColumn = 10 + ucCursorIndex;
         }

         //------------------------------------------------------------------
         switch ( enKeypress )
         {
            case enKEYPRESS_UP:
               llCurrentParam_EditedValue += uiDigitWeight;

               gbEditsMade = 1;
               break;

            case enKEYPRESS_DOWN:
               llCurrentParam_EditedValue -= uiDigitWeight;

               gbEditsMade = 1;
               break;

            case enKEYPRESS_LEFT:
               if ( ucCursorIndex )
               {
                  --ucCursorIndex;
               }
               else
               {
                  enReturnValue = enACTION__EXIT_TO_LEFT;
               }
               break;

            case enKEYPRESS_RIGHT:
               if ( ucCursorIndex < (gucNumValueDigits - 1) )
               {
                  ++ucCursorIndex;
               }
               else
               {
                  enReturnValue = enACTION__EXIT_TO_RIGHT;
               }
               break;

            default:
               break;
         }

         if ( llCurrentParam_EditedValue < 0 )
         {
            llCurrentParam_EditedValue = 0;
         }
         else
         {
            if ( llCurrentParam_EditedValue > guiMaxValue )
            {
               llCurrentParam_EditedValue = guiMaxValue;
            }
         }
      }
   }

   guiCurrentParam_EditedValue = (uint32_t) llCurrentParam_EditedValue;

   return enReturnValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Value( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   enum en_field_action enReturnValue = enACTION__EXIT_TO_LEFT;

   if ( gbValidParamSelected )
   {
      switch ( genBlockType )
      {
         case enPARAM_BLOCK_TYPE__UINT8:
         case enPARAM_BLOCK_TYPE__UINT16:
         case enPARAM_BLOCK_TYPE__UINT24:
         case enPARAM_BLOCK_TYPE__UINT32:
            enReturnValue = Edit_DecOrHexValue( enKeypress, enFieldAction );
            break;

         case enPARAM_BLOCK_TYPE__BIT:
            enReturnValue = Edit_BinaryValue( enKeypress, enFieldAction );
            break;

         default:  // should never get here
            break;
      }
   }

   return enReturnValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Index( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         //ucCursorIndex = 2;
    	  ucCursorIndex = 3;
      }
   }
   else  // enACTION__EDIT
   {
      gucCursorColumn = 3 + ucCursorIndex;

      //------------------------------------------------------------------
      int32_t uiDigitWeight = 3 - ucCursorIndex;

      uiDigitWeight = pow( 10, uiDigitWeight );  // 10 to the power of uiDigitWeight

      //------------------------------------------------------------------
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            gwCurrentParamIndex += uiDigitWeight;
            break;

         case enKEYPRESS_DOWN:
            gwCurrentParamIndex -= uiDigitWeight;
            break;

         case enKEYPRESS_LEFT:
            if ( ucCursorIndex )
            {
               --ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_LEFT;
            }
            break;

         case enKEYPRESS_RIGHT:
            if ( ucCursorIndex < 3 )
            {
               ++ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;

         default:
            break;
      }
   }


   //------------------------------------------------------------------
   if ( gwCurrentParamIndex < 0 )
   {
      gwCurrentParamIndex = 0;
   }
   else if ( gwCurrentParamIndex >= Get_MaxParamIndex()-1)
   {
      gwCurrentParamIndex = Get_MaxParamIndex()-1;
   }

   return enReturnValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_BitType( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = 1;
      }
   }
   else  // enACTION__EDIT
   {
      gucCursorColumn = ucCursorIndex;

      //------------------------------------------------------------------
      //int8_t cDigitWeight = (ucCursorIndex == 0) ? 10 : 1;
      int8_t cDigitWeight = 8;

      //------------------------------------------------------------------
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
        	 // if current BitType =1 add only 7 otherwise add 8
        	 gcCurrentBitTypeIndex += ((gcCurrentBitTypeIndex == 1) ? cDigitWeight-1: cDigitWeight);
        	 //if(genBlockType <enPARAM_BLOCK_TYPE__UINT32) genBlockType++;
            break;

         case enKEYPRESS_DOWN:
            gcCurrentBitTypeIndex -= cDigitWeight;
            //if(genBlockType > enPARAM_BLOCK_TYPE__BIT)genBlockType--;
            break;

         case enKEYPRESS_LEFT:
            if ( ucCursorIndex )
            {
               --ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_LEFT;
            }
            break;

         case enKEYPRESS_RIGHT:
            if ( ucCursorIndex < 1 )
            {
               ++ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;

         default:
            break;
      }
   }

   //------------------------------------------------------------------
   if ( gcCurrentBitTypeIndex < 1 )
   {
      gcCurrentBitTypeIndex = 1;
   }
   else if ( gcCurrentBitTypeIndex > 32 )
   {
      gcCurrentBitTypeIndex = 32;
   }

   return enReturnValue;
}

/*-----------------------------------------------------------------------------
01234567890123456789
--------------------
As dec = 4294967296
                Save
99-999 = x12345678 |
.. ...    ........ .
--------------------
As dec = 16777216
As dist= 9999'99.99"
                Save
99-999 = x123456   |
.. ...    ......   .
--------------------
As dec = 65535
As dist= 999'99.99"
                Save
99-999 = x1234     |
.. ...    ....     .
--------------------
As dec = 255
As dist= 99'99.99"
                Save
99-999 = x12       |
.. ...    ..       .


 -----------------------------------------------------------------------------*/
static void GenericEdit( void )
{
   //------------------------------------------------------------------
   GetParamInfo( gcCurrentBitTypeIndex, gwCurrentParamIndex );

   switch( gucNumBytes )
   {
      case 2:
         if ( gucEditFormat == enEDIT_FORMAT__DEC )
         {
            gucNumValueDigits = 5;  // 16-bit max value = 65535
         }
         else
         {
            gucNumValueDigits = 4;  // 16-bit max value = FFFF
         }
         break;

      case 3:
         if ( gucEditFormat == enEDIT_FORMAT__DEC )
         {
            gucNumValueDigits = 8;  // 24-bit max value = 16777215
         }
         else
         {
            gucNumValueDigits = 6;  // 24-bit max value = FFFFFF
         }
         break;

      case 4:
         if ( gucEditFormat == enEDIT_FORMAT__DEC )
         {
            gucNumValueDigits = 10;  // 32-bit max value = 4294967295
         }
         else
         {
            gucNumValueDigits = 8;  // 32-bit max value = FFFFFFFF
         }
         break;

      default:  // assume 1
         if ( gucEditFormat == enEDIT_FORMAT__DEC )
         {
            gucNumValueDigits = 3;  // 8-bit max value = 255
         }
         else
         {
            gucNumValueDigits = 2;  // 8-bit max value = FF
         }
         break;
   }

   //------------------------------------------------------------------
   if ( gucFieldIndex == enFIELD__NONE )
   {
      guiCurrentParam_EditedValue = guiCurrentParam_SavedValue;

      Edit_BitType( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT );

      gucFieldIndex = enFIELD__BLOCK;
   }
   else
   {
      enum en_field_action enFieldAction;

      enum en_keypresses enKeypress = Button_GetKeypress();

      //------------------------------------------------------------------
      switch ( gucFieldIndex )
      {
         //------------------------------------------------------------------
         case enFIELD__BLOCK:

            enFieldAction = Edit_BitType( enKeypress, enACTION__EDIT );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               PrevScreen();

               gucFieldIndex = enFIELD__NONE;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               Edit_Index( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT );

               gucFieldIndex = enFIELD__INDEX;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__INDEX:

            enFieldAction = Edit_Index( enKeypress, enACTION__EDIT );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               Edit_BitType( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT );

               gucFieldIndex = enFIELD__BLOCK;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT );

               gucFieldIndex = enFIELD__VALUE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__VALUE:

            enFieldAction = Edit_Value( enKeypress, enACTION__EDIT );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               Edit_Index( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT );

               gucFieldIndex = enFIELD__INDEX;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Save( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT );

               gucFieldIndex = enFIELD__SAVE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__SAVE:

            enFieldAction = Edit_Save( enKeypress, enACTION__EDIT );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT );

               gucFieldIndex = enFIELD__VALUE;
            }
            break;
      }
   }

   LCD_Char_Clear();

   LCD_Char_GotoXY( 0, 0 );

   //------------------------------------------------------------------
   const char *ps;

   if ( !gbValidParamSelected )
   {
      ps = "Invalid Selection";
   }
   else if ( genBlockType == enPARAM_BLOCK_TYPE__BIT )
   {
      ps = "Edit as Binary";
   }
   else
   {
      if ( gucEditFormat == enEDIT_FORMAT__DEC )
      {
         ps = "Edit as Decimal";
      }
      else
      {
         ps = "Edit as Hex";
      }
   }

   LCD_Char_WriteStringUpper( ps );

   Print_Parameter_Name();
   //------------------------------------------------------------------
   LCD_Char_GotoXY( gucCursorColumn, 3 );
   LCD_Char_WriteChar( '*' );

   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteInteger_MinLength( gcCurrentBitTypeIndex, 2 );

   LCD_Char_WriteChar( '-' );

   LCD_Char_WriteInteger_MinLength( gwCurrentParamIndex, 4 );

   LCD_Char_WriteString( "= " );

   if ( !gbValidParamSelected )
   {
      LCD_Char_WriteString( "(Invalid)" );
   }
   else
   {
      if ( genBlockType == enPARAM_BLOCK_TYPE__BIT )
      {
         if ( guiCurrentParam_EditedValue == 0 )
         {
            LCD_Char_WriteString( "Off" );
         }
         else
         {
            LCD_Char_WriteString( "On" );
         }
      }
      else
      {
         if ( gucEditFormat == enEDIT_FORMAT__DEC )
         {
            LCD_Char_WriteInteger_MinLength( guiCurrentParam_EditedValue, gucNumValueDigits );
         }
         else if ( gucEditFormat == enEDIT_FORMAT__HEX )
         {
            LCD_Char_WriteChar( 'x' );

            LCD_Char_WriteHex_ExactLength( guiCurrentParam_EditedValue, gucNumValueDigits );
         }
      }
   }

   if ( guiCurrentParam_SavedValue != guiCurrentParam_EditedValue )
   {
      LCD_Char_GotoXY( 16, 1 );
      LCD_Char_WriteString( "Save" );

      LCD_Char_GotoXY( 19, 2 );
      LCD_Char_WriteChar( '|' );
   }
   SavingParamsScreen();

}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_DecimalParams( void )
{
   gucEditFormat = enEDIT_FORMAT__DEC;
   gucSetupFormat = enSETUP_FORMAT__GENERIC;
   GenericEdit();
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_HexParams( void )
{
   gucEditFormat = enEDIT_FORMAT__HEX;
   gucSetupFormat = enSETUP_FORMAT__GENERIC;
   GenericEdit();
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void SavingParamsScreen( void )
{
   static uint8_t ucCounter = 0;
   if( ucbSaveFlag )
   {
      LCD_Char_Clear();

      LCD_Char_GotoXY( 2, 0 );

      LCD_Char_WriteString( "SAVING " );
      LCD_Char_WriteString( "PARAMETER");

      LCD_Char_GotoXY( 0, 2 );

      for( uint8_t i = 0; i < ucCounter; i++)
      {
         LCD_Char_WriteChar( '*' );
      }
      if( ucCounter < 20 )
      {
         ucCounter++;
      }
   }
   else
   {
      ucCounter = 0;
   }
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
