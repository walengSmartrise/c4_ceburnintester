/******************************************************************************
 *
 * @file     ui_ffs_about.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "sru.h"
#include "mod.h"
#include <stdint.h>
#include <string.h>
#include "config.h"
#include "version.h"
#ifdef DEBUG
static const char * gpsJob = VF_JOB_NAME;
#endif
static const char * gpsCopyright = COPYRIGHT_NOTICE;

// Should be in the default all header file.
#define NUM_HEADER_BYTES   ((uint32_t)6)
#define FLASH_BASE_ADDRESS ( ((uint32_t)0x9000) + NUM_HEADER_BYTES )
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_About( void );

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_ui_screen__freeform gstFreeFormScreen_About =
{
   .pfnDraw = UI_FFS_About,
};

static const char * gpasBoards[SRU_NUM_DEPLOYMENT_TYPES] =
{
         "Invalid0",
         "COP SRU",

         "CT SRU",
         "Invalid3",
         "MR SRU",
         "HIO SRU",
         "CN SRU",
         "RIS SRU",
};

static const char * gpsVersion = SOFTWARE_RELEASE_VERSION;


static uint8_t ucScrollY;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_AboutScreen =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFreeFormScreen_About,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MAX_SCROLL_Y    (2)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/


static void Print_Job(uint8_t ucLine)
{
   LCD_Char_GotoXY( 0, ucLine );
#ifdef DEBUG
   LCD_Char_WriteStringUpper_Centered(gpsJob);
#else
   uint32_t addressOffset = ( (NUM_32BIT_PARAMS*4) + (NUM_24BIT_PARAMS*3) + (NUM_16BIT_PARAMS*2) + (NUM_8BIT_PARAMS*1) + (NUM_1BIT_PARAMS*1));
   addressOffset += FLASH_BASE_ADDRESS;

   char *gpsJob = ((char *)(addressOffset));
   LCD_Char_WriteStringUpper_Centered(gpsJob);
#endif
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Print_Board(uint8_t ucLine)
{
   LCD_Char_GotoXY( 0, ucLine );
   uint8_t ucDeployment = GetSRU_Deployment();
   LCD_Char_WriteStringUpper_Centered(gpasBoards[ucDeployment]);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Print_Version(uint8_t ucLine)
{
   LCD_Char_GotoXY( 0, ucLine );
   LCD_Char_WriteString_Centered(gpsVersion);
}
/*-----------------------------------------------------------------------------
  Only for MRA, remove for CT/COP
 -----------------------------------------------------------------------------*/
static void Print_CarLabel(uint8_t ucLine)
{
   LCD_Char_GotoXY( 0, ucLine );
#ifdef DEBUG
   LCD_Char_WriteStringUpper_Centered("CAR ");
   LCD_Char_WriteInteger_MinLength(Param_ReadValue_8Bit(enPARAM8__GroupCarIndex)+1, 1);
#else
   if( GetSRU_Deployment() == enSRU_DEPLOYMENT__MR )
   {
      uint32_t addressOffset = ( (NUM_32BIT_PARAMS*4) + (NUM_24BIT_PARAMS*3) + (NUM_16BIT_PARAMS*2) + (NUM_8BIT_PARAMS*1) + (NUM_1BIT_PARAMS*1));
      addressOffset += FLASH_BASE_ADDRESS;
      char *gpsJob = ((char *)(addressOffset));
      char *gpsCarLabel = ((char *)(addressOffset+ strlen(gpsJob) + 1 ));
      LCD_Char_WriteStringUpper_Centered(gpsCarLabel);
   }
   else
   {
      LCD_Char_WriteStringUpper_Centered("CAR ANY");
   }
#endif
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Print_JobID(uint8_t ucLine)
{
   LCD_Char_GotoXY( 2, ucLine );
   LCD_Char_WriteString("JOB ID: ");
   LCD_Char_WriteInteger_ExactLength(Param_ReadValue_24Bit(enPARAM24__JobID), 8);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Print_Copyright(uint8_t ucLine)
{
   LCD_Char_GotoXY( 0, ucLine );
   LCD_Char_WriteString_Centered(gpsCopyright);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_About( void )
{
   LCD_Char_Clear();

   for( uint8_t ucLine = 0; ucLine < 4; ucLine++ )
   {
      uint8_t ucPrintIndex = ucLine+ucScrollY;
      switch( ucPrintIndex )
      {
         case 0:
            Print_Job(ucLine);
            break;
         case 1:
            Print_Board(ucLine);
            break;
         case 2:
            Print_CarLabel(ucLine);
            break;
         case 3:
            Print_JobID(ucLine);
            break;
         case 4:
            Print_Version(ucLine);
            break;
         case 5:
            Print_Copyright(ucLine);
            break;
         default:
            break;
      }
   }

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
      ucScrollY = 0;
   }
   else if ( enKeypress == enKEYPRESS_DOWN )
   {
      if( ucScrollY < MAX_SCROLL_Y )
      {
         ucScrollY++;
      }
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucScrollY)
      {
         ucScrollY--;
      }
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
