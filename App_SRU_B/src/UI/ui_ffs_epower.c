/******************************************************************************
 *
 * @file     ui_ffs_epower.c
 * @brief    E-Power UI pages
 * @version  V1.00
 * @date     12, Dec 2018
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_EPower_NumCars( void );
static void UI_FFS_EPower_PriorityCar( void );
static void UI_FFS_EPower_PretransferStall( void );

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_EPower_NumCars =
{
   .pfnDraw = UI_FFS_EPower_NumCars,
};
static struct st_ui_screen__freeform gstFFS_EPower_PriorityCar =
{
   .pfnDraw = UI_FFS_EPower_PriorityCar,
};
static struct st_ui_screen__freeform gstFFS_EPower_PretransferStall =
{
   .pfnDraw = UI_FFS_EPower_PretransferStall,
};

//----------------------------------------------------------
static struct st_ui_menu_item gstMI_EPower_NumCars =
{
   .psTitle = "Num Active Cars",
   .pstUGS_Next = &gstUGS_EPower_NumCars,
};
static struct st_ui_menu_item gstMI_EPower_PriorityCar =
{
   .psTitle = "Priority Car",
   .pstUGS_Next = &gstUGS_EPower_PriorityCar,
};
static struct st_ui_menu_item gstMI_EPower_PretransferStall =
{
   .psTitle = "Pretransfer Stall",
   .pstUGS_Next = &gstUGS_EPower_PretransferStall,
};
static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_EPower_NumCars,
   &gstMI_EPower_PriorityCar,
   &gstMI_EPower_PretransferStall,
};
static struct st_ui_screen__menu gstMenu_Setup_EPower =
{
   .psTitle = "E-Power",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_EPower_NumCars =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_EPower_NumCars,
};
struct st_ui_generic_screen gstUGS_EPower_PriorityCar =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_EPower_PriorityCar,
};
struct st_ui_generic_screen gstUGS_EPower_PretransferStall =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_EPower_PretransferStall,
};

//-------------------------
struct st_ui_generic_screen gstUGS_Menu_Setup_EPower =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_EPower,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_EPower_NumCars( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__NumEPowerCars;
   stParamEdit.uwParamIndex_End = enPARAM8__NumEPowerCars;
   stParamEdit.psTitle = "Num Active Cars";
   stParamEdit.psUnit = "";
   stParamEdit.ulValue_Max = 8;
   stParamEdit.ucValueOffset = 0;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_EPower_PriorityCar( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__EPowerPriorityCar;
   stParamEdit.uwParamIndex_End = enPARAM8__EPowerPriorityCar;
   stParamEdit.psTitle = "Priority Car";
   stParamEdit.psUnit = "";
   stParamEdit.ulValue_Max = 7;
   stParamEdit.ucValueOffset = 1;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_EPower_PretransferStall( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__EPowerPretransferStall;
   stParamEdit.uwParamIndex_End = enPARAM1__EPowerPretransferStall;
   stParamEdit.psTitle = "Pretransfer Stall";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
