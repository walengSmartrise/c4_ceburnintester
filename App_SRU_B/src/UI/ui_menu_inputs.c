/******************************************************************************
 *
 * @file     ui_menu_io.c
 * @brief    I/O Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "GlobalData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Inputs_Functions_Inspection( void );

static void UI_FFS_Inputs_Functions_Locks( void );
static void UI_FFS_Inputs_Functions_Hoistway( void );
static void UI_FFS_Inputs_Functions_Safety( void );
static void UI_FFS_Inputs_Functions_Relays( void );
static void UI_FFS_Inputs_Functions_Misc( void );

static void UI_FFS_Inputs_Functions_Doors( void );
static void UI_FFS_Inputs_Functions_Contactors( void );
static void UI_FFS_Inputs_Functions_Auto( void );
static void UI_FFS_Inputs_Functions_FireEQ( void );
static void UI_FFS_Inputs_Functions_EPower( void );
static void UI_FFS_Inputs_Functions_Unfixed( void );

static void UI_FFS_Inputs_Functions_Control( void );
static void UI_FFS_Inputs_Functions_SafetyOperations( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
static struct st_ui_menu_item gstMI_Functions_Inspection =
{
   .psTitle = "Inspection",
   .pstUGS_Next = &gstUGS_Status_Inputs_Inspection,
};

static struct st_ui_menu_item gstMI_Functions_Locks =
{
   .psTitle = "Locks",
   .pstUGS_Next = &gstUGS_Status_Inputs_Locks,
};

static struct st_ui_menu_item gstMI_Functions_Hoistway =
{
   .psTitle = "Hoistway Access",
   .pstUGS_Next = &gstUGS_Status_Inputs_Hoistway,
};

static struct st_ui_menu_item gstMI_Functions_Safety =
{
   .psTitle = "Safety Zones",
   .pstUGS_Next = &gstUGS_Status_Inputs_Safety,
};

static struct st_ui_menu_item gstMI_Functions_Relays =
{
   .psTitle = "Relays",
   .pstUGS_Next = &gstUGS_Status_Inputs_Relays,
};

static struct st_ui_menu_item gstMI_Functions_Misc =
{
   .psTitle = "Miscellaneous",
   .pstUGS_Next = &gstUGS_Status_Inputs_Misc,
};

/* Movable */
static struct st_ui_menu_item gstMI_Functions_Doors =
{
   .psTitle = "Doors",
   .pstUGS_Next = &gstUGS_Status_Inputs_Doors,
};

static struct st_ui_menu_item gstMI_Functions_Contactors =
{
   .psTitle = "Contactors",
   .pstUGS_Next = &gstUGS_Status_Inputs_Contactors,
};
static struct st_ui_menu_item gstMI_Functions_Auto =
{
   .psTitle = "Auto Operation",
   .pstUGS_Next = &gstUGS_Status_Inputs_Auto,
};
static struct st_ui_menu_item gstMI_Functions_FireEQ =
{
   .psTitle = "Fire/Earthquake",
   .pstUGS_Next = &gstUGS_Status_Inputs_FireEQ,
};
static struct st_ui_menu_item gstMI_Functions_EPower =
{
   .psTitle = "E-Power",
   .pstUGS_Next = &gstUGS_Status_Inputs_EPower,
};
static struct st_ui_menu_item gstMI_Functions_Control =
{
   .psTitle = "Controller",
   .pstUGS_Next = &gstUGS_Status_Inputs_Control,
};
static struct st_ui_menu_item gstMI_Functions_SafetyOperations =
{
   .psTitle = "Safety Operations",
   .pstUGS_Next = &gstUGS_Status_Inputs_SafetyOperations,
};
static struct st_ui_menu_item * gastMenuItems_Functions[] =
{
   &gstMI_Functions_Inspection,
   &gstMI_Functions_Locks,
   &gstMI_Functions_Hoistway,
   &gstMI_Functions_Safety,
   &gstMI_Functions_Relays,
   &gstMI_Functions_Doors,
   &gstMI_Functions_Contactors,
   &gstMI_Functions_Auto,
   &gstMI_Functions_FireEQ,
   &gstMI_Functions_EPower,
   &gstMI_Functions_Misc,
   &gstMI_Functions_Control,
   &gstMI_Functions_SafetyOperations,
};
static struct st_ui_screen__menu gstMenu_Functions =
{
   .psTitle = "Inputs By Function",
   .pastMenuItems = &gastMenuItems_Functions,
   .ucNumItems = sizeof(gastMenuItems_Functions) / sizeof(gastMenuItems_Functions[ 0 ]),
};
enum en_input_functions gaenInspection[] =
{
   enIN_CTTR,
   enIN_ICTR,
   enIN_HATR,
   enIN_ILTR,
   enIN_IPTR,
   enIN_CTDN,
   enIN_CTUP,
   enIN_CTEN,
   enIN_ICDN,
   enIN_ICUP,
   enIN_ILDN,
   enIN_ILUP,
   enIN_IPDN,
   enIN_IPUP,
   enIN_MRTR,
   enIN_MM,
   enIN_GOV,
   enIN_MRUP,
   enIN_MRDN,
   enIN_CONST_DN,
   enIN_CONST_UP,
   enIN_CONST_EN,
   enIN_BYPH,
   enIN_BYPC,
};
enum en_input_functions gaenLocks[] =
{
   enIN_LTF,
   enIN_LMF,
   enIN_LBF,
   enIN_LTR,
   enIN_LMR,
   enIN_LBR,
   enIN_TCL_F,
   enIN_MCL_F,
   enIN_BCL_F,
   enIN_TCL_R,
   enIN_MCL_R,
   enIN_BCL_R,
};
enum en_input_functions gaenHoistway[] =
{
   enIN_ATU,
   enIN_ATD,
   enIN_ABU,
   enIN_ABD,
};
enum en_input_functions gaenSafety[] =
{
   enIN_PIT,
   enIN_BUF,
   enIN_TFL,
   enIN_BFL,
   enIN_ZMIN,
   enIN_ZHIN,
   enIN_SAFE_FUSE,
   enIN_EB_FUSE,
   enIN_ICST,
   enIN_CT_SW,
   enIN_ESC_HATCH,
   enIN_CAR_SAFE,
};
enum en_input_functions gaenRelays[] =
{
   enIN_CRGB,
   enIN_CRGP,
   enIN_CSFP,
   enIN_MRGM,
   enIN_MSFM,
   enIN_MRGP,
   enIN_MRGBP,
   enIN_MSFP,
   enIN_MRGBM,
   enIN_M120VAC,
   enIN_CPLD_AUX,
};
enum en_input_functions gaenDoors[] =
{
   enIN_DOB_F,
   enIN_DCB_F,
   enIN_DOL_F,
   enIN_DCL_F,
   enIN_PHE_F,
   enIN_PHE_F2,
   enIN_DPM_F,
   enIN_GSWF,
   enIN_DZ_F,
   enIN_SafetyEdge_F,
   enIN_DoorHold_F,

   enIN_DOB_R,
   enIN_DCB_R,
   enIN_DOL_R,
   enIN_DCL_R,
   enIN_PHE_R,
   enIN_PHE_R2,
   enIN_DPM_R,
   enIN_GSWR,
   enIN_DZ_R,
   enIN_SafetyEdge_R,
   enIN_DoorHold_R,
};
enum en_input_functions gaenContactors[] =
{
   enIN_MMC,
   enIN_MBC,
   enIN_MBC2,
};
enum en_input_functions gaenAuto[] =
{
   enIN_INDP,
   enIN_CAR_TO_LOBBY,
   enIN_LIGHT_LOAD,
   enIN_SABBATH,
   enIN_SECURITY_ENABLE_ALL_CAR_CALLS,
   enIN_SWING_ENABLE_KEY,
   enIN_CUSTOM,
   enIN_EMS2_ON,
   enIN_PARKING_OFF,
   enIN_ATTD_ON,
   enIN_ATTD_UP,
   enIN_ATTD_DN,
   enIN_ATTD_BYP,
   enIN_WANDER_GUARD,
   enIN_EnableAllHC,
   enIN_PASSING_CHIME_DISABLE,
   enIN_DISABLE_ALL_HC,
   enIN_ENABLE_LOBBY_PEAK,
   enIN_ENABLE_UP_PEAK,
   enIN_ENABLE_DOWN_PEAK,
   enIN_MARSHAL_MODE,
   enIN_ACTIVE_SHOOTER_MODE,
   enIN_DISTRESS_BUTTON,
   enIN_DISTRESS_ACKNOWLEDGE,
   enIN_ENABLE_ALL_CC_FRONT,
   enIN_ENABLE_ALL_CC_REAR,
};
enum en_input_functions gaenFireEQ[] =
{
   enIN_FIRE_STOP_SW,
   enIN_FIRE_II_ON,
   enIN_FIRE_II_OFF,
   enIN_FIRE_II_HOLD,
   enIN_FIRE_II_CNCL,
   enIN_FIRE_RECALL_OFF,
   enIN_FIRE_RECALL_RESET,
   enIN_SMOKE_HA,
   enIN_SMOKE_MR,
   enIN_SMOKE_MR_2,
   enIN_SMOKE_HA_2,
   enIN_SMOKE_MAIN,
   enIN_SMOKE_ALT,
   enIN_SMOKE_PIT,
   enIN_REMOTE_FIRE_KEY_SWITCH,
   enIN_SEISMIC,
   enIN_CW_DERAIL,
   enIN_EQ_HOISTWAY_SCAN,
};

enum en_input_functions gaenEPower[] =
{
   enIN_BATTERY_POWER,
   enIN_EP_ON,
   enIN_EP_UP_TO_SPEED,
   enIN_EP_AUTO_SELECT,
   enIN_EP_SELECT_1,
   enIN_EP_SELECT_2,
   enIN_EP_SELECT_3,
   enIN_EP_SELECT_4,
   enIN_EP_SELECT_5,
   enIN_EP_SELECT_6,
   enIN_EP_SELECT_7,
   enIN_EP_SELECT_8,
   enIN_AUTO_RESCUE,
   enIN_EP_PRETRANSFER,
   enIN_BATTERY_FLT,
   enIN_RecTrvOn,
   enIN_RecTrvDir,
};

enum en_input_functions gaenMiscellaneous[] =
{
   enIN_CAPT,
   enIN_BAT_STATUS,
   enIN_FAN_AND_LIGHT,
   //enIN_CAR_TO_LOBBY,
   //enIN_REGEN_FLT,
   //enIN_OVER_LOAD,
   //enIN_FULL_LOAD,
   //enIN_LIGHT_LOAD,
   //enIN_SABBATH,
   enIN_UETS ,
   enIN_DETS,
};

enum en_input_functions gaenControl[] =
{
   enIN_REGEN_FLT,
   enIN_MANUAL_PICK,
   enIN_FAULT,
   enIN_DSD_RunEngaged,
   enIN_BRAKE1_BPS,
   enIN_BRAKE2_BPS,
   enIN_VALVE_FLT,
};

enum en_input_functions gaenSafetyOperations[] =
{
   enIN_OVER_LOAD,
   enIN_FULL_LOAD,
   enIN_OOS,
   enIN_FLOOD,
   enIN_PhoneFailActive,
   enIN_PhoneFailReset,
   enIN_TLOSS_RESET,
};
//------------------------------------------------------
/* Functions */
static const char * gpasInputStrings_Fixed[enIN_Reserved+1] =
{
   /* Hoistway Access Directions */
   "Access Top Up",//enIN_ATU
   "Access Top Down",//enIN_ATD
   "Access Bot Up",//enIN_ABU
   "Access Bot Down",//enIN_ABD
   /* Locks */
   "Top Lock (F)",//enIN_LTF
   "Middle Lock (F)",//enIN_LMFHH
   "Bottom Lock (F)",//enIN_LBF
   "Top Lock (R)",//enIN_LTRh
   "Middle Lock (R)",//enIN_LMR
   "Bottom Lock (R)",//enIN_LBR

   "ZoneM In",//enIN_ZMIN
   "ZoneH In",//enIN_ZHIN

   "Bypass Hoistway Door",//enIN_BYPH
   "Bypass Car Door",//enIN_BYPC

   "MR Inspection",//enIN_MRTR

   "Mechanics Mode",//enIN_MM
   "Governor",//enIN_GOV

   "SAFE Fuse",//enIN_SAFE_FUSE
   "EB Fuse",//enIN_EB_FUSE

   "MR Up",//enIN_MRUP
   "MR Down",//enIN_MRDN

   /* Relay Control */
   "EB3 Control",//enIN_CRGB
   "EB1 Control",//enIN_CRGP
   "SFP Control",//enIN_CSFP

   /* Relay Status - 1 */
   "EB2 Status",//enIN_MRGM
   "SFM Status",//enIN_MSFM
   "EB1 Status",//enIN_MRGP
   "EB3 Status",//enIN_MRGBP
   "SFP Status",//enIN_MSFP
   "EB4 Status",//enIN_MRGBM

   "120 VAC In",//enIN_M120VAC
   "CPLD Aux MR",//enIN_CPLD_AUX

   /* Contactor Monitoring */
   "M Cont.",//enIN_MMC
   "Const. Enable", //enIN_CONST_EN
   "B2 Cont.",//enIN_MBC2

   "Capture Button",//enIN_CAPT

   "CW Derail",//enIN_CW_DERAIL

   "Battery Status",//enIN_BAT_STATUS

   "SF Pit",//enIN_PIT
   "SF Buffer",//enIN_BUF
   "Top Final Limit",//enIN_TFL
   "Bottom Final Limit",//enIN_BFL

   /* Inspection */
   "CT Inspection",//enIN_CTTR
   "IC Inspection",//enIN_ICTR
   "HA Inspection",//enIN_HATR
   "LD Inspection",//enIN_ILTR
   "PT Inspection",//enIN_IPTR

   "CT Down",//enIN_CTDN
   "CT Up",//enIN_CTUP
   "IC Down",//enIN_ICDN
   "IC Up",//enIN_ICUP
   "LD Down",//enIN_ILDN
   "LD Up",//enIN_ILUP
   "PT Down",//enIN_IPDN
   "PT Up",//enIN_IPUP

   /* Safety Zones In */
   "CT Enable",//enIN_CTEN

   /* MISC (GSW, BYP, InspBoard) */
   "Fire Stop SW",//enIN_FIRE_STOP_SW
   "In Car Stop",//enIN_ICST
   "Gateswitch (R)",//enIN_GSWR
   "Gateswitch (F)",//enIN_GSWF
   "Door Zone (F)",//enIN_DZ_F
   "Door Zone (R)",//enIN_DZ_R
   "Const. Down",//enIN_CONST_DN
   "Const. Up",//enIN_CONST_UP
   "Cartop Switch",//enIN_CT_SW
   "Escape Hatch",//enIN_ESC_HATCH
   "Car Safety",//enIN_CAR_SAFE
   "B. Cont.", //enIN_MBC
};
static const char * gpasInputStrings_Assigned[NUM_INPUT_FUNCTIONS - enIN_Reserved - 1] =
{
   /* ETS */
   "UETS",
   "DETS",

   /* Doors */
   "DOB_F",
   "DCB_F",
   "DOL_F",
   "DCL_F",
   "PHE_F",

   /* Auto Operation */
   "Independent Srv",

   /* Fire/EQ */
   "Phase2 On",
   "Phase2 Off",
   "Phase2 Hold",
   "Phase2 Cancel",//call cancel
   "Fire RCL Off",
   "Fire RCL RST",

   "Smoke HA",
   "Smoke MR",
   "Smoke Main",
   "Smoke Alt",
   "Seismic",

   "Fan and Light",
   "CarToLobby",

   "DPM_F",

   "DOB_R",
   "DCB_R",
   "DOL_R",
   "DCL_R",
   "PHE_R",

   "DPM_R",
   "RegenFault",

   "Over Load",
   "Full Load",
   "Light Load",
   "Sabbath",
   "Enable All Car Calls",
   "Remote Fire key-switch",
   "OOS",
   "FLOOD",
   "Swing Enable Key",
   "Custom",
   "EMS2_ON",
   "UNUSED_EMS2_OFF",
   "ATTD_ON",
   "ATTD_UP",
   "ATTD_DN",
   "ATTD_BYP",
   "Wander Guard",
   "Battery Power",
   "E-Power On",
   "Up to Speed",
   "Auto Select",
   "Select 1",
   "Select 2",
   "Select 3",
   "Select 4",
   "Select 5",
   "Select 6",
   "Select 7",
   "Select 8",
   "Auto Rescue",
   "DoorHold_F",
   "DoorHold_R",
   "RecTrvOn",
   "RecTrvDir",
   "EnableAllHC",
   "Smoke Pit",
   "PreTransfer",
   "Manual Pick",
   "Battery Fault",
   "FAULT",
   "Parking Off",
   "Phone Fail Active",
   "Phone Fail Reset",
   "DSD Run Engaged",
   "Smoke MR 2",
   "Smoke HA 2",
   "Front TCL",
   "Front MCL",
   "Front BCL",
   "Rear TCL",
   "Rear MCL",
   "Rear BCL",
   "Safety Edge F",
   "Safety Edge R",
   "TLoss Reset", //enIN_TLOSS_RESET
   "Passing Chime Disable", //enIN_PASSING_CHIME_DISABLE
   "PHE_F2", //enIN_PHE_F2
   "PHE_R2", //enIN_PHE_R2
   "Disable All HC", //enIN_DISABLE_ALL_HC
   "Enable Lobby Peak", //enIN_ENABLE_LOBBY_PEAK
   "Enable Up Peak", //enIN_ENABLE_UP_PEAK
   "Enable Down Peak", //enIN_ENABLE_DOWN_PEAK
   "Valve FLT", //enIN_VALVE_FLT
   "Brake1 BPS", //enIN_BRAKE1_BPS
   "Brake2 BPS", //enIN_BRAKE2_BPS
   "Marshal Mode", //enIN_MARSHAL_MODE
   "Active Shooter Mode", //enIN_ACTIVE_SHOOTER_MODE
   "Distress Button",//enIN_DISTRESS_BUTTON
   "Distress Acknowledge",//enIN_DISTRESS_ACKNOWLEDGE
   "EQ Hoistway Scan",//enIN_EQ_HOISTWAY_SCAN
   "Enable All CC Front",//enIN_ENABLE_ALL_CC_FRONT
   "Enable All CC Rear",//enIN_ENABLE_ALL_CC_REAR
};

//---------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Status_Inputs_Functions_Inspection =
{
   .pfnDraw = UI_FFS_Inputs_Functions_Inspection,
};

static struct st_ui_screen__freeform gstFFS_Status_Inputs_Functions_Locks =
{
   .pfnDraw = UI_FFS_Inputs_Functions_Locks,
};
static struct st_ui_screen__freeform gstFFS_Status_Inputs_Functions_Hoistway =
{
   .pfnDraw = UI_FFS_Inputs_Functions_Hoistway,
};
static struct st_ui_screen__freeform gstFFS_Status_Inputs_Functions_Safety =
{
   .pfnDraw = UI_FFS_Inputs_Functions_Safety,
};
static struct st_ui_screen__freeform gstFFS_Status_Inputs_Functions_Relays =
{
   .pfnDraw = UI_FFS_Inputs_Functions_Relays,
};
static struct st_ui_screen__freeform gstFFS_Status_Inputs_Functions_Misc =
{
   .pfnDraw = UI_FFS_Inputs_Functions_Misc,
};

static struct st_ui_screen__freeform gstFFS_Status_Inputs_Functions_Doors =
{
   .pfnDraw = UI_FFS_Inputs_Functions_Doors,
};
static struct st_ui_screen__freeform gstFFS_Status_Inputs_Functions_Contactors =
{
   .pfnDraw = UI_FFS_Inputs_Functions_Contactors,
};
static struct st_ui_screen__freeform gstFFS_Status_Inputs_Functions_Auto =
{
   .pfnDraw = UI_FFS_Inputs_Functions_Auto,
};
static struct st_ui_screen__freeform gstFFS_Status_Inputs_Functions_FireEQ =
{
   .pfnDraw = UI_FFS_Inputs_Functions_FireEQ,
};
static struct st_ui_screen__freeform gstFFS_Status_Inputs_Functions_EPower =
{
   .pfnDraw = UI_FFS_Inputs_Functions_EPower,
};
static struct st_ui_screen__freeform gstFFS_Status_Inputs_Functions_Unfixed =
{
   .pfnDraw = UI_FFS_Inputs_Functions_Unfixed,
};
static struct st_ui_screen__freeform gstFFS_Status_Inputs_Functions_Control =
{
   .pfnDraw = UI_FFS_Inputs_Functions_Control,
};
static struct st_ui_screen__freeform gstFFS_Status_Inputs_Functions_SafetyOperations =
{
   .pfnDraw = UI_FFS_Inputs_Functions_SafetyOperations,
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_Status_Inputs_Functions =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Functions,
   .pstUGS_Previous = &gstUGS_Menu_Status
};

struct st_ui_generic_screen gstUGS_Status_Inputs_Inspection =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Status_Inputs_Functions_Inspection,
   .pstUGS_Previous = &gstUGS_Status_Inputs_Functions
};

struct st_ui_generic_screen gstUGS_Status_Inputs_Locks =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Status_Inputs_Functions_Locks,
   .pstUGS_Previous = &gstUGS_Status_Inputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Inputs_Hoistway =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Status_Inputs_Functions_Hoistway,
   .pstUGS_Previous = &gstUGS_Status_Inputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Inputs_Safety =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Status_Inputs_Functions_Safety,
   .pstUGS_Previous = &gstUGS_Status_Inputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Inputs_Relays =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Status_Inputs_Functions_Relays,
   .pstUGS_Previous = &gstUGS_Status_Inputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Inputs_Misc =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Status_Inputs_Functions_Misc,
   .pstUGS_Previous = &gstUGS_Status_Inputs_Functions
};

struct st_ui_generic_screen gstUGS_Status_Inputs_Doors =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Status_Inputs_Functions_Doors,
   .pstUGS_Previous = &gstUGS_Status_Inputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Inputs_Contactors =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Status_Inputs_Functions_Contactors,
   .pstUGS_Previous = &gstUGS_Status_Inputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Inputs_Auto =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Status_Inputs_Functions_Auto,
   .pstUGS_Previous = &gstUGS_Status_Inputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Inputs_FireEQ =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Status_Inputs_Functions_FireEQ,
   .pstUGS_Previous = &gstUGS_Status_Inputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Inputs_EPower =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Status_Inputs_Functions_EPower,
   .pstUGS_Previous = &gstUGS_Status_Inputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Inputs_Unfixed =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Status_Inputs_Functions_Unfixed,
   .pstUGS_Previous = &gstUGS_Status_Inputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Inputs_Control =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Status_Inputs_Functions_Control,
   .pstUGS_Previous = &gstUGS_Status_Inputs_Functions
};
struct st_ui_generic_screen gstUGS_Status_Inputs_SafetyOperations =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Status_Inputs_Functions_SafetyOperations,
   .pstUGS_Previous = &gstUGS_Status_Inputs_Functions
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
static void Print_Inputs(uint8_t ucCursor, uint8_t ucStartIndex, uint8_t ucEndIndex)
{
   uint8_t ucIndex;
   for(ucIndex = 0; ucIndex < 3; ucIndex++)
   {
      uint8_t ucBitPos = ucStartIndex + ucCursor + ucIndex;
      if(ucBitPos <= ucEndIndex)
      {
         LCD_Char_GotoXY( 0, 1+ucIndex );
         LCD_Char_WriteString("[");

         uint8_t bStatus = GetInputValue(ucBitPos);
         const char * ps = (bStatus) ? "X":" ";
         LCD_Char_WriteString(ps);
         LCD_Char_WriteString("] ");
         ps = (ucBitPos <= enIN_Reserved) ? gpasInputStrings_Fixed[ucBitPos]:gpasInputStrings_Assigned[ucBitPos - enIN_Reserved - 1];
         LCD_Char_WriteString(ps);
      }
   }
}
static void Print_Inputs_edited(uint8_t ucCursor, uint8_t ucArraySize,enum en_input_functions *InputStatusFunction)
{
   uint8_t ucIndex;


   for(ucIndex = 0; ucIndex < 3; ucIndex++)
   {
      uint8_t ucBitPos;
      if((ucCursor + ucIndex) < ucArraySize)
      {
        ucBitPos = *(InputStatusFunction+ ucIndex + ucCursor);
         LCD_Char_GotoXY( 0, 1+ucIndex );
         LCD_Char_WriteString("[");

         uint8_t bStatus = GetInputValue(ucBitPos);
         const char * ps = (bStatus) ? "X":" ";
         LCD_Char_WriteString(ps);
         LCD_Char_WriteString("] ");
         ps = (ucBitPos <= enIN_Reserved) ? gpasInputStrings_Fixed[ucBitPos]:gpasInputStrings_Assigned[ucBitPos - enIN_Reserved - 1];
         LCD_Char_WriteString(ps);
      }
   }
}

/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_Inputs_Functions_Inspection( void )
{
   static uint8_t ucCursorY = 0;
//   static uint8_t ucStart = enIN_CTTR;
//   static uint8_t ucEnd = enIN_IPUP;
   uint8_t ucSizeofArray = NUM_ARRAY_ELEMENTS(gaenInspection);


   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Inspection");

   Print_Inputs_edited(ucCursorY, ucSizeofArray, gaenInspection);

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      int8_t cLimit = ucSizeofArray - 3;
      cLimit = (cLimit < 0) ? 0:cLimit;
      if(ucCursorY < cLimit)
      {
         ucCursorY++;
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_Inputs_Functions_Locks( void )
{
   static uint8_t ucCursorY = 0;
   uint8_t ucSizeofArray = NUM_ARRAY_ELEMENTS(gaenLocks);


   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Locks");

   Print_Inputs_edited(ucCursorY, ucSizeofArray, gaenLocks);

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      int8_t cLimit = ucSizeofArray - 3;
      cLimit = (cLimit < 0) ? 0:cLimit;
      if(ucCursorY < cLimit)
      {
         ucCursorY++;
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_Inputs_Functions_Hoistway( void )
{
   static uint8_t ucCursorY = 0;
//   static uint8_t ucStart = enIN_ATU;
//   static uint8_t ucEnd = enIN_ABD;
   uint8_t ucSizeofArray = NUM_ARRAY_ELEMENTS(gaenHoistway);


   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Hoistway Access");

   Print_Inputs_edited(ucCursorY, ucSizeofArray, gaenHoistway);

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      int8_t cLimit = ucSizeofArray - 3;
      cLimit = (cLimit < 0) ? 0:cLimit;
      if(ucCursorY < cLimit)
      {
         ucCursorY++;
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_Inputs_Functions_Safety( void )
{
   static uint8_t ucCursorY = 0;
   uint8_t ucSizeofArray = NUM_ARRAY_ELEMENTS(gaenSafety);


   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Safety");

   Print_Inputs_edited(ucCursorY, ucSizeofArray, gaenSafety);

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      int8_t cLimit = ucSizeofArray - 3;
      cLimit = (cLimit < 0) ? 0:cLimit;
      if(ucCursorY < cLimit)
      {
         ucCursorY++;
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_Inputs_Functions_Relays( void )
{
   static uint8_t ucCursorY = 0;
   uint8_t ucSizeofArray = NUM_ARRAY_ELEMENTS(gaenRelays);

   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Relays");

   Print_Inputs_edited(ucCursorY, ucSizeofArray, gaenRelays);

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      int8_t cLimit = ucSizeofArray - 3;
      cLimit = (cLimit < 0) ? 0:cLimit;
      if(ucCursorY < cLimit)
      {
         ucCursorY++;
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_Inputs_Functions_Misc( void )
{
   static uint8_t ucCursorY = 0;
   uint8_t ucSizeofArray = NUM_ARRAY_ELEMENTS(gaenMiscellaneous);

   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Misc");

   Print_Inputs_edited(ucCursorY, ucSizeofArray, gaenMiscellaneous);

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      int8_t cLimit = ucSizeofArray - 3;
      cLimit = (cLimit < 0) ? 0:cLimit;
      if(ucCursorY < cLimit)
      {
         ucCursorY++;
      }
   }
}


/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_Inputs_Functions_Doors( void )
{
   static uint8_t ucCursorY = 0;
   uint8_t ucSizeofArray = NUM_ARRAY_ELEMENTS(gaenDoors);


   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Doors");

   Print_Inputs_edited(ucCursorY, ucSizeofArray, gaenDoors);

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      int8_t cLimit = ucSizeofArray - 3;//+1 for DZ
      cLimit = (cLimit < 0) ? 0:cLimit;
      if(ucCursorY < cLimit)
      {
         ucCursorY++;
      }
   }
}

/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_Inputs_Functions_Contactors( void )
{
   static uint8_t ucCursorY = 0;
   uint8_t ucSizeofArray = NUM_ARRAY_ELEMENTS(gaenContactors);

   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Contactors");

   Print_Inputs_edited(ucCursorY, ucSizeofArray, gaenContactors);

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      int8_t cLimit = ucSizeofArray - 3;
      cLimit = (cLimit < 0) ? 0:cLimit;
      if(ucCursorY < cLimit)
      {
         ucCursorY++;
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_Inputs_Functions_Auto( void )
{
   static uint8_t ucCursorY = 0;
   uint8_t ucSizeofArray = NUM_ARRAY_ELEMENTS(gaenAuto);

   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Auto Operation");

   Print_Inputs_edited(ucCursorY, ucSizeofArray, gaenAuto);

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      int8_t cLimit = ucSizeofArray - 3;
      cLimit = (cLimit < 0) ? 0:cLimit;
      if(ucCursorY < cLimit)
      {
         ucCursorY++;
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_Inputs_Functions_FireEQ( void )
{
   static uint8_t ucCursorY = 0;
   uint8_t ucSizeofArray = NUM_ARRAY_ELEMENTS(gaenFireEQ);

   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Fire/Earthquake");

   Print_Inputs_edited(ucCursorY, ucSizeofArray, gaenFireEQ);

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      int8_t cLimit = ucSizeofArray - 3;
      cLimit = (cLimit < 0) ? 0:cLimit;
      if(ucCursorY < cLimit)
      {
         ucCursorY++;
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_Inputs_Functions_EPower( void )
{
   static uint8_t ucCursorY = 0;
   uint8_t ucSizeofArray = NUM_ARRAY_ELEMENTS(gaenEPower);

   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("E-Power");

   Print_Inputs_edited(ucCursorY, ucSizeofArray, gaenEPower);

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      int8_t cLimit = ucSizeofArray - 3;
      cLimit = (cLimit < 0) ? 0:cLimit;
      if(ucCursorY < cLimit)
      {
         ucCursorY++;
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_Inputs_Functions_Unfixed( void )
{
   static uint8_t ucCursorY = 0;
   static uint8_t ucStart = enIN_Reserved+1;
   static uint8_t ucEnd = NUM_INPUT_FUNCTIONS-1;

   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Unfixed");

   Print_Inputs(ucCursorY, ucStart, ucEnd);

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      int8_t cLimit = ucEnd - ucStart - 2;
      cLimit = (cLimit < 0) ? 0:cLimit;
      if(ucCursorY < cLimit)
      {
         ucCursorY++;
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_Inputs_Functions_Control( void )
{
   static uint8_t ucCursorY = 0;
   uint8_t ucSizeofArray = NUM_ARRAY_ELEMENTS(gaenControl);

   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Controller");

   Print_Inputs_edited(ucCursorY, ucSizeofArray, gaenControl);

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      int8_t cLimit = ucSizeofArray - 3;
      cLimit = (cLimit < 0) ? 0:cLimit;
      if(ucCursorY < cLimit)
      {
         ucCursorY++;
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_Inputs_Functions_SafetyOperations( void )
{
   static uint8_t ucCursorY = 0;
   uint8_t ucSizeofArray = NUM_ARRAY_ELEMENTS(gaenSafetyOperations);

   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Safety Operations");

   Print_Inputs_edited(ucCursorY, ucSizeofArray, gaenSafetyOperations);

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      int8_t cLimit = ucSizeofArray - 3;
      cLimit = (cLimit < 0) ? 0:cLimit;
      if(ucCursorY < cLimit)
      {
         ucCursorY++;
      }
   }
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
