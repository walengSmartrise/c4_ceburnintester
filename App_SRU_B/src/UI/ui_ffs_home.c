/******************************************************************************
 *
 * @file     ui_ffs_home.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "sru.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_Home( void );

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_ui_screen__freeform gstFreeFormScreen_Home =
{
   .pfnDraw = UI_FFS_Home,
};

static const char *gpasManualMode_Strings[ NUM_MODE_MANUAL ] =
{
   "Unknown",
   "Invalid Insp.",
   "No Inspection",
   "CT Inspection",
   "IC Inspection",
   "HA Inspection",
   "MR Inspection",
   "PIT Inspection",
   "LND Inspection",
   "CONSTRUCTION",
   "HA Insp. (TOP)",
   "HA Insp. (BOT)",
};

static const char *gpasLearnMode_Strings[ NUM_MODE_LEARN ] =
{
   "Unknown",
   "Invalid DIP SW",
   "Unknown",
   "Go To A Terminal",
   "Hold DIR Learn BRK",
   "Hold DIR Learn BRK2",
   "Bypass Term Limits",
   "Hold UP/DN To Start",
   "Learning BRK",
   "Learning BRK2",
   "Learning BTM To TOP",
   "Learning TOP To BTM",
   "Enable Brake BPS",
   "Learn - UNUSED",
   "Learn Complete",
};

static const char *gpasAutoMode_Strings[ NUM_MODE_AUTO ] =
{
   "Unknown",
   "Invalid",
   "Normal ",
   "Fire Phase I",
   "Fire Phase II",
   "EMS Phase I",
   "EMS Phase II",
   "Attendant",
   "Indp. Service",
   "Seismic",
   "CW Derail",
   "Sabbath",
   "Emergency Power",
   "Evacuation",
   "Out Of Service",
   "Battery Lowering",
   "Battery Rescue",
   "Prison Transport 1",
   "Prison Transport 2",
   "Acceptance Test",
   "Wanderguard",
   "HUGS",
   "Car Switch",
   "Test",
   "Wind Operation",
   "Flood Operation",
   "Swing Operation",
   "Custom",
   "Active Shooter",
   "Marshal Mode",
   "VIP Mode",
};

static uint8_t gucScrollY = 0;

static uint8_t bInitSync = 0;

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_HomeScreen =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFreeFormScreen_Home,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Home_Draw_MOP( uint8_t ucCLASSOP, uint8_t ucAutoOP, uint8_t ucManualOP, uint8_t ucLearnOP )
{
   LCD_Char_GotoXY( 0, 0 );

   switch ( ucCLASSOP )
   {
      case CLASSOP__SEMI_AUTO:
         LCD_Char_WriteString( gpasLearnMode_Strings[ ucLearnOP ] );
         break;

      case CLASSOP__AUTO:
         if( CheckIf_EPowerCarInNormal() )
         {
            LCD_Char_WriteString( gpasAutoMode_Strings[ MODE_A__NORMAL ] );
         }
         else
         {
            LCD_Char_WriteString( gpasAutoMode_Strings[ ucAutoOP ] );
         }

         if(GetOperation3_CaptureMode_MR() == CAPTURE_IDLE)
         {
            LCD_Char_GotoXY( 6, 0 );
            LCD_Char_WriteString("(Captured)");
         }
         else if( GetFullLoadFlag() )
         {
            LCD_Char_GotoXY( 6, 0 );
            LCD_Char_WriteString("(FULL LD)");
         }
         else if( Param_ReadValue_1Bit(enPARAM1__DEBUG_MonitorCarDirection)
             && ( GetOperation_AutoMode() == MODE_A__NORMAL ) )
         {
            LCD_Char_GotoXY( 10, 0 );
            if( GetGroup_DispatchData() & 0x04 )
            {
               LCD_Char_WriteString("(P-UP)");
            }
            else
            {
               LCD_Char_WriteString("(P-DN)");
            }
         }
         else if(Param_ReadValue_1Bit(enPARAM1__Disable_Rear_DOB)
             && ( GetOperation_AutoMode() == MODE_A__NORMAL ))
         {
            LCD_Char_GotoXY( 6, 0 );
            LCD_Char_WriteString("(DOB-R DISA)");
         }
         break;

      case CLASSOP__MANUAL:
         LCD_Char_WriteString( gpasManualMode_Strings[ ucManualOP ] );
         break;

      default:
         LCD_Char_WriteString( "Unknown" );
         break;
   }
   if( GetOperation4_ParamSync() ) //
   {
      bInitSync = 1;
      LCD_Char_GotoXY( 0, 0 );
      LCD_Char_WriteString( "Sync in Progress");
   }
   else if( !GetOperation4_ParamSync() && bInitSync  )
   {
      LCD_Char_GotoXY( 0, 0 );
      LCD_Char_WriteString( "Sync Complete");
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Home_Draw_DZ( uint8_t bDZ, uint8_t ucCLASSOP )
{
   if ( ucCLASSOP == CLASSOP__SEMI_AUTO )
   {
      LCD_Char_GotoXY( 18, 1-gucScrollY );
   }
   else
   {
      LCD_Char_GotoXY( 18, 0 );
   }

   if ( bDZ )
   {
      LCD_Char_WriteString( "DZ" );
   }
   else
   {
      LCD_Char_WriteString( ".." );
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Home_Draw_Doors( uint8_t bNumDoors, enum en_door_states eDoor1, enum en_door_states eDoor2, int8_t cDirection )
{
   LCD_Char_GotoXY( 0, 1-gucScrollY );
   uint8_t bIN_PHE_F = ( GetPHE2InputProgrammed_F2() ) ? ( GetInputValue(enIN_PHE_F) & GetInputValue(enIN_PHE_F2) ):GetInputValue(enIN_PHE_F);
   uint8_t bIN_PHE_R = ( GetPHE2InputProgrammed_R2() ) ? ( GetInputValue(enIN_PHE_R) & GetInputValue(enIN_PHE_R2) ):GetInputValue(enIN_PHE_R);
   if( !bNumDoors )
   {
      if ( cDirection == 1 )
      {
         LCD_Char_WriteString( "UP  " );
      }
      else if ( cDirection == -1 )
      {
         LCD_Char_WriteString( "DN  " );
      }
      else
      {
         LCD_Char_WriteString( "..  " );
      }

      UI_PrintDoorSymbol( eDoor1,
                          bIN_PHE_F,
                          GetInputValue(enIN_GSWF),
                          GetOutputValue(enOUT_DO_F),
                          GetOutputValue(enOUT_DC_F) );
   }
   else
   {
      UI_PrintDoorSymbol( eDoor1,
                          bIN_PHE_F,
                          GetInputValue(enIN_GSWF),
                          GetOutputValue(enOUT_DO_F),
                          GetOutputValue(enOUT_DC_F) );

      UI_PrintDoorSymbol( eDoor2,
                          bIN_PHE_R,
                          GetInputValue(enIN_GSWR),
                          GetOutputValue(enOUT_DO_R),
                          GetOutputValue(enOUT_DC_R) );
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Home_Draw_PI_and_Dest( uint8_t ucPI, uint8_t ucDest, int8_t cDirection, int8_t cArrival )
{
   LCD_Char_GotoXY( 10, 1-gucScrollY );
   char * psPI;
   if ( ucPI != 0xFF && ucPI != 0 )
   {
      if ( cDirection )
      {
         if ( ucDest != 0xFF && ucDest != 0 )
         {

            if ( cDirection == 1 )
            {
               LCD_Char_WriteString( "(");
               psPI = Get_PI_Label( ucPI - 1 );
               //ignore first char for two PI displays
               if( ( Param_ReadValue_1Bit(enPARAM1__Enable3DigitPI) )
                && ( *psPI != ' ' ) ) // Drop the third char if its blank to save space
               {
                  LCD_Char_WriteChar(*(psPI));
               }
               LCD_Char_WriteChar(*(psPI+1));
               LCD_Char_WriteChar(*(psPI+2));

               LCD_Char_WriteString( ")->(");
               psPI = Get_PI_Label( ucDest - 1 );
               //ignore first char for two PI displays
               if( ( Param_ReadValue_1Bit(enPARAM1__Enable3DigitPI) )
                && ( *psPI != ' ' ) ) // Drop the third char if its blank to save space
               {
                  LCD_Char_WriteChar(*(psPI));
               }
               LCD_Char_WriteChar(*(psPI+1));
               LCD_Char_WriteChar(*(psPI+2));

               LCD_Char_WriteString( ")");
            }
            else if ( cDirection == -1 )
            {
               LCD_Char_WriteString( "(");
               psPI = Get_PI_Label( ucDest - 1 );
               //ignore first char for two PI displays
               if( ( Param_ReadValue_1Bit(enPARAM1__Enable3DigitPI) )
                && ( *psPI != ' ' ) ) // Drop the third char if its blank to save space
               {
                  LCD_Char_WriteChar(*(psPI));
               }
               LCD_Char_WriteChar(*(psPI+1));
               LCD_Char_WriteChar(*(psPI+2));

               LCD_Char_WriteString( ")<-(");
               psPI = Get_PI_Label( ucPI - 1 );

               //ignore first char for two PI displays
               if( ( Param_ReadValue_1Bit(enPARAM1__Enable3DigitPI) )
                && ( *psPI != ' ' ) ) // Drop the third char if its blank to save space
               {
                  LCD_Char_WriteChar(*(psPI));
               }
               LCD_Char_WriteChar(*(psPI+1));
               LCD_Char_WriteChar(*(psPI+2));
               LCD_Char_WriteString( ")");
            }
         }
      }
      else if ( cArrival )
      {
         if ( cArrival == 1 )
         {
            LCD_Char_WriteString( "(");

            psPI = Get_PI_Label( ucPI - 1 );
            //ignore first char for two PI displays
            if( ( Param_ReadValue_1Bit(enPARAM1__Enable3DigitPI) )
             && ( *psPI != ' ' ) ) // Drop the third char if its blank to save space
            {
               LCD_Char_WriteChar(*(psPI));
            }
            LCD_Char_WriteChar(*(psPI+1));
            LCD_Char_WriteChar(*(psPI+2));

            LCD_Char_WriteString( ")->");
         }
         else if ( cArrival == -1 )
         {
            LCD_Char_WriteString( "<-(");

            psPI = Get_PI_Label( ucPI - 1 );
            //ignore first char for two PI displays
            if(Param_ReadValue_1Bit(enPARAM1__Enable3DigitPI))
            {
               LCD_Char_WriteChar(*(psPI));
            }
            LCD_Char_WriteChar(*(psPI+1));
            LCD_Char_WriteChar(*(psPI+2));

            LCD_Char_WriteString( ")");
         }
      }
      else
      {
         LCD_Char_WriteString( "(");

         psPI = Get_PI_Label( ucPI - 1 );
         //ignore first char for two PI displays
         if( ( Param_ReadValue_1Bit(enPARAM1__Enable3DigitPI) )
          && ( *psPI != ' ' ) ) // Drop the third char if its blank to save space
         {
            LCD_Char_WriteChar(*(psPI));
         }
         LCD_Char_WriteChar(*(psPI+1));
         LCD_Char_WriteChar(*(psPI+2));

         LCD_Char_WriteString( ")");
      }
   }
   else
   {
      LCD_Char_WriteString( "(--)");
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Home_Draw_Position( uint32_t ulPos )
{
   LCD_Char_GotoXY( 0, 2-gucScrollY );

   LCD_Char_WriteInteger( ulPos );

   LCD_Char_GotoXY( 9, 2-gucScrollY );

   LCD_WritePositionInFeet_Shortened( ulPos );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Home_Draw_Speed( int32_t iSpeed_Command, int32_t iSpeed_Feedback )
{
   LCD_Char_GotoXY( 0, 3-gucScrollY );

   LCD_Char_WriteString( "CMD:");

   if( GetMotion_RunFlag() )
   {
      LCD_Char_WriteSignedInteger( iSpeed_Command );
   }
   else if( GetOperation_MotionCmd() == MOCMD__EMERG_STOP )
   {
      LCD_Char_WriteString( "ESTOP");
   }
   else
   {
      LCD_Char_WriteString( "STOP");
   }

   LCD_Char_GotoXY( 10, 3+gucScrollY );

   LCD_Char_WriteString( "FPM:");
   LCD_Char_WriteSignedInteger( iSpeed_Feedback );
}
/*-----------------------------------------------------------------------------
 -----------------------------------------------------------------------------*/
static void Home_Draw_DoorDataF( uint8_t ucLine )
{
   /*
    ---DOOR DATA FRONT--
    DOB DCB PHE NDG DO
    DOL DCL GSW DPM DC
    TCL MCL BCL CAM SE
    PHE2
   */
   char * ps;

   if(!ucLine)
   {
      LCD_Char_GotoXY(0,4-gucScrollY);
      LCD_Char_WriteStringUpper("---DOOR DATA FRONT--");
   }
   else if(ucLine == 1)
   {
      LCD_Char_GotoXY(0,5-gucScrollY);

      uint8_t bDOB = GetInputValue(enIN_DOB_F);
      uint8_t bDCB = GetInputValue(enIN_DCB_F);
      uint8_t bPHE = ( GetPHE2InputProgrammed_F2() ) ? ( GetInputValue(enIN_PHE_F) & GetInputValue(enIN_PHE_F2) ):GetInputValue(enIN_PHE_F);
      uint8_t bNDG = GetOutputValue(enOUT_NDG_F);
      uint8_t bDO = GetOutputValue(enOUT_DO_F);

      ps = (bDOB) ? "DOB":"...";
      LCD_Char_WriteStringUpper(ps);

      ps = (bDCB) ? "DCB":"...";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);

      ps = (!bPHE) ? "PHE":"...";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);

      ps = (bNDG) ? "NDG":"...";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);

      ps = (bDO) ? "DO":"..";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);
   }
   else if( ucLine == 2)
   {
      LCD_Char_GotoXY(0,6-gucScrollY);

      uint8_t bDOL = GetInputValue(enIN_DOL_F);
      uint8_t bDCL = GetInputValue(enIN_DCL_F);
      uint8_t bGSW = GetInputValue(enIN_GSWF);
      uint8_t bDPM = GetInputValue(enIN_DPM_F);
      uint8_t bDC = GetOutputValue(enOUT_DC_F);

      ps = (!bDOL) ? "DOL":"...";
      LCD_Char_WriteStringUpper(ps);

      ps = (!bDCL) ? "DCL":"...";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);

      ps = (bGSW) ? "GSW":"...";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);

      ps = (bDPM) ? "DPM":"...";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);

      ps = (bDC) ? "DC":"..";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);
   }
   else if( ucLine == 3 )
   {
      LCD_Char_GotoXY(0,7-gucScrollY);

      uint8_t bTCL = GetInputValue(enIN_TCL_F);
      uint8_t bMCL = GetInputValue(enIN_MCL_F);
      uint8_t bBCL = GetInputValue(enIN_BCL_F);
      uint8_t bSE = GetSEInputProgrammed_F() ? GetInputValue(enIN_SafetyEdge_F): 1;
      uint8_t bCAM = GetOutputValue(enOUT_CAM_F);

      ps = (bTCL) ? "TCL":"...";
      LCD_Char_WriteStringUpper(ps);

      ps = (bMCL) ? "MCL":"...";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);

      ps = (bBCL) ? "BCL":"...";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);

      ps = (bCAM) ? "CAM":"...";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);

      ps = (!bSE) ? "SE":"..";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);
   }
   else
   {
      LCD_Char_GotoXY(0,8-gucScrollY);

      uint8_t bPHE2 = GetPHE2InputProgrammed_F2() ? GetInputValue(enIN_PHE_F2) : 1;

      ps = (!bPHE2) ? "PHE2":"....";
      LCD_Char_WriteStringUpper(ps);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Home_Draw_DoorDataR( uint8_t ucLine )
{
   /*
    ---DOOR DATA REAR---
    DOB DCB PHE NDG DO
    DOL DCL GSW DPM DC
    TCL MCL BCL CAM SE
    PHE2
   */
   char * ps;

   if(!ucLine)
   {
      LCD_Char_GotoXY(0,9-gucScrollY);
      LCD_Char_WriteStringUpper("---DOOR DATA REAR---");
   }
   else if(ucLine == 1)
   {
      LCD_Char_GotoXY(0,10-gucScrollY);

      uint8_t bDOB = GetInputValue(enIN_DOB_R);
      uint8_t bDCB = GetInputValue(enIN_DCB_R);
      uint8_t bPHE = ( GetPHE2InputProgrammed_R2() ) ? ( GetInputValue(enIN_PHE_R) & GetInputValue(enIN_PHE_R2) ):GetInputValue(enIN_PHE_R);
      uint8_t bNDG = GetOutputValue(enOUT_NDG_R);
      uint8_t bDO = GetOutputValue(enOUT_DO_R);

      ps = (bDOB && GetFP_RearDoors()) ? "DOB":"...";
      LCD_Char_WriteStringUpper(ps);

      ps = (bDCB && GetFP_RearDoors()) ? "DCB":"...";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);

      ps = (!bPHE && GetFP_RearDoors()) ? "PHE":"...";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);

      ps = (bNDG) ? "NDG":"...";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);

      ps = (bDO) ? "DO":"..";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);
    }
    else if( ucLine == 2)
    {
       LCD_Char_GotoXY(0,11-gucScrollY);
       uint8_t bDOL = GetInputValue(enIN_DOL_R);
       uint8_t bDCL = GetInputValue(enIN_DCL_R);
       uint8_t bGSW = GetInputValue(enIN_GSWR);
       uint8_t bDPM = GetInputValue(enIN_DPM_R);
       uint8_t bDC = GetOutputValue(enOUT_DC_R);

      ps = (!bDOL && GetFP_RearDoors()) ? "DOL":"...";
      LCD_Char_WriteStringUpper(ps);

      ps = (!bDCL && GetFP_RearDoors()) ? "DCL":"...";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);

      ps = (bGSW && GetFP_RearDoors()) ? "GSW":"...";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);

      ps = (bDPM) ? "DPM":"...";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);

      ps = (bDC && GetFP_RearDoors()) ? "DC":"..";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);
   }
    else if( ucLine == 3 )
    {
       LCD_Char_GotoXY(0,12-gucScrollY);

       uint8_t bTCL = GetInputValue(enIN_TCL_R);
       uint8_t bMCL = GetInputValue(enIN_MCL_R);
       uint8_t bBCL = GetInputValue(enIN_BCL_R);
       uint8_t bSE = GetSEInputProgrammed_R() ? GetInputValue(enIN_SafetyEdge_R): 1;
       uint8_t bCAM = GetOutputValue(enOUT_CAM_R);

       ps = (bTCL) ? "TCL":"...";
       LCD_Char_WriteStringUpper(ps);

       ps = (bMCL) ? "MCL":"...";
       LCD_Char_AdvanceX(1);
       LCD_Char_WriteStringUpper(ps);

       ps = (bBCL) ? "BCL":"...";
       LCD_Char_AdvanceX(1);
       LCD_Char_WriteStringUpper(ps);

       ps = (bCAM) ? "CAM":"..";
       LCD_Char_AdvanceX(1);
       LCD_Char_WriteStringUpper(ps);

       ps = (!bSE) ? "SE":"...";
       LCD_Char_AdvanceX(1);
       LCD_Char_WriteStringUpper(ps);
    }
    else
    {
       LCD_Char_GotoXY(0,13-gucScrollY);

       uint8_t bPHE2 = GetPHE2InputProgrammed_R2() ? GetInputValue(enIN_PHE_R2) : 1;

       ps = (!bPHE2 && GetFP_RearDoors()) ? "PHE2":"....";
       LCD_Char_WriteStringUpper(ps);
    }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Home_Draw_LockData( uint8_t ucLine )
{
   static const uint8_t ucStartPosY = 14;
   /*
    -----LOCK DATA-----
    LTF LMF LBF
    LTR LMR LBR
   */
   char * ps;

   if(!ucLine) /* First line */
   {
      LCD_Char_GotoXY(0,ucStartPosY-gucScrollY);
      LCD_Char_WriteStringUpper("-----LOCK DATA-----");
   }
   else if(ucLine == 1) /* Second line */
   {
      LCD_Char_GotoXY(0,ucStartPosY+1-gucScrollY);

      uint8_t bLTF = GetInputValue(enIN_LTF);
      uint8_t bLMF = GetInputValue(enIN_LMF);
      uint8_t bLBF = GetInputValue(enIN_LBF);

      ps = (bLTF) ? "LTF":"...";
      LCD_Char_WriteStringUpper(ps);

      ps = (bLMF) ? "LMF":"...";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);

      ps = (bLBF) ? "LBF":"...";
      LCD_Char_AdvanceX(1);
      LCD_Char_WriteStringUpper(ps);
    }
    else/* Third line */
    {
       LCD_Char_GotoXY(0,ucStartPosY+2-gucScrollY);

       uint8_t bLTR = GetInputValue(enIN_LTR) && GetFP_RearDoors();
       uint8_t bLMR = GetInputValue(enIN_LMR) && GetFP_RearDoors();
       uint8_t bLBR = GetInputValue(enIN_LBR) && GetFP_RearDoors();

       ps = (bLTR) ? "LTR":"...";
       LCD_Char_WriteStringUpper(ps);

       ps = (bLMR) ? "LMR":"...";
       LCD_Char_AdvanceX(1);
       LCD_Char_WriteStringUpper(ps);

       ps = (bLBR) ? "LBR":"...";
       LCD_Char_AdvanceX(1);
       LCD_Char_WriteStringUpper(ps);
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Home( void )
{
   uint32_t ulPos;
   int32_t iSpeed_Command;
   int32_t iSpeed_Feedback;
   uint8_t ucCLASSOP, ucAutoOP, ucManualOP, ucLearnOP;
   enum en_door_states eDoor1, eDoor2;
   uint8_t ucPI;
   uint8_t ucDest;
   uint8_t bDZ;
   int8_t cDirection, cArrival;

   LCD_Char_Clear();
   //----------------------------------------
   ulPos = GetPosition_PositionCount();
   iSpeed_Command = GetMotion_SpeedCommand();
   iSpeed_Feedback = GetPosition_Velocity();
   ucCLASSOP = GetOperation_ClassOfOp();
   ucAutoOP = GetOperation_AutoMode();
   ucLearnOP = GetOperation_LearnMode();
   ucManualOP = GetOperation_ManualMode();
   eDoor1 = GetDoorState_Front();
   eDoor2 = GetDoorState_Rear();
   bDZ = ( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R) );
   ucPI = GetOperation_CurrentFloor() + 1;
   ucDest = GetOperation_DestinationFloor() + 1;
   cDirection = GetMotion_Direction();
   cArrival = 0;

   //-----------------------------------------
   uint8_t ucIndex;
   for(ucIndex = 0; ucIndex < 4; ucIndex++)
   {
      uint8_t ucLine = gucScrollY+ucIndex;
      switch(ucLine)
      {
         case 0:
            Home_Draw_MOP( ucCLASSOP, ucAutoOP, ucManualOP, ucLearnOP );
            Home_Draw_DZ( bDZ, ucCLASSOP );
            break;

         case 1:
            Home_Draw_Doors( GetFP_RearDoors(), eDoor1, eDoor2, cDirection );
            if ( ucCLASSOP != CLASSOP__AUTO )
            {
               Home_Draw_PI_and_Dest( ucPI, 0, cDirection, cArrival );
            }
            else
            {
               Home_Draw_PI_and_Dest( ucPI, ucDest, cDirection, cArrival );
            }
            break;

         case 2:
            Home_Draw_Position( ulPos );
            break;

         case 3:
            Home_Draw_Speed( iSpeed_Command, iSpeed_Feedback );
            break;

         case 4:
         case 5:
         case 6:
         case 7:
         case 8:
            Home_Draw_DoorDataF(ucLine-4);
            break;

         case 9:
         case 10:
         case 11:
         case 12:
         case 13:
            Home_Draw_DoorDataR(ucLine-9);
            break;

         case 14:
         case 15:
         case 16:
            Home_Draw_LockData(ucLine-14);
            break;
         default:
            break;
      }
   }
   //-----------------------------------------
   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_RIGHT )
   {
      NextScreen( &gstUGS_MainMenu );
   }
   else if(enKeypress == enKEYPRESS_UP)
   {
      if(gucScrollY)
      {
         gucScrollY--;
      }
   }
   else if(enKeypress == enKEYPRESS_DOWN)
   {
      if(gucScrollY < 13)
      {
         gucScrollY++;
      }
   }
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
