/******************************************************************************
 *
 * @file     ui_ffs_doors.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
#include "motion.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Misc_NumberOfDoors( void );
static void UI_FFS_Doors_Control_Both( void );
static void UI_FFS_Doors_Control_Front( void );

static void UI_FFS_Edit_Door_Timer( uint32_t ulParamNumber, char* psTitle );
static void UI_FFS_Edit_Door_ParamBit( uint32_t ulParamNumber, char* psTitle );

static void UI_FFS_Door_DwellTimer( void );
static void UI_FFS_Door_StuckTimer( void );
static void UI_FFS_Door_NudgeTimer( void );
static void UI_FFS_Door_DwellHallTimer( void );
static void UI_FFS_Door_DwellADATimer( void );
static void UI_FFS_Door_PreOpeningDistance( void );
static void UI_FFS_Door_DC_On_Run( void );
static void UI_FFS_Door_DC_On_Close( void );
static void UI_FFS_Door_DO_On_Open( void );
static void UI_FFS_Door_DisableOnCTStop( void );
static void UI_FFS_Door_DisableOnHA( void );
static void UI_FFS_Door_IMotionDoors( void );
static void UI_FFS_Door_DwellHoldTimer( void );
static void UI_FFS_Door_SabbathDwell( void );
static void UI_FFS_Door_NoDemandDoorsOpen( void );
static void UI_FFS_Door_JumperTimeout( void );
static void UI_FFS_Door_JumperOnDOL( void );
static void UI_FFS_Door_HourlyFaultLimit( void );
static void UI_FFS_Door_NudgeBuzzerOnly( void );
static void UI_FFS_Door_NudgeNoBuzzer( void );
static void UI_FFS_Door_OpeningTime( void );
static void UI_FFS_Door_CheckTime( void );
static void UI_FFS_Door_LobbyDwellTimer( void );
static void UI_FFS_Door_DoorTypeSelect_F( void );
static void UI_FFS_Door_DoorTypeSelect_R( void );
static void UI_FFS_Door_SwingDoorBitmap_F( void );
static void UI_FFS_Door_SwingDoorBitmap_R( void );
static void UI_FFS_Door_TimeoutLockAndCAM( void );
static void UI_FFS_Door_RetiringCAM( void );
static void UI_FFS_Door_FixedCAM( void );
static void UI_FFS_Door_Swing_GSW_Lock_Timeout( void );
static void UI_FFS_Door_Swing_Contacts_Timeout( void );
static void UI_FFS_Door_Disable_Rear_DOB( void );

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_Misc_NumberOfDoors =
{
   .pfnDraw = UI_FFS_Misc_NumberOfDoors,
};
static struct st_ui_screen__freeform gstFreeFormScreen_Doors_Control =
{
   .pfnDraw = UI_FFS_Doors_Control_Both,
};
static struct st_ui_screen__freeform gstFFS_Door_DwellTimer =
{
   .pfnDraw = UI_FFS_Door_DwellTimer,
};
static struct st_ui_screen__freeform gstFFS_Door_StuckTimer =
{
   .pfnDraw = UI_FFS_Door_StuckTimer,
};
static struct st_ui_screen__freeform gstFFS_Door_NudgeTimer =
{
   .pfnDraw = UI_FFS_Door_NudgeTimer,
};
static struct st_ui_screen__freeform gstFFS_Door_DwellHallTimer =
{
   .pfnDraw = UI_FFS_Door_DwellHallTimer,
};
static struct st_ui_screen__freeform gstFFS_Door_DwellADATimer =
{
   .pfnDraw = UI_FFS_Door_DwellADATimer,
};
static struct st_ui_screen__freeform gstFFS_Door_PreOpeningDistance =
{
   .pfnDraw = UI_FFS_Door_PreOpeningDistance,
};
static struct st_ui_screen__freeform gstFFS_Door_DC_On_Run =
{
   .pfnDraw = UI_FFS_Door_DC_On_Run,
};
static struct st_ui_screen__freeform gstFFS_Door_DC_On_Close =
{
   .pfnDraw = UI_FFS_Door_DC_On_Close,
};
static struct st_ui_screen__freeform gstFFS_Door_DO_On_Open =
{
   .pfnDraw = UI_FFS_Door_DO_On_Open,
};
static struct st_ui_screen__freeform gstFFS_Door_DisableOnCTStop =
{
   .pfnDraw = UI_FFS_Door_DisableOnCTStop,
};
static struct st_ui_screen__freeform gstFFS_Door_DisableOnHA =
{
   .pfnDraw = UI_FFS_Door_DisableOnHA,
};
static struct st_ui_screen__freeform gstFFS_Door_IMotionDoors =
{
   .pfnDraw = UI_FFS_Door_IMotionDoors,
};
static struct st_ui_screen__freeform gstFFS_Door_DwellHoldTimer =
{
   .pfnDraw = UI_FFS_Door_DwellHoldTimer,
};
static struct st_ui_screen__freeform gstFFS_Door_SabbathDwell =
{
   .pfnDraw = UI_FFS_Door_SabbathDwell,
};
static struct st_ui_screen__freeform gstFFS_Door_NoDemandDoorsOpen =
{
   .pfnDraw = UI_FFS_Door_NoDemandDoorsOpen,
};
static struct st_ui_screen__freeform gstFFS_Door_JumperTimeout =
{
   .pfnDraw = UI_FFS_Door_JumperTimeout,
};
static struct st_ui_screen__freeform gstFFS_Door_JumperOnDOL =
{
   .pfnDraw = UI_FFS_Door_JumperOnDOL,
};
static struct st_ui_screen__freeform gstFFS_Door_HourlyFaultLimit =
{
   .pfnDraw = UI_FFS_Door_HourlyFaultLimit,
};
static struct st_ui_screen__freeform gstFFS_Door_NudgeBuzzerOnly =
{
   .pfnDraw = UI_FFS_Door_NudgeBuzzerOnly,
};
static struct st_ui_screen__freeform gstFFS_Door_NudgeNoBuzzer =
{
   .pfnDraw = UI_FFS_Door_NudgeNoBuzzer,
};
static struct st_ui_screen__freeform gstFFS_Door_OpeningTime =
{
   .pfnDraw = UI_FFS_Door_OpeningTime,
};
static struct st_ui_screen__freeform gstFFS_Door_CheckTime =
{
   .pfnDraw = UI_FFS_Door_CheckTime,
};
static struct st_ui_screen__freeform gstFFS_Door_LobbyDwellTimer =
{
   .pfnDraw = UI_FFS_Door_LobbyDwellTimer,
};
static struct st_ui_screen__freeform gstFFS_Door_DoorTypeSelect_F =
{
   .pfnDraw = UI_FFS_Door_DoorTypeSelect_F,
};
static struct st_ui_screen__freeform gstFFS_Door_DoorTypeSelect_R =
{
   .pfnDraw = UI_FFS_Door_DoorTypeSelect_R,
};
static struct st_ui_screen__freeform gstFFS_Door_SwingDoorBitmap_F =
{
   .pfnDraw = UI_FFS_Door_SwingDoorBitmap_F,
};
static struct st_ui_screen__freeform gstFFS_Door_SwingDoorBitmap_R =
{
   .pfnDraw = UI_FFS_Door_SwingDoorBitmap_R,
};
static struct st_ui_screen__freeform gstFFS_Door_TimeoutLockAndCAM =
{
   .pfnDraw = UI_FFS_Door_TimeoutLockAndCAM,
};
static struct st_ui_screen__freeform gstFFS_Door_RetiringCAM =
{
   .pfnDraw = UI_FFS_Door_RetiringCAM,
};
static struct st_ui_screen__freeform gstFFS_Door_FixedCAM =
{
   .pfnDraw = UI_FFS_Door_FixedCAM,
};
static struct st_ui_screen__freeform gstFFS_Door_Swing_GSW_Lock_Timeout =
{
   .pfnDraw = UI_FFS_Door_Swing_GSW_Lock_Timeout,
};
static struct st_ui_screen__freeform gstFFS_Door_Swing_Contacts_Timeout =
{
   .pfnDraw = UI_FFS_Door_Swing_Contacts_Timeout,
};
static struct st_ui_screen__freeform gstFFS_Door_Disable_Rear_DOB =
{
   .pfnDraw = UI_FFS_Door_Disable_Rear_DOB,
};

//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Misc_NumDoors =
{
   .psTitle = "Rear Doors",
   .pstUGS_Next = &gstUGS_Misc_NumberOfDoors,
};
static struct st_ui_menu_item gstMI_Control_Doors =
{
   .psTitle = "Control Doors",
   .pstUGS_Next = &gstUGS_Doors_Control,
};
static struct st_ui_menu_item gstMI_Door_DwellTimer =
{
   .psTitle = "Door Dwell Timer",
   .pstUGS_Next = &gstUGS_Door_DwellTimer,
};
static struct st_ui_menu_item gstMI_Door_StuckTimer =
{
   .psTitle = "Door Stuck Timer",
   .pstUGS_Next = &gstUGS_Door_StuckTimer,
};
static struct st_ui_menu_item gstMI_Door_NudgeTimer =
{
   .psTitle = "Door Nudge Timer",
   .pstUGS_Next = &gstUGS_Door_NudgeTimer,
};
static struct st_ui_menu_item gstMI_Door_DwellHallTimer =
{
   .psTitle = "Hall Dwell Timer",
   .pstUGS_Next = &gstUGS_Door_DwellHallTimer,
};
static struct st_ui_menu_item gstMI_Door_DwellADATimer =
{
   .psTitle = "ADA Dwell Timer",
   .pstUGS_Next = &gstUGS_Door_DwellADATimer,
};
static struct st_ui_menu_item gstMI_Door_PreOpeningDistance =
{
   .psTitle = "PreOpening Distance",
   .pstUGS_Next = &gstUGS_Door_PreOpeningDistance,
};
static struct st_ui_menu_item gstMI_Door_DC_On_Run =
{
   .psTitle = "DC On Run",
   .pstUGS_Next = &gstUGS_Door_DC_On_Run,
};
static struct st_ui_menu_item gstMI_Door_DC_On_Close =
{
   .psTitle = "DC On Close",
   .pstUGS_Next = &gstUGS_Door_DC_On_Close,
};
static struct st_ui_menu_item gstMI_Door_DO_On_Open =
{
   .psTitle = "DO On Open",
   .pstUGS_Next = &gstUGS_Door_DO_On_Open,
};
static struct st_ui_menu_item gstMI_Door_DisableOnCTStop =
{
   .psTitle = "Disable On CT Stop",
   .pstUGS_Next = &gstUGS_Door_DisableOnCTStop,
};
static struct st_ui_menu_item gstMI_Door_DisableOnHA =
{
   .psTitle = "Disable On HA",
   .pstUGS_Next = &gstUGS_Door_DisableOnHA,
};
static struct st_ui_menu_item gstMI_Door_IMotionDoors =
{
   .psTitle = "I-Motion Doors",
   .pstUGS_Next = &gstUGS_Door_IMotionDoors,
};
static struct st_ui_menu_item gstMI_Door_DwellHoldTimer =
{
   .psTitle = "Hold Dwell Timer",
   .pstUGS_Next = &gstUGS_Door_DwellHoldTimer,
};
static struct st_ui_menu_item gstMI_Door_SabbathDwell =
{
   .psTitle = "Sabbath Dwell Timer",
   .pstUGS_Next = &gstUGS_Door_SabbathDwell,
};
static struct st_ui_menu_item gstMI_Door_NoDemandDoorsOpen =
{
   .psTitle = "No Demand DO",
   .pstUGS_Next = &gstUGS_Door_NoDemandDoorsOpen,
};
static struct st_ui_menu_item gstMI_Door_JumperTimeout =
{
   .psTitle = "Jumper Timer",
   .pstUGS_Next = &gstUGS_Door_JumperTimeout,
};
static struct st_ui_menu_item gstMI_Door_JumperOnDOL=
{
   .psTitle = "Jumper On DOL",
   .pstUGS_Next = &gstUGS_Door_JumperOnDOL,
};
static struct st_ui_menu_item gstMI_Door_HourlyFaultLimit=
{
   .psTitle = "Hourly Fault Limit",
   .pstUGS_Next = &gstUGS_Door_HourlyFaultLimit,
};
static struct st_ui_menu_item gstMI_Door_NudgeBuzzerOnly =
{
   .psTitle = "Nudge - Buzzer Only",
   .pstUGS_Next = &gstUGS_Door_NudgeBuzzerOnly,
};
static struct st_ui_menu_item gstMI_Door_NudgeNoBuzzer =
{
   .psTitle = "Nudge - No Buzzer",
   .pstUGS_Next = &gstUGS_Door_NudgeNoBuzzer,
};
static struct st_ui_menu_item gstMI_Door_OpeningTime =
{
   .psTitle = "Opening Time",
   .pstUGS_Next = &gstUGS_Door_OpeningTime,
};
static struct st_ui_menu_item gstMI_Door_CheckTime =
{
   .psTitle = "Check Time",
   .pstUGS_Next = &gstUGS_Door_CheckTime,
};
static struct st_ui_menu_item gstMI_Door_LobbyDwellTimer =
{
   .psTitle = "Lobby Dwell Timer",
   .pstUGS_Next = &gstUGS_Door_LobbyDwellTimer,
};
static struct st_ui_menu_item gstMI_Door_DoorTypeSelect_F =
{
   .psTitle = "Door Type (F)",
   .pstUGS_Next = &gstUGS_Door_DoorTypeSelect_F,
};
static struct st_ui_menu_item gstMI_Door_DoorTypeSelect_R =
{
   .psTitle = "Door Type (R)",
   .pstUGS_Next = &gstUGS_Door_DoorTypeSelect_R,
};
static struct st_ui_menu_item gstMI_Door_SwingDoorBitmap_F =
{
   .psTitle = "Swing Openings (F)",
   .pstUGS_Next = &gstUGS_Door_SwingDoorBitmap_F,
};
static struct st_ui_menu_item gstMI_Door_SwingDoorBitmap_R =
{
   .psTitle = "Swing Openings (R)",
   .pstUGS_Next = &gstUGS_Door_SwingDoorBitmap_R,
};
static struct st_ui_menu_item gstMI_Door_TimeoutLockAndCAM =
{
   .psTitle = "Lock And CAM Timeout",
   .pstUGS_Next = &gstUGS_Door_TimeoutLockAndCAM,
};
static struct st_ui_menu_item gstMI_Door_RetiringCAM =
{
   .psTitle = "Retiring CAM",
   .pstUGS_Next = &gstUGS_Door_RetiringCAM,
};
static struct st_ui_menu_item gstMI_Door_FixedCAM =
{
   .psTitle = "Fixed CAM",
   .pstUGS_Next = &gstUGS_Door_FixedCAM,
};
static struct st_ui_menu_item gstMI_Door_Swing_GSW_Lock_Timeout =
{
   .psTitle = "Swing LCK GSW Timeout",
   .pstUGS_Next = &gstUGS_Door_Swing_GSW_Lock_Timeout,
};
static struct st_ui_menu_item gstMI_Door_Swing_Contacts_Timeout =
{
   .psTitle = "Swing Contacts Timeout",
   .pstUGS_Next = &gstUGS_Door_Swing_Contacts_Timeout,
};
static struct st_ui_menu_item gstMI_Door_Disable_Rear_DOB =
{
   .psTitle = "Disable DOB Rear",
   .pstUGS_Next = &gstUGS_Door_Disable_Rear_DOB,
};
static struct st_ui_menu_item * gastMenuItems_Doors[] =
{
   &gstMI_Control_Doors,
   &gstMI_Door_DwellTimer,
   &gstMI_Door_SabbathDwell,
   &gstMI_Door_DwellHallTimer,
   &gstMI_Door_DwellADATimer,
   &gstMI_Door_DwellHoldTimer,
   &gstMI_Door_LobbyDwellTimer,
   &gstMI_Door_StuckTimer,
   &gstMI_Door_NudgeTimer,
   &gstMI_Misc_NumDoors,
   &gstMI_Door_PreOpeningDistance,
   &gstMI_Door_DC_On_Run,
   &gstMI_Door_DC_On_Close,
   &gstMI_Door_DO_On_Open,
   &gstMI_Door_DisableOnCTStop,
   &gstMI_Door_DisableOnHA,
   &gstMI_Door_IMotionDoors,
   &gstMI_Door_NoDemandDoorsOpen,
   &gstMI_Door_JumperTimeout,
   &gstMI_Door_JumperOnDOL,
   &gstMI_Door_HourlyFaultLimit,
   &gstMI_Door_NudgeBuzzerOnly,
   &gstMI_Door_NudgeNoBuzzer,
   &gstMI_Door_OpeningTime,
   &gstMI_Door_CheckTime,
   &gstMI_Door_DoorTypeSelect_F,
   &gstMI_Door_DoorTypeSelect_R,
   &gstMI_Door_SwingDoorBitmap_F,
   &gstMI_Door_SwingDoorBitmap_R,
   &gstMI_Door_TimeoutLockAndCAM,
   &gstMI_Door_RetiringCAM,
   &gstMI_Door_FixedCAM,
   &gstMI_Door_Swing_GSW_Lock_Timeout,
   &gstMI_Door_Swing_Contacts_Timeout,
   &gstMI_Door_Disable_Rear_DOB,
};
static struct st_ui_screen__menu gstMenu_Setup_Doors =
{
   .psTitle = "Doors",
   .pastMenuItems = &gastMenuItems_Doors,
   .ucNumItems = sizeof(gastMenuItems_Doors) / sizeof(gastMenuItems_Doors[ 0 ]),
};
//---------------------------------------------------------------
static char const * const gpasUIDoorControl_Strings[ NUM_DOOR_UI_CTRL-1 ] =
{
   "Close",
   "Open",
   "Nudge",
};

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Misc_NumberOfDoors =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_NumberOfDoors,
};
struct st_ui_generic_screen gstUGS_Doors_Control =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFreeFormScreen_Doors_Control,
};
struct st_ui_generic_screen gstUGS_Door_DwellTimer =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_DwellTimer,
};
struct st_ui_generic_screen gstUGS_Door_StuckTimer =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_StuckTimer,
};
struct st_ui_generic_screen gstUGS_Door_NudgeTimer =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_NudgeTimer,
};
struct st_ui_generic_screen gstUGS_Door_DwellHallTimer =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_DwellHallTimer,
};
struct st_ui_generic_screen gstUGS_Door_DwellADATimer =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_DwellADATimer,
};
struct st_ui_generic_screen gstUGS_Door_PreOpeningDistance =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_PreOpeningDistance,
};
struct st_ui_generic_screen gstUGS_Door_DC_On_Run =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_DC_On_Run,
};
struct st_ui_generic_screen gstUGS_Door_DC_On_Close =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_DC_On_Close,
};
struct st_ui_generic_screen gstUGS_Door_DO_On_Open =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_DO_On_Open,
};
struct st_ui_generic_screen gstUGS_Door_DisableOnCTStop =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_DisableOnCTStop,
};
struct st_ui_generic_screen gstUGS_Door_DisableOnHA =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_DisableOnHA,
};
struct st_ui_generic_screen gstUGS_Door_IMotionDoors =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_IMotionDoors,
};
struct st_ui_generic_screen gstUGS_Door_DwellHoldTimer =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_DwellHoldTimer,
};
struct st_ui_generic_screen gstUGS_Door_SabbathDwell =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_SabbathDwell,
};
struct st_ui_generic_screen gstUGS_Door_NoDemandDoorsOpen =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_NoDemandDoorsOpen,
};
struct st_ui_generic_screen gstUGS_Door_JumperTimeout =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_JumperTimeout,
};
struct st_ui_generic_screen gstUGS_Door_JumperOnDOL =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_JumperOnDOL,
};
struct st_ui_generic_screen gstUGS_Door_HourlyFaultLimit =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_HourlyFaultLimit,
};
struct st_ui_generic_screen gstUGS_Door_NudgeBuzzerOnly =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_NudgeBuzzerOnly,
};
struct st_ui_generic_screen gstUGS_Door_NudgeNoBuzzer =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_NudgeNoBuzzer,
};
struct st_ui_generic_screen gstUGS_Door_OpeningTime =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_OpeningTime,
};
struct st_ui_generic_screen gstUGS_Door_CheckTime =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_CheckTime,
};
struct st_ui_generic_screen gstUGS_Door_LobbyDwellTimer =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_LobbyDwellTimer,
};
struct st_ui_generic_screen gstUGS_Door_DoorTypeSelect_F =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_DoorTypeSelect_F,
};
struct st_ui_generic_screen gstUGS_Door_DoorTypeSelect_R =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_DoorTypeSelect_R,
};
struct st_ui_generic_screen gstUGS_Door_SwingDoorBitmap_F =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_SwingDoorBitmap_F,
};
struct st_ui_generic_screen gstUGS_Door_SwingDoorBitmap_R =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_SwingDoorBitmap_R,
};
struct st_ui_generic_screen gstUGS_Door_TimeoutLockAndCAM =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_TimeoutLockAndCAM,
};
struct st_ui_generic_screen gstUGS_Door_RetiringCAM =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_RetiringCAM,
};
struct st_ui_generic_screen gstUGS_Door_FixedCAM =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_FixedCAM,
};
struct st_ui_generic_screen gstUGS_Door_Swing_GSW_Lock_Timeout =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_Swing_GSW_Lock_Timeout,
};
struct st_ui_generic_screen gstUGS_Door_Swing_Contacts_Timeout =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Door_Swing_Contacts_Timeout,
};
struct st_ui_generic_screen gstUGS_Door_Disable_Rear_DOB =
{
  .ucType = enUI_STYPE__FREEFORM,
  .pvReference = &gstFFS_Door_Disable_Rear_DOB,
};
//---------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_Setup_Doors =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Doors,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_NumberOfDoors( void )
{
   UI_FFS_Edit_Door_ParamBit( enPARAM1__NumDoors, "Rear Doors" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Doors_Control_Front( void )
{
   static uint8_t ucScroll = 1;
   static uint8_t bInvalidState = 0;
   static uint8_t ucDebounceCounter_30MS = 0;
   static uint8_t ucStarIndex;
   static enum en_door_ui_ctrl eUIDoorControl = DOOR_UI_CTRL__NONE;

   enum en_keypresses enKeypress = Button_GetKeypress();

   switch(enKeypress)
   {
      case enKEYPRESS_ENTER:
         bInvalidState = 1;
         if (   ( GetOperation_ClassOfOp() == CLASSOP__AUTO
               || GetOperation_ClassOfOp() == CLASSOP__MANUAL
                )
           &&  !GetMotion_RunFlag()
            )
         {
            if( ( GetInputValue(enIN_DZ_F) )
             || ( ( GetOperation_ClassOfOp() == CLASSOP__MANUAL ) && Param_ReadValue_1Bit(enPARAM1__EnableInspDoorOpenOutOfDZ) ) )
            {
               ucDebounceCounter_30MS = 10;//300ms
               bInvalidState = 0;
            }
            else
            {
               switch(ucScroll)
                {
                  case 1:
                     ucDebounceCounter_30MS = 10;//300ms
                     bInvalidState = 0;
//                     eUIDoorControl = DOOR_UI_CTRL__CLOSE_F;
                     break;
                default:
                   break;
                }
            }
         }
         break;
      case enKEYPRESS_LEFT:
         if(ucScroll > 1)
         {
            ucScroll--;
         }
         else
         {
            eUIDoorControl = DOOR_UI_CTRL__NONE;
            ucScroll = 1;
            bInvalidState = 0;
            PrevScreen();
         }
         break;
      case enKEYPRESS_RIGHT:
         // Needs to be changed
         if( ucScroll < 3 )
         {
            ucScroll++;
         }
         break;
      default:
         break;
   }
//   ucStarIndex = ((ucScroll < 4 ) ? (ucScroll*2) : (ucScroll*2 +4));
   ucStarIndex = 4 + (ucScroll - 1)*6;
   if(ucDebounceCounter_30MS)
   {
      // Needs to be changed, based on ucScroll position we will use switch statement to determine
      // which UI Control is being called
      switch(ucScroll)
      {
      case 1:
         eUIDoorControl = DOOR_UI_CTRL__CLOSE_F;
         break;
      case 2:
         eUIDoorControl = DOOR_UI_CTRL__OPEN_F;
         break;
      case 3:
         eUIDoorControl = DOOR_UI_CTRL__NUDGE_F;
         break;
      case 6:
      default:
         eUIDoorControl = DOOR_UI_CTRL__NONE;
         break;
      }
      //eUIDoorControl = (ucScroll) ? DOOR_UI_CTRL__OPEN_F:DOOR_UI_CTRL__CLOSE_F;
      ucDebounceCounter_30MS--;
   }
   else
   {
      eUIDoorControl = DOOR_UI_CTRL__NONE;
   }

   SetUIRequest_DoorControl(eUIDoorControl, GetSRU_Deployment());
   //----------------------------------------------------------------
   LCD_Char_Clear();

   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Door Control");

   LCD_Char_GotoXY( 14, 0 );

   uint8_t bIN_PHE_F = ( GetPHE2InputProgrammed_F2() ) ? ( GetInputValue(enIN_PHE_F) & GetInputValue(enIN_PHE_F2) ):GetInputValue(enIN_PHE_F);

   UI_PrintDoorSymbol( GetDoorState_Front(),
                       bIN_PHE_F,
                       GetInputValue(enIN_GSWF),
                       GetOutputValue(enOUT_DO_F),
                       GetOutputValue(enOUT_DC_F) );

   if(bInvalidState)
   {
      LCD_Char_GotoXY( 1, 2 );

      if ( GetOperation_ClassOfOp() == CLASSOP__UNKNOWN
        || GetOperation_ClassOfOp() == CLASSOP__SEMI_AUTO
         )
      {
         LCD_Char_WriteString("Invalid Mode");
         LCD_Char_GotoXY( 1, 3 );
         LCD_Char_WriteString("Of Operation");
      }
      else if( !( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R) )
            && !( ( GetOperation_ClassOfOp() == CLASSOP__MANUAL ) && Param_ReadValue_1Bit(enPARAM1__EnableInspDoorOpenOutOfDZ) ) )
      {
         LCD_Char_WriteString("Not in DZ");
      }
      else if(GetMotion_RunFlag() != 0)
      {
         LCD_Char_WriteString("Car Not Stopped");
      }
      else
      {
         LCD_Char_WriteString("Unknown");
      }
   }
   else
   {
      LCD_Char_GotoXY( 1, 1 );
      LCD_Char_WriteStringUpper( gpasUIDoorControl_Strings[0] );

      LCD_Char_GotoXY( 8, 1 );
      LCD_Char_WriteStringUpper( gpasUIDoorControl_Strings[1] );

      LCD_Char_GotoXY( 14, 1 );
      LCD_Char_WriteStringUpper( gpasUIDoorControl_Strings[2] );

      //LCD_Char_GotoXY( 2, 2 );
      //LCD_Char_WriteString("F  R  F  R  F  R");

      LCD_Char_GotoXY( ucStarIndex, 2 );
      LCD_Char_WriteString("*");
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Doors_Control_Both( void )
{
   if(Param_ReadValue_1Bit(enPARAM1__NumDoors))
   {
      static uint8_t ucScroll = 1;
      static uint8_t bInvalidState = 0;
      static uint8_t ucDebounceCounter_30MS = 0;
      static uint8_t ucStarIndex;
      static enum en_door_ui_ctrl eUIDoorControl = DOOR_UI_CTRL__NONE;

      enum en_keypresses enKeypress = Button_GetKeypress();

      switch(enKeypress)
      {
        case enKEYPRESS_ENTER:
           bInvalidState = 1;
           if (   ( GetOperation_ClassOfOp() == CLASSOP__AUTO
               || GetOperation_ClassOfOp() == CLASSOP__MANUAL
               )
            &&  !GetMotion_RunFlag()
            )
          {
             if( ( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R) )
              || ( ( GetOperation_ClassOfOp() == CLASSOP__MANUAL ) && Param_ReadValue_1Bit(enPARAM1__EnableInspDoorOpenOutOfDZ) ) )
             {
                ucDebounceCounter_30MS = 10;//300ms
                  bInvalidState = 0;
             }
             else
             {
                 switch(ucScroll)
                  {
                  case 1:
                     ucDebounceCounter_30MS = 10;//300ms
                     bInvalidState = 0;
   //                eUIDoorControl = DOOR_UI_CTRL__CLOSE_F;
                     break;
                  case 2:
   //                eUIDoorControl = DOOR_UI_CTRL__CLOSE_R;
                     ucDebounceCounter_30MS = 10;//300ms
                     bInvalidState = 0;
                     break;
                  default:
                     break;
                  }
             }
          }
          break;
        case enKEYPRESS_LEFT:
          if(ucScroll > 1)
          {
            ucScroll--;
          }
          else
          {
            eUIDoorControl = DOOR_UI_CTRL__NONE;
            ucScroll = 1;
            bInvalidState = 0;
            PrevScreen();
          }
          break;
        case enKEYPRESS_RIGHT:
           // Needs to be changed
          if( ucScroll < 6 )
          {
            ucScroll++;
          }
          break;
        default:
          break;
      }
   //   ucStarIndex = ((ucScroll < 4 ) ? (ucScroll*2) : (ucScroll*2 +4));
      ucStarIndex = ucScroll*3 - 1;
      if(ucDebounceCounter_30MS)
      {
         // Needs to be changed, based on ucScroll position we will use switch statement to determine
         // which UI Control is being called
         switch(ucScroll)
         {
         case 1:
            eUIDoorControl = DOOR_UI_CTRL__CLOSE_F;
            break;
         case 2:
            eUIDoorControl = DOOR_UI_CTRL__CLOSE_R;
            break;
         case 3:
            //eUIDoorControl = DOOR_UI_CTRL__CLOSE_B;
            eUIDoorControl = DOOR_UI_CTRL__OPEN_F;
            break;
         case 4:
   //         eUIDoorControl = DOOR_UI_CTRL__OPEN_F;
            eUIDoorControl = DOOR_UI_CTRL__OPEN_R;
            break;
         case 5:
            eUIDoorControl = DOOR_UI_CTRL__NUDGE_F;
            break;
         case 6:
            eUIDoorControl = DOOR_UI_CTRL__NUDGE_R;
            break;
         default:
            eUIDoorControl = DOOR_UI_CTRL__NONE;
            break;
         }
        //eUIDoorControl = (ucScroll) ? DOOR_UI_CTRL__OPEN_F:DOOR_UI_CTRL__CLOSE_F;
        ucDebounceCounter_30MS--;
      }
      else
      {
        eUIDoorControl = DOOR_UI_CTRL__NONE;
      }

      SetUIRequest_DoorControl(eUIDoorControl, GetSRU_Deployment());
      //----------------------------------------------------------------
      LCD_Char_Clear();

      LCD_Char_GotoXY( 0, 0 );
      LCD_Char_WriteStringUpper("F");

      LCD_Char_GotoXY( 1, 0 );
      uint8_t bIN_PHE_F = ( GetPHE2InputProgrammed_F2() ) ? ( GetInputValue(enIN_PHE_F) & GetInputValue(enIN_PHE_F2) ):GetInputValue(enIN_PHE_F);
      uint8_t bIN_PHE_R = ( GetPHE2InputProgrammed_R2() ) ? ( GetInputValue(enIN_PHE_R) & GetInputValue(enIN_PHE_R2) ):GetInputValue(enIN_PHE_R);

      UI_PrintDoorSymbol( GetDoorState_Front(),
                          bIN_PHE_F,
                          GetInputValue(enIN_GSWF),
                          GetOutputValue(enOUT_DO_F),
                          GetOutputValue(enOUT_DC_F) );

      LCD_Char_GotoXY( 8, 0 );
      LCD_Char_WriteStringUpper("Door");

      LCD_Char_GotoXY( 14, 0 );
      UI_PrintDoorSymbol( GetDoorState_Rear(),
                          bIN_PHE_R,
                          GetInputValue(enIN_GSWR),
                          GetOutputValue(enOUT_DO_R),
                          GetOutputValue(enOUT_DC_R) );

      LCD_Char_GotoXY( 19, 0 );
      LCD_Char_WriteStringUpper("R");

      if(bInvalidState)
      {
        LCD_Char_GotoXY( 1, 2 );

        if ( GetOperation_ClassOfOp() == CLASSOP__UNKNOWN
         || GetOperation_ClassOfOp() == CLASSOP__SEMI_AUTO
          )
        {
          LCD_Char_WriteString("Invalid Mode");
          LCD_Char_GotoXY( 1, 3 );
          LCD_Char_WriteString("Of Operation");
        }
        else if( !( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R) )
              && !( ( GetOperation_ClassOfOp() == CLASSOP__MANUAL ) && Param_ReadValue_1Bit(enPARAM1__EnableInspDoorOpenOutOfDZ) ) )
        {
          LCD_Char_WriteString("Not in DZ");
        }
        else if(GetMotion_RunFlag() != 0)
        {
          LCD_Char_WriteString("Car Not Stopped");
        }
        else
        {
          LCD_Char_WriteString("Unknown");
        }
      }
      else
      {
        LCD_Char_GotoXY( 1, 1 );
        LCD_Char_WriteStringUpper( gpasUIDoorControl_Strings[0] );

        LCD_Char_GotoXY( 8, 1 );
        LCD_Char_WriteStringUpper( gpasUIDoorControl_Strings[1] );

        LCD_Char_GotoXY( 14, 1 );
        LCD_Char_WriteStringUpper( gpasUIDoorControl_Strings[2] );

        LCD_Char_GotoXY( 2, 2 );
        LCD_Char_WriteString("F  R  F  R  F  R");

        LCD_Char_GotoXY( ucStarIndex, 3 );
        LCD_Char_WriteString("*");
      }
   }
   else
   {
      UI_FFS_Doors_Control_Front();
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Edit_Door_Timer( uint32_t ulParamNumber, char* psTitle )
{
   static struct st_param_edit_menu stParamEdit;

   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = ulParamNumber;
   stParamEdit.uwParamIndex_End   = stParamEdit.uwParamIndex_Start;
   stParamEdit.psTitle = psTitle;
   stParamEdit.psUnit = " sec";

   UI_ParamEditSceenTemplate( &stParamEdit );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Edit_Door_ParamBit( uint32_t ulParamNumber, char* psTitle )
{
   static struct st_param_edit_menu stParamEdit;

   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = ulParamNumber;
   stParamEdit.uwParamIndex_End = stParamEdit.uwParamIndex_Start;
   stParamEdit.psTitle = psTitle;

   UI_ParamEditSceenTemplate( &stParamEdit );
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_NudgeTimer( void )
{
   UI_FFS_Edit_Door_Timer( enPARAM8__DoorNudgeTime_1s, "Door Nudge Timer" );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_StuckTimer( void )
{
   UI_FFS_Edit_Door_Timer( enPARAM8__DoorStuckTime_1s, "Door Stuck Timer" );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_DwellTimer( void )
{
   UI_FFS_Edit_Door_Timer( enPARAM8__DoorDwellTime_1s, "Door Dwell Timer" );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_DwellHallTimer( void )
{
   UI_FFS_Edit_Door_Timer( enPARAM8__DoorDwellHallTime_1s, "Hall Dwell Timer" );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_DwellADATimer( void )
{
   UI_FFS_Edit_Door_Timer( enPARAM8__DoorDwellADATime_1s, "ADA Dwell Timer" );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_PreOpeningDistance( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__PreOpeningDistance;
   stParamEdit.uwParamIndex_End = stParamEdit.uwParamIndex_Start;
   stParamEdit.ulValue_Max = SIX_INCHES;
   stParamEdit.psTitle = "PreOpening Distance";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_DC_On_Run( void )
{
   UI_FFS_Edit_Door_ParamBit( enPARAM1__Door_DC_ON_RUN, "DC On Run" );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_DC_On_Close( void )
{
   UI_FFS_Edit_Door_ParamBit( enPARAM1__DC_On_DoorCloseState, "DC On Door Close" );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_DO_On_Open( void )
{
   UI_FFS_Edit_Door_ParamBit( enPARAM1__DO_On_DoorOpenState, "DO On Door Open" );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_DisableOnCTStop( void )
{
   UI_FFS_Edit_Door_ParamBit( enPARAM1__CT_Stop_SW_Kills_Doors, "Disable On CT Stop" );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_DisableOnHA( void )
{
   UI_FFS_Edit_Door_ParamBit( enPARAM1__DisableDoorsOnHA, "Disable On HA" );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_IMotionDoors( void )
{
   UI_FFS_Edit_Door_ParamBit( enPARAM1__EnableIMotionDoors, "Enable I-Motion Door" );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_DwellHoldTimer( void )
{
   UI_FFS_Edit_Door_Timer( enPARAM8__DoorDwellHoldTime_1s, "Hold Dwell Timer" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_SabbathDwell( void )
{
   UI_FFS_Edit_Door_Timer( enPARAM8__DoorDwellSabbathTimer_1s, "Sabbath Dwell Timer" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_NoDemandDoorsOpen( void )
{
   UI_FFS_Edit_Door_ParamBit( enPARAM1__NoDemandDoorsOpen, "No Demand Doors Open" );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_JumperTimeout( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__DoorJumperTimeout_100ms;
   stParamEdit.uwParamIndex_End = stParamEdit.uwParamIndex_Start;
   stParamEdit.psTitle = "Jumper Timeout";
   stParamEdit.psUnit = "00 msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_JumperOnDOL( void )
{
   UI_FFS_Edit_Door_ParamBit( enPARAM1__DoorLocksJumpedOnDOL, "Locks Jumped On DOL" );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_HourlyFaultLimit( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__DoorHourlyFaultLimit;
   stParamEdit.uwParamIndex_End = stParamEdit.uwParamIndex_Start;
   stParamEdit.psTitle = "Door Hourly Fault Limit";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_NudgeBuzzerOnly( void )
{
   UI_FFS_Edit_Door_ParamBit( enPARAM1__Doors_NudgeBuzzerOnly, "Nudge - Buzzer Only" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_NudgeNoBuzzer( void )
{
   UI_FFS_Edit_Door_ParamBit( enPARAM1__Doors_NudgeNoBuzzer, "Nudge - No Buzzer" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_OpeningTime( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__DoorOpeningTime_100ms;
   stParamEdit.uwParamIndex_End = enPARAM8__DoorOpeningTime_100ms;
   stParamEdit.psTitle = "Opening Time";
   stParamEdit.psUnit = " sec";
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_One;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_CheckTime( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__DoorCheckTime_100ms;
   stParamEdit.uwParamIndex_End = enPARAM8__DoorCheckTime_100ms;
   stParamEdit.psTitle = "Check Time";
   stParamEdit.psUnit = " sec";
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_One;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_LobbyDwellTimer( void )
{
   UI_FFS_Edit_Door_Timer( enPARAM8__LobbyDwellTime_1s, "Lobby Dwell Timer" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static char *(asDoorTypeArray[]) =
{
   "AUTOMATIC",
   "FREIGHT",
   "MANUAL",
   "SWING",
};
static void UI_FFS_Door_DoorTypeSelect_F( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__DoorTypeSelect_F;
   stParamEdit.uwParamIndex_End = stParamEdit.uwParamIndex_Start;
   stParamEdit.psTitle = "DOOR TYPE SELECT";
   stParamEdit.psUnit = "";
   stParamEdit.ucaPTRChoiceList = &asDoorTypeArray;
   stParamEdit.ulValue_Max = (sizeof(asDoorTypeArray)/sizeof(*asDoorTypeArray)) - 1;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
static void UI_FFS_Door_DoorTypeSelect_R( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__DoorTypeSelect_R;
   stParamEdit.uwParamIndex_End = stParamEdit.uwParamIndex_Start;
   stParamEdit.psTitle = "DOOR TYPE SELECT";
   stParamEdit.psUnit = "";
   stParamEdit.ucaPTRChoiceList = &asDoorTypeArray;
   stParamEdit.ulValue_Max = (sizeof(asDoorTypeArray)/sizeof(*asDoorTypeArray)) - 1;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_SwingDoorBitmap_F( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__SwingDoorOpening_F_0;
   stParamEdit.uwParamIndex_End = enPARAM16__SwingDoorOpening_F_5;
   stParamEdit.psTitle = "Swing Door Openings";
   stParamEdit.psUnit = "";
   stParamEdit.ucFieldSize_Value = 1;
   stParamEdit.ucFieldSize_Index = 2;
   UI_ParamEditScreenTemplate_EditByBit_PerFloor(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_SwingDoorBitmap_R( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = enPARAM16__SwingDoorOpening_R_0;
   stParamEdit.uwParamIndex_End = enPARAM16__SwingDoorOpening_R_5;
   stParamEdit.psTitle = "Swing Door Openings";
   stParamEdit.psUnit = "";
   stParamEdit.ucFieldSize_Value = 1;
   stParamEdit.ucFieldSize_Index = 2;
   UI_ParamEditScreenTemplate_EditByBit_PerFloor(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_TimeoutLockAndCAM( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__TimeoutLockAndCAM_100ms;
   stParamEdit.uwParamIndex_End = stParamEdit.uwParamIndex_Start;
   stParamEdit.psTitle = "Timeout Lock And CAM";
   stParamEdit.psUnit = "00 ms";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_RetiringCAM( void )
{
   UI_FFS_Edit_Door_ParamBit( enPARAM1__Door_RetiringCAM, "Retiring CAM" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_FixedCAM( void )
{
   UI_FFS_Edit_Door_ParamBit( enPARAM1__Door_FixedHallCAM, "Fixed CAM" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Door_Swing_GSW_Lock_Timeout( void )
{
   UI_FFS_Edit_Door_Timer( enPARAM8__Swing_GSW_Locks_Timeout_1s, "Swing Lock GSW Timeout" );
}

static void UI_FFS_Door_Swing_Contacts_Timeout( void )
{
   UI_FFS_Edit_Door_Timer( enPARAM8__Swing_Contacts_Timeout_1s, "Swing Contacts Timeout" );
}

static void UI_FFS_Door_Disable_Rear_DOB( void )
{
   UI_FFS_Edit_Door_ParamBit( enPARAM1__Door_FixedHallCAM, "Disable Rear DOB" );
}


/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
