/******************************************************************************
 *
 * @file     ui_ffs_floors.c
 * @brief    Setup floors UI pages
 * @version  V1.00
 * @date     13, Feb 2018
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>

 /*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_Setup_Floors_AccessCode_Floor( uint8_t bRear, uint32_t ulParamNumber );

static void UI_FFS_Setup_Floors_AccessCode_Floor_1F( void );
static void UI_FFS_Setup_Floors_AccessCode_Floor_2F( void );
static void UI_FFS_Setup_Floors_AccessCode_Floor_3F( void );
static void UI_FFS_Setup_Floors_AccessCode_Floor_4F( void );
static void UI_FFS_Setup_Floors_AccessCode_Floor_5F( void );
static void UI_FFS_Setup_Floors_AccessCode_Floor_6F( void );
static void UI_FFS_Setup_Floors_AccessCode_Floor_7F( void );
static void UI_FFS_Setup_Floors_AccessCode_Floor_8F( void );

static void UI_FFS_Setup_Floors_AccessCode_Floor_1R( void );
static void UI_FFS_Setup_Floors_AccessCode_Floor_2R( void );
static void UI_FFS_Setup_Floors_AccessCode_Floor_3R( void );
static void UI_FFS_Setup_Floors_AccessCode_Floor_4R( void );
static void UI_FFS_Setup_Floors_AccessCode_Floor_5R( void );
static void UI_FFS_Setup_Floors_AccessCode_Floor_6R( void );
static void UI_FFS_Setup_Floors_AccessCode_Floor_7R( void );
static void UI_FFS_Setup_Floors_AccessCode_Floor_8R( void );

static void UI_FFS_Setup_Floors_AccessCode_CCBTime( void );


static void ValidateParameter(struct st_param_edit_menu * pstParamEditMenu);

static enum en_field_action Edit_Save( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_param_edit_menu * pstParamEditMenu, struct FloorIncrement * pstFloor_Num );

static void SaveParameter( struct st_param_edit_menu * pstParamEditMenu, struct FloorIncrement * pstFloor_Num );


/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

struct AccessCode
{
    uint8_t ucCode1 : 4;
    uint8_t ucCode2 : 4;
    uint8_t ucCode3 : 4;
    uint8_t ucCode4 : 4;
};

typedef enum
{
    ACCESS_CODE_BUTTON__1F,
    ACCESS_CODE_BUTTON__2F,
    ACCESS_CODE_BUTTON__3F,
    ACCESS_CODE_BUTTON__4F,
    ACCESS_CODE_BUTTON__5F,
    ACCESS_CODE_BUTTON__6F,
    ACCESS_CODE_BUTTON__7F,
    ACCESS_CODE_BUTTON__8F,
    ACCESS_CODE_BUTTON__1R,
    ACCESS_CODE_BUTTON__2R,
    ACCESS_CODE_BUTTON__3R,
    ACCESS_CODE_BUTTON__4R,
    ACCESS_CODE_BUTTON__5R,
    ACCESS_CODE_BUTTON__6R,
    ACCESS_CODE_BUTTON__7R,
    ACCESS_CODE_BUTTON__8R,
    NUM_ACCESS_CODE_BUTTONS
} en_access_code_buttons;


/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Floor_1F =
{
   .pfnDraw = UI_FFS_Setup_Floors_AccessCode_Floor_1F,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Floor_2F =
{
   .pfnDraw = UI_FFS_Setup_Floors_AccessCode_Floor_2F,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Floor_3F =
{
   .pfnDraw = UI_FFS_Setup_Floors_AccessCode_Floor_3F,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Floor_4F =
{
   .pfnDraw = UI_FFS_Setup_Floors_AccessCode_Floor_4F,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Floor_5F =
{
   .pfnDraw = UI_FFS_Setup_Floors_AccessCode_Floor_5F,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Floor_6F =
{
   .pfnDraw = UI_FFS_Setup_Floors_AccessCode_Floor_6F,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Floor_7F =
{
   .pfnDraw = UI_FFS_Setup_Floors_AccessCode_Floor_7F,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Floor_8F =
{
   .pfnDraw = UI_FFS_Setup_Floors_AccessCode_Floor_8F,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Floor_1R =
{
   .pfnDraw = UI_FFS_Setup_Floors_AccessCode_Floor_1R,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Floor_2R =
{
   .pfnDraw = UI_FFS_Setup_Floors_AccessCode_Floor_2R,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Floor_3R =
{
   .pfnDraw = UI_FFS_Setup_Floors_AccessCode_Floor_3R,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Floor_4R =
{
   .pfnDraw = UI_FFS_Setup_Floors_AccessCode_Floor_4R,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Floor_5R =
{
   .pfnDraw = UI_FFS_Setup_Floors_AccessCode_Floor_5R,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Floor_6R =
{
   .pfnDraw = UI_FFS_Setup_Floors_AccessCode_Floor_6R,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Floor_7R =
{
   .pfnDraw = UI_FFS_Setup_Floors_AccessCode_Floor_7R,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_Floor_8R =
{
   .pfnDraw = UI_FFS_Setup_Floors_AccessCode_Floor_8R,
};
static struct st_ui_screen__freeform gstFFS_Setup_Floors_CCBTime =
{
   .pfnDraw = UI_FFS_Setup_Floors_AccessCode_CCBTime,
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Floors_AccessCode_Front =
{
   .psTitle = "Access Codes (F)",
   .pstUGS_Next = &gstUGS_Setup_Floors_AccessCode_Front,
};

static struct st_ui_menu_item gstMI_Floors_AccessCode_Rear =
{
   .psTitle = "Access Codes (R)",
   .pstUGS_Next = &gstUGS_Setup_Floors_AccessCode_Rear,
};
static struct st_ui_menu_item gstMI_Setup_Floors_CCBTime =
{
   .psTitle = "CCB Timer",
   .pstUGS_Next = &gstUGS_AccessCode_CCBTime,
};

static struct st_ui_menu_item * gastMenuItems_AccessCode[] =
{
   &gstMI_Floors_AccessCode_Front,
   &gstMI_Floors_AccessCode_Rear,
   &gstMI_Setup_Floors_CCBTime,
};
static struct st_ui_screen__menu gstMenu_AccessCode =
{
   .psTitle = "Access Code",
   .pastMenuItems = &gastMenuItems_AccessCode,
   .ucNumItems = sizeof(gastMenuItems_AccessCode) / sizeof(gastMenuItems_AccessCode[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Setup_Floors_Floor_1F =
{
   .psTitle = "Floor 1",
   .pstUGS_Next = &gstUGS_AccessCode_Floor_1F,
};
static struct st_ui_menu_item gstMI_Setup_Floors_Floor_2F =
{
   .psTitle = "Floor 2",
   .pstUGS_Next = &gstUGS_AccessCode_Floor_2F,
};
static struct st_ui_menu_item gstMI_Setup_Floors_Floor_3F =
{
   .psTitle = "Floor 3",
   .pstUGS_Next = &gstUGS_AccessCode_Floor_3F,
};
static struct st_ui_menu_item gstMI_Setup_Floors_Floor_4F =
{
   .psTitle = "Floor 4",
   .pstUGS_Next = &gstUGS_AccessCode_Floor_4F,
};
static struct st_ui_menu_item gstMI_Setup_Floors_Floor_5F =
{
   .psTitle = "Floor 5",
   .pstUGS_Next = &gstUGS_AccessCode_Floor_5F,
};
static struct st_ui_menu_item gstMI_Setup_Floors_Floor_6F =
{
   .psTitle = "Floor 6",
   .pstUGS_Next = &gstUGS_AccessCode_Floor_6F,
};
static struct st_ui_menu_item gstMI_Setup_Floors_Floor_7F =
{
   .psTitle = "Floor 7",
   .pstUGS_Next = &gstUGS_AccessCode_Floor_7F,
};
static struct st_ui_menu_item gstMI_Setup_Floors_Floor_8F =
{
   .psTitle = "Floor 8",
   .pstUGS_Next = &gstUGS_AccessCode_Floor_8F,
};
static struct st_ui_menu_item * gastMenuItems_AccessCode_Front[] =
{
   &gstMI_Setup_Floors_Floor_1F,
   &gstMI_Setup_Floors_Floor_2F,
   &gstMI_Setup_Floors_Floor_3F,
   &gstMI_Setup_Floors_Floor_4F,
   &gstMI_Setup_Floors_Floor_5F,
   &gstMI_Setup_Floors_Floor_6F,
   &gstMI_Setup_Floors_Floor_7F,
   &gstMI_Setup_Floors_Floor_8F,
};
static struct st_ui_screen__menu gstMenu_AccessCode_Front =
{
   .psTitle = "Access Codes (F)",
   .pastMenuItems = &gastMenuItems_AccessCode_Front,
   .ucNumItems = sizeof(gastMenuItems_AccessCode_Front) / sizeof(gastMenuItems_AccessCode_Front[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Setup_Floors_Floor_1R =
{
   .psTitle = "Floor 1",
   .pstUGS_Next = &gstUGS_AccessCode_Floor_1R,
};
static struct st_ui_menu_item gstMI_Setup_Floors_Floor_2R =
{
   .psTitle = "Floor 2",
   .pstUGS_Next = &gstUGS_AccessCode_Floor_2R,
};
static struct st_ui_menu_item gstMI_Setup_Floors_Floor_3R =
{
   .psTitle = "Floor 3",
   .pstUGS_Next = &gstUGS_AccessCode_Floor_3R,
};
static struct st_ui_menu_item gstMI_Setup_Floors_Floor_4R =
{
   .psTitle = "Floor 4",
   .pstUGS_Next = &gstUGS_AccessCode_Floor_4R,
};
static struct st_ui_menu_item gstMI_Setup_Floors_Floor_5R =
{
   .psTitle = "Floor 5",
   .pstUGS_Next = &gstUGS_AccessCode_Floor_5R,
};
static struct st_ui_menu_item gstMI_Setup_Floors_Floor_6R =
{
   .psTitle = "Floor 6",
   .pstUGS_Next = &gstUGS_AccessCode_Floor_6R,
};
static struct st_ui_menu_item gstMI_Setup_Floors_Floor_7R =
{
   .psTitle = "Floor 7",
   .pstUGS_Next = &gstUGS_AccessCode_Floor_7R,
};
static struct st_ui_menu_item gstMI_Setup_Floors_Floor_8R =
{
   .psTitle = "Floor 8",
   .pstUGS_Next = &gstUGS_AccessCode_Floor_8R,
};
static struct st_ui_menu_item * gastMenuItems_AccessCode_Rear[] =
{
   &gstMI_Setup_Floors_Floor_1R,
   &gstMI_Setup_Floors_Floor_2R,
   &gstMI_Setup_Floors_Floor_3R,
   &gstMI_Setup_Floors_Floor_4R,
   &gstMI_Setup_Floors_Floor_5R,
   &gstMI_Setup_Floors_Floor_6R,
   &gstMI_Setup_Floors_Floor_7R,
   &gstMI_Setup_Floors_Floor_8R,
};
static struct st_ui_screen__menu gstMenu_AccessCode_Rear =
{
   .psTitle = "Access Codes (R)",
   .pastMenuItems = &gastMenuItems_AccessCode_Rear,
   .ucNumItems = sizeof(gastMenuItems_AccessCode_Rear) / sizeof(gastMenuItems_AccessCode_Rear[ 0 ]),
};

static uint8_t bReturnToSaved;

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Setup_Floors_AccessCode =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_AccessCode,
};
struct st_ui_generic_screen gstUGS_Setup_Floors_AccessCode_Front =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_AccessCode_Front,
};

struct st_ui_generic_screen gstUGS_Setup_Floors_AccessCode_Rear =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_AccessCode_Rear,
};
//---------------------------------------------------------------
struct st_ui_generic_screen gstUGS_AccessCode_Floor_1F =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Floor_1F,
};
struct st_ui_generic_screen gstUGS_AccessCode_Floor_2F =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Floor_2F,
};
struct st_ui_generic_screen gstUGS_AccessCode_Floor_3F =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Floor_3F,
};
struct st_ui_generic_screen gstUGS_AccessCode_Floor_4F =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Floor_4F,
};
struct st_ui_generic_screen gstUGS_AccessCode_Floor_5F =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Floor_5F,
};
struct st_ui_generic_screen gstUGS_AccessCode_Floor_6F =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Floor_6F,
};
struct st_ui_generic_screen gstUGS_AccessCode_Floor_7F =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Floor_7F,
};
struct st_ui_generic_screen gstUGS_AccessCode_Floor_8F =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Floor_8F,
};
//---------------------------------------------------------------
struct st_ui_generic_screen gstUGS_AccessCode_Floor_1R =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Floor_1R,
};
struct st_ui_generic_screen gstUGS_AccessCode_Floor_2R =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Floor_2R,
};
struct st_ui_generic_screen gstUGS_AccessCode_Floor_3R =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Floor_3R,
};
struct st_ui_generic_screen gstUGS_AccessCode_Floor_4R =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Floor_4R,
};
struct st_ui_generic_screen gstUGS_AccessCode_Floor_5R =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Floor_5R,
};
struct st_ui_generic_screen gstUGS_AccessCode_Floor_6R =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Floor_6R,
};
struct st_ui_generic_screen gstUGS_AccessCode_Floor_7R =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Floor_7R,
};
struct st_ui_generic_screen gstUGS_AccessCode_Floor_8R =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_Floor_8R,
};

struct st_ui_generic_screen gstUGS_AccessCode_CCBTime =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Floors_CCBTime,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_Screen(struct st_param_edit_menu * pstParamEditMenu, uint8_t bRear, struct FloorIncrement * pstFloor_Num)
{
   LCD_Char_Clear();

   LCD_Char_GotoXY( 0, 0 );

   //------------------------------------------------------------------


   if(pstParamEditMenu->bSaving)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Saving");
   }
   else if(pstParamEditMenu->bInvalid)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Invalid");
   }
   else
   {
      if(bRear)
      {
         LCD_Char_WriteStringUpper( "Access Codes (R)" );
      }
      else
      {
         LCD_Char_WriteStringUpper( "Access Codes (F)" );
      }

      LCD_Char_GotoXY(0,2);
      uint16_t uwAllIncrement = 0;
      uwAllIncrement |= ( pstFloor_Num->ucIncrement1 << 12 );
      uwAllIncrement |= ( pstFloor_Num->ucIncrement2 << 8 );
      uwAllIncrement |= ( pstFloor_Num->ucIncrement3 << 4 );
      uwAllIncrement |= ( pstFloor_Num->ucIncrement4 );
      pstParamEditMenu->ulValue_Edited = uwAllIncrement;

      // The user should be able to edit 4 segments of values as described below.
      // Those values will be displayed as the PI label for that floor,
      // with a F or and R to the right of it to indicate if its is the front or rear button for that floor

      en_access_code_buttons eEditValue = pstFloor_Num->ucIncrement1; // where X is the value that changes when the user navigates up or down valid [0 to 15]
      uint8_t ucFloorIndex = eEditValue;
      uint8_t bRear = 0;
      uint8_t bWrongAccessCode = 0;
      if(eEditValue >= ACCESS_CODE_BUTTON__1R)
      {
         ucFloorIndex = eEditValue - ACCESS_CODE_BUTTON__1R;
         bRear = 1;
      }
      if( !GetFloorOpening_Rear(ucFloorIndex) && bRear )
      {
         LCD_Char_WriteString("N/A");
         bWrongAccessCode = 1;
      }
      else if( !GetFloorOpening_Front(ucFloorIndex) && !bRear )
      {
         LCD_Char_WriteString("N/A");
         bWrongAccessCode = 1;
      }
      else
      {
         LCD_Char_WriteString(Get_PI_Label(ucFloorIndex));
         if(bRear)
         {
            LCD_Char_WriteString("R");
         }
         else
         {
            LCD_Char_WriteString("F");
         }
      }

      LCD_Char_GotoXY(4,2);

      eEditValue = pstFloor_Num->ucIncrement2; // where X is the value that changes when the user navigates up or down valid [0 to 15]
      ucFloorIndex = eEditValue;
      bRear = 0;
      if(eEditValue >= ACCESS_CODE_BUTTON__1R)
      {
         ucFloorIndex = eEditValue - ACCESS_CODE_BUTTON__1R;
         bRear = 1;
      }
      if( !GetFloorOpening_Rear(ucFloorIndex) && bRear )
      {
         LCD_Char_WriteString("N/A");
         bWrongAccessCode = 1;
      }
      else if( !GetFloorOpening_Front(ucFloorIndex) && !bRear )
      {
         LCD_Char_WriteString("N/A");
         bWrongAccessCode = 1;
      }
      else
      {
         LCD_Char_WriteString(Get_PI_Label(ucFloorIndex));
         if(bRear)
         {
            LCD_Char_WriteString("R");
         }
         else
         {
            LCD_Char_WriteString("F");
         }
      }

      LCD_Char_GotoXY(8,2);

      eEditValue = pstFloor_Num->ucIncrement3; // where X is the value that changes when the user navigates up or down valid [0 to 15]
      ucFloorIndex = eEditValue;
      bRear = 0;
      if(eEditValue >= ACCESS_CODE_BUTTON__1R)
      {
         ucFloorIndex = eEditValue - ACCESS_CODE_BUTTON__1R;
         bRear = 1;
      }
      if( !GetFloorOpening_Rear(ucFloorIndex) && bRear )
      {
         LCD_Char_WriteString("N/A");
         bWrongAccessCode = 1;
      }
      else if( !GetFloorOpening_Front(ucFloorIndex) && !bRear )
      {
         LCD_Char_WriteString("N/A");
         bWrongAccessCode = 1;
      }
      else
      {
         LCD_Char_WriteString(Get_PI_Label(ucFloorIndex));
         if(bRear)
         {
            LCD_Char_WriteString("R");
         }
         else
         {
            LCD_Char_WriteString("F");
         }
      }

      LCD_Char_GotoXY(12,2);

      eEditValue = pstFloor_Num->ucIncrement4; // where X is the value that changes when the user navigates up or down valid [0 to 15]
      ucFloorIndex = eEditValue;
      bRear = 0;
      if(eEditValue >= ACCESS_CODE_BUTTON__1R)
      {
         ucFloorIndex = eEditValue - ACCESS_CODE_BUTTON__1R;
         bRear = 1;
      }
      if( !GetFloorOpening_Rear(ucFloorIndex) && bRear )
      {
         LCD_Char_WriteString("N/A");
         bWrongAccessCode = 1;
      }
      else if( !GetFloorOpening_Front(ucFloorIndex) && !bRear )
      {
         LCD_Char_WriteString("N/A");
         bWrongAccessCode = 1;
      }
      else
      {
         LCD_Char_WriteString(Get_PI_Label(ucFloorIndex));
         if(bRear)
         {
            LCD_Char_WriteString("R");
         }
         else
         {
            LCD_Char_WriteString("F");
         }
      }

      //------------------------------------------------------------------

      LCD_Char_GotoXY( pstParamEditMenu->ucCursorColumn, 3 );

      LCD_Char_WriteChar( '*' );

      //------------------------------------------------------------------
      if ( ( pstParamEditMenu->ulValue_Edited != pstParamEditMenu->ulValue_Saved ) && !bWrongAccessCode )
      {
         LCD_Char_GotoXY( 16, 1 );
         LCD_Char_WriteString( "Save" );

         LCD_Char_GotoXY( 19, 2 );
         LCD_Char_WriteChar( '|' );
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Value( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_param_edit_menu * pstParamEditMenu, struct FloorIncrement * pstFloor_Num )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 2;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = 14;
      }
   }
   else  // enACTION__EDIT
   {
      //------------------------------------------------------------------
      pstParamEditMenu->ucCursorColumn = ucCursorIndex;
      //------------------------------------------------------------------
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            if( (pstFloor_Num->ucIncrement1 < 16 ) && ( ucCursorIndex == 2 ) )
            {
               (pstFloor_Num->ucIncrement1)++;
            }
            else if( (pstFloor_Num->ucIncrement2 < 16 ) && ( ucCursorIndex == 6 ) )
            {
               (pstFloor_Num->ucIncrement2)++;
            }
            else if( (pstFloor_Num->ucIncrement3 < 16 ) && ( ucCursorIndex == 10 ) )
            {
               (pstFloor_Num->ucIncrement3)++;
            }
            else if( (pstFloor_Num->ucIncrement4 < 16 ) && ( ucCursorIndex == 14 ) )
            {
               (pstFloor_Num->ucIncrement4)++;
            }
            break;

         case enKEYPRESS_DOWN:
            if( pstFloor_Num->ucIncrement1 && ( ucCursorIndex == 2 ) )
            {
               (pstFloor_Num->ucIncrement1)--;
            }
            else if( pstFloor_Num->ucIncrement2 && ( ucCursorIndex == 6 ) )
            {
               (pstFloor_Num->ucIncrement2)--;
            }
            else if( pstFloor_Num->ucIncrement3 && ( ucCursorIndex == 10 ) )
            {
               (pstFloor_Num->ucIncrement3)--;
            }
            else if(pstFloor_Num->ucIncrement4 && ( ucCursorIndex == 14 ) )
            {
               (pstFloor_Num->ucIncrement4)--;
            }
            break;

         case enKEYPRESS_LEFT:
            if( ucCursorIndex == 2 )
            {
               enReturnValue = enACTION__EXIT_TO_LEFT;
            }
            else if( ucCursorIndex == 6 )
            {
               ucCursorIndex = 2;
            }
            else if( ucCursorIndex == 10 )
            {
               ucCursorIndex = 6;
            }
            else if( ucCursorIndex == 14 )
            {
               ucCursorIndex = 10;
            }
            break;

         case enKEYPRESS_RIGHT:
            if( ucCursorIndex == 2 )
            {
               ucCursorIndex = 6;
            }
            else if( ucCursorIndex == 6 )
            {
               ucCursorIndex = 10;
            }
            else if( ucCursorIndex == 10 )
            {
               ucCursorIndex = 14;
            }
            else if( ucCursorIndex == 14 )
            {
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;

         default:
            break;
      }
   }

   return enReturnValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Edit_Keycode_PI( struct st_param_edit_menu * pstParamEditMenu, uint8_t bRear, struct FloorIncrement * pstFloor_Num )
{
   ValidateParameter(pstParamEditMenu);
   if ( pstParamEditMenu->ucFieldIndex == enFIELD__NONE )
   {
      pstParamEditMenu->ulValue_Edited = pstParamEditMenu->ulValue_Saved;
      pstParamEditMenu->bSaving = 0;
      pstParamEditMenu->bInvalid = 0;
      pstParamEditMenu->ucFieldIndex = enFIELD__VALUE;

      Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu, pstFloor_Num );
   }
   else
   {
      enum en_field_action enFieldAction;

      enum en_keypresses enKeypress = Button_GetKeypress();
      //------------------------------------------------------------------
      switch ( pstParamEditMenu->ucFieldIndex )
      {
         case enFIELD__VALUE: //function

            enFieldAction = Edit_Value( enKeypress, enACTION__EDIT, pstParamEditMenu, pstFloor_Num );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               PrevScreen();
               pstParamEditMenu->ucFieldIndex = enFIELD__NONE;
               bReturnToSaved = 0;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Save( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu, pstFloor_Num );

               pstParamEditMenu->ucFieldIndex = enFIELD__SAVE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__SAVE:

            enFieldAction = Edit_Save( enKeypress, enACTION__EDIT, pstParamEditMenu, pstFloor_Num );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT, pstParamEditMenu, pstFloor_Num );

               pstParamEditMenu->ucFieldIndex = enFIELD__VALUE;
            }
            break;

         default:
            break;
      }
      Update_Screen(pstParamEditMenu, bRear, pstFloor_Num);
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_AccessCode_Floor( uint8_t bRear, uint32_t ulParamNumber )
{
   static struct st_param_edit_menu stParamEdit;
   static struct FloorIncrement stFloorAccessCode;

   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start   = ulParamNumber;
   stParamEdit.uwParamIndex_End     = ulParamNumber;
   stParamEdit.uwParamIndex_Current = ulParamNumber;

   if(!bReturnToSaved)
   {
      stParamEdit.ulValue_Saved = Param_ReadValue_16Bit( ulParamNumber );
      stFloorAccessCode.ucIncrement1 = (stParamEdit.ulValue_Saved >> 12) & 0xF;
      stFloorAccessCode.ucIncrement2 = (stParamEdit.ulValue_Saved >> 8 ) & 0xF;
      stFloorAccessCode.ucIncrement3 = (stParamEdit.ulValue_Saved >> 4 ) & 0xF;
      stFloorAccessCode.ucIncrement4 = (stParamEdit.ulValue_Saved      ) & 0xF;
      bReturnToSaved = 1;
   }

   Edit_Keycode_PI( &stParamEdit, bRear, &stFloorAccessCode );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_AccessCode_Floor_1F( void )
{
   UI_FFS_Setup_Floors_AccessCode_Floor( 0, enPARAM16__AccessCodeFloor_1F );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_AccessCode_Floor_2F( void )
{
   UI_FFS_Setup_Floors_AccessCode_Floor( 0, enPARAM16__AccessCodeFloor_2F );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_AccessCode_Floor_3F( void )
{
   UI_FFS_Setup_Floors_AccessCode_Floor( 0, enPARAM16__AccessCodeFloor_3F );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_AccessCode_Floor_4F( void )
{
   UI_FFS_Setup_Floors_AccessCode_Floor( 0, enPARAM16__AccessCodeFloor_4F );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_AccessCode_Floor_5F( void )
{
   UI_FFS_Setup_Floors_AccessCode_Floor( 0, enPARAM16__AccessCodeFloor_5F );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_AccessCode_Floor_6F( void )
{
   UI_FFS_Setup_Floors_AccessCode_Floor( 0, enPARAM16__AccessCodeFloor_6F );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_AccessCode_Floor_7F( void )
{
   UI_FFS_Setup_Floors_AccessCode_Floor( 0, enPARAM16__AccessCodeFloor_7F );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_AccessCode_Floor_8F( void )
{
   UI_FFS_Setup_Floors_AccessCode_Floor( 0, enPARAM16__AccessCodeFloor_8F );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_AccessCode_Floor_1R( void )
{
   UI_FFS_Setup_Floors_AccessCode_Floor( 1, enPARAM16__AccessCodeFloor_1R );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_AccessCode_Floor_2R( void )
{
   UI_FFS_Setup_Floors_AccessCode_Floor( 1, enPARAM16__AccessCodeFloor_2R );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_AccessCode_Floor_3R( void )
{
   UI_FFS_Setup_Floors_AccessCode_Floor( 1, enPARAM16__AccessCodeFloor_3R );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_AccessCode_Floor_4R( void )
{
   UI_FFS_Setup_Floors_AccessCode_Floor( 1, enPARAM16__AccessCodeFloor_4R );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_AccessCode_Floor_5R( void )
{
   UI_FFS_Setup_Floors_AccessCode_Floor( 1, enPARAM16__AccessCodeFloor_5R );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_AccessCode_Floor_6R( void )
{
   UI_FFS_Setup_Floors_AccessCode_Floor( 1, enPARAM16__AccessCodeFloor_6R );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_AccessCode_Floor_7R( void )
{
   UI_FFS_Setup_Floors_AccessCode_Floor( 1, enPARAM16__AccessCodeFloor_7R );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_AccessCode_Floor_8R( void )
{
   UI_FFS_Setup_Floors_AccessCode_Floor( 1, enPARAM16__AccessCodeFloor_8R );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Floors_AccessCode_CCBTime( void )
{
   static struct st_param_edit_menu stParamEdit;

   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__AccessCode_CCB_Time_1s;
   stParamEdit.uwParamIndex_End = enPARAM8__AccessCode_CCB_Time_1s;
   stParamEdit.psTitle = "CCB Timer";
   stParamEdit.ucFieldSize_Value = 3;
   stParamEdit.psUnit = " Sec";

   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ValidateParameter(struct st_param_edit_menu * pstParamEditMenu)
{
   switch(pstParamEditMenu->enParamType)
   {
      case enPARAM_BLOCK_TYPE__BIT:
         pstParamEditMenu->ulValue_Max = ( !pstParamEditMenu->ulValue_Max )
                                     ? 1 : pstParamEditMenu->ulValue_Max;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_1BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_1Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT8:
         pstParamEditMenu->ulValue_Max = ( !pstParamEditMenu->ulValue_Max )
                                       ? 0xFF : pstParamEditMenu->ulValue_Max;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_8BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_8Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT16:
         pstParamEditMenu->ulValue_Max = ( !pstParamEditMenu->ulValue_Max )
                                       ? 0xFFFF : pstParamEditMenu->ulValue_Max;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_16BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_16Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT24:
         pstParamEditMenu->ulValue_Max = ( !pstParamEditMenu->ulValue_Max )
                                       ? 0xFFFFFF : pstParamEditMenu->ulValue_Max;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_24BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_24Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT32:
         pstParamEditMenu->ulValue_Max = ( !pstParamEditMenu->ulValue_Max )
                                       ? 0xFFFFFFFF : pstParamEditMenu->ulValue_Max;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_32BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_32Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      default:
         pstParamEditMenu->bInvalid = 1;
         break;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Save( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_param_edit_menu * pstParamEditMenu, struct FloorIncrement * pstFloor_Num )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static enum en_save_state enSaveState = SAVE_STATE__SAVED;

   pstParamEditMenu->ucCursorColumn = 19;
   //------------------------------------------
   if ( enFieldAction == enACTION__EDIT )
   {
      if ( enKeypress == enKEYPRESS_ENTER )
      {
         enSaveState = SAVE_STATE__SAVING;
      }
      else if ( enKeypress == enKEYPRESS_LEFT )
      {
         enSaveState = SAVE_STATE__SAVED;
         enReturnValue = enACTION__EXIT_TO_LEFT;
      }
   }

   //--------------------------------
   switch(enSaveState)
   {
      case SAVE_STATE__CLEARING:
         pstParamEditMenu->bSaving = 1;
         if(!pstParamEditMenu->ulValue_Saved)
         {
            enSaveState = SAVE_STATE__SAVING;
         }
         break;
      case SAVE_STATE__SAVING:
         SaveParameter(pstParamEditMenu, pstFloor_Num);
         pstParamEditMenu->bSaving = 1;

         if(pstParamEditMenu->ulValue_Saved == pstParamEditMenu->ulValue_Edited)
         {
            enReturnValue = enACTION__EXIT_TO_LEFT;
            pstParamEditMenu->bSaving = 0;
            enSaveState = SAVE_STATE__SAVED;
         }
         break;
      case SAVE_STATE__SAVED:
         pstParamEditMenu->bSaving = 0;
         break;
      default:
         enSaveState = SAVE_STATE__SAVED;
         break;
   }
   return enReturnValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void SaveParameter( struct st_param_edit_menu * pstParamEditMenu, struct FloorIncrement * pstFloor_Num )
{
   uint16_t ucIndex = pstParamEditMenu->uwParamIndex_Current;

   switch(pstParamEditMenu->enParamType)
   {
      case enPARAM_BLOCK_TYPE__UINT32:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_32Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_32Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT24:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_24Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_24Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT16:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_16Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited )
         {
            Param_WriteValue_16Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT8:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_8Bit( ucIndex );

         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited )
         {
            Param_WriteValue_8Bit( ucIndex, pstParamEditMenu->ulValue_Edited);
         }
         break;
      default:
      case enPARAM_BLOCK_TYPE__BIT:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_1Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_1Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
