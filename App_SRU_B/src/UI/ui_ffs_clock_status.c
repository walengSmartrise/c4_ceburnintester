/******************************************************************************
 *
 * @file     ui_ffs_clock_status.c
 * @brief    Page for viewing date and time
 * @version  V1.00
 * @date     27, Sept 2018
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "GlobalData.h"
#include "rtc.h"
#include <math.h>
#include <time.h>
#include <stdint.h>
#include <string.h>

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_ClockStatus( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
//---------------------------------------------------------------

static struct st_ui_screen__freeform gstFFS_ClockStatus =
{
   .pfnDraw = UI_FFS_ClockStatus,
};


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_ClockStatus =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ClockStatus,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
static void Print_Screen( void )
{
   uint32_t ulTime = RTC_GetLocalTime();
   char* psTime = ctime(&ulTime);
   char* pchr = strtok(psTime, " \t\n");

   LCD_Char_Clear();
   LCD_Char_GotoXY( 7, 1 );
   // LCD_Char_WriteString(pchr);
   // LCD_Char_WriteChar(' ');

   for( uint8_t i = 1; i < 3; i++ )
   {
      pchr = strtok(NULL," \t\n");
      LCD_Char_WriteString(pchr);
      LCD_Char_WriteChar(' ');
   }

   LCD_Char_GotoXY( 6, 2 );
   pchr = strtok(NULL," \t\n");
   LCD_Char_WriteString(pchr);

   LCD_Char_GotoXY( 7, 0 );
   pchr = strtok(NULL," \t\n"); 
   LCD_Char_WriteChar(' '); 
   LCD_Char_WriteString(pchr);
   
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
#define MAX_CURSOR_X_POS      (2)
static void UI_FFS_ClockStatus( void )
{
   enum en_keypresses enKeypress = Button_GetKeypress();
   switch(enKeypress)
   {
      case enKEYPRESS_RIGHT:
         break;
      case enKEYPRESS_LEFT:
         PrevScreen();
         break;
      case enKEYPRESS_ENTER:
         break;
      default:
         break;
   }
   Print_Screen();
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
