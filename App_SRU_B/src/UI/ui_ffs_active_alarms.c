/******************************************************************************
 *
 * @file     ui_ffs_about.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <stdint.h>
#include <string.h>
#include <time.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_PopUp_Alarms( void );
static void UI_FFS_ActiveAlarms( void );
static void UI_FFS_Logged_Alarms( void );
static void UI_FFS_Alarms_ClearLog( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_ui_screen__freeform gstFreeFormScreen_PopUpAlarms =
{
   .pfnDraw = UI_FFS_PopUp_Alarms,
};
static struct st_ui_screen__freeform gstFFS_ActiveAlarms =
{
   .pfnDraw = UI_FFS_ActiveAlarms,
};
static struct st_ui_screen__freeform gstFreeFormScreen_LoggedAlarms =
{
   .pfnDraw = UI_FFS_Logged_Alarms,
};
static struct st_ui_screen__freeform gstFFS_Alarms_ClearLog =
{
   .pfnDraw = UI_FFS_Alarms_ClearLog,
};
//---------
static struct st_ui_menu_item gstMI_Active =
{
   .psTitle = "Active",
   .pstUGS_Next = &gstUGS_ActiveAlarms,
};
static struct st_ui_menu_item gstMI_Logged =
{
   .psTitle = "Logged",
   .pstUGS_Next = &gstUGS_LoggedAlarms,
};
static struct st_ui_menu_item gstMI_ClearLog =
{
   .psTitle = "Clear Log",
   .pstUGS_Next = &gstUGS_Alarms_ClearLog,
};
static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_Active,
   &gstMI_Logged,
   &gstMI_ClearLog,
};

static struct st_ui_screen__menu gstMenu =
{
   .psTitle = "Alarms",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};



static char const * const pasBoardStrings[NUM_ALARM_NODES] =
{
      "CPLD: ",
      "MRA: ",
      "MRB: ",
      "CTA: ",
      "CTB: ",
      "COPA: ",
      "COPB: ",
};


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/


struct st_ui_generic_screen gstUGS_PopUpAlarms =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFreeFormScreen_PopUpAlarms,
   .pstUGS_Previous = &gstUGS_HomeScreen,
};

struct st_ui_generic_screen gstUGS_ActiveAlarms =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ActiveAlarms,
};

struct st_ui_generic_screen gstUGS_LoggedAlarms =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFreeFormScreen_LoggedAlarms,
};
struct st_ui_generic_screen gstUGS_Alarms_ClearLog =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Alarms_ClearLog,
};

struct st_ui_generic_screen gstUGS_Menu_Alarms =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu,
};

//----------------------------------------------------------------------------


/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

#define MAX_SCROLL_X_POS      7
#define MAX_DETAILS_SCROLLY   (3)
#define MAX_VISIBLE_ACTIVE_ALARMS (16U)

#define UI_REQUEST_CLEAR_LOG     (0xFE)
#define TIME_TO_CLEAR_LOG_1MS    (5000)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Print_Details( en_alarm_nodes eNode, uint8_t ucScrollY )
{
   st_alarm_data stAlarmData;
   char * psTime;
   en_alarms eAlarmNum;
   int16_t wSpeed;
   uint32_t uiPosition;

   GetActiveAlarmData( &stAlarmData, eNode );
   eAlarmNum = stAlarmData.eAlarmNum;
   wSpeed = stAlarmData.wSpeed;
   uiPosition = stAlarmData.ulPosition;
   psTime = ctime(&stAlarmData.ulTimestamp);

   LCD_Char_Clear();
   LCD_Char_GotoXY(0,0);
   for(uint8_t ucRow = 0; ucRow < 4; ucRow++)
   {
      uint8_t ucIndex = ucRow + ucScrollY;
      switch( ucIndex )
      {
         case 0:
            PrintAlarmString(eAlarmNum);
            break;

         case 1:
            LCD_Char_WriteString("Num:");
            LCD_Char_WriteInteger_MinLength( eAlarmNum, 1);
            break;

         case 2:
            LCD_Char_WriteString("Time:");
            PrintMovingText(psTime);
            break;

         case 3:
            LCD_Char_WriteString("Spd: ");
            LCD_Char_WriteSignedInteger(wSpeed);
            break;

         case 4:
            LCD_Char_WriteString("Pos: ");
            LCD_WritePositionInFeet_Shortened(uiPosition);
            break;
         default:
            break;
      }
      LCD_Char_GoToStartOfNextRow();
      LCD_Char_AdvanceX(1);
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_PopUp_Alarms( void )
{
   uint16_t ucAlarm = gstAlarm.eAlarmNumber;
   uint8_t ucNode_Plus1 = gstAlarm.eActiveNode_Plus1;

   if( !ucNode_Plus1 )
   {
      PrevScreen();
   }
   else if(ucAlarm)
   {
      Print_Details(ucNode_Plus1 - 1, 0);
   }
   else
   {
      LCD_Char_Clear();
      LCD_Char_GotoXY( 0, 0 );
   }

   enum en_keypresses enKeypress = Button_GetKeypress();
   if ( enKeypress != enKEYPRESS_NONE )
   {
      PrevScreen();
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_ActiveAlarms( void )
{
   static uint8_t ucScrollY_Details; // Alarm Details Y position
   static uint8_t ucScrollY;// Alarm List Y position
   static uint8_t ucCursorY;
   static uint8_t ucScrollX;
   uint16_t gaucActiveAlarms[NUM_ALARM_NODES] = {0};
   uint8_t ucNumActive = NUM_ALARM_NODES;

   /* Check for active alarms*/
   for( uint8_t i = 0; i < NUM_ALARM_NODES; i++ )
   {
         gaucActiveAlarms[i] = gstAlarm.aeActiveAlarmNum[i];
   }

   if(ucScrollY+3 >= ucNumActive)
   {
      ucScrollY = (ucNumActive < 3) ? 0:ucNumActive - 3;
   }
   if(ucCursorY >= ucNumActive)
   {
      ucCursorY = ucNumActive - 1;
   }

   LCD_Char_Clear();
   LCD_Char_GotoXY(0,0);
   LCD_Char_WriteString("Active Alarms ");

   LCD_Char_WriteString("(");
   LCD_Char_WriteInteger_MinLength(ucNumActive,1);
   LCD_Char_WriteString(")");

   if(ucScrollX)//Details screen
   {
      en_alarm_nodes eAlarmNode;
      eAlarmNode = ucScrollY + ucCursorY;
      Print_Details( eAlarmNode, ucScrollY_Details );
   }
   else//Enterance screen
   {
      for( uint8_t i = 0; i < 3; i++ )
      {
         if(ucScrollY < ucNumActive)
         {
            LCD_Char_GotoXY( 1, 1+i );
            LCD_Char_WriteString( pasBoardStrings[i+ucScrollY] );
            PrintAlarmString(gaucActiveAlarms[i+ucScrollY]);
         }
      }
      LCD_Char_GotoXY(0,1+ucCursorY);
      LCD_Char_WriteString("*");
   }

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      if(ucScrollX)
      {
         ucScrollX--;
      }
      else
      {
         ucScrollY = 0;
         ucScrollY_Details = 0;
         PrevScreen();
      }
   }
   else if( enKeypress == enKEYPRESS_RIGHT )
   {
      if(ucScrollX < 1 && ucNumActive)
      {
         ucScrollX++;
      }
   }
   else if( enKeypress == enKEYPRESS_DOWN )
   {
      if(!ucScrollX)
      {
         if( ucCursorY == 2)
         {
            if(ucScrollY < ucNumActive - 3)
            {
               ucScrollY++;
            }
         }
         else
         {
            ucCursorY++;
         }
      }
      else
      {
         if(ucScrollY_Details < MAX_DETAILS_SCROLLY - 1)
         {
            ucScrollY_Details++;
         }
      }
   }
   else if( enKeypress == enKEYPRESS_UP )
   {
      if(!ucScrollX)
      {
         if( !ucCursorY )
         {
            if(ucScrollY)
            {
               ucScrollY--;
            }
         }
         else
         {
            ucCursorY--;
         }
      }
      else
      {
         if(ucScrollY_Details)
         {
            ucScrollY_Details--;
         }
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Print_LoggedAlarmDetails( uint8_t ucLogIndex, uint8_t ucScrollY )
{
   st_alarm_data stAlarm;
   char * psTime;
   en_faults eAlarmNum;
   int16_t wSpeed;
   uint32_t uiPosition;

   GetAlarmLogElement(&stAlarm, ucLogIndex+1);
   eAlarmNum = stAlarm.eAlarmNum;
   wSpeed = stAlarm.wSpeed;
   uiPosition = stAlarm.ulPosition;
   psTime = ctime(&stAlarm.ulTimestamp);

   LCD_Char_Clear();
   LCD_Char_GotoXY(0,0);
   for(uint8_t ucRow = 0; ucRow < 4; ucRow++)
   {
      uint8_t ucIndex = ucRow + ucScrollY;
      switch( ucIndex )
      {
         case 0:
            PrintAlarmString(eAlarmNum);
            break;

         case 1:
            LCD_Char_WriteString("Num:");
            LCD_Char_WriteInteger_MinLength( eAlarmNum, 1);
            break;

         case 2:
            LCD_Char_WriteString("Time:");
            PrintMovingText(psTime);
            break;

         case 3:
            LCD_Char_WriteString("Spd: ");
            LCD_Char_WriteSignedInteger(wSpeed);
            break;

         case 4:
            LCD_Char_WriteString("Pos: ");
            LCD_Char_WriteInteger_MinLength(uiPosition, 1);
            break;
         default:
            break;
      }
      LCD_Char_GoToStartOfNextRow();
      LCD_Char_AdvanceX(1);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void FetchAlarmLog()
{
   static uint8_t ucIndex;
   static uint32_t uiDelay_1ms;
   if( uiDelay_1ms > FAULT_LOG_REQ_UPDATE_RATE_1MS )
   {
      uiDelay_1ms = 0;
      if(++ucIndex >= NUM_FAULTLOG_ITEMS)
      {
         ucIndex = 0;
      }
   }
   else
   {
      uiDelay_1ms += MOD_RUN_PERIOD_UI_1MS;
   }
   SetUIRequest_FaultLog(NUM_FAULTLOG_ITEMS+ucIndex+1, GetSRU_Deployment());
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Logged_Alarms( void )
{
   static uint8_t ucScrollX;//details screen vs list
   static uint8_t ucScrollY;//up/dn on list
   static uint8_t ucScrollY_Details;//up/down on details screen
   static uint8_t ucCursorY;//ui pos indicator

   FetchAlarmLog();
   //--------------------------------
   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Alarm Log");
   if(ucScrollX)//Details screen
   {
      uint8_t ucLogIndex = ucScrollY + ucCursorY;
      Print_LoggedAlarmDetails( ucLogIndex, ucScrollY_Details );
   }
   else//Enterance screen
   {
      for(uint8_t i = 0; i < 3; i++)
      {
         uint8_t ucLogIndex = ucScrollY + i;
         uint16_t ucAlarm = GetAlarmLog_AlarmNumber( ucLogIndex + 1 );

         LCD_Char_GotoXY( 1, i+1 );
         LCD_Char_WriteInteger_MinLength(ucLogIndex+1,1);
         LCD_Char_WriteString(".");
         PrintAlarmString(ucAlarm);
      }
      LCD_Char_GotoXY(0,1+ucCursorY);
      LCD_Char_WriteString("*");
   }

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      if(ucScrollX)
      {
         ucScrollX--;
      }
      else
      {
         ucScrollY = 0;
         ucScrollY_Details = 0;
         SetUIRequest_FaultLog(0, GetSRU_Deployment());
         PrevScreen();
      }
   }
   else if( enKeypress == enKEYPRESS_RIGHT )
   {
      if(ucScrollX < 1)
      {
         ucScrollX++;
      }
   }
   else if( enKeypress == enKEYPRESS_DOWN )
   {
      if(!ucScrollX)
      {
         if( ucCursorY == 2)
         {
            if(ucScrollY < NUM_FAULTLOG_ITEMS - 3)
            {
               ucScrollY++;
            }
         }
         else
         {
            ucCursorY++;
         }
      }
      else
      {
         if(ucScrollY_Details < MAX_DETAILS_SCROLLY - 1)
         {
            ucScrollY_Details++;
         }
      }
   }
   else if( enKeypress == enKEYPRESS_UP )
   {
      if(!ucScrollX)
      {
         if( !ucCursorY )
         {
            if(ucScrollY)
            {
               ucScrollY--;
            }
         }
         else
         {
            ucCursorY--;
         }
      }
      else
      {
         if(ucScrollY_Details)
         {
            ucScrollY_Details--;
         }
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Alarms_ClearLog( void )
{
   static uint16_t uwClearingCounter;
   static uint8_t ucCursor = 0;
   static uint8_t bFinished = 1;
   const uint8_t ucFields = 2;

   enum en_keypresses enKeypress = Button_GetKeypress();
   switch(enKeypress)
   {
      case enKEYPRESS_RIGHT:
         if(ucCursor < ucFields-1)
         {
            ucCursor++;
         }
         break;
      case enKEYPRESS_LEFT:
         if( ucCursor )
         {
            ucCursor--;
         }
         else
         {
            SetUIRequest_FaultLog(0, GetSRU_Deployment());
            PrevScreen();
         }
         break;
      case enKEYPRESS_ENTER:
         if(ucCursor)
         {
            bFinished = 0;
            uwClearingCounter = 0;
         }
         else
         {
            SetUIRequest_FaultLog(0, GetSRU_Deployment());
            PrevScreen();
         }
         break;
      default:
         break;
   }

   if(bFinished)
   {
      LCD_Char_Clear();
      LCD_Char_GotoXY( 0, 0 );
      LCD_Char_WriteStringUpper("Clear Alarm Log?");
      LCD_Char_GotoXY(4,2);
      LCD_Char_WriteStringUpper("NO");
      LCD_Char_GotoXY(12,2);
      LCD_Char_WriteStringUpper("YES");
      if(!ucCursor)
      {
         LCD_Char_GotoXY(5,3);
      }
      else
      {
         LCD_Char_GotoXY(12,3);
      }
      LCD_Char_WriteString("*");
   }
   else
   {
      LCD_Char_Clear();
      LCD_Char_GotoXY(4,2);

      LCD_Char_WriteStringUpper("Clearing");

      if(uwClearingCounter < TIME_TO_CLEAR_LOG_1MS)
      {
         SetUIRequest_FaultLog(UI_REQUEST_CLEAR_LOG, GetSRU_Deployment());
         uwClearingCounter += MOD_RUN_PERIOD_UI_1MS;
      }
      else
      {
         uwClearingCounter = 0;
         bFinished = 1;
         ucCursor = 0;
         SetUIRequest_FaultLog(0, GetSRU_Deployment());
         PrevScreen();
      }
   }
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
