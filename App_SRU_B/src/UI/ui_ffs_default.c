/******************************************************************************
 *
 * @file     ui_ffs_misc.c
 * @brief    Setup Misc UI pages
 * @version  V1.00
 * @date     13, Feb 2018
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>
#include "param.h"
#include "config_System.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Default_All( void );
static void UI_FFS_Default_Floors( void );
static void UI_FFS_Default_SCurve( void );
static void UI_FFS_Default_RunTimers( void );
static void UI_FFS_Default_IO( void );
static void UI_FFS_Default_Factory( void );

static void UI_FFS_Default_FRAM( void );
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_Default_All =
{
   .pfnDraw = UI_FFS_Default_All,
};
static struct st_ui_screen__freeform gstFFS_Default_Floors =
{
   .pfnDraw = UI_FFS_Default_Floors,
};
static struct st_ui_screen__freeform gstFFS_Default_SCurve =
{
   .pfnDraw = UI_FFS_Default_SCurve,
};
static struct st_ui_screen__freeform gstFFS_Default_RunTimers =
{
   .pfnDraw = UI_FFS_Default_RunTimers,
};
static struct st_ui_screen__freeform gstFFS_Default_IO =
{
   .pfnDraw = UI_FFS_Default_IO,
};
static struct st_ui_screen__freeform gstFFS_Default_Factory =
{
   .pfnDraw = UI_FFS_Default_Factory,
};
static struct st_ui_screen__freeform gstFFS_Default_FRAM =
{
   .pfnDraw = UI_FFS_Default_FRAM,
};

//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Default_All =
{
   .psTitle = "Default Other",
   .pstUGS_Next = &gstUGS_Default_Other,
};
static struct st_ui_menu_item gstMI_Default_Floors =
{
   .psTitle = "Default Floors",
   .pstUGS_Next = &gstUGS_Default_Floors,
};
static struct st_ui_menu_item gstMI_Default_SCurve =
{
   .psTitle = "Default S-Curve",
   .pstUGS_Next = &gstUGS_Default_SCurve,
};
static struct st_ui_menu_item gstMI_Default_RunTimers =
{
   .psTitle = "Default Run Timers",
   .pstUGS_Next = &gstUGS_Default_RunTimers,
};
static struct st_ui_menu_item gstMI_Default_IO =
{
   .psTitle = "Default I/O",
   .pstUGS_Next = &gstUGS_Default_IO,
};
static struct st_ui_menu_item gstMI_Default_Factory =
{
   .psTitle = "Default Factory",
   .pstUGS_Next = &gstUGS_Default_Factory,
};
static struct st_ui_menu_item gstMI_Default_FRAM =
{
   .psTitle = "Default FRAM",
   .pstUGS_Next = &gstUGS_Default_FRAM,
};
static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_Default_Floors,
   &gstMI_Default_SCurve,
   &gstMI_Default_RunTimers,
   &gstMI_Default_IO,
   &gstMI_Default_Factory,
   &gstMI_Default_FRAM,
   &gstMI_Default_All,
};
static struct st_ui_screen__menu gstMenu =
{
   .psTitle = "Default",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};
//-----------------------------------------------------
static uint16_t uwDefaultCountdown_30ms;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Default_Other =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Default_All,
};
struct st_ui_generic_screen gstUGS_Default_Floors =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Default_Floors,
};
struct st_ui_generic_screen gstUGS_Default_SCurve =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Default_SCurve,
};
struct st_ui_generic_screen gstUGS_Default_RunTimers =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Default_RunTimers,
};
struct st_ui_generic_screen gstUGS_Default_IO =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Default_IO,
};
struct st_ui_generic_screen gstUGS_Default_Factory =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Default_Factory,
};
struct st_ui_generic_screen gstUGS_Default_FRAM =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Default_FRAM,
};
//-----------------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_Default =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define DEFAULTING_IN_PROGRESS_DISPLAY_TIME_30MS   (2000)
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void Update_UIRequest_DefaultCommand( void )
{
   if(GetUIRequest_DefaultCommand())
   {
      if(uwDefaultCountdown_30ms)
      {
         uwDefaultCountdown_30ms--;
      }
      else
      {
         SetUIRequest_DefaultCommand( DEFAULT_CMD__NONE );
      }
   }
   else
   {
      uwDefaultCountdown_30ms = 0;
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_DefaultTemplate( enum_default_cmd eCommand )
{
   static uint8_t ucCursor = 0;
   const uint8_t ucFields = 2;
   enum en_keypresses enKeypress = Button_GetKeypress();

   switch(enKeypress)
   {
      case enKEYPRESS_RIGHT:
         if(ucCursor < ucFields-1)
         {
            ucCursor++;
         }
         break;
      case enKEYPRESS_LEFT:
         if( ucCursor )
         {
            ucCursor--;
         }
         else
         {
            SetUIRequest_DefaultCommand( DEFAULT_CMD__NONE );
            PrevScreen();
         }
         break;
      case enKEYPRESS_ENTER:
         if(ucCursor)
         {
            uwDefaultCountdown_30ms = DEFAULTING_IN_PROGRESS_DISPLAY_TIME_30MS;
            SetUIRequest_DefaultCommand( eCommand );
         }
         else
         {
            SetUIRequest_DefaultCommand( DEFAULT_CMD__NONE );
            PrevScreen();
         }
         break;
      default:
         break;
   }

   if( !GetUIRequest_DefaultCommand() )
   {
      LCD_Char_Clear();
      LCD_Char_GotoXY( 0, 0 );
      LCD_Char_WriteString("DEFAULT ");
      switch(eCommand)
      {
         case DEFAULT_CMD__FLOORS: LCD_Char_WriteString("FLOORS"); break;
         case DEFAULT_CMD__SCURVE: LCD_Char_WriteString("S-CURVE"); break;
         case DEFAULT_CMD__RUN_TIMERS: LCD_Char_WriteString("RUN TIMERS"); break;
         case DEFAULT_CMD__IO: LCD_Char_WriteString("I/O"); break;
         case DEFAULT_CMD__GRP6: LCD_Char_WriteString("GRP 6"); break;
         case DEFAULT_CMD__GRP7: LCD_Char_WriteString("GRP 7"); break;
         case DEFAULT_CMD__GRP8: LCD_Char_WriteString("GRP 8"); break;
         case DEFAULT_CMD__FACTORY: LCD_Char_WriteString("FACTORY"); break;
         default: LCD_Char_WriteString("Other"); break;
      }
      LCD_Char_WriteString("?");
      LCD_Char_GotoXY(4,2);
      LCD_Char_WriteStringUpper("NO");
      LCD_Char_GotoXY(12,2);
      LCD_Char_WriteStringUpper("YES");
      if(!ucCursor)
      {
         LCD_Char_GotoXY(5,3);
      }
      else
      {
         LCD_Char_GotoXY(12,3);
      }
      LCD_Char_WriteString("*");
   }
   // If the state is not 0 then system is being updated
   else if( uwDefaultCountdown_30ms
       && ( ( GetDefaultAllState_MRA() >= DEFAULT_ALL_ONE_BIT ) && ( GetDefaultAllState_MRA() <= DEFAULT_ALL_THIRTYTWO_BIT ) ) )
   {
      LCD_Char_Clear();
      LCD_Char_GotoXY( 0, 0 );
      LCD_Char_WriteStringUpper("Defaulting Params");

      LCD_Char_GotoXY(2,2);

      switch(GetDefaultAllState_MRA() )
      {
          case DEFAULT_ALL_ONE_BIT:
             LCD_Char_WriteStringUpper("Defaulting 1 Bits");
             break;
          case DEFAULT_ALL_EIGHT_BIT:
             LCD_Char_WriteStringUpper("Defaulting 8 Bits");
             break;
          case DEFAULT_ALL_SIXTEEN_BIT:
             LCD_Char_WriteStringUpper("Defaulting 16 Bits");
             break;
          case DEFAULT_ALL_TWENTYFOUR_BIT:
             LCD_Char_WriteStringUpper("Defaulting 24 Bits");
             break;
          case DEFAULT_ALL_THIRTYTWO_BIT:
             LCD_Char_WriteStringUpper("Defaulting 32 Bits");
             break;
          default:
             LCD_Char_WriteStringUpper("Defaulting...");
             break;
      }
   }
   else if( GetDefaultAllState_MRA() == DEFAULT_ALL_IDLE )
   {
      ucCursor = 0;
      LCD_Char_GotoXY(0,2);
      LCD_Char_WriteStringUpper("Parameters Defaulted");
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Default_All( void )
{
   UI_DefaultTemplate(DEFAULT_CMD__ALL);
}
static void UI_FFS_Default_Floors( void )
{
   UI_DefaultTemplate(DEFAULT_CMD__FLOORS);
}
static void UI_FFS_Default_SCurve( void )
{
   UI_DefaultTemplate(DEFAULT_CMD__SCURVE);
}
static void UI_FFS_Default_RunTimers( void )
{
   UI_DefaultTemplate(DEFAULT_CMD__RUN_TIMERS);
}
static void UI_FFS_Default_IO( void )
{
   UI_DefaultTemplate(DEFAULT_CMD__IO);
}
static void UI_FFS_Default_Factory( void )
{
   UI_DefaultTemplate(DEFAULT_CMD__FACTORY);
}
static void UI_FFS_Default_FRAM( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__DefaultFRAM;
   stParamEdit.uwParamIndex_End = enPARAM1__DefaultFRAM;
   stParamEdit.psTitle = "Default FRAM";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
