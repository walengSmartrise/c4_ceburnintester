
/******************************************************************************
 *
 * @file     ui_menu_io.c
 * @brief    I/O Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "GlobalData.h"
#include "fram_data.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_EmergencyBitmap( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
//---------------------------------------------------------------

static struct st_ui_screen__freeform gstFFS_EmergencyBitmap =
{
   .pfnDraw = UI_FFS_EmergencyBitmap,
};

static char const * const pasEmergencyBFStrings[Num_EmergenyBF] =
{
"FireI_RecallToAlt",
"FireI_FlashHat",
"FireI_ArmReset",
"FireI_Active",
"FireII_Active",
"CW_Recall_Done",
"CW_Derail_InProg",
"EQ_Seismic_InProg",
"EMS1_Active",//EmergencyBF_EMS_Phase1_Active
"EMS2_Active",//EmergencyBF_EMS_Phase2_Active
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_EmergencyBitmap =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_EmergencyBitmap,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
static void Print_Status(uint8_t ucCursor)
{
   uint8_t ucIndex;

   for(ucIndex = 0; ucIndex < 3; ucIndex++)
   {
      uint8_t ucBitPos = ucCursor + ucIndex;
      if(ucBitPos < 16)//max bitmap size
      {
         LCD_Char_GotoXY( 0, 1+ucIndex );
         LCD_Char_WriteString("[");

         uint8_t bStatus = GetEmergencyBit((enum en_emergency_bitmap)ucBitPos);
         const char * ps = (bStatus) ? "X":" ";
         LCD_Char_WriteString(ps);
         LCD_Char_WriteString("] ");
         ps = pasEmergencyBFStrings[ucBitPos];
         LCD_Char_WriteString(ps);
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_EmergencyBitmap( void )
{
   static uint8_t ucCursorY = 0;

   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Emergency Status");

   Print_Status(ucCursorY);

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      if(ucCursorY < Num_EmergenyBF-3)//Minus cursor scroll+1
      {
         ucCursorY++;
      }
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
