/******************************************************************************
 *
 * @file     mod_lcd.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     22, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "buttons.h"
#include "lcd.h"

#include "sru_b.h"
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_LCD =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

enum ui_char_lcd_state
{
   LCD_CHAR_STATE__INIT,
   LCD_CHAR_STATE__DISPLAY_ON,
   LCD_CHAR_STATE__SET_MODE,
   LCD_CHAR_STATE__CLEAR_DISPLAY,
   LCD_CHAR_STATE__UPDATING,

   NUM_LCD_CHAR_STATE
};

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static enum ui_char_lcd_state geLCD_Char_State;

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   geLCD_Char_State = LCD_CHAR_STATE__DISPLAY_ON;

   pstThisModule->uwInitialDelay_1ms = 500;
   pstThisModule->uwRunPeriod_1ms = 1;  // run as often as possible

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   switch ( geLCD_Char_State )
   {
      case LCD_CHAR_STATE__DISPLAY_ON:
         LCD_Write( 0, 0x0C );

         geLCD_Char_State = LCD_CHAR_STATE__SET_MODE;
         break;

      case LCD_CHAR_STATE__SET_MODE:
         LCD_Write( 0, 0x38 );

         geLCD_Char_State = LCD_CHAR_STATE__CLEAR_DISPLAY;
         break;

      case LCD_CHAR_STATE__CLEAR_DISPLAY:
         LCD_Write( 0, 0x01 );

         geLCD_Char_State = LCD_CHAR_STATE__UPDATING;
         break;

      case LCD_CHAR_STATE__UPDATING:
         LCD_Char_Update();
         break;

      default: // should never be here
         geLCD_Char_State = LCD_CHAR_STATE__DISPLAY_ON;
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
