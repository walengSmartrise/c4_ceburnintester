/******************************************************************************
 *
 * @file     ui_ffs_earthquake.c
 * @brief    Earthquake UI pages
 * @version  V1.00
 * @date     13, Feb 2018
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_EQ_Enable( void );
static void UI_FFS_EQ_CounterweightPos( void );
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_EQ_Enable =
{
   .pfnDraw = UI_FFS_EQ_Enable ,
};
static struct st_ui_screen__freeform gstFFS_EQ_CounterweightPos =
{
   .pfnDraw = UI_FFS_EQ_CounterweightPos,
};
//----------------------------------------------------------
static struct st_ui_menu_item gstMI_EQ_Enable =
{
   .psTitle = "Enable EQ",
   .pstUGS_Next = &gstUGS_EQ_Enable,
};
static struct st_ui_menu_item gstMI_EQ_CounterweightPos =
{
   .psTitle = "Set CW Pos",
   .pstUGS_Next = &gstUGS_EQ_CounterweightPos,
};
static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_EQ_Enable,
   &gstMI_EQ_CounterweightPos,
};
static struct st_ui_screen__menu gstMenu_Setup_Earthquake =
{
   .psTitle = "Earthquake",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_EQ_Enable =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_EQ_Enable,
};
struct st_ui_generic_screen gstUGS_EQ_CounterweightPos =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_EQ_CounterweightPos,
};

struct st_ui_generic_screen gstUGS_Menu_Setup_EQ =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_Earthquake,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MAX_CURSOR_X_POS (1)
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_EQ_Enable( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__EnableEarthQuake;
   stParamEdit.uwParamIndex_End = enPARAM1__EnableEarthQuake;
   stParamEdit.psTitle = "Enable EQ";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

|01234567890123456789|

|Save CW Position?   |
|No Yes    999'11.99"|
|*                   |
|Stored:   999'11.99"|

 *----------------------------------------------------------------------------*/
static void Print_Screen(uint8_t ucCursor)
{
   LCD_Char_Clear();

   /* Line 1 */
   LCD_Char_GotoXY(0, 0);
   LCD_Char_WriteString("Save CW Position?");

   /* Line 2 */
   LCD_Char_GotoXY(0, 1);
   LCD_Char_WriteString("No Yes    ");
   LCD_WritePositionInFeet(GetPosition_PositionCount());

   /* Line 3 */
   uint8_t ucPosition = (ucCursor) ? 3:0;
   LCD_Char_GotoXY(ucPosition, 2);
   LCD_Char_WriteString("*");

   /* Line 4 */
   LCD_Char_GotoXY(0, 3);
   LCD_Char_WriteString("Stored:   ");
   LCD_WritePositionInFeet(Param_ReadValue_24Bit(enPARAM24__COUNTER_WEIGHT_MID_POINT));
}
static void UI_FFS_EQ_CounterweightPos( void )
{
   static uint8_t ucCursorX;
   enum en_keypresses enKeypress = Button_GetKeypress();
   if ( enKeypress == enKEYPRESS_LEFT )
   {
      if(!ucCursorX)
      {
         PrevScreen();
      }
      else
      {
         ucCursorX--;
      }
   }
   else if ( enKeypress == enKEYPRESS_RIGHT )
   {
      if( ucCursorX < MAX_CURSOR_X_POS )
      {
         ucCursorX++;
      }
   }
   else if ( enKeypress == enKEYPRESS_ENTER ) // Save
   {
      if( ucCursorX == MAX_CURSOR_X_POS )
      {
         uint32_t uiValue = GetPosition_PositionCount();
         Param_WriteValue_24Bit(enPARAM24__COUNTER_WEIGHT_MID_POINT, uiValue);
      }
   }

   Print_Screen(ucCursorX);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
