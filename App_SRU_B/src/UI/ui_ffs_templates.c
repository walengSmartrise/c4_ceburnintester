/******************************************************************************
 *
 * @file     ui_ffs_templates.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>
#include <string.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t gbEnteredMenu;
static uint8_t hours;
static uint8_t minutes;

/* Bitmask parameter edit variables */
static uint8_t ucCurrentBitIndex;
static uint8_t ucBitsPerBitmap;
static int32_t iMaxBitIndex;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define START_CURSOR_POS_VALUE (7U)
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
static void PrintArrayString(  struct st_param_edit_menu * pstParamEditMenu  )
{
   static uint8_t ucIndex;
   static uint8_t ucShiftDelay_30ms;
   if( pstParamEditMenu->ucaPTRChoiceList != 0 )
   {
      uint8_t ucSizeOfArray = pstParamEditMenu->ulValue_Max+1;
      uint8_t ucArrayIndex = (pstParamEditMenu->ucFieldSize_Index)
                           ? (pstParamEditMenu->uwParamIndex_Current - pstParamEditMenu->uwParamIndex_Start)
                           : (pstParamEditMenu->ulValue_Edited);
      uint8_t ucLength;
      if(ucArrayIndex< ucSizeOfArray)
      {
         // print the characters
         ucLength = strlen((*pstParamEditMenu->ucaPTRChoiceList)[ucArrayIndex]);
         if(ucLength > 10)
          {
            if(++ucShiftDelay_30ms >= 20)
            {
              ucShiftDelay_30ms = 0;
              if(++ucIndex >= (ucLength - 10))
              {
                ucIndex = 0;
              }
            }
          }else
          {
             ucIndex = 0;
          }

         LCD_Char_GotoXY( 0, 1 );
         LCD_Char_WriteStringUpper((*pstParamEditMenu->ucaPTRChoiceList)[ucArrayIndex] + ucIndex);
      }
   }
}
#if 0
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void PrintMovingTitle(struct st_param_edit_menu * pstParamEditMenu)
{
   static uint8_t ucIndex;
   static uint8_t ucShiftDelay_30ms;
   uint8_t ucLength = strlen(pstParamEditMenu->psTitle);
   if(ucLength > 20)
   {
      if(gbEnteredMenu)
      {
         ucIndex = 0;
         gbEnteredMenu = 0;
      }
      if(++ucShiftDelay_30ms >= 20)
      {
         ucShiftDelay_30ms = 0;
         if(++ucIndex >= (ucLength - 20))
         {
            ucIndex = 0;
         }
      }
   }
   LCD_Char_WriteStringUpper( pstParamEditMenu->psTitle + ucIndex );
}
#endif
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void PrintPI(struct st_param_edit_menu * pstParamEditMenu)
{
   if(pstParamEditMenu->ulValue_Edited < MAX_NUM_FLOORS)
   {
      LCD_Char_WriteString("[");
      LCD_Char_WriteString(Get_PI_Label(pstParamEditMenu->ulValue_Edited));
      LCD_Char_WriteString("]");
   }
}
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void Display_DecimalPoints_None(struct st_param_edit_menu * pstParamEditMenu)
{
   uint8_t ucIncreasedFieldSize = 0;
   uint32_t uiValueModified = pstParamEditMenu->ulValue_Edited + pstParamEditMenu->ucValueOffset;
   uint32_t uiValue = uiValueModified;

   if(pstParamEditMenu->bSignedInteger)
   {
      un_type_conv unTypeConv;
      unTypeConv.ui = uiValue;
      uint32_t uiUnsignedValue;
      switch(pstParamEditMenu->enParamType)
      {
         case enPARAM_BLOCK_TYPE__UINT8:
            uiUnsignedValue = unTypeConv.ac[0];
            if(unTypeConv.ac[0] < 0)
            {
               uiUnsignedValue = unTypeConv.ac[0] * -1;
               LCD_Char_GotoXY(START_CURSOR_POS_VALUE-1, 2);
               LCD_Char_WriteChar('-');
            }
            LCD_Char_WriteInteger_ExactLength(uiUnsignedValue, pstParamEditMenu->ucFieldSize_Value+ucIncreasedFieldSize);
            break;
         case enPARAM_BLOCK_TYPE__UINT16:
            uiUnsignedValue = unTypeConv.aw[0];
            if(unTypeConv.aw[0] < 0)
            {
               uiUnsignedValue = unTypeConv.aw[0] * -1;
               LCD_Char_GotoXY(START_CURSOR_POS_VALUE-1, 2);
               LCD_Char_WriteChar('-');
            }
            LCD_Char_WriteInteger_ExactLength(uiUnsignedValue, pstParamEditMenu->ucFieldSize_Value+ucIncreasedFieldSize);
            break;
         case enPARAM_BLOCK_TYPE__UINT32:
            uiUnsignedValue = unTypeConv.i;
            if(unTypeConv.i < 0)
            {
               uiUnsignedValue = unTypeConv.i * -1;
               LCD_Char_GotoXY(START_CURSOR_POS_VALUE-1, 2);
               LCD_Char_WriteChar('-');
            }
            LCD_Char_WriteInteger_ExactLength(uiUnsignedValue, pstParamEditMenu->ucFieldSize_Value+ucIncreasedFieldSize);
            break;
         default: break;
      }
   }
   else
   {
      LCD_Char_WriteInteger_ExactLength(uiValue, pstParamEditMenu->ucFieldSize_Value+ucIncreasedFieldSize);
   }

   LCD_Char_WriteString(pstParamEditMenu->psUnit);
   LCD_Char_GotoXY( pstParamEditMenu->ucCursorColumn+ucIncreasedFieldSize, 3 );
   LCD_Char_WriteChar( '*' );
}
static void Display_DecimalPoints(struct st_param_edit_menu * pstParamEditMenu)
{
   uint8_t ucDecimalPoints = pstParamEditMenu->enDecimalFormat;
   uint32_t uiValueModified = pstParamEditMenu->ulValue_Edited + pstParamEditMenu->ucValueOffset;

   uint16_t ulDivisor = pow(10,ucDecimalPoints);
   uint32_t ulInteger = uiValueModified/ulDivisor;
   uint16_t uwDecimal = uiValueModified%ulDivisor;

   if((pstParamEditMenu->uwParamIndex_Current == enPARAM8__P1_LevelingDistance_5mm)
         || (pstParamEditMenu->uwParamIndex_Current == enPARAM8__P2_LevelingDistance_5mm)
         || (pstParamEditMenu->uwParamIndex_Current == enPARAM8__P3_LevelingDistance_5mm)
         || (pstParamEditMenu->uwParamIndex_Current == enPARAM8__P4_LevelingDistance_5mm))
   {
      ulInteger = (uiValueModified * 2)/ulDivisor;
      uwDecimal = (uiValueModified * 2)%ulDivisor;
   }
   LCD_Char_WriteInteger_ExactLength(ulInteger, pstParamEditMenu->ucFieldSize_Value - ucDecimalPoints);
   uint8_t ucCursorPos = pstParamEditMenu->ucCursorColumn;
   if(pstParamEditMenu->ucCursorColumn == (START_CURSOR_POS_VALUE + pstParamEditMenu->ucFieldSize_Value - 1))
   {
      ucCursorPos++;
   }
   LCD_Char_WriteString(".");
   LCD_Char_WriteInteger_ExactLength(uwDecimal, ucDecimalPoints);
   LCD_Char_WriteString(pstParamEditMenu->psUnit);

   LCD_Char_GotoXY( ucCursorPos, 3 );
   LCD_Char_WriteChar( '*' );
}
static void Display_Time(struct st_param_edit_menu * pstParamEditMenu)
{
   uint32_t uiValue = pstParamEditMenu->ulValue_Edited;
   pstParamEditMenu->ulValue_Max = 2359;
   pstParamEditMenu->ulValue_Min = 0;

   hours = uiValue / 100;
   minutes = uiValue % 100; 
  
   LCD_Char_WriteInteger_ExactLength(hours, 2);
   LCD_Char_WriteChar(':');
   LCD_Char_WriteInteger_ExactLength(minutes, 2);

   LCD_Char_WriteString(pstParamEditMenu->psUnit);
   LCD_Char_GotoXY( pstParamEditMenu->ucCursorColumn, 3 );
   LCD_Char_WriteChar( '*' );
}
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void Screen_EditByBit_PerFloor(struct st_param_edit_menu * pstParamEditMenu)
{
   LCD_Char_Clear();
   //------------------------------------------------------------------
   if(pstParamEditMenu->bSaving)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Saving");
   }
   else if(pstParamEditMenu->bInvalid)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Invalid");
   }
   else
   {
      LCD_Char_GotoXY( 0, 0 );
      LCD_Char_WriteStringUpper( pstParamEditMenu->psTitle);
      LCD_Char_GotoXY( 0, 1 );
      char *psPI = Get_PI_Label(ucCurrentBitIndex);
      LCD_Char_WriteStringUpper( "[" );
      if(Param_ReadValue_1Bit(enPARAM1__Enable3DigitPI))
      {
         LCD_Char_WriteStringUpper( psPI );
      }
      else
      {
         LCD_Char_WriteStringUpper( psPI+1 );
      }
      LCD_Char_WriteStringUpper( "]" );
      //------------------------------------------------------------------
      LCD_Char_GotoXY(1,2);
      LCD_Char_WriteInteger_ExactLength( ucCurrentBitIndex + 1, 2 );
      LCD_Char_WriteStringUpper(" = ");

      LCD_Char_GotoXY(START_CURSOR_POS_VALUE, 2);

      uint8_t ucBitMapIndex = ucCurrentBitIndex%ucBitsPerBitmap;
      uint8_t bEditedValue = Sys_Bit_Get(&pstParamEditMenu->ulValue_Edited, ucBitMapIndex);

      if(bEditedValue)
      {
         LCD_Char_WriteString("On");
      }
      else
      {
         LCD_Char_WriteString("Off");
      }

      LCD_Char_GotoXY( pstParamEditMenu->ucCursorColumn, 3 );
      LCD_Char_WriteChar( '*' );
      //------------------------------------------------------------------
      if ( pstParamEditMenu->ulValue_Edited != pstParamEditMenu->ulValue_Saved )
      {
         LCD_Char_GotoXY( 16, 1 );
         LCD_Char_WriteString( "Save" );

         LCD_Char_GotoXY( 19, 2 );
         LCD_Char_WriteChar( '|' );
      }
   }
}
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void Screen_EditByBit_PerLanding(struct st_param_edit_menu * pstParamEditMenu)
{
   LCD_Char_Clear();
   //------------------------------------------------------------------
   if(pstParamEditMenu->bSaving)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Saving");
   }
   else if(pstParamEditMenu->bInvalid)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Invalid");
   }
   else
   {
      LCD_Char_GotoXY( 0, 0 );
      LCD_Char_WriteStringUpper( pstParamEditMenu->psTitle);
      LCD_Char_GotoXY( 0, 1 );
      LCD_Char_WriteString( "[" );
      uint8_t ucFloorIndex = ucCurrentBitIndex - Param_ReadValue_8Bit(enPARAM8__GroupLandingOffset);
      if( ucFloorIndex < GetFP_NumFloors() )
      {
         char *psPI = Get_PI_Label(ucFloorIndex);
         if(Param_ReadValue_1Bit(enPARAM1__Enable3DigitPI))
         {
            LCD_Char_WriteString( psPI );
         }
         else
         {
            LCD_Char_WriteString( psPI+1 );
         }
      }
      else
      {
         LCD_Char_WriteString("EX");
      }
      LCD_Char_WriteString( "]" );
      //------------------------------------------------------------------
      LCD_Char_GotoXY(1,2);
      LCD_Char_WriteInteger_ExactLength( ucCurrentBitIndex + 1, 2 );
      LCD_Char_WriteString(" = ");

      LCD_Char_GotoXY(START_CURSOR_POS_VALUE, 2);

      uint8_t ucBitMapIndex = ucCurrentBitIndex%ucBitsPerBitmap;
      uint8_t bEditedValue = Sys_Bit_Get(&pstParamEditMenu->ulValue_Edited, ucBitMapIndex);

      if(bEditedValue)
      {
         LCD_Char_WriteString("On");
      }
      else
      {
         LCD_Char_WriteString("Off");
      }

      LCD_Char_GotoXY( pstParamEditMenu->ucCursorColumn, 3 );
      LCD_Char_WriteChar( '*' );
      //------------------------------------------------------------------
      if ( pstParamEditMenu->ulValue_Edited != pstParamEditMenu->ulValue_Saved )
      {
         LCD_Char_GotoXY( 16, 1 );
         LCD_Char_WriteString( "Save" );
         LCD_Char_GotoXY( 19, 2 );
         LCD_Char_WriteChar( '|' );
      }
   }
}
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void Screen_EditByBit_PerHallFunction(struct st_param_edit_menu * pstParamEditMenu)
{
   LCD_Char_Clear();
   //------------------------------------------------------------------
   if(pstParamEditMenu->bSaving)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Saving");
   }
   else if(pstParamEditMenu->bInvalid)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Invalid");
   }
   else
   {
      LCD_Char_GotoXY( 0, 0 );
      LCD_Char_WriteStringUpper( pstParamEditMenu->psTitle);

      LCD_Char_GotoXY( 0, 1 );
      LCD_Char_WriteString( "[F" );
      uint8_t ucFunctionNumber = ( ucCurrentBitIndex % iMaxBitIndex ) + 1;
      LCD_Char_WriteInteger_MinLength(ucFunctionNumber, 1);
      LCD_Char_WriteString( "] DIPS: " );
      for(uint8_t i = 0; i < NUM_HALLBOARD_FUNCTION_DIPS; i++)
      {
         uint8_t bDIP = Sys_Bit_Get(&ucCurrentBitIndex, i);
         if( bDIP )
         {
            if( Param_ReadValue_1Bit(enPARAM1__EnableExtHallBoards) )
            {
               LCD_Char_WriteInteger_ExactLength(( FIRST_HALLBOARD_FUNCTION_DIP+i ) % 10, 1);
            }
            else
            {
               LCD_Char_WriteInteger_ExactLength(( FIRST_HALLBOARD_FUNCTION_DIP_OLD+i ) % 10, 1);
            }
         }
         else
         {
            LCD_Char_WriteChar('.');
         }
      }

      //------------------------------------------------------------------
      LCD_Char_GotoXY(1,2);
      LCD_Char_WriteInteger_ExactLength( ucCurrentBitIndex + 1, 2 );
      LCD_Char_WriteString(" = ");

      LCD_Char_GotoXY(START_CURSOR_POS_VALUE, 2);
      uint8_t ucBitMapIndex = ucCurrentBitIndex%ucBitsPerBitmap;
      uint8_t bEditedValue = Sys_Bit_Get(&pstParamEditMenu->ulValue_Edited, ucBitMapIndex);

      if(bEditedValue)
      {
         LCD_Char_WriteString("On");
      }
      else
      {
         LCD_Char_WriteString("Off");
      }

      LCD_Char_GotoXY( pstParamEditMenu->ucCursorColumn, 3 );
      LCD_Char_WriteChar( '*' );
      //------------------------------------------------------------------
      if ( pstParamEditMenu->ulValue_Edited != pstParamEditMenu->ulValue_Saved )
      {
         LCD_Char_GotoXY( 16, 1 );
         LCD_Char_WriteString( "Save" );
         LCD_Char_GotoXY( 19, 2 );
         LCD_Char_WriteChar( '|' );
      }
   }
}
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void Screen_PositionCountsAsFeet(struct st_param_edit_menu * pstParamEditMenu, uint8_t ucMultiplier)
{
   LCD_Char_Clear();

   LCD_Char_GotoXY( 0, 0 );

   //------------------------------------------------------------------
   if(pstParamEditMenu->bSaving)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Saving");
   }
   else if(pstParamEditMenu->bInvalid)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Invalid");
   }
   else
   {
      LCD_Char_WriteStringUpper( pstParamEditMenu->psTitle);

      //------------------------------------------------------------------
      LCD_Char_GotoXY(2,1);

      LCD_WritePositionInFeet(( pstParamEditMenu->ulValue_Edited * ucMultiplier)+GetPositionReference());

      //------------------------------------------------------------------
      LCD_Char_GotoXY(START_CURSOR_POS_VALUE, 2);
      LCD_Char_WriteInteger_ExactLength(pstParamEditMenu->ulValue_Edited, pstParamEditMenu->ucFieldSize_Value);
      //------------------------------------------------------------------

      LCD_Char_GotoXY( pstParamEditMenu->ucCursorColumn, 3 );

      LCD_Char_WriteChar( '*' );

      //------------------------------------------------------------------
      if ( pstParamEditMenu->ulValue_Edited != pstParamEditMenu->ulValue_Saved )
      {
         LCD_Char_GotoXY( 16, 1 );
         LCD_Char_WriteString( "Save" );

         LCD_Char_GotoXY( 19, 2 );
         LCD_Char_WriteChar( '|' );
      }
   }
}
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void Screen_TooHighTooLow(struct st_param_edit_menu * pstParamEditMenu)
{
   //------------------------------------------------------------------
   /* Modification to limit floor level chances to
    * < 2 inches. Prevents unintended movement */
   uint32_t uiFloorPos = Param_ReadValue_24Bit(pstParamEditMenu->uwParamIndex_Current);
   uint32_t uiChangeLimit = ( TWO_INCHES ) * 0.90;
   pstParamEditMenu->ulValue_Max = CEDES_INVALID_POSITION;
   pstParamEditMenu->ulValue_Min = 0;

   if( ( GetDoorState_Front() == DOOR__CLOSED )
    && ( !GetFP_RearDoors() || ( GetDoorState_Rear() == DOOR__CLOSED ) ) )
   {
      uiChangeLimit = ( ONE_INCHS*100 );
   }

   if( ( uiFloorPos + uiChangeLimit ) < CEDES_INVALID_POSITION )
   {
      pstParamEditMenu->ulValue_Max = uiFloorPos + uiChangeLimit;
   }
   if( uiFloorPos > uiChangeLimit )
   {
      pstParamEditMenu->ulValue_Min = uiFloorPos - uiChangeLimit;
   }

   //------------------------------------------------------------------

   LCD_Char_Clear();

   LCD_Char_GotoXY( 0, 0 );

   //------------------------------------------------------------------
   if(pstParamEditMenu->bSaving)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Saving");
   }
   else if(pstParamEditMenu->bInvalid)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Invalid");
   }
   else
   {
      /* Line 1 */
      LCD_Char_WriteStringUpper( pstParamEditMenu->psTitle);

      LCD_Char_GotoXY( 15, 0 );
      LCD_Char_WriteStringUpper( "[" );
      char * psPI = Get_PI_Label( GetOperation_CurrentFloor() );
      LCD_Char_WriteStringUpper( psPI );
      LCD_Char_WriteStringUpper( "]" );

      //------------------------------------------------------------------
      /* Line 2 */
      LCD_Char_GotoXY(6,1);
      // Redefined too high too low to display offset in feet rather than absolute pos
      LCD_WriteDistanceInInches(pstParamEditMenu->ulValue_Edited - pstParamEditMenu->ulValue_Saved, 1 );

      LCD_Char_GotoXY(0,1);
      psPI = Get_PI_Label( pstParamEditMenu->uwParamIndex_Current - pstParamEditMenu->uwParamIndex_Start );
      LCD_Char_WriteStringUpper( "[" );
      LCD_Char_WriteStringUpper( psPI );
      LCD_Char_WriteStringUpper( "]" );
      //------------------------------------------------------------------
      /* Line 3 */
      LCD_Char_GotoXY(1,2);
      LCD_Char_WriteInteger_MinLength( pstParamEditMenu->uwParamIndex_Current - pstParamEditMenu->uwParamIndex_Start + 1, 2 );
      LCD_Char_WriteStringUpper(" = ");



      //------------------------------------------------------------------
      uint32_t ulDifference = (pstParamEditMenu->ulValue_Edited >= pstParamEditMenu->ulValue_Saved) ?
                                    (pstParamEditMenu->ulValue_Edited - pstParamEditMenu->ulValue_Saved) :
                                    (pstParamEditMenu->ulValue_Saved - pstParamEditMenu->ulValue_Edited);

      const char *ps = (pstParamEditMenu->ulValue_Edited >= pstParamEditMenu->ulValue_Saved) ? "+":"-";

      LCD_Char_WriteStringUpper(ps);
      LCD_Char_WriteInteger_ExactLength(ulDifference,8);
      //------------------------------------------------------------------
      /* Line 4 */
      LCD_Char_GotoXY( pstParamEditMenu->ucCursorColumn, 3 );

      LCD_Char_WriteChar( '*' );

      //------------------------------------------------------------------
      if ( pstParamEditMenu->ulValue_Edited != pstParamEditMenu->ulValue_Saved )
      {
         LCD_Char_GotoXY( 16, 1 );
         LCD_Char_WriteString( "Save" );

         LCD_Char_GotoXY( 19, 2 );
         LCD_Char_WriteChar( '|' );
      }
   }
}
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void Screen_Sabbath(struct st_param_edit_menu * pstParamEditMenu)
{
   LCD_Char_Clear();

   LCD_Char_GotoXY( 0, 0 );

   //------------------------------------------------------------------
   if(pstParamEditMenu->bSaving)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Saving");
   }
   else if(pstParamEditMenu->bInvalid)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Invalid");
   }
   else
   {
     LCD_Char_GotoXY( 0, 0 );
     LCD_Char_WriteStringUpper( pstParamEditMenu->psTitle);
      PrintArrayString(pstParamEditMenu);
      //------------------------------------------------------------------
      if(pstParamEditMenu->uwParamIndex_Start != pstParamEditMenu->uwParamIndex_End)
      {
         LCD_Char_GotoXY(1,2);
         LCD_Char_WriteInteger_ExactLength( pstParamEditMenu->uwParamIndex_Current - pstParamEditMenu->uwParamIndex_Start + 1, 2 );
         LCD_Char_WriteStringUpper(" = ");
      }

      LCD_Char_GotoXY(START_CURSOR_POS_VALUE, 2);
      
      Display_Time( pstParamEditMenu);

      //------------------------------------------------------------------
      if ( pstParamEditMenu->ulValue_Edited != pstParamEditMenu->ulValue_Saved )
      {
         LCD_Char_GotoXY( 16, 1 );
         LCD_Char_WriteString( "Save" );

         LCD_Char_GotoXY( 19, 2 );
         LCD_Char_WriteChar( '|' );
      }
   }
}
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void Screen_FloorIndexAsPI(struct st_param_edit_menu * pstParamEditMenu)
{
   LCD_Char_Clear();

   LCD_Char_GotoXY( 0, 0 );

   //------------------------------------------------------------------
   if(pstParamEditMenu->bSaving)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Saving");
   }
   else if(pstParamEditMenu->bInvalid)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Invalid");
   }
   else
   {
     LCD_Char_GotoXY( 0, 0 );
     LCD_Char_WriteStringUpper( pstParamEditMenu->psTitle);
     //------------------------------------------------------------------
     LCD_Char_GotoXY( 0, 2 );
     PrintPI(pstParamEditMenu);
     //------------------------------------------------------------------
      if(pstParamEditMenu->uwParamIndex_Start != pstParamEditMenu->uwParamIndex_End)
      {
         LCD_Char_GotoXY(1,2);
         LCD_Char_WriteInteger_ExactLength( pstParamEditMenu->uwParamIndex_Current - pstParamEditMenu->uwParamIndex_Start + 1, 2 );
         LCD_Char_WriteStringUpper(" = ");
      }

      LCD_Char_GotoXY(START_CURSOR_POS_VALUE, 2);
      if(pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__BIT)
      {
         if(pstParamEditMenu->ulValue_Edited)
         {
            LCD_Char_WriteString("On");
         }
         else
         {
            LCD_Char_WriteString("Off");
         }
         LCD_Char_GotoXY( pstParamEditMenu->ucCursorColumn, 3 );

         LCD_Char_WriteChar( '*' );
      }
      else
      {
         if(pstParamEditMenu->enDecimalFormat == enNumberDecimalPoints_None)
         {
            Display_DecimalPoints_None(pstParamEditMenu);
         }
         else
         {
            Display_DecimalPoints(pstParamEditMenu);
         }
      }

      //------------------------------------------------------------------
      if ( pstParamEditMenu->ulValue_Edited != pstParamEditMenu->ulValue_Saved )
      {
         LCD_Char_GotoXY( 16, 1 );
         LCD_Char_WriteString( "Save" );

         LCD_Char_GotoXY( 19, 2 );
         LCD_Char_WriteChar( '|' );
      }
   }
}
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void Screen_Default(struct st_param_edit_menu * pstParamEditMenu)
{
   LCD_Char_Clear();

   LCD_Char_GotoXY( 0, 0 );

   //------------------------------------------------------------------
   if(pstParamEditMenu->bSaving)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Saving");
   }
   else if(pstParamEditMenu->bInvalid)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Invalid");
   }
   else
   {
     LCD_Char_GotoXY( 0, 0 );
     LCD_Char_WriteStringUpper( pstParamEditMenu->psTitle);
      PrintArrayString(pstParamEditMenu);
      //------------------------------------------------------------------
      if(pstParamEditMenu->uwParamIndex_Start != pstParamEditMenu->uwParamIndex_End)
      {
         LCD_Char_GotoXY(1,2);
         LCD_Char_WriteInteger_ExactLength( pstParamEditMenu->uwParamIndex_Current - pstParamEditMenu->uwParamIndex_Start + 1, 2 );
         LCD_Char_WriteStringUpper(" = ");
      }

      LCD_Char_GotoXY(START_CURSOR_POS_VALUE, 2);
      if(pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__BIT)
      {
         if(pstParamEditMenu->ulValue_Edited)
         {
            LCD_Char_WriteString("On");
         }
         else
         {
            LCD_Char_WriteString("Off");
         }
         LCD_Char_GotoXY( pstParamEditMenu->ucCursorColumn, 3 );

         LCD_Char_WriteChar( '*' );
      }
      else
      {
         if(pstParamEditMenu->enDecimalFormat == enNumberDecimalPoints_None)
         {
            Display_DecimalPoints_None(pstParamEditMenu);
         }
         else
         {
            Display_DecimalPoints(pstParamEditMenu);
         }
      }

      //------------------------------------------------------------------
      if ( pstParamEditMenu->ulValue_Edited != pstParamEditMenu->ulValue_Saved )
      {
         LCD_Char_GotoXY( 16, 1 );
         LCD_Char_WriteString( "Save" );

         LCD_Char_GotoXY( 19, 2 );
         LCD_Char_WriteChar( '|' );
      }
   }
}
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/

static void Update_Screen(struct st_param_edit_menu * pstParamEditMenu)
{
   if( pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__UINT24
    && pstParamEditMenu->uwParamIndex_Start == enPARAM24__LearnedFloor_0 )
   {
      Screen_TooHighTooLow(pstParamEditMenu);
   }

   else if(( pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__UINT24 )
         && (( pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_Start_Time)
         || ( pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_End_Time) ) )
   {
      Screen_Sabbath( pstParamEditMenu );
   }
   else if( ( ( pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__UINT8 )
           && ( pstParamEditMenu->uwParamIndex_Start == enPARAM8__RelevelingDistance_05mm ) )
         || ( ( pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__UINT16 )
           && ( ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__BufferDistance_05mm )
             || ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__ETSL_CameraOffset_05mm )
             || ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Acceptance_SlideDistance )
             || ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__PreOpeningDistance ) ) ) )
   {
      Screen_PositionCountsAsFeet(pstParamEditMenu, 1);
   }
   else if( ( pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__UINT8 )
         && ( pstParamEditMenu->uwParamIndex_Start == enPARAM8__ETS_OffsetFromNTS_5mm ) )
   {
      Screen_PositionCountsAsFeet(pstParamEditMenu, 10);
   }
   else if( ( pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__UINT8 )
         && ( ( pstParamEditMenu->uwParamIndex_Start == enPARAM8__OverrideParkingFloor )
           || ( pstParamEditMenu->uwParamIndex_Start == enPARAM8__FireRecallFloorM )
           || ( pstParamEditMenu->uwParamIndex_Start == enPARAM8__FireRecallFloorA ) ) )
   {
      Screen_FloorIndexAsPI(pstParamEditMenu);
   }
   else if( ( pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__UINT16 )
         && (( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekday_StartTime)
         || ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekday_EndTime)
         || ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekend_StartTime)
         || ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekend_EndTime) ) )
   {
      Screen_Sabbath( pstParamEditMenu );
   }
   else
   {
      Screen_Default(pstParamEditMenu);
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ValidateParameter(struct st_param_edit_menu * pstParamEditMenu)
{
   switch(pstParamEditMenu->enParamType)
   {
      case enPARAM_BLOCK_TYPE__BIT:
         pstParamEditMenu->ulValue_Max = ( !pstParamEditMenu->ulValue_Max )
                                     ? 1 : pstParamEditMenu->ulValue_Max;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_1BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_1Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT8:
         pstParamEditMenu->ulValue_Max = ( !pstParamEditMenu->ulValue_Max )
                                       ? 0xFF : pstParamEditMenu->ulValue_Max;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_8BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_8Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT16:
         pstParamEditMenu->ulValue_Max = ( !pstParamEditMenu->ulValue_Max )
                                       ? 0xFFFF : pstParamEditMenu->ulValue_Max;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_16BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_16Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT24:
         pstParamEditMenu->ulValue_Max = ( !pstParamEditMenu->ulValue_Max )
                                       ? 0xFFFFFF : pstParamEditMenu->ulValue_Max;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_24BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_24Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT32:
         pstParamEditMenu->ulValue_Max = ( !pstParamEditMenu->ulValue_Max )
                                       ? 0xFFFFFFFF : pstParamEditMenu->ulValue_Max;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_32BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_32Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      default:
         pstParamEditMenu->bInvalid = 1;
         break;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_BitmaskIndex( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_param_edit_menu * pstParamEditMenu )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex = 0;
   int32_t lBitmapIndex_Edited;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = pstParamEditMenu->ucFieldSize_Index-1;
      }
   }
   else  // enACTION__EDIT
   {

      lBitmapIndex_Edited = ucCurrentBitIndex;

      pstParamEditMenu->ucCursorColumn = 1+ucCursorIndex;
      //------------------------------------------------------------------
      int32_t uiDigitWeight = (pstParamEditMenu->ucFieldSize_Index - 1) - ucCursorIndex;

      uiDigitWeight = pow( 10, uiDigitWeight );  // 10 to the power of uiDigitWeight
      //------------------------------------------------------------------
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            lBitmapIndex_Edited += uiDigitWeight;
            break;
         case enKEYPRESS_DOWN:
            lBitmapIndex_Edited -= uiDigitWeight;
            break;
         case enKEYPRESS_LEFT:
            if ( ucCursorIndex )
            {
               --ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_LEFT;
            }
            break;
         case enKEYPRESS_RIGHT:
            if ( ucCursorIndex < pstParamEditMenu->ucFieldSize_Index - 1 )
            {
               ++ucCursorIndex;
            }
            else
            {
               pstParamEditMenu->ulValue_Edited = pstParamEditMenu->ulValue_Saved;
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;
         default:
            break;
      }
      //------------------------------------------------------------------
      if(lBitmapIndex_Edited >= iMaxBitIndex-1)
      {
         lBitmapIndex_Edited = iMaxBitIndex-1;
      }
      else if(lBitmapIndex_Edited <= 0)
      {
         lBitmapIndex_Edited = 0;
      }

      ucCurrentBitIndex = (uint8_t) lBitmapIndex_Edited;

      pstParamEditMenu->ulValue_Edited = pstParamEditMenu->ulValue_Saved;

   }

   return enReturnValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Value_ByBit( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_param_edit_menu * pstParamEditMenu )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex;
   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = pstParamEditMenu->ucFieldSize_Value - 1;
      }
   }
   else  // enACTION__EDIT
   {
      uint8_t ucBitMapIndex = ucCurrentBitIndex % ucBitsPerBitmap;
      //------------------------------------------------------------------
      uint8_t bEditedValue = Sys_Bit_Get(&pstParamEditMenu->ulValue_Edited, ucBitMapIndex);
      //------------------------------------------------------------------
      pstParamEditMenu->ucCursorColumn = START_CURSOR_POS_VALUE + ucCursorIndex;
      //------------------------------------------------------------------
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            bEditedValue = 1;
            break;

         case enKEYPRESS_DOWN:
            bEditedValue = 0;
            break;

         case enKEYPRESS_LEFT:
            if ( ucCursorIndex )
            {
               --ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_LEFT;
            }
            break;

         case enKEYPRESS_RIGHT:
            if ( ucCursorIndex < (pstParamEditMenu->ucFieldSize_Value - 1) )
            {
               ++ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;

         default:
            break;
      }
      Sys_Bit_Set(&pstParamEditMenu->ulValue_Edited, ucBitMapIndex, bEditedValue);
   }

   return enReturnValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Index( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_param_edit_menu * pstParamEditMenu )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex = 0;
   int32_t lParamIndex_Edited;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = pstParamEditMenu->ucFieldSize_Index-1;
      }
   }
   else  // enACTION__EDIT
   {

      lParamIndex_Edited = pstParamEditMenu->uwParamIndex_Current;

      pstParamEditMenu->ucCursorColumn = 1+ucCursorIndex;
      //------------------------------------------------------------------
      int32_t uiDigitWeight = (pstParamEditMenu->ucFieldSize_Index - 1) - ucCursorIndex;

      uiDigitWeight = pow( 10, uiDigitWeight );  // 10 to the power of uiDigitWeight

      //------------------------------------------------------------------
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            lParamIndex_Edited += uiDigitWeight;
            break;
         case enKEYPRESS_DOWN:
            lParamIndex_Edited -= uiDigitWeight;
            break;
         case enKEYPRESS_LEFT:
            if ( ucCursorIndex )
            {
               --ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_LEFT;
            }
            break;
         case enKEYPRESS_RIGHT:
            if ( ucCursorIndex < pstParamEditMenu->ucFieldSize_Index - 1 )
            {
               ++ucCursorIndex;
            }
            else
            {
               pstParamEditMenu->ulValue_Edited = pstParamEditMenu->ulValue_Saved;
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;
         default:
            break;
      }
      //------------------------------------------------------------------
      if(lParamIndex_Edited > pstParamEditMenu->uwParamIndex_End)
      {
         lParamIndex_Edited = pstParamEditMenu->uwParamIndex_End;
      }
      else if(lParamIndex_Edited <= pstParamEditMenu->uwParamIndex_Start)
      {
         lParamIndex_Edited = pstParamEditMenu->uwParamIndex_Start;
      }

      pstParamEditMenu->uwParamIndex_Current = (uint16_t) lParamIndex_Edited;

      pstParamEditMenu->ulValue_Edited = pstParamEditMenu->ulValue_Saved;

   }

   return enReturnValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Value( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_param_edit_menu * pstParamEditMenu )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex;
   int64_t llCurrentParam_EditedValue;
   uint8_t ucMaxSet;
   uint8_t ucMinSet;
   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = pstParamEditMenu->ucFieldSize_Value - 1;
      }
   }
   else  // enACTION__EDIT
   {
      //Check to see if there is a max or min set
      ucMaxSet = (pstParamEditMenu->ulValue_Max) ? 1 : 0;
      ucMinSet = (pstParamEditMenu->ulValue_Min) ? 1 : 0;
      //------------------------------------------------------------------
      llCurrentParam_EditedValue = pstParamEditMenu->ulValue_Edited;
      //------------------------------------------------------------------
      int32_t uiDigitWeight = (pstParamEditMenu->ucFieldSize_Value - 1) - ucCursorIndex;

      if(  ( pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__UINT24 ) &&
          ( ( (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_Start_Time)
          || (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_End_Time) ) )
          && (uiDigitWeight > 2) )
       {
         uiDigitWeight = uiDigitWeight - 1;
       }

      if( ( pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__UINT16 )
               && (( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekday_StartTime)
               || ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekday_EndTime)
               || ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekend_StartTime)
               || ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekend_EndTime) )
               && (uiDigitWeight > 2) )
      {
         uiDigitWeight = uiDigitWeight - 1;
      }

       // else if(  ( (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_Start_Time)
       //    || (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_End_Time) )
       //    && (uiDigitWeight < 2) )
       // {
       //   uiDigitWeight = uiDigitWeight - 1;
       // }

      uiDigitWeight = pow( 10, uiDigitWeight );  // 10 to the power of uiDigitWeight


      if( ( pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__UINT24 ) &&
        ((pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_Start_Time)
       || (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_End_Time)))
      {
         if( (( pstParamEditMenu->ucFieldSize_Value -1) - ucCursorIndex) == 2)
         {
            uiDigitWeight = 0;
         }
      }

      if( ( pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__UINT16 )
               && (( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekday_StartTime)
               || ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekday_EndTime)
               || ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekend_StartTime)
               || ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekend_EndTime) ) )
      {
         if( (( pstParamEditMenu->ucFieldSize_Value -1) - ucCursorIndex) == 2)

         {
            uiDigitWeight = 0;
         }
      }

      if(((pstParamEditMenu->uwParamIndex_Current == enPARAM8__P1_LevelingDistance_5mm)
                     || (pstParamEditMenu->uwParamIndex_Current == enPARAM8__P2_LevelingDistance_5mm)
                     || (pstParamEditMenu->uwParamIndex_Current == enPARAM8__P3_LevelingDistance_5mm)
                     || (pstParamEditMenu->uwParamIndex_Current == enPARAM8__P4_LevelingDistance_5mm))
                           && uiDigitWeight > 1)
     {
        uiDigitWeight /= 2;
     }

      //------------------------------------------------------------------
      pstParamEditMenu->ucCursorColumn = START_CURSOR_POS_VALUE + ucCursorIndex;

      //------------------------------------------------------------------
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:

            llCurrentParam_EditedValue += uiDigitWeight;

          if(( pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__UINT24 ) &&
            ((pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_Start_Time)
             || (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_End_Time)))
          {

            uint8_t ucMinutes = llCurrentParam_EditedValue % 100;

            if( ucMinutes > 59)
            {
               llCurrentParam_EditedValue = llCurrentParam_EditedValue - ucMinutes + 100;
            }
          }

          if( ( pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__UINT16 )
                   && (( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekday_StartTime)
                   || ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekday_EndTime)
                   || ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekend_StartTime)
                   || ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekend_EndTime) ) )
          {
             uint8_t ucMinutes = llCurrentParam_EditedValue % 100;

             if( ucMinutes > 59)
             {
                llCurrentParam_EditedValue = llCurrentParam_EditedValue - ucMinutes + 100;
             }
          }

          if( ucMaxSet && llCurrentParam_EditedValue > pstParamEditMenu->ulValue_Max)
          {
            llCurrentParam_EditedValue = pstParamEditMenu->ulValue_Max;
          }

            break;

         case enKEYPRESS_DOWN:
            llCurrentParam_EditedValue -= uiDigitWeight;

          uint8_t ucMinutes = llCurrentParam_EditedValue % 100;


         if( ( pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__UINT24 ) &&
            ((pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_Start_Time)
             || (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_End_Time)))
         {
            if( ucMinutes > 59)
            {
               llCurrentParam_EditedValue = llCurrentParam_EditedValue - ucMinutes + 100;
            }
         }

         if( ( pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__UINT16 )
                  && (( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekday_StartTime)
                  || ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekday_EndTime)
                  || ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekend_StartTime)
                  || ( pstParamEditMenu->uwParamIndex_Start == enPARAM16__Weekend_EndTime) ) )
         {
            if( ucMinutes > 59)
            {
               llCurrentParam_EditedValue = llCurrentParam_EditedValue - ucMinutes + 100;
            }
         }

          if(ucMinSet)
          {

             if( llCurrentParam_EditedValue < pstParamEditMenu->ulValue_Min)
             {
                llCurrentParam_EditedValue = pstParamEditMenu->ulValue_Min;
             }
          }

            break;

         case enKEYPRESS_LEFT:
            if ( ucCursorIndex )
            {
               --ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_LEFT;
            }
            break;

         case enKEYPRESS_RIGHT:
            if ( ucCursorIndex < (pstParamEditMenu->ucFieldSize_Value - 1) )
            {
               ++ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;

         default:
            break;
      }

      if(llCurrentParam_EditedValue > pstParamEditMenu->ulValue_Max)
      {
         llCurrentParam_EditedValue = pstParamEditMenu->ulValue_Max;
      }
      else if(llCurrentParam_EditedValue < 0)
      {
         llCurrentParam_EditedValue = 0;
      }
      pstParamEditMenu->ulValue_Edited = (uint32_t) llCurrentParam_EditedValue;
   }

   return enReturnValue;
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void SaveParameter( struct st_param_edit_menu * pstParamEditMenu )
{
   uint16_t ucIndex = pstParamEditMenu->uwParamIndex_Current;
   switch(pstParamEditMenu->enParamType)
   {
      case enPARAM_BLOCK_TYPE__UINT32:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_32Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_32Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT24:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_24Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_24Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT16:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_16Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_16Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT8:

         pstParamEditMenu->ulValue_Saved = Param_ReadValue_8Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_8Bit( ucIndex,pstParamEditMenu->ulValue_Edited);
         }
         break;
      default:
      case enPARAM_BLOCK_TYPE__BIT:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_1Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_1Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Save( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_param_edit_menu * pstParamEditMenu )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static enum en_save_state enSaveState = SAVE_STATE__SAVED;

   pstParamEditMenu->ucCursorColumn = 19;
   //------------------------------------------
   if( enFieldAction == enACTION__ENTER_FROM_LEFT )
   {
      enSaveState = SAVE_STATE__SAVED;
   }
   else if ( enFieldAction == enACTION__EDIT )
   {
      if ( enKeypress == enKEYPRESS_ENTER )
      {
         enSaveState = SAVE_STATE__SAVING;
      }
      else if ( enKeypress == enKEYPRESS_LEFT )
      {
         enSaveState = SAVE_STATE__SAVED;
         enReturnValue = enACTION__EXIT_TO_LEFT;
      }
   }

   //--------------------------------
   switch(enSaveState)
   {
      case SAVE_STATE__CLEARING:
         pstParamEditMenu->bSaving = 1;
         if(!pstParamEditMenu->ulValue_Saved)
         {
            enSaveState = SAVE_STATE__SAVING;
         }
         break;
      case SAVE_STATE__SAVING:
         SaveParameter(pstParamEditMenu);
         pstParamEditMenu->bSaving = 1;
         if(pstParamEditMenu->ulValue_Saved == pstParamEditMenu->ulValue_Edited)
         {
            enReturnValue = enACTION__EXIT_TO_LEFT;
            pstParamEditMenu->bSaving = 0;
            enSaveState = SAVE_STATE__SAVED;
         }
         break;
      case SAVE_STATE__SAVED:
         pstParamEditMenu->bSaving = 0;
         break;
      default:
         enSaveState = SAVE_STATE__SAVED;
         break;
   }
   return enReturnValue;
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void Edit_Parameter_Range(struct st_param_edit_menu * pstParamEditMenu)
{
   ValidateParameter(pstParamEditMenu);
   if ( pstParamEditMenu->ucFieldIndex == enFIELD__NONE )
   {
      pstParamEditMenu->ulValue_Edited = pstParamEditMenu->ulValue_Saved;
      pstParamEditMenu->bSaving = 0;
      pstParamEditMenu->bInvalid = 0;
      pstParamEditMenu->ucFieldIndex = enFIELD__INDEX;

      Edit_Index( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );
   }
   else
   {
      enum en_field_action enFieldAction;

      enum en_keypresses enKeypress = Button_GetKeypress();
      //------------------------------------------------------------------
      switch ( pstParamEditMenu->ucFieldIndex )
      {
         case enFIELD__INDEX:
            enFieldAction = Edit_Index( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               PrevScreen();
               pstParamEditMenu->ucFieldIndex = enFIELD__NONE;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__VALUE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__VALUE: //function

            enFieldAction = Edit_Value( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Index( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT, pstParamEditMenu);

               pstParamEditMenu->ucFieldIndex = enFIELD__INDEX;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Save( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__SAVE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__SAVE:

            enFieldAction = Edit_Save( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__VALUE;
            }
            break;

         default:
            break;
      }
      Update_Screen(pstParamEditMenu);
   }
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void Edit_Parameter_Single(struct st_param_edit_menu * pstParamEditMenu)
{
   ValidateParameter(pstParamEditMenu);
   if ( pstParamEditMenu->ucFieldIndex == enFIELD__NONE )
   {
      pstParamEditMenu->ulValue_Edited = pstParamEditMenu->ulValue_Saved;
      pstParamEditMenu->bSaving = 0;
      pstParamEditMenu->bInvalid = 0;
      pstParamEditMenu->ucFieldIndex = enFIELD__VALUE;
      Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );
   }
   else
   {
      if( ( pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__UINT24 ) &&
         ((pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_Start_Time)
         ||  (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_End_Time) ))
      {
         pstParamEditMenu->ucFieldSize_Value = 5;
      }
      enum en_field_action enFieldAction;

      enum en_keypresses enKeypress = Button_GetKeypress();
      //------------------------------------------------------------------
      switch ( pstParamEditMenu->ucFieldIndex )
      {
         case enFIELD__VALUE:

            enFieldAction = Edit_Value( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               PrevScreen();
               pstParamEditMenu->ucFieldIndex = enFIELD__NONE;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Save( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__SAVE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__SAVE:

            enFieldAction = Edit_Save( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__VALUE;
            }
            break;

         default:
            break;
      }
      Update_Screen(pstParamEditMenu);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void GetIndexFieldSize(  struct st_param_edit_menu * pstParamEditMenu  )
{
   uint16_t uwRangeOfIndex = pstParamEditMenu->uwParamIndex_End - pstParamEditMenu->uwParamIndex_Start;
   if(uwRangeOfIndex < 1)
   {
      pstParamEditMenu->ucFieldSize_Index = 0;
   }
   else if(uwRangeOfIndex < 10)
   {
      pstParamEditMenu->ucFieldSize_Index = 1;
   }
   else if(uwRangeOfIndex < 100)
   {
      pstParamEditMenu->ucFieldSize_Index = 2;
   }
   else//Accept none higher
   {
      pstParamEditMenu->ucFieldSize_Index = 3;
   }
}
/*-----------------------------------------------------------------------------
In decimal
 -----------------------------------------------------------------------------*/
static void GetValueFieldSize(  struct st_param_edit_menu * pstParamEditMenu  )
{
   switch(pstParamEditMenu->enParamType)
   {
      case enPARAM_BLOCK_TYPE__UINT32:
         pstParamEditMenu->ucFieldSize_Value = 10;
         break;
      case enPARAM_BLOCK_TYPE__UINT24:
         pstParamEditMenu->ucFieldSize_Value = 8;
         break;
      case enPARAM_BLOCK_TYPE__UINT16:
         pstParamEditMenu->ucFieldSize_Value = 5;
         break;
      case enPARAM_BLOCK_TYPE__UINT8:
         pstParamEditMenu->ucFieldSize_Value = 3;
         break;
      case enPARAM_BLOCK_TYPE__BIT:
         pstParamEditMenu->ucFieldSize_Value = 1;
         break;
      default:
         pstParamEditMenu->ucFieldSize_Value = 1;
         break;
   }
}
/*-----------------------------------------------------------------------------
If this is a new menu, reset local values
 -----------------------------------------------------------------------------*/
static void EnterNewMenu(  struct st_param_edit_menu * pstParamEditMenu  )
{
   GetIndexFieldSize(pstParamEditMenu);
   GetValueFieldSize(pstParamEditMenu);
   pstParamEditMenu->bInvalid = 0;
   pstParamEditMenu->bSaving = 0;
   pstParamEditMenu->ucCursorColumn = 0;
   pstParamEditMenu->uwParamIndex_Current = pstParamEditMenu->uwParamIndex_Start;
   pstParamEditMenu->ucFieldIndex = enFIELD__NONE;
   gbEnteredMenu = 1;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UI_ParamEditSceenTemplate( struct st_param_edit_menu * pstParamEditMenu )
{
   static uint16_t uwLastParamIndex = 0xFFFF;
   static uint8_t ucLastParamType = 0xFF;
   if(pstParamEditMenu->uwParamIndex_Start != uwLastParamIndex
   || pstParamEditMenu->enParamType != ucLastParamType )
   {
      EnterNewMenu(pstParamEditMenu);
      uwLastParamIndex = pstParamEditMenu->uwParamIndex_Start;
      ucLastParamType = pstParamEditMenu->enParamType;
   }
   //---------------------------------------

   if(pstParamEditMenu->ucFieldSize_Index)
   {
      Edit_Parameter_Range(pstParamEditMenu);
   }
   else
   {
      Edit_Parameter_Single(pstParamEditMenu);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UI_SetFlagScreenTemplate( struct st_set_flag_menu * pstSetFlagMenu )
{
//TODO for defaulting,
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ParamEditScreenTemplate_EditByBit(struct st_param_edit_menu * pstParamEditMenu)
{
   static uint16_t uwLastParamIndex = 0xFFFF;
   static uint8_t ucLastParamType = 0xFF;
   if(pstParamEditMenu->uwParamIndex_Start != uwLastParamIndex
   || pstParamEditMenu->enParamType != ucLastParamType )
   {
      EnterNewMenu(pstParamEditMenu);
      uwLastParamIndex = pstParamEditMenu->uwParamIndex_Start;
      ucLastParamType = pstParamEditMenu->enParamType;

      switch( pstParamEditMenu->enParamType )
      {
         case enPARAM_BLOCK_TYPE__UINT32:
            ucBitsPerBitmap = NUM_BITS_PER_BITMAP32;
            break;
         case enPARAM_BLOCK_TYPE__UINT24:
            ucBitsPerBitmap = NUM_BITS_PER_BITMAP24;
            break;
         case enPARAM_BLOCK_TYPE__UINT16:
            ucBitsPerBitmap = NUM_BITS_PER_BITMAP16;
            break;
         case enPARAM_BLOCK_TYPE__UINT8:
            ucBitsPerBitmap = NUM_BITS_PER_BITMAP8;
            break;
         default: ucBitsPerBitmap = 1;
            break;
      }
   }

   pstParamEditMenu->uwParamIndex_Current = pstParamEditMenu->uwParamIndex_Start + ( ucCurrentBitIndex / ucBitsPerBitmap );

   ValidateParameter(pstParamEditMenu);
   if ( pstParamEditMenu->ucFieldIndex == enFIELD__NONE )
   {
      pstParamEditMenu->ulValue_Edited = pstParamEditMenu->ulValue_Saved;
      pstParamEditMenu->bSaving = 0;
      pstParamEditMenu->bInvalid = 0;
      pstParamEditMenu->ucFieldIndex = enFIELD__BIT_INDEX;

      Edit_BitmaskIndex( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );
   }
   else
   {
      enum en_field_action enFieldAction;

      enum en_keypresses enKeypress = Button_GetKeypress();
      //------------------------------------------------------------------
      switch ( pstParamEditMenu->ucFieldIndex )
      {
         case enFIELD__BIT_INDEX:
            enFieldAction = Edit_BitmaskIndex( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               PrevScreen();
               pstParamEditMenu->ucFieldIndex = enFIELD__NONE;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Value_ByBit( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__VALUE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__VALUE: //function

            enFieldAction = Edit_Value_ByBit( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_BitmaskIndex( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT, pstParamEditMenu);

               pstParamEditMenu->ucFieldIndex = enFIELD__BIT_INDEX;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Save( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__SAVE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__SAVE:

            enFieldAction = Edit_Save( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Value_ByBit( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__VALUE;
            }
            break;

         default:
            break;
      }
//      Screen_EditByBit_PerFloor(pstParamEditMenu);
   }
}
/*-----------------------------------------------------------------------------
   This template should be used when editing a continuous range of
   parameters bitmap where each corresponds to a specific floor index
-----------------------------------------------------------------------------*/
void UI_ParamEditScreenTemplate_EditByBit_PerFloor(struct st_param_edit_menu * pstParamEditMenu)
{
   iMaxBitIndex = GetFP_NumFloors();

   ParamEditScreenTemplate_EditByBit(pstParamEditMenu);
   Screen_EditByBit_PerFloor(pstParamEditMenu);
}
/*-----------------------------------------------------------------------------
   This template should be used when editing a continuous range of
   parameters bitmap where each corresponds to a specific landing index
-----------------------------------------------------------------------------*/
void UI_ParamEditScreenTemplate_EditByBit_PerLanding(struct st_param_edit_menu * pstParamEditMenu)
{
   iMaxBitIndex = MAX_NUM_FLOORS;
   ParamEditScreenTemplate_EditByBit(pstParamEditMenu);
   Screen_EditByBit_PerLanding(pstParamEditMenu);
}
/*-----------------------------------------------------------------------------
   This template should be used when editing a continuous range of
   parameters bitmap where each corresponds to a specific hall board function index
-----------------------------------------------------------------------------*/
void UI_ParamEditScreenTemplate_EditByBit_PerHallMaskFunction(struct st_param_edit_menu * pstParamEditMenu)

{
   iMaxBitIndex = NUM_OF_HALLBOARD_BLOCKS;
   ParamEditScreenTemplate_EditByBit(pstParamEditMenu);
   Screen_EditByBit_PerHallFunction(pstParamEditMenu);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
