/******************************************************************************
 *
 * @file     ui_ffs_doors.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_Edit_Brakes_Time_10ms( uint32_t ulParamNumber, char* psTitle );
static void UI_FFS_Edit_Brakes_Voltage( uint32_t ulParamNumber, char* psTitle );
static void UI_FFS_Edit_Brakes_ParamBit( uint32_t ulParamNumber, char* psTitle );

static void UI_FFS_Brakes_DelayPick( void );
static void UI_FFS_Brakes_RampTime1( void );
static void UI_FFS_Brakes_RampTime2( void );
static void UI_FFS_Brakes_VoltagePick( void );
static void UI_FFS_Brakes_VoltageHold( void );
static void UI_FFS_Brakes_VoltageRelevel( void );
static void UI_FFS_Brakes_BPS_NC( void );
static void UI_FFS_DisableBPS_StopSeq( void );
static void UI_FFS_DisableBPS_StuckHigh( void );
static void UI_FFS_DisableBPS_StuckLow( void );
static void UI_FFS_BPS_Timeout( void );

static void UI_FFS_Brakes2_DelayPick( void );
static void UI_FFS_Brakes2_RampTime( void );
static void UI_FFS_Brakes2_VoltagePick( void );
static void UI_FFS_Brakes2_VoltageHold( void );
static void UI_FFS_Brakes2_VoltageRelevel( void );
static void UI_FFS_Brakes2_Enable( void );
static void UI_FFS_Brakes2_BPS_NC( void );
static void UI_FFS_DisableBPS2_StuckHigh( void );
static void UI_FFS_DisableBPS2_StuckLow( void );
static void UI_FFS_BPS2_Timeout( void );

static void UI_FFS_DisableLatchingBrakeFault( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
//---------------------------------------------------------------------

static struct st_ui_screen__freeform gstFFS_Brakes_DelayPick =
{
   .pfnDraw = UI_FFS_Brakes_DelayPick,
};
static struct st_ui_screen__freeform gstFFS_Brakes_RampTime1 =//Auto
{
   .pfnDraw = UI_FFS_Brakes_RampTime1,
};
static struct st_ui_screen__freeform gstFFS_Brakes_RampTime2 =//Insp
{
   .pfnDraw = UI_FFS_Brakes_RampTime2,
};
static struct st_ui_screen__freeform gstFFS_Brakes_VoltagePick =
{
   .pfnDraw = UI_FFS_Brakes_VoltagePick,
};
static struct st_ui_screen__freeform gstFFS_Brakes_VoltageHold =
{
   .pfnDraw = UI_FFS_Brakes_VoltageHold,
};
static struct st_ui_screen__freeform gstFFS_Brakes_VoltageRelevel =
{
   .pfnDraw = UI_FFS_Brakes_VoltageRelevel,
};
static struct st_ui_screen__freeform gstFFS_Brakes_BPS_NC =
{
   .pfnDraw = UI_FFS_Brakes_BPS_NC,
};
static struct st_ui_screen__freeform gstFFS_DisableBPS_StopSeq =
{
   .pfnDraw = UI_FFS_DisableBPS_StopSeq,
};
static struct st_ui_screen__freeform gstFFS_DisableBPS_StuckHigh =
{
   .pfnDraw = UI_FFS_DisableBPS_StuckHigh,
};
static struct st_ui_screen__freeform gstFFS_DisableBPS_StuckLow =
{
   .pfnDraw = UI_FFS_DisableBPS_StuckLow,
};
static struct st_ui_screen__freeform gstFFS_BPS_Timeout =
{
   .pfnDraw = UI_FFS_BPS_Timeout,
};
//-----------------------------
static struct st_ui_screen__freeform gstFFS_Brakes2_DelayPick =
{
   .pfnDraw = UI_FFS_Brakes2_DelayPick,
};
static struct st_ui_screen__freeform gstFFS_Brakes2_RampTime =
{
   .pfnDraw = UI_FFS_Brakes2_RampTime,
};

static struct st_ui_screen__freeform gstFFS_Brakes2_VoltagePick =
{
   .pfnDraw = UI_FFS_Brakes2_VoltagePick,
};
static struct st_ui_screen__freeform gstFFS_Brakes2_VoltageHold =
{
   .pfnDraw = UI_FFS_Brakes2_VoltageHold,
};
static struct st_ui_screen__freeform gstFFS_Brakes2_VoltageRelevel =
{
   .pfnDraw = UI_FFS_Brakes2_VoltageRelevel,
};
static struct st_ui_screen__freeform gstFFS_Brakes2_BPS_NC =
{
   .pfnDraw = UI_FFS_Brakes2_BPS_NC,
};
static struct st_ui_screen__freeform gstFFS_Brakes2_Enable =
{
   .pfnDraw = UI_FFS_Brakes2_Enable,
};
static struct st_ui_screen__freeform gstFFS_DisableBPS2_StuckHigh =
{
   .pfnDraw = UI_FFS_DisableBPS2_StuckHigh,
};
static struct st_ui_screen__freeform gstFFS_DisableBPS2_StuckLow =
{
   .pfnDraw = UI_FFS_DisableBPS2_StuckLow,
};
static struct st_ui_screen__freeform gstFFS_BPS2_Timeout =
{
   .pfnDraw = UI_FFS_BPS2_Timeout,
};

static struct st_ui_screen__freeform gstFFS_DisableLatchingBrakeFault =
{
   .pfnDraw = UI_FFS_DisableLatchingBrakeFault,
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Brakes_DelayPick =
{
   .psTitle = "Pick Time",
   .pstUGS_Next = &gstUGS_Brakes_DelayPick,
};

static struct st_ui_menu_item gstMI_Brakes_RampTime1 =//Auto
{
   .psTitle = "Ramp Time - Auto",
   .pstUGS_Next = &gstUGS_Brakes_RampTime1,
};
static struct st_ui_menu_item gstMI_Brakes_RampTime2 =//Insp
{
   .psTitle = "Ramp Time - Insp",
   .pstUGS_Next = &gstUGS_Brakes_RampTime2,
};
static struct st_ui_menu_item gstMI_Brakes_VoltagePick =
{
   .psTitle = "Pick Voltage",
   .pstUGS_Next = &gstUGS_Brakes_VoltagePick,
};
static struct st_ui_menu_item gstMI_Brakes_VoltageHold =
{
   .psTitle = "Hold Voltage",
   .pstUGS_Next = &gstUGS_Brakes_VoltageHold,
};
static struct st_ui_menu_item gstMI_Brakes_VoltageRelevel =
{
   .psTitle = "Relevel Voltage",
   .pstUGS_Next = &gstUGS_Brakes_VoltageRelevel,
};
static struct st_ui_menu_item gstMI_Brakes_BPS_NC =
{
   .psTitle = "BPS NC",
   .pstUGS_Next = &gstUGS_Brakes_BPS_NC,
};
static struct st_ui_menu_item gstMI_DisableBPS_StopSeq =
{
   .psTitle = "BPS - Stop Seq",
   .pstUGS_Next = &gstUGS_DisableBPS_StopSeq,
};
static struct st_ui_menu_item gstMI_DisableBPS_StuckHigh =
{
   .psTitle = "BPS - Stuck Active",
   .pstUGS_Next = &gstUGS_DisableBPS_StuckHigh,
};
static struct st_ui_menu_item gstMI_DisableBPS_StuckLow =
{
   .psTitle = "BPS - Stuck Inactive",
   .pstUGS_Next = &gstUGS_DisableBPS_StuckLow,
};
static struct st_ui_menu_item gstMI_BPS_Timeout =
{
   .psTitle = "BPS Timeout",
   .pstUGS_Next = &gstUGS_BPS_Timeout,
};
static struct st_ui_menu_item * gastMenuItems_Brakes[] =
{
   &gstMI_Brakes_VoltagePick,
   &gstMI_Brakes_VoltageHold,
   &gstMI_Brakes_VoltageRelevel,
   &gstMI_Brakes_DelayPick,
   &gstMI_Brakes_RampTime1,
   &gstMI_Brakes_RampTime2,
   &gstMI_Brakes_BPS_NC,
   &gstMI_DisableBPS_StopSeq,
   &gstMI_DisableBPS_StuckHigh,
   &gstMI_DisableBPS_StuckLow,
   &gstMI_BPS_Timeout,
};
static struct st_ui_screen__menu gstMenu_Brakes =
{
   .psTitle = "Brake Setup",
   .pastMenuItems = &gastMenuItems_Brakes,
   .ucNumItems = sizeof(gastMenuItems_Brakes) / sizeof(gastMenuItems_Brakes[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Brakes2_Enable =
{
   .psTitle = "Enable Secondary",
   .pstUGS_Next = &gstUGS_Brakes2_Enable,
};
static struct st_ui_menu_item gstMI_Brakes2_DelayPick =
{
   .psTitle = "Pick Time",
   .pstUGS_Next = &gstUGS_Brakes2_DelayPick,
};

static struct st_ui_menu_item gstMI_Brakes2_RampTime =
{
   .psTitle = "Ramp Time",
   .pstUGS_Next = &gstUGS_Brakes2_RampTime,
};
static struct st_ui_menu_item gstMI_Brakes2_VoltagePick =
{
   .psTitle = "Pick Voltage",
   .pstUGS_Next = &gstUGS_Brakes2_VoltagePick,
};
static struct st_ui_menu_item gstMI_Brakes2_VoltageHold =
{
   .psTitle = "Hold Voltage",
   .pstUGS_Next = &gstUGS_Brakes2_VoltageHold,
};
static struct st_ui_menu_item gstMI_Brakes2_VoltageRelevel =
{
   .psTitle = "Relevel Voltage",
   .pstUGS_Next = &gstUGS_Brakes2_VoltageRelevel,
};
static struct st_ui_menu_item gstMI_Brakes2_BPS_NC =
{
   .psTitle = "BPS NC",
   .pstUGS_Next = &gstUGS_Brakes2_BPS_NC,
};
static struct st_ui_menu_item gstMI_DisableBPS2_StuckHigh =
{
   .psTitle = "BPS - Stuck Active",
   .pstUGS_Next = &gstUGS_DisableBPS2_StuckHigh,
};
static struct st_ui_menu_item gstMI_DisableBPS2_StuckLow =
{
   .psTitle = "BPS - Stuck Inactive",
   .pstUGS_Next = &gstUGS_DisableBPS2_StuckLow,
};
static struct st_ui_menu_item gstMI_BPS2_Timeout =
{
   .psTitle = "BPS2 Timeout",
   .pstUGS_Next = &gstUGS_BPS2_Timeout,
};
static struct st_ui_menu_item * gastMenuItems_Brakes2[] =
{
   &gstMI_Brakes2_Enable,
   &gstMI_Brakes2_VoltagePick,
   &gstMI_Brakes2_VoltageHold,
   &gstMI_Brakes2_VoltageRelevel,
   &gstMI_Brakes2_DelayPick,
   &gstMI_Brakes2_RampTime,
   &gstMI_Brakes2_BPS_NC,
   &gstMI_DisableBPS2_StuckHigh,
   &gstMI_DisableBPS2_StuckLow,
   &gstMI_BPS2_Timeout,
};
static struct st_ui_screen__menu gstMenu_Brakes2 =
{
   .psTitle = "Secondary Brake Setup",
   .pastMenuItems = &gastMenuItems_Brakes2,
   .ucNumItems = sizeof(gastMenuItems_Brakes2) / sizeof(gastMenuItems_Brakes2[ 0 ]),
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_SetupAllBrakes_Primary =
{
   .psTitle = "Primary Setup",
   .pstUGS_Next = &gstUGS_Menu_Setup_Brakes,
};
static struct st_ui_menu_item gstMI_SetupAllBrakes_Secondary =
{
   .psTitle = "Secondary Setup",
   .pstUGS_Next = &gstUGS_Menu_Setup_Brakes2,
};
static struct st_ui_menu_item gstMI_DisableLatchingBrakeFault =
{
   .psTitle = "Disable Latching Fault",
   .pstUGS_Next = &gstUGS_DisableLatchingBrakeFault,
};
static struct st_ui_menu_item * gastMenuItems_SetupAllBrakes[] =
{
   &gstMI_SetupAllBrakes_Primary,
   &gstMI_SetupAllBrakes_Secondary,
   &gstMI_DisableLatchingBrakeFault,
};
static struct st_ui_screen__menu gstMenu_SetupAllBrakes =
{
   .psTitle = "Brake Setup",
   .pastMenuItems = &gastMenuItems_SetupAllBrakes,
   .ucNumItems = sizeof(gastMenuItems_SetupAllBrakes) / sizeof(gastMenuItems_SetupAllBrakes[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
//-----------------------------------------------------
struct st_ui_generic_screen gstUGS_Brakes_DelayPick =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Brakes_DelayPick,
};
struct st_ui_generic_screen gstUGS_Brakes_RampTime1 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Brakes_RampTime1,
};
struct st_ui_generic_screen gstUGS_Brakes_RampTime2 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Brakes_RampTime2,
};
struct st_ui_generic_screen gstUGS_Brakes_VoltagePick =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Brakes_VoltagePick,
};
struct st_ui_generic_screen gstUGS_Brakes_VoltageHold =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Brakes_VoltageHold,
};
struct st_ui_generic_screen gstUGS_Brakes_VoltageRelevel =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Brakes_VoltageRelevel,
};
struct st_ui_generic_screen gstUGS_Brakes_BPS_NC =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Brakes_BPS_NC,
};
struct st_ui_generic_screen gstUGS_DisableBPS_StopSeq=
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DisableBPS_StopSeq,
};
struct st_ui_generic_screen gstUGS_DisableBPS_StuckHigh=
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DisableBPS_StuckHigh,
};
struct st_ui_generic_screen gstUGS_DisableBPS_StuckLow=
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DisableBPS_StuckLow,
};
struct st_ui_generic_screen gstUGS_BPS_Timeout=
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_BPS_Timeout,
};
//---------------------------
struct st_ui_generic_screen gstUGS_Brakes2_DelayPick =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Brakes2_DelayPick,
};
struct st_ui_generic_screen gstUGS_Brakes2_RampTime =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Brakes2_RampTime,
};
struct st_ui_generic_screen gstUGS_Brakes2_VoltagePick =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Brakes2_VoltagePick,
};
struct st_ui_generic_screen gstUGS_Brakes2_VoltageHold =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Brakes2_VoltageHold,
};
struct st_ui_generic_screen gstUGS_Brakes2_VoltageRelevel =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Brakes2_VoltageRelevel,
};
struct st_ui_generic_screen gstUGS_Brakes2_BPS_NC =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Brakes2_BPS_NC,
};
struct st_ui_generic_screen gstUGS_Brakes2_Enable =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Brakes2_Enable,
};
struct st_ui_generic_screen gstUGS_DisableBPS2_StuckHigh=
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DisableBPS2_StuckHigh,
};
struct st_ui_generic_screen gstUGS_DisableBPS2_StuckLow=
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DisableBPS2_StuckLow,
};
struct st_ui_generic_screen gstUGS_BPS2_Timeout=
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_BPS2_Timeout,
};
//------------------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_Setup_Brakes =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Brakes,
};
struct st_ui_generic_screen gstUGS_Menu_Setup_Brakes2 =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Brakes2,
};
struct st_ui_generic_screen gstUGS_Menu_SetupAllBrakes =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_SetupAllBrakes,
};

struct st_ui_generic_screen gstUGS_DisableLatchingBrakeFault =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DisableLatchingBrakeFault,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/


 /*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Edit_Brakes_Time_10ms( uint32_t ulParamNumber, char* psTitle )
{
   static struct st_param_edit_menu stParamEdit;

   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = ulParamNumber;
   stParamEdit.uwParamIndex_End   = stParamEdit.uwParamIndex_Start;
   stParamEdit.psTitle = psTitle;
   stParamEdit.psUnit = "0 msec";

   UI_ParamEditSceenTemplate( &stParamEdit );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Edit_Brakes_Voltage( uint32_t ulParamNumber, char* psTitle )
{
   static struct st_param_edit_menu stParamEdit;

   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = ulParamNumber;
   stParamEdit.uwParamIndex_End   = stParamEdit.uwParamIndex_Start;
   stParamEdit.psTitle = psTitle;
   stParamEdit.psUnit  = " vdc";

   UI_ParamEditSceenTemplate( &stParamEdit );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Edit_Brakes_ParamBit( uint32_t ulParamNumber, char* psTitle )
{
   static struct st_param_edit_menu stParamEdit;

   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = ulParamNumber;
   stParamEdit.uwParamIndex_End   = stParamEdit.uwParamIndex_Start;
   stParamEdit.psTitle = psTitle;

   UI_ParamEditSceenTemplate( &stParamEdit );
}

 /*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Brakes_DelayPick( void )
{
   UI_FFS_Edit_Brakes_Time_10ms( enPARAM8__BrakePickDelay_10ms, "Brake Pick Time" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Brakes_RampTime1( void )
{
   UI_FFS_Edit_Brakes_Time_10ms( enPARAM8__BrakeRampTime_Auto_10ms, "Brake Ramp Time (Auto)" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Brakes_RampTime2( void )
{
   UI_FFS_Edit_Brakes_Time_10ms( enPARAM8__BrakeRampTime_Insp_10ms, "Brake Ramp Time (Insp)" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Brakes_VoltagePick( void )
{
   UI_FFS_Edit_Brakes_Voltage( enPARAM8__BrakePickVoltage, "Brake Pick Voltage" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Brakes_VoltageHold( void )
{
   UI_FFS_Edit_Brakes_Voltage( enPARAM8__BrakeHoldVoltage, "Brake Hold Voltage" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Brakes_VoltageRelevel( void )
{
   UI_FFS_Edit_Brakes_Voltage( enPARAM8__BrakeRelevelVoltage, "Brake Relevel Voltage" );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Brakes_BPS_NC( void )
{
   UI_FFS_Edit_Brakes_ParamBit( enPARAM1__BPS_PRIM_NC, "Brake BPS NC" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Brakes2_DelayPick( void )
{
   UI_FFS_Edit_Brakes_Time_10ms( enPARAM8__SecBrakePickDelay_10ms, "Brake Pick Time" );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Brakes2_RampTime( void )
{
   UI_FFS_Edit_Brakes_Time_10ms( enPARAM8__SecBrakeRampTime_10ms, "Brake Ramp Time" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Brakes2_VoltagePick( void )
{
   UI_FFS_Edit_Brakes_Voltage( enPARAM8__SecBrakePickVoltage, "Brake Pick Voltage" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Brakes2_VoltageHold( void )
{
   UI_FFS_Edit_Brakes_Voltage( enPARAM8__SecBrakeHoldVoltage, "Brake Hold Voltage" );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Brakes2_VoltageRelevel( void )
{
   UI_FFS_Edit_Brakes_Voltage( enPARAM8__SecBrakeRelevelVoltage, "Brake Relevel Voltage" );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Brakes2_BPS_NC( void )
{
   UI_FFS_Edit_Brakes_ParamBit( enPARAM1__BPS_SEC_NC, "Brake Pick Switch NC" );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Brakes2_Enable( void )
{
   UI_FFS_Edit_Brakes_ParamBit( enPARAM1__EnableSecondaryBrake, "Enable Secondary Brake" );
}

static void UI_FFS_DisableBPS_StopSeq( void )
{
   UI_FFS_Edit_Brakes_ParamBit( enPARAM1__DisableBPS_StopSeq, "Disable BPS Stop Seq" );
}

static void UI_FFS_DisableBPS_StuckHigh( void )
{
   UI_FFS_Edit_Brakes_ParamBit( enPARAM1__DisableBPS_StuckHigh, "Disable BPS Active" );
}

static void UI_FFS_DisableBPS_StuckLow( void )
{
   UI_FFS_Edit_Brakes_ParamBit( enPARAM1__DisableBPS_StuckLow, "Disable BPS Inactive" );
}

static void UI_FFS_DisableBPS2_StuckHigh( void )
{
   UI_FFS_Edit_Brakes_ParamBit( enPARAM1__DisableBPS2_StuckHigh, "Disable BPS Active" );
}

static void UI_FFS_DisableBPS2_StuckLow( void )
{
   UI_FFS_Edit_Brakes_ParamBit( enPARAM1__DisableBPS2_StuckLow, "Disable BPS Inactive" );
}

static void UI_FFS_BPS_Timeout( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__BPS_Timeout_100ms;
   stParamEdit.uwParamIndex_End = enPARAM8__BPS_Timeout_100ms;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_One;
   stParamEdit.psTitle = "BPS Timeout";
   stParamEdit.psUnit = " sec";
   stParamEdit.ucValueOffset = 30;
   stParamEdit.ulValue_Max = 225;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
static void UI_FFS_BPS2_Timeout( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__BPS2_Timeout_100ms;
   stParamEdit.uwParamIndex_End = enPARAM8__BPS2_Timeout_100ms;
   stParamEdit.enDecimalFormat = enNumberDecimalPoints_One;
   stParamEdit.psTitle = "BPS Timeout";
   stParamEdit.psUnit = " sec";
   stParamEdit.ucValueOffset = 30;
   stParamEdit.ulValue_Max = 225;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_DisableLatchingBrakeFault( void )
{
   UI_FFS_Edit_Brakes_ParamBit( enPARAM1__DisableLatchingBrakeFault, "Disable Latching Fault" );
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
