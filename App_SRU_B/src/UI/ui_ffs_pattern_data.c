
/******************************************************************************
 *
 * @file     ui_menu_io.c
 * @brief    I/O Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "GlobalData.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_PatternData( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
//---------------------------------------------------------------

static struct st_ui_screen__freeform gstFFS_PatternData =
{
   .pfnDraw = UI_FFS_PatternData,
};


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_PatternData =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_PatternData,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

enum en_pattern_data
{
   PATTERN_DATA__RampUp,
   PATTERN_DATA__Slowdown,
   PATTERN_DATA__ActualDestination,
   PATTERN_DATA__RequestedDestination,

   NUM_PATTERN_DATA
};
static char const * const pasDataStrings[NUM_PATTERN_DATA] =
{
      "RampUp: ",
      "Slowdown: ",
      "A. Dest: ",
      "R. Dest: ",
};
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
static void Print_Screen(uint8_t ucCursor)
{
   uint8_t ucIndex;

   for(ucIndex = 0; ucIndex < 3; ucIndex++)//num lines
   {
      uint8_t ucLine = ucCursor + ucIndex;
      if(ucLine < NUM_PATTERN_DATA)//max bitmap size
      {
         LCD_Char_GotoXY( 0, 1+ucIndex );

         const char *ps = pasDataStrings[ucLine];
         LCD_Char_WriteString(ps);

         switch(ucLine)
         {
            case PATTERN_DATA__RampUp:
               LCD_Char_WriteInteger_MinLength(GetDebugPatternData_RampUpDistance(), 1);
               break;
            case PATTERN_DATA__Slowdown:
               LCD_Char_WriteInteger_MinLength(GetDebugPatternData_SlowdownDistance(), 1);
               break;
            case PATTERN_DATA__ActualDestination:
               LCD_Char_WriteInteger_MinLength(GetMotion_Destination(), 1);
               break;
            case PATTERN_DATA__RequestedDestination:
               LCD_Char_WriteInteger_MinLength(GetOperation_RequestedDestination(), 1);
               break;
            default:
               break;
         }
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_PatternData( void )
{
   static uint8_t ucCursorY = 0;

   LCD_Char_Clear();
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper("Pattern Data");

   Print_Screen(ucCursorY);

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      if(ucCursorY)
      {
         ucCursorY--;
      }
   }
   else if ( enKeypress == enKEYPRESS_DOWN)
   {
      uint8_t ucLimit = (NUM_PATTERN_DATA > 3) ? NUM_PATTERN_DATA-3:0;

      if(ucCursorY < ucLimit)//Minus cursor scroll+1
      {
         ucCursorY++;
      }
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

