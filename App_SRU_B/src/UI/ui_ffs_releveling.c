/******************************************************************************
 *
 * @file     ui_ffs_cc_bl.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include <stdint.h>
#include "sys.h"
#include "math.h"

#if 0
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_Relevel_TooHigh( void );
static void UI_FFS_Relevel_DeadZone( void );
static void UI_FFS_Relevel_Releveling( void );
static void UI_FFS_Relevel_UpStop( void );
static void UI_FFS_Relevel_DownStop( void );

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_ui_screen__freeform gstFFS_Relevel_TooHigh =
{
   .pfnDraw = UI_FFS_Relevel_TooHigh,
};
static struct st_ui_screen__freeform gstFFS_Relevel_DeadZone =
{
   .pfnDraw = UI_FFS_Relevel_DeadZone,
};
static struct st_ui_screen__freeform gstFFS_Relevel_Releveling =
{
   .pfnDraw = UI_FFS_Relevel_Releveling,
};
static struct st_ui_screen__freeform gstFFS_Relevel_RelevelingDelay =
{
   .pfnDraw = UI_FFS_Relevel_RelevelingDelay,
};
static struct st_ui_screen__freeform gstFFS_Relevel_UpStop =
{
   .pfnDraw = UI_FFS_Relevel_UpStop,
};
static struct st_ui_screen__freeform gstFFS_Relevel_DownStop =
{
   .pfnDraw = UI_FFS_Relevel_DownStop,
};

static uint8_t gucFieldIndex;
static uint16_t guwFieldSize = 1; // always 1
static uint8_t gucIndexFieldSize = 1;// param index

static int8_t gcBlockIndex_Current = 4;
static int8_t gcBlockIndex_Start = 4;
static int8_t gcBlockIndex_End = 4;

static int16_t gwParamIndex_Current = enPARAM16__CC_ButtonAndLamp_Start;
static int16_t gwParamIndex_Start = enPARAM16__CC_ButtonAndLamp_Start;
static int16_t gwParamIndex_End = enPARAM16__CC_ButtonAndLamp_End;

static uint16_t guwMaxValue;

// Number of digits in the editable field of the parameter value. For example,
// when editing a 16-bit parameter in hex format, there are 4 value digits.
static uint8_t gucNumValueDigits;

// Number of bytes used to store the parameter value. This value is used,
// among other things, to determine the max value the parameter can hold.
static uint8_t gucNumBytes;


static uint8_t gucCursorColumn;

static int8_t gbEditsMade;

// Indicates parameter changed without this user saving. Likely another user
// on another board changed the value or maybe a learned value was automatically
// saved.
//static int8_t gbParamChangedInBackground;

static int8_t gbValidParamSelected;

static uint8_t gucEditFormat;

static enum en_param_block_types genBlockType;

static uint32_t guiCurrentParam_EditedValue;
static uint32_t guiCurrentParam_SavedValue;

/* Partial parameter selection*/
static uint8_t gucButtonsNotLamps;
static uint8_t gucShift;
static uint32_t gulMask = 0x0000000F;


static uint8_t gucBoard_Current;
static uint8_t gucValue_Current;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_Relevel_TooHigh =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Relevel_TooHigh,
};
struct st_ui_generic_screen gstUGS_Relevel_DeadZone =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Relevel_DeadZone,
};
struct st_ui_generic_screen gstUGS_Relevel_Releveling =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Relevel_Releveling,
};
struct st_ui_generic_screen gstUGS_Relevel_RelevelingDelay =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Relevel_RelevelingDelay,
};
struct st_ui_generic_screen gstUGS_Relevel_UpStop =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Relevel_UpStop,
};
struct st_ui_generic_screen gstUGS_Relevel_DownStop =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Relevel_DownStop,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

static void Update_Screen(void)
{
   LCD_Char_Clear();

   LCD_Char_GotoXY( 0, 0 );

   //------------------------------------------------------------------
   const char *ps;
   const char *ps_IO;

   if ( !gbValidParamSelected )
   {
      ps = "Invalid Selection";
   }
   else
   {
      switch(gucButtonsNotLamps)
      {
         case 1:
            ps = "Edit CC Buttons";
            LCD_Char_WriteStringUpper( ps );
            LCD_Char_GotoXY(12,2);
            ps_IO = "\"5";
            LCD_Char_WriteStringUpper( ps_IO );
            LCD_Char_GotoXY(14,2);
            LCD_Char_WriteInteger_ExactLength(gucValue_Current+1,2);
            LCD_Char_GotoXY(16,2);
            ps_IO = "\"";
            LCD_Char_WriteStringUpper( ps_IO );
            break;
         case 0:
            ps = "Edit CC Lamps";
            LCD_Char_WriteStringUpper( ps );
            LCD_Char_GotoXY(12,2);
            ps_IO = "\"6  \"";
            LCD_Char_WriteStringUpper( ps_IO );
            LCD_Char_GotoXY(14,2);
            LCD_Char_WriteInteger_ExactLength(gucValue_Current+1,2);
            LCD_Char_GotoXY(16,2);
            ps_IO = "\"";
            LCD_Char_WriteStringUpper( ps_IO );
            break;
         default:
            ps = "Invalid";
            LCD_Char_WriteStringUpper( ps );
            break;
      }
   }

   //------------------------------------------------------------------
   if(gucCursorColumn > 18)
   {
      LCD_Char_GotoXY( 18, 3 );
   }
   else
   {
      LCD_Char_GotoXY( gucCursorColumn+1, 3 );
   }
   LCD_Char_WriteChar( '*' );

   LCD_Char_GotoXY( 0, 2);
   LCD_Char_WriteStringUpper("FL");
   LCD_Char_GotoXY( 3, 2 );
   LCD_Char_WriteInteger_ExactLength( gwParamIndex_Current-gwParamIndex_Start+1, 2 );
   LCD_Char_GotoXY( 5, 2 );
   LCD_Char_WriteStringUpper(":");
   if ( !gbValidParamSelected )
   {
      LCD_Char_WriteString( "(Invalid)" );
   }
   else
   {
      LCD_Char_GotoXY( 6, 2 );
      LCD_Char_WriteString(gpasBoards[gucBoard_Current]);
   }

   if ( guiCurrentParam_SavedValue != guiCurrentParam_EditedValue )
   {
      LCD_Char_GotoXY( 16, 1 );
      LCD_Char_WriteString( "Save" );

      LCD_Char_GotoXY( 19, 2 );
      LCD_Char_WriteChar( '|' );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Value( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex;

   uint8_t ucShift = gucShift;
   int32_t lCurrentParam_EditedValue = guiCurrentParam_EditedValue;
   int32_t lCurrentParam_PartialValue = (guiCurrentParam_EditedValue >> ucShift) & gulMask;
   guiCurrentParam_EditedValue &= !(gulMask << ucShift);
   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = gucNumValueDigits - 1;
      }
   }
   else  // enACTION__EDIT
   {
      //------------------------------------------------------------------
      if ( gucNumValueDigits == 0 )
      {
         lCurrentParam_PartialValue = 0;

         enReturnValue = enACTION__EXIT_TO_LEFT;
      }
      else
      {
         int32_t uiDigitWeight = 1;
         gucCursorColumn = 14 + ucCursorIndex;
         //------------------------------------------------------------------
         switch ( enKeypress )
         {
            case enKEYPRESS_UP:
               lCurrentParam_PartialValue += uiDigitWeight;

               gbEditsMade = 1;
               break;

            case enKEYPRESS_DOWN:
               lCurrentParam_PartialValue -= uiDigitWeight;

               gbEditsMade = 1;
               break;

            case enKEYPRESS_LEFT:
               if ( ucCursorIndex )
               {
                  --ucCursorIndex;
               }
               else
               {
                  enReturnValue = enACTION__EXIT_TO_LEFT;
               }
               break;

            case enKEYPRESS_RIGHT:
               if ( ucCursorIndex < (gucNumValueDigits - 1) )
               {
                  ++ucCursorIndex;
               }
               else
               {
                  enReturnValue = enACTION__EXIT_TO_RIGHT;
               }
               break;

            default:
               ;
         }

         if ( lCurrentParam_PartialValue < 0 )
         {
            lCurrentParam_PartialValue = 0;
         }
         else
         {
            if ( lCurrentParam_PartialValue > guwMaxValue )
            {
               lCurrentParam_PartialValue = guwMaxValue;
            }
         }
      }
   }
   uint32_t ulCurrentParam_PartialValue = (uint32_t) lCurrentParam_PartialValue;

   gucValue_Current= (uint8_t) lCurrentParam_PartialValue;
   guiCurrentParam_EditedValue |= (ulCurrentParam_PartialValue & gulMask) << ucShift;

   return enReturnValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Board( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex;

   uint8_t ucShift = gucShift + 4;
//   int32_t lCurrentParam_EditedValue = guiCurrentParam_EditedValue;
   int32_t lCurrentParam_PartialValue = (guiCurrentParam_EditedValue >> ucShift) & gulMask;

   guiCurrentParam_EditedValue &= !(gulMask << ucShift);
   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = gucNumValueDigits - 1;
      }
   }
   else  // enACTION__EDIT
   {
      //------------------------------------------------------------------
      if ( gucNumValueDigits == 0 )
      {
         lCurrentParam_PartialValue = 0;

         enReturnValue = enACTION__EXIT_TO_LEFT;
      }
      else
      {
         int32_t uiDigitWeight = 1;
         gucCursorColumn = 7 + ucCursorIndex;
         //------------------------------------------------------------------
         switch ( enKeypress )
         {
            case enKEYPRESS_UP:
               lCurrentParam_PartialValue += uiDigitWeight;

               gbEditsMade = 1;
               break;

            case enKEYPRESS_DOWN:
               lCurrentParam_PartialValue -= uiDigitWeight;

               gbEditsMade = 1;
               break;

            case enKEYPRESS_LEFT:
               if ( ucCursorIndex )
               {
                  --ucCursorIndex;
               }
               else
               {
                  enReturnValue = enACTION__EXIT_TO_LEFT;
               }
               break;

            case enKEYPRESS_RIGHT:
               if ( ucCursorIndex < (gucNumValueDigits - 1) )
               {
                  ++ucCursorIndex;
               }
               else
               {
                  enReturnValue = enACTION__EXIT_TO_RIGHT;
               }
               break;

            default:
               ;
         }

         if ( lCurrentParam_PartialValue < 0 )
         {
            lCurrentParam_PartialValue = 0;
         }
         else if ( lCurrentParam_PartialValue >= MAX__CC_BOARDS )
         {
            lCurrentParam_PartialValue = MAX__CC_BOARDS-1;
         }
      }
   }
   uint32_t ulCurrentParam_PartialValue = (uint32_t) lCurrentParam_PartialValue;
   gucBoard_Current = (uint8_t) lCurrentParam_PartialValue;

   guiCurrentParam_EditedValue |= (ulCurrentParam_PartialValue & gulMask) << ucShift;

   return enReturnValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Index( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = gucIndexFieldSize-1;
      }
   }
   else  // enACTION__EDIT
   {
//      gucCursorColumn = gucIndexFieldSize - 1 + ucCursorIndex;
      gucCursorColumn = 5;

      //------------------------------------------------------------------
      int32_t uiDigitWeight = gucIndexFieldSize - 1 - ucCursorIndex;

      uiDigitWeight = pow( 10, uiDigitWeight );  // 10 to the power of uiDigitWeight

      //------------------------------------------------------------------
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            gwParamIndex_Current += uiDigitWeight;
            break;

         case enKEYPRESS_DOWN:
            gwParamIndex_Current -= uiDigitWeight;
            break;

         case enKEYPRESS_LEFT:
            if ( ucCursorIndex )
            {
               --ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_LEFT;
            }
            break;

         case enKEYPRESS_RIGHT:
            if ( ucCursorIndex < gucIndexFieldSize - 1 )
            {
               ++ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;

         default:
            ;
      }
   }

   //------------------------------------------------------------------
   if ( gwParamIndex_Current < gwParamIndex_Start )
   {
      gwParamIndex_Current = gwParamIndex_Start;
   }
   else if ( gwParamIndex_Current > gwParamIndex_End )
   {
      gwParamIndex_Current = gwParamIndex_End;
   }

   return enReturnValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void SaveParameter( void )
{
   struct st_param_and_value stParamAndValue;

   stParamAndValue.ucBlockIndex = gcBlockIndex_Current;
   stParamAndValue.uwParamIndex = gwParamIndex_Current;
   stParamAndValue.uiValue = guiCurrentParam_EditedValue;

   SaveAndVerifyParameter( &stParamAndValue );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Save( enum en_keypresses enKeypress, enum en_field_action enFieldAction )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t bSaveInProgress;

   if (    !gbEditsMade
        || (guiCurrentParam_EditedValue == guiCurrentParam_SavedValue)
      )
   {
      bSaveInProgress = 0;

      gbEditsMade = 0;
      enReturnValue = enACTION__EXIT_TO_LEFT;
   }
   else
   {
      gucCursorColumn = 19;

      if ( enFieldAction == enACTION__EDIT )
      {
         if ( enKeypress == enKEYPRESS_ENTER )
         {
            bSaveInProgress = 1;
         }
         else if ( enKeypress == enKEYPRESS_LEFT )
         {
            bSaveInProgress = 0;

            enReturnValue = enACTION__EXIT_TO_LEFT;
         }
      }

      // Keep transmitting save request until we receive confimation that new
      // value was saved.
      if ( bSaveInProgress )
      {
         SaveParameter();
      }
   }

   return enReturnValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t GetParamInfo_CC_BL( int8_t cBlockIndex, int16_t wParamIndex )
{
   uint8_t bSelectionChanged = 0;


   static int16_t wParamIndex_Old = -1;  // force to initially appear changed
   static int8_t cBlockIndex_Old = -1;  // force to initially appear changed

   //Keep param index within range
   if( gwParamIndex_Current > gwParamIndex_End )
   {
      gwParamIndex_Current = gwParamIndex_End;
   }
   else if( gwParamIndex_Current < gwParamIndex_Start )
   {
      gwParamIndex_Current = gwParamIndex_Start;
   }
   if( gcBlockIndex_Current > gcBlockIndex_End )
   {
      gcBlockIndex_Current = gcBlockIndex_End;
   }
   else if( gcBlockIndex_Current < gcBlockIndex_Start )
   {
      gcBlockIndex_Current = gcBlockIndex_Start;
   }



   if ( cBlockIndex_Old != cBlockIndex )
   {
      cBlockIndex_Old = cBlockIndex;
      bSelectionChanged = 1;
   }

   if ( wParamIndex_Old != wParamIndex )
   {
      wParamIndex_Old = wParamIndex;
      bSelectionChanged = 1;
   }

   if ( bSelectionChanged )
   {
      gbEditsMade = 0;

      gbValidParamSelected = Sys_Param_IsValid( cBlockIndex, wParamIndex );

      if ( gbValidParamSelected )
      {
         genBlockType = Sys_Param_BlockType( cBlockIndex );
         gucNumBytes = 2;
         gucNumValueDigits = 1;
         guwMaxValue = 0xF;
      }
   }



   struct st_param_and_value stParamAndValue;
   stParamAndValue.ucBlockIndex = cBlockIndex;
   stParamAndValue.uwParamIndex = wParamIndex;
   guiCurrentParam_SavedValue = Param_ReadValue( &stParamAndValue );

   if ( !gbEditsMade )
   {
      guiCurrentParam_EditedValue = guiCurrentParam_SavedValue;
   }

   return 0;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

static void Edit_Parameter(void)
{
   GetParamInfo_CC_BL( gcBlockIndex_Current,gwParamIndex_Current );

   if ( gucFieldIndex == enFIELD__NONE )
   {
      guiCurrentParam_EditedValue = guiCurrentParam_SavedValue;

      Edit_Board( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT );

      gucFieldIndex = enFIELD__BOARD;
   }
   else
   {
      enum en_field_action enFieldAction;

      enum en_keypresses enKeypress = Button_GetKeypress();


         //------------------------------------------------------------------
      switch ( gucFieldIndex )
      {
         case enFIELD__INDEX:

            enFieldAction = Edit_Index( enKeypress, enACTION__EDIT );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               PrevScreen();

               gucFieldIndex = enFIELD__NONE;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               Edit_Board( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT );

               gucFieldIndex = enFIELD__BOARD;
            }
            break;
            //------------------------------------------------------------------
         case enFIELD__BOARD:

               enFieldAction = Edit_Board( enKeypress, enACTION__EDIT );

               if ( enFieldAction == enACTION__EXIT_TO_LEFT )
               {
                  Edit_Index( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT );

                  gucFieldIndex = enFIELD__INDEX;
               }
               else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
               {
                  Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT );

                  gucFieldIndex = enFIELD__VALUE;
               }
               break;
         //------------------------------------------------------------------
         case enFIELD__VALUE:

            enFieldAction = Edit_Value( enKeypress, enACTION__EDIT );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               Edit_Board( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT );

               gucFieldIndex = enFIELD__BOARD;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Save( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT );

               gucFieldIndex = enFIELD__SAVE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__SAVE:

            enFieldAction = Edit_Save( enKeypress, enACTION__EDIT );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT );

               gucFieldIndex = enFIELD__VALUE;
            }
            break;
      }
   }
   Update_Screen();
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_CC_Buttons( void )
{
   gucShift = 0;
   gucButtonsNotLamps = 1;

   Edit_Parameter();
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_CC_Lamps( void )
{
   gucShift = 8;
   gucButtonsNotLamps = 0;

   Edit_Parameter();
}
#endif
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
