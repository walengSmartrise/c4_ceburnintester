/******************************************************************************
 *
 * @file     ui_ffs_loadweigher.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     20, Dec 2018
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_LoadWeigher_Select( void );
static void UI_FFS_LWD_EnableWifi( void );
static void UI_FFS_LWD_Debug( void );
static void UI_FFS_LWD_AutoRecalibrate( void );
static void UI_FFS_LWD_TriggerRecalibrate( void );
static void UI_FFS_LWD_TriggerLoadLearn( void );
static void UI_FFS_LWD_TorqueScaling( void );
static void UI_FFS_LWD_TorqueOffset( void );
static void UI_FFS_LWD_MonthlyCalibrationDay( void );
static void UI_FFS_LWD_MonthlyCalibrationHour( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_ui_screen__freeform gstFFS_LoadWeigher_Select =
{
   .pfnDraw = UI_FFS_LoadWeigher_Select,
};
static struct st_ui_screen__freeform gstFFS_LWD_EnableWifi =
{
   .pfnDraw = UI_FFS_LWD_EnableWifi,
};
static struct st_ui_screen__freeform gstFFS_LWD_Debug =
{
   .pfnDraw = UI_FFS_LWD_Debug,
};
static struct st_ui_screen__freeform gstFFS_LWD_AutoRecalibrate =
{
   .pfnDraw = UI_FFS_LWD_AutoRecalibrate,
};
static struct st_ui_screen__freeform gstFFS_LWD_TriggerRecalibrate =
{
   .pfnDraw = UI_FFS_LWD_TriggerRecalibrate,
};
static struct st_ui_screen__freeform gstFFS_LWD_TriggerLoadLearn =
{
   .pfnDraw = UI_FFS_LWD_TriggerLoadLearn,
};
static struct st_ui_screen__freeform gstFFS_LWD_TorqueScaling =
{
   .pfnDraw = UI_FFS_LWD_TorqueScaling,
};
static struct st_ui_screen__freeform gstFFS_LWD_TorqueOffset =
{
   .pfnDraw = UI_FFS_LWD_TorqueOffset,
};
static struct st_ui_screen__freeform gstFFS_LWD_MonthlyCalibrationDay =
{
   .pfnDraw = UI_FFS_LWD_MonthlyCalibrationDay,
};
static struct st_ui_screen__freeform gstFFS_LWD_MonthlyCalibrationHour =
{
   .pfnDraw = UI_FFS_LWD_MonthlyCalibrationHour,
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_LoadWeigher_Select =
{
   .psTitle = "Type Select",
   .pstUGS_Next = &gstUGS_LoadWeigher_Select,
};
static struct st_ui_menu_item gstMI_LWD_EnableWifi =
{
   .psTitle = "Enable WiFi",
   .pstUGS_Next = &gstUGS_LWD_EnableWifi,
};
static struct st_ui_menu_item gstMI_LWD_Debug =
{
   .psTitle = "Debug",
   .pstUGS_Next = &gstUGS_LWD_Debug,
};
static struct st_ui_menu_item gstMI_LWD_AutoRecalibrate =
{
   .psTitle = "Auto Recalibrate",
   .pstUGS_Next = &gstUGS_LWD_AutoRecalibrate,
};
static struct st_ui_menu_item gstMI_LWD_TriggerRecalibrate =
{
   .psTitle = "Trigger Recalibrate",
   .pstUGS_Next = &gstUGS_LWD_TriggerRecalibrate,
};
static struct st_ui_menu_item gstMI_LWD_TriggerLoadLearn =
{
   .psTitle = "Trigger Load Learn",
   .pstUGS_Next = &gstUGS_LWD_TriggerLoadLearn,
};
static struct st_ui_menu_item gstMI_LWD_TorqueScaling =
{
   .psTitle = "Torque Scaling",
   .pstUGS_Next = &gstUGS_LWD_TorqueScaling,
};
static struct st_ui_menu_item gstMI_LWD_TorqueOffset =
{
   .psTitle = "Torque Offset",
   .pstUGS_Next = &gstUGS_LWD_TorqueOffset,
};
static struct st_ui_menu_item gstMI_LWD_Requests =
{
   .psTitle = "Enter Requests",
   .pstUGS_Next = &gstUGS_LWD_Req,
};
static struct st_ui_menu_item gstMI_LWD_MonthlyCalibrationHour =
{
   .psTitle = "Monthly Calib. Hour",
   .pstUGS_Next = &gstUGS_LWD_MonthlyCalibrationHour,
};
static struct st_ui_menu_item gstMI_LWD_MonthlyCalibrationDay =
{
   .psTitle = "Monthly Calib. Day",
   .pstUGS_Next = &gstUGS_LWD_MonthlyCalibrationDay,
};
static struct st_ui_menu_item * gastMenuItems_LoadWeigher[] =
{
   &gstMI_LoadWeigher_Select,
   &gstMI_LWD_EnableWifi,
   &gstMI_LWD_Debug,
   &gstMI_LWD_AutoRecalibrate,
   &gstMI_LWD_MonthlyCalibrationDay,
   &gstMI_LWD_MonthlyCalibrationHour,
   &gstMI_LWD_TriggerRecalibrate,
   &gstMI_LWD_TriggerLoadLearn,
   &gstMI_LWD_TorqueScaling,
   &gstMI_LWD_TorqueOffset,
   &gstMI_LWD_Requests,
};
static struct st_ui_screen__menu gstMenu_LoadWeigher =
{
   .psTitle = "Load Weigher Setup",
   .pastMenuItems = &gastMenuItems_LoadWeigher,
   .ucNumItems = sizeof(gastMenuItems_LoadWeigher) / sizeof(gastMenuItems_LoadWeigher[ 0 ]),
};

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
//-----------------------------------------------------
struct st_ui_generic_screen gstUGS_LoadWeigher_Select =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_LoadWeigher_Select,
};
struct st_ui_generic_screen gstUGS_LWD_EnableWifi =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_LWD_EnableWifi,
};
struct st_ui_generic_screen gstUGS_LWD_Debug =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_LWD_Debug,
};
struct st_ui_generic_screen gstUGS_LWD_AutoRecalibrate =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_LWD_AutoRecalibrate,
};
struct st_ui_generic_screen gstUGS_LWD_TriggerRecalibrate =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_LWD_TriggerRecalibrate,
};
struct st_ui_generic_screen gstUGS_LWD_TriggerLoadLearn =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_LWD_TriggerLoadLearn,
};
struct st_ui_generic_screen gstUGS_LWD_TorqueScaling =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_LWD_TorqueScaling,
};
struct st_ui_generic_screen gstUGS_LWD_TorqueOffset =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_LWD_TorqueOffset,
};
struct st_ui_generic_screen gstUGS_LWD_MonthlyCalibrationHour =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_LWD_MonthlyCalibrationHour,
};
struct st_ui_generic_screen gstUGS_LWD_MonthlyCalibrationDay =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_LWD_MonthlyCalibrationDay,
};

struct st_ui_generic_screen gstUGS_Menu_Setup_LoadWeigher =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_LoadWeigher,
};


/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static char *(LoadWeigherSelectArray[]) =
{
      "Discrete",
      "Serial MR",
      "Serial CT",
};
static void UI_FFS_LoadWeigher_Select( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__LoadWeigherSelect;
   stParamEdit.uwParamIndex_End = enPARAM8__LoadWeigherSelect;
   stParamEdit.psTitle = "Location Select";
   stParamEdit.ucaPTRChoiceList = &LoadWeigherSelectArray;
   stParamEdit.ulValue_Max = (sizeof(LoadWeigherSelectArray)/sizeof(*LoadWeigherSelectArray)) - 1;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_LWD_EnableWifi( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__LWD_EnableWifi;
   stParamEdit.uwParamIndex_End = enPARAM1__LWD_EnableWifi;
   stParamEdit.psTitle = "Enable WiFi";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_LWD_Debug( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__Debug_LWD;
   stParamEdit.uwParamIndex_End = enPARAM1__Debug_LWD;
   stParamEdit.psTitle = "Debug LWD";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_LWD_AutoRecalibrate( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__LWD_AutoRecalibrate;
   stParamEdit.uwParamIndex_End = enPARAM1__LWD_AutoRecalibrate;
   stParamEdit.psTitle = "Auto Recalibrate";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_LWD_TriggerRecalibrate( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__LWD_TriggerRecalibrate;
   stParamEdit.uwParamIndex_End = enPARAM1__LWD_TriggerRecalibrate;
   stParamEdit.psTitle = "Trigger Recalibrate";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_LWD_TriggerLoadLearn( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__LWD_TriggerLoadLearn;
   stParamEdit.uwParamIndex_End = enPARAM1__LWD_TriggerLoadLearn;
   stParamEdit.psTitle = "Trigger Load Learn";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_LWD_TorqueScaling( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__LWD_TorqueScaling;
   stParamEdit.uwParamIndex_End = enPARAM8__LWD_TorqueScaling;
   stParamEdit.psTitle = "Torque Scaling";
   stParamEdit.psUnit = " %";
   stParamEdit.bSignedInteger = 1;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_LWD_TorqueOffset( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__LWD_TorqueOffset;
   stParamEdit.uwParamIndex_End = enPARAM8__LWD_TorqueOffset;
   stParamEdit.psTitle = "Torque Offset";
   stParamEdit.psUnit = " %";
   stParamEdit.bSignedInteger = 1;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_LWD_MonthlyCalibrationHour( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__LWD_MonthlyCalibrationHour;
   stParamEdit.uwParamIndex_End = enPARAM8__LWD_MonthlyCalibrationHour;
   stParamEdit.psTitle = "Monthly Calib. Hour";
   stParamEdit.psUnit = ":00";
   stParamEdit.ucValueOffset = 1;
   stParamEdit.ulValue_Max = 23;
   stParamEdit.ucFieldSize_Value = 2;

   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_LWD_MonthlyCalibrationDay( void )
{
   static char *(pasOptions[]) =
   {
      "SUN",
      "MON",
      "TUE",
      "WED",
      "THU",
      "FRI",
      "SAT",
   };

   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__LWD_MonthlyCalibrationDay;
   stParamEdit.uwParamIndex_End = enPARAM8__LWD_MonthlyCalibrationDay;
   stParamEdit.psTitle = "Monthly Calib. Day";
   stParamEdit.psUnit = "";
   stParamEdit.ucaPTRChoiceList = &pasOptions;
   stParamEdit.ulValue_Max = (sizeof(pasOptions)/sizeof(*pasOptions)) - 1;
   UI_ParamEditSceenTemplate(&stParamEdit);
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
