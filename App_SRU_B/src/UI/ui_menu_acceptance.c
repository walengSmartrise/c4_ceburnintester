/******************************************************************************
 *
 * @file     ui_menu_setup.c
 * @brief    Setup Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "mod.h"
#include "sru.h"
#include <string.h>
#include <math.h>
#include "lcd.h"
#include "acceptance.h"
#include "operation.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_Debug_AcceptanceTest( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_ui_screen__freeform gstFFS_Debug_AcceptanceTest =
{
   .pfnDraw = UI_FFS_Debug_AcceptanceTest,
};

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

//----------------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Debug_AcceptanceTest =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Debug_AcceptanceTest,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define START_CURSOR_POS_VALUE (2U)
#define START_CURSOR_POS_SAVE  (19U)

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

static char const * const psAccptStrings[NUM_ACCEPTANCE_TEST] =
{
    "Inactive",
    "UNTS",
    "DNTS",
    "UETS",
    "DETS",
    "TOP LIMIT",
    "BOTTOM LIMIT",
    "REDUNDANCY CPLD",
    "REDUNDANCY MCU",
    "OVERSPEED",
    "GOV ELECTRICAL",
    "GOV MECHANICAL",
    "INSPECTION SPEED",
    "LEVELING SPEED",
    "COUNTER BUFFER",
    "CAR BUFFER",
    "UNINT MOVEMENT"
};
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
void SetAcceptanceTestCompleted()
{
   SetAcceptanceTest_Plus1(NO_ACTIVE_ACCEPTANCE_TEST);
}
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void Screen_Default(struct st_runtime_signal_edit_menu * pstEditMenu)
{
   LCD_Char_Clear();

   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteString(pstEditMenu->psTitle);
   //------------------------------------------------------------------
   if(pstEditMenu->bSaving)
   {
      LCD_Char_GotoXY(START_CURSOR_POS_VALUE,2);
      LCD_Char_WriteStringUpper("Saving");
   }
   else if(pstEditMenu->bInvalid)
   {
      LCD_Char_GotoXY(START_CURSOR_POS_VALUE,2);
      LCD_Char_WriteStringUpper("Invalid");
   }
   else
   {
      //------------------------------------------------------------------
      LCD_Char_GotoXY(2, 2);
      if(pstEditMenu->ulValue_Edited <= pstEditMenu->ulValue_Limit)
      {
         LCD_Char_WriteString(psAccptStrings[pstEditMenu->ulValue_Edited]);
      }

      //------------------------------------------------------------------
      LCD_Char_GotoXY(pstEditMenu->ucCursorColumn, 3);
      LCD_Char_WriteString("*");
      //------------------------------------------------------------------
      if ( pstEditMenu->ulValue_Edited != pstEditMenu->ulValue_Saved )
      {
         LCD_Char_GotoXY( 16, 1 );
         LCD_Char_WriteString( "Save" );

         LCD_Char_GotoXY( 19, 2 );
         LCD_Char_WriteChar( '|' );
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Value( enum en_keypresses enKeypress,
      enum en_field_action enFieldAction,
      struct st_runtime_signal_edit_menu * pstEditMenu )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex;
   int64_t llCurrentParam_EditedValue;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = pstEditMenu->ucFieldSize_Value - 1;
      }
   }
   else  // enACTION__EDIT
   {
      llCurrentParam_EditedValue = pstEditMenu->ulValue_Edited;
      //------------------------------------------------------------------
      int32_t uiDigitWeight = (pstEditMenu->ucFieldSize_Value - 1) - ucCursorIndex;

      uiDigitWeight = pow( 10, uiDigitWeight );  // 10 to the power of uiDigitWeight

      //------------------------------------------------------------------
      pstEditMenu->ucCursorColumn = START_CURSOR_POS_VALUE + ucCursorIndex;
      //------------------------------------------------------------------
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            llCurrentParam_EditedValue += uiDigitWeight;
            break;

         case enKEYPRESS_DOWN:
            llCurrentParam_EditedValue -= uiDigitWeight;
            break;

         case enKEYPRESS_LEFT:
            if ( ucCursorIndex )
            {
               --ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_LEFT;
            }
            break;

         case enKEYPRESS_RIGHT:
            if ( ucCursorIndex < (pstEditMenu->ucFieldSize_Value - 1) )
            {
               ++ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;

         default:
            break;
      }

      if(llCurrentParam_EditedValue > pstEditMenu->ulValue_Limit)
      {
         llCurrentParam_EditedValue = pstEditMenu->ulValue_Limit;
      }
      else if(llCurrentParam_EditedValue < 0)
      {
         llCurrentParam_EditedValue = pstEditMenu->ulValue_Limit;
      }
      pstEditMenu->ulValue_Edited = (uint32_t) llCurrentParam_EditedValue;
   }

   return enReturnValue;
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void SaveValue( struct st_runtime_signal_edit_menu * pstEditMenu )
{
    pstEditMenu->pfnSetSignal(pstEditMenu->ulValue_Edited);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Save( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_runtime_signal_edit_menu * pstEditMenu )
{
    enum en_field_action enReturnValue = enACTION__NONE;
    static enum en_save_state enSaveState = SAVE_STATE__SAVED;

    pstEditMenu->ucCursorColumn = START_CURSOR_POS_SAVE;
    //------------------------------------------
    if ( enFieldAction == enACTION__EDIT )
    {
        if ( enKeypress == enKEYPRESS_ENTER )
        {
            enSaveState = SAVE_STATE__SAVING;
        }
        else if ( enKeypress == enKEYPRESS_LEFT )
        {
            enSaveState = SAVE_STATE__SAVED;
            enReturnValue = enACTION__EXIT_TO_LEFT;
        }
    }

    //--------------------------------
    switch ( enSaveState )
    {
        case SAVE_STATE__CLEARING:
            pstEditMenu->bSaving = 1;
            if ( !pstEditMenu->ulValue_Saved )
            {
                enSaveState = SAVE_STATE__SAVING;
            }
            break;
        case SAVE_STATE__SAVING:
            SaveValue( pstEditMenu );
            pstEditMenu->bSaving = 1;
            if ( pstEditMenu->ulValue_Saved == pstEditMenu->ulValue_Edited )
            {
                enReturnValue = enACTION__EXIT_TO_LEFT;
                pstEditMenu->bSaving = 0;
                enSaveState = SAVE_STATE__SAVED;
            }
            break;
        case SAVE_STATE__SAVED:

            pstEditMenu->bSaving = 0;
            break;
        default:


            enSaveState = SAVE_STATE__SAVED;
            break;
    }
    return enReturnValue;
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void Edit_RuntimeSignal(struct st_runtime_signal_edit_menu * pstEditMenu)
{
   pstEditMenu->ulValue_Saved = pstEditMenu->pfnGetSignal();
   if ( pstEditMenu->ucFieldIndex == enFIELD__NONE )
   {
      pstEditMenu->ulValue_Edited = pstEditMenu->ulValue_Saved;
      pstEditMenu->bSaving = 0;
      pstEditMenu->bInvalid = 0;
      pstEditMenu->ucFieldIndex = enFIELD__VALUE;
      Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstEditMenu );
   }
   else
   {
      enum en_field_action enFieldAction;

      enum en_keypresses enKeypress = Button_GetKeypress();
      //------------------------------------------------------------------
      switch ( pstEditMenu->ucFieldIndex )
      {
         case enFIELD__VALUE:

            enFieldAction = Edit_Value( enKeypress, enACTION__EDIT, pstEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               PrevScreen();
               pstEditMenu->ucFieldIndex = enFIELD__NONE;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Save( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstEditMenu );

               pstEditMenu->ucFieldIndex = enFIELD__SAVE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__SAVE:

            enFieldAction = Edit_Save( enKeypress, enACTION__EDIT, pstEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT, pstEditMenu );

               pstEditMenu->ucFieldIndex = enFIELD__VALUE;
            }
            break;

         default:
            break;
      }
      Screen_Default(pstEditMenu);
   }
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void Enter_AcceptanceMenu()
{
   static struct st_runtime_signal_edit_menu stMenuEdit;

   stMenuEdit.ulValue_Limit = NUM_ACCEPTANCE_TEST - 1;
   stMenuEdit.psTitle = "Select Acceptance Test";
   stMenuEdit.ucFieldSize_Value = 1;
   stMenuEdit.pfnGetSignal = GetAcceptanceTest_Plus1;
   stMenuEdit.pfnSetSignal = SetAcceptanceTest_Plus1;

   Edit_RuntimeSignal(&stMenuEdit);

}
/*----------------------------------------------------------------------------
   Access only on MRB when in Auto - Acceptance Mode
 *----------------------------------------------------------------------------*/
static void UI_FFS_Debug_AcceptanceTest( void )
{
    //SDA REVIEW: This should check for accpt_tst not _test as the mode of op. Test is not the same thing as accpt test
   if(     ( GetSRU_Deployment()     == enSRU_DEPLOYMENT__MR )
        && ( GetOperation_ClassOfOp() == CLASSOP__AUTO        )
        && ( GetOperation_AutoMode()  == MODE_A__TEST    )
     )
   {
      Enter_AcceptanceMenu();
   }
   else
   {
      LCD_Char_Clear();
      if(GetSRU_Deployment() != enSRU_DEPLOYMENT__MR)
      {
         LCD_Char_GotoXY(1,1);
         LCD_Char_WriteString("Access Denied:");
         LCD_Char_GotoXY(2,2);
         LCD_Char_WriteString("Move To MR SRU");
      }
      else
      {
         LCD_Char_GotoXY(1,1);
         LCD_Char_WriteString("Access Denied:");
         LCD_Char_GotoXY(2,2);
         LCD_Char_WriteString("On MR Board,");
         LCD_Char_GotoXY(2,3);
         LCD_Char_WriteString("Jump MM Input");
      }
      enum en_keypresses enKeypress = Button_GetKeypress();
      if(enKeypress == enKEYPRESS_LEFT)
      {
         PrevScreen();
      }
   }
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
