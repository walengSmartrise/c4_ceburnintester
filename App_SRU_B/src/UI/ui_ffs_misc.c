/******************************************************************************
 *
 * @file     ui_ffs_misc.c
 * @brief    Setup Misc UI pages
 * @version  V1.00
 * @date     13, Feb 2018
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>
#include "param.h"
#include "config_System.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Preflight( void );
static void UI_FFS_Misc_BypassTermLimits( void );
static void UI_FFS_Misc_DynamicParking( void );
static void UI_FFS_Misc_ParkingTimer( void );
static void UI_FFS_Misc_ParkingFloor( void );
static void UI_FFS_Misc_ParkingDoorOpen( void );
static void UI_FFS_Misc_EnableConstructionBox( void );
static void UI_FFS_Misc_OOS_Disable( void );
static void UI_FFS_Misc_OOS_HourlyLimit( void );
static void UI_FFS_Misc_MaxRuntime( void );

static void UI_FFS_Misc_ICInspReqForCT( void );
static void UI_FFS_Misc_DisableIdleTravelArrow( void );
static void UI_FFS_Misc_UIDriveEdit( void );
static void UI_FFS_Misc_EnableLatchesCC();
static void UI_FFS_Misc_FanAndLightTimer();
static void UI_FFS_Misc_FanAndLightTimer2();
static void UI_FFS_Misc_CarToLobbyFloor();
static void UI_FFS_Misc_ArrivalUpdateTime( void );
static void UI_FFS_Misc_EnableOldFRAM( void );
static void UI_FFS_Misc_EnableHallSecurity( void );
static void UI_FFS_Misc_EnablePitInsp( void );
static void UI_FFS_Misc_EnableLandInsp( void );
static void UI_FFS_Misc_Enable3DigitPI( void );
static void UI_FFS_Misc_PaymentPasscode( void );

static void UI_FFS_DirChangeDelay( void );

static void UI_FFS_OOS_MaxStartsPerMinute( void );
static void UI_FFS_OOS_DisablePIOOS( void );
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
//---------------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Setup_Preflight =
{
   .pfnDraw = UI_FFS_Setup_Preflight,
};
//---------------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Misc_BypassTermLimits =
{
   .pfnDraw = UI_FFS_Misc_BypassTermLimits,
};
static struct st_ui_screen__freeform gstFFS_Misc_DynamicParking =
{
   .pfnDraw = UI_FFS_Misc_DynamicParking,
};
static struct st_ui_screen__freeform gstFFS_Misc_ParkingTimer =
{
   .pfnDraw = UI_FFS_Misc_ParkingTimer,
};
static struct st_ui_screen__freeform gstFFS_Misc_ParkingFloor =
{
   .pfnDraw = UI_FFS_Misc_ParkingFloor,
};
static struct st_ui_screen__freeform gstFFS_Misc_ParkingDoorOpen =
{
   .pfnDraw = UI_FFS_Misc_ParkingDoorOpen,
};
static struct st_ui_screen__freeform gstFFS_Misc_EnableConstructionBox =
{
   .pfnDraw = UI_FFS_Misc_EnableConstructionBox,
};
static struct st_ui_screen__freeform gstFFS_Misc_OOS_Disable =
{
   .pfnDraw = UI_FFS_Misc_OOS_Disable,
};
static struct st_ui_screen__freeform gstFFS_Misc_OOS_HourlyLimit =
{
   .pfnDraw = UI_FFS_Misc_OOS_HourlyLimit,
};
static struct st_ui_screen__freeform gstFFS_Misc_MaxRuntime =
{
   .pfnDraw = UI_FFS_Misc_MaxRuntime,
};
static struct st_ui_screen__freeform gstFFS_Misc_ICInspReqForCT =
{
   .pfnDraw = UI_FFS_Misc_ICInspReqForCT,
};
static struct st_ui_screen__freeform gstFFS_Misc_DisableIdleTravelArrow =
{
   .pfnDraw = UI_FFS_Misc_DisableIdleTravelArrow,
};
static struct st_ui_screen__freeform gstFFS_Misc_UIDriveEdit =
{
   .pfnDraw = UI_FFS_Misc_UIDriveEdit,
};
static struct st_ui_screen__freeform gstFFS_Misc_EnableLatchesCC =
{
   .pfnDraw = UI_FFS_Misc_EnableLatchesCC,
};
static struct st_ui_screen__freeform gstFFS_Misc_FanAndLightTimer =
{
   .pfnDraw = UI_FFS_Misc_FanAndLightTimer,
};
static struct st_ui_screen__freeform gstFFS_Misc_FanAndLightTimer2 =
{
   .pfnDraw = UI_FFS_Misc_FanAndLightTimer2,
};
static struct st_ui_screen__freeform gstFFS_Misc_CarToLobbyFloor =
{
   .pfnDraw = UI_FFS_Misc_CarToLobbyFloor,
};
static struct st_ui_screen__freeform gstFFS_Misc_ArrivalUpdateTime =
{
   .pfnDraw = UI_FFS_Misc_ArrivalUpdateTime,
};
static struct st_ui_screen__freeform gstFFS_Misc_EnableOldFRAM =
{
   .pfnDraw = UI_FFS_Misc_EnableOldFRAM,
};
static struct st_ui_screen__freeform gstFFS_Misc_EnableHallSecurity =
{
   .pfnDraw = UI_FFS_Misc_EnableHallSecurity,
};
static struct st_ui_screen__freeform gstFFS_Misc_EnablePitInsp =
{
   .pfnDraw = UI_FFS_Misc_EnablePitInsp,
};
static struct st_ui_screen__freeform gstFFS_Misc_EnableLandInsp =
{
   .pfnDraw = UI_FFS_Misc_EnableLandInsp,
};
static struct st_ui_screen__freeform gstFFS_Misc_Enable3DigitPI =
{
   .pfnDraw = UI_FFS_Misc_Enable3DigitPI,
};
static struct st_ui_screen__freeform gstFFS_Misc_PaymentPasscode =
{
   .pfnDraw = UI_FFS_Misc_PaymentPasscode,
};
static struct st_ui_screen__freeform gstFFS_OOS_MaxStartsPerMinute =
{
   .pfnDraw = UI_FFS_OOS_MaxStartsPerMinute,
};
static struct st_ui_screen__freeform gstFFS_OOS_DisablePIOOS =
{
   .pfnDraw = UI_FFS_OOS_DisablePIOOS,
};
static struct st_ui_screen__freeform gstFFS_DirChangeDelay =
{
   .pfnDraw = UI_FFS_DirChangeDelay,
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_Misc_BypassTermLimits =
{
   .psTitle = "Bypass Term Limits",
   .pstUGS_Next = &gstUGS_Misc_BypassTermLimits,
};
static struct st_ui_menu_item gstMI_Misc_Default =
{
   .psTitle = "Default",
   .pstUGS_Next = &gstUGS_Menu_Default,
};
static struct st_ui_menu_item gstMI_Misc_EnableConstructionBox =
{
   .psTitle = "Enable Const. Box",
   .pstUGS_Next = &gstUGS_Misc_EnableConstructionBox,
};
static struct st_ui_menu_item gstMI_Misc_MaxRuntime =
{
   .psTitle = "Max Run Time",
   .pstUGS_Next = &gstUGS_Misc_MaxRuntime,
};
static struct st_ui_menu_item gstMI_Misc_ICInspReqForCT =
{
   .psTitle = "CT Insp. Req. IC",
   .pstUGS_Next = &gstUGS_Misc_ICInspReqForCT,
};
static struct st_ui_menu_item gstMI_Misc_DisableIdleTravelArrow =
{
   .psTitle = "Dis. IdleTrvArrow",
   .pstUGS_Next = &gstUGS_Misc_DisableIdleTravelArrow,
};
static struct st_ui_menu_item gstMI_Misc_UIDriveEdit =
{
   .psTitle = "En. UI Drive Edit",
   .pstUGS_Next = &gstUGS_Misc_UIDriveEdit,
};
static struct st_ui_menu_item gstMI_Misc_EnableLatchesCC =
{
   .psTitle = "Enable Latches CC",
   .pstUGS_Next = &gstUGS_Misc_EnableLatchesCC,
};
static struct st_ui_menu_item gstMI_Misc_FanAndLightTimer =
{
   .psTitle = "Fan & Light Timer",
   .pstUGS_Next = &gstUGS_Misc_FanAndLightTimer,
};
static struct st_ui_menu_item gstMI_Misc_FanAndLightTimer2 =
{
   .psTitle = "Ext. Fan Timer",
   .pstUGS_Next = &gstUGS_Misc_FanAndLightTimer2,
};
static struct st_ui_menu_item gstMI_Misc_CarToLobbyFloor =
{
   .psTitle = "Car To Lobby Flr",
   .pstUGS_Next = &gstUGS_Misc_CarToLobbyFloor,
};
static struct st_ui_menu_item gstMI_Misc_ArrivalUpdateTime =
{
   .psTitle = "Arrival Update Time",
   .pstUGS_Next = &gstUGS_Misc_ArrivalUpdateTime,
};
static struct st_ui_menu_item gstMI_Misc_EnableOldFRAM =
{
   .psTitle = "Enable Old FRAM",
   .pstUGS_Next = &gstUGS_Misc_EnableOldFRAM,
};
static struct st_ui_menu_item gstMI_Misc_EnableHallSecurity =
{
   .psTitle = "Enable Hall Security",
   .pstUGS_Next = &gstUGS_Misc_EnableHallSecurity,
};
static struct st_ui_menu_item gstMI_Misc_EnablePitInsp =
{
   .psTitle = "Enable Pit Insp",
   .pstUGS_Next = &gstUGS_Misc_EnablePitInsp,
};
static struct st_ui_menu_item gstMI_Misc_EnableLandInsp =
{
   .psTitle = "Enable Landing Insp",
   .pstUGS_Next = &gstUGS_Misc_EnableLandInsp,
};
static struct st_ui_menu_item gstMI_Misc_Parking =
{
   .psTitle = "Parking",
   .pstUGS_Next = &gstUGS_Menu_Setup_Parking,
};
static struct st_ui_menu_item gstMI_Misc_OOS =
{
   .psTitle = "OOS",
   .pstUGS_Next = &gstUGS_Menu_Setup_OOS,
};
static struct st_ui_menu_item gstMI_Misc_Enable3DigitPI =
{
   .psTitle = "En. 3 Digit PI",
   .pstUGS_Next = &gstUGS_Misc_Enable3DigitPI,
};
static struct st_ui_menu_item gstMI_Misc_PaymentPasscode =
{
   .psTitle = "Payment Passcode",
   .pstUGS_Next = &gstUGS_Misc_PaymentPasscode,
};
static struct st_ui_menu_item gstMI_DirChangeDelay =
{
   .psTitle = "Dir. Change Delay",
   .pstUGS_Next = &gstUGS_DirChangeDelay,
};
static struct st_ui_menu_item * gastMenuItems_Misc[] =
{
   &gstMI_Misc_BypassTermLimits,
   &gstMI_Misc_EnableConstructionBox,
   &gstMI_Misc_MaxRuntime,
   &gstMI_Misc_ICInspReqForCT,
   &gstMI_Misc_DisableIdleTravelArrow,
   &gstMI_Misc_UIDriveEdit,
   &gstMI_Misc_EnableLatchesCC,
   &gstMI_Misc_FanAndLightTimer,
   &gstMI_Misc_FanAndLightTimer2,
   &gstMI_Misc_CarToLobbyFloor,
   &gstMI_Misc_ArrivalUpdateTime,
   &gstMI_Misc_EnableOldFRAM,
   &gstMI_Misc_EnableHallSecurity,
   &gstMI_Misc_EnablePitInsp,
   &gstMI_Misc_EnableLandInsp,
   &gstMI_Misc_Parking,
   &gstMI_Misc_OOS,
   &gstMI_Misc_Enable3DigitPI,
   &gstMI_Misc_PaymentPasscode,
   &gstMI_DirChangeDelay,
   &gstMI_Misc_Default,
};
static struct st_ui_screen__menu gstMenu_Misc =
{
   .psTitle = "Misc",
   .pastMenuItems = &gastMenuItems_Misc,
   .ucNumItems = sizeof(gastMenuItems_Misc) / sizeof(gastMenuItems_Misc[ 0 ]),
};
//-----------------------------------------------------
static struct st_ui_menu_item gstMI_Misc_DynamicParking =
{
   .psTitle = "Dynamic Parking",
   .pstUGS_Next = &gstUGS_Misc_DynamicParking,
};
static struct st_ui_menu_item gstMI_Misc_ParkingTimer =
{
   .psTitle = "Parking Timer",
   .pstUGS_Next = &gstUGS_Misc_ParkingTimer,
};
static struct st_ui_menu_item gstMI_Misc_ParkingFloor =
{
   .psTitle = "Parking Floor",
   .pstUGS_Next = &gstUGS_Misc_ParkingFloor,
};
static struct st_ui_menu_item gstMI_Misc_ParkingDoorOpen =
{
   .psTitle = "Parking Door Open",
   .pstUGS_Next = &gstUGS_Misc_ParkingDoorOpen,
};
static struct st_ui_menu_item * gastMenuItems_Parking[] =
{
      &gstMI_Misc_DynamicParking,
      &gstMI_Misc_ParkingTimer,
      &gstMI_Misc_ParkingFloor,
      &gstMI_Misc_ParkingDoorOpen,
};
static struct st_ui_screen__menu gstMenu_Parking =
{
   .psTitle = "Parking",
   .pastMenuItems = &gastMenuItems_Parking,
   .ucNumItems = sizeof(gastMenuItems_Parking) / sizeof(gastMenuItems_Parking[ 0 ]),
};
//-----------------------------------------------------
static struct st_ui_menu_item gstMI_Misc_OOS_Disable =
{
   .psTitle = "Disable OOS",
   .pstUGS_Next = &gstUGS_Misc_OOS_Disable,
};
static struct st_ui_menu_item gstMI_Misc_OOS_HourlyLimit =
{
   .psTitle = "Hourly Fault Limit",
   .pstUGS_Next = &gstUGS_Misc_OOS_HourlyLimit,
};
static struct st_ui_menu_item gstMI_OOS_MaxStartsPerMinute =
{
   .psTitle = "Max Starts Per Minute",
   .pstUGS_Next = &gstUGS_OOS_MaxStartsPerMinute,
};
static struct st_ui_menu_item gstMI_OOS_DisablePIOOS =
{
   .psTitle = "Disable PI OOS",
   .pstUGS_Next = &gstUGS_OOS_DisablePIOOS,
};
static struct st_ui_menu_item * gastMenuItems_OOS[] =
{
   &gstMI_Misc_OOS_Disable,
   &gstMI_Misc_OOS_HourlyLimit,
   &gstMI_OOS_MaxStartsPerMinute,
   &gstMI_OOS_DisablePIOOS,
};
static struct st_ui_screen__menu gstMenu_OOS =
{
   .psTitle = "OOS",
   .pastMenuItems = &gastMenuItems_OOS,
   .ucNumItems = sizeof(gastMenuItems_OOS) / sizeof(gastMenuItems_OOS[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
//-----------------------------------------------------
struct st_ui_generic_screen gstUGS_Setup_Preflight =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_Preflight,
};
//-----------------------------------------------------
struct st_ui_generic_screen gstUGS_Misc_BypassTermLimits =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_BypassTermLimits,
};
struct st_ui_generic_screen gstUGS_Misc_DynamicParking =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_DynamicParking,
};
struct st_ui_generic_screen gstUGS_Misc_ParkingTimer =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_ParkingTimer,
};
struct st_ui_generic_screen gstUGS_Misc_ParkingFloor =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_ParkingFloor,
};
struct st_ui_generic_screen gstUGS_Misc_ParkingDoorOpen =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_ParkingDoorOpen,
};
struct st_ui_generic_screen gstUGS_Misc_EnableConstructionBox =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_EnableConstructionBox,
};
struct st_ui_generic_screen gstUGS_Misc_OOS_Disable =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_OOS_Disable,
};
struct st_ui_generic_screen gstUGS_Misc_OOS_HourlyLimit =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_OOS_HourlyLimit,
};
struct st_ui_generic_screen gstUGS_Misc_MaxRuntime =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_MaxRuntime,
};
struct st_ui_generic_screen gstUGS_Misc_ICInspReqForCT =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_ICInspReqForCT,
};
struct st_ui_generic_screen gstUGS_Misc_DisableIdleTravelArrow =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_DisableIdleTravelArrow,
};
struct st_ui_generic_screen gstUGS_Misc_UIDriveEdit =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_UIDriveEdit,
};
struct st_ui_generic_screen gstUGS_Misc_EnableLatchesCC =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_EnableLatchesCC,
};
struct st_ui_generic_screen gstUGS_Misc_FanAndLightTimer =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_FanAndLightTimer,
};
struct st_ui_generic_screen gstUGS_Misc_FanAndLightTimer2 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_FanAndLightTimer2,
};
struct st_ui_generic_screen gstUGS_Misc_CarToLobbyFloor =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_CarToLobbyFloor,
};
struct st_ui_generic_screen gstUGS_Misc_ArrivalUpdateTime =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_ArrivalUpdateTime,
};
struct st_ui_generic_screen gstUGS_Misc_EnableOldFRAM =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_EnableOldFRAM,
};
struct st_ui_generic_screen gstUGS_Misc_EnableHallSecurity =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_EnableHallSecurity,
};
struct st_ui_generic_screen gstUGS_Misc_EnablePitInsp =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_EnablePitInsp,
};
struct st_ui_generic_screen gstUGS_Misc_EnableLandInsp =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_EnableLandInsp,
};
struct st_ui_generic_screen gstUGS_Misc_Enable3DigitPI =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_Enable3DigitPI,
};
struct st_ui_generic_screen gstUGS_Misc_PaymentPasscode =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_PaymentPasscode,
};
struct st_ui_generic_screen gstUGS_OOS_MaxStartsPerMinute =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_OOS_MaxStartsPerMinute,
};
struct st_ui_generic_screen gstUGS_OOS_DisablePIOOS =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_OOS_DisablePIOOS,
};
struct st_ui_generic_screen gstUGS_DirChangeDelay =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DirChangeDelay,
};
//----------------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_Setup_Parking =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Parking,
};
struct st_ui_generic_screen gstUGS_Menu_Setup_OOS =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_OOS,
};
struct st_ui_generic_screen gstUGS_Menu_Setup_Misc =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Misc,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_Preflight( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__DEBUG_DisablePreflight;
   stParamEdit.uwParamIndex_End = enPARAM1__DEBUG_DisablePreflight;

   stParamEdit.psTitle = "Disable Preflight";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_BypassTermLimits( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__BypassTermLimits;
   stParamEdit.uwParamIndex_End = enPARAM1__BypassTermLimits;
   stParamEdit.psTitle = "Bypass Term Limits";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_DynamicParking( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__Enable_Dynamic_Parking;
   stParamEdit.uwParamIndex_End = enPARAM1__Enable_Dynamic_Parking;
   stParamEdit.psTitle = "Enable Dynamic Parking";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_ParkingTimer( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__Parking_Timer_1sec;
   stParamEdit.uwParamIndex_End = enPARAM8__Parking_Timer_1sec;
   stParamEdit.psTitle = "Parking Timer";
   stParamEdit.psUnit = " sec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_ParkingFloor( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__OverrideParkingFloor;
   stParamEdit.uwParamIndex_End = enPARAM8__OverrideParkingFloor;
   stParamEdit.psTitle = "Parking Floor Index";
   stParamEdit.psUnit = " ";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_ParkingDoorOpen( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__ParkingWithDoorOpen;
   stParamEdit.uwParamIndex_End = enPARAM1__ParkingWithDoorOpen;
   stParamEdit.psTitle = "Parking - Door Open";
   stParamEdit.psUnit = " ";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_EnableConstructionBox( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__EnableConstructionRunBox;
   stParamEdit.uwParamIndex_End = enPARAM1__EnableConstructionRunBox;
   stParamEdit.psTitle = "Enable Const. Box";
   stParamEdit.psUnit = " ";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_OOS_Disable( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__OOS_Disable;
   stParamEdit.uwParamIndex_End = enPARAM1__OOS_Disable;
   stParamEdit.psTitle = "Disable OOS";
   stParamEdit.psUnit = " ";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_OOS_HourlyLimit( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__HourlyFaultLimit;
   stParamEdit.uwParamIndex_End = enPARAM8__HourlyFaultLimit;
   stParamEdit.psTitle = "Hourly Fault Limit";
   stParamEdit.psUnit = " ";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_MaxRuntime( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__Max_Runtime_1sec;
   stParamEdit.uwParamIndex_End = enPARAM8__Max_Runtime_1sec;
   stParamEdit.psTitle = "Max Run Time";
   stParamEdit.psUnit = " sec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_OOS_MaxStartsPerMinute( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__MaxStartsPerMinute;
   stParamEdit.uwParamIndex_End = enPARAM8__MaxStartsPerMinute;
   stParamEdit.psTitle = "Max Starts Per Min";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_ICInspReqForCT( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__ICInspectionRequiredForCT;
   stParamEdit.uwParamIndex_End = enPARAM1__ICInspectionRequiredForCT;
   stParamEdit.psTitle = "IC Req for CT";
   stParamEdit.psUnit = " ";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_DisableIdleTravelArrow( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__DisableIdleTravelArrows;
   stParamEdit.uwParamIndex_End = enPARAM1__DisableIdleTravelArrows;
   stParamEdit.psTitle = "Dis. Idle Trv Arrow";
   stParamEdit.psUnit = " ";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_UIDriveEdit( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__EnableUIDriveEdit;
   stParamEdit.uwParamIndex_End = enPARAM1__EnableUIDriveEdit;
   stParamEdit.psTitle = "En. UI Drive Edit";
   stParamEdit.psUnit = " ";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_EnableLatchesCC( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__EnableLatchesCC;
   stParamEdit.uwParamIndex_End = enPARAM1__EnableLatchesCC;
   stParamEdit.psTitle = "Enable Latches CC";
   stParamEdit.psUnit = " ";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_FanAndLightTimer( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__FanAndLight_1s;
   stParamEdit.uwParamIndex_End = enPARAM8__FanAndLight_1s;
   stParamEdit.psTitle = "Fan & Light Timer";
   stParamEdit.psUnit = " sec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_FanAndLightTimer2( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__MRFanTimer_min;
   stParamEdit.uwParamIndex_End = enPARAM8__MRFanTimer_min;
   stParamEdit.psTitle = "MR Fan Timer";
   stParamEdit.psUnit = " min";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_CarToLobbyFloor( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__CarToLobbyFloor;
   stParamEdit.uwParamIndex_End = enPARAM8__CarToLobbyFloor;
   stParamEdit.psTitle = "Car To Lobby Floor";
   stParamEdit.psUnit = " ";
   UI_ParamEditSceenTemplate(&stParamEdit);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_ArrivalUpdateTime( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__ArrivalUpdateTime_sec;
   stParamEdit.uwParamIndex_End = enPARAM8__ArrivalUpdateTime_sec;
   stParamEdit.psTitle = "Arrival Update Time";
   stParamEdit.psUnit = " sec";
   stParamEdit.ulValue_Max = 10;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_EnableOldFRAM( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__EnableOldFRAM;
   stParamEdit.uwParamIndex_End = enPARAM1__EnableOldFRAM;
   stParamEdit.psTitle = "Enable Old FRAM";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_EnableHallSecurity( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__EnableHallSecurity;
   stParamEdit.uwParamIndex_End = enPARAM1__EnableHallSecurity;
   stParamEdit.psTitle = "Enable Hall Security";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_EnablePitInsp( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__Enable_Pit_Inspection;
   stParamEdit.uwParamIndex_End = enPARAM1__Enable_Pit_Inspection;
   stParamEdit.psTitle = "Enable Pit Inspection";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_EnableLandInsp( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__Enable_Landing_Inspection;
   stParamEdit.uwParamIndex_End = enPARAM1__Enable_Landing_Inspection;
   stParamEdit.psTitle = "Enable LND Inspection";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_Enable3DigitPI( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__Enable3DigitPI;
   stParamEdit.uwParamIndex_End = enPARAM1__Enable3DigitPI;
   stParamEdit.psTitle = "En. 3 Digit PI";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Misc_PaymentPasscode( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT24;
   stParamEdit.uwParamIndex_Start = enPARAM24__PaymentPasscode;
   stParamEdit.uwParamIndex_End = enPARAM24__PaymentPasscode;
   stParamEdit.psTitle = "Payment Passcode";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_DirChangeDelay( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__DirChangeDelay_1s;
   stParamEdit.uwParamIndex_End = enPARAM8__DirChangeDelay_1s;
   stParamEdit.psTitle = "Dir. Change Delay";
   stParamEdit.psUnit = " sec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_OOS_DisablePIOOS( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__DisablePIOOS;
   stParamEdit.uwParamIndex_End = enPARAM1__DisablePIOOS;
   stParamEdit.psTitle = "Disable PI OOS";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
