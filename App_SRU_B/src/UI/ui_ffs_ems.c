/******************************************************************************
 *
 * @file     ui_ffs_ems.c
 * @brief    EMS UI pages
 * @version  V1.00
 * @date     13, Feb 2018
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_EMS_AllowPh2WithoutPh1( void );
static void UI_FFS_EMS_ExitPh2AnyFloor( void );
static void UI_FFS_EMS_FireOverridesEMS1( void );
static void UI_FFS_EMS_EMS1_ExitDelay( void );
static void UI_FFS_EMS_EMS2_ExitDelay( void );
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_EMS_AllowPh2WithoutPh1 =
{
   .pfnDraw = UI_FFS_EMS_AllowPh2WithoutPh1,
};
static struct st_ui_screen__freeform gstFFS_EMS_ExitPh2AnyFloor =
{
   .pfnDraw = UI_FFS_EMS_ExitPh2AnyFloor,
};
static struct st_ui_screen__freeform gstFFS_EMS_FireOverridesEMS1 =
{
   .pfnDraw = UI_FFS_EMS_FireOverridesEMS1,
};
static struct st_ui_screen__freeform gstFFS_EMS_EMS1_ExitDelay =
{
   .pfnDraw = UI_FFS_EMS_EMS1_ExitDelay,
};
static struct st_ui_screen__freeform gstFFS_EMS_EMS2_ExitDelay =
{
   .pfnDraw = UI_FFS_EMS_EMS2_ExitDelay,
};
//----------------------------------------------------------
static struct st_ui_menu_item gstMI_EMS_AllowPh2WithoutPh1 =
{
   .psTitle = "AllowPh2WithoutPh1",
   .pstUGS_Next = &gstUGS_EMS_AllowPh2WithoutPh1,
};
static struct st_ui_menu_item gstMI_EMS_ExitPh2AnyFloor =
{
   .psTitle = "Exit Ph2 Any Floor",
   .pstUGS_Next = &gstUGS_EMS_ExitPh2AnyFloor,
};
static struct st_ui_menu_item gstMI_EMS_FireOverridesEMS1 =
{
   .psTitle = "Fire Overrides Ph1",
   .pstUGS_Next = &gstUGS_EMS_FireOverridesEMS1,
};
static struct st_ui_menu_item gstMI_EMS_EMS1_ExitDelay =
{
   .psTitle = "Ph1 Exit Delay",
   .pstUGS_Next = &gstUGS_EMS_EMS1_ExitDelay,
};
static struct st_ui_menu_item gstMI_EMS_EMS2_ExitDelay =
{
   .psTitle = "Ph2 Exit Delay",
   .pstUGS_Next = &gstUGS_EMS_EMS2_ExitDelay,
};
static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_EMS_AllowPh2WithoutPh1,
   &gstMI_EMS_ExitPh2AnyFloor,
   &gstMI_EMS_FireOverridesEMS1,
   &gstMI_EMS_EMS1_ExitDelay,
   &gstMI_EMS_EMS2_ExitDelay,
};
static struct st_ui_screen__menu gstMenu_Setup_EMS =
{
   .psTitle = "EMS",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_EMS_AllowPh2WithoutPh1 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_EMS_AllowPh2WithoutPh1,
};
struct st_ui_generic_screen gstUGS_EMS_ExitPh2AnyFloor =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_EMS_ExitPh2AnyFloor,
};
struct st_ui_generic_screen gstUGS_EMS_FireOverridesEMS1 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_EMS_FireOverridesEMS1,
};
struct st_ui_generic_screen gstUGS_EMS_EMS1_ExitDelay =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_EMS_EMS1_ExitDelay,
};
struct st_ui_generic_screen gstUGS_EMS_EMS2_ExitDelay =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_EMS_EMS2_ExitDelay,
};
//-------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_Setup_EMS =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_EMS,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_EMS_AllowPh2WithoutPh1( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__EMS_AllowPh2WithoutPh1;
   stParamEdit.uwParamIndex_End = enPARAM1__EMS_AllowPh2WithoutPh1;
   stParamEdit.psTitle = "AllowPh2WithoutPh1";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_EMS_ExitPh2AnyFloor( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__EMS_ExitPh2AtAnyFloor;
   stParamEdit.uwParamIndex_End = enPARAM1__EMS_ExitPh2AtAnyFloor;
   stParamEdit.psTitle = "Exit Ph2 Any Floor";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_EMS_FireOverridesEMS1( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__EMS_FireOverridesEMS1;
   stParamEdit.uwParamIndex_End = enPARAM1__EMS_FireOverridesEMS1;
   stParamEdit.psTitle = "Fire Overrides Ph1";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_EMS_EMS1_ExitDelay( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__EMS1_ExitDelay_1s;
   stParamEdit.uwParamIndex_End = enPARAM8__EMS1_ExitDelay_1s;
   stParamEdit.psTitle = "Ph1 Exit Delay";
   stParamEdit.psUnit = " sec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UI_FFS_EMS_EMS2_ExitDelay( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__EMS2_ExitDelay_1s;
   stParamEdit.uwParamIndex_End = enPARAM8__EMS2_ExitDelay_1s;
   stParamEdit.psTitle = "Ph2 Exit Delay";
   stParamEdit.psUnit = " sec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
