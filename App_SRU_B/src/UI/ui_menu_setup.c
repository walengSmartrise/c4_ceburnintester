/******************************************************************************
 *
 * @file     ui_menu_setup.c
 * @brief    Setup Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "mod.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *

   { "", UIS_MENU_SETUP_INITIAL },
   { "", UIS_MENU_SAFETY },
#if CONTROLLER_TYPE != CONTROLLER_TYPE_SRH
   { "", UIS_MENU_TIMERS },
#endif
   { "", UIS_MENU_DOOR_SETUP },
   { "", UIS_MENU_RTC },
   { "", UIS_MENU_SETUP_MISC },
   { "", UIS_MENU_SETUP_ADVANCED },

 *----------------------------------------------------------------------------*/
static struct st_ui_menu_item gstMI_SetupIO =
{
   .psTitle = "Setup I/O",
   .pstUGS_Next = &gstUGS_Menu_SetupIO,
};
static struct st_ui_menu_item gstMI_Safety =
{
   .psTitle = "Safety",
   .pstUGS_Next = &gstUGS_Menu_Safety,
};
static struct st_ui_menu_item gstMI_RunTimers =
{
   .psTitle = "Run Timers",
   .pstUGS_Next = &gstUGS_Menu_Setup_Timers,
};
static struct st_ui_menu_item gstMI_DoorSetup =
{
   .psTitle = "Door Setup",
   .pstUGS_Next = &gstUGS_Menu_Setup_Doors,
};
static struct st_ui_menu_item gstMI_EditBrakes =
{
   .psTitle = "Brake Setup",
   .pstUGS_Next = &gstUGS_Menu_SetupAllBrakes,
};
static struct st_ui_menu_item gstMI_Speeds =
{
   .psTitle = "Speeds",
   .pstUGS_Next = &gstUGS_Menu_Setup_Speeds,
};
static struct st_ui_menu_item gstMI_DriveSetup =
{
   .psTitle = "Drive Setup",
   .pstUGS_Next = &gstUGS_Menu_DriveSetup,
};
static struct st_ui_menu_item gstMI_Floors =
{
   .psTitle = "Floors",
   .pstUGS_Next = &gstUGS_Menu_Setup_Floors,
};

static struct st_ui_menu_item gstMI_CarCalls =
{
   .psTitle = "Car Calls",
   .pstUGS_Next = &gstUGS_Menu_Setup_CarCalls,
};
static struct st_ui_menu_item gstMI_Hoistway =
{
   .psTitle = "Hoistway Access",
   .pstUGS_Next = &gstUGS_Menu_Setup_Hoistway,
};
static struct st_ui_menu_item gstMI_Fire =
{
   .psTitle = "Fire",
   .pstUGS_Next = &gstUGS_Menu_Setup_Fire,
};
static struct st_ui_menu_item gstMI_Misc =
{
   .psTitle = "Miscellaneous",
   .pstUGS_Next = &gstUGS_Menu_Setup_Misc,
};
static struct st_ui_menu_item gstMI_NTS =
{
   .psTitle = "NTS",
   .pstUGS_Next = &gstUGS_Setup_NTS,
};

static struct st_ui_menu_item gstMI_PILabels =
{
   .psTitle = "PI Labels",
   .pstUGS_Next = &gstUGS_Setup_PI,
};
//static struct st_ui_menu_item gstMI_RealTimeClock =
//{
//  .psTitle = "Real-Time Clock",
//  .pstUGS_Next = &gstUGS_Menu_Setup_RTC,
//};
static struct st_ui_menu_item gstMI_SCurve =
{
   .psTitle = "S-Curve",
   .pstUGS_Next = &gstUGS_Menu_Scurve,
};
static struct st_ui_menu_item gstMI_LoadWeigher =
{
   .psTitle = "Load Weigher",
   .pstUGS_Next = &gstUGS_Menu_Setup_LoadWeigher,
};
static struct st_ui_menu_item gstMI_Group =
{
   .psTitle = "Group Setup",
   .pstUGS_Next = &gstUGS_Menu_Setup_Group,
};
static struct st_ui_menu_item gstMI_EQ =
{
   .psTitle = "Earthquake",
   .pstUGS_Next = &gstUGS_Menu_Setup_EQ,
};
static struct st_ui_menu_item gstMI_Flood =
{
   .psTitle = "Flood",
   .pstUGS_Next = &gstUGS_Menu_Setup_Flood,
};
static struct st_ui_menu_item gstMI_EMS =
{
   .psTitle = "EMS",
   .pstUGS_Next = &gstUGS_Menu_Setup_EMS,
};
static struct st_ui_menu_item gstMI_Sabbath =
{
   .psTitle = "Sabbath",
   .pstUGS_Next = &gstUGS_Menu_Setup_Sabbath,
};
static struct st_ui_menu_item gstMI_Swing =
{
   .psTitle = "Swing",
   .pstUGS_Next = &gstUGS_Menu_Setup_Swing,
};
static struct st_ui_menu_item gstMI_ATTD =
{
   .psTitle = "Attendant",
   .pstUGS_Next = &gstUGS_Menu_Setup_ATTD,
};
static struct st_ui_menu_item gstMI_EPower =
{
   .psTitle = "E-Power",
   .pstUGS_Next = &gstUGS_Menu_Setup_EPower,
};
static struct st_ui_menu_item gstMI_Floors_AccessCode =
{
   .psTitle = "Access Code",
   .pstUGS_Next = &gstUGS_Setup_Floors_AccessCode,
};
static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_SetupIO,
   &gstMI_Safety,
   &gstMI_RunTimers,
   &gstMI_DoorSetup,
   &gstMI_SCurve,
   &gstMI_Speeds,
   &gstMI_DriveSetup,
   &gstMI_EditBrakes,
   &gstMI_Floors,
   &gstMI_CarCalls,

   &gstMI_Hoistway,
   &gstMI_Fire,
   &gstMI_EQ,
   &gstMI_Misc,
   &gstMI_NTS,

   &gstMI_PILabels,
//   &gstMI_RealTimeClock,
   &gstMI_LoadWeigher,
   &gstMI_Group,
   &gstMI_Flood,
   &gstMI_EMS,
   &gstMI_Sabbath,
   &gstMI_Swing,
   &gstMI_ATTD,
   &gstMI_EPower,
   &gstMI_Floors_AccessCode,
};
static struct st_ui_screen__menu gstMenu =
{
   .psTitle = "Setup",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};
//---------------------------------------------------------------

//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_CarCalls_Latch_F =
{
   .psTitle = "Front",
   .pstUGS_Next = &gstUGS_CC_Latch_F,
};
static struct st_ui_menu_item gstMI_CarCalls_Latch_R =
{
   .psTitle = "Rear",
   .pstUGS_Next = &gstUGS_CC_Latch_R,
};
static struct st_ui_menu_item * gastMenuItems_CarCalls[] =
{
   &gstMI_CarCalls_Latch_F,
   &gstMI_CarCalls_Latch_R,
};
static struct st_ui_screen__menu gstMenu_Setup_CarCalls =
{
   .psTitle = "Enter Car Calls",
   .pastMenuItems = &gastMenuItems_CarCalls,
   .ucNumItems = sizeof(gastMenuItems_CarCalls) / sizeof(gastMenuItems_CarCalls[ 0 ]),
};
//---------------------------------------------------------------

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Menu_Setup =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu,
};
struct st_ui_generic_screen gstUGS_Menu_Setup_CarCalls =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Setup_CarCalls,
};
//----------------------------------------------------------------------

//----------------------------------------------------------------------
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
