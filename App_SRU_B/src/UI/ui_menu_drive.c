/******************************************************************************
 *
 * @file     ui_menu_setup.c
 * @brief    Setup Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "mod.h"
#include "sru.h"
#include <string.h>
#include <math.h>
#include "lcd.h"
#include "sys_drive.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Setup_DriveSelect( void );
static void UI_FFS_Drive_EnableUIDriveEdit( void );

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_menu_item gstMI_SelectDrive =
{
   .psTitle = "Select Drive",
   .pstUGS_Next = &gstUGS_Setup_DriveSelect,
};
static struct st_ui_menu_item gstMI_EnableUIDriveEdit =
{
   .psTitle = "Enable Drive Edit",
   .pstUGS_Next = &gstUGS_DriveSetup_EnableUIDriveEdit,
};

static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_SelectDrive,
   &gstMI_EnableUIDriveEdit,
};
static struct st_ui_screen__menu gstMenu =
{
   .psTitle = "Drive Setup",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};

//---------------------------------------------------------------

static struct st_ui_screen__freeform gstFFS_Setup_DriveSelect =
{
   .pfnDraw = UI_FFS_Setup_DriveSelect,
};
static struct st_ui_screen__freeform gstFFS_Drive_EnableUIDriveEdit =
{
   .pfnDraw = UI_FFS_Drive_EnableUIDriveEdit,
};

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Setup_DriveSelect =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Setup_DriveSelect,
};
struct st_ui_generic_screen gstUGS_DriveSetup_EnableUIDriveEdit =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Drive_EnableUIDriveEdit,
};
struct st_ui_generic_screen gstUGS_Menu_DriveSetup =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static char *(DriveTypeArray[]) =
{
      "MAG",
      "KEB",
      "DSD412",
};
static void UI_FFS_Setup_DriveSelect( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = enPARAM8__DriveSelect;
   stParamEdit.uwParamIndex_End = enPARAM8__DriveSelect;

   stParamEdit.psTitle = "Drive Select";
   stParamEdit.psUnit = "";
   stParamEdit.ucaPTRChoiceList = &DriveTypeArray;
   stParamEdit.ulValue_Max = (sizeof(DriveTypeArray)/sizeof(*DriveTypeArray)) - 1;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Drive_EnableUIDriveEdit( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = enPARAM1__EnableUIDriveEdit;
   stParamEdit.uwParamIndex_End = enPARAM1__EnableUIDriveEdit;
   stParamEdit.psTitle = "Enable UI Drive Edit";
   stParamEdit.psUnit = " ";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
