/******************************************************************************
 *
 * @file     mod_can_test.c
 * @brief
 * @version  V1.00
 * @date     30, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru_b.h"
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Position_B =
{
   .pfnInit = Init,
   .pfnRun = Run,
};


struct st_position_b gstPosition_NTS;

extern RINGBUFF_T RxCedesRingBuffer_0x1A;
extern RINGBUFF_T RxCedesRingBuffer_0x1B;

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define RING_BUFFER_SIZE (8)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *-----------------------------------------------------------------------------*/

extern st_position_frame channel1_0x11;
extern st_position_frame channel1_0x12;

static CAN_MSG_T RxCedesBuffer_0x1A[RING_BUFFER_SIZE];
static CAN_MSG_T RxCedesBuffer_0x1B[RING_BUFFER_SIZE];

static CAN_MSG_T stRxMsg;

static uint16_t uwOfflineCounter_1ms;

// This flag marks when a valid position/speed has not been received after a fixed number of cycles (CEDES_ERROR_LIMIT)
static uint8_t bPositionFeedbackError;
static uint8_t ucPositionErrorCounter;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
  Get clip status for channel 1. Get bit 0 of byte 7
 -----------------------------------------------------------------------------*/
uint8_t GetCEDES_Clip()
{
  return gstPosition_NTS.ucClip & 0x01;
}
/*-----------------------------------------------------------------------------
   Unload most recent element of the ring buffer.
   Discard the rest.
 -----------------------------------------------------------------------------*/
static uint8_t Receive_Packet1()
{
  Status bSuccess = ERROR;

  while ( RingBuffer_GetCount( &RxCedesRingBuffer_0x1A ) > 0 )
  {
    RingBuffer_Pop( &RxCedesRingBuffer_0x1A, &stRxMsg );
    int iCount = RingBuffer_GetCount( &RxCedesRingBuffer_0x1A );
    if( iCount < 1 )
    {
      uint8_t aucData[8];
      /* TODO: Check CRC and Sequence Count */
      aucData[0] = stRxMsg.Data[0];
      aucData[1] = stRxMsg.Data[1] ;
      aucData[2] = stRxMsg.Data[2] ;
      aucData[3] = stRxMsg.Data[3] ;
      aucData[4] = stRxMsg.Data[4] ;
      aucData[5] = stRxMsg.Data[5] ;
      aucData[6] = stRxMsg.Data[6] ;
      aucData[7] = stRxMsg.Data[7] ;

      // TODO NEEDS INVALID POSITION/VELOCITY CHECK
      gstPosition_NTS.ulPosCount = aucData[3] | ( aucData[2] << 8 ) | ( aucData[1] << 16 );

      gstPosition_NTS.wVelocity = aucData[5] | ( aucData[4] << 8 );

      gstPosition_NTS.ucError = aucData[6];

      gstPosition_NTS.ucClip = aucData[7];

      bSuccess = SUCCESS;
      break;
    }
  }

  SetStructPosition_NTS(&gstPosition_NTS);
   
  return bSuccess;
}
/*-----------------------------------------------------------------------------
   Unload most recent element of the ring buffer.
   Discard the rest.
 -----------------------------------------------------------------------------*/
static uint8_t Receive_Packet2()
{
  Status bSuccess = ERROR;

  while ( RingBuffer_GetCount( &RxCedesRingBuffer_0x1B ) > 0 )
  {
    RingBuffer_Pop( &RxCedesRingBuffer_0x1B, &stRxMsg );
    int iCount = RingBuffer_GetCount( &RxCedesRingBuffer_0x1B );
    if( iCount < 1 )
    {
      uint8_t aucData[8];
      /* TODO: Check CRC and Sequence Count */
      aucData[0] = stRxMsg.Data[0];
      aucData[1] = stRxMsg.Data[1] ;
      aucData[2] = stRxMsg.Data[2] ;
      aucData[3] = stRxMsg.Data[3] ;
      aucData[4] = stRxMsg.Data[4] ;
      aucData[5] = stRxMsg.Data[5] ;
      aucData[6] = stRxMsg.Data[6] ;
      aucData[7] = stRxMsg.Data[7] ;

      gstPosition_NTS.ucClipOffset = aucData[3];

      bSuccess = SUCCESS;
      break;
    }
  }
 
   return bSuccess;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Read_Packet1( void )
{
    static int16_t lastGoodVelocity;
    static uint32_t lastGoodPosition;
    uint32_t tmpPosition = 0;
    uint32_t tmpVelocity = 0;

    tmpPosition  = channel1_0x11.Data[3] <<  0;
    tmpPosition |= channel1_0x11.Data[2] <<  8;
    tmpPosition |= channel1_0x11.Data[1] << 16;

    tmpVelocity  = channel1_0x11.Data[5] <<  0;
    tmpVelocity |= channel1_0x11.Data[4] <<  8;

    if(tmpPosition != CEDES_INVALID_POSITION)
    {
        lastGoodPosition = tmpPosition;
        ucPositionErrorCounter = 0;
        bPositionFeedbackError = 0;
    }
    else
    {
       if( ucPositionErrorCounter >= CEDES_POS_ERROR_LIMIT )
       {
          bPositionFeedbackError = 1;
       }
       else
       {
          ucPositionErrorCounter++;
          bPositionFeedbackError = 0;
       }
    }

    if( tmpVelocity != CEDES_INVALID_VELOCITY )
    {
        lastGoodVelocity = tmpVelocity;
    }

    /* Offset between channel 1 and channel 2 is 45mm.
     * PosCount has a 0.5mm resolution so 45mm is 90 counts.
     * That is 45 counts off the center in each direction.
     */
    gstPosition_NTS.ulPosCount = (lastGoodPosition > CHANNEL_POS_COUNT_OFFSET) ? lastGoodPosition - CHANNEL_POS_COUNT_OFFSET : lastGoodPosition;
    gstPosition_NTS.wVelocity = (int16_t)(  lastGoodVelocity / CEDES_SPEED_TO_FPM );
    SetStructPosition_NTS(&gstPosition_NTS);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Read_Packet2( void )
{
   gstPosition_NTS.ucError = channel1_0x12.Data[1];

   gstPosition_NTS.uwStatus = channel1_0x12.Data[3];
   gstPosition_NTS.uwStatus |= channel1_0x12.Data[2] << 8;
}
/*-----------------------------------------------------------------------------
  Match CEDES 2 error to CEDES 1 protocol
 -----------------------------------------------------------------------------*/
uint8_t GetCEDES2_GeneralError( void )
{
  uint8_t error;
  
  if( gstPosition_NTS.ucError & 0x3 )
  {
      /*Internal and comm errors are set in Byte 6 for new protocol*/
      switch( gstPosition_NTS.ucError & 0x3 )
      {
        case 0x01:
          error = 0x06;
          break;
        case 0x10:
          error = 0x07;
          break;
        case 0x11:
          error = 0x07;
          break;
        default:
          error = 0;
          break;
      }
  }
  /* Allignment errors are set in byte 7 for new protocol*/
  else if( gstPosition_NTS.ucClip & 0xC0 ) //allign far-near
  {
    switch( (gstPosition_NTS.ucClip & 0x20) >> 5 )
    {
      case 0x00:
        error = 0x02;
        break;
      case 0x01:
        error = 0x03;
        break;
    }
  }
  else if( gstPosition_NTS.ucClip & 0x1C ) //allign left-right
  {
    switch( (gstPosition_NTS.ucClip & 0x18) >> 3 )
    {
      case 0x00:
        error = 0x04;
        break;
      case 0x01:
        error = 0x05;
        break;
    }
  }
  return error; 
}
/*-----------------------------------------------------------------------------
bit 7-6 Pos1 Err: Result of the internal position and velocity cross-check (channel 1 to channel 2)
   00 = Position1 and Velocity1 OK (position/velocity is safe)
   01 = Position1 Error (position comparison failed)
   10 = Velocity1 Error (velocity comparison failed)
   11 = Position1 and Velocity1 Error(position and velocity comparison failed)
bit 5-4 Pos2 Err: Result of the internal position and velocity cross-check (channel 2 to channel 1)
   00 = Position2 and Velocity2 OK (position and velocity is safe)
   01 = Position2 Error (position comparison failed)
   10 = Velocity2 Error (velocity comparison failed)
   11 = Position2 and Velocity2 Error (position and velocity comparison failed)
bit 3 Reserved: Transmitted as '0'
bit 2-0 General Error:
   000 = OK, no general error
   001 = Cannot Read Tape
   010 = Alignment error - too close
   011 = Alignment error - too far
   100 = Alignment error - too far left
   101 = Alignment error - too far right
   110 = Internal Fault
   111 = Communication Error
 -----------------------------------------------------------------------------*/
static void CheckFor_CEDESFault()
{
   static uint8_t counter_CEDESError;

   uint8_t ucPos1Error;
   uint8_t ucPos2Error;
   uint8_t ucGeneralError;

   if( ( !Param_ReadValue_1Bit(enPARAM1__BypassTermLimits) )
    && ( GetOperation_ManualMode() != MODE_M__CONSTRUCTION ) )
   {
      if ( uwOfflineCounter_1ms >= CEDES_OFFLINE_LIMIT_1MS )
      {
         if(!Param_ReadValue_1Bit(enPARAM1__DisableCEDESOffline))
         {
            SetFault(FLT__CEDES1_OFFLINE);
         }
      }
      else if ( gstPosition_NTS.ucError )
      {
         if(counter_CEDESError >= CEDES_ERROR_LIMIT)
         {
            enum en_cedes_error eError = SUBF_CEDES_FAULT__UNKNOWN;

            if( Param_ReadValue_1Bit( enPARAM1__Enable_CEDES2 ) )
            {
              ucPos1Error = (gstPosition_NTS.ucError >> 4) & 0x1;
              ucPos2Error = (gstPosition_NTS.ucError >> 5) & 0x1;

              ucGeneralError = GetCEDES2_GeneralError();
            }
            else
            {
              ucPos1Error = (gstPosition_NTS.ucError >> 6) & 0x03;
              ucPos2Error = (gstPosition_NTS.ucError >> 4) & 0x03;
              ucGeneralError = gstPosition_NTS.ucError & 0x07;
            }

            if( ucGeneralError ) // General Error
            {
               eError = CEDES_ERROR__READ_FAIL + ucGeneralError - 1;
            }
            else if( ucPos1Error )
            {
               eError = CEDES_ERROR__CROSS1_POS + ucPos1Error - 1;
            }
            else if( ucPos2Error )
            {
               eError = CEDES_ERROR__CROSS2_POS + ucPos2Error - 1;
            }
            en_faults eFault = FLT__CEDES1_READ_FAIL + eError;
            if( eFault > FLT__CEDES1_CROSS2_BOTH )
            {
               eFault = FLT__CEDES1_CROSS2_BOTH;
            }
            SetFault(eFault);
         }
         else
         {
            counter_CEDESError++;
         }
      }
      else
      {
         counter_CEDESError = 0;
         if(bPositionFeedbackError)
         {
            SetFault(FLT__CEDES1_READ_FAIL); // TODO Use unique fault if confusing
         }
      }
   }
}

/*-----------------------------------------------------------------------------

Run old CEDES protocol

 -----------------------------------------------------------------------------*/
void Read_CEDES( void )
{
  if( channel1_0x11.bNewMsg )
   {
      Read_Packet1();
      uwOfflineCounter_1ms = 0;
      channel1_0x11.bNewMsg = 0;
   }
   else if( uwOfflineCounter_1ms <= CEDES_OFFLINE_LIMIT_1MS )
   {
      uwOfflineCounter_1ms += MOD_RUN_PERIOD_POSITION_1MS;
   }

   if( channel1_0x12.bNewMsg )
   {
      Read_Packet2();
      uwOfflineCounter_1ms = 0;
      channel1_0x12.bNewMsg = 0;
   }
}
 /*-----------------------------------------------------------------------------

Run new CEDES protocol

 -----------------------------------------------------------------------------*/
void Read_CEDES2( void )
{
  if( Receive_Packet1() )
  {
    uwOfflineCounter_1ms = 0;
  }
  else if( uwOfflineCounter_1ms <= CEDES_OFFLINE_LIMIT_1MS )
  {
    uwOfflineCounter_1ms += MOD_RUN_PERIOD_POSITION_1MS;
  }
  if( Receive_Packet2() )
  {

  }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_POSITION_1MS;

   RingBuffer_Init(&RxCedesRingBuffer_0x1A, RxCedesBuffer_0x1A, sizeof(RxCedesBuffer_0x1A[0]), RING_BUFFER_SIZE);
   RingBuffer_Init(&RxCedesRingBuffer_0x1B, RxCedesBuffer_0x1B, sizeof(RxCedesBuffer_0x1B[0]), RING_BUFFER_SIZE);

   return 0;
}

/*-----------------------------------------------------------------------------



 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
  if( Param_ReadValue_1Bit( enPARAM1__Enable_CEDES2 ) )
  {
    Read_CEDES2();
  }
  else
  {
    Read_CEDES();
  }
  
   CheckFor_CEDESFault();
   return 0;
}

