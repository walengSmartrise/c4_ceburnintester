/******************************************************************************
 * @file     mod_alarm_app.c
 * @author   Keith Soneda
 * @version  V1.00
 * @date     1, August 2016
 *
 * @note   Each alarm node (TRC, MRA, MRB, CTA, CTB, COPA, COPB) will assert their own alarm.
 *         Nodes will forward their local active alarm to eachother.
 *         Nodes will auto-clear their active alarm after 2 seconds.
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "sru.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "sys.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
enum en_alarmapp_tx_state
{
  TX_STATE__GetNext,
  TX_STATE__Send1,
  TX_STATE__Confirm1,
  TX_STATE__Send2,
  TX_STATE__Confirm2,

  NUM_TX_STATE
};
static enum en_alarmapp_tx_state eTXState = TX_STATE__GetNext;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadDatagram_MRA( st_alarm_data *pastActiveAlarms )
{
    st_alarm_data *pstAlarmData = pastActiveAlarms + enALARM_NODE__MRA;
   if(    ( GetAlarm1_Alarm_MRA()     < NUM_ALARMS                      )
       && ( GetAlarm1_Alarm_MRA()    == GetAlarm2_Alarm_MRA()   )
     )
   {
      pstAlarmData->eAlarmNum   = GetAlarm1_Alarm_MRA();
      pstAlarmData->ulTimestamp = GetAlarm1_Timestamp_MRA();
      pstAlarmData->wSpeed      = GetAlarm2_Speed_MRA();
      pstAlarmData->ulPosition  = GetAlarm2_Position_MRA();
   }
}
/*-----------------------------------------------------------------------------
Unload from MRB
 -----------------------------------------------------------------------------*/
static void UnloadDatagram_MRB( st_alarm_data *pastActiveAlarms )
{
   st_alarm_data *pstAlarmData = pastActiveAlarms + enALARM_NODE__MRB;
  if(    ( GetAlarm1_Alarm_MRB()     < NUM_ALARMS                      )
      && ( GetAlarm1_Alarm_MRB()    == GetAlarm2_Alarm_MRB()   )
    )
  {
     pstAlarmData->eAlarmNum   = GetAlarm1_Alarm_MRB();
     pstAlarmData->ulTimestamp = GetAlarm1_Timestamp_MRB();
     pstAlarmData->wSpeed      = GetAlarm2_Speed_MRB();
     pstAlarmData->ulPosition  = GetAlarm2_Position_MRB();
  }
}

/*-----------------------------------------------------------------------------
Unload from CTA
 -----------------------------------------------------------------------------*/
static void UnloadDatagram_CTA( st_alarm_data *pastActiveAlarms )
{
   st_alarm_data *pstAlarmData = pastActiveAlarms + enALARM_NODE__CTA;
  if(    ( GetAlarm1_Alarm_CTA()     < NUM_ALARMS                      )
      && ( GetAlarm1_Alarm_CTA()    == GetAlarm2_Alarm_CTA()   )
    )
  {
     pstAlarmData->eAlarmNum   = GetAlarm1_Alarm_CTA();
     pstAlarmData->ulTimestamp = GetAlarm1_Timestamp_CTA();
     pstAlarmData->wSpeed      = GetAlarm2_Speed_CTA();
     pstAlarmData->ulPosition  = GetAlarm2_Position_CTA();
  }
}
///*-----------------------------------------------------------------------------
//Unload from CTB
// -----------------------------------------------------------------------------*/
//static void UnloadDatagram_CTB( st_alarm_data *pastActiveAlarms )
//{
//   st_alarm_data *pstAlarmData = pastActiveAlarms + enALARM_NODE__CTB;
//  if(    ( GetAlarm1_Alarm_CTB()     < NUM_ALARMS                      )
//      && ( GetAlarm1_Alarm_CTB()    == GetAlarm2_Alarm_CTB()   )
//    )
//  {
//     pstAlarmData->eAlarmNum   = GetAlarm1_Alarm_CTB();
//     pstAlarmData->ulTimestamp = GetAlarm1_Timestamp_CTB();
//     pstAlarmData->wSpeed      = GetAlarm2_Speed_CTB();
//     pstAlarmData->ulPosition  = GetAlarm2_Position_CTB();
//  }
//}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadDatagram_COPA( st_alarm_data *pastActiveAlarms )
{
    st_alarm_data *pstAlarmData = pastActiveAlarms + enALARM_NODE__COPA;
   if(    ( GetAlarm1_Alarm_COPA()     < NUM_ALARMS                      )
       && ( GetAlarm1_Alarm_COPA()    == GetAlarm2_Alarm_COPA()   )
     )
   {
      pstAlarmData->eAlarmNum   = GetAlarm1_Alarm_COPA();
      pstAlarmData->ulTimestamp = GetAlarm1_Timestamp_COPA();
      pstAlarmData->wSpeed      = GetAlarm2_Speed_COPA();
      pstAlarmData->ulPosition  = GetAlarm2_Position_COPA();
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadDatagram_COPB( st_alarm_data *pastActiveAlarms )
{
    st_alarm_data *pstAlarmData = pastActiveAlarms + enALARM_NODE__COPB;
   if(    ( GetAlarm1_Alarm_COPB()     < NUM_ALARMS                      )
       && ( GetAlarm1_Alarm_COPB()    == GetAlarm2_Alarm_COPB()   )
     )
   {
      pstAlarmData->eAlarmNum   = GetAlarm1_Alarm_COPB();
      pstAlarmData->ulTimestamp = GetAlarm1_Timestamp_COPB();
      pstAlarmData->wSpeed      = GetAlarm2_Speed_COPB();
      pstAlarmData->ulPosition  = GetAlarm2_Position_COPB();
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UnloadDatagram_ActiveAlarms_CT( st_alarm_data *pastActiveAlarms )
{
   //-----------------------------------------------
   UnloadDatagram_MRA(pastActiveAlarms);
   //-----------------------------------------------
   UnloadDatagram_MRB(pastActiveAlarms);
   //-----------------------------------------------
   UnloadDatagram_CTA(pastActiveAlarms);
   //-----------------------------------------------
//   UnloadDatagram_CTB(pastActiveAlarms);
   //-----------------------------------------------
   UnloadDatagram_COPA(pastActiveAlarms);
   //-----------------------------------------------
   UnloadDatagram_COPB(pastActiveAlarms);
}
/*-----------------------------------------------------------------------------
Load active alarms from local board
 -----------------------------------------------------------------------------*/
static void LoadDatagram( st_alarm_data *pastActiveAlarms )
{
   static uint16_t uwResendCounter_ms;
   static un_sdata_datagram unDatagram1;
   static un_sdata_datagram unDatagram2;
   st_alarm_data *pstAlarmData;

   //load resend structure
   uint8_t ucNode = AlarmApp_GetNode();
   pstAlarmData = pastActiveAlarms + ucNode;

   switch(eTXState)
   {
      case TX_STATE__GetNext:
         uwResendCounter_ms += MOD_RUN_PERIOD_ALARM_1MS;
         if(uwResendCounter_ms >= ALARM_RESEND_RATE_1MS)
         {
            uwResendCounter_ms = 0;
            eTXState = TX_STATE__Send1;
         }
         break;

      case TX_STATE__Send1:
         unDatagram1.auw16[0] = pstAlarmData->eAlarmNum;
         unDatagram1.auw16[1] = 0;
         unDatagram1.aui32[1] = pstAlarmData->ulTimestamp;
         SDATA_WriteDatagram( &gstSData_LocalData, DG_CTB__Alarm1, &unDatagram1 );
         SDATA_DirtyBit_Set( &gstSData_LocalData , DG_CTB__Alarm1 );
         eTXState = TX_STATE__Confirm1;
         break;

      case TX_STATE__Confirm1:
         if(!SDATA_DirtyBit_Get( &gstSData_LocalData , DG_CTB__Alarm1 ))
         {
            eTXState = TX_STATE__Send2;
         }
         break;

      case TX_STATE__Send2:
         pstAlarmData = pastActiveAlarms + ucNode;
         unDatagram2.auw16[0] = unDatagram1.auw16[0];
         unDatagram2.auw16[1] = pstAlarmData->wSpeed;
         unDatagram2.aui32[1] = pstAlarmData->ulPosition;
         SDATA_WriteDatagram( &gstSData_LocalData, DG_CTB__Alarm2, &unDatagram2 );
         SDATA_DirtyBit_Set( &gstSData_LocalData , DG_CTB__Alarm2 );
         eTXState = TX_STATE__Confirm2;
         break;

      case TX_STATE__Confirm2:
         if(!SDATA_DirtyBit_Get( &gstSData_LocalData , DG_CTB__Alarm2 ))
         {
            eTXState = TX_STATE__GetNext;
         }
         break;
      default:
         uwResendCounter_ms = 0;
         eTXState = TX_STATE__GetNext;
         break;
   }
}
/*-----------------------------------------------------------------------------
Load active alarms from local & other boards
 -----------------------------------------------------------------------------*/
void LoadDatagram_ActiveAlarms_CT( st_alarm_data *pastActiveAlarms )
{
   LoadDatagram(pastActiveAlarms);
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
