#include "mod.h"
#include "board.h"
#include <stdio.h>
#include "sru.h"
#include "sru_b.h"
#include "sys.h"

//------------------------------------------------------------------------------
static struct st_module * gastModules_CTB[] =
{
   &gstSys_Mod,  // must include the system module in addition to app modules
   &gstMod_Fault,
   &gstMod_FaultApp,
   &gstMod_Heartbeat,
   &gstMod_Watchdog,

   &gstMod_CAN,
   &gstMod_ParamEEPROM,
   &gstMod_ParamApp,
   &gstMod_ParamCTB,
   &gstMod_UART,

   &gstMod_SData,
   &gstMod_Position_B,
   &gstMod_LCD,
   &gstMod_Buttons,
   &gstMod_UI,

   &gstMod_NTS,
   &gstMod_RTC,
   &gstMod_AlarmApp,
   &gstMod_LocalOutputs,
   &gstMod_SData_AuxNet,

   &gstMod_LoadWeigher,
   &gstMod_CEboards,
};

static struct st_module_control gstModuleControl_CTB =
{
   .uiNumModules = sizeof(gastModules_CTB) / sizeof(gastModules_CTB[ 0 ]),

   .pastModules = &gastModules_CTB,

   .enMCU_ID = enMCUB_SRU_UI,

   .enDeployment = enSRU_DEPLOYMENT__CT,
};
static struct st_module * gastModules_CTB_Pre[] =
{
         &gstSys_Mod,  // must include the system module in addition to app modules

         &gstMod_ParamEEPROM,
         &gstMod_ParamApp,
         &gstMod_ParamCTB,
};

static struct st_module_control gstModuleControl_CTB_Pre =
{
   .uiNumModules = sizeof(gastModules_CTB_Pre) / sizeof(gastModules_CTB_Pre[ 0 ]),

   .pastModules = &gastModules_CTB_Pre,

   .enMCU_ID = enMCUB_SRU_UI,

   .enDeployment = enSRU_DEPLOYMENT__CT,
};
/**
 * @brief   main routine for systick example
 * @return  Function should not exit.
 */
int main_CT( void )
{
     __enable_irq();

     // These are the modules that need to be started before the main application.
     // Right now this is just parameters. This is because most modules require
     // parameters and the parameters are not valid till they are loaded into RAM
     Mod_InitAllModules( &gstModuleControl_CTB_Pre );
     while(!GetFP_FlashParamsReady())
     {
        Mod_RunOneModule( &gstModuleControl_CTB_Pre );
     }

     Mod_InitAllModules( &gstModuleControl_CTB );

    // This is really a while(1) loop. Sys_Shutdown() always returns 0. The
    // function is being provided in anticipation of a future time when this
    // application might be running on a PC simulator and we would need a
    // mechanism to terminate this application.
    while ( !Sys_Shutdown() )
    {
        Mod_RunOneModule( &gstModuleControl_CTB );

        uint8_t ucCurrModIndex = gstModuleControl_CTB.uiCurrModIndex;
        uint8_t ucNumOfMods = gstModuleControl_CTB.uiNumModules;

        struct st_module * (* pastModules)[] = gstModuleControl_CTB.pastModules;
        struct st_module * pstCurrentModule = (*pastModules)[ ucCurrModIndex ];

        if( pstCurrentModule->bTimeViolation && (ucCurrModIndex < ucNumOfMods ) )
        {
           SetAlarm( ALM__RUN_TIME_FAULT + gstModuleControl_CTB.uiCurrModIndex - 1);
        }
        if( ( pstCurrentModule->bTimeViolation )
         && ( ucCurrModIndex < ucNumOfMods )
         && ( ( Param_ReadValue_8Bit(enPARAM8__TimeViolationModule) == MRC__ALL )
           || ( ( Param_ReadValue_8Bit(enPARAM8__TimeViolationModule)-MRC__CTB_START  ) == ucCurrModIndex ) ) )
        {
           SetAlarm( ALM__RUN_TIME_FAULT_CTB + ucCurrModIndex - 1);
        }

        Debug_CAN_MonitorUtilization();
    }

    return 0;
}
