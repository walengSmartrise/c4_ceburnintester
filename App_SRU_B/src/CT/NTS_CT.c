#include "mod.h"
#include "sru.h"
#include "sru_b.h"
#include "sys.h"
#include "NTS_CT.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "acceptance.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

// Scale factor is set to 10%.
#define NTS_VELOCITY_SCALE_FACTOR ( 1.1 )
#define NTS_VELOCITY_SCALE_FACTOR_TEST (0.85)

// The min and max contract speeds supported by C4 for now.
#define MAX_CONTRACT_SPEED_FPM ( 2000 )
#define MIN_CONTRACT_SPEED_FPM ( 50 )

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static const enum en_16bit_params aeStartParam_Vel[NUM_MOTION_PROFILES] =
{
      enPARAM16__NTS_VEL_P1_0,
      enPARAM16__NTS_VEL_P2_0,
      enPARAM16__NTS_VEL_P3_0,
      enPARAM16__NTS_VEL_P4_0,
};
static const enum en_16bit_params aeStartParam_Pos[NUM_MOTION_PROFILES] =
{
      enPARAM16__NTS_POS_P1_0,
      enPARAM16__NTS_POS_P2_0,
      enPARAM16__NTS_POS_P3_0,
      enPARAM16__NTS_POS_P4_0,
};

// On startup an unknown fault is issued
static NTS_Faults geNTS_Fault;
// On startup there is no NTS alarm.
static NTS_Alarms geNTS_Alarm;
// On startup we are in the fault state
 NTS_State geNTS_State = NTS__ALARM;
// On startup we are in the fault state
static NTS_State geNTS_LastState = NTS__ALARM;

//static uint8_t gucCounter_PostitionSystemError;
//static uint8_t gucCounter_PostitionSystemOK;

static uint8_t ucDebounceCounter;

// The following globals are accessed through set/reset/get functions
// This makes it a lot easer to find potential bugs. The functions are
// static inline. See http://koopman.us/bess/chap19_globals.pdf
// on the benefits of this method.

static uint8_t gbQuickStop;
static inline void SetQuickStop( void )
{
   gbQuickStop = 1;
}

static inline void ResetQuickStop( void )
{
   gbQuickStop = 0;
}

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
NTS access functions
 *----------------------------------------------------------------------------*/
uint8_t NTS_GetStopFlag()
{
   return gbQuickStop;
}
uint8_t NTS_GetState_CT( void )
{
   return geNTS_State;
}
uint8_t NTS_GetAlarm_CT( void )
{
   return geNTS_Alarm;
}
uint8_t NTS_GetFault_CT( void )
{
   return geNTS_Fault;
}

/*
 * Checks to see if car is on auto operation.
 */
static inline uint8_t  OnAutoOperation( void )
{
   return (GetOperation_ClassOfOp() == CLASSOP__AUTO);
}

/*
   Makes sure that the NTS parameters are valid.
   Checks to see if trip speed or positions are 0. If any are 0 then the NTS
   parameters are not valid. The next check is to make sure that the saved
   parameters are sequential. If not sequential then NTS parameters are not
   valid.

   Return = 0 when NTS parameters are not valid
   Return = 1 when NTS parameters are valid.
*/
/*-----------------------------------------------------------------------------
   Checks that NTS points are increasing, and that speeds are nonzero
 -----------------------------------------------------------------------------*/
static uint8_t CheckFor_InvalidNTSAlarm()
{
   uint8_t bValid = 1;
   /* If not on learn, and bottom floor has been learned */
   if( ( GetOperation_ClassOfOp() != CLASSOP__SEMI_AUTO )
    && ( Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0) ) )
   {
      for( Motion_Profile eProfile = MOTION_PROFILE__1; eProfile < NUM_MOTION_PROFILES; eProfile++ )
      {
         /* Ignore the Inspection profile, and the short profile if its not enabled. */
         if( ( eProfile != MOTION_PROFILE__2 ) &&
             ( ( eProfile != MOTION_PROFILE__4 ) || ( Param_ReadValue_8Bit(enPARAM8__ShortProfileMinDist_ft) ) ) )
         {
            uint32_t uiTripSpeed = Param_ReadValue_16Bit( aeStartParam_Vel[eProfile] );
            uint32_t uiTripPos = Param_ReadValue_16Bit( aeStartParam_Pos[eProfile] );
            for( uint8_t i = 1; i < NUM_NTS_TRIP_POINTS; i++ )
            {
               uint16_t uwSpeed = Param_ReadValue_16Bit( aeStartParam_Vel[eProfile] + i );
               uint32_t uiPos = Param_ReadValue_16Bit( aeStartParam_Pos[eProfile] + i );
               if( !uwSpeed )
               {
                  SetAlarm(ALM__NTS_INVALID_P1+eProfile);
                  bValid = 0;
                  break;
               }
               else if( uiTripSpeed > uwSpeed )
               {
                  SetAlarm(ALM__NTS_INVALID_P1+eProfile);
                  bValid = 0;
                  break;
               }
               else if( uiTripPos > uiPos )
               {
                  SetAlarm(ALM__NTS_INVALID_P1+eProfile);
                  bValid = 0;
                  break;
               }
               else if( uiTripSpeed > Param_ReadValue_16Bit(enPARAM16__ContractSpeed) )
               {
                  SetAlarm(ALM__NTS_INVALID_P1+eProfile);
                  bValid = 0;
                  break;
               }
               uiTripSpeed = uwSpeed;
               uiTripPos = uiPos;
            }
         }
      }
   }
   return bValid;
}
/*-----------------------------------------------------------------------------

   Checks car speed/position against precalculated NTS points.

   If the car exceeds any of these points by more than 10% for
   a number of cycles determined by enPARAM8__DebounceNTS,
   then the car will perform an emergency stop.

   One of four sets of ETS points will be selected depending
   on the active profile, viewed via GetMotion_Profile(),

 -----------------------------------------------------------------------------*/
static uint8_t CheckFor_NTSTripUpAlarm( void )
{
   uint8_t bTripped = 0;
   uint8_t bClearCounter = 1;
   Motion_Profile eProfile = GetMotion_Profile();
   int16_t wCarSpeed = gstPosition_NTS.wVelocity;
   uint8_t ucDebounceLimit = Param_ReadValue_8Bit( enPARAM8__DebounceNTS );

   for( uint8_t i = 0; i < NUM_NTS_TRIP_POINTS; i++ )
   {
      uint32_t uiTripPos = Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 + GetFP_NumFloors() - 1 )
                         - ( Param_ReadValue_16Bit( aeStartParam_Pos[ eProfile ] + i ) * NTS_POSITION_SCALING );
      if( gstPosition_NTS.ulPosCount > uiTripPos )
      {
         int16_t wSpeedLimit = Param_ReadValue_16Bit( aeStartParam_Vel[ eProfile ] + i ) * NTS_VELOCITY_SCALE_FACTOR;
         if( ( GetDebug_ActiveAcceptanceTest() == ACCEPTANCE_TEST_NTS )
          && ( GetOperation_AutoMode() == MODE_A__TEST ) )
         {
            wSpeedLimit = Param_ReadValue_16Bit( aeStartParam_Vel[ eProfile ] + i ) * NTS_VELOCITY_SCALE_FACTOR_TEST;
            ucDebounceLimit = 1;
         }
         if( wCarSpeed > wSpeedLimit )
         {
            if( ucDebounceCounter >= ucDebounceLimit )
            {
               en_alarms eAlarm = ALM__NTS_UP_P1_1 + (eProfile*NUM_NTS_TRIP_POINTS) + i;
               if(eAlarm > ALM__NTS_UP_P4_8)
               {
                  eAlarm = ALM__NTS_UP_P4_8;
               }
               SetAlarm(eAlarm);
               bTripped = 1;
            }
            else
            {
               bClearCounter = 0;
               ucDebounceCounter++;
            }
            break;
         }
      }
   }

   if( bClearCounter )
   {
      ucDebounceCounter = 0;
   }

   return bTripped;
}
static uint8_t CheckFor_NTSTripDownAlarm( void )
{
   uint8_t bTripped = 0;
   uint8_t bClearCounter = 1;
   Motion_Profile eProfile = GetMotion_Profile();
   int16_t wCarSpeed = gstPosition_NTS.wVelocity;
   uint8_t ucDebounceLimit = Param_ReadValue_8Bit( enPARAM8__DebounceNTS );

   for( uint8_t i = 0; i < NUM_NTS_TRIP_POINTS; i++ )
   {
      uint32_t uiTripPos = Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 )
                       + ( Param_ReadValue_16Bit( aeStartParam_Pos[ eProfile ] + i ) * NTS_POSITION_SCALING );
      if( gstPosition_NTS.ulPosCount < uiTripPos )
      {
         int16_t wSpeedLimit = Param_ReadValue_16Bit( aeStartParam_Vel[ eProfile ] + i ) * NTS_VELOCITY_SCALE_FACTOR * -1;
         if( ( GetDebug_ActiveAcceptanceTest() == ACCEPTANCE_TEST_NTS )
          && ( GetOperation_AutoMode() == MODE_A__TEST ) )
         {
            wSpeedLimit = Param_ReadValue_16Bit( aeStartParam_Vel[ eProfile ] + i ) * NTS_VELOCITY_SCALE_FACTOR_TEST * -1;
            ucDebounceLimit = 1;
         }
         if( wCarSpeed < wSpeedLimit )
         {
            if( ucDebounceCounter >= ucDebounceLimit )
            {
               en_alarms eAlarm = ALM__NTS_DN_P1_1 + (eProfile*NUM_NTS_TRIP_POINTS) + i;
               if(eAlarm > ALM__NTS_DN_P4_8)
               {
                  eAlarm = ALM__NTS_DN_P4_8;
               }
               SetAlarm(eAlarm);
               bTripped = 1;
            }
            else
            {
               bClearCounter = 0;
               ucDebounceCounter++;
            }
            break;
         }
      }
   }

   if( bClearCounter )
   {
      ucDebounceCounter = 0;
   }

   return bTripped;
}

// The following functions for the NTS state machine

/*-----------------------------------------------------------------------------


 -----------------------------------------------------------------------------*/
static void ProcessNTS_Fault( void )
{
   geNTS_Fault = NTS_FAULT__NONE;
   geNTS_State = NTS__ALARM;
   ResetQuickStop();
}



/*-----------------------------------------------------------------------------


 -----------------------------------------------------------------------------*/
static void ProcessNTS_Alarm( void )
{
   // Parameters are valid, go to ready state
   if( CheckFor_InvalidNTSAlarm() )
   {
      geNTS_State  = NTS__READY;
   }
   // The car can run even with invalid parameters. This will cause the
   // car to trip on NTS ,ore often
   else if ( OnAutoOperation() &&  GetMotion_RunFlag())
   {
      geNTS_State  = NTS__RUNNING;
   }
}


/*-----------------------------------------------------------------------------


 -----------------------------------------------------------------------------*/
static void ProcessNTS_Ready( void )
{
   // Parameters are not valid, go to alarm state
   if( !CheckFor_InvalidNTSAlarm() )
   {
      geNTS_State  = NTS__ALARM;
   }
   // On auto operation and moving, go to running state
   else if ( OnAutoOperation() &&  GetMotion_RunFlag())
   {
      geNTS_State  = NTS__RUNNING;
   }
   ucDebounceCounter = 0;
}

/*-----------------------------------------------------------------------------


 -----------------------------------------------------------------------------*/

/*
   This state controls the quick stop output. If a this state determines that
   an NTS quick stop needs to occur then the quick stop variable is set.
*/
static void ProcessNTS_Running( void )
{
   // Car is not moving. Velocity = 0
    uint8_t bBypassTermLimits = GetOperation_BypassTermLimits();
   if ( !gstPosition_NTS.wVelocity && !GetMotion_RunFlag() )
   {
       geNTS_State  = NTS__READY;
       ucDebounceCounter = 0;
   }
   else if( ( GetOperation_ClassOfOp() == CLASSOP__AUTO ) && ( !bBypassTermLimits ) )
   {
      // Velocity  is positive when traveling up
      if( gstPosition_NTS.wVelocity > MIN_NTS_TRIP_SPEED )
      {
         if( CheckFor_NTSTripUpAlarm() )
         {
            SetQuickStop();
            geNTS_State  = NTS__TRIPPED;
         }
      }
      // Velocity is negative when traveling down
      else if( gstPosition_NTS.wVelocity < ( -1 * MIN_NTS_TRIP_SPEED ) )
      {
         if( CheckFor_NTSTripDownAlarm() )
         {
            SetQuickStop();
            geNTS_State  = NTS__TRIPPED;
         }
      }
   }
   else
   {
      ucDebounceCounter = 0;
   }
}


/*-----------------------------------------------------------------------------

If car is within two inches of the destination's learned floor postion
OR Car velocity is less than 5
OR Run flag is low
OR Faulted
THEN reset NTS

 -----------------------------------------------------------------------------*/
static void ProcessNTS_Tripped( void )
{
   uint32_t uiDestinationPosition = Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0 + GetOperation_DestinationFloor());
   uint32_t uiPositionUpperLimit = uiDestinationPosition + (uint32_t) TWO_INCHES;
   uint32_t uiPositionLowerLimit = uiDestinationPosition - (uint32_t) TWO_INCHES;
   uint32_t uiCurrentPosition = GetPosition_PositionCount();
   if( ( GetOperation_ClassOfOp() != CLASSOP__AUTO )
    || ( (gstPosition_NTS.wVelocity < 5) && (gstPosition_NTS.wVelocity > -5) )
    || ( !GetMotion_RunFlag() )
    || ( gstFault.bActiveFault )
    || ( ( uiCurrentPosition < uiPositionUpperLimit ) && ( uiCurrentPosition > uiPositionLowerLimit ) ) )
   {
      ResetQuickStop();
      geNTS_State  = NTS__READY;
   }
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
uint32_t NTS_CT_Init( struct st_module *pstThisModule )
{

   return 0;
}

/*------------------------------------------------------------------------------
Determines the state that should be processed. First a check is made to see if
there is an NTS error. If so the system will imedialty go to the nts fault
state. This is done in order to assume process a fault as soon as possible.
------------------------------------------------------------------------------*/
void NTS_CT_Run( void )
{
  // Next state logic for NTS.
  switch ( geNTS_State )
  {
      case NTS__READY:
          ProcessNTS_Ready();
          break;

      case NTS__RUNNING:
          ProcessNTS_Running();
          break;

      case NTS__TRIPPED:
          ProcessNTS_Tripped();
          break;

      case NTS__ALARM:
          ProcessNTS_Alarm();
          break;

      default:
      case NTS__FAULT:
          ProcessNTS_Fault();
          break;
  }

  if ( geNTS_State != geNTS_LastState )
  {
      geNTS_LastState = geNTS_State;
  }

  /* Send NTS trip signal to CPLD */
  uint8_t bInvertOutput = Param_ReadValue_1Bit(enPARAM1__Invert_NTS_Stop);

  SRU_Write_CPLD_Output( enSRU_CPLD_OUTPUTS_X1, gbQuickStop ^ bInvertOutput );
}

/*------------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

