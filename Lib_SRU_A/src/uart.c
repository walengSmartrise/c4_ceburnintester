#include "sru_a.h"
#include "sys.h"

#define MCUA_RS485            (LPC_UART1)
#define RS485_BAUD_EMOTIVE      (9600)
#define RS485_BAUD_DL20       (19200)
#define MCUA_RS485_IRQ        (UART1_IRQn)

#define MCUA_UART      (LPC_UART3)
#define MCUA_UART_BAUD (115200)
#define MCUA_UART_IRQ  (UART3_IRQn)


/* Transmit and receive ring buffers */
static RINGBUFF_T txring;
static RINGBUFF_T rxring;

static RINGBUFF_T txring_RS485;
static RINGBUFF_T rxring_RS485;

/* Ring buffer size */
#define UART_RB_SIZE (256)//(96)

/* Transmit and receive buffers */
static uint8_t rxbuff[UART_RB_SIZE];
static uint8_t txbuff[UART_RB_SIZE];

static uint8_t rxbuff_RS485[UART_RB_SIZE];
static uint8_t txbuff_RS485[UART_RB_SIZE];

void UART_AB_Init( void )
{
/* Setup UART for 115.2K8N1 */
   Chip_UART_Init      (MCUA_UART);
   Chip_UART_SetBaudFDR(MCUA_UART, MCUA_UART_BAUD);
   Chip_UART_ConfigData(MCUA_UART, (UART_LCR_WLEN8   | UART_LCR_SBS_1BIT));
   Chip_UART_SetupFIFOS(MCUA_UART, (UART_FCR_FIFO_EN | UART_FCR_TRG_LEV3 ));
   Chip_UART_TXEnable  (MCUA_UART);

   RingBuffer_Init(&rxring, rxbuff, 1, UART_RB_SIZE);
   RingBuffer_Init(&txring, txbuff, 1, UART_RB_SIZE);

   /* Enable receive data and line status interrupt */
   Chip_UART_IntEnable(MCUA_UART, (UART_IER_THREINT| UART_IER_RBRINT));

   /* preemption = 1, sub-priority = 1 */
#if INCREASED_PRIORITY_FOR_BITBANGED_CPLD_SPI
   NVIC_SetPriority(MCUA_UART_IRQ, 1);
#endif
   NVIC_EnableIRQ  (MCUA_UART_IRQ);
}
/* Initialize SRU RJ45 RS422 port for Dupar fixture */
void UART_RS485_EMOTIVE_Init( void )
{
    // Initilize each of the uarts.
    Chip_UART_Init(MCUA_RS485);

    Chip_UART_SetBaudFDR(MCUA_RS485, RS485_BAUD_EMOTIVE);
    Chip_UART_ConfigData(MCUA_RS485, (UART_LCR_WLEN8   | UART_LCR_SBS_1BIT) );
    Chip_UART_TXEnable  (MCUA_RS485);
    Chip_UART_SetupFIFOS(MCUA_RS485, (UART_FCR_FIFO_EN | UART_FCR_TRG_LEV3 ) );

    RingBuffer_Init(&rxring_RS485, rxbuff_RS485, 1, UART_RB_SIZE);
    RingBuffer_Init(&txring_RS485, txbuff_RS485, 1, UART_RB_SIZE);

    Chip_UART_IntEnable(MCUA_RS485, (UART_IER_THREINT| UART_IER_RBRINT) );
#if INCREASED_PRIORITY_FOR_BITBANGED_CPLD_SPI
    NVIC_SetPriority(MCUA_RS485_IRQ, 1);
#endif
    NVIC_EnableIRQ(MCUA_RS485_IRQ);
}
/* Initialize SRU RJ45 RS422 port for DL-20 fixture */
void UART_RS485_DL20_Init( void )
{
    // Initilize each of the uarts.
    Chip_UART_Init(MCUA_RS485);

    Chip_UART_SetBaudFDR(MCUA_RS485, RS485_BAUD_DL20);
    Chip_UART_ConfigData(MCUA_RS485, (UART_LCR_WLEN8   | UART_LCR_SBS_1BIT | UART_LCR_PARITY_DIS) );
    Chip_UART_TXEnable  (MCUA_RS485);
    Chip_UART_SetupFIFOS(MCUA_RS485, (UART_FCR_FIFO_EN | UART_FCR_TRG_LEV3 ) );

    RingBuffer_Init(&rxring_RS485, rxbuff_RS485, 1, UART_RB_SIZE);
    RingBuffer_Init(&txring_RS485, txbuff_RS485, 1, UART_RB_SIZE);

    Chip_UART_IntEnable(MCUA_RS485, (UART_IER_THREINT| UART_IER_RBRINT) );
#if INCREASED_PRIORITY_FOR_BITBANGED_CPLD_SPI
    NVIC_SetPriority(MCUA_RS485_IRQ, 1);
#endif
    NVIC_EnableIRQ(MCUA_RS485_IRQ);
}
/* Initialize SRU RJ45 RS422 port for EX-51 fixture */
void UART_RS485_EX51_Init( void )
{
    // Initilize each of the uarts.
    Chip_UART_Init(MCUA_RS485);

    Chip_UART_SetBaudFDR(MCUA_RS485, RS485_BAUD_DL20);
    Chip_UART_ConfigData(MCUA_RS485, (UART_LCR_WLEN8   | UART_LCR_SBS_1BIT | UART_LCR_PARITY_DIS) );
    Chip_UART_TXEnable  (MCUA_RS485);
    Chip_UART_SetupFIFOS(MCUA_RS485, (UART_FCR_FIFO_EN | UART_FCR_TRG_LEV3 ) );

    RingBuffer_Init(&txring_RS485, txbuff_RS485, 1, UART_RB_SIZE);

    Chip_UART_IntEnable(MCUA_RS485, (UART_IER_THREINT| UART_IER_RBRINT) );
#if INCREASED_PRIORITY_FOR_BITBANGED_CPLD_SPI
    NVIC_SetPriority(MCUA_RS485_IRQ, 1);
#endif
    NVIC_EnableIRQ(MCUA_RS485_IRQ);
}

// Only loads if there is room in buffer
uint32_t UART_AB_SendRB(const void *data, int bytes)
{
   uint32_t uiReturn = 0;
   if(RingBuffer_GetFree(&txring) >= bytes)
   {
      uiReturn = Chip_UART_SendRB(MCUA_UART, &txring, data, bytes);
   }

   return uiReturn;
}

uint8_t UART_AB_ReadByte(uint8_t *pucByte)
{
   if(!RingBuffer_IsEmpty(&rxring))
   {
      RingBuffer_Pop(&rxring, pucByte);
      return 1;
   }
   return 0;
}

uint32_t UART_RS485_SendRB(const void *data, int bytes)
{
   uint32_t uiReturn = 0;
   if(RingBuffer_GetFree(&txring_RS485) >= bytes)
   {
      uiReturn = Chip_UART_SendRB(MCUA_RS485, &txring_RS485, data, bytes);
   }

   return uiReturn;
}

uint8_t UART_RS485_ReadByte(uint8_t *pucByte)
{
   if(!RingBuffer_IsEmpty(&rxring_RS485))
   {
      RingBuffer_Pop(&rxring_RS485, pucByte);
      return 1;
   }
   return 0;
}


void UART1_IRQHandler(void)
{
   // handles tx - same as default handler
   uint8_t bTxEnabled = ( Chip_UART_GetIntsEnabled(MCUA_RS485) & UART_IER_THREINT ) != 0;
   uint8_t bTxBufferNotEmpty = ( Chip_UART_ReadLineStatus(MCUA_RS485) & UART_LSR_THRE ) != 0;
   if ( bTxEnabled
     && bTxBufferNotEmpty )
   {
      Chip_UART_TXIntHandlerRB(MCUA_RS485, &txring_RS485);
   }

   if (RingBuffer_IsEmpty(&txring_RS485)) {
      Chip_UART_IntDisable(MCUA_RS485, UART_IIR_INTID_THRE);
   }

   Chip_UART_RXIntHandlerRB(MCUA_RS485, &rxring_RS485);
}

void UART3_IRQHandler(void)
{
   // handles tx - same as default handler
   uint8_t bTxEnabled = ( Chip_UART_GetIntsEnabled(MCUA_UART) & UART_IER_THREINT ) != 0;
   uint8_t bTxBufferNotEmpty = ( Chip_UART_ReadLineStatus(MCUA_UART) & UART_LSR_THRE ) != 0;
   if ( bTxEnabled
     && bTxBufferNotEmpty )
   {
      Chip_UART_TXIntHandlerRB(MCUA_UART, &txring);
   }

   if (RingBuffer_IsEmpty(&txring)) {
      Chip_UART_IntDisable(MCUA_UART, UART_IIR_INTID_THRE);
   }

   Chip_UART_RXIntHandlerRB(MCUA_UART, &rxring);
}

