/***
 *       _____                          __         _
 *      / ___/ ____ ___   ____ _ _____ / /_ _____ (_)_____ ___
 *      \__ \ / __ `__ \ / __ `// ___// __// ___// // ___// _ \
 *     ___/ // / / / / // /_/ // /   / /_ / /   / /(__  )/  __/
 *    /____//_/ /_/ /_/ \__,_//_/    \__//_/   /_//____/ \___/
 *        ______               _                           _
 *       / ____/____   ____ _ (_)____   ___   ___   _____ (_)____   ____ _
 *      / __/  / __ \ / __ `// // __ \ / _ \ / _ \ / ___// // __ \ / __ `/
 *     / /___ / / / // /_/ // // / / //  __//  __// /   / // / / // /_/ /
 *    /_____//_/ /_/ \__, //_//_/ /_/ \___/ \___//_/   /_//_/ /_/ \__, /
 *                  /____/                                       /____/
 */
#ifndef GROUPNET_DATAGRAM_H_
#define GROUPNET_DATAGRAM_H_


/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MAX_GROUP_NET_DATAGRAMS        ( 255 )
#define MAX_GROUP_NET_CAR_DATAGRAMS    ( 25  )
#define MAX_GROUP_NET_RISER_DATAGRAMS  ( 10  )
#define MAX_GROUP_NET_SHIELD_DATAGRAMS ( 10  )
#define MAX_GROUP_NET_XREG_DATAGRAMS   ( 2  )
#define MAX_GROUP_NET_DDM_DATAGRAMS    ( 3  )

/* Destination bitmap bitmasks from enum en_group_net_nodes */
#define GROUP_DEST__NONE       (0U)
#define GROUP_DEST__CAR1       (1 << GROUP_NET__CAR_1)
#define GROUP_DEST__CAR2       (1 << GROUP_NET__CAR_2)
#define GROUP_DEST__CAR3       (1 << GROUP_NET__CAR_3)
#define GROUP_DEST__CAR4       (1 << GROUP_NET__CAR_4)
#define GROUP_DEST__CAR5       (1 << GROUP_NET__CAR_5)
#define GROUP_DEST__CAR6       (1 << GROUP_NET__CAR_6)
#define GROUP_DEST__CAR7       (1 << GROUP_NET__CAR_7)
#define GROUP_DEST__CAR8       (1 << GROUP_NET__CAR_8)
#define GROUP_DEST__RIS1       (1 << GROUP_NET__RIS_1)
#define GROUP_DEST__RIS2       (1 << GROUP_NET__RIS_2)
#define GROUP_DEST__RIS3       (1 << GROUP_NET__RIS_3)
#define GROUP_DEST__RIS4       (1 << GROUP_NET__RIS_4)
#define GROUP_DEST__SHIELD     (1 << GROUP_NET__SHIELD)
#define GROUP_DEST__XREG       (1 << GROUP_NET__XREG)
#define GROUP_DEST__DDM        (1 << GROUP_NET__RIS_1) /* Due to limited size of destination bitmap field, DDM map will be the same as the riser */

#define GROUP_DEST__ALL_CAR    (GROUP_DEST__CAR1|GROUP_DEST__CAR2|GROUP_DEST__CAR3|GROUP_DEST__CAR4|GROUP_DEST__CAR5|GROUP_DEST__CAR6|GROUP_DEST__CAR7|GROUP_DEST__CAR8)
#define GROUP_DEST__ALL_RIS    (GROUP_DEST__RIS1|GROUP_DEST__RIS2|GROUP_DEST__RIS3|GROUP_DEST__RIS4)
#define GROUP_DEST__ALL        (0x3FFF)

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/


typedef struct
{
        un_sdata_datagram data;
        // This is the number of ms counts can be 0 to 255
        //    uint16_t lifetime_ms;

        //    uint32_t uiReceivedCount; // Number times this datagram has been received
} GroupNetTable_Element;


/*
    ID value for each datagrams. Each Datagram is given
    a unique ID. The lower the datagram id the higher
    priority that it will have. Support a max of 255
    datagrams.
*/
/*
 * Global group net mappings
 */
typedef enum
{
    GROUP_NET_DATAGRAM_CAR0_DATA0 ,
    GROUP_NET_DATAGRAM_CAR0_DATA1 ,
    GROUP_NET_DATAGRAM_CAR0_DATA2 ,
    GROUP_NET_DATAGRAM_CAR0_DATA3 ,
    GROUP_NET_DATAGRAM_CAR0_DATA4 ,
    GROUP_NET_DATAGRAM_CAR0_DATA5 ,
    GROUP_NET_DATAGRAM_CAR0_DATA6 ,
    GROUP_NET_DATAGRAM_CAR0_DATA7 ,
    GROUP_NET_DATAGRAM_CAR0_DATA8 ,
    GROUP_NET_DATAGRAM_CAR0_DATA9 ,
    GROUP_NET_DATAGRAM_CAR0_DATA10,
    GROUP_NET_DATAGRAM_CAR0_DATA11,
    GROUP_NET_DATAGRAM_CAR0_DATA12,
    GROUP_NET_DATAGRAM_CAR0_DATA13,
    GROUP_NET_DATAGRAM_CAR0_DATA14,
    GROUP_NET_DATAGRAM_CAR0_DATA15,
    GROUP_NET_DATAGRAM_CAR0_DATA16,
    GROUP_NET_DATAGRAM_CAR0_DATA17,
    GROUP_NET_DATAGRAM_CAR0_DATA18,
    GROUP_NET_DATAGRAM_CAR0_DATA19,
    GROUP_NET_DATAGRAM_CAR0_DATA20,
    GROUP_NET_DATAGRAM_CAR0_DATA21,
    GROUP_NET_DATAGRAM_CAR0_DATA22,
    GROUP_NET_DATAGRAM_CAR0_DATA23,
    GROUP_NET_DATAGRAM_CAR0_DATA24,
    GROUP_NET_DATAGRAM_CAR1_DATA0 ,
    GROUP_NET_DATAGRAM_CAR1_DATA1 ,
    GROUP_NET_DATAGRAM_CAR1_DATA2 ,
    GROUP_NET_DATAGRAM_CAR1_DATA3 ,
    GROUP_NET_DATAGRAM_CAR1_DATA4 ,
    GROUP_NET_DATAGRAM_CAR1_DATA5 ,
    GROUP_NET_DATAGRAM_CAR1_DATA6 ,
    GROUP_NET_DATAGRAM_CAR1_DATA7 ,
    GROUP_NET_DATAGRAM_CAR1_DATA8 ,
    GROUP_NET_DATAGRAM_CAR1_DATA9 ,
    GROUP_NET_DATAGRAM_CAR1_DATA10,
    GROUP_NET_DATAGRAM_CAR1_DATA11,
    GROUP_NET_DATAGRAM_CAR1_DATA12,
    GROUP_NET_DATAGRAM_CAR1_DATA13,
    GROUP_NET_DATAGRAM_CAR1_DATA14,
    GROUP_NET_DATAGRAM_CAR1_DATA15,
    GROUP_NET_DATAGRAM_CAR1_DATA16,
    GROUP_NET_DATAGRAM_CAR1_DATA17,
    GROUP_NET_DATAGRAM_CAR1_DATA18,
    GROUP_NET_DATAGRAM_CAR1_DATA19,
    GROUP_NET_DATAGRAM_CAR1_DATA20,
    GROUP_NET_DATAGRAM_CAR1_DATA21,
    GROUP_NET_DATAGRAM_CAR1_DATA22,
    GROUP_NET_DATAGRAM_CAR1_DATA23,
    GROUP_NET_DATAGRAM_CAR1_DATA24,
    GROUP_NET_DATAGRAM_CAR2_DATA0 ,
    GROUP_NET_DATAGRAM_CAR2_DATA1 ,
    GROUP_NET_DATAGRAM_CAR2_DATA2 ,
    GROUP_NET_DATAGRAM_CAR2_DATA3 ,
    GROUP_NET_DATAGRAM_CAR2_DATA4 ,
    GROUP_NET_DATAGRAM_CAR2_DATA5 ,
    GROUP_NET_DATAGRAM_CAR2_DATA6 ,
    GROUP_NET_DATAGRAM_CAR2_DATA7 ,
    GROUP_NET_DATAGRAM_CAR2_DATA8 ,
    GROUP_NET_DATAGRAM_CAR2_DATA9 ,
    GROUP_NET_DATAGRAM_CAR2_DATA10,
    GROUP_NET_DATAGRAM_CAR2_DATA11,
    GROUP_NET_DATAGRAM_CAR2_DATA12,
    GROUP_NET_DATAGRAM_CAR2_DATA13,
    GROUP_NET_DATAGRAM_CAR2_DATA14,
    GROUP_NET_DATAGRAM_CAR2_DATA15,
    GROUP_NET_DATAGRAM_CAR2_DATA16,
    GROUP_NET_DATAGRAM_CAR2_DATA17,
    GROUP_NET_DATAGRAM_CAR2_DATA18,
    GROUP_NET_DATAGRAM_CAR2_DATA19,
    GROUP_NET_DATAGRAM_CAR2_DATA20,
    GROUP_NET_DATAGRAM_CAR2_DATA21,
    GROUP_NET_DATAGRAM_CAR2_DATA22,
    GROUP_NET_DATAGRAM_CAR2_DATA23,
    GROUP_NET_DATAGRAM_CAR2_DATA24,
    GROUP_NET_DATAGRAM_CAR3_DATA0 ,
    GROUP_NET_DATAGRAM_CAR3_DATA1 ,
    GROUP_NET_DATAGRAM_CAR3_DATA2 ,
    GROUP_NET_DATAGRAM_CAR3_DATA3 ,
    GROUP_NET_DATAGRAM_CAR3_DATA4 ,
    GROUP_NET_DATAGRAM_CAR3_DATA5 ,
    GROUP_NET_DATAGRAM_CAR3_DATA6 ,
    GROUP_NET_DATAGRAM_CAR3_DATA7 ,
    GROUP_NET_DATAGRAM_CAR3_DATA8 ,
    GROUP_NET_DATAGRAM_CAR3_DATA9 ,
    GROUP_NET_DATAGRAM_CAR3_DATA10,
    GROUP_NET_DATAGRAM_CAR3_DATA11,
    GROUP_NET_DATAGRAM_CAR3_DATA12,
    GROUP_NET_DATAGRAM_CAR3_DATA13,
    GROUP_NET_DATAGRAM_CAR3_DATA14,
    GROUP_NET_DATAGRAM_CAR3_DATA15,
    GROUP_NET_DATAGRAM_CAR3_DATA16,
    GROUP_NET_DATAGRAM_CAR3_DATA17,
    GROUP_NET_DATAGRAM_CAR3_DATA18,
    GROUP_NET_DATAGRAM_CAR3_DATA19,
    GROUP_NET_DATAGRAM_CAR3_DATA20,
    GROUP_NET_DATAGRAM_CAR3_DATA21,
    GROUP_NET_DATAGRAM_CAR3_DATA22,
    GROUP_NET_DATAGRAM_CAR3_DATA23,
    GROUP_NET_DATAGRAM_CAR3_DATA24,
    GROUP_NET_DATAGRAM_CAR4_DATA0 ,
    GROUP_NET_DATAGRAM_CAR4_DATA1 ,
    GROUP_NET_DATAGRAM_CAR4_DATA2 ,
    GROUP_NET_DATAGRAM_CAR4_DATA3 ,
    GROUP_NET_DATAGRAM_CAR4_DATA4 ,
    GROUP_NET_DATAGRAM_CAR4_DATA5 ,
    GROUP_NET_DATAGRAM_CAR4_DATA6 ,
    GROUP_NET_DATAGRAM_CAR4_DATA7 ,
    GROUP_NET_DATAGRAM_CAR4_DATA8 ,
    GROUP_NET_DATAGRAM_CAR4_DATA9 ,
    GROUP_NET_DATAGRAM_CAR4_DATA10,
    GROUP_NET_DATAGRAM_CAR4_DATA11,
    GROUP_NET_DATAGRAM_CAR4_DATA12,
    GROUP_NET_DATAGRAM_CAR4_DATA13,
    GROUP_NET_DATAGRAM_CAR4_DATA14,
    GROUP_NET_DATAGRAM_CAR4_DATA15,
    GROUP_NET_DATAGRAM_CAR4_DATA16,
    GROUP_NET_DATAGRAM_CAR4_DATA17,
    GROUP_NET_DATAGRAM_CAR4_DATA18,
    GROUP_NET_DATAGRAM_CAR4_DATA19,
    GROUP_NET_DATAGRAM_CAR4_DATA20,
    GROUP_NET_DATAGRAM_CAR4_DATA21,
    GROUP_NET_DATAGRAM_CAR4_DATA22,
    GROUP_NET_DATAGRAM_CAR4_DATA23,
    GROUP_NET_DATAGRAM_CAR4_DATA24,
    GROUP_NET_DATAGRAM_CAR5_DATA0 ,
    GROUP_NET_DATAGRAM_CAR5_DATA1 ,
    GROUP_NET_DATAGRAM_CAR5_DATA2 ,
    GROUP_NET_DATAGRAM_CAR5_DATA3 ,
    GROUP_NET_DATAGRAM_CAR5_DATA4 ,
    GROUP_NET_DATAGRAM_CAR5_DATA5 ,
    GROUP_NET_DATAGRAM_CAR5_DATA6 ,
    GROUP_NET_DATAGRAM_CAR5_DATA7 ,
    GROUP_NET_DATAGRAM_CAR5_DATA8 ,
    GROUP_NET_DATAGRAM_CAR5_DATA9 ,
    GROUP_NET_DATAGRAM_CAR5_DATA10,
    GROUP_NET_DATAGRAM_CAR5_DATA11,
    GROUP_NET_DATAGRAM_CAR5_DATA12,
    GROUP_NET_DATAGRAM_CAR5_DATA13,
    GROUP_NET_DATAGRAM_CAR5_DATA14,
    GROUP_NET_DATAGRAM_CAR5_DATA15,
    GROUP_NET_DATAGRAM_CAR5_DATA16,
    GROUP_NET_DATAGRAM_CAR5_DATA17,
    GROUP_NET_DATAGRAM_CAR5_DATA18,
    GROUP_NET_DATAGRAM_CAR5_DATA19,
    GROUP_NET_DATAGRAM_CAR5_DATA20,
    GROUP_NET_DATAGRAM_CAR5_DATA21,
    GROUP_NET_DATAGRAM_CAR5_DATA22,
    GROUP_NET_DATAGRAM_CAR5_DATA23,
    GROUP_NET_DATAGRAM_CAR5_DATA24,
    GROUP_NET_DATAGRAM_CAR6_DATA0 ,
    GROUP_NET_DATAGRAM_CAR6_DATA1 ,
    GROUP_NET_DATAGRAM_CAR6_DATA2 ,
    GROUP_NET_DATAGRAM_CAR6_DATA3 ,
    GROUP_NET_DATAGRAM_CAR6_DATA4 ,
    GROUP_NET_DATAGRAM_CAR6_DATA5 ,
    GROUP_NET_DATAGRAM_CAR6_DATA6 ,
    GROUP_NET_DATAGRAM_CAR6_DATA7 ,
    GROUP_NET_DATAGRAM_CAR6_DATA8 ,
    GROUP_NET_DATAGRAM_CAR6_DATA9 ,
    GROUP_NET_DATAGRAM_CAR6_DATA10,
    GROUP_NET_DATAGRAM_CAR6_DATA11,
    GROUP_NET_DATAGRAM_CAR6_DATA12,
    GROUP_NET_DATAGRAM_CAR6_DATA13,
    GROUP_NET_DATAGRAM_CAR6_DATA14,
    GROUP_NET_DATAGRAM_CAR6_DATA15,
    GROUP_NET_DATAGRAM_CAR6_DATA16,
    GROUP_NET_DATAGRAM_CAR6_DATA17,
    GROUP_NET_DATAGRAM_CAR6_DATA18,
    GROUP_NET_DATAGRAM_CAR6_DATA19,
    GROUP_NET_DATAGRAM_CAR6_DATA20,
    GROUP_NET_DATAGRAM_CAR6_DATA21,
    GROUP_NET_DATAGRAM_CAR6_DATA22,
    GROUP_NET_DATAGRAM_CAR6_DATA23,
    GROUP_NET_DATAGRAM_CAR6_DATA24,
    GROUP_NET_DATAGRAM_CAR7_DATA0 ,
    GROUP_NET_DATAGRAM_CAR7_DATA1 ,
    GROUP_NET_DATAGRAM_CAR7_DATA2 ,
    GROUP_NET_DATAGRAM_CAR7_DATA3 ,
    GROUP_NET_DATAGRAM_CAR7_DATA4 ,
    GROUP_NET_DATAGRAM_CAR7_DATA5 ,
    GROUP_NET_DATAGRAM_CAR7_DATA6 ,
    GROUP_NET_DATAGRAM_CAR7_DATA7 ,
    GROUP_NET_DATAGRAM_CAR7_DATA8 ,
    GROUP_NET_DATAGRAM_CAR7_DATA9 ,
    GROUP_NET_DATAGRAM_CAR7_DATA10,
    GROUP_NET_DATAGRAM_CAR7_DATA11,
    GROUP_NET_DATAGRAM_CAR7_DATA12,
    GROUP_NET_DATAGRAM_CAR7_DATA13,
    GROUP_NET_DATAGRAM_CAR7_DATA14,
    GROUP_NET_DATAGRAM_CAR7_DATA15,
    GROUP_NET_DATAGRAM_CAR7_DATA16,
    GROUP_NET_DATAGRAM_CAR7_DATA17,
    GROUP_NET_DATAGRAM_CAR7_DATA18,
    GROUP_NET_DATAGRAM_CAR7_DATA19,
    GROUP_NET_DATAGRAM_CAR7_DATA20,
    GROUP_NET_DATAGRAM_CAR7_DATA21,
    GROUP_NET_DATAGRAM_CAR7_DATA22,
    GROUP_NET_DATAGRAM_CAR7_DATA23,
    GROUP_NET_DATAGRAM_CAR7_DATA24,
    GROUP_NET_DATAGRAM_RISER0_DATA0,
    GROUP_NET_DATAGRAM_RISER0_DATA1,
    GROUP_NET_DATAGRAM_RISER0_DATA2,
    GROUP_NET_DATAGRAM_RISER0_DATA3,
    GROUP_NET_DATAGRAM_RISER0_DATA4,
    GROUP_NET_DATAGRAM_RISER0_DATA5,
    GROUP_NET_DATAGRAM_RISER0_DATA6,
    GROUP_NET_DATAGRAM_RISER0_DATA7,
    GROUP_NET_DATAGRAM_RISER0_DATA8,
    GROUP_NET_DATAGRAM_RISER0_DATA9,
    GROUP_NET_DATAGRAM_RISER1_DATA0,
    GROUP_NET_DATAGRAM_RISER1_DATA1,
    GROUP_NET_DATAGRAM_RISER1_DATA2,
    GROUP_NET_DATAGRAM_RISER1_DATA3,
    GROUP_NET_DATAGRAM_RISER1_DATA4,
    GROUP_NET_DATAGRAM_RISER1_DATA5,
    GROUP_NET_DATAGRAM_RISER1_DATA6,
    GROUP_NET_DATAGRAM_RISER1_DATA7,
    GROUP_NET_DATAGRAM_RISER1_DATA8,
    GROUP_NET_DATAGRAM_RISER1_DATA9,
    GROUP_NET_DATAGRAM_RISER2_DATA0,
    GROUP_NET_DATAGRAM_RISER2_DATA1,
    GROUP_NET_DATAGRAM_RISER2_DATA2,
    GROUP_NET_DATAGRAM_RISER2_DATA3,
    GROUP_NET_DATAGRAM_RISER2_DATA4,
    GROUP_NET_DATAGRAM_RISER2_DATA5,
    GROUP_NET_DATAGRAM_RISER2_DATA6,
    GROUP_NET_DATAGRAM_RISER2_DATA7,
    GROUP_NET_DATAGRAM_RISER2_DATA8,
    GROUP_NET_DATAGRAM_RISER2_DATA9,
    GROUP_NET_DATAGRAM_RISER3_DATA0,
    GROUP_NET_DATAGRAM_RISER3_DATA1,
    GROUP_NET_DATAGRAM_RISER3_DATA2,
    GROUP_NET_DATAGRAM_RISER3_DATA3,
    GROUP_NET_DATAGRAM_RISER3_DATA4,
    GROUP_NET_DATAGRAM_RISER3_DATA5,
    GROUP_NET_DATAGRAM_RISER3_DATA6,
    GROUP_NET_DATAGRAM_RISER3_DATA7,
    GROUP_NET_DATAGRAM_RISER3_DATA8,
    GROUP_NET_DATAGRAM_RISER3_DATA9,
    GROUP_NET_DATAGRAM_SHIELD_DATA0,
    GROUP_NET_DATAGRAM_SHIELD_DATA1,
    GROUP_NET_DATAGRAM_SHIELD_DATA2,
    GROUP_NET_DATAGRAM_SHIELD_DATA3,
    GROUP_NET_DATAGRAM_SHIELD_DATA4,
    GROUP_NET_DATAGRAM_SHIELD_DATA5,
    GROUP_NET_DATAGRAM_SHIELD_DATA6,
    GROUP_NET_DATAGRAM_SHIELD_DATA7,
    GROUP_NET_DATAGRAM_SHIELD_DATA8,
    GROUP_NET_DATAGRAM_SHIELD_DATA9,
    GROUP_NET_DATAGRAM_XREG_DATA0,
    GROUP_NET_DATAGRAM_XREG_DATA1,
    GROUP_NET_DATAGRAM_DDM_DATA0,
    GROUP_NET_DATAGRAM_DDM_DATA1,
    GROUP_NET_DATAGRAM_DDM_DATA2,

    GROUP_NET_NUM_DATAGRAMS
}GroupNet_DatagramID;

/*
 * Local datagram names
 */
typedef enum
{
   DG_GroupNet_SHIELD__SyncTime,
   DG_GroupNet_SHIELD__RemoteCommand,
   DG_GroupNet_SHIELD__LatchCarCall,
   DG_GroupNet_SHIELD__LatchHallCall,
   DG_GroupNet_SHIELD__ClearHallCall,
   DG_GroupNet_SHIELD__ParamEditRequest,
   DG_GroupNet_SHIELD__ParkingFloors,
   DG_GroupNet_SHIELD__Virtual_CCEnables,
   DG_GroupNet_SHIELD__DynamicParking,
   DG_GroupNet_SHIELD__UNUSED_9,

   NUM_GroupNet_SHIELD_DATAGRAMS
} en_group_net_shield_datagrams;

typedef enum
{
   DG_GroupNet_RIS__INPUT_STATE,
   DG_GroupNet_RIS__LATCHED_HALL_CALLS_UP,
   DG_GroupNet_RIS__LATCHED_HALL_CALLS_DN,
   DG_GroupNet_RIS__HALLBOARD_STATUS,
   DG_GroupNet_RIS__CAR_REQUEST,
   DG_GroupNet_RIS__DispatchPanel,// TODO move to shield
   NUM_GroupNet_RIS_DATAGRAMS
}en_group_net_ris_datagrams;

typedef enum
{
   DG_GroupNet_MRB__MappedInputs1,
   DG_GroupNet_MRB__MappedInputs2,
   DG_GroupNet_MRB__MappedInputs3,
   DG_GroupNet_MRB__MappedOutputs1,
   DG_GroupNet_MRB__MappedOutputs2,
   DG_GroupNet_MRB__CAR_STATUS,
   DG_GroupNet_MRB__CAR_POSITION,
   DG_GroupNet_MRB__CarCalls1,
   DG_GroupNet_MRB__CarCalls2,
   DG_GroupNet_MRB__CarCalls3,
   DG_GroupNet_MRB__RiserOutputs,
   DG_GroupNet_MRB__CarData,
   DG_GroupNet_MRB__RemoteCommandFeedback,
   DG_GroupNet_MRB__VIPMasterDispatcher,
   DG_GroupNet_MRB__Faults,
   DG_GroupNet_MRB__Alarms,
   DG_GroupNet_MRB__RunLog,
   DG_GroupNet_MRB__MasterDispatcher,
   DG_GroupNet_MRB__DynamicParking,
   DG_GroupNet_MRB__CarOperation,
   DG_GroupNet_MRB__MasterCommand,
   DG_GroupNet_MRB__EmergencyPower,
   DG_GroupNet_MRB__UNUSED22,
   DG_GroupNet_MRB__ParamEditResponse,
   DG_GroupNet_MRB__SyncClock,
   DG_GroupNet_MRB__XRegCommand, // Keep at end of datagrams enum. This is an add on that is independent of the typical 25 datagram limit per car. This datagram is transmitted over the group XREG DATA 0 datagram.
   NUM_GroupNet_MRB_DATAGRAMS
} en_group_net_mrb_datagrams;

typedef enum
{
   DG_GroupNet_XREG__UNUSED0,
   DG_GroupNet_XREG__UNUSED1,

   NUM_GroupNet_XREG_DATAGRAMS
} en_group_net_xreg_datagrams;

typedef enum
{
   DG_GroupNet_DDM__LATCH_HC,
   DG_GroupNet_DDM__LATCH_CC,
   DG_GroupNet_DDM__HEARTBEAT,

   NUM_GroupNet_DDM_DATAGRAMS
} en_group_net_ddm_datagrams;

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*
* Place function prototypes that need to be seen by multiple source files here.
*
*----------------------------------------------------------------------------*/
/*
 * Local to global network mappings
 */
GroupNet_DatagramID GroupNet_LocalCarIDToGlobalID   (uint8_t ucCarIndex, en_group_net_mrb_datagrams ucDatagramID);
GroupNet_DatagramID GroupNet_LocalRiserIDToGlobalID (uint8_t ucRiserIndex, en_group_net_ris_datagrams ucDatagramID);
GroupNet_DatagramID GroupNet_LocalShieldIDToGlobalID(en_group_net_shield_datagrams ucDatagramID);
GroupNet_DatagramID GroupNet_LocalXRegIDToGlobalID(en_group_net_xreg_datagrams ucDatagramID);
GroupNet_DatagramID GroupNet_LocalDDMIDToGlobalID(en_group_net_ddm_datagrams ucDatagramID);

uint8_t GroupNet_GlobalIDToLocalNodeID( GroupNet_DatagramID eGroupDatagramID );

GroupNet_DatagramID  ExtractFromBusID_DatagramID(uint32_t uiCAN_Message_ID);
uint8_t  ExtractFromBusID_NetworkID (uint32_t uiCAN_Message_ID);
uint8_t  ExtractFromBusID_SourceID  (uint32_t uiCAN_Message_ID);
uint16_t ExtractFromBusID_DestinationBitmap(uint32_t uiCAN_Message_ID);

uint32_t GroupNet_EncodeCANMessageID( GroupNet_DatagramID eDatagram,
                                      enum en_sdata_networks eNet,
                                      enum en_group_net_nodes eSource,
                                      uint16_t uwDest );

uint16_t GroupNet_GetDestinationBitmap( enum en_group_net_nodes eNode );


#endif
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
