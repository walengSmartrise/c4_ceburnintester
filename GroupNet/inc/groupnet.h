/*
 * groupnet.h
 *
 *  Created on: Sep 28, 2017
 *      Author: SeanA
 */

#ifndef GROUPNET_DATAGRAMS_H_
#define GROUPNET_DATAGRAMS_H_

#include "groupnet_datagrams.h"

void InsertGroupNetDatagram_ByDatagramID(un_sdata_datagram*, GroupNet_DatagramID);
void ClrGroupNetDatagramDirtyBit( GroupNet_DatagramID eID );
uint8_t GetGroupNetDatagramDirtyBit( GroupNet_DatagramID eID );

un_sdata_datagram* GetShieldDatagram(GroupNet_DatagramID tmpID);
un_sdata_datagram* GetRiserDatagram(GroupNet_DatagramID tmpID);

un_sdata_datagram* GetGroupDatagram(GroupNet_DatagramID tmpID);

uint8_t GroupNet_GetDatagramRXCounter( GroupNet_DatagramID eGroupDatagramID );
#endif /* GROUPNET_H_ */
