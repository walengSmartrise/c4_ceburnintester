#include "groupnet.h"
#include "groupnet_datagrams.h"

static GroupNetTable_Element data[GROUP_NET_NUM_DATAGRAMS];
static uint32_t auiDatagramDirtyBits[BITMAP32_SIZE(GROUP_NET_NUM_DATAGRAMS)];
/*----------------------------------------------------------------------------
   TODO remove debugging counters
 *----------------------------------------------------------------------------*/
static uint8_t aucRXCounter[GROUP_NET_NUM_DATAGRAMS];
uint8_t GroupNet_GetDatagramRXCounter( GroupNet_DatagramID eGroupDatagramID )
{
   return aucRXCounter[eGroupDatagramID];
}
/*----------------------------------------------------------------------------
   Generic insert network datagram function
   //TODO add bettter filtering before calling this function
 *----------------------------------------------------------------------------*/
void InsertGroupNetDatagram_ByDatagramID(un_sdata_datagram* rcvdDatagram, GroupNet_DatagramID id)
{
    // Set the dirty bit
    Sys_Bit_Set_Array32(&auiDatagramDirtyBits[0], id, BITMAP32_SIZE(GROUP_NET_NUM_DATAGRAMS), 1);

    // Set the datagram lifetime
    //shared_data[id].lifetime_ms = GetDatagramLifetime_ms(id);

    // Set the datagram id
    data[id].data.aui32[0] = rcvdDatagram->aui32[0];
    data[id].data.aui32[1] = rcvdDatagram->aui32[1];

    aucRXCounter[id]++;
}
/*----------------------------------------------------------------------------
   InsertSharedDatagram_ByDatagramID will set the dirty bit for the datagram
 *----------------------------------------------------------------------------*/
inline void ClrGroupNetDatagramDirtyBit( GroupNet_DatagramID eID )
{
   Sys_Bit_Set_Array32(&auiDatagramDirtyBits[0], eID, BITMAP32_SIZE(GROUP_NET_NUM_DATAGRAMS), 0);
}
inline uint8_t GetGroupNetDatagramDirtyBit( GroupNet_DatagramID eID )
{
   return Sys_Bit_Get_Array32(&auiDatagramDirtyBits[0], eID, BITMAP32_SIZE(GROUP_NET_NUM_DATAGRAMS));
}
/*----------------------------------------------------------------------------
   Get datagram by ID
 *----------------------------------------------------------------------------*/
void GetGroupDatagram_ByDatagramID(un_sdata_datagram* punDatagram, GroupNet_DatagramID id)
{
   if(id < GROUP_NET_NUM_DATAGRAMS)
   {
      punDatagram->aui32[0] = data[id].data.aui32[0];
      punDatagram->aui32[1] = data[id].data.aui32[1];
   }
   else
   {
      punDatagram->aui32[0] = 0;
      punDatagram->aui32[1] = 0;
   }
}
un_sdata_datagram* GetShieldDatagram(GroupNet_DatagramID datagramID)
{
    // The assumption is that the user has specified a valid id
    return &data[datagramID].data;
}

un_sdata_datagram* GetRiserDatagram(GroupNet_DatagramID datagramID)
{
    // The assumption is that the user has specified a valid id
    return &data[datagramID].data;
}
un_sdata_datagram* GetGroupDatagram(GroupNet_DatagramID datagramID)
{
    // The assumption is that the user has specified a valid id
    return &data[datagramID].data;
}
/*----------------------------------------------------------------------------
   extract sections from CAN message ID
 *----------------------------------------------------------------------------*/
GroupNet_DatagramID ExtractFromBusID_DatagramID(uint32_t uiCAN_Message_ID)
{
   return (uiCAN_Message_ID >> 21) & 0xFF;
}
uint8_t ExtractFromBusID_NetworkID(uint32_t uiCAN_Message_ID)
{
   return (uiCAN_Message_ID >> 18) & 0x07;
}
uint8_t ExtractFromBusID_SourceID(uint32_t uiCAN_Message_ID)
{
   return (uiCAN_Message_ID >> 14) & 0x0F;
}
uint16_t ExtractFromBusID_DestinationBitmap(uint32_t uiCAN_Message_ID)
{
   return uiCAN_Message_ID & 0x3FFF;
}
/*----------------------------------------------------------------------------
   Encode CAN message ID
 *----------------------------------------------------------------------------*/
uint32_t GroupNet_EncodeCANMessageID( GroupNet_DatagramID eDatagram,
                                      enum en_sdata_networks eNet,
                                      enum en_group_net_nodes eSource,
                                      uint16_t uwDest )
{
   uint32_t uiID = CAN_EXTEND_ID_USAGE
                 | ( (eDatagram & 0x00FF) << 21 )
                 | ( (eNet & 0x0007) << 18 )
                 | ( (eSource & 0x000F) << 14 )
                 | ( (uwDest & 0x3FFF) );
   return uiID;
}


/*----------------------------------------------------------------------------
   Convert local ID to a global ID
 *----------------------------------------------------------------------------*/
GroupNet_DatagramID GroupNet_LocalCarIDToGlobalID   (uint8_t ucCarIndex, en_group_net_mrb_datagrams ucDatagramID)
{
    // Make sure that there are never more than MAX_GROUP_NET_CAR_DATAGRAMS datagranms at compile time
    _Static_assert(DG_GroupNet_MRB__VIPMasterDispatcher <= MAX_GROUP_NET_CAR_DATAGRAMS, "Unsupported number of car datagrams" );
    // Get index of the first car datagram, add the current car offset to this, then add the
    // local datagram ID to this.
    return ( GROUP_NET_DATAGRAM_CAR0_DATA0 + (ucCarIndex * MAX_GROUP_NET_CAR_DATAGRAMS)  + ucDatagramID);
}

GroupNet_DatagramID GroupNet_LocalRiserIDToGlobalID (uint8_t ucRiserIndex, en_group_net_ris_datagrams ucDatagramID)
{
    // Make sure that there are never more than MAX_GROUP_NET_CAR_DATAGRAMS datagranms at compile time
    _Static_assert(NUM_GroupNet_RIS_DATAGRAMS <= MAX_GROUP_NET_RISER_DATAGRAMS, "Unsupported number of riser datagrams" );
    // Get index of the first riser datagram, add the current car offset to this, then add the
    // local datagram ID to this.
    return ( GROUP_NET_DATAGRAM_RISER0_DATA0 + (ucRiserIndex * MAX_GROUP_NET_RISER_DATAGRAMS)  + ucDatagramID);
}

GroupNet_DatagramID GroupNet_LocalShieldIDToGlobalID(en_group_net_shield_datagrams ucDatagramID)
{
    // Make sure that there are never more than MAX_GROUP_NET_CAR_DATAGRAMS datagranms at compile time
    _Static_assert( (NUM_GroupNet_SHIELD_DATAGRAMS) <= MAX_GROUP_NET_SHIELD_DATAGRAMS, "Unsupported number of shield datagrams" );
    // Get the index of the first shield datagram, the add the local datagram offset to this
    return ( GROUP_NET_DATAGRAM_SHIELD_DATA0 + ucDatagramID);
}
GroupNet_DatagramID GroupNet_LocalXRegIDToGlobalID(en_group_net_xreg_datagrams ucDatagramID)
{
    // Make sure that there are never more than MAX_GROUP_NET_CAR_DATAGRAMS datagranms at compile time
    _Static_assert( (NUM_GroupNet_XREG_DATAGRAMS) <= MAX_GROUP_NET_XREG_DATAGRAMS, "Unsupported number of xreg datagrams" );
    // Get the index of the first shield datagram, the add the local datagram offset to this
    return ( GROUP_NET_DATAGRAM_XREG_DATA0 + ucDatagramID);
}
GroupNet_DatagramID GroupNet_LocalDDMIDToGlobalID(en_group_net_ddm_datagrams ucDatagramID)
{
    // Make sure that there are never more than MAX_GROUP_NET_CAR_DATAGRAMS datagranms at compile time
    _Static_assert( (NUM_GroupNet_DDM_DATAGRAMS) <= MAX_GROUP_NET_DDM_DATAGRAMS, "Unsupported number of ddm datagrams" );
    // Get the index of the first shield datagram, the add the local datagram offset to this
    return ( GROUP_NET_DATAGRAM_DDM_DATA0 + ucDatagramID);
}
/*----------------------------------------------------------------------------
   Convert global ID to a local ID
 *----------------------------------------------------------------------------*/
uint8_t GroupNet_GlobalIDToLocalNodeID( GroupNet_DatagramID eGroupDatagramID )
{
   uint8_t ucLocalDatagramID = MAX_GROUP_NET_DATAGRAMS;
   if( eGroupDatagramID < GROUP_NET_DATAGRAM_RISER0_DATA0 ) //Car
   {
      ucLocalDatagramID = eGroupDatagramID % MAX_GROUP_NET_CAR_DATAGRAMS;
   }
   else if( eGroupDatagramID < GROUP_NET_DATAGRAM_SHIELD_DATA0 )//Riser
   {
      ucLocalDatagramID = (eGroupDatagramID-GROUP_NET_DATAGRAM_RISER0_DATA0) % MAX_GROUP_NET_RISER_DATAGRAMS;
   }
   else if( eGroupDatagramID < GROUP_NET_DATAGRAM_XREG_DATA0 ) // Shield
   {
      ucLocalDatagramID = (eGroupDatagramID-GROUP_NET_DATAGRAM_SHIELD_DATA0) % MAX_GROUP_NET_SHIELD_DATAGRAMS;
   }
   else if( eGroupDatagramID < GROUP_NET_DATAGRAM_DDM_DATA0 ) // Xreg
   {
      ucLocalDatagramID = (eGroupDatagramID-GROUP_NET_DATAGRAM_XREG_DATA0) % MAX_GROUP_NET_XREG_DATAGRAMS;
   }
   else if( eGroupDatagramID < GROUP_NET_NUM_DATAGRAMS ) // DDM
   {
      ucLocalDatagramID = (eGroupDatagramID-GROUP_NET_DATAGRAM_DDM_DATA0) % MAX_GROUP_NET_DDM_DATAGRAMS;
   }
   return ucLocalDatagramID;
}
/*----------------------------------------------------------------------------
   Returns the destination bitmap of the argument node
 *----------------------------------------------------------------------------*/
uint16_t GroupNet_GetDestinationBitmap( enum en_group_net_nodes eNode )
{
   uint16_t uwBitmap = GROUP_DEST__NONE;
   switch( eNode )
   {
      case GROUP_NET__CAR_1:
         uwBitmap = GROUP_DEST__CAR1;
         break;
      case GROUP_NET__CAR_2:
         uwBitmap = GROUP_DEST__CAR2;
         break;
      case GROUP_NET__CAR_3:
         uwBitmap = GROUP_DEST__CAR3;
         break;
      case GROUP_NET__CAR_4:
         uwBitmap = GROUP_DEST__CAR4;
         break;
      case GROUP_NET__CAR_5:
         uwBitmap = GROUP_DEST__CAR5;
         break;
      case GROUP_NET__CAR_6:
         uwBitmap = GROUP_DEST__CAR6;
         break;
      case GROUP_NET__CAR_7:
         uwBitmap = GROUP_DEST__CAR7;
         break;
      case GROUP_NET__CAR_8:
         uwBitmap = GROUP_DEST__CAR8;
         break;
      case GROUP_NET__RIS_1:
         uwBitmap = GROUP_DEST__RIS1;
         break;
      case GROUP_NET__RIS_2:
         uwBitmap = GROUP_DEST__RIS2;
         break;
      case GROUP_NET__RIS_3:
         uwBitmap = GROUP_DEST__RIS3;
         break;
      case GROUP_NET__RIS_4:
         uwBitmap = GROUP_DEST__RIS4;
         break;
      case GROUP_NET__SHIELD:
         uwBitmap = GROUP_DEST__SHIELD;
         break;
      case GROUP_NET__XREG:
         uwBitmap = GROUP_DEST__XREG;
         break;
      case GROUP_NET__DDM:
         uwBitmap = GROUP_DEST__DDM;
         break;
      default:
         break;
   }
   return uwBitmap;
}
