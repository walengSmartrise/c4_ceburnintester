/******************************************************************************
 *
 * @file     mod_ExpansionManager.c
 * @brief
 * @version  V1.00
 * @date     13, June 2018
 * @author   Keith Soneda
 *
 * @note     Updated expansion board online flags and trips offline alarms
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"

#include <stdint.h>
#include "sys.h"
#include "kioskData.h"
#include "ddCommand.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_module gstMod_ExpansionManager =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckForExpansionOfflineAlarm(void)
{
   static uint8_t aucBF_LastOnlineFlag[NUM_EXP_MASTER_BOARDS];

   for(uint8_t ucMasterIndex = 0; ucMasterIndex < NUM_EXP_MASTER_BOARDS; ucMasterIndex++)
   {
      for(uint8_t ucBoardIndex = 0; ucBoardIndex < NUM_EXP_BOARDS_PER_MASTER; ucBoardIndex++)
      {
         uint8_t bNewFlag = ExpData_GetOnlineFlag(ucMasterIndex, ucBoardIndex);
         if( !bNewFlag && Sys_Bit_Get(&aucBF_LastOnlineFlag[ucMasterIndex], ucBoardIndex) )
         {
            uint8_t ucExpansionIndex = ucMasterIndex*NUM_EXP_BOARDS_PER_MASTER + ucBoardIndex;
            SetAlarm(DDM_ALM__EXP_01+ucExpansionIndex);
         }
         Sys_Bit_Set(&aucBF_LastOnlineFlag[ucMasterIndex], ucBoardIndex, bNewFlag);
      }
   }
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 300;
   pstThisModule->uwRunPeriod_1ms = 250;

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   ExpData_UpdateMasterOfflineTimers(pstThisModule->uwRunPeriod_1ms);

   CheckForExpansionOfflineAlarm();
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

