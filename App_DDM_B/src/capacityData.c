/******************************************************************************
 *
 * @file     capacityData.c
 * @brief
 * @version  V1.00
 * @date     7, November 2018
 * @author   Keith Soneda
 *
 * @note     Functions for assessing car's current passenger loads,
 *           used for dispatching
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>
#include <math.h>
#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "ddCommand.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint16_t auwCurrentCarLoads_lb[MAX_GROUP_CARS];
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
Returns 1 if the car can accept another call
Must be called after CapacityData_UpdateCurrentLoad
 *----------------------------------------------------------------------------*/
uint8_t CapacityData_CheckIfUnderCapacity( enum en_group_net_nodes eCarID )
{
   uint8_t bAvailable = 0;

   uint32_t uiLoadEstimate_lb = Param_ReadValue_8Bit(DDM_PARAM8__WeightPerCall_10lb)*10;
   if(!uiLoadEstimate_lb)
   {
      uiLoadEstimate_lb = DEFAULT_WEIGHT_PER_CALL_LB;
   }
   uiLoadEstimate_lb += CapacityData_GetCurrentLoad(eCarID);

   if( uiLoadEstimate_lb <= Param_ReadValue_16Bit(DDM_PARAM16__RatedLoad_Car1_lb+eCarID) )
   {
      bAvailable = 1;
   }
   return bAvailable;
}
/*----------------------------------------------------------------------------
Returns the estimated current load of a car based on its current car calls
as well as its assigned DD panel calls.
Must be called after CapacityData_UpdateCurrentLoad
 *----------------------------------------------------------------------------*/
uint16_t CapacityData_GetCurrentLoad( enum en_group_net_nodes eCarID )
{
   return auwCurrentCarLoads_lb[eCarID];
}
/*----------------------------------------------------------------------------
Updates the estimated current load of a car based on its current car calls
as well as its assigned DD panel calls.
 *----------------------------------------------------------------------------*/
void CapacityData_UpdateCurrentLoad( enum en_group_net_nodes eCarID )
{
   uint32_t uiLoad_LB = UNDEFINED_CAR_LOAD__LB;

   st_cardata *pstCarData = GetCarDataStructure(eCarID);
   if( ( pstCarData->ucLastLanding < MAX_NUM_FLOORS )
    && ( pstCarData->ucFirstLanding < pstCarData->ucLastLanding ) )
   {
      uint16_t uwNumCalls = 0;
      /* Check current car calls */
      for(uint8_t i = pstCarData->ucFirstLanding; i <= pstCarData->ucLastLanding; i++)
      {
         if( i < MAX_NUM_FLOORS )
         {
            /* Check car calls currently reported latched by the car */
            uint8_t ucMapIndex = i / NUM_BITS_PER_BITMAP32;
            uint8_t ucBitIndex = i % NUM_BITS_PER_BITMAP32;
            uint32_t uiMask = 1 << ucBitIndex;
            if( ( pstCarData->auiBF_CarCalls_F[ucMapIndex] & uiMask )
             || ( pstCarData->auiBF_CarCalls_R[ucMapIndex] & uiMask ) )
            {
               uwNumCalls++;
            }

            /* Check car calls not yet latched to the car, assigned via panel */
            st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
            uwNumCalls += pstAssignment->ucCallIndex;
         }
      }

      uiLoad_LB = Param_ReadValue_8Bit(DDM_PARAM8__WeightPerCall_10lb)*10;
      uiLoad_LB *= uwNumCalls;
   }

   if( uiLoad_LB > UNDEFINED_CAR_LOAD__LB )
   {
      uiLoad_LB = UNDEFINED_CAR_LOAD__LB;
   }

   auwCurrentCarLoads_lb[eCarID] = uiLoad_LB;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
