/******************************************************************************
 *
 * @file     ddcommand.c
 * @brief
 * @version  V1.00
 * @date     13, June 2018
 * @author   Keith Soneda
 *
 * @note     Manages commands used for CC/HC assignments to cars.
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>

#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "ddCommand.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define CC_REQUEST_BUFFER_SIZE        (64)
#define HC_REQUEST_BUFFER_SIZE        (4)
#define REQUEST_RESEND_COUNT          (2)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static st_ddc_cc_req astCCRequest[CC_REQUEST_BUFFER_SIZE];
static uint8_t ucCarRequestIndex;/* Marks the next request slot that will be used */

static st_ddc_hc_req astHCRequest[HC_REQUEST_BUFFER_SIZE];
static uint8_t ucHallRequestIndex;/* Marks the next request slot that will be used */
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void DDC_RequestHallCall( st_ddc_hc_req *pstHCReq )
{
   memcpy(&astHCRequest[ucHallRequestIndex], pstHCReq, sizeof(st_ddc_hc_req));
   astHCRequest[ucHallRequestIndex].ucSendCountdown = REQUEST_RESEND_COUNT;

   ucHallRequestIndex = (ucHallRequestIndex+1) % HC_REQUEST_BUFFER_SIZE;
}
void DDC_RequestCarCall( st_ddc_cc_req *pstCCReq )
{
   memcpy(&astCCRequest[ucCarRequestIndex], pstCCReq, sizeof(st_ddc_cc_req));
   astCCRequest[ucCarRequestIndex].ucSendCountdown = REQUEST_RESEND_COUNT;

   ucCarRequestIndex = (ucCarRequestIndex+1) % CC_REQUEST_BUFFER_SIZE;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t DDC_LoadDatagram_LatchHC(un_sdata_datagram *punDatagram)
{
   uint8_t bSend = 0;
   uint8_t ucIndex = 0;
   for(uint8_t i = 0; i < HC_REQUEST_BUFFER_SIZE; i++)
   {
      /* Newest will be ucHallRequestIndex-1, oldest will be ucHallRequestIndex */
      ucIndex = (ucHallRequestIndex+i) % HC_REQUEST_BUFFER_SIZE;
      if(astHCRequest[ucIndex].ucSendCountdown)
      {
         break;
      }
   }

   if(astHCRequest[ucIndex].ucSendCountdown)
   {
      astHCRequest[ucIndex].ucSendCountdown--;

      memset(punDatagram, 0, sizeof(un_sdata_datagram));

      punDatagram->auc8[0] = astHCRequest[ucIndex].ucCarID;
      punDatagram->auc8[1] = astHCRequest[ucIndex].ucLanding;
      punDatagram->auc8[2] = astHCRequest[ucIndex].eDir;
      punDatagram->auc8[3] = astHCRequest[ucIndex].bADA;
      if(astHCRequest[ucIndex].bRear)
      {
         punDatagram->aui32[1] = CarData_GetUniqueHallMask_Rear(astHCRequest[ucIndex].ucCarID);
      }
      else
      {
         punDatagram->aui32[1] = CarData_GetUniqueHallMask_Front(astHCRequest[ucIndex].ucCarID);
      }

      bSend = 1;
   }

   return bSend;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t DDC_LoadDatagram_LatchCC(un_sdata_datagram *punDatagram)
{
   uint8_t bSend = 0;
   uint8_t ucIndex = 0;
   for(uint8_t i = 0; i < CC_REQUEST_BUFFER_SIZE; i++)
   {
      /* Newest will be ucHallRequestIndex-1, oldest will be ucHallRequestIndex */
      ucIndex = (ucCarRequestIndex+i) % CC_REQUEST_BUFFER_SIZE;
      if(astCCRequest[ucIndex].ucSendCountdown)
      {
         break;
      }
   }

   if(astCCRequest[ucIndex].ucSendCountdown)
   {
      astCCRequest[ucIndex].ucSendCountdown--;

      memset(punDatagram, 0, sizeof(un_sdata_datagram));

      punDatagram->auc8[0] = astCCRequest[ucIndex].ucCarID;
      punDatagram->auc8[1] = astCCRequest[ucIndex].ucLanding;
      punDatagram->auc8[2] = astCCRequest[ucIndex].bRear;
      punDatagram->auc8[3] = astCCRequest[ucIndex].bADA;

      bSend = 1;
   }

   return bSend;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
