/******************************************************************************
 *
 * @file     kioskData.c
 * @brief
 * @version  V1.00
 * @date     13, June 2018
 * @author   Keith Soneda
 *
 * @note     Manages calls from DD panels at various floors and assigns them to cars.
 *           These panels are also referred to as kiosks throught the code.
 *           Referenced from: CE Electronics CAN Protocol for Destination Entry Devices Version 1
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>
#include <math.h>
#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "ddCommand.h"
#include "totalCostData.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define RESPONSE_BUFFER_SIZE                                 (8)

#define DEFAULT_DISABLE_CALL_ENTRY_COUNTDOWN_MS              (10000)

#define SECURITY_PASSCODE_NEEDED                             (0xFFFFFFFF)

#define NUM_PACKET_RESEND_PANEL_RESPONSE                     (1U)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   SECURITY_FLAG__ACCEPTED,
   SECURITY_FLAG__CODE_NEEDED,
   SECURITY_FLAG__CODE_INVALID,
   SECURITY_FLAG__DEVICE_NEEDED,
   SECURITY_FLAG__DEVICE_INVALID,
   NUM_SECURITY_FLAG
}en_security_flag;
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint16_t aauwOfflineTimer_ms[MAX_SUPPORTED_KIOSK_LOCATIONS][MAX_NUM_FLOORS];

static st_kiosk_assignments aastKioskAssignments[MAX_GROUP_CARS][MAX_NUM_FLOORS];

static uint8_t abSendParameterUpdate[BITMAP8_SIZE(MAX_NUM_FLOORS)];

static uint8_t ucLastTimeoutFloor;

/* Response to DED panel */
static st_kiosk_response astKioskResponse[RESPONSE_BUFFER_SIZE];
static uint8_t ucResponseIndex; /* Marks the next response slot that will be used */
static uint8_t ucActiveResponseFloor; /* Marks the floor of the response currently being transmitted */

static uint32_t uiCallRequestCounter;
static uint32_t uiCallResponseCounter;

/* From CE: 3.3:
Hall Call Response
   Message Id = 0x03
   This is the response sent from the dispatcher in response to a Hall Call Request (message id 0x02). If the call status returned is 0x00 then only bytes 1 and 2 are required to be returned other wise bytes 1 – 4 are required to be returned.
 *  */
static uint8_t bLastCallAccepted;
en_kiosk_location_id eLastLocation;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

   Access functions

 *----------------------------------------------------------------------------*/
st_kiosk_assignments * DDPanel_GetAssignmentStructure( enum en_group_net_nodes eCarID, uint8_t ucLanding )
{
   return &aastKioskAssignments[eCarID][ucLanding];
}
en_kiosk_offline_states DDPanel_GetOfflineState(uint8_t ucLanding, en_kiosk_location_id ucLocation)
{
   en_kiosk_offline_states eState = KIOSK_OFFLINE_STATE__INACTIVE;

   if( ( ucLanding < MAX_NUM_FLOORS )
    && ( ucLocation < MAX_SUPPORTED_KIOSK_LOCATIONS ) )
   {
      if( aauwOfflineTimer_ms[ucLocation][ucLanding] < KIOSK_OFFLINE_TIMER_MS )
      {
         eState = KIOSK_OFFLINE_STATE__ONLINE;
      }
      else if( aauwOfflineTimer_ms[ucLocation][ucLanding] < KIOSK_INACTIVE_TIMER_MS )
      {
         eState = KIOSK_OFFLINE_STATE__OFFLINE;
      }
   }
   return eState;
}
/* Returns 1 if the panel calls to a rear opening, 0 if the panel calls to a front opening */
uint8_t DDPanel_GetOpening(uint8_t ucLanding, en_kiosk_location_id ucLocation)
{
   uint8_t ucBitMask = Param_ReadValue_8Bit(DDM_PARAM8__PanelOpeningMap01+ucLanding);
   uint8_t bRear = Sys_Bit_Get(&ucBitMask, ucLocation);
   return bRear;
}

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void DDPanel_InitOfflineTimers()
{
   for(uint8_t i = 0; i < MAX_SUPPORTED_KIOSK_LOCATIONS; i++)
   {
      for(uint8_t j = 0; j < MAX_NUM_FLOORS; j++)
      {
         aauwOfflineTimer_ms[i][j] = KIOSK_INACTIVE_TIMER_MS;
      }
   }
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void DDPanel_ClearAssignments( enum en_group_net_nodes eCarID, uint8_t ucLanding )
{
   if( ucLanding >= MAX_NUM_FLOORS )
   {
      for(uint8_t i = 0; i < MAX_NUM_FLOORS; i++)
      {
         aastKioskAssignments[eCarID][i].ucCallIndex = 0;
      }
   }
   else
   {
      aastKioskAssignments[eCarID][ucLanding].ucCallIndex = 0;
   }
}
/*----------------------------------------------------------------------------
   Get floor of the panel that made the last timeouts request
   Unused since panels seem to respond to messages sent to all floors
 *----------------------------------------------------------------------------*/
uint8_t DDPanel_GetLastTimeoutFloor()
{
   return ucLastTimeoutFloor;
}
/*----------------------------------------------------------------------------
   Get the destination floor of current hall call response packet
 *----------------------------------------------------------------------------*/
uint8_t DDPanel_GetActiveResponseFloor()
{
   return ucActiveResponseFloor;
}
/*----------------------------------------------------------------------------
   Returns 1 if the call response packet waiting to be sent was accepted or rejected.
 *----------------------------------------------------------------------------*/
uint8_t DDPanel_GetLastCallAccepted(void)
{
   return bLastCallAccepted;
}
/*----------------------------------------------------------------------------
   Returns the location of the panel the current call response packet should be addresssed to.
 *----------------------------------------------------------------------------*/
uint8_t DDPanel_GetLastLocation(void)
{
   return eLastLocation;
}

/*----------------------------------------------------------------------------
   Get the mode to display on the destination entry panel
 *----------------------------------------------------------------------------*/
en_kiosk_op_mode DDPanel_GetOperationMode()
{
   en_kiosk_op_mode eMode = KIOSK_OP_MODE__DEST_DIS;
   uint8_t bOOS = 1;
   uint8_t bFire = 0;
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      if( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
      {
         st_cardata * pstCarData = GetCarDataStructure(i);
         if( ( pstCarData->eAutoMode == MODE_A__FIRE1 )
          || ( pstCarData->eAutoMode == MODE_A__FIRE2 )
          || ( GetOutputValue_ByCar(i, enOUT_LMP_FIRE) )
          || ( GetOutputValue_ByCar(i, enOUT_FIRE_LOBBY_LAMP) ) )
         {
            bFire = 1;
            break;
         }
         else if( CarData_GetHallMask(i, DOOR_ANY) )
         {
            bOOS = 0;
         }
      }
   }

   if( bFire )
   {
      eMode = KIOSK_OP_MODE__FIRE;
   }
   else if( bOOS )
   {
      eMode = KIOSK_OP_MODE__CAR_OOS;
   }

   return eMode;
}
/*----------------------------------------------------------------------------
   Returns 1 when theres a new datagram to send.

   Parameter sent to the DED for configuration of timeouts associated with screens and key press entries.

Byte  Bits  Description
1     Normal Screen Timeout (1/4 second Interval)
2     Assisted Screen Timeout (1/4 second Interval)
3     Key Press Entry Timeout (1/4 second Interval)
4     Key Press Assisted Timeout (1/4 second Interval)
5     Code Entry Timeout (1/4 second interval)
6     Assisted Code Entry Timeout (1/4 second interval)
7     Dispatcher Response Timeout (1/4 second interval)
8     Reserved

 *----------------------------------------------------------------------------*/
#define MIN_TIMEOUT_RESEND_RATE_5MS          (4000)
uint8_t DDPanel_LoadDatagram_Timeouts( un_sdata_datagram *punDatagram )
{
   uint8_t bReturn = 0;

   /* Ensure a minimum update rate */
   static uint16_t uwUpdateTimer_5ms;
   uint8_t bNeedToSend = 0;
   for(uint8_t i = 0; i < MAX_NUM_FLOORS; i++)
   {
      if(Sys_Bit_Get(&abSendParameterUpdate[0], i))
      {
         bNeedToSend = 1;
         break;
      }
   }

   if( bNeedToSend )
   {
      uwUpdateTimer_5ms = 0;
   }
   else if( ++uwUpdateTimer_5ms >= MIN_TIMEOUT_RESEND_RATE_5MS )
   {
      Sys_Bit_Set(&abSendParameterUpdate[0], 0, 1);
   }

   for(uint8_t i = 0; i < MAX_NUM_FLOORS; i++)
   {
      if(Sys_Bit_Get(&abSendParameterUpdate[0], i))
      {
         uint8_t ucNormalScreenTimeout_250ms = ( Param_ReadValue_8Bit(DDM_PARAM8__ScreenTO_100ms) )
                                             ? ( Param_ReadValue_8Bit(DDM_PARAM8__ScreenTO_100ms) / 2.5 )
                                             : ( DEFAULT_TIMEOUT_NORMAL_SCREEN_250MS );
         uint8_t ucAssistedScreenTO_250ms = ( Param_ReadValue_8Bit(DDM_PARAM8__ScreenAssistTO_100ms) )
                                          ? ( Param_ReadValue_8Bit(DDM_PARAM8__ScreenAssistTO_100ms) / 2.5 )
                                          : ( DEFAULT_TIMEOUT_ASSITED_SCREEN_250MS );
         uint8_t ucKeyPressTO_250ms = ( Param_ReadValue_8Bit(DDM_PARAM8__KeyTO_100ms) )
                                    ? ( Param_ReadValue_8Bit(DDM_PARAM8__KeyTO_100ms) / 2.5 )
                                    : ( DEFAULT_TIMEOUT_KEY_PRESS_250MS );
         uint8_t ucKeyPressAssistedTO_250ms = ( Param_ReadValue_8Bit(DDM_PARAM8__KeyAssistTO_100ms) )
                                            ? ( Param_ReadValue_8Bit(DDM_PARAM8__KeyAssistTO_100ms) / 2.5 )
                                            : ( DEFAULT_TIMEOUT_KEY_PRESS_ASSISTED_250MS );
         uint8_t ucCodeEntryTO_250ms = ( Param_ReadValue_8Bit(DDM_PARAM8__CodeTO_100ms) )
                                     ? ( Param_ReadValue_8Bit(DDM_PARAM8__CodeTO_100ms) / 2.5 )
                                     : ( DEFAULT_TIMEOUT_CODE_ENTRY_250MS );
         uint8_t ucAssistedCodeEntryTO_250ms = ( Param_ReadValue_8Bit(DDM_PARAM8__CodeAssistTO_100ms) )
                                             ? ( Param_ReadValue_8Bit(DDM_PARAM8__CodeAssistTO_100ms) / 2.5 )
                                             : ( DEFAULT_TIMEOUT_CODE_ENTRY_ASSISTED_250MS );
         uint8_t ucDispatcherResponseTO_250ms = ( Param_ReadValue_8Bit(DDM_PARAM8__DispRespTO_100ms) )
                                              ? ( Param_ReadValue_8Bit(DDM_PARAM8__DispRespTO_100ms) / 2.5 )
                                              : ( DEFAULT_TIMEOUT_DISPATCHER_RESPONSE_250MS );
         punDatagram->auc8[0] = ucNormalScreenTimeout_250ms;
         punDatagram->auc8[1] = ucAssistedScreenTO_250ms;
         punDatagram->auc8[2] = ucKeyPressTO_250ms;
         punDatagram->auc8[3] = ucKeyPressAssistedTO_250ms;
         punDatagram->auc8[4] = ucCodeEntryTO_250ms;
         punDatagram->auc8[5] = ucAssistedCodeEntryTO_250ms;
         punDatagram->auc8[6] = ucDispatcherResponseTO_250ms;
         punDatagram->auc8[7] = 0;
         uwUpdateTimer_5ms = 0;
         Sys_Bit_Set(&abSendParameterUpdate[0], i, 0);
         ucLastTimeoutFloor = i+1; // Record the panel floor for packet addressing
         bReturn = 1;
         break;
      }
   }

   return bReturn;
}

/*-----------------------------------------------------------------------------
   Load dispatch panel response

   Returns 1 when theres a new datagram to send.

   DG_GroupNet_MRB__DispatchPanel
      unDatagram.auc8[0] = stKioskResponse.ucLanding_Panel_Plus1;
      unDatagram.auc8[1] = stKioskResponse.ucLanding_Dest_Plus1;
      unDatagram.auc8[2] = stKioskResponse.bRear;
      unDatagram.auc8[3] = stKioskResponse.eType;
      unDatagram.auc8[4] = stKioskResponse.ucCarID;

Byte  Bits     Description
1              Status of Call Request
               0x05 = COP Call Accepted (Call Accepted)

2              Car Id Assigned to Passenger (0x00 – 0xFE)

3              Floor Requested

4     0        Exit Door from Car
               0 = Front Door Destination
               1 = Rear Door Destination
4     1 – 7    Call Type Requested
               0 = Normal Call
5 – 8    Reserved
 -----------------------------------------------------------------------------*/
uint8_t DDPanel_LoadDatagram_CallResponse( un_sdata_datagram *punDatagram )
{
   uint8_t bReturn = 0;

   uint8_t ucIndex = 0;
   for(uint8_t i = 0; i < RESPONSE_BUFFER_SIZE; i++)
   {
      /* Newest will be ucResponseIndex-1, oldest will be ucResponseIndex */
      ucIndex = (ucResponseIndex+i) % RESPONSE_BUFFER_SIZE;
      if(astKioskResponse[ucIndex].bDirty)
      {
         break;
      }
   }
   if(astKioskResponse[ucIndex].bDirty)
   {
      astKioskResponse[ucIndex].bDirty--;
      if(astKioskResponse[ucIndex].ucLanding_Dest_Plus1 && astKioskResponse[ucIndex].ucLanding_Panel_Plus1)
      {
         memset(punDatagram, 0, sizeof(un_sdata_datagram));

         punDatagram->auc8[0] = astKioskResponse[ucIndex].eCallResponse;
         punDatagram->auc8[1] = astKioskResponse[ucIndex].ucCarID;
         punDatagram->auc8[2] = astKioskResponse[ucIndex].ucLanding_Dest_Plus1-1;
#if 0 // Fixed in dupar panel v750
         if( Param_ReadValue_1Bit(DDM_PARAM1__EnableDuparPanel) )
         {
            // Dupar panel floors start with 1 while CE floors start with 0
            punDatagram->auc8[2] += 1;
         }
#endif
         punDatagram->auc8[3] = (astKioskResponse[ucIndex].bRear & 0x01)
                              | (astKioskResponse[ucIndex].eType << 1);
         uiCallResponseCounter++;//todo rm
         /* Mark mark the index for response */
         ucActiveResponseFloor = astKioskResponse[ucIndex].ucLanding_Panel_Plus1-1;
#if 0 // Fixed in dupar panel v750
         if( Param_ReadValue_1Bit(DDM_PARAM1__EnableDuparPanel) )
         {
            // Dupar panel floors start with 1 while CE floors start with 0
            ucActiveResponseFloor += 1;
         }
#endif
         bLastCallAccepted = ( astKioskResponse[ucIndex].eCallResponse == KIOSK_CALL_RESP__ACCEPTED ) ? 1:0;
         eLastLocation = astKioskResponse[ucIndex].ucLocation;
         bReturn = 1;
      }
   }
   return bReturn;
}
/*-----------------------------------------------------------------------------
   Check if message is from Kiosk and unload
 -----------------------------------------------------------------------------*/
uint8_t DDPanel_ReceiveMessage( CAN_MSG_T *msg )
{
   uint8_t bValid = 0;
   uint8_t ucDeviceID = EXTRACT_DEVICE_ID(msg->ID);
   uint8_t ucMsgID = EXTRACT_MSG_ID(msg->ID);
   if( ucDeviceID == KIOSK_DEVICE_ID__SCREEN )
   {
      /* Hall Call Request */
      if( ucMsgID == KIOSK_MSG_ID__HALL_REQ )
      {
         DDPanel_UnloadCallRequest(msg);
         bValid = 1;
      }
      /* Watchdog / Car Operation message */
      else if( ucMsgID == KIOSK_MSG_ID__CAR_OP )
      {
         DDPanel_UnloadWatchdogMessage(msg);
         bValid = 1;
      }
   }
   return bValid;
}

/*-----------------------------------------------------------------------------
   Returns index of car to assign call
 -----------------------------------------------------------------------------*/
static uint8_t GetCarAssignment( st_kiosk_request *pstReq )
{
   uint8_t ucLandingsAway = 255;
   uint8_t ucCar = INVALID_CAR_INDEX; // invalid car

   uint8_t ucPanelLanding = pstReq->ucLanding_Panel_Plus1-1;
   uint8_t ucDestLanding = pstReq->ucLanding_Dest_Plus1-1;
   uint8_t bRear = pstReq->bRear;

   if( ( Param_ReadValue_8Bit(DDM_PARAM8__DispatchingType) >= DDM_DISPATCH_TYPE__TOTAL_COST )
    && ( Param_ReadValue_8Bit(DDM_PARAM8__DispatchingType) <= DDM_DISPATCH_TYPE__TIME_TO_PICKUP ) )
   {
      uint32_t uiMinimumCost_sec = INVALID_TOTAL_ASSIGNMENT_COST_SEC;
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         TotalCost_ResetAssignmentCost(i);
         /* If car is online, in valid mode, is below capacity,
          * is serving the requested floor, and has the lowest
          * total cost to passengers (in time) */
         st_cardata *pstCarData = GetCarDataStructure(i);
         if( ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( pstCarData->uiHallMask_F || pstCarData->uiHallMask_R ) // In group TODO revise
          && ( pstCarData->eClassOp == CLASSOP__AUTO )
          && ( pstCarData->eAutoMode == MODE_A__NORMAL )
          && ( CarData_CheckOpening(i, ucDestLanding, bRear) )
          && ( CarData_CheckOpening(i, ucPanelLanding, bRear) )
          && ( ( !aastKioskAssignments[i][ucPanelLanding].uwCountdown_ms )
            || ( ( Param_ReadValue_1Bit(DDM_PARAM1__InProximityEntry) )
              && ( CarData_GetDoorOpen_Front(i) || CarData_GetDoorOpen_Rear(i) ) ) )
          && ( CapacityData_CheckIfUnderCapacity(i) )
          )
         {
            uint32_t uiTotalCost_sec = TotalCost_GetAssignmentCost_sec(i, ucDestLanding, ucPanelLanding);

            if( uiMinimumCost_sec > uiTotalCost_sec )
            {
               uiMinimumCost_sec = uiTotalCost_sec;
               ucCar = i;
            }
         }
      }
   }
   else
   {
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         /* If car is online, in valid mode, is below capacity,
          * is serving the requested floor, and is closest */
         st_cardata *pstCarData = GetCarDataStructure(i);
         if( ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( pstCarData->uiHallMask_F || pstCarData->uiHallMask_R ) // In group
          && ( pstCarData->eClassOp == CLASSOP__AUTO )
          && ( pstCarData->eAutoMode == MODE_A__NORMAL )
          && ( CarData_CheckOpening(i, ucDestLanding, bRear) )
          && ( ( !aastKioskAssignments[i][ucPanelLanding].uwCountdown_ms )
            || ( ( Param_ReadValue_1Bit(DDM_PARAM1__InProximityEntry) )
              && ( CarData_GetDoorOpen_Front(i) || CarData_GetDoorOpen_Rear(i) ) ) )
          && ( CapacityData_CheckIfUnderCapacity(i) )
          )
         {
            /* Determine the closest available car */
            st_cardata *pstCarData = GetCarDataStructure(i);
            uint8_t ucLandingDiff = 255;
            if( ( pstCarData->cMotionDir >= DIR__NONE )
             && ( pstCarData->ucReachableLanding <= ucPanelLanding ) )
            {
               ucLandingDiff = pstCarData->ucCurrentLanding - ucPanelLanding;
            }
            else if( ( pstCarData->cMotionDir <= DIR__NONE )
                  && ( pstCarData->ucReachableLanding >= ucPanelLanding ) )
            {
               ucLandingDiff = ucPanelLanding - pstCarData->ucCurrentLanding;
            }

            if( ucLandingDiff < ucLandingsAway )
            {
               ucLandingsAway = ucLandingDiff;
               ucCar = i;
            }
         }
      }
   }

   return ucCar;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void LoadCallAssignment( st_kiosk_request *pstReq, uint8_t ucCarID )
{
   uint8_t ucPanelLanding = pstReq->ucLanding_Panel_Plus1-1;

   uint8_t ucCallIndex = aastKioskAssignments[ucCarID][ucPanelLanding].ucCallIndex;

   aastKioskAssignments[ucCarID][ucPanelLanding].aucLanding_Dest_Plus1[ucCallIndex] = pstReq->ucLanding_Dest_Plus1;
   aastKioskAssignments[ucCarID][ucPanelLanding].aeType[ucCallIndex] = pstReq->eType;
   Sys_Bit_Set(&aastKioskAssignments[ucCarID][ucPanelLanding].abRear[0], ucCallIndex, pstReq->bRear);

   aastKioskAssignments[ucCarID][ucPanelLanding].ucCallIndex = ucCallIndex+1;

   /* Load Hall Call Request */
   st_ddc_hc_req stHC_Req;
   stHC_Req.ucCarID = ucCarID;
   stHC_Req.ucLanding = ucPanelLanding;
   stHC_Req.bRear = DDPanel_GetOpening(ucPanelLanding, pstReq->ucLocation);
   stHC_Req.bADA = pstReq->eType;
   stHC_Req.eDir = (pstReq->ucLanding_Dest_Plus1 > pstReq->ucLanding_Panel_Plus1) ? HC_DIR__UP:HC_DIR__DOWN;
   DDC_RequestHallCall(&stHC_Req);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static en_security_flag CheckFloorAccess(st_kiosk_request *pstReq)
{
   en_security_flag eAccess = SECURITY_FLAG__ACCEPTED;
   uint8_t ucLanding = pstReq->ucLanding_Dest_Plus1-1;

   if( Param_ReadValue_1Bit(DDM_PARAM1__EnableSecurityCode) )
   {
      enum en_ddm_24bit_params eParam = (pstReq->bRear) ? DDM_PARAM24__SecurityCodeR_0:DDM_PARAM24__SecurityCodeF_0;
      eParam += ucLanding;
      uint32_t uiAccessPasscode = Param_ReadValue_24Bit(eParam);
      if( pstReq->uiPasscode == SECURITY_PASSCODE_NEEDED )
      {
         eAccess = SECURITY_FLAG__CODE_NEEDED;
      }
      else if( pstReq->uiPasscode != uiAccessPasscode )
      {
         eAccess = SECURITY_FLAG__CODE_INVALID;
      }
   }
   else if( Param_ReadValue_1Bit(DDM_PARAM1__EnableSecurityDevice) )
   {
      if( !GetSecurityInput( ucLanding, pstReq->bRear ) )
      {
         eAccess = SECURITY_FLAG__DEVICE_NEEDED;
      }
   }
   return eAccess;
}
/*----------------------------------------------------------------------------
 From CAN Destination Protocol for destination devices version 1
 3.2 Hall Call Request

Assumes Security Code 1 is the left most digit
Note document says:
   Byte 3, Bits 3-0 = Security Code 1
   Byte 3, Bits 7-4 = Security Code 2
   Byte 4, Bits 3-0 = Security Code 3
   Byte 4, Bits 7-4 = Security Code 4
But is received as follows:
   Byte 3, Bits 3-0 = Security Code 2
   Byte 3, Bits 7-4 = Security Code 1
   Byte 4, Bits 3-0 = Security Code 4
   Byte 4, Bits 7-4 = Security Code 3
 *----------------------------------------------------------------------------*/
static uint32_t ConvertSecurityCodeToDecimalValue( CAN_MSG_T *msg )
{
   uint32_t uiPasscode = SECURITY_PASSCODE_NEEDED;
   uint8_t aucDigit[4];
   if( Param_ReadValue_1Bit(DDM_PARAM1__EnableDuparPanel) )
   {
      aucDigit[3] = msg->Data[2] & 0xF;
      aucDigit[2] = (msg->Data[2] >> 4) & 0xF;
      aucDigit[1] = msg->Data[3] & 0xF;
      aucDigit[0] = (msg->Data[3] >> 4) & 0xF;
   }
   else // CE Panel
   {
      aucDigit[2] = msg->Data[2] & 0xF;
      aucDigit[3] = (msg->Data[2] >> 4) & 0xF;
      aucDigit[0] = msg->Data[3] & 0xF;
      aucDigit[1] = (msg->Data[3] >> 4) & 0xF;
   }

   for(uint8_t i = 0; i < 4; i++)
   {
      if(aucDigit[i] < 10)
      {
         if(!i)
         {
            uiPasscode = 0;
         }
         uiPasscode += aucDigit[i] * powf(10, i);
      }
   }

   return uiPasscode;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void DDPanel_UnloadCallRequest( CAN_MSG_T *msg )
{
   st_kiosk_request stReq;
   stReq.ucLanding_Panel_Plus1 = EXTRACT_FLOOR_ID(msg->ID)+1;
#if 0 // Fixed in dupar panel v750
   if( Param_ReadValue_1Bit(DDM_PARAM1__EnableDuparPanel) )
   {
      // Dupar panel floors start with 1 while CE floors start with 0
      stReq.ucLanding_Panel_Plus1 -= 1;
   }
#endif
   stReq.ucLocation = EXTRACT_LOCATION_ID(msg->ID);
   stReq.ucLanding_Dest_Plus1 = msg->Data[0]+1;
#if 0 // Fixed in dupar panel v750
   if( Param_ReadValue_1Bit(DDM_PARAM1__EnableDuparPanel) )
   {
      // Dupar panel floors start with 1 while CE floors start with 0
      stReq.ucLanding_Dest_Plus1 -= 1;
   }
#endif
   stReq.bRear = msg->Data[1] & 0x01;
   stReq.eType = msg->Data[1] >> 1;
   stReq.uiPasscode = ConvertSecurityCodeToDecimalValue(msg);
   uiCallRequestCounter++;//todo rm
   uint8_t ucCarAssignment = INVALID_CAR_INDEX;

   astKioskResponse[ucResponseIndex].eCallResponse = KIOSK_CALL_RESP__NO_CARS;
   if( ( stReq.ucLanding_Dest_Plus1 )
    && ( stReq.ucLanding_Panel_Plus1 )
    && ( stReq.ucLanding_Dest_Plus1 <= MAX_NUM_FLOORS )
    && ( stReq.ucLanding_Panel_Plus1 <= MAX_NUM_FLOORS ) )
   {
      /* Determine which car to assign */
      en_security_flag eAccess = CheckFloorAccess(&stReq);
      if(eAccess == SECURITY_FLAG__CODE_NEEDED)
      {
         astKioskResponse[ucResponseIndex].eCallResponse = KIOSK_CALL_RESP__NEED_CODE;
      }
      else if(eAccess == SECURITY_FLAG__CODE_INVALID)
      {
         astKioskResponse[ucResponseIndex].eCallResponse = KIOSK_CALL_RESP__INVALID_CODE;
      }
      else if(eAccess == SECURITY_FLAG__DEVICE_NEEDED)
      {
         astKioskResponse[ucResponseIndex].eCallResponse = KIOSK_CALL_RESP__NEED_SECURITY;
      }
      else if(eAccess == SECURITY_FLAG__DEVICE_INVALID)
      {
         astKioskResponse[ucResponseIndex].eCallResponse = KIOSK_CALL_RESP__INVALID_SECURITY;
      }
      else
      {
         ucCarAssignment = GetCarAssignment( &stReq );
         if( ucCarAssignment < MAX_GROUP_CARS)
         {
            astKioskResponse[ucResponseIndex].eCallResponse =  KIOSK_CALL_RESP__ACCEPTED;
            LoadCallAssignment( &stReq, ucCarAssignment );
         }
      }
   }

   astKioskResponse[ucResponseIndex].bDirty = NUM_PACKET_RESEND_PANEL_RESPONSE;
   astKioskResponse[ucResponseIndex].bRear = stReq.bRear;
   astKioskResponse[ucResponseIndex].ucCarID = ucCarAssignment;
   astKioskResponse[ucResponseIndex].ucLanding_Dest_Plus1 = stReq.ucLanding_Dest_Plus1;
   astKioskResponse[ucResponseIndex].ucLanding_Panel_Plus1 = stReq.ucLanding_Panel_Plus1;
   astKioskResponse[ucResponseIndex].ucLocation = stReq.ucLocation;
   astKioskResponse[ucResponseIndex].eType = stReq.eType;

   ucResponseIndex = (ucResponseIndex+1) % RESPONSE_BUFFER_SIZE;
}
/*-----------------------------------------------------------------------------
3.1.1 Watchdog Commands
0x00  DED transmits this at regular intervals to the dispatcher to indicate it is alive.
0x01  DED transmits this to the dispatcher when the DED powers up to indicate it needs its configuration parameters.  If the DED fails to receive the Watchdog Command 0x03 the DED will display the “No Communications” message.
0x02  Dispatcher transmits this at regular intervals to the DED’s to indicate it is alive and functioning.  If the DED does not receive this message the “No Communications” message is displayed on the DED’s screen.
0x03  Dispatcher transmits this when it has sent all the configuration information to the requesting DED.
0x04  When this watchdog command is seen on the network all normal call operation will be diverted to the security manager and it will make the hall call request instead of the DED devices.  All DED hall call requests will be interpreted by the security manager, validate the call security and release the hall call to the appropriate hall call close contact devices on the network when the security is validated.
0x05  This is transmitted to request that the DEDs go through the logon sequence again. This will be sent when the design controller goes through a power cycle, the car select DIP switches are changed, or the EEPROM is reprogrammed through the CAN link.
0x06  The DED will send this command to request that all data in the EEPROM be sent down the CAN link. This will be used to read the current data on the EEPROM so that it can be modified or transferred.

 -----------------------------------------------------------------------------*/
void DDPanel_UnloadWatchdogMessage( CAN_MSG_T *msg )
{
   uint8_t ucLanding_Panel = EXTRACT_FLOOR_ID(msg->ID);
#if 0 // Fixed in dupar panel v750
   if( Param_ReadValue_1Bit(DDM_PARAM1__EnableDuparPanel) )
   {
      // Dupar panel floors start with 1 while CE floors start with 0
      ucLanding_Panel -= 1;
   }
#endif
   en_kiosk_location_id ucLocation = EXTRACT_LOCATION_ID(msg->ID);
   en_kiosk_status_cmd eCommand = msg->Data[0];
   uint8_t bValid = 0;
   static uint8_t ucTest;
   static CAN_MSG_T stLastMsg;
   if( ucLanding_Panel < MAX_NUM_FLOORS )
   {
      if( eCommand == KIOSK_STATUS_CMD__DED_WD )
      {
         bValid = 1;
      }
      else if( eCommand == KIOSK_STATUS_CMD__DED_REQ )
      {
         /* A Panel is requesting parameter update. Mark the floor for update. */
         Sys_Bit_Set(&abSendParameterUpdate[0], ucLanding_Panel, 1);
         bValid = 1;
      }

      if( ( bValid )
       && ( ucLocation < MAX_SUPPORTED_KIOSK_LOCATIONS ) )
      {
         aauwOfflineTimer_ms[ucLocation][ucLanding_Panel] = 0;
      }
      else
      {
         ucTest++;
         memcpy(&stLastMsg, msg, sizeof(CAN_MSG_T));
      }
   }
   else
   {
      ucTest++;
      memcpy(&stLastMsg, msg, sizeof(CAN_MSG_T));
   }
}
/*-----------------------------------------------------------------------------
   Check for panel offline
 -----------------------------------------------------------------------------*/
void DDPanel_UpdateOfflineTimers( struct st_module *pstThisModule )
{
   for(en_kiosk_location_id ucLocation = 0; ucLocation < MAX_SUPPORTED_KIOSK_LOCATIONS; ucLocation++)
   {
      for(uint8_t ucFloor = 0; ucFloor < MAX_NUM_FLOORS; ucFloor++)
      {
         if( aauwOfflineTimer_ms[ucLocation][ucFloor] < KIOSK_OFFLINE_TIMER_MS )
         {
            aauwOfflineTimer_ms[ucLocation][ucFloor] += pstThisModule->uwRunPeriod_1ms;
         }
         else if( aauwOfflineTimer_ms[ucLocation][ucFloor] != KIOSK_INACTIVE_TIMER_MS )
         {
            SetAlarm(DDM_ALM__PANEL_01+ucFloor);
         }
      }
   }
}
/*-----------------------------------------------------------------------------
   Clear out call requests for a car when:
   It leaves auto mode
   OR
   It is out of group
   OR
   It goes offline
 -----------------------------------------------------------------------------*/
void DDPanel_ClearInvalidCalls()
{
   static enum en_classop aeLastClassOp[MAX_GROUP_CARS];
   static uint8_t abLastOnlineFlag[MAX_GROUP_CARS];
   static uint32_t auiLastLatchableMask[MAX_GROUP_CARS];
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      st_cardata *pstCarData = GetCarDataStructure(i);
      uint8_t bOnline = GetCarOnlineFlag_ByCar(i);
      uint32_t uiLatchMask = pstCarData->uiLatchableHallMask_F | pstCarData->uiLatchableHallMask_R;
      if( !bOnline && abLastOnlineFlag[i] )
      {
         DDPanel_ClearAssignments(i, INVALID_FLOOR);
      }
      else if( ( pstCarData->eClassOp != aeLastClassOp[i] )
            && ( pstCarData->eClassOp != CLASSOP__AUTO ) )
      {
         DDPanel_ClearAssignments(i, INVALID_FLOOR);
      }
      else if( !uiLatchMask && auiLastLatchableMask[i] )
      {
         DDPanel_ClearAssignments(i, INVALID_FLOOR);
      }
      abLastOnlineFlag[i] = bOnline;
      aeLastClassOp[i] = pstCarData->eClassOp;
      auiLastLatchableMask[i] = uiLatchMask;
   }
}
/*-----------------------------------------------------------------------------
   Manages assigned panel calls.
   Monitors if cars have arrived at kiosk panel floor.
   Enters the assigned car calls after car arrival.
   TODO clear out call requests if there is not active hall mask for an extended period.
 -----------------------------------------------------------------------------*/
void DDPanel_UpdatePanelCalls( struct st_module *pstThisModule )
{
   st_ddc_cc_req stCCReq;
   for(uint8_t ucCar = 0; ucCar < MAX_GROUP_CARS; ucCar++)
   {
      /* Update per floor countdown timers */
      for(uint8_t ucFloor = 0; ucFloor < MAX_NUM_FLOORS; ucFloor++)
      {
         if( aastKioskAssignments[ucCar][ucFloor].uwCountdown_ms > pstThisModule->uwRunPeriod_1ms )
         {
            aastKioskAssignments[ucCar][ucFloor].uwCountdown_ms -= pstThisModule->uwRunPeriod_1ms;
         }
         else
         {
            aastKioskAssignments[ucCar][ucFloor].uwCountdown_ms = 0;
         }
      }

      /* If car is active and arriving at a floor */
      if( ( GetCarOfflineTimer_ByCar(ucCar) < GROUP_CAR_OFFLINE_TIMER_MS )
       && ( CarData_GetHallMask(ucCar, DOOR_ANY) ) )
      {
         if( CarData_GetDoorOpen_Front(ucCar) || CarData_GetDoorOpen_Rear(ucCar) )
         {
            st_cardata *pstCarData =  GetCarDataStructure(ucCar);
            uint8_t ucCurrentLanding = pstCarData->ucCurrentLanding;
            /* If car arrives answering up calls, load any assigned CCs for floors above */
            if(pstCarData->ucCurrentLanding < MAX_NUM_FLOORS)
            {
               uint8_t ucNumCalls = aastKioskAssignments[ucCar][ucCurrentLanding].ucCallIndex;
               if( ucNumCalls )
               {
                  /* Select only floors above current */
                  for(uint8_t ucIndex = 0; ucIndex < ucNumCalls; ucIndex++)
                  {
                     uint8_t ucDestLanding = aastKioskAssignments[ucCar][ucCurrentLanding].aucLanding_Dest_Plus1[ucIndex]-1;
                     stCCReq.ucCarID = ucCar;
                     stCCReq.ucLanding = ucDestLanding;
                     stCCReq.bRear = Sys_Bit_Get(&aastKioskAssignments[ucCar][ucCurrentLanding].abRear[0], ucIndex);
                     stCCReq.bADA = aastKioskAssignments[ucCar][ucCurrentLanding].aeType[ucIndex];
                     DDC_RequestCarCall(&stCCReq);
                  }

                  /* Clear mark the current floor's calls as sent */
                  aastKioskAssignments[ucCar][ucCurrentLanding].ucCallIndex = 0;

                  /* Temporarily prevent additional call entry a floor for an adjustable time initial passenger pickup */
                  uint16_t uwCountdown_ms = ( ( Param_ReadValue_8Bit(DDM_PARAM8__DisableEntryTimer_s) )
                                           && ( Param_ReadValue_8Bit(DDM_PARAM8__DisableEntryTimer_s) < 30 ) )
                                          ? ( Param_ReadValue_8Bit(DDM_PARAM8__DisableEntryTimer_s) * 1000 )
                                          : ( DEFAULT_DISABLE_CALL_ENTRY_COUNTDOWN_MS );
                  aastKioskAssignments[ucCar][ucCurrentLanding].uwCountdown_ms = uwCountdown_ms;
               }
            }
         }
      }
   }
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
