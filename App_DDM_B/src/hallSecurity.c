
/******************************************************************************
 *
 * @file     hallSecurity.c
 * @brief
 * @version  V1.00
 * @date     11, July 2019
 *
 * @note     Functions and data for determining if a hall call is secured
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>

#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "carData.h"
#include "carDestination.h"
#include "hallSecurity.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint32_t auiBF_SecurityInputs_F[BITMAP32_SIZE(NEW_HALL_BOARD_MAX_FLOOR_LIMIT)];
static uint32_t auiBF_SecurityInputs_R[BITMAP32_SIZE(NEW_HALL_BOARD_MAX_FLOOR_LIMIT)];
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Check secured hall floors and return mask for the calls that can be taken
   clear out hall calls that can't be taken.
   Note: Differs from function used on App_MCU_E (Riser Boards)
 -----------------------------------------------------------------------------*/
void ClearSecuredHallCalls_ByLanding( uint8_t ucLanding )
{
   uint32_t uiValidMask = 0;
   for(uint8_t eCarID = 0; eCarID < MAX_GROUP_CARS; eCarID++)
   {
      st_cardata *pstCarData = GetCarDataStructure(eCarID);
      if(GetCarOfflineTimer_ByCar(eCarID) < GROUP_CAR_OFFLINE_TIMER_MS)
      {
         if( !pstCarData->bEnableHallCallSecurity )
         {
            uint32_t uiCarMask = 0;
            if( CarData_CheckOpening(eCarID, ucLanding, DOOR_FRONT) )
            {
               uiCarMask |= pstCarData->uiLatchableHallMask_F;
               uiCarMask |= pstCarData->ucMedicalMask; /* Security clearing bypassed for medical calls */
            }
            if( CarData_CheckOpening(eCarID, ucLanding, DOOR_REAR) )
            {
               uiCarMask |= pstCarData->uiLatchableHallMask_R;
               uiCarMask |= pstCarData->ucMedicalMask; /* Security clearing bypassed for medical calls */
            }

            uiValidMask |= uiCarMask;
         }
         else
         {
            uint32_t uiCarMask = 0;
            uint8_t bFloorSecured = Sys_Bit_Get(&pstCarData->auiBF_HallSecurityMap[0], ucLanding);
            uint8_t bSecurityContact_F = Sys_Bit_Get(&auiBF_SecurityInputs_F[0], ucLanding);
            uint8_t bSecurityContact_R = Sys_Bit_Get(&auiBF_SecurityInputs_R[0], ucLanding);

            if( CarData_CheckOpening(eCarID, ucLanding, DOOR_FRONT) )
            {
               if( !bFloorSecured || bSecurityContact_F )
               {
                  uiCarMask |= pstCarData->uiHallMask_F;
               }
               else // Floor is secured, mask out effected hall calls
               {
                  uiCarMask |= ( pstCarData->uiHallMask_F & ~pstCarData->ucHallSecurityMask_F );
               }
               uiCarMask |= pstCarData->ucMedicalMask; /* Security clearing bypassed for medical calls */
            }
            if( CarData_CheckOpening(eCarID, ucLanding, DOOR_REAR) )
            {
               if( !bFloorSecured || bSecurityContact_R )
               {
                  uiCarMask |= pstCarData->uiHallMask_R;
               }
               else // Floor is secured, mask out effected hall calls
               {
                  uiCarMask |= ( pstCarData->uiHallMask_R & ~pstCarData->ucHallSecurityMask_R );
               }
               uiCarMask |= pstCarData->ucMedicalMask; /* Security clearing bypassed for medical calls */
            }
            uiValidMask |= uiCarMask;
         }
      }
   }
   ClrRawLatchedHallCalls(ucLanding, HC_DIR__DOWN, ~uiValidMask);
   ClrRawLatchedHallCalls(ucLanding, HC_DIR__UP, ~uiValidMask);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void ResetHallSecurity(void)
{
   memset(&auiBF_SecurityInputs_F[0], 0, sizeof(auiBF_SecurityInputs_F));
   memset(&auiBF_SecurityInputs_R[0], 0, sizeof(auiBF_SecurityInputs_R));
}
/*-----------------------------------------------------------------------------
TODO review case where car is attempting to go to serve a HC on both front
and rear opening at a floor, but only one one side's security input is active.
 -----------------------------------------------------------------------------*/
uint8_t GetHallCallSecurity(enum en_group_net_nodes eCarID, uint8_t ucLanding, enum en_doors eDoor, uint32_t uiMask)
{
   uint8_t bAccessGranted = 0;
   st_cardata *pstCarData = GetCarDataStructure(eCarID);

   if( !pstCarData->bEnableHallCallSecurity )
   {
      bAccessGranted = 1;
   }
   else if(ucLanding < MAX_NUM_FLOORS)
   {
      uint8_t bFloorSecured = Sys_Bit_Get(&pstCarData->auiBF_HallSecurityMap[0], ucLanding);
      if(!bFloorSecured)
      {
         bAccessGranted = 1;
      }
      else
      {
         uint8_t ucSecuredMask = 0;
         uint8_t bValidSecurityContacts = 0;
         if( eDoor == DOOR_FRONT )
         {
            ucSecuredMask |= pstCarData->ucHallSecurityMask_F;
            bValidSecurityContacts = Sys_Bit_Get(&auiBF_SecurityInputs_F[0], ucLanding);
         }
         else if( eDoor == DOOR_REAR )
         {
            ucSecuredMask |= pstCarData->ucHallSecurityMask_R;
            bValidSecurityContacts = Sys_Bit_Get(&auiBF_SecurityInputs_R[0], ucLanding);
         }
         else if( eDoor == DOOR_ANY )
         {
            ucSecuredMask |= pstCarData->ucHallSecurityMask_F | pstCarData->ucHallSecurityMask_R;
            bValidSecurityContacts = Sys_Bit_Get(&auiBF_SecurityInputs_F[0], ucLanding);
            bValidSecurityContacts &= Sys_Bit_Get(&auiBF_SecurityInputs_R[0], ucLanding);
         }

         if( !( ucSecuredMask & uiMask ) )
         {
            bAccessGranted = 1;
         }
         else if( bValidSecurityContacts )
         {
            bAccessGranted = 1;
         }
      }
   }
   return bAccessGranted;
}
/*-----------------------------------------------------------------------------
   Unload status of hall call security contacts.
   These are sent over in the latched HC datagrams. Up calls are front contacts.
   Down calls are rear contacts.
 -----------------------------------------------------------------------------*/
void UnloadDatagram_RiserSecurityContacts_F( un_sdata_datagram *punDatagram )
{
   uint8_t ucIndex = punDatagram->auc8[0];
   if( ucIndex < BITMAP32_SIZE(NEW_HALL_BOARD_MAX_FLOOR_LIMIT) )
   {
      auiBF_SecurityInputs_F[ucIndex] = punDatagram->aui32[1];
   }
}
void UnloadDatagram_RiserSecurityContacts_R( un_sdata_datagram *punDatagram )
{
   uint8_t ucIndex = punDatagram->auc8[0];
   if( ucIndex < BITMAP32_SIZE(NEW_HALL_BOARD_MAX_FLOOR_LIMIT) )
   {
      auiBF_SecurityInputs_R[ucIndex] = punDatagram->aui32[1];
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
