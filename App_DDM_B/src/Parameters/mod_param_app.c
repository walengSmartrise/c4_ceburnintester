/******************************************************************************
 *
 * @file     mod_heartbeat.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "sru.h"
#include "sru_b.h"
#include <stdlib.h>
#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_ParamApp =
{
   .pfnInit = Init,
   .pfnRun = Run,
};


/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Return 1 for success
 -----------------------------------------------------------------------------*/
uint8_t SaveAndVerifyParameter( struct st_param_and_value *pstPV )
{
   uint16_t uwIndex;
   uint8_t ucType = Sys_Param_BlockType(pstPV->ucBlockIndex);
   uint8_t bReturn = 0;
   switch(ucType)
   {
      case enPARAM_BLOCK_TYPE__UINT32:
         uwIndex = pstPV->uwParamIndex
                 + enPARAM_NUM_PER_TYPE__UINT32 * (pstPV->ucBlockIndex - STARTING_BLOCK_U32);
         if(uwIndex >= NUM_32BIT_PARAMS)
         {
            bReturn = 0;
         }
         else
         {
            bReturn = Param_WriteValue_32Bit( uwIndex, pstPV->uiValue );
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT24:
         uwIndex = pstPV->uwParamIndex
                 + enPARAM_NUM_PER_TYPE__UINT24 * (pstPV->ucBlockIndex - STARTING_BLOCK_U24);
         if(uwIndex >= NUM_24BIT_PARAMS)
         {
            bReturn = 0;
         }
         else
         {
            bReturn = Param_WriteValue_24Bit( uwIndex, pstPV->uiValue );
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT16:
         uwIndex = pstPV->uwParamIndex
                 + enPARAM_NUM_PER_TYPE__UINT16 * (pstPV->ucBlockIndex - STARTING_BLOCK_U16);
         if(uwIndex >= NUM_16BIT_PARAMS)
         {
            bReturn = 0;
         }
         else
         {
            bReturn = Param_WriteValue_16Bit( uwIndex, pstPV->uiValue );
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT8:
         uwIndex = pstPV->uwParamIndex
                 + enPARAM_NUM_PER_TYPE__UINT8 * (pstPV->ucBlockIndex - STARTING_BLOCK_U8);
         if(uwIndex >= NUM_8BIT_PARAMS)
         {
            bReturn = 0;
         }
         else
         {
            bReturn = Param_WriteValue_8Bit( uwIndex, pstPV->uiValue );
         }
         break;
      default:
      case enPARAM_BLOCK_TYPE__BIT:
         uwIndex = pstPV->uwParamIndex
                 + enPARAM_NUM_PER_TYPE__BIT * (pstPV->ucBlockIndex - STARTING_BLOCK_BIT);
         if(uwIndex >= NUM_1BIT_PARAMS)
         {
            bReturn = 0;
         }
         else
         {
            bReturn = Param_WriteValue_1Bit( uwIndex, pstPV->uiValue );
         }
         break;
   }
   return bReturn;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void VerifyCriticalParams( void )
{
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = 500;

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   if ( !GetFP_FlashParamsReady() )
   {
      if ( Param_ParametersAreValid() )
      {
         Param_InitRingBuffers();
         SetFP_FlashParamsReady();
      }
   }
   else
   {
      VerifyCriticalParams();
      if( !SRU_Read_DIP_Switch( enSRU_DIP_A1 ) )
      {
         Param_ServiceBuffer();
      }
      if( ParamBuff_GetFault() )
      {
         SetAlarm(DDM_ALM__PARAM_QUEUE_B);
      }
   }
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
