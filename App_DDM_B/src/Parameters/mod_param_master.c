/******************************************************************************
 *
 * @file     mod_param_master.c
 * @brief
 * @version  V1.00
 * @date     13, April 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "mod.h"
#include "sru_b.h"
#include "sys.h"
#include "motion.h"
#include "config_System.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_ParamMaster =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

enum en_mod_param_states
{
   enPSTATE__AWAITING_VALID_RAM_COPY,
   enPSTATE__RUNNING,
};

// These are all the networks that the master parameter module talks on
// directly.
enum enum_master_param_nets
{
   MASTER_PARAM_NET__MRA_MRB,
   MASTER_PARAM_NET__CAR_NET,

   NUM_MASTER_PARAM_NETS
};

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static enum_default_cmd eDefaultCommand;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetDefaultParameterCommand( enum_default_cmd eCommand )
{
   if( eCommand < NUM_DEFAULT_CMD )
   {
      eDefaultCommand = eCommand;
   }
}
/*-----------------------------------------------------------------------------
    Check for default all request from UI
 -----------------------------------------------------------------------------*/
static void CheckFor_DefaultAllRequest()
{
   if(eDefaultCommand)
   {
#ifdef DEBUG
      if(1)
#else
      uint8_t ucState;
      uint32_t uiIndex;
      if(DefaultAll(&ucState, &uiIndex, eDefaultCommand ))
#endif
      {
         eDefaultCommand = DEFAULT_CMD__NONE;
      }
      else
      {
         DefaultAll_FSM eState = GetDefaultAllState();
         if( ( eState != DEFAULT_ALL_IDLE )
          && ( eState < DEFAULT_ALL_COMPLETE ) )
         {
            en_alarms eAlarm = ALM__DEFAULT_PARAM_1BIT+eState-DEFAULT_ALL_ONE_BIT;
            if(eAlarm > ALM__DEFAULT_PARAM_32BIT)
            {
               eAlarm = ALM__DEFAULT_PARAM_32BIT;
            }
            SetAlarm(eAlarm);
         }
      }
   }
}
/*-----------------------------------------------------------------------------
 Returns 1 if parameter updates are allowed
 -----------------------------------------------------------------------------*/
static uint8_t CheckIf_ValidParameterUpdateState()
{
   uint8_t bValid = 0;
   if( !SRU_Read_DIP_Switch( enSRU_DIP_A1 ) )
   {
      bValid = 1;
   }
   return bValid;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadSlaveRequest_DDM_A( struct st_param_control *pstParamControl )
{
   struct st_param_and_value stParamAndValue;
   uint8_t ucValidNode = DDM_NODE__A;
   enum en_param_datagram_message_types enParamDatagramType = ABNet_GetDatagram_U8(0, DG_DDMA__ParamSlaveRequest, ucValidNode);
   switch ( enParamDatagramType )
   {
      case enPARAM_DG__REQ_TO_SET_A_PARAM:  // request to set a parameter's value
         {
            uint8_t ucBlock_Plus1 = ABNet_GetDatagram_U8(1, DG_DDMA__ParamSlaveRequest, ucValidNode);
            uint16_t uwParam_Plus1 = ABNet_GetDatagram_U16(1, DG_DDMA__ParamSlaveRequest, ucValidNode);
            if( ucBlock_Plus1
             && uwParam_Plus1 )
            {
               stParamAndValue.ucBlockIndex = ucBlock_Plus1 - 1;
               stParamAndValue.uwParamIndex = uwParam_Plus1 - 1;
               stParamAndValue.uiValue = ABNet_GetDatagram_U32(1, DG_DDMA__ParamSlaveRequest, ucValidNode);
               Param_WriteValue( &stParamAndValue );
            }
         }
         break;

      case enPARAM_DG__REQ_FOR_PARAM_CHUNK:
         {
            uint8_t ucBlock_Plus1 = ABNet_GetDatagram_U8(1, DG_DDMA__ParamSlaveRequest, ucValidNode);
            uint8_t ucChunk_Plus1 = ABNet_GetDatagram_U8(2, DG_DDMA__ParamSlaveRequest, ucValidNode);
            uint8_t ucNode = ABNet_GetDatagram_U8(3, DG_DDMA__ParamSlaveRequest, ucValidNode);
            if( (ucNode == ucValidNode)
              && ucBlock_Plus1
              && ucChunk_Plus1  )
            {
               if( Sys_ParamChunk_IsValid(ucBlock_Plus1 - 1, ucChunk_Plus1 - 1) )
               {
                  struct st_param_chunk_req *pstSlaveRequestedChunk;
                  pstSlaveRequestedChunk = &(pstParamControl->astSlaveRequestedChunk[ ucValidNode ]);
                  pstSlaveRequestedChunk->ucBlock_Plus1 = ucBlock_Plus1;
                  pstSlaveRequestedChunk->ucChunk_Plus1 = ucChunk_Plus1;
                  pstSlaveRequestedChunk->ucCountdown = PARAM_CHUNK_REQ_COUNTDOWN;
               }
            }
         }
         break;
      default:  // assume enPARAM_DG__NULL -- no action requested, no data being sent
         break;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadDatagram__ParamSlaveRequest( struct st_param_control *pstParamControl )
{
   UnloadSlaveRequest_DDM_A(pstParamControl);
}

/*-----------------------------------------------------------------------------
   Each Block's CRC will be sent 5 times.
   Then the next changed CRC will be sent.
 -----------------------------------------------------------------------------*/
static void LoadDatagram__ParamMaster_BlockCRCs( struct st_param_control *pstParamControl )
{
   static const uint32_t uiDatagramIndex = DG_DDMB__ParamMaster_BlockCRCs1;
   static uint8_t aucResendCountdowns[ NUM_PARAM_BLOCKS ];
   struct st_sdata_control *pstSData_Control = &gstSData_LocalData;
   un_sdata_datagram unDatagram;

   if( pstSData_Control )
   {
      for( uint8_t i = 0; i < NUM_PARAM_BLOCKS; i++ )
      {
         int iOldDirtyBit = SDATA_DirtyBit_Get( pstSData_Control, uiDatagramIndex + i );
         uint32_t ulCRC = Param_GetBlockCRC_RAM(i);
         unDatagram.auc8[0] = enPARAM_DG__SENDING_BLOCK_CRC;
         unDatagram.auc8[1] = i + 1;
         unDatagram.auw16[1] = 0;
         unDatagram.aui32[1] = ulCRC;
         SDATA_WriteDatagram( pstSData_Control, uiDatagramIndex + i, &unDatagram );

         //If CRC has changed, send a few times
         if( !iOldDirtyBit && SDATA_DirtyBit_Get( pstSData_Control, uiDatagramIndex + i ) )
         {
            aucResendCountdowns[ i ] = NUM_TIMES_TO_RESEND_PARAM_CRC;
         }

         if( aucResendCountdowns[ i ] )
         {
            aucResendCountdowns[ i ]--;
            SDATA_DirtyBit_Set( pstSData_Control, uiDatagramIndex + i );
         }
      }
   }
}

/*-----------------------------------------------------------------------------
   Master will load values only after receiving a chunk request from a slave.

 -----------------------------------------------------------------------------*/
static void LoadDatagram__ParamValues( struct st_param_control *pstParamControl )
{
   struct st_sdata_control *pstSData_Control = &gstSData_LocalData;
   if( !SDATA_DirtyBit_Get(pstSData_Control, DG_DDMB__ParamMaster_ParamValues) )
   {
      un_sdata_datagram unDatagram;
      struct st_param_chunk_req *pstRequestedChunk = &pstParamControl->astSlaveRequestedChunk[ DDM_NODE__A ];
      if( pstRequestedChunk->ucBlock_Plus1
       && pstRequestedChunk->ucChunk_Plus1
       && pstRequestedChunk->ucCountdown )
      {
         pstRequestedChunk->ucCountdown--;
         unDatagram.auc8[ 0 ] = enPARAM_DG__SENDING_PARAM_CHUNK;
         unDatagram.auc8[ 1 ] = pstRequestedChunk->ucBlock_Plus1;
         unDatagram.auc8[ 2 ] = pstRequestedChunk->ucChunk_Plus1;
         unDatagram.auc8[ 3 ] = DDM_NODE__A;
         unDatagram.aui32[ 1 ] = Param_ReadChunk( pstRequestedChunk->ucBlock_Plus1 - 1,
                                                  pstRequestedChunk->ucChunk_Plus1 - 1 );

         SDATA_WriteDatagram( pstSData_Control,
                              DG_DDMB__ParamMaster_ParamValues,
                              &unDatagram
                            );
         SDATA_DirtyBit_Set( pstSData_Control,
                             DG_DDMB__ParamMaster_ParamValues );
      }
      else
      {
         pstRequestedChunk->ucBlock_Plus1 = 0;
         pstRequestedChunk->ucChunk_Plus1 = 0;
         unDatagram.aui32[0] = 0;
         unDatagram.aui32[1] = 0;
         SDATA_WriteDatagram( pstSData_Control,
                              DG_DDMB__ParamMaster_ParamValues,
                              &unDatagram
                            );
      }
   }
}

/*-----------------------------------------------------------------------------

   Clear send requests for parameter datagrams

 -----------------------------------------------------------------------------*/
static void ClearParamUpdateRequests()
{
   for( uint8_t i = 0; i < NUM_PARAM_BLOCKS; i++ )
   {
      SDATA_DirtyBit_Clr( &gstSData_LocalData,
                          DG_DDMB__ParamMaster_BlockCRCs1 + i);
   }
   SDATA_DirtyBit_Clr( &gstSData_LocalData,
                       DG_DDMB__ParamMaster_ParamValues);
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   gstParamControl.ucParamNodeType = DDM_NODE__B;
   gstParamControl.pstParamBlockInfo = &gstSys_ParamBlockInfo;

   gstParamControl.bMaster = 1;

   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = 10;
   return 0;
}

/*-----------------------------------------------------------------------------

   The EEPROMI can take up to 3 ms to write a page so don't call Run() more
   often than that and never do more than one write operation per call.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static uint8_t ucState = enPSTATE__AWAITING_VALID_RAM_COPY;
   struct st_param_control *pstParamControl = &gstParamControl;
   switch( ucState )
   {
      case enPSTATE__RUNNING:
         if( !CheckIf_ValidParameterUpdateState() )
         {
            ClearParamUpdateRequests();
         }
         else
         {
            CheckFor_DefaultAllRequest();
            UnloadDatagram__ParamSlaveRequest( pstParamControl );

            LoadDatagram__ParamMaster_BlockCRCs( pstParamControl );
            LoadDatagram__ParamValues( pstParamControl );
         }
         break;

      default:  // enPSTATE__AWAITING_VALID_RAM_COPY
         if ( pstParamControl->bRAM_CopyValid )
         {
            Param_SetMaster();
            ucState = enPSTATE__RUNNING;
         }
         break;
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

