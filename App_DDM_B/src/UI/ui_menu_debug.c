/******************************************************************************
 *
 * @file     ui_menu_setup.c
 * @brief    Setup Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "mod.h"
#include "sru.h"
#include <string.h>
#include <math.h>
#include "lcd.h"
#include "groupnet_datagrams.h"
#include "groupnet.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Debug_ViewDebugData( void );
static void UI_FFS_Debug_ViewGroupPackets( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_ui_menu_item gstMI_ParamEdit =
{
   .psTitle = "Edit Parameters",
   .pstUGS_Next = &gstUGS_Menu_ParamEdit,
};
static struct st_ui_menu_item gstMI_ViewGroupPackets =
{
   .psTitle = "View Group Packets",
   .pstUGS_Next = &gstUGS_Debug_ViewGroupPackets,
};
static struct st_ui_menu_item gstMI_ViewDebugData =
{
   .psTitle = "View Debug Data",
   .pstUGS_Next = &gstUGS_Debug_ViewDebugData,
};
static struct st_ui_menu_item gstMI_AssignedCalls =
{
   .psTitle = "Assigned Calls",
   .pstUGS_Next = &gstUGS_AssignedCalls,
};
static struct st_ui_menu_item gstMI_CarData =
{
   .psTitle = "Car Data",
   .pstUGS_Next = &gstUGS_CarData,
};
static struct st_ui_menu_item gstMI_PanelStatus =
{
   .psTitle = "Panel Status",
   .pstUGS_Next = &gstUGS_PanelStatus,
};
static struct st_ui_menu_item gstMI_ExpansionStatus =
{
   .psTitle = "Expansion Status",
   .pstUGS_Next = &gstUGS_Menu_ExpansionStatus,
};
static struct st_ui_menu_item gstMI_TotalCostData =
{
   .psTitle = "Total Cost Data",
   .pstUGS_Next = &gstUGS_TotalCostData,
};
static struct st_ui_menu_item gstMI_HallCallStatus =
{
   .psTitle = "Hall Call Status",
   .pstUGS_Next = &gstUGS_Menu_HallCallStatus,
};
static struct st_ui_menu_item gstMI_RiserStatus =
{
   .psTitle = "Riser Board Status",
   .pstUGS_Next = &gstUGS_RiserStatus,
};
static struct st_ui_menu_item gstMI_CarDestinations =
{
   .psTitle = "Car Destinations",
   .pstUGS_Next = &gstUGS_CarDestination,
};
static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_ParamEdit,
   &gstMI_ViewGroupPackets,
   &gstMI_ViewDebugData,
   &gstMI_AssignedCalls,
   &gstMI_CarData,
   &gstMI_CarDestinations,
   &gstMI_RiserStatus,
   &gstMI_PanelStatus,
   &gstMI_ExpansionStatus,
   &gstMI_TotalCostData,
   &gstMI_HallCallStatus,
};

static struct st_ui_screen__menu gstMenu =
{
   .psTitle = "Debug",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};

//---------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Debug_ViewGroupPackets =
{
   .pfnDraw = UI_FFS_Debug_ViewGroupPackets,
};
//---------------------------------------------------------------
static struct st_ui_screen__freeform gstFFS_Debug_ViewDebugData =
{
   .pfnDraw = UI_FFS_Debug_ViewDebugData,
};

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_Menu_Debug =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu,
};

//----------------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Debug_ViewGroupPackets =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Debug_ViewGroupPackets,
};
//----------------------------------------------------------------------
struct st_ui_generic_screen gstUGS_Debug_ViewDebugData =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Debug_ViewDebugData,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
For viewing debugging data such as network error counters

|01234567890123456789|

|View Debug Data     |
|000 (COP CAN4)      |
|*                   |
|Err:65535 Util:100% | <-- Error Count, CAN Utilization
 -----------------------------------------------------------------------------*/
#define VIEW_DEBUG_DATA_MAX_SCROLL_X      (3)
static void PrintScreen_ViewDebugData( uint8_t ucScrollX, en_view_debug_data_ddm eCommand )
{
   static char const* const pasDebugDataStr[NUM_VDDD] =
   {
      "None",//VDD__NONE,
      "DDM CAN1",//VDDD__DDM_CAN1,
      "DDM CAN2",//VDDD__DDM_CAN2,
      "DDM CAN3",//VDDD__DDM_CAN3,
      "DDM CAN4",//VDDD__DDM_CAN4,
      "DDM A NET",//VDDD__DDM_A_NET,
      "DDM B NET",//VDDD__DDM_B_NET,
      "DDM RS485",//VDD__DDM_RS485,
      "DDMA Vers.",//VDDD__DDMA_VERSION,
      "DDMB Vers.",//VDDD__DDMB_VERSION,
   };

   LCD_Char_Clear();

   /* L1 */
   LCD_Char_GotoXY(0,0);
   LCD_Char_WriteString("View Debug Data");

   /* L2 */
   LCD_Char_GotoXY(0,1);
   LCD_Char_WriteInteger_ExactLength(eCommand, 3);
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteString("(");
   LCD_Char_WriteString(pasDebugDataStr[eCommand]);
   LCD_Char_WriteString(")");

   /* L3 */
   uint8_t ucCursorPos = ucScrollX;
   LCD_Char_GotoXY(ucCursorPos,2);
   LCD_Char_WriteString("*");

   /* L4 - Data */
   LCD_Char_GotoXY(0,3);
   switch(eCommand)
   {
      case VDDD__DDM_CAN1:
      case VDDD__DDM_CAN2:
      case VDDD__DDM_RS485:
      case VDDD__DDM_A_NET:
         LCD_Char_WriteString("Err:");
         LCD_Char_WriteInteger_ExactLength(ABNet_GetDatagram_U16(1, DG_DDMA__Debug, DDM_NODE__A), 5);
         if( ( eCommand == VDDD__DDM_CAN1 ) || ( eCommand == VDDD__DDM_CAN2 ) )
         {
            LCD_Char_AdvanceX(1);
            LCD_Char_WriteString("Util:");
            LCD_Char_WriteInteger_MinLength(ABNet_GetDatagram_U8(0, DG_DDMA__Debug, DDM_NODE__A), 1);
            LCD_Char_WriteString("%");
         }
         break;
      case VDDD__DDM_CAN3:
      case VDDD__DDM_CAN4:
      case VDDD__DDM_B_NET:
         LCD_Char_WriteString("Err:");
         if( eCommand == VDDD__DDM_CAN3 )
         {
            LCD_Char_WriteInteger_ExactLength(GetDebugBusOfflineCounter_CAN1(), 5);
            LCD_Char_AdvanceX(1);
            LCD_Char_WriteString("Util:");
            LCD_Char_WriteInteger_MinLength(Debug_CAN_GetUtilization(0), 1);
            LCD_Char_WriteString("%");
         }
         else if( eCommand == VDDD__DDM_CAN4 )
         {
            LCD_Char_WriteInteger_ExactLength(GetDebugBusOfflineCounter_CAN2(), 5);
            LCD_Char_AdvanceX(1);
            LCD_Char_WriteString("Util:");
            LCD_Char_WriteInteger_MinLength(Debug_CAN_GetUtilization(1), 1);
            LCD_Char_WriteString("%");
         }
         else
         {
            LCD_Char_WriteInteger_ExactLength(UART_GetRxErrorCount(), 5);
         }
         break;
      case VDDD__DDMA_VERSION:
      {
         LCD_Char_WriteString("v.");
         uint32_t uiVersion = ABNet_GetDatagram_U32(0, DG_DDMA__Debug, DDM_NODE__A);
         for(uint8_t i = 0; i < 4; i++)
         {
            uint8_t ucChar = GET_BYTE_U32( uiVersion, i );
            if( !( ( ( ucChar >= '0' ) && ( ucChar <= '9' ) )
                || ( ( ucChar >= 'a' ) && ( ucChar <= 'z' ) )
                || ( ( ucChar >= 'A' ) && ( ucChar <= 'Z' ) ) ) )
            {
               ucChar = ' ';
            }
            LCD_Char_WriteChar(ucChar);
         }
      }
         break;
      case VDDD__DDMB_VERSION:
         LCD_Char_WriteString("v.");
         for(uint8_t i = 0; i < 4; i++)
         {
            uint8_t ucChar = *(pasVersion+i);
            if( !( ( ( ucChar >= '0' ) && ( ucChar <= '9' ) )
                || ( ( ucChar >= 'a' ) && ( ucChar <= 'z' ) )
                || ( ( ucChar >= 'A' ) && ( ucChar <= 'Z' ) ) ) )
            {
               ucChar = ' ';
            }
            LCD_Char_WriteChar(ucChar);
         }
         break;
      default:
         LCD_Char_WriteString("N/A");
         break;
   }
}
static void UI_FFS_Debug_ViewDebugData( void )
{
   static uint8_t ucScrollX;
   static en_view_debug_data_ddm eLastViewDebugData;
   int16_t wEditedValue = eLastViewDebugData;

   //-----------------------------------------
   /* Screen control */
   int8_t cMagnitude = 0;
   if( ucScrollX == 0 )
   {
      cMagnitude = 100;
   }
   else if( ucScrollX == 1 )
   {
      cMagnitude = 10;
   }
   else if( ucScrollX == 2 )
   {
      cMagnitude = 1;
   }

   enum en_keypresses enKeypress = Button_GetKeypress();
   switch(enKeypress)
   {
      case enKEYPRESS_LEFT:
         if(!ucScrollX)
         {
            PrevScreen();
         }
         else
         {
            ucScrollX--;
         }
         break;
      case enKEYPRESS_RIGHT:
         ucScrollX++;
         break;
      case enKEYPRESS_UP:
         wEditedValue += cMagnitude;
         break;
      case enKEYPRESS_DOWN:
         wEditedValue -= cMagnitude;
         break;
      default: break;
   }
   /* Bound Values */
   if( wEditedValue < 0 )
   {
      wEditedValue = 0;
   }
   else if( wEditedValue >= NUM_VDDD )
   {
      wEditedValue = NUM_VDDD-1;
   }

   if(ucScrollX >= VIEW_DEBUG_DATA_MAX_SCROLL_X)
   {
      ucScrollX = VIEW_DEBUG_DATA_MAX_SCROLL_X-1;
   }
   eLastViewDebugData = wEditedValue;
   SetUIRequest_ViewDebugDataCommand(eLastViewDebugData);
   PrintScreen_ViewDebugData(ucScrollX, eLastViewDebugData);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void PrintScreen_ViewGroupPackets(uint8_t ucCursor, GroupNet_DatagramID eID)
{
   LCD_Char_Clear();

   /* Marker */
   LCD_Char_GotoXY(3,0);
   LCD_Char_WriteString("(LSB)");

   /* Datagram Data */
   un_sdata_datagram *punDatagram = GetGroupDatagram( eID );
   LCD_Char_GotoXY(9,0);
   LCD_Char_WriteHex_8( punDatagram->auc8[0] );
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteHex_8( punDatagram->auc8[1] );
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteHex_8( punDatagram->auc8[2] );
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteHex_8( punDatagram->auc8[3] );

   LCD_Char_GotoXY(9,1);
   LCD_Char_WriteHex_8( punDatagram->auc8[4] );
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteHex_8( punDatagram->auc8[5] );
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteHex_8( punDatagram->auc8[6] );
   LCD_Char_AdvanceX(1);
   LCD_Char_WriteHex_8( punDatagram->auc8[7] );

   /* Datagram Num */
   LCD_Char_GotoXY(0,2);
   LCD_Char_WriteString("PACKET ");
   LCD_Char_WriteInteger_ExactLength( eID, 3 );


   /* Receive Count */
   LCD_Char_GotoXY(11,3);
   LCD_Char_WriteString("RX:");
   LCD_Char_WriteInteger_ExactLength( (uint16_t) GroupNet_GetDatagramRXCounter(eID), 5 );

   /* Cursor */
   LCD_Char_GotoXY(7+ucCursor,3);
   LCD_Char_WriteString("*");
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Debug_ViewGroupPackets( void )
{
   static const uint8_t ucMaxCursorPos = 2;
   static uint8_t ucCursorPos;
   static Datagram_ID ePacketIndex;
   /* Screen Navigation */
   int32_t iNewPacketIndex = ePacketIndex;
   int32_t iPosMagnitude = powf(10, ucMaxCursorPos-ucCursorPos);
   enum en_keypresses enKeypress = Button_GetKeypress();
   if ( enKeypress == enKEYPRESS_LEFT )
   {
      if( ucCursorPos )
      {
         ucCursorPos--;
      }
      else
      {
         PrevScreen();
      }
   }
   else if ( enKeypress == enKEYPRESS_RIGHT )
   {
      if( ucCursorPos < ucMaxCursorPos )
      {
         ucCursorPos++;
      }
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      iNewPacketIndex += iPosMagnitude;
   }
   else if ( enKeypress == enKEYPRESS_DOWN )
   {
      iNewPacketIndex -= iPosMagnitude;
   }

   if( iNewPacketIndex < 0 )
   {
      iNewPacketIndex = 0;
   }
   else if( iNewPacketIndex > GROUP_NET_NUM_DATAGRAMS )
   {
      iNewPacketIndex = GROUP_NET_NUM_DATAGRAMS;
   }
   ePacketIndex = iNewPacketIndex;

   /* Screen Update */
   PrintScreen_ViewGroupPackets(ucCursorPos, ePacketIndex);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
