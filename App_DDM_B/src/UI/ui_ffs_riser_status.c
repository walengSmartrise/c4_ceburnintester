
/******************************************************************************
 *
 * @file     ui_menu_io.c
 * @brief    I/O Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "GlobalData.h"
#include "riser.h"
#include "riserData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_RiserStatus( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
//---------------------------------------------------------------

static struct st_ui_screen__freeform gstFFS_RiserStatus =
{
   .pfnDraw = UI_FFS_RiserStatus,
};


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_RiserStatus =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_RiserStatus,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

static char const * const pasError[NUM_RIS_EXP_ERROR] =
{
      "Unknown",//ERROR_RIS_EXP__UNKNOWN,
      "None",//ERROR_RIS_EXP__NONE,
      "POR Rst",//ERROR_RIS_EXP__RESET_POR,
      "WDT Rst",//ERROR_RIS_EXP__RESET_WDT,
      "BOD Rst",//ERROR_RIS_EXP__RESET_BOD,
      "Comm. Group",//ERROR_RIS_EXP__COM_GROUP,
      "Comm. Hall",//ERROR_RIS_EXP__COM_HALL,
      "Comm. Car",//ERROR_RIS_EXP__COM_CAR,
      "Comm. Master",//ERROR_RIS_EXP__COM_MASTER,
      "Driver",//ERROR_RIS_EXP__DRIVER_FLT,
      "Address",//ERROR_RIS_EXP__ADDRESS, // Two boards with same DIP address
      "CAN1 Rst",//ERROR_RIS_EXP__BUS_RESET_CAN1,
      "CAN2 Rst",//ERROR_RIS_EXP__BUS_RESET_CAN2,
      "Comm. Slave",//ERROR_RIS_EXP__COM_SLAVE,
      "Unused14",//ERROR_RIS_EXP__UNUSED14,
};
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
|01234567890123456789|

|RIS1 Inactive v61b  |
|ERR: None           |
|IN:  ....5678       |
|OUT: 1234....       |

 ----------------------------------------------------------------------------*/
static void Print_Screen( uint8_t ucAddress )
{
   LCD_Char_Clear();

   /* Riser Number */
   LCD_Char_GotoXY( 0, 0 );
   uint8_t ucRiserNumber = ucAddress+1;
   LCD_Char_WriteString("RIS");
   LCD_Char_WriteInteger_ExactLength(ucRiserNumber, 1);

   /* Online status */
   LCD_Char_GotoXY( 5, 0 );
   uint16_t uwOfflineCounter_ms = RiserData_GetOfflineCounter(ucAddress);
   if( uwOfflineCounter_ms < RISER_OFFLINE_COUNT_MS__FAULTED )
   {
      LCD_Char_WriteString("Active");
   }
   else if( uwOfflineCounter_ms < RISER_OFFLINE_COUNT_MS__UNUSED )
   {
      LCD_Char_WriteString("Offline");
   }
   else
   {
      LCD_Char_WriteString("Inactive");
   }

   /* Software Version */
   LCD_Char_GotoXY( 14, 0 );
   char *psVer = RiserData_GetVersionString(ucAddress);
   LCD_Char_WriteString("v.");
   for(uint8_t i = 0; i < NUM_RIS_VERSION_CHARACTERS; i++)
   {
      uint8_t uc = ( *(psVer+i) ) ? *(psVer+i) : ' ';
      LCD_Char_WriteChar(uc);
   }

   /* ERR */
   LCD_Char_GotoXY( 1, 1 );
   en_ris_exp_error eError = RiserData_GetError(ucAddress);
   LCD_Char_WriteString("ERR: ");
   LCD_Char_WriteString(pasError[eError]);

   /* Inputs */
   LCD_Char_GotoXY( 1, 2 );
   uint8_t ucInputs = RiserData_GetInputs(ucAddress);
   LCD_Char_WriteString("IN:  ");
   for(uint8_t i = 0; i < 8; i++)
   {
      if(Sys_Bit_Get(&ucInputs, i))
      {
         LCD_Char_WriteInteger_ExactLength(i+1, 1);
      }
      else
      {
         LCD_Char_WriteString(".");
      }
   }

   /* Outputs */
   LCD_Char_GotoXY( 1, 3 );
   uint8_t ucOutputs = RiserData_GetOutputs(ucAddress);
   LCD_Char_WriteString("OUT: ");
   for(uint8_t i = 0; i < 8; i++)
   {
      if(Sys_Bit_Get(&ucOutputs, i))
      {
         LCD_Char_WriteInteger_ExactLength(i+1, 1);
      }
      else
      {
         LCD_Char_WriteString(".");
      }
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_RiserStatus( void )
{
   static int8_t cAddress;

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      cAddress++;
   }
   else if ( enKeypress == enKEYPRESS_DOWN )
   {
      cAddress--;
   }

   /* Bound value */
   if( cAddress >= MAX_NUM_RISER_BOARDS )
   {
      cAddress = MAX_NUM_RISER_BOARDS-1;
   }
   else if( cAddress < 0 )
   {
      cAddress = 0;
   }


   Print_Screen( cAddress );
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/


