/******************************************************************************
 *
 * @file     ui_ffs_templates.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>
#include <string.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t gbEnteredMenu;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define START_CURSOR_POS_VALUE (7U)
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
static void PrintArrayString(  struct st_param_edit_menu * pstParamEditMenu  )
{
	uint8_t ucSizeOfArray = pstParamEditMenu->ucArraySize; //sizeof((*pstParamEditMenu->ucaPTRChoiceList))/sizeof((*pstParamEditMenu->ucaPTRChoiceList)[0]);
	uint8_t ucArrayIndex = (pstParamEditMenu->ucFieldSize_Index) ?
			(pstParamEditMenu->uwParamIndex_Current - pstParamEditMenu->uwParamIndex_Start)
			: pstParamEditMenu->ulValue_Edited;
	uint8_t ucLength;
	static uint8_t ucIndex;
    static uint8_t ucShiftDelay_30ms;

	if(ucArrayIndex< ucSizeOfArray)
	{
		// print the characters
		ucLength = strlen((*pstParamEditMenu->ucaPTRChoiceList)[ucArrayIndex]);
		if(ucLength > 10)
	    {
		   if(++ucShiftDelay_30ms >= 20)
		   {
			  ucShiftDelay_30ms = 0;
			  if(++ucIndex >= (ucLength - 10))
			  {
			 	ucIndex = 0;
			  }
		   }
	    }else
	    {
	    	ucIndex = 0;
	    }



		LCD_Char_GotoXY( 0, 1 );
		LCD_Char_WriteStringUpper((*pstParamEditMenu->ucaPTRChoiceList)[ucArrayIndex] + ucIndex);
	} else
	{
		// print nothing
		LCD_Char_GotoXY( 0, 1 );
		LCD_Char_WriteString("");
	}

}
#if 0
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void PrintMovingTitle(struct st_param_edit_menu * pstParamEditMenu)
{
   static uint8_t ucIndex;
   static uint8_t ucShiftDelay_30ms;
   uint8_t ucLength = strlen(pstParamEditMenu->psTitle);
   if(ucLength > 20)
   {
      if(gbEnteredMenu)
      {
         ucIndex = 0;
         gbEnteredMenu = 0;
      }
      if(++ucShiftDelay_30ms >= 20)
      {
         ucShiftDelay_30ms = 0;
         if(++ucIndex >= (ucLength - 20))
         {
            ucIndex = 0;
         }
      }
   }
   LCD_Char_WriteStringUpper( pstParamEditMenu->psTitle + ucIndex );
}
#endif

/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void Display_DecimalPoints_None(struct st_param_edit_menu * pstParamEditMenu)
{
   uint8_t ucIncreasedFieldSize = 0;
   uint32_t uiValue = pstParamEditMenu->ulValue_Edited;
   if( ( pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__UINT8 )
    && ( pstParamEditMenu->uwParamIndex_Current == enPARAM8__GroupCarIndex ) )
   {
      uiValue = pstParamEditMenu->ulValue_Edited+1;
   }
   else if( pstParamEditMenu->ucValueMultiplier )
   {
      uiValue = pstParamEditMenu->ulValue_Edited*pstParamEditMenu->ucValueMultiplier;
      if(pstParamEditMenu->ucValueMultiplier <= 10)
      {
         ucIncreasedFieldSize=1;
      }
      else if(pstParamEditMenu->ucValueMultiplier <= 100)
      {
         ucIncreasedFieldSize=2;
      }
   }
   LCD_Char_WriteInteger_ExactLength(uiValue, pstParamEditMenu->ucFieldSize_Value+ucIncreasedFieldSize);

   LCD_Char_WriteString(pstParamEditMenu->psUnit);
   LCD_Char_GotoXY( pstParamEditMenu->ucCursorColumn+ucIncreasedFieldSize, 3 );
   LCD_Char_WriteChar( '*' );
}
static void Display_DecimalPoints(struct st_param_edit_menu * pstParamEditMenu)
{
   uint8_t ucDecimalPoints = pstParamEditMenu->enDecimalFormat;
   uint16_t ulDivisor = pow(10,ucDecimalPoints);
   uint32_t ulInteger = pstParamEditMenu->ulValue_Edited/ulDivisor;
   uint16_t uwDecimal = pstParamEditMenu->ulValue_Edited%ulDivisor;

   LCD_Char_WriteInteger_ExactLength(ulInteger, pstParamEditMenu->ucFieldSize_Value - ucDecimalPoints);
   uint8_t ucCursorPos = pstParamEditMenu->ucCursorColumn;
   if(pstParamEditMenu->ucCursorColumn == (START_CURSOR_POS_VALUE + pstParamEditMenu->ucFieldSize_Value - 1))
   {
      ucCursorPos++;
   }
   LCD_Char_WriteString(".");
   LCD_Char_WriteInteger_ExactLength(uwDecimal, ucDecimalPoints);
   LCD_Char_WriteString(pstParamEditMenu->psUnit);

   LCD_Char_GotoXY( ucCursorPos, 3 );
   LCD_Char_WriteChar( '*' );
}

/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/
static void Screen_Default(struct st_param_edit_menu * pstParamEditMenu)
{
   LCD_Char_Clear();

   LCD_Char_GotoXY( 0, 0 );

   //------------------------------------------------------------------
   if(pstParamEditMenu->bSaving)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Saving");
   }
   else if(pstParamEditMenu->bInvalid)
   {
      LCD_Char_GotoXY(7,2);
      LCD_Char_WriteStringUpper("Invalid");
   }
   else
   {
	  LCD_Char_GotoXY( 0, 0 );
	  LCD_Char_WriteStringUpper( pstParamEditMenu->psTitle);
      PrintArrayString(pstParamEditMenu);
      //------------------------------------------------------------------
      if(pstParamEditMenu->uwParamIndex_Start != pstParamEditMenu->uwParamIndex_End)
      {
         LCD_Char_GotoXY(1,2);
         LCD_Char_WriteInteger_ExactLength( pstParamEditMenu->uwParamIndex_Current - pstParamEditMenu->uwParamIndex_Start + 1, 2 );
         LCD_Char_WriteStringUpper(" = ");
      }

      LCD_Char_GotoXY(START_CURSOR_POS_VALUE, 2);
      if(pstParamEditMenu->enParamType == enPARAM_BLOCK_TYPE__BIT)
      {
         if(pstParamEditMenu->ulValue_Edited)
         {
            LCD_Char_WriteString("On");
         }
         else
         {
            LCD_Char_WriteString("Off");
         }
         LCD_Char_GotoXY( pstParamEditMenu->ucCursorColumn, 3 );

         LCD_Char_WriteChar( '*' );
      }
      else
      {
         if(pstParamEditMenu->enDecimalFormat == enNumberDecimalPoints_None)
         {
            Display_DecimalPoints_None(pstParamEditMenu);
         }
         else
         {
            Display_DecimalPoints(pstParamEditMenu);
         }
      }

      //------------------------------------------------------------------
      if ( pstParamEditMenu->ulValue_Edited != pstParamEditMenu->ulValue_Saved )
      {
         LCD_Char_GotoXY( 16, 1 );
         LCD_Char_WriteString( "Save" );

         LCD_Char_GotoXY( 19, 2 );
         LCD_Char_WriteChar( '|' );
      }
   }
}
/*-----------------------------------------------------------------------------

------------------------------------------------------------------------------*/

static void Update_Screen(struct st_param_edit_menu * pstParamEditMenu)
{
   Screen_Default(pstParamEditMenu);
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ValidateParameter(struct st_param_edit_menu * pstParamEditMenu)
{
   switch(pstParamEditMenu->enParamType)
   {
      case enPARAM_BLOCK_TYPE__BIT:
         pstParamEditMenu->ulValue_Limit = 1;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_1BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_1Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT8:
         pstParamEditMenu->ulValue_Limit = 0xFF;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_8BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_8Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT16:
         pstParamEditMenu->ulValue_Limit = 0xFFFF;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_16BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_16Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT24:
         pstParamEditMenu->ulValue_Limit = 0xFFFFFF;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_24BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_24Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT32:
         pstParamEditMenu->ulValue_Limit = 0xFFFFFFFF;
         if(pstParamEditMenu->uwParamIndex_Current >= NUM_32BIT_PARAMS)
         {
            pstParamEditMenu->bInvalid = 1;
         }
         else
         {
            pstParamEditMenu->ulValue_Saved = Param_ReadValue_32Bit(pstParamEditMenu->uwParamIndex_Current);
         }
         break;
      default:
         pstParamEditMenu->bInvalid = 1;
         break;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Index( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_param_edit_menu * pstParamEditMenu )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex = 0;
   int32_t lParamIndex_Edited;

   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = pstParamEditMenu->ucFieldSize_Index-1;
      }
   }
   else  // enACTION__EDIT
   {

      lParamIndex_Edited = pstParamEditMenu->uwParamIndex_Current;

      pstParamEditMenu->ucCursorColumn = 1+ucCursorIndex;
      //------------------------------------------------------------------
      int32_t uiDigitWeight = (pstParamEditMenu->ucFieldSize_Index - 1) - ucCursorIndex;

      uiDigitWeight = pow( 10, uiDigitWeight );  // 10 to the power of uiDigitWeight

      //------------------------------------------------------------------
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:
            lParamIndex_Edited += uiDigitWeight;
            break;
         case enKEYPRESS_DOWN:
            lParamIndex_Edited -= uiDigitWeight;
            break;
         case enKEYPRESS_LEFT:
            if ( ucCursorIndex )
            {
               --ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_LEFT;
            }
            break;
         case enKEYPRESS_RIGHT:
            if ( ucCursorIndex < pstParamEditMenu->ucFieldSize_Index - 1 )
            {
               ++ucCursorIndex;
            }
            else
            {
               pstParamEditMenu->ulValue_Edited = pstParamEditMenu->ulValue_Saved;
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;
         default:
            break;
      }
      //------------------------------------------------------------------
      if(lParamIndex_Edited > pstParamEditMenu->uwParamIndex_End)
      {
         lParamIndex_Edited = pstParamEditMenu->uwParamIndex_End;
      }
      else if(lParamIndex_Edited <= pstParamEditMenu->uwParamIndex_Start)
      {
         lParamIndex_Edited = pstParamEditMenu->uwParamIndex_Start;
      }

      pstParamEditMenu->uwParamIndex_Current = (uint16_t) lParamIndex_Edited;

      pstParamEditMenu->ulValue_Edited = pstParamEditMenu->ulValue_Saved;

   }

   return enReturnValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Value( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_param_edit_menu * pstParamEditMenu )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static uint8_t ucCursorIndex;
   int64_t llCurrentParam_EditedValue;
   uint8_t ucMaxSet;
   uint8_t ucMinSet;
   if ( enFieldAction != enACTION__EDIT )
   {
      if ( enFieldAction == enACTION__ENTER_FROM_LEFT )
      {
         ucCursorIndex = 0;
      }
      else if ( enFieldAction == enACTION__ENTER_FROM_RIGHT )
      {
         ucCursorIndex = pstParamEditMenu->ucFieldSize_Value - 1;
      }
   }
   else  // enACTION__EDIT
   {
	   //Check to see if there is a max or min set
	   ucMaxSet = (pstParamEditMenu->ulValue_Max) ? 1 : 0;
	   ucMinSet = (pstParamEditMenu->ulValue_Min) ? 1 : 0;
	   //------------------------------------------------------------------
      llCurrentParam_EditedValue = pstParamEditMenu->ulValue_Edited;
      //------------------------------------------------------------------
      int32_t uiDigitWeight = (pstParamEditMenu->ucFieldSize_Value - 1) - ucCursorIndex;

      if(  ( (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_Start_Time)
          || (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_End_Time) )
          && (uiDigitWeight > 2) )
       {
         uiDigitWeight = uiDigitWeight - 1;
       }

       // else if(  ( (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_Start_Time)
       //    || (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_End_Time) )
       //    && (uiDigitWeight < 2) )
       // {
       //   uiDigitWeight = uiDigitWeight - 1;
       // }

      uiDigitWeight = pow( 10, uiDigitWeight );  // 10 to the power of uiDigitWeight


      if( (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_Start_Time)
       || (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_End_Time))
      {
         if( (( pstParamEditMenu->ucFieldSize_Value -1) - ucCursorIndex) == 2)
         {
            uiDigitWeight = 0;
         }
      } 

      if(((pstParamEditMenu->uwParamIndex_Current == enPARAM8__P1_LevelingDistance_5mm)
            		   || (pstParamEditMenu->uwParamIndex_Current == enPARAM8__P2_LevelingDistance_5mm)
            		   || (pstParamEditMenu->uwParamIndex_Current == enPARAM8__P3_LevelingDistance_5mm)
            		   || (pstParamEditMenu->uwParamIndex_Current == enPARAM8__P4_LevelingDistance_5mm))
          	  	   	   && uiDigitWeight > 1)
	  {
		  uiDigitWeight /= 2;
	  }

      //------------------------------------------------------------------
      pstParamEditMenu->ucCursorColumn = START_CURSOR_POS_VALUE + ucCursorIndex;

      //------------------------------------------------------------------
      switch ( enKeypress )
      {
         case enKEYPRESS_UP:

        	 llCurrentParam_EditedValue += uiDigitWeight;

          if( (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_Start_Time)
             || (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_End_Time))
          {

            uint8_t ucMinutes = llCurrentParam_EditedValue % 100;

            if( ucMinutes > 59)
            {
               llCurrentParam_EditedValue = llCurrentParam_EditedValue - ucMinutes + 100;
            }
          }

			 if( ucMaxSet && llCurrentParam_EditedValue > pstParamEditMenu->ulValue_Max)
			 {
				llCurrentParam_EditedValue = pstParamEditMenu->ulValue_Max;
			 }

            break;

         case enKEYPRESS_DOWN:
        	 llCurrentParam_EditedValue -= uiDigitWeight;

          uint8_t ucMinutes = llCurrentParam_EditedValue % 100;


         if( (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_Start_Time)
             || (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_End_Time))
         {
            if( ucMinutes > 59)
            {
               llCurrentParam_EditedValue = llCurrentParam_EditedValue - ucMinutes + 100;
            }
         }
      
			 if(ucMinSet)
			 {

				 if( llCurrentParam_EditedValue < pstParamEditMenu->ulValue_Min)
				 {
					 llCurrentParam_EditedValue = pstParamEditMenu->ulValue_Min;
				 }
			 }

            break;

         case enKEYPRESS_LEFT:
            if ( ucCursorIndex )
            {
               --ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_LEFT;
            }
            break;

         case enKEYPRESS_RIGHT:
            if ( ucCursorIndex < (pstParamEditMenu->ucFieldSize_Value - 1) )
            {
               ++ucCursorIndex;
            }
            else
            {
               enReturnValue = enACTION__EXIT_TO_RIGHT;
            }
            break;

         default:
            break;
      }

      if(llCurrentParam_EditedValue > pstParamEditMenu->ulValue_Limit)
      {
         llCurrentParam_EditedValue = pstParamEditMenu->ulValue_Limit;
      }
      else if(llCurrentParam_EditedValue < 0)
      {
         //llCurrentParam_EditedValue = pstParamEditMenu->ulValue_Limit;
    	  llCurrentParam_EditedValue = 0;
      }
      pstParamEditMenu->ulValue_Edited = (uint32_t) llCurrentParam_EditedValue;
   }

   return enReturnValue;
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void SaveParameter( struct st_param_edit_menu * pstParamEditMenu )
{
   uint16_t ucIndex = pstParamEditMenu->uwParamIndex_Current;
   switch(pstParamEditMenu->enParamType)
   {
      case enPARAM_BLOCK_TYPE__UINT32:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_32Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_32Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT24:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_24Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_24Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT16:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_16Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_16Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
      case enPARAM_BLOCK_TYPE__UINT8:

         pstParamEditMenu->ulValue_Saved = Param_ReadValue_8Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
        	 Param_WriteValue_8Bit( ucIndex,pstParamEditMenu->ulValue_Edited);
         }
         break;
      default:
      case enPARAM_BLOCK_TYPE__BIT:
         pstParamEditMenu->ulValue_Saved = Param_ReadValue_1Bit( ucIndex );
         if(pstParamEditMenu->ulValue_Saved != pstParamEditMenu->ulValue_Edited)
         {
            Param_WriteValue_1Bit( ucIndex, pstParamEditMenu->ulValue_Edited );
         }
         break;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static enum en_field_action Edit_Save( enum en_keypresses enKeypress, enum en_field_action enFieldAction, struct st_param_edit_menu * pstParamEditMenu )
{
   enum en_field_action enReturnValue = enACTION__NONE;
   static enum en_save_state enSaveState = SAVE_STATE__SAVED;

   pstParamEditMenu->ucCursorColumn = 19;
   //------------------------------------------
   if ( enFieldAction == enACTION__EDIT )
   {
      if ( enKeypress == enKEYPRESS_ENTER )
      {
         enSaveState = SAVE_STATE__SAVING;
      }
      else if ( enKeypress == enKEYPRESS_LEFT )
      {
         enSaveState = SAVE_STATE__SAVED;
         enReturnValue = enACTION__EXIT_TO_LEFT;
      }
   }

   //--------------------------------
   switch(enSaveState)
   {
      case SAVE_STATE__CLEARING:
         pstParamEditMenu->bSaving = 1;
         if(!pstParamEditMenu->ulValue_Saved)
         {
            enSaveState = SAVE_STATE__SAVING;
         }
         break;
      case SAVE_STATE__SAVING:
         SaveParameter(pstParamEditMenu);
         pstParamEditMenu->bSaving = 1;
         if(pstParamEditMenu->ulValue_Saved == pstParamEditMenu->ulValue_Edited)
         {
            enReturnValue = enACTION__EXIT_TO_LEFT;
            pstParamEditMenu->bSaving = 0;
            enSaveState = SAVE_STATE__SAVED;
         }
         break;
      case SAVE_STATE__SAVED:
         pstParamEditMenu->bSaving = 0;
         break;
      default:
         enSaveState = SAVE_STATE__SAVED;
         break;
   }
   return enReturnValue;
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void Edit_Parameter_Range(struct st_param_edit_menu * pstParamEditMenu)
{
   ValidateParameter(pstParamEditMenu);
   if ( pstParamEditMenu->ucFieldIndex == enFIELD__NONE )
   {
      pstParamEditMenu->ulValue_Edited = pstParamEditMenu->ulValue_Saved;
      pstParamEditMenu->bSaving = 0;
      pstParamEditMenu->bInvalid = 0;
      pstParamEditMenu->ucFieldIndex = enFIELD__INDEX;

      Edit_Index( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );
   }
   else
   {
      enum en_field_action enFieldAction;

      enum en_keypresses enKeypress = Button_GetKeypress();
      //------------------------------------------------------------------
      switch ( pstParamEditMenu->ucFieldIndex )
      {
         case enFIELD__INDEX:
            enFieldAction = Edit_Index( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               PrevScreen();
               pstParamEditMenu->ucFieldIndex = enFIELD__NONE;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__VALUE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__VALUE: //function

            enFieldAction = Edit_Value( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Index( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT, pstParamEditMenu);

               pstParamEditMenu->ucFieldIndex = enFIELD__INDEX;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Save( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__SAVE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__SAVE:

            enFieldAction = Edit_Save( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__VALUE;
            }
            break;

         default:
            break;
      }
      Update_Screen(pstParamEditMenu);
   }
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void Edit_Parameter_Single(struct st_param_edit_menu * pstParamEditMenu)
{
   ValidateParameter(pstParamEditMenu);
   if ( pstParamEditMenu->ucFieldIndex == enFIELD__NONE )
   {
      pstParamEditMenu->ulValue_Edited = pstParamEditMenu->ulValue_Saved;
      pstParamEditMenu->bSaving = 0;
      pstParamEditMenu->bInvalid = 0;
      pstParamEditMenu->ucFieldIndex = enFIELD__VALUE;
      Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );
   }
   else
   {
      if(    (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_Start_Time)
         ||  (pstParamEditMenu->uwParamIndex_Start == enPARAM24__Sabbath_End_Time) )
      {
         pstParamEditMenu->ucFieldSize_Value = 5;
      }
      enum en_field_action enFieldAction;

      enum en_keypresses enKeypress = Button_GetKeypress();
      //------------------------------------------------------------------
      switch ( pstParamEditMenu->ucFieldIndex )
      {
         case enFIELD__VALUE:

            enFieldAction = Edit_Value( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               PrevScreen();
               pstParamEditMenu->ucFieldIndex = enFIELD__NONE;
            }
            else if ( enFieldAction == enACTION__EXIT_TO_RIGHT )
            {
               enFieldAction = Edit_Save( enKEYPRESS_NONE, enACTION__ENTER_FROM_LEFT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__SAVE;
            }
            break;

         //------------------------------------------------------------------
         case enFIELD__SAVE:

            enFieldAction = Edit_Save( enKeypress, enACTION__EDIT, pstParamEditMenu );

            if ( enFieldAction == enACTION__EXIT_TO_LEFT )
            {
               enFieldAction = Edit_Value( enKEYPRESS_NONE, enACTION__ENTER_FROM_RIGHT, pstParamEditMenu );

               pstParamEditMenu->ucFieldIndex = enFIELD__VALUE;
            }
            break;

         default:
            break;
      }
      Update_Screen(pstParamEditMenu);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void GetIndexFieldSize(  struct st_param_edit_menu * pstParamEditMenu  )
{
   uint16_t uwRangeOfIndex = pstParamEditMenu->uwParamIndex_End - pstParamEditMenu->uwParamIndex_Start;
   if(uwRangeOfIndex < 1)
   {
      pstParamEditMenu->ucFieldSize_Index = 0;
   }
   else if(uwRangeOfIndex < 10)
   {
      pstParamEditMenu->ucFieldSize_Index = 1;
   }
   else if(uwRangeOfIndex < 100)
   {
      pstParamEditMenu->ucFieldSize_Index = 2;
   }
   else//Accept none higher
   {
      pstParamEditMenu->ucFieldSize_Index = 3;
   }
}
/*-----------------------------------------------------------------------------
In decimal
 -----------------------------------------------------------------------------*/
static void GetValueFieldSize(  struct st_param_edit_menu * pstParamEditMenu  )
{
   switch(pstParamEditMenu->enParamType)
   {
      case enPARAM_BLOCK_TYPE__UINT32:
         pstParamEditMenu->ucFieldSize_Value = 10;
         break;
      case enPARAM_BLOCK_TYPE__UINT24:
         pstParamEditMenu->ucFieldSize_Value = 8;
         break;
      case enPARAM_BLOCK_TYPE__UINT16:
         pstParamEditMenu->ucFieldSize_Value = 5;
         break;
      case enPARAM_BLOCK_TYPE__UINT8:
         pstParamEditMenu->ucFieldSize_Value = 3;
         break;
      case enPARAM_BLOCK_TYPE__BIT:
         pstParamEditMenu->ucFieldSize_Value = 1;
         break;
      default:
         pstParamEditMenu->ucFieldSize_Value = 1;
         break;
   }
}
/*-----------------------------------------------------------------------------
If this is a new menu, reset local values
 -----------------------------------------------------------------------------*/
static void EnterNewMenu(  struct st_param_edit_menu * pstParamEditMenu  )
{
   GetIndexFieldSize(pstParamEditMenu);
   GetValueFieldSize(pstParamEditMenu);
   pstParamEditMenu->bInvalid = 0;
   pstParamEditMenu->bSaving = 0;
   pstParamEditMenu->ucCursorColumn = 0;
   pstParamEditMenu->uwParamIndex_Current = pstParamEditMenu->uwParamIndex_Start;
   pstParamEditMenu->ucFieldIndex = enFIELD__NONE;
   gbEnteredMenu = 1;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UI_ParamEditSceenTemplate( struct st_param_edit_menu * pstParamEditMenu )
{
   static uint16_t uwLastParamIndex = 0xFFFF;
   static uint8_t ucLastParamType = 0xFF;
   if(pstParamEditMenu->uwParamIndex_Start != uwLastParamIndex
   || pstParamEditMenu->enParamType != ucLastParamType )
   {
      EnterNewMenu(pstParamEditMenu);
      uwLastParamIndex = pstParamEditMenu->uwParamIndex_Start;
      ucLastParamType = pstParamEditMenu->enParamType;
   }
   //---------------------------------------

   if(pstParamEditMenu->ucFieldSize_Index)
   {
      Edit_Parameter_Range(pstParamEditMenu);
   }
   else
   {
      Edit_Parameter_Single(pstParamEditMenu);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UI_SetFlagScreenTemplate( struct st_set_flag_menu * pstSetFlagMenu )
{
//TODO for defaulting,
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
