/******************************************************************************
 *
 * @file     ui_ffs_about.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <stdint.h>
#include <string.h>
#include <time.h>
#include "alarm_app.h"
#include "alarmLog.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_PopUp_Alarms( void );
static void UI_FFS_ActiveAlarms( void );
static void UI_FFS_LoggedAlarms( void );
static void UI_FFS_ClearAlarms( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_ui_screen__freeform gstFreeFormScreen_PopUpAlarms =
{
   .pfnDraw = UI_FFS_PopUp_Alarms,
};
static struct st_ui_screen__freeform gstFFS_ActiveAlarms =
{
   .pfnDraw = UI_FFS_ActiveAlarms,
};
static struct st_ui_screen__freeform gstFFS_LoggedAlarms =
{
   .pfnDraw = UI_FFS_LoggedAlarms,
};
static struct st_ui_screen__freeform gstFFS_ClearAlarms =
{
   .pfnDraw = UI_FFS_ClearAlarms,
};

//---------
static struct st_ui_menu_item gstMI_Active =
{
   .psTitle = "Active Alarms",
   .pstUGS_Next = &gstUGS_ActiveAlarms,
};
static struct st_ui_menu_item gstMI_Logged =
{
   .psTitle = "Logged Alarms",
   .pstUGS_Next = &gstUGS_LoggedAlarms,
};
static struct st_ui_menu_item gstMI_Clear =
{
   .psTitle = "Clear Log",
   .pstUGS_Next = &gstUGS_ClearAlarms,
};

static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_Active,
   &gstMI_Logged,
   &gstMI_Clear,
};

static struct st_ui_screen__menu gstMenu =
{
   .psTitle = "Alarms",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_PopUpAlarms =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFreeFormScreen_PopUpAlarms,
   .pstUGS_Previous = &gstUGS_HomeScreen,
};
struct st_ui_generic_screen gstUGS_ActiveAlarms =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ActiveAlarms,
};
struct st_ui_generic_screen gstUGS_LoggedAlarms =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_LoggedAlarms,
};
struct st_ui_generic_screen gstUGS_ClearAlarms =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ClearAlarms,
};

struct st_ui_generic_screen gstUGS_Menu_Alarms =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu,
};

//----------------------------------------------------------------------------


/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MAX_DETAILS_SCROLLY   (1)
#define TIME_TO_CLEAR_LOG_1MS    (3000)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Print_Details( en_ddm_alarms eAlarmNum, uint32_t uiTimeStamp )
{
   char * psTime = ctime(&uiTimeStamp);

   LCD_Char_Clear();
   LCD_Char_GotoXY(0,0);
   for(uint8_t ucRow = 0; ucRow < 4; ucRow++)
   {
      uint8_t ucIndex = ucRow;
      switch( ucIndex )
      {
         case 0:
            PrintAlarmString(eAlarmNum);
            break;

         case 1:
            LCD_Char_WriteString("Num:");
            LCD_Char_WriteInteger_MinLength(eAlarmNum, 1);
            break;

         case 2:
            LCD_Char_WriteString("Time:");
            PrintMovingText(psTime);
            break;

         default:
            break;
      }
      LCD_Char_GoToStartOfNextRow();
      LCD_Char_AdvanceX(1);
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_PopUp_Alarms( void )
{
   en_ddm_alarms eAlarm = DDM_ALM__NONE;
   en_ddm_nodes eNode = NUM_DDM_NODES;
   for( uint8_t i = 0; i < NUM_DDM_NODES; i++ )
   {
      if( GetAlarm_ByNode(i) != DDM_ALM__NONE )
      {
         eAlarm = GetAlarm_ByNode(i);
         eNode = i;
         break;
      }
   }

   enum en_keypresses enKeypress = Button_GetKeypress();
   if( ( enKeypress != enKEYPRESS_NONE )
    || ( eAlarm == DDM_ALM__NONE) )
   {
      PrevScreen();
   }
   else
   {
      en_ddm_alarms eAlarmNum = GetAlarm_ByNode( eNode );
      uint32_t uiTimeStamp = GetAlarm_TimeStamp( eNode );
      Print_Details(eAlarmNum, uiTimeStamp);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_ActiveAlarms( void )
{
   static en_ddm_nodes eNode; /* Marker for main node selection screen */
   static uint8_t bDetailPage;
   int8_t iEditValue = eNode;
   enum en_keypresses enKeypress = Button_GetKeypress();
   switch( enKeypress )
   {
      case enKEYPRESS_UP:
         iEditValue--;
         break;
      case enKEYPRESS_DOWN:
         iEditValue++;
         break;
      case enKEYPRESS_LEFT:
         if(bDetailPage)
         {
            bDetailPage = 0;
         }
         else
         {
            PrevScreen();
         }
         break;
      case enKEYPRESS_RIGHT:
         bDetailPage = 1;
         break;
      default: break;
   }

   if( iEditValue >= NUM_DDM_NODES )
   {
      iEditValue = NUM_DDM_NODES-1;
   }
   else if( iEditValue < 0 )
   {
      iEditValue = 0;
   }

   eNode = iEditValue;

   if( bDetailPage )
   {
      en_ddm_alarms eAlarmNum = GetAlarm_ByNode( eNode );
      uint32_t uiTimeStamp = GetAlarm_TimeStamp( eNode );
      Print_Details(eAlarmNum, uiTimeStamp);
   }
   else /* Alarm node selection menu */
   {
      LCD_Char_Clear();
      LCD_Char_GotoXY(0,0);
      LCD_Char_WriteString("Active Alarms");

      for( uint8_t i = 0; i < NUM_DDM_NODES; i++ )
      {
         LCD_Char_GotoXY(1,i+1);
         switch(i)
         {
            case DDM_NODE__A:
               LCD_Char_WriteString("DDMA: ");
               break;
            case DDM_NODE__B:
               LCD_Char_WriteString("DDMB: ");
               break;
            default: break;
         }
         PrintAlarmString(GetAlarm_ByNode(i));
      }
      LCD_Char_GotoXY(0,1+eNode);
      LCD_Char_WriteString("*");
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define NUM_DISPLAYED_LOG_LINES     (3)
static void UI_FFS_LoggedAlarms( void )
{
   static uint8_t ucScrollX;//details screen vs list
   static uint8_t ucScrollY;//up/dn on list
   static uint8_t ucCursorY;//ui pos indicator

   /* UI Navigation */
   enum en_keypresses enKeypress = Button_GetKeypress();
   if ( enKeypress == enKEYPRESS_LEFT )
   {
      if(ucScrollX)
      {
         ucScrollX--;
      }
      else
      {
         ucScrollY = 0;
         PrevScreen();
      }
   }
   else if( enKeypress == enKEYPRESS_RIGHT )
   {
      if(ucScrollX < 1)
      {
         ucScrollX++;
      }
   }
   else if( enKeypress == enKEYPRESS_DOWN )
   {
      if(!ucScrollX)
      {
         if( ucCursorY == 2)
         {
            if(ucScrollY < (NUM_ALARM_LOG_ELEMENTS - NUM_DISPLAYED_LOG_LINES))
            {
               ucScrollY++;
            }
         }
         else
         {
            ucCursorY++;
         }
      }

   }
   else if( enKeypress == enKEYPRESS_UP )
   {
      if(!ucScrollX)
      {
         if( !ucCursorY )
         {
            if(ucScrollY)
            {
               ucScrollY--;
            }
         }
         else
         {
            ucCursorY--;
         }
      }
   }

   /* Print screen */
   if(ucScrollX)//Details screen
   {
      en_ddm_alarms eAlarmNum = AlarmLog_GetAlarm(ucScrollY);
      uint32_t uiTimeStamp = AlarmLog_GetTimestamp(ucScrollY);
      Print_Details(eAlarmNum, uiTimeStamp);
   }
   else
   {
      LCD_Char_Clear();
      LCD_Char_GotoXY( 0, 0 );
      LCD_Char_WriteString("ALARM LOG");
      for(uint8_t i = 0; i < NUM_DISPLAYED_LOG_LINES; i++)
      {
         uint8_t ucLogIndex = ucScrollY + i;
         en_ddm_alarms eAlarmNum = AlarmLog_GetAlarm(ucLogIndex);
         LCD_Char_GotoXY( 1, i+1 );
         LCD_Char_WriteInteger_MinLength(ucLogIndex+1,1);
         LCD_Char_WriteString(".");
         PrintAlarmString(eAlarmNum);
      }
      LCD_Char_GotoXY(0,1+ucCursorY);
      LCD_Char_WriteString("*");
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_ClearAlarms( void )
{
   static uint16_t uwClearingCounter_ms;
   static uint8_t ucCursor = 0;
   static uint8_t bFinished = 1;
   const uint8_t ucFields = 2;

   enum en_keypresses enKeypress = Button_GetKeypress();
   switch(enKeypress)
   {
      case enKEYPRESS_RIGHT:
         if(ucCursor < ucFields-1)
         {
            ucCursor++;
         }
         break;
      case enKEYPRESS_LEFT:
         if( ucCursor )
         {
            ucCursor--;
         }
         else
         {
            bFinished = 1;
            uwClearingCounter_ms = 0;
            PrevScreen();
         }
         break;
      case enKEYPRESS_ENTER:
         if(ucCursor)
         {
            bFinished = 0;
            uwClearingCounter_ms = 0;
            AlarmLog_ClearLog();
         }
         else
         {
            PrevScreen();
         }
         break;
      default:
         break;
   }

   if(bFinished)
   {
      LCD_Char_Clear();
      LCD_Char_GotoXY( 0, 0 );
      LCD_Char_WriteStringUpper("Clear Alarm Log?");
      LCD_Char_GotoXY(4,2);
      LCD_Char_WriteStringUpper("NO");
      LCD_Char_GotoXY(12,2);
      LCD_Char_WriteStringUpper("YES");
      if(!ucCursor)
      {
         LCD_Char_GotoXY(5,3);
      }
      else
      {
         LCD_Char_GotoXY(12,3);
      }
      LCD_Char_WriteString("*");
   }
   else
   {
      LCD_Char_Clear();
      LCD_Char_GotoXY(4,2);
      LCD_Char_WriteStringUpper("Clearing");

      if(uwClearingCounter_ms < TIME_TO_CLEAR_LOG_1MS)
      {
         uwClearingCounter_ms += MOD_RUN_PERIOD_UI_1MS;
      }
      else
      {
         uwClearingCounter_ms = 0;
         bFinished = 1;
         ucCursor = 0;
         PrevScreen();
      }
   }
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
