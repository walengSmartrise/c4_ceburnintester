/******************************************************************************
 *
 * @file     ui_ffs_misc.c
 * @brief    Setup Misc UI pages
 * @version  V1.00
 * @date     13, Feb 2018
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "ui.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include "sys.h"
#include "sru.h"
#include <math.h>
#include "param.h"
#include "config_System.h"
#include "totalCostData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Misc_DefaultAll( void );
static void UI_FFS_DispatchType( void );

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_Misc_DefaultAll =
{
   .pfnDraw = UI_FFS_Misc_DefaultAll,
};
static struct st_ui_screen__freeform gstFFS_DispatchType =
{
   .pfnDraw = UI_FFS_DispatchType,
};

//---------------------------------------------------------------
//static struct st_ui_menu_item gstMI_Misc_DefaultAll =
//{
//   .psTitle = "Default All",
//   .pstUGS_Next = &gstUGS_Misc_DefaultAll,
//};
static struct st_ui_menu_item gstMI_DispatchType =
{
   .psTitle = "Dispatch Type",
   .pstUGS_Next = &gstUGS_DispatchType,
};

static struct st_ui_menu_item * gastMenuItems_Misc[] =
{
   &gstMI_DispatchType,
//   &gstMI_Misc_DefaultAll,
};
static struct st_ui_screen__menu gstMenu_Misc =
{
   .psTitle = "Misc",
   .pastMenuItems = &gastMenuItems_Misc,
   .ucNumItems = sizeof(gastMenuItems_Misc) / sizeof(gastMenuItems_Misc[ 0 ]),
};

//-----------------------------------------------------
static uint16_t uwDefaultCountdown_30ms;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_DispatchType =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DispatchType,
};
struct st_ui_generic_screen gstUGS_Misc_DefaultAll =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_Misc_DefaultAll,
};
struct st_ui_generic_screen gstUGS_Menu_Setup_Misc =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu_Misc,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void Update_UIRequest_DefaultCommand( void )
{
   if(GetUIRequest_DefaultCommand())
   {
      if(uwDefaultCountdown_30ms)
      {
         uwDefaultCountdown_30ms--;
      }
      else
      {
         SetUIRequest_DefaultCommand( DEFAULT_CMD__NONE );
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define DEFAULTING_IN_PROGRESS_DISPLAY_TIME_30MS   (2000)
static void UI_FFS_Misc_DefaultAll( void )
{
   static uint8_t ucCursor = 0;
   const uint8_t ucFields = 2;
   enum en_keypresses enKeypress = Button_GetKeypress();


   switch(enKeypress)
   {
   case enKEYPRESS_RIGHT:
      if(ucCursor < ucFields-1)
      {
         ucCursor++;
      }
      break;
   case enKEYPRESS_LEFT:
      if( ucCursor )
      {
         ucCursor--;
      }
      else
      {
         SetUIRequest_DefaultCommand( DEFAULT_CMD__NONE );
         PrevScreen();
      }
      break;
   case enKEYPRESS_ENTER:
      if(!ucCursor)
      {
         uwDefaultCountdown_30ms = DEFAULTING_IN_PROGRESS_DISPLAY_TIME_30MS;
         SetUIRequest_DefaultCommand( DEFAULT_CMD__ALL );
      }
      else
      {
         SetUIRequest_DefaultCommand( DEFAULT_CMD__NONE );
         PrevScreen();
      }
      break;
   default:
      break;
   }

   if( !GetUIRequest_DefaultCommand() )
   {
      LCD_Char_Clear();
      LCD_Char_GotoXY( 0, 0 );
      LCD_Char_WriteStringUpper("Default All?");
      LCD_Char_GotoXY(4,2);
      LCD_Char_WriteStringUpper("YES");
      LCD_Char_GotoXY(12,2);
      LCD_Char_WriteStringUpper("NO");
      if(!ucCursor)
      {
         LCD_Char_GotoXY(5,3);
      }
      else
      {
         LCD_Char_GotoXY(12,3);
      }
      LCD_Char_WriteString("*");
   }
   // If the state is not 0 then system is being updated
   //else if( GetDefaultAllState() || GetUIRequest_DefaultCommand()  )
   else if(GetDefaultAllState() && uwDefaultCountdown_30ms)
   {
      LCD_Char_Clear();
      LCD_Char_GotoXY( 0, 0 );
      LCD_Char_WriteStringUpper("Defaulting Params");

      LCD_Char_GotoXY(2,2);

      switch(GetDefaultAllState() )
      {
          case DEFAULT_ALL_ONE_BIT:
              LCD_Char_WriteStringUpper("Defaulting 1 Bits");
              break;
          case DEFAULT_ALL_EIGHT_BIT:
              LCD_Char_WriteStringUpper("Defaulting 8 Bits");
              break;
          case DEFAULT_ALL_SIXTEEN_BIT:
              LCD_Char_WriteStringUpper("Defaulting 16 Bits");
              break;
          case DEFAULT_ALL_TWENTYFOUR_BIT:
              LCD_Char_WriteStringUpper("Defaulting 24 Bits");
              break;
          case DEFAULT_ALL_THIRTYTWO_BIT:
              LCD_Char_WriteStringUpper("Defaulting 32 Bits");
              break;
          default:

              // do nothing
              break;
      }

   }
   else if( (GetDefaultAllState() == DEFAULT_ALL_IDLE) )
   {
           ucCursor = 0;
           LCD_Char_GotoXY(0,2);
           LCD_Char_WriteStringUpper("Parameters Defaulted");
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_DispatchType( void )
{
   static char *(pasTypeArray[]) =
   {
         "BASIC",
         "TOTAL COST",
         "TIME TO DEST",
         "TIME TO PICKUP",
   };
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = DDM_PARAM8__DispatchingType;
   stParamEdit.uwParamIndex_End = DDM_PARAM8__DispatchingType;
   stParamEdit.psTitle = "Dispatch Type";
   stParamEdit.psUnit = "";
   stParamEdit.ucaPTRChoiceList = &pasTypeArray;
   stParamEdit.ucArraySize = NUM_DDM_DISPATCH_TYPE;
   stParamEdit.ulValue_Max = NUM_DDM_DISPATCH_TYPE-1;
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
