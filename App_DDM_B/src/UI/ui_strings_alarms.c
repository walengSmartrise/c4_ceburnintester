/******************************************************************************
 *
 * @file     ui_strings_faults.c
 * @brief    Fault string access
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "ui.h"
#include "lcd.h"
#include "mod.h"
#include <stdint.h>
#include <string.h>

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/


static char const * const pasAlarmStrings[NUM_DDM_ALARMS] =
{
"None",//DDM_ALM__NONE,
"POR Rst B",//DDM_ALM__RESET_POR_B,
"WDT Rst B",//DDM_ALM__RESET_WDT_B,
"BOD Rst B",//DDM_ALM__RESET_BOD_B,
"POR Rst A",//DDM_ALM__RESET_POR_A,
"WDT Rst A",//DDM_ALM__RESET_WDT_A,
"BOD Rst A",//DDM_ALM__RESET_BOD_A,
"CAN Rst B1",//DDM_ALM__CAN_RESET_B1,
"CAN Rst B2",//DDM_ALM__CAN_RESET_B2,
"A Offline",//DDM_ALM__OFFLINE_A,
"B Offline",//DDM_ALM__OFFLINE_B,
"Exp Offline",//DDM_ALM__OFFLINE_EXP,
"Group Offline",//DDM_ALM__OFFLINE_GROUP,
"Duplicate",//DDM_ALM__DUPLICATE,
"CAN Rst A1",//DDM_ALM__CAN_RESET_A1,
"CAN Rst A2",//DDM_ALM__CAN_RESET_A2,
"Param Queue A",//DDM_ALM__PARAM_QUEUE_A,
"Param Queue B",//DDM_ALM__PARAM_QUEUE_B,
"Param Sync A",//DDM_ALM__PARAM_SYNC_A,
"CPU Stop A",//DDM_ALM__CPU_STOP_A,
"CPU Stop B",//DDM_ALM__CPU_STOP_B,
"WDT DISA A",//DDM_ALM__WDT_DISABLED_A,
"WDT DISA B",//DDM_ALM__WDT_DISABLED_B,

"EXP01 Offline",//DDM_ALM__EXP_01,
"EXP02 Offline",//DDM_ALM__EXP_02,
"EXP03 Offline",//DDM_ALM__EXP_03,
"EXP04 Offline",//DDM_ALM__EXP_04,
"EXP05 Offline",//DDM_ALM__EXP_05,
"EXP06 Offline",//DDM_ALM__EXP_06,
"EXP07 Offline",//DDM_ALM__EXP_07,
"EXP08 Offline",//DDM_ALM__EXP_08,
"EXP09 Offline",//DDM_ALM__EXP_09,
"EXP10 Offline",//DDM_ALM__EXP_10,
"EXP11 Offline",//DDM_ALM__EXP_11,
"EXP12 Offline",//DDM_ALM__EXP_12,
"EXP13 Offline",//DDM_ALM__EXP_13,
"EXP14 Offline",//DDM_ALM__EXP_14,
"EXP15 Offline",//DDM_ALM__EXP_15,
"EXP16 Offline",//DDM_ALM__EXP_16,
"EXP17 Offline",//DDM_ALM__EXP_17,
"EXP18 Offline",//DDM_ALM__EXP_18,
"EXP19 Offline",//DDM_ALM__EXP_19,
"EXP20 Offline",//DDM_ALM__EXP_20,
"EXP21 Offline",//DDM_ALM__EXP_21,
"EXP22 Offline",//DDM_ALM__EXP_22,
"EXP23 Offline",//DDM_ALM__EXP_23,
"EXP24 Offline",//DDM_ALM__EXP_24,
"EXP25 Offline",//DDM_ALM__EXP_25,
"EXP26 Offline",//DDM_ALM__EXP_26,
"EXP27 Offline",//DDM_ALM__EXP_27,
"EXP28 Offline",//DDM_ALM__EXP_28,
"EXP29 Offline",//DDM_ALM__EXP_29,
"EXP30 Offline",//DDM_ALM__EXP_30,
"EXP31 Offline",//DDM_ALM__EXP_31,
"EXP32 Offline",//DDM_ALM__EXP_32,
"EXP33 Offline",//DDM_ALM__EXP_33,
"EXP34 Offline",//DDM_ALM__EXP_34,
"EXP35 Offline",//DDM_ALM__EXP_35,
"EXP36 Offline",//DDM_ALM__EXP_36,
"EXP37 Offline",//DDM_ALM__EXP_37,
"EXP38 Offline",//DDM_ALM__EXP_38,
"EXP39 Offline",//DDM_ALM__EXP_39,
"EXP40 Offline",//DDM_ALM__EXP_40,

"Panel01 Offline",//DDM_ALM__PANEL_01,
"Panel02 Offline",//DDM_ALM__PANEL_02,
"Panel03 Offline",//DDM_ALM__PANEL_03,
"Panel04 Offline",//DDM_ALM__PANEL_04,
"Panel05 Offline",//DDM_ALM__PANEL_05,
"Panel06 Offline",//DDM_ALM__PANEL_06,
"Panel07 Offline",//DDM_ALM__PANEL_07,
"Panel08 Offline",//DDM_ALM__PANEL_08,
"Panel09 Offline",//DDM_ALM__PANEL_09,

"Panel10 Offline",//DDM_ALM__PANEL_10,
"Panel11 Offline",//DDM_ALM__PANEL_11,
"Panel12 Offline",//DDM_ALM__PANEL_12,
"Panel13 Offline",//DDM_ALM__PANEL_13,
"Panel14 Offline",//DDM_ALM__PANEL_14,
"Panel15 Offline",//DDM_ALM__PANEL_15,
"Panel16 Offline",//DDM_ALM__PANEL_16,
"Panel17 Offline",//DDM_ALM__PANEL_17,
"Panel18 Offline",//DDM_ALM__PANEL_18,
"Panel19 Offline",//DDM_ALM__PANEL_19,

"Panel20 Offline",//DDM_ALM__PANEL_20,
"Panel21 Offline",//DDM_ALM__PANEL_21,
"Panel22 Offline",//DDM_ALM__PANEL_22,
"Panel23 Offline",//DDM_ALM__PANEL_23,
"Panel24 Offline",//DDM_ALM__PANEL_24,
"Panel25 Offline",//DDM_ALM__PANEL_25,
"Panel26 Offline",//DDM_ALM__PANEL_26,
"Panel27 Offline",//DDM_ALM__PANEL_27,
"Panel28 Offline",//DDM_ALM__PANEL_28,
"Panel29 Offline",//DDM_ALM__PANEL_29,

"Panel30 Offline",//DDM_ALM__PANEL_30,
"Panel31 Offline",//DDM_ALM__PANEL_31,
"Panel32 Offline",//DDM_ALM__PANEL_32,
"Panel33 Offline",//DDM_ALM__PANEL_33,
"Panel34 Offline",//DDM_ALM__PANEL_34,
"Panel35 Offline",//DDM_ALM__PANEL_35,
"Panel36 Offline",//DDM_ALM__PANEL_36,
"Panel37 Offline",//DDM_ALM__PANEL_37,
"Panel38 Offline",//DDM_ALM__PANEL_38,
"Panel39 Offline",//DDM_ALM__PANEL_39,

"Panel40 Offline",//DDM_ALM__PANEL_40,
"Panel41 Offline",//DDM_ALM__PANEL_41,
"Panel42 Offline",//DDM_ALM__PANEL_42,
"Panel43 Offline",//DDM_ALM__PANEL_43,
"Panel44 Offline",//DDM_ALM__PANEL_44,
"Panel45 Offline",//DDM_ALM__PANEL_45,
"Panel46 Offline",//DDM_ALM__PANEL_46,
"Panel47 Offline",//DDM_ALM__PANEL_47,
"Panel48 Offline",//DDM_ALM__PANEL_48,
"Panel49 Offline",//DDM_ALM__PANEL_49,

"Panel50 Offline",//DDM_ALM__PANEL_50,
"Panel51 Offline",//DDM_ALM__PANEL_51,
"Panel52 Offline",//DDM_ALM__PANEL_52,
"Panel53 Offline",//DDM_ALM__PANEL_53,
"Panel54 Offline",//DDM_ALM__PANEL_54,
"Panel55 Offline",//DDM_ALM__PANEL_55,
"Panel56 Offline",//DDM_ALM__PANEL_56,
"Panel57 Offline",//DDM_ALM__PANEL_57,
"Panel58 Offline",//DDM_ALM__PANEL_58,
"Panel59 Offline",//DDM_ALM__PANEL_59,

"Panel60 Offline",//DDM_ALM__PANEL_60,
"Panel61 Offline",//DDM_ALM__PANEL_61,
"Panel62 Offline",//DDM_ALM__PANEL_62,
"Panel63 Offline",//DDM_ALM__PANEL_63,
"Panel64 Offline",//DDM_ALM__PANEL_64,
"Panel65 Offline",//DDM_ALM__PANEL_65,
"Panel66 Offline",//DDM_ALM__PANEL_66,
"Panel67 Offline",//DDM_ALM__PANEL_67,
"Panel68 Offline",//DDM_ALM__PANEL_68,
"Panel69 Offline",//DDM_ALM__PANEL_69,

"Panel70 Offline",//DDM_ALM__PANEL_70,
"Panel71 Offline",//DDM_ALM__PANEL_71,
"Panel72 Offline",//DDM_ALM__PANEL_72,
"Panel73 Offline",//DDM_ALM__PANEL_73,
"Panel74 Offline",//DDM_ALM__PANEL_74,
"Panel75 Offline",//DDM_ALM__PANEL_75,
"Panel76 Offline",//DDM_ALM__PANEL_76,
"Panel77 Offline",//DDM_ALM__PANEL_77,
"Panel78 Offline",//DDM_ALM__PANEL_78,
"Panel79 Offline",//DDM_ALM__PANEL_79,

"Panel80 Offline",//DDM_ALM__PANEL_80,
"Panel81 Offline",//DDM_ALM__PANEL_81,
"Panel82 Offline",//DDM_ALM__PANEL_82,
"Panel83 Offline",//DDM_ALM__PANEL_83,
"Panel84 Offline",//DDM_ALM__PANEL_84,
"Panel85 Offline",//DDM_ALM__PANEL_85,
"Panel86 Offline",//DDM_ALM__PANEL_86,
"Panel87 Offline",//DDM_ALM__PANEL_87,
"Panel88 Offline",//DDM_ALM__PANEL_88,
"Panel89 Offline",//DDM_ALM__PANEL_89,

"Panel90 Offline",//DDM_ALM__PANEL_90,
"Panel91 Offline",//DDM_ALM__PANEL_91,
"Panel92 Offline",//DDM_ALM__PANEL_92,
"Panel93 Offline",//DDM_ALM__PANEL_93,
"Panel94 Offline",//DDM_ALM__PANEL_94,
"Panel95 Offline",//DDM_ALM__PANEL_95,
"Panel96 Offline",//DDM_ALM__PANEL_96,

};

static char const * const psInvalidAlarmNumString = "Unknown Alarm";
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void PrintAlarmString( en_ddm_alarms eAlarmNum )
{
   if( eAlarmNum < NUM_DDM_ALARMS )
   {
      LCD_Char_WriteString( pasAlarmStrings[ eAlarmNum ]);
   }
   else
   {
      LCD_Char_WriteString( psInvalidAlarmNumString );
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

