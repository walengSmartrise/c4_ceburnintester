/******************************************************************************
 *
 * @file     ui_ffs_about.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "sru.h"
#include "mod.h"
#include <stdint.h>
#include <string.h>
#include "config.h"
#include "version.h"

#ifdef DEBUG
static const char * gpsJob = VF_JOB_NAME;
static const char * gpsCopyright = "(C) 2016 Smartrise";

#else
// Should be in the default all header file.
#define NUM_HEADER_BYTES   ((uint32_t)6)
#define FLASH_BASE_ADDRESS ( ((uint32_t)0x9000) + NUM_HEADER_BYTES )
#endif
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_About( void );

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_ui_screen__freeform gstFreeFormScreen_About =
{
   .pfnDraw = UI_FFS_About,
};

static const char * gpsVersion = SOFTWARE_RELEASE_VERSION;

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_AboutScreen =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFreeFormScreen_About,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

#ifdef DEBUG
static void Print_Job(void)
{
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper_Centered(gpsJob);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Print_Board(void)
{
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteStringUpper_Centered("DD Manager");
#if !ENABLED_REDUCED_MAX_FLOOR_SYSTEM
   LCD_Char_WriteString(" - 96 FL");
#endif
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Print_Version(void)
{
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString_Centered(gpsVersion);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Print_Copyright(void)
{
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString_Centered(gpsCopyright);
}
#else


static void Print_Job(void)
{
   uint32_t addressOffset = ( (NUM_32BIT_PARAMS*4) + (NUM_24BIT_PARAMS*3) + (NUM_16BIT_PARAMS*2) + (NUM_8BIT_PARAMS*1) + (NUM_1BIT_PARAMS*1));
   addressOffset += FLASH_BASE_ADDRESS;

   char *gpsJob = ((uint8_t *)(addressOffset));
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteStringUpper_Centered(gpsJob);
}

static void Print_CarLabel(void)
{
    uint32_t addressOffset = ( (NUM_32BIT_PARAMS*4) + (NUM_24BIT_PARAMS*3) + (NUM_16BIT_PARAMS*2) + (NUM_8BIT_PARAMS*1) + (NUM_1BIT_PARAMS*1));
    addressOffset += FLASH_BASE_ADDRESS;

    char *gpsJob = ((uint8_t *)(addressOffset));
    char *gpsCarLabel = ((uint8_t *)(addressOffset+ strlen(gpsJob) + 1 ));

   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteStringUpper_Centered(gpsCarLabel);
}

static void Print_Board(void)
{
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteStringUpper_Centered("DD Manager");
#if !ENABLED_REDUCED_MAX_FLOOR_SYSTEM
   LCD_Char_WriteString(" - 96 FL");
#endif
}



static void Print_Version(void)
{
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString_Centered(gpsVersion);
}



#endif

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_About( void )
{
   LCD_Char_Clear();

#ifdef DEBUG
   Print_Job();
   Print_Board();
   Print_Version();
   Print_Copyright();
#else
   Print_Job();
   Print_CarLabel();
   Print_Board();
   Print_Version();

#endif
   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      PrevScreen();
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
