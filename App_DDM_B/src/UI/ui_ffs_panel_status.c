/******************************************************************************
 *
 * @file     ui_ffs_assigned_calls.c
 * @brief    Kiosk Assigned Calls Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note     Displayed the calls assigned to a car on a per floor basis
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "sru.h"
#include "mod.h"
#include <stdint.h>
#include <string.h>
#include "config.h"
#include "kioskData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_PanelStatus( void );

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_PanelStatus =
{
   .pfnDraw = UI_FFS_PanelStatus,
};

//--------------------------------------
static char const * const pasOfflineStrings[ NUM_KIOSK_OFFLINE_STATES ] =
{
      "INACTIVE",
      "OFFLINE",
      "ONLINE",
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_PanelStatus =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_PanelStatus,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MAX_CURSOR_X_VALUE    (3)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

   |01234567890123456789|

   |Panel Landing 1    >|
   |L1: INACTIVE        |
   |L2: OFFLINE         |
   |L3: ONLINE          |

 -----------------------------------------------------------------------------*/
static void UpdateScreen( uint8_t ucCursorX, uint8_t ucCursorY )
{
   LCD_Char_Clear();

   /* Line 1 */
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteString("Panel Landing ");
   LCD_Char_WriteInteger_MinLength(ucCursorY+1, 1);
   if(ucCursorX)
   {
      LCD_Char_GotoXY( 18, 0 );
      LCD_Char_WriteString("<");
   }
   if(ucCursorX != (MAX_CURSOR_X_VALUE-1))
   {
      LCD_Char_GotoXY( 19, 0 );
      LCD_Char_WriteString(">");
   }

   /* Line 2/3/4 */
   for(uint8_t i = 0; i < 3; i++)
   {
      uint8_t ucLocation = ucCursorX*3 + i;
      en_kiosk_offline_states ePanelStatus = DDPanel_GetOfflineState(ucCursorY, ucLocation);
      if(ucLocation < MAX_SUPPORTED_KIOSK_LOCATIONS)
      {
         LCD_Char_GotoXY( 0, i+1 );
         LCD_Char_WriteString("L");
         LCD_Char_WriteInteger_MinLength(ucLocation+1, 1);
         LCD_Char_WriteString(": ");
         LCD_Char_WriteString(pasOfflineStrings[ePanelStatus]);
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_PanelStatus( void )
{
   static int8_t cCursorX, cCursorY;

   enum en_keypresses eKeypress = Button_GetKeypress();
   switch(eKeypress)
   {
      case enKEYPRESS_LEFT:
         if(cCursorX)
         {
            cCursorX--;
         }
         else
         {
            PrevScreen();
         }
         break;

      case enKEYPRESS_RIGHT:
         cCursorX++;
         break;

      case enKEYPRESS_UP:
         cCursorY++;
         break;

      case enKEYPRESS_DOWN:
         cCursorY--;
         break;

      default:
         break;
   }
   if( cCursorX < 0 )
   {
      cCursorX = 0;
   }
   else if( cCursorX >= MAX_CURSOR_X_VALUE )
   {
      cCursorX = MAX_CURSOR_X_VALUE-1;
   }

   if( cCursorY < 0 )
   {
      cCursorY = 0;
   }
   else if( cCursorY >= MAX_NUM_FLOORS )
   {
      cCursorY = MAX_NUM_FLOORS-1;
   }

   UpdateScreen(cCursorX, cCursorY);
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
