/******************************************************************************
 *
 * @file     ui_ffs_home.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "sru.h"
#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_Home( void );

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_ui_screen__freeform gstFreeFormScreen_Home =
{
   .pfnDraw = UI_FFS_Home,
};

static uint8_t gucScrollY = 0;

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_HomeScreen =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFreeFormScreen_Home,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Home( void )
{
   LCD_Char_Clear();
   LCD_Char_GotoXY(0, 0);
   LCD_Char_WriteString("Home Screen");
   //-----------------------------------------
   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_RIGHT )
   {
      NextScreen( &gstUGS_MainMenu );
   }
   else if(enKeypress == enKEYPRESS_UP)
   {
      if(gucScrollY)
      {
         gucScrollY--;
      }
   }
   else if(enKeypress == enKEYPRESS_DOWN)
   {
      if(gucScrollY < 9)
      {
         gucScrollY++;
      }
   }
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/


