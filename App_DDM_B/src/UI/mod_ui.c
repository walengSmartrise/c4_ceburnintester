/******************************************************************************
 *
 * @file     mod_ui.c
 * @brief
 * @version  V1.00
 * @date     23, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "buttons.h"
#include "lcd.h"
#include "sru_b.h"
#include <stdint.h>
#include "sru.h"
#include "sys.h"
#include "ui.h"
#include "alarmLog.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/


struct st_module gstMod_UI =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

uint8_t gbParamSaveInProgress;

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_Alarm_Support(struct st_ui_control *pstUI_Control)
{
   static uint8_t ucAlarm_Last = 0;

   /* If DIP 7 on MCUB is high, popup blocking enabled */
   uint8_t bPopupBlocking = SRU_Read_DIP_Switch(enSRU_DIP_A7);
   if(!bPopupBlocking)
   {
      if( gstUI_Control.pstUGS_Current != &gstUGS_PopUpAlarms)
      {
         en_alarms eAlarmNumber = 0;
         for(uint8_t i = 0; i < NUM_DDM_NODES; i++)
         {
            if(GetAlarm_ByNode(i))
            {
               eAlarmNumber = GetAlarm_ByNode(i);
               break;
            }
         }
         if(eAlarmNumber)
         {
            if( ucAlarm_Last != eAlarmNumber)
            {
               if( gbParamSaveInProgress == 0 )
               {
                 if( (gstUI_Control.pstUGS_Current == &gstUGS_HomeScreen)
                  || (gstUI_Control.pstUGS_Current == &gstUGS_PopUpAlarms) )
                 {
                    gstUGS_PopUpAlarms.pstUGS_Previous = &gstUGS_HomeScreen;
                 }
                 else
                 {
                    gstUGS_PopUpAlarms.pstUGS_Previous = gstUI_Control.pstUGS_Current;
                 }
                 gstUI_Control.pstUGS_Current = &gstUGS_PopUpAlarms;
               }
            }
         }
         ucAlarm_Last = eAlarmNumber;
      }
   }
   //---------------------------------------------------------------
   else if( gstUI_Control.pstUGS_Current == &gstUGS_PopUpAlarms)
   {
      //Gets stuck in popup screen if popups are disabled while on a popup menu
      gstUGS_PopUpAlarms.pstUGS_Previous = &gstUGS_HomeScreen;
      PrevScreen();
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 750;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_UI_1MS;
   LCD_Char_Clear();
   gbParamSaveInProgress = 0;
   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   struct st_ui_control *pstUI_Control = &gstUI_Control;
   struct st_ui_generic_screen *pstUGS_Current = pstUI_Control->pstUGS_Current;
   UI_UpdateNewPageFlag(&gstUI_Control);
   UI_UpdateMovingTextVariables(&gstUI_Control);
   Update_UIRequest_DefaultCommand();
   SetUIRequest_ViewDebugDataCommand(VDDD__NONE);
   AlarmLog_UpdateLog();
   UI_Alarm_Support(&gstUI_Control);
   //---------------------------------------------------------------
   if ( pstUGS_Current->ucType == enUI_STYPE__FREEFORM )
   {
      struct st_ui_screen__freeform *pstFreeFormScreen;
      pstFreeFormScreen = (struct st_ui_screen__freeform *) pstUGS_Current->pvReference;

      pstFreeFormScreen->pfnDraw();
   }
   else if ( pstUGS_Current->ucType == enUI_STYPE__MENU )
   {
      struct st_ui_screen__menu * pstMenuScreen;
      pstMenuScreen = (struct st_ui_screen__menu *) pstUGS_Current->pvReference;
      Draw_Menu( pstMenuScreen );
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
