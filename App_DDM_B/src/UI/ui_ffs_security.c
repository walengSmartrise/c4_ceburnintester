/******************************************************************************
 *
 * @file     ui_ffs_doors.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Setup_EnableSecurityCode( void );
static void UI_FFS_Setup_EnableSecurityDevice( void );
static void UI_FFS_Setup_SecurityCode_F( void );
static void UI_FFS_Setup_SecurityCode_R( void );

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_EnableSecurityCode =
{
   .pfnDraw = UI_FFS_Setup_EnableSecurityCode,
};
static struct st_ui_screen__freeform gstFFS_EnableSecurityDevice =
{
   .pfnDraw = UI_FFS_Setup_EnableSecurityDevice,
};
static struct st_ui_screen__freeform gstFFS_SecurityCode_F =
{
   .pfnDraw = UI_FFS_Setup_SecurityCode_F,
};
static struct st_ui_screen__freeform gstFFS_SecurityCode_R =
{
   .pfnDraw = UI_FFS_Setup_SecurityCode_R,
};

//---------------------------------------------------------------
//static struct st_ui_menu_item gstMI_EnableSecurityCode =
//{
//   .psTitle = "En. Security Code",
//   .pstUGS_Next = &gstUGS_Setup_EnableSecurityCode,
//};
static struct st_ui_menu_item gstMI_EnableSecurityDevice =
{
   .psTitle = "En. Security Device",
   .pstUGS_Next = &gstUGS_Setup_EnableSecurityDevice,
};
//static struct st_ui_menu_item gstMI_SecurityCode_F =
//{
//   .psTitle = "Security Code (F)",
//   .pstUGS_Next = &gstUGS_Setup_SecurityCode_F,
//};
//static struct st_ui_menu_item gstMI_SecurityCode_R =
//{
//   .psTitle = "Security Code (R)",
//   .pstUGS_Next = &gstUGS_Setup_SecurityCode_R,
//};
static struct st_ui_menu_item * gastMenuItems[] =
{
//   &gstMI_EnableSecurityCode,
   &gstMI_EnableSecurityDevice,
//   &gstMI_SecurityCode_F,
//   &gstMI_SecurityCode_R,
};
static struct st_ui_screen__menu gstMenu =
{
   .psTitle = "Security",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Setup_EnableSecurityCode =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_EnableSecurityCode,
};
struct st_ui_generic_screen gstUGS_Setup_EnableSecurityDevice =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_EnableSecurityDevice,
};
struct st_ui_generic_screen gstUGS_Setup_SecurityCode_F =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SecurityCode_F,
};
struct st_ui_generic_screen gstUGS_Setup_SecurityCode_R =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_SecurityCode_R,
};

//-----------------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_Setup_Security =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_EnableSecurityCode( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = DDM_PARAM1__EnableSecurityCode;
   stParamEdit.uwParamIndex_End = DDM_PARAM1__EnableSecurityCode;
   stParamEdit.psTitle = "En. Security Code";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_EnableSecurityDevice( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = DDM_PARAM1__EnableSecurityDevice;
   stParamEdit.uwParamIndex_End = DDM_PARAM1__EnableSecurityDevice;
   stParamEdit.psTitle = "En. Security Device";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_SecurityCode_F( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT24;
   stParamEdit.uwParamIndex_Start = DDM_PARAM24__SecurityCodeF_0;
   stParamEdit.uwParamIndex_End = DDM_PARAM24__SecurityCodeF_95;
   stParamEdit.psTitle = "Security Code (F)";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_SecurityCode_R( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT24;
   stParamEdit.uwParamIndex_Start = DDM_PARAM24__SecurityCodeR_0;
   stParamEdit.uwParamIndex_End = DDM_PARAM24__SecurityCodeR_95;
   stParamEdit.psTitle = "Security Code (R)";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
