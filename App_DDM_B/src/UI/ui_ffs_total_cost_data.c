/******************************************************************************
 *
 * @file     ui_ffs_total_cost_data.c
 * @brief    I/O Menu
 * @version  V1.00
 * @date     19, March 2019
 *
 * @note     Displays data on the associated call assignment costs for each
 *           car for the last DD panel call request
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "mod.h"
#include "ui.h"
#include "lcd.h"
#include "buttons.h"
#include "carData.h"
#include "totalCostData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void UI_FFS_TotalCostData( void );
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_TotalCostData =
{
   .pfnDraw = UI_FFS_TotalCostData,
};

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_TotalCostData =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_TotalCostData,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   TCD_PAGE__T2P,
   TCD_PAGE__T2D,
   TCD_PAGE__TC,
   NUM_TCD_PAGES
}en_tcd_pages;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

static void PrintScreen_T2P( enum en_group_net_nodes eCarID )
{
   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("T2P-TR: ");
   LCD_Char_WriteInteger_MinLength(TotalCost_GetLastAssignment_T2P_Travel(eCarID), 1);

   /* L3 */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("T2P-CC: ");
   LCD_Char_WriteInteger_MinLength(TotalCost_GetLastAssignment_T2P_CC(eCarID), 1);

   /* L4 */
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("T2P-HC: ");
   LCD_Char_WriteInteger_MinLength(TotalCost_GetLastAssignment_T2P_HC(eCarID), 1);
}
static void PrintScreen_T2D( enum en_group_net_nodes eCarID )
{
   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("T2D-TR: ");
   LCD_Char_WriteInteger_MinLength(TotalCost_GetLastAssignment_T2D_Travel(eCarID), 1);

   /* L3 */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("T2D-CC: ");
   LCD_Char_WriteInteger_MinLength(TotalCost_GetLastAssignment_T2D_CC(eCarID), 1);

   /* L4 */
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("T2D-HC: ");
   LCD_Char_WriteInteger_MinLength(TotalCost_GetLastAssignment_T2D_HC(eCarID), 1);
}
static void PrintScreen_TC( enum en_group_net_nodes eCarID )
{
   /* L2 */
   LCD_Char_GotoXY( 0, 1 );
   LCD_Char_WriteString("TA-CC: ");
   LCD_Char_WriteInteger_MinLength(TotalCost_GetLastAssignment_TA_CC(eCarID), 1);

   /* L3 */
   LCD_Char_GotoXY( 0, 2 );
   LCD_Char_WriteString("TA-HC: ");
   LCD_Char_WriteInteger_MinLength(TotalCost_GetLastAssignment_TA_HC(eCarID), 1);

   /* L4 */
   LCD_Char_GotoXY( 0, 3 );
   LCD_Char_WriteString("TC   : ");
   if(TotalCost_GetLastAssignment_TotalCost(eCarID) == INVALID_TOTAL_ASSIGNMENT_COST_SEC)
   {
      LCD_Char_WriteString("INVALID");
   }
   else
   {
      LCD_Char_WriteInteger_MinLength(TotalCost_GetLastAssignment_TotalCost(eCarID), 1);
   }

}
/*----------------------------------------------------------------------------
|01234567890123456789| |01234567890123456789| |01234567890123456789|

|CAR1 C-10 P-12 D-12 | |CAR1 C-10 P-12 D-12 | |CAR1 C-10 P-12 D-12 |
|T2P-TR: 65535       | |T2D-TR: 65535       | |TA-CC: 65535        |
|T2P-CC: 65535       | |T2D-CC: 65535       | |TA-HC: 65535        |
|T2P-HC: 65535     <>| |T2D-HC: 65535     <>| |TC   : 65535      <>|

 ----------------------------------------------------------------------------*/
static void Print_Screen( uint8_t ucCursorX, uint8_t ucCursorY )
{
   LCD_Char_Clear();

   /* Print Row 1 */
   LCD_Char_GotoXY( 0, 0 );

   LCD_Char_WriteString("CAR");
   LCD_Char_WriteInteger_ExactLength(ucCursorY+1, 1);

   LCD_Char_AdvanceX(1);
   LCD_Char_WriteString("C-");
   LCD_Char_WriteInteger_ExactLength(TotalCost_GetLastAssignment_CurrentLanding(ucCursorY)+1, 2);

   LCD_Char_AdvanceX(1);
   LCD_Char_WriteString("P-");
   LCD_Char_WriteInteger_ExactLength(TotalCost_GetLastAssignment_PanelLanding(ucCursorY)+1, 2);

   LCD_Char_AdvanceX(1);
   LCD_Char_WriteString("D-");
   LCD_Char_WriteInteger_ExactLength(TotalCost_GetLastAssignment_DestinationLanding(ucCursorY)+1, 2);

   /* Print Rows 2-4 */
   switch(ucCursorX)
   {
      case TCD_PAGE__T2P:
         PrintScreen_T2P(ucCursorY);
         break;
      case TCD_PAGE__T2D:
         PrintScreen_T2D(ucCursorY);
         break;
      case TCD_PAGE__TC:
         PrintScreen_TC(ucCursorY);
         break;
      default: break;
   }

   if( ucCursorX )
   {
      LCD_Char_GotoXY( 18, 3 );
      LCD_Char_WriteString("<");
   }
   if( ucCursorX < (NUM_TCD_PAGES-1) )
   {
      LCD_Char_GotoXY( 19, 3 );
      LCD_Char_WriteString(">");
   }
}
/*----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static void UI_FFS_TotalCostData( void )
{
   static int8_t cCursorX, cCursorY;

   enum en_keypresses enKeypress = Button_GetKeypress();

   if ( enKeypress == enKEYPRESS_LEFT )
   {
      if(!cCursorX)
      {
         PrevScreen();
      }
      else
      {
         cCursorX--;
      }
   }
   else if ( enKeypress == enKEYPRESS_RIGHT )
   {
      cCursorX++;
   }
   else if ( enKeypress == enKEYPRESS_UP )
   {
      cCursorY--;
   }
   else if ( enKeypress == enKEYPRESS_DOWN )
   {
      cCursorY++;
   }

   /* Bound values */
   if( cCursorY >= MAX_GROUP_CARS )
   {
      cCursorY = MAX_GROUP_CARS-1;
   }
   else if( cCursorY < 0 )
   {
      cCursorY = 0;
   }
   if( cCursorX >= NUM_TCD_PAGES )
   {
      cCursorX = NUM_TCD_PAGES-1;
   }
   else if( cCursorX < 0 )
   {
      cCursorX = 0;
   }

   Print_Screen( cCursorX, cCursorY );
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/


