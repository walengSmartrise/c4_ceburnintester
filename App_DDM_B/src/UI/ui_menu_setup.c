/******************************************************************************
 *
 * @file     ui_menu_setup.c
 * @brief    Setup Menu
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"
#include "mod.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *----------------------------------------------------------------------------*/
static struct st_ui_menu_item gstMI_Panel =
{
   .psTitle = "Panel Setup",
   .pstUGS_Next = &gstUGS_Menu_Setup_Panel,
};
static struct st_ui_menu_item gstMI_Security =
{
   .psTitle = "Security",
   .pstUGS_Next = &gstUGS_Menu_Setup_Security,
};
static struct st_ui_menu_item gstMI_Misc =
{
   .psTitle = "Miscellaneous",
   .pstUGS_Next = &gstUGS_Menu_Setup_Misc,
};

static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_Panel,
   &gstMI_Security,
   &gstMI_Misc,
};
static struct st_ui_screen__menu gstMenu =
{
   .psTitle = "Setup",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Menu_Setup =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
