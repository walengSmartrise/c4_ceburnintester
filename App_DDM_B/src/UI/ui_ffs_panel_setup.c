/******************************************************************************
 *
 * @file     ui_ffs_doors.c
 * @brief    Home Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "mod.h"
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_Setup_ScreenTO( void );
static void UI_FFS_Setup_ScreenAssistTO( void );
static void UI_FFS_Setup_KeyTO( void );
static void UI_FFS_Setup_KeyAssistTO( void );
static void UI_FFS_Setup_CodeTO( void );
static void UI_FFS_Setup_CodeAssistTO( void );
static void UI_FFS_Setup_DispRespTO( void );
static void UI_FFS_Setup_WeightPerCall( void );
static void UI_FFS_Setup_RatedLoad_C1( void );
static void UI_FFS_Setup_RatedLoad_C2( void );
static void UI_FFS_Setup_RatedLoad_C3( void );
static void UI_FFS_Setup_RatedLoad_C4( void );
static void UI_FFS_Setup_RatedLoad_C5( void );
static void UI_FFS_Setup_RatedLoad_C6( void );
static void UI_FFS_Setup_RatedLoad_C7( void );
static void UI_FFS_Setup_RatedLoad_C8( void );
static void UI_FFS_Setup_InProximityEntry( void );
static void UI_FFS_Setup_DisableEntryTimer( void );
static void UI_FFS_Setup_EnableDuparPanel( void );

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_ScreenTO =
{
   .pfnDraw = UI_FFS_Setup_ScreenTO,
};
static struct st_ui_screen__freeform gstFFS_ScreenAssistTO =
{
   .pfnDraw = UI_FFS_Setup_ScreenAssistTO,
};
static struct st_ui_screen__freeform gstFFS_KeyTO =
{
   .pfnDraw = UI_FFS_Setup_KeyTO,
};
static struct st_ui_screen__freeform gstFFS_KeyAssistTO =
{
   .pfnDraw = UI_FFS_Setup_KeyAssistTO,
};
static struct st_ui_screen__freeform gstFFS_CodeTO =
{
   .pfnDraw = UI_FFS_Setup_CodeTO,
};
static struct st_ui_screen__freeform gstFFS_CodeAssistTO =
{
   .pfnDraw = UI_FFS_Setup_CodeAssistTO,
};
static struct st_ui_screen__freeform gstFFS_DispRespTO =
{
   .pfnDraw = UI_FFS_Setup_DispRespTO,
};
static struct st_ui_screen__freeform gstFFS_WeightPerCall =
{
   .pfnDraw = UI_FFS_Setup_WeightPerCall,
};
static struct st_ui_screen__freeform gstFFS_RatedLoad_C1 =
{
   .pfnDraw = UI_FFS_Setup_RatedLoad_C1,
};
static struct st_ui_screen__freeform gstFFS_RatedLoad_C2 =
{
   .pfnDraw = UI_FFS_Setup_RatedLoad_C2,
};
static struct st_ui_screen__freeform gstFFS_RatedLoad_C3 =
{
   .pfnDraw = UI_FFS_Setup_RatedLoad_C3,
};
static struct st_ui_screen__freeform gstFFS_RatedLoad_C4 =
{
   .pfnDraw = UI_FFS_Setup_RatedLoad_C4,
};
static struct st_ui_screen__freeform gstFFS_RatedLoad_C5 =
{
   .pfnDraw = UI_FFS_Setup_RatedLoad_C5,
};
static struct st_ui_screen__freeform gstFFS_RatedLoad_C6 =
{
   .pfnDraw = UI_FFS_Setup_RatedLoad_C6,
};
static struct st_ui_screen__freeform gstFFS_RatedLoad_C7 =
{
   .pfnDraw = UI_FFS_Setup_RatedLoad_C7,
};
static struct st_ui_screen__freeform gstFFS_RatedLoad_C8 =
{
   .pfnDraw = UI_FFS_Setup_RatedLoad_C8,
};
static struct st_ui_screen__freeform gstFFS_InProximityEntry =
{
   .pfnDraw = UI_FFS_Setup_InProximityEntry,
};
static struct st_ui_screen__freeform gstFFS_DisableEntryTimer =
{
   .pfnDraw = UI_FFS_Setup_DisableEntryTimer,
};
static struct st_ui_screen__freeform gstFFS_EnableDuparPanel =
{
   .pfnDraw = UI_FFS_Setup_EnableDuparPanel,
};
//---------------------------------------------------------------
static struct st_ui_menu_item gstMI_ScreenTO =
{
   .psTitle = "Screen Timeout",
   .pstUGS_Next = &gstUGS_Setup_ScreenTO,
};
static struct st_ui_menu_item gstMI_ScreenAssistTO =
{
   .psTitle = "ADA Screen Timeout",
   .pstUGS_Next = &gstUGS_Setup_ScreenAssistTO,
};
static struct st_ui_menu_item gstMI_KeyTO =
{
   .psTitle = "Key Press Timeout",
   .pstUGS_Next = &gstUGS_Setup_KeyTO,
};
static struct st_ui_menu_item gstMI_KeyAssistTO =
{
   .psTitle = "ADA Key Press Timeout",
   .pstUGS_Next = &gstUGS_Setup_KeyAssistTO,
};
static struct st_ui_menu_item gstMI_CodeTO =
{
   .psTitle = "Code Entry Timeout",
   .pstUGS_Next = &gstUGS_Setup_CodeTO,
};
static struct st_ui_menu_item gstMI_CodeAssistTO =
{
   .psTitle = "ADA Code Entry Timeout",
   .pstUGS_Next = &gstUGS_Setup_CodeAssistTO,
};
static struct st_ui_menu_item gstMI_DispRespTO =
{
   .psTitle = "Dispatch Resp. Timeout",
   .pstUGS_Next = &gstUGS_Setup_DispRespTO,
};
static struct st_ui_menu_item gstMI_WeightPerCall =
{
   .psTitle = "Weight Per Call",
   .pstUGS_Next = &gstUGS_Setup_WeightPerCall,
};
static struct st_ui_menu_item gstMI_RatedLoad_C1 =
{
   .psTitle = "Rated Load Car 1",
   .pstUGS_Next = &gstUGS_Setup_RatedLoad_C1,
};
static struct st_ui_menu_item gstMI_RatedLoad_C2 =
{
   .psTitle = "Rated Load Car 2",
   .pstUGS_Next = &gstUGS_Setup_RatedLoad_C2,
};
static struct st_ui_menu_item gstMI_RatedLoad_C3 =
{
   .psTitle = "Rated Load Car 3",
   .pstUGS_Next = &gstUGS_Setup_RatedLoad_C3,
};
static struct st_ui_menu_item gstMI_RatedLoad_C4 =
{
   .psTitle = "Rated Load Car 4",
   .pstUGS_Next = &gstUGS_Setup_RatedLoad_C4,
};
static struct st_ui_menu_item gstMI_RatedLoad_C5 =
{
   .psTitle = "Rated Load Car 5",
   .pstUGS_Next = &gstUGS_Setup_RatedLoad_C5,
};
static struct st_ui_menu_item gstMI_RatedLoad_C6 =
{
   .psTitle = "Rated Load Car 6",
   .pstUGS_Next = &gstUGS_Setup_RatedLoad_C6,
};
static struct st_ui_menu_item gstMI_RatedLoad_C7 =
{
   .psTitle = "Rated Load Car 7",
   .pstUGS_Next = &gstUGS_Setup_RatedLoad_C7,
};
static struct st_ui_menu_item gstMI_RatedLoad_C8 =
{
   .psTitle = "Rated Load Car 8",
   .pstUGS_Next = &gstUGS_Setup_RatedLoad_C8,
};
static struct st_ui_menu_item gstMI_InProximityEntry =
{
   .psTitle = "In Proximity Entry",
   .pstUGS_Next = &gstUGS_Setup_InProximityEntry,
};
static struct st_ui_menu_item gstMI_DisableEntryTimer =
{
   .psTitle = "Disable Entry Timer",
   .pstUGS_Next = &gstUGS_Setup_DisableEntryTimer,
};
static struct st_ui_menu_item gstMI_EnableDuparPanel =
{
   .psTitle = "Enable Dupar Panel",
   .pstUGS_Next = &gstUGS_Setup_EnableDuparPanel,
};
static struct st_ui_menu_item * gastMenuItems[] =
{
   &gstMI_ScreenTO,
   &gstMI_ScreenAssistTO,
   &gstMI_KeyTO,
   &gstMI_KeyAssistTO,
   &gstMI_CodeTO,
   &gstMI_CodeAssistTO,
   &gstMI_DispRespTO,
   &gstMI_WeightPerCall,
   &gstMI_RatedLoad_C1,
   &gstMI_RatedLoad_C2,
   &gstMI_RatedLoad_C3,
   &gstMI_RatedLoad_C4,
   &gstMI_RatedLoad_C5,
   &gstMI_RatedLoad_C6,
   &gstMI_RatedLoad_C7,
   &gstMI_RatedLoad_C8,
   &gstMI_InProximityEntry,
   &gstMI_DisableEntryTimer,
   &gstMI_EnableDuparPanel,
};
static struct st_ui_screen__menu gstMenu =
{
   .psTitle = "Panel Setup",
   .pastMenuItems = &gastMenuItems,
   .ucNumItems = sizeof(gastMenuItems) / sizeof(gastMenuItems[ 0 ]),
};

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_ui_generic_screen gstUGS_Setup_ScreenTO =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ScreenTO,
};
struct st_ui_generic_screen gstUGS_Setup_ScreenAssistTO =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_ScreenAssistTO,
};
struct st_ui_generic_screen gstUGS_Setup_KeyTO =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_KeyTO,
};
struct st_ui_generic_screen gstUGS_Setup_KeyAssistTO =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_KeyAssistTO,
};
struct st_ui_generic_screen gstUGS_Setup_CodeTO =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_CodeTO,
};
struct st_ui_generic_screen gstUGS_Setup_CodeAssistTO =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_CodeAssistTO,
};
struct st_ui_generic_screen gstUGS_Setup_DispRespTO =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DispRespTO,
};
struct st_ui_generic_screen gstUGS_Setup_WeightPerCall =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_WeightPerCall,
};
struct st_ui_generic_screen gstUGS_Setup_RatedLoad_C1 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_RatedLoad_C1,
};
struct st_ui_generic_screen gstUGS_Setup_RatedLoad_C2 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_RatedLoad_C2,
};
struct st_ui_generic_screen gstUGS_Setup_RatedLoad_C3 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_RatedLoad_C3,
};
struct st_ui_generic_screen gstUGS_Setup_RatedLoad_C4 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_RatedLoad_C4,
};
struct st_ui_generic_screen gstUGS_Setup_RatedLoad_C5 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_RatedLoad_C5,
};
struct st_ui_generic_screen gstUGS_Setup_RatedLoad_C6 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_RatedLoad_C6,
};
struct st_ui_generic_screen gstUGS_Setup_RatedLoad_C7 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_RatedLoad_C7,
};
struct st_ui_generic_screen gstUGS_Setup_RatedLoad_C8 =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_RatedLoad_C8,
};
struct st_ui_generic_screen gstUGS_Setup_InProximityEntry =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_InProximityEntry,
};
struct st_ui_generic_screen gstUGS_Setup_DisableEntryTimer =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_DisableEntryTimer,
};
struct st_ui_generic_screen gstUGS_Setup_EnableDuparPanel =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_EnableDuparPanel,
};
//-----------------------------------------------------
struct st_ui_generic_screen gstUGS_Menu_Setup_Panel =
{
   .ucType = enUI_STYPE__MENU,
   .pvReference = &gstMenu,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_ScreenTO( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = DDM_PARAM8__ScreenTO_100ms;
   stParamEdit.uwParamIndex_End = DDM_PARAM8__ScreenTO_100ms;
   stParamEdit.psTitle = "Screen Timeout";
   stParamEdit.psUnit = "00 msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_ScreenAssistTO( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = DDM_PARAM8__ScreenAssistTO_100ms;
   stParamEdit.uwParamIndex_End = DDM_PARAM8__ScreenAssistTO_100ms;
   stParamEdit.psTitle = "ADA Screen Timeout";
   stParamEdit.psUnit = "00 msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_KeyTO( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = DDM_PARAM8__KeyTO_100ms;
   stParamEdit.uwParamIndex_End = DDM_PARAM8__KeyTO_100ms;
   stParamEdit.psTitle = "Key Press Timeout";
   stParamEdit.psUnit = "00 msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_KeyAssistTO( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = DDM_PARAM8__KeyAssistTO_100ms;
   stParamEdit.uwParamIndex_End = DDM_PARAM8__KeyAssistTO_100ms;
   stParamEdit.psTitle = "ADA Key Press Timeout";
   stParamEdit.psUnit = "00 msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_CodeTO( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = DDM_PARAM8__CodeTO_100ms;
   stParamEdit.uwParamIndex_End = DDM_PARAM8__CodeTO_100ms;
   stParamEdit.psTitle = "Code Entry Timeout";
   stParamEdit.psUnit = "00 msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_CodeAssistTO( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = DDM_PARAM8__CodeAssistTO_100ms;
   stParamEdit.uwParamIndex_End = DDM_PARAM8__CodeAssistTO_100ms;
   stParamEdit.psTitle = "ADA Code Entry Timeout";
   stParamEdit.psUnit = "00 msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_DispRespTO( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = DDM_PARAM8__DispRespTO_100ms;
   stParamEdit.uwParamIndex_End = DDM_PARAM8__DispRespTO_100ms;
   stParamEdit.psTitle = "Dispatcher Resp. Timeout";
   stParamEdit.psUnit = "00 msec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_WeightPerCall( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = DDM_PARAM8__WeightPerCall_10lb;
   stParamEdit.uwParamIndex_End = DDM_PARAM8__WeightPerCall_10lb;
   stParamEdit.psTitle = "Weight Per Call";
   stParamEdit.psUnit = "0 lbs";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_RatedLoad_C1( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = DDM_PARAM16__RatedLoad_Car1_lb;
   stParamEdit.uwParamIndex_End = DDM_PARAM16__RatedLoad_Car1_lb;
   stParamEdit.psTitle = "Rated Load Car 1";
   stParamEdit.psUnit = " lbs";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_RatedLoad_C2( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = DDM_PARAM16__RatedLoad_Car2_lb;
   stParamEdit.uwParamIndex_End = DDM_PARAM16__RatedLoad_Car2_lb;
   stParamEdit.psTitle = "Rated Load Car 2";
   stParamEdit.psUnit = " lbs";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_RatedLoad_C3( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = DDM_PARAM16__RatedLoad_Car3_lb;
   stParamEdit.uwParamIndex_End = DDM_PARAM16__RatedLoad_Car3_lb;
   stParamEdit.psTitle = "Rated Load Car 3";
   stParamEdit.psUnit = " lbs";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_RatedLoad_C4( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = DDM_PARAM16__RatedLoad_Car4_lb;
   stParamEdit.uwParamIndex_End = DDM_PARAM16__RatedLoad_Car4_lb;
   stParamEdit.psTitle = "Rated Load Car 4";
   stParamEdit.psUnit = " lbs";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_RatedLoad_C5( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = DDM_PARAM16__RatedLoad_Car5_lb;
   stParamEdit.uwParamIndex_End = DDM_PARAM16__RatedLoad_Car5_lb;
   stParamEdit.psTitle = "Rated Load Car 5";
   stParamEdit.psUnit = " lbs";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_RatedLoad_C6( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = DDM_PARAM16__RatedLoad_Car6_lb;
   stParamEdit.uwParamIndex_End = DDM_PARAM16__RatedLoad_Car6_lb;
   stParamEdit.psTitle = "Rated Load Car 6";
   stParamEdit.psUnit = " lbs";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_RatedLoad_C7( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = DDM_PARAM16__RatedLoad_Car7_lb;
   stParamEdit.uwParamIndex_End = DDM_PARAM16__RatedLoad_Car7_lb;
   stParamEdit.psTitle = "Rated Load Car 7";
   stParamEdit.psUnit = " lbs";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_RatedLoad_C8( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT16;
   stParamEdit.uwParamIndex_Start = DDM_PARAM16__RatedLoad_Car8_lb;
   stParamEdit.uwParamIndex_End = DDM_PARAM16__RatedLoad_Car8_lb;
   stParamEdit.psTitle = "Rated Load Car 8";
   stParamEdit.psUnit = " lbs";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_InProximityEntry( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = DDM_PARAM1__InProximityEntry;
   stParamEdit.uwParamIndex_End = DDM_PARAM1__InProximityEntry;
   stParamEdit.psTitle = "In Proximity Entry";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_DisableEntryTimer( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__UINT8;
   stParamEdit.uwParamIndex_Start = DDM_PARAM8__DisableEntryTimer_s;
   stParamEdit.uwParamIndex_End = DDM_PARAM8__DisableEntryTimer_s;
   stParamEdit.psTitle = "Disable Entry Timer";
   stParamEdit.psUnit = " sec";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UI_FFS_Setup_EnableDuparPanel( void )
{
   static struct st_param_edit_menu stParamEdit;
   stParamEdit.enParamType = enPARAM_BLOCK_TYPE__BIT;
   stParamEdit.uwParamIndex_Start = DDM_PARAM1__EnableDuparPanel;
   stParamEdit.uwParamIndex_End = DDM_PARAM1__EnableDuparPanel;
   stParamEdit.psTitle = "Enable Dupar Panel";
   stParamEdit.psUnit = "";
   UI_ParamEditSceenTemplate(&stParamEdit);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
