/******************************************************************************
 *
 * @file     ui_ffs_assigned_calls.c
 * @brief    Kiosk Assigned Calls Screen
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note     Displayed the calls assigned to a car on a per floor basis
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "ui.h"

#include "buttons.h"
#include "lcd.h"
#include "sru.h"
#include "mod.h"
#include <stdint.h>
#include <string.h>
#include "config.h"
#include "kioskData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void UI_FFS_AssignedCalls( void );

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_ui_screen__freeform gstFFS_AssignedCalls =
{
   .pfnDraw = UI_FFS_AssignedCalls,
};

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_ui_generic_screen gstUGS_AssignedCalls =
{
   .ucType = enUI_STYPE__FREEFORM,
   .pvReference = &gstFFS_AssignedCalls,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

   |01234567890123456789|

   |Car 1 - Landing 96 ^|
   |                 *  |
   | 1F  2F  3R  4F  5R |
   | 6R  7F  8R  9F 10R |

   |11R 12F 13R 14F 15R | //ucScroll = 1
   |16R 17F 18R 19F 20R | //ucScroll = 2

 -----------------------------------------------------------------------------*/
#define CALLS_PER_LINE     (5)
static void UpdateScreen( uint8_t ucCursorX, uint8_t ucCar, uint8_t ucLanding, uint8_t ucScroll )
{
   st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(ucCar, ucLanding);
   uint8_t ucStartIndex = 0;
   uint8_t ucStopIndex = 0;

   LCD_Char_Clear();

   /* Line 1 */
   LCD_Char_GotoXY( 0, 0 );
   LCD_Char_WriteString("Car ");
   LCD_Char_WriteInteger_MinLength(ucCar+1, 1);
   LCD_Char_WriteString(" - Landing ");
   LCD_Char_WriteInteger_MinLength(ucLanding+1, 2);
   LCD_Char_WriteString(" ^");

   /* Line 2 */
   switch(ucCursorX)
   {
      case 0: LCD_Char_GotoXY( 4, 1 ); break;
      case 1: LCD_Char_GotoXY( 17, 1 ); break;
      case 2: LCD_Char_GotoXY( 19, 1 ); break;
      default: LCD_Char_GotoXY( 0, 1 ); break;
   }
   LCD_Char_WriteString("*");

   /* Line 3/4 */
   for(uint8_t j = 0; j < 2; j++)
   {
      ucStartIndex = CALLS_PER_LINE * (ucScroll+j);
      ucStopIndex = ucStartIndex + CALLS_PER_LINE;
      ucStopIndex = (ucStopIndex < pstAssignment->ucCallIndex) ? ucStopIndex:pstAssignment->ucCallIndex;
      LCD_Char_GotoXY( 0, 2+j );
      for(uint8_t i = ucStartIndex; i < ucStopIndex; i++)
      {
         LCD_Char_WriteInteger_MinLength(pstAssignment->aucLanding_Dest_Plus1[i], 2);
         if( pstAssignment->abRear[i] )
         {
            LCD_Char_WriteString("R");
         }
         else
         {
            LCD_Char_WriteString("F");
         }
         LCD_Char_AdvanceX(1);
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define ASSIGNED_CALLS_X_LIMIT         (3)
#define ASSIGNED_CALLS_SCROLL_LIMIT    (3)
static void UI_FFS_AssignedCalls( void )
{
   static uint8_t ucCursorX, ucCar, ucLanding, ucScroll;

   enum en_keypresses eKeypress = Button_GetKeypress();
   switch(eKeypress)
   {
      case enKEYPRESS_LEFT:
         if(ucCursorX)
         {
            ucCursorX--;
         }
         else
         {
            PrevScreen();
         }
         break;

      case enKEYPRESS_RIGHT:
         if(ucCursorX < ASSIGNED_CALLS_X_LIMIT-1)
         {
            ucCursorX++;
         }
         break;

      case enKEYPRESS_UP:
         switch(ucCursorX)
         {
            case 0:
               if(ucCar < MAX_GROUP_CARS-1)
               {
                  ucCar++;
               }
               break;
            case 1:
               if(ucLanding < MAX_NUM_FLOORS-1)
               {
                  ucLanding++;
               }
               break;
            case 2:
               if(ucScroll < ASSIGNED_CALLS_SCROLL_LIMIT-1)
               {
                  ucScroll++;
               }
               break;
            default: break;
         }
         break;

      case enKEYPRESS_DOWN:
         switch(ucCursorX)
         {
            case 0:
               if(ucCar)
               {
                  ucCar--;
               }
               break;
            case 1:
               if(ucLanding)
               {
                  ucLanding--;
               }
               break;
            case 2:
               if(ucScroll)
               {
                  ucScroll--;
               }
               break;
            default: break;
         }
         break;

      default:
         break;
   }
   UpdateScreen(ucCursorX, ucCar, ucLanding, ucScroll);
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
