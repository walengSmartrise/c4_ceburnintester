/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief
 * @version  V1.00
 * @date     27, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>

#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "carData.h"
#include "carDestination.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static st_cardata astCarData[MAX_GROUP_CARS];

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Initialize car data
 -----------------------------------------------------------------------------*/
void Init_CarDataStructure()
{
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      astCarData[i].uwOfflineCounter_ms = GROUP_CAR_INVALID_TIMER_MS;
   }
}
void Init_CarDataStructure_ByCar(enum en_group_net_nodes eCarID)
{
   memset(&astCarData[eCarID], 0, sizeof(st_cardata));
   astCarData[eCarID].uwOfflineCounter_ms = GROUP_CAR_INVALID_TIMER_MS;
}

/*-----------------------------------------------------------------------------
   Access functions
 -----------------------------------------------------------------------------*/
st_cardata * GetCarDataStructure(enum en_group_net_nodes eCarID)
{
   return &astCarData[eCarID];
}
uint8_t CarData_GetMasterDispatcherFlag(enum en_group_net_nodes eCarID)
{
   return astCarData[eCarID].bMasterDispatcher;
}
uint8_t CarData_GetDoorOpen_Front( enum en_group_net_nodes eCarID )
{
   uint8_t bOpen = 0;
   if( ( astCarData[eCarID].eDoor_F == DOOR__OPENING )
    || ( astCarData[eCarID].eDoor_F == DOOR__OPEN ) )
   {
      bOpen = 1;
   }
   return bOpen;
}
uint8_t CarData_GetDoorOpen_Rear( enum en_group_net_nodes eCarID )
{
   uint8_t bOpen = 0;
   if( ( astCarData[eCarID].eDoor_R == DOOR__OPENING )
    || ( astCarData[eCarID].eDoor_R == DOOR__OPEN ) )
   {
      bOpen = 1;
   }
   return bOpen;
}
//Returns 1 if valid opening exists
uint8_t CarData_CheckOpening( enum en_group_net_nodes eCarID, uint8_t ucLanding, enum en_doors eDoor )
{
   uint8_t bValid = 0;
   //Assumes valid door and car id
   if( ucLanding <= astCarData[eCarID].ucLastLanding )
   {
      if( eDoor == DOOR_FRONT )
      {
         bValid = Sys_Bit_Get(&astCarData[eCarID].auiBF_OpeningMap_F[0], ucLanding);
      }
      else if( eDoor == DOOR_REAR )
      {
         bValid = Sys_Bit_Get(&astCarData[eCarID].auiBF_OpeningMap_R[0], ucLanding);
      }
      else
      {
         bValid = Sys_Bit_Get(&astCarData[eCarID].auiBF_OpeningMap_F[0], ucLanding)
                | Sys_Bit_Get(&astCarData[eCarID].auiBF_OpeningMap_R[0], ucLanding);
      }
   }
   return bValid;
}

uint32_t CarData_GetHallMask( enum en_group_net_nodes eCarID, enum en_doors eDoor )
{
   uint32_t uiMask = 0;
   if( eDoor == DOOR_FRONT )
   {
      uiMask = astCarData[eCarID].uiHallMask_F;
   }
   else if( eDoor == DOOR_REAR )
   {
      uiMask = astCarData[eCarID].uiHallMask_R;
   }
   else
   {
      uiMask = astCarData[eCarID].uiHallMask_F;
      uiMask |= astCarData[eCarID].uiHallMask_R;
   }

   return uiMask;
}

uint8_t CarData_GetFastResend(void)
{
   uint8_t bReturn = 0;
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      bReturn |= astCarData[i].bFastGroupResend;
   }
   return bReturn;
}

uint8_t GetInputValue_ByCar( enum en_group_net_nodes eCarID, enum en_input_functions enInput )
{
   uint8_t bReturn = 0;
   if( enInput < NUM_INPUT_FUNCTIONS )
   {
      bReturn = Sys_Bit_Get(&astCarData[eCarID].auiBF_InputMap[0], enInput);
   }
   return bReturn;
}
uint8_t GetOutputValue_ByCar( enum en_group_net_nodes eCarID, enum en_output_functions enOutput )
{
   uint8_t bReturn = 0;
   if( enOutput < NUM_OUTPUT_FUNCTIONS )
   {
      bReturn = Sys_Bit_Get(&astCarData[eCarID].auiBF_OutputMap[0], enOutput);
   }
   return bReturn;
}
/*----------------------------------------------------------------------------
   Returns a single mask with the active hall mask for each group car.
   If ucLanding is INVALID_LANDING, then the mask is not floor specific
 *----------------------------------------------------------------------------*/
uint32_t CarData_GetGroupHallCallMask( uint8_t ucLanding, enum en_doors eDoor )
{
   uint32_t uiMask = 0;
   if( ucLanding == INVALID_LANDING )
   {
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         if(GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS)
         {
            st_cardata *pstCarData = GetCarDataStructure(i);
            if( eDoor == DOOR_ANY )
            {
               uiMask |= ( pstCarData->uiLatchableHallMask_F | pstCarData->uiLatchableHallMask_R );
            }
            else if( eDoor == DOOR_FRONT )
            {
               uiMask |= ( pstCarData->uiLatchableHallMask_F );
            }
            else
            {
               uiMask |= ( pstCarData->uiLatchableHallMask_R );
            }
         }
      }
   }
   else
   {
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         if(GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS)
         {
            if(CarData_CheckOpening(i, ucLanding, eDoor))
            {
               st_cardata *pstCarData = GetCarDataStructure(i);
               if( eDoor == DOOR_ANY )
               {
                  uiMask |= ( pstCarData->uiLatchableHallMask_F | pstCarData->uiLatchableHallMask_R );
               }
               else if( eDoor == DOOR_FRONT )
               {
                  uiMask |= ( pstCarData->uiLatchableHallMask_F );
               }
               else
               {
                  uiMask |= ( pstCarData->uiLatchableHallMask_R );
               }
            }
         }
      }
   }

   return uiMask;
}
/*----------------------------------------------------------------------------
   Returns a single mask with the active hall mask for each group car.
   If ucLanding is INVALID_LANDING, then the mask is not floor specific
   Returns the mask for latchable EMS emergency medical recall calls.
 *----------------------------------------------------------------------------*/
uint8_t CarData_GetMedicalHallCallMask( uint8_t ucLanding, enum en_doors eDoor )
{
   uint8_t ucMask = 0;
   if( ucLanding == INVALID_LANDING )
   {
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         if(GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS)
         {
            st_cardata *pstCarData = GetCarDataStructure(i);
            if( !pstCarData->ucMedicalLanding_Plus1 ) /* If no medical landing is specified, the mask applies to any landing. */
            {
               ucMask |= pstCarData->ucMedicalMask;
            }
         }
      }
   }
   else
   {
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         if(GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS)
         {
            if(CarData_CheckOpening(i, ucLanding, eDoor))
            {
               st_cardata *pstCarData = GetCarDataStructure(i);
               /* EMS medical emergency recall support */
               if( pstCarData->ucMedicalLanding_Plus1 )
               {
                  uint8_t ucMedicalLanding = pstCarData->ucMedicalLanding_Plus1-1;
                  if( ucMedicalLanding == ucLanding )
                  {
                     ucMask |= pstCarData->ucMedicalMask;
                  }
               }
               else /* If no medical landing is specified, the mask applies to any landing. */
               {
                  ucMask |= pstCarData->ucMedicalMask;
               }
            }
         }
      }
   }

   return ucMask;
}
/*-----------------------------------------------------------------------------
   Update car offline timer
 -----------------------------------------------------------------------------*/
void IncrementAllCarOfflineTimers( struct st_module *pstThisModule )
{
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      if( astCarData[i].uwOfflineCounter_ms < GROUP_CAR_OFFLINE_TIMER_MS )
      {
         astCarData[i].uwOfflineCounter_ms += pstThisModule->uwRunPeriod_1ms;
      }
   }
}
void ClrCarOfflineTimer_ByCar( enum en_group_net_nodes eCarID )
{
   astCarData[eCarID].uwOfflineCounter_ms = 0;
}
uint16_t GetCarOfflineTimer_ByCar( enum en_group_net_nodes eCarID )
{
   return astCarData[eCarID].uwOfflineCounter_ms;
}
uint8_t GetCarOnlineFlag_ByCar( enum en_group_net_nodes eCarID )
{
   return ( astCarData[eCarID].uwOfflineCounter_ms < GROUP_CAR_OFFLINE_TIMER_MS );
}
en_car_request GetCarRequest_ByCar( enum en_group_net_nodes eCarID )
{
   return astCarData[eCarID].eCarRequest;
}
en_car_request GetCarRequest_AnyCar(void)
{
   en_car_request eRequest = CAR_REQUEST__NONE;
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      if(astCarData[i].eCarRequest)
      {
         eRequest = astCarData[i].eCarRequest;
         break;
      }
   }
   return eRequest;
}

uint8_t GetMasterDispatcherFlag_ByCar( enum en_group_net_nodes eCarID )
{
   return astCarData[eCarID].bMasterDispatcher;
}
/*----------------------------------------------------------------------------
   Defines a unique hall mask for each car, combined with the car's parameter controlled mask
   Used for assigning DD panel calls to specific cars
 *----------------------------------------------------------------------------*/
uint32_t CarData_GetUniqueHallMask_Front( enum en_group_net_nodes eCarID )
{
   uint32_t uiMask = 0;
   switch(eCarID)
   {
      case 0:
         uiMask = UNIQUE_HALL_MASK_F__CAR1;
         break;
      case 1:
         uiMask = UNIQUE_HALL_MASK_F__CAR2;
         break;
      case 2:
         uiMask = UNIQUE_HALL_MASK_F__CAR3;
         break;
      case 3:
         uiMask = UNIQUE_HALL_MASK_F__CAR4;
         break;
      case 4:
         uiMask = UNIQUE_HALL_MASK_F__CAR5;
         break;
      case 5:
         uiMask = UNIQUE_HALL_MASK_F__CAR6;
         break;
      case 6:
         uiMask = UNIQUE_HALL_MASK_F__CAR7;
         break;
      case 7:
         uiMask = UNIQUE_HALL_MASK_F__CAR8;
         break;
      default: break;
   }
   return uiMask;
}
uint32_t CarData_GetUniqueHallMask_Rear( enum en_group_net_nodes eCarID )
{
   uint32_t uiMask = 0;
   switch(eCarID)
   {
      case 0:
         uiMask = UNIQUE_HALL_MASK_R__CAR1;
         break;
      case 1:
         uiMask = UNIQUE_HALL_MASK_R__CAR2;
         break;
      case 2:
         uiMask = UNIQUE_HALL_MASK_R__CAR3;
         break;
      case 3:
         uiMask = UNIQUE_HALL_MASK_R__CAR4;
         break;
      case 4:
         uiMask = UNIQUE_HALL_MASK_R__CAR5;
         break;
      case 5:
         uiMask = UNIQUE_HALL_MASK_R__CAR6;
         break;
      case 6:
         uiMask = UNIQUE_HALL_MASK_R__CAR7;
         break;
      case 7:
         uiMask = UNIQUE_HALL_MASK_R__CAR8;
         break;
      default: break;
   }
   return uiMask;
}
/*-----------------------------------------------------------------------------
   Returns the highest active landing
 -----------------------------------------------------------------------------*/
uint8_t CarData_GetHighestActiveLanding()
{
   uint8_t ucLanding = 0;
   for(uint8_t ucCar = 0; ucCar < MAX_GROUP_CARS; ucCar++)
   {
      if( ucLanding < astCarData[ucCar].ucLastLanding )
      {
         ucLanding = astCarData[ucCar].ucLastLanding;
      }
   }
   return ucLanding;
}
/*-----------------------------------------------------------------------------
   punDatagram->auc8[0] = astCarData[eCarID].ucCurrentLanding;
   punDatagram->auc8[1] = astCarData[eCarID].ucDestinationLanding;
   punDatagram->auc8[2] = astCarData[eCarID].ucReachableLanding;
   punDatagram->auc8[3] = astCarData[eCarID].eDoor_F;
   punDatagram->auc8[4] = astCarData[eCarID].eDoor_R;
   punDatagram->auc8[5] = astCarData[eCarID].cMotionDir;
   punDatagram->auc8[6] = astCarData[eCarID].enPriority;
   punDatagram->auc8[7] = ( astCarData[eCarID].bFastGroupResend )
                        | ( astCarData[eCarID].bIdleDirection << 1 )
                        | ( astCarData[eCarID].bInSlowdown << 2 )
                        | ( astCarData[eCarID].bSuppressReopen << 3 );
 -----------------------------------------------------------------------------*/
void UnloadDatagram_CarStatus( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].ucCurrentLanding = punDatagram->auc8[0];
   astCarData[eCarID].ucDestinationLanding = punDatagram->auc8[1];
   astCarData[eCarID].ucReachableLanding = punDatagram->auc8[2];
   astCarData[eCarID].eDoor_F = ( punDatagram->auc8[3] & 0x0F );
   astCarData[eCarID].eDoor_R = ( punDatagram->auc8[4] & 0x0F );
   astCarData[eCarID].cMotionDir = punDatagram->auc8[5];
   astCarData[eCarID].enPriority = ( punDatagram->auc8[6] & 0x01 );
   astCarData[eCarID].bFastGroupResend = punDatagram->auc8[7] & 1;
   astCarData[eCarID].bIdleDirection = ( punDatagram->auc8[7] >> 1 ) & 1;
   astCarData[eCarID].bInSlowdown = ( punDatagram->auc8[7] >> 2 ) & 1;
   astCarData[eCarID].bSuppressReopen = ( punDatagram->auc8[7] >> 3 ) & 1;

   astCarData[eCarID].bDirty = 1;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UnloadDatagram_CarData( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   en_car_data_packets ePacket = punDatagram->auc8[0];
   astCarData[eCarID].ucFirstLanding = punDatagram->auc8[1];
   astCarData[eCarID].ucLastLanding = punDatagram->auc8[2];
   astCarData[eCarID].bMasterDispatcher = punDatagram->auc8[3];
   switch(ePacket)
   {
      case CAR_DATA_PACKET__OPENING_F_1:
         astCarData[eCarID].auiBF_OpeningMap_F[0] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__OPENING_F_2:
         astCarData[eCarID].auiBF_OpeningMap_F[1] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__OPENING_F_3:
         astCarData[eCarID].auiBF_OpeningMap_F[2] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__OPENING_R_1:
         astCarData[eCarID].auiBF_OpeningMap_R[0] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__OPENING_R_2:
         astCarData[eCarID].auiBF_OpeningMap_R[1] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__OPENING_R_3:
         astCarData[eCarID].auiBF_OpeningMap_R[2] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__HALLMASK_F:
         astCarData[eCarID].uiHallMask_F = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__HALLMASK_R:
         astCarData[eCarID].uiHallMask_R = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__LATCHABLE_HALLMASK_F:
         astCarData[eCarID].uiLatchableHallMask_F = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__PAIRED_HB_MASK:
         astCarData[eCarID].aucPairedHBMask[0] = ( punDatagram->aui32[1] ) & 0xFF;
         astCarData[eCarID].aucPairedHBMask[1] = ( punDatagram->aui32[1] >> 8 ) & 0xFF;
         astCarData[eCarID].aucPairedHBMask[2] = ( punDatagram->aui32[1] >> 16 ) & 0xFF;
         astCarData[eCarID].aucPairedHBMask[3] = ( punDatagram->aui32[1] >> 24 ) & 0xFF;
         break;
      case CAR_DATA_PACKET__SECURITY_F_1:
         astCarData[eCarID].auiBF_SecurityMap_F[0] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__SECURITY_F_2:
         astCarData[eCarID].auiBF_SecurityMap_F[1] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__SECURITY_F_3:
         astCarData[eCarID].auiBF_SecurityMap_F[2] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__SECURITY_R_1:
         astCarData[eCarID].auiBF_SecurityMap_R[0] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__SECURITY_R_2:
         astCarData[eCarID].auiBF_SecurityMap_R[1] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__SECURITY_R_3:
         astCarData[eCarID].auiBF_SecurityMap_R[2] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__TIME:
         astCarData[eCarID].ucMaxFloorToFloorTime_sec = ( punDatagram->aui32[1] >> 0 ) & 0xFF;
         astCarData[eCarID].ucDoorDwellTime_sec = ( punDatagram->aui32[1] >> 8 ) & 0xFF;
         astCarData[eCarID].ucDoorDwellHallTime_sec = ( punDatagram->aui32[1] >> 16 ) & 0xFF;
         break;
      case CAR_DATA_PACKET__HALL_SEC_1:
         astCarData[eCarID].auiBF_HallSecurityMap[0] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__HALL_SEC_2:
         astCarData[eCarID].auiBF_HallSecurityMap[1] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__HALL_SEC_3:
         astCarData[eCarID].auiBF_HallSecurityMap[2] = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__REQUEST:
         astCarData[eCarID].eCarRequest = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__HALL_SEC_MASK:
         astCarData[eCarID].ucHallSecurityMask_F = ( punDatagram->aui32[1] ) & 0xFF;
         astCarData[eCarID].ucHallSecurityMask_R = ( punDatagram->aui32[1] >> 8 ) & 0xFF;
         astCarData[eCarID].bEnableHallCallSecurity = ( punDatagram->aui32[1] >> 31 ) & 1;
         break;
      case CAR_DATA_PACKET__LATCHABLE_HALLMASK_R:
         astCarData[eCarID].uiLatchableHallMask_R = punDatagram->aui32[1];
         break;
      case CAR_DATA_PACKET__HALL_MEDICAL:
         astCarData[eCarID].ucMedicalMask = ( punDatagram->aui32[1] ) & 0xFF;
         astCarData[eCarID].ucMedicalLanding_Plus1 = ( punDatagram->aui32[1] >> 8 ) & 0xFF;
         // 2 Byte unused
         break;
      case CAR_DATA_PACKET__FLAGS:
         astCarData[eCarID].bDynamicParking = GET_BITFLAG_U32( punDatagram->aui32[1], 0);
         astCarData[eCarID].bParking = GET_BITFLAG_U32( punDatagram->aui32[1], 1);
         astCarData[eCarID].bExtFloorLimit = GET_BITFLAG_U32( punDatagram->aui32[1], 2);
         astCarData[eCarID].bVIP = GET_BITFLAG_U32( punDatagram->aui32[1], 3);
         astCarData[eCarID].ucbClosestCar = GET_BITFLAG_U32( punDatagram->aui32[1], 4);
         astCarData[eCarID].bCarReady = GET_BITFLAG_U32( punDatagram->aui32[1], 5);
         astCarData[eCarID].bCarCapture = GET_BITFLAG_U32( punDatagram->aui32[1], 6);
         astCarData[eCarID].bFaulted = GET_BITFLAG_U32( punDatagram->aui32[1], 7);
         break;
      case CAR_DATA_PACKET__VIPMASK:
         astCarData[eCarID].uiVIPMask_F = punDatagram->aui32[1] & 0xFF;
         astCarData[eCarID].uiVIPMask_R = ( punDatagram->aui32[1] >> 8 ) & 0xFF;
         break;
      default:
         break;
   }
}
/*-----------------------------------------------------------------------------
   punDatagram->auc8[0] = astCarData[eCarID].eClassOp;
   switch( astCarData[eCarID].eClassOp )
   {
      case CLASSOP__AUTO:
         punDatagram->auc8[1] = astCarData[eCarID].eAutoMode;
         break;

      case CLASSOP__MANUAL:
         punDatagram->auc8[1] = astCarData[eCarID].eManualMode;
         break;

      case CLASSOP__SEMI_AUTO:
         punDatagram->auc8[1] = astCarData[eCarID].eLearnMode;
         break;

      default:
         break;
   }
   punDatagram->auc8[2] = astCarData[eCarID].eAutoState;
   punDatagram->auc8[3] = astCarData[eCarID].eRecallState;
   punDatagram->auc8[4] = astCarData[eCarID].eCaptureMode;
   punDatagram->auc8[5] = astCarData[eCarID].ucFireService;
   punDatagram->auc8[6] = astCarData[eCarID].eFire2State;
   punDatagram->auc8[7] = astCarData[eCarID].eEP_CommandFeedback & 0x7F;
   punDatagram->auc8[7] |= (astCarData[eCarID].bEP_CarRecalledFlag & 0x01) << 7;
 -----------------------------------------------------------------------------*/
void UnloadDatagram_CarOperation( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].eAutoMode = 0;
   astCarData[eCarID].eManualMode = 0;
   astCarData[eCarID].eLearnMode = 0;

   astCarData[eCarID].eClassOp = punDatagram->auc8[0] & 0x07;
   switch( astCarData[eCarID].eClassOp )
   {
      case CLASSOP__AUTO:
         astCarData[eCarID].eAutoMode = punDatagram->auc8[1];
         break;

      case CLASSOP__MANUAL:
         astCarData[eCarID].eManualMode = punDatagram->auc8[1];
         break;

      case CLASSOP__SEMI_AUTO:
         astCarData[eCarID].eLearnMode = punDatagram->auc8[1];
         break;
      default:
         break;
   }
   astCarData[eCarID].eAutoState = punDatagram->auc8[2];
   astCarData[eCarID].eRecallState = punDatagram->auc8[3] & 0x07;
   astCarData[eCarID].eCaptureMode = punDatagram->auc8[4] & 0x03;
   astCarData[eCarID].ucFireService = punDatagram->auc8[5];
   astCarData[eCarID].eFire2State = punDatagram->auc8[6];

   astCarData[eCarID].bEP_CarRecalledFlag = (punDatagram->auc8[7] >> 7) & 1;
   astCarData[eCarID].eEP_CommandFeedback = punDatagram->auc8[7] & 0x7F;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UnloadDatagram_CarCalls1( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].auiBF_CarCalls_F[0] = punDatagram->aui32[0];
   astCarData[eCarID].auiBF_CarCalls_R[0] = punDatagram->aui32[1];
}
void UnloadDatagram_CarCalls2( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].auiBF_CarCalls_F[1] = punDatagram->aui32[0];
   astCarData[eCarID].auiBF_CarCalls_R[1] = punDatagram->aui32[1];
}
void UnloadDatagram_CarCalls3( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
#if !ENABLED_REDUCED_MAX_FLOOR_SYSTEM
   astCarData[eCarID].auiBF_CarCalls_F[2] = punDatagram->aui32[0];
   astCarData[eCarID].auiBF_CarCalls_R[2] = punDatagram->aui32[1];
#endif
}

/*-----------------------------------------------------------------------------
 I/O
 -----------------------------------------------------------------------------*/
void UnloadDatagram_InputMap1( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].auiBF_InputMap[0] = punDatagram->aui32[0];
   astCarData[eCarID].auiBF_InputMap[1] = punDatagram->aui32[1];
}
void UnloadDatagram_InputMap2( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].auiBF_InputMap[2] = punDatagram->aui32[0];
   astCarData[eCarID].auiBF_InputMap[3] = punDatagram->aui32[1];
}
void UnloadDatagram_InputMap3( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].auiBF_InputMap[4] = punDatagram->aui32[0];
   astCarData[eCarID].auiBF_InputMap[5] = punDatagram->aui32[1];
}

void UnloadDatagram_OutputMap1( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].auiBF_OutputMap[0] = punDatagram->aui32[0];
   astCarData[eCarID].auiBF_OutputMap[1] = punDatagram->aui32[1];
}
void UnloadDatagram_OutputMap2( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   astCarData[eCarID].auiBF_OutputMap[2] = punDatagram->aui32[0];
   astCarData[eCarID].auiBF_OutputMap[3] = punDatagram->aui32[1];
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t GetLatchedCarCall_Front_ByCar( uint8_t eCarID, uint8_t ucLanding )
{
   uint8_t bLatched = Sys_Bit_Get(&astCarData[eCarID].auiBF_CarCalls_F[0], ucLanding);
   return bLatched;
}
uint8_t GetLatchedCarCall_Rear_ByCar( uint8_t eCarID, uint8_t ucLanding )
{
   uint8_t bLatched = Sys_Bit_Get(&astCarData[eCarID].auiBF_CarCalls_R[0], ucLanding);
   return bLatched;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
en_dispatch_mode GetDispatchMode_ByCar(enum en_group_net_nodes eCarID)
{
   return astCarData[eCarID].enDispatchMode;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
