/******************************************************************************
 *
 * @file     main.c
 * @brief    Program start point and main loop.
 * @version  V1.00
 * @author   Keith Soneda
 * @date     12, June 2018
 *
 * @note     This project is for the DD Manager board.
 *           It receives calls from DD panels and assigns them to available cars.
 *           It is loaded on the SR-3030I B processor.
 *
 *           Note: Uses the same board and configuration as Lib_SRU_B,
 *           with exception of different CAN configurations.
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "board.h"
#include <stdio.h>
#include "sru.h"
#include "sru_b.h"
#include "sys.h"
#include "GlobalData.h"
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_module * gastModules_DDM_B[] =
{
   &gstSys_Mod,  // must include the system module in addition to app modules

   &gstMod_Heartbeat,
   &gstMod_Watchdog,

   &gstMod_ParamEEPROM,
   &gstMod_ParamApp,
   &gstMod_ParamMaster,

   &gstMod_LCD,
   &gstMod_Buttons,
   &gstMod_UI,

   &gstMod_RTC,
   &gstMod_AlarmApp,
   &gstMod_LocalOutputs,

   &gstMod_UART,
   &gstMod_CAN,
   &gstMod_SData,
   &gstMod_SData_AuxNet,
   &gstMod_SData_GroupNet,

   &gstMod_KioskManager,

   &gstMod_ExpansionManager,
   &gstMod_CapacityManager,
};

static struct st_module_control gstModuleControl_DDM_B =
{
   .uiNumModules = sizeof(gastModules_DDM_B) / sizeof(gastModules_DDM_B[ 0 ]),

   .pastModules = &gastModules_DDM_B,

   .enMCU_ID = enMCUB_SRU_UI,

   .enDeployment = enSRU_DEPLOYMENT__CT,
};
static struct st_module * gastModules_DDM_B_Pre[] =
{
         &gstSys_Mod,  // must include the system module in addition to app modules

         &gstMod_ParamEEPROM,
         &gstMod_ParamApp,
         &gstMod_ParamMaster,
};

static struct st_module_control gstModuleControl_DDM_B_Pre =
{
   .uiNumModules = sizeof(gastModules_DDM_B_Pre) / sizeof(gastModules_DDM_B_Pre[ 0 ]),

   .pastModules = &gastModules_DDM_B_Pre,

   .enMCU_ID = enMCUB_SRU_UI,

   .enDeployment = enSRU_DEPLOYMENT__CT,
};

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
void Deployment_Init( void )
{
   uint8_t iError = 1;  // assume error

   en_sru_deployments deplyment = GetSRU_Deployment();

   if ( ( deplyment == enSRU_DEPLOYMENT__CT )
     || ( deplyment == enSRU_DEPLOYMENT__COP ) )
   {
      SetSystemNodeID(SYS_NODE__DDM_B);
      SharedData_GroupNet_Init();
      SharedData_AuxNet_Init();
      SharedData_Init();
      iError = 0;
   }

   if ( iError )
   {
      while ( 1 )
      {
         static uint32_t uiCounter, bState;

         if( uiCounter >= 0xfffff )
         {
            uiCounter = 0;

            bState = ( bState )? 0: 1;

            SRU_Write_LED( enSRU_LED_Heartbeat, bState );
            SRU_Write_LED( enSRU_LED_Alarm,     bState );
            SRU_Write_LED( enSRU_LED_Fault,     bState );
         }
         else
         {
            uiCounter++;
         }
      }
   }
}

/**
 * @brief   main routine for systick example
 * @return   Function should not exit.
 */
int main(void)
{
   /* Generic Initialization */
   SystemCoreClockUpdate();

   Sys_Init();

   MCU_B_Init();

   Deployment_Init();

   __enable_irq();

   // These are the modules that need to be started before the main application.
   // Right now this is just parameters. This is because most modules require
   // parameters and the parameters are not valid till they are loaded into RAM
   Mod_InitAllModules( &gstModuleControl_DDM_B_Pre );
   while(!GetFP_FlashParamsReady())
   {
      Mod_RunOneModule( &gstModuleControl_DDM_B_Pre );
   }

   Mod_InitAllModules( &gstModuleControl_DDM_B );

   // This is really a while(1) loop. Sys_Shutdown() always returns 0. The
   // function is being provided in anticipation of a future time when this
   // application might be running on a PC simulator and we would need a
   // mechanism to terminate this application.
   while ( !Sys_Shutdown() )
   {
       Mod_RunOneModule( &gstModuleControl_DDM_B );
       Debug_CAN_MonitorUtilization();
   }

   return 0;
}


