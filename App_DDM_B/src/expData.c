/******************************************************************************
 * @author   Keith Soneda
 * @version  V1.00
 * @date     6, Sept 2018
 *
 * @note    Tracks the online state of master I/O boards
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "sru.h"
#include "sys.h"

#include <stdint.h>
#include <string.h>
#include "riser.h"
#include "expData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint16_t auwMasterOfflineTimers_ms[NUM_EXP_MASTER_BOARDS] =
{
      RISER_OFFLINE_COUNT_MS__UNUSED,
      RISER_OFFLINE_COUNT_MS__UNUSED,
      RISER_OFFLINE_COUNT_MS__UNUSED,
      RISER_OFFLINE_COUNT_MS__UNUSED,
      RISER_OFFLINE_COUNT_MS__UNUSED,
};

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void ExpData_UpdateMasterOfflineTimers( uint16_t uwRunPeriod_1ms )
{
   for(uint8_t i = 0; i < NUM_EXP_MASTER_BOARDS; i++)
   {
      /* Clear timer of new packet from an IO master has been received */
      if( ABNet_GetDirtyBit(DG_DDMA__ExpInputs1+i, DDM_NODE__A) || ABNet_GetDirtyBit(DG_DDMA__ExpStatus1+i, DDM_NODE__A) )
      {
         ABNet_ClrDirtyBit(DG_DDMA__ExpInputs1+i, DDM_NODE__A);
         ABNet_ClrDirtyBit(DG_DDMA__ExpStatus1+i, DDM_NODE__A);
         auwMasterOfflineTimers_ms[i] = 0;
      }
      else if( auwMasterOfflineTimers_ms[i] < MASTER_IO_OFFLINE_LIMIT_MS )
      {
         auwMasterOfflineTimers_ms[i] += uwRunPeriod_1ms;
      }
   }
}
uint8_t ExpData_GetOnlineFlag( uint8_t ucMasterIndex, uint8_t ucBoardIndex )
{
   uint8_t bOnline = 0;
   if( ( auwMasterOfflineTimers_ms[ucMasterIndex] < MASTER_IO_OFFLINE_LIMIT_MS )
    && ( ucMasterIndex < NUM_EXP_MASTER_BOARDS )
    && ( ucBoardIndex < NUM_EXP_BOARDS_PER_MASTER ) )
   {
      uint8_t ucOnlineBitmap = ABNet_GetDatagram_U8(3, DG_DDMA__ExpStatus1+ucMasterIndex, DDM_NODE__A);
      bOnline = Sys_Bit_Get(&ucOnlineBitmap, ucBoardIndex);
   }
   return bOnline;
}
uint8_t ExpData_GetInputs( uint8_t ucMasterIndex, uint8_t ucBoardIndex )
{
   uint8_t ucInputs = 0;
   if( ( ucMasterIndex < NUM_EXP_MASTER_BOARDS )
    && ( ucBoardIndex < NUM_EXP_BOARDS_PER_MASTER ) )
   {
      ucInputs = ABNet_GetDatagram_U8(ucBoardIndex, DG_DDMA__ExpInputs1+ucMasterIndex, DDM_NODE__A);
   }
   return ucInputs;
}
uint8_t ExpData_GetError( uint8_t ucMasterIndex, uint8_t ucBoardIndex )
{
   uint8_t ucError = 0;
   if( ( ucMasterIndex < NUM_EXP_MASTER_BOARDS )
    && ( ucBoardIndex < NUM_EXP_BOARDS_PER_MASTER ) )
   {
      uint8_t ucByteIndex = 4 + ucBoardIndex/2;
      uint8_t ucShift = 4 * (ucBoardIndex%2);
      ucError = ( ABNet_GetDatagram_U8(ucByteIndex, DG_DDMA__ExpStatus1+ucMasterIndex, DDM_NODE__A) >> ucShift ) & 0x0F;
   }

   return ucError;
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
