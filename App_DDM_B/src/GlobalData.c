#include "GlobalData.h"
#include <stdint.h>
#include <string.h>
#include "sys.h"
#include "sru.h"
#include "mod.h"

/*-----------------------------------------------------------------------------
   Parameters
 -----------------------------------------------------------------------------*/
static uint8_t gbFlashParamsRdy = 0;

inline uint8_t GetFP_FlashParamsReady()
{
   return gbFlashParamsRdy;
}
// Only for mod_param_app
inline void SetFP_FlashParamsReady()
{
   gbFlashParamsRdy = 1;
}

/*-----------------------------------------------------------------------------
   UI Req - Default CMD
 ----------------------------------------------------------------------------*/
static enum_default_cmd eDefaultCMD;
void SetUIRequest_DefaultCommand( enum_default_cmd eCMD )
{
   if( eCMD < NUM_DEFAULT_CMD )
   {
      eDefaultCMD = eCMD;
   }
}
enum_default_cmd GetUIRequest_DefaultCommand()
{
   return eDefaultCMD;
}

/*-----------------------------------------------------------------------------
   Discrete Security Inputs

   Front Security: Master Boards 1-2
   Rear Security:  Master Boards 3-4

   64 = Number of inputs per master expansion set
   8 = Number of inputs per expansion board
 ----------------------------------------------------------------------------*/
#define INPUTS_PER_EXPANSION_MASTER                         (64)
#define INPUTS_PER_EXPANSION_BOARD                          (8)
#define REAR_SECURITY_MASTER_EXPANSION_INDEX_START          (2)
uint8_t GetSecurityInput( uint8_t ucLanding, enum en_doors eDoor )
{
   uint8_t bActive = 0;
   if( ucLanding < MAX_NUM_FLOORS )
   {
      if( eDoor == DOOR_FRONT )
      {
         uint8_t ucMasterIndex = ucLanding / INPUTS_PER_EXPANSION_MASTER;
         uint8_t ucBoardIndex = ( ucLanding % INPUTS_PER_EXPANSION_MASTER ) / INPUTS_PER_EXPANSION_BOARD;
         uint8_t ucBitIndex = ( ucLanding % INPUTS_PER_EXPANSION_MASTER ) % INPUTS_PER_EXPANSION_BOARD;
         uint8_t ucInputs = ExpData_GetInputs(ucMasterIndex, ucBoardIndex);
         bActive = Sys_Bit_Get(&ucInputs, ucBitIndex);
         if( Param_ReadValue_1Bit(DDM_PARAM1__SecurityContactNC) )
         {
            bActive = !bActive;
         }
      }
      else if( eDoor == DOOR_REAR )
      {
         uint8_t ucMasterIndex = ( ucLanding / INPUTS_PER_EXPANSION_MASTER ) + REAR_SECURITY_MASTER_EXPANSION_INDEX_START;
         uint8_t ucBoardIndex = ( ucLanding % INPUTS_PER_EXPANSION_MASTER ) / INPUTS_PER_EXPANSION_BOARD;
         uint8_t ucBitIndex = ( ucLanding % INPUTS_PER_EXPANSION_MASTER ) % INPUTS_PER_EXPANSION_BOARD;
         uint8_t ucInputs = ExpData_GetInputs(ucMasterIndex, ucBoardIndex);
         bActive = Sys_Bit_Get(&ucInputs, ucBitIndex);
         if( Param_ReadValue_1Bit(DDM_PARAM1__SecurityContactNC) )
         {
            bActive = !bActive;
         }
      }
   }

   return bActive;
}
/*----------------------------------------------------------------------------
   Flag signaling that a packet from an new style hall board has been received. Pre SR1060G

   This means the backwards compatible support code should be bypassed and the
   increased floor and hall board address limits should be used.
 *----------------------------------------------------------------------------*/
uint8_t GetHallBoard_MaxNumberOfFloors(void)
{
   if( Param_ReadValue_1Bit(enPARAM1__EnableExtHallBoards) )
   {
      return NEW_HALL_BOARD_MAX_FLOOR_LIMIT;
   }
   else
   {
      return MAX_NUM_FLOORS;
   }
}
uint16_t GetHallBoard_MaxNumberOfBoards(void)
{
   if( Param_ReadValue_1Bit(enPARAM1__EnableExtHallBoards) )
   {
      return MAX_NUM_HALLBOARDS;
   }
   else
   {
      return MAX_NUM_HALLBOARDS_OLD;
   }
}
uint8_t GetHallBoard_NumberOfDIPs(void)
{
   if( Param_ReadValue_1Bit(enPARAM1__EnableExtHallBoards) )
   {
      return NUM_OF_HALLBOARD_DIPS;
   }
   else
   {
      return NUM_OF_HALLBOARD_DIPS_OLD;
   }
}

/*-----------------------------------------------------------------------------
   View Debug Data DDM Requests
 ----------------------------------------------------------------------------*/
static en_view_debug_data_ddm eViewDebugData;
void SetUIRequest_ViewDebugDataCommand( en_view_debug_data_ddm eCommand )
{
   eViewDebugData = eCommand;
}
en_view_debug_data_ddm GetUIRequest_ViewDebugDataCommand(void)
{
   return eViewDebugData;
}
