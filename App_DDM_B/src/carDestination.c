/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief
 * @version  V1.00
 * @date     27, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>

#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "carData.h"
#include "carDestination.h"
#include "hallSecurity.h"
#include "XRegData.h"
#include "XRegDestination.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void ClrProposedDestination_ByCar(enum en_group_net_nodes eCarID);
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static st_carDestination astProposedDestination[MAX_GROUP_CARS];

static st_carDestination astCarDestination[MAX_GROUP_CARS];

static const st_carDestination stClearedCarDestination =
{
  .bDirty = 0,
  .eCallDir = DIR__NONE,
  .eDoor = DOOR_FRONT,
  .ucLanding = INVALID_LANDING,
  .uiMask = 0,
};
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Initialize car data
 -----------------------------------------------------------------------------*/
void Init_CarDestinationStructure()
{
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      ClrProposedDestination_ByCar(i);
      ClrCarDestination_ByCar(i);
      astCarDestination[i].bDirty = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void ClrCarDestination_ByCar(enum en_group_net_nodes eCarID)
{
   if( (astCarDestination[eCarID].eCallDir != stClearedCarDestination.eCallDir)
    || (astCarDestination[eCarID].eDoor != stClearedCarDestination.eDoor)
    || (astCarDestination[eCarID].ucLanding != stClearedCarDestination.ucLanding)
    || (astCarDestination[eCarID].uiMask != stClearedCarDestination.uiMask))
   {
      astCarDestination[eCarID].eCallDir = stClearedCarDestination.eCallDir;
      astCarDestination[eCarID].eDoor = stClearedCarDestination.eDoor;
      astCarDestination[eCarID].ucLanding = stClearedCarDestination.ucLanding;
      astCarDestination[eCarID].uiMask = stClearedCarDestination.uiMask;
      astCarDestination[eCarID].bDirty = 1;
   }
}
static void ClrProposedDestination_ByCar(enum en_group_net_nodes eCarID)
{
   memcpy(&astProposedDestination[eCarID], &stClearedCarDestination, sizeof(st_carDestination));
}
/*-----------------------------------------------------------------------------
   Access functions
 -----------------------------------------------------------------------------*/
st_carDestination * GetCarDestinationStructure(enum en_group_net_nodes eCarID)
{
   return &astCarDestination[eCarID];
}
uint8_t CarDestination_GetLanding(enum en_group_net_nodes eCarID)
{
   return astCarDestination[eCarID].ucLanding;
}
uint32_t CarDestination_GetMask(enum en_group_net_nodes eCarID)
{
   return astCarDestination[eCarID].uiMask;
}
enum en_doors CarDestination_GetDoor(enum en_group_net_nodes eCarID)
{
   return astCarDestination[eCarID].eDoor;
}
enum direction_enum CarDestination_GetDirection(enum en_group_net_nodes eCarID)
{
   return astCarDestination[eCarID].eCallDir;
}

/*-----------------------------------------------------------------------------
   punDatagram->auc8[0] = eCarID;
   punDatagram->auc8[1] = astCarDestination[eCarID].ucLanding;
   punDatagram->auc8[2] = astCarDestination[eCarID].eDoor;
   punDatagram->auc8[3] = astCarDestination[eCarID].eCallDir;
   punDatagram->aui32[1] = astCarDestination[eCarID].uiMask;
 -----------------------------------------------------------------------------*/
void UnloadDatagram_MasterDispatcher( enum en_group_net_nodes eCarID, un_sdata_datagram *punDatagram )
{
   /* If the source of the destination assignment has a lower car ID, then listen to it */
   enum en_group_net_nodes eLocalCarID = GROUP_NET__DDM;
   enum en_group_net_nodes eCommandedCarID = punDatagram->auc8[0];
   if( eCarID < eLocalCarID )
   {
      astCarDestination[eCommandedCarID].ucLanding = punDatagram->auc8[1];
      astCarDestination[eCommandedCarID].eDoor = punDatagram->auc8[2];
      astCarDestination[eCommandedCarID].eCallDir = punDatagram->auc8[3];
      astCarDestination[eCommandedCarID].uiMask = punDatagram->aui32[1];
   }
}
/*-----------------------------------------------------------------------------
todo revisit cost of checks
 -----------------------------------------------------------------------------*/
uint8_t GetLatchedHallCallUp_Front_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding )
{
   uint8_t bLatched = 0;
   uint8_t bValidOpening = CarData_CheckOpening( eCarID, ucLanding, DOOR_FRONT );
   uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__UP );
   if( bValidOpening )
   {
      // mask out calls already assigned
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         st_carDestination *pstCarDestination = GetCarDestinationStructure( i );
         if( ( i != eCarID )
          && ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( pstCarDestination->ucLanding == ucLanding )
          && ( pstCarDestination->eCallDir == DIR__UP )
          && ( pstCarDestination->eDoor != DOOR_REAR ) )
         {
            uiLatchedCalls &= ~(pstCarDestination->uiMask);
         }
      }
      // Mask out XREG calls already assigned
      uint8_t ucNumXRegCars = Param_ReadValue_8Bit(enPARAM8__NumXRegCars);
      for(uint8_t i = 0; i < ucNumXRegCars; i++)
      {
         if( ( XRegData_GetCarActive(i) )
          && ( XRegDestination_GetLanding(i) == ucLanding )
          && ( XRegDestination_GetDirection(i) == DIR__UP )
          && ( XRegDestination_GetDoor(i) != DOOR_REAR ) )
         {
            uiLatchedCalls &= ~(XRegDestination_GetMask(i));
         }
      }

      // Look only at calls this car can take
      uiLatchedCalls &= CarData_GetHallMask(eCarID, DOOR_FRONT);
      if( uiLatchedCalls
       && ( ( GetHallCallSecurity(eCarID, ucLanding, DOOR_FRONT, uiLatchedCalls) )
         || ( uiLatchedCalls & CarData_GetUniqueHallMask_Front(eCarID) ) ) ) /* Uniquely assigned hall calls exempt from security check */
      {
         bLatched = 1;
      }
   }
   return bLatched;
}
uint8_t GetLatchedHallCallUp_Rear_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding )
{
   uint8_t bLatched = 0;
   uint8_t bValidOpening = CarData_CheckOpening( eCarID, ucLanding, DOOR_REAR );
   uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__UP );
   if( bValidOpening )
   {
      // mask out calls already assigned
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         st_carDestination *pstCarDestination = GetCarDestinationStructure( i );
         if( ( i != eCarID )
          && ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( pstCarDestination->ucLanding == ucLanding )
          && ( pstCarDestination->eCallDir == DIR__UP )
          && ( pstCarDestination->eDoor != DOOR_FRONT ) )
         {
            uiLatchedCalls &= ~(pstCarDestination->uiMask);
         }
      }
      // Mask out XREG calls already assigned
      uint8_t ucNumXRegCars = Param_ReadValue_8Bit(enPARAM8__NumXRegCars);
      for(uint8_t i = 0; i < ucNumXRegCars; i++)
      {
         if( ( XRegData_GetCarActive(i) )
          && ( XRegDestination_GetLanding(i) == ucLanding )
          && ( XRegDestination_GetDirection(i) == DIR__UP )
          && ( XRegDestination_GetDoor(i) != DOOR_FRONT ) )
         {
            uiLatchedCalls &= ~(XRegDestination_GetMask(i));
         }
      }

      // Look only at calls this car can take
      uiLatchedCalls &= CarData_GetHallMask(eCarID, DOOR_REAR);
      if( uiLatchedCalls
       && ( ( GetHallCallSecurity(eCarID, ucLanding, DOOR_REAR, uiLatchedCalls) )
         || ( uiLatchedCalls & CarData_GetUniqueHallMask_Rear(eCarID) ) ) ) /* Uniquely assigned hall calls exempt from security check */
      {
         bLatched = 1;
      }
   }
   return bLatched;
}
uint8_t GetLatchedHallCallDown_Front_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding )
{
   uint8_t bLatched = 0;
   uint8_t bValidOpening = CarData_CheckOpening( eCarID, ucLanding, DOOR_FRONT );
   uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__DOWN );
   if( bValidOpening )
   {
      // mask out calls already assigned
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         st_carDestination *pstCarDestination = GetCarDestinationStructure( i );
         if( ( i != eCarID )
          && ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( pstCarDestination->ucLanding == ucLanding )
          && ( pstCarDestination->eCallDir == DIR__DN )
          && ( pstCarDestination->eDoor != DOOR_REAR ) )
         {
            uiLatchedCalls &= ~(pstCarDestination->uiMask);
         }
      }
      // Mask out XREG calls already assigned
      uint8_t ucNumXRegCars = Param_ReadValue_8Bit(enPARAM8__NumXRegCars);
      for(uint8_t i = 0; i < ucNumXRegCars; i++)
      {
         if( ( XRegData_GetCarActive(i) )
          && ( XRegDestination_GetLanding(i) == ucLanding )
          && ( XRegDestination_GetDirection(i) == DIR__DN )
          && ( XRegDestination_GetDoor(i) != DOOR_REAR ) )
         {
            uiLatchedCalls &= ~(XRegDestination_GetMask(i));
         }
      }

      // Look only at calls this car can take
      uiLatchedCalls &= CarData_GetHallMask(eCarID, DOOR_FRONT);
      if( uiLatchedCalls
       && ( ( GetHallCallSecurity(eCarID, ucLanding, DOOR_FRONT, uiLatchedCalls) )
         || ( uiLatchedCalls & CarData_GetUniqueHallMask_Front(eCarID) ) ) ) /* Uniquely assigned hall calls exempt from security check */
      {
         bLatched = 1;
      }
   }
   return bLatched;
}
uint8_t GetLatchedHallCallDown_Rear_ByCar( enum en_group_net_nodes eCarID, uint8_t ucLanding )
{
   uint8_t bLatched = 0;
   uint8_t bValidOpening = CarData_CheckOpening( eCarID, ucLanding, DOOR_REAR );
   uint32_t uiLatchedCalls = GetRawLatchedHallCalls( ucLanding, HC_DIR__DOWN );
   if( bValidOpening )
   {
      // mask out calls already assigned
      for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
      {
         st_carDestination *pstCarDestination = GetCarDestinationStructure( i );
         if( ( i != eCarID )
          && ( GetCarOfflineTimer_ByCar(i) < GROUP_CAR_OFFLINE_TIMER_MS )
          && ( pstCarDestination->ucLanding == ucLanding )
          && ( pstCarDestination->eCallDir == DIR__DN )
          && ( pstCarDestination->eDoor != DOOR_FRONT ) )
         {
            uiLatchedCalls &= ~(pstCarDestination->uiMask);
         }
      }
      // Mask out XREG calls already assigned
      uint8_t ucNumXRegCars = Param_ReadValue_8Bit(enPARAM8__NumXRegCars);
      for(uint8_t i = 0; i < ucNumXRegCars; i++)
      {
         if( ( XRegData_GetCarActive(i) )
          && ( XRegDestination_GetLanding(i) == ucLanding )
          && ( XRegDestination_GetDirection(i) == DIR__DN )
          && ( XRegDestination_GetDoor(i) != DOOR_FRONT ) )
         {
            uiLatchedCalls &= ~(XRegDestination_GetMask(i));
         }
      }

      // Look only at calls this car can take
      uiLatchedCalls &= CarData_GetHallMask(eCarID, DOOR_REAR);
      if( uiLatchedCalls
       && ( ( GetHallCallSecurity(eCarID, ucLanding, DOOR_REAR, uiLatchedCalls) )
         || ( uiLatchedCalls & CarData_GetUniqueHallMask_Rear(eCarID) ) ) ) /* Uniquely assigned hall calls exempt from security check */
      {
         bLatched = 1;
      }
   }
   return bLatched;
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
