/******************************************************************************
 *
 * @file     remoteCommands.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     13 August 2019
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include "mod.h"
#include "remoteCommands.h"
#include "carData.h"
#include "GlobalData.h"
#include <stdint.h>
#include <string.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   VIRTUAL_CARCALL_SECURITY,
   VIRTUAL_HALLCALL_SECURITY,
   VIRTUAL_SYSTEM_INPUTS,

   NUM_VIRTUAL_INPUTS_TYPES,
} en_virtual_input_types;

typedef enum
{
   REMOTE_DISPATCH_MODE,
   REMOTE_CARCALL_SECURITY_F0,
   REMOTE_CARCALL_SECURITY_F1,
   REMOTE_CARCALL_SECURITY_F2,

   REMOTE_CARCALL_SECURITY_R0,
   REMOTE_CARCALL_SECURITY_R1,
   REMOTE_CARCALL_SECURITY_R2,

   REMOTE_HALLCALL_SECURITY_F0,
   REMOTE_HALLCALL_SECURITY_F1,
   REMOTE_HALLCALL_SECURITY_F2,

   REMOTE_HALLCALL_SECURITY_R0,
   REMOTE_HALLCALL_SECURITY_R1,
   REMOTE_HALLCALL_SECURITY_R2,

   REMOTE_SYSTEM_INPUTS,

   NUM_REMOTE_DATA
} en_remote_data;
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static en_dispatch_mode enActiveDispatchCommand = DISPATCH_MODE__NONE;

static uint32_t auiBF_Virtual_CarCallSecurity[ NUM_OF_DOORS ][BITMAP32_SIZE(MAX_NUM_FLOORS)];
static uint32_t auiBF_Virtual_HallCallSecurity[ NUM_DOOR_STATES ][BITMAP32_SIZE(MAX_NUM_FLOORS)];
static uint32_t uiBF_Virtual_SysInputs;
/*
    static uint32_t auiBF_Virtual_SysInputs[ BITMAP32_SIZE ( NUM_VIRTUAL_INPUTS ) ];
    Use if number of virtual system inputs becomes greater than 32
*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 * Sets virtual car call security enable bitmaps
 *----------------------------------------------------------------------------*/
void SetVirtualCarCallSecurity(enum en_doors enDoorIndex, uint8_t ucMapIndex, uint32_t uiValue)
{
   if((enDoorIndex < NUM_OF_DOORS) && (ucMapIndex < BITMAP32_SIZE(MAX_NUM_FLOORS)))
   {
      auiBF_Virtual_CarCallSecurity[enDoorIndex][ucMapIndex] = uiValue;
   }
}

uint32_t GetVirtualCarCallSecurity(enum en_doors enDoorIndex, uint8_t ucMapIndex)
{
   return auiBF_Virtual_CarCallSecurity[enDoorIndex][ucMapIndex];
}
/*----------------------------------------------------------------------------
 * Sets virtual hall call security enable bitmaps
 *----------------------------------------------------------------------------*/
void SetVirtualHallCallSecurity(enum en_doors enDoorIndex, uint8_t ucMapIndex, uint32_t uiValue)
{
   if((enDoorIndex < NUM_OF_DOORS) && (ucMapIndex < BITMAP32_SIZE(MAX_NUM_FLOORS)))
   {
      auiBF_Virtual_HallCallSecurity[enDoorIndex][ucMapIndex] = uiValue;
   }
}
/*----------------------------------------------------------------------------
 * Sets virtual system inputs bitmap. Modify if virtual sys inputs
 * grow past 32
 *----------------------------------------------------------------------------*/
void SetVirtualSystemInputs(uint32_t uiValue)
{
   uiBF_Virtual_SysInputs = uiValue;
}
/*----------------------------------------------------------------------------
 * Sets remote dispatch mode based on MRM command. Remote command will be OR
 * with the physical dispatch enables.
 *----------------------------------------------------------------------------*/
void RemoteCommand_SetDispatchMode(en_dispatch_mode enDispatchMode)
{
   if(enDispatchMode < NUM_DISPATCH_MODES)
   {
      enActiveDispatchCommand = enDispatchMode;
   }
}
/*----------------------------------------------------------------------------
 * Sets dynamic/predictive parking floor 
 *----------------------------------------------------------------------------*/
void RemoteCommand_SetParkingFloor(uint8_t ucFloor)
{

}
/*----------------------------------------------------------------------------
 * Updates parameter when a group parameter value changes on another car
 *----------------------------------------------------------------------------*/
void RemoteCommand_SetGroupParameter(enum en_param_block_types eParamType, uint16_t uwParamIndex, uint32_t uiParamValue)
{

}

/*----------------------------------------------------------------------------
 * Sets virtual input bitmap
 *----------------------------------------------------------------------------*/
void RemoteCommand_SetVirtualInputs( un_sdata_datagram *punDatagram)
{
   en_virtual_input_types enVirtualInputType = punDatagram->auc8[1];

   enum en_doors enDoorIndex = punDatagram->auc8[2];
   uint8_t ucMapIndex = punDatagram->auc8[3];
   uint32_t uiBF_Value = punDatagram->aui32[1];

   switch(enVirtualInputType)
   {
      case VIRTUAL_CARCALL_SECURITY:
         SetVirtualCarCallSecurity(enDoorIndex, ucMapIndex, uiBF_Value);
         break;
      case VIRTUAL_HALLCALL_SECURITY:
         SetVirtualHallCallSecurity(enDoorIndex, ucMapIndex, uiBF_Value);
         break;
      case VIRTUAL_SYSTEM_INPUTS:
         // Modify if virtual system inputs grow past 32
         SetVirtualSystemInputs(uiBF_Value);
         break;
      default:
         break;
   }
}
/*----------------------------------------------------------------------------
 * Returns current remote dispatch mode command
 *----------------------------------------------------------------------------*/
en_dispatch_mode RemoteCommand_GetDispatchMode(void)
{
   return enActiveDispatchCommand;
}

/*----------------------------------------------------------------------------
 * Reset all virtual inputs bitmaps and remote dispatch mode
 *----------------------------------------------------------------------------*/
void Reset_Virtual_Inputs(void)
{
   enActiveDispatchCommand = DISPATCH_MODE__NONE;

   memset(&auiBF_Virtual_CarCallSecurity[0], 0, sizeof(auiBF_Virtual_CarCallSecurity[0]));
   memset(&auiBF_Virtual_CarCallSecurity[1], 0, sizeof(auiBF_Virtual_CarCallSecurity[1]));

   memset(&auiBF_Virtual_HallCallSecurity[0], 0, sizeof(auiBF_Virtual_HallCallSecurity[0]));
   memset(&auiBF_Virtual_HallCallSecurity[1], 0, sizeof(auiBF_Virtual_HallCallSecurity[1]));

   memset(&uiBF_Virtual_SysInputs, 0, sizeof(uiBF_Virtual_SysInputs));

}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

