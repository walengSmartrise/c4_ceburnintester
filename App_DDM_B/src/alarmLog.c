/******************************************************************************
 *
 * @file     alarmLog.c
 * @brief
 * @version  V1.00
 * @date     13, June 2018
 * @author   Keith Soneda
 *
 * @note     Functions for maintaining and accessing a running log of DDM board faults. These faults are lost on reset.
 *
 *****************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <alarmLog.h>
#include "mod.h"
#include <string.h>
#include <math.h>
#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "alarmLog.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static en_ddm_alarms aeAlarmLog[NUM_ALARM_LOG_ELEMENTS];
static uint32_t auiTimestampLog[NUM_ALARM_LOG_ELEMENTS];
static uint8_t ucCurrentIndex;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void AlarmLog_ClearLog(void)
{
   for(uint8_t i = 0; i < NUM_ALARM_LOG_ELEMENTS; i++)
   {
      aeAlarmLog[i] = 0;
      auiTimestampLog[i] = 0;
   }
}
/*----------------------------------------------------------------------------
Log access functions. Where (ucIndex == 0) is the newest element in the log
 *----------------------------------------------------------------------------*/
en_ddm_alarms AlarmLog_GetAlarm(uint8_t ucIndex)
{
   uint8_t ucLogIndex = ( ucCurrentIndex + ( NUM_ALARM_LOG_ELEMENTS - ucIndex - 1 ) ) % NUM_ALARM_LOG_ELEMENTS;
   return aeAlarmLog[ucLogIndex];
}
uint32_t AlarmLog_GetTimestamp(uint8_t ucIndex)
{
   uint8_t ucLogIndex = ( ucCurrentIndex + ( NUM_ALARM_LOG_ELEMENTS - ucIndex - 1 ) ) % NUM_ALARM_LOG_ELEMENTS;
   return auiTimestampLog[ucLogIndex];
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void AlarmLog_UpdateLog(void)
{
   static en_ddm_alarms aeLastAlarm[NUM_DDM_NODES];

   for(uint8_t i = 0; i < NUM_DDM_NODES; i++)
   {
      en_ddm_alarms eAlarm = GetAlarm_ByNode(i);
      if( ( eAlarm > DDM_ALM__NONE )
       && ( eAlarm != aeLastAlarm[i] ) )
      {
         aeAlarmLog[ucCurrentIndex] = eAlarm;
         auiTimestampLog[ucCurrentIndex] = GetAlarm_TimeStamp(i);
         ucCurrentIndex = ( ucCurrentIndex + 1 ) % NUM_ALARM_LOG_ELEMENTS;
      }
      aeLastAlarm[i] = eAlarm;
   }

}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
