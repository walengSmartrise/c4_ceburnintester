
/******************************************************************************
 *
 * @file     XRegDestination.c
 * @brief
 * @version  V1.00
 * @date     15, May 2018
 *
 * @note    Functions for assigning XReg destinations
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>

#include "sru_b.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "carData.h"
#include "carDestination.h"
#include "XRegData.h"
#include "XRegDestination.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void ClrDestination_ByCar(uint8_t ucCarIndex);
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static st_XRegDestination astXRegCarDestination[MAX_GROUP_CARS];

static const st_XRegDestination stClearedCarDestination =
{
  .ucSendCountdown = 0,
  .eCallDir = DIR__NONE,
  .eDoor = DOOR_FRONT,
  .ucLanding = INVALID_LANDING,
  .bCallAccepted = 0,
};
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Initialize car data
 -----------------------------------------------------------------------------*/
void Init_XRegDestinationStructure()
{
   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      ClrDestination_ByCar(i);
      astXRegCarDestination[i].ucSendCountdown = 0;
      astXRegCarDestination[i].uiTimeout_ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ClrDestination_ByCar( uint8_t ucCarIndex )
{
   if( (astXRegCarDestination[ucCarIndex].eCallDir != stClearedCarDestination.eCallDir)
    || (astXRegCarDestination[ucCarIndex].eDoor != stClearedCarDestination.eDoor)
    || (astXRegCarDestination[ucCarIndex].ucLanding != stClearedCarDestination.ucLanding) )
   {
      astXRegCarDestination[ucCarIndex].eCallDir = stClearedCarDestination.eCallDir;
      astXRegCarDestination[ucCarIndex].eDoor = stClearedCarDestination.eDoor;
      astXRegCarDestination[ucCarIndex].ucLanding = stClearedCarDestination.ucLanding;
      astXRegCarDestination[ucCarIndex].ucSendCountdown = XREG_DESTINATION_RESEND_COUNT;
   }
   astXRegCarDestination[ucCarIndex].uiTimeout_ms = 0;
}

/*-----------------------------------------------------------------------------
   Access functions
 -----------------------------------------------------------------------------*/
st_XRegDestination * GetXRegDestinationStructure( uint8_t ucCarIndex )
{
   return &astXRegCarDestination[ucCarIndex];
}
uint8_t XRegDestination_GetLanding( uint8_t ucCarIndex )
{
   return astXRegCarDestination[ucCarIndex].ucLanding;
}
enum en_doors XRegDestination_GetDoor( uint8_t ucCarIndex )
{
   return astXRegCarDestination[ucCarIndex].eDoor;
}
enum direction_enum XRegDestination_GetDirection( uint8_t ucCarIndex )
{
   return astXRegCarDestination[ucCarIndex].eCallDir;
}
uint32_t XRegDestination_GetMask( uint8_t ucCarIndex )
{
   return astXRegCarDestination[ucCarIndex].uiMask;
}
uint8_t XRegDestination_GetCallAccepted( uint8_t ucCarIndex )
{
   return astXRegCarDestination[ucCarIndex].bCallAccepted;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void XRegDestination_ClrSendCountdown( uint8_t ucCarIndex )
{
   astXRegCarDestination[ucCarIndex].ucSendCountdown = 0;
}
void XRegDestination_SetCallAccepted( uint8_t ucCarIndex )
{
   astXRegCarDestination[ucCarIndex].bCallAccepted = 1;
}

uint32_t XRegDestination_GetTimeout( uint8_t ucCarIndex )
{
   return astXRegCarDestination[ucCarIndex].uiTimeout_ms;
}
void XRegDestination_IncrementTimeout( uint8_t ucCarIndex, uint16_t uwRunPeriod_ms )
{
   astXRegCarDestination[ucCarIndex].uiTimeout_ms += uwRunPeriod_ms;
}
void XRegDestination_ClrTimeout( uint8_t ucCarIndex )
{
   astXRegCarDestination[ucCarIndex].uiTimeout_ms = 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
