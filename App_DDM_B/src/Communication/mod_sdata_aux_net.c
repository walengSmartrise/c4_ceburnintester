/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief
 * @version  V1.00
 * @date     26, March 2016
 *
 * @note     For C.E. Elite Touch v1 Kiosk
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sru_b.h"
#include "sys.h"
#include "GlobalData.h"
#include "kioskData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init(struct st_module *pstThisModule);
static uint32_t Run(struct st_module *pstThisModule);

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_SData_AuxNet =
{ .pfnInit = Init, .pfnRun = Run, };

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
/* Below are all the nodes in Hall Network */
static struct st_sdata_control gstSData_AuxNet_SHIELD;
static const uint8_t ucNumDatagrams = NUM_AuxNet_SHIELD_DATAGRAMS;
static CAN_MSG_T gstCAN_MSG_Tx;


struct st_sdata_local gstAuxNetSData_Config;


/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SharedData_AuxNet_Init()
{
   int iError;

   gstAuxNetSData_Config.ucLocalNodeID = SHIELD_AUX_NET__SHIELD;

   gstAuxNetSData_Config.ucNetworkID = SDATA_NET__AUX;

   gstAuxNetSData_Config.ucNumNodes = NUM_SHIELD_AUX_NET_NODES;

   gstAuxNetSData_Config.ucDestNodeID = 0x1F;
   iError = SDATA_CreateDatagrams( &gstSData_AuxNet_SHIELD,
                                    ucNumDatagrams );

   if ( iError )
   {
      while ( 1 )
      {
         // TODO: unable to allocate shared data
         SRU_Write_LED( enSRU_LED_Fault, 1 );
      }
   }

   gstCAN_MSG_Tx.DLC = 8;
}
/*-----------------------------------------------------------------------------
   Returns CAN bus message ID from local datagram enum en_aux_net_shield_datagrams
 -----------------------------------------------------------------------------*/
static uint32_t GetMessageIDFromLocalDatagramID( uint8_t ucDatagramID )
{
   uint32_t uiID = 0;
   // For now, address all devices
   en_kiosk_device_id ucDeviceID = KIOSK_DEVICE_ID__SCREEN;
   en_kiosk_location_id ucLocationID = KIOSK_LOCATION_ID__ALL;
   uint8_t ucFloorID = KIOSK_FLOOR_NUM__ALL;
   uint8_t ucSubDeviceID = 0;
   en_kiosk_msg_id ucMessageID = 0;
   gstCAN_MSG_Tx.DLC = 8;
   if( ucDatagramID == DG_AuxNet_SHIELD__CarOperation )
   {
      ucMessageID = KIOSK_MSG_ID__CAR_OP;
      ucFloorID = KIOSK_FLOOR_NUM__ALL;
   }
   else if( ucDatagramID == DG_AuxNet_SHIELD__CallResponse )
   {
      ucMessageID = KIOSK_MSG_ID__HALL_RESP;
      ucFloorID = DDPanel_GetActiveResponseFloor();
      if( !Param_ReadValue_1Bit(DDM_PARAM1__EnableDuparPanel) )
      {
         gstCAN_MSG_Tx.DLC = ( DDPanel_GetLastCallAccepted() ) ? 2:4;
      }
      ucLocationID = DDPanel_GetLastLocation();
   }
   else if( ucDatagramID == DG_AuxNet_SHIELD__Timeouts )
   {
      ucMessageID = KIOSK_MSG_ID__TIMEOUTS;

   }
   else if( ucDatagramID == DG_AuxNet_SHIELD__ButtonState )
   {
      ucMessageID = KIOSK_MSG_ID__BUTTON;
   }
   else if( ucDatagramID == DG_AuxNet_SHIELD__OpMode )
   {
      ucMessageID = KIOSK_MSG_ID__OP_MODE;
   }

   uiID = CAN_EXTEND_ID_USAGE
         | (ucDeviceID << 26)
         | (ucMessageID << 21)
         | (ucLocationID << 16)
         | (ucFloorID << 8)
         | (ucSubDeviceID << 4);

   return uiID;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Receive(void)
{
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Transmit(void)
{
   un_sdata_datagram unDatagram;
   int iError = 0;
   uint8_t ucDatagramID;

   ucDatagramID = SDATA_GetNextDatagramToSendIndex_Plus1(&gstSData_AuxNet_SHIELD);

   if ( ucDatagramID && ucDatagramID <= ucNumDatagrams )
   {
      ucDatagramID--;
      SDATA_ReadDatagram( &gstSData_AuxNet_SHIELD, ucDatagramID,
               &unDatagram );

      memcpy( gstCAN_MSG_Tx.Data, unDatagram.auc8,
               sizeof( unDatagram.auc8 ) );

      gstCAN_MSG_Tx.ID = GetMessageIDFromLocalDatagramID( ucDatagramID );

      /* Update packet length by message type */
      if( ucDatagramID == DG_AuxNet_SHIELD__CallResponse )
      {
         if(unDatagram.auc8[0] == KIOSK_CALL_RESP__ACCEPTED)
         {
            gstCAN_MSG_Tx.DLC = 2;
         }
         else
         {
            gstCAN_MSG_Tx.DLC = 4;
         }
      }
      else
      {
         gstCAN_MSG_Tx.DLC = 8;
      }

      CAN_BUFFER_ID_T   TxBuf = Chip_CAN_GetFreeTxBuf(LPC_CAN2);
      iError =  !Chip_CAN_Send( LPC_CAN2, TxBuf, &gstCAN_MSG_Tx);

      if ( iError )
      {
         SDATA_DirtyBit_Set( &gstSData_AuxNet_SHIELD, ucDatagramID );
         if ( gstSData_AuxNet_SHIELD.uwLastSentIndex )
         {
            --gstSData_AuxNet_SHIELD.uwLastSentIndex;
         }
         else
         {
            gstSData_AuxNet_SHIELD.uwLastSentIndex =
                     gstSData_AuxNet_SHIELD.uiNumDatagrams - 1;
         }
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadData(void)
{
}
/*-----------------------------------------------------------------------------
3.4   Destination Entry Kiosk Timeouts
Message Id = 0x04
Source: Dispatcher

Parameter sent to the DED for configuration of timeouts associated with screens and key press entries.

Byte  Bits  Description
1     Normal Screen Timeout (1/4 second Interval)
2     Assisted Screen Timeout (1/4 second Interval)
3     Key Press Entry Timeout (1/4 second Interval)
4     Key Press Assisted Timeout (1/4 second Interval)
5     Code Entry Timeout (1/4 second interval)
6     Assisted Code Entry Timeout (1/4 second interval)
7     Dispatcher Response Timeout (1/4 second interval)
8     Reserved
 -----------------------------------------------------------------------------*/
static void LoadDatagram_KioskTimeouts_Aux()
{
   un_sdata_datagram unDatagram;
   if(!SDATA_DirtyBit_Get(&gstSData_AuxNet_SHIELD, DG_AuxNet_SHIELD__Timeouts))
   {
      if( DDPanel_LoadDatagram_Timeouts(&unDatagram) )
      {
         SDATA_WriteDatagram(&gstSData_AuxNet_SHIELD, DG_AuxNet_SHIELD__Timeouts, &unDatagram);
         SDATA_DirtyBit_Set(&gstSData_AuxNet_SHIELD, DG_AuxNet_SHIELD__Timeouts);
      }
   }
}
/*-----------------------------------------------------------------------------
Byte  Bits     Description
1              Status of Call Request
               0x05 = COP Call Accepted (Call Accepted)

2              Car Id Assigned to Passenger (0x00 – 0xFE)

3              Floor Requested

4     0        Exit Door from Car
               0 = Front Door Destination
               1 = Rear Door Destination
4     1 – 7    Call Type Requested
               0 = Normal Call
5 – 8    Reserved
 -----------------------------------------------------------------------------*/
static void LoadDatagram_CallResponse_Aux()
{
   un_sdata_datagram unDatagram;
   if(!SDATA_DirtyBit_Get(&gstSData_AuxNet_SHIELD, DG_AuxNet_SHIELD__CallResponse))
   {
      if( DDPanel_LoadDatagram_CallResponse(&unDatagram) )
      {
         SDATA_WriteDatagram(&gstSData_AuxNet_SHIELD, DG_AuxNet_SHIELD__CallResponse, &unDatagram);
         SDATA_DirtyBit_Set(&gstSData_AuxNet_SHIELD, DG_AuxNet_SHIELD__CallResponse);
      }
   }
}
/*-----------------------------------------------------------------------------
   Direct CAN Communication Protocol for Elite Touch Devices for New Customers - 3.1
 -----------------------------------------------------------------------------*/
static void LoadDatagram_CarOperation_Aux()
{
   static uint8_t bTimeoutMessageSent = 1;
   static uint16_t uwResendCounter_ms;
   un_sdata_datagram unDatagram;

   /* If panel parameters just update, send a parameter updated status message */
   if( SDATA_DirtyBit_Get(&gstSData_AuxNet_SHIELD, DG_AuxNet_SHIELD__Timeouts) )
   {
      bTimeoutMessageSent = 0;
   }

   if( !bTimeoutMessageSent
    && !SDATA_DirtyBit_Get(&gstSData_AuxNet_SHIELD, DG_AuxNet_SHIELD__Timeouts) )
   {
      // Send message confirming startup parameters have been sent
      if( !SDATA_DirtyBit_Get(&gstSData_AuxNet_SHIELD, DG_AuxNet_SHIELD__CarOperation) )
      {
         bTimeoutMessageSent = 1;
         uwResendCounter_ms = MIN_DATAGRAM_RESEND_IN_MS__WATCHDOG; // Make sure regular watchdog wasn't interrupted

         memset(&unDatagram, 0, sizeof(un_sdata_datagram));
         unDatagram.auc8[0] = KIOSK_STATUS_CMD__DIS_RES;
         unDatagram.auc8[1] = DDPanel_GetOperationMode();

         SDATA_WriteDatagram(&gstSData_AuxNet_SHIELD, DG_AuxNet_SHIELD__CarOperation, &unDatagram);
         SDATA_DirtyBit_Set(&gstSData_AuxNet_SHIELD, DG_AuxNet_SHIELD__CarOperation);
      }
   }
   /* Otherwise,send regular watchdog status message */
   else if(!SDATA_DirtyBit_Get(&gstSData_AuxNet_SHIELD, DG_AuxNet_SHIELD__CarOperation))
   {
      if( uwResendCounter_ms < MIN_DATAGRAM_RESEND_IN_MS__WATCHDOG )
      {
         uwResendCounter_ms += MOD_RUN_PERIOD_AUX_NET_1MS;
      }
      else
      {
         memset(&unDatagram, 0, sizeof(un_sdata_datagram));

         unDatagram.auc8[0] = KIOSK_STATUS_CMD__DIS_WD;
         unDatagram.auc8[1] = DDPanel_GetOperationMode();

         SDATA_WriteDatagram(&gstSData_AuxNet_SHIELD, DG_AuxNet_SHIELD__CarOperation, &unDatagram);
         SDATA_DirtyBit_Set(&gstSData_AuxNet_SHIELD, DG_AuxNet_SHIELD__CarOperation);
         uwResendCounter_ms = 0;
      }
   }
   else
   {
      uwResendCounter_ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData(void)
{
   LoadDatagram_KioskTimeouts_Aux();

   LoadDatagram_CallResponse_Aux();

   LoadDatagram_CarOperation_Aux();

}

/*-----------------------------------------------------------------------------

 Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init(struct st_module *pstThisModule)
{

   //------------------------------------------------
   pstThisModule->uwInitialDelay_1ms = 5000;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_AUX_NET_1MS;

   /* Disable auto resend */
   gstSData_AuxNet_SHIELD.bDisableResend = 1;
   gstSData_AuxNet_SHIELD.bDisableHeartbeat = 1;

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run(struct st_module *pstThisModule)
{
   Receive();
   UnloadData();

   LoadData();
   Transmit();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
