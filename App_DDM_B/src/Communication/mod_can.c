/******************************************************************************
 *
 * @file
 * @brief
 * @version  V1.00
 * @date
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "sru_b.h"
#include "sys.h"

#include <string.h>
#include "GlobalData.h"
#include "ring_buffer.h"
#include "carData.h"
#include "groupnet_datagrams.h"
#include "groupnet.h"
#include "carDestination.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_CAN =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define LIMIT_UNLOAD_CAN_CYCLES       (32)

#define EXTRACT_NET_ID(x)      (( x >> 25 ) & 0x0F )
#define EXTRACT_SRC_ID(x)      (( x >> 20 ) & 0x1F )
#define EXTRACT_DEST_ID(x)     (( x >> 15 ) & 0x1F )
#define EXTRACT_DATAGRAM_ID(x) (( x >> 10 ) & 0x1F )

/* Since there won't always be a device communicating on CAN2,
 * initialize the offline timer with a starter value and suppress
 * communication errors for the bus if not communication was ever detected */
#define OFFLINE_TIMER_COM_NOT_DETECTED          (0xFFFF)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *-----------------------------------------------------------------------------*/
static uint16_t uwOfflineTimeout_CAN1_5ms = OFFLINE_TIMER_COM_NOT_DETECTED;
static uint16_t uwOfflineTimeout_CAN2_5ms = OFFLINE_TIMER_COM_NOT_DETECTED;
static uint16_t uwBusErrorCounter_CAN1;
static uint16_t uwBusErrorCounter_CAN2;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Returns bus error counter
 -----------------------------------------------------------------------------*/
uint16_t GetDebugBusOfflineCounter_CAN1()
{
   return uwBusErrorCounter_CAN1;
}
uint16_t GetDebugBusOfflineCounter_CAN2()
{
   return uwBusErrorCounter_CAN2;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ReceiveGroupCarDatagram(enum en_group_net_nodes eCarID,
                                    en_group_net_mrb_datagrams eLocalDatagramID,
                                    un_sdata_datagram *punDatagram)
{
   ClrCarOfflineTimer_ByCar(eCarID);
   switch(eLocalDatagramID)
   {
      case DG_GroupNet_MRB__MappedInputs1:
         UnloadDatagram_InputMap1(eCarID, punDatagram);
         break;
      case DG_GroupNet_MRB__MappedInputs2:
         UnloadDatagram_InputMap2(eCarID, punDatagram);
         break;
      case DG_GroupNet_MRB__MappedInputs3:
         UnloadDatagram_InputMap3(eCarID, punDatagram);
         break;
      case DG_GroupNet_MRB__MappedOutputs1:
         UnloadDatagram_OutputMap1(eCarID, punDatagram);
         break;
      case DG_GroupNet_MRB__MappedOutputs2:
         UnloadDatagram_OutputMap2(eCarID, punDatagram);
         break;
      case DG_GroupNet_MRB__CAR_STATUS:
         UnloadDatagram_CarStatus(eCarID, punDatagram);
         break;
      case DG_GroupNet_MRB__CarData:
         UnloadDatagram_CarData(eCarID, punDatagram);
         break;
      case DG_GroupNet_MRB__CarOperation:
         UnloadDatagram_CarOperation(eCarID, punDatagram);
         break;
      case DG_GroupNet_MRB__MasterDispatcher:
         UnloadDatagram_MasterDispatcher(eCarID, punDatagram);
         break;
      case DG_GroupNet_MRB__MasterCommand:
         UnloadDatagram_MasterCommand(eCarID, punDatagram);
         break;

      default:
         break;
   }
}
/*
 * Riser board commands
 */
uint8_t RecieveRiserCommand( uint8_t ucRiser, uint8_t ucDatagramID, un_sdata_datagram *unDatagram )
{
   uint8_t bFailed = 1;
   if( ucRiser < MAX_NUM_RISER_BOARDS )
   {
      switch (ucDatagramID) {
         case  DG_GroupNet_RIS__LATCHED_HALL_CALLS_UP:
            UnloadDatagram_RiserLatchedHallCalls( ucRiser, HC_DIR__UP, unDatagram );
            bFailed = 0;
            break;
         case  DG_GroupNet_RIS__LATCHED_HALL_CALLS_DN:
            UnloadDatagram_RiserLatchedHallCalls( ucRiser, HC_DIR__DOWN, unDatagram );
            bFailed = 0;
            break;
         case  DG_GroupNet_RIS__INPUT_STATE:
            UnloadDatagram_RiserStatus( ucRiser, unDatagram );
            bFailed = 0;
            break;
         case  DG_GroupNet_RIS__HALLBOARD_STATUS:
            UnloadDatagram_RiserHallboardStatus( ucRiser, unDatagram );
            bFailed = 0;
            break;
         default:
            break;
      }
   }
   if(!bFailed)
   {
      ClrRiserOfflineCounter(ucRiser);
   }
   return bFailed;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ReceiveShieldDatagram( en_group_net_shield_datagrams eLocalDatagramID,
                                    un_sdata_datagram *punDatagram)
{
   switch(eLocalDatagramID)
   {
      case DG_GroupNet_SHIELD__SyncTime:
      {
         /* Unix time sent LSB to MSB */
         uint32_t uiUnixTime = ( punDatagram->auc8[7] << 0 )
                             | ( punDatagram->auc8[6] << 8 )
                             | ( punDatagram->auc8[5] << 16 )
                             | ( punDatagram->auc8[4] << 24 );
         RTC_SetSyncTime(uiUnixTime);
      }
         break;
      default:
         break;
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = 5; // Should be as fast as the fastest producer.

   Initialize_RiserData();
   Init_CarDestinationStructure();

   return 0;
}

static void UnloadCAN( void )
{
    CAN_MSG_T tmp;

    // Devices on CAN1: Car 1-8, Shield, XREG, Riser 1-4
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN1_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
          uwOfflineTimeout_CAN1_5ms = 0;
          un_sdata_datagram unDatagram;
          enum en_group_net_nodes ucSourceID      = ExtractFromBusID_SourceID(tmp.ID);
          GroupNet_DatagramID ucDatagramID    = ExtractFromBusID_DatagramID(tmp.ID);
          uint16_t uwDestinationBitmap = ExtractFromBusID_DestinationBitmap(tmp.ID);

          // Map the old system datagram ID to the new system datagram ID and insert into the data table
          // TODO: The below can be removed when all other baords on the groupnet are set up send and
          // receive using the same standard. But right now the old system and the new system are not
          // directly compatible.

          // Check for valid destination
          if( uwDestinationBitmap & GROUP_DEST__ALL )
          {
             if( ucSourceID <= GROUP_NET__CAR_8 )
             {
                 // Mapping a car data gram to the new system. The new system ID is calculated as follows.
                 // First extract the CAR ID and multiply by the number of datagrams a car can produce,
                 // hard coded to 25. Then add the old system datagram ID to the above. This results in a
                 // unique number for converting from the old system to the new system.
                 memcpy( unDatagram.auc8, tmp.Data, sizeof( tmp.Data ) );
                 InsertGroupNetDatagram_ByDatagramID(&unDatagram ,ucDatagramID);

                 // Unload some DG on receive to prevent missing multiplexed data
                 en_group_net_mrb_datagrams eMRBDatagramID = GroupNet_GlobalIDToLocalNodeID(ucDatagramID);
                 ReceiveGroupCarDatagram( ucSourceID, eMRBDatagramID, &unDatagram );
             }
             //Risers
             else if (ucSourceID <= GROUP_NET__RIS_4)
             {
                memcpy( unDatagram.auc8, tmp.Data, sizeof( tmp.Data ) );
                InsertGroupNetDatagram_ByDatagramID(&unDatagram ,ucDatagramID);
                en_group_net_ris_datagrams eLocalDatagramID = GroupNet_GlobalIDToLocalNodeID(ucDatagramID);
                if (eLocalDatagramID < NUM_GroupNet_RIS_DATAGRAMS)
                {
                    uint8_t ucRiserIndex = ucSourceID - GROUP_NET__RIS_1;
                    RecieveRiserCommand( ucRiserIndex, eLocalDatagramID, GetRiserDatagram(ucDatagramID));
                }
             }
             else if(ucSourceID == GROUP_NET__SHIELD)
             {
                InsertGroupNetDatagram_ByDatagramID(&unDatagram ,ucDatagramID);
                en_group_net_shield_datagrams eLocalDatagramID = GroupNet_GlobalIDToLocalNodeID(ucDatagramID);
                if (eLocalDatagramID < NUM_GroupNet_SHIELD_DATAGRAMS)
                {
                   ReceiveShieldDatagram( eLocalDatagramID, &unDatagram );
                }
             }
             else if( ucSourceID == GROUP_NET__XREG )
             {
                memcpy( unDatagram.auc8, tmp.Data, sizeof( tmp.Data ) );
                InsertGroupNetDatagram_ByDatagramID(&unDatagram ,ucDatagramID);

                /* Currently only one datagram ID expected from the XREG */
//                UnloadDatagram_XRegStatus(&unDatagram);
             }
             else if( ucSourceID == GROUP_NET__DDM )
             {
                memcpy( unDatagram.auc8, tmp.Data, sizeof( tmp.Data ) );
                InsertGroupNetDatagram_ByDatagramID(&unDatagram ,ucDatagramID);
                SetAlarm(DDM_ALM__DUPLICATE);
             }
          }
       }
    }

    //Devices on CAN2: Kiosk DD Panels
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN2_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
          uwOfflineTimeout_CAN2_5ms = 0;
          DDPanel_ReceiveMessage(&tmp);
       }
    }
}
/*-----------------------------------------------------------------------------
   Check for CAN Bus offline and reset if offline
 -----------------------------------------------------------------------------*/
static void CheckFor_CANBusOffline()
{
   if( Chip_CAN_GetGlobalStatus( LPC_CAN1 ) & CAN_GSR_BS ) // Bus offline if 1
   {
      Chip_CAN_SetMode( LPC_CAN1, CAN_RESET_MODE, ENABLE );
      __NOP();
      __NOP();
      __NOP();
      __NOP();
      __NOP();
      __NOP();
      Chip_CAN_SetMode( LPC_CAN1, CAN_RESET_MODE, DISABLE );
      SetAlarm(DDM_ALM__CAN_RESET_B1);
   }

   if( Chip_CAN_GetGlobalStatus( LPC_CAN2 ) & CAN_GSR_BS ) // Bus offline if 1
   {
      Chip_CAN_SetMode( LPC_CAN2, CAN_RESET_MODE, ENABLE );
      __NOP();
      __NOP();
      __NOP();
      __NOP();
      __NOP();
      __NOP();
      Chip_CAN_SetMode( LPC_CAN2, CAN_RESET_MODE, DISABLE );
      SetAlarm(DDM_ALM__CAN_RESET_B2);
   }
}
/*-----------------------------------------------------------------------------
   Updates the CAN bus error counters viewed via View Debug Data menus
 -----------------------------------------------------------------------------*/
static void UpdateDebugBusErrorCounter()
{
   static uint8_t ucLastCountRx_CAN1;
   static uint8_t ucLastCountTx_CAN1;
   static uint8_t ucLastCountRx_CAN2;
   static uint8_t ucLastCountTx_CAN2;
   uint32_t uiStatus = Chip_CAN_GetGlobalStatus( LPC_CAN1 );
   uint8_t ucCountRx = (uiStatus >> 16) & 0xFF;
   uint8_t ucCountTx = (uiStatus >> 24) & 0xFF;
   if( ( ucCountRx > ucLastCountRx_CAN1 )
    || ( ucCountTx > ucLastCountTx_CAN1 ))
   {
       uwBusErrorCounter_CAN1++;
   }
   ucLastCountRx_CAN1 = ucCountRx;
   ucLastCountTx_CAN1 = ucCountTx;

   uiStatus = Chip_CAN_GetGlobalStatus( LPC_CAN2 );
   ucCountRx = (uiStatus >> 16) & 0xFF;
   ucCountTx = (uiStatus >> 24) & 0xFF;
   if( ( ucCountRx > ucLastCountRx_CAN2 )
    || ( ucCountTx > ucLastCountTx_CAN2 ))
   {
       uwBusErrorCounter_CAN2++;
   }
   ucLastCountRx_CAN2 = ucCountRx;
   ucLastCountTx_CAN2 = ucCountTx;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define CAN1_OFFLINE_TIMEOUT__5MS      (200)
#define CAN2_OFFLINE_TIMEOUT__5MS      (200)
static void CheckFor_CommunicationLoss(void)
{
   if( uwOfflineTimeout_CAN1_5ms > CAN1_OFFLINE_TIMEOUT__5MS )
   {
      if( uwOfflineTimeout_CAN1_5ms != OFFLINE_TIMER_COM_NOT_DETECTED )
      {
//         SetAlarm( DDM_ALM__OFFLINE_EXP );
      }
   }
   else
   {
      uwOfflineTimeout_CAN1_5ms++;
   }

   if( uwOfflineTimeout_CAN2_5ms > CAN2_OFFLINE_TIMEOUT__5MS )
   {
      if( uwOfflineTimeout_CAN2_5ms != OFFLINE_TIMER_COM_NOT_DETECTED )
      {
//         SetAlarm( DDM_ALM__OFFLINE_EXP );
      }
   }
   else
   {
      uwOfflineTimeout_CAN2_5ms++;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   CheckFor_CommunicationLoss();
   // This is the only place the can periphs are init.
   UnloadCAN();
   CheckFor_CANBusOffline();
   UpdateDebugBusErrorCounter();
   ExpData_UpdateMasterOfflineTimers(pstThisModule->uwRunPeriod_1ms);
   CheckFor_RiserBoardFault(pstThisModule);

   return 0;
}

