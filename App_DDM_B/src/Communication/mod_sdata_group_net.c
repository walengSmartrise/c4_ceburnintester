/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     26, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "sru.h"
#include "sru_b.h"
#include "sys.h"

#include <string.h>
#include "carData.h"

#include "param_edit_protocol.h"
#include "groupnet_datagrams.h"
#include "ddCommand.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
uint8_t gucComFault;
uint8_t gucDipFault;
struct st_module gstMod_SData_GroupNet =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/* Below are all the nodes in Group Network */
static struct st_sdata_control gstSData_GroupNet_DDM;

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static const uint8_t ucNumDatagrams_GroupNet = NUM_GroupNet_DDM_DATAGRAMS;

static struct st_sdata_local gstGroupNetSData_Config;

static CAN_MSG_T gstCAN_MSG_Tx;

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SharedData_GroupNet_Init()
{
   int iError = 0;
   /* Out of node definitions, so pretend to be XREG */
   gstGroupNetSData_Config.ucLocalNodeID = GROUP_NET__DDM;

   gstGroupNetSData_Config.ucNetworkID = SDATA_NET__GROUP;

   gstGroupNetSData_Config.ucNumNodes = NUM_GROUP_NET_NODES;

   gstGroupNetSData_Config.ucDestNodeID = 0x1F; /* All */

   iError = SDATA_CreateDatagrams( &gstSData_GroupNet_DDM, ucNumDatagrams_GroupNet );
   if ( iError )
   {
      while ( 1 )
      {
         // TODO: unable to allocate shared data
         SRU_Write_LED( enSRU_LED_Fault, 1 );
      }
   }

   gstCAN_MSG_Tx.DLC = 8;

}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Receive( void )
{
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Transmit( void )
{
   un_sdata_datagram unDatagram;
   int iError = 0;
   uint8_t ucDatagramID;

   uint16_t uwDatagram_Plus1 = SDATA_GetNextDatagramToSendIndex_Plus1(&gstSData_GroupNet_DDM );
   if( uwDatagram_Plus1
  && ( uwDatagram_Plus1 <= ucNumDatagrams_GroupNet ) )
   {
      ucDatagramID = uwDatagram_Plus1-1;
      SDATA_ReadDatagram( &gstSData_GroupNet_DDM,
                          ucDatagramID,
                          &unDatagram
                        );

      memcpy( gstCAN_MSG_Tx.Data, unDatagram.auc8, sizeof( unDatagram.auc8 ) );

      enum en_sdata_networks eNet = SDATA_NET__GROUP;
      enum en_group_net_nodes eLocalNode = gstGroupNetSData_Config.ucLocalNodeID;
      GroupNet_DatagramID eID = GroupNet_LocalDDMIDToGlobalID( ucDatagramID );
      uint16_t uwDest = GROUP_DEST__ALL;
      gstCAN_MSG_Tx.ID = GroupNet_EncodeCANMessageID(eID, eNet, eLocalNode, uwDest);

      CAN_BUFFER_ID_T   TxBuf = Chip_CAN_GetFreeTxBuf(LPC_CAN1);

      // Chip_can_send returns 0 for error and 1 for success. We need to invert this.
      iError = !Chip_CAN_Send(LPC_CAN1, TxBuf, &gstCAN_MSG_Tx );

      if ( iError )
      {
         if ( gstSData_GroupNet_DDM.uwLastSentIndex )
         {
            --gstSData_GroupNet_DDM.uwLastSentIndex;
         }
         else
         {
            gstSData_GroupNet_DDM.uwLastSentIndex = gstSData_GroupNet_DDM.uiNumDatagrams - 1;
         }
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadData( void ){}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadDatagram_Heartbeat(un_sdata_datagram *punDatagram)
{
   memset(punDatagram, 0, sizeof(un_sdata_datagram));
   en_ddm_alarms eAlarm = ( GetAlarm_ByNode(DDM_NODE__B) )
                        ? ( GetAlarm_ByNode(DDM_NODE__B) )
                        : ( GetAlarm_ByNode(DDM_NODE__A) );
   punDatagram->auw16[0] = eAlarm;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData( void )
{
   un_sdata_datagram unDatagram;
   LoadDatagram_Heartbeat(&unDatagram);
   SDATA_WriteDatagram(&gstSData_GroupNet_DDM, DG_GroupNet_DDM__HEARTBEAT, &unDatagram);

   if( !SDATA_DirtyBit_Get(&gstSData_GroupNet_DDM, DG_GroupNet_DDM__LATCH_HC) )
   {
      if( DDC_LoadDatagram_LatchHC(&unDatagram) )
      {
         SDATA_WriteDatagram(&gstSData_GroupNet_DDM, DG_GroupNet_DDM__LATCH_HC, &unDatagram);
         SDATA_DirtyBit_Set(&gstSData_GroupNet_DDM, DG_GroupNet_DDM__LATCH_HC);
      }
   }

   if( !SDATA_DirtyBit_Get(&gstSData_GroupNet_DDM, DG_GroupNet_DDM__LATCH_CC) )
   {
      if( DDC_LoadDatagram_LatchCC(&unDatagram) )
      {
         SDATA_WriteDatagram(&gstSData_GroupNet_DDM, DG_GroupNet_DDM__LATCH_CC, &unDatagram);
         SDATA_DirtyBit_Set(&gstSData_GroupNet_DDM, DG_GroupNet_DDM__LATCH_CC);
      }
   }
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   //------------------------------------------------
   pstThisModule->uwInitialDelay_1ms = 5;
   pstThisModule->uwRunPeriod_1ms = 5;

   gstSData_GroupNet_DDM.bDisableHeartbeat = 1;

   gstSData_GroupNet_DDM.paiResendRate_1ms[DG_GroupNet_DDM__LATCH_HC] = DISABLED_DATAGRAM_RESEND_RATE_MS;
   gstSData_GroupNet_DDM.paiResendRate_1ms[DG_GroupNet_DDM__LATCH_CC] = DISABLED_DATAGRAM_RESEND_RATE_MS;
   gstSData_GroupNet_DDM.paiResendRate_1ms[DG_GroupNet_DDM__HEARTBEAT] = 10000;

   Init_CarDataStructure();

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   IncrementAllCarOfflineTimers(pstThisModule);
   //------------------------------------------------
   // Check for receive data every time.
   Receive();
   UnloadData();
   //------------------------------------------------
   // Send data.
   LoadData();
   Transmit();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
