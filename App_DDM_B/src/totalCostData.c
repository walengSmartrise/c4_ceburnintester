/******************************************************************************
 *
 * @file     totalCostData.c
 * @brief
 * @version  V1.00
 * @date     7, November 2018
 * @author   Keith Soneda
 *
 * @note     Functions for assessing a calls total cost when assigned to a given car
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <string.h>
#include <math.h>
#include <stdint.h>
#include "sys.h"
#include "totalCostData.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
typedef struct
{
      uint32_t uiTimeToPanel_Travel_sec; // The est. time spent in motion before before passenger pick up.
      uint32_t uiTimeToPanel_CC_sec;     // The est. time spent on CC dwell times before passenger pick up.
      uint32_t uiTimeToPanel_HC_sec;     // The est. time spent on HC dwell times before passenger pick up.
      uint32_t uiTimeToDest_Travel_sec;  // The est. time spent in motion before before passenger drop off.
      uint32_t uiTimeToDest_CC_sec;      // The est. time spent on CC dwell times before passenger drop off.
      uint32_t uiTimeToDest_HC_sec;      // The est. time spent on HC dwell times before passenger drop off.
      uint32_t uiTimeAdded_CC_sec;       // The est. time added to each other passenger's trip time due to the additional drop off.
      uint32_t uiTimeAdded_HC_sec;       // The est. time added to each other passenger's trip time due to the additional pick up.
      uint32_t uiTotalCost_sec;          // The est. total cost in to assigning the call to the car.

      uint8_t ucCurrentLanding;
      uint8_t ucDestinationLanding;
      uint8_t ucPanelLanding;
} st_assignment_cost;
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static st_assignment_cost astAssignmentCost[MAX_GROUP_CARS];
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
Access functions , returns times for most recent request
 *----------------------------------------------------------------------------*/
void TotalCost_ResetAssignmentCost( enum en_group_net_nodes eCarID )
{
   memset(&astAssignmentCost[eCarID], 0, sizeof(st_assignment_cost));
   astAssignmentCost[eCarID].uiTotalCost_sec = INVALID_TOTAL_ASSIGNMENT_COST_SEC;
}
uint32_t TotalCost_GetLastAssignment_T2P_Travel( enum en_group_net_nodes eCarID )
{
   return astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec;
}
uint32_t TotalCost_GetLastAssignment_T2P_CC( enum en_group_net_nodes eCarID )
{
   return astAssignmentCost[eCarID].uiTimeToPanel_CC_sec;
}
uint32_t TotalCost_GetLastAssignment_T2P_HC( enum en_group_net_nodes eCarID )
{
   return astAssignmentCost[eCarID].uiTimeToPanel_HC_sec;
}
uint32_t TotalCost_GetLastAssignment_T2D_Travel( enum en_group_net_nodes eCarID )
{
   return astAssignmentCost[eCarID].uiTimeToDest_Travel_sec;
}
uint32_t TotalCost_GetLastAssignment_T2D_CC( enum en_group_net_nodes eCarID )
{
   return astAssignmentCost[eCarID].uiTimeToDest_CC_sec;
}
uint32_t TotalCost_GetLastAssignment_T2D_HC( enum en_group_net_nodes eCarID )
{
   return astAssignmentCost[eCarID].uiTimeToDest_HC_sec;
}
uint32_t TotalCost_GetLastAssignment_TA_CC( enum en_group_net_nodes eCarID )
{
   return astAssignmentCost[eCarID].uiTimeAdded_CC_sec;
}
uint32_t TotalCost_GetLastAssignment_TA_HC( enum en_group_net_nodes eCarID )
{
   return astAssignmentCost[eCarID].uiTimeAdded_HC_sec;
}
uint32_t TotalCost_GetLastAssignment_TotalCost( enum en_group_net_nodes eCarID )
{
   return astAssignmentCost[eCarID].uiTotalCost_sec;
}

uint8_t TotalCost_GetLastAssignment_CurrentLanding( enum en_group_net_nodes eCarID )
{
   return astAssignmentCost[eCarID].ucCurrentLanding;
}
uint8_t TotalCost_GetLastAssignment_DestinationLanding( enum en_group_net_nodes eCarID )
{
   return astAssignmentCost[eCarID].ucDestinationLanding;
}
uint8_t TotalCost_GetLastAssignment_PanelLanding( enum en_group_net_nodes eCarID )
{
   return astAssignmentCost[eCarID].ucPanelLanding;
}

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void GetAssignmentCost_PanelAboveDestAbove( enum en_group_net_nodes eCarID, uint8_t ucDestLanding, uint8_t ucPanelLanding )
{
   /* Bitmaps to mark CCs already accounted for */
   uint8_t aucCallsToPanel[BITMAP8_SIZE(MAX_NUM_FLOORS)] = {0}; // Tracks the CCs cleared on the way to the panel floor
   uint8_t aucCallsToDest[BITMAP8_SIZE(MAX_NUM_FLOORS)] = {0}; // Tracks the CCs cleared on the way to the destination (after reaching the panel floor)

   /* If 1, the car is already heading to the floor */
   uint8_t bPanelLatched, bDestLatched = 0;

   st_cardata *pstCarData = GetCarDataStructure(eCarID);

   /* Check time in motion to reach panel floor */
   uint8_t ucFloorsToPanel = ucPanelLanding - pstCarData->ucCurrentLanding;
   astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec = ucFloorsToPanel * pstCarData->ucMaxFloorToFloorTime_sec;

   /* Check latched CCs between current floor and panel */
   for(int8_t i = pstCarData->ucReachableLanding; i <= ucPanelLanding; i++) {
      if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) || Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
         if( i == ucPanelLanding ) {
            bPanelLatched = 1;
         }
         else {
            astAssignmentCost[eCarID].uiTimeToPanel_CC_sec += pstCarData->ucDoorDwellTime_sec;
            Sys_Bit_Set(&aucCallsToPanel[0], i, 1);
         }
      }
   }

   /* Check CCs not yet latched between current floor and panel */
   for(int8_t i = pstCarData->ucReachableLanding; i <= ucPanelLanding; i++) {
      st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
      /* Are there any calls assigned from this panel? */
      uint8_t ucNumCalls = pstAssignment->ucCallIndex;
      for(uint8_t j = 0; j < ucNumCalls; j++) {
         /* Count the CCs between the destination  */
         uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
         if( ( ucLanding > i )
          && ( ucLanding <= ucPanelLanding )
          && ( !Sys_Bit_Get(&aucCallsToPanel[0], ucLanding) ) ) {
            if( ucLanding == ucPanelLanding ) {
               bPanelLatched = 1;
            }
            else {
               astAssignmentCost[eCarID].uiTimeToPanel_CC_sec += pstCarData->ucDoorDwellTime_sec;
               Sys_Bit_Set(&aucCallsToPanel[0], ucLanding, 1);
            }
         }
      }
   }

   /* Check HCs between current floor and panel */
   for(int8_t i = pstCarData->ucReachableLanding; i <= ucPanelLanding; i++) {
      if( GetLatchedHallCallUp_Front_ByCar(eCarID, i) || GetLatchedHallCallUp_Rear_ByCar(eCarID, i) ) {
         if( i == ucPanelLanding ) {
            bPanelLatched = 1;
         }
         else {
            if( Sys_Bit_Get(&aucCallsToPanel[0], i) ) {
               astAssignmentCost[eCarID].uiTimeToPanel_CC_sec -= pstCarData->ucDoorDwellTime_sec;
            }
            astAssignmentCost[eCarID].uiTimeToPanel_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
         }
      }
   }

   //--------------------------------------------------------------------------
   //--------------------------------------------------------------------------
   /* Check time in motion to reach destination floor */
   uint8_t ucFloorsToDest = ucDestLanding - ucPanelLanding;
   astAssignmentCost[eCarID].uiTimeToDest_Travel_sec = ucFloorsToDest * pstCarData->ucMaxFloorToFloorTime_sec;

   /* Check latched CCs between panel and destination floor */
   for(int8_t i = ucPanelLanding+1; i <= ucDestLanding; i++) {
      if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) || Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
         if( ucDestLanding == i ) {
            bDestLatched = 1;
         }
         else {
            astAssignmentCost[eCarID].uiTimeToDest_CC_sec += pstCarData->ucDoorDwellTime_sec;
            Sys_Bit_Set(&aucCallsToDest[0], i, 1);
         }
      }
   }

   /* Check CCs not yet latched between panel and destination floor */
   for(int8_t i = ucPanelLanding; i < ucDestLanding; i++) {
      st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
      /* Are there any calls assigned from this panel? */
      uint8_t ucNumCalls = pstAssignment->ucCallIndex;
      for(uint8_t j = 0; j < ucNumCalls; j++) {
         /* Count the CCs between the destination  */
         uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
         if( ( ucLanding > i )
          && ( ucLanding <= ucDestLanding )
          && ( !Sys_Bit_Get(&aucCallsToDest[0], ucLanding) ) ) {
            if( ucLanding == ucDestLanding ) {
               bDestLatched = 1;
            }
            else {
               astAssignmentCost[eCarID].uiTimeToDest_CC_sec += pstCarData->ucDoorDwellTime_sec;
               Sys_Bit_Set(&aucCallsToDest[0], ucLanding, 1);
            }
         }
      }
   }

   /* Check HCs between panel and destination floor */
   for(int8_t i = ucPanelLanding+1; i <= ucDestLanding; i++) {
      if( GetLatchedHallCallUp_Front_ByCar(eCarID, i) || GetLatchedHallCallUp_Rear_ByCar(eCarID, i) ) {
         if( ucDestLanding == i ) {
            bDestLatched = 1;
         }
         else {
            if( Sys_Bit_Get(&aucCallsToDest[0], i) ) {
               astAssignmentCost[eCarID].uiTimeToDest_CC_sec -= pstCarData->ucDoorDwellTime_sec;
            }
            astAssignmentCost[eCarID].uiTimeToDest_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
         }
      }
   }

   //--------------------------------------------------------------------------
   //--------------------------------------------------------------------------
   /* Check time added to other passenger's estimated time to destination.
    * Assumes each CC represents a passenger */
   for(int8_t i = ucPanelLanding; i <= pstCarData->ucLastLanding; i++) {
      /* Any passengers going to floors above the destination floor will be
       * delayed by both the destination and panel floor dwell times */
      if( i >= ucDestLanding ) {
         if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) ) {
            astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
            astAssignmentCost[eCarID].uiTimeAdded_CC_sec += pstCarData->ucDoorDwellTime_sec;
         }
         if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
            astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
            astAssignmentCost[eCarID].uiTimeAdded_CC_sec += pstCarData->ucDoorDwellTime_sec;
         }
      }
      /* Any passengers going to floors below the destination floor will
       * only be delayed by the panel floor dwell time */
      else {
         if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) ) {
            astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
         }
         if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
            astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
         }
      }

      /* Check for assignments that haven't been latched */
      st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
      uint8_t ucNumCalls = pstAssignment->ucCallIndex;
      for(uint8_t j = 0; j < ucNumCalls; j++) {
         /* Count the CCs between the destination  */
         uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
         if( ( !Sys_Bit_Get(&aucCallsToPanel[0], ucLanding) )
          && ( ucLanding > i ) // TODO review if this condition is necessary
           ) {
            if( ucLanding >= ucDestLanding )
            {
               astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
               astAssignmentCost[eCarID].uiTimeAdded_CC_sec += pstCarData->ucDoorDwellTime_sec;
            }
            else
            {
               astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
            }
         }
      }
   }

   if( bPanelLatched ) {
      astAssignmentCost[eCarID].uiTimeAdded_HC_sec = 0;
   }
   if( bDestLatched ) {
      astAssignmentCost[eCarID].uiTimeAdded_CC_sec = 0;
   }

   if( Param_ReadValue_8Bit(DDM_PARAM8__DispatchingType) == DDM_DISPATCH_TYPE__TOTAL_COST )
   {
      astAssignmentCost[eCarID].uiTotalCost_sec  = astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec + astAssignmentCost[eCarID].uiTimeToPanel_CC_sec + astAssignmentCost[eCarID].uiTimeToPanel_HC_sec;
      astAssignmentCost[eCarID].uiTotalCost_sec += astAssignmentCost[eCarID].uiTimeToDest_Travel_sec + astAssignmentCost[eCarID].uiTimeToDest_CC_sec + astAssignmentCost[eCarID].uiTimeToDest_HC_sec;
      astAssignmentCost[eCarID].uiTotalCost_sec += astAssignmentCost[eCarID].uiTimeAdded_CC_sec + astAssignmentCost[eCarID].uiTimeAdded_HC_sec;
   }
   else if( Param_ReadValue_8Bit(DDM_PARAM8__DispatchingType) == DDM_DISPATCH_TYPE__TIME_TO_DEST )
   {
      astAssignmentCost[eCarID].uiTotalCost_sec  = astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec + astAssignmentCost[eCarID].uiTimeToPanel_CC_sec + astAssignmentCost[eCarID].uiTimeToPanel_HC_sec;
      astAssignmentCost[eCarID].uiTotalCost_sec += astAssignmentCost[eCarID].uiTimeToDest_Travel_sec + astAssignmentCost[eCarID].uiTimeToDest_CC_sec + astAssignmentCost[eCarID].uiTimeToDest_HC_sec;
   }
   else if( Param_ReadValue_8Bit(DDM_PARAM8__DispatchingType) == DDM_DISPATCH_TYPE__TIME_TO_PICKUP )
   {
      astAssignmentCost[eCarID].uiTotalCost_sec = astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec + astAssignmentCost[eCarID].uiTimeToPanel_CC_sec + astAssignmentCost[eCarID].uiTimeToPanel_HC_sec;
   }
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void GetAssignmentCost_PanelAboveDestBelow( enum en_group_net_nodes eCarID, uint8_t ucDestLanding, uint8_t ucPanelLanding )
{
   /* Bitmaps to mark CCs already accounted for */
   uint8_t aucCallsToPanel[BITMAP8_SIZE(MAX_NUM_FLOORS)] = {0}; // Tracks the CCs cleared on the way to the panel floor (Pre direction change)
   uint8_t aucCallsToDest[BITMAP8_SIZE(MAX_NUM_FLOORS)] = {0}; // Tracks the CCs cleared on the way to the destination (after reaching the panel floor)
   uint8_t aucCallsToPanel2[BITMAP8_SIZE(MAX_NUM_FLOORS)] = {0}; // Tracks the CCs cleared on the way to the panel floor (Post direction change)

   /* If 1, the car is already heading to the floor */
   uint8_t bPanelLatched, bDestLatched = 0;

   st_cardata *pstCarData = GetCarDataStructure(eCarID);

   uint8_t ucDirectionChangeLanding = INVALID_LANDING;
   for(int8_t i = pstCarData->ucLastLanding; i > pstCarData->ucReachableLanding; i--) {
      if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) || Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i)
       || GetLatchedHallCallDown_Front_ByCar(eCarID, i) || GetLatchedHallCallDown_Rear_ByCar(eCarID, i)
       || GetLatchedHallCallUp_Front_ByCar(eCarID, i) || GetLatchedHallCallUp_Rear_ByCar(eCarID, i) ) {
         if( ucDirectionChangeLanding < i ) {
            ucDirectionChangeLanding = i;
         }
      }
      else {
         st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
         /* Are there any calls assigned from this panel? */
         uint8_t ucNumCalls = pstAssignment->ucCallIndex;
         for(uint8_t j = 0; j < ucNumCalls; j++) {
            uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
            if( ( ucLanding > ucPanelLanding )
             && ( ucDirectionChangeLanding < ucLanding ) ) {
               ucDirectionChangeLanding = ucLanding;
            }
         }
      }
   }

   /* Car will proceed directly to the panel floor */
   if( ucDirectionChangeLanding == INVALID_LANDING ) {
      /* Check time in motion to reach panel floor */
      uint8_t ucFloorsToPanel = ucPanelLanding - pstCarData->ucCurrentLanding;
      astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec = ucFloorsToPanel * pstCarData->ucMaxFloorToFloorTime_sec;

      /* Check latched CCs between current floor and panel */
      for(int8_t i = pstCarData->ucReachableLanding; i <= ucPanelLanding; i++) {
         if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) || Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
            if( i == ucPanelLanding ) {
               bPanelLatched = 1;
            }
            else {
               astAssignmentCost[eCarID].uiTimeToPanel_CC_sec += pstCarData->ucDoorDwellTime_sec;
               Sys_Bit_Set(&aucCallsToPanel[0], i, 1);
            }
         }
      }

      /* Check CCs not yet latched between current floor and panel */
      for(int8_t i = pstCarData->ucReachableLanding; i <= ucPanelLanding; i++) {
         st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
         /* Are there any calls assigned from this panel? */
         uint8_t ucNumCalls = pstAssignment->ucCallIndex;
         for(uint8_t j = 0; j < ucNumCalls; j++) {
            /* Count the CCs between the destination  */
            uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
            if( ( ucLanding > i )
             && ( ucLanding <= ucPanelLanding )
             && ( !Sys_Bit_Get(&aucCallsToPanel[0], ucLanding) ) ) {
               if( ucLanding == ucPanelLanding ) {
                  bPanelLatched = 1;
               }
               else {
                  astAssignmentCost[eCarID].uiTimeToPanel_CC_sec += pstCarData->ucDoorDwellTime_sec;
                  Sys_Bit_Set(&aucCallsToPanel[0], ucLanding, 1);
               }
            }
         }
      }

      /* Check HCs between current floor and panel */
      for(int8_t i = pstCarData->ucReachableLanding; i <= ucPanelLanding; i++) {
         if( i == ucPanelLanding ) {
            if( GetLatchedHallCallDown_Front_ByCar(eCarID, i) || GetLatchedHallCallDown_Rear_ByCar(eCarID, i) ) {
               bPanelLatched = 1;
            }
         }
         else if( GetLatchedHallCallUp_Front_ByCar(eCarID, i) || GetLatchedHallCallUp_Rear_ByCar(eCarID, i) ) {
            if( Sys_Bit_Get(&aucCallsToPanel[0], i) ) {
               astAssignmentCost[eCarID].uiTimeToPanel_CC_sec -= pstCarData->ucDoorDwellTime_sec;
            }
            astAssignmentCost[eCarID].uiTimeToPanel_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
         }
      }

      //--------------------------------------------------------------------------
      //--------------------------------------------------------------------------
      /* Check time in motion to reach destination floor */
      uint8_t ucFloorsToDest = ucPanelLanding - ucDestLanding;
      astAssignmentCost[eCarID].uiTimeToDest_Travel_sec = ucFloorsToDest * pstCarData->ucMaxFloorToFloorTime_sec;

      /* Check latched CCs between panel and destination floor */
      for(int8_t i = ucPanelLanding-1; i >= ucDestLanding; i--) {
         if( ( !Sys_Bit_Get(&aucCallsToPanel[0], i) )
          && ( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) || Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) ) {
            if( ucDestLanding == i ) {
               bDestLatched = 1;
            }
            else {
               astAssignmentCost[eCarID].uiTimeToDest_CC_sec += pstCarData->ucDoorDwellTime_sec;
               Sys_Bit_Set(&aucCallsToDest[0], i, 1);
            }
         }
      }

      /* Check CCs not yet latched between panel and destination floor */
      for(int8_t i = ucPanelLanding; i > ucDestLanding; i--) {
         st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
         /* Are there any calls assigned from this panel? */
         uint8_t ucNumCalls = pstAssignment->ucCallIndex;
         for(uint8_t j = 0; j < ucNumCalls; j++) {
            /* Count the CCs between the destination  */
            uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
            if( ( ucLanding < i )
             && ( ucLanding >= ucDestLanding )
             && ( !Sys_Bit_Get(&aucCallsToDest[0], ucLanding) ) ) {
               if( ucLanding == ucDestLanding ) {
                  bDestLatched = 1;
               }
               else {
                  astAssignmentCost[eCarID].uiTimeToDest_CC_sec += pstCarData->ucDoorDwellTime_sec;
                  Sys_Bit_Set(&aucCallsToDest[0], ucLanding, 1);
               }
            }
         }
      }

      /* Check HCs between panel and destination floor */
      for(int8_t i = ucPanelLanding-1; i >= ucDestLanding; i--) {
         if( GetLatchedHallCallDown_Front_ByCar(eCarID, i) || GetLatchedHallCallDown_Rear_ByCar(eCarID, i) ) {
            if( ucDestLanding == i ) {
               bDestLatched = 1;
            }
            else {
               if( Sys_Bit_Get(&aucCallsToDest[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeToDest_CC_sec -= pstCarData->ucDoorDwellTime_sec;
               }
               astAssignmentCost[eCarID].uiTimeToDest_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
            }
         }
      }

      //--------------------------------------------------------------------------
      //--------------------------------------------------------------------------
      /* Check time added to other passenger's estimated time to destination.
       * Assumes each CC represents a passenger */
      for(int8_t i = ucPanelLanding; i >= pstCarData->ucFirstLanding; i--) {
         /* Any passengers going to floors after the destination floor will be
          * delayed by both the destination and panel floor dwell times */
         if( i <= ucDestLanding ) {
            if( !Sys_Bit_Get(&aucCallsToPanel[0], i) ) {
               if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
                  astAssignmentCost[eCarID].uiTimeAdded_CC_sec += pstCarData->ucDoorDwellTime_sec;
               }
               if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
                  astAssignmentCost[eCarID].uiTimeAdded_CC_sec += pstCarData->ucDoorDwellTime_sec;
               }
            }
         }
         /* Any passengers going to floors below the panel and destination floor will
          * only be delayed by the panel floor dwell time */
         else {
            /* This is a return path so ignore calls that will be serviced on the way to the panel floor */
            if( !Sys_Bit_Get(&aucCallsToPanel[0], i) ) {
               if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
               }
               if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
               }
            }
         }

         /* Check for assignments that haven't been latched */
         st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
         uint8_t ucNumCalls = pstAssignment->ucCallIndex;
         for(uint8_t j = 0; j < ucNumCalls; j++) {
            /* Count the CCs between the destination  */
            uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
            if( ( !Sys_Bit_Get(&aucCallsToPanel[0], ucLanding) )
             && ( ucLanding > i ) // TODO review if this condition is necessary
              ) {
               if( ucLanding <= ucDestLanding )
               {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
                  astAssignmentCost[eCarID].uiTimeAdded_CC_sec += pstCarData->ucDoorDwellTime_sec;
               }
               else
               {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
               }
            }
         }
      }
   }
   /* Car will move to past the panel floor then return to the panel floor */
   else
   {
      /* Check time in motion to reach panel floor */
      uint8_t ucFloorsToPanel = ( ucDirectionChangeLanding - pstCarData->ucCurrentLanding )
                              + ( ucDirectionChangeLanding - ucPanelLanding );
      astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec = ucFloorsToPanel * pstCarData->ucMaxFloorToFloorTime_sec;

      /* Check latched CCs between current floor and panel */
      for(int8_t i = pstCarData->ucReachableLanding; i <= ucDirectionChangeLanding; i++) {
         if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) || Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
            astAssignmentCost[eCarID].uiTimeToPanel_CC_sec += pstCarData->ucDoorDwellTime_sec;
            Sys_Bit_Set(&aucCallsToPanel[0], i, 1);
         }
      }

      /* Check CCs not yet latched between current floor and panel (up to the direction change floor)*/
      for(int8_t i = pstCarData->ucReachableLanding; i <= ucDirectionChangeLanding; i++) {
         st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
         /* Are there any calls assigned from this panel? */
         uint8_t ucNumCalls = pstAssignment->ucCallIndex;
         for(uint8_t j = 0; j < ucNumCalls; j++) {
            /* Count the CCs between the destination  */
            uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
            if( ( ucLanding > i )
             && ( ucLanding <= ucDirectionChangeLanding )
             && ( !Sys_Bit_Get(&aucCallsToPanel[0], ucLanding) ) ) {
               astAssignmentCost[eCarID].uiTimeToPanel_CC_sec += pstCarData->ucDoorDwellTime_sec;
               Sys_Bit_Set(&aucCallsToPanel[0], ucLanding, 1);
            }
         }
      }
      /* Check CCs not yet latched between current floor and panel (after the direction change floor)*/
      for(int8_t i = ucDirectionChangeLanding; i >= ucPanelLanding; i--) {
         st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
         /* Are there any calls assigned from this panel? */
         uint8_t ucNumCalls = pstAssignment->ucCallIndex;
         for(uint8_t j = 0; j < ucNumCalls; j++) {
            /* Count the CCs between the destination  */
            uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
            if( ucLanding < i )
            {
               if( ( ucLanding > ucPanelLanding )
                && ( !Sys_Bit_Get(&aucCallsToPanel2[0], ucLanding) ) ) {
                 astAssignmentCost[eCarID].uiTimeToPanel_CC_sec += pstCarData->ucDoorDwellTime_sec;
                 Sys_Bit_Set(&aucCallsToPanel2[0], ucLanding, 1);
              }
              if( ucLanding == ucPanelLanding ) {
                 bPanelLatched = 1;
              }
            }
         }
      }

      /* Check HCs between current floor and panel (pre direction change)*/
      for(int8_t i = pstCarData->ucReachableLanding; i <= ucDirectionChangeLanding; i++) {
         if( GetLatchedHallCallUp_Front_ByCar(eCarID, i) || GetLatchedHallCallUp_Rear_ByCar(eCarID, i) ) {
            if( Sys_Bit_Get(&aucCallsToPanel[0], i) ) {
               astAssignmentCost[eCarID].uiTimeToPanel_CC_sec -= pstCarData->ucDoorDwellTime_sec;
            }
            astAssignmentCost[eCarID].uiTimeToPanel_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
         }
      }
      /* Check HCs between current floor and panel (post direction change)*/
      for(int8_t i = ucDirectionChangeLanding; i >= ucPanelLanding; i--) {
         if( GetLatchedHallCallDown_Front_ByCar(eCarID, i) || GetLatchedHallCallDown_Rear_ByCar(eCarID, i) ) {
            if( Sys_Bit_Get(&aucCallsToPanel2[0], i) ) {
               astAssignmentCost[eCarID].uiTimeToPanel_CC_sec -= pstCarData->ucDoorDwellTime_sec;
            }
            astAssignmentCost[eCarID].uiTimeToPanel_HC_sec += pstCarData->ucDoorDwellHallTime_sec;

            if( i == ucPanelLanding ) {
               bPanelLatched = 1;
            }
         }
      }

      //--------------------------------------------------------------------------
      //--------------------------------------------------------------------------
      /* Check time in motion to reach destination floor */
      uint8_t ucFloorsToDest = ucPanelLanding - ucDestLanding;
      astAssignmentCost[eCarID].uiTimeToDest_Travel_sec = ucFloorsToDest * pstCarData->ucMaxFloorToFloorTime_sec;

      /* Check latched CCs between panel and destination floor */
      for(int8_t i = ucPanelLanding-1; i >= ucDestLanding; i--) {
         if( ( !Sys_Bit_Get(&aucCallsToPanel[0], i) )
          && ( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) || Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) ) {
            if( ucDestLanding == i ) {
               bDestLatched = 1;
            }
            else {
               astAssignmentCost[eCarID].uiTimeToDest_CC_sec += pstCarData->ucDoorDwellTime_sec;
               Sys_Bit_Set(&aucCallsToDest[0], i, 1);
            }
         }
      }

      /* Check CCs not yet latched between panel and destination floor */
      for(int8_t i = ucPanelLanding-1; i > ucDestLanding; i--) {
         st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
         /* Are there any calls assigned from this panel? */
         uint8_t ucNumCalls = pstAssignment->ucCallIndex;
         for(uint8_t j = 0; j < ucNumCalls; j++) {
            /* Count the CCs between the destination  */
            uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
            if( ucLanding < i ) {
               if( ucLanding == ucDestLanding ) {
                  bDestLatched = 1;
               }
               else if( ( ucLanding > ucDestLanding )
                     && ( !Sys_Bit_Get(&aucCallsToDest[0], ucLanding) ) ) {
                  astAssignmentCost[eCarID].uiTimeToDest_CC_sec += pstCarData->ucDoorDwellTime_sec;
                  Sys_Bit_Set(&aucCallsToDest[0], ucLanding, 1);
               }
            }
         }
      }

      /* Check HCs between panel and destination floor */
      for(int8_t i = ucPanelLanding-1; i >= ucDestLanding; i--) {
         if( GetLatchedHallCallDown_Front_ByCar(eCarID, i) || GetLatchedHallCallDown_Rear_ByCar(eCarID, i) ) {
            if( ucDestLanding == i ) {
               bDestLatched = 1;
            }
            else {
               if( Sys_Bit_Get(&aucCallsToDest[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeToDest_CC_sec -= pstCarData->ucDoorDwellTime_sec;
               }
               astAssignmentCost[eCarID].uiTimeToDest_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
            }
         }
      }

      //--------------------------------------------------------------------------
      //--------------------------------------------------------------------------
      /* Check time added to other passenger's estimated time to destination.
       * Assumes each CC represents a passenger */
      for(int8_t i = ucPanelLanding; i >= pstCarData->ucFirstLanding; i--) {
         /* Any passengers going to floors after the destination floor will be
          * delayed by both the destination and panel floor dwell times */
         if( i <= ucDestLanding ) {
            if( !Sys_Bit_Get(&aucCallsToPanel[0], i) ) {
               if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
                  astAssignmentCost[eCarID].uiTimeAdded_CC_sec += pstCarData->ucDoorDwellTime_sec;
               }
               if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
                  astAssignmentCost[eCarID].uiTimeAdded_CC_sec += pstCarData->ucDoorDwellTime_sec;
               }
            }
         }
         /* Any passengers going to floors between the panel and destination are
          * delayed by the panel floor dwell time */
         else {
            /* This is a return path so ignore calls that will be serviced on the way to the panel floor */
            if( !Sys_Bit_Get(&aucCallsToPanel[0], i) ) {
               if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
               }
               if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
               }
            }
         }

         /* Check for assignments that haven't been latched */
         st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
         uint8_t ucNumCalls = pstAssignment->ucCallIndex;
         for(uint8_t j = 0; j < ucNumCalls; j++) {
            /* Count the CCs between the destination  */
            uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
            if( ( !Sys_Bit_Get(&aucCallsToPanel[0], ucLanding) )
             && ( ucLanding > i ) // TODO review if this condition is necessary
              ) {
               if( ucLanding <= ucDestLanding )
               {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
                  astAssignmentCost[eCarID].uiTimeAdded_CC_sec += pstCarData->ucDoorDwellTime_sec;
               }
               else
               {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
               }
            }
         }
      }
   }

   if( bPanelLatched ) {
      astAssignmentCost[eCarID].uiTimeAdded_HC_sec = 0;
   }
   if( bDestLatched ) {
      astAssignmentCost[eCarID].uiTimeAdded_CC_sec = 0;
   }

   if( Param_ReadValue_8Bit(DDM_PARAM8__DispatchingType) == DDM_DISPATCH_TYPE__TOTAL_COST )
   {
      astAssignmentCost[eCarID].uiTotalCost_sec  = astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec + astAssignmentCost[eCarID].uiTimeToPanel_CC_sec + astAssignmentCost[eCarID].uiTimeToPanel_HC_sec;
      astAssignmentCost[eCarID].uiTotalCost_sec += astAssignmentCost[eCarID].uiTimeToDest_Travel_sec + astAssignmentCost[eCarID].uiTimeToDest_CC_sec + astAssignmentCost[eCarID].uiTimeToDest_HC_sec;
      astAssignmentCost[eCarID].uiTotalCost_sec += astAssignmentCost[eCarID].uiTimeAdded_CC_sec + astAssignmentCost[eCarID].uiTimeAdded_HC_sec;
   }
   else if( Param_ReadValue_8Bit(DDM_PARAM8__DispatchingType) == DDM_DISPATCH_TYPE__TIME_TO_DEST )
   {
      astAssignmentCost[eCarID].uiTotalCost_sec  = astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec + astAssignmentCost[eCarID].uiTimeToPanel_CC_sec + astAssignmentCost[eCarID].uiTimeToPanel_HC_sec;
      astAssignmentCost[eCarID].uiTotalCost_sec += astAssignmentCost[eCarID].uiTimeToDest_Travel_sec + astAssignmentCost[eCarID].uiTimeToDest_CC_sec + astAssignmentCost[eCarID].uiTimeToDest_HC_sec;
   }
   else if( Param_ReadValue_8Bit(DDM_PARAM8__DispatchingType) == DDM_DISPATCH_TYPE__TIME_TO_PICKUP )
   {
      astAssignmentCost[eCarID].uiTotalCost_sec = astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec + astAssignmentCost[eCarID].uiTimeToPanel_CC_sec + astAssignmentCost[eCarID].uiTimeToPanel_HC_sec;
   }
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void GetAssignmentCost_PanelBelowDestBelow( enum en_group_net_nodes eCarID, uint8_t ucDestLanding, uint8_t ucPanelLanding )
{
   /* Bitmaps to mark CCs already accounted for */
   uint8_t aucCallsToPanel[BITMAP8_SIZE(MAX_NUM_FLOORS)] = {0}; // Tracks the CCs cleared on the way to the panel floor
   uint8_t aucCallsToDest[BITMAP8_SIZE(MAX_NUM_FLOORS)] = {0}; // Tracks the CCs cleared on the way to the destination (after reaching the panel floor)

   /* If 1, the car is already heading to the floor */
   uint8_t bPanelLatched, bDestLatched = 0;

   st_cardata *pstCarData = GetCarDataStructure(eCarID);

   /* Check time in motion to reach panel floor */
   uint8_t ucFloorsToPanel = pstCarData->ucCurrentLanding - ucPanelLanding;
   astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec = ucFloorsToPanel * pstCarData->ucMaxFloorToFloorTime_sec;

   /* Check latched CCs between current floor and panel */
   for(int8_t i = pstCarData->ucReachableLanding; i >= ucPanelLanding; i--) {
      if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) || Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
         if( i == ucPanelLanding ) {
            bPanelLatched = 1;
         }
         else {
            astAssignmentCost[eCarID].uiTimeToPanel_CC_sec += pstCarData->ucDoorDwellTime_sec;
            Sys_Bit_Set(&aucCallsToPanel[0], i, 1);
         }
      }
   }

   /* Check CCs not yet latched between current floor and panel */
   for(int8_t i = pstCarData->ucReachableLanding; i >= ucPanelLanding; i--) {
      st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
      /* Are there any calls assigned from this panel? */
      uint8_t ucNumCalls = pstAssignment->ucCallIndex;
      for(uint8_t j = 0; j < ucNumCalls; j++) {
         /* Count the CCs between the destination  */
         uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
         if( ( ucLanding < i )
          && ( ucLanding >= ucPanelLanding )
          && ( !Sys_Bit_Get(&aucCallsToPanel[0], ucLanding) ) ) {
            if( ucLanding == ucPanelLanding ) {
               bPanelLatched = 1;
            }
            else {
               astAssignmentCost[eCarID].uiTimeToPanel_CC_sec += pstCarData->ucDoorDwellTime_sec;
               Sys_Bit_Set(&aucCallsToPanel[0], ucLanding, 1);
            }
         }
      }
   }

   /* Check HCs between current floor and panel */
   for(int8_t i = pstCarData->ucReachableLanding; i >= ucPanelLanding; i--) {
      if( GetLatchedHallCallDown_Front_ByCar(eCarID, i) || GetLatchedHallCallDown_Rear_ByCar(eCarID, i) ) {
         if( i == ucPanelLanding ) {
            bPanelLatched = 1;
         }
         else {
            if( Sys_Bit_Get(&aucCallsToPanel[0], i) ) {
               astAssignmentCost[eCarID].uiTimeToPanel_CC_sec -= pstCarData->ucDoorDwellTime_sec;
            }
            astAssignmentCost[eCarID].uiTimeToPanel_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
         }
      }
   }

   //--------------------------------------------------------------------------
   //--------------------------------------------------------------------------
   /* Check time in motion to reach destination floor */
   uint8_t ucFloorsToDest = ucPanelLanding - ucDestLanding;
   astAssignmentCost[eCarID].uiTimeToDest_Travel_sec = ucFloorsToDest * pstCarData->ucMaxFloorToFloorTime_sec;

   /* Check latched CCs between panel and destination floor */
   for(int8_t i = ucPanelLanding-1; i >= ucDestLanding; i--) {
      if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) || Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
         if( ucDestLanding == i ) {
            bDestLatched = 1;
         }
         else {
            astAssignmentCost[eCarID].uiTimeToDest_CC_sec += pstCarData->ucDoorDwellTime_sec;
            Sys_Bit_Set(&aucCallsToDest[0], i, 1);
         }
      }
   }

   /* Check CCs not yet latched between panel and destination floor */
   for(int8_t i = ucPanelLanding; i > ucDestLanding; i--) {
      st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
      /* Are there any calls assigned from this panel? */
      uint8_t ucNumCalls = pstAssignment->ucCallIndex;
      for(uint8_t j = 0; j < ucNumCalls; j++) {
         /* Count the CCs between the destination  */
         uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
         if( ( ucLanding < i )
          && ( ucLanding >= ucDestLanding )
          && ( !Sys_Bit_Get(&aucCallsToDest[0], ucLanding) ) ) {
            if( ucLanding == ucDestLanding ) {
               bDestLatched = 1;
            }
            else {
               astAssignmentCost[eCarID].uiTimeToDest_CC_sec += pstCarData->ucDoorDwellTime_sec;
               Sys_Bit_Set(&aucCallsToDest[0], ucLanding, 1);
            }
         }
      }
   }

   /* Check HCs between panel and destination floor */
   for(int8_t i = ucPanelLanding-1; i >= ucDestLanding; i--) {
      if( GetLatchedHallCallDown_Front_ByCar(eCarID, i) || GetLatchedHallCallDown_Rear_ByCar(eCarID, i) ) {
         if( ucDestLanding == i ) {
            bDestLatched = 1;
         }
         else {
            if( Sys_Bit_Get(&aucCallsToDest[0], i) ) {
               astAssignmentCost[eCarID].uiTimeToDest_CC_sec -= pstCarData->ucDoorDwellTime_sec;
            }
            astAssignmentCost[eCarID].uiTimeToDest_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
         }
      }
   }

   //--------------------------------------------------------------------------
   //--------------------------------------------------------------------------
   /* Check time added to other passenger's estimated time to destination.
    * Assumes each CC represents a passenger */
   for(int8_t i = ucPanelLanding; i >= pstCarData->ucFirstLanding; i--) {
      /* Any passengers going to floors above the destination floor will be
       * delayed by both the destination and panel floor dwell times */
      if( i <= ucDestLanding ) {
         if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) ) {
            astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
            astAssignmentCost[eCarID].uiTimeAdded_CC_sec += pstCarData->ucDoorDwellTime_sec;
         }
         if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
            astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
            astAssignmentCost[eCarID].uiTimeAdded_CC_sec += pstCarData->ucDoorDwellTime_sec;
         }
         st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
         uint8_t ucNumCalls = pstAssignment->ucCallIndex;
         for(uint8_t j = 0; j < ucNumCalls; j++) {
            /* Count the CCs between the destination  */
            uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
            if( ( ucLanding < i )
             && ( ucLanding != ucDestLanding ) ) {
               astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
               astAssignmentCost[eCarID].uiTimeAdded_CC_sec += pstCarData->ucDoorDwellTime_sec;
            }
         }
      }
      /* Any passengers going to floors below the destination floor will
       * only be delayed by the panel floor dwell time */
      else {
         if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) ) {
            astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
         }
         if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
            astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
         }
         st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
         uint8_t ucNumCalls = pstAssignment->ucCallIndex;
         for(uint8_t j = 0; j < ucNumCalls; j++) {
            /* Count the CCs between the destination  */
            uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
            if( ( ucLanding < i )
             && ( ucLanding != ucDestLanding ) ) {
               astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
            }
         }
      }
   }

   if( bPanelLatched ) {
      astAssignmentCost[eCarID].uiTimeAdded_HC_sec = 0;
   }
   if( bDestLatched ) {
      astAssignmentCost[eCarID].uiTimeAdded_CC_sec = 0;
   }

   if( Param_ReadValue_8Bit(DDM_PARAM8__DispatchingType) == DDM_DISPATCH_TYPE__TOTAL_COST )
   {
      astAssignmentCost[eCarID].uiTotalCost_sec  = astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec + astAssignmentCost[eCarID].uiTimeToPanel_CC_sec + astAssignmentCost[eCarID].uiTimeToPanel_HC_sec;
      astAssignmentCost[eCarID].uiTotalCost_sec += astAssignmentCost[eCarID].uiTimeToDest_Travel_sec + astAssignmentCost[eCarID].uiTimeToDest_CC_sec + astAssignmentCost[eCarID].uiTimeToDest_HC_sec;
      astAssignmentCost[eCarID].uiTotalCost_sec += astAssignmentCost[eCarID].uiTimeAdded_CC_sec + astAssignmentCost[eCarID].uiTimeAdded_HC_sec;
   }
   else if( Param_ReadValue_8Bit(DDM_PARAM8__DispatchingType) == DDM_DISPATCH_TYPE__TIME_TO_DEST )
   {
      astAssignmentCost[eCarID].uiTotalCost_sec  = astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec + astAssignmentCost[eCarID].uiTimeToPanel_CC_sec + astAssignmentCost[eCarID].uiTimeToPanel_HC_sec;
      astAssignmentCost[eCarID].uiTotalCost_sec += astAssignmentCost[eCarID].uiTimeToDest_Travel_sec + astAssignmentCost[eCarID].uiTimeToDest_CC_sec + astAssignmentCost[eCarID].uiTimeToDest_HC_sec;
   }
   else if( Param_ReadValue_8Bit(DDM_PARAM8__DispatchingType) == DDM_DISPATCH_TYPE__TIME_TO_PICKUP )
   {
      astAssignmentCost[eCarID].uiTotalCost_sec = astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec + astAssignmentCost[eCarID].uiTimeToPanel_CC_sec + astAssignmentCost[eCarID].uiTimeToPanel_HC_sec;
   }
}

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void GetAssignmentCost_PanelBelowDestAbove( enum en_group_net_nodes eCarID, uint8_t ucDestLanding, uint8_t ucPanelLanding )
{
   /* Bitmaps to mark CCs already accounted for */
   uint8_t aucCallsToPanel[BITMAP8_SIZE(MAX_NUM_FLOORS)] = {0}; // Tracks the CCs cleared on the way to the panel floor (Pre direction change)
   uint8_t aucCallsToDest[BITMAP8_SIZE(MAX_NUM_FLOORS)] = {0}; // Tracks the CCs cleared on the way to the destination (after reaching the panel floor)
   uint8_t aucCallsToPanel2[BITMAP8_SIZE(MAX_NUM_FLOORS)] = {0}; // Tracks the CCs cleared on the way to the panel floor (Post direction change)

   /* If 1, the car is already heading to the floor */
   uint8_t bPanelLatched, bDestLatched = 0;

   st_cardata *pstCarData = GetCarDataStructure(eCarID);

   uint8_t ucDirectionChangeLanding = INVALID_LANDING;
   for(int8_t i = pstCarData->ucFirstLanding; i < pstCarData->ucReachableLanding; i++) {
      if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) || Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i)
       || GetLatchedHallCallDown_Front_ByCar(eCarID, i) || GetLatchedHallCallDown_Rear_ByCar(eCarID, i)
       || GetLatchedHallCallUp_Front_ByCar(eCarID, i) || GetLatchedHallCallUp_Rear_ByCar(eCarID, i) ) {
         if( ucDirectionChangeLanding > i ) {
            ucDirectionChangeLanding = i;
         }
      }
      else {
         st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
         /* Are there any calls assigned from this panel? */
         uint8_t ucNumCalls = pstAssignment->ucCallIndex;
         for(uint8_t j = 0; j < ucNumCalls; j++) {
            uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
            if( ( ucLanding < ucPanelLanding )
             && ( ucDirectionChangeLanding > ucLanding ) ) {
               ucDirectionChangeLanding = ucLanding;
            }
         }
      }
   }

   /* Car will proceed directly to the panel floor */
   if( ucDirectionChangeLanding == INVALID_LANDING ) {
      /* Check time in motion to reach panel floor */
      uint8_t ucFloorsToPanel = pstCarData->ucCurrentLanding - ucPanelLanding;
      astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec = ucFloorsToPanel * pstCarData->ucMaxFloorToFloorTime_sec;

      /* Check latched CCs between current floor and panel */
      for(int8_t i = pstCarData->ucReachableLanding; i >= ucPanelLanding; i--) {
         if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) || Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
            if( i == ucPanelLanding ) {
               bPanelLatched = 1;
            }
            else {
               astAssignmentCost[eCarID].uiTimeToPanel_CC_sec += pstCarData->ucDoorDwellTime_sec;
               Sys_Bit_Set(&aucCallsToPanel[0], i, 1);
            }
         }
      }

      /* Check CCs not yet latched between current floor and panel */
      for(int8_t i = pstCarData->ucReachableLanding; i >= ucPanelLanding; i--) {
         st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
         /* Are there any calls assigned from this panel? */
         uint8_t ucNumCalls = pstAssignment->ucCallIndex;
         for(uint8_t j = 0; j < ucNumCalls; j++) {
            /* Count the CCs between the destination  */
            uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
            if( ( ucLanding < i )
             && ( ucLanding >= ucPanelLanding )
             && ( !Sys_Bit_Get(&aucCallsToPanel[0], ucLanding) ) ) {
               if( ucLanding == ucPanelLanding ) {
                  bPanelLatched = 1;
               }
               else {
                  astAssignmentCost[eCarID].uiTimeToPanel_CC_sec += pstCarData->ucDoorDwellTime_sec;
                  Sys_Bit_Set(&aucCallsToPanel[0], ucLanding, 1);
               }
            }
         }
      }

      /* Check HCs between current floor and panel */
      for(int8_t i = pstCarData->ucReachableLanding; i >= ucPanelLanding; i--) {
         if( i == ucPanelLanding ) {
            if( GetLatchedHallCallUp_Front_ByCar(eCarID, i) || GetLatchedHallCallUp_Rear_ByCar(eCarID, i) ) {
               bPanelLatched = 1;
            }
         }
         else if( GetLatchedHallCallDown_Front_ByCar(eCarID, i) || GetLatchedHallCallDown_Rear_ByCar(eCarID, i) ) {
            if( Sys_Bit_Get(&aucCallsToPanel[0], i) ) {
               astAssignmentCost[eCarID].uiTimeToPanel_CC_sec -= pstCarData->ucDoorDwellTime_sec;
            }
            astAssignmentCost[eCarID].uiTimeToPanel_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
         }
      }

      //--------------------------------------------------------------------------
      //--------------------------------------------------------------------------
      /* Check time in motion to reach destination floor */
      uint8_t ucFloorsToDest = ucDestLanding - ucPanelLanding;
      astAssignmentCost[eCarID].uiTimeToDest_Travel_sec = ucFloorsToDest * pstCarData->ucMaxFloorToFloorTime_sec;

      /* Check latched CCs between panel and destination floor */
      for(int8_t i = ucPanelLanding+1; i <= ucDestLanding; i++) {
         if( ( !Sys_Bit_Get(&aucCallsToPanel[0], i) )
          && ( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) || Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) ) {
            if( ucDestLanding == i ) {
               bDestLatched = 1;
            }
            else {
               astAssignmentCost[eCarID].uiTimeToDest_CC_sec += pstCarData->ucDoorDwellTime_sec;
               Sys_Bit_Set(&aucCallsToDest[0], i, 1);
            }
         }
      }

      /* Check CCs not yet latched between panel and destination floor */
      for(int8_t i = ucPanelLanding; i < ucDestLanding; i++) {
         st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
         /* Are there any calls assigned from this panel? */
         uint8_t ucNumCalls = pstAssignment->ucCallIndex;
         for(uint8_t j = 0; j < ucNumCalls; j++) {
            /* Count the CCs between the destination  */
            uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
            if( ( ucLanding > i )
             && ( ucLanding <= ucDestLanding )
             && ( !Sys_Bit_Get(&aucCallsToDest[0], ucLanding) ) ) {
               if( ucLanding == ucDestLanding ) {
                  bDestLatched = 1;
               }
               else {
                  astAssignmentCost[eCarID].uiTimeToDest_CC_sec += pstCarData->ucDoorDwellTime_sec;
                  Sys_Bit_Set(&aucCallsToDest[0], ucLanding, 1);
               }
            }
         }
      }

      /* Check HCs between panel and destination floor */
      for(int8_t i = ucPanelLanding+1; i <= ucDestLanding; i++) {
         if( GetLatchedHallCallUp_Front_ByCar(eCarID, i) || GetLatchedHallCallUp_Rear_ByCar(eCarID, i) ) {
            if( ucDestLanding == i ) {
               bDestLatched = 1;
            }
            else {
               if( Sys_Bit_Get(&aucCallsToDest[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeToDest_CC_sec -= pstCarData->ucDoorDwellTime_sec;
               }
               astAssignmentCost[eCarID].uiTimeToDest_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
            }
         }
      }

      //--------------------------------------------------------------------------
      //--------------------------------------------------------------------------
      /* Check time added to other passenger's estimated time to destination.
       * Assumes each CC represents a passenger */
      for(int8_t i = ucPanelLanding; i <= pstCarData->ucLastLanding; i++) {
         st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
         uint8_t ucNumCalls = pstAssignment->ucCallIndex;
         for(uint8_t j = 0; j < ucNumCalls; j++) {
            /* Count the CCs between the destination  */
            uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
            if( ucLanding >= ucDestLanding )
            {
               if( ( ucLanding > i )
                && ( ucLanding != ucDestLanding ) ) {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
                  astAssignmentCost[eCarID].uiTimeAdded_CC_sec += pstCarData->ucDoorDwellTime_sec;
               }
            }
            else if( ( ucLanding > i )
                  && ( ucLanding != ucDestLanding ) ) {
               astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
            }
         }
         /* Any passengers going to floors after the destination floor will be
          * delayed by both the destination and panel floor dwell times */
         if( i >= ucDestLanding ) {
            if( !Sys_Bit_Get(&aucCallsToPanel[0], i) ) {
               if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
                  astAssignmentCost[eCarID].uiTimeAdded_CC_sec += pstCarData->ucDoorDwellTime_sec;
               }
               if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
                  astAssignmentCost[eCarID].uiTimeAdded_CC_sec += pstCarData->ucDoorDwellTime_sec;
               }
            }
         }
         /* Any passengers going to floors below the panel and destination floor will
          * only be delayed by the panel floor dwell time */
         else {
            /* This is a return path so ignore calls that will be serviced on the way to the panel floor */
            if( !Sys_Bit_Get(&aucCallsToPanel[0], i) ) {
               if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
               }
               if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
               }
            }
         }
      }
   }
   /* Car will move to past the panel floor then return to the panel floor */
   else
   {
      /* Check time in motion to reach panel floor */
      uint8_t ucFloorsToPanel = ( pstCarData->ucCurrentLanding - ucDirectionChangeLanding )
                              + ( ucPanelLanding - ucDirectionChangeLanding );
      astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec = ucFloorsToPanel * pstCarData->ucMaxFloorToFloorTime_sec;

      /* Check latched CCs between current floor and panel */
      for(int8_t i = pstCarData->ucReachableLanding; i >= ucDirectionChangeLanding; i--) {
         if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) || Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
            astAssignmentCost[eCarID].uiTimeToPanel_CC_sec += pstCarData->ucDoorDwellTime_sec;
            Sys_Bit_Set(&aucCallsToPanel[0], i, 1);
         }
      }

      /* Check CCs not yet latched between current floor and panel (up to the direction change floor)*/
      for(int8_t i = pstCarData->ucReachableLanding; i >= ucDirectionChangeLanding; i--) {
         st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
         /* Are there any calls assigned from this panel? */
         uint8_t ucNumCalls = pstAssignment->ucCallIndex;
         for(uint8_t j = 0; j < ucNumCalls; j++) {
            /* Count the CCs between the destination  */
            uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
            if( ( ucLanding < i )
             && ( ucLanding >= ucDirectionChangeLanding )
             && ( !Sys_Bit_Get(&aucCallsToPanel[0], ucLanding) ) ) {
               astAssignmentCost[eCarID].uiTimeToPanel_CC_sec += pstCarData->ucDoorDwellTime_sec;
               Sys_Bit_Set(&aucCallsToPanel[0], ucLanding, 1);
            }
         }
      }
      /* Check CCs not yet latched between current floor and panel (after the direction change floor)*/
      for(int8_t i = ucDirectionChangeLanding; i <= ucPanelLanding; i++) {
         st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
         /* Are there any calls assigned from this panel? */
         uint8_t ucNumCalls = pstAssignment->ucCallIndex;
         for(uint8_t j = 0; j < ucNumCalls; j++) {
            /* Count the CCs between the destination  */
            uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
            if( ucLanding > i )
            {
               if( ( ucLanding < ucPanelLanding )
                && ( !Sys_Bit_Get(&aucCallsToPanel2[0], ucLanding) ) ) {
                 astAssignmentCost[eCarID].uiTimeToPanel_CC_sec += pstCarData->ucDoorDwellTime_sec;
                 Sys_Bit_Set(&aucCallsToPanel2[0], ucLanding, 1);
              }
              if( ucLanding == ucPanelLanding ) {
                 bPanelLatched = 1;
              }
            }
         }
      }

      /* Check HCs between current floor and panel (pre direction change)*/
      for(int8_t i = pstCarData->ucReachableLanding; i >= ucDirectionChangeLanding; i--) {
         if( GetLatchedHallCallDown_Front_ByCar(eCarID, i) || GetLatchedHallCallDown_Rear_ByCar(eCarID, i) ) {
            if( Sys_Bit_Get(&aucCallsToPanel[0], i) ) {
               astAssignmentCost[eCarID].uiTimeToPanel_CC_sec -= pstCarData->ucDoorDwellTime_sec;
            }
            astAssignmentCost[eCarID].uiTimeToPanel_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
         }
      }
      /* Check HCs between current floor and panel (post direction change)*/
      for(int8_t i = ucDirectionChangeLanding; i <= ucPanelLanding; i++) {
         if( GetLatchedHallCallUp_Front_ByCar(eCarID, i) || GetLatchedHallCallUp_Rear_ByCar(eCarID, i) ) {
            if( Sys_Bit_Get(&aucCallsToPanel2[0], i) ) {
               astAssignmentCost[eCarID].uiTimeToPanel_CC_sec -= pstCarData->ucDoorDwellTime_sec;
            }
            if( i == ucPanelLanding ) {
               bPanelLatched = 1;
            }
            else
            {
               astAssignmentCost[eCarID].uiTimeToPanel_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
            }
         }
      }

      //--------------------------------------------------------------------------
      //--------------------------------------------------------------------------
      /* Check time in motion to reach destination floor */
      uint8_t ucFloorsToDest = ucDestLanding - ucPanelLanding;
      astAssignmentCost[eCarID].uiTimeToDest_Travel_sec = ucFloorsToDest * pstCarData->ucMaxFloorToFloorTime_sec;

      /* Check latched CCs between panel and destination floor */
      for(int8_t i = ucPanelLanding+1; i <= ucDestLanding; i++) {
         if( ( !Sys_Bit_Get(&aucCallsToPanel[0], i) )
          && ( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) || Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) ) {
            if( ucDestLanding == i ) {
               bDestLatched = 1;
            }
            else {
               astAssignmentCost[eCarID].uiTimeToDest_CC_sec += pstCarData->ucDoorDwellTime_sec;
               Sys_Bit_Set(&aucCallsToDest[0], i, 1);
            }
         }
      }

      /* Check CCs not yet latched between panel and destination floor */
      for(int8_t i = ucPanelLanding+1; i < ucDestLanding; i++) {
         st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
         /* Are there any calls assigned from this panel? */
         uint8_t ucNumCalls = pstAssignment->ucCallIndex;
         for(uint8_t j = 0; j < ucNumCalls; j++) {
            /* Count the CCs between the destination  */
            uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
            if( ucLanding > i ) {
               if( ucLanding == ucDestLanding ) {
                  bDestLatched = 1;
               }
               else if( ( ucLanding < ucDestLanding )
                     && ( !Sys_Bit_Get(&aucCallsToDest[0], ucLanding) ) ) {
                  astAssignmentCost[eCarID].uiTimeToDest_CC_sec += pstCarData->ucDoorDwellTime_sec;
                  Sys_Bit_Set(&aucCallsToDest[0], ucLanding, 1);
               }
            }
         }
      }

      /* Check HCs between panel and destination floor */
      for(int8_t i = ucPanelLanding+1; i <= ucDestLanding; i++) {
         if( GetLatchedHallCallUp_Front_ByCar(eCarID, i) || GetLatchedHallCallUp_Rear_ByCar(eCarID, i) ) {
            if( ucDestLanding == i ) {
               bDestLatched = 1;
            }
            else {
               if( Sys_Bit_Get(&aucCallsToDest[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeToDest_CC_sec -= pstCarData->ucDoorDwellTime_sec;
               }
               astAssignmentCost[eCarID].uiTimeToDest_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
            }
         }
      }

      //--------------------------------------------------------------------------
      //--------------------------------------------------------------------------
      /* Check time added to other passenger's estimated time to destination.
       * Assumes each CC represents a passenger */
      for(int8_t i = ucPanelLanding; i <= pstCarData->ucLastLanding; i++) {
         /* Any passengers going to floors after the destination floor will be
          * delayed by both the destination and panel floor dwell times */
         if( i >= ucDestLanding ) {
            if( !Sys_Bit_Get(&aucCallsToPanel[0], i) ) {
               if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
                  astAssignmentCost[eCarID].uiTimeAdded_CC_sec += pstCarData->ucDoorDwellTime_sec;
               }
               if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
                  astAssignmentCost[eCarID].uiTimeAdded_CC_sec += pstCarData->ucDoorDwellTime_sec;
               }
            }
         }
         /* Any passengers going to floors between the panel and destination are
          * delayed by the panel floor dwell time */
         else {
            /* This is a return path so ignore calls that will be serviced on the way to the panel floor */
            if( !Sys_Bit_Get(&aucCallsToPanel[0], i) ) {
               if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_F[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
               }
               if( Sys_Bit_Get(&pstCarData->auiBF_CarCalls_R[0], i) ) {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
               }
            }
         }

         /* Check for assignments that haven't been latched */
         st_kiosk_assignments *pstAssignment = DDPanel_GetAssignmentStructure(eCarID, i);
         uint8_t ucNumCalls = pstAssignment->ucCallIndex;
         for(uint8_t j = 0; j < ucNumCalls; j++) {
            /* Count the CCs between the destination  */
            uint8_t ucLanding = pstAssignment->aucLanding_Dest_Plus1[j]-1;
            if( ( !Sys_Bit_Get(&aucCallsToPanel[0], ucLanding) )
             && ( ucLanding > i ) // TODO review if this condition is necessary
              ) {
               if( ucLanding >= ucDestLanding )
               {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
                  astAssignmentCost[eCarID].uiTimeAdded_CC_sec += pstCarData->ucDoorDwellTime_sec;
               }
               else
               {
                  astAssignmentCost[eCarID].uiTimeAdded_HC_sec += pstCarData->ucDoorDwellHallTime_sec;
               }
            }
         }
      }
   }

   if( bPanelLatched ) {
      astAssignmentCost[eCarID].uiTimeAdded_HC_sec = 0;
   }
   if( bDestLatched ) {
      astAssignmentCost[eCarID].uiTimeAdded_CC_sec = 0;
   }

   if( Param_ReadValue_8Bit(DDM_PARAM8__DispatchingType) == DDM_DISPATCH_TYPE__TOTAL_COST )
   {
      astAssignmentCost[eCarID].uiTotalCost_sec  = astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec + astAssignmentCost[eCarID].uiTimeToPanel_CC_sec + astAssignmentCost[eCarID].uiTimeToPanel_HC_sec;
      astAssignmentCost[eCarID].uiTotalCost_sec += astAssignmentCost[eCarID].uiTimeToDest_Travel_sec + astAssignmentCost[eCarID].uiTimeToDest_CC_sec + astAssignmentCost[eCarID].uiTimeToDest_HC_sec;
      astAssignmentCost[eCarID].uiTotalCost_sec += astAssignmentCost[eCarID].uiTimeAdded_CC_sec + astAssignmentCost[eCarID].uiTimeAdded_HC_sec;
   }
   else if( Param_ReadValue_8Bit(DDM_PARAM8__DispatchingType) == DDM_DISPATCH_TYPE__TIME_TO_DEST )
   {
      astAssignmentCost[eCarID].uiTotalCost_sec  = astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec + astAssignmentCost[eCarID].uiTimeToPanel_CC_sec + astAssignmentCost[eCarID].uiTimeToPanel_HC_sec;
      astAssignmentCost[eCarID].uiTotalCost_sec += astAssignmentCost[eCarID].uiTimeToDest_Travel_sec + astAssignmentCost[eCarID].uiTimeToDest_CC_sec + astAssignmentCost[eCarID].uiTimeToDest_HC_sec;
   }
   else if( Param_ReadValue_8Bit(DDM_PARAM8__DispatchingType) == DDM_DISPATCH_TYPE__TIME_TO_PICKUP )
   {
      astAssignmentCost[eCarID].uiTotalCost_sec = astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec + astAssignmentCost[eCarID].uiTimeToPanel_CC_sec + astAssignmentCost[eCarID].uiTimeToPanel_HC_sec;
   }
}
/*----------------------------------------------------------------------------
   Returns the cost in seconds of assigning a call to a given car

   Time To Panel: The est. time between call entry and passenger pick up.
      astAssignmentCost[eCarID].uiTimeToPanel_Travel_sec: The est. time spent in motion before before passenger pick up.
      astAssignmentCost[eCarID].uiTimeToPanel_CC_sec: The est. time spent on CC dwell times before passenger pick up.
      astAssignmentCost[eCarID].uiTimeToPanel_HC_sec: The est. time spent on HC dwell times before passenger pick up.

   Time To Destination: The est. time between passenger pick up and drop off.
      astAssignmentCost[eCarID].uiTimeToDest_Travel_sec: The est. time spent in motion before before passenger drop off.
      astAssignmentCost[eCarID].uiTimeToDest_CC_sec: The est. time spent on CC dwell times before passenger drop off.
      astAssignmentCost[eCarID].uiTimeToDest_HC_sec: The est. time spent on HC dwell times before passenger drop off.

   Total Cost = (Time To Panel) + (Time To Destination) + (Cost To Other Passengers)

   Note: TODO calculation assume the car will proceed directly to the panel landing, then to the destination landing, taking calls in between but not overshooting
         TODO factor in masked out car destinations in HC calc
         TODO factor in car dir priority
         TODO factor in cost to other passengers

   Based on: https://www.peters-research.com/index.php/support/articles-and-papers/42-etd-algorithm-with-destination-dispatch-and-booster-options

TODO add full load lockout
TODO add LWD factoring
 *----------------------------------------------------------------------------*/
uint32_t TotalCost_GetAssignmentCost_sec( enum en_group_net_nodes eCarID, uint8_t ucDestLanding, uint8_t ucPanelLanding )
{
   st_cardata *pstCarData = GetCarDataStructure(eCarID);

   astAssignmentCost[eCarID].ucCurrentLanding = pstCarData->ucCurrentLanding;
   astAssignmentCost[eCarID].ucDestinationLanding = ucDestLanding;
   astAssignmentCost[eCarID].ucPanelLanding = ucPanelLanding;

   uint8_t ucCurrentCarDestination = CarDestination_GetLanding(eCarID);
   /* The requesting panel is above AND
    *    Car is serving up calls
    *    OR
    *    Car is idle, and has no destination */
   if( ( pstCarData->ucReachableLanding <= ucPanelLanding ) &&
       ( ( pstCarData->enPriority == HC_DIR__UP ) ||
         ( ( pstCarData->bIdleDirection ) && ( ucCurrentCarDestination == INVALID_LANDING ) ) ) )
   {
      /* Car moving up and destination is above the panel floor... */
      if( ucDestLanding >= ucPanelLanding )
      {
         GetAssignmentCost_PanelAboveDestAbove(eCarID, ucDestLanding, ucPanelLanding);
      }
      /* Car moving up and destination is below the panel floor... */
      else
      {
         GetAssignmentCost_PanelAboveDestBelow(eCarID, ucDestLanding, ucPanelLanding);
      }
   }
   /* The requesting panel is below AND
    *    Car is serving down calls
    *    OR
    *    Car is idle, and has no destination */
   else if( ( pstCarData->ucReachableLanding >= ucPanelLanding ) &&
            ( ( pstCarData->enPriority == HC_DIR__DOWN ) ||
            ( ( pstCarData->bIdleDirection ) && ( ucCurrentCarDestination == INVALID_LANDING ) ) ) )
   {
      /* Car moving down and destination is below the panel floor... */
      if( ucDestLanding <= ucPanelLanding )
      {
         GetAssignmentCost_PanelBelowDestBelow(eCarID, ucDestLanding, ucPanelLanding);
      }
      /* Car moving down and destination is above the panel floor... */
      else
      {
         GetAssignmentCost_PanelBelowDestAbove(eCarID, ucDestLanding, ucPanelLanding);
      }
   }
   else {
      /* If car is moving away from the panel/dest floor, can't make the floor, or is serving the opposing direction, ignore it. */
      astAssignmentCost[eCarID].uiTotalCost_sec = INVALID_TOTAL_ASSIGNMENT_COST_SEC;
   }

   return astAssignmentCost[eCarID].uiTotalCost_sec;
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
