/******************************************************************************
 *
 * @file     alarmLog.c
 * @brief
 * @version  V1.00
 * @date     13, June 2018
 * @author   Keith Soneda
 *
 * @note     Functions for maintaining and accessing a running log of DDM board faults. These faults are lost on reset.
 *
 *****************************************************************************/
#ifndef _ALARM_LOG_H_
#define _ALARM_LOG_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include "DDM_Alarms.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define NUM_ALARM_LOG_ELEMENTS         (32)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void AlarmLog_ClearLog(void);
en_ddm_alarms AlarmLog_GetAlarm(uint8_t ucIndex);
uint32_t AlarmLog_GetTimestamp(uint8_t ucIndex);
void AlarmLog_UpdateLog(void);
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
