/******************************************************************************
 *
 * @file     ui.h
 * @brief    UI Header File
 * @version  V1.00
 * @date     25, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef UI_H
#define UI_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "sru_b.h"
#include "buttons.h"
#include <stdint.h>
#include "lcd.h"
#include "DDM_Alarms.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
enum en_number_decimal_formats
{
   enNumberDecimalPoints_None,
   enNumberDecimalPoints_One,
   enNumberDecimalPoints_Two,
   enNumberDecimalPoints_Three,
};

enum en_save_state
{
   SAVE_STATE__UNK,
   SAVE_STATE__SAVED,//aka idle
   SAVE_STATE__CLEARING,
   SAVE_STATE__SAVING,

   NUM_SAVE_STATE,
};
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
//-----------------------------------------------------------------------------


struct st_set_flag_menu
{
      uint8_t bFlag;
      uint8_t bFinished;
      uint8_t ucCursorColumn;

      uint8_t bSaving;
      uint8_t bInvalid;

      char * psTitle;
      char * psActive;
};
//-----------------------------------------------------------------------------


struct st_param_edit_menu
{
      uint32_t ulValue_Saved;
      uint32_t ulValue_Edited;
      uint32_t ulValue_Limit;
      uint32_t ulValue_Max;
      uint32_t ulValue_Min;
      uint16_t uwParamIndex_Start;
      uint16_t uwParamIndex_End;
      uint16_t uwParamIndex_Current;
      uint8_t ucCursorColumn;
      uint8_t ucFieldIndex;
      uint8_t ucFieldSize_Index;
      uint8_t ucFieldSize_Value;
      uint8_t bSaving;
      uint8_t bInvalid;

      uint8_t ucArraySize;
      char * ((*ucaPTRChoiceList)[]);

      enum en_param_block_types enParamType;
      enum en_number_decimal_formats  enDecimalFormat;
      char * psTitle;
      char * psUnit;
      /* For use with timers with non-base 10 units,
       * valid only if decimal format is enNumberDecimalPoints_None.
       * Max of 100 */
      uint8_t ucValueMultiplier;
};
struct st_runtime_signal_edit_menu
{
      uint32_t ulValue_Saved;
      uint32_t ulValue_Edited;
      uint32_t ulValue_Limit;
      uint8_t ucCursorColumn;
      uint8_t ucFieldIndex;
      uint8_t ucFieldSize_Index;
      uint8_t ucFieldSize_Value;
      uint8_t bSaving;
      uint8_t bInvalid;

      uint32_t (*pfnGetSignal) (void);
      void (*pfnSetSignal) (uint32_t);

      uint8_t *pucSignal;
      uint8_t ucSignalSize;

      enum en_param_block_types enParamType;
      enum en_number_decimal_formats  enDecimalFormat;
      char * psTitle;
      char * pasValueStrings;
};

struct st_ui_parameter_edit
{
      enum en_param_block_types enParamType;

      uint16_t uwParamIndex_Current;
      uint16_t uwParamIndex_Start;
      uint16_t uwParamIndex_End;

      uint32_t ulValue_Saved;
      uint32_t ulValue_Edited;

      uint8_t ucCursorColumn;

      uint8_t ucFieldIndex;
      uint8_t ucFieldSize_Index;
      uint8_t ucFieldSize_Value;

      uint32_t ulMask;

      uint8_t ucShift; // if data is shifted into larger parameter size

      uint8_t bSaving;
      uint8_t bInvalid;
};

enum en_ui_screen_types
{
   enUI_STYPE__FREEFORM,
   enUI_STYPE__MENU,
   enUI_STYPE__PARAMETER,
};
enum en_parm_edit_format
{
   enEDIT_FORMAT__HEX,
   enEDIT_FORMAT__DEC,
   enEDIT_FORMAT__BIN,

   NUM_EDIT_FORMATS
};

enum en_parm_edit_fields
{
   enFIELD__NONE,  // this is the field when screen is first entered
   enFIELD__BOARD,
   enFIELD__PIN,
   enFIELD__BLOCK,
   enFIELD__INDEX,
   enFIELD__VALUE,
   enFIELD__SAVE,
   enFIELD__DATA_TYPE,//for debug memory
   enFIELD__MCU,
   enFIELD__IO_GROUP,
   enFIELD__IO_FUNCT,
   enFIELD__FLOOR,
   NUM_PARM_EDIT_FIELDS,
};

enum en_field_action
{
   enACTION__NONE,

   enACTION__EDIT,
   enACTION__ENTER_FROM_RIGHT,
   enACTION__ENTER_FROM_LEFT,
   enACTION__EXIT_TO_LEFT,
   enACTION__EXIT_TO_RIGHT,

   NUM_FIELD_ACTIONS
};
enum en_setup_format
{
   enSETUP_FORMAT__GENERIC,
   enSETUP_FORMAT__PI_LABEL,
   enSETUP_FORMAT__SPEED,
   enSETUP_FORMAT__VOLTAGE,
   enSETUP_FORMAT__DELAY,
   enSETUP_FORMAT__NTS_TRIP,
   enSETUP_FORMAT__NTS_POS,
   enSETUP_FORMAT__NTS_VEL,
   enSETUP_FORMAT__LEARNED,
   enSETUP_FORMAT__CC_BL,
   enSETUP_FORMAT__IO,
   NUM_SETUP_FORMATS
};

//-----------------------------------------------------------------------------
struct st_ui_generic_screen
{
   struct st_ui_generic_screen *pstUGS_Previous;
   void *pvReference;

   const uint8_t ucType;
};

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
struct st_ui_screen__freeform
{
   void (*pfnDraw)( void );
};

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
struct st_ui_menu_item
{
   char *psTitle;
   struct st_ui_generic_screen *pstUGS_Next;
};

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
struct st_ui_screen__menu
{
   char *psTitle;

   struct st_ui_menu_item * (* pastMenuItems)[];

   uint8_t ucNumItems;
   uint8_t ucTopItem;
   uint8_t ucCurrentItem;
};

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
struct st_ui_control
{
   struct st_ui_generic_screen *pstUGS_Current;
};

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

extern struct st_ui_generic_screen gstUGS_HomeScreen;
extern struct st_ui_generic_screen gstUGS_MainMenu;
extern struct st_ui_generic_screen gstUGS_Menu_Status;
extern struct st_ui_generic_screen gstUGS_AboutScreen;
extern struct st_ui_generic_screen gstUGS_Menu_Inputs;
extern struct st_ui_generic_screen gstUGS_Menu_Outputs;
extern struct st_ui_generic_screen gstUGS_Menu_Setup;

extern struct st_ui_generic_screen gstUGS_Menu_ParamEdit;
extern struct st_ui_generic_screen gstUGS_DecimalParams;
extern struct st_ui_generic_screen gstUGS_HexParams;

extern struct st_ui_generic_screen gstUGS_Menu_Setup_Misc;

extern struct st_ui_generic_screen gstUGS_Misc_DefaultAll;

extern struct st_ui_generic_screen gstUGS_Menu_Debug;

extern struct st_ui_generic_screen gstUGS_Debug_ViewDebugData;

extern struct st_ui_generic_screen gstUGS_Menu_Alarms;
extern struct st_ui_generic_screen gstUGS_ActiveAlarms;
extern struct st_ui_generic_screen gstUGS_PopUpAlarms;
extern struct st_ui_generic_screen gstUGS_LoggedAlarms;
extern struct st_ui_generic_screen gstUGS_ClearAlarms;

extern struct st_ui_generic_screen gstUGS_AssignedCalls;

extern struct st_ui_generic_screen gstUGS_Setup_ScreenTO;
extern struct st_ui_generic_screen gstUGS_Setup_ScreenAssistTO;
extern struct st_ui_generic_screen gstUGS_Setup_KeyTO;
extern struct st_ui_generic_screen gstUGS_Setup_KeyAssistTO;
extern struct st_ui_generic_screen gstUGS_Setup_CodeTO;
extern struct st_ui_generic_screen gstUGS_Setup_CodeAssistTO;
extern struct st_ui_generic_screen gstUGS_Setup_DispRespTO;
extern struct st_ui_generic_screen gstUGS_Setup_WeightPerCall;
extern struct st_ui_generic_screen gstUGS_Setup_RatedLoad_C1;
extern struct st_ui_generic_screen gstUGS_Setup_RatedLoad_C2;
extern struct st_ui_generic_screen gstUGS_Setup_RatedLoad_C3;
extern struct st_ui_generic_screen gstUGS_Setup_RatedLoad_C4;
extern struct st_ui_generic_screen gstUGS_Setup_RatedLoad_C5;
extern struct st_ui_generic_screen gstUGS_Setup_RatedLoad_C6;
extern struct st_ui_generic_screen gstUGS_Setup_RatedLoad_C7;
extern struct st_ui_generic_screen gstUGS_Setup_RatedLoad_C8;
extern struct st_ui_generic_screen gstUGS_Setup_InProximityEntry;
extern struct st_ui_generic_screen gstUGS_Setup_DisableEntryTimer;
extern struct st_ui_generic_screen gstUGS_Setup_EnableDuparPanel;
extern struct st_ui_generic_screen gstUGS_Menu_Setup_Panel;

extern struct st_ui_generic_screen gstUGS_Setup_EnableSecurityCode;
extern struct st_ui_generic_screen gstUGS_Setup_EnableSecurityDevice;
extern struct st_ui_generic_screen gstUGS_Setup_SecurityCode_F;
extern struct st_ui_generic_screen gstUGS_Setup_SecurityCode_R;
extern struct st_ui_generic_screen gstUGS_Menu_Setup_Security;

extern struct st_ui_generic_screen gstUGS_CarData;

extern struct st_ui_generic_screen gstUGS_PanelStatus;

extern struct st_ui_generic_screen gstUGS_ExpansionStatus1;
extern struct st_ui_generic_screen gstUGS_ExpansionStatus2;
extern struct st_ui_generic_screen gstUGS_ExpansionStatus3;
extern struct st_ui_generic_screen gstUGS_ExpansionStatus4;
extern struct st_ui_generic_screen gstUGS_ExpansionStatus5;
extern struct st_ui_generic_screen gstUGS_Menu_ExpansionStatus;

extern struct st_ui_generic_screen gstUGS_Menu_HallCallStatus;
extern struct st_ui_generic_screen gstUGS_HallCallStatus_Up;
extern struct st_ui_generic_screen gstUGS_HallCallStatus_Down;

extern struct st_ui_generic_screen gstUGS_RiserStatus;
extern struct st_ui_generic_screen gstUGS_Debug_ViewGroupPackets;
extern struct st_ui_generic_screen gstUGS_CarDestination;

extern struct st_ui_generic_screen gstUGS_TotalCostData;

extern struct st_ui_generic_screen gstUGS_DispatchType;

extern struct st_ui_control gstUI_Control;

extern uint8_t gbParamSaveInProgress;//5-25

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

extern void gEdit_Parameter(struct st_ui_parameter_edit * pstParamEdit);

extern void Print_AccessDeniedScreen( const char * psDenialReason );

void LCD_WriteDistanceInInches( uint32_t ulCount_500UM, uint8_t bInchesOnly );

void PrevScreen( void );
void NextScreen( struct st_ui_generic_screen *pstUGS_Next );

void Draw_Menu( struct st_ui_screen__menu * pstMenuScreen );
void UI_ParamEditSceenTemplate( struct st_param_edit_menu * pstParamEditMenu );

uint8_t Param_PrintParamString_1Bit( enum en_1bit_params enParamNum);
uint8_t Param_PrintParamString_8Bit( enum en_8bit_params enParamNum);
uint8_t Param_PrintParamString_16Bit( enum en_16bit_params enParamNum);
uint8_t Param_PrintParamString_24Bit( enum en_24bit_params enParamNum);
uint8_t Param_PrintParamString_32Bit( enum en_32bit_params enParamNum);

void PrintFaultString( en_faults eFaultNum );
char const * const GetAlarmString( en_ddm_alarms eAlarmNum );
void PrintAlarmString( en_ddm_alarms eAlarmNum );
void PrintMovingText( char const * const psString );//one per screen
void UI_UpdateMovingTextVariables(struct st_ui_control *pstUI_Control);

void UI_UpdateNewPageFlag(struct st_ui_control *pstUI_Control);
uint8_t UI_GetNewPageFlag();

void UI_PrintDoorSymbol( enum en_door_states eState,
                         uint8_t bPHE, uint8_t bGSW,
                         uint8_t bDO, uint8_t bDC );
#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
