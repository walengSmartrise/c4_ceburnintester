/******************************************************************************
 *
 * @file     mod.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef MOD_H
#define MOD_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
#include "alarm_app.h"
#include "shared_data.h"
#include "datagrams.h"

#include "sru_b.h"
#include "motion.h"
#include "position.h"
#include "operation.h"
#include "DDM_Alarms.h"
#include "carData.h"
#include "kioskData.h"
#include "ddCommand.h"
#include "ddm_param_1Bit.h"
#include "ddm_param_8Bit.h"
#include "ddm_param_16Bit.h"
#include "ddm_param_24Bit.h"
#include "ddm_param_32Bit.h"
#include "ddm_network.h"
#include "ab_net_data.h"
#include "expData.h"
#include "capacityData.h"
#include "hallData.h"
#include "riserData.h"
#include "carDestination.h"
#include "masterCommand.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_UART_1MS              (5U)
#define MOD_RUN_PERIOD_CAN_1MS               (5U)
#define MOD_RUN_PERIOD_AB_NET_1MS            (5U)
#define MOD_RUN_PERIOD_UI_1MS                (30U)
#define MOD_RUN_PERIOD_AUX_NET_1MS           ( 5 )

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

extern struct st_module_control *gpstModuleControl_ThisDeployment;

extern struct st_sdata_control gstSData_LocalData;

extern struct st_module gstMod_UART;
extern struct st_module gstMod_CAN;
extern struct st_module gstMod_SData;
extern struct st_module gstMod_SData_AuxNet;
extern struct st_module gstMod_SData_GroupNet;

extern struct st_module gstMod_UI;
extern struct st_module gstMod_Buttons;
extern struct st_module gstMod_LCD;

extern struct st_module gstMod_Heartbeat;

extern struct st_module gstMod_Watchdog;

extern struct st_module gstMod_LocalOutputs;

extern struct st_module gstMod_KioskManager;

extern struct st_module gstMod_ParamMaster;
extern struct st_module gstMod_ParamApp;

extern struct st_module gstMod_ExpansionManager;
extern struct st_module gstMod_CapacityManager;

extern char *pasVersion;
 /*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

void SharedData_GroupNet_Init();
void SharedData_AuxNet_Init();
void SharedData_Init();

extern uint8_t ParamApp_GetFault(void);

uint8_t SaveAndVerifyParameter( struct st_param_and_value *pstPV );

uint16_t UART_GetRxErrorCount(void);
Status UART_LoadCANMessage(CAN_MSG_T *pstTxMsg);

void SetLocalOutputs_ThisNode(uint16_t uwOutputs);
void Update_UIRequest_DefaultCommand( void );

/*-----------------------------------------------------------------------------
Returns bus error counter
 -----------------------------------------------------------------------------*/
uint16_t GetDebugBusOfflineCounter_CAN1();
uint16_t GetDebugBusOfflineCounter_CAN2();
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
