/******************************************************************************
 *
 * @file     lcd.h
 * @brief    LCD Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef LCD_H
#define LCD_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

#define LCD_CHAR_NUM_COLUMNS                ( 20U )
#define LCD_CHAR_NUM_ROWS                   ( 4U )

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

void LCD_Char_AdvanceX( uint_fast8_t const ucAdvanceAmount );
void LCD_Char_AdvanceY( uint_fast8_t const ucAdvanceAmount );
void LCD_Char_GoToStartOfNextRow();
void LCD_Char_GotoXY( uint_fast8_t const ucX, uint_fast8_t const ucY );

void LCD_Char_WriteChar( char const cData );

void LCD_Char_WriteString( char const * const ps );

void LCD_Char_WriteStringUpper( char const * const ps );

void LCD_Char_WriteString_Centered( char const * const ps );

void LCD_Char_WriteStringUpper_Centered( char const * const ps );

void LCD_Char_WriteInteger( uint32_t ul );

void LCD_Char_WriteHex_8( uint8_t uc );

void LCD_Char_WriteHex_ExactLength( uint32_t ul, uint_fast8_t ucExactLength );

void LCD_Char_WriteInteger_MinLength( uint32_t ul, uint_fast8_t ucMinLength );

void LCD_Char_WriteInteger_ExactLength( uint32_t ul, uint_fast8_t ucExactLength );

void LCD_Char_Clear( void );

void LCD_Char_ClearRow( void );

void LCD_Char_IdleTask( void );

void LCD_Char_Update( void );

void LCD_Write( uint_fast8_t const bData, uint8_t const ucDataValue );

void LCD_Char_WriteSignedInteger( int32_t iValue );

void LCD_Char_WriteBin_16( uint16_t uw );

#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
