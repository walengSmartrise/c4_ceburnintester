/******************************************************************************
 *
 * @file     ddCommand.h
 * @brief
 * @version  V1.00
 * @date     13, June 2018
 * @author   Keith Soneda
 *
 * @note     Manages commands used for CC/HC assignments to cars.
 *
 ******************************************************************************/
#ifndef _DD_COMMAND_H_
#define _DD_COMMAND_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
typedef struct
{
   uint8_t ucCarID;
   uint8_t ucLanding;
   uint8_t bRear:1;
   en_hc_dir eDir: 1;
   uint8_t bADA: 1;
   uint8_t ucSendCountdown;
} st_ddc_hc_req;

typedef struct
{
   uint8_t ucCarID;
   uint8_t ucLanding;
   uint8_t bRear:1;
   uint8_t bADA:1;
   uint8_t ucSendCountdown;
} st_ddc_cc_req;
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

   For TX side

 -----------------------------------------------------------------------------*/
void DDC_RequestHallCall( st_ddc_hc_req *pstHCReq );
void DDC_RequestCarCall( st_ddc_cc_req *pstCCReq );

uint8_t DDC_LoadDatagram_LatchHC(un_sdata_datagram *punDatagram);
uint8_t DDC_LoadDatagram_LatchCC(un_sdata_datagram *punDatagram);

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
