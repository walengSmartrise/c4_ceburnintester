/******************************************************************************
 *
 * @file     capacityData.c
 * @brief
 * @version  V1.00
 * @date     7, November 2018
 * @author   Keith Soneda
 *
 * @note     Functions for assessing car's current passenger loads,
 *           used for dispatching
 *
 ******************************************************************************/
#ifndef _CAPACITY_DATA_H_
#define _CAPACITY_DATA_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include "sru_b.h"
#include "sys.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define UNDEFINED_CAR_LOAD__LB      (0xFFFF)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
uint8_t CapacityData_CheckIfUnderCapacity( enum en_group_net_nodes eCarID );
uint16_t CapacityData_GetCurrentLoad( enum en_group_net_nodes eCarID );
void CapacityData_UpdateCurrentLoad( enum en_group_net_nodes eCarID );
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
