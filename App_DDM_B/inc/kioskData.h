/******************************************************************************
 *
 * @file     kioskData.h
 * @brief
 * @version  V1.00
 * @date     13, June 2018
 * @author   Keith Soneda
 *
 * @note     Manages calls from DD panels at various floors and assigns them to cars.
 *           These panels are also referred to as kiosks throught the code.
 *           Referenced from: CE Electronics CAN Protocol for Destination Entry Devices Version 1
 *
 ******************************************************************************/
#ifndef _KIOSK_DATA_H_
#define _KIOSK_DATA_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define KIOSK_INACTIVE_TIMER_MS                          (65535U)
#define KIOSK_OFFLINE_TIMER_MS                           (60000U)

#define EXTRACT_DEVICE_ID(x)        (( x >> 26 ) & 0x07 )
#define EXTRACT_MSG_ID(x)           (( x >> 21 ) & 0x1F )
#define EXTRACT_LOCATION_ID(x)      (( x >> 16 ) & 0x1F )
#define EXTRACT_FLOOR_ID(x)         (( x >> 8  ) & 0xFF )
#define EXTRACT_SUBDEVICE_ID(x)     (( x >> 4  ) & 0x0F )

#define MIN_DATAGRAM_RESEND_IN_MS__WATCHDOG              (10000)
#define MIN_DATAGRAM_RESEND_IN_MS__TIMEOUT               (30000)

#define INVALID_CAR_INDEX              (0xFF)
#define MAX_CALLS_PER_CAR              (16)
#define DEFAULT_WEIGHT_PER_CALL_LB     (300)

#define KIOSK_FLOOR_NUM__ALL                             (0xFF)


/* Default DD Panel Timeouts */
#define DEFAULT_TIMEOUT_NORMAL_SCREEN_250MS              (20)
#define DEFAULT_TIMEOUT_ASSITED_SCREEN_250MS             (40)
#define DEFAULT_TIMEOUT_KEY_PRESS_250MS                  (5)
#define DEFAULT_TIMEOUT_KEY_PRESS_ASSISTED_250MS         (5)
#define DEFAULT_TIMEOUT_CODE_ENTRY_250MS                 (20)
#define DEFAULT_TIMEOUT_CODE_ENTRY_ASSISTED_250MS        (20)
#define DEFAULT_TIMEOUT_DISPATCHER_RESPONSE_250MS        (20)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

typedef enum
{
   KIOSK_DEVICE_ID__SUPPORT,
   KIOSK_DEVICE_ID__RES1,
   KIOSK_DEVICE_ID__RES2,
   KIOSK_DEVICE_ID__RES3,
   KIOSK_DEVICE_ID__RES4,
   KIOSK_DEVICE_ID__SECURE,
   KIOSK_DEVICE_ID__SCREEN,
   KIOSK_DEVICE_ID__RES7,

   NUM_KIOSK_DEVICE_ID,
   KIOSK_DEVICE_ID__ALL = 0x7,
} en_kiosk_device_id;

typedef enum
{
   KIOSK_LOCATION_ID__0,
   KIOSK_LOCATION_ID__1,
   KIOSK_LOCATION_ID__2,
   KIOSK_LOCATION_ID__3,
   KIOSK_LOCATION_ID__4,
   KIOSK_LOCATION_ID__5,
   KIOSK_LOCATION_ID__6,
   KIOSK_LOCATION_ID__7,

   MAX_SUPPORTED_KIOSK_LOCATIONS,

   KIOSK_LOCATION_ID__8 = MAX_SUPPORTED_KIOSK_LOCATIONS,
   KIOSK_LOCATION_ID__9,
   KIOSK_LOCATION_ID__10,
   KIOSK_LOCATION_ID__11,
   KIOSK_LOCATION_ID__12,
   KIOSK_LOCATION_ID__13,
   KIOSK_LOCATION_ID__14,
   KIOSK_LOCATION_ID__15,
   KIOSK_LOCATION_ID__16,
   KIOSK_LOCATION_ID__17,
   KIOSK_LOCATION_ID__18,
   KIOSK_LOCATION_ID__19,
   KIOSK_LOCATION_ID__20,
   KIOSK_LOCATION_ID__21,
   KIOSK_LOCATION_ID__22,
   KIOSK_LOCATION_ID__23,
   KIOSK_LOCATION_ID__24,
   KIOSK_LOCATION_ID__25,
   KIOSK_LOCATION_ID__26,
   KIOSK_LOCATION_ID__27,
   KIOSK_LOCATION_ID__28,
   KIOSK_LOCATION_ID__29,
   KIOSK_LOCATION_ID__30,

   NUM_KIOSK_LOCATION_ID,

   KIOSK_LOCATION_ID__ALL = 0x1F,
} en_kiosk_location_id;

typedef enum
{
   KIOSK_MSG_ID__CAR_OP = 0x01,
   KIOSK_MSG_ID__HALL_REQ = 0x02,
   KIOSK_MSG_ID__HALL_RESP = 0x03,
   KIOSK_MSG_ID__TIMEOUTS = 0x04,
   KIOSK_MSG_ID__BUTTON = 0x05,
   KIOSK_MSG_ID__OP_MODE = 0x0F,
} en_kiosk_msg_id;

typedef enum
{
   KIOSK_STATUS_CMD__DED_WD = 0x00,
   KIOSK_STATUS_CMD__DED_REQ = 0x01,
   KIOSK_STATUS_CMD__DIS_WD = 0x02,
   KIOSK_STATUS_CMD__DIS_RES = 0x03,
} en_kiosk_status_cmd;

typedef enum
{
   KIOSK_CALL_RESP__ACCEPTED = 0x00,
   KIOSK_CALL_RESP__NEED_CODE = 0x01,
   KIOSK_CALL_RESP__INVALID_CODE = 0x02,
   KIOSK_CALL_RESP__NEED_SECURITY = 0x04,
   KIOSK_CALL_RESP__INVALID_SECURITY = 0x08,
   KIOSK_CALL_RESP__NO_CARS = 0x10,
   KIOSK_CALL_RESP__INVALID_REQUEST = 0x11,
} en_kiosk_call_response;

typedef enum
{
   KIOSK_CALL_TYPE__NORMAL = 0x00,
   KIOSK_CALL_TYPE__ADA = 0x01,
} en_kiosk_call_type;

typedef enum
{
   KIOSK_OP_MODE__DEST_DIS,
   KIOSK_OP_MODE__CAR_OOS,
   KIOSK_OP_MODE__FIRE,
   // Only above options available on DD panel
   KIOSK_OP_MODE__DIS_OOS,
   KIOSK_OP_MODE__TRADITIONAL,
   KIOSK_OP_MODE__TRADITIONAL_UP,
   KIOSK_OP_MODE__TRADITIONAL_DN,
   KIOSK_OP_MODE__FIRE2,
   KIOSK_OP_MODE__EMS,
   KIOSK_OP_MODE__INDEP_SRV,
   KIOSK_OP_MODE__SWING,
} en_kiosk_op_mode;

//--------------------------------------------------
typedef enum
{
   KIOSK_OFFLINE_STATE__INACTIVE,
   KIOSK_OFFLINE_STATE__OFFLINE,
   KIOSK_OFFLINE_STATE__ONLINE,

   NUM_KIOSK_OFFLINE_STATES
} en_kiosk_offline_states;
//--------------------------------------------------
typedef struct
{
   uint8_t ucLanding_Panel_Plus1;//needed?
   uint8_t ucLanding_Dest_Plus1;
   uint8_t bRear:1;
   en_kiosk_call_type eType;
   en_kiosk_location_id ucLocation;
   uint32_t uiPasscode;
} st_kiosk_request;
typedef struct
{
   en_kiosk_call_response eCallResponse;
   uint8_t ucLanding_Panel_Plus1;
   uint8_t ucLanding_Dest_Plus1;
   uint8_t bRear:1;
   uint8_t ucCarID;
   en_kiosk_call_type eType;
   en_kiosk_location_id ucLocation;
   uint8_t bDirty:2;
} st_kiosk_response;
typedef struct
{
   uint8_t aucLanding_Dest_Plus1[MAX_CALLS_PER_CAR];
   en_kiosk_call_type aeType[MAX_CALLS_PER_CAR];
   uint8_t abRear[BITMAP8_SIZE(MAX_CALLS_PER_CAR)];
   uint8_t ucCallIndex; /* Logs number of car assignments */
   uint16_t uwCountdown_ms; /* Counter temporarily prevents additional call entry after pickup at a panel floor */
} st_kiosk_assignments;
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

   Access functions

 *----------------------------------------------------------------------------*/
st_kiosk_assignments * DDPanel_GetAssignmentStructure( enum en_group_net_nodes eCarID, uint8_t ucLanding );
en_kiosk_offline_states DDPanel_GetOfflineState(uint8_t ucLanding, en_kiosk_location_id ucLocation);
uint8_t DDPanel_GetOpening(uint8_t ucLanding, en_kiosk_location_id ucLocation);

uint8_t LoadDatagram_DispatchPanel( un_sdata_datagram *punDatagram );

void DDPanel_InitOfflineTimers();
void DDPanel_UpdateOfflineTimers( struct st_module *pstThisModule );
void DDPanel_ClearInvalidCalls();
void DDPanel_UpdatePanelCalls( struct st_module *pstThisModule );

uint8_t DDPanel_ReceiveMessage( CAN_MSG_T *msg );
void DDPanel_UnloadWatchdogMessage( CAN_MSG_T *msg );
void DDPanel_UnloadCallRequest( CAN_MSG_T *msg );

void DDPanel_ClearAssignments( enum en_group_net_nodes eCarID, uint8_t ucLanding );

void DDPanel_UpdateOfflineTimers( struct st_module *pstThisModule );
uint8_t DDPanel_GetLastTimeoutFloor();
uint8_t DDPanel_GetActiveResponseFloor();
uint8_t DDPanel_GetLastCallAccepted(void);
uint8_t DDPanel_GetLastLocation(void);
en_kiosk_op_mode DDPanel_GetOperationMode();
uint8_t DDPanel_LoadDatagram_Timeouts( un_sdata_datagram *punDatagram );
uint8_t DDPanel_LoadDatagram_CallResponse( un_sdata_datagram *punDatagram );
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
