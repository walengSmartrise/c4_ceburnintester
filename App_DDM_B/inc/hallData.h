/******************************************************************************
 *
 * @file     mod_run_log.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef _HALL_DATA_H_
#define _HALL_DATA_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
typedef struct
{
   /* Raw latched hall calls */
   uint32_t aauiRawLatched_Up[MAX_NUM_RISER_BOARDS][MAX_NUM_FLOORS]; //latched up will be used for the hall call lamp
   uint32_t aauiRawLatched_Down[MAX_NUM_RISER_BOARDS][MAX_NUM_FLOORS]; //latched down for the down hall call lamp

} st_hallcall_data;

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void UnloadDatagram_RiserCarRequest( uint8_t ucRiserIndex, un_sdata_datagram *punDatagram );
void UnloadDatagram_RiserLatchedHallCalls( uint8_t ucRiserIndex, en_hc_dir eDir, un_sdata_datagram *punDatagram );
void UnloadDatagram_RiserHallboardStatus( uint8_t ucRiserIndex, un_sdata_datagram *punDatagram );
void LoadDatagram_HallCalls( un_sdata_datagram *punDatagram );

uint32_t GetRawLatchedHallCalls( uint8_t ucLanding, en_hc_dir eDir );
void ClrRawLatchedHallCalls( uint8_t ucLanding, en_hc_dir eDir, uint32_t uiClrMask );
void SetRawLatchedHallCalls( uint8_t ucLanding, en_hc_dir eDir, uint32_t uiSetMask );

uint8_t GetLatchedHallCallUp_ByLanding( uint8_t ucLanding, enum en_doors eDoor );
uint8_t GetLatchedHallCallDown_ByLanding( uint8_t ucLanding, enum en_doors eDoor );

uint8_t GetHallBoardStatus_IO(void);
en_hallboard_error GetHallBoardStatus_Error(void);
en_hb_com_state GetHallBoardStatus_Comm(void);
uint8_t GetHallBoardStatus_Source(void);

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
