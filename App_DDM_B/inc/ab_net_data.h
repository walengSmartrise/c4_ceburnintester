/******************************************************************************
 *
 * @file     ab_net_data.h
 * @brief    Access functions for datagrams on the DDM A to B processor network
 * @version  V1.00
 * @date     31, Oct 2018
 *
 * @note
 *
 ******************************************************************************/

#ifndef AB_NET_DATA_H
#define AB_NET_DATA_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
#include "alarm_app.h"
#include "shared_data.h"
#include "datagrams.h"

#include "motion.h"
#include "position.h"
#include "operation.h"
#include "DDM_Alarms.h"
#include "ddm_param_1Bit.h"
#include "ddm_param_8Bit.h"
#include "ddm_param_16Bit.h"
#include "ddm_param_24Bit.h"
#include "ddm_param_32Bit.h"
#include "ddm_network.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define AB_NET_OFFLINE_TIMEOUT_1MS      (1000)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/


 /*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

void ABNet_SetLocalNodeID( en_ddm_nodes eNode );
en_ddm_nodes ABNet_GetLocalNodeID(void);
void ABNet_SetDestinationNodeID( en_ddm_nodes eNode );
en_ddm_nodes ABNet_GetDestinationNodeID(void);
uint8_t ABNet_GetMaxNumDatagrams( en_ddm_nodes eNode );

uint16_t ABNet_GetDatagramLifetime( uint8_t ucDatagramIndex, en_ddm_nodes eNode );

uint32_t ABNet_GetMessageID( uint8_t ucDatagramIndex, en_ddm_nodes eSrcNode, en_ddm_nodes eDestNode );
en_ddm_nodes ABNet_GetSrcNodeID( uint32_t uiMessageID );
en_ddm_nodes ABNet_GetDestNodeID( uint32_t uiMessageID );
uint8_t ABNet_GetDatagramIndex( uint32_t uiMessageID );

uint8_t ABNet_GetDirtyBit( uint8_t ucDatagramIndex, en_ddm_nodes eNode );
void ABNet_ClrDirtyBit( uint8_t ucDatagramIndex, en_ddm_nodes eNode );
void ABNet_ReceiveDatagram( un_sdata_datagram *punDatagram, uint8_t ucDatagramIndex, en_ddm_nodes eNode );

uint8_t ABNet_GetDatagram_U8( uint8_t ucDataIndex, uint8_t ucDatagramIndex, en_ddm_nodes eNode );
uint16_t ABNet_GetDatagram_U16( uint8_t ucDataIndex, uint8_t ucDatagramIndex, en_ddm_nodes eNode );
uint32_t ABNet_GetDatagram_U32( uint8_t ucDataIndex, uint8_t ucDatagramIndex, en_ddm_nodes eNode );
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
