/******************************************************************************
 * @author   Keith Soneda
 * @version  V1.00
 * @date     6, Sept 2018
 *
 * @note    Access functions for master expansion board's data
 *
 ******************************************************************************/

#ifndef _EXP_DATA_H_
#define _EXP_DATA_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include "sys.h"
#include "riser.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define MASTER_IO_OFFLINE_LIMIT_MS        (30000)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

 /*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void ExpData_UpdateMasterOfflineTimers( uint16_t uwRunPeriod_1ms );
uint8_t ExpData_GetOnlineFlag( uint8_t ucMasterIndex, uint8_t ucBoardIndex );
uint8_t ExpData_GetInputs( uint8_t ucMasterIndex, uint8_t ucBoardIndex );
uint8_t ExpData_GetError( uint8_t ucMasterIndex, uint8_t ucBoardIndex );
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
