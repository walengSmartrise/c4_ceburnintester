/******************************************************************************
 *
 * @file     totalCostData.h
 * @brief
 * @version  V1.00
 * @date     7, November 2018
 * @author   Keith Soneda
 *
 * @note     Functions for assessing a calls total cost when assigned to a given car
 *
 ******************************************************************************/
#ifndef _TOTAL_COST_DATA_H
#define _TOTAL_COST_DATA_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define INVALID_TOTAL_ASSIGNMENT_COST_SEC          (0xFFFFFFFF)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/* Definition of different DDM dispatching
 * options set by DDM_PARAM8__DispatchingType */
typedef enum
{
   DDM_DISPATCH_TYPE__BASIC,
   DDM_DISPATCH_TYPE__TOTAL_COST,
   DDM_DISPATCH_TYPE__TIME_TO_DEST,
   DDM_DISPATCH_TYPE__TIME_TO_PICKUP,

   NUM_DDM_DISPATCH_TYPE
}en_ddm_dispatch_type;
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

void TotalCost_ResetAssignmentCost( enum en_group_net_nodes eCarID );
uint32_t TotalCost_GetLastAssignment_T2P_Travel( enum en_group_net_nodes eCarID );
uint32_t TotalCost_GetLastAssignment_T2P_CC( enum en_group_net_nodes eCarID );
uint32_t TotalCost_GetLastAssignment_T2P_HC( enum en_group_net_nodes eCarID );
uint32_t TotalCost_GetLastAssignment_T2D_Travel( enum en_group_net_nodes eCarID );
uint32_t TotalCost_GetLastAssignment_T2D_CC( enum en_group_net_nodes eCarID );
uint32_t TotalCost_GetLastAssignment_T2D_HC( enum en_group_net_nodes eCarID );
uint32_t TotalCost_GetLastAssignment_TA_CC( enum en_group_net_nodes eCarID );
uint32_t TotalCost_GetLastAssignment_TA_HC( enum en_group_net_nodes eCarID );
uint32_t TotalCost_GetLastAssignment_TotalCost( enum en_group_net_nodes eCarID );

uint8_t TotalCost_GetLastAssignment_CurrentLanding( enum en_group_net_nodes eCarID );
uint8_t TotalCost_GetLastAssignment_DestinationLanding( enum en_group_net_nodes eCarID );
uint8_t TotalCost_GetLastAssignment_PanelLanding( enum en_group_net_nodes eCarID );

uint32_t TotalCost_GetAssignmentCost_sec( enum en_group_net_nodes eCarID, uint8_t ucDestLanding, uint8_t ucPanelLanding );

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
