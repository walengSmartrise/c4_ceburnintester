/******************************************************************************
 *
 * @file     XRegData.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     3, May 2018
 *
 * @note
 *
 ******************************************************************************/

#ifndef _XREG_DATA_H_
#define _XREG_DATA_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/

#define XREG_CAR_OFFLINE_INACTIVE_MS      (0xFFFF)
#define XREG_CAR_OFFLINE_LIMIT_MS         (60000)

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/* As defined in C4_GroupDatagrams.xlsx 5/3/2018 */
typedef enum
{
   XREG_CMD__CAR_STATUS,
   XREG_CMD__FIRE_EP,
   XREG_CMD__LATCH_HC,
   XREG_CMD__TIME,

   NUM_XREG_CMD
} en_xreg_cmd;

typedef enum
{
   XREG_STAT__CAR_STATUS,
   XREG_STAT__FIRE_EP,
   XREG_STAT__LATCHED_HC,
   XREG_STAT__CLEAR_HC,
   XREG_STAT__FLOOR_DOOR_MAP,
   XREG_STAT__HALL_CALL_MASK,

   NUM_XREG_STAT
} en_xreg_stat;


typedef struct
{
   enum en_classop eClassOp;
   enum en_mode_auto eAutoMode;
   uint8_t ucLandingIndex;
   enum direction_enum cMotionDir;
   enum direction_enum cArrivalDir;
   uint8_t bInGroup:1;

   uint8_t aucBF_LandingMap[BITMAP8_SIZE(MAX_NUM_FLOORS)];
   uint8_t aucBF_FrontMap[BITMAP8_SIZE(MAX_NUM_FLOORS)];
   uint8_t aucBF_RearMap[BITMAP8_SIZE(MAX_NUM_FLOORS)];

   /* Local variables */
   uint8_t ucFirstLanding;
   uint8_t ucLastLanding;
   uint8_t bCarOnline:1;
   uint8_t bRemovedFromGroup:1; // Removes car from group after failure to take destination command
   en_hc_dir enPriority; // Have XREG system maintain this variable
   uint32_t uiHallMask_F; // Have XREG system maintain this variable
   uint32_t uiHallMask_R; // Have XREG system maintain this variable
   uint16_t uwOfflineCounter_ms;
} st_xreg_car_data;
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void XRegData_Init(uint8_t ucCarIndex);
void XRegData_UpdateCarOnlineFlag(uint8_t ucCarIndex, uint16_t uwModRunPeriod_ms);

/*-----------------------------------------------------------------------------
   Access variables
   ucCarIndex < 8
 -----------------------------------------------------------------------------*/
uint8_t XRegData_GetCarActive(uint8_t ucCarIndex);
enum en_classop XRegData_GetClassOp(uint8_t ucCarIndex);
enum en_mode_auto XRegData_GetAutoMode(uint8_t ucCarIndex);
uint8_t XRegData_GetCurrentLanding(uint8_t ucCarIndex);
enum direction_enum XRegData_GetMotionDirection(uint8_t ucCarIndex);
enum direction_enum XRegData_GetArrivalDirection(uint8_t ucCarIndex);
uint8_t XRegData_GetOpening_Front(uint8_t ucCarIndex, uint8_t ucLanding);
uint8_t XRegData_GetOpening_Rear(uint8_t ucCarIndex, uint8_t ucLanding);
uint8_t XRegData_GetFirstLanding(uint8_t ucCarIndex);
uint8_t XRegData_GetLastLanding(uint8_t ucCarIndex);
en_hc_dir XRegData_GetPriority(uint8_t ucCarIndex);
uint32_t XRegData_GetHallMask_F(uint8_t ucCarIndex);
uint32_t XRegData_GetHallMask_R(uint8_t ucCarIndex);
uint8_t XRegData_GetCarOnlineFlag(uint8_t ucCarIndex);

uint16_t XRegData_GetGroupHallCallMask();

void UnloadDatagram_XRegStatus( un_sdata_datagram *punDatagram );

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
