/******************************************************************************
 *
 * @file     XRegDestination.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     2, May 2018
 *
 * @note
 *
 ******************************************************************************/

#ifndef _XREG_DESTINATION_H_
#define _XREG_DESTINATION_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
/*  Number of times to resend XREG destinations */
#define XREG_DESTINATION_RESEND_COUNT        (2)

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

typedef struct
{
      uint8_t ucLanding;
      enum en_doors eDoor; //Destination opening
      enum direction_enum eCallDir; // Call direction
      uint32_t uiMask;
      uint8_t ucSendCountdown;//When nonzero the assignment should be sent
      uint8_t bCallAccepted;
      // Zeroed, when assignment first made, when greater than limit, the car will temporarily be removed so the call can be reassigned
      uint32_t uiTimeout_ms;
} st_XRegDestination;


/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

void Init_XRegDestinationStructure();

void ClrXRegDestination_ByCar(enum en_group_net_nodes eCarID);

st_XRegDestination * GetXRegDestinationStructure( uint8_t ucCarIndex );
uint8_t XRegDestination_GetLanding( uint8_t ucCarIndex );
enum en_doors XRegDestination_GetDoor( uint8_t ucCarIndex );
enum direction_enum XRegDestination_GetDirection( uint8_t ucCarIndex );
uint32_t XRegDestination_GetMask( uint8_t ucCarIndex );
uint8_t XRegDestination_GetCallAccepted( uint8_t ucCarIndex );

void XRegDestination_ClrSendCountdown( uint8_t ucCarIndex );
void XRegDestination_SetCallAccepted( uint8_t ucCarIndex );

void UpdateXRegCarTimeout( uint16_t uwRunPeriod_ms );

uint32_t XRegDestination_GetTimeout( uint8_t ucCarIndex );
void XRegDestination_IncrementTimeout( uint8_t ucCarIndex, uint16_t uwRunPeriod_ms );
void XRegDestination_ClrTimeout( uint8_t ucCarIndex );
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
