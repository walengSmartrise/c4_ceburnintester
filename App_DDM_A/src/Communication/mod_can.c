/******************************************************************************
 *
 * @file
 * @brief
 * @version  V1.00
 * @date
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "sru_a.h"
#include "sys.h"

#include "ring_buffer.h"
#include <stdlib.h>
#include <string.h>
#include "GlobalData.h"
#include "position.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_CAN =
{
   .pfnInit = Init,
   .pfnRun = Run,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define LIMIT_UNLOAD_CAN_CYCLES       (16)
/* Since there won't always be a device communicating on CAN2,
 * initialize the offline timer with a starter value and suppress
 * communication errors for the bus if not communication was ever detected */
#define OFFLINE_TIMER_COM_NOT_DETECTED          (0xFFFF)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *-----------------------------------------------------------------------------*/
static uint16_t uwOfflineTimeout_CAN1_5ms = OFFLINE_TIMER_COM_NOT_DETECTED;
static uint16_t uwOfflineTimeout_CAN2_5ms = OFFLINE_TIMER_COM_NOT_DETECTED;
static uint16_t uwBusErrorCounter_CAN1;
static uint16_t uwBusErrorCounter_CAN2;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Returns bus error counter
 -----------------------------------------------------------------------------*/
uint16_t GetDebugBusOfflineCounter_CAN1()
{
   return uwBusErrorCounter_CAN1;
}
uint16_t GetDebugBusOfflineCounter_CAN2()
{
   return uwBusErrorCounter_CAN2;
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = 5;

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Receive_Packet(CAN_MSG_T *pstRxMsg)
{
   un_sdata_datagram unDatagram;
   uint8_t ucNetworkDatagramID = ExtractFromCAN_DatagramID(pstRxMsg->ID);

   memcpy(&unDatagram, &pstRxMsg->Data, 8);
   InsertSharedDatagram_ByDatagramID( &unDatagram, ucNetworkDatagramID);
}

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UnloadCAN( void )
{
    static CAN_MSG_T tmp;
    // Read data from ring buffer till it is empty
    // Devices on CAN1: Car net
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN1_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
          uwOfflineTimeout_CAN1_5ms = 0;
          // Unload or forward packet
          Receive_Packet(&tmp);
       }
    }

    // Read data from ring buffer till it is empty
    // This information is sent directly to traction in the ISR.
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN2_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
          uwOfflineTimeout_CAN2_5ms = 0;
          switch ( tmp.ID )
          {
              default: break;
          }
       }
    }
}

/*-----------------------------------------------------------------------------
   Check for CAN Bus offline and reset if offline
 -----------------------------------------------------------------------------*/
static void CheckFor_CANBusOffline()
{
   if( Chip_CAN_GetGlobalStatus( LPC_CAN1 ) & CAN_GSR_BS ) // Bus offline if 1
   {
         Chip_CAN_SetMode( LPC_CAN1, CAN_RESET_MODE, ENABLE );
         __NOP();
         __NOP();
         __NOP();
         Chip_CAN_SetMode( LPC_CAN1, CAN_RESET_MODE, DISABLE );
         SetAlarm(DDM_ALM__CAN_RESET_A1);
   }

   if( Chip_CAN_GetGlobalStatus( LPC_CAN2 ) & CAN_GSR_BS ) // Bus offline if 1
   {
         Chip_CAN_SetMode( LPC_CAN2, CAN_RESET_MODE, ENABLE );
         __NOP();
         __NOP();
         __NOP();
         Chip_CAN_SetMode( LPC_CAN2, CAN_RESET_MODE, DISABLE );
         SetAlarm(DDM_ALM__CAN_RESET_A2);
   }
}

/*-----------------------------------------------------------------------------
   Updates the CAN bus error counters viewed via View Debug Data menus
 -----------------------------------------------------------------------------*/
static void UpdateDebugBusErrorCounter()
{
   static uint8_t ucLastCountRx_CAN1;
   static uint8_t ucLastCountTx_CAN1;
   static uint8_t ucLastCountRx_CAN2;
   static uint8_t ucLastCountTx_CAN2;
   uint32_t uiStatus = Chip_CAN_GetGlobalStatus( LPC_CAN1 );
   uint8_t ucCountRx = (uiStatus >> 16) & 0xFF;
   uint8_t ucCountTx = (uiStatus >> 24) & 0xFF;
   if( ( ucCountRx > ucLastCountRx_CAN1 )
    || ( ucCountTx > ucLastCountTx_CAN1 ))
   {
       uwBusErrorCounter_CAN1++;
   }
   ucLastCountRx_CAN1 = ucCountRx;
   ucLastCountTx_CAN1 = ucCountTx;

   uiStatus = Chip_CAN_GetGlobalStatus( LPC_CAN2 );
   ucCountRx = (uiStatus >> 16) & 0xFF;
   ucCountTx = (uiStatus >> 24) & 0xFF;
   if( ( ucCountRx > ucLastCountRx_CAN2 )
    || ( ucCountTx > ucLastCountTx_CAN2 ))
   {
       uwBusErrorCounter_CAN2++;
   }
   ucLastCountRx_CAN2 = ucCountRx;
   ucLastCountTx_CAN2 = ucCountTx;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define CAN1_OFFLINE_TIMEOUT__5MS      (200)
#define CAN2_OFFLINE_TIMEOUT__5MS      (200)
static void CheckFor_CommunicationLoss(void)
{
   if( uwOfflineTimeout_CAN1_5ms > CAN1_OFFLINE_TIMEOUT__5MS )
   {
      if( uwOfflineTimeout_CAN1_5ms != OFFLINE_TIMER_COM_NOT_DETECTED )
      {
//         SetAlarm( DDM_ALM__OFFLINE_EXP );
      }
   }
   else
   {
      uwOfflineTimeout_CAN1_5ms++;
   }

   if( uwOfflineTimeout_CAN2_5ms > CAN2_OFFLINE_TIMEOUT__5MS )
   {
      if( uwOfflineTimeout_CAN2_5ms != OFFLINE_TIMER_COM_NOT_DETECTED )
      {
//         SetAlarm( DDM_ALM__OFFLINE_EXP );
      }
   }
   else
   {
      uwOfflineTimeout_CAN2_5ms++;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   CheckFor_CommunicationLoss();
   UnloadCAN();
   CheckFor_CANBusOffline();
   UpdateDebugBusErrorCounter();
   CAN1_FillHardwareBuffer();
   return 0;
}
