/******************************************************************************
 *
 * @file     mod_uart.c
 * @brief
 * @version  V1.00
 * @date     26, March 2016
 *
 * @note    Unloads bytes from uart ring buffer and forms packets.
 *          Checks packet and forwards to the next destination node and/or
 *          loads the packet into the global shared data structure for system access.
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "sru.h"
#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
#include <string.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_UART =
{
   .pfnInit = Init,
   .pfnRun = Run,
};


static uint8_t ucCommTimeout_5ms;

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static CAN_MSG_T stRxMsg;
static uint8_t gaU3_StatusBytes[ NUM_UART_STATUS_BYTES ];

static uint8_t ucCommTimeout_5ms;

static uint8_t ucCommTimeout_5ms;
static uint32_t uiOfflineDelay_ms;
static uint32_t uiOfflineCounter_ms;

static uint16_t uwRxErrorCounter;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Returns the number of bad bytes/packets detected
-----------------------------------------------------------------------------*/
uint16_t UART_GetRxErrorCount(void)
{
   return uwRxErrorCounter;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
Status UART_LoadCANMessage(CAN_MSG_T *pstTxMsg)
{
   Status bReturn = ERROR;
   uint8_t *puc;
   uint8_t *puc2;
   uint8_t ucByteToSend;
   uint8_t aucXmtBuffer1[ 20 ];
   uint8_t aucXmtBuffer2[ 35 ];

   //------------------------------------------------
   int iIndex1 = 0;
   uint8_t ucChecksum = 0;
   //------------------------------------------------
   // Load ID
   puc = (uint8_t *) &pstTxMsg->ID;
   for(int i = 0; i < 4; i++)
   {
      ucByteToSend = *puc++;
      ucChecksum += ucByteToSend;
      aucXmtBuffer1[ iIndex1++ ] = ucByteToSend;
   }
   //------------------------------------------------
   // Load data
   puc2 = (uint8_t *) &pstTxMsg->Data[0];

   for ( int i = 0; i < 8; i++ )
   {
      ucByteToSend = *puc2++;
      ucChecksum += ucByteToSend;
      aucXmtBuffer1[ iIndex1++ ] = ucByteToSend;
   }
   //------------------------------------------------
   aucXmtBuffer1[ iIndex1++ ] = ucChecksum;


   //------------------------------------------------
   // Now load data buffer into UART frame for send
   int iIndex2 = 0;

   aucXmtBuffer2[ iIndex2++ ] = 0x55;   // STX

   //------------------------------------------------
   for ( int i = 0; i < iIndex1; ++i )
   {
      uint8_t ucByteToSend = aucXmtBuffer1[ i ];

      if (    (ucByteToSend == 0x55)   // STX
           || (ucByteToSend == 0x56)   // ETX
           || (ucByteToSend == 0x1B)   // ESC
         )
      {
         aucXmtBuffer2[ iIndex2++ ] = 0x1B;  // ESC

         ucByteToSend = ~ucByteToSend;
      }

      aucXmtBuffer2[ iIndex2++ ] = ucByteToSend;
   }

   //------------------------------------------------
   aucXmtBuffer2[ iIndex2++ ] = 0x56;   // ETX

   //------------------------------------------------
   if(UART_AB_SendRB(&(aucXmtBuffer2[ 0 ]), iIndex2))
   {
      ++gaU3_StatusBytes[ UART_STATUS__PACKET_TRANSMITTED ];
      bReturn = SUCCESS;
   }

   return bReturn;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Receive_Packet(CAN_MSG_T *pstRxMsg)
{
   en_ddm_nodes eSrcNodeID = ABNet_GetSrcNodeID(pstRxMsg->ID);
   en_ddm_nodes eDestNodeID = ABNet_GetDestNodeID(pstRxMsg->ID);
   uint8_t ucDatagramIndex = ABNet_GetDatagramIndex(pstRxMsg->ID);

   if( ( eDestNodeID == ABNet_GetLocalNodeID() )
    && ( ucDatagramIndex < ABNet_GetMaxNumDatagrams(eSrcNodeID) ) )
   {
      ABNet_ReceiveDatagram( (un_sdata_datagram *)&pstRxMsg->Data[0], ucDatagramIndex, eSrcNodeID);
   }
   uiOfflineCounter_ms = 0;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Receive( void )
{
   uint8_t ucByteReceived;
   static uint8_t ucRcvBufferIndex;
   static uint8_t ucChecksum_Calcd;
   static uint8_t bNextByteIsEscaped;
   static uint8_t bPacketReceived;
   static uint8_t aucRcvBuffer[ 32 ];
   uint8_t ucByteLimit = 196;
   while(UART_AB_ReadByte(&ucByteReceived) && ucByteLimit)
   {
      ucByteLimit--;
      if ( ucRcvBufferIndex >= sizeof( aucRcvBuffer ) )
      {
         ++gaU3_StatusBytes[ UART_STATUS__RCV_OVERFLOW2 ];
         uwRxErrorCounter++;
         ucRcvBufferIndex = 0;
      }
      else
      {
         if ( ucByteReceived == 0x1B )  // ESC
         {
            bNextByteIsEscaped = 1;
         }
         else
         {
            if ( ucByteReceived == 0x55 )  // STX
            {
               ucRcvBufferIndex = 0;

               ucChecksum_Calcd = 0;  // initialize checksum
            }
            else if ( ucByteReceived == 0x56 )  // ETX
            {
               if ( ucRcvBufferIndex )
               {
                  uint8_t ucChecksum_Received = aucRcvBuffer[ ucRcvBufferIndex - 1 ];

                  // The calculated checksum will include the previous byte
                  // received which was the checksum itself. Subtract it before
                  // validating the checksum.
                  ucChecksum_Calcd -= ucChecksum_Received;

                  if ( ucChecksum_Calcd == ucChecksum_Received )
                  {
                     bPacketReceived = 1;
                     ucCommTimeout_5ms = 0;
                  }
                  else
                  {
                     ++gaU3_StatusBytes[ UART_STATUS__RCV_CHECKSUM ];
                     uwRxErrorCounter++;
                  }
               }
            }
            else
            {
               if ( bNextByteIsEscaped )
               {
                  ucByteReceived = ~ucByteReceived;
               }

               ucChecksum_Calcd += ucByteReceived;
            }

            aucRcvBuffer[ ucRcvBufferIndex++ ] = ucByteReceived;

            bNextByteIsEscaped = 0;
         }
      }

      if ( bPacketReceived )
      {
         bPacketReceived = 0;
         ucRcvBufferIndex = 0;
         ++gaU3_StatusBytes[ UART_STATUS__PACKET_RECEIVED ];
         stRxMsg.ID = aucRcvBuffer[1];
         stRxMsg.ID |= aucRcvBuffer[2] << 8;
         stRxMsg.ID |= aucRcvBuffer[3] << 16;
         stRxMsg.ID |= aucRcvBuffer[4] << 24;
         stRxMsg.DLC = 8;
         stRxMsg.Type = 0;
         memcpy((un_sdata_datagram *)&stRxMsg.Data[0], (un_sdata_datagram *)&aucRcvBuffer[5], sizeof(un_sdata_datagram));
         Receive_Packet(&stRxMsg);
      }
   }
}

/*-----------------------------------------------------------------------------
   Check for B processor of board offline
 -----------------------------------------------------------------------------*/
static void CheckFor_BoardOffline()
{
   if(uiOfflineCounter_ms < uiOfflineDelay_ms)
   {
      uiOfflineCounter_ms += MOD_RUN_PERIOD_UART_1MS;
   }
   else
   {
      if( ABNet_GetLocalNodeID() == DDM_NODE__A )
      {
         SetAlarm(DDM_ALM__OFFLINE_B);
      }
      else if( ABNet_GetLocalNodeID() == DDM_NODE__B )
      {
         SetAlarm(DDM_ALM__OFFLINE_A);
      }
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{

    //------------------------------------------------
    pstThisModule->uwInitialDelay_1ms = 5;
    pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_UART_1MS;

    uiOfflineDelay_ms = AB_NET_OFFLINE_TIMEOUT_1MS;

    uiOfflineCounter_ms = 0;
    return 0;
}

static uint32_t Run( struct st_module *pstThisModule )
{
    Receive();
    CheckFor_BoardOffline();

    return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
