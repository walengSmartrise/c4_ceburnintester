/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief
 * @version  V1.00
 * @date     26, March 2016
 *
 * @note    Handles datagrams between EXP boards wired to security inputs, and the DDMA processor
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_module gstMod_SData_Exp =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

struct st_sdata_control gstSData_ExpData;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define DG_TX_CYCLE_LIMIT  (3)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static Sys_Nodes eSysNodeID;// network id
static uint8_t ucNumberOfDatagrams;// number of datagrams generated locally
static CAN_MSG_T gstCAN_MSG_Tx;

static const Datagram_ID aucDatagramMap_DDMA_To_EXP[NUM_DDMA_TO_EXP_DATAGRAMS] =
{
      DATAGRAM_ID_55,//DG_DDMA_TO_EXP__ExpOutputs1, See DG_MRA__LocalOutputs2
      DATAGRAM_ID_56,//DG_DDMA_TO_EXP__ExpOutputs2, See DG_MRA__LocalOutputs3
      DATAGRAM_ID_57,//DG_DDMA_TO_EXP__ExpOutputs3, See DG_MRA__LocalOutputs4
      DATAGRAM_ID_58,//DG_DDMA_TO_EXP__ExpOutputs4, See DG_MRA__LocalOutputs5
      DATAGRAM_ID_59,//DG_DDMA_TO_EXP__ExpOutputs5, See DG_MRA__LocalOutputs6
};
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------
   Creates datagrams produced by this node
 -----------------------------------------------------------------------------*/
void SharedData_EXP_Init(void)
{
   int iError = 1;
   eSysNodeID = GetSystemNodeID();
   switch(eSysNodeID)
   {
      case SYS_NODE__DDM_A:
        ucNumberOfDatagrams = NUM_DDMA_TO_EXP_DATAGRAMS;
        break;
      default:
         break;

   }
   if(ucNumberOfDatagrams)
   {
      iError = SDATA_CreateDatagrams( &gstSData_ExpData,
                                      ucNumberOfDatagrams
                                     );
   }

   if ( iError )
   {
      while ( 1 )
      {
         ;  // TODO: unable to allocate shared data
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData( void )
{
   un_sdata_datagram unDatagram;
   memset(&unDatagram, 0, sizeof(un_sdata_datagram));
   /* Currently outputs not needed... */
   SDATA_WriteDatagram(&gstSData_ExpData, DG_DDMA_TO_EXP__ExpOutputs1, &unDatagram);
   SDATA_WriteDatagram(&gstSData_ExpData, DG_DDMA_TO_EXP__ExpOutputs2, &unDatagram);
   SDATA_WriteDatagram(&gstSData_ExpData, DG_DDMA_TO_EXP__ExpOutputs3, &unDatagram);
   SDATA_WriteDatagram(&gstSData_ExpData, DG_DDMA_TO_EXP__ExpOutputs4, &unDatagram);
   SDATA_WriteDatagram(&gstSData_ExpData, DG_DDMA_TO_EXP__ExpOutputs5, &unDatagram);
}

/*-----------------------------------------------------------------------------
 Loads local datagrams for transmit over Car Net and/or AB Net
 -----------------------------------------------------------------------------*/
static void Transmit( void )
{
   un_sdata_datagram unDatagram;
   int iError = 0;
   uint8_t ucLocalDatagramID;
   uint16_t uwDatagramToSend_Plus1;
   uint8_t ucTXCycle;
   for( ucTXCycle = 0; ucTXCycle < DG_TX_CYCLE_LIMIT; ucTXCycle++)
   {
      uwDatagramToSend_Plus1 = SDATA_GetNextDatagramToSendIndex_Plus1( &gstSData_ExpData );
      if(uwDatagramToSend_Plus1)
      {
         ucLocalDatagramID = uwDatagramToSend_Plus1 - 1;

         if ( ucLocalDatagramID < ucNumberOfDatagrams )
         {
            SDATA_ReadDatagram( &gstSData_ExpData,
                                 ucLocalDatagramID,
                                &unDatagram
                              );

            memcpy( gstCAN_MSG_Tx.Data, unDatagram.auc8, sizeof( unDatagram.auc8 ) );
            gstCAN_MSG_Tx.DLC = 8;
            gstCAN_MSG_Tx.Type = 0;
            gstCAN_MSG_Tx.ID = CAN_EXTEND_ID_USAGE;
            gstCAN_MSG_Tx.ID |= aucDatagramMap_DDMA_To_EXP[ucLocalDatagramID] << 21;
            gstCAN_MSG_Tx.ID |= (SDATA_NET__CAR & 0x07) << 18;
            gstCAN_MSG_Tx.ID |= (SYS_NODE__MRA & 0x0F) << 14;
            gstCAN_MSG_Tx.ID |= NODE_ALL_CORE;

            iError = !CAN1_LoadToRB(&gstCAN_MSG_Tx);

            if ( iError )
            {
               SDATA_DirtyBit_Set( &gstSData_ExpData, ucLocalDatagramID );

               if ( gstSData_ExpData.uwLastSentIndex )
               {
                  --gstSData_ExpData.uwLastSentIndex;
               }
               else
               {
                  gstSData_ExpData.uwLastSentIndex = gstSData_ExpData.uiNumDatagrams - 1;
               }
               ucTXCycle = DG_TX_CYCLE_LIMIT;
               break;
            }
         }
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadData( void )
{
   if( ABNet_GetLocalNodeID() == DDM_NODE__A )
   {
      un_sdata_datagram unDatagram;
      /* Unload EXP board packets and automatically send it to the B processor */
      for(uint8_t i = 0; i < NUM_EXP_MASTER_BOARDS; i++)
      {
         /* Inputs */
         if( GetDatagramDirtyBit(DATAGRAM_ID_29+i) )
         {
            ClrDatagramDirtyBit(DATAGRAM_ID_29+i);
            GetSharedDatagram_ByDatagramID(&unDatagram, DATAGRAM_ID_29+i);
            SDATA_WriteDatagram( &gstSData_LocalData,
                                 DG_DDMA__ExpInputs1+i,
                                 &unDatagram );
            SDATA_DirtyBit_Set(&gstSData_LocalData, DG_DDMA__ExpInputs1+i);
         }
         /* Status */
         if( GetDatagramDirtyBit(DATAGRAM_ID_198+i) )
         {
            ClrDatagramDirtyBit(DATAGRAM_ID_198+i);
            GetSharedDatagram_ByDatagramID(&unDatagram, DATAGRAM_ID_198+i);
            SDATA_WriteDatagram( &gstSData_LocalData,
                                 DG_DDMA__ExpStatus1+i,
                                 &unDatagram );
            SDATA_DirtyBit_Set(&gstSData_LocalData, DG_DDMA__ExpStatus1+i);
         }
      }
   }
}

/*-----------------------------------------------------------------------------
   Set datagram resend rates
 -----------------------------------------------------------------------------*/
static void Initialize_DatagramSendRates()
{
   uint8_t ucOfflineDelay_5ms = AB_NET_OFFLINE_TIMEOUT_1MS/5;

   uint16_t uwHeartbeatInterval_1ms = (ucOfflineDelay_5ms*5)/3;
   gstSData_ExpData.uwHeartbeatInterval_1ms = uwHeartbeatInterval_1ms;

   for( uint8_t i = 0; i < ucNumberOfDatagrams; i++ )
   {
      uint16_t uwLifetime_ms = UNDEF_LIFETIME_MS;
      gstSData_ExpData.paiResendRate_1ms[i] = uwLifetime_ms/2;
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   //------------------------------------------------
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_AB_NET_1MS;

   Initialize_DatagramSendRates();

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
    LoadData();
    Transmit();

    // Polls ring buffer for data and loads into CAN hardware buffer
    CAN1_FillHardwareBuffer();

    UnloadData();

    return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
