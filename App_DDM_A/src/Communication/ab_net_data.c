/******************************************************************************
 *
 * @file     ab_net_data.c
 * @brief    Access functions for datagrams on the DDM A to B processor network
 * @version  V1.00
 * @date     31, Oct 2018
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static en_ddm_nodes eLocalNodeID = DDM_NODE__UNK;
static en_ddm_nodes eDestNodeID = DDM_NODE__UNK;

/* Datagrams received */
static un_sdata_datagram aunRxDatagrams_DDMA[NUM_DDMA_DATAGRAMS];
static uint8_t abRxDirtyBits_DDMA[BITMAP8_SIZE(NUM_DDMA_DATAGRAMS)];

static un_sdata_datagram aunRxDatagrams_DDMB[NUM_DDMB_DATAGRAMS];
static uint8_t abRxDirtyBits_DDMB[BITMAP8_SIZE(NUM_DDMB_DATAGRAMS)];

/* Datagrams Lifetimes */
static const uint16_t auwDatagramLifetimes_DDMA[NUM_DDMA_DATAGRAMS] =
{
   UNDEF_LIFETIME_MS, //DG_DDMA__ParamSlaveRequest,
   UNDEF_LIFETIME_MS, //DG_DDMA__Alarm,
   IGNORED_DATAGRAM, //DG_DDMA__ExpInputs1,
   IGNORED_DATAGRAM, //DG_DDMA__ExpInputs2,
   IGNORED_DATAGRAM, //DG_DDMA__ExpInputs3,
   IGNORED_DATAGRAM, //DG_DDMA__ExpInputs4,
   IGNORED_DATAGRAM, //DG_DDMA__ExpInputs5,
   IGNORED_DATAGRAM, //DG_DDMA__ExpStatus1,
   IGNORED_DATAGRAM, //DG_DDMA__ExpStatus2,
   IGNORED_DATAGRAM, //DG_DDMA__ExpStatus3,
   IGNORED_DATAGRAM, //DG_DDMA__ExpStatus4,
   IGNORED_DATAGRAM, //DG_DDMA__ExpStatus5,
   UNDEF_LIFETIME_MS, //DG_DDMA__Debug,
};
static const uint16_t auwDatagramLifetimes_DDMB[NUM_DDMB_DATAGRAMS] =
{
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_ParamValues,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs1,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs2,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs3,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs4,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs5,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs6,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs7,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs8,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs9,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs10,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs11,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs12,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs13,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs14,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs15,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs16,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs17,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs18,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs19,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs20,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs21,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs22,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs23,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs24,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs25,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs26,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs27,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs28,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs29,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs30,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs31,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs32,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs33,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs34,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs35,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs36,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs37,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs38,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs39,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs40,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs41,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs42,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs43,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs44,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs45,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs46,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs47,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs48,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs49,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs50,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs51,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs52,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs53,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs54,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs55,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs56,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs57,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs58,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs59,
   UNDEF_LIFETIME_MS, //DG_DDMB__ParamMaster_BlockCRCs60,
   UNDEF_LIFETIME_MS, //DG_DDMB__Alarm,
   UNDEF_LIFETIME_MS, //DG_DDMB__SyncTime,
   UNDEF_LIFETIME_MS, //DG_DDMB__Debug,
};
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void ABNet_SetLocalNodeID( en_ddm_nodes eNode )
{
   eLocalNodeID = eNode;
}
en_ddm_nodes ABNet_GetLocalNodeID(void)
{
   return eLocalNodeID;
}
void ABNet_SetDestinationNodeID( en_ddm_nodes eNode )
{
   eDestNodeID = eNode;
}
en_ddm_nodes ABNet_GetDestinationNodeID(void)
{
   return eDestNodeID;
}
uint8_t ABNet_GetMaxNumDatagrams( en_ddm_nodes eNode )
{
   uint8_t ucNumDatagrams = 0;
   switch(eNode)
   {
      case DDM_NODE__A:
         ucNumDatagrams = NUM_DDMA_DATAGRAMS;
         break;
      case DDM_NODE__B:
         ucNumDatagrams = NUM_DDMB_DATAGRAMS;
         break;
      default: break;
   }
   return ucNumDatagrams;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint16_t ABNet_GetDatagramLifetime( uint8_t ucDatagramIndex, en_ddm_nodes eNode )
{
   uint16_t uwLifetime_ms = 0;
   switch(eNode)
   {
      case DDM_NODE__A:
         if( ucDatagramIndex < NUM_DDMA_DATAGRAMS )
         {
            uwLifetime_ms = auwDatagramLifetimes_DDMA[ucDatagramIndex];
         }
         break;
      case DDM_NODE__B:
         if( ucDatagramIndex < NUM_DDMB_DATAGRAMS )
         {
            uwLifetime_ms = auwDatagramLifetimes_DDMB[ucDatagramIndex];
         }
         break;
      default: break;
   }
   return uwLifetime_ms;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint32_t ABNet_GetMessageID( uint8_t ucDatagramIndex, en_ddm_nodes eSrcNode, en_ddm_nodes eDestNode )
{
   uint32_t uiMessageID = CAN_EXTEND_ID_USAGE;
   uiMessageID |= ucDatagramIndex << 21;
   uiMessageID |= (eSrcNode & 0x0F) << 14;
   uiMessageID |= eDestNode; // Convert to bitmap if multiple destinations needed
   return uiMessageID;
}
en_ddm_nodes ABNet_GetSrcNodeID( uint32_t uiMessageID )
{
   en_ddm_nodes eNode = (uiMessageID >> 14) & 0x0F;
   return eNode;
}
en_ddm_nodes ABNet_GetDestNodeID( uint32_t uiMessageID )
{
   en_ddm_nodes eNode = uiMessageID & 0x0F;
   return eNode;
}
uint8_t ABNet_GetDatagramIndex( uint32_t uiMessageID )
{
   uint8_t ucDatagramIndex = (uiMessageID >> 21) & 0xFF;
   return ucDatagramIndex;
}
/*-----------------------------------------------------------------------------
   Check if flag for new packet received is set
 -----------------------------------------------------------------------------*/
uint8_t ABNet_GetDirtyBit( uint8_t ucDatagramIndex, en_ddm_nodes eNode )
{
   uint8_t bDirty = 0;
   switch(eNode)
   {
      case DDM_NODE__A:
         if( ucDatagramIndex < NUM_DDMA_DATAGRAMS )
         {
            bDirty = Sys_Bit_Get(&abRxDirtyBits_DDMA[0], ucDatagramIndex);
         }
         break;
      case DDM_NODE__B:
         if( ucDatagramIndex < NUM_DDMB_DATAGRAMS )
         {
            bDirty = Sys_Bit_Get(&abRxDirtyBits_DDMB[0], ucDatagramIndex);
         }
         break;
      default: break;
   }
   return bDirty;
}
void ABNet_ClrDirtyBit( uint8_t ucDatagramIndex, en_ddm_nodes eNode )
{
   switch(eNode)
   {
      case DDM_NODE__A:
         if( ucDatagramIndex < NUM_DDMA_DATAGRAMS )
         {
            Sys_Bit_Set(&abRxDirtyBits_DDMA[0], ucDatagramIndex, 0);
         }
         break;
      case DDM_NODE__B:
         if( ucDatagramIndex < NUM_DDMB_DATAGRAMS )
         {
            Sys_Bit_Set(&abRxDirtyBits_DDMB[0], ucDatagramIndex, 0);
         }
         break;
      default: break;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void ABNet_ReceiveDatagram( un_sdata_datagram *punDatagram, uint8_t ucDatagramIndex, en_ddm_nodes eNode )
{
   switch(eNode)
   {
      case DDM_NODE__A:
         if( ucDatagramIndex < NUM_DDMA_DATAGRAMS )
         {
            memcpy(&aunRxDatagrams_DDMA[ucDatagramIndex], punDatagram, sizeof(un_sdata_datagram));
            Sys_Bit_Set(&abRxDirtyBits_DDMA[0], ucDatagramIndex, 1);
         }
         break;
      case DDM_NODE__B:
         if( ucDatagramIndex < NUM_DDMB_DATAGRAMS )
         {
            memcpy(&aunRxDatagrams_DDMB[ucDatagramIndex], punDatagram, sizeof(un_sdata_datagram));
            Sys_Bit_Set(&abRxDirtyBits_DDMB[0], ucDatagramIndex, 1);
         }
         break;
      default: break;
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t ABNet_GetDatagram_U8( uint8_t ucDataIndex, uint8_t ucDatagramIndex, en_ddm_nodes eNode )
{
   uint8_t ucData = 0;
   switch(eNode)
   {
      case DDM_NODE__A:
         if( ( ucDatagramIndex < NUM_DDMA_DATAGRAMS )
          && ( ucDataIndex < 8 ) )
         {
            ucData = aunRxDatagrams_DDMA[ucDatagramIndex].auc8[ucDataIndex];
         }
         break;
      case DDM_NODE__B:
         if( ( ucDatagramIndex < NUM_DDMB_DATAGRAMS )
          && ( ucDataIndex < 8 ) )
         {
            ucData = aunRxDatagrams_DDMB[ucDatagramIndex].auc8[ucDataIndex];
         }
         break;
      default: break;
   }
   return ucData;
}
uint16_t ABNet_GetDatagram_U16( uint8_t ucDataIndex, uint8_t ucDatagramIndex, en_ddm_nodes eNode )
{
   uint16_t uwData = 0;
   switch(eNode)
   {
      case DDM_NODE__A:
         if( ( ucDatagramIndex < NUM_DDMA_DATAGRAMS )
          && ( ucDataIndex < 4 ) )
         {
            uwData = aunRxDatagrams_DDMA[ucDatagramIndex].auw16[ucDataIndex];
         }
         break;
      case DDM_NODE__B:
         if( ( ucDatagramIndex < NUM_DDMB_DATAGRAMS )
          && ( ucDataIndex < 4 ) )
         {
            uwData = aunRxDatagrams_DDMB[ucDatagramIndex].auw16[ucDataIndex];
         }
         break;
      default: break;
   }
   return uwData;
}
uint32_t ABNet_GetDatagram_U32( uint8_t ucDataIndex, uint8_t ucDatagramIndex, en_ddm_nodes eNode )
{
   uint32_t uiData = 0;
   switch(eNode)
   {
      case DDM_NODE__A:
         if( ( ucDatagramIndex < NUM_DDMA_DATAGRAMS )
          && ( ucDataIndex < 2 ) )
         {
            uiData = aunRxDatagrams_DDMA[ucDatagramIndex].aui32[ucDataIndex];
         }
         break;
      case DDM_NODE__B:
         if( ( ucDatagramIndex < NUM_DDMB_DATAGRAMS )
          && ( ucDataIndex < 2 ) )
         {
            uiData = aunRxDatagrams_DDMB[ucDatagramIndex].aui32[ucDataIndex];
         }
         break;
      default: break;
   }
   return uiData;
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
