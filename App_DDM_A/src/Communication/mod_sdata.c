/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     26, March 2016
 *
 * @note    Load local datagrams and selects datagrams for transmit
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_SData =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

struct st_sdata_control gstSData_LocalData;
char *pasVersion;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define DG_TX_CYCLE_LIMIT  (3)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static Sys_Nodes eSysNodeID;// network id
static uint8_t ucNumberOfDatagrams;// number of datagrams generated locally
static CAN_MSG_T gstCAN_MSG_Tx;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Gets the last segment of the software version string for transmit on the group net
 -----------------------------------------------------------------------------*/
static void UpdateVersionString(void)
{
   uint8_t ucSize = sizeof(SOFTWARE_RELEASE_VERSION);
   uint8_t ucFirstIndex = 0;
   for(int8_t i = ucSize-1; i >= 0; i--)
   {
      char *ps = SOFTWARE_RELEASE_VERSION+i;
      if(*ps == '.')
      {
         ucFirstIndex = i+1;
         break;
      }
   }

   pasVersion = SOFTWARE_RELEASE_VERSION+ucFirstIndex;
}

/*-----------------------------------------------------------------------------
   Creates datagrams produced by this node
 -----------------------------------------------------------------------------*/
void SharedData_Init()
{
   int iError = 1;
   eSysNodeID = GetSystemNodeID();
   switch(eSysNodeID)
   {
      case SYS_NODE__DDM_A:
        ucNumberOfDatagrams = NUM_DDMB_DATAGRAMS;
        ABNet_SetLocalNodeID(DDM_NODE__A);
        ABNet_SetDestinationNodeID(DDM_NODE__B);
        break;
      case SYS_NODE__DDM_B:
        ucNumberOfDatagrams = NUM_DDMB_DATAGRAMS;
        ABNet_SetLocalNodeID(DDM_NODE__B);
        ABNet_SetDestinationNodeID(DDM_NODE__A);
        break;
      default:
         break;

   }
   if(ucNumberOfDatagrams)
   {
      iError = SDATA_CreateDatagrams( &gstSData_LocalData,
                                      ucNumberOfDatagrams
                                     );
   }

   if ( iError )
   {
      while ( 1 )
      {
         ;  // TODO: unable to allocate shared data
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData( void )
{
   un_sdata_datagram unDatagram;
   if( ABNet_GetLocalNodeID() == DDM_NODE__A )
   {
      memset(&unDatagram, 0, sizeof(un_sdata_datagram));
      switch( GetUIRequest_ViewDebugDataCommand() )
      {
         case VDDD__DDM_CAN1:
            unDatagram.auc8[0] = Debug_CAN_GetUtilization(0);
            unDatagram.auc8[1] = 0;
            unDatagram.auw16[1] = GetDebugBusOfflineCounter_CAN1();
            break;
         case VDDD__DDM_CAN2:
            unDatagram.auc8[0] = Debug_CAN_GetUtilization(1);
            unDatagram.auc8[1] = 0;
            unDatagram.auw16[1] = GetDebugBusOfflineCounter_CAN2();
            break;
         case VDDD__DDM_A_NET:
            unDatagram.auc8[0] = 0;
            unDatagram.auc8[1] = 0;
            unDatagram.auw16[1] = UART_GetRxErrorCount();
            break;
         case VDDD__DDMA_VERSION:
            unDatagram.auc8[0] = *(pasVersion+0);
            unDatagram.auc8[1] = *(pasVersion+1);
            unDatagram.auc8[2] = *(pasVersion+2);
            unDatagram.auc8[3] = *(pasVersion+3);
            break;
         default: break;
      }
      SDATA_WriteDatagram( &gstSData_LocalData,
                           DG_DDMA__Debug,
                           &unDatagram );
   }
   else if( ABNet_GetLocalNodeID() == DDM_NODE__B )
   {
      memset(&unDatagram, 0, sizeof(un_sdata_datagram));
      unDatagram.aui32[1] = RTC_GetSyncTime();
      SDATA_WriteDatagram( &gstSData_LocalData,
                           DG_DDMB__SyncTime,
                           &unDatagram );

      memset(&unDatagram, 0, sizeof(un_sdata_datagram));
      unDatagram.auc8[0] = GetUIRequest_ViewDebugDataCommand();
      SDATA_WriteDatagram( &gstSData_LocalData,
                           DG_DDMB__Debug,
                           &unDatagram );
   }
}

/*-----------------------------------------------------------------------------
 Loads local datagrams for transmit over Car Net and/or AB Net
 -----------------------------------------------------------------------------*/
static void Transmit( void )
{
   un_sdata_datagram unDatagram;
   int iError_AB = 0;
   uint8_t ucLocalDatagramID;
   uint16_t uwDatagramToSend_Plus1;
   uint8_t ucTXCycle;
   for( ucTXCycle = 0; ucTXCycle < DG_TX_CYCLE_LIMIT; ucTXCycle++)
   {
      uwDatagramToSend_Plus1 = SDATA_GetNextDatagramToSendIndex_Plus1( &gstSData_LocalData );
      if(uwDatagramToSend_Plus1)
      {
         ucLocalDatagramID = uwDatagramToSend_Plus1 - 1;

         if ( ucLocalDatagramID < ucNumberOfDatagrams )
         {
            SDATA_ReadDatagram( &gstSData_LocalData,
                                 ucLocalDatagramID,
                                &unDatagram
                              );

            memcpy( gstCAN_MSG_Tx.Data, unDatagram.auc8, sizeof( unDatagram.auc8 ) );
            gstCAN_MSG_Tx.DLC = 8;
            gstCAN_MSG_Tx.Type = 0;
            gstCAN_MSG_Tx.ID = ABNet_GetMessageID(ucLocalDatagramID, ABNet_GetLocalNodeID(), ABNet_GetDestinationNodeID());

            iError_AB = !UART_LoadCANMessage(&gstCAN_MSG_Tx);

            if ( iError_AB )
            {
               SDATA_DirtyBit_Set( &gstSData_LocalData, ucLocalDatagramID );

               if ( gstSData_LocalData.uwLastSentIndex )
               {
                  --gstSData_LocalData.uwLastSentIndex;
               }
               else
               {
                  gstSData_LocalData.uwLastSentIndex = gstSData_LocalData.uiNumDatagrams - 1;
               }
               ucTXCycle = DG_TX_CYCLE_LIMIT;
               break;
            }
         }
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadData( void )
{
   if( ABNet_GetLocalNodeID() == DDM_NODE__A )
   {
      RTC_SetSyncTime(ABNet_GetDatagram_U32(1, DG_DDMB__SyncTime, DDM_NODE__B));
      SetUIRequest_ViewDebugDataCommand(ABNet_GetDatagram_U8(0, DG_DDMB__Debug, DDM_NODE__B));
   }
   else if( ABNet_GetLocalNodeID() == DDM_NODE__B )
   {

   }
}

/*-----------------------------------------------------------------------------
   Set datagram resend rates
 -----------------------------------------------------------------------------*/
static void Initialize_DatagramSendRates()
{
   uint8_t ucOfflineDelay_5ms = AB_NET_OFFLINE_TIMEOUT_1MS/5;

   uint16_t uwHeartbeatInterval_1ms = (ucOfflineDelay_5ms*5)/3;
   gstSData_LocalData.uwHeartbeatInterval_1ms = uwHeartbeatInterval_1ms;

   for( uint8_t i = 0; i < ucNumberOfDatagrams; i++ )
   {
      uint16_t uwLifetime_ms = ABNet_GetDatagramLifetime(i, ABNet_GetLocalNodeID());
      if( uwLifetime_ms != IGNORED_DATAGRAM )
      {
         gstSData_LocalData.paiResendRate_1ms[i] = uwLifetime_ms/2;
      }
      else
      {
         gstSData_LocalData.paiResendRate_1ms[i] = DISABLED_DATAGRAM_RESEND_RATE_MS;
      }
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   //------------------------------------------------
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_AB_NET_1MS;

   Initialize_DatagramSendRates();
   UpdateVersionString();

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
    LoadData();
    Transmit();

    // Polls ring buffer for data and loads into CAN hardware buffer
    CAN1_FillHardwareBuffer();

    UnloadData();

    return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
