/******************************************************************************
 *
 * @file     mod_local_inputs.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "sru.h"
#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_LocalInputs =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static uint8_t gucSRU_Deployment;
static uint32_t guiLocalInputs_ThisNode;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint32_t GetLocalInputs_ThisNode(void)
{
   return guiLocalInputs_ThisNode;
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   gucSRU_Deployment = GetSRU_Deployment();

   //------------------------
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = 10;

   return 0;
}

/*-----------------------------------------------------------------------------

   This module is used on any MCUA. It simply collects all 16 inputs bitmapped
   in one variable, and forwards it to MRA for input mapping.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   en_sru_inputs enInput;
   uint32_t uiInputs = 0;
   for ( enInput = enSRU_Input_501; enInput < SRU_NUM_INPUTS; ++enInput )
   {
      uint_fast8_t bValue = SRU_Read_Input( enInput );

      uiInputs |= bValue << enInput;
   }

   for( en_ext_inputs eExtInput = eEXT_Input_SAF1; eExtInput < EXT_NUM_INPUTS; ++eExtInput )
   {
      uint_fast8_t bValue = SRU_Read_ExtInput( eExtInput );
      uiInputs |= bValue << (eExtInput + SRU_NUM_INPUTS);
   }
   guiLocalInputs_ThisNode = uiInputs;

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
