#include "GlobalData.h"
#include <stdint.h>
#include <string.h>
#include "sys.h"
#include "sru.h"
#include "mod.h"

/*-----------------------------------------------------------------------------
   Parameters
 -----------------------------------------------------------------------------*/
static uint8_t gbFlashParamsRdy = 0;

inline uint8_t GetFP_FlashParamsReady()
{
   return gbFlashParamsRdy;
}
// Only for mod_param_app
inline void SetFP_FlashParamsReady()
{
   gbFlashParamsRdy = 1;
}


/*-----------------------------------------------------------------------------
   UI Req - Default CMD
 ----------------------------------------------------------------------------*/
static enum_default_cmd eDefaultCMD;
void SetUIRequest_DefaultCommand( enum_default_cmd eCMD )
{
   if( eCMD < NUM_DEFAULT_CMD )
   {
      eDefaultCMD = eCMD;
   }
}
enum_default_cmd GetUIRequest_DefaultCommand()
{
   return eDefaultCMD;
}

/*-----------------------------------------------------------------------------
   View Debug Data DDM Requests
 ----------------------------------------------------------------------------*/
static en_view_debug_data_ddm eViewDebugData;
void SetUIRequest_ViewDebugDataCommand( en_view_debug_data_ddm eCommand )
{
   eViewDebugData = eCommand;
}
en_view_debug_data_ddm GetUIRequest_ViewDebugDataCommand(void)
{
   return eViewDebugData;
}
