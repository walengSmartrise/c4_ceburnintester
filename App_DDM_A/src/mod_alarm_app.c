/******************************************************************************
 * @file     mod_alarm_app.c
 * @author   Keith Soneda
 * @version  V1.00
 * @date     1, August 2016
 *
 * @note     Manages board alarms
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "sru.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "sys.h"
#include "position.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_module gstMod_AlarmApp =
{
   .pfnInit = Init,
   .pfnRun = Run,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
static en_ddm_nodes eAlarmNodeID;

static en_ddm_alarms aeAlarm[NUM_DDM_NODES];
static uint32_t auiTimeStamp[NUM_DDM_NODES];

static uint16_t uwAlarmLatchCountdown = ALARM_LATCH_PERIOD_1MS/MOD_RUN_PERIOD_ALARM_1MS;

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
   Accepts new alarm only if no alarm has been latched locally for 3 seconds.
   Asserting a alarm that is locally latched will reset the alarm's latch count
 -----------------------------------------------------------------------------*/
void SetAlarm( en_ddm_alarms eAlarmNum )
{
   if( eAlarmNum < NUM_DDM_ALARMS )
   {
      if( aeAlarm[AlarmApp_GetNode()] == DDM_ALM__NONE )
      {
         aeAlarm[AlarmApp_GetNode()] = eAlarmNum;
         auiTimeStamp[AlarmApp_GetNode()] = RTC_GetLocalTime();
         uwAlarmLatchCountdown = ALARM_LATCH_PERIOD_1MS/MOD_RUN_PERIOD_ALARM_1MS;
      }
      else if( aeAlarm[AlarmApp_GetNode()] == eAlarmNum )
      {
         uwAlarmLatchCountdown = ALARM_LATCH_PERIOD_1MS/MOD_RUN_PERIOD_ALARM_1MS;
      }
   }
}

/*-----------------------------------------------------------------------------
   Returns 1 if any alarm in a range is active. Inclusive.
   Must be eFirstAlarmNum < eLastAlarmNum
 -----------------------------------------------------------------------------*/
uint8_t GetAlarm_ByRange( en_ddm_alarms eFirstAlarmNum,  en_ddm_alarms eLastAlarmNum )
{
   uint8_t bActive = 0;
   if(eFirstAlarmNum <= eLastAlarmNum)
   {
      for(uint8_t i = 0; i < NUM_DDM_NODES; i++)
      {
         if( ( aeAlarm[i] >= eFirstAlarmNum )
          && ( aeAlarm[i] <= eLastAlarmNum ) )
         {
            bActive = 1;
            break;
         }
      }
   }
   return bActive;
}
/*-----------------------------------------------------------------------------
   Returns 1 if a specific fault is active
 -----------------------------------------------------------------------------*/
uint8_t GetAlarm_ByNumber( en_ddm_alarms eAlarmNum )
{
   uint8_t bActive = 0;
   for(uint8_t i = 0; i < NUM_DDM_NODES; i++)
   {
      if( aeAlarm[i] == eAlarmNum)
      {
         bActive = 1;
         break;
      }
   }
   return bActive;
}

/*-----------------------------------------------------------------------------
   Returns the active alarm for the specified alarm node
 -----------------------------------------------------------------------------*/
en_ddm_alarms GetAlarm_ByNode( en_ddm_nodes eAlarmNode )
{
   if( eAlarmNode < NUM_DDM_NODES)
   {
      return aeAlarm[eAlarmNode];
   }
   return DDM_ALM__NONE;
}

/*-----------------------------------------------------------------------------
   Returns timestamp of an active alarm
 -----------------------------------------------------------------------------*/
uint32_t GetAlarm_TimeStamp( en_ddm_nodes eAlarmNode )
{
   uint32_t uiTimeStamp = 0;
   if( eAlarmNode < NUM_DDM_NODES)
   {
      uiTimeStamp = auiTimeStamp[eAlarmNode];
   }
   return uiTimeStamp;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckFor_LatchedAlarmTimeout(void)
{
   en_ddm_alarms eAlarmNum = aeAlarm[AlarmApp_GetNode()];
   if( eAlarmNum != DDM_ALM__NONE )
   {
      if( uwAlarmLatchCountdown )
      {
         uwAlarmLatchCountdown--;
      }
      else
      {
         aeAlarm[AlarmApp_GetNode()] = DDM_ALM__NONE;
         auiTimeStamp[AlarmApp_GetNode()] = 0;
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_AlarmLED()
{
   static uint8_t ucTimer_5ms;
   static uint8_t bLastLED;
   uint8_t bActive = 0;
   for( uint8_t i = 0; i < NUM_DDM_NODES; i++ )
   {
      if( aeAlarm[i] != DDM_ALM__NONE )
      {
         bActive = 1;
         break;
      }
   }

   if( bActive )
   {
      if ( ++ucTimer_5ms >= 20 )
      {
         SRU_Write_LED( enSRU_LED_Alarm, bLastLED );

         ucTimer_5ms = 0;

         bLastLED = ( bLastLED ) ? 0 : 1;
      }
   }
   else
   {
      SRU_Write_LED( enSRU_LED_Alarm, 0 );
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
en_ddm_nodes AlarmApp_GetNode( void )
{
   return eAlarmNodeID;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadDatagram_Alarm(void)
{
   un_sdata_datagram unDatagram;
   en_ddm_nodes eNode = AlarmApp_GetNode();
   if( eNode == DDM_NODE__A )
   {
      unDatagram.auw16[0] = aeAlarm[eNode];
      unDatagram.auc8[2] = eNode+1;
      unDatagram.auc8[3] = 0;
      unDatagram.aui32[1] = auiTimeStamp[eNode];

      SDATA_WriteDatagram( &gstSData_LocalData, DG_DDMA__Alarm, &unDatagram );
   }
   else if( eNode == DDM_NODE__B )
   {
      unDatagram.auw16[0] = aeAlarm[eNode];
      unDatagram.auc8[2] = eNode+1;
      unDatagram.auc8[3] = 0;
      unDatagram.aui32[1] = auiTimeStamp[eNode];

      SDATA_WriteDatagram( &gstSData_LocalData, DG_DDMB__Alarm, &unDatagram );
   }
}
static void UnloadDatagram_Alarm(void)
{
   if( AlarmApp_GetNode() == DDM_NODE__A )
   {
      en_ddm_alarms eAlarm = ABNet_GetDatagram_U16(0, DG_DDMB__Alarm, DDM_NODE__B);
      uint8_t ucNode_Plus1 = ABNet_GetDatagram_U8(2, DG_DDMB__Alarm, DDM_NODE__B);
      uint32_t uiTimeStamp = ABNet_GetDatagram_U32(1, DG_DDMB__Alarm, DDM_NODE__B);
      if( ( eAlarm < NUM_DDM_ALARMS )
       && ( ucNode_Plus1 )
       && ( ucNode_Plus1 <= NUM_DDM_NODES )
       && ( (ucNode_Plus1-1) != AlarmApp_GetNode() ) )
      {
         aeAlarm[ucNode_Plus1-1] = eAlarm;
         auiTimeStamp[ucNode_Plus1-1] = uiTimeStamp;
      }
   }
   else if( AlarmApp_GetNode() == DDM_NODE__B )
   {
      en_ddm_alarms eAlarm = ABNet_GetDatagram_U16(0, DG_DDMA__Alarm, DDM_NODE__A);
      uint8_t ucNode_Plus1 = ABNet_GetDatagram_U8(2, DG_DDMA__Alarm, DDM_NODE__A);
      uint32_t uiTimeStamp = ABNet_GetDatagram_U32(1, DG_DDMA__Alarm, DDM_NODE__A);
      if( ( eAlarm < NUM_DDM_ALARMS )
       && ( ucNode_Plus1 )
       && ( ucNode_Plus1 <= NUM_DDM_NODES )
       && ( (ucNode_Plus1-1) != AlarmApp_GetNode() ) )
      {
         aeAlarm[ucNode_Plus1-1] = eAlarm;
         auiTimeStamp[ucNode_Plus1-1] = uiTimeStamp;
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateResetAlarm(void)
{
   static uint32_t uiResetSrc;
   if(!uiResetSrc)
   {
      uiResetSrc = Chip_SYSCTL_GetSystemRSTStatus();
      Chip_SYSCTL_ClearSystemRSTStatus(uiResetSrc);
   }

   if( uiResetSrc & SYSCTL_RST_BOD )
   {
      if( eAlarmNodeID == DDM_NODE__A )
      {
         aeAlarm[AlarmApp_GetNode()] = DDM_ALM__RESET_BOD_A;
      }
      else if( eAlarmNodeID == DDM_NODE__B )
      {
         aeAlarm[AlarmApp_GetNode()] = DDM_ALM__RESET_BOD_B;
      }
   }
   else if( uiResetSrc & SYSCTL_RST_WDT )
   {
      if( eAlarmNodeID == DDM_NODE__A )
      {
         aeAlarm[AlarmApp_GetNode()] = DDM_ALM__RESET_WDT_A;
      }
      else if( eAlarmNodeID == DDM_NODE__B )
      {
         aeAlarm[AlarmApp_GetNode()] = DDM_ALM__RESET_WDT_B;
      }
   }
   else//assume SYSCTL_RST_POR
   {
      if( eAlarmNodeID == DDM_NODE__A )
      {
         aeAlarm[AlarmApp_GetNode()] = DDM_ALM__RESET_POR_A;
      }
      else if( eAlarmNodeID == DDM_NODE__B )
      {
         aeAlarm[AlarmApp_GetNode()] = DDM_ALM__RESET_POR_B;
      }
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 1000;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_ALARM_1MS;

   eAlarmNodeID = ABNet_GetLocalNodeID();
   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static uint32_t uiStartupCounter_5ms = BOARD_RESET_FAULT_TIMER_MS/MOD_RUN_PERIOD_ALARM_1MS;

   if( uiStartupCounter_5ms )
   {
      UpdateResetAlarm();
      uiStartupCounter_5ms--;
   }
   else
   {
      CheckFor_LatchedAlarmTimeout();

      if( SRU_Read_DIP_Switch(enSRU_DIP_A1) )
      {
         if( eAlarmNodeID == DDM_NODE__A )
         {
            SetAlarm(DDM_ALM__CPU_STOP_A);
         }
         else if( eAlarmNodeID == DDM_NODE__B )
         {
            SetAlarm(DDM_ALM__CPU_STOP_B);
         }
      }
   }

   Update_AlarmLED();

   LoadDatagram_Alarm();

   UnloadDatagram_Alarm();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
