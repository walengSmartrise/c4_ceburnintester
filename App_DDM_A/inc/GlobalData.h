
#ifndef GLOBAL_DATA_H
#define GLOBAL_DATA_H

#include <stdint.h>
#include "sys.h"
#include "fram_data.h"
#include "sru.h"
#include "shared_data.h"

typedef enum
{
   VDDD__NONE,
   VDDD__DDM_CAN1,
   VDDD__DDM_CAN2,
   VDDD__DDM_CAN3,
   VDDD__DDM_CAN4,
   VDDD__DDM_A_NET,
   VDDD__DDM_B_NET,
   VDDD__DDM_RS485,
   VDDD__DDMA_VERSION,
   VDDD__DDMB_VERSION,

   NUM_VDDD
} en_view_debug_data_ddm;

/*-----------------------------------------------------------------------------
   Parameters
 -----------------------------------------------------------------------------*/
uint8_t GetFP_FlashParamsReady();
void SetFP_FlashParamsReady();

/*-----------------------------------------------------------------------------
   UI Req - Default CMD
 ----------------------------------------------------------------------------*/
void SetUIRequest_DefaultCommand( enum_default_cmd eCMD );
enum_default_cmd GetUIRequest_DefaultCommand();

/*-----------------------------------------------------------------------------
   View Debug Data DDM Requests
 ----------------------------------------------------------------------------*/
void SetUIRequest_ViewDebugDataCommand( en_view_debug_data_ddm eCommand );
en_view_debug_data_ddm GetUIRequest_ViewDebugDataCommand(void);

#endif // GLOBAL_DATA_H
