/******************************************************************************
 *
 * @file     fault_app.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef _ALARM_APP_H_
#define _ALARM_APP_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
#include "DDM_Alarms.h"
#include "ddm_network.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_ALARM_1MS             (5U)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_module gstMod_AlarmApp;
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

uint8_t GetAlarm_ByRange( en_ddm_alarms eFirstAlarmNum,  en_ddm_alarms eLastAlarmNum );
uint8_t GetAlarm_ByNumber( en_ddm_alarms eAlarmNum );
en_ddm_alarms GetAlarm_ByNode( en_ddm_nodes eAlarmNode );
uint32_t GetAlarm_TimeStamp( en_ddm_nodes eAlarmNode );

/* Alarm access functions */
void SetAlarm( en_ddm_alarms eAlarmNum );
en_ddm_nodes AlarmApp_GetNode( void );


#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
