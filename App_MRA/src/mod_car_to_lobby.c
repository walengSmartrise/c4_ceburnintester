/******************************************************************************
 *
 * @file     mod_car_to_lobby.c
 * @brief    Control Logic for Car To Lobby.
 * @version  V1.00
 * @date     24 Sept, 2018
 *
 * @note     This module pulls the car down to the lobby floor when the Car To Lobby
 *           input is on, and opens its doors.
 *           If configured, with enPARAM1__CarToLobbyExpress on, the car will immediately move to the recall floor and open its doors.
 *           Otherwise, the car will wait until the car is capture before moving to lobby
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_CarToLobby =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

static uint8_t bCarToLobbyLamp; // Sets lamp output when at recall floor with doors open
static uint8_t bCarToLobbyFlag; // Debounced car to lobby input
static uint8_t bHasBeenCaptured;
static uint8_t bLastCarToLobbyFlag;
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t GetCarToLobbyRecallFlag(void)
{
   uint8_t bRecall = 0;
   if( ( GetOperation_AutoMode() == MODE_A__NORMAL )
    && ( bCarToLobbyFlag ) )
   {
      if( Param_ReadValue_1Bit(enPARAM1__CarToLobbyExpress) )
      {
         bRecall = 1;
      }
      else if( bHasBeenCaptured ) // Recall only once the car has been captured
      {
         bRecall = 1;
      }
   }
   return bRecall;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t GetCarToLobbyLamp(void)
{
   return bCarToLobbyLamp;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateCarToLobbyFlag(void)
{
   #define CAR_TO_LOBBY_DEBOUNCE_500MS       (3)
   static uint8_t ucDebounce_500ms;
   bCarToLobbyFlag = 0;
   if( GetInputValue( enIN_CAR_TO_LOBBY ) )
   {
      if( ucDebounce_500ms >= CAR_TO_LOBBY_DEBOUNCE_500MS )
      {
         bCarToLobbyFlag = 1;
      }
      else
      {
         ucDebounce_500ms++;
      }
   }
   else
   {
      ucDebounce_500ms = 0;
   }
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10000;
   pstThisModule->uwRunPeriod_1ms = 500;
   return 0;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   UpdateCarToLobbyFlag();

   if( ( GetOperation_AutoMode() == MODE_A__NORMAL )
    && ( bCarToLobbyFlag ) )
   {
      /* When car to lobby is active, stop taking HCs */
      StartCaptureMode(INVALID_FLOOR);
      if( getCaptureMode() == CAPTURE_IDLE )
      {
         bHasBeenCaptured = 1;
      }

      /* If the express bit is set, also stop taking CCs */
      if( Param_ReadValue_1Bit(enPARAM1__CarToLobbyExpress) )
      {
         ClearLatchedCarCalls();
      }
   }
   else
   {
      bHasBeenCaptured = 0;
   }

   bCarToLobbyLamp = 0;
   if( ( GetCarToLobbyRecallFlag() )
    && ( GetRecallState() == RECALL__RECALL_FINISHED ) )
   {
      bCarToLobbyLamp = 1;
   }

   /* Make sure capture mode is cleared the second the car to lobby
    * input turns off. Do not wait for timeout */
   if( bLastCarToLobbyFlag && !bCarToLobbyFlag )
   {
      ResetCaptureMode();
   }
   bLastCarToLobbyFlag = bCarToLobbyFlag;
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
