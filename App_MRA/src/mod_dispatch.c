/******************************************************************************
 *
 * @file     mod_dispatch.c
 * @brief    Logic that scans the call data to give dispatch control.
 * @version  V1.00
 * @date     28, July 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

 #include "mod.h"

 #include "sru_a.h"
 #include <stdint.h>
 #include "sys.h"
 #include "operation.h"
 #include "position.h"
 #include "hallboard.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init(struct st_module * pstThisModule);
static uint32_t Run(struct st_module * pstThisModule);
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Dispatch = {
   .pfnInit = Init,
   .pfnRun = Run,
};

struct st_destination
{
   uint8_t ucFloorIndex; // Current destination opening
   enum direction_enum eCallDir; // Call direction

   uint8_t ucNextFloorIndex; // Current destination opening
   enum direction_enum eNextCallDir; // Call direction

   uint8_t ucDirection; // Arrival direction announcement
   uint8_t bDirectionChange_HC; // Flag marks if this destination is a HC in the opposite direction. Triggers an immediate arrival lantern, skipping the direction delay timer.
};


struct st_hallcalls * gpastHallCalls_F;
struct st_hallcalls * gpastHallCalls_R;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

#define GOING_UP   (4)
#define GOING_DOWN (2)
#define GOING_NONE (0)

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
struct st_autoModeRules gstCurrentModeRules[NUM_MODE_AUTO];
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static uint8_t bPriorityUp = 0; // Direction car is going
static uint8_t bNextPriorityUp = 0; // Direction of car's destination
static uint8_t bRelevelingFlag = 0;
// If 1, car has been idle for the enPARAM8__DirChangeDelay_1s and qualifies for direction change
static uint8_t bIdleDirection;

static uint8_t bStopNextFloor;

static enum direction_enum eArrivalDir_F = DIR__NONE;
static enum direction_enum eArrivalDir_R = DIR__NONE;


static uint8_t ucNextDestination; // Used to verify if the car has demand in either direction

/* Current destination */
static struct st_destination stDestination_Current;

/* Next destination, checking in the same direction priority */
static struct st_destination stDestination_Next_SameDir;

/* Next destination, checking in the opposite direction priority */
static struct st_destination stDestination_Next_DiffDir;


/* Stores last cleared call type, when car comes in to a floor servicing a car call, the direction change delay is bypassed, allowing the car to flip direction immediately.
 * If the CC has already been cleared when demand in opposite direction is found, this variable ensures the delay is still bypassed. */
static enum direction_enum aeLastClearedCallType[NUM_OF_DOORS];

/* Track the last floor doors were opened on to prevent locking
 * out HC reopening on a floor on initial approach */
static uint8_t ucLastOpenedFloor_Plus1;
static uint8_t bSuppressReopen;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   Returns modified auto mode rules to allow fire phase 2 door rules
   to be used even if it isnt the active automatic operation mode
 *----------------------------------------------------------------------------*/
uint8_t AutoModeRules_GetAutoDoorOpenFlag(void)
{
   enum en_mode_auto eAutoMode = GetOperation_AutoMode();
   uint8_t bFlag = gstCurrentModeRules[eAutoMode].bAutoDoorOpen;
   if( Fire_CheckIfFire2DoorsEnabled() )
   {
      bFlag = gstCurrentModeRules[MODE_A__FIRE2].bAutoDoorOpen;
   }
   return bFlag;
}
uint8_t AutoModeRules_GetDoorHoldFlag(void)
{
   enum en_mode_auto eAutoMode = GetOperation_AutoMode();
   uint8_t bFlag = gstCurrentModeRules[eAutoMode].bDoorHold;
   if( Fire_CheckIfFire2DoorsEnabled() )
   {
      bFlag = gstCurrentModeRules[MODE_A__FIRE2].bDoorHold;
   }
   return bFlag;
}
uint8_t AutoModeRules_GetForceDoorsOpenOrClosedFlag(void)
{
   enum en_mode_auto eAutoMode = GetOperation_AutoMode();
   uint8_t bFlag = gstCurrentModeRules[eAutoMode].bForceDoorsOpenOrClosed;
   if( Fire_CheckIfFire2DoorsEnabled() )
   {
      bFlag = gstCurrentModeRules[MODE_A__FIRE2].bForceDoorsOpenOrClosed;
   }
   return bFlag;
}
uint8_t AutoModeRules_GetIgnoreDCBFlag(void)
{
   enum en_mode_auto eAutoMode = GetOperation_AutoMode();
   uint8_t bFlag = gstCurrentModeRules[eAutoMode].bIgnoreDCB;
   if( Fire_CheckIfFire2DoorsEnabled() )
   {
      bFlag = gstCurrentModeRules[MODE_A__FIRE2].bIgnoreDCB;
   }
   return bFlag;
}
/*-----------------------------------------------------------------------------
   Returns projected destinations in the following encoding:
      Bits  0 to  7 :  ucFloorIndex
      Bits  8 to  9 :  the arrival direction, en_hc_dir_plus1
      Bits 10 to 11 :  the call type, en_hc_dir_plus1
      Bits 12       :  bDirectionChange_HC, 1 if the destination is a HC in the opposite direction
-----------------------------------------------------------------------------*/
uint16_t Dispatch_GetDebugDestination_Current(void)
{
   uint16_t uwDestination = INVALID_FLOOR;
   if( stDestination_Current.ucFloorIndex < GetFP_NumFloors() )
   {
      en_hc_dir_plus1 eArrivalDir = HC_DIR_PLUS1__NONE;
      en_hc_dir_plus1 eCallDir = HC_DIR_PLUS1__NONE;
      if( stDestination_Current.eCallDir == DIR__UP )
      {
         eCallDir = HC_DIR_PLUS1__UP;
      }
      else if( stDestination_Current.eCallDir == DIR__DN )
      {
         eCallDir = HC_DIR_PLUS1__DN;
      }
      if( stDestination_Current.ucDirection == GOING_UP )
      {
         eArrivalDir = HC_DIR_PLUS1__UP;
      }
      else if( stDestination_Current.eCallDir == GOING_DOWN )
      {
         eArrivalDir = HC_DIR_PLUS1__DN;
      }
      uwDestination  = stDestination_Current.ucFloorIndex;
      uwDestination |= eArrivalDir << 8;
      uwDestination |= eCallDir << 10;
      uwDestination |= stDestination_Current.bDirectionChange_HC << 12;
   }
   return uwDestination;
}
uint16_t Dispatch_GetDebugDestination_Next_SameDir(void)
{
   uint16_t uwDestination = INVALID_FLOOR;
   if( stDestination_Next_SameDir.ucFloorIndex < GetFP_NumFloors() )
   {
      en_hc_dir_plus1 eArrivalDir = HC_DIR_PLUS1__NONE;
      en_hc_dir_plus1 eCallDir = HC_DIR_PLUS1__NONE;
      if( stDestination_Next_SameDir.eCallDir == DIR__UP )
      {
         eCallDir = HC_DIR_PLUS1__UP;
      }
      else if( stDestination_Next_SameDir.eCallDir == DIR__DN )
      {
         eCallDir = HC_DIR_PLUS1__DN;
      }
      if( stDestination_Next_SameDir.ucDirection == GOING_UP )
      {
         eArrivalDir = HC_DIR_PLUS1__UP;
      }
      else if( stDestination_Next_SameDir.eCallDir == GOING_DOWN )
      {
         eArrivalDir = HC_DIR_PLUS1__DN;
      }
      uwDestination  = stDestination_Next_SameDir.ucFloorIndex;
      uwDestination |= eArrivalDir << 8;
      uwDestination |= eCallDir << 10;
      uwDestination |= stDestination_Next_SameDir.bDirectionChange_HC << 12;
   }
   return uwDestination;
}
uint16_t Dispatch_GetDebugDestination_Next_DiffDir(void)
{
   uint16_t uwDestination = INVALID_FLOOR;
   if( stDestination_Next_DiffDir.ucFloorIndex < GetFP_NumFloors() )
   {
      en_hc_dir_plus1 eArrivalDir = HC_DIR_PLUS1__NONE;
      en_hc_dir_plus1 eCallDir = HC_DIR_PLUS1__NONE;
      if( stDestination_Next_DiffDir.eCallDir == DIR__UP )
      {
         eCallDir = HC_DIR_PLUS1__UP;
      }
      else if( stDestination_Next_DiffDir.eCallDir == DIR__DN )
      {
         eCallDir = HC_DIR_PLUS1__DN;
      }
      if( stDestination_Next_DiffDir.ucDirection == GOING_UP )
      {
         eArrivalDir = HC_DIR_PLUS1__UP;
      }
      else if( stDestination_Next_DiffDir.eCallDir == GOING_DOWN )
      {
         eArrivalDir = HC_DIR_PLUS1__DN;
      }
      uwDestination  = stDestination_Next_DiffDir.ucFloorIndex;
      uwDestination |= eArrivalDir << 8;
      uwDestination |= eCallDir << 10;
      uwDestination |= stDestination_Next_DiffDir.bDirectionChange_HC << 12;
   }
   return uwDestination;
}
/*-----------------------------------------------------------------------------
   Abort calls at current landing for doors fail to open faults
-----------------------------------------------------------------------------*/
void AbortCallsAtCurrentLanding()
{
   uint8_t ucCurrentFloor = GetOperation_CurrentFloor();

   if( ucCurrentFloor < GetFP_NumFloors() )
   {
      gpastCarCalls_F[ucCurrentFloor].bLatched = 0;
      gpastHallCalls_F[ucCurrentFloor].bLatched_Down = 0;
      gpastHallCalls_F[ucCurrentFloor].bLatched_Up = 0;
      if( GetFP_RearDoors() )
      {
         gpastCarCalls_R[ucCurrentFloor].bLatched = 0;
         gpastHallCalls_R[ucCurrentFloor].bLatched_Down = 0;
         gpastHallCalls_R[ucCurrentFloor].bLatched_Up = 0;
      }
   }
}
/*-----------------------------------------------------------------------------
   Returns current car direction, 1 = up, 0 = down (FOR DEBUGGING)
-----------------------------------------------------------------------------*/
uint8_t GetCurrentCarDirection()
{
   return bPriorityUp;
}
/*-----------------------------------------------------------------------------
   Returns direction of active arrival lamp
-----------------------------------------------------------------------------*/
enum direction_enum GetArrivalLamp_Front()
{
   return eArrivalDir_F;
}
enum direction_enum GetArrivalLamp_Rear()
{
   return eArrivalDir_R;
}
/*-----------------------------------------------------------------------------
   returns 1 if out of group
-----------------------------------------------------------------------------*/
static uint8_t CheckIf_CarOutOfGroup()
{
   uint8_t bOutOfGroup = 0;
   /* When in swing and attempting to capture the car,
    * temporarily disable all hall calls */
   if( ( ( GetOperation_AutoMode() == MODE_A__SWING )
    && ( GetAutoSwingState() != SWING__ON ) )
    || ( (GetOperation_AutoMode() == MODE_A__VIP_MODE )
    && ( VIP_GetAutoSwingState() != SWING__ON ) ) )
   {
      bOutOfGroup = 1;
   }
   return bOutOfGroup;
}
static uint8_t GetNextReachableFloor ( void );
static void GetNextDestinationBelow( uint8_t ucStartFloor, struct st_destination * stDestination);
static void GetNextDestinationAbove( uint8_t ucStartFloor, struct st_destination * stDestination);
/*-----------------------------------------------------------------------------
   Returns 1 if the current auto mode supports arrival lanterns
-----------------------------------------------------------------------------*/
static uint8_t CheckIfValidArrivalMode(void)
{
   uint8_t bReturn = 0;
   enum en_mode_auto eAutoMode  = GetOperation_AutoMode();
   if( HallCallsDisabled() ) /* Suppress arrival output when hall calls are disabled */
   {
      bReturn = 0;
   }
   else if( ( eAutoMode == MODE_A__NORMAL )
         || ( eAutoMode == MODE_A__SABBATH )
         || ( eAutoMode == MODE_A__EMS1 )
         || ( eAutoMode == MODE_A__SWING )
         || ( eAutoMode == MODE_A__ATTENDANT )
         || ( eAutoMode == MODE_A__ACTIVE_SHOOTER_MODE)
         || ( eAutoMode == MODE_A__VIP_MODE)
    )
   {
      bReturn = 1;
   }
   return bReturn;
}
/*-----------------------------------------------------------------------------
   Get car call arrival direciton
-----------------------------------------------------------------------------*/
enum direction_enum GetAnsweredCarCallDirection()
{
   enum direction_enum eArrivalDir = DIR__NONE;
   uint8_t bNextDirectionUp = bPriorityUp;
   enum direction_enum eMotionDir = GetMotion_Direction();

   uint8_t ucDemand = INVALID_FLOOR;

   uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
   uint8_t ucDestinationFloor = GetOperation_DestinationFloor();
   uint8_t ucReachableFloor = GetNextReachableFloor();

   /* If there was a next destination found in the current direction... */
   if( stDestination_Next_SameDir.ucFloorIndex < GetFP_NumFloors() )
   {
      if( ucDestinationFloor < stDestination_Next_SameDir.ucFloorIndex )
      {
         eArrivalDir = DIR__UP;
      }
      else if( ucDestinationFloor > stDestination_Next_SameDir.ucFloorIndex )
      {
         eArrivalDir = DIR__DN;
      }
   }
#if 0 // Remove this to address car call direction change issue when there is a hall calls in the current direction but car call in the opposing direction
   /* If there was not a next destination found in the current direction... Check the other direction */
   else if( stDestination_Next_DiffDir.ucFloorIndex < GetFP_NumFloors() )
   {
      if( ucDestinationFloor < stDestination_Next_DiffDir.ucFloorIndex )
      {
         eArrivalDir = DIR__UP;
      }
      else if( ucDestinationFloor > stDestination_Next_DiffDir.ucFloorIndex )
      {
         eArrivalDir = DIR__DN;
      }
   }
#endif

   return eArrivalDir;
}
/*-----------------------------------------------------------------------------
GetCurrentLandingCalls function used to check what calls are available at the current floor.
Returns the call types in bitmap form.
B0 = CC_F
B1 = HC_F
B4 = CC_R
B5 = HC_R
-----------------------------------------------------------------------------*/
uint8_t GetCurrentLandingCalls(void)
{
   uint8_t bNextDirectionUp = bPriorityUp;
   uint8_t ucOpenDoorRequest = 0;
   uint8_t ucCurrentFloor = GetOperation_CurrentFloor();



   if( ucCurrentFloor < GetFP_NumFloors() )
   {
      if (gpastFloors[ucCurrentFloor].bFrontOpening)
      {
         if (gpastCarCalls_F[ucCurrentFloor].bLatched || GetDoorOpenButton ( DOOR_FRONT ) ) {
            if( ( !AutoModeRules_GetAutoDoorOpenFlag() )
             || ( getCaptureMode() == CAPTURE_IDLE ) )
            {
               gpastCarCalls_F[ucCurrentFloor].bLatched = 0;
            }
            ucOpenDoorRequest |= 1;

            if( Get_ADARequest( ucCurrentFloor, ADA_TYPE__CC ) )
            {
               SetADAFlag(1);
            }
         }

         if (!bNextDirectionUp && gpastHallCalls_F[ucCurrentFloor].bLatched_Down) {
            if( !bSuppressReopen )
            {
               ucOpenDoorRequest |= 0x02;
               /* The type of call last cleared should be logged for direction change delays */
               aeLastClearedCallType[DOOR_FRONT] = DIR__DN;

               if( Get_ADARequest( ucCurrentFloor, ADA_TYPE__DN ) )
               {
                  SetADAFlag(1);
               }
            }
         }
         else if (bNextDirectionUp && gpastHallCalls_F[ucCurrentFloor].bLatched_Up) {
            if( !bSuppressReopen )
            {
               ucOpenDoorRequest |= 0x02;
               /* The type of call last cleared should be logged for direction change delays */
               aeLastClearedCallType[DOOR_FRONT] = DIR__UP;

               if( Get_ADARequest( ucCurrentFloor, ADA_TYPE__UP ) )
               {
                  SetADAFlag(1);
               }
            }
         }
      }

      if (GetFP_RearDoors() && gpastFloors[ucCurrentFloor].bRearOpening) {
         if (gpastCarCalls_R[ucCurrentFloor].bLatched || GetDoorOpenButton ( DOOR_REAR ) ) {
            if( ( !AutoModeRules_GetAutoDoorOpenFlag() )
             || ( getCaptureMode() == CAPTURE_IDLE ) )
            {
               gpastCarCalls_R[ucCurrentFloor].bLatched = 0;
            }
            ucOpenDoorRequest |= 0x10;

            if( Get_ADARequest( ucCurrentFloor, ADA_TYPE__CC ) )
            {
               SetADAFlag(1);
            }
         }

         if (!bNextDirectionUp && gpastHallCalls_R[ucCurrentFloor].bLatched_Down) {
            if( !bSuppressReopen )
            {
               ucOpenDoorRequest |= 0x20;
               /* The type of call last cleared should be logged for direction change delays */
               aeLastClearedCallType[DOOR_REAR] = DIR__DN;

               if( Get_ADARequest( ucCurrentFloor, ADA_TYPE__DN ) )
               {
                  SetADAFlag(1);
               }
            }
         }
         else if (bNextDirectionUp && gpastHallCalls_R[ucCurrentFloor].bLatched_Up) {
            if( !bSuppressReopen )
            {
               ucOpenDoorRequest |= 0x20;
               /* The type of call last cleared should be logged for direction change delays */
               aeLastClearedCallType[DOOR_REAR] = DIR__UP;

               if( Get_ADARequest( ucCurrentFloor, ADA_TYPE__UP ) )
               {
                  SetADAFlag(1);
               }
            }
         }
      }
   }

   return ucOpenDoorRequest;
}
/*-----------------------------------------------------------------------------
   Clear current landing calls. Used by manual doors to clear calls if door
   did not open at destination before N seconds.
-----------------------------------------------------------------------------*/
void ClearCurrentLandingCalls_ManualDoor( enum en_doors enDoorIndex )
{
   uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
   if( GetFP_DoorType(enDoorIndex) == DOOR_TYPE__MANUAL )
   {
      if(enDoorIndex == DOOR_FRONT)
      {
         gpastHallCalls_F[ucCurrentFloor].bLatched_Down = 0;
         gpastHallCalls_F[ucCurrentFloor].bLatched_Up = 0;
         gpastCarCalls_F[ucCurrentFloor].bLatched = 0;
      }
      else
      {
         gpastHallCalls_R[ucCurrentFloor].bLatched_Up = 0;
         gpastHallCalls_R[ucCurrentFloor].bLatched_Down = 0;
         gpastCarCalls_R[ucCurrentFloor].bLatched = 0;
      }
   }
}
/*-----------------------------------------------------------------------------
clearCurrentLandingCalls function used to clear calls on the floor the
elevator is at currently.
-----------------------------------------------------------------------------*/
void ClearCurrentLandingCalls(void)
{
   uint8_t bNextDirectionUp = bPriorityUp;
   uint8_t ucCurrentFloor = GetOperation_CurrentFloor();

   static enum en_door_states aeLastDoorState[NUM_OF_DOORS];

   if( bSuppressReopen )
   {
      if (!bNextDirectionUp)
      {
         gpastHallCalls_F[ucCurrentFloor].bLatched_Down = 0;
         if( GetFP_RearDoors() )
         {
            gpastHallCalls_R[ucCurrentFloor].bLatched_Down = 0;
         }
      }
      else
      {
         gpastHallCalls_F[ucCurrentFloor].bLatched_Up = 0;
         if( GetFP_RearDoors() )
         {
            gpastHallCalls_R[ucCurrentFloor].bLatched_Up = 0;
         }
      }
   }

   if( ucCurrentFloor < GetFP_NumFloors() )
   {
      if (gpastFloors[ucCurrentFloor].bFrontOpening)
      {
         if( GetDoorState(DOOR_FRONT) == DOOR__OPEN )
         {
            if (gpastCarCalls_F[ucCurrentFloor].bLatched || GetDoorOpenButton ( DOOR_FRONT ) ) {
               gpastCarCalls_F[ucCurrentFloor].bLatched = 0;

               if( Get_ADARequest( ucCurrentFloor, ADA_TYPE__CC ) )
               {
                  Clr_ADARequest( ucCurrentFloor, ADA_TYPE__CC );
               }
            }

//            if( aeLastDoorState[DOOR_FRONT] != DOOR__OPEN )
            if(1)
            {
               if( ( !bNextDirectionUp )
                && ( gpastHallCalls_F[ucCurrentFloor].bLatched_Down )
                && ( eArrivalDir_F != DIR__UP ) ) {
                  gpastHallCalls_F[ucCurrentFloor].bLatched_Down = 0;
                  if( Get_ADARequest( ucCurrentFloor, ADA_TYPE__DN ) )
                  {
                     Clr_ADARequest( ucCurrentFloor, ADA_TYPE__DN );
                  }
               }
               else if( ( bNextDirectionUp )
                     && ( gpastHallCalls_F[ucCurrentFloor].bLatched_Up )
                     && ( eArrivalDir_F != DIR__DN ) ) {
                  gpastHallCalls_F[ucCurrentFloor].bLatched_Up = 0;
                  if( Get_ADARequest( ucCurrentFloor, ADA_TYPE__UP ) )
                  {
                     Clr_ADARequest( ucCurrentFloor, ADA_TYPE__UP );
                  }
               }
            }
         }
         else if( ( !AutoModeRules_GetAutoDoorOpenFlag() ) ||
                  ( getCaptureMode() == CAPTURE_IDLE ) )
         {
            if (gpastCarCalls_F[ucCurrentFloor].bLatched || GetDoorOpenButton ( DOOR_FRONT ) ) {
               gpastCarCalls_F[ucCurrentFloor].bLatched = 0;

               if( Get_ADARequest( ucCurrentFloor, ADA_TYPE__CC ) )
               {
                  Clr_ADARequest( ucCurrentFloor, ADA_TYPE__CC );
               }
            }
         }
      }

      if (GetFP_RearDoors() && gpastFloors[ucCurrentFloor].bRearOpening)
      {
         if( GetDoorState(DOOR_REAR) == DOOR__OPEN )
         {
            if (gpastCarCalls_R[ucCurrentFloor].bLatched || GetDoorOpenButton ( DOOR_REAR ) ) {
               gpastCarCalls_R[ucCurrentFloor].bLatched = 0;

               if( Get_ADARequest( ucCurrentFloor, ADA_TYPE__CC ) )
               {
                  Clr_ADARequest( ucCurrentFloor, ADA_TYPE__CC );
               }
            }

//            if( aeLastDoorState[DOOR_REAR] != DOOR__OPEN )
            if(1)
            {
               if( ( !bNextDirectionUp )
                && ( gpastHallCalls_R[ucCurrentFloor].bLatched_Down )
                && ( eArrivalDir_R != DIR__UP ) ) {
                  gpastHallCalls_R[ucCurrentFloor].bLatched_Down = 0;
                  if( Get_ADARequest( ucCurrentFloor, ADA_TYPE__DN ) )
                  {
                     Clr_ADARequest( ucCurrentFloor, ADA_TYPE__DN );
                  }
               }
               else if( ( bNextDirectionUp )
                     && ( gpastHallCalls_R[ucCurrentFloor].bLatched_Up )
                     && ( eArrivalDir_R != DIR__DN ) ) {
                  gpastHallCalls_R[ucCurrentFloor].bLatched_Up = 0;
                  if( Get_ADARequest( ucCurrentFloor, ADA_TYPE__UP ) )
                  {
                     Clr_ADARequest( ucCurrentFloor, ADA_TYPE__UP );
                  }
               }
            }
         }
         else if( ( !AutoModeRules_GetAutoDoorOpenFlag() ) ||
                  ( getCaptureMode() == CAPTURE_IDLE ) )
         {
            if (gpastCarCalls_R[ucCurrentFloor].bLatched || GetDoorOpenButton ( DOOR_REAR ) ) {
               gpastCarCalls_R[ucCurrentFloor].bLatched = 0;

               if( Get_ADARequest( ucCurrentFloor, ADA_TYPE__CC ) )
               {
                  Clr_ADARequest( ucCurrentFloor, ADA_TYPE__CC );
               }
            }
         }
      }
   }
   aeLastDoorState[DOOR_FRONT] = GetDoorState(DOOR_FRONT);
   aeLastDoorState[DOOR_REAR] = GetDoorState(DOOR_REAR);
}
/*-----------------------------------------------------------------------------

   Used to determine the car's next destination seen above a specified floor.

-----------------------------------------------------------------------------*/
static void GetNextDestinationAbove( uint8_t ucStartFloor, struct st_destination * stDestination)
{
   uint8_t bFullCallFound = 0;
   uint8_t ucTopFloor = GetFP_NumFloors()-1;
   uint8_t bHallCallBypass = Attendant_GetHallCallBypass();
   bHallCallBypass |= ( GetOperation_AutoMode() == MODE_A__VIP_MODE )
                   && ( ( VIP_GetAutoSwingState() == VIP_STATE__ACCEPTING_CC ) || ( VIP_GetAutoSwingState() == VIP_STATE__WAIT_FOR_IDLE ) );
   if( ucStartFloor < GetFP_NumFloors() )
   {
      //scan above for calls
      for (int16_t fl = ucStartFloor; fl <= ucTopFloor; fl++) {
         uint8_t ucFloor = (uint8_t) fl;
         if( ucFloor < GetFP_NumFloors() )
         {
            if( !bHallCallBypass )
            {
               //if hall call,  if floor is set, set direction and exit, else set floor and direction
               if ( ( gpastFloors[ucFloor].bFrontOpening && gpastHallCalls_F[ucFloor].bLatched_Up )
                 || ( GetFP_RearDoors() && gpastFloors[ucFloor].bRearOpening && gpastHallCalls_R[ucFloor].bLatched_Up ) )
               {
                  if (stDestination->ucFloorIndex == INVALID_FLOOR)
                  {
                     stDestination->ucFloorIndex = ucFloor;
                     stDestination->ucDirection = GOING_UP;
                     stDestination->eCallDir = DIR__UP;
                  }
                  else if ( stDestination->ucDirection == GOING_NONE )
                  {
                     stDestination->ucDirection = GOING_UP;
                     stDestination->eNextCallDir = DIR__UP;
                     stDestination->ucNextFloorIndex = ucFloor;
                  }
                  bFullCallFound = 1;
                  break;
               }
            }

            //if car call, and floor is invalid, set floor, if floor is valid, then set direction and exit
            if ( ( gpastFloors[ucFloor].bFrontOpening && gpastCarCalls_F[ucFloor].bLatched )
              || ( GetFP_RearDoors() && gpastFloors[ucFloor].bRearOpening && gpastCarCalls_R[ucFloor].bLatched ) )
            {
               if (stDestination->ucFloorIndex == INVALID_FLOOR)
               {
                  stDestination->ucFloorIndex = ucFloor;
                  stDestination->eCallDir = DIR__NONE;
               }
               else if ( ( stDestination->ucDirection == GOING_NONE )
                      && ( ucFloor != stDestination->ucFloorIndex ) )
               {
                  stDestination->ucDirection = GOING_UP;
                  stDestination->eNextCallDir = DIR__NONE;
                  stDestination->ucNextFloorIndex = ucFloor;
                  bFullCallFound = 1;
                  break;
               }
            }
         }
      }
      //if only a car call is found in the direction, look for anything on the way back up to set direction.
      //if coming back up the floor is the same floor as the car call above, set direction to turn around.
      if( !bHallCallBypass )
      {
         if( ( stDestination->ucFloorIndex == INVALID_FLOOR )
          || ( stDestination->ucDirection == GOING_NONE ) ) {
            for (int16_t fl = ucTopFloor; fl >= ucStartFloor; fl--) {
               uint8_t ucFloor = (uint8_t) fl;
               if( ucFloor < GetFP_NumFloors() )
               {
                  if ( ( gpastFloors[ucFloor].bFrontOpening && gpastHallCalls_F[ucFloor].bLatched_Down ) ||
                       ( GetFP_RearDoors() && gpastFloors[ucFloor].bRearOpening && gpastHallCalls_R[ucFloor].bLatched_Down ) )
                  {
                     /* No destination found in the priority direction, record the first down call encountered */
                     if (stDestination->ucFloorIndex == INVALID_FLOOR)
                     {
                        stDestination->ucFloorIndex = ucFloor;
                        stDestination->ucDirection = GOING_DOWN;
                        stDestination->bDirectionChange_HC = 1;
                        stDestination->eCallDir = DIR__DN;
                     }
                     /* First destination found is a CC, if next found is a HC above the CC, set the arrival direction */
                     else if (ucFloor > stDestination->ucFloorIndex)
                     {
                        stDestination->ucDirection = GOING_UP;
                        stDestination->eNextCallDir = DIR__DN;
                        stDestination->ucNextFloorIndex = ucFloor;
                     }
                     /* First destination found is a CC, if next found is a HC below or equal to the CC, set the arrival direction */
                     else if (ucFloor <= stDestination->ucFloorIndex)
                     {
                        stDestination->ucDirection = GOING_DOWN;
                        stDestination->bDirectionChange_HC = 1;
                        stDestination->eNextCallDir = DIR__DN;
                        stDestination->ucNextFloorIndex = ucFloor;
                     }
                     break;
                  }
               }
            }
         }
      }

      /* Corrects entering top landing with a up priority */
      if( ( stDestination->ucFloorIndex == GetFP_NumFloors()-1 )
       && ( stDestination->ucDirection == GOING_UP ) )
      {
         stDestination->ucDirection = GOING_DOWN;
         stDestination->bDirectionChange_HC = 1;
      }
   }

   //TODO:
}

static void GetNextDestinationBelow( uint8_t ucStartFloor, struct st_destination * stDestination)
{
   uint8_t bFullCallFound = 0;
   uint8_t bHallCallBypass = Attendant_GetHallCallBypass();
   bHallCallBypass |= ( GetOperation_AutoMode() == MODE_A__VIP_MODE )
                   && ( ( VIP_GetAutoSwingState() == VIP_STATE__ACCEPTING_CC ) || ( VIP_GetAutoSwingState() == VIP_STATE__WAIT_FOR_IDLE ) );
   if( ucStartFloor < GetFP_NumFloors() )
   {
      //scan below for calls
      for (int16_t fl = ucStartFloor; fl >= 0; fl--) {
         //if hall call,  if floor is set, set direction and exit, else set floor and direction
         uint8_t ucFloor = (uint8_t) fl;
         if( ucFloor < GetFP_NumFloors() )
         {
            if( !bHallCallBypass )
            {
               if ( ( gpastFloors[ucFloor].bFrontOpening && gpastHallCalls_F[ucFloor].bLatched_Down )
                 || ( GetFP_RearDoors() && gpastFloors[ucFloor].bRearOpening && gpastHallCalls_R[ucFloor].bLatched_Down ) )
               {
                  if (stDestination->ucFloorIndex == INVALID_FLOOR) /* First call found was a CC if next call is a HC, set direction and break. */
                  {
                     stDestination->ucFloorIndex = ucFloor;
                     stDestination->ucDirection = GOING_DOWN;
                     stDestination->eCallDir = DIR__DN;
                  }
                  else if( stDestination->ucDirection == GOING_NONE ) /* First call found was a CC if next call is a HC, set direction and break. */
                  {
                     stDestination->ucDirection = GOING_DOWN;
                     stDestination->eNextCallDir = DIR__DN;
                     stDestination->ucNextFloorIndex = ucFloor;
                  }
                  bFullCallFound = 1;
                  break;
               }
            }

            //if car call, and floor is invalid, set floor, if floor is valid, then set direction and exit
            if ( ( gpastFloors[ucFloor].bFrontOpening && gpastCarCalls_F[ucFloor].bLatched )
              || ( GetFP_RearDoors() && gpastFloors[ucFloor].bRearOpening && gpastCarCalls_R[ucFloor].bLatched ) )
            {
               if (stDestination->ucFloorIndex == INVALID_FLOOR)
               {
                  stDestination->ucFloorIndex = ucFloor;
                  stDestination->eCallDir = DIR__NONE;
               }
               else if( ( stDestination->ucDirection == GOING_NONE )
                     && ( ucFloor != stDestination->ucFloorIndex ) )
               {
                  stDestination->ucDirection = GOING_DOWN;
                  stDestination->eNextCallDir = DIR__NONE;
                  stDestination->ucNextFloorIndex = ucFloor;
                  bFullCallFound = 1;
                  break;
               }
            }
         }
      }
      //if only a car call is found in the direction, look for anything on the way back up to set direction.
      //if coming back up the floor is the same floor as the car call above, set direction to turn around.
      if( !bHallCallBypass )
      {
         if( ( stDestination->ucFloorIndex == INVALID_FLOOR )
          || ( stDestination->ucDirection == GOING_NONE ) ) {
            for (int16_t fl = 0; fl <= ucStartFloor; fl++) {
               uint8_t ucFloor = (uint8_t) fl;
               if( ucFloor < GetFP_NumFloors() )
               {
                  if ( ( gpastFloors[ucFloor].bFrontOpening && gpastHallCalls_F[ucFloor].bLatched_Up )
                    || ( GetFP_RearDoors() && gpastFloors[ucFloor].bRearOpening && gpastHallCalls_R[ucFloor].bLatched_Up ) )
                   {
                     /* No destination found in the priority direction, record the first up call encountered */
                     if (stDestination->ucFloorIndex == INVALID_FLOOR)
                     {
                        stDestination->ucFloorIndex = ucFloor;
                        stDestination->ucDirection = GOING_UP;
                        stDestination->eCallDir = DIR__UP;
                        stDestination->bDirectionChange_HC = 1;
                     }
                     /* First destination found is a CC, if next found is a HC below the CC, set the arrival direction */
                     else if (ucFloor < stDestination->ucFloorIndex)
                     {
                        stDestination->ucDirection = GOING_DOWN;
                        stDestination->ucNextFloorIndex = ucFloor;
                        stDestination->eNextCallDir = DIR__UP;
                     }
                     /* First destination found is a CC, if next found is a HC above or equal to the CC, set the arrival direction */
                     else if (ucFloor >= stDestination->ucFloorIndex)
                     {
                        stDestination->ucDirection = GOING_UP;
                        stDestination->ucNextFloorIndex = ucFloor;
                        stDestination->eNextCallDir = DIR__UP;
                        stDestination->bDirectionChange_HC = 1;
                     }
                     break;
                   }
               }
            }
         }
      }

      /* Corrects entering bottom landing with a down priority */
      if( ( stDestination->ucFloorIndex == 0 )
       && ( stDestination->ucDirection == GOING_DOWN ) )
      {
         stDestination->ucDirection = GOING_UP;
         stDestination->bDirectionChange_HC = 1;
      }
   }
}

void SetDestination(uint8_t ucDestination) {
   //  Prevents repeat request to go to a destination floor when already parked at that destination
   uint8_t bAlreadyAtDestination = ( GetOperation_CurrentFloor() == ucDestination )
                                && ( InsideDeadZone() );
   if( ( ucDestination < GetFP_NumFloors() ) && ( !bAlreadyAtDestination ) )
   {
      if( ( gpastFloors[ucDestination].ulPosition > GetOperation_PositionLimit_DN() )
       && ( gpastFloors[ucDestination].ulPosition < GetOperation_PositionLimit_UP() ) ) {
         uint8_t bInvalidRelevelingDest = ( Motion_GetRelevelingFlag() )
                                       && ( ucDestination != GetOperation_CurrentFloor() );
         if( ( Motion_GetMotionState() != MOTION__START_SEQUENCE )
          && ( !bInvalidRelevelingDest ) )
         {
            SetOperation_RequestedDestination(gpastFloors[ucDestination].ulPosition);
            SetOperation_MotionCmd(MOCMD__GOTO_DEST);
         }
      }
   }
}

void StartReleveling(uint8_t ucFloorIndex) {
   if (!GetMotion_RunFlag() && (ucFloorIndex == GetOperation_CurrentFloor())) {
      bRelevelingFlag = 0;
      SetDestination(ucFloorIndex);
      bRelevelingFlag = 1;
   }
}

void StopNextAvailableLanding(void) {
   if (GetMotion_RunFlag()) {
      bStopNextFloor = 1;
      // Set alarm
      if(Param_ReadValue_1Bit(enPARAM1__EnableStopAtNextAlarm))
      {
         SetAlarm(ALM__STOP_AT_NEXT_FLOOR);
      }
   }
}

/*----------------------------------------------------------------------------
   Returns next reachable floor
 *----------------------------------------------------------------------------*/
static uint8_t GetNextReachableFloor ( void )
{
   return Motion_GetNextReachableFloor();
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static uint8_t GetInSlowdownFlag(void)
{
   uint8_t bInSlowdown = ( Motion_GetMotionState() == MOTION__STOPPED )
                      || ( Motion_GetMotionState() == MOTION__DECELERATING )
                      || ( Motion_GetMotionState() == MOTION__STOP_SEQUENCE );
   return bInSlowdown;
}
/*----------------------------------------------------------------------------
Byte0: DispatchData
   b0:    Any Door Open
   b1-b2: Priority_Plus1
   b3:    Hall Calls Disabled
   b4:    In Slowdown
   b5:    Suppress Reopen
   b6:    Car Out Of Group
   b7:    bIdleDirection
Byte1: Next Reachable Floor
 *----------------------------------------------------------------------------*/
void GetDispatchData( un_sdata_datagram * unDatagram ) {
   uint8_t ucDispatchData = 0;
   uint8_t ucNextReachableFloor = GetNextReachableFloor();

   ucDispatchData = (AnyDoorOpen()) ? 1 : 0; //door open (used to clear calls)

   if (!HallCallsDisabled()) {
      ucDispatchData |= (bPriorityUp + 1) << 1; //direction car is going
   }

   ucDispatchData |= HallCallsDisabled() << 3; //Don't latch Hall Calls flag
   ucDispatchData |= GetInSlowdownFlag() << 4;  // Dispatching flag

   ucDispatchData |= bSuppressReopen << 5;  // Suppress reopen

   ucDispatchData |= CheckIf_CarOutOfGroup() << 6;// Remove car from group

   ucDispatchData |= bIdleDirection << 7;

   unDatagram->auc8[0] = ucDispatchData;
   unDatagram->auc8[1] = ucNextReachableFloor;

}

/*
 * Mid flight stop for if another car has 'won' the bid for the call or call has been dropped.
 * This would be if a car comes available for a call while this car was the fastest before.
 */
void CheckDestination ( void ) {
   uint8_t ucDestinationFloor = GetOperation_DestinationFloor();
   if( ( !Param_ReadValue_1Bit(enPARAM1__DisableDestLossStop) )
    && ( ucDestinationFloor != GetOperation_CurrentFloor() )
    && ( ucDestinationFloor < GetFP_NumFloors() )
    && ( GetOperation_AutoMode() != MODE_A__TEST )
    && ( GetOperation_AutoMode() != MODE_A__SEISMC ) )
   {
      uint8_t latchedCall = 0;
      latchedCall |= gpastCarCalls_F[ucDestinationFloor].bLatched;
      latchedCall |= gpastHallCalls_F[ucDestinationFloor].bLatched_Down;
      latchedCall |= gpastHallCalls_F[ucDestinationFloor].bLatched_Up;
      if (GetFP_RearDoors()) {
         latchedCall |= gpastCarCalls_R[ucDestinationFloor].bLatched;
         latchedCall |= gpastHallCalls_R[ucDestinationFloor].bLatched_Down;
         latchedCall |= gpastHallCalls_R[ucDestinationFloor].bLatched_Up;
      }
      latchedCall |= ( ucDestinationFloor == IdleTime_GetParkingFloor() );
      latchedCall |= ( getCaptureMode() != CAPTURE_OFF );
      if (!latchedCall) {
         StopNextAvailableLanding();
      }
   }
}

/*-----------------------------------------------------------------------------
Returns the id used to assign if we are answering an up, down or neither direction call.
0 = 0 no direction
1 = 1 down
2 = 2 up
-----------------------------------------------------------------------------*/
uint8_t GetArrivalLanterns(void) {

   if (eArrivalDir_F == DIR__UP || eArrivalDir_R == DIR__UP) {
      return 2;
   } else if (eArrivalDir_F == DIR__DN || eArrivalDir_R == DIR__DN) {
      return 1;
   }
   return 0;
}

/*-----------------------------------------------------------------------------
 Returns the destination in the currently serviced direction only
-----------------------------------------------------------------------------*/
uint8_t GetDestination_CurrentDirection(void) {
   return stDestination_Current.ucFloorIndex;
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
#define ARRIVAL_TIME_LIMIT_SEC      (10)
static void UpdateArrivalLanterns ( void )
{
   static uint8_t bDoorHasOpened = 0;
   enum direction_enum eNewArrivalDir = DIR__NONE;
   uint8_t ucDestinationFloor = GetOperation_DestinationFloor();
   if( ( CheckIfValidArrivalMode() )
    && ( ucDestinationFloor < GetFP_NumFloors() ) )
   {
      uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
      uint8_t bNextDirectionUp = bPriorityUp;

      uint8_t ucFloorDifference = ( (ucCurrentFloor < ucDestinationFloor) ?
            (ucDestinationFloor - ucCurrentFloor) :
            (ucCurrentFloor - ucDestinationFloor) );

      uint8_t bInSlowdown = ( ( Motion_GetMotionState() == MOTION__DECELERATING )
                           || ( ( GetMotion_RunFlag() ) && ( ucFloorDifference <= 1 ) ) )
                          ? 1 : 0;

      uint8_t bNewRunStarted = ( ( Motion_GetMotionState() >= MOTION__MANUAL_RUN )
                              && ( Motion_GetMotionState() <= MOTION__CRUISING ) )
                              && ( ucFloorDifference >= 1 );
      //--------------------------------------------
      /* Check time from arrival to trigger arrival lamps early */
      uint8_t ucDecelTime_sec = Motion_GetRemainingDecelTime();
      uint8_t ucArrivalTime_sec = ( Param_ReadValue_8Bit( enPARAM8__ArrivalUpdateTime_sec ) > ARRIVAL_TIME_LIMIT_SEC )
                                ? ( ARRIVAL_TIME_LIMIT_SEC )
                                : ( Param_ReadValue_8Bit( enPARAM8__ArrivalUpdateTime_sec ) );
      uint8_t bEarlyArrival = ( ucDecelTime_sec <= ucArrivalTime_sec ) ? 1:0; // Set to 1 if expected to arrive at floor within enPARAM8__ArrivalUpdateTime_sec

      //--------------------------------------------
      uint8_t bAnyDoorManual = ( GetFP_DoorType(DOOR_FRONT) == DOOR_TYPE__MANUAL ) || ( GetFP_RearDoors() && ( GetFP_DoorType(DOOR_REAR) == DOOR_TYPE__MANUAL ) );
      /* Assert arrival lanterns if doors are opening or early arrival option is set */
      if( ( AnyDoorOpen() )
       || ( bInSlowdown && bEarlyArrival )
       || (InDestinationDoorzone() && (bAnyDoorManual) && (IdleTime_GetParkingTime_Seconds() < 5) ) )
      {
         enum direction_enum eCarCallDirection = GetAnsweredCarCallDirection();
         if( ( !eArrivalDir_F ) && ( gpastFloors[ucDestinationFloor].bFrontOpening ) )
         {
            if( ( !bNextDirectionUp )
             && ( ( gpastHallCalls_F[ucDestinationFloor].bLatched_Down ) || ( aeLastClearedCallType[DOOR_FRONT] == DIR__DN ) ) )
            {
               eArrivalDir_F = DIR__DN;
            }
            else if( ( bNextDirectionUp )
                  && ( ( gpastHallCalls_F[ucDestinationFloor].bLatched_Up ) || ( aeLastClearedCallType[DOOR_FRONT] == DIR__UP ) ) )
            {
               eArrivalDir_F = DIR__UP;
            }
            else if( gpastCarCalls_F[ucDestinationFloor].bLatched && !eArrivalDir_F )
            {
               eArrivalDir_F = eCarCallDirection;
            }
         }
         if( gpastFloors[ucDestinationFloor].bRearOpening )
         {
            if( ( !bNextDirectionUp )
             && ( ( gpastHallCalls_R[ucDestinationFloor].bLatched_Down ) || ( aeLastClearedCallType[DOOR_REAR] == DIR__DN ) ) )
            {
               eArrivalDir_R = DIR__DN;
            }
            else if( ( bNextDirectionUp )
                  && ( ( gpastHallCalls_R[ucDestinationFloor].bLatched_Up ) || ( aeLastClearedCallType[DOOR_REAR] == DIR__UP ) ) )
            {
               eArrivalDir_R = DIR__UP;
            }
            else if( gpastCarCalls_R[ucDestinationFloor].bLatched && !eArrivalDir_R )
            {
               eArrivalDir_R = eCarCallDirection;
            }
         }
      }
      else if( ( CarDoorsClosed() && ( bDoorHasOpened || ( IdleTime_GetParkingTime_Seconds() > 1 ) ) )
            || ( bNewRunStarted && !Motion_GetRelevelingFlag() ) )
      {
         eNewArrivalDir = DIR__NONE;
         eArrivalDir_R  = DIR__NONE;
         eArrivalDir_F  = DIR__NONE;
         bDoorHasOpened = 0;
      }
      else if(bAnyDoorManual && (IdleTime_GetParkingTime_Seconds() > 10))
      {
         eNewArrivalDir = DIR__NONE;
         eArrivalDir_R  = DIR__NONE;
         eArrivalDir_F  = DIR__NONE;
         bDoorHasOpened = 0;
      }
      bDoorHasOpened = AnyDoorOpen();
   }
   else
   {
      eArrivalDir_F = DIR__NONE;
      eArrivalDir_R = DIR__NONE;
      bDoorHasOpened = 0;
   }
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void CheckForStopAtNextFloor(void)
{
   /* Ramp down to next reachable floor if destination is lost
    * midflight and there are no alternate destinations.
    * Prevent midflight ramp down when in attendant mode. */
   if( ( GetMotion_RunFlag() ) && ( GetOperation_AutoMode() != MODE_A__ATTENDANT ) )
   {
      if( ( bStopNextFloor )
       && ( Motion_GetMotionState() != MOTION__DECELERATING )
       && ( Motion_GetMotionState() != MOTION__RSL_DECEL ))
      {
         uint8_t ucReachableFloor = Motion_GetNextReachableFloor();
         SetDestination(ucReachableFloor);
     }
   }
   else
   {
      bStopNextFloor = 0;
   }
}
/*-----------------------------------------------------------------------------
Returns the next destination (in either the up or down direction)

GetDestination_CurrentDirection() only returns the destination in the present direction

-----------------------------------------------------------------------------*/
uint8_t GetNextDestination_AnyDirection(void)
{
   return ucNextDestination;
}
/*-----------------------------------------------------------------------------
   Update structure holding the current car destination
      stDestination_Current

      uint8_t ucFloorIndex; // Current destination opening
   enum direction_enum eCallDir; // Call direction
   enum en_doors eDoor; // Destination opening

   uint8_t ucDirection; // Arrival announcement direction
   uint8_t bDirectionChange_HC; // Flag marks if this destination is a HC in the opposite direction
-----------------------------------------------------------------------------*/
static void UpdateCurrentDestination(void)
{
   stDestination_Current.ucFloorIndex = INVALID_FLOOR;
   stDestination_Current.eCallDir = DIR__NONE;
   stDestination_Current.ucNextFloorIndex = INVALID_FLOOR;
   stDestination_Current.eNextCallDir = DIR__NONE;
   stDestination_Current.ucDirection = GOING_NONE;
   stDestination_Current.bDirectionChange_HC = 0;

   uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
   uint8_t ucDestinationFloor = GetOperation_DestinationFloor();
   uint8_t ucReachableFloor = GetNextReachableFloor();
   enum direction_enum eDir = GetMotion_Direction();
   uint8_t bUpDirection = bPriorityUp;

   //If i am in motion
   if( eDir != DIR__NONE )
   {
        //if destination floor is closer than the 'reachable' floor reassign reachable floor to destination
      if ( ((GetMotion_Direction() == DIR__UP) && (ucDestinationFloor < ucReachableFloor)) ||
           ((GetMotion_Direction() == DIR__DN) && (ucDestinationFloor > ucReachableFloor)) )
      {
          ucReachableFloor =  ucDestinationFloor;
      }

      if( eDir == DIR__UP ) // In motion going up
      {
         GetNextDestinationAbove(ucReachableFloor, &stDestination_Current);
      }
      else // In motion going down
      {
         GetNextDestinationBelow(ucReachableFloor, &stDestination_Current);
      }
   }
   else if (bUpDirection) // Not in motion and looking up
   {
      GetNextDestinationAbove(ucCurrentFloor, &stDestination_Current);
   }
   else // Not in motion and looking down
   {
      GetNextDestinationBelow(ucCurrentFloor, &stDestination_Current);
   }

   /* TODO review Suppress the output when the destination is the current floor but requires a direction change, and car is not idle */
   uint8_t bDirectionChangeRequired = ( ( stDestination_Current.eCallDir == DIR__UP ) && !bPriorityUp )
                                   || ( ( stDestination_Current.eCallDir == DIR__DN ) &&  bPriorityUp );
   if( ( !GetMotion_RunFlag() )
    && ( stDestination_Current.ucFloorIndex == ucCurrentFloor )
    && ( bDirectionChangeRequired )
//    && ( !bIdleDirection )
    )
   {
      stDestination_Current.ucFloorIndex = INVALID_FLOOR;
      stDestination_Current.eCallDir = DIR__NONE;
      stDestination_Current.ucNextFloorIndex = INVALID_FLOOR;
      stDestination_Current.eNextCallDir = DIR__NONE;
      stDestination_Current.ucDirection = GOING_NONE;
      stDestination_Current.bDirectionChange_HC = 0;
   }
}
/*-----------------------------------------------------------------------------
   Updates structures with car's next destination, suppressing the current floor's destinations
-----------------------------------------------------------------------------*/
static void UpdateNextDestinations(void)
{
   stDestination_Next_SameDir.ucFloorIndex = INVALID_FLOOR;
   stDestination_Next_SameDir.eCallDir = DIR__NONE;
   stDestination_Next_SameDir.ucNextFloorIndex = INVALID_FLOOR;
   stDestination_Next_SameDir.eNextCallDir = DIR__NONE;
   stDestination_Next_SameDir.ucDirection = GOING_NONE;
   stDestination_Next_SameDir.bDirectionChange_HC = 0;

   stDestination_Next_DiffDir.ucFloorIndex = INVALID_FLOOR;
   stDestination_Next_DiffDir.eCallDir = DIR__NONE;
   stDestination_Next_DiffDir.ucNextFloorIndex = INVALID_FLOOR;
   stDestination_Next_DiffDir.eNextCallDir = DIR__NONE;
   stDestination_Next_DiffDir.ucDirection = GOING_NONE;
   stDestination_Next_DiffDir.bDirectionChange_HC = 0;

   uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
   uint8_t ucDestinationFloor = GetOperation_DestinationFloor();
   uint8_t ucReachableFloor = GetNextReachableFloor();
   enum direction_enum eDir = GetMotion_Direction();
   uint8_t bUpDirection = bPriorityUp;
   uint8_t bNextUpDirection = bNextPriorityUp;

   uint8_t bSaved_CCF;
   uint8_t bSaved_HCF;
   uint8_t bSaved_CCR;
   uint8_t bSaved_HCR;
   if( stDestination_Current.ucFloorIndex < GetFP_NumFloors() )
   {
      /* Temporarily suppress calls at the current destination to solve for the next destination */
      bSaved_CCF = gpastCarCalls_F[stDestination_Current.ucFloorIndex].bLatched;
      gpastCarCalls_F[stDestination_Current.ucFloorIndex].bLatched = 0;
      if( ( stDestination_Current.ucFloorIndex != ( GetFP_NumFloors()-1 ) ) &&
          ( ( bUpDirection ) || ( !stDestination_Current.ucFloorIndex ) ) ) {
         bSaved_HCF = gpastHallCalls_F[stDestination_Current.ucFloorIndex].bLatched_Up;
         gpastHallCalls_F[stDestination_Current.ucFloorIndex].bLatched_Up = 0;
      }
      else {
         bSaved_HCF = gpastHallCalls_F[stDestination_Current.ucFloorIndex].bLatched_Down;
         gpastHallCalls_F[stDestination_Current.ucFloorIndex].bLatched_Down = 0;
      }
      if( GetFP_RearDoors() ) {
         bSaved_CCR = gpastCarCalls_R[stDestination_Current.ucFloorIndex].bLatched;
         gpastCarCalls_R[stDestination_Current.ucFloorIndex].bLatched = 0;
         if( ( stDestination_Current.ucFloorIndex != ( GetFP_NumFloors()-1 ) ) &&
             ( ( bUpDirection ) || ( !stDestination_Current.ucFloorIndex ) ) ) {
            bSaved_HCR = gpastHallCalls_R[stDestination_Current.ucFloorIndex].bLatched_Up;
            gpastHallCalls_R[stDestination_Current.ucFloorIndex].bLatched_Up = 0;
         }
         else {
            bSaved_HCR = gpastHallCalls_R[stDestination_Current.ucFloorIndex].bLatched_Down;
            gpastHallCalls_R[stDestination_Current.ucFloorIndex].bLatched_Down = 0;
         }
      }
   }

   //If i am in motion
   if( eDir != DIR__NONE )
   {
        //if destination floor is closer than the 'reachable' floor reassign reachable floor to destination
      if ( ((GetMotion_Direction() == DIR__UP) && (ucDestinationFloor < ucReachableFloor)) ||
           ((GetMotion_Direction() == DIR__DN) && (ucDestinationFloor > ucReachableFloor)) )
      {
          ucReachableFloor =  ucDestinationFloor;
      }

      if( eDir == DIR__UP ) // In motion going up
      {
         GetNextDestinationAbove(ucReachableFloor, &stDestination_Next_SameDir);
         GetNextDestinationBelow(ucReachableFloor, &stDestination_Next_DiffDir);
      }
      else // In motion going down
      {
         GetNextDestinationBelow(ucReachableFloor, &stDestination_Next_SameDir);
         GetNextDestinationAbove(ucReachableFloor, &stDestination_Next_DiffDir);
      }
   }
   else if (bUpDirection) // Not in motion and looking up
   {
      GetNextDestinationAbove(ucCurrentFloor, &stDestination_Next_SameDir);
      GetNextDestinationBelow(ucReachableFloor, &stDestination_Next_DiffDir);
   }
   else // Not in motion and looking down
   {
      GetNextDestinationBelow(ucCurrentFloor, &stDestination_Next_SameDir);
      GetNextDestinationAbove(ucReachableFloor, &stDestination_Next_DiffDir);
   }

   if( stDestination_Current.ucFloorIndex < GetFP_NumFloors() )
   {
      /* Restore calls for current destination */
      gpastCarCalls_F[stDestination_Current.ucFloorIndex].bLatched = bSaved_CCF;
      if( ( stDestination_Current.ucFloorIndex != ( GetFP_NumFloors()-1 ) ) &&
          ( ( bUpDirection ) || ( !stDestination_Current.ucFloorIndex ) ) ) {
         gpastHallCalls_F[stDestination_Current.ucFloorIndex].bLatched_Up = bSaved_HCF;
      }
      else {
         gpastHallCalls_F[stDestination_Current.ucFloorIndex].bLatched_Down = bSaved_HCF;
      }
      if( GetFP_RearDoors() ) {
         gpastCarCalls_R[stDestination_Current.ucFloorIndex].bLatched = bSaved_CCR;
         if( ( stDestination_Current.ucFloorIndex != ( GetFP_NumFloors()-1 ) ) &&
             ( ( bUpDirection ) || ( !stDestination_Current.ucFloorIndex ) ) ) {
            gpastHallCalls_R[stDestination_Current.ucFloorIndex].bLatched_Up = bSaved_HCR;
         }
         else {
            gpastHallCalls_R[stDestination_Current.ucFloorIndex].bLatched_Down = bSaved_HCR;
         }
      }
   }
}
/*-----------------------------------------------------------------------------
   Special handling for attendant mode arrival lantern and direction priority control
-----------------------------------------------------------------------------*/
static void Attendant_UpdateArrivalAndDirection(void)
{
   static enum direction_enum eLastAttendantDir;
   static enum direction_enum eLastDirCommand;
   if( GetOperation_AutoMode() == MODE_A__ATTENDANT )
   {
      bIdleDirection = 0;

      enum direction_enum eDir = Attendant_GetDirection();
      if( eDir == DIR__UP )
      {
         bPriorityUp = HC_DIR__UP;
         bNextPriorityUp = bPriorityUp;
         /* Clear any existing arrival lanterns on direction change */
         if( eLastAttendantDir != eDir )
         {
            aeLastClearedCallType[DOOR_FRONT] = DIR__NONE;
            aeLastClearedCallType[DOOR_REAR] = DIR__NONE;
            eArrivalDir_R  = DIR__NONE;
            eArrivalDir_F  = DIR__NONE;
         }
         eLastDirCommand = eDir;
      }
      else if( eDir == DIR__DN )
      {
         bPriorityUp = HC_DIR__DOWN;
         bNextPriorityUp = bPriorityUp;
         /* Clear any existing arrival lanterns on direction change */
         if( eLastAttendantDir != eDir )
         {
            aeLastClearedCallType[DOOR_FRONT] = DIR__NONE;
            aeLastClearedCallType[DOOR_REAR] = DIR__NONE;
            eArrivalDir_R  = DIR__NONE;
            eArrivalDir_F  = DIR__NONE;
         }
         eLastDirCommand = eDir;
      }
      else if( eLastDirCommand == DIR__UP )
      {
         bPriorityUp = HC_DIR__UP;
         bNextPriorityUp = bPriorityUp;
      }
      else if( eLastDirCommand == DIR__DN )
      {
         bPriorityUp = HC_DIR__DOWN;
         bNextPriorityUp = bPriorityUp;
      }

      eLastAttendantDir = eDir;
   }
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void UpdateDirectionPriority(void)
{
   static uint8_t bLastRunFlag;
   static uint8_t ucIdleTime_CC_500ms = 0;
   uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
   uint8_t ucDestinationFloor = GetOperation_DestinationFloor();
   uint8_t bCarDoorsClosed = CarDoorsClosed();
   uint16_t uwIdleTime_s = IdleTime_GetDirectionChangeTime_Seconds();
   // uint8_t ucIdleTime_CC_500ms = Param_ReadValue_8Bit(enPARAM8__IdleCC_500ms);
   static uint16_t uwCCDirectionChangeCounter_50ms;

   /* Clear last answered call variable on start of run */
   if( !bLastRunFlag && GetMotion_RunFlag() )
   {
      aeLastClearedCallType[DOOR_FRONT] = DIR__NONE;
      aeLastClearedCallType[DOOR_REAR] = DIR__NONE;
      uwCCDirectionChangeCounter_50ms = 0;
   }

   /* Also update next destination variable used to determine if the car has demand */
   if( stDestination_Current.ucFloorIndex != INVALID_FLOOR )
   {
      ucNextDestination = stDestination_Current.ucFloorIndex;
   }
   else if( stDestination_Next_SameDir.ucFloorIndex != INVALID_FLOOR )
   {
      ucNextDestination = stDestination_Next_SameDir.ucFloorIndex;
   }
   else if( stDestination_Next_DiffDir.ucFloorIndex != INVALID_FLOOR )
   {
      ucNextDestination = stDestination_Next_DiffDir.ucFloorIndex;
   }
   else
   {
      ucNextDestination = INVALID_FLOOR;
   }

   /* This prevents a delay to door opening in response to
    * HCs in the event of car direction change. The car direction
    * is otherwise only set in UpdateDirectionPriority().
    * Also allows immediate direction update if responding to a CC */
   if( ( GetMotion_RunFlag() ) &&
       ( stDestination_Current.ucFloorIndex == ucDestinationFloor ) &&
       ( ucCurrentFloor == ucDestinationFloor ) &&
       ( bCarDoorsClosed ) &&
       ( stDestination_Current.bDirectionChange_HC ) )
   {
      /* Update immediately if there is a HC direction change at this floor */
      if (stDestination_Current.ucDirection == GOING_UP) {
         bNextPriorityUp = HC_DIR__UP;
         Attendant_SetDirection(DIR__UP);
      } else if (stDestination_Current.ucDirection == GOING_DOWN) {
         bNextPriorityUp = HC_DIR__DOWN;
         Attendant_SetDirection(DIR__DN);
      }
      if( bPriorityUp != bNextPriorityUp )
      {
         bPriorityUp = bNextPriorityUp;
      }
   }
   /* Bypass direction change delay if the last call was a car call ( or arriving at a floor with CC ) and there is no valid next destination in the current direction. */
   else if( ( stDestination_Current.eCallDir == DIR__NONE )
         && ( stDestination_Current.ucFloorIndex == INVALID_FLOOR )
         && ( ucCurrentFloor == ucDestinationFloor )
         && ( !aeLastClearedCallType[DOOR_FRONT] && !aeLastClearedCallType[DOOR_REAR] )
         && ( stDestination_Next_SameDir.ucFloorIndex == INVALID_FLOOR )
         && ( stDestination_Next_DiffDir.ucFloorIndex != INVALID_FLOOR )
         && ( !bCarDoorsClosed || !GetMotion_RunFlag() ) ) // Otherwise counter starts as soon as car PI changes to destination PI
   {
      /* This allows time for the group dispatcher's hall call assignment to be sent to the
       * car before performing a direction change to a car call in reverse direction. */
      uint16_t uwLimit_50ms = Param_ReadValue_8Bit(enPARAM8__CCDirectionChange_50ms);
      if( ( uwCCDirectionChangeCounter_50ms >= uwLimit_50ms )
       || ( HallCallsDisabled() ) )
      {
         if( stDestination_Next_DiffDir.ucFloorIndex > ucDestinationFloor ) {
            bNextPriorityUp = HC_DIR__UP;
         } else if( stDestination_Next_DiffDir.ucFloorIndex < ucDestinationFloor ) {
            bNextPriorityUp = HC_DIR__DOWN;
         }
         if( bPriorityUp != bNextPriorityUp )
         {
            bPriorityUp = bNextPriorityUp;
         }
      }
      else
      {
         uwCCDirectionChangeCounter_50ms++;
      }
   }
   else if( ( !GetMotion_RunFlag() ) &&
            ( bCarDoorsClosed || ( IdleTime_GetParkingDoorCommand() || Doors_GetNoDemandDoorsOpen() ) ) )
   {
      uint16_t uwMinimumIdleTime_s = Param_ReadValue_8Bit(enPARAM8__DirChangeDelay_1s);
      if( HallCallsDisabled() ) /* Bypass idle time requirement if not serving hall calls */
      {
         uwMinimumIdleTime_s = 0;
      }
      else if ( uwMinimumIdleTime_s > 30 )
      {
         uwMinimumIdleTime_s = 30;
      }

      /* If there is no valid destination with the current direction priority, and there is in the opposite direction,
       * wait an adjustable time for a call to be latched, then flip priority */
      if( uwIdleTime_s >= uwMinimumIdleTime_s )
      {
         if( ( stDestination_Current.ucFloorIndex == INVALID_FLOOR ) &&
             ( stDestination_Next_DiffDir.ucFloorIndex != INVALID_FLOOR ) )
         {
            bNextPriorityUp = !bPriorityUp;
         }
         else if( ( stDestination_Current.ucFloorIndex == ucCurrentFloor ) && ( stDestination_Current.bDirectionChange_HC ) )
         {
            /* If hall call at current landing is in the
             * opposite direction, stDestination_Current
             * will return a valid floor. Compensate for
             * this case. */
            if( ( ( stDestination_Current.ucDirection == GOING_UP )   && ( !bPriorityUp ) ) ||
                ( ( stDestination_Current.ucDirection == GOING_DOWN ) && (  bPriorityUp ) ) )
            {
               bNextPriorityUp = !bPriorityUp;
            }
         }

         bIdleDirection = 1;
         aeLastClearedCallType[DOOR_FRONT] = DIR__NONE;
         aeLastClearedCallType[DOOR_REAR] = DIR__NONE;
         if(bPriorityUp != bNextPriorityUp)
         {
            bPriorityUp = bNextPriorityUp;
         }
      }
   }
   else
   {
      int a = 0;
   }

   //todo review
   /* If car is stopped,
    * If its last answered call was not a HC,
    * If it has no current destination or its current destination is the current floor,
    * If there is no next destination in the current direction,
    *    Then the car is idle and permitted to change directions. */
   if( ( !GetMotion_RunFlag() )
    && ( !aeLastClearedCallType[DOOR_FRONT] && !aeLastClearedCallType[DOOR_REAR] )
    && ( ( stDestination_Current.ucFloorIndex == ucCurrentFloor ) || ( stDestination_Current.ucFloorIndex == INVALID_FLOOR ) )
    && ( stDestination_Next_SameDir.ucFloorIndex == INVALID_FLOOR ) )
   {
      /* Signal to group dispatcher that the car is able to switch directions. */
      bIdleDirection = 1;
   }

   Attendant_UpdateArrivalAndDirection();

   /* Exception for terminal floors */
   if( !GetOperation_CurrentFloor() )
   {
      bPriorityUp = HC_DIR__UP;
      bNextPriorityUp = bPriorityUp;
   }
   else if( GetOperation_CurrentFloor() == ( GetFP_NumFloors()-1 ) )
   {
      bPriorityUp = HC_DIR__DOWN;
      bNextPriorityUp = bPriorityUp;
   }

   bLastRunFlag = GetMotion_RunFlag();
}
/*-----------------------------------------------------------------------------
   Updates the signal used to suppress reopening at
   a floor when preparing to take off
-----------------------------------------------------------------------------*/
static void UpdateSuppressReopenSignal(void)
{
   static uint8_t ucLastDestFloor;
   if( ( GetDoorState(DOOR_FRONT) == DOOR__OPEN ) || ( GetDoorState(DOOR_REAR) == DOOR__OPEN ) )
   {
      ucLastOpenedFloor_Plus1 = GetOperation_CurrentFloor()+1;
   }

   if( ( GetMotion_RunFlag() )
    && ( ucLastDestFloor != GetOperation_DestinationFloor() ) )
   {
      ucLastOpenedFloor_Plus1 = 0;
      ucLastDestFloor = GetOperation_DestinationFloor();
   }

   /* Suppress open request if GSW is made, doors were last opened on this floor, and there is demand */
   bSuppressReopen = ( Param_ReadValue_1Bit(enPARAM1__SuppressReopenOnGSW) )
                  && ( GetGSW(DOOR_FRONT) && ( !GetFP_RearDoors() || GetGSW(DOOR_REAR) ) )
                  && ( ( ucLastOpenedFloor_Plus1-1 ) == GetOperation_CurrentFloor() )
                  && ( ( ( stDestination_Next_SameDir.ucFloorIndex != INVALID_FLOOR ) && ( stDestination_Next_SameDir.ucFloorIndex != GetOperation_CurrentFloor() ) )
                    || ( ( stDestination_Next_DiffDir.ucFloorIndex != INVALID_FLOOR ) && ( stDestination_Next_DiffDir.ucFloorIndex != GetOperation_CurrentFloor() ) )
                    || ( ( stDestination_Current.ucFloorIndex != INVALID_FLOOR )
                      && ( stDestination_Current.ucFloorIndex != GetOperation_CurrentFloor() ) ) );
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
uint8_t HallCallsDisabled(void) {
   if (( GetOperation_ClassOfOp() == CLASSOP__AUTO ) &&  //Auto Operation
       ( getCaptureMode() == CAPTURE_OFF )               //not in capture mode
      )
   {
      return gstCurrentModeRules[GetOperation_AutoMode()].bIgnoreHallCall;
   }
   /* Added exception for capture mode disabling hall calls when in swing */
   else if( GetOperation_AutoMode() == MODE_A__SWING )
   {
      return gstCurrentModeRules[GetOperation_AutoMode()].bIgnoreHallCall;
   }
   else if( GetOperation_AutoMode() == MODE_A__VIP_MODE )
   {
      return gstCurrentModeRules[GetOperation_AutoMode()].bIgnoreHallCall;
   }
   else
   {
      return 1;
   }
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
uint8_t GetMidflightDestination(void) {
   return GetDestination_CurrentDirection();
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
uint8_t GetDistpatchDirectionPriority(void) {
   return bNextPriorityUp;
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
void ClearLatchedHallCalls(void) {
   uint8_t ucFloor;

   for (ucFloor = 0; ucFloor < GetFP_NumFloors(); ucFloor++) {
      gpastHallCalls_F[ucFloor].bLatched_Up = 0;
      gpastHallCalls_F[ucFloor].bLatched_Down = 0;

      if (GetFP_RearDoors()) {
         gpastHallCalls_R[ucFloor].bLatched_Up = 0;
         gpastHallCalls_R[ucFloor].bLatched_Down = 0;
      }
   }
}

void ClearLatchedHallCalls_Flood(void)
{
   uint8_t ucFloor;
   uint8_t uFloodFloor = Param_ReadValue_8Bit(enPARAM8__FLOOD_NumFloors_Plus1);

   for (ucFloor = 0; ucFloor < uFloodFloor; ucFloor++)
   {
      gpastHallCalls_F[ucFloor].bLatched_Up = 0;
      gpastHallCalls_F[ucFloor].bLatched_Down = 0;

      if (GetFP_RearDoors()) {
         gpastHallCalls_R[ucFloor].bLatched_Up = 0;
         gpastHallCalls_R[ucFloor].bLatched_Down = 0;
      }
   }
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
void ClearLatchedCarCalls(void) {
   uint8_t ucFloor;

   for (ucFloor = 0; ucFloor < GetFP_NumFloors(); ucFloor++) {
     gpastCarCalls_F[ucFloor].bLatched = 0;

     if (GetFP_RearDoors()) {
       gpastCarCalls_R[ucFloor].bLatched = 0;
     }
   }
}

void ClearLatchedCarCalls_Flood(void)
{
   uint8_t ucFloor;
   uint8_t uFloodFloor = Param_ReadValue_8Bit(enPARAM8__FLOOD_NumFloors_Plus1);

   for (ucFloor = 0; ucFloor < uFloodFloor; ucFloor++)
   {
      gpastCarCalls_F[ucFloor].bLatched = 0;

      if (GetFP_RearDoors()) {
         gpastCarCalls_R[ucFloor].bLatched = 0;
      }
   }
}


/*-----------------------------------------------------------------------------

Initialize anything required prior to running for the first time

-----------------------------------------------------------------------------*/
static uint32_t Init(struct st_module * pstThisModule) {
   pstThisModule->uwInitialDelay_1ms = 500;
   pstThisModule->uwRunPeriod_1ms = 100;

   for (uint8_t ucMode = 0; ucMode < NUM_MODE_AUTO; ucMode++) {
      gstCurrentModeRules[ucMode].bIgnoreCarCallSecurity = 0;
      gstCurrentModeRules[ucMode].bIgnoreHallCallSecurity = 0;
      gstCurrentModeRules[ucMode].bAllowedOutsideDoorZone = 0;
      gstCurrentModeRules[ucMode].bParkingEnabled = 0;
      gstCurrentModeRules[ucMode].bIgnoreCarCall_F = 1;
      gstCurrentModeRules[ucMode].bIgnoreCarCall_R = 1;
      gstCurrentModeRules[ucMode].bIgnoreHallCall = 1;
      gstCurrentModeRules[ucMode].bAutoDoorOpen = 0;
      gstCurrentModeRules[ucMode].bDoorHold = 0;
      gstCurrentModeRules[ucMode].bIgnoreDCB = 0;
      gstCurrentModeRules[ucMode].bForceDoorsOpenOrClosed = 0;
   }



   /* Automatic Operations */
   gstCurrentModeRules[MODE_A__NORMAL].bParkingEnabled = 1;
   gstCurrentModeRules[MODE_A__NORMAL].bIgnoreHallCall = 0;
   gstCurrentModeRules[MODE_A__NORMAL].bAutoDoorOpen = 1;
   gstCurrentModeRules[MODE_A__NORMAL].bIgnoreCarCall_F = 0;
   gstCurrentModeRules[MODE_A__NORMAL].bIgnoreCarCall_R = 0;
   gstCurrentModeRules[MODE_A__NORMAL].bDoorHold = Param_ReadValue_1Bit(enPARAM1__Enable_Freight_Doors) &&
      !Param_ReadValue_1Bit(enPARAM1__Enable_Freight_AutoClose);


   /* Sabbath Operation */
   gstCurrentModeRules[MODE_A__SABBATH].bIgnoreCarCall_F = 1;
   gstCurrentModeRules[MODE_A__SABBATH].bIgnoreCarCall_R = 1;
   gstCurrentModeRules[MODE_A__SABBATH].bAutoDoorOpen = 1;
   gstCurrentModeRules[MODE_A__SABBATH].bIgnoreDCB = 1;

   /* Attendant */
   gstCurrentModeRules[MODE_A__ATTENDANT].bIgnoreHallCall = 0;
   gstCurrentModeRules[MODE_A__ATTENDANT].bAutoDoorOpen = 1;
   gstCurrentModeRules[MODE_A__ATTENDANT].bIgnoreCarCall_F = 0;
   gstCurrentModeRules[MODE_A__ATTENDANT].bIgnoreCarCall_R = 0;
   gstCurrentModeRules[MODE_A__ATTENDANT].bDoorHold = 1;

   /* IND Srv */
   gstCurrentModeRules[MODE_A__INDP_SRV].bIgnoreCarCall_F = 0;
   gstCurrentModeRules[MODE_A__INDP_SRV].bIgnoreCarCall_R = 0;
   gstCurrentModeRules[MODE_A__INDP_SRV].bAutoDoorOpen = 1;
   gstCurrentModeRules[MODE_A__INDP_SRV].bDoorHold = 1;

   /*
    *    Medical rules.
    */
   gstCurrentModeRules[MODE_A__EMS2].bIgnoreCarCallSecurity = 1;
   gstCurrentModeRules[MODE_A__EMS2].bAutoDoorOpen = 1;
   gstCurrentModeRules[MODE_A__EMS2].bIgnoreCarCall_F = 0;
   gstCurrentModeRules[MODE_A__EMS2].bIgnoreCarCall_R = 0;
   gstCurrentModeRules[MODE_A__EMS2].bIgnoreHallCall = 1;
   gstCurrentModeRules[MODE_A__EMS2].bForceDoorsOpenOrClosed = 0;
   gstCurrentModeRules[MODE_A__EMS2].bDoorHold = 1;

   gstCurrentModeRules[MODE_A__EMS1].bIgnoreCarCallSecurity = 1;
   gstCurrentModeRules[MODE_A__EMS1].bAutoDoorOpen = 1;
   gstCurrentModeRules[MODE_A__EMS1].bIgnoreCarCall_F = 1;
   gstCurrentModeRules[MODE_A__EMS1].bIgnoreCarCall_R = 1;
   gstCurrentModeRules[MODE_A__EMS1].bIgnoreHallCall = 0;
   gstCurrentModeRules[MODE_A__EMS1].bForceDoorsOpenOrClosed = 0;
   gstCurrentModeRules[MODE_A__EMS1].bDoorHold = 1;
   gstCurrentModeRules[MODE_A__EMS1].bIgnoreDCB = 1;

   /*
    *    Fire Service rules.
    */
   gstCurrentModeRules[MODE_A__FIRE2].bIgnoreCarCallSecurity = 1;
   gstCurrentModeRules[MODE_A__FIRE2].bAllowedOutsideDoorZone = 1;
   gstCurrentModeRules[MODE_A__FIRE2].bIgnoreCarCall_F = 0;
   gstCurrentModeRules[MODE_A__FIRE2].bIgnoreCarCall_R = 0;
   gstCurrentModeRules[MODE_A__FIRE2].bIgnoreHallCall = 1;
   gstCurrentModeRules[MODE_A__FIRE2].bForceDoorsOpenOrClosed = 1;
   gstCurrentModeRules[MODE_A__FIRE2].bDoorHold = 1;

   /* Test/Acceptance Mode */
   gstCurrentModeRules[MODE_A__TEST].bAllowedOutsideDoorZone = 1;

   /* Earthquake CW Drail*/
   gstCurrentModeRules[MODE_A__CW_DRAIL].bAllowedOutsideDoorZone = 1;

   /* Emergency Power */
   gstCurrentModeRules[MODE_A__EPOWER].bIgnoreCarCall_F = 0;
   gstCurrentModeRules[MODE_A__EPOWER].bIgnoreCarCall_R = 0;
   gstCurrentModeRules[MODE_A__EPOWER].bIgnoreHallCall = 0;
   gstCurrentModeRules[MODE_A__EPOWER].bAutoDoorOpen = 1;

   /* Swing */
   gstCurrentModeRules[MODE_A__SWING].bIgnoreCarCall_F = 0;
   gstCurrentModeRules[MODE_A__SWING].bIgnoreCarCall_R = 0;
   gstCurrentModeRules[MODE_A__SWING].bIgnoreHallCall = 0;
   gstCurrentModeRules[MODE_A__SWING].bAutoDoorOpen = 1;

   /* Battery Rescue */
   gstCurrentModeRules[MODE_A__BATT_RESQ].bIgnoreDCB = 1;

   /* Flood Operation */
   gstCurrentModeRules[MODE_A__FLOOD_OP].bIgnoreCarCall_F = 0;
   gstCurrentModeRules[MODE_A__FLOOD_OP].bIgnoreCarCall_R = 0;
   gstCurrentModeRules[MODE_A__FLOOD_OP].bIgnoreHallCall = 0;
   gstCurrentModeRules[MODE_A__FLOOD_OP].bAllowedOutsideDoorZone = 0;
   gstCurrentModeRules[MODE_A__FLOOD_OP].bAutoDoorOpen = 1;

   /* Wander guard operations */
   gstCurrentModeRules[MODE_A__WANDERGUARD].bParkingEnabled = 1;
   gstCurrentModeRules[MODE_A__WANDERGUARD].bIgnoreHallCall = 0;
   gstCurrentModeRules[MODE_A__WANDERGUARD].bAutoDoorOpen = 1;
   gstCurrentModeRules[MODE_A__WANDERGUARD].bIgnoreCarCall_F = 0;
   gstCurrentModeRules[MODE_A__WANDERGUARD].bIgnoreCarCall_R = 0;
   gstCurrentModeRules[MODE_A__WANDERGUARD].bDoorHold = Param_ReadValue_1Bit(enPARAM1__Enable_Freight_Doors) &&
                                                       !Param_ReadValue_1Bit(enPARAM1__Enable_Freight_AutoClose);

    /* Active Shooter operations */
    gstCurrentModeRules[MODE_A__ACTIVE_SHOOTER_MODE].bIgnoreCarCall_F = 1;
    gstCurrentModeRules[MODE_A__ACTIVE_SHOOTER_MODE].bIgnoreCarCall_R = 1;
    gstCurrentModeRules[MODE_A__ACTIVE_SHOOTER_MODE].bIgnoreHallCall = 1;
    gstCurrentModeRules[MODE_A__ACTIVE_SHOOTER_MODE].bAutoDoorOpen = 0;

    /* Marshal Mode */
    gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_F = 1;
    gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_R = 1;
    gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreDCB = 1;
    gstCurrentModeRules[MODE_A__MARSHAL_MODE].bForceDoorsOpenOrClosed = 1;

    /* VIP Mode */
    gstCurrentModeRules[MODE_A__VIP_MODE].bIgnoreCarCall_F = 0;
    gstCurrentModeRules[MODE_A__VIP_MODE].bIgnoreCarCall_R = 0;
    gstCurrentModeRules[MODE_A__VIP_MODE].bIgnoreHallCall = 0;
    gstCurrentModeRules[MODE_A__VIP_MODE].bIgnoreCarCallSecurity = 1;
    gstCurrentModeRules[MODE_A__VIP_MODE].bIgnoreHallCallSecurity = 1;
    gstCurrentModeRules[MODE_A__VIP_MODE].bAutoDoorOpen = 1;

   return 0;
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static uint32_t Run(struct st_module * pstThisModule) {
   static uint8_t bLastRunFlag = 0;
   bIdleDirection = 0;

   //only run these function in auto operation modes
   if (GetOperation_ClassOfOp() == CLASSOP__AUTO)
   {
      if( gbBackupSync )
      {
         ClearLatchedCarCalls();
         ClearLatchedHallCalls();
      }

     CheckForClearingCarCallCommand();

     UpdateCurrentDestination();
     UpdateNextDestinations();

     UpdateDirectionPriority();

     CheckForStopAtNextFloor();

     UpdateSuppressReopenSignal();

     if (bLastRunFlag == 1 && GetMotion_RunFlag() == 0)
     {
        bRelevelingFlag = 0;
     }
   }
   else
   {
      ucLastOpenedFloor_Plus1 = 0;
      bSuppressReopen = 0;
   }

   UpdateArrivalLanterns();

   bLastRunFlag = GetMotion_RunFlag();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
*----------------------------------------------------------------------------*/
