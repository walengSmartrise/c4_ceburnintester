/******************************************************************************
 * @author   Keith Soneda
 * @version  V1.00
 * @date     16, Sept 2016
 *
 * @note     Latched fault & fault log operations
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "sys.h"
#include "fram.h"
#include "ring_buffer.h"
#include <stdint.h>
#include <string.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint32_t uiLocalEmergencyBitmap;
static uint32_t uiSetEmergencyBitmap;
static uint32_t uiClrEmergencyBitmap;

static uint8_t bEmergencyBitmapReady;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   Returns 1 if emergency bitmap is ready
 *----------------------------------------------------------------------------*/
uint8_t FRAM_EmergencyBitmapReady()
{
   return bEmergencyBitmapReady;
}
/*----------------------------------------------------------------------------
Emergency bitmaps
 *----------------------------------------------------------------------------*/
void FRAM_WriteEmergencyBitmap(uint32_t uiBitmap)
{
   FRAM_WriteArray_U32(&uiBitmap, 1, FRAM_TADDR_EMERGENCYBITMAP_START);
}
uint32_t FRAM_ReadEmergencyBitmap()
{
   uint32_t uiBitmap;
   FRAM_ReadArray_U32(&uiBitmap, 1, FRAM_TADDR_EMERGENCYBITMAP_START);
   return uiBitmap;
}

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/

void SetEmergencyBit(enum en_emergency_bitmap enEmergencyBF)
{
   if(enEmergencyBF < Num_EmergenyBF)
   {
      uiSetEmergencyBitmap |= (1 << enEmergencyBF);
      uiClrEmergencyBitmap &= ~(1 << enEmergencyBF);
   }
}
void ClrEmergencyBit(enum en_emergency_bitmap enEmergencyBF)
{
   if(enEmergencyBF < Num_EmergenyBF)
   {
      uiSetEmergencyBitmap &= ~(1 << enEmergencyBF);
      uiClrEmergencyBitmap |= (1 << enEmergencyBF);
   }
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void FRAM_UpdateEmergencyBitmap()
{
   static uint32_t uiLastEmergencyBitmap;
   static uint8_t bFirst = 1;
   if( !bEmergencyBitmapReady )
   {
      /* Check for NV emergency state  Only at POR */
      uiLocalEmergencyBitmap = FRAM_ReadEmergencyBitmap();
      uiLastEmergencyBitmap = uiLocalEmergencyBitmap;
      bEmergencyBitmapReady = 1;
   }
   uint8_t bUpdateEmergencyBitmap = 0;
   uint32_t uiModifiedEmergencyBitmap = uiLocalEmergencyBitmap | uiSetEmergencyBitmap;
   uiModifiedEmergencyBitmap &= ~uiClrEmergencyBitmap;
   if(uiLastEmergencyBitmap != uiModifiedEmergencyBitmap)
   {
      bUpdateEmergencyBitmap = 1;
   }

   if(bUpdateEmergencyBitmap)
   {
      FRAM_WriteEmergencyBitmap(uiModifiedEmergencyBitmap);
      uiLocalEmergencyBitmap = FRAM_ReadEmergencyBitmap();
      uiLastEmergencyBitmap = uiLocalEmergencyBitmap;
   }

   SetEmergencyBitmap(uiModifiedEmergencyBitmap);
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
