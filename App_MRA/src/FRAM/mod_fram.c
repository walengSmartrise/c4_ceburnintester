/******************************************************************************
 * @author   Keith Soneda
 * @version  V1.00
 * @date     16, Sept 2016
 *
 * @note     Communication via SPI with cypress 256KBit FRAM chip (FM25V02A)
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru_a.h"
#include "sru.h"
#include "sys.h"
#include "fram.h"
#include "ring_buffer.h"
#include <stdint.h>
#include <string.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_FRAM =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static const uint8_t gaucFirstWriteTag[4] =
{
   0x53,//S
   0x52,//R
   0x45,//E
   0x00,//For Clearing
};
static enum en_framstates enFramState;

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
enum en_framstates FRAM_GetState()
{
   return enFramState;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Running(void)
{
   if(!GetMotion_RunFlag())
   {
      FRAM_UpdateLatchedFaults();
      FRAM_UpdateFaultLog();
      FRAM_UpdateAlarmLog();
      FRAM_UpdateEmergencyBitmap();
      FRAM_UpdateRecallFloor();
   }
   else
   {
      FRAM_UpdateAlarmLog(); /* Fix for missing NTS trip in log */
      FRAM_UpdateEmergencyBitmap();
      FRAM_UpdateRecallFloor();
   }
   FRAM_UpdateRunCounter();

   FRAM_UpdateDirChangeCounter();

   /* Check for default FRAM command */
   if( Param_ReadValue_1Bit(enPARAM1__DefaultFRAM) )
   {
      enFramState = enFRAMSTATE__Defaulting;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void FetchingStartupVariables()
{
   /*Check startup vars*/

   //move to running state
   enFramState = enFRAMSTATE__Running;
}
/*-----------------------------------------------------------------------------

   New chips should be defaulted before use.

 -----------------------------------------------------------------------------*/
static void DefaultingFRAM()
{
#define DEFAULTING_SIZE_BYTES     (100)
   static uint8_t aucClear[DEFAULTING_SIZE_BYTES];
   static uint32_t ulDefaultIndex;
   uint8_t ucBytes = FRAM_WriteMemory( aucClear, DEFAULTING_SIZE_BYTES, ulDefaultIndex );

   ulDefaultIndex += DEFAULTING_SIZE_BYTES;

   uint32_t uiTotalNumBytes = FRAM_TOTAL_NUM_BYTES;
   if(!Param_ReadValue_1Bit(enPARAM1__EnableOldFRAM))
   {
      uiTotalNumBytes = FRAM_TOTAL_NUM_BYTES_NEW;
   }

   if( ulDefaultIndex < uiTotalNumBytes )
   {
      SetFault(FLT__FRAM_DEFAULTING);
   }
   else
   {
      FRAM_WriteMemory_Triplets(&gaucFirstWriteTag[0], 3, FRAM_TADDR_FIRSTWRITETAG_START);
      enFramState = enFRAMSTATE__CheckingForFirstWrite;
      Param_WriteValue_1Bit(enPARAM1__DefaultFRAM, 0);
      ulDefaultIndex = 0;
   }
}

/*-----------------------------------------------------------------------------

   Checks for a tag indicating the FRAM chip has been initialized.
   Verifies that the first 9 bytes of the FRAM chip have the string "SSSRRREEE"
   If defaulting has been attempted 3 times without success, set fram fault

 -----------------------------------------------------------------------------*/
static void CheckingForFirstWrite(void)
{
   static uint8_t ucCycleTimeout;
   uint8_t aucWriteTag[3];
   uint8_t bNeedToDefault = 0;

   /* Initialize status register */
   uint8_t ucStatusCMD = STATUS_REG_CMD__WP_EN;
   FRAM_WriteStatus( ucStatusCMD );

   /* Check for tag signaling that the FRAM has been defaulted */
   if( ucCycleTimeout < 3 )
   {
      FRAM_ReadMemory_Triplets(&aucWriteTag[0], 3, FRAM_TADDR_FIRSTWRITETAG_START);
      for(uint8_t i = 0; i < 3; i++ )
      {
         bNeedToDefault |= aucWriteTag[i] != gaucFirstWriteTag[i];
      }

      if(bNeedToDefault)
      {
         ucCycleTimeout++;
         enFramState = enFRAMSTATE__Defaulting;
      }
      else
      {
         enFramState = enFRAMSTATE__FetchingStartupVariables;
      }
   }
   else
   {
      SetFault(FLT__FRAM_DEFAULT_FAIL);//todo rm
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 200;
   pstThisModule->uwRunPeriod_1ms = 10;
   enFramState = enFRAMSTATE__Initializing;
   FRAM_Init();
   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   switch(enFramState)
   {
      case enFRAMSTATE__Running:
         Running();
         break;
      case enFRAMSTATE__FetchingStartupVariables:
         FetchingStartupVariables();
         break;
      case enFRAMSTATE__Defaulting:
         DefaultingFRAM();
         break;
      case enFRAMSTATE__CheckingForFirstWrite:
         CheckingForFirstWrite();
         break;

      default:
         enFramState = enFRAMSTATE__CheckingForFirstWrite;
         break;
   }
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
