/******************************************************************************
 * @author   Keith Soneda
 * @version  V1.00
 * @date     16, Sept 2016
 *
 * @note     fault log operations
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "sys.h"
#include "fram.h"
#include <stdint.h>
#include <string.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static void FRAM_ReadFromFaultLog(st_fault_data *pstLog, uint8_t ucIndex);
static void FRAM_ReadFromAlarmLog(st_alarm_data *pstLog, uint8_t ucIndex);
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t ucLastRequest_Plus1 = 0xFF;

static uint8_t bClearFaultLog;
static uint8_t ucClearFaultLogIndex;

static uint8_t bClearAlarmLog;
static uint8_t ucClearAlarmLogIndex;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   Load fault log datagrams
 *----------------------------------------------------------------------------*/
#define FAULT_LOG_UPDATE_DELAY_IDLE_5MS          (400)
#define FAULT_LOG_UPDATE_DELAY_ACTIVE_5MS        (20)
void LoadFaultLogDatagrams(void)
{
   un_sdata_datagram unDatagram;
   static uint8_t ucLogIndex;
   static uint16_t uwUpdateCounter_5ms;
   if( !GetMotion_RunFlag() )
   {
      if( !SDATA_DirtyBit_Get(&gstSData_LocalData, DG_MRA__LoggedFaults1)
       && !SDATA_DirtyBit_Get(&gstSData_LocalData, DG_MRA__LoggedFaults2)
       && !SDATA_DirtyBit_Get(&gstSData_LocalData, DG_MRA__LoggedFaults3) )
      {
         uint16_t uwLimit_5ms = ( gstFault.bActiveFault || gstAlarm.bActiveAlarm || GetUIRequest_FaultLog() )
                              ? FAULT_LOG_UPDATE_DELAY_ACTIVE_5MS
                              : FAULT_LOG_UPDATE_DELAY_IDLE_5MS;
         if( ++uwUpdateCounter_5ms >= uwLimit_5ms )
         {
            /* Read from fault log */
            if( ucLogIndex < NUM_FAULTLOG_ITEMS )
            {
               st_fault_data stLog;
               FRAM_ReadFromFaultLog( &stLog, ucLogIndex );
               unDatagram.auc8[0] = ucLogIndex+1;
               unDatagram.auc8[1] = 0;
               unDatagram.auw16[1] = stLog.eFaultNum;
               unDatagram.aui32[1] = stLog.ulTimestamp;
               SDATA_WriteDatagram( &gstSData_LocalData,
                                    DG_MRA__LoggedFaults1,
                                    &unDatagram );
               unDatagram.auc8[0] = ucLogIndex+1;
               unDatagram.auc8[1] = 0;
               unDatagram.auw16[1] = stLog.wSpeed;
               unDatagram.aui32[1] = stLog.ulPosition;
               SDATA_WriteDatagram( &gstSData_LocalData,
                                    DG_MRA__LoggedFaults2,
                                    &unDatagram );
               unDatagram.auc8[0] = ucLogIndex+1;
               unDatagram.auc8[1] = 0;
               unDatagram.auc8[2] = stLog.ucCurrentFloor;
               unDatagram.auc8[3] = stLog.ucDestinationFloor;
               unDatagram.auw16[2] = stLog.wCommandSpeed;
               unDatagram.auw16[3] = stLog.wEncoderSpeed;
               SDATA_WriteDatagram( &gstSData_LocalData,
                                    DG_MRA__LoggedFaults3,
                                    &unDatagram );

               SDATA_DirtyBit_Set(&gstSData_LocalData, DG_MRA__LoggedFaults1);
               SDATA_DirtyBit_Set(&gstSData_LocalData, DG_MRA__LoggedFaults2);
               SDATA_DirtyBit_Set(&gstSData_LocalData, DG_MRA__LoggedFaults3);
            }
            /* Read from alarm log */
            else
            {
               st_alarm_data stLog;
               FRAM_ReadFromAlarmLog( &stLog, ucLogIndex-NUM_FAULTLOG_ITEMS );
               unDatagram.auc8[0] = ucLogIndex+1;
               unDatagram.auc8[1] = 0;
               unDatagram.auw16[1] = stLog.eAlarmNum;
               unDatagram.aui32[1] = stLog.ulTimestamp;
               SDATA_WriteDatagram( &gstSData_LocalData,
                                    DG_MRA__LoggedFaults1,
                                    &unDatagram );
               unDatagram.auc8[0] = ucLogIndex+1;
               unDatagram.auc8[1] = 0;
               unDatagram.auw16[1] = stLog.wSpeed;
               unDatagram.aui32[1] = stLog.ulPosition;
               SDATA_WriteDatagram( &gstSData_LocalData,
                                    DG_MRA__LoggedFaults2,
                                    &unDatagram );
               unDatagram.auc8[0] = ucLogIndex+1;
               unDatagram.auc8[1] = 0;
               unDatagram.auc8[2] = 0;
               unDatagram.auc8[3] = 0;
               unDatagram.auw16[2] = 0;
               unDatagram.auw16[3] = 0;
               SDATA_WriteDatagram( &gstSData_LocalData,
                                    DG_MRA__LoggedFaults3,
                                    &unDatagram );

               SDATA_DirtyBit_Set(&gstSData_LocalData, DG_MRA__LoggedFaults1);
               SDATA_DirtyBit_Set(&gstSData_LocalData, DG_MRA__LoggedFaults2);
               SDATA_DirtyBit_Set(&gstSData_LocalData, DG_MRA__LoggedFaults3);
            }

            ucLogIndex = (ucLogIndex+1) % (2*NUM_FAULTLOG_ITEMS);
         }
      }
      else
      {
         uwUpdateCounter_5ms = 0;
      }
   }
}
/*----------------------------------------------------------------------------
   Get last fault log write index
 *----------------------------------------------------------------------------*/
static void FRAM_WriteFaultLogIndex(uint8_t ucIndex)
{
   FRAM_WriteMemory_Triplets(&ucIndex, 1, FRAM_TADDR_FLOGINDEX_START);
}
static void FRAM_WriteAlarmLogIndex(uint8_t ucIndex)
{
   FRAM_WriteMemory_Triplets(&ucIndex, 1, FRAM_TADDR_ALOGINDEX_START);
}
/*----------------------------------------------------------------------------
   Get last fault log write index
 *----------------------------------------------------------------------------*/
static uint8_t FRAM_ReadFaultLogIndex(void)
{
   uint8_t ucData = 0;
   FRAM_ReadMemory_Triplets(&ucData, 1, FRAM_TADDR_FLOGINDEX_START);
   return ucData;
}
static uint8_t FRAM_ReadAlarmLogIndex(void)
{
   uint8_t ucData = 0;
   FRAM_ReadMemory_Triplets(&ucData, 1, FRAM_TADDR_ALOGINDEX_START);
   return ucData;
}
/*----------------------------------------------------------------------------
Write to fault log
 *----------------------------------------------------------------------------*/
static void FRAM_WriteToFaultLog( st_fault_data *pstFLog)
{
   uint8_t ucLogIndex = FRAM_ReadFaultLogIndex();
   uint8_t ucNewLogIndex = (ucLogIndex + 1) % NUM_FAULTLOG_ITEMS;
   uint32_t ulAddress = (ucNewLogIndex) * NUM_BYTES_PER_FAULTITEM + FRAM_TADDR_FAULTLOG_START;
   FRAM_WriteArray_U16(&pstFLog->eFaultNum, 1, ulAddress);
   FRAM_WriteArray_U16((uint16_t *)&pstFLog->wSpeed, 1, ulAddress+2);
   FRAM_WriteArray_U24(&pstFLog->ulPosition, 1, ulAddress+4);
   FRAM_WriteArray_U32(&pstFLog->ulTimestamp, 1, ulAddress+7);

   FRAM_WriteArray_U16((uint16_t *)&pstFLog->wCommandSpeed, 1, ulAddress+11);
   FRAM_WriteArray_U16((uint16_t *)&pstFLog->wEncoderSpeed, 1, ulAddress+13);
   FRAM_WriteArray_U8(&pstFLog->ucCurrentFloor, 1, ulAddress+15);
   FRAM_WriteArray_U8(&pstFLog->ucDestinationFloor, 1, ulAddress+16);

   FRAM_WriteFaultLogIndex(ucNewLogIndex);
}
static void FRAM_WriteToAlarmLog(st_alarm_data *pstALog)
{
   uint8_t ucLogIndex = FRAM_ReadAlarmLogIndex();
   uint8_t ucNewLogIndex = (ucLogIndex + 1) % NUM_FAULTLOG_ITEMS;
   uint32_t ulAddress = (ucNewLogIndex) * NUM_BYTES_PER_ALARMITEM + FRAM_TADDR_ALARMLOG_START;
   FRAM_WriteArray_U16((uint16_t *)&pstALog->eAlarmNum, 1, ulAddress);
   FRAM_WriteArray_U16((uint16_t *)&pstALog->wSpeed, 1, ulAddress+2);
   FRAM_WriteArray_U24(&pstALog->ulPosition, 1, ulAddress+4);
   FRAM_WriteArray_U32(&pstALog->ulTimestamp, 1, ulAddress+7);

   FRAM_WriteAlarmLogIndex(ucNewLogIndex);
}
/*----------------------------------------------------------------------------
Write to fault log
 *----------------------------------------------------------------------------*/
static void FRAM_ClearFaultLog_ByIndex(uint8_t ucIndex)
{
   uint32_t uiClear = 0;
   uint32_t ulAddress = (ucIndex) * NUM_BYTES_PER_FAULTITEM + FRAM_TADDR_FAULTLOG_START;
   FRAM_WriteArray_U16((uint16_t *)&uiClear, 1, ulAddress);
   FRAM_WriteArray_U16((uint16_t *)&uiClear, 1, ulAddress+2);
   FRAM_WriteArray_U24(&uiClear, 1, ulAddress+4);
   FRAM_WriteArray_U32(&uiClear, 1, ulAddress+7);

   FRAM_WriteArray_U16((uint16_t *)&uiClear, 1, ulAddress+11);
   FRAM_WriteArray_U16((uint16_t *)&uiClear, 1, ulAddress+13);
   FRAM_WriteArray_U8((uint8_t *)&uiClear, 1, ulAddress+15);
   FRAM_WriteArray_U8((uint8_t *)&uiClear, 1, ulAddress+16);
}
static void FRAM_ClearAlarmLog_ByIndex(uint8_t ucIndex)
{
   uint32_t uiClear = 0;
   uint32_t ulAddress = (ucIndex) * NUM_BYTES_PER_ALARMITEM + FRAM_TADDR_ALARMLOG_START;
   FRAM_WriteArray_U16((uint16_t *)&uiClear, 1, ulAddress);
   FRAM_WriteArray_U16((uint16_t *)&uiClear, 1, ulAddress+2);
   FRAM_WriteArray_U24(&uiClear, 1, ulAddress+4);
   FRAM_WriteArray_U32(&uiClear, 1, ulAddress+7);

//   FRAM_WriteArray_U16((uint16_t *)&uiClear, 1, ulAddress+11);
//   FRAM_WriteArray_U16((uint16_t *)&uiClear, 1, ulAddress+13);
//   FRAM_WriteArray_U8((uint8_t *)&uiClear, 1, ulAddress+15);
//   FRAM_WriteArray_U8((uint8_t *)&uiClear, 1, ulAddress+16);
}
/*----------------------------------------------------------------------------
Write to fault log
 *----------------------------------------------------------------------------*/
static void FRAM_ReadFromFaultLog(st_fault_data *pstLog, uint8_t ucIndex)
{
   uint8_t ucLogIndex = FRAM_ReadFaultLogIndex() % NUM_FAULTLOG_ITEMS;
   uint8_t ucNewLogIndex;


   if(ucLogIndex >= ucIndex)
   {
      ucNewLogIndex = ucLogIndex - ucIndex;
   }
   else
   {
      ucNewLogIndex = NUM_FAULTLOG_ITEMS - (ucIndex - ucLogIndex);
   }

   uint32_t ulAddress = (ucNewLogIndex) * NUM_BYTES_PER_FAULTITEM + FRAM_TADDR_FAULTLOG_START;
   FRAM_ReadArray_U16((uint16_t *)&pstLog->eFaultNum, 1, ulAddress);
   FRAM_ReadArray_U16((uint16_t *)&pstLog->wSpeed, 1, ulAddress+2);
   FRAM_ReadArray_U24(&pstLog->ulPosition, 1, ulAddress+4);
   FRAM_ReadArray_U32(&pstLog->ulTimestamp, 1, ulAddress+7);

   FRAM_ReadArray_U16((uint16_t *)&pstLog->wCommandSpeed, 1, ulAddress+11);
   FRAM_ReadArray_U16((uint16_t *)&pstLog->wEncoderSpeed, 1, ulAddress+13);
   FRAM_ReadArray_U8(&pstLog->ucCurrentFloor, 1, ulAddress+15);
   FRAM_ReadArray_U8(&pstLog->ucDestinationFloor, 1, ulAddress+16);
}
static void FRAM_ReadFromAlarmLog(st_alarm_data *pstLog, uint8_t ucIndex)
{
   uint8_t ucLogIndex = FRAM_ReadAlarmLogIndex() % NUM_FAULTLOG_ITEMS;
   uint8_t ucNewLogIndex;
   if(ucLogIndex >= ucIndex)
   {
      ucNewLogIndex = ucLogIndex - ucIndex;
   }
   else
   {
      ucNewLogIndex = NUM_FAULTLOG_ITEMS - (ucIndex - ucLogIndex);
   }

   uint32_t ulAddress = (ucNewLogIndex) * NUM_BYTES_PER_ALARMITEM + FRAM_TADDR_ALARMLOG_START;
   FRAM_ReadArray_U16((uint16_t *)&pstLog->eAlarmNum, 1, ulAddress);
   FRAM_ReadArray_U16((uint16_t *)&pstLog->wSpeed, 1, ulAddress+2);
   FRAM_ReadArray_U24(&pstLog->ulPosition, 1, ulAddress+4);
   FRAM_ReadArray_U32(&pstLog->ulTimestamp, 1, ulAddress+7);
}

/*----------------------------------------------------------------------------
   Clear Fault Log
 *----------------------------------------------------------------------------*/
static void FRAM_ClearFaultLog(void)
{
   FRAM_ClearFaultLog_ByIndex(ucClearFaultLogIndex);
   if(++ucClearFaultLogIndex >= NUM_FAULTLOG_ITEMS)
   {
      FRAM_WriteFaultLogIndex(0);
      bClearFaultLog = 0;
      ucClearFaultLogIndex = 0;
   }
}
static void FRAM_ClearAlarmLog(void)
{
   FRAM_ClearAlarmLog_ByIndex(ucClearAlarmLogIndex);
   if(++ucClearAlarmLogIndex >= NUM_FAULTLOG_ITEMS)
   {
      FRAM_WriteAlarmLogIndex(0);
      bClearAlarmLog = 0;
      ucClearAlarmLogIndex = 0;
   }
}
/*----------------------------------------------------------------------------
   Service fault log request.
   Log index received + 1
   0xFF will trigger fault log clear
   0xFE will trigger alarm log clear
 *----------------------------------------------------------------------------*/
static void ServiceLogRequests(void)
{
   /* Request for fault log? */
   uint8_t ucLogRequest_IndexPlus1 = GetUIRequest_FaultLog();
   if( ucLastRequest_Plus1 != ucLogRequest_IndexPlus1 )
   {
      ucLastRequest_Plus1 = ucLogRequest_IndexPlus1;
      if( ucLogRequest_IndexPlus1 == 0xFF )
      {
         ucClearFaultLogIndex = 0;
         bClearFaultLog = 1;
      }
      else if( ucLogRequest_IndexPlus1 == 0xFE )
      {
         ucClearAlarmLogIndex = 0;
         bClearAlarmLog = 1;
      }
   }
}
/*----------------------------------------------------------------------------
   Updates the fault log stored in fram

 *----------------------------------------------------------------------------*/
void FRAM_UpdateFaultLog(void)
{
   static en_faults aeLastFault[NUM_FAULT_NODES];
   if(!bClearFaultLog) // Dont log new faults if clearing the fault log
   {
      for(uint8_t i = 0; i < NUM_FAULT_NODES; i++)
      {
         uint8_t eNode = ( enFAULT_NODE__MRA + i ) % NUM_FAULT_NODES;
         en_faults eFault = GetFault_ByNode(eNode);
         if( eFault )
         {
            if( eFault != aeLastFault[eNode] )
            {
               st_fault_data stLog;
               GetBoardActiveFault(&stLog, eNode);
               FRAM_WriteToFaultLog(&stLog);
               aeLastFault[eNode] = eFault;
            }
         }
         else
         {
            aeLastFault[eNode] = 0;
         }
      }
   }
   else if( !GetMotion_RunFlag() )
   {
      FRAM_ClearFaultLog();
   }

   ServiceLogRequests();
}
/*----------------------------------------------------------------------------
   Updates the alarm log stored in fram

 *----------------------------------------------------------------------------*/
void FRAM_UpdateAlarmLog(void)
{
   static en_alarms aeLastAlarm[NUM_ALARM_NODES];
   if(!bClearAlarmLog) // Dont log new faults if clearing the fault log
   {
      for(uint8_t i = 0; i < NUM_ALARM_NODES; i++)
      {
         uint8_t eNode = ( enALARM_NODE__MRA + i ) % NUM_ALARM_NODES;
         en_alarms eAlarm = GetAlarm_ByNode(eNode);
         if( eAlarm )
         {
            if( eAlarm != aeLastAlarm[eNode] )
            {
               st_alarm_data stLog;
               GetActiveAlarmData(&stLog, eNode);
               FRAM_WriteToAlarmLog(&stLog);
               aeLastAlarm[eNode] = eAlarm;
            }
         }
         else
         {
            aeLastAlarm[eNode] = 0;
         }
      }
   }
   else if( !GetMotion_RunFlag() )
   {
      FRAM_ClearAlarmLog();
   }
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
