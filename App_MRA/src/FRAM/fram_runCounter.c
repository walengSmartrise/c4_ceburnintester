/******************************************************************************
 * @author   Keith Soneda
 * @version  V1.00
 * @date     16, Sept 2016
 *
 * @note     Tracks and logs the number of runs
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "sys.h"
#include "fram.h"
#include "ring_buffer.h"
#include <stdint.h>
#include <string.h>
#include "motion.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define NUM_VALID_RUNS_BEFORE_SAVING  (2)//(4)
#define MIN_SUCCESSFUL_RUN_TIME_10MS    (500)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t ucRunCounter;
static uint32_t uiTotalRunCount;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint32_t FRAM_GetTotalRunCount(void)
{
   return uiTotalRunCount+ucRunCounter;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static uint32_t FRAM_ReadRunCounter(void)
{
   uint32_t ulData;
   FRAM_ReadArray_U32(&ulData, 1, FRAM_TADDR_RUNCOUNTER_START);

   return ulData;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void FRAM_WriteRunCounter(uint32_t ulCount)
{
   FRAM_WriteArray_U32(&ulCount, 1, FRAM_TADDR_RUNCOUNTER_START);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void IncrementRunCounter(void)
{
   if(++ucRunCounter == NUM_VALID_RUNS_BEFORE_SAVING)
   {
      uiTotalRunCount = FRAM_ReadRunCounter()+ucRunCounter;
      FRAM_WriteRunCounter(uiTotalRunCount);
      ucRunCounter = 0;
   }
}
/*----------------------------------------------------------------------------
   Increment run counter if run was longer than 5sec
 *----------------------------------------------------------------------------*/
void FRAM_UpdateRunCounter()
{
   static uint8_t bFirst = 1;
   static uint8_t bLastRunFlag;
   static uint16_t uwCounter_10ms;

   if(bFirst)
   {
      bFirst = 0;
      uiTotalRunCount = FRAM_ReadRunCounter();
   }
   if( GetOperation_ClassOfOp() == CLASSOP__AUTO )
   {
      if(GetMotion_RunFlag())
      {
         if(!bLastRunFlag)
         {
            if(++uwCounter_10ms > MIN_SUCCESSFUL_RUN_TIME_10MS)
            {
               uwCounter_10ms = 0;
               bLastRunFlag = GetMotion_RunFlag();
               IncrementRunCounter();
            }
         }
      }
      else
      {
         uwCounter_10ms = 0;
         bLastRunFlag = 0;
      }
   }
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
