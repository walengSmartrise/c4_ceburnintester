/******************************************************************************
 * @author   Keith Soneda
 * @version  V1.00
 * @date     16, Sept 2016
 *
 * @note     Latched fault & fault log operations
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "sys.h"
#include "fram.h"
#include "ring_buffer.h"
#include <stdint.h>
#include <string.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint32_t uiLatchedFaultBitmap;
static uint32_t uiClrFaultBitmask;
static uint32_t uiSetFaultBitmask;
static uint32_t uiLastClrFaultBitmask;
static uint32_t uiLastSetFaultBitmask;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
   Will override a clear request for the given latching fault
 *----------------------------------------------------------------------------*/
void SetLatchingSafetyFault( enum en_latching_safety_faults eFault )
{
   uiSetFaultBitmask |= ( 1 << eFault );
   uiClrFaultBitmask &= ~( 1 << eFault );
}
/*----------------------------------------------------------------------------
   Will override a set request for the given latching fault
 *----------------------------------------------------------------------------*/
void ClrLatchingSafetyFault( enum en_latching_safety_faults eFault )
{
   uiClrFaultBitmask |= ( 1 << eFault );
   uiSetFaultBitmask &= ~( 1 << eFault );
   ClearLatchingSafetyFault(eFault);
}
/*----------------------------------------------------------------------------
 Latched Faults
 *----------------------------------------------------------------------------*/
void FRAM_WriteLatchedFaultBitmap(uint32_t ulBitmap)
{
   FRAM_WriteArray_U32(&ulBitmap, 1, FRAM_TADDR_LATCHEDFAULTS_START);
}
uint32_t FRAM_ReadLatchedFaultBitmap()
{
   uint32_t ulBitmap;
   FRAM_ReadArray_U32(&ulBitmap, 1, FRAM_TADDR_LATCHEDFAULTS_START);
   return ulBitmap;
}
/*----------------------------------------------------------------------------
   Updates the latched fault bitmap stored on FRAM
 *----------------------------------------------------------------------------*/
void FRAM_UpdateLatchedFaults()
{
   uiLatchedFaultBitmap = FRAM_ReadLatchedFaultBitmap();

   uint32_t uiNewBitmap = uiLatchedFaultBitmap;
   if( uiLastSetFaultBitmask != uiSetFaultBitmask )
   {
      uiLastSetFaultBitmask = uiSetFaultBitmask;
      uiNewBitmap |= uiSetFaultBitmask;
      FRAM_WriteLatchedFaultBitmap( uiNewBitmap );
   }
   else if( uiLastClrFaultBitmask != uiClrFaultBitmask )
   {
      uiLastClrFaultBitmask = uiClrFaultBitmask;
      uiNewBitmap &= ~uiClrFaultBitmask;
      FRAM_WriteLatchedFaultBitmap( uiNewBitmap );
   }

   //To maintain support for GlobalData.c functions
   SetLatchedFaultBitmap(uiLatchedFaultBitmap);
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
