/******************************************************************************
 * @author   Keith Soneda
 * @version  V1.00
 * @date     16, Sept 2016
 *
 * @note     Emergency recall floor/door stored here
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "sys.h"
#include "fram.h"

#include <stdint.h>
#include <string.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
/* Door encoded in MSBit - 1 is rear */
static uint16_t uwLocalRecallFloor_Plus1;
static uint16_t uwStoredRecallFloor_Plus1;

static uint8_t bRecallFloorReady;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   Returns 1 if emergency bitmap is ready
 *----------------------------------------------------------------------------*/
uint8_t FRAM_RecallFloorReady()
{
   return bRecallFloorReady;
}
/*----------------------------------------------------------------------------
Emergency bitmaps
 *----------------------------------------------------------------------------*/
static void FRAM_WriteRecallFloor_Plus1(uint16_t uwFloor_Plus1)
{
   FRAM_WriteArray_U16(&uwFloor_Plus1, 1, FRAM_TADDR_RECALL_FLOOR_START);
}
static uint16_t FRAM_ReadRecallFloor_Plus1()
{
   uint16_t uwFloor_Plus1;
   FRAM_ReadArray_U16(&uwFloor_Plus1, 1, FRAM_TADDR_RECALL_FLOOR_START);
   return uwFloor_Plus1;
}

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void FRAM_SetRecallFloor_Plus1( uint8_t ucFloor_Plus1 )
{
   if( ucFloor_Plus1 <= MAX_NUM_FLOORS )
   {
      uwLocalRecallFloor_Plus1 &= ~0x7FFF;
      uwLocalRecallFloor_Plus1 |= ucFloor_Plus1;
   }
}
/*----------------------------------------------------------------------------
 1 if rear door
 //TODO remove
 *----------------------------------------------------------------------------*/
void FRAM_SetRecallDoor( uint8_t bRear )
{
   if( bRear )
   {
      uwLocalRecallFloor_Plus1 |= 0x8000;
   }
   else
   {
      uwLocalRecallFloor_Plus1 &= ~0x8000;
   }
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint8_t FRAM_GetRecallFloor_Plus1()
{
   return uwStoredRecallFloor_Plus1 & 0x00FF;
}
/*----------------------------------------------------------------------------
   Returns 1 if rear
 *----------------------------------------------------------------------------*/
uint8_t FRAM_GetRecallDoor()
{
   uint8_t bRear = ( uwStoredRecallFloor_Plus1 & 0x8000 ) ? 1:0;
   return bRear;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void FRAM_UpdateRecallFloor()
{
   if( !bRecallFloorReady )
   {
      /* Check for NV emergency state  Only at POR */
      uwLocalRecallFloor_Plus1 = FRAM_ReadRecallFloor_Plus1();
      uwStoredRecallFloor_Plus1 = uwLocalRecallFloor_Plus1;
      bRecallFloorReady = 1;
   }

   uint8_t bUpdateRecallFloor = 0;
   //TODO add timeout for failure to write
   if(uwStoredRecallFloor_Plus1 != uwLocalRecallFloor_Plus1)
   {
      bUpdateRecallFloor = 1;
   }

   if(bUpdateRecallFloor)
   {
      FRAM_WriteRecallFloor_Plus1(uwLocalRecallFloor_Plus1);
      uwStoredRecallFloor_Plus1 = FRAM_ReadRecallFloor_Plus1();
   }

}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
