/******************************************************************************
 * @author   Keith Soneda
 * @version  V1.00
 * @date     14, April 2019
 *
 * @note     Tracks and logs the number of times the car changes direciton of movement.
 *           This is used to monitor the rated life span of ropes.
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "sys.h"
#include "fram.h"
#include "ring_buffer.h"
#include <stdint.h>
#include <string.h>
#include "motion.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MIN_SUCCESSFUL_RUN_TIME_10MS      (500)
#define NUM_UNSAVED_COUNTS_BEFORE_SAVING  (3)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static enum direction_enum eLastDir;
static uint16_t uwDirectionChangeCount;
static uint8_t ucUnsavedCounts;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint16_t FRAM_GetDirChangeCount(void)
{
   return uwDirectionChangeCount+ucUnsavedCounts;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static uint16_t FRAM_ReadDirChangeCounter(void)
{
   uint16_t uwData;
   FRAM_ReadArray_U16(&uwData, 1, FRAM_TADDR_RUNCOUNTER_START);
   return uwData;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void FRAM_WriteDirChangeCounter(uint16_t uwCount)
{
   FRAM_WriteArray_U16(&uwCount, 1, FRAM_TADDR_RUNCOUNTER_START);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void StoreDirChangeCounter(void)
{
   if( ucUnsavedCounts >= NUM_UNSAVED_COUNTS_BEFORE_SAVING )
   {
      uwDirectionChangeCount = FRAM_ReadDirChangeCounter();
      uwDirectionChangeCount += ucUnsavedCounts;
      FRAM_WriteDirChangeCounter(uwDirectionChangeCount);
      ucUnsavedCounts = 0;
   }
}
/*----------------------------------------------------------------------------
   Increment direction change counter if
 *----------------------------------------------------------------------------*/
void FRAM_UpdateDirChangeCounter(void)
{
   static uint8_t bFirst = 1;
   static uint16_t uwCounter_10ms;

   if(bFirst)
   {
      bFirst = 0;
      uwDirectionChangeCount = FRAM_ReadDirChangeCounter();
   }

   if( GetMotion_RunFlag() )
   {
      if(uwCounter_10ms >= MIN_SUCCESSFUL_RUN_TIME_10MS)
      {
         enum direction_enum eDir = GetMotion_Direction();
         if( ( eDir ) // End of run direction desasserted before dropping of run flag.
          && ( eLastDir != eDir ) )
         {
            /* The first time this is run the last direction
             * is unknown. Don't increment. */
            if( eLastDir )
            {
               ucUnsavedCounts++;
            }
            eLastDir = eDir;
         }
      }
      else
      {
         uwCounter_10ms++;
      }
   }
   else
   {
      uwCounter_10ms = 0;
      StoreDirChangeCounter();
   }
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
