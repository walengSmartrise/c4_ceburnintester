/******************************************************************************
 *
 * @file     fram.c
 * @brief
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note     fram read/write functions.
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "fram.h"
#include "mod.h"


#include <stdint.h>
#include <stdio.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
static enum en_fram_fault eFRAM_Fault;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define FRAM_SSP     (LPC_SSP0)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

enum en_fram_stat_bits
{
   FRAMStat_WriteEn = 1,
   FRAMStatBit_BP0 = 2,
   FRAMStatBit_BP1 = 3,
   FRAMStatBit_EnWP = 7,
};

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint32_t ulAddressOfDataCorruption;

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
Writes 8 Bit
 *----------------------------------------------------------------------------*/
void FRAM_WriteArray_U8(uint8_t *pucData, uint8_t ucSize, uint32_t ulAddr )
{
   FRAM_WriteMemory_Triplets( pucData, ucSize, ulAddr);
}
/*----------------------------------------------------------------------------
Writes 16 Bit
 *----------------------------------------------------------------------------*/
void FRAM_WriteArray_U16(uint16_t *puwData, uint8_t ucSize, uint32_t ulAddr )
{
   uint8_t aucData[2];
   for(uint8_t i = 0; i < ucSize; i++)
   {
      uint32_t ulNewAddr = ulAddr + 2 * i;
      for(uint8_t j = 0; j < 2; j++)
      {
         aucData[j] = ( *(puwData+i) >> (j*8) ) & 0xFF;
      }
      FRAM_WriteMemory_Triplets(&aucData[0], 2, ulNewAddr);
   }
}
/*----------------------------------------------------------------------------
Writes 24 Bit
 *----------------------------------------------------------------------------*/
void FRAM_WriteArray_U24(uint32_t *pulData, uint8_t ucSize, uint32_t ulAddr )
{
   static const uint8_t ucNumBytes = 3;
   uint8_t aucData[4];
   for(uint8_t i = 0; i < ucSize; i++)
   {
      uint32_t ulNewAddr = ulAddr + ucNumBytes * i;
      for(uint8_t j = 0; j < ucNumBytes; j++)
      {
         aucData[j] = ( *(pulData+i) >> (j*8) ) & 0xFF;
      }
      FRAM_WriteMemory_Triplets(&aucData[0], ucNumBytes, ulNewAddr);
   }
}
/*----------------------------------------------------------------------------
Writes 32 Bit
 *----------------------------------------------------------------------------*/
void FRAM_WriteArray_U32(uint32_t *pulData, uint8_t ucSize, uint32_t ulAddr )
{
   static const uint8_t ucNumBytes = 4;
   uint8_t aucData[4];
   for(uint8_t i = 0; i < ucSize; i++)
   {
      uint32_t ulNewAddr = ulAddr + ucNumBytes * i;
      for(uint8_t j = 0; j < ucNumBytes; j++)
      {
         aucData[j] = ( *(pulData+i) >> (j*8) ) & 0xFF;
      }
      FRAM_WriteMemory_Triplets(&aucData[0], ucNumBytes, ulNewAddr);
   }
}
/*----------------------------------------------------------------------------
Read 8 Bit
 *----------------------------------------------------------------------------*/
void FRAM_ReadArray_U8(uint8_t *pucData, uint8_t ucSize, uint32_t ulAddr )
{
   FRAM_ReadMemory_Triplets(pucData, ucSize, ulAddr);
}
/*----------------------------------------------------------------------------
Read 16 Bit
 *----------------------------------------------------------------------------*/
void FRAM_ReadArray_U16(uint16_t *puwData, uint8_t ucSize, uint32_t ulAddr )
{
   uint8_t aucData[2] = {0, 0};
   for(uint8_t i = 0; i < ucSize; i++)
   {
      uint32_t ulNewAddr = ulAddr + 2 * i;
      FRAM_ReadMemory_Triplets(&aucData[0], 2, ulNewAddr);
      *(puwData+i) = (aucData[1] << 8) | (aucData[0]);
   }
}
/*----------------------------------------------------------------------------
Read 24 Bit
 *----------------------------------------------------------------------------*/
void FRAM_ReadArray_U24(uint32_t *pulData, uint8_t ucSize, uint32_t ulAddr )
{
   uint8_t aucData[3] = {0, 0, 0};
   for(uint8_t i = 0; i < ucSize; i++)
   {
      uint32_t ulNewAddr = ulAddr + 3 * i;
      FRAM_ReadMemory_Triplets(&aucData[0], 3, ulNewAddr);
      *(pulData+i) = (aucData[2] << 16) | (aucData[1] << 8) | (aucData[0]);
   }
}
/*----------------------------------------------------------------------------
Read 32 Bit
 *----------------------------------------------------------------------------*/
void FRAM_ReadArray_U32(uint32_t *pulData, uint8_t ucSize, uint32_t ulAddr )
{
   uint8_t aucData[4] = {0, 0, 0, 0};
   for(uint8_t i = 0; i < ucSize; i++)
   {
      uint32_t ulNewAddr = ulAddr + 4 * i;
      FRAM_ReadMemory_Triplets(&aucData[0], 4, ulNewAddr);
      *(pulData+i) = (aucData[3] << 24) | (aucData[2] << 16) | (aucData[1] << 8) | (aucData[0]);
   }
}
/*----------------------------------------------------------------------------
   Set FRAM fault state. Only should b called in mod_fram.c
 *----------------------------------------------------------------------------*/
void FRAM_SetFault(enum en_fram_fault eFault)
{
   eFRAM_Fault = eFault;
}
/*----------------------------------------------------------------------------
   Get fram fault state
 *----------------------------------------------------------------------------*/
enum en_fram_fault FRAM_GetFault()
{
   return eFRAM_Fault;
}

/*----------------------------------------------------------------------------
   Correct corrupted data.
   Returns 0 if data was corrupted and nonrestorable
 *----------------------------------------------------------------------------*/
static uint8_t FRAM_CheckForDataCorruption(uint8_t *pucData, uint32_t ulAddress )
{
   enum en_fram_fault eFault = FRAM_FAULT__NONE;
   uint8_t ucReturn = 0;
   uint8_t ucCopy1 = *(pucData);
   uint8_t ucCopy2 = *(pucData+1);
   uint8_t ucCopy3 = *(pucData+2);
   uint8_t bAllValid = (ucCopy2 & ucCopy3) == ucCopy1;

   if(bAllValid)//All data copies match
   {
      ucReturn = ucCopy1;
   }
   //Two of three data copies match
   else if( ( ucCopy1 == ucCopy2 )
         || ( ucCopy1 == ucCopy3 ) )
   {
      ucReturn = ucCopy1;
      FRAM_WriteMemory_Triplets(&ucCopy1, 1, ulAddress);
      ulAddressOfDataCorruption = ulAddress;
      eFault = FRAM_FAULT__REDUNDANCY;
      if(Param_ReadValue_1Bit(enPARAM1__FRAM_EnableAlarms))
      {
         SetAlarm(ALM__FRAM_REDUNDANCY);
      }
   }
   else if( ucCopy2 == ucCopy3 )
   {
      ucReturn = ucCopy2;
      FRAM_WriteMemory_Triplets(&ucCopy2, 1, ulAddress);
      ulAddressOfDataCorruption = ulAddress;
      eFault = FRAM_FAULT__REDUNDANCY;
      if(Param_ReadValue_1Bit(enPARAM1__FRAM_EnableAlarms))
      {
         SetAlarm(ALM__FRAM_REDUNDANCY);
      }
   }
   //All three data copies are different
   else
   {
      FRAM_WriteMemory_Triplets(&ucCopy1, 1, ulAddress);
      ulAddressOfDataCorruption = ulAddress;
      eFault = FRAM_FAULT__RESTORE;
      if(!GetMotion_RunFlag())
      {
         SetFault(FLT__FRAM_DATA_CORRUPTION);
      }
   }

   eFRAM_Fault = eFault;

   return ucReturn;
}
/*----------------------------------------------------------------------------
   Writes each byte three times in consecutive addresses
   ulStartTAddress = triplet address
 *----------------------------------------------------------------------------*/
uint8_t FRAM_WriteMemory_Triplets(const uint8_t *pucData, uint8_t ucNumBytes, uint32_t ulStartTAddress)
{
   uint8_t aucByte[3];
   uint32_t ulAddress;
   for(uint8_t i = 0; i < ucNumBytes; i++)
   {
      for(uint8_t j = 0; j < 3; j++)
      {
         aucByte[j] = *(pucData+i);
      }
      /* Convert triplet address to fram address */
      ulAddress = (ulStartTAddress + i) * 3;
      FRAM_WriteMemory(&aucByte[0], 3, ulAddress);
   }

   return 1;
}
/*----------------------------------------------------------------------------
   Reads each byte and checks it against its duplicates
   ulStartTAddress = triplet address
 *----------------------------------------------------------------------------*/
uint8_t FRAM_ReadMemory_Triplets(uint8_t *pucData, uint8_t ucNumBytes, uint32_t ulStartTAddress)
{
   uint8_t aucByte[3] = {0, 0, 0};
   uint32_t ulAddress;
   uint8_t i = 0;
   for( i = 0; i < ucNumBytes; i++)
   {
      /* Convert triplet address to fram address */
      ulAddress = (ulStartTAddress + i) * 3;
      FRAM_ReadMemory(&aucByte[0], 3, ulAddress);
      uint8_t ucData = FRAM_CheckForDataCorruption(&aucByte[0], ulAddress);
      *(pucData+i) = ucData;
   }

   return i;
}


/*----------------------------------------------------------------------------
   check if packet transmission has finished
   Returns 0 if transmission has timed out
 *----------------------------------------------------------------------------*/
static uint8_t FRAM_WaitForTransmissionComplete()
{
   uint16_t uwLimit = 10000;
   uint8_t bReturn = 1;
   while( uwLimit
       && ( ( Chip_SSP_GetStatus(FRAM_SSP,SSP_STAT_BSY) == SET ) || ( Chip_SSP_GetStatus(FRAM_SSP,SSP_STAT_TFE) == RESET ) ) )
   {
      uwLimit--;
   }
   if(!uwLimit)
   {
      SetFault(FLT__FRAM_TIMEOUT);
      bReturn = 0;
   }
   return bReturn;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void FRAM_Init(void)
{
   Chip_SSP_Init(FRAM_SSP);
   Chip_SSP_SetMaster(FRAM_SSP, 1);
#if ENABLE_FRAM_SSP_MODIFICATION
   Chip_SSP_SetFormat(FRAM_SSP,SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_CPHA1_CPOL1 );
//   Chip_SSP_SetBitRate(FRAM_SSP, 250000);
   Chip_SSP_SetBitRate(FRAM_SSP, 1000000);
#else
   Chip_SSP_SetFormat(FRAM_SSP,SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_CPHA0_CPOL0 );
   Chip_SSP_SetBitRate(FRAM_SSP, 1000000);
#endif
   Chip_SSP_DisableLoopBack(FRAM_SSP);
   Chip_SSP_Enable(FRAM_SSP);
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void FRAM_WriteEnable(void)
{
   static uint8_t ucCommand = CMD_WR_ENABLE;
   static uint8_t ucLengthTx = 1;
   uint8_t ucCountTx = 0;
   while((Chip_SSP_GetStatus(FRAM_SSP, SSP_STAT_RNE) == SET))//Clear Rx Buffer
   {
      Chip_SSP_ReceiveFrame(FRAM_SSP);
   }

   if(FRAM_WaitForTransmissionComplete())
   {
      SRU_Write_FRAM_SSEL(0);
      while( ucCountTx < ucLengthTx )
      {
         if(ucCountTx < ucLengthTx)
         {
            while(Chip_SSP_GetStatus(FRAM_SSP, SSP_STAT_TNF) == RESET){}//if TX FIFO is full, wait.
            Chip_SSP_SendFrame(FRAM_SSP, (*(uint8_t *) ((uint32_t) &ucCommand + ucCountTx)));
            ucCountTx++;
         }
         else
         {
            Chip_SSP_SendFrame(FRAM_SSP, 0xFF);
         }
      }
      FRAM_WaitForTransmissionComplete();
      SRU_Write_FRAM_SSEL(1);
   }
}
/*----------------------------------------------------------------------------
First received byte will be thrown out
 *----------------------------------------------------------------------------*/
uint8_t FRAM_ReadStatus(void)
{
   static uint8_t ucCommand = CMD_RD_STATUS;
   static Chip_SSP_DATA_SETUP_T stSetup;
   static uint8_t aucDataRx[2];
   static uint8_t ucLengthRx = 1;
   static uint8_t ucLengthTx = 1;
   stSetup.length = ucLengthRx+ucLengthTx;
   stSetup.rx_cnt = 0;
   stSetup.tx_cnt = 0;
   stSetup.rx_data = &aucDataRx[0];
   stSetup.tx_data = &ucCommand;

   uint16_t rDat;
   //Clear Rx Buffer
   while((Chip_SSP_GetStatus(FRAM_SSP, SSP_STAT_RNE) == SET))
   {
      Chip_SSP_ReceiveFrame(FRAM_SSP);
   }
   while( Chip_SSP_GetStatus(FRAM_SSP,SSP_STAT_BSY) == SET
     || Chip_SSP_GetStatus(FRAM_SSP,SSP_STAT_TFE) == RESET){}

   //Send Frame
   SRU_Write_FRAM_SSEL(0);

   while( stSetup.rx_cnt < stSetup.length )
   {
      if(stSetup.tx_cnt < ucLengthTx)
      {
         while(Chip_SSP_GetStatus(FRAM_SSP, SSP_STAT_TNF) == RESET){}//if TX FIFO is full, wait.
         Chip_SSP_SendFrame(FRAM_SSP, (*(uint8_t *) ((uint32_t) stSetup.tx_data + stSetup.tx_cnt)));
         stSetup.tx_cnt++;
      }
      else
      {
         Chip_SSP_SendFrame(FRAM_SSP, 0xFF);
      }
      if(Chip_SSP_GetStatus(FRAM_SSP, SSP_STAT_RNE) == SET)
      {
         rDat = Chip_SSP_ReceiveFrame(FRAM_SSP);
         if (stSetup.rx_data) {
            *(uint8_t *) ((uint32_t) stSetup.rx_data + stSetup.rx_cnt) = rDat;
         }
         stSetup.rx_cnt++;
      }
   }
   SRU_Write_FRAM_SSEL(1);

   return aucDataRx[1];
}

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint8_t FRAM_WriteStatus(uint8_t ucDataTx)
{
   static uint8_t aucDataTx[2];
   aucDataTx[0] = CMD_WR_STATUS;
   aucDataTx[1] = ucDataTx;
   static Chip_SSP_DATA_SETUP_T stSetup;
   static uint8_t ucLengthTx = 2;
   static uint8_t ucLengthRx = 0;

   SRU_Write_FRAM_nWP(1); // Turn off write protection

   stSetup.length = ucLengthTx+ucLengthRx;
   stSetup.rx_cnt = 0;
   stSetup.tx_cnt = 0;
   stSetup.tx_data = &aucDataTx[0];

   //Clear Rx Buffer
   while((Chip_SSP_GetStatus(FRAM_SSP, SSP_STAT_RNE) == SET))
   {
      Chip_SSP_ReceiveFrame(FRAM_SSP);
   }

   FRAM_WriteEnable();

   while( Chip_SSP_GetStatus(FRAM_SSP,SSP_STAT_BSY) == SET
        || Chip_SSP_GetStatus(FRAM_SSP,SSP_STAT_TFE) == RESET){}

   SRU_Write_FRAM_SSEL(0);

   while( stSetup.tx_cnt < stSetup.length )
   {
      if(stSetup.tx_cnt < ucLengthTx)
      {
         while(Chip_SSP_GetStatus(FRAM_SSP, SSP_STAT_TNF) == RESET)//if TX FIFO is full, wait.
         {
         }
         Chip_SSP_SendFrame(FRAM_SSP, (*(uint8_t *) ((uint32_t) stSetup.tx_data + stSetup.tx_cnt)));
         stSetup.tx_cnt++;
      }
      else
      {
         Chip_SSP_SendFrame(FRAM_SSP, 0xFF);
      }
   }
   FRAM_WaitForTransmissionComplete();
   SRU_Write_FRAM_SSEL(1);

   SRU_Write_FRAM_nWP(0); // Turn on write protection

   return stSetup.tx_cnt;
}

/*----------------------------------------------------------------------------
   NOTE:
      Manual toggling of enTRC_Output_FRAM_SSEL at start and end of frame
      necessary because hardware SPI will toggle the output every 8-16 bits otherwise.
      This correupts the write/read.
 *----------------------------------------------------------------------------*/
uint8_t FRAM_WriteMemory(uint8_t *pucData, uint32_t ulNumBytes, uint32_t ulStartAddress)
{
   static uint8_t aucCommand[4];
   static Chip_SSP_DATA_SETUP_T stSetup;
   uint32_t ucLengthRx = 0;
   uint8_t ucAddressSize_Bytes = 4;

   if(!Param_ReadValue_1Bit(enPARAM1__EnableOldFRAM))
   {
      ucAddressSize_Bytes = 3;
      aucCommand[0] = CMD_WR_MEMORY;
      aucCommand[1] = ulStartAddress >> 8 & 0xFF;
      aucCommand[2] = ulStartAddress & 0xFF;
   }
   else
   {
      aucCommand[0] = CMD_WR_MEMORY;
      aucCommand[1] = ulStartAddress >> 16 & 0xFF;
      aucCommand[2] = ulStartAddress >> 8 & 0xFF;
      aucCommand[3] = ulStartAddress & 0xFF;
   }

   uint32_t ucLengthTx = ulNumBytes + ucAddressSize_Bytes;
   stSetup.length = ucLengthTx+ucLengthRx;
   stSetup.rx_cnt = 0;
   stSetup.tx_cnt = 0;
   stSetup.tx_data = pucData;

   //Clear Rx Buffer
   while((Chip_SSP_GetStatus(FRAM_SSP, SSP_STAT_RNE) == SET))
   {
      Chip_SSP_ReceiveFrame(FRAM_SSP);
   }

   FRAM_WriteEnable();

   if(FRAM_WaitForTransmissionComplete())
   {
      SRU_Write_FRAM_SSEL(0);

      while( stSetup.tx_cnt < stSetup.length )
      {
         if(stSetup.tx_cnt < ucLengthTx)
         {
            while(Chip_SSP_GetStatus(FRAM_SSP, SSP_STAT_TNF) == RESET){}//if TX FIFO is full, wait.
            if(stSetup.tx_cnt < ucAddressSize_Bytes)
            {
               Chip_SSP_SendFrame(FRAM_SSP, aucCommand[stSetup.tx_cnt]);
            }
            else
            {

               Chip_SSP_SendFrame(FRAM_SSP, (*(uint8_t *) ((uint32_t) stSetup.tx_data + stSetup.tx_cnt - ucAddressSize_Bytes)));
            }

            stSetup.tx_cnt++;
         }
         else
         {
            break;
         }
      }
      FRAM_WaitForTransmissionComplete();
      SRU_Write_FRAM_SSEL(1);
   }
   return stSetup.tx_cnt;
}
/*----------------------------------------------------------------------------
   NOTE:
      Manual toggling of enTRC_Output_FRAM_SSEL at start and end of frame
      necessary because hardware SPI will toggle the output every 8-16 bits otherwise.
      This correupts the write/read.

   Limited to 3 read bytes at a time
 *----------------------------------------------------------------------------*/
uint8_t FRAM_ReadMemory(uint8_t *pucData, uint32_t ulNumBytes, uint32_t ulStartAddress)
{
   static uint8_t aucCommand[4];
   static Chip_SSP_DATA_SETUP_T stSetup;
   uint32_t ucLengthRx = ulNumBytes + 1;
   uint8_t ucAddressSize_Bytes = 4;

   if(!Param_ReadValue_1Bit(enPARAM1__EnableOldFRAM))
   {
      ucAddressSize_Bytes = 3;
      aucCommand[0] = CMD_RD_MEMORY;
      aucCommand[1] = ulStartAddress >> 8 & 0xFF;
      aucCommand[2] = ulStartAddress & 0xFF;
   }
   else
   {
      aucCommand[0] = CMD_RD_MEMORY;
      aucCommand[1] = ulStartAddress >> 16 & 0xFF;
      aucCommand[2] = ulStartAddress >> 8 & 0xFF;
      aucCommand[3] = ulStartAddress & 0xFF;
   }

   uint32_t ucLengthTx = ucAddressSize_Bytes;

   stSetup.length = ucLengthTx+ucLengthRx;
   stSetup.rx_cnt = 0;
   stSetup.tx_cnt = 0;
   stSetup.rx_data = pucData;
   stSetup.tx_data = &aucCommand[0];

#if ENABLE_FRAM_SSP_MODIFICATION
   if(FRAM_WaitForTransmissionComplete())
   {
      Chip_SSP_RWFrames_Blocking(FRAM_SSP, &stSetup);
      FRAM_WaitForTransmissionComplete();
   }

   return stSetup.rx_cnt;
#else
   uint16_t rDat;
   //Clear Rx Buffer
   while((Chip_SSP_GetStatus(FRAM_SSP, SSP_STAT_RNE) == SET))
   {
      Chip_SSP_ReceiveFrame(FRAM_SSP);
   }

   FRAM_WriteEnable();

   if(FRAM_WaitForTransmissionComplete())
   {
      SRU_Write_FRAM_SSEL(0);

      while( stSetup.rx_cnt < stSetup.length )
      {
         if(stSetup.tx_cnt < ucLengthTx)
         {
            while(Chip_SSP_GetStatus(FRAM_SSP, SSP_STAT_TNF) == RESET){}//if TX FIFO is full, wait.
            Chip_SSP_SendFrame(FRAM_SSP, aucCommand[stSetup.tx_cnt]);
            stSetup.tx_cnt++;
         }
         else
         {
            Chip_SSP_SendFrame(FRAM_SSP, 0xFF);
         }

         if(Chip_SSP_GetStatus(FRAM_SSP, SSP_STAT_RNE) == SET)
         {
            rDat = Chip_SSP_ReceiveFrame(FRAM_SSP);
            if (stSetup.rx_data)
            {
               /* The first few bytes will be garbage captured while transmitting */
               uint8_t ucGarbageBytes = ucAddressSize_Bytes+1;
               if(stSetup.rx_cnt >= ucGarbageBytes)
               {
                  *(uint8_t *) ((uint32_t) stSetup.rx_data + stSetup.rx_cnt - ucGarbageBytes) = (uint8_t)rDat;
               }
            }
            stSetup.rx_cnt++;
         }
      }
      FRAM_WaitForTransmissionComplete();
      SRU_Write_FRAM_SSEL(1);
   }

   return stSetup.rx_cnt;
#endif
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
