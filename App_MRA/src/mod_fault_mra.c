/******************************************************************************
 *
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru_a.h"
#include "sys.h"

#include "GlobalData.h"
#include <stdint.h>
#include <string.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define LIMIT_CONSECUTIVE_FAULTS    ( 3 )
#define OOS_STARTUP_CHECK_DELAY_MS     (15000)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t gabFaultType_ClearCalls[NUM_FAULTS] =
{
   FAULT_TABLE(EXPAND_FAULT_TABLE_AS_CC_ARRAY)
};
static uint8_t ucConsecutiveFaultCount;
static uint8_t ucHourlyFaultCount;
static uint8_t ucStartsPerMinuteCount;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Checks for invalid speed parameters
 -----------------------------------------------------------------------------*/
static void CheckFor_InvalidSpeed()
{
   SetFP_ContractSpeed(Param_ReadValue_16Bit(enPARAM16__ContractSpeed));
   if ( GetFP_ContractSpeed() < MIN_CONTRACT_SPEED
     || GetFP_ContractSpeed() > MAX_CAR_SPEED
      )
   {
      SetFault(FLT__INVALID_CONTRACT_SPD);
   }
   //----------------------------------------------------
   uint16_t uwFP_InspectionSpeed = Param_ReadValue_16Bit(enPARAM16__InspectionSpeed);
   if ( uwFP_InspectionSpeed > MAX_INSP_SPEED )
   {
      SetFault(FLT__INVALID_INSP_SPD);
   }
   //----------------------------------------------------
   if( GetOperation_ManualMode() != MODE_M__CONSTRUCTION )
   {
      //----------------------------------------------------
      uint16_t uwFP_LearnSpeed = Param_ReadValue_16Bit(enPARAM16__LearnSpeed);
      uint16_t uwFP_TerminalSpeed = Param_ReadValue_16Bit(enPARAM16__InspectionTerminalSpeed);
      uint16_t uwFP_LevelingSpeed = Param_ReadValue_16Bit(enPARAM16__LevelingSpeed);
      uint8_t ucFP_NTSD_Speed = Param_ReadValue_8Bit(enPARAM8__NTSD_Speed);
      uint16_t uwFP_EPowerSpeed = Param_ReadValue_16Bit(enPARAM16__EPowerSpeed_fpm);
      uint8_t ucFP_AccessSpeed = Param_ReadValue_8Bit(enPARAM8__AccessSpeed_fpm);

      if ( ( uwFP_LearnSpeed < MIN_LEARN_SPEED )
        || ( uwFP_LearnSpeed > Param_ReadValue_16Bit(enPARAM16__ContractSpeed) )
         )
      {
         SetFault(FLT__INVALID_LEARN_SPD);
      }
      else if ( uwFP_TerminalSpeed > MAX_TERMINAL_SPEED )
      {
         SetFault(FLT__INVALID_TERM_SPD);
      }
      else if ( ( uwFP_LevelingSpeed > MAX_LEVELING_SPEED )
             || ( uwFP_LevelingSpeed < MIN_LEVELING_SPEED ) )
      {
         SetFault(FLT__INVALID_LEVEL_SPD);
      }
      else if ( ( ucFP_NTSD_Speed > MAX_NTSD_SPEED )
             || ( ucFP_NTSD_Speed < MIN_NTSD_SPEED ) )
      {
         SetFault(FLT__INVALID_NTSD_SPD);
      }
      else if ( ( uwFP_EPowerSpeed < MIN_CONTRACT_SPEED )
             || ( uwFP_EPowerSpeed > Param_ReadValue_16Bit(enPARAM16__ContractSpeed) )
         )
      {
         if( CheckIfInputIsProgrammed(enIN_EP_ON) )
         {
            SetFault(FLT__INVALID_EPWR_SPD);
         }
      }
      else if ( ucFP_AccessSpeed > MAX_INSP_SPEED )
      {
         SetFault(FLT__INVALID_ACCESS_SPD);
      }
   }
}
/*-----------------------------------------------------------------------------
   Checks for invalid critical parameter fault
   Set when a critical parameter has been changed:
      Number of floors
      Rear Doors
      Contract speed
      Drive Select
 -----------------------------------------------------------------------------*/
static void CheckFor_NeedToReset()
{
   static uint8_t bFirst = 1;
   static uint8_t ucDriveSelect;
   static uint32_t auiOpeningF[BITMAP32_SIZE(MAX_NUM_FLOORS)];
   static uint32_t auiOpeningR[BITMAP32_SIZE(MAX_NUM_FLOORS)];
   static uint8_t bBrakeV2;
   if(bFirst)
   {
      bFirst = 0;
      ucDriveSelect = Param_ReadValue_8Bit(enPARAM8__DriveSelect);
      for(uint8_t i = 0; i < BITMAP32_SIZE(MAX_NUM_FLOORS); i++)
      {
         auiOpeningF[i] = Param_ReadValue_32Bit( enPARAM32__OpeningBitmapF_0 + i );
         auiOpeningR[i] = Param_ReadValue_32Bit( enPARAM32__OpeningBitmapR_0 + i );
      }
      bBrakeV2 = Param_ReadValue_1Bit(enPARAM1__EnableBrakeV2);
   }
   else if( !GetMotion_RunFlag() )
   {
      if(GetFP_ContractSpeed() != Param_ReadValue_16Bit(enPARAM16__ContractSpeed))
      {
         /* Exception for Acceptance Test */
         if( GetOperation_AutoMode()  != MODE_A__TEST )
         {
            SetFault(FLT__NEED_TO_CYCLE_PWR_MR);
         }
      }
      else if(ucDriveSelect != Param_ReadValue_8Bit(enPARAM8__DriveSelect))
      {
         SetFault(FLT__NEED_TO_CYCLE_PWR_MR);
      }
      else if( bBrakeV2 != Param_ReadValue_1Bit(enPARAM1__EnableBrakeV2) )
      {
         SetFault(FLT__NEED_TO_CYCLE_PWR_MR);
      }
      else if( GetOperation_ManualMode() != MODE_M__CONSTRUCTION )
      {
         if(GetFP_RearDoors() != Param_ReadValue_1Bit(enPARAM1__NumDoors))
         {
            SetFault(FLT__NEED_TO_CYCLE_PWR_MR);
         }
         else if(GetFP_FreightDoors() != Param_ReadValue_1Bit(enPARAM1__Enable_Freight_Doors))
         {
            SetFault(FLT__NEED_TO_CYCLE_PWR_MR);
         }
         else if(GetFP_NumFloors() != Param_ReadValue_8Bit(enPARAM8__NumFloors))
         {
            SetFault(FLT__NEED_TO_CYCLE_PWR_MR);
         }
         else if(GetFP_DoorType(DOOR_FRONT) != Param_ReadValue_8Bit(enPARAM8__DoorTypeSelect_F))
         {
            SetFault(FLT__NEED_TO_CYCLE_PWR_MR);
         }
         else if(GetFP_DoorType(DOOR_REAR) != Param_ReadValue_8Bit(enPARAM8__DoorTypeSelect_R))
         {
            SetFault(FLT__NEED_TO_CYCLE_PWR_MR);
         }
         else
         {
            for(uint8_t i = 0; i < BITMAP32_SIZE(GetFP_NumFloors()); i++)
            {
               if(auiOpeningF[i] != Param_ReadValue_32Bit( enPARAM32__OpeningBitmapF_0 + i ) )
               {
                  SetFault(FLT__NEED_TO_CYCLE_PWR_MR);
                  break;
               }
               else if( GetFP_RearDoors() && ( auiOpeningR[i] != Param_ReadValue_32Bit( enPARAM32__OpeningBitmapR_0 + i ) ) )
               {
                  SetFault(FLT__NEED_TO_CYCLE_PWR_MR);
                  break;
               }
            }
         }
      }
   }
}
/*-----------------------------------------------------------------------------

   The fault enum is organized in such a way that only a (consecutive) section
   of that enum is able to clear all latched calls when the fault occurs.

 -----------------------------------------------------------------------------*/
static void CheckFor_ClearingCallsFaults( void )
{
   for( enum en_fault_nodes eFaultNode = enFAULT_NODE__CPLD; eFaultNode < NUM_FAULT_NODES; eFaultNode++ )
   {
      en_faults eFaultNum = GetFault_ByNode( eFaultNode );
      if( ( eFaultNum != FLT__NONE )
       && ( gabFaultType_ClearCalls[eFaultNum] == FT_CC__ON ) )
      {
         ClearLatchedCarCalls();
         ClearLatchedHallCalls();
         break;
      }
   }
}
/*-----------------------------------------------------------------------------
   CheckFor_OOS_HourlyLimit()

 -----------------------------------------------------------------------------*/
#define ONE_HOUR_TO_MSEC      (3600000U)
static void CheckFor_OOS_HourlyLimit()
{
   static uint32_t ulHourCounter_1ms;
   static en_faults eLastFault;
   uint8_t ucHourlyFaultLimit = Param_ReadValue_8Bit( enPARAM8__HourlyFaultLimit );
   if( ucHourlyFaultLimit < 5 )
   {
      ucHourlyFaultLimit = 5;
   }
   /* Check Hourly Limit */
   if( ( eLastFault == FLT__NONE )
    && ( gstFault.eFaultNumber != FLT__NONE )
    && ( GetFault_OOSType( gstFault.eFaultNumber ) == FT_OOS__ON ) )
   {
      if( ucHourlyFaultCount < ucHourlyFaultLimit)
      {
         ucHourlyFaultCount++;
      }
   }
   eLastFault = gstFault.eFaultNumber;

   /* If an hour has passed, clear fault counts */
   if(ulHourCounter_1ms >= ONE_HOUR_TO_MSEC)
   {
      ulHourCounter_1ms = 0;
      ucHourlyFaultCount = 0;
      if( GetOOSSubFault() == enOOS_HourlyLimit )
      {
         UnsetOOSFault(0);
      }
   }
   else
   {
      ulHourCounter_1ms += MOD_RUN_PERIOD_FAULT_1MS;
   }

   if( ucHourlyFaultCount >= ucHourlyFaultLimit )
   {
      SetOOSFault(enOOS_HourlyLimit);
   }
}
/*-----------------------------------------------------------------------------

   This functions is to check if any specific fault occured 3 consecutive
   times or 7 times within an hour. If so, the car should go out of service and
   require a manual reset.

   A consecutive fault is any same fault that reoccurs 3 times separated by only FLT__NONE
   Currently uses single hour timer for all faults
 -----------------------------------------------------------------------------*/

static void CheckFor_OOS_ConsecutiveLimit( void )
{
   static uint32_t ulHourCounter_1ms;
   static uint8_t bFallingEdge;              // Set when all faults are cleared
   static uint8_t ucHourlyFaultCount;
   static en_faults eLastFault;
   static en_faults eLoggedFault; // Fault being tracked
   if( gstFault.eFaultNumber )
   {
      if( ( eLastFault == FLT__NONE )
       && ( gstFault.eFaultNumber != FLT__NONE )
       && ( GetFault_OOSType( gstFault.eFaultNumber ) == FT_OOS__ON ) )
      {
         if( gstFault.eFaultNumber == eLoggedFault )
         {
            if( ucConsecutiveFaultCount < LIMIT_CONSECUTIVE_FAULTS-1 )
            {
               ucConsecutiveFaultCount++;
            }
         }
         else if(GetOOSFault())
         {
        	 // Do nothing
         }
         else
         {
            eLoggedFault = gstFault.eFaultNumber;

            ucConsecutiveFaultCount = 0;
         }
      }
   }
   if( (ulHourCounter_1ms >= ONE_HOUR_TO_MSEC)  && (!GetOOSFault()) )
   {
      ulHourCounter_1ms = 0;
      ucHourlyFaultCount = 0;
   }
   else
   {
      ulHourCounter_1ms += MOD_RUN_PERIOD_FAULT_1MS;
   }


   if( ucConsecutiveFaultCount >= LIMIT_CONSECUTIVE_FAULTS-1)
   {
	   SetOOSFault(enOOS_ConsecutiveLimit);
   }
   eLastFault = gstFault.eFaultNumber;
}
/*-----------------------------------------------------------------------------
   Monitors run flag to track the number of runs per minute.
   Moves the car to OOS if the count exceeds the preset limit.
   Every minute this counter is reset and the OOS state is cleared.
 -----------------------------------------------------------------------------*/
#define ONE_MINUTE_TO_MSEC      (60000U)
static void CheckFor_OOS_MaxStartsPerMin(void)
{
   static uint8_t bLastRunFlag;
   static uint32_t ulMinuteCounter_1ms;
   uint8_t ucStartsPerMinLimit = Param_ReadValue_8Bit(enPARAM8__MaxStartsPerMinute);
   if( ucStartsPerMinLimit
  && ( GetOperation_ClassOfOp() == CLASSOP__AUTO ) )
   {
      /* Monitor run flag to track number of runs per minute */
      uint8_t bRunFlag = GetMotion_RunFlag();
      if( !bRunFlag && bLastRunFlag )
      {
         if( ucStartsPerMinuteCount >= ucStartsPerMinLimit )
         {
            SetOOSFault(enOOS_MaxStartsPerMin);
            SetFault(FLT__MAX_RUNS_PER_MINUTE);
         }
         else
         {
            ucStartsPerMinuteCount++;
         }
      }
      bLastRunFlag = bRunFlag;

      /* If a minute has passed, clear the run counts */
      if( ulMinuteCounter_1ms >= ONE_MINUTE_TO_MSEC )
      {
         ulMinuteCounter_1ms = 0;
         ucStartsPerMinuteCount = 0;
         if( GetOOSSubFault() == enOOS_MaxStartsPerMin )
         {
            UnsetOOSFault(0);
         }
      }
      else
      {
         ulMinuteCounter_1ms += MOD_RUN_PERIOD_FAULT_1MS;
      }
   }
   else
   {
      ucStartsPerMinuteCount = 0;
      if( GetOOSSubFault() == enOOS_MaxStartsPerMin )
      {
         UnsetOOSFault(0);
      }
   }
}
/*-----------------------------------------------------------------------------
This function delays latching of GOV fault to avoid latching the fault during power down.
-----------------------------------------------------------------------------*/
#define DEBOUNCE_LATCHING_GOV_FAULT_MS    (2500)
#define INCREASED_DEBOUNCE_LATCHING_GOV_FAULT_MS    (6000)
static void UpdateLatchingGOVFault( void )
{
   static uint16_t uwDebounce_ms;
   if( GetFault_ByNumber(FLT__GOV) )
   {
      if(Param_ReadValue_1Bit(enPARAM1__DebounceLatchedFault))
      {
         if( uwDebounce_ms < INCREASED_DEBOUNCE_LATCHING_GOV_FAULT_MS )
         {
            uwDebounce_ms+= MOD_RUN_PERIOD_FAULT_1MS;
         }
         else
         {
            uwDebounce_ms = 0;
            SetLatchingSafetyFault( LSF__GOV );
         }
      }
      else
      {
         if( uwDebounce_ms < DEBOUNCE_LATCHING_GOV_FAULT_MS )
         {
            uwDebounce_ms+= MOD_RUN_PERIOD_FAULT_1MS;
         }
         else
         {
            uwDebounce_ms = 0;
            SetLatchingSafetyFault( LSF__GOV );
         }
      }
   }
   else
   {
      uwDebounce_ms = 0;
   }
}

/*-----------------------------------------------------------------------------

   This function checks for latching faults on power up, it only clears them
   if the specific reset button is pressed.

-----------------------------------------------------------------------------*/
static void CheckFor_LatchingFaults( void )
{
   UpdateLatchingGOVFault();
   for(uint8_t i = 0; i < NUM_LATCHING_SAFETY_FAULTS; i++)
   {
      switch(i)
      {
         case 0:
            if ( GetLatchingSafetyFault( LSF__EB1_DROPPED ) )
            {
              if ( SRU_Read_ResetButton( eRESET_BUTTON_E_BRAKE ) )
              {
                ClrLatchingSafetyFault( LSF__EB1_DROPPED );
              }
            }
            break;
         case 1:
               if ( GetLatchingSafetyFault( LSF__UNINTENDED_MOVEMENT ) )
               {
                  if ( SRU_Read_ResetButton( eRESET_BUTTON_E_BRAKE ) )
                  {
                     ClrLatchingSafetyFault( LSF__UNINTENDED_MOVEMENT );
                  }
                  else if( !GetFault_ByNumber(FLT__UNINTENDED_MOV) )
                  {
                     SetFault(FLT__UNINTENDED_MOV_L);
                  }
               }
            break;
         case 2:
               if ( GetLatchingSafetyFault( LSF__TRACTION_LOSS ) )
               {
                  if ( SRU_Read_ResetButton( eRESET_BUTTON_TRC_LOSS )
                    || GetInputValue(enIN_TLOSS_RESET) )
                  {
                     ClrLatchingSafetyFault( LSF__TRACTION_LOSS );
                  }
                  else if( !GetFault_ByNumber(FLT__TRACTION_LOSS) )
                  {
                     SetFault(FLT__TRACTION_LOSS_L);
                  }
               }
            break;
         case 3:
               if ( Param_ReadValue_1Bit( enPARAM1__EBrakeOnOverspeed ) )
               {
                  if ( GetLatchingSafetyFault( LSF__OVERSPEED ) )
                  {
                     if ( SRU_Read_ResetButton( eRESET_BUTTON_E_BRAKE ) )
                     {
                        ClrLatchingSafetyFault( LSF__OVERSPEED );
                     }
                     else if( !GetFault_ByNumber(FLT__OVERSPEED_GENERAL) )
                     {
                        SetFault(FLT__OVERSPEED_GENERAL_L);
                     }
                  }
               }
               break;
         case 4:
               if ( GetLatchingSafetyFault( LSF__GOV ) )
               {
                  if ( SRU_Read_ResetButton( eRESET_BUTTON_E_BRAKE ) )
                  {
                     ClrLatchingSafetyFault( LSF__GOV );
                  }
                  else if( !GetFault_ByNumber(FLT__GOV) )
                  {
                     SetFault(FLT__GOV_L);
                  }
               }
            break;
         default:
            break;
      }
   }
}
/*-----------------------------------------------------------------------------
   Sets a fault when a system parameter is outside the valid range
 -----------------------------------------------------------------------------*/
static void CheckFor_InvalidSetting()
{
   /* Check if parking floor/door is valid */
   uint8_t ucParkingFloor = Param_ReadValue_8Bit(enPARAM8__OverrideParkingFloor);
   if( Param_ReadValue_8Bit(enPARAM8__Parking_Timer_1sec) // Parking is on
    && !( GetFloorOpening_Front(ucParkingFloor) || GetFloorOpening_Rear(ucParkingFloor) ) )
   {
      SetFault(FLT__INVALID_PARKING);
   }

   /* Check if fire floor/door is valid */
   uint8_t ucMainFireRecallFloor = Param_ReadValue_8Bit(enPARAM8__FireRecallFloorM);
   uint8_t bMainFireRecallDoor = Param_ReadValue_1Bit(enPARAM1__FireRecallDoorM);
   if( ( bMainFireRecallDoor && !GetFloorOpening_Rear(ucMainFireRecallFloor) )
    || ( !bMainFireRecallDoor && !GetFloorOpening_Front(ucMainFireRecallFloor) ) )
   {
      SetFault(FLT__INVALID_FIRE_MAIN);
   }

   uint8_t ucAltFireRecallFloor = Param_ReadValue_8Bit(enPARAM8__FireRecallFloorA);
   uint8_t bAltFireRecallDoor = Param_ReadValue_1Bit(enPARAM1__FireRecallDoorA);
   if( ( bAltFireRecallDoor && !GetFloorOpening_Rear(ucAltFireRecallFloor) )
    || ( !bAltFireRecallDoor && !GetFloorOpening_Front(ucAltFireRecallFloor) ) )
   {
      SetFault(FLT__INVALID_FIRE_ALT);
   }

   if( !CheckIfAllDoorInputsInitialized() )
   {
      SetFault(FLT__DOOR_INVALID);
   }
}
/*----------------------------------------------------------------------------

    Called from mod_fault.c

 *----------------------------------------------------------------------------*/
void Run_ModFault_MRA( void )
{
   static uint32_t uiOOSCheckDelay_ms;
   UpdateActiveFaults();
   if(!GetMotion_RunFlag())
   {
      CheckFor_InvalidSpeed();
      CheckFor_NeedToReset();
      CheckFor_InvalidSetting();
   }
   CheckFor_LatchingFaults();
   CheckFor_ClearingCallsFaults();

   if( uiOOSCheckDelay_ms < OOS_STARTUP_CHECK_DELAY_MS )
   {
      uiOOSCheckDelay_ms += MOD_RUN_PERIOD_FAULT_1MS;
   }
   else if( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
         && ( GetOperation_AutoMode() != MODE_A__TEST ) )
   {
      CheckFor_OOS_HourlyLimit();
      CheckFor_OOS_ConsecutiveLimit();
      CheckFor_OOS_MaxStartsPerMin();
   }
   else
   {
      UnsetOOSFault(1);
      ucConsecutiveFaultCount = 0;
      ucHourlyFaultCount = 0;
      ucStartsPerMinuteCount = 0;
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
