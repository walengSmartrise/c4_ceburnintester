/******************************************************************************
 *
 * @file     mod_can_test.c
 * @brief
 * @version  V1.00
 * @date     30, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "sru_a.h"
#include "sys.h"
#include "position.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

extern RINGBUFF_T RxPositionRingBuffer;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define DELAY_POS_FWD_TO_B_5MS   (5)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static CAN_MSG_T stRxMsg;
static CAN_MSG_T stTxMsg;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
   Unload most recent element of the ring buffer.
   Discard the rest.
 -----------------------------------------------------------------------------*/
static uint8_t Unload_ForwardedPosition()
{
   Status bSuccess = ERROR;
   while ( RingBuffer_GetCount( &RxPositionRingBuffer ) > 0 )
   {
       RingBuffer_Pop( &RxPositionRingBuffer, &stRxMsg );
       int iCount = RingBuffer_GetCount( &RxPositionRingBuffer );
       if( iCount < 1 )
       {
          uint32_t uiPosition = stRxMsg.Data[0]
                              | (stRxMsg.Data[1] << 8)
                              | (stRxMsg.Data[2] << 16);
          int16_t wVelocity = (int16_t) ( stRxMsg.Data[4] | (stRxMsg.Data[5] << 8));
          SetPosition_PositionCount(uiPosition);
          SetPosition_Velocity(wVelocity);

          bSuccess = SUCCESS;
          break;
       }
   }
   return bSuccess;
}
/*-----------------------------------------------------------------------------
   Forward position to B processor
 -----------------------------------------------------------------------------*/
static void Load_ForwardedPosition()
{
   static uint16_t uwDelayCounter;
   /* Reduce transmission rate of position packets to B processor */
   if( ++uwDelayCounter >= DELAY_POS_FWD_TO_B_5MS )
   {
      uwDelayCounter = 0;
      uint32_t uiNewPosition = GetPosition_PositionCount();
      int16_t wNewVelocity = GetPosition_Velocity();
      stTxMsg.DLC = 8;
      stTxMsg.ID = POS_FWD_MSG_ID;
      stTxMsg.Data[0] = uiNewPosition & 0xFF;
      stTxMsg.Data[1] = (uiNewPosition >> 8) & 0xFF;
      stTxMsg.Data[2] = (uiNewPosition >> 16) & 0xFF;

      stTxMsg.Data[4] = wNewVelocity & 0xFF;
      stTxMsg.Data[5] = (wNewVelocity >> 8) & 0xFF;
      UART_LoadCANMessage(&stTxMsg);
   }
}
/*-----------------------------------------------------------------------------
   Unloads position an forwards to B processor
 -----------------------------------------------------------------------------*/
void Position_MRA()
{
   if(Unload_ForwardedPosition())
   {
      Load_ForwardedPosition();
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
