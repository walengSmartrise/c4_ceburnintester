/******************************************************************************
 *
 * @file     mod_capture.c
 * @brief    Control Logic for Capture Mode.
 * @version  V1.00
 * @date     4 August, 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"

#define TIMER_TIMEOUT_500ms (10)
#define CAPTURE_INPUT_DEBOUNCE_500MS      (2)
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

static enum en_Capture_Mode ucCaptureMode = CAPTURE_OFF;
static enum en_Capture_Mode ucLastCaptureMode = CAPTURE_OFF;
static uint8_t ucCaptureTimeout_500ms;

static uint8_t ucCaptureDestination = INVALID_FLOOR;

static enum en_classop eLastClassOfOperation;

enum en_Capture_Mode getCaptureMode ( void )
{
   return ucCaptureMode;
}
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Capture =
{
   .pfnInit = Init,
   .pfnRun = Run,
};


/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 500;
   pstThisModule->uwRunPeriod_1ms = 500;
   return 0;
}

static uint8_t CheckIfResetCaptureMode()
{
   return ( ( GetOperation_ClassOfOp() != CLASSOP__AUTO )
         || ( GetOperation_AutoMode() != MODE_A__NORMAL ) );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void ResetCaptureMode(void)
{
   ucCaptureTimeout_500ms = 0;
   ucCaptureMode = CAPTURE_OFF;
}
/*
 * StartCaptureMode (ucDestination)
 * ucDestination is used to send the car to a specific floor and open
 * its doors on arrival. if the floor is outside of the range of floors,
 * then StartCaptureMode will just stop when idle. if this is called
 * while running, it will extend the timer of the idle time to keep the
 * car captured.
 */
void StartCaptureMode (uint8_t ucDestination)
{
   if (ucCaptureMode == CAPTURE_OFF)
   {
      ucCaptureMode = CAPTURE_ON;
      /* If moving from inspection or starting up, immediately capture car without delay to empty car */
      if( eLastClassOfOperation != CLASSOP__AUTO )
      {
         ucCaptureTimeout_500ms = 0;
      }
      else
      {
         ucCaptureTimeout_500ms = TIMER_TIMEOUT_500ms;
      }
      ucCaptureDestination = ucDestination;
   }
   //If on idle mode, extend timeout.
   else if (ucCaptureMode == CAPTURE_IDLE)
   {
      ucCaptureTimeout_500ms = TIMER_TIMEOUT_500ms;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t ucLWDNextLearnFloor;
static uint8_t ucLWDLearnFloorRequest_Plus1;
uint8_t GetLWD_LearnFloorRequest_Plus1(void)
{
   return ucLWDLearnFloorRequest_Plus1;
}
uint8_t GetLWD_CaptureFlag(void)
{
   uint8_t bCapture = ( GetOperation_AutoMode() == MODE_A__NORMAL )
                   && ( ( GetLW_GetCalibrationState_MRB() != LWD_CALIB_STATE__NONE ) || ( GetLW_GetCalibrationState_CTB() != LWD_CALIB_STATE__NONE ) )
                   && ( Param_ReadValue_1Bit(enPARAM1__LWD_TriggerRecalibrate) || Param_ReadValue_1Bit(enPARAM1__LWD_TriggerLoadLearn) || Param_ReadValue_1Bit(enPARAM1__LWD_AutoRecalibrate) );
   return bCapture;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   uint8_t ucIdleTimeout = TIMER_TIMEOUT_500ms;
   uint8_t ucOnTimeout = TIMER_TIMEOUT_500ms;
   uint8_t bLEDState = 0;
   static uint8_t bLastDIP;
   static uint8_t ucBlinking = 0;
   /* Dip 3 - Disable doors */
   uint8_t bDIP3 = SRU_Read_DIP_Switch( enSRU_DIP_A3 ) || GetLWD_CaptureFlag();
   if ( bDIP3 )
   {
      StartCaptureMode(INVALID_FLOOR);
   }

   if ( CheckIfResetCaptureMode()
   || ( !bDIP3 && bLastDIP ) )
   {
      ResetCaptureMode();
   }
   else
   {
      static uint8_t ucCaptureDebounce_500ms;
      if ( GetInputValue( enIN_CAPT ) )
      {
         if(ucCaptureDebounce_500ms >= CAPTURE_INPUT_DEBOUNCE_500MS)
         {
            StartCaptureMode(INVALID_FLOOR);
         }
         else
         {
            ucCaptureDebounce_500ms++;
         }
      }
      /* Capture car at bottom terminal floor if LWD recalibration is requested */
      else if( GetLWD_CaptureFlag() )
      {
         if( ucCaptureMode == CAPTURE_IDLE )
         {
            en_lwd_calib_state eState_MRB = GetLW_GetCalibrationState_MRB();
            en_lwd_calib_state eState_CTB = GetLW_GetCalibrationState_CTB();
            if( ( eState_MRB == LWD_CALIB_STATE__MOVE_TO_BOTTOM ) || ( eState_CTB == LWD_CALIB_STATE__MOVE_TO_BOTTOM ) )
            {
               ucLWDLearnFloorRequest_Plus1 = 1;
               ucLWDNextLearnFloor = 0;
            }
            else if( ( eState_MRB == LWD_CALIB_STATE__MOVE_TO_NEXT ) || ( eState_CTB == LWD_CALIB_STATE__MOVE_TO_NEXT ) )
            {
               ucLWDLearnFloorRequest_Plus1 = ucLWDNextLearnFloor+1;
            }
            else if( ( eState_MRB == LWD_CALIB_STATE__LEARN_FLOOR ) || ( eState_CTB == LWD_CALIB_STATE__LEARN_FLOOR ) )
            {
               ucLWDNextLearnFloor = GetOperation_CurrentFloor()+1;
            }
            else
            {
               ucLWDLearnFloorRequest_Plus1 = 0;
               ucLWDNextLearnFloor = 0;
            }
         }
         else
         {
            ucLWDLearnFloorRequest_Plus1 = 0;
         }
      }
      else
      {
         ucCaptureDebounce_500ms = 0;
         ucLWDLearnFloorRequest_Plus1 = 0;
         ucLWDNextLearnFloor = 0;
      }

      /* No more Destinations */
      if ( ucCaptureMode != CAPTURE_OFF)
      {
         bLEDState = 1;

         if ( ( GetNextDestination_AnyDirection() == INVALID_FLOOR )
           && ( GetDoorState(DOOR_FRONT) != DOOR__OPEN )
           && ( GetDoorState(DOOR_REAR) != DOOR__OPEN ) )
         {
            if (ucCaptureTimeout_500ms)
            {
               ucCaptureTimeout_500ms--;
            }
         }
         else
         {
            if ( ucCaptureMode == CAPTURE_ON )
            {
               ucCaptureTimeout_500ms = ucOnTimeout;
            }
            else
            {
               ucCaptureTimeout_500ms = ucIdleTimeout;
            }

         }

         if ( ucCaptureTimeout_500ms == 0 )
         {
            if ( ucCaptureMode == CAPTURE_ON )
            {

               ucBlinking = !ucBlinking;
               bLEDState = ucBlinking;

               if (
                     ( ucCaptureDestination  < GetFP_NumFloors() ) &&
                     ( ucCaptureDestination != GetOperation_CurrentFloor() )
                  )
               {
                  if( gpastFloors[ucCaptureDestination].bFrontOpening )
                  {
                     gpastCarCalls_F[ ucCaptureDestination ].bLatched = 1;
                  }
                  else if( gpastFloors[ucCaptureDestination].bRearOpening )
                  {
                     gpastCarCalls_R[ ucCaptureDestination ].bLatched = 1;
                  }
                  ucCaptureTimeout_500ms = ucIdleTimeout;
               }
               else
               {
                  ucCaptureMode = CAPTURE_IDLE;
                  ucCaptureTimeout_500ms = ucIdleTimeout;
               }
            }
            else
            {
               ucCaptureMode = CAPTURE_OFF;
            }
         }
         else if ( ucCaptureMode == CAPTURE_ON )
         {
            ucBlinking = !ucBlinking;
            bLEDState = ucBlinking;
         }
      }
   }

   SRU_Write_LED( enSRU_LED_Capture, bLEDState );

   ucLastCaptureMode = ucCaptureMode;
   bLastDIP = bDIP3;
   eLastClassOfOperation = GetOperation_ClassOfOp();
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
