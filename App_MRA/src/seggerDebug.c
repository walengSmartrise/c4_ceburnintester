/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     26, March 2016
 *
 * @note    Load local datagrams and selects datagrams for transmit
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sru_a.h"
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
#include "motion.h"
#include "seggerDebug.h"
#include "pattern.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   Records pattern data for a run at start of accel
 *----------------------------------------------------------------------------*/
void RTTLog_MotionStart( Motion_Control *pstMotionCtrl, Curve_Parameters *pstCurveParameters )
{
#if RTT_ENABLE_MOTION_LOGGING
   uint32_t uiRunDistance;
   if(GetPosition_PositionCount() >= GetMotion_Destination())
   {
      uiRunDistance = GetPosition_PositionCount() - GetMotion_Destination();
   }
   else
   {
      uiRunDistance = GetMotion_Destination() - GetPosition_PositionCount();
   }
   static uint8_t bTransmitPattern = 0;
   static uint8_t bAccel = 1;
   static uint8_t bDecel = 1;
   static uint16_t uwAccelIndex, uwDecelIndex;
   static enum en_motion_state ePrevMotionState;
   static uint32_t uiStartPosition;
   if(ePrevMotionState == MOTION__START_SEQUENCE
   && Motion_GetMotionState() == MOTION__ACCELERATING)
   {
      bAccel = 0;
      bDecel = 0;
      uwAccelIndex = 0;
      uwDecelIndex = pstMotionCtrl->stRunParameters.uiEndTime_JerkOutDecel-1;
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "SOR:\r\n");
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "D = %d\t%d\t%d\t%d\t%d\t\r\n",
                                                uiRunDistance,
                                                pstMotionCtrl->stRunParameters.uiRampUpDistance,
                                                pstMotionCtrl->stRunParameters.uiSlowdownDistance,
                                                pstMotionCtrl->stRunParameters.uiStartPosition_Cruise,
                                                pstMotionCtrl->stRunParameters.uiStartPosition_Decel);
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "Vmax = %d\r\n", pstMotionCtrl->uwMaxSpeedOfCurrentRun_FPM);
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "RunTimes = %d\t%d\t%d\t%d\t%d\t%d\t\r\n",
                                                pstMotionCtrl->stRunParameters.uiEndTime_JerkInAccel,
                                                pstMotionCtrl->stRunParameters.uiEndTime_ConstantAccel,
                                                pstMotionCtrl->stRunParameters.uiEndTime_JerkOutAccel,
                                                pstMotionCtrl->stRunParameters.uiEndTime_JerkOutDecel,
                                                pstMotionCtrl->stRunParameters.uiEndTime_ConstantDecel,
                                                pstMotionCtrl->stRunParameters.uiEndTime_JerkInDecel);
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "Parameters = %d\t%d\t%d\t%d\t%d\t%d\t\r\n",
                                 (uint32_t)pstCurveParameters->fAccel,
                                 (uint32_t)pstCurveParameters->fJerkInAccel,
                                 (uint32_t)pstCurveParameters->fJerkOutAccel,
                                 (uint32_t)pstCurveParameters->fDecel,
                                 (uint32_t)pstCurveParameters->fJerkInDecel,
                                 (uint32_t)pstCurveParameters->fJerkOutDecel);
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "Leveling = %d\t%d\r\n", pstCurveParameters->uwLevelingSpeed_FPM, pstCurveParameters->uwLevelingDistance);
      uiStartPosition = GetPosition_PositionCount();
   }
   else if(ePrevMotionState == MOTION__START_SEQUENCE
        && Motion_GetMotionState() == MOTION__MANUAL_RUN)
   {
      bAccel = 0;
      bDecel = 0;
      uwAccelIndex = 0;
      uwDecelIndex = pstMotionCtrl->stRunParameters.uiEndTime_JerkOutDecel-1;
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "SOR:\r\n");
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "D = %d\r\n", 0);
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "Vmax = %d\r\n", pstMotionCtrl->uwMaxSpeedOfCurrentRun_FPM);
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "Parameters = %d\t%d\t%d\t%d\t%d\t%d\t\r\n", (uint32_t)pstCurveParameters->fAccel, (uint32_t)pstCurveParameters->fJerkInAccel, (uint32_t)pstCurveParameters->fJerkOutAccel, (uint32_t)pstCurveParameters->fDecel, (uint32_t)pstCurveParameters->fJerkInDecel, (uint32_t)pstCurveParameters->fJerkOutDecel);
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "Leveling = %d\t%d\r\n", pstCurveParameters->uwLevelingSpeed_FPM, pstCurveParameters->uwLevelingDistance);
      uiStartPosition = GetPosition_PositionCount();
   }
#if RTT_ENABLE_OUTPUT_GENERATED_PATTERN
   if(!bAccel)
   {
      uint32_t uiAbsolutePosition;
      if(gstMotion.cDirection == DIR__UP)
      {
         uiAbsolutePosition = uiStartPosition + Pattern_GetAccelPosition(uwAccelIndex);
      }
      else
      {
         uiAbsolutePosition = uiStartPosition - Pattern_GetAccelPosition(uwAccelIndex);
      }
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", uwAccelIndex);
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", uiAbsolutePosition);
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetPosition_Velocity());
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\r\n", Pattern_GetAccelCommandSpeed(uwAccelIndex));//gauwAccelPattern_Speed[uwAccelIndex]);
      if(++uwAccelIndex >= pstMotionCtrl->stRunParameters.uiEndTime_JerkOutAccel-1)
      {
         bAccel = 1;
      }
   }
   else if(!bDecel)
   {
      uint32_t uiAbsolutePosition;
      if(gstMotion.cDirection == DIR__UP)
      {
         uiAbsolutePosition = pstMotionCtrl->stRunParameters.uiStartPosition_Decel + Pattern_GetDecelPosition(pstMotionCtrl->stRunParameters.uiEndTime_JerkOutDecel - uwDecelIndex);
      }
      else
      {
         uiAbsolutePosition = pstMotionCtrl->stRunParameters.uiStartPosition_Decel - Pattern_GetDecelPosition(pstMotionCtrl->stRunParameters.uiEndTime_JerkOutDecel - uwDecelIndex);
      }
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", uwDecelIndex);
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", uiAbsolutePosition);
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetPosition_Velocity());
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\r\n", Pattern_GetDecelCommandSpeed(uwDecelIndex));
      if(--uwDecelIndex < 1 )
      {
         bDecel = 1;
      }
   }
#endif

   ePrevMotionState = pstMotionCtrl->eMotionState;
#endif
}

void RTTLog_InMotion( Motion_Control *pstMotionCtrl )
{
#if (!RTT_LOG_OUTPUT_PATTERN && RTT_ENABLE_MOTION_LOGGING)
   static uint8_t ucDelay = 0;
   if(++ucDelay>=RTT_OUTPUT_RATE_5MS)
   {
      ucDelay = 0;
      enum en_motion_state eState = Motion_GetMotionState();
      if( eState == MOTION__ACCELERATING )
      {
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", pstMotionCtrl->uiPrevTime );
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", pstMotionCtrl->uiPrevTime_Added );
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetPosition_PositionCount());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetPosition_Velocity());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetMotion_SpeedCommand());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetDriveSpeedFeedback());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "A\t\r\n");
      }
      else if( eState == MOTION__CRUISING )
      {
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", pstMotionCtrl->uiPrevTime );
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", pstMotionCtrl->uiPrevTime_Added );
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetPosition_PositionCount());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetPosition_Velocity());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetMotion_SpeedCommand());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetDriveSpeedFeedback());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "C\t\r\n");
      }
      else if( eState == MOTION__DECELERATING )
      {
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", pstMotionCtrl->uiPrevTime );
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", pstMotionCtrl->uiPrevTime_Added );
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetPosition_PositionCount());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetPosition_Velocity());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetMotion_SpeedCommand());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetDriveSpeedFeedback());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "D\t\r\n");
      }
      else if( eState == MOTION__RSL_DECEL )
      {
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", pstMotionCtrl->uiPrevTime );
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", pstMotionCtrl->uiPrevTime_RSLDecel );
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetPosition_PositionCount());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetPosition_Velocity());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetMotion_SpeedCommand());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetDriveSpeedFeedback());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "R\t\r\n");
      }
      else if( eState == MOTION__MANUAL_RUN )
      {
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", pstMotionCtrl->uiRunTimeCounter );
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", pstMotionCtrl->uiPrevTime_Added );
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetPosition_PositionCount());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetPosition_Velocity());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetMotion_SpeedCommand());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%d\t", GetDriveSpeedFeedback());
         SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "M\t\r\n");
      }
   }
#endif
}
/*----------------------------------------------------------------------------
   For logging the 1ms timestamp of an event.
      Passed a two char string as a tag for the event.
 *----------------------------------------------------------------------------*/
void RTTLog_TimeStampEvent( char *ps )
{
#if RTT_ENABLE_BRAKE_COM_LOGGING
   static uint8_t bStartupComplete = 0;
   if(!bStartupComplete)
   {
      if(Sys_GetTickCount(enTickCount_1s) > 5)
      {
         bStartupComplete = 1;
      }
   }
   else
   {
      SEGGER_RTT_printf(SEGGER_CHANNEL_IN_USE, "%c%c\t%d\r\n", ps[0], ps[1], Sys_GetTickCount(enTickCount_1ms));
   }
#endif
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
