/******************************************************************************
 *
 * @file     mod_flood_oper.c
 * @brief    Logic that controls how flood operation work.
 * @version  V1.00
 * @date     1, March 2018
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "GlobalData.h"
#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Flood_Op = { .pfnInit = Init, .pfnRun = Run, };

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define FLOOD_INPUT_DEBOUNCE__200MS    (5)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
/* Add single cycle delay before floor selection, otherwise reachable floor assessment will be
 * calculated prior to speed limit change  */
static uint8_t uClosestOpening = 0xff;

static uint8_t bFloodActive;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t Flood_GetActiveFlag(void)
{
   return bFloodActive;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t GetClosestValidOpening_Flood( enum direction_enum eDir, enum en_doors eDoor )
{
   uint8_t ucClosestFloor = INVALID_FLOOR;
   uint32_t uiCurrentPos = GetPosition_PositionCount();
   uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
   if( ucCurrentFloor < GetFP_NumFloors() )
   {
      if( eDir == DIR__UP )
      {
         ucCurrentFloor = Motion_GetNextReachableFloor();
         for( uint8_t ucFloor = ucCurrentFloor; ucFloor < GetFP_NumFloors(); ucFloor++ )
         {
            if( ( uiCurrentPos <= gpastFloors[ucFloor].ulPosition ) )
            {
               if( ( ( ( eDoor == DOOR_FRONT ) || ( eDoor == DOOR_ANY ) )&& gpastFloors[ucFloor].bFrontOpening )
                || ( ( ( eDoor == DOOR_REAR ) || ( eDoor == DOOR_ANY ) ) && gpastFloors[ucFloor].bRearOpening ) )
               {
                  if( ucFloor > (Param_ReadValue_8Bit(enPARAM8__FLOOD_NumFloors_Plus1) - 1) )//TODO add guards and retest
                  {
                     ucClosestFloor = ucFloor;
                     break;
                  }
               }
            }
         }
      }
      else if( eDir == DIR__DN )
      {
         ucCurrentFloor = Motion_GetNextReachableFloor();
         for( uint8_t ucFloor_Plus1 = ucCurrentFloor; ucFloor_Plus1 > 0; ucFloor_Plus1-- )
         {
            if( ( uiCurrentPos >= gpastFloors[ucFloor_Plus1].ulPosition ) )
            {
               if( ( ( ( eDoor == DOOR_FRONT ) || ( eDoor == DOOR_ANY ) ) && gpastFloors[ucFloor_Plus1].bFrontOpening )
                || ( ( ( eDoor == DOOR_REAR ) || ( eDoor == DOOR_ANY ) ) && gpastFloors[ucFloor_Plus1].bRearOpening ) )
               {
                  if( ucFloor_Plus1 > (Param_ReadValue_8Bit(enPARAM8__FLOOD_NumFloors_Plus1) - 1)) //TODO add guards and retest
                  {
                     ucClosestFloor = ucFloor_Plus1;
                     break;
                  }
               }
            }
         }
      }
      else
      {
         uint32_t uiAbovePosDiff = 0xFFFFFFFF;
         uint32_t uiBelowPosDiff = 0xFFFFFFFF;
         uint8_t ucAboveFloor = INVALID_FLOOR;
         uint8_t ucBelowFloor = INVALID_FLOOR;
         for( uint8_t ucFloor = ucCurrentFloor; ucFloor < GetFP_NumFloors(); ucFloor++ )
         {
            if( ( uiCurrentPos <= gpastFloors[ucFloor].ulPosition ) )
            {
               if( ( ( ( eDoor == DOOR_FRONT ) || ( eDoor == DOOR_ANY ) ) && gpastFloors[ucFloor].bFrontOpening )
                || ( ( ( eDoor == DOOR_REAR ) || ( eDoor == DOOR_ANY ) ) && gpastFloors[ucFloor].bRearOpening ) )
               {
                  if( ucFloor > (Param_ReadValue_8Bit(enPARAM8__FLOOD_NumFloors_Plus1) - 1))//TODO add guards and retest
                  {
                     ucAboveFloor = ucFloor;
                     uiAbovePosDiff = gpastFloors[ucFloor].ulPosition - uiCurrentPos;
                     break;
                  }
               }
            }
         }
         for( uint8_t ucFloor_Plus1 = ucCurrentFloor; ucFloor_Plus1 > 0; ucFloor_Plus1-- )
         {
            if( ( uiCurrentPos >= gpastFloors[ucFloor_Plus1].ulPosition ) )
            {
               if( ( ( ( eDoor == DOOR_FRONT ) || ( eDoor == DOOR_ANY ) ) && gpastFloors[ucFloor_Plus1].bFrontOpening )
                || ( ( ( eDoor == DOOR_REAR ) || ( eDoor == DOOR_ANY ) ) && gpastFloors[ucFloor_Plus1].bRearOpening ) )
               {
                  if( ucFloor_Plus1 > (Param_ReadValue_8Bit(enPARAM8__FLOOD_NumFloors_Plus1) - 1))//TODO add guards and retest
                  {
                     ucBelowFloor = ucFloor_Plus1;
                     uiBelowPosDiff = uiCurrentPos - gpastFloors[ucFloor_Plus1].ulPosition;
                     break;
                  }

               }
            }
         }

         ucClosestFloor = (uiAbovePosDiff < uiBelowPosDiff) ? ucAboveFloor:ucBelowFloor;
      }
   }
   if( (Param_ReadValue_8Bit(enPARAM8__FLOOD_NumFloors_Plus1) - 1) )
   {
      ucClosestFloor = ucCurrentFloor;
   }

   return ucClosestFloor;
}


void Auto_Flood(uint8_t bInit)
{
   uint8_t uFloodFloor = Param_ReadValue_8Bit(enPARAM8__FLOOD_NumFloors_Plus1);

   uint8_t bOkayToRun = Param_ReadValue_1Bit(enPARAM1__FLOOD_OkayToRun);

   Auto_Set_Limits_Flood();

   ClearLatchedCarCalls_Flood();
   ClearLatchedHallCalls_Flood();

   if(bInit)
   {
      ResetRecallState();
      if(gpastFloors[GetOperation_CurrentFloor()].bFrontOpening)
      {
         SetDoorCommand(DOOR_FRONT, DOOR_COMMAND__CLOSE);
      }

      if(gpastFloors[GetOperation_CurrentFloor()].bRearOpening)
      {
         SetDoorCommand(DOOR_REAR, DOOR_COMMAND__CLOSE);
      }

      uClosestOpening = GetClosestValidOpening_Flood( GetMotion_Direction() , DOOR_ANY);
   }

   if( (uClosestOpening == INVALID_FLOOR) && GetMotion_RunFlag())
   {
      SetOperation_MotionCmd(MOCMD__EMERG_STOP);
      if(Param_ReadValue_1Bit(enPARAM1__EnableEStopAlarms))
      {
         SetAlarm(ALM__ESTOP_FLOOD);
      }
      uClosestOpening = uFloodFloor;
   }
   else if( GetPosition_PositionCount() < gpastFloors[uFloodFloor].ulPosition)
   {
      uClosestOpening = uFloodFloor;
   }

   if(bOkayToRun)
   {
      if((GetOperation_CurrentFloor() < uFloodFloor))
      {
         Auto_Recall(bInit, uClosestOpening, RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR);
      }

      else
      {
         Auto_SetSpeed(GetFP_ContractSpeed());
         Auto_Oper(bInit);
      }
   }
   else
   {
      if((GetOperation_CurrentFloor() < uFloodFloor)
      || (!getDoorZone( DOOR_ANY )) )
      {
         Auto_Recall(bInit, uClosestOpening, RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR);
      }
      else if( Auto_Recall(bInit, uClosestOpening, RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR))
      {
         SetFault(FLT__FLOOD_OOS);
      }
   }
}

/*-----------------------------------------------------------------------------

 Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 1500;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_FLOOD_1MS;

   return 0;
}

/*-----------------------------------------------------------------------------
Earthquake Specifications from Functional Test Procedures Document for 2.41c

Bring the car down to the bottom landing and activate the flood sensor. Verify the car raises up the landings denoted in Parameter 13-148

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static uint8_t ucDebounce_200ms;
   uint8_t bFlood = GetInputValue(enIN_FLOOD);
   if( bFloodActive != bFlood )
   {
      if( ++ucDebounce_200ms >= FLOOD_INPUT_DEBOUNCE__200MS )
      {
         ucDebounce_200ms = 0;
         bFloodActive = bFlood;
      }
   }
   else
   {
      ucDebounce_200ms = 0;
   }

   SetOutputValue(enOUT_LMP_FLOOD, bFloodActive );


   if( bFloodActive )
   {
      SetAlarm(ALM__FLOOD_SWITCH);
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
