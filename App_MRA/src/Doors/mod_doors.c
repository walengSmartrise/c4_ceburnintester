/******************************************************************************
 *
 * @file     mod_doors.c
 * @brief    Door logic
 * @version  V2.00
 * @date     16 January 2017
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sru_a.h"
#include "sys.h"
#include "mod.h"
#include "GlobalData.h"

#include "position.h"
#include "motion.h"
#include "operation.h"
#include "doors_private.h"
#include "automatic_operation.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

static uint8_t CheckIf_DoorsKilled(void);
// The set functions are used to set the controller outputs for the door operator
static void SetAllDoorOutputsOff ( enum en_doors enDoorIndex );

static enum en_door_states DecideDoorState( enum en_doors enDoorIndex );

static void processDoorCloseRequest(enum en_doors enDoorIndex, enum en_door_states enDoorState);
static void processDoorOpenRequest(enum en_doors enDoorIndex, enum en_door_commands enDoorCommand);
static void processDoorCommandNone(enum en_doors enDoorIndex);
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_module gstMod_Doors =
{
      .pfnInit = Init,
      .pfnRun = Run,
};

en_door_type aenDoorType[NUM_OF_DOORS];

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *--------------------------------------------------------s--------------------*/
// Lock clip time in 10ms counts, module runs at 100ms count.
#define LOCK_CLIP_TIME_SCALE_FACTOR             (10)
#define DOOR_TIMER_LIMIT_100MS                  (6000)//10 mins
#define LOCK_EXPECTED_CLOSED_DEBOUNCE_100MS     (30)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
struct st_DoorBuildup {
      /* Door Inputs */
      enum en_input_functions DOL;
      enum en_input_functions DCL;
      enum en_input_functions PHE;
      enum en_input_functions GSW;
      enum en_input_functions DPM;

      enum en_input_functions BLOCK;
      enum en_input_functions MLOCK;
      enum en_input_functions TLOCK;
      enum en_input_functions DZ;

      enum en_input_functions BCL;
      enum en_input_functions MCL;
      enum en_input_functions TCL;

      enum en_input_functions SAFETY_EDGE;

      enum en_input_functions PHE2;

      /* Door Outputs */
      enum en_output_functions DO;
      enum en_output_functions DC;
      enum en_output_functions CAM;
      enum en_output_functions DCP;
      enum en_output_functions GATE_RELEASE;
      enum en_output_functions NUDGE;

      enum en_output_functions FREIGHT_DCM;
      enum en_output_functions FREIGHT_BUZZER;
      enum en_output_functions FREIGHT_TEST;
};

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t ucNumberOfDoors = 0;
static uint8_t bCarDoorsBypassed = 0;
static uint8_t bHallDoorsBypassed = 0;
static uint8_t bNoDemandDoorsOpen;
static struct st_door_control stDoorState[NUM_OF_DOORS];

const struct st_DoorBuildup stDoorSignals[NUM_OF_DOORS]  = {
      {
            enIN_DOL_F,
            enIN_DCL_F,
            enIN_PHE_F,
            enIN_GSWF,
            enIN_DPM_F,
            enIN_LBF,
            enIN_LMF,
            enIN_LTF,
            enIN_DZ_F,
            enIN_BCL_F,
            enIN_MCL_F,
            enIN_TCL_F,
            enIN_SafetyEdge_F,
            enIN_PHE_F2,

            enOUT_DO_F,
            enOUT_DC_F,
            enOUT_CAM_F,
            enOUT_DCP_F,
            enOUT_GATE_RELEASE_F,
            enOUT_NDG_F,
            enOUT_FreightDoor_DCM_F,
            enOUT_FreightDoor_Buzzer_F,
            enOUT_FreightDoor_Test_F,
      },
      {
            enIN_DOL_R,
            enIN_DCL_R,
            enIN_PHE_R,
            enIN_GSWR,
            enIN_DPM_R,
            enIN_LBR,
            enIN_LMR,
            enIN_LTR,
            enIN_DZ_R,
            enIN_BCL_R,
            enIN_MCL_R,
            enIN_TCL_R,
            enIN_SafetyEdge_R,
            enIN_PHE_R2,
            
            enOUT_DO_R,
            enOUT_DC_R,
            enOUT_CAM_R,
            enOUT_DCP_R,
            enOUT_GATE_RELEASE_R,
            enOUT_NDG_R,
            enOUT_FreightDoor_DCM_R,
            enOUT_FreightDoor_Buzzer_R,
            enOUT_FreightDoor_Test_R,
      }
};

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   @fn  uint8_t Doors_CheckIfManualSwingDoor( enum en_doors enDoorIndex, uint8_t ucFloorIndex )
   @return Returns 1 if the opening has a manual swing hall door
   @brief This function checks if the opening has a manual swing hall door
 *----------------------------------------------------------------------------*/
uint8_t Doors_CheckIfManualSwingDoor( enum en_doors enDoorIndex, uint8_t ucFloorIndex )
{
   uint8_t bSwingDoor = 0;
   if( aenDoorType[enDoorIndex] == DOOR_TYPE__SWING )
   {
      if( ucFloorIndex >= GetFP_NumFloors() ) // Any floor keyword
      {
         bSwingDoor = 1;
      }
      else
      {
         uint8_t ucAddressOffset = ( ucFloorIndex / NUM_BITS_PER_BITMAP16 );
         uint8_t ucBitIndex = ucFloorIndex % NUM_BITS_PER_BITMAP16;
         enum en_16bit_params eParamAddress = ( enDoorIndex == DOOR_FRONT )
                                            ? ( enPARAM16__SwingDoorOpening_F_0 )
                                            : ( enPARAM16__SwingDoorOpening_R_0 );
         eParamAddress += ucAddressOffset;
         uint16_t uwBitMap = Param_ReadValue_16Bit(eParamAddress);
         bSwingDoor = Sys_Bit_Get(&uwBitMap, ucBitIndex);
      }
   }
   else if( ( aenDoorType[enDoorIndex] == DOOR_TYPE__FREIGHT ) || ( aenDoorType[enDoorIndex] == DOOR_TYPE__MANUAL ) )
   {
      bSwingDoor = 1;
   }

   return bSwingDoor;
}
/*----------------------------------------------------------------------------
   @fn static uint8_t Doors_CheckIfSwingLocksExpectedClosed( enum en_doors enDoorIndex )
   @return Returns 1 if locks should be expected closed
   @brief This function checks to see if locks should be expected closed for jobs with swing hall doors.
 *----------------------------------------------------------------------------*/
uint8_t Doors_CheckIfSwingLocksExpectedClosed( enum en_doors enDoorIndex )
{
   uint8_t bLocksExpectedClosed = GetGSW(enDoorIndex) || !GetDZ(enDoorIndex);
   if( Doors_CheckIfManualSwingDoor(enDoorIndex, GetOperation_CurrentFloor()) )
   {
      if( Param_ReadValue_1Bit(enPARAM1__Door_FixedHallCAM ) )
      {
         bLocksExpectedClosed = !GetDZ(enDoorIndex);
      }
      else if( Param_ReadValue_1Bit(enPARAM1__Door_RetiringCAM ) )
      {
         bLocksExpectedClosed = GetMotion_RunFlag() || GetCAM(enDoorIndex) || !GetDZ(enDoorIndex);
         /* Option to suppress CAM output when on hoistway access */
         if( Param_ReadValue_1Bit(enPARAM1__DISA_CAM_ON_HA) )
         {
            if( GetOperation_ManualMode() == MODE_M__INSP_HA_TOP )
            {
               if( Param_ReadValue_8Bit(enPARAM8__HA_TopOpening) == enDoorIndex )
               {
                  bLocksExpectedClosed = 0;
               }
            }
            else if( GetOperation_ManualMode() == MODE_M__INSP_HA_BOTTOM )
            {
               if( Param_ReadValue_8Bit(enPARAM8__HA_BottomOpening) == enDoorIndex )
               {
                  bLocksExpectedClosed = 0;
               }
            }
         }
      }
      else /* Door operator controls interlocks */
      {
         bLocksExpectedClosed = GetGSW(enDoorIndex) || !GetDZ(enDoorIndex);
      }
   }

   return bLocksExpectedClosed;
}

/*----------------------------------------------------------------------------
   View Debug Data diagnostics
      Bits  0 to  4: Current Door Command (enum en_door_commands)
      Bits  5 to  9: Last Door Command (enum en_door_commands)
      Bits 10 to 20: Current Timer 200MS
      Bits 21 to 31: Timer Limit 200MS
 *----------------------------------------------------------------------------*/
uint32_t Door_GetDebugDoorData( enum en_doors enDoorIndex )
{
   uint32_t uiData = 0;
   uiData |= ( stDoorState[enDoorIndex].enDoorCommand & 0x1F ) << 0;
   uiData |= ( stDoorState[enDoorIndex].enLastDoorCommand & 0x1F ) << 5;
   uiData |= ( ( stDoorState[enDoorIndex].uwCurrentTimer_100ms / 2 ) & 0x7FF ) << 10;
   uiData |= ( ( stDoorState[enDoorIndex].uwTimer_100ms / 2 ) & 0x7FF ) << 21;
   return uiData;
}
/*----------------------------------------------------------------------------
 *
 * Access Functions - START
 *
 *----------------------------------------------------------------------------*/
//Setting the command to the door, these will be accepted (1) or rejected (0) based on door states and rules at the time
uint8_t SetDoorCommand( enum en_doors enDoorIndex, enum en_door_commands enDoorCommand )
{
   uint8_t bCommandAccepted = 0;
   if( (enDoorIndex < ucNumberOfDoors)
    && (aenDoorType[enDoorIndex] != DOOR_TYPE__MANUAL) )
   {
      uint8_t bFireBypassesPHE = ( Fire_CheckIfFire2DoorsEnabled() || Fire_CheckIfFire1Recall() ) && !Param_ReadValue_1Bit(enPARAM1__Fire__EnablePHE_OnPhase2);
      uint8_t bNeedToOpen = !GetGSW(enDoorIndex);
      bNeedToOpen &= ( ( aenDoorType[enDoorIndex] == DOOR_TYPE__FREIGHT ) && !GetSafetyEdge(enDoorIndex) )
                    || ( !GetPhotoEye(enDoorIndex) && !bFireBypassesPHE );

      stDoorState[enDoorIndex].enDoorCommand = enDoorCommand;
      switch ( enDoorCommand ) {
         case DOOR_COMMAND__OPEN_UI_REQUEST:
         case DOOR_COMMAND__OPEN_IN_CAR_REQUEST:
         case DOOR_COMMAND__OPEN_HALL_REQUEST:
         case DOOR_COMMAND__OPEN_HOLD_REQUEST:
         case DOOR_COMMAND__OPEN_CONSTANT_PRESSURE:
         case DOOR_COMMAND__OPEN_ADA_MODE:
         case DOOR_COMMAND__OPEN_HOLD_DWELL_REQUEST:
         case DOOR_COMMAND__OPEN_SABBATH_MODE:
         case DOOR_COMMAND__OPEN_LOBBY_REQUEST:
            processDoorOpenRequest(enDoorIndex, enDoorCommand);
            break;

         case DOOR_COMMAND__CLOSE:
         case DOOR_COMMAND__CLOSE_CONSTANT_PRESSURE:
            /* This is to prevent a door close request from interrupting nudge operation */
            if( ( GetOperation_ClassOfOp() != CLASSOP__AUTO )
             || ( !bNeedToOpen )
             || ( GetDoorCloseButton(enDoorIndex) )
             || ( GetDoorState(enDoorIndex) != DOOR__OPEN ) )
            {
               processDoorCloseRequest(enDoorIndex, DOOR__CLOSING);
            }
            break;

         case DOOR_COMMAND__NUDGE:
         case DOOR_COMMAND__NUDGE_CONSTANT_PRESSURE:
            processDoorCloseRequest(enDoorIndex, DOOR__NUDGING);
            break;

         case DOOR_COMMAND__NONE:
            processDoorCommandNone(enDoorIndex);
            break;
         default: break;
      }
      stDoorState[enDoorIndex].enLastDoorCommand = stDoorState[enDoorIndex].enDoorCommand;
      bCommandAccepted = 1;
   }

   return bCommandAccepted;
}

void ResetDoors ( void )
{
   enum en_classop enClass = GetOperation_ClassOfOp();
   enum en_mode_auto enAutoMode = GetOperation_AutoMode();
   for (enum en_doors enDoorIndex = 0; enDoorIndex < ucNumberOfDoors; enDoorIndex++ ) {
      if( ( enClass == CLASSOP__AUTO ) && ( aenDoorType[enDoorIndex] != DOOR_TYPE__MANUAL) )
      {
         switch (stDoorState[enDoorIndex].enCurrentDoorState)
         {
            case DOOR__OPEN:
            case DOOR__OPENING:
               if ( AutoModeRules_GetDoorHoldFlag() ) {
                  SetDoorCommand( enDoorIndex, DOOR_COMMAND__OPEN_HOLD_REQUEST );
               }
               else {
                  SetDoorCommand( enDoorIndex, DOOR_COMMAND__OPEN_IN_CAR_REQUEST );
               }
               break;

            case DOOR__PARTIALLY_OPEN:
               if ( AutoModeRules_GetDoorHoldFlag() ) {
                  SetDoorCommand( enDoorIndex, DOOR_COMMAND__OPEN_HOLD_REQUEST );
               } else {
                  SetDoorCommand( enDoorIndex, DOOR_COMMAND__CLOSE );
               }
               break;

            default:
            case DOOR__CLOSING:
            case DOOR__NUDGING:
               break;
         }
      }
   }
}

/* This function will return 1 only if ALL active doors are in a safe condition.
 * Note: If used in automatic operation, InDestinationDZ flag must be set low prior to check */
uint8_t DoorsSafeAndReadyToRun ( void )
{
   uint8_t bSafe = 1;
   for (enum en_doors enDoorIndex = DOOR_FRONT; enDoorIndex < ucNumberOfDoors; enDoorIndex++ )
   {
      switch( aenDoorType[enDoorIndex] )
      {
         case DOOR_TYPE__AUTOMATIC:
         case DOOR_TYPE__FREIGHT:
         case DOOR_TYPE__SWING:
            if ( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
            {
               if( ( !IsDoorFullyClosed(enDoorIndex) )
                || ( stDoorState[enDoorIndex].enCurrentDoorState == DOOR__OPENING ) )
               {
                  bSafe = 0;
                  break;
               }
            }
            else
            {
               uint8_t bInDestDZ = InDestinationDoorzone();
               if ( !bInDestDZ )
               {
                  if( ( !IsDoorFullyClosed(enDoorIndex) )
                   || ( stDoorState[enDoorIndex].enCurrentDoorState == DOOR__OPENING ) )
                  {
                     bSafe = 0;
                     break;
                  }
               }
               else
               {
                  bSafe = 0;
                  break;
               }
            }
            break;
         case DOOR_TYPE__MANUAL:
            if ( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
            {
               if( !IsDoorFullyClosed(enDoorIndex) )
               {
                  bSafe = 0;
                  break;
               }
            }
            else
            {
               uint8_t bInDestDZ = InDestinationDoorzone();
               if ( !bInDestDZ )
               {
                  if( !IsDoorFullyClosed(enDoorIndex) )
                  {
                     bSafe = 0;
                     break;
                  }
               }
               else
               {
                  bSafe = 0;
                  break;
               }
            }
            break;
         default: bSafe = 0;
            break;
      }
   }

   return bSafe;
}
/* Hall door checks used pre run */
uint8_t AllHallDoorsClosed ( void ) {

   uint8_t bSafe = 1;
   if( !bHallDoorsBypassed )
   {
      for (enum en_doors enDoorIndex = DOOR_FRONT; enDoorIndex < ucNumberOfDoors; enDoorIndex++ )
      {
         uint8_t bLCK = GetLocks(enDoorIndex);
         uint8_t bClosedContacts = ( Door_GetSwingClosedContactsProgrammedFlag(enDoorIndex) ) ? GetCloseContacts(enDoorIndex):bLCK;
         switch( aenDoorType[enDoorIndex] )
         {
            case DOOR_TYPE__AUTOMATIC:
               if( !bLCK )
               {
                  bSafe = 0;
                  break;
               }
               break;
            case DOOR_TYPE__FREIGHT:
            case DOOR_TYPE__SWING:
            case DOOR_TYPE__MANUAL:
               if( !bClosedContacts || ( Doors_CheckIfSwingLocksExpectedClosed(enDoorIndex) && !bLCK ) )
               {
                  bSafe = 0;
                  break;
               }
               break;
            default: bSafe = 0;
               break;
         }
      }
   }

   return bSafe;
}
/* Inner car door checks used pre run */
uint8_t CarDoorsClosed ( void )
{
   uint8_t bSafe = 1;
   if( !bCarDoorsBypassed )
   {
      for (enum en_doors enDoorIndex = DOOR_FRONT; enDoorIndex < ucNumberOfDoors; enDoorIndex++ )
      {
         uint8_t bFullyClosed = 0;
         uint8_t bGSW = GetGSW(enDoorIndex);
         uint8_t bDPM = GetDPM(enDoorIndex);
         uint8_t bLCK = GetLocks(enDoorIndex);
         uint8_t bDCL = GetDCL(enDoorIndex);
         uint8_t bDOL = GetDOL(enDoorIndex);
         uint8_t bClosedContacts = GetCloseContacts(enDoorIndex) || !Door_GetSwingClosedContactsProgrammedFlag(enDoorIndex);
         switch( aenDoorType[enDoorIndex] )
         {
            case DOOR_TYPE__AUTOMATIC:
            case DOOR_TYPE__FREIGHT:
            case DOOR_TYPE__SWING:
               if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
               {
                  bFullyClosed = bGSW && bDPM;
                  if( !bFullyClosed )
                  {
                     bSafe = 0;
                     break;
                  }
               }
               else
               {
                  bFullyClosed = !bDCL && bDOL && bGSW && bDPM;
                  if( ( !bFullyClosed )
                   || ( stDoorState[enDoorIndex].enCurrentDoorState != DOOR__CLOSED ) )
//                   || ( ( stDoorState[enDoorIndex].enCurrentDoorState != DOOR__CLOSING ) && ( stDoorState[enDoorIndex].enCurrentDoorState != DOOR__CLOSED ) ) )// TODO review need
                  {
                     bSafe = 0;
                     break;
                  }
               }
               break;
            case DOOR_TYPE__MANUAL:
               if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
               {
                  if( !bGSW || !bDPM )
                  {
                     bSafe = 0;
                     break;
                  }
               }
               else if( !bGSW || !bDPM
                   || ( stDoorState[enDoorIndex].enCurrentDoorState != DOOR__CLOSED ) )
               {
                  bSafe = 0;
                  break;
               }
               break;
            default: bSafe = 0;
               break;
         }
      }
   }

   return bSafe;
}
/* Hall door checks used pre run and during inspection runs */
uint8_t AllHallDoorsClosed_Inspection( void ) {

   uint8_t bSafe = 1;
   if( !bHallDoorsBypassed )
   {
      for (enum en_doors enDoorIndex = DOOR_FRONT; enDoorIndex < ucNumberOfDoors; enDoorIndex++ )
      {
         uint8_t bLCK = GetLocks(enDoorIndex);
         uint8_t bClosedContacts = GetCloseContacts(enDoorIndex) || !Door_GetSwingClosedContactsProgrammedFlag(enDoorIndex);
         uint8_t bDZ = GetDZ(enDoorIndex);
         switch( aenDoorType[enDoorIndex] )
         {
            case DOOR_TYPE__AUTOMATIC:
               if( !bLCK )
               {
                  bSafe = 0;
                  break;
               }
               break;
            case DOOR_TYPE__FREIGHT:
            case DOOR_TYPE__SWING:
            case DOOR_TYPE__MANUAL:
               if( !bClosedContacts || ( !bDZ && !bLCK ) )
               {
                  bSafe = 0;
                  break;
               }
               break;
            default: bSafe = 0;
               break;
         }
      }
   }

   return bSafe;
}
/* Returns 1 if any door is opening or open. */
uint8_t AnyDoorOpen ( void ) {
   uint8_t bDoorOpen = 0;
   for (enum en_doors enDoorIndex = DOOR_FRONT; enDoorIndex < ucNumberOfDoors; enDoorIndex++ )
   {
      if( ( IsDoorFullyOpen(enDoorIndex) )
       || ( stDoorState[enDoorIndex].enCurrentDoorState == DOOR__OPENING )
       || ( stDoorState[enDoorIndex].enCurrentDoorState == DOOR__OPEN ) )
      {
         bDoorOpen = 1;
         break;
      }
   }

   return bDoorOpen;
}
/* Returns 1 if any door is fully open */
uint8_t AnyDoorFullyOpen ( void ) {
   uint8_t bDoorOpen = 0;
   for (enum en_doors enDoorIndex = DOOR_FRONT; enDoorIndex < ucNumberOfDoors; enDoorIndex++ ) {
      if ( IsDoorFullyOpen(enDoorIndex))
      {
         bDoorOpen = 1;
         break;
      }
   }
   return bDoorOpen;
}
/* Commands closed any open door that isn't already attempting to close */
void CloseAnyOpenDoor ( void )
{
   for (enum en_doors enDoorIndex = DOOR_FRONT; enDoorIndex < ucNumberOfDoors; enDoorIndex++ )
   {
      if(aenDoorType[enDoorIndex] != DOOR_TYPE__MANUAL)
      {
         enum en_door_states eState = GetDoorState(enDoorIndex);
         if ( ( !IsDoorFullyClosed(enDoorIndex) )
           && ( eState != DOOR__CLOSING )
           && ( eState != DOOR__CLOSED )
           && ( eState != DOOR__NUDGING ) )
         {
            SetDoorCommand(enDoorIndex, DOOR_COMMAND__CLOSE);
            break;
         }
      }
   }
}

enum en_door_commands GetLastDoorCommand( enum en_doors enDoorIndex )
{
   enum en_door_commands enDoorCommand = NUM_OF_DOOR_COMMANDS;
   if (enDoorIndex < ucNumberOfDoors) {
      enDoorCommand = stDoorState[enDoorIndex].enDoorCommand;
   }
   return enDoorCommand;
}

uint8_t GetNumberOfDoors ( void )
{
   return ucNumberOfDoors;
}

uint8_t getDoorZone (enum en_doors enDoorIndex) {

   uint8_t bDoorZone = 0;
   if (enDoorIndex == DOOR_ANY) {
      for (enDoorIndex = DOOR_FRONT; enDoorIndex < ucNumberOfDoors; enDoorIndex++){
         bDoorZone |= GetDZ( enDoorIndex );
      }
   } else if (enDoorIndex < ucNumberOfDoors) {
      bDoorZone = GetDZ( enDoorIndex );
   }
   return bDoorZone;
}
uint8_t GetDoorsFieldEnableFlag(void) {
   uint8_t bEnable = 1;
   for (enum en_doors enDoorIndex = DOOR_FRONT; enDoorIndex < ucNumberOfDoors; enDoorIndex++){
      enum en_door_states eState = GetDoorState(enDoorIndex);
      bEnable &= ( ( eState == DOOR__CLOSED ) || ( eState == DOOR__CLOSING ) );
   }
   return bEnable;
}

uint8_t Doors_GetBypassHallDoors()
{
   return bHallDoorsBypassed;
}
uint8_t Doors_GetBypassCarDoors()
{
   return bCarDoorsBypassed;
}
uint8_t Doors_GetNoDemandDoorsOpen(void)
{
   return bNoDemandDoorsOpen;
}
/*----------------------------------------------------------------------------
   A door open hold request will not be cleared until a new command is given.
   This function will clear out a door hold command
 *----------------------------------------------------------------------------*/
void Doors_ClearDoorHoldCommand(enum en_doors enDoorIndex)
{
   if(aenDoorType[enDoorIndex] != DOOR_TYPE__MANUAL) {
      if( GetLastDoorCommand(enDoorIndex) == DOOR_COMMAND__OPEN_HOLD_REQUEST )
      {
         SetDoorCommand(enDoorIndex, DOOR_COMMAND__NONE);
         stDoorState[enDoorIndex].uwTimer_100ms = stDoorState[enDoorIndex].uwCurrentTimer_100ms+1;
      }
   }
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint8_t Doors_GetBuzzerFlag()
{
   uint8_t bActive = 0;
   if(!Param_ReadValue_1Bit(enPARAM1__Doors_NudgeNoBuzzer))
   {
      for (enum en_doors enDoorIndex = 0; enDoorIndex < ucNumberOfDoors; enDoorIndex++ )
      {
         if( ( GetDoorState(enDoorIndex) == DOOR__NUDGING ) && (GetNextDestination_AnyDirection() != INVALID_FLOOR) )
         {
            bActive = 1;
            break;
         }
      }
   }
   if((GetOperation_AutoMode() == MODE_A__SABBATH && Param_ReadValue_8Bit(enPARAM8__SabbathClosingBuzzer_100ms)))
   {
      for(enum en_doors enDoorIndex = DOOR_FRONT; enDoorIndex < ucNumberOfDoors; enDoorIndex++)
      {
         if( stDoorState[enDoorIndex].enCurrentDoorState == DOOR__OPEN )
         {
            uint16_t uwRemainingDwellTime_100ms = ( stDoorState[enDoorIndex].uwCurrentTimer_100ms < stDoorState[enDoorIndex].uwTimer_100ms )
                                                ? ( stDoorState[enDoorIndex].uwTimer_100ms - stDoorState[enDoorIndex].uwCurrentTimer_100ms )
                                                : ( 0 );

            uint16_t uwCloseBuzzerLimit_100ms = Param_ReadValue_8Bit(enPARAM8__SabbathClosingBuzzer_100ms);
            if( uwRemainingDwellTime_100ms < uwCloseBuzzerLimit_100ms )
            {
               bActive = 1;
               break;
            }
         }
         else if( Param_ReadValue_1Bit(enPARAM1__Sabbath_EnableExtBuzzer) && ( stDoorState[enDoorIndex].enCurrentDoorState == DOOR__CLOSING ) )
         {
            bActive = 1;
            break;
         }
      }
   }
   return bActive;
}

uint8_t GetDoorOpenButton ( enum en_doors enDoorIndex )
{
   uint8_t bDOB = 0;
   switch (enDoorIndex)
   {
      case DOOR_FRONT:
         if( GetFloorOpening_Front(GetOperation_CurrentFloor()) )
         {
            if( ( !Param_ReadValue_1Bit(enPARAM1__DisableDOBSecuredFloor) )
                  || ( GetOperation_ClassOfOp() != CLASSOP__AUTO )
                  || ( !gpastCarCalls_F[GetOperation_CurrentFloor()].bSecured )
                  || ( gstCurrentModeRules[GetOperation_AutoMode()].bIgnoreCarCallSecurity ) )
            {
               bDOB  = (GetInputValue( enIN_DOB_F )?1:0);
               bDOB = ( GetOperation_AutoMode() == MODE_A__SABBATH ) ? 0:bDOB;
               bDOB |= (GetUIRequest_DoorControl_COPB() & DOOR_UI_CTRL__OPEN_F)?0x2:0;
               bDOB |= (GetUIRequest_DoorControl_CTB() & DOOR_UI_CTRL__OPEN_F)?0x4:0;
               bDOB |= (GetUIRequest_DoorControl_MRB() & DOOR_UI_CTRL__OPEN_F)?0x8:0;
            }

            /* Freight door type currently used for swing doors. When swing hall door is opened,
               Inner car doors need to open */
            if( ( ( aenDoorType[enDoorIndex] == DOOR_TYPE__FREIGHT ) || ( aenDoorType[enDoorIndex] == DOOR_TYPE__SWING ) )
             && ( GetOperation_ClassOfOp() == CLASSOP__AUTO ) )
            {
               uint8_t bBypassReopen = ( GetEmergencyBit( EmergencyBF_FirePhaseII_Active ) )
                                    && ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
                                    && ( !Param_ReadValue_1Bit(enPARAM1__Fire2SwingReopen) );
               if( !GetCloseContacts(enDoorIndex) && !bBypassReopen && Door_GetSwingClosedContactsProgrammedFlag(enDoorIndex) )
               {
                  bDOB |= 1;
               }
            }
         }
         break;

      case DOOR_REAR:
         if( GetFloorOpening_Rear(GetOperation_CurrentFloor()) )
         {
            if( ( !Param_ReadValue_1Bit(enPARAM1__DisableDOBSecuredFloor) )
                  || ( GetOperation_ClassOfOp() != CLASSOP__AUTO )
                  || ( !gpastCarCalls_R[GetOperation_CurrentFloor()].bSecured )
                  || ( gstCurrentModeRules[GetOperation_AutoMode()].bIgnoreCarCallSecurity )
                 )
            {
               if(Param_ReadValue_1Bit(enPARAM1__Disable_Rear_DOB) && GetOperation_AutoMode() == MODE_A__NORMAL)
               {
                  bDOB = 0;
               }
               else
               {
                  bDOB  = (GetInputValue(enIN_DOB_R)?1:0);
                  bDOB = ( GetOperation_AutoMode() == MODE_A__SABBATH ) ? 0:bDOB;
               }
               bDOB |= (GetUIRequest_DoorControl_COPB() & DOOR_UI_CTRL__OPEN_R)?0x2:0;
               bDOB |= (GetUIRequest_DoorControl_CTB() & DOOR_UI_CTRL__OPEN_R)?0x4:0;
               bDOB |= (GetUIRequest_DoorControl_MRB() & DOOR_UI_CTRL__OPEN_R)?0x8:0;

            }

            /* Freight door type currently used for swing doors. When swing hall door is opened,
               Inner car doors need to open */
            if( ( ( aenDoorType[enDoorIndex] == DOOR_TYPE__FREIGHT ) || ( aenDoorType[enDoorIndex] == DOOR_TYPE__SWING ) )
             && ( GetOperation_ClassOfOp() == CLASSOP__AUTO ) )
            {
               uint8_t bBypassReopen = ( GetEmergencyBit( EmergencyBF_FirePhaseII_Active ) )
                                    && ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
                                    && ( !Param_ReadValue_1Bit(enPARAM1__Fire2SwingReopen) );
               if( !GetCloseContacts(enDoorIndex) && !bBypassReopen && Door_GetSwingClosedContactsProgrammedFlag(enDoorIndex) )
               {
                  bDOB |= 1;
               }
            }
         }
         break;
   }

   return bDOB;
}

uint8_t GetDoorCloseButton ( enum en_doors enDoorIndex )
{
   uint8_t bDCB = 0;
   switch (enDoorIndex)
   {
      case DOOR_FRONT:
         bDCB =  ( GetInputValue( enIN_DCB_F ) ) ? 1:0;
         bDCB |= GetFF2_MomentaryCloseButton(DOOR_FRONT);
         bDCB = ( GetOperation_AutoMode() == MODE_A__SABBATH ) ? 0:bDCB;
         bDCB |= (GetUIRequest_DoorControl_MRB() & DOOR_UI_CTRL__NUDGE_F) ? 0x10:0;
         bDCB |= (GetUIRequest_DoorControl_CTB() & DOOR_UI_CTRL__NUDGE_F) ? 0x10:0;
         bDCB |= (GetUIRequest_DoorControl_COPB() & DOOR_UI_CTRL__NUDGE_F) ? 0x10:0;

         bDCB |= (GetUIRequest_DoorControl_MRB() & DOOR_UI_CTRL__CLOSE_F) ? 0x8:0;
         bDCB |= (GetUIRequest_DoorControl_CTB() & DOOR_UI_CTRL__CLOSE_F) ? 0x4:0;
         bDCB |= (GetUIRequest_DoorControl_COPB() & DOOR_UI_CTRL__CLOSE_F) ? 0x2:0;
         break;

      case DOOR_REAR:
         bDCB = ( GetInputValue( enIN_DCB_R ) ) ? 1:0;
         bDCB |= GetFF2_MomentaryCloseButton(DOOR_REAR);
         bDCB = ( GetOperation_AutoMode() == MODE_A__SABBATH ) ? 0:bDCB;
         bDCB |= (GetUIRequest_DoorControl_MRB() & DOOR_UI_CTRL__NUDGE_R) ? 0x10:0;
         bDCB |= (GetUIRequest_DoorControl_CTB() & DOOR_UI_CTRL__NUDGE_R) ? 0x10:0;
         bDCB |= (GetUIRequest_DoorControl_COPB() & DOOR_UI_CTRL__NUDGE_R) ? 0x10:0;

         bDCB |= (GetUIRequest_DoorControl_MRB() &  DOOR_UI_CTRL__CLOSE_R) ? 0x8:0;
         bDCB |= (GetUIRequest_DoorControl_CTB() &  DOOR_UI_CTRL__CLOSE_R) ? 0x4:0;
         bDCB |= (GetUIRequest_DoorControl_COPB() &  DOOR_UI_CTRL__CLOSE_R) ? 0x2:0;
         break;
   }

   return bDCB;
}

//Door state return
inline enum en_door_states GetDoorState( enum en_doors enDoorIndex )
{
   enum en_door_states enDoorState = DOOR__UNKNOWN;
   if (enDoorIndex < ucNumberOfDoors) {
      enDoorState = stDoorState[enDoorIndex].enCurrentDoorState;
   }
   return enDoorState;
}

/*-----------------------------------------------------------------------------
  If doors are open and either dwell timer has elapsed or
  the door is in a hold state ( uwCurrentTime and uwTimer are 0 ) then return 1.
 -----------------------------------------------------------------------------*/
uint8_t Doors_GetDwellCompleteFlag( enum en_doors enDoorIndex )
{
   uint8_t bComplete = 1;

   if( enDoorIndex == DOOR_ANY )
   {
      for (enum en_doors enDoorIndex = 0; enDoorIndex < ucNumberOfDoors; enDoorIndex++)
      {
         if( ( stDoorState[enDoorIndex].enCurrentDoorState != DOOR__CLOSED )
          && ( ( stDoorState[enDoorIndex].enCurrentDoorState != DOOR__OPEN )
            || ( ( stDoorState[enDoorIndex].uwCurrentTimer_100ms <= stDoorState[enDoorIndex].uwTimer_100ms )
              && ( stDoorState[enDoorIndex].uwCurrentTimer_100ms || stDoorState[enDoorIndex].uwTimer_100ms ) ) ) )
         {
            bComplete = 0;
            break;
         }
         else if(aenDoorType[enDoorIndex] == DOOR_TYPE__MANUAL)
         {
            bComplete = 0;
            break;
         }
      }
   }
   else
   {
      if( ( stDoorState[enDoorIndex].enCurrentDoorState != DOOR__CLOSED )
       && ( ( stDoorState[enDoorIndex].enCurrentDoorState != DOOR__OPEN )
         || ( ( stDoorState[enDoorIndex].uwCurrentTimer_100ms <= stDoorState[enDoorIndex].uwTimer_100ms )
           && ( stDoorState[enDoorIndex].uwCurrentTimer_100ms || stDoorState[enDoorIndex].uwTimer_100ms ) ) ) )
      {
         bComplete = 0;
      }
      else if(aenDoorType[enDoorIndex] == DOOR_TYPE__MANUAL)
      {
         bComplete = 0;
      }
   }

   return bComplete;
}

/*-----------------------------------------------------------------------------
  Forces doors open if:
   - not being commanded to close
   - not fully closed
  Used for fire 2 operation
 -----------------------------------------------------------------------------*/
void Doors_ForceDoorsFullyOpen( enum en_doors enDoorIndex )
{
   if(aenDoorType[enDoorIndex] != DOOR_TYPE__MANUAL)
   {
      if( ( getDoorZone(enDoorIndex) )
       && ( !GetDoorCloseButton(enDoorIndex) )
       && ( !( ( IsDoorFullyClosed(enDoorIndex) )
            || ( GetDoorState(enDoorIndex) == DOOR__CLOSED ) ) ) )
      {
         SetDoorCommand(enDoorIndex, DOOR_COMMAND__OPEN_HOLD_REQUEST);
      }
   }
}
/*-----------------------------------------------------------------------------
  Forces doors open if faulted at a floor and attempting a recall
 -----------------------------------------------------------------------------*/
void Doors_HoldDoorOpen( enum en_doors enDoorIndex )
{
   if( getDoorZone(DOOR_ANY) )
   {
      if(enDoorIndex == DOOR_ANY)
      {
         if( ( !GetDoorCloseButton(DOOR_FRONT) ) && ( aenDoorType[DOOR_FRONT] != DOOR_TYPE__MANUAL ) )
         {
            SetDoorCommand(DOOR_FRONT, DOOR_COMMAND__OPEN_IN_CAR_REQUEST);
         }
         if( ( !GetDoorCloseButton(DOOR_REAR) ) && ( aenDoorType[DOOR_REAR] != DOOR_TYPE__MANUAL ) )
         {
            SetDoorCommand(DOOR_REAR, DOOR_COMMAND__OPEN_IN_CAR_REQUEST);
         }
      }
      else if(enDoorIndex == DOOR_FRONT)
      {
         if( ( !GetDoorCloseButton(DOOR_FRONT) ) && ( aenDoorType[DOOR_FRONT] != DOOR_TYPE__MANUAL ) )
         {
            SetDoorCommand(DOOR_FRONT, DOOR_COMMAND__OPEN_IN_CAR_REQUEST);
         }
      }
      else if(enDoorIndex == DOOR_REAR)
      {
         if( ( !GetDoorCloseButton(DOOR_REAR) ) && ( aenDoorType[DOOR_REAR] != DOOR_TYPE__MANUAL ) )
         {
            SetDoorCommand(DOOR_REAR, DOOR_COMMAND__OPEN_IN_CAR_REQUEST);
         }
      }
   }
}
/*----------------------------------------------------------------------------
 *
 * Access Functions - END
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Private Access Functions - START
 *
 *----------------------------------------------------------------------------*/
void SetDoorState( enum en_doors enDoorIndex, enum en_door_states enDoorState )
{
   if(aenDoorType[enDoorIndex] != DOOR_TYPE__MANUAL)
   {
      stDoorState[enDoorIndex].enCurrentDoorState = enDoorState;
   }
}

uint8_t GetDOL ( enum en_doors enDoorIndex )
{
   uint8_t bDOL = 0;
   bDOL = GetInputValue( stDoorSignals[enDoorIndex].DOL );
   return bDOL;
}
uint8_t GetDCL ( enum en_doors enDoorIndex )
{
   uint8_t bDCL = 0;
   bDCL = GetInputValue( stDoorSignals[enDoorIndex].DCL );
   return bDCL;
}
uint8_t GetDPM ( enum en_doors enDoorIndex )
{
   //check if programmed or not, if not programmed, report GSW state
   uint8_t bValue = 0;
   if ( CheckIfInputIsProgrammed(stDoorSignals[enDoorIndex].DPM) ) 
   {
      bValue = GetInputValue( stDoorSignals[enDoorIndex].DPM );
   }
   else
   {
      bValue = GetInputValue( stDoorSignals[enDoorIndex].GSW );
   }
   return bValue;
}
uint8_t GetPhotoEye ( enum en_doors enDoorIndex )
{
   uint8_t bPHE = 0;
   bPHE = GetInputValue( stDoorSignals[enDoorIndex].PHE );
   if ( CheckIfInputIsProgrammed(stDoorSignals[enDoorIndex].PHE2) ) 
   {
      bPHE &= GetInputValue( stDoorSignals[enDoorIndex].PHE2);
   }
   if (GetOperation_AutoMode() == MODE_A__SABBATH)
   {
       bPHE = 1;
   }
   return bPHE;
}
uint8_t GetGSW ( enum en_doors enDoorIndex )
{
   return GetInputValue( stDoorSignals[enDoorIndex].GSW );
}
uint8_t GetDZ ( enum en_doors enDoorIndex )
{
   return GetInputValue( stDoorSignals[enDoorIndex].DZ );
}
uint8_t GetLocks ( enum en_doors enDoorIndex )
{
   uint8_t bLocks = 0;
   bLocks  = GetInputValue( stDoorSignals[enDoorIndex].BLOCK );
   bLocks &= GetInputValue( stDoorSignals[enDoorIndex].MLOCK );
   bLocks &= GetInputValue( stDoorSignals[enDoorIndex].TLOCK );
   return bLocks;
}
uint8_t GetLock_Top( enum en_doors enDoorIndex )
{
   uint8_t bLocks = GetInputValue( stDoorSignals[enDoorIndex].TLOCK );
   return bLocks;
}
uint8_t GetLock_Mid( enum en_doors enDoorIndex )
{
   uint8_t bLocks = GetInputValue( stDoorSignals[enDoorIndex].MLOCK );
   return bLocks;
}
uint8_t GetLock_Bot( enum en_doors enDoorIndex )
{
   uint8_t bLocks = GetInputValue( stDoorSignals[enDoorIndex].BLOCK );
   return bLocks;
}
uint8_t GetCloseContacts ( enum en_doors enDoorIndex )
{
   uint8_t bLocks = 0;
   bLocks  = GetInputValue( stDoorSignals[enDoorIndex].BCL );
   bLocks &= GetInputValue( stDoorSignals[enDoorIndex].MCL );
   bLocks &= GetInputValue( stDoorSignals[enDoorIndex].TCL );
   return bLocks;
}
uint8_t GetCloseContact_Top( enum en_doors enDoorIndex )
{
   uint8_t bLocks = GetInputValue( stDoorSignals[enDoorIndex].TCL );
   return bLocks;
}
uint8_t GetCloseContact_Mid( enum en_doors enDoorIndex )
{
   uint8_t bLocks = GetInputValue( stDoorSignals[enDoorIndex].MCL );
   return bLocks;
}
uint8_t GetCloseContact_Bot( enum en_doors enDoorIndex )
{
   uint8_t bLocks = GetInputValue( stDoorSignals[enDoorIndex].BCL );
   return bLocks;
}
uint8_t GetSafetyEdge( enum en_doors enDoorIndex )
{
   uint8_t bValue = 0;
   /*Only check for safety edge if freight door  TODO add support for jobs with PHE and SE */
   if( aenDoorType[enDoorIndex] == DOOR_TYPE__FREIGHT )
   {
      bValue = GetInputValue(stDoorSignals[enDoorIndex].SAFETY_EDGE);
   }
   else
   {
      bValue = GetInputValue(stDoorSignals[enDoorIndex].PHE);
   }
   return bValue;
}
uint8_t GetCAM( enum en_doors enDoorIndex )
{
   uint8_t bValue = 0;
   bValue = GetOutputValue(stDoorSignals[enDoorIndex].CAM);
   return bValue;
}

/* Returns 1 if doors are fully closed. Used for door state transitions. */
uint8_t IsDoorFullyClosed( enum en_doors enDoorIndex )
{
   uint8_t bClosed = 0;

   uint8_t bGSW = GetGSW(enDoorIndex);
   uint8_t bDPM = GetDPM(enDoorIndex);
   uint8_t bLCK = GetLocks(enDoorIndex);
   uint8_t bDCL = GetDCL(enDoorIndex);
   uint8_t bDOL = GetDOL(enDoorIndex);
   uint8_t bClosedContacts = GetCloseContacts(enDoorIndex);

   switch(aenDoorType[enDoorIndex])
   {
      case DOOR_TYPE__AUTOMATIC:
         if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
         {
            bClosed = ( bGSW )
                   && ( bDPM )
                   && ( bLCK || bHallDoorsBypassed );
         }
         else
         {
            bClosed = ( !bDCL )
                   && ( bDOL )
                   && ( bGSW )
                   && ( bDPM )
                   && ( bLCK || bHallDoorsBypassed );
         }
         break;
      case DOOR_TYPE__FREIGHT:
      case DOOR_TYPE__SWING:
         if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
         {
            uint8_t bLocksExpectedClosed = Doors_CheckIfSwingLocksExpectedClosed(enDoorIndex);
            bClosed = ( bGSW )
                   && ( bDPM )
                   && ( bClosedContacts || bHallDoorsBypassed || !Door_GetSwingClosedContactsProgrammedFlag(enDoorIndex) )
                   && ( bLCK || !bLocksExpectedClosed || bHallDoorsBypassed );

         }
         else
         {
            uint8_t bLocksExpectedClosed = Doors_CheckIfSwingLocksExpectedClosed(enDoorIndex);
            bClosed = ( !bDCL )
                   && ( bDOL )
                   && ( bGSW )
                   && ( bDPM )
                   && ( bClosedContacts || bHallDoorsBypassed  || !Door_GetSwingClosedContactsProgrammedFlag(enDoorIndex) )
                   && ( bLCK || !bLocksExpectedClosed || bHallDoorsBypassed );
         }
         break;
      case DOOR_TYPE__MANUAL:
      {
         uint8_t bLocksExpectedClosed = Doors_CheckIfSwingLocksExpectedClosed(enDoorIndex);
         bClosed = ( bGSW )
                && ( bDPM )
                && ( bClosedContacts || bHallDoorsBypassed || !Door_GetSwingClosedContactsProgrammedFlag(enDoorIndex) )
                && ( bLCK || !bLocksExpectedClosed || bHallDoorsBypassed );
      }
         break;
      default: break;
   }

   return bClosed;
}
/* Returns 1 if doors are fully opened. Used for door state transitions. */
uint8_t IsDoorFullyOpen( enum en_doors enDoorIndex )
{
   uint8_t bOpen = 0;
   uint8_t bBypassJumperOnLockDetection = ( Param_ReadValue_1Bit(enPARAM1__Fire__IgnoreLocksJumpedOnPhase2) )
                                       && ( GetEmergencyBit( EmergencyBF_FirePhaseII_Active ) )
                                       && ( GetOperation_ClassOfOp() == CLASSOP__AUTO );
   uint8_t bGSW = GetGSW(enDoorIndex);
   uint8_t bDPM = GetDPM(enDoorIndex);
   uint8_t bLCK = GetLocks(enDoorIndex);
   uint8_t bDCL = GetDCL(enDoorIndex);
   uint8_t bDOL = GetDOL(enDoorIndex);
   uint8_t bClosedContacts = GetCloseContacts(enDoorIndex);

   if( aenDoorType[enDoorIndex] == DOOR_TYPE__MANUAL )
   {
      bOpen = ( !bGSW )
           && ( !bDPM )
           && ( !bClosedContacts || bBypassJumperOnLockDetection || !Door_GetSwingClosedContactsProgrammedFlag(enDoorIndex) )
           && ( !bLCK || bBypassJumperOnLockDetection );
   }
   else if( ( aenDoorType[enDoorIndex] == DOOR_TYPE__AUTOMATIC )
         || ( aenDoorType[enDoorIndex] == DOOR_TYPE__FREIGHT )
         || ( aenDoorType[enDoorIndex] == DOOR_TYPE__SWING ) )
   {
      bOpen = ( bDCL )
           && ( !bDOL )
           && ( !bGSW )
           && ( !bDPM )
           && ( !bLCK || bBypassJumperOnLockDetection );
   }

   return bOpen;
}

void SetTimersForDoorFault(enum en_doors enDoorIndex, uint16_t uwCurrentTimer_100ms, uint16_t uwLimitTimer_100ms)
{
   if(aenDoorType[enDoorIndex] != DOOR_TYPE__MANUAL) {
      stDoorState[enDoorIndex].uwCurrentTimer_100ms = uwCurrentTimer_100ms;
      stDoorState[enDoorIndex].uwTimer_100ms = uwLimitTimer_100ms;
   }
}
/*----------------------------------------------------------------------------
 *
 * Private Access Functions - END
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * State Transition Functions - START
 *
 *----------------------------------------------------------------------------*/
static void processDoorCloseRequest(enum en_doors enDoorIndex, enum en_door_states enDoorState)
{
   /* If already closing or closed, don't allow timers to be reset */
   if( ( ( stDoorState[enDoorIndex].enCurrentDoorState != DOOR__CLOSING ) || ( enDoorState != stDoorState[enDoorIndex].enCurrentDoorState ) ) // todo review
    && ( stDoorState[enDoorIndex].enCurrentDoorState != DOOR__CLOSED ) )
//   if( ( stDoorState[enDoorIndex].enCurrentDoorState != DOOR__CLOSING )
//    && ( stDoorState[enDoorIndex].enCurrentDoorState != DOOR__CLOSED ) )
   {
//      uint8_t bFireBypassesPHE = ( Fire_CheckIfFire2DoorsEnabled() || Fire_CheckIfFire1Recall() ) && !Param_ReadValue_1Bit(enPARAM1__Fire__EnablePHE_OnPhase2);
//      if( ( bFireBypassesPHE )
//       || ( stDoorState[enDoorIndex].eTestPHE_State == PHE_TEST_STATE__PASSED ) )
      if( stDoorState[enDoorIndex].eTestPHE_State == PHE_TEST_STATE__PASSED )
      {
         stDoorState[enDoorIndex].enCurrentDoorState = enDoorState;
         stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
         stDoorState[enDoorIndex].uwTimer_100ms = MOD_DOORS_ONE_SECOND_SCALE_FACTOR *
                                            Param_ReadValue_8Bit( enPARAM8__DoorStuckTime_1s );
      }
   }
}

static void processDoorOpenRequest(enum en_doors enDoorIndex, enum en_door_commands enDoorCommand)
{
   uint8_t bOkayToOpen = 0;
   if( ( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
    && ( !GetMotion_RunFlag() )
    && ( getDoorZone(DOOR_ANY) || Param_ReadValue_1Bit(enPARAM1__EnableInspDoorOpenOutOfDZ) ) )
   {
      bOkayToOpen = 1;
   }
   else if( ( GetOperation_ClassOfOp() != CLASSOP__MANUAL )
         && ( InDestinationDoorzone() )
         && ( ( ( enDoorIndex == DOOR_FRONT ) && gpastFloors[GetOperation_CurrentFloor()].bFrontOpening )
           || ( ( enDoorIndex == DOOR_REAR ) && gpastFloors[GetOperation_CurrentFloor()].bRearOpening ) ) )
   {
      bOkayToOpen = 1;
   }
   if (bOkayToOpen)
   {
      if( aenDoorType[enDoorIndex] == DOOR_TYPE__MANUAL )
      {
         stDoorState[enDoorIndex].enCurrentDoorState = DOOR__OPEN;
         switch (enDoorCommand) {
            case DOOR_COMMAND__OPEN_HOLD_REQUEST:
            case DOOR_COMMAND__OPEN_CONSTANT_PRESSURE:
               //no timer, so door holds open until commanded to close
               stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
               stDoorState[enDoorIndex].uwTimer_100ms = 0;
               break;
            case DOOR_COMMAND__OPEN_UI_REQUEST:
            case DOOR_COMMAND__OPEN_IN_CAR_REQUEST:
               stDoorState[enDoorIndex].uwTimer_100ms = (MOD_DOORS_ONE_SECOND_SCALE_FACTOR *
                     Param_ReadValue_8Bit( enPARAM8__DoorDwellTime_1s ) +
                     stDoorState[enDoorIndex].uwCurrentTimer_100ms);
               break;
            case DOOR_COMMAND__OPEN_HALL_REQUEST:
               stDoorState[enDoorIndex].uwTimer_100ms = (MOD_DOORS_ONE_SECOND_SCALE_FACTOR *
                     Param_ReadValue_8Bit( enPARAM8__DoorDwellHallTime_1s ) +
                     stDoorState[enDoorIndex].uwCurrentTimer_100ms);
               break;
            case DOOR_COMMAND__OPEN_ADA_MODE:
               stDoorState[enDoorIndex].uwTimer_100ms = (MOD_DOORS_ONE_SECOND_SCALE_FACTOR *
                     Param_ReadValue_8Bit( enPARAM8__DoorDwellADATime_1s ) +
                     stDoorState[enDoorIndex].uwCurrentTimer_100ms);
               break;
            case DOOR_COMMAND__OPEN_SABBATH_MODE:
               stDoorState[enDoorIndex].uwTimer_100ms = (MOD_DOORS_ONE_SECOND_SCALE_FACTOR *
                     Param_ReadValue_8Bit( enPARAM8__DoorDwellSabbathTimer_1s ) +
                     stDoorState[enDoorIndex].uwCurrentTimer_100ms);
               break;
            case DOOR_COMMAND__OPEN_HOLD_DWELL_REQUEST:
               stDoorState[enDoorIndex].uwTimer_100ms = (MOD_DOORS_ONE_SECOND_SCALE_FACTOR *
                     Param_ReadValue_8Bit( enPARAM8__DoorDwellHoldTime_1s ) +
                     stDoorState[enDoorIndex].uwCurrentTimer_100ms);
               break;
            case DOOR_COMMAND__OPEN_LOBBY_REQUEST:
               stDoorState[enDoorIndex].uwTimer_100ms = ( MOD_DOORS_ONE_SECOND_SCALE_FACTOR * Param_ReadValue_8Bit(enPARAM8__LobbyDwellTime_1s) )
                                                + stDoorState[enDoorIndex].uwCurrentTimer_100ms;
               break;
            default:
               break;
         }
      }
      else
      {
         if( stDoorState[enDoorIndex].enCurrentDoorState == DOOR__OPEN )
         {
            switch (enDoorCommand) {
               case DOOR_COMMAND__OPEN_HOLD_REQUEST:
               case DOOR_COMMAND__OPEN_CONSTANT_PRESSURE:
                  //no timer, so door holds open until commanded to close
                  stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
                  stDoorState[enDoorIndex].uwTimer_100ms = 0;
                  break;
               case DOOR_COMMAND__OPEN_UI_REQUEST:
               case DOOR_COMMAND__OPEN_IN_CAR_REQUEST:
                  stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
                  stDoorState[enDoorIndex].uwTimer_100ms = MOD_DOORS_ONE_SECOND_SCALE_FACTOR
                                                   * Param_ReadValue_8Bit( enPARAM8__DoorDwellTime_1s );
                  break;
               case DOOR_COMMAND__OPEN_HALL_REQUEST:
                  stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
                  stDoorState[enDoorIndex].uwTimer_100ms = MOD_DOORS_ONE_SECOND_SCALE_FACTOR
                                                   * Param_ReadValue_8Bit( enPARAM8__DoorDwellHallTime_1s );
                  break;
               case DOOR_COMMAND__OPEN_ADA_MODE:
                  stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
                  stDoorState[enDoorIndex].uwTimer_100ms = MOD_DOORS_ONE_SECOND_SCALE_FACTOR
                                                   * Param_ReadValue_8Bit( enPARAM8__DoorDwellADATime_1s );
                  break;
               case DOOR_COMMAND__OPEN_SABBATH_MODE:
                  stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
                  stDoorState[enDoorIndex].uwTimer_100ms = MOD_DOORS_ONE_SECOND_SCALE_FACTOR
                                                   * Param_ReadValue_8Bit( enPARAM8__DoorDwellSabbathTimer_1s );
                  break;
               case DOOR_COMMAND__OPEN_HOLD_DWELL_REQUEST:
                  stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
                  stDoorState[enDoorIndex].uwTimer_100ms = MOD_DOORS_ONE_SECOND_SCALE_FACTOR
                                                   * Param_ReadValue_8Bit( enPARAM8__DoorDwellHoldTime_1s );
                  break;
               case DOOR_COMMAND__OPEN_LOBBY_REQUEST:
                  stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
                  stDoorState[enDoorIndex].uwTimer_100ms = MOD_DOORS_ONE_SECOND_SCALE_FACTOR
                                                   * Param_ReadValue_8Bit( enPARAM8__LobbyDwellTime_1s );
                  break;
               default:
                  break;
            }
         }
         /* Don't refresh door timers if doors are already opening */
         else if( stDoorState[enDoorIndex].enCurrentDoorState != DOOR__OPENING )
         {
            stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
            if( GetDoorFault(enDoorIndex) == DOOR_FAULT__NONE ) {
               stDoorState[enDoorIndex].uwTimer_100ms = MOD_DOORS_ONE_SECOND_SCALE_FACTOR
                                                * Param_ReadValue_8Bit( enPARAM8__DoorStuckTime_1s );
            }
            stDoorState[enDoorIndex].enCurrentDoorState = DOOR__OPENING;
         }
      }
   }
}
/*-----------------------------------------------------------------------------
   Determines behavior for loss of constant pressure DOB/DCB signal
 -----------------------------------------------------------------------------*/
static void processDoorCommandNone(enum en_doors enDoorIndex)
{
   if( aenDoorType[enDoorIndex] != DOOR_TYPE__MANUAL )
   {
      /* If inspection constant pressure commands are lost, determine current door state */
      if ( stDoorState[enDoorIndex].enDoorCommand == DOOR_COMMAND__CLOSE_CONSTANT_PRESSURE
        || stDoorState[enDoorIndex].enDoorCommand == DOOR_COMMAND__OPEN_CONSTANT_PRESSURE
        || stDoorState[enDoorIndex].enDoorCommand == DOOR_COMMAND__NUDGE_CONSTANT_PRESSURE )
      {
         stDoorState[enDoorIndex].enCurrentDoorState = DecideDoorState(enDoorIndex);
      }
      /* If in automatic operation constant pressure mode (i.e. Fire 2), determine which state doors should return to. */
      else if ( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
            &&  ( AutoModeRules_GetForceDoorsOpenOrClosedFlag() ) )
      {
         uint8_t bDoorFullyClosed = IsDoorFullyClosed(enDoorIndex);
         uint8_t bDoorFullyOpen = IsDoorFullyOpen(enDoorIndex);
         uint8_t ucDOB = GetDoorOpenButton(enDoorIndex);
         uint8_t ucDCB = GetDoorCloseButton(enDoorIndex);
         if ( !bDoorFullyClosed && !bDoorFullyOpen )
         {
            if ( ( ucDCB )
            || ( ( !ucDOB ) && ( stDoorState[enDoorIndex].enLastFullDoorState == DOOR__CLOSED ) ) ) {
               processDoorCloseRequest(enDoorIndex, DOOR__CLOSING);
            } else if ( ( ucDOB )
                   || ( ( getDoorZone(enDoorIndex) ) && ( stDoorState[enDoorIndex].enLastFullDoorState == DOOR__OPEN ) ) ) {
               processDoorOpenRequest(enDoorIndex, DOOR_COMMAND__OPEN_HOLD_REQUEST);
            } else if ( ( Fire_CheckIfFire2DoorsEnabled() ) && (stDoorState[enDoorIndex].enLastFullDoorState == DOOR__UNKNOWN) ) {
               stDoorState[enDoorIndex].enCurrentDoorState = DecideDoorState(enDoorIndex);
            } else if ( ( stDoorState[enDoorIndex].enCurrentDoorState == DOOR__CLOSING )
                    ||  ( stDoorState[enDoorIndex].enCurrentDoorState == DOOR__NUDGING )
                    ||  ( stDoorState[enDoorIndex].enLastDoorCommand == DOOR_COMMAND__CLOSE ) ) {
               processDoorCloseRequest(enDoorIndex, DOOR__CLOSING);
            } else if ( getDoorZone(enDoorIndex) ){
               processDoorOpenRequest(enDoorIndex, DOOR_COMMAND__OPEN_HOLD_REQUEST);
            }
         }
         /* If doors already fully opened or closed update door state */
         else
         {
            stDoorState[enDoorIndex].enCurrentDoorState = DecideDoorState(enDoorIndex);
         }
      }
      /* Fix for bug where if doors were opening when moving from auto to insp, they would continue opening */
      else if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
      {
         stDoorState[enDoorIndex].enCurrentDoorState = DecideDoorState(enDoorIndex);
      }
   }
}
/*----------------------------------------------------------------------------
 *
 * State Transition Functions - END
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Output Control Functions - START
 *
 *----------------------------------------------------------------------------*/
// The set functions are used to set the controller outputs for the door operator
static void SetAllDoorOutputsOff ( enum en_doors enDoorIndex )
{
   SetOutputValue( stDoorSignals[enDoorIndex].DO,  0 );
   SetOutputValue( stDoorSignals[enDoorIndex].DC,  0 );
   SetOutputValue( stDoorSignals[enDoorIndex].NUDGE, 0 );
   SetOutputValue( stDoorSignals[enDoorIndex].GATE_RELEASE, 0);
   SetOutputValue( stDoorSignals[enDoorIndex].DCP, 0);
   SetOutputValue( stDoorSignals[enDoorIndex].CAM, 0);
   SetOutputValue( stDoorSignals[enDoorIndex].FREIGHT_BUZZER, 0);
   SetOutputValue( stDoorSignals[enDoorIndex].FREIGHT_DCM, 0);
   SetOutputValue( stDoorSignals[enDoorIndex].FREIGHT_TEST, 0);
}
// DOOR__OPEN
static void Doors_SetOutputs_DoorOpenState( enum en_doors enDoorIndex )
{
   uint8_t bDoorsKilled = CheckIf_DoorsKilled();
   SetAllDoorOutputsOff(enDoorIndex);
   if( !bDoorsKilled )
   {
      switch( aenDoorType[enDoorIndex] )
      {
         case DOOR_TYPE__AUTOMATIC:
         case DOOR_TYPE__FREIGHT:
         case DOOR_TYPE__SWING:
            if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
            {
               uint8_t bDoorsAllowedToOpen = !GetMotion_RunFlag() && ( GetDZ(enDoorIndex) || Param_ReadValue_1Bit(enPARAM1__EnableInspDoorOpenOutOfDZ) );
               if( Param_ReadValue_1Bit(enPARAM1__DO_On_DoorOpenState) && bDoorsAllowedToOpen )
               {
                  SetOutputValue( stDoorSignals[enDoorIndex].DO,  1 );
                  SetOutputValue( stDoorSignals[enDoorIndex].DC,  0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].NUDGE, 0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].GATE_RELEASE, 1);
                  SetOutputValue( stDoorSignals[enDoorIndex].DCP, 0);
               }
            }
            else
            {
               uint8_t bDoorsAllowedToOpen = InDestinationDoorzone() && GetDZ(enDoorIndex);
               if( ( bDoorsAllowedToOpen )
                && ( ( !IsDoorFullyOpen(enDoorIndex) ) || ( Param_ReadValue_1Bit(enPARAM1__DO_On_DoorOpenState) ) ) )
               {
                  SetOutputValue( stDoorSignals[enDoorIndex].DO,  1 );
                  SetOutputValue( stDoorSignals[enDoorIndex].DC,  0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].NUDGE, 0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].GATE_RELEASE, 1);
                  SetOutputValue( stDoorSignals[enDoorIndex].DCP, 0);
               }

               /* Peelle Freight door operator buzzer feature. */
               uint16_t uwRemainingDwellTime_100ms = ( stDoorState[enDoorIndex].uwCurrentTimer_100ms < stDoorState[enDoorIndex].uwTimer_100ms )
                                                   ? ( stDoorState[enDoorIndex].uwTimer_100ms - stDoorState[enDoorIndex].uwCurrentTimer_100ms )
                                                   : ( 0 );
               uint16_t uwCloseBuzzerLimit_100ms = Param_ReadValue_8Bit(enPARAM8__DoorCloseBuzzer_100ms);
               if( uwRemainingDwellTime_100ms <= uwCloseBuzzerLimit_100ms )
               {
                  SetOutputValue( stDoorSignals[enDoorIndex].FREIGHT_BUZZER, 1);
               }
               SetOutputValue( stDoorSignals[enDoorIndex].FREIGHT_TEST, ( stDoorState[enDoorIndex].eTestPHE_State == PHE_TEST_STATE__IN_PROGRESS ) );
            }
            break;
         case DOOR_TYPE__MANUAL:
            if( GetOperation_ClassOfOp() != CLASSOP__MANUAL )
            {
               uint8_t bNeedToOpen = !IsDoorFullyOpen(enDoorIndex) && GetDZ(enDoorIndex);
               if( bNeedToOpen )
               {
                  SetOutputValue( stDoorSignals[enDoorIndex].DO,  1 );
                  SetOutputValue( stDoorSignals[enDoorIndex].DC,  0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].NUDGE, 0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].GATE_RELEASE, 1);
                  SetOutputValue( stDoorSignals[enDoorIndex].DCP, 0);
               }
            }
            break;
         default: break;
      }
      /* Retiring CAM output should behave the same at all times */
      uint8_t bCAM = GetMotion_RunFlag() || !GetDZ(enDoorIndex);
      /* Option to suppress CAM output when on hoistway access */
      if( Param_ReadValue_1Bit(enPARAM1__DISA_CAM_ON_HA) )
      {
         if( GetOperation_ManualMode() == MODE_M__INSP_HA_TOP )
         {
            if( Param_ReadValue_8Bit(enPARAM8__HA_TopOpening) == enDoorIndex )
            {
               bCAM = 0;
            }
         }
         else if( GetOperation_ManualMode() == MODE_M__INSP_HA_BOTTOM )
         {
            if( Param_ReadValue_8Bit(enPARAM8__HA_BottomOpening) == enDoorIndex )
            {
               bCAM = 0;
            }
         }
      }
      SetOutputValue(stDoorSignals[enDoorIndex].CAM, bCAM);
   }
}
// DOOR__OPENING
static void Doors_SetOutputs_DoorOpeningState( enum en_doors enDoorIndex )
{
   uint8_t bDoorsKilled = CheckIf_DoorsKilled();
   SetAllDoorOutputsOff(enDoorIndex);
   if( !bDoorsKilled )
   {
      switch( aenDoorType[enDoorIndex] )
      {
         case DOOR_TYPE__AUTOMATIC:
         case DOOR_TYPE__FREIGHT:
         case DOOR_TYPE__SWING:
            if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
            {
               uint8_t bDoorsAllowedToOpen = !GetMotion_RunFlag() && ( GetDZ(enDoorIndex) || Param_ReadValue_1Bit(enPARAM1__EnableInspDoorOpenOutOfDZ) );
               if( bDoorsAllowedToOpen )
               {
                  SetOutputValue( stDoorSignals[enDoorIndex].DO,  1 );
                  SetOutputValue( stDoorSignals[enDoorIndex].DC,  0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].NUDGE, 0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].GATE_RELEASE, 1);
                  SetOutputValue( stDoorSignals[enDoorIndex].DCP, 0);
               }
            }
            else
            {
               uint8_t bDoorsAllowedToOpen = InDestinationDoorzone() && GetDZ(enDoorIndex);
               if( bDoorsAllowedToOpen )
               {
                  SetOutputValue( stDoorSignals[enDoorIndex].DO,  1 );
                  SetOutputValue( stDoorSignals[enDoorIndex].DC,  0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].NUDGE, 0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].GATE_RELEASE, 1);
                  SetOutputValue( stDoorSignals[enDoorIndex].DCP, 0);
               }
            }
            break;
         case DOOR_TYPE__MANUAL:
            if( GetOperation_ClassOfOp() != CLASSOP__MANUAL )
            {
               uint8_t bNeedToOpen = !IsDoorFullyOpen(enDoorIndex) && GetDZ(enDoorIndex);
               if( bNeedToOpen )
               {
                  SetOutputValue( stDoorSignals[enDoorIndex].DO,  1 );
                  SetOutputValue( stDoorSignals[enDoorIndex].DC,  0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].NUDGE, 0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].GATE_RELEASE, 1);
                  SetOutputValue( stDoorSignals[enDoorIndex].DCP, 0);
               }
            }
            break;
         default: break;
      }
      /* Retiring CAM output should behave the same at all times */
      uint8_t bCAM = GetMotion_RunFlag() || !GetDZ(enDoorIndex);
      /* Option to suppress CAM output when on hoistway access */
      if( Param_ReadValue_1Bit(enPARAM1__DISA_CAM_ON_HA) )
      {
         if( GetOperation_ManualMode() == MODE_M__INSP_HA_TOP )
         {
            if( Param_ReadValue_8Bit(enPARAM8__HA_TopOpening) == enDoorIndex )
            {
               bCAM = 0;
            }
         }
         else if( GetOperation_ManualMode() == MODE_M__INSP_HA_BOTTOM )
         {
            if( Param_ReadValue_8Bit(enPARAM8__HA_BottomOpening) == enDoorIndex )
            {
               bCAM = 0;
            }
         }
      }
      SetOutputValue(stDoorSignals[enDoorIndex].CAM, bCAM);
   }
}
// DOOR__CLOSING
static void Doors_SetOutputs_DoorClosingState( enum en_doors enDoorIndex )
{
   uint8_t bDoorsKilled = CheckIf_DoorsKilled();
   SetAllDoorOutputsOff(enDoorIndex);
   if( !bDoorsKilled )
   {
      switch( aenDoorType[enDoorIndex] )
      {
         case DOOR_TYPE__AUTOMATIC:
         case DOOR_TYPE__FREIGHT:
         case DOOR_TYPE__SWING:
            if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
            {
               SetOutputValue( stDoorSignals[enDoorIndex].DO,  0 );
               SetOutputValue( stDoorSignals[enDoorIndex].GATE_RELEASE, GetDCL( enDoorIndex ) );
               SetOutputValue( stDoorSignals[enDoorIndex].DCP, GetMotion_RunFlag() );
               if( Param_ReadValue_1Bit(enPARAM1__EnableIMotionDoors))
               {
                  SetOutputValue( stDoorSignals[enDoorIndex].DC,  1);
                  SetOutputValue( stDoorSignals[enDoorIndex].NUDGE,  1);
               }
               else
               {
                  SetOutputValue( stDoorSignals[enDoorIndex].DC,  1);
                  SetOutputValue( stDoorSignals[enDoorIndex].NUDGE, 0 );
               }
            }
            else
            {
               uint8_t bFireBypassesPHE = ( Fire_CheckIfFire2DoorsEnabled() || Fire_CheckIfFire1Recall() ) && !Param_ReadValue_1Bit(enPARAM1__Fire__EnablePHE_OnPhase2);
               uint8_t bNeedToOpen = !GetGSW(enDoorIndex);
               bNeedToOpen &= ( ( aenDoorType[enDoorIndex] == DOOR_TYPE__FREIGHT ) && !GetSafetyEdge(enDoorIndex) )
                             || ( !GetPhotoEye(enDoorIndex) && !bFireBypassesPHE );
               if( bNeedToOpen && GetDZ(enDoorIndex) )
               {
                  /* If user is closing DCB with PHE, command a DO but remain
                   * in door closing state instead of reverting to opening
                   * so nudging can activate */
                  SetOutputValue( stDoorSignals[enDoorIndex].DO,  1 );
                  SetOutputValue( stDoorSignals[enDoorIndex].DC,  0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].NUDGE, 0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].GATE_RELEASE, 1);
                  SetOutputValue( stDoorSignals[enDoorIndex].DCP, 0);
               }
               else if( !bNeedToOpen )
               {
                  SetOutputValue( stDoorSignals[enDoorIndex].DO,  0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].GATE_RELEASE, GetDCL( enDoorIndex ) );
                  SetOutputValue( stDoorSignals[enDoorIndex].DCP, GetMotion_RunFlag() );
                  if( Param_ReadValue_1Bit(enPARAM1__EnableIMotionDoors))
                  {
                     SetOutputValue( stDoorSignals[enDoorIndex].DC,  1);
                     SetOutputValue( stDoorSignals[enDoorIndex].NUDGE,  1);
                  }
                  else
                  {
                     SetOutputValue( stDoorSignals[enDoorIndex].DC,  1);
                     SetOutputValue( stDoorSignals[enDoorIndex].NUDGE, 0 );
                  }
               }
               else
               {
                  SetAllDoorOutputsOff(enDoorIndex);
               }

               SetOutputValue( stDoorSignals[enDoorIndex].FREIGHT_BUZZER, 1);
            }
            break;
         case DOOR_TYPE__MANUAL:
            if( GetOperation_ClassOfOp() != CLASSOP__MANUAL )
            {
               uint8_t bNeedToOpen = !IsDoorFullyOpen(enDoorIndex) && GetDZ(enDoorIndex);
               if( bNeedToOpen )
               {
                  SetOutputValue( stDoorSignals[enDoorIndex].DO,  1 );
                  SetOutputValue( stDoorSignals[enDoorIndex].DC,  0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].NUDGE, 0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].GATE_RELEASE, 1);
                  SetOutputValue( stDoorSignals[enDoorIndex].DCP, 0);
               }
            }
            break;
         default: break;
      }
      /* Retiring CAM output should behave the same at all times */
      uint8_t bCAM = GetMotion_RunFlag() || !GetDZ(enDoorIndex);
      /* Option to suppress CAM output when on hoistway access */
      if( Param_ReadValue_1Bit(enPARAM1__DISA_CAM_ON_HA) )
      {
         if( GetOperation_ManualMode() == MODE_M__INSP_HA_TOP )
         {
            if( Param_ReadValue_8Bit(enPARAM8__HA_TopOpening) == enDoorIndex )
            {
               bCAM = 0;
            }
         }
         else if( GetOperation_ManualMode() == MODE_M__INSP_HA_BOTTOM )
         {
            if( Param_ReadValue_8Bit(enPARAM8__HA_BottomOpening) == enDoorIndex )
            {
               bCAM = 0;
            }
         }
      }
      SetOutputValue(stDoorSignals[enDoorIndex].CAM, bCAM);
   }
}
// DOOR__NUDGING
static void Doors_SetOutputs_DoorNudgingState( enum en_doors enDoorIndex )
{
   uint8_t bDoorsKilled = CheckIf_DoorsKilled();
   SetAllDoorOutputsOff(enDoorIndex);
   if( !bDoorsKilled )
   {
      switch( aenDoorType[enDoorIndex] )
      {
         case DOOR_TYPE__AUTOMATIC:
         case DOOR_TYPE__FREIGHT:
         case DOOR_TYPE__SWING:
         {
            uint8_t bFireBypassesPHE = ( Fire_CheckIfFire2DoorsEnabled() || Fire_CheckIfFire1Recall() ) && !Param_ReadValue_1Bit(enPARAM1__Fire__EnablePHE_OnPhase2);
            uint8_t bNeedToOpen = !GetGSW(enDoorIndex);
            bNeedToOpen &= ( ( aenDoorType[enDoorIndex] == DOOR_TYPE__FREIGHT ) && !GetSafetyEdge(enDoorIndex) )
                          || ( !GetPhotoEye(enDoorIndex) && !bFireBypassesPHE );

            uint8_t bNudge = ( !Param_ReadValue_1Bit(enPARAM1__Doors_NudgeBuzzerOnly) )
                          && ( ( GetNextDestination_AnyDirection() != INVALID_FLOOR )
                            || ( GetOperation_AutoMode() != MODE_A__NORMAL )
                            || ( Param_ReadValue_1Bit(enPARAM1__NudgeWithoutOnwardDemand) && GetOperation_AutoMode() == MODE_A__NORMAL)
                            || ( GetOperation_AutoMode() == MODE_A__SABBATH));

            /* Always nudge when commanded for Fire2 and phase 1 recall  */
            bNudge |= Fire_CheckIfFire2DoorsEnabled() || Fire_CheckIfFire1Recall();
            if( bNudge ) // NDG
            {
               SetOutputValue( stDoorSignals[enDoorIndex].DO,  0 );
               SetOutputValue( stDoorSignals[enDoorIndex].GATE_RELEASE, 1);
               SetOutputValue( stDoorSignals[enDoorIndex].DCP, 0);
               if( Param_ReadValue_1Bit(enPARAM1__EnableIMotionDoors) )
               {
                  SetOutputValue( stDoorSignals[enDoorIndex].DC,  1 );
                  SetOutputValue( stDoorSignals[enDoorIndex].NUDGE, 0 );
               }
               else
               {
                  SetOutputValue( stDoorSignals[enDoorIndex].DC,  1 );
                  SetOutputValue( stDoorSignals[enDoorIndex].NUDGE, 1 );
               }
            }
            else if( !bNeedToOpen ) // DC
            {
               SetOutputValue( stDoorSignals[enDoorIndex].DO,  0 );
               SetOutputValue( stDoorSignals[enDoorIndex].GATE_RELEASE, GetDCL( enDoorIndex ) );
               SetOutputValue( stDoorSignals[enDoorIndex].DCP, GetMotion_RunFlag() );
               if( Param_ReadValue_1Bit(enPARAM1__EnableIMotionDoors) )
               {
                  SetOutputValue( stDoorSignals[enDoorIndex].DC,  1);
                  SetOutputValue( stDoorSignals[enDoorIndex].NUDGE,  1);
               }
               else
               {
                  SetOutputValue( stDoorSignals[enDoorIndex].DC,  1);
                  SetOutputValue( stDoorSignals[enDoorIndex].NUDGE, 0 );
               }
            }

            SetOutputValue( stDoorSignals[enDoorIndex].FREIGHT_BUZZER, 1);
         }
            break;
         case DOOR_TYPE__MANUAL:
            SetOutputValue( stDoorSignals[enDoorIndex].DO,  0 );
            SetOutputValue( stDoorSignals[enDoorIndex].GATE_RELEASE,1 );
            SetOutputValue( stDoorSignals[enDoorIndex].DCP, 0 );
            if( Param_ReadValue_1Bit(enPARAM1__EnableIMotionDoors) )
            {
               SetOutputValue( stDoorSignals[enDoorIndex].DC,  1);
               SetOutputValue( stDoorSignals[enDoorIndex].NUDGE,  1);
            }
            else
            {
               SetOutputValue( stDoorSignals[enDoorIndex].DC,  1);
               SetOutputValue( stDoorSignals[enDoorIndex].NUDGE, 0 );
            }
            break;
         default: break;
      }
      /* Retiring CAM output should behave the same at all times */
      uint8_t bCAM = GetMotion_RunFlag() || !GetDZ(enDoorIndex);
      /* Option to suppress CAM output when on hoistway access */
      if( Param_ReadValue_1Bit(enPARAM1__DISA_CAM_ON_HA) )
      {
         if( GetOperation_ManualMode() == MODE_M__INSP_HA_TOP )
         {
            if( Param_ReadValue_8Bit(enPARAM8__HA_TopOpening) == enDoorIndex )
            {
               bCAM = 0;
            }
         }
         else if( GetOperation_ManualMode() == MODE_M__INSP_HA_BOTTOM )
         {
            if( Param_ReadValue_8Bit(enPARAM8__HA_BottomOpening) == enDoorIndex )
            {
               bCAM = 0;
            }
         }
      }
      SetOutputValue(stDoorSignals[enDoorIndex].CAM, bCAM);
   }
}
// DOOR__CLOSED
static void Doors_SetOutputs_DoorClosedState( enum en_doors enDoorIndex )
{
   uint8_t bDoorsKilled = CheckIf_DoorsKilled();
   SetAllDoorOutputsOff(enDoorIndex);
   if( !bDoorsKilled )
   {
      switch( aenDoorType[enDoorIndex] )
      {
         case DOOR_TYPE__AUTOMATIC:
         case DOOR_TYPE__FREIGHT:
         case DOOR_TYPE__SWING:
            if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
            {
               uint8_t bNeedToClose = GetMotion_RunFlag() || Param_ReadValue_1Bit(enPARAM1__DC_On_DoorCloseState);
               if( bNeedToClose )
               {
                  SetOutputValue( stDoorSignals[enDoorIndex].DO,  0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].GATE_RELEASE, GetDCL( enDoorIndex ) );
                  SetOutputValue( stDoorSignals[enDoorIndex].DCP, GetMotion_RunFlag() );
                  if( Param_ReadValue_1Bit(enPARAM1__EnableIMotionDoors) )
                  {
                     SetOutputValue( stDoorSignals[enDoorIndex].DC,  1);
                     SetOutputValue( stDoorSignals[enDoorIndex].NUDGE,  1);
                  }
                  else
                  {
                     SetOutputValue( stDoorSignals[enDoorIndex].DC,  1);
                     SetOutputValue( stDoorSignals[enDoorIndex].NUDGE, 0 );
                  }
               }
            }
            else
            {
               uint8_t bNeedToClose = !GetPreflightFlag() && !IsDoorFullyClosed(enDoorIndex);
               bNeedToClose |= GetMotion_RunFlag() || Param_ReadValue_1Bit(enPARAM1__DC_On_DoorCloseState);
               if(bNeedToClose)
               {
                  SetOutputValue( stDoorSignals[enDoorIndex].DO,  0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].GATE_RELEASE, GetDCL( enDoorIndex ) );
                  SetOutputValue( stDoorSignals[enDoorIndex].DCP, GetMotion_RunFlag() );
                  if( Param_ReadValue_1Bit(enPARAM1__EnableIMotionDoors) )
                  {
                     SetOutputValue( stDoorSignals[enDoorIndex].DC,  1);
                     SetOutputValue( stDoorSignals[enDoorIndex].NUDGE,  1);
                  }
                  else
                  {
                     SetOutputValue( stDoorSignals[enDoorIndex].DC,  1);
                     SetOutputValue( stDoorSignals[enDoorIndex].NUDGE, 0 );
                  }
               }
            }
            break;
         case DOOR_TYPE__MANUAL: break;
         default: break;
      }
      /* Retiring CAM output should behave the same at all times */
      uint8_t bCAM = GetMotion_RunFlag() || !GetDZ(enDoorIndex);
      /* Option to suppress CAM output when on hoistway access */
      if( Param_ReadValue_1Bit(enPARAM1__DISA_CAM_ON_HA) )
      {
         if( GetOperation_ManualMode() == MODE_M__INSP_HA_TOP )
         {
            if( Param_ReadValue_8Bit(enPARAM8__HA_TopOpening) == enDoorIndex )
            {
               bCAM = 0;
            }
         }
         else if( GetOperation_ManualMode() == MODE_M__INSP_HA_BOTTOM )
         {
            if( Param_ReadValue_8Bit(enPARAM8__HA_BottomOpening) == enDoorIndex )
            {
               bCAM = 0;
            }
         }
      }
      SetOutputValue(stDoorSignals[enDoorIndex].CAM, bCAM);
   }
}
// DOOR__PARTIALLY_OPEN
static void Doors_SetOutputs_DoorPartiallyOpenState( enum en_doors enDoorIndex )
{
   uint8_t bDoorsKilled = CheckIf_DoorsKilled();
   SetAllDoorOutputsOff(enDoorIndex);
   if( !bDoorsKilled )
   {
      switch( aenDoorType[enDoorIndex] )
      {
         case DOOR_TYPE__AUTOMATIC:
         case DOOR_TYPE__FREIGHT:
         case DOOR_TYPE__SWING:
            break;
         case DOOR_TYPE__MANUAL:
            if( GetOperation_ClassOfOp() != CLASSOP__MANUAL )
            {
               uint8_t bNeedToOpen = !IsDoorFullyOpen(enDoorIndex) && GetDZ(enDoorIndex);
               if( bNeedToOpen )
               {
                  SetOutputValue( stDoorSignals[enDoorIndex].DO,  1 );
                  SetOutputValue( stDoorSignals[enDoorIndex].DC,  0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].NUDGE, 0 );
                  SetOutputValue( stDoorSignals[enDoorIndex].GATE_RELEASE, 1);
                  SetOutputValue( stDoorSignals[enDoorIndex].DCP, 0);
               }
            }
            break;
         default: break;
      }
      /* Retiring CAM output should behave the same at all times */
      uint8_t bCAM = GetMotion_RunFlag() || !GetDZ(enDoorIndex);
      /* Option to suppress CAM output when on hoistway access */
      if( Param_ReadValue_1Bit(enPARAM1__DISA_CAM_ON_HA) )
      {
         if( GetOperation_ManualMode() == MODE_M__INSP_HA_TOP )
         {
            if( Param_ReadValue_8Bit(enPARAM8__HA_TopOpening) == enDoorIndex )
            {
               bCAM = 0;
            }
         }
         else if( GetOperation_ManualMode() == MODE_M__INSP_HA_BOTTOM )
         {
            if( Param_ReadValue_8Bit(enPARAM8__HA_BottomOpening) == enDoorIndex )
            {
               bCAM = 0;
            }
         }
      }
      SetOutputValue(stDoorSignals[enDoorIndex].CAM, bCAM);
   }
}
/*----------------------------------------------------------------------------
 *
 * Output Control Functions - END
 *
 *----------------------------------------------------------------------------*/
static enum en_door_states DecideDoorState( enum en_doors enDoorIndex )
{
   enum en_door_states result = DOOR__UNKNOWN;
   uint8_t bDoorFullyClosed = IsDoorFullyClosed( enDoorIndex );
   uint8_t bDoorFullyOpen = IsDoorFullyOpen( enDoorIndex );
   switch( aenDoorType[enDoorIndex] )
   {
      case DOOR_TYPE__AUTOMATIC:
      case DOOR_TYPE__FREIGHT:
      case DOOR_TYPE__SWING:
         if ( bDoorFullyClosed )
         {
            if ( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
              && ( AutoModeRules_GetForceDoorsOpenOrClosedFlag() )
              && ( stDoorState[enDoorIndex].enCurrentDoorState != DOOR__CLOSED ) ) {
               stDoorState[enDoorIndex].enDoorCommand = DOOR_COMMAND__CLOSE;
               stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
               stDoorState[enDoorIndex].uwTimer_100ms = MOD_DOORS_ONE_SECOND_SCALE_FACTOR *
                                                  Param_ReadValue_8Bit( enPARAM8__DoorStuckTime_1s );
            }
            result = DOOR__CLOSED;
         }
         else if ( bDoorFullyOpen )
         {
            stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
            if ( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
              && ( AutoModeRules_GetForceDoorsOpenOrClosedFlag() )
              && ( stDoorState[enDoorIndex].enCurrentDoorState != DOOR__OPEN ) ) {
               stDoorState[enDoorIndex].enDoorCommand = DOOR_COMMAND__OPEN_IN_CAR_REQUEST;
               stDoorState[enDoorIndex].uwTimer_100ms = 0;
            }
            result = DOOR__OPEN;
         }
         else
         {
            result = DOOR__PARTIALLY_OPEN;
         }
         break;
      case DOOR_TYPE__MANUAL:
         if( bDoorFullyClosed )
         {
            result = DOOR__CLOSED;
            if ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
            {
               stDoorState[enDoorIndex].enDoorCommand = DOOR_COMMAND__CLOSE;
               stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
               stDoorState[enDoorIndex].uwTimer_100ms = MOD_DOORS_ONE_SECOND_SCALE_FACTOR *
                                                  Param_ReadValue_8Bit( enPARAM8__DoorStuckTime_1s );
            }
         }
         else
         {
            result = DOOR__OPEN;
            stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
            if( GetOperation_ClassOfOp() == CLASSOP__AUTO )
            {
               stDoorState[enDoorIndex].enDoorCommand = DOOR_COMMAND__OPEN_IN_CAR_REQUEST;
               stDoorState[enDoorIndex].uwTimer_100ms = 0;
            }
         }
         break;
      default: break;
   }
   return result;
}

/*-----------------------------------------------------------------------------
   Requested at OneFranklin PA job, CT stop switch should kill
   the I-Motion door operator, which requires constant DC/DO to maintain state

   The conditions below will cause all door outputs to be dropped.
 -----------------------------------------------------------------------------*/
static uint8_t CheckIf_DoorsKilled(void)
{
   uint8_t bDoorsKilled = 0;
   if( !GetPreflightFlag() )
   {
      if( Param_ReadValue_1Bit( enPARAM1__CT_Stop_SW_Kills_Doors )
       && !GetInputValue( enIN_CT_SW ) )
      {
         bDoorsKilled = 1;
      }
      else if( Param_ReadValue_1Bit( enPARAM1__DisableDoorsOnHA )
          && ( ( GetOperation_ManualMode() == MODE_M__INSP_HA ) ||
               ( GetOperation_ManualMode() == MODE_M__INSP_HA_TOP ) ||
               ( GetOperation_ManualMode() == MODE_M__INSP_HA_BOTTOM ) ) )
      {
         bDoorsKilled = 1;
      }
      else if( Param_ReadValue_1Bit(enPARAM1__Fire__FireStopSwitchKillsDoorOperator) )
      {
         if( !GetInputValue( enIN_FIRE_STOP_SW ) )
         {
            bDoorsKilled = 1;
         }
         else if( !Safety_GetInCarStopBypass()
               && !GetInputValue( enIN_ICST ) )
         {
            bDoorsKilled = 1;
         }
      }
   }
   return bDoorsKilled;
}

/* Handler for failed to close instance */
static void DoorFailedToClose ( enum en_doors enDoorIndex ) // todo
{
   uint8_t bUnlockedWithoutCAM = (GetFP_DoorType(enDoorIndex) == DOOR_TYPE__MANUAL) || (GetFP_DoorType(enDoorIndex) == DOOR_TYPE__FREIGHT);
   
   if (
         !GetDCL(enDoorIndex) &&
         GetDOL(enDoorIndex) &&
         !GetGSW(enDoorIndex) )
   {
      SetDoorFault(enDoorIndex, DOOR_FAULT__GSW_OPEN);
      SetTimersForDoorFault(enDoorIndex, 0, 60);
   }
   else if ( !GetDCL(enDoorIndex)
          && GetDOL(enDoorIndex)
          && ( ( !GetLocks(enDoorIndex) && !bUnlockedWithoutCAM )
            || ( !GetCloseContacts(enDoorIndex) && bUnlockedWithoutCAM && !Door_GetSwingClosedContactsProgrammedFlag(enDoorIndex) ) ) )
   {
      SetDoorFault(enDoorIndex, DOOR_FAULT__LOCKS_OPEN);
      SetTimersForDoorFault(enDoorIndex, 0, 60);
   }
   else
   {
      SetDoorFault(enDoorIndex, DOOR_FAULT__FAILED_TO_CLOSE);
      SetTimersForDoorFault(enDoorIndex, 0, 30);
   }
}

/* Checks for loss of door signals during a run */
static void CheckForLostDoorSignal( enum en_doors enDoorIndex )
{
   uint8_t bFullyClosed = IsDoorFullyClosed(enDoorIndex);
   //while running, check door status while running if the door status is in the 'closed' position.
   if( !GetPreflightFlag() && GetMotion_RunFlag() && (stDoorState[enDoorIndex].enCurrentDoorState == DOOR__CLOSED) )
   {
      uint16_t uwLockClipTime_100ms = Param_ReadValue_16Bit( enPARAM16__LockClipTime_10ms )/LOCK_CLIP_TIME_SCALE_FACTOR;
      uint8_t bDoorOkay = 1;
      uint8_t bSwingHallDoor = Doors_CheckIfManualSwingDoor(enDoorIndex, INVALID_FLOOR);
      uint8_t bDZ = GetDZ(enDoorIndex);
      if( bSwingHallDoor && bDZ )
      {
         uwLockClipTime_100ms = Param_ReadValue_8Bit(enPARAM8__TimeoutLockAndCAM_100ms);
         if( !uwLockClipTime_100ms )
         {
            uwLockClipTime_100ms = DEFAULT_MANUAL_DOORS_PREPARE_TO_RUN_TIMEOUT_1MS/100;
         }
      }

      switch( aenDoorType[enDoorIndex] )
      {
         case DOOR_TYPE__AUTOMATIC:
         case DOOR_TYPE__FREIGHT:
         case DOOR_TYPE__SWING:
            if( ( bFullyClosed ) || ( InDestinationDoorzone() ) )
            {
               stDoorState[enDoorIndex].uwTimer_100ms = 0;
            }
            else
            {
               //this is a generic check for loss of any signal loss from the doors while running
               if (stDoorState[enDoorIndex].uwTimer_100ms > uwLockClipTime_100ms)
               {
                  //set fault
                  SetDoorFault(enDoorIndex, DOOR_FAULT__LOST_DOOR_SIGNAL);
                  SetTimersForDoorFault(enDoorIndex, 0, 30);
               }
               else
               {
                  stDoorState[enDoorIndex].uwTimer_100ms++;
               }
            }
            break;
         case DOOR_TYPE__MANUAL:
            if( !GetDZ(enDoorIndex) )
            {
               if( ( bFullyClosed ) || ( InDestinationDoorzone() ) )
               {
                  stDoorState[enDoorIndex].uwTimer_100ms = 0;
               }
               else
               {
                  //this is a generic check for loss of any signal loss from the doors while running
                  if (stDoorState[enDoorIndex].uwTimer_100ms > uwLockClipTime_100ms)
                  {
                     //set fault
                     SetDoorFault(enDoorIndex, DOOR_FAULT__LOST_DOOR_SIGNAL);
                     SetTimersForDoorFault(enDoorIndex, 0, 30);
                  }
                  else
                  {
                     stDoorState[enDoorIndex].uwTimer_100ms++;
                  }
               }
            }
            break;
         default: break;
      }
   }
   else if( ( aenDoorType[enDoorIndex] == DOOR_TYPE__MANUAL ) && !GetMotion_RunFlag() && GetDZ(enDoorIndex) ) // todo review port
   {
      stDoorState[enDoorIndex].uwTimer_100ms = 0;
   }
   else if( ( stDoorState[enDoorIndex].enCurrentDoorState == DOOR__CLOSED )
         && ( !GetMotion_RunFlag() )
         && ( bFullyClosed ) )
   {
      stDoorState[enDoorIndex].uwTimer_100ms = 0;
   }
}
/*-----------------------------------------------------------------------------
   Check & Execution of no demand doors open
      Normal Op
      Param option set
      No request for run
      No request to close
 -----------------------------------------------------------------------------*/
static void NoDemandDoorsOpen(void)
{
   static uint8_t aucCounter_100ms[NUM_OF_DOORS];
   static uint8_t abLastOpenDoors[NUM_OF_DOORS];
   uint8_t abOpenDoors[NUM_OF_DOORS] = {0, 0};
   if( Param_ReadValue_1Bit(enPARAM1__NoDemandDoorsOpen) )
   {
      if( ( GetOperation_AutoMode() == MODE_A__NORMAL )
       && ( !GetMotion_RunFlag() )
       && ( getCaptureMode() == CAPTURE_OFF )
       && ( GetNextDestination_AnyDirection() >= GetFP_NumFloors() ) )
      {
         for( enum en_doors enDoorIndex = DOOR_FRONT; enDoorIndex < ucNumberOfDoors; enDoorIndex++ )
         {
            if( ( aenDoorType[enDoorIndex] == DOOR_TYPE__MANUAL )
             || ( GetLastDoorCommand(enDoorIndex) == DOOR_COMMAND__CLOSE )
             || ( GetLastDoorCommand(enDoorIndex) == DOOR_COMMAND__CLOSE_CONSTANT_PRESSURE ) )
            {
               aucCounter_100ms[enDoorIndex] = 0;
            }
            else if( aucCounter_100ms[enDoorIndex] < DEBOUNCE_NO_DEMAND_DOOR_OPEN_SIGNAL_100MS )
            {
               aucCounter_100ms[enDoorIndex]++;
            }
         }
      }
      else
      {
         aucCounter_100ms[DOOR_FRONT] = 0;
         aucCounter_100ms[DOOR_REAR] = 0;
      }

      for( enum en_doors enDoorIndex = DOOR_FRONT; enDoorIndex < ucNumberOfDoors; enDoorIndex++ )
      {
         abOpenDoors[enDoorIndex] = ( aucCounter_100ms[enDoorIndex] >= DEBOUNCE_NO_DEMAND_DOOR_OPEN_SIGNAL_100MS )? 1:0;
         if( aenDoorType[enDoorIndex] != DOOR_TYPE__MANUAL )
         {
            if( ( abOpenDoors[enDoorIndex] )
             && ( GetLastDoorCommand(enDoorIndex) != DOOR_COMMAND__OPEN_HOLD_REQUEST ) )
            {
               // Assert door hold command
               SetDoorCommand(enDoorIndex, DOOR_COMMAND__OPEN_HOLD_REQUEST);
            }
            else if( !abOpenDoors[enDoorIndex] && abLastOpenDoors[enDoorIndex] )
            {
               // After the loss of the hold doors signal, clear the hold doors command.
               Doors_ClearDoorHoldCommand(enDoorIndex);
            }

         }
         abLastOpenDoors[enDoorIndex] = abOpenDoors[enDoorIndex];
      }
   }
   bNoDemandDoorsOpen = abOpenDoors[DOOR_FRONT] | abOpenDoors[DOOR_REAR];
}
/*-----------------------------------------------------------------------------
   Handles PHE testing that must occur for Peelle freight door operator prior to close
 -----------------------------------------------------------------------------*/
static void UpdateFreightTestPHE( enum en_doors enDoorIndex )
{
   uint8_t bFireBypassesPHE = ( Fire_CheckIfFire2DoorsEnabled() || Fire_CheckIfFire1Recall() ) && !Param_ReadValue_1Bit(enPARAM1__Fire__EnablePHE_OnPhase2);
   if( ( !Param_ReadValue_1Bit(enPARAM1__FreightTestPHE) )
    && ( aenDoorType[enDoorIndex] == DOOR_TYPE__FREIGHT )
    && ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
    && ( !bFireBypassesPHE ) )
   {
      if( stDoorState[enDoorIndex].enCurrentDoorState != DOOR__OPEN )
      {
         stDoorState[enDoorIndex].eTestPHE_State = PHE_TEST_STATE__WAITING;
         stDoorState[enDoorIndex].ucTestPHE_Counter_100ms = 0;
         stDoorState[enDoorIndex].ucTestPHE_Timeout_100ms = 0;
      }
      else
      {
         switch(stDoorState[enDoorIndex].eTestPHE_State)
         {
            case PHE_TEST_STATE__WAITING:
               // Wait for PHE to be inactive for a certain period, freight buzzer must also be on to prevent triggering the test as people are entering the car
               if( ( GetOutputValue( stDoorSignals[enDoorIndex].FREIGHT_BUZZER ) )
                || ( stDoorState[enDoorIndex].uwCurrentTimer_100ms > stDoorState[enDoorIndex].uwTimer_100ms ))
               {
                  if( GetInputValue(stDoorSignals[enDoorIndex].PHE) && GetInputValue(stDoorSignals[enDoorIndex].PHE2) )
                  {
                     stDoorState[enDoorIndex].eTestPHE_State = PHE_TEST_STATE__IN_PROGRESS;
                     stDoorState[enDoorIndex].ucTestPHE_Counter_100ms = 0;
                     stDoorState[enDoorIndex].ucTestPHE_Timeout_100ms = 0;
                  }
               }
               break;
            case PHE_TEST_STATE__IN_PROGRESS:
               if( !GetInputValue(stDoorSignals[enDoorIndex].PHE) && !GetInputValue(stDoorSignals[enDoorIndex].PHE2) )
               {
                  if( ++stDoorState[enDoorIndex].ucTestPHE_Counter_100ms >= PHE_TEST_COUNTER_TIME_100MS )
                  {
                     stDoorState[enDoorIndex].eTestPHE_State = PHE_TEST_STATE__PASSED;
                     stDoorState[enDoorIndex].ucTestPHE_Counter_100ms = 0;
                  }
               }
               else
               {
                  stDoorState[enDoorIndex].ucTestPHE_Counter_100ms = 0;
                  if( stDoorState[enDoorIndex].ucTestPHE_Timeout_100ms >= PHE_TEST_TIMEOUT_100MS )
                  {
                     SetFault(FLT__PHE_TEST_FAILED);
                  }
                  else
                  {
                     stDoorState[enDoorIndex].ucTestPHE_Timeout_100ms++;
                  }
               }
               break;
            case PHE_TEST_STATE__PASSED:
               // Wait to move from the open state.
               break;
            default:
               stDoorState[enDoorIndex].eTestPHE_State = PHE_TEST_STATE__WAITING;
               stDoorState[enDoorIndex].ucTestPHE_Counter_100ms = 0;
               break;
         }
      }
   }
   else
   {
      stDoorState[enDoorIndex].eTestPHE_State = PHE_TEST_STATE__PASSED;
      stDoorState[enDoorIndex].ucTestPHE_Counter_100ms = 0;
      stDoorState[enDoorIndex].ucTestPHE_Timeout_100ms = 0;
   }
}
/*-----------------------------------------------------------------------------
Check for car/hall door bypass
 -----------------------------------------------------------------------------*/
static void UpdateDoorBypassFlags(void)
{
   bCarDoorsBypassed = 0;
   bHallDoorsBypassed = 0;
   if( !GetPreflightFlag() )
   {
      if ( GetOperation_ManualMode() == MODE_M__CONSTRUCTION )
      {
         bCarDoorsBypassed = 1;
         bHallDoorsBypassed = 1;
      }
      else if( ( GetOperation_ManualMode() == MODE_M__INSP_CT )
            || ( GetOperation_ManualMode() == MODE_M__INSP_IC ) )
      {
         if( GetInputValue( enIN_BYPC ) )
         {
            bCarDoorsBypassed = 1;
         }
         if( GetInputValue( enIN_BYPH ) )
         {
            bHallDoorsBypassed = 1;
         }
      }
      // TODO HA bypass conditions can be moved here
      else if( GetOperation_ManualMode() == MODE_M__INSP_HA_TOP )
      {
      }
      else if( GetOperation_ManualMode() == MODE_M__INSP_HA_BOTTOM )
      {
      }
   }
}

/*-----------------------------------------------------------------------------
   In certain instances, doors can remain stuck open,
   such as when switching from INDEP SRV or INSPECTION to normal.
   In these cases doors seem to remain open until commanded to close,
   even if there is demand.
   This function covers these cases.

 -----------------------------------------------------------------------------*/
static void CheckForDoorsStalledOpen(void)
{
   static enum en_mode_auto eLastAutoState;
   static uint8_t bLastDoorHoldFlag;
   enum en_mode_auto eAutoState = GetOperation_AutoMode();
   if( GetOperation_ClassOfOp() == CLASSOP__AUTO )
   {
      for( enum en_doors enDoorIndex = DOOR_FRONT; enDoorIndex < ucNumberOfDoors; enDoorIndex++ )
      {
         if( ( aenDoorType[enDoorIndex] != DOOR_TYPE__MANUAL )
          && ( stDoorState[enDoorIndex].enCurrentDoorState == DOOR__OPEN )
          && ( stDoorState[enDoorIndex].enLastDoorState == DOOR__OPEN )
          && ( !stDoorState[enDoorIndex].uwCurrentTimer_100ms && !stDoorState[enDoorIndex].uwTimer_100ms )
          && ( !AutoModeRules_GetDoorHoldFlag() )
          && ( !AutoModeRules_GetForceDoorsOpenOrClosedFlag() )
          && ( GetRecallState() != RECALL__RECALL_FINISHED )
          && ( ( stDoorState[enDoorIndex].enDoorCommand != DOOR_COMMAND__OPEN_HOLD_REQUEST )
            || ( bLastDoorHoldFlag && ( eLastAutoState != eAutoState ) ) ) )
         {
            /* Reset hold timers (when uwCurrentTimer and uwTimer
             * are stalled at zero) by commanding close briefly */
            SetDoorCommand(enDoorIndex, DOOR_COMMAND__CLOSE);
         }
      }
      bLastDoorHoldFlag = AutoModeRules_GetDoorHoldFlag();
   }
   else
   {
      bLastDoorHoldFlag = 0;
   }
   eLastAutoState = eAutoState;
}

/*-----------------------------------------------------------------------------

   DOOR STATES

 -----------------------------------------------------------------------------*/
// DOOR__OPEN
static void DoorState_Open ( enum en_doors enDoorIndex )
{
   uint8_t bDoorsKilled = CheckIf_DoorsKilled();
   uint8_t bDoorFullyOpen = IsDoorFullyOpen( enDoorIndex );
   uint8_t bDoorFullyClosed = IsDoorFullyClosed( enDoorIndex );
   if( stDoorState[enDoorIndex].enLastFullDoorState != DOOR__OPEN )
   {
      stDoorState[enDoorIndex].uwOpenLimitTimer_100ms = 0;
      stDoorState[enDoorIndex].enLastFullDoorState = DOOR__OPEN;
      if( stDoorState[enDoorIndex].uwCurrentTimer_100ms )
      {
         stDoorState[enDoorIndex].uwCurrentTimer_100ms = 1;
      }

      /* Capture door opening time for door state estimation during preflight */
      if( !Param_ReadValue_1Bit(enPARAM1__DEBUG_DisablePreflight) )
      {
         /* Prevent auto saving value as soon as preflight is disabled. */
         stDoorState[enDoorIndex].ucOpeningTime_100ms = 0;
      }
      else if( Param_ReadValue_1Bit(enPARAM1__DEBUG_DisablePreflight)
            && Param_ReadValue_1Bit(enPARAM1__LearnOpeningTime)
            && stDoorState[enDoorIndex].ucOpeningTime_100ms )
      {
         Param_WriteValue_1Bit(enPARAM1__LearnOpeningTime, 0);
         Param_WriteValue_8Bit(enPARAM8__DoorOpeningTime_100ms, stDoorState[enDoorIndex].ucOpeningTime_100ms*1.10);
      }
   }

   switch(aenDoorType[enDoorIndex])
   {
      case DOOR_TYPE__AUTOMATIC:
      case DOOR_TYPE__SWING:
      case DOOR_TYPE__FREIGHT:
         if( CheckIf_DoorsKilled() )
         {
            stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
         }
         else if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
         {
            stDoorState[enDoorIndex].uwOpenLimitTimer_100ms = 0;
         }
         else // Automatic
         {
            if( stDoorState[enDoorIndex].eTestPHE_State == PHE_TEST_STATE__PASSED )
            {
               uint8_t bFireBypassesPHE = ( Fire_CheckIfFire2DoorsEnabled() || Fire_CheckIfFire1Recall() ) && !Param_ReadValue_1Bit(enPARAM1__Fire__EnablePHE_OnPhase2);
               uint8_t bNeedToOpen = !GetGSW(enDoorIndex);
               bNeedToOpen &= ( ( aenDoorType[enDoorIndex] == DOOR_TYPE__FREIGHT ) && !GetSafetyEdge(enDoorIndex) )
                             || ( !GetPhotoEye(enDoorIndex) && !bFireBypassesPHE );
               /* Check for elapsed dwell timer */
               uint16_t uwNudgeTime = ( Param_ReadValue_8Bit(enPARAM8__DoorNudgeTime_1s) * MOD_DOORS_ONE_SECOND_SCALE_FACTOR );

               if ((GetOperation_AutoMode() == MODE_A__SABBATH)
                     && ( stDoorState[enDoorIndex].uwCurrentTimer_100ms > stDoorState[enDoorIndex].uwTimer_100ms )
                     && ( Param_ReadValue_1Bit(enPARAM1__Sabbath_NudgeDoors)))
               {
                  processDoorCloseRequest(enDoorIndex, DOOR__NUDGING);
               }
               else if (stDoorState[enDoorIndex].uwCurrentTimer_100ms > ( uwNudgeTime + stDoorState[enDoorIndex].uwTimer_100ms ))
               {
                  processDoorCloseRequest(enDoorIndex, DOOR__NUDGING);
               }
               else if( ( stDoorState[enDoorIndex].uwCurrentTimer_100ms > stDoorState[enDoorIndex].uwTimer_100ms ) && ( !bNeedToOpen ))
               {
                  processDoorCloseRequest(enDoorIndex, DOOR__CLOSING);
               }
            }

            /* Check for failure to open */
            if( !bDoorFullyOpen )
            {
               if(stDoorState[enDoorIndex].uwOpenLimitTimer_100ms > DOOR_FAILED_TO_OPEN_TIMEOUT_100MS)
               {
                  SetDoorFault(enDoorIndex, DOOR_FAULT__FAILED_TO_OPEN);
                  SetTimersForDoorFault(enDoorIndex, 0, 30);
               }
               else if( !GetPreflightFlag() )
               {
                  stDoorState[enDoorIndex].uwOpenLimitTimer_100ms++;
               }
            }
            else
            {
               stDoorState[enDoorIndex].uwOpenLimitTimer_100ms = 0;
            }
         }
         break;
      case DOOR_TYPE__MANUAL:
         if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
         {
            if( bDoorFullyClosed )
            {
               processDoorCloseRequest(enDoorIndex, DOOR__CLOSED);
            }
         }
         else
         {
            uint16_t uwNudgeTime = ( Param_ReadValue_8Bit(enPARAM8__DoorNudgeTime_1s) * MOD_DOORS_ONE_SECOND_SCALE_FACTOR );
            if ( uwNudgeTime
            && ( stDoorState[enDoorIndex].uwCurrentTimer_100ms  > ( uwNudgeTime + stDoorState[enDoorIndex].uwTimer_100ms ) ) )
            {
               processDoorCloseRequest(enDoorIndex, DOOR__NUDGING);
            }
            else if( IsDoorFullyClosed( enDoorIndex ) )
            {
               processDoorCloseRequest(enDoorIndex, DOOR__CLOSED);
            }
         }
         break;
      default: break;
   }

   Doors_SetOutputs_DoorOpenState(enDoorIndex);
}
// DOOR__OPENING
static void DoorState_Opening ( enum en_doors enDoorIndex )
{
   if( stDoorState[enDoorIndex].ucOpeningTime_100ms <= Param_ReadValue_8Bit(enPARAM8__DoorOpeningTime_100ms) )
   {
      stDoorState[enDoorIndex].ucOpeningTime_100ms++;
   }
   uint8_t bEstimatedDoorOpen = ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
                             && ( !Param_ReadValue_1Bit(enPARAM1__DEBUG_DisablePreflight) )
                             && ( Param_ReadValue_8Bit(enPARAM8__DoorOpeningTime_100ms) )
                             && ( stDoorState[enDoorIndex].ucOpeningTime_100ms > Param_ReadValue_8Bit(enPARAM8__DoorOpeningTime_100ms) );
   uint8_t bDoorFullyOpen = IsDoorFullyOpen( enDoorIndex );
   uint8_t bDoorFullyClosed = IsDoorFullyClosed( enDoorIndex );
   switch(aenDoorType[enDoorIndex])
   {
      case DOOR_TYPE__AUTOMATIC:
      case DOOR_TYPE__FREIGHT:
      case DOOR_TYPE__SWING:
         if( CheckIf_DoorsKilled() )
         {
            stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
         }
         else if ( bDoorFullyOpen
              || ( bEstimatedDoorOpen ) )
         {
            stDoorState[enDoorIndex].enCurrentDoorState = DOOR__OPEN;
            stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;

            if ( AutoModeRules_GetForceDoorsOpenOrClosedFlag() ) {
               stDoorState[enDoorIndex].enDoorCommand = DOOR_COMMAND__OPEN_CONSTANT_PRESSURE;
            }

            switch (stDoorState[enDoorIndex].enDoorCommand) {

               case DOOR_COMMAND__OPEN_HOLD_REQUEST:
               case DOOR_COMMAND__OPEN_CONSTANT_PRESSURE:
                  //no timer, so door holds open until commanded to close
                  stDoorState[enDoorIndex].uwTimer_100ms = 0;
                  break;

               case DOOR_COMMAND__OPEN_UI_REQUEST:
               case DOOR_COMMAND__OPEN_IN_CAR_REQUEST:
                  stDoorState[enDoorIndex].uwTimer_100ms = (Param_ReadValue_8Bit(
                        enPARAM8__DoorDwellTime_1s) * MOD_DOORS_ONE_SECOND_SCALE_FACTOR);
                  break;

               case DOOR_COMMAND__OPEN_ADA_MODE:
                  stDoorState[enDoorIndex].uwTimer_100ms = (Param_ReadValue_8Bit(
                        enPARAM8__DoorDwellADATime_1s) * MOD_DOORS_ONE_SECOND_SCALE_FACTOR);
                  break;
               case DOOR_COMMAND__OPEN_SABBATH_MODE:
                  stDoorState[enDoorIndex].uwTimer_100ms = (Param_ReadValue_8Bit(
                        enPARAM8__DoorDwellSabbathTimer_1s) * MOD_DOORS_ONE_SECOND_SCALE_FACTOR);
                  break;

               case DOOR_COMMAND__OPEN_HALL_REQUEST:
                  stDoorState[enDoorIndex].uwTimer_100ms = (Param_ReadValue_8Bit(
                        enPARAM8__DoorDwellHallTime_1s) * MOD_DOORS_ONE_SECOND_SCALE_FACTOR);
                  break;

               case DOOR_COMMAND__OPEN_LOBBY_REQUEST:
                  stDoorState[enDoorIndex].uwTimer_100ms = ( MOD_DOORS_ONE_SECOND_SCALE_FACTOR * Param_ReadValue_8Bit(enPARAM8__LobbyDwellTime_1s) );
                  break;
               default:
                  stDoorState[enDoorIndex].uwTimer_100ms = 1;
                  break;
            }
         }
         else if( stDoorState[enDoorIndex].uwCurrentTimer_100ms >= (Param_ReadValue_8Bit(enPARAM8__DoorStuckTime_1s)*MOD_DOORS_ONE_SECOND_SCALE_FACTOR) )
         {
            SetDoorFault(enDoorIndex, DOOR_FAULT__FAILED_TO_OPEN);
            SetTimersForDoorFault(enDoorIndex, 0, 30);
         }
         break;
      case DOOR_TYPE__MANUAL:
         if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
         {
            if( bDoorFullyClosed )
            {
               processDoorCloseRequest(enDoorIndex, DOOR__CLOSED);
            }
         }
         else
         {
            uint16_t uwNudgeTime = ( Param_ReadValue_8Bit(enPARAM8__DoorNudgeTime_1s) * MOD_DOORS_ONE_SECOND_SCALE_FACTOR );
            if ( uwNudgeTime
            && ( stDoorState[enDoorIndex].uwCurrentTimer_100ms  > ( uwNudgeTime + stDoorState[enDoorIndex].uwTimer_100ms ) ) )
            {
               processDoorCloseRequest(enDoorIndex, DOOR__NUDGING);
            }
            else if( IsDoorFullyClosed( enDoorIndex ) )
            {
               processDoorCloseRequest(enDoorIndex, DOOR__CLOSED);
            }
         }
         break;
      default: break;
   }

   Doors_SetOutputs_DoorOpeningState(enDoorIndex);
}
// DOOR__CLOSING
static void DoorState_Closing ( enum en_doors enDoorIndex )
{
   uint8_t bFireBypassesPHE;
   uint8_t bNeedToOpen;
   stDoorState[enDoorIndex].ucOpeningTime_100ms = 0;
   uint8_t bDoorFullyClosed = IsDoorFullyClosed( enDoorIndex );
   switch(aenDoorType[enDoorIndex])
   {
      case DOOR_TYPE__AUTOMATIC:
      case DOOR_TYPE__FREIGHT:
      case DOOR_TYPE__SWING:
         bFireBypassesPHE = ( Fire_CheckIfFire2DoorsEnabled() || Fire_CheckIfFire1Recall() ) && !Param_ReadValue_1Bit(enPARAM1__Fire__EnablePHE_OnPhase2);
         bNeedToOpen = !GetGSW(enDoorIndex);
         bNeedToOpen &= ( ( aenDoorType[enDoorIndex] == DOOR_TYPE__FREIGHT ) && !GetSafetyEdge(enDoorIndex) )
                       || ( !GetPhotoEye(enDoorIndex) && !bFireBypassesPHE );
         if( CheckIf_DoorsKilled() )
         {
            stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
         }
         else if ( bDoorFullyClosed )
         {
            stDoorState[enDoorIndex].enCurrentDoorState = DOOR__CLOSED;
            stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
            stDoorState[enDoorIndex].uwTimer_100ms = 0;
         }
         /* If attempting to close w/o PHE while in auto op, reopen */
         else if( bNeedToOpen && GetDZ(enDoorIndex) )
         {
            /* If user is closing DCB with PHE, command a DO but remain
             * in door closing state instead of reverting to opening
             * so nudging can activate */
            if(!GetDoorCloseButton(enDoorIndex))
            {
               SetDoorCommand(enDoorIndex, DOOR_COMMAND__OPEN_IN_CAR_REQUEST);
            }
         }
         else if( stDoorState[enDoorIndex].uwCurrentTimer_100ms >= (Param_ReadValue_8Bit(enPARAM8__DoorStuckTime_1s)*MOD_DOORS_ONE_SECOND_SCALE_FACTOR) )
         {
            DoorFailedToClose(enDoorIndex);
         }
         break;

      case DOOR_TYPE__MANUAL:
         if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
         {
            if( bDoorFullyClosed )
            {
               processDoorCloseRequest(enDoorIndex, DOOR__CLOSED);
            }
         }
         else
         {
            uint16_t uwNudgeTime = ( Param_ReadValue_8Bit(enPARAM8__DoorNudgeTime_1s) * MOD_DOORS_ONE_SECOND_SCALE_FACTOR );
            if ( uwNudgeTime
            && ( stDoorState[enDoorIndex].uwCurrentTimer_100ms  > ( uwNudgeTime + stDoorState[enDoorIndex].uwTimer_100ms ) ) )
            {
               processDoorCloseRequest(enDoorIndex, DOOR__NUDGING);
            }
            else if( bDoorFullyClosed )
            {
               processDoorCloseRequest(enDoorIndex, DOOR__CLOSED);
            }
         }
         break;
      default: break;
   }

   Doors_SetOutputs_DoorClosingState(enDoorIndex);
}

// DOOR__NUDGING
static void DoorState_Nudging ( enum en_doors enDoorIndex )
{
   stDoorState[enDoorIndex].ucOpeningTime_100ms = 0;
   uint8_t bDoorFullyClosed = IsDoorFullyClosed( enDoorIndex );
   switch(aenDoorType[enDoorIndex])
   {
      case DOOR_TYPE__AUTOMATIC:
      case DOOR_TYPE__FREIGHT:
      case DOOR_TYPE__SWING:
         if( CheckIf_DoorsKilled() )
         {
            stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
         }
         else if (stDoorState[enDoorIndex].uwCurrentTimer_100ms > (Param_ReadValue_8Bit(enPARAM8__DoorStuckTime_1s)*MOD_DOORS_ONE_SECOND_SCALE_FACTOR) )
         {
            DoorFailedToClose(enDoorIndex);
         }
         else if ( bDoorFullyClosed )
         {
            stDoorState[enDoorIndex].enCurrentDoorState = DOOR__CLOSED;
            stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
            stDoorState[enDoorIndex].uwTimer_100ms = 0;
         }
         break;

      case DOOR_TYPE__MANUAL:
         if( bDoorFullyClosed )
         {
            stDoorState[enDoorIndex].enCurrentDoorState = DOOR__CLOSED;
            stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
            stDoorState[enDoorIndex].uwTimer_100ms = 0;
         }
         break;
      default: break;
   }

   Doors_SetOutputs_DoorNudgingState(enDoorIndex);
}


// DOOR__CLOSED
static void DoorState_Closed ( enum en_doors enDoorIndex )
{
   stDoorState[enDoorIndex].ucOpeningTime_100ms = 0;
   if( stDoorState[enDoorIndex].enLastFullDoorState != DOOR__CLOSED )
   {
      stDoorState[enDoorIndex].uwCloseLimitTimer_100ms = 0;
      stDoorState[enDoorIndex].enLastFullDoorState = DOOR__CLOSED;
      if( stDoorState[enDoorIndex].uwCurrentTimer_100ms )
      {
         stDoorState[enDoorIndex].uwCurrentTimer_100ms = 1;
      }
   }
   uint8_t bDoorFullyClosed = IsDoorFullyClosed( enDoorIndex );
   uint16_t uwCallClearTime_100ms = DEFAULT_MANUAL_DOOR_CALL_CLEAR_TIME_100MS + (Param_ReadValue_8Bit( enPARAM8__DoorDwellHallTime_1s )*MOD_DOORS_ONE_SECOND_SCALE_FACTOR);
   switch(aenDoorType[enDoorIndex])
   {
      case DOOR_TYPE__AUTOMATIC:
      case DOOR_TYPE__FREIGHT:
      case DOOR_TYPE__SWING:
         if( CheckIf_DoorsKilled() )
         {
            stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
            stDoorState[enDoorIndex].uwCloseLimitTimer_100ms = 0;
            stDoorState[enDoorIndex].uwOpenLimitTimer_100ms = 0;
         }
         else if( GetOperation_ClassOfOp() != CLASSOP__MANUAL )
         {
            if( ( !GetPreflightFlag() )
             && ( !bDoorFullyClosed ) )
            {
               if (stDoorState[enDoorIndex].uwCloseLimitTimer_100ms > DOOR_FAILED_TO_CLOSE_TIMEOUT_100MS)
               {
                  SetDoorFault(enDoorIndex, DOOR_FAULT__FAILED_TO_CLOSE);
                  SetTimersForDoorFault(enDoorIndex, 0, 30);
               }
               else
               {
                  stDoorState[enDoorIndex].uwCloseLimitTimer_100ms++;
               }
            }
            else
            {
               stDoorState[enDoorIndex].uwCloseLimitTimer_100ms = 0;
            }
         }
         break;

      case DOOR_TYPE__MANUAL:
         if( ( GetOperation_ClassOfOp() == CLASSOP__MANUAL ) )
         {
            stDoorState[enDoorIndex].uwCloseLimitTimer_100ms = 0;
            if( !bDoorFullyClosed )
            {
               processDoorOpenRequest(enDoorIndex, DOOR__OPEN);
            }
         }
         else
         {
            if( !GetPreflightFlag() && !bDoorFullyClosed )
            {
               if(stDoorState[enDoorIndex].uwCloseLimitTimer_100ms > DOOR_FAILED_TO_CLOSE_MANUAL_TIMEOUT_100MS)
               {
                  processDoorOpenRequest(enDoorIndex, DOOR__OPEN);
               }
               else
               {
                  stDoorState[enDoorIndex].uwCloseLimitTimer_100ms++;
               }
            }
            else if( bDoorFullyClosed )
            {
               stDoorState[enDoorIndex].uwCloseLimitTimer_100ms = 0;
               if( ( stDoorState[enDoorIndex].uwCallDropTimer_100ms > uwCallClearTime_100ms )
                && ( !GetMotion_RunFlag() ) )
               {
                  ClearCurrentLandingCalls_ManualDoor(enDoorIndex);
               }
               else if( !GetMotion_RunFlag() && GetCurrentLandingCalls() )
               {
                  stDoorState[enDoorIndex].uwCallDropTimer_100ms++;
               }
               else
               {
                  stDoorState[enDoorIndex].uwCallDropTimer_100ms = 0;
               }
            }
         }
         break;
      default: break;
   }

   Doors_SetOutputs_DoorClosedState(enDoorIndex);
}
// DOOR__PARTIALLY_OPEN
static void DoorState_PartiallyOpen( enum en_doors enDoorIndex )
{
   stDoorState[enDoorIndex].ucOpeningTime_100ms = 0;
   /* If there's a door fault, hold doors idle and await door state transition from doorFault.c */
   if( GetDoorFault(enDoorIndex) )
   {
      SetAllDoorOutputsOff(enDoorIndex);
   }
   else if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
   {
      SetAllDoorOutputsOff(enDoorIndex);
      if( ( IsDoorFullyOpen(enDoorIndex) )
       || ( IsDoorFullyClosed(enDoorIndex) )
       || ( stDoorState[enDoorIndex].enDoorCommand == DOOR_COMMAND__NONE ) )
      {
         stDoorState[enDoorIndex].enCurrentDoorState = DecideDoorState(enDoorIndex);
      }
   }
   else // Auto/learn & w/o door fault
   {
      if( GetOperation_AutoMode() != MODE_A__UNKNOWN ) // May not be needed
      {
         if ( !IsDoorFullyClosed(enDoorIndex) && !IsDoorFullyOpen(enDoorIndex) )
         {
            if ( ( GetDoorCloseButton(enDoorIndex) )
            || ( ( !GetDoorOpenButton(enDoorIndex) ) && ( stDoorState[enDoorIndex].enLastFullDoorState == DOOR__CLOSED ) ) ) {
               processDoorCloseRequest(enDoorIndex, DOOR__CLOSING);
            } else if ( ( GetDoorOpenButton(enDoorIndex) )
                   || ( ( getDoorZone(enDoorIndex) ) && ( stDoorState[enDoorIndex].enLastFullDoorState == DOOR__OPEN ) ) ) {
               processDoorOpenRequest(enDoorIndex, DOOR_COMMAND__OPEN_IN_CAR_REQUEST);
            } else if ( ( Fire_CheckIfFire2DoorsEnabled() ) && (stDoorState[enDoorIndex].enLastFullDoorState == DOOR__UNKNOWN) ) {
               stDoorState[enDoorIndex].enCurrentDoorState = DecideDoorState(enDoorIndex);
               /* This covers the case if the car starts up on FF2 with doors partially open.
                * Since it doesn't know the last full door state, assume the last full state
                * was closed if the locks are all closed.  */
               if( stDoorState[enDoorIndex].enCurrentDoorState == DOOR__PARTIALLY_OPEN )
               {
                  if( GetLocks(enDoorIndex) )
                  {
                     stDoorState[enDoorIndex].enLastFullDoorState = DOOR__CLOSED;
                  }
                  else
                  {
                     stDoorState[enDoorIndex].enLastFullDoorState = DOOR__OPEN;
                  }
               }
            } else if ( ( GetOperation_AutoMode() == MODE_A__FIRE1 ) && ( getDoorZone(enDoorIndex) ) && ( !GetDoorCloseButton(enDoorIndex) ) ) {
               SetDoorCommand(enDoorIndex, DOOR_COMMAND__OPEN_HOLD_REQUEST);
            } else if ( ( !getDoorZone(enDoorIndex) )
                    ||  ( stDoorState[enDoorIndex].enCurrentDoorState == DOOR__CLOSING )
                    ||  ( stDoorState[enDoorIndex].enCurrentDoorState == DOOR__NUDGING )
                    ||  ( stDoorState[enDoorIndex].enLastDoorCommand == DOOR_COMMAND__CLOSE ) ) {
               processDoorCloseRequest(enDoorIndex, DOOR__CLOSING);
            } else if ( getDoorZone(enDoorIndex) ){
               processDoorOpenRequest(enDoorIndex, DOOR_COMMAND__OPEN_HOLD_REQUEST);
            }
         }
         /* If doors already fully opened or closed update door state */
         else
         {
            SetAllDoorOutputsOff(enDoorIndex);
            stDoorState[enDoorIndex].enCurrentDoorState = DecideDoorState(enDoorIndex);
         }
      }
   }
   Doors_SetOutputs_DoorPartiallyOpenState(enDoorIndex);
}
/*-----------------------------------------------------------------------------
   Once the init functionality is fixed,
   we can switch this to being the Run function.
 -----------------------------------------------------------------------------*/
static void FullRun ( void )
{
   /* Verify that the CPLD and MCU agree on if there are rear doors. */
   if ( !GetMotion_RunFlag() && ( SRU_Read_DIP_Switch( enSRU_DIP_B2 ) != GetFP_RearDoors() ) )
   {
      SetFault(FLT__INVALID_DIP_SW_B2);
   }

   for (enum en_doors enDoorIndex = 0; enDoorIndex < ucNumberOfDoors; enDoorIndex++)
   {
      //Increment timer as long as it isn't set to 0;
      if ( ( stDoorState[enDoorIndex].uwCurrentTimer_100ms || stDoorState[enDoorIndex].uwTimer_100ms )
        && ( stDoorState[enDoorIndex].uwCurrentTimer_100ms < DOOR_TIMER_LIMIT_100MS ) )
      {
         stDoorState[enDoorIndex].uwCurrentTimer_100ms++;
      }
      /* Guard against unreachable door timer limit */
      else if( stDoorState[enDoorIndex].uwTimer_100ms > DOOR_TIMER_LIMIT_100MS )
      {
         uint16_t uwTimerDiff = stDoorState[enDoorIndex].uwTimer_100ms - stDoorState[enDoorIndex].uwCurrentTimer_100ms;
         stDoorState[enDoorIndex].uwCurrentTimer_100ms = 0;
         stDoorState[enDoorIndex].uwTimer_100ms = ( uwTimerDiff <= DOOR_TIMER_LIMIT_100MS ) ? uwTimerDiff:1;
         // TODO add diagnostic alarm to inform coding error
      }

      DoorFaultHandler(enDoorIndex);
      UpdateFreightTestPHE(enDoorIndex);
      switch (stDoorState[enDoorIndex].enCurrentDoorState)
      {
         case DOOR__OPEN:             DoorState_Open(enDoorIndex);              break;

         case DOOR__CLOSED:           DoorState_Closed(enDoorIndex);            break;

         case DOOR__PARTIALLY_OPEN:   DoorState_PartiallyOpen(enDoorIndex);     break;

         case DOOR__OPENING:          DoorState_Opening(enDoorIndex);           break;

         case DOOR__CLOSING:          DoorState_Closing(enDoorIndex);           break;

         case DOOR__NUDGING:          DoorState_Nudging(enDoorIndex);           break;

         case DOOR__UNKNOWN:
         default:
            SetAllDoorOutputsOff(enDoorIndex);
            stDoorState[enDoorIndex].ucOpeningTime_100ms = 0;
            stDoorState[enDoorIndex].enCurrentDoorState = DecideDoorState(enDoorIndex);
            break;
      }

      CheckForLostDoorSignal(enDoorIndex);
      stDoorState[enDoorIndex].enLastDoorState = stDoorState[enDoorIndex].enCurrentDoorState;
   }
}


/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 7500;
   pstThisModule->uwRunPeriod_1ms    = MOD_RUN_PERIOD_DOORS_1MS;

   // Initial state for doors are unknown and No Command.
   for (uint8_t i =0; i < NUM_OF_DOORS; i++) {
      stDoorState[ i ].enDoorCommand = DOOR_COMMAND__NONE;
      stDoorState[ i ].enCurrentDoorState = DOOR__UNKNOWN;
      stDoorState[ i ].enLastFullDoorState = DOOR__UNKNOWN;
      stDoorState[ i ].uwCurrentTimer_100ms = 0; //clear timers
      stDoorState[ i ].uwTimer_100ms = 0;
      stDoorState[ i ].ucOpeningTime_100ms = 0;
      stDoorState[ i ].uwOpenLimitTimer_100ms = 0; //clear timers
      stDoorState[ i ].uwCloseLimitTimer_100ms = 0;
      stDoorState[ i ].uwCallDropTimer_100ms = 0;
   }

   ucNumberOfDoors = (GetFP_RearDoors()+1);
   aenDoorType[DOOR_FRONT] = GetFP_DoorType(DOOR_FRONT);
   aenDoorType[DOOR_REAR] = GetFP_DoorType(DOOR_REAR);

   return 0;
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   if( ( ucNumberOfDoors > 0 ) && ( ucNumberOfDoors <= NUM_OF_DOORS ) && CheckIfAllDoorInputsInitialized() && CheckIf_AllStartupSignalsReceived() && FRAM_EmergencyBitmapReady() )
   {
      CheckForDoorsStalledOpen();
      UpdateDoorBypassFlags();
      NoDemandDoorsOpen();
      FullRun();
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
