/******************************************************************************
 *
 * @file     doorFault.c
 * @brief    Door Faults
 * @version  V2.00
 * @date     29 August 2018
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "sru_a.h"
#include "sys.h"
#include "mod.h"
#include "GlobalData.h"
#include "doors_private.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define DEFAULT_DOOR_FAULT_TIMEOUT_100MS           (150)
#define DOOR_FAULT_DELAY_100MS                     (30)

#define DEFAULT_JUMPER_ON_LOCKS_TIMEOUT_100MS      (16)
#define DEFAULT_JUMPER_ON_GSW_TIMEOUT_100MS        (16)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
static enum en_door_faults aeFaultIndex[NUM_OF_DOORS];
static enum en_door_faults aeLastFaultIndex[NUM_OF_DOORS];

static uint16_t auwFaultTimer_100ms[NUM_OF_DOORS]; // Records time a fault has been active to trigger abort sequence
static uint16_t auwDelayTimer_100ms[NUM_OF_DOORS];

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void SetDoorFault( enum en_doors enDoorIndex, enum en_door_faults eFault )
{
   aeFaultIndex[enDoorIndex] = eFault;
   SetDoorCommand(enDoorIndex, DOOR_COMMAND__FAULT);
}
enum en_door_faults GetDoorFault( enum en_doors enDoorIndex )
{
   return aeFaultIndex[enDoorIndex];
}
enum en_door_faults GetLastDoorFault( enum en_doors enDoorIndex )
{
   return aeLastFaultIndex[enDoorIndex];
}
/* If a door fault's handling conflicts with the door command, it can be cleared */
void CheckIfDoorCommandClearsFault( enum en_doors enDoorIndex )
{
   if( ( aeFaultIndex[enDoorIndex] )
    && ( auwDelayTimer_100ms[enDoorIndex] >= DOOR_FAULT_DELAY_100MS ) ) /* Wait until idle time is elapse to ensure the fault is logged */
   {
      if( GetDoorOpenButton(enDoorIndex) )
      {

      }
      else if( GetDoorCloseButton(enDoorIndex) )
      {

      }
   }
}

/*-----------------------------------------------------------------------------
Door state independent locks jumper check
Checks that locks are made and either DOL is made, or GSW is open
 -----------------------------------------------------------------------------*/
static void CheckForJumperOnGSWFault( enum en_doors enDoorIndex )
{
   static uint8_t aucDebounce_100ms[NUM_OF_DOORS];
   uint8_t bJumperDetected = 0;
   if( !Param_ReadValue_1Bit(enPARAM1__DISA_DoorJumperCheck) )
   {
      if( aenDoorType[enDoorIndex] == DOOR_TYPE__MANUAL )
      {
         if( GetGSW(enDoorIndex) && !GetDPM(enDoorIndex) )
         {
            bJumperDetected = 1;
         }
      }
      else
      {
         if( ( GetGSW( enDoorIndex ) )
          && ( !GetDOL( enDoorIndex ) )
          && ( GetDCL( enDoorIndex ) ) )
         {
            bJumperDetected = 1;
         }
      }
   }

   if(bJumperDetected)
   {
      uint16_t ucTimeout_100ms = Param_ReadValue_8Bit(enPARAM8__DoorJumperTimeout_100ms);
      ucTimeout_100ms += DEFAULT_JUMPER_ON_GSW_TIMEOUT_100MS;
      if( ucTimeout_100ms > 254 )
      {
         ucTimeout_100ms = 254;
      }

      if(aucDebounce_100ms[enDoorIndex] < ucTimeout_100ms)
      {
         aucDebounce_100ms[enDoorIndex]++;
      }
      else
      {
         SetDoorFault(enDoorIndex, DOOR_FAULT__JUMPER_ON_GSW);
         SetTimersForDoorFault(enDoorIndex, 0, 30);
      }
   }
   else
   {
      aucDebounce_100ms[enDoorIndex] = 0;
   }
}
/*-----------------------------------------------------------------------------
Door state independent locks jumper check
Checks that locks are made and either DOL is made, or GSW is open
 -----------------------------------------------------------------------------*/
static void CheckForJumperOnLocksFault( enum en_doors enDoorIndex )
{
   static uint8_t aucDebounce_100ms[NUM_OF_DOORS];
   uint8_t bJumperDetected = 0;
   if( !Param_ReadValue_1Bit(enPARAM1__DISA_DoorJumperCheck) )
   {
      uint8_t bBypassJumperOnLockDetection = ( Param_ReadValue_1Bit(enPARAM1__Fire__IgnoreLocksJumpedOnPhase2) )
                                          && ( GetEmergencyBit( EmergencyBF_FirePhaseII_Active ) )
                                          && ( GetOperation_ClassOfOp() == CLASSOP__AUTO );
      if( aenDoorType[enDoorIndex] == DOOR_TYPE__AUTOMATIC )
      {
         if( ( !bBypassJumperOnLockDetection )
          && ( GetLocks( enDoorIndex ) ) )
         {
            if( Param_ReadValue_1Bit(enPARAM1__DoorLocksJumpedOnDOL) )
            {
               bJumperDetected = !GetDOL(enDoorIndex);
            }
            else
            {
               bJumperDetected = !GetGSW(enDoorIndex);
            }
         }
      }
      else if( ( aenDoorType[enDoorIndex] == DOOR_TYPE__FREIGHT )
            || ( aenDoorType[enDoorIndex] == DOOR_TYPE__SWING )
            || ( aenDoorType[enDoorIndex] == DOOR_TYPE__MANUAL ) )
      {
         uint8_t bLocksExpectedClosed = Doors_CheckIfSwingLocksExpectedClosed(enDoorIndex);
         if( ( !bBypassJumperOnLockDetection )
          && ( !bLocksExpectedClosed )
          && ( GetLocks( enDoorIndex ) ) )
         {
            bJumperDetected = 1;
         }
      }
   }

   if(bJumperDetected)
   {
      uint16_t ucTimeout_100ms = Param_ReadValue_8Bit(enPARAM8__DoorJumperTimeout_100ms);
      ucTimeout_100ms += DEFAULT_JUMPER_ON_LOCKS_TIMEOUT_100MS;
      if( ucTimeout_100ms > 254 )
      {
         ucTimeout_100ms = 254;
      }

      if(aucDebounce_100ms[enDoorIndex] < ucTimeout_100ms)
      {
         aucDebounce_100ms[enDoorIndex]++;
      }
      else
      {
         SetDoorFault(enDoorIndex, DOOR_FAULT__JUMPER_ON_LOCK);
         SetTimersForDoorFault(enDoorIndex, 0, 30);
      }
   }
   else
   {
      aucDebounce_100ms[enDoorIndex] = 0;
   }
}

/*-----------------------------------------------------------------------------
Puts car OOS if exceeds hourly fault limit
 -----------------------------------------------------------------------------*/
#define ONE_HOUR_TO_100MS      (36000U)
static void CheckForDoorHourlyLimit( enum en_doors enDoorIndex )
{
   static uint32_t auiHourCounter_100ms[NUM_OF_DOORS];
   static uint16_t auwHourlyFaultCount[NUM_OF_DOORS];
   uint16_t uwHourlyFaultLimit = Param_ReadValue_8Bit( enPARAM8__DoorHourlyFaultLimit );
   if( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
    && ( GetOperation_AutoMode() != MODE_A__TEST )
    && ( !Param_ReadValue_1Bit(enPARAM1__OOS_Disable) )
    && ( uwHourlyFaultLimit ) )
   {
      /* Check Hourly Limit */
      if( ( GetLastDoorFault(enDoorIndex) == DOOR_FAULT__NONE )
       && ( GetDoorFault(enDoorIndex) != DOOR_FAULT__NONE )
       && ( GetDoorFault(enDoorIndex) != DOOR_FAULT__FAILED_TO_OPEN ) )
      {
         if( auwHourlyFaultCount[enDoorIndex] < uwHourlyFaultLimit)
         {
            auwHourlyFaultCount[enDoorIndex]++;
         }
      }

      /* If an hour has passed, clear fault counts */
      if(auiHourCounter_100ms[enDoorIndex] >= ONE_HOUR_TO_100MS)
      {
         auiHourCounter_100ms[enDoorIndex] = 0;
         auwHourlyFaultCount[enDoorIndex] = 0;

         if( GetOOSSubFault() == enOOS_DoorHourlyLimit )
         {
            UnsetOOSFault(0);
         }
      }
      else
      {
         auiHourCounter_100ms[enDoorIndex]++;
      }

      if( auwHourlyFaultCount[enDoorIndex] >= uwHourlyFaultLimit )
      {
         SetOOSFault(enOOS_DoorHourlyLimit);
      }
   }
   else
   {
      auiHourCounter_100ms[enDoorIndex] = 0;
      auwHourlyFaultCount[enDoorIndex] = 0;
   }
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void Handler_JumperOnGSW( enum en_doors enDoorIndex )
{
   if( Param_ReadValue_1Bit(enPARAM1__DISA_DoorJumperCheck) )
   {
      SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
   }
   else if( GetFP_DoorType(enDoorIndex) == DOOR_TYPE__MANUAL )
   {
      if( !GetGSW(enDoorIndex) && !GetDPM(enDoorIndex) )
      {
         SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
         // Clear CC
         AbortCallsAtCurrentLanding();
      }
   }
   else
   {
      /* This fault will latch until GSW is open */
      if( !GetGSW(enDoorIndex) )
      {
         SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
         SetDoorCommand(enDoorIndex, DOOR_COMMAND__NONE);

         // Clear CC
         AbortCallsAtCurrentLanding();
      }
      else
      {
         SetDoorCommand(enDoorIndex, DOOR_COMMAND__OPEN_UI_REQUEST);
      }
   }

}
static void Handler_JumperOnLock( enum en_doors enDoorIndex )
{
   uint8_t bBypassJumperOnLockDetection = ( Param_ReadValue_1Bit(enPARAM1__Fire__IgnoreLocksJumpedOnPhase2) )
                                       && ( GetEmergencyBit( EmergencyBF_FirePhaseII_Active ) )
                                       && ( GetOperation_ClassOfOp() == CLASSOP__AUTO );
   if( Param_ReadValue_1Bit(enPARAM1__DISA_DoorJumperCheck) )
   {
      SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
   }
   /* This fault will latch until locks are open, there is no
    * timeout because of the concern of an undetected open
    * hall door */
   else if( ( !GetLocks(enDoorIndex) )
         || ( bBypassJumperOnLockDetection ) )
   {
      SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
      SetDoorCommand(enDoorIndex, DOOR_COMMAND__NONE);

      // Clear CC
      AbortCallsAtCurrentLanding();
   }
   else
   {
      SetDoorCommand(enDoorIndex, DOOR_COMMAND__OPEN_UI_REQUEST);
   }
}
static void Handler_LocksOpen( enum en_doors enDoorIndex )
{
   if( auwFaultTimer_100ms[enDoorIndex] < DEFAULT_DOOR_FAULT_TIMEOUT_100MS )
   {
      auwFaultTimer_100ms[enDoorIndex]++;

      /* Keep doors in closing state until timeout or a lock goes high */
      if( ( GetLocks(enDoorIndex) )
       || ( GetDoorOpenButton(enDoorIndex) ) )
      {
         SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
         SetDoorCommand(enDoorIndex, DOOR_COMMAND__NONE);
      }
      else
      {
         SetDoorCommand(enDoorIndex, DOOR_COMMAND__CLOSE);
      }
   }
   else
   {
      /* If fault was not resolved by the timeout period... */
      // Clear fault
      SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
      SetDoorCommand(enDoorIndex, DOOR_COMMAND__NONE);
      // Command door open
      SetDoorCommand(enDoorIndex, DOOR_COMMAND__OPEN_UI_REQUEST);
   }
}
static void Handler_GSWOpen( enum en_doors enDoorIndex )
{
   if( auwFaultTimer_100ms[enDoorIndex] < DEFAULT_DOOR_FAULT_TIMEOUT_100MS )
   {
      auwFaultTimer_100ms[enDoorIndex]++;

      /* Keep doors in closing state until timeout or a GSW goes high */
      if( ( GetGSW(enDoorIndex) )
       || ( GetDoorOpenButton(enDoorIndex) ) )
      {
         SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
         SetDoorCommand(enDoorIndex, DOOR_COMMAND__NONE);
      }
      else
      {
         SetDoorCommand(enDoorIndex, DOOR_COMMAND__CLOSE);
      }
   }
   else
   {
      /* If fault was not resolved by the timeout period... */
      // Clear fault
      SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
      SetDoorCommand(enDoorIndex, DOOR_COMMAND__NONE);
      // Command door close
      SetDoorCommand(enDoorIndex, DOOR_COMMAND__OPEN_UI_REQUEST);
   }
}
static void Handler_FailedToOpen( enum en_doors enDoorIndex )
{
   if(GetFP_DoorType(enDoorIndex) != DOOR_TYPE__MANUAL)
   {
      if( auwFaultTimer_100ms[enDoorIndex] < DEFAULT_DOOR_FAULT_TIMEOUT_100MS )
      {
         auwFaultTimer_100ms[enDoorIndex]++;
   
         /* Keep doors in opening state until timeout or fully open */
         if( ( IsDoorFullyOpen(enDoorIndex) )
          || ( GetDoorCloseButton(enDoorIndex) ) )
         {
            SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
            SetDoorCommand(enDoorIndex, DOOR_COMMAND__NONE);
   
            // Clear CC
            AbortCallsAtCurrentLanding();
         }
         else
         {
            SetDoorCommand(enDoorIndex, DOOR_COMMAND__OPEN_UI_REQUEST);
         }
      }
      else
      {
         /* If fault was not resolved by the timeout period... */
         // Clear fault
         SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
         SetDoorCommand(enDoorIndex, DOOR_COMMAND__NONE);
         // Command door close
         SetDoorCommand(enDoorIndex, DOOR_COMMAND__CLOSE);
         // Clear CC
         AbortCallsAtCurrentLanding();
      }
   }
   else
   {
      // This should not occur
      SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
   }
}
static void Handler_FailedToClose( enum en_doors enDoorIndex )
{
   if(GetFP_DoorType(enDoorIndex) != DOOR_TYPE__MANUAL)
   {
      if( auwFaultTimer_100ms[enDoorIndex] < DEFAULT_DOOR_FAULT_TIMEOUT_100MS )
      {
         auwFaultTimer_100ms[enDoorIndex]++;
   
         /* Keep doors in closing state until timeout or fully closed */
         if( ( IsDoorFullyClosed(enDoorIndex) )
          || ( GetDoorOpenButton(enDoorIndex) ) )
         {
            SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
            SetDoorCommand(enDoorIndex, DOOR_COMMAND__NONE);
         }
         else
         {
            /* If nudging is enabled... */
            if( Param_ReadValue_8Bit(enPARAM8__DoorNudgeTime_1s) )
            {
               SetDoorCommand(enDoorIndex, DOOR_COMMAND__NUDGE);
            }
            else
            {
               SetDoorCommand(enDoorIndex, DOOR_COMMAND__CLOSE);
            }
         }
      }
      else
      {
         /* If fault was not resolved by the timeout period... */
         // Clear fault
         SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
         SetDoorCommand(enDoorIndex, DOOR_COMMAND__NONE);
         // Command door close
         SetDoorCommand(enDoorIndex, DOOR_COMMAND__OPEN_UI_REQUEST);
      }
   }
   else
   {
      // This should not occur
      SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
   }
}
static void Handler_LostDoorSignal( enum en_doors enDoorIndex )
{
   /* Until doors are fully closed...? */
   if( auwFaultTimer_100ms[enDoorIndex] < DEFAULT_DOOR_FAULT_TIMEOUT_100MS )
   {
      auwFaultTimer_100ms[enDoorIndex]++;

      /* Keep doors in closing state until timeout or fully closed */
      if( aenDoorType[enDoorIndex] == DOOR_TYPE__MANUAL )
      {
         if ( IsDoorFullyClosed(enDoorIndex) )
         {
            SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
            SetDoorCommand(enDoorIndex, DOOR_COMMAND__NONE);
         }
      }
      else if( ( IsDoorFullyClosed(enDoorIndex) )
            || ( GetDoorOpenButton(enDoorIndex) ) )
      {
         SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
         SetDoorCommand(enDoorIndex, DOOR_COMMAND__NONE);
      }
      else
      {
         SetDoorCommand(enDoorIndex, DOOR_COMMAND__CLOSE);
      }
   }
   else
   {
      /* If fault was not resolved by the timeout period... */
      // Clear fault
      SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
      SetDoorCommand(enDoorIndex, DOOR_COMMAND__NONE);
   }
}

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void DoorFaultHandler( enum en_doors enDoorIndex )
{
   if( !GetPreflightFlag() )
   {
      CheckForJumperOnGSWFault(enDoorIndex);
      CheckForJumperOnLocksFault(enDoorIndex);

      CheckForDoorHourlyLimit(enDoorIndex);

      if( aeLastFaultIndex[enDoorIndex] != aeFaultIndex[enDoorIndex] )
      {
         aeLastFaultIndex[enDoorIndex] = aeFaultIndex[enDoorIndex];
         auwFaultTimer_100ms[enDoorIndex] = 0;
         auwDelayTimer_100ms[enDoorIndex] = 0;
      }

      if( aeFaultIndex[enDoorIndex] )
      {
         if( aenDoorType[enDoorIndex] == DOOR_TYPE__MANUAL )
         {
            //clear faults on inspection
            if( ( GetOperation_ClassOfOp() == CLASSOP__MANUAL ) &&
                ( !GetMotion_RunFlag() ) )
            {
               SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
            }
            else
            {
               switch( aeFaultIndex[enDoorIndex] )
               {
                  case DOOR_FAULT__JUMPER_ON_GSW:     Handler_JumperOnGSW(enDoorIndex);      break;

                  case DOOR_FAULT__JUMPER_ON_LOCK:    Handler_JumperOnLock(enDoorIndex);     break;

                  case DOOR_FAULT__LOCKS_OPEN:        Handler_LocksOpen(enDoorIndex);        break;

                  case DOOR_FAULT__GSW_OPEN:          Handler_GSWOpen(enDoorIndex);          break;

                  case DOOR_FAULT__FAILED_TO_OPEN:    Handler_FailedToOpen(enDoorIndex);     break;

                  case DOOR_FAULT__FAILED_TO_CLOSE:   Handler_FailedToClose(enDoorIndex);    break;

                  case DOOR_FAULT__LOST_DOOR_SIGNAL:  Handler_LostDoorSignal(enDoorIndex);   break;

                  default:
                     aeFaultIndex[enDoorIndex] = DOOR_FAULT__NONE;
                     break;
               }
            }
         }
         else
         {
            //clear faults on inspection
            if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
            {
               SetDoorFault(enDoorIndex, DOOR_FAULT__NONE);
            }
            /* Hold the door idle (DOOR__PARTIALLY_OPEN) and fault for 3 sec */
            else if( auwDelayTimer_100ms[enDoorIndex] < DOOR_FAULT_DELAY_100MS )
            {
               auwDelayTimer_100ms[enDoorIndex]++;
               SetDoorState(enDoorIndex, DOOR__PARTIALLY_OPEN);
            }
            switch( aeFaultIndex[enDoorIndex] )
            {
               case DOOR_FAULT__JUMPER_ON_GSW:     Handler_JumperOnGSW(enDoorIndex);      break;

               case DOOR_FAULT__JUMPER_ON_LOCK:    Handler_JumperOnLock(enDoorIndex);     break;

               case DOOR_FAULT__LOCKS_OPEN:        Handler_LocksOpen(enDoorIndex);        break;

               case DOOR_FAULT__GSW_OPEN:          Handler_GSWOpen(enDoorIndex);          break;

               case DOOR_FAULT__FAILED_TO_OPEN:    Handler_FailedToOpen(enDoorIndex);     break;

               case DOOR_FAULT__FAILED_TO_CLOSE:   Handler_FailedToClose(enDoorIndex);    break;

               case DOOR_FAULT__LOST_DOOR_SIGNAL:  Handler_LostDoorSignal(enDoorIndex);   break;

               default:
                  aeFaultIndex[enDoorIndex] = DOOR_FAULT__NONE;
                  break;
            }
         }

         /* Set system-wide fault */
         if( aeFaultIndex[enDoorIndex] )
         {
            if( enDoorIndex == DOOR_FRONT )
            {
               en_faults eFault = FLT__DOOR_F_JUMPER_GSW+aeFaultIndex[enDoorIndex]-DOOR_FAULT__JUMPER_ON_GSW;
               eFault = (eFault > FLT__DOOR_F_LOST_SIGNAL) ? FLT__DOOR_F_LOST_SIGNAL:eFault;
               SetFault(eFault);
            }
            else
            {
               en_faults eFault = FLT__DOOR_R_JUMPER_GSW+aeFaultIndex[enDoorIndex]-DOOR_FAULT__JUMPER_ON_GSW;
               eFault = (eFault > FLT__DOOR_R_LOST_SIGNAL) ? FLT__DOOR_R_LOST_SIGNAL:eFault;
               SetFault(eFault);
            }
         }
      }
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
