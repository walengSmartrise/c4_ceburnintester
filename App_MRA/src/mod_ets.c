/******************************************************************************
 *
 * @brief    
 * @version  V1.00
 * @date     2, May 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "drive.h"
#include "mod.h"
#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "acceptance.h"
#include "acceptanceTest.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_ETS =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
typedef struct
{
   uint16_t auwSpeed[NUM_ETS_TRIP_POINTS];
   uint32_t auiPos[NUM_ETS_TRIP_POINTS];
} ETS_Profile;
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static ETS_Profile astETS_Profiles[NUM_MOTION_PROFILES];
static uint8_t bAcceptanceBypassETS;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   For acceptance test ONLY
 -----------------------------------------------------------------------------*/
void SetETS_AcceptanceBypassFlag( uint8_t bBypass )
{
    bAcceptanceBypassETS = bBypass;


}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetETS_Speed( uint16_t uwSpeed, uint8_t ucIndex, Motion_Profile eProfile )
{
   if( ucIndex < NUM_ETS_TRIP_POINTS )
   {
      astETS_Profiles[ eProfile ].auwSpeed[ ucIndex ] = uwSpeed;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetETS_Position( uint32_t uiPos, uint8_t ucIndex, Motion_Profile eProfile )
{
   if( ucIndex < NUM_ETS_TRIP_POINTS )
   {
      astETS_Profiles[ eProfile ].auiPos[ ucIndex ] = uiPos;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
inline uint16_t GetETS_Speed( uint8_t ucIndex, Motion_Profile eProfile )
{
   uint16_t uwReturn = 0;
   if( ucIndex < NUM_ETS_TRIP_POINTS )
   {
      uwReturn = astETS_Profiles[ eProfile ].auwSpeed[ ucIndex ];
   }
   return uwReturn;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
inline uint32_t GetETS_Position( uint8_t ucIndex, Motion_Profile eProfile )
{
   uint32_t uiReturn = 0;
   if( ucIndex < NUM_ETS_TRIP_POINTS )
   {
      uiReturn = astETS_Profiles[ eProfile ].auiPos[ ucIndex ];
   }
   return uiReturn;
}
/*-----------------------------------------------------------------------------
   Checks that ETS points are increasing, and that speeds are nonzero
 -----------------------------------------------------------------------------*/
static void CheckFor_InvalidETSFault()
{
   /* If not on learn, and bottom floor has been learned */
   if( ( GetOperation_ClassOfOp() != CLASSOP__SEMI_AUTO )
    && ( Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0) ) )
   {
      for( Motion_Profile eProfile = MOTION_PROFILE__1; eProfile < NUM_MOTION_PROFILES; eProfile++ )
      {
         /* Ignore the Inspection profile, and the short profile if its not enabled. */
         if( ( eProfile != MOTION_PROFILE__2 ) &&
             ( ( eProfile != MOTION_PROFILE__4 ) || ( Param_ReadValue_8Bit(enPARAM8__ShortProfileMinDist_ft) ) ) )
         {
            uint32_t uiTripSpeed = GetETS_Speed( 0, eProfile );
            uint32_t uiTripPos = GetETS_Position( 0, eProfile );
            for( uint8_t i = 1; i < NUM_ETS_TRIP_POINTS; i++ )
            {
               if( !GetETS_Speed( i, eProfile ) )
               {
                  SetFault(FLT__INVALID_ETS_1+eProfile);
                  break;
               }
               else if( uiTripSpeed > GetETS_Speed( i, eProfile ) )
               {
                  SetFault(FLT__INVALID_ETS_1+eProfile);
                  break;
               }
               else if( uiTripPos > GetETS_Position( i, eProfile ) )
               {
                  SetFault(FLT__INVALID_ETS_1+eProfile);
                  break;
               }
               else if( uiTripSpeed > Param_ReadValue_16Bit(enPARAM16__ContractSpeed) )
               {
                  SetFault(FLT__INVALID_ETS_1+eProfile);
                  break;
               }
               uiTripSpeed = GetETS_Speed( i, eProfile );
               uiTripPos = GetETS_Position( i, eProfile );
            }
         }
      }
   }
}

/*-----------------------------------------------------------------------------

   Checks car speed/position against precalculated ETS points.

   If the car exceeds any of these points by more than 10% for
   a number of cycles determined by enPARAM8__ETSOverspeedDebounceLimit,
   then the car will perform an emergency stop.

   One of three sets of ETS points will be selected depending
   on the active profile, viewed via GetMotion_Profile(),

 -----------------------------------------------------------------------------*/
static void CheckFor_ETSOverspeedFault( void )
{
    uint8_t bClearCounter = 1;
    static uint8_t ucCounter;

    if( (!Param_ReadValue_1Bit( enPARAM1__BypassTermLimits ) && ( GetOperation_ManualMode() != MODE_M__CONSTRUCTION ) ) || (OnETS_AcceptanceTest()) )
    {
       Motion_Profile eProfile = GetMotion_Profile();
       int16_t wCarSpeed;

       uint8_t ucOverspeedDebounceLimit = Param_ReadValue_8Bit(enPARAM8__ETSOverspeedDebounceLimit);
       ucOverspeedDebounceLimit = ( ucOverspeedDebounceLimit >= MAX_DEBOUNCE_LIMIT_OVERSPEED_ETS )
             ? (MAX_DEBOUNCE_LIMIT_OVERSPEED_ETS):(ucOverspeedDebounceLimit);
       wCarSpeed = GetPosition_Velocity();
       if ( wCarSpeed > MIN_ETS_TRIP_SPEED )
       {
          for( uint8_t i = 0; i < NUM_ETS_TRIP_POINTS; i++ )
          {
             uint32_t uiTripPos = Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 + GetFP_NumFloors() - 1 )
                                - GetETS_Position( i, eProfile );
             if( GetPosition_PositionCount() > uiTripPos )
             {
                int16_t wSpeedLimit = GetETS_Speed( i, eProfile ) * ETS_VELOCITY_SCALE_FACTOR;
                if( GetDebug_ActiveAcceptanceTest() == ACCEPTANCE_TEST_ETS )
                {
                	wSpeedLimit = GetETS_Speed( i, eProfile ) * ETS_VELOCITY_SCALE_FACTOR_TEST;
                	ucOverspeedDebounceLimit = 1;
                }
                if( wCarSpeed > wSpeedLimit )
                {
                   bClearCounter = 0;
                   if( ucCounter >= ucOverspeedDebounceLimit )
                   {
                      SetFault(FLT__OVERSPEED_UETS_1+i);
                   }
                   else
                   {
                      ucCounter++;
                   }
                   break;
                }
             }
          }
       }
       else if( wCarSpeed < ( -1 * MIN_ETS_TRIP_SPEED ) )
       {
          for( uint8_t i = 0; i < NUM_ETS_TRIP_POINTS; i++ )
          {
             uint32_t uiTripPos = Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 )
                                + GetETS_Position( i, eProfile );
             if( GetPosition_PositionCount() < uiTripPos )
             {
                int16_t wSpeedLimit = GetETS_Speed( i, eProfile ) * ETS_VELOCITY_SCALE_FACTOR * -1;
                if( GetDebug_ActiveAcceptanceTest() == ACCEPTANCE_TEST_ETS )
                {
                	wSpeedLimit = GetETS_Speed( i, eProfile ) * ETS_VELOCITY_SCALE_FACTOR_TEST * -1;
                	ucOverspeedDebounceLimit = 1;
                }
                if( wCarSpeed < wSpeedLimit )
                {
                   bClearCounter = 0;
                   if( ucCounter >= ucOverspeedDebounceLimit )
                   {
                      SetFault(FLT__OVERSPEED_DETS_1+i);
                   }
                   else
                   {
                      ucCounter++;
                   }
                   break;
                }
             }
          }
       }
    }

    if( bClearCounter )
    {
       ucCounter = 0;
    }
}
/*-----------------------------------------------------------------------------

 Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
    pstThisModule->uwInitialDelay_1ms = 1200;
    pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_ETS_1MS;

    return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
    // If in acceptance mode and ETS is bypassed then do not check ETS
    // Otherwise always check ETS.
   if( ( GetOperation_AutoMode() == MODE_A__TEST ) && (bAcceptanceBypassETS == 1 ) )
   {
       // ETS is bypassed.
       uint8_t a;
       a = 100;
   }
   else
   {
      /* Validate ETS points */
      CheckFor_InvalidETSFault();

      /* Check for ETS trip */
      CheckFor_ETSOverspeedFault();
   }

    return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
