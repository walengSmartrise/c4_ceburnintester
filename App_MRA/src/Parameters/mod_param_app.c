/******************************************************************************
 *
 * @file     mod_heartbeat.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru.h"
#include "sru_a.h"
#include <stdlib.h>
#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_ParamApp =
{
   .pfnInit = Init,
   .pfnRun = Run,
};


/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void VerifyCriticalParams( void )
{
   if( !GetMotion_RunFlag() )
   {
      if ( GetFP_NumFloors() < 2
        || GetFP_NumFloors() > MAX_NUM_FLOORS
         )
      {
         SetFault(FLT__INVALID_NUM_FLOORS);
      }
      else if( ( ( GetFP_NumFloors() + Param_ReadValue_8Bit(enPARAM8__GroupLandingOffset) ) > MAX_NUM_FLOORS )
            || ( Param_ReadValue_8Bit(enPARAM8__GroupLandingOffset) >= NUM_BITS_PER_BITMAP32 ) ) /* Maximum group offset as determined by the landing adjustments performed in cardata.c */
      {
         SetFault(FLT__INVALID_LAND_OFF);
      }
   }
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = 500;

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   if ( !GetFP_FlashParamsReady() )
   {
      // SDA REVIEW: This function below is ill formed. Should be a static function of param app. No other modules are allowed to use it.
      if ( Param_ParametersAreValid() )
      {
         SetFP_ContractSpeed(Param_ReadValue_16Bit( enPARAM16__ContractSpeed ));

         uint8_t ucFP_NumFloors = Param_ReadValue_8Bit( enPARAM8__NumFloors );

         uint8_t bFP_RearDoors = Param_ReadValue_1Bit( enPARAM1__NumDoors );

         uint8_t ucFP_DoorType_F = Param_ReadValue_8Bit( enPARAM8__DoorTypeSelect_F );
         uint8_t ucFP_DoorType_R = Param_ReadValue_8Bit( enPARAM8__DoorTypeSelect_R );

         uint8_t bFP_FreightDoors = Param_ReadValue_1Bit( enPARAM1__Enable_Freight_Doors );

         if ( ucFP_NumFloors < MIN_NUM_FLOORS )
         {
            ucFP_NumFloors = MIN_NUM_FLOORS;
         }
         else if(ucFP_NumFloors >= MAX_NUM_FLOORS )
         {
            ucFP_NumFloors = MAX_NUM_FLOORS;
         }

         gpastFloors = calloc( ucFP_NumFloors, sizeof( *gpastFloors ) );
         gpastCarCalls_F = calloc( ucFP_NumFloors, sizeof( *gpastCarCalls_F ) );
         gpastHallCalls_F = calloc( ucFP_NumFloors, sizeof( *gpastHallCalls_F ) );

         if ( bFP_RearDoors )
         {
            gpastCarCalls_R  = calloc( ucFP_NumFloors, sizeof( *gpastCarCalls_F ) );
            gpastHallCalls_R = calloc( ucFP_NumFloors, sizeof( *gpastHallCalls_R ) );
         }
         Param_InitRingBuffers();
         SetFP_FlashParamsReady();
         SetFP_RearDoors(bFP_RearDoors);
         SetFP_NumFloors(ucFP_NumFloors);
         SetFP_DoorType(ucFP_DoorType_F, DOOR_FRONT);
         SetFP_DoorType(ucFP_DoorType_R, DOOR_REAR);

         SetFP_FreightDoors(bFP_FreightDoors);
      }
   }
   else
   {
      VerifyCriticalParams();

      if( ParamBuff_GetFault() && !GetMotion_RunFlag() )
      {
         SetFault(FLT__PARAM_QUEUE_MRA);
      }
   }
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
