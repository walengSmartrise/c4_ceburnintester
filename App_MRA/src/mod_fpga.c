/******************************************************************************
 *
 * @file     mod_heartbeat.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include <chip.h>
#include <stdint.h>
#include "sys.h"
#include "mod_motion.h"
#include "acceptance.h"
#include "fpga_api.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_FPGA =
{
        .pfnInit = Init,
        .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
#define PREFLIGHT_TIMEOUT_DELAY_1MS    (4000)
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static FPGA_Packet fpgaData;
static uint16_t uwOfflineCounter_ms;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void CheckFor_PreflightFault(void)
{
   static uint32_t uiPreflightTimeout_ms;
   uint8_t bPreflightFlag = GetPreflightFlag();
   PreflightStatus eStatus = FPGA_GetPreflightStatus(FPGA_LOC__MRA);
   if(GetPreflightFlag())
   {
      if( Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V2) || Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) )
      {
         /* Preflight failure now monitored by CPLD fault */
         if( eStatus == PREFLIGHT_STATUS__INACTIVE )
         {
            if( uiPreflightTimeout_ms >= PREFLIGHT_TIMEOUT_DELAY_1MS )
            {
               SetFault(FLT__PREFLIGHT_MR);
            }
            else
            {
               uiPreflightTimeout_ms += MOD_RUN_PERIOD_FPGA_1MS;
            }
         }
         else
         {
            uiPreflightTimeout_ms = 0;
         }
      }
      else
      {
         if( eStatus == PREFLIGHT_STATUS__FAIL )
         {
            SetFault(FLT__PREFLIGHT_MR);
         }
         else if( eStatus == PREFLIGHT_STATUS__INACTIVE )
         {
            if( uiPreflightTimeout_ms >= PREFLIGHT_TIMEOUT_DELAY_1MS )
            {
               SetFault(FLT__PREFLIGHT_MR);
            }
            else
            {
               uiPreflightTimeout_ms += MOD_RUN_PERIOD_FPGA_1MS;
            }
         }
         else
         {
            uiPreflightTimeout_ms = 0;
         }
      }
   }
}
/*----------------------------------------------------------------------------
   Checks for FPGA Preflight state = complete
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
/*
 * Sets up function for this module.
 * This module has a initial delay of 10ms and a run time delay of 5ms.
 * The function is responsible for setting up the GPIO pins for "bit bang spi"
 */
static uint32_t Init( struct st_module *pstThisModule )
{
    pstThisModule->uwInitialDelay_1ms = 10;
    pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_FPGA_1MS;

    if( Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) )
    {
       FPGA_Init_V3();
    }
    else
    {
       FPGA_Init_V1_V2();
    }
    return 0;
}

/**************************************************************************************************
                             MR Safety Inputs
 *************************************************************************************************/
#define MCU_V1_SFP_CONTROL             ( !SRU_Read_RawExtInput(eEXT_Input_CSFP) )
#define MCU_V1_RGP_CONTROL             ( !SRU_Read_RawExtInput(eEXT_Input_CRGP) )
#define MCU_V1_RGB_CONTROL             ( SRU_Read_RawExtInput(eEXT_Input_CRGB) )
#define MCU_V1_SFP_STATUS              ( !SRU_Read_RawExtInput(eEXT_Input_MSFP) )
#define MCU_V1_RGP_STATUS              ( !SRU_Read_RawExtInput(eEXT_Input_MRGP) )
#define MCU_V1_RGB_STATUS              ( SRU_Read_RawExtInput(eEXT_Input_MRGBP) )

#define MCU_V1_SFM_STATUS              ( SRU_Read_RawExtInput(eEXT_Input_MSFM  ) )
#define MCU_V1_RGM_STATUS              ( SRU_Read_RawExtInput(eEXT_Input_MRGM  ) )
#define MCU_V1_SFM_CONTROL_STATUS      ( SRU_Read_RawExtInput(eExt_Output_CSFM) )
#define MCU_V1_RGM_CONTROL_STATUS      ( SRU_Read_RawExtInput(eExt_Output_CRGM) )
#define MCU_V1_INSPECTION_LANDING      ( SRU_Read_Input(enSRU_Input_502     ) )
#define MCU_V1_INSPECTION_MACHINE_ROOM ( SRU_Read_RawExtInput(eEXT_Input_MRTR  ) )

#define MCU_V1_INSPECTION_PIT          ( SRU_Read_Input(enSRU_Input_501   ))
#define MCU_V1_NONBYPASS_PIT           ( SRU_Read_RawExtInput(eEXT_Input_PIT ))
#define MCU_V1_NONBYPASS_HOISTWAY      ( SRU_Read_RawExtInput(eEXT_Input_ZHIN))
#define MCU_V1_NONBYPASS_MACHINE_ROOM  ( SRU_Read_RawExtInput(eEXT_Input_ZMIN))
#define MCU_V1_MECHANIC_MODE           ( SRU_Read_ExtInput(eEXT_Input_MM  ))

#define MCU_V1_BYPASS_HALL_DOOR        ( SRU_Read_RawExtInput(eEXT_Input_BYPH))
#define MCU_V1_BYPASS_CAR_DOOR         ( SRU_Read_RawExtInput(eEXT_Input_BYPC))
#define MCU_V1_ACCESS_BOTTOM_DOWN      ( SRU_Read_RawExtInput(eEXT_Input_ABD ))
#define MCU_V1_ACCESS_BOTTOM_UP        ( SRU_Read_RawExtInput(eEXT_Input_ABU ))
#define MCU_V1_ACCESS_TOP_DOWN         ( SRU_Read_RawExtInput(eEXT_Input_ATD ))
#define MCU_V1_ACCESS_TOP_UP           ( SRU_Read_RawExtInput(eEXT_Input_ATU ))

#define MCU_V1_LOCK_FRONT_TOP          ( SRU_Read_RawExtInput(eEXT_Input_LTF))
#define MCU_V1_LOCK_FRONT_MIDDLE       ( SRU_Read_RawExtInput(eEXT_Input_LMF))
#define MCU_V1_LOCK_FRONT_BOTTOM       ( SRU_Read_RawExtInput(eEXT_Input_LBF))
#define MCU_V1_LOCK_REAR_TOP           ( SRU_Read_RawExtInput(eEXT_Input_LTR))
#define MCU_V1_LOCK_REAR_MIDDLE        ( SRU_Read_RawExtInput(eEXT_Input_LMR))

#define MCU_V1_LOCK_REAR_BOTTOM        ( SRU_Read_RawExtInput(eEXT_Input_LBR))
//---------------------------
#define MCU_V2_SFP_CONTROL             ( SRU_Read_RawExtInput(eEXT_Input_CSFP) )
#define MCU_V2_RGP_CONTROL             ( SRU_Read_RawExtInput(eEXT_Input_CRGP) )
#define MCU_V2_RGB_CONTROL             ( SRU_Read_RawExtInput(eEXT_Input_CRGB) )
#define MCU_V2_SFP_STATUS              ( !SRU_Read_RawExtInput(eEXT_Input_MSFP) )
#define MCU_V2_RGP_STATUS              ( SRU_Read_ExtInput(eEXT_Input_MRGP) )
#define MCU_V2_RGB_STATUS              ( SRU_Read_ExtInput(eEXT_Input_MRGBP) )

#define MCU_V2_SFM_STATUS              ( SRU_Read_ExtInput(eEXT_Input_MSFM  ) )
#define MCU_V2_RGM_STATUS              ( SRU_Read_ExtInput(eEXT_Input_MRGM  ) )
#define MCU_V2_SFM_CONTROL_STATUS      ( SRU_Read_ExtOutput(eExt_Output_CSFM) )
#define MCU_V2_RGM_CONTROL_STATUS      ( SRU_Read_ExtOutput(eExt_Output_CRGM) )
#define MCU_V2_INSPECTION_LANDING      ( SRU_Read_Input(enSRU_Input_502     ) )
#define MCU_V2_INSPECTION_MACHINE_ROOM ( SRU_Read_ExtInput(eEXT_Input_MRTR  ) )

#define MCU_V2_INSPECTION_PIT          ( SRU_Read_Input(enSRU_Input_501   ))
#define MCU_V2_NONBYPASS_PIT           ( SRU_Read_RawExtInput(eEXT_Input_PIT ))
#define MCU_V2_NONBYPASS_HOISTWAY      ( SRU_Read_RawExtInput(eEXT_Input_ZHIN))
#define MCU_V2_NONBYPASS_MACHINE_ROOM  ( SRU_Read_RawExtInput(eEXT_Input_ZMIN))
#define MCU_V2_MECHANIC_MODE           ( SRU_Read_ExtInput(eEXT_Input_MM  ))

#define MCU_V2_BYPASS_HALL_DOOR        ( SRU_Read_RawExtInput(eEXT_Input_BYPH))
#define MCU_V2_BYPASS_CAR_DOOR         ( SRU_Read_RawExtInput(eEXT_Input_BYPC))
#define MCU_V2_ACCESS_BOTTOM_DOWN      ( SRU_Read_RawExtInput(eEXT_Input_ABD ))
#define MCU_V2_ACCESS_BOTTOM_UP        ( SRU_Read_RawExtInput(eEXT_Input_ABU ))
#define MCU_V2_ACCESS_TOP_DOWN         ( SRU_Read_RawExtInput(eEXT_Input_ATD ))
#define MCU_V2_ACCESS_TOP_UP           ( SRU_Read_RawExtInput(eEXT_Input_ATU ))

#define MCU_V2_LOCK_FRONT_TOP          ( SRU_Read_RawExtInput(eEXT_Input_LTF))
#define MCU_V2_LOCK_FRONT_MIDDLE       ( SRU_Read_RawExtInput(eEXT_Input_LMF))
#define MCU_V2_LOCK_FRONT_BOTTOM       ( SRU_Read_RawExtInput(eEXT_Input_LBF))
#define MCU_V2_LOCK_REAR_TOP           ( SRU_Read_RawExtInput(eEXT_Input_LTR))
#define MCU_V2_LOCK_REAR_MIDDLE        ( SRU_Read_RawExtInput(eEXT_Input_LMR))

#define MCU_V2_LOCK_REAR_BOTTOM        ( SRU_Read_RawExtInput(eEXT_Input_LBR))


// Bit index for these variables
typedef enum
{
    REDUNDANCY_LOCK_REAR_BOTTOM,
    REDUNDANCY_LOCK_REAR_MIDDLE,
    REDUNDANCY_LOCK_REAR_TOP,
    REDUNDANCY_LOCK_FRONT_BOTTOM,
    REDUNDANCY_LOCK_FRONT_MIDDLE,
    REDUNDANCY_LOCK_FRONT_TOP,
    REDUNDANCY_ACCESS_TOP_UP,
    REDUNDANCY_ACCESS_TOP_DOWN,
    REDUNDANCY_ACCESS_BOTTOM_UP,
    REDUNDANCY_ACCESS_BOTTOM_DOWN,
    REDUNDANCY_BYPASS_CAR_DOOR,
    REDUNDANCY_BYPASS_HALL_DOOR,
    REDUNDANCY_MECHANIC_MODE,
    REDUNDANCY_NONBYPASS_MACHINE_ROOM,
    REDUNDANCY_NONBYPASS_HOISTWAY,
    REDUNDANCY_NONBYPASS_PIT,
    REDUNDANCY_INSPECTION_PIT,
    REDUNDANCY_INSPECTION_MACHINE_ROOM,
    REDUNDANCY_INSPECTION_LANDING,
    REDUNDANCY_RGM_CONTROL_STATUS, // Unused
    REDUNDANCY_SFM_CONTROL_STATUS, // Unused
    REDUNDANCY_RGM_STATUS,
    REDUNDANCY_SFM_STATUS,
    REDUNDANCY_RGB_STATUS,
    REDUNDANCY_RGP_STATUS,
    REDUNDANCY_SFP_STATUS,
    REDUNDANCY_RGB_CONTROL,
    REDUNDANCY_RGP_CONTROL,
    REDUNDANCY_SFP_CONTROL,

    NUM_REDUNDANT_INPUTS_MR // limit to 32
} RedundantInputs_MR;

static uint8_t CheckRedundancyMR( uint32_t fpga, uint32_t mcu )
{
    uint8_t error = 0;

    for(RedundantInputs_MR i = 0; i < NUM_REDUNDANT_INPUTS_MR; i++)
    {
        uint8_t fpgaBit = (fpga>>i) & 0x1;
        uint8_t mcuBit  = (mcu >>i) & 0x1;
        /* Add exceptions for IPTR */
        if( ( i == REDUNDANCY_INSPECTION_PIT ) && ( !SRU_Read_DIP_Switch( enSRU_DIP_B4 ) ) )//Pit
        {
           //Do nothing
        }
        else if( ( i == REDUNDANCY_INSPECTION_LANDING ) && ( !SRU_Read_DIP_Switch( enSRU_DIP_B3 ) ) )//Landing
        {
           //Do nothing
        }
        else if( ( i == REDUNDANCY_RGM_CONTROL_STATUS ) || ( i == REDUNDANCY_SFM_CONTROL_STATUS ) )
        {
            // Unused
        }
        else if( fpgaBit != mcuBit )
        {
            error = i+1;
            break;
        }
    }

    return error;
}

uint32_t GetMCUData( void )
{
    uint32_t result = 0;
    if( Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V2) || Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) )
    {
       result |= MCU_V2_SFP_CONTROL             << REDUNDANCY_SFP_CONTROL            ;
       result |= MCU_V2_RGP_CONTROL             << REDUNDANCY_RGP_CONTROL            ;
       result |= MCU_V2_RGB_CONTROL             << REDUNDANCY_RGB_CONTROL            ;
       result |= MCU_V2_SFP_STATUS              << REDUNDANCY_SFP_STATUS             ;
       result |= MCU_V2_RGP_STATUS              << REDUNDANCY_RGP_STATUS             ;
       result |= MCU_V2_RGB_STATUS              << REDUNDANCY_RGB_STATUS             ;

       result |= MCU_V2_SFM_STATUS              << REDUNDANCY_SFM_STATUS             ;
       result |= MCU_V2_RGM_STATUS              << REDUNDANCY_RGM_STATUS             ;
       result |= MCU_V2_SFM_CONTROL_STATUS      << REDUNDANCY_SFM_CONTROL_STATUS     ;
       result |= MCU_V2_RGM_CONTROL_STATUS      << REDUNDANCY_RGM_CONTROL_STATUS     ;
       result |= MCU_V2_INSPECTION_LANDING      << REDUNDANCY_INSPECTION_LANDING     ;
       result |= MCU_V2_INSPECTION_MACHINE_ROOM << REDUNDANCY_INSPECTION_MACHINE_ROOM;

       result |= MCU_V2_INSPECTION_PIT          << REDUNDANCY_INSPECTION_PIT         ;
       result |= MCU_V2_NONBYPASS_PIT           << REDUNDANCY_NONBYPASS_PIT          ;
       result |= MCU_V2_NONBYPASS_HOISTWAY      << REDUNDANCY_NONBYPASS_HOISTWAY     ;
       result |= MCU_V2_NONBYPASS_MACHINE_ROOM  << REDUNDANCY_NONBYPASS_MACHINE_ROOM ;
       result |= MCU_V2_MECHANIC_MODE           << REDUNDANCY_MECHANIC_MODE          ;

       result |= MCU_V2_BYPASS_HALL_DOOR        << REDUNDANCY_BYPASS_HALL_DOOR       ;
       result |= MCU_V2_BYPASS_CAR_DOOR         << REDUNDANCY_BYPASS_CAR_DOOR        ;
       result |= MCU_V2_ACCESS_BOTTOM_DOWN      << REDUNDANCY_ACCESS_BOTTOM_DOWN     ;
       result |= MCU_V2_ACCESS_BOTTOM_UP        << REDUNDANCY_ACCESS_BOTTOM_UP       ;
       result |= MCU_V2_ACCESS_TOP_DOWN         << REDUNDANCY_ACCESS_TOP_DOWN        ;
       result |= MCU_V2_ACCESS_TOP_UP           << REDUNDANCY_ACCESS_TOP_UP          ;

       result |= MCU_V2_LOCK_FRONT_TOP          << REDUNDANCY_LOCK_FRONT_TOP         ;
       result |= MCU_V2_LOCK_FRONT_MIDDLE       << REDUNDANCY_LOCK_FRONT_MIDDLE      ;
       result |= MCU_V2_LOCK_FRONT_BOTTOM       << REDUNDANCY_LOCK_FRONT_BOTTOM      ;
       result |= MCU_V2_LOCK_REAR_TOP           << REDUNDANCY_LOCK_REAR_TOP          ;
       result |= MCU_V2_LOCK_REAR_MIDDLE        << REDUNDANCY_LOCK_REAR_MIDDLE       ;

       result |= MCU_V2_LOCK_REAR_BOTTOM        << REDUNDANCY_LOCK_REAR_BOTTOM       ;
    }
    else
    {
       result |= MCU_V1_SFP_CONTROL             << REDUNDANCY_SFP_CONTROL            ;
       result |= MCU_V1_RGP_CONTROL             << REDUNDANCY_RGP_CONTROL            ;
       result |= MCU_V1_RGB_CONTROL             << REDUNDANCY_RGB_CONTROL            ;
       result |= MCU_V1_SFP_STATUS              << REDUNDANCY_SFP_STATUS             ;
       result |= MCU_V1_RGP_STATUS              << REDUNDANCY_RGP_STATUS             ;
       result |= MCU_V1_RGB_STATUS              << REDUNDANCY_RGB_STATUS             ;

       result |= MCU_V1_SFM_STATUS              << REDUNDANCY_SFM_STATUS             ;
       result |= MCU_V1_RGM_STATUS              << REDUNDANCY_RGM_STATUS             ;
       result |= MCU_V1_SFM_CONTROL_STATUS      << REDUNDANCY_SFM_CONTROL_STATUS     ;
       result |= MCU_V1_RGM_CONTROL_STATUS      << REDUNDANCY_RGM_CONTROL_STATUS     ;
       result |= MCU_V1_INSPECTION_LANDING      << REDUNDANCY_INSPECTION_LANDING     ;
       result |= MCU_V1_INSPECTION_MACHINE_ROOM << REDUNDANCY_INSPECTION_MACHINE_ROOM;

       result |= MCU_V1_INSPECTION_PIT          << REDUNDANCY_INSPECTION_PIT         ;
       result |= MCU_V1_NONBYPASS_PIT           << REDUNDANCY_NONBYPASS_PIT          ;
       result |= MCU_V1_NONBYPASS_HOISTWAY      << REDUNDANCY_NONBYPASS_HOISTWAY     ;
       result |= MCU_V1_NONBYPASS_MACHINE_ROOM  << REDUNDANCY_NONBYPASS_MACHINE_ROOM ;
       result |= MCU_V1_MECHANIC_MODE           << REDUNDANCY_MECHANIC_MODE          ;

       result |= MCU_V1_BYPASS_HALL_DOOR        << REDUNDANCY_BYPASS_HALL_DOOR       ;
       result |= MCU_V1_BYPASS_CAR_DOOR         << REDUNDANCY_BYPASS_CAR_DOOR        ;
       result |= MCU_V1_ACCESS_BOTTOM_DOWN      << REDUNDANCY_ACCESS_BOTTOM_DOWN     ;
       result |= MCU_V1_ACCESS_BOTTOM_UP        << REDUNDANCY_ACCESS_BOTTOM_UP       ;
       result |= MCU_V1_ACCESS_TOP_DOWN         << REDUNDANCY_ACCESS_TOP_DOWN        ;
       result |= MCU_V1_ACCESS_TOP_UP           << REDUNDANCY_ACCESS_TOP_UP          ;

       result |= MCU_V1_LOCK_FRONT_TOP          << REDUNDANCY_LOCK_FRONT_TOP         ;
       result |= MCU_V1_LOCK_FRONT_MIDDLE       << REDUNDANCY_LOCK_FRONT_MIDDLE      ;
       result |= MCU_V1_LOCK_FRONT_BOTTOM       << REDUNDANCY_LOCK_FRONT_BOTTOM      ;
       result |= MCU_V1_LOCK_REAR_TOP           << REDUNDANCY_LOCK_REAR_TOP          ;
       result |= MCU_V1_LOCK_REAR_MIDDLE        << REDUNDANCY_LOCK_REAR_MIDDLE       ;

       result |= MCU_V1_LOCK_REAR_BOTTOM        << REDUNDANCY_LOCK_REAR_BOTTOM       ;
    }
    // Do not have rear doors
    if(!GetFP_RearDoors())
    {
        result = result & 0xFFFFFFF8;
    }
    return result;
}

static uint32_t ConvertRawSafetySignalsToBitMap()
{
    uint32_t result = 0;
    if( Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V2) || Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) )
    {
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__RELAY_C_SFP) ) << REDUNDANCY_SFP_CONTROL;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__RELAY_C_EB1) ) << REDUNDANCY_RGP_CONTROL;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__RELAY_C_EB3) ) << REDUNDANCY_RGB_CONTROL;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__RELAY_M_SFP) ) << REDUNDANCY_SFP_STATUS ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__RELAY_M_EB1) ) << REDUNDANCY_RGP_STATUS ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__RELAY_M_EB3) ) << REDUNDANCY_RGB_STATUS ;

       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__RELAY_M_SFM) ) << REDUNDANCY_SFM_STATUS             ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__RELAY_M_EB2) ) << REDUNDANCY_RGM_STATUS             ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__RELAY_C_SFM) ) << REDUNDANCY_SFM_CONTROL_STATUS     ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__RELAY_C_EB2) ) << REDUNDANCY_RGM_CONTROL_STATUS     ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__INSP_LAND) ) << REDUNDANCY_INSPECTION_LANDING     ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__INSP_MR) ) << REDUNDANCY_INSPECTION_MACHINE_ROOM;

       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__INSP_PIT) ) << REDUNDANCY_INSPECTION_PIT        ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__PIT) ) << REDUNDANCY_NONBYPASS_PIT         ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__SFH) ) << REDUNDANCY_NONBYPASS_HOISTWAY    ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__SFM) ) << REDUNDANCY_NONBYPASS_MACHINE_ROOM;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__MM) ) << REDUNDANCY_MECHANIC_MODE         ;

       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__BYP_H) ) << REDUNDANCY_BYPASS_HALL_DOOR;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__BYP_C) ) << REDUNDANCY_BYPASS_CAR_DOOR ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__ABD) ) << REDUNDANCY_ACCESS_BOTTOM_DOWN;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__ABU) ) << REDUNDANCY_ACCESS_BOTTOM_UP  ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__ATD) ) << REDUNDANCY_ACCESS_TOP_DOWN   ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__ATU) ) << REDUNDANCY_ACCESS_TOP_UP     ;

       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__LFT) ) << REDUNDANCY_LOCK_FRONT_TOP   ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__LFM) ) << REDUNDANCY_LOCK_FRONT_MIDDLE;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__LFB) ) << REDUNDANCY_LOCK_FRONT_BOTTOM;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__LRT) ) << REDUNDANCY_LOCK_REAR_TOP    ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__LRM) ) << REDUNDANCY_LOCK_REAR_MIDDLE ;

       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V2__LRB) ) << REDUNDANCY_LOCK_REAR_BOTTOM;
    }
    else
    {
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__RELAY_C_SFP) ) << REDUNDANCY_SFP_CONTROL;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__RELAY_C_EB1) ) << REDUNDANCY_RGP_CONTROL;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__RELAY_C_EB3) ) << REDUNDANCY_RGB_CONTROL;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__RELAY_M_SFP) ) << REDUNDANCY_SFP_STATUS ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__RELAY_M_EB1) ) << REDUNDANCY_RGP_STATUS ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__RELAY_M_EB3) ) << REDUNDANCY_RGB_STATUS ;

       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__RELAY_M_SFM) ) << REDUNDANCY_SFM_STATUS             ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__RELAY_M_EB2) ) << REDUNDANCY_RGM_STATUS             ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__RELAY_C_SFM) ) << REDUNDANCY_SFM_CONTROL_STATUS     ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__RELAY_C_EB2) ) << REDUNDANCY_RGM_CONTROL_STATUS     ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__INSP_LAND) ) << REDUNDANCY_INSPECTION_LANDING     ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__INSP_MR) ) << REDUNDANCY_INSPECTION_MACHINE_ROOM;

       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__INSP_PIT) ) << REDUNDANCY_INSPECTION_PIT        ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__PIT) ) << REDUNDANCY_NONBYPASS_PIT         ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__SFH) ) << REDUNDANCY_NONBYPASS_HOISTWAY    ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__SFM) ) << REDUNDANCY_NONBYPASS_MACHINE_ROOM;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__MM) ) << REDUNDANCY_MECHANIC_MODE         ;

       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__BYP_H) ) << REDUNDANCY_BYPASS_HALL_DOOR;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__BYP_C) ) << REDUNDANCY_BYPASS_CAR_DOOR ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__ABD) ) << REDUNDANCY_ACCESS_BOTTOM_DOWN;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__ABU) ) << REDUNDANCY_ACCESS_BOTTOM_UP  ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__ATD) ) << REDUNDANCY_ACCESS_TOP_DOWN   ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__ATU) ) << REDUNDANCY_ACCESS_TOP_UP     ;

       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__LFT) ) << REDUNDANCY_LOCK_FRONT_TOP   ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__LFM) ) << REDUNDANCY_LOCK_FRONT_MIDDLE;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__LFB) ) << REDUNDANCY_LOCK_FRONT_BOTTOM;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__LRT) ) << REDUNDANCY_LOCK_REAR_TOP    ;
       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__LRM) ) << REDUNDANCY_LOCK_REAR_MIDDLE ;

       result |= ( FPGA_GetDataBit(FPGA_LOC__MRA, FPGA_BITS_MR_V1__LRB) ) << REDUNDANCY_LOCK_REAR_BOTTOM;
    }
    static uint32_t last;
    if(last != result)
    {
       last = result;
    }
    // Do not have rear doors
    if(!GetFP_RearDoors())
    {
        result = result & 0xFFFFFFF8;
    }
    return result;
}

static uint32_t fpgaBitMap = 0;;
static void SetFPGABitMap( uint32_t passed )
{
    fpgaBitMap = passed;
}

static uint32_t GetFPGABitMap( void )
{
    return fpgaBitMap;
}
/*-----------------------------------------------------------------------------

   This module will be used to communicate with the FPGA via SPI or I2C and
   read all of its discrete inputs into gauiFPGA_RedundantInputs[]
   These inputs will be compared against the MCU D's discrete inputs in the
   safety module.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static uint8_t errorCounter_5ms = 0;
   // Assume no error
    uint8_t error = 0;
    uint32_t dataFPGA   = 0;
    uint32_t dataMCU    = 0;

    if( FPGA_UnloadData(FPGA_LOC__MRA) )
    {
        FPGA_GetRecentPacket(&fpgaData, FPGA_LOC__MRA);
        SetFPGABitMap(ConvertRawSafetySignalsToBitMap());
        uwOfflineCounter_ms = 0;
    }

    if( FPGA_GetBufferOverflowFlag() )
    {
       FPGA_ClrBufferOverflowFlag();
       if( Param_ReadValue_1Bit(enPARAM1__DISA_CPLD_OVF_ALARM) )
       {
          SetAlarm(ALM__CPLD_OVF_MR);
       }
    }

     CheckFor_PreflightFault();
     PreflightStatus eStatus = FPGA_GetPreflightStatus(FPGA_LOC__MRA);
     if( eStatus != PREFLIGHT_STATUS__ACTIVE )
     {
         dataFPGA = ( GetActiveAcceptanceTest() == ACCEPTANCE_TEST_REDUNDANCY_CPLD ) ? 0:GetFPGABitMap();
         dataMCU = ( GetActiveAcceptanceTest() == ACCEPTANCE_TEST_REDUNDANCY_MCU ) ? 0:GetMCUData();
         error = CheckRedundancyMR(dataFPGA, dataMCU);
         if(error)
         {
             // Want 1 second
             // run period is 5ms
             if(errorCounter_5ms >= FPGA_REDUNDANCY_DEBOUNCE_5MS)
             {
                 errorCounter_5ms = 0;
                 en_faults eFault = FLT__REDUNDANCY_LRB + error - 1;
                 if(eFault > FLT__REDUNDANCY_C_SFP)
                 {
                    eFault = FLT__REDUNDANCY_C_SFP;
                 }
                 SetFault(eFault);
             }
             else
             {
                 errorCounter_5ms++;
             }
         }
         else
         {
             errorCounter_5ms = 0;
         }

         if( Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V2) || Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) )
         {
            FPGA_Fault_MR_V2 eFault = FPGA_GetFault(FPGA_LOC__MRA);
            if( eFault != FPGA_FAULT_MR_V2__NONE )
            {
               en_faults eFaultNum = eFault - FPGA_FAULT_MR_V2__STARTUP_MR + FLT__CPLD_2_MR_STARTUP;
               if(eFaultNum > FLT__CPLD_MR_UNKNOWN)
               {
                  eFaultNum = FLT__CPLD_MR_UNKNOWN;
               }
               SetCPLDFault(eFaultNum);
            }
         }
         else
         {
            FPGA_Fault_MR_V1 eFault = FPGA_GetFault(FPGA_LOC__MRA);
            if( eFault != FPGA_FAULT_MR_V1_None )
            {
               en_faults eFaultNum = FLT__CPLD_STARTUP + eFault - FPGA_FAULT_MR_V1_InStartup;
               if(eFaultNum > FLT__CPLD_PREFLIGHT)
               {
                  eFaultNum = FLT__CPLD_MR_UNKNOWN;
               }
               SetCPLDFault(eFaultNum);
            }
         }
     }
     else
     {
        errorCounter_5ms = 0;
     }

     /* Detect communication loss */
     if( ( GetOperation_ManualMode() != MODE_M__CONSTRUCTION )
      && ( Param_ReadValue_1Bit(enPARAM1__EnableCPLDOffline) ) )
     {
        uint16_t uwLimit_ms = Param_ReadValue_8Bit(enPARAM8__CPLDOfflineTimeout_10ms)*10;
        if( uwLimit_ms < 50 )
        {
           uwLimit_ms = 50;
        }
        if( uwOfflineCounter_ms > uwLimit_ms )
        {
           // Suppress fault until CPLD communication issues worked out
 //          SetFault(FLT__CPLD_OFFLINE_MR);
           SetAlarm(ALM__CPLD_OFFLINE_MR);
        }
        else
        {
           uwOfflineCounter_ms += pstThisModule->uwRunPeriod_1ms;
        }
     }

    return 0;
}


