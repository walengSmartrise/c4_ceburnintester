/******************************************************************************
 *
 * @file     mod_local_inputs.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "sru.h"
#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_LocalInputs =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define INPUT_DEBOUNCE_10MS         (3)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static uint8_t gucSRU_Deployment;
static uint8_t gucLocalInputs_ThisNode;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t GetLocalInputs_ThisNode(void)
{
   return gucLocalInputs_ThisNode;
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   gucSRU_Deployment = GetSRU_Deployment();

   //------------------------
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = 10;

   return 0;
}

/*-----------------------------------------------------------------------------

   This module is used on any MCUA. It simply collects all 16 inputs bitmapped
   in one variable, and forwards it to MRA for input mapping.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static uint8_t aucDebounceTimer_10ms[SRU_NUM_INPUTS];
   for ( en_sru_inputs enInput = enSRU_Input_501; enInput < SRU_NUM_INPUTS; ++enInput )
   {
      uint_fast8_t bValue = SRU_Read_Input( enInput );
      if( bValue != Sys_Bit_Get(&gucLocalInputs_ThisNode, enInput) )
      {
         if( ++aucDebounceTimer_10ms[enInput] >= INPUT_DEBOUNCE_10MS )
         {
            aucDebounceTimer_10ms[enInput] = 0;
            Sys_Bit_Set( &gucLocalInputs_ThisNode, enInput, bValue );
         }
      }
      else
      {
         aucDebounceTimer_10ms[enInput] = 0;
      }
   }

   SetLocalInputs(gucLocalInputs_ThisNode, IO_BOARDS__MRA);

   // Read extended inputs
   uint32_t auiExtInputs_ThisNode[BITMAP32_SIZE(EXT_NUM_INPUTS)] = {0};
   for ( en_ext_inputs eExtInput = 0; eExtInput < EXT_NUM_INPUTS; ++eExtInput )
   {
      uint_fast8_t bValue = SRU_Read_ExtInput( eExtInput );

      Sys_Bit_Set_Array32(&auiExtInputs_ThisNode[0], eExtInput, BITMAP32_SIZE(EXT_NUM_INPUTS), bValue);
   }

   /* Load local extended inputs directly into the input map */
   SetMachineRoomExtInputs( auiExtInputs_ThisNode[0], 0 );
   SetMachineRoomExtInputs( auiExtInputs_ThisNode[1], 1 );

   SetInputValue(enIN_CONST_DN, SRU_Read_ExtInput(eEXT_Input_CDN));
   SetInputValue(enIN_CONST_UP, SRU_Read_ExtInput(eEXT_Input_CUP));
   if( Param_ReadValue_1Bit(enPARAM1__BContactsNC) )
   {
      SetInputValue(enIN_MBC, !SRU_Read_ExtInput(eEXT_Input_MBC));
      SetInputValue(enIN_MBC2, !SRU_Read_ExtInput(eEXT_Input_MBC2));
   }
   else
   {
      SetInputValue(enIN_MBC, SRU_Read_ExtInput(eEXT_Input_MBC));
      SetInputValue(enIN_MBC2, SRU_Read_ExtInput(eEXT_Input_MBC2));
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
