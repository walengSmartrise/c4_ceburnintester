/******************************************************************************
 *
 * @file     gstMod_IdleTime.c
 * @brief    Maintains idle timers used by other auto operation modules
 * @version  V1.00
 * @date     19, March 2020
 *
 * @note     Maintains idle timers used by other auto operation features such as parking
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "remoteCommands.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_IdleTime =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define TIMER_COUNTS_PER_SEC            ( 10 )
#define IDLE_COUNTS_PER_MIN             ( TIMER_COUNTS_PER_SEC * 60 )
#define MAX_IDLE_TIME_100MS             ( IDLE_COUNTS_PER_MIN * 90 )

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint16_t uwParkingTimer_100ms = MAX_IDLE_TIME_100MS;
static uint16_t uwDirectionChangeTimer_100ms = MAX_IDLE_TIME_100MS;
static uint16_t uwFanTimer_100ms;
static uint16_t uwFaultedTimer_100ms;

/* Parking variables */
static uint8_t ucParkingFloor = INVALID_FLOOR;
static en_recall_door_command eRecallDoorCommand; /* This specifies if a car needs to be moved to a floor and parked with doors open */
static en_recall_door_command eLastRecallDoorCommand;
static uint8_t bReadyToPark;

static uint8_t ucLastParkingFloor = INVALID_FLOOR;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
   Idle Time access functions
      This timer tracks when car has any demand
 -----------------------------------------------------------------------------*/
uint16_t IdleTime_GetParkingTime_100MS(void)
{
   return uwParkingTimer_100ms;
}
uint16_t IdleTime_GetParkingTime_Seconds(void)
{
   uint16_t uwSeconds = uwParkingTimer_100ms/TIMER_COUNTS_PER_SEC;
   return uwSeconds;
}
uint16_t IdleTime_GetParkingTime_Minutes(void)
{
   uint16_t uwMinutes = uwParkingTimer_100ms/IDLE_COUNTS_PER_MIN;
   return uwMinutes;
}
/*-----------------------------------------------------------------------------
   Direction change Time access functions
      This timer tracks if the car can perform a direction change
 -----------------------------------------------------------------------------*/
uint16_t IdleTime_GetDirectionChangeTime_100MS(void)
{
   return uwDirectionChangeTimer_100ms;
}
uint16_t IdleTime_GetDirectionChangeTime_Seconds(void)
{
   uint16_t uwSeconds = uwDirectionChangeTimer_100ms/TIMER_COUNTS_PER_SEC;
   return uwSeconds;
}
uint16_t IdleTime_GetDirectionChangeTime_Minutes(void)
{
   uint16_t uwMinutes = uwDirectionChangeTimer_100ms/IDLE_COUNTS_PER_MIN;
   return uwMinutes;
}
/*-----------------------------------------------------------------------------
   Fan and light time access functions
      This timer tracks if the motor room cooling fan, or the fan/lights in car should turn on.
 -----------------------------------------------------------------------------*/
uint16_t IdleTime_GetFanAndLightTime_100MS(void)
{
   return uwFanTimer_100ms;
}
uint16_t IdleTime_GetFanAndLightTime_Seconds(void)
{
   uint16_t uwSeconds = uwFanTimer_100ms/TIMER_COUNTS_PER_SEC;
   return uwSeconds;
}
uint16_t IdleTime_GetFanAndLightTime_Minutes(void)
{
   uint16_t uwMinutes = uwFanTimer_100ms/IDLE_COUNTS_PER_MIN;
   return uwMinutes;
}
/*-----------------------------------------------------------------------------
   Idle Time access functions
      This timer tracks when car has any demand
 -----------------------------------------------------------------------------*/
uint16_t IdleTime_GetFaultedTime_100MS(void)
{
   return uwFaultedTimer_100ms;
}
uint16_t IdleTime_GetFaultedTime_Seconds(void)
{
   uint16_t uwSeconds = uwFaultedTimer_100ms/TIMER_COUNTS_PER_SEC;
   return uwSeconds;
}
uint16_t IdleTime_GetFaultedTime_Minutes(void)
{
   uint16_t uwMinutes = uwFaultedTimer_100ms/IDLE_COUNTS_PER_MIN;
   return uwMinutes;
}
/*-----------------------------------------------------------------------------
   Access function for parking variables
 -----------------------------------------------------------------------------*/
uint8_t IdleTime_GetParkingFloor(void)
{
   return ucParkingFloor;
}
en_recall_door_command IdleTime_GetParkingDoorCommand(void)
{
   return eRecallDoorCommand;
}
uint8_t IdleTime_GetReadyToPark(void)
{
   return bReadyToPark;
}
uint8_t IdleTime_GetParkingFloor_Plus1(void)
{
   uint8_t ucParkingFloor_Plus1 = ( ucParkingFloor >= GetFP_NumFloors() )
                                ? ( 0 )
                                : ( ucParkingFloor+1 );
   return ucParkingFloor_Plus1;
}

/*-----------------------------------------------------------------------------
   Updates a timer marking how long the car has been faulted
 -----------------------------------------------------------------------------*/
static void UpdateFaultedTimer(void)
{
   if( gstFault.bActiveFault )
   {
      if( uwFaultedTimer_100ms < MAX_IDLE_TIME_100MS )
      {
         uwFaultedTimer_100ms++;
      }
   }
   else
   {
      uwFaultedTimer_100ms = 0;
   }
}
/*-----------------------------------------------------------------------------
   Updates a timer marking how long the car has been idle
      This timer is used for determining if car can be parked
 -----------------------------------------------------------------------------*/
static void UpdateParkingTimer(void)
{
   uint8_t ucNextDestination = GetNextDestination_AnyDirection();
   uint8_t bAnyDoorOpen = AnyDoorOpen();
   uint8_t bMotionRunFlag = GetMotion_RunFlag();
   uint8_t bDoorOpenRequested = ( Auto_CheckDoorOpenButtons() )
                             && ( getDoorZone(DOOR_ANY) )
                             && ( !GetMotion_RunFlag() );
   /* A valid destination was found */
   if( ucNextDestination != INVALID_FLOOR )
   {
      uwParkingTimer_100ms = 0;
   }
   /* A door is open but not parking with doors open */
   else if( ( bAnyDoorOpen || bDoorOpenRequested )
         && ( !( IdleTime_GetParkingDoorCommand() || Doors_GetNoDemandDoorsOpen() ) ) )
   {
      uwParkingTimer_100ms = 0;
   }
   /* In motion but not moving to a parking floor */
   else if( ( GetMotion_RunFlag() )
         && ( IdleTime_GetParkingFloor() >= GetFP_NumFloors() ) )
   {
      uwParkingTimer_100ms = 0;
   }
   else if( uwParkingTimer_100ms < MAX_IDLE_TIME_100MS )
   {
      uwParkingTimer_100ms++;
   }
}
/*-----------------------------------------------------------------------------
   Updates a timer marking how long the car has been idle
      This timer is used for determining if the car should begin servicing calls in the opposite direction
 -----------------------------------------------------------------------------*/
static void UpdateDirectionChangeTimer(void)
{
   uint8_t bActive = 0;
   uint8_t ucNextDestination = GetDestination_CurrentDirection();
   uint8_t bAnyDoorOpen = AnyDoorOpen();
   uint8_t bMotionRunFlag = GetMotion_RunFlag();
   /* A valid destination was found */
   if( ucNextDestination != INVALID_FLOOR )
   {
      uwDirectionChangeTimer_100ms = 0;
   }
   /* A door is open but not parking with doors open */
   else if( ( bAnyDoorOpen )
         && ( !( IdleTime_GetParkingDoorCommand() || Doors_GetNoDemandDoorsOpen() ) ) )
   {
      uwDirectionChangeTimer_100ms = 0;
   }
   /* In motion but not moving to a parking floor */
   else if( ( GetMotion_RunFlag() )
         && ( IdleTime_GetParkingFloor() >= GetFP_NumFloors() ) )
   {
      uwDirectionChangeTimer_100ms = 0;
   }
   else if( uwDirectionChangeTimer_100ms < MAX_IDLE_TIME_100MS )
   {
      uwDirectionChangeTimer_100ms++;
   }
}
/*-----------------------------------------------------------------------------
   Updates a timer marking how long the car has been idle
      This timer is used for determining if the in car fan and light should be turned on
      This timer is used for determining if the motor room fan should be turned on
 -----------------------------------------------------------------------------*/
static void UpdateFanTimer(void)
{
   static uint8_t bLastDoorOpen;
   uint8_t bDoorOpen = AnyDoorOpen();
   uint8_t ucNextDestination = GetNextDestination_AnyDirection();
   if( ( GetMotion_RunFlag() )
    || ( ucNextDestination != INVALID_FLOOR )
    || ( bDoorOpen && !bLastDoorOpen ) )
   {
      uwFanTimer_100ms = 0;
   }
   else if( uwFanTimer_100ms < MAX_IDLE_TIME_100MS )
   {
      uwFanTimer_100ms++;
   }
   bLastDoorOpen = bDoorOpen;
}
/*-----------------------------------------------------------------------------
   Updates parking variables that signal...
    - If a car is being dispatched to a floor without a valid hall call or car call at that floor
    - If a car should be held at a floor with doors open
 -----------------------------------------------------------------------------*/
static void UpdateParkingStatus(void)
{
   ucLastParkingFloor = ucParkingFloor;
   eLastRecallDoorCommand = eRecallDoorCommand;

   ucParkingFloor = INVALID_FLOOR;
   eRecallDoorCommand = RECALL_DOOR_COMMAND__OPEN_NEITHER_DOOR;
   bReadyToPark = 0;

   if( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
    && ( gstCurrentModeRules[ GetOperation_AutoMode() ].bParkingEnabled ) ) /* Flag to lockout certain auto modes such as fire where automatic car dispatching should be ignored */
   {
      if( GetLWD_LearnFloorRequest_Plus1() )
      {
         uint8_t ucCurrentLearnFloor = GetLWD_LearnFloorRequest_Plus1()-1;
         if( !( ( !GetMotion_RunFlag() ) && ( GetOperation_CurrentFloor() == ucCurrentLearnFloor ) && ( !gstFault.bActiveFault )
             && ( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R) || ( !GetFloorOpening_Front(GetOperation_CurrentFloor()) && !GetFloorOpening_Rear(GetOperation_CurrentFloor()) ) ) ) )
         {
            ucParkingFloor = ucCurrentLearnFloor;
            eRecallDoorCommand = RECALL_DOOR_COMMAND__OPEN_NEITHER_DOOR;
         }
      }
      else if( !GetLWD_CaptureFlag() )
      {
         if( ( getCaptureMode() == CAPTURE_OFF )
          && ( !GetInputValue(enIN_PARKING_OFF) )
          && ( Param_ReadValue_8Bit(enPARAM8__Parking_Timer_1sec) ) )
         {
            uint8_t bValidDestination = GetNextDestination_AnyDirection() < GetFP_NumFloors();
            if( !bValidDestination )
            {
               if( IdleTime_GetParkingTime_Seconds() > Param_ReadValue_8Bit(enPARAM8__Parking_Timer_1sec) )
               {
                  bReadyToPark = 1; // Flag sent to group parking dispatcher letting it know its available for parking floor assignment
                  /* todo review: currently up/down/lobby peak features need to wait for parking timer to activate. review this as it was a carry over from sang's development */
                  en_dispatch_mode eDispatchMode = GetDispatchMode();
                  if( eDispatchMode == DISPATCH_MODE__LOBBY )
                  {
                     ucParkingFloor = Param_ReadValue_8Bit(enPARAM8__CarToLobbyFloor);
                     eRecallDoorCommand = RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR;
                  }
                  else if ( eDispatchMode == DISPATCH_MODE__DOWN )
                  {
                     ucParkingFloor = GetFP_NumFloors() - 1;
                     eRecallDoorCommand = RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR;
                  }
                  else if( eDispatchMode == DISPATCH_MODE__UP )
                  {
                     ucParkingFloor = 0;
                     eRecallDoorCommand = RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR;
                  }
                  else if( Param_ReadValue_1Bit( enPARAM1__Enable_Dynamic_Parking ) )
                  {
                     ucParkingFloor = GetDynamicParkingFloor_Plus1_MRB()-1;
                     eRecallDoorCommand = ( GetDynamicParkingWithDoorsOpen_MRB() )
                                        ? RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR
                                        : RECALL_DOOR_COMMAND__OPEN_NEITHER_DOOR;
                  }
                  else // Static parking floor
                  {
                     ucParkingFloor = Param_ReadValue_8Bit(enPARAM8__OverrideParkingFloor);
                     eRecallDoorCommand = ( Param_ReadValue_1Bit(enPARAM1__ParkingWithDoorOpen) )
                                        ? RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR
                                        : RECALL_DOOR_COMMAND__OPEN_NEITHER_DOOR;
                  }
               }
            }
         }
         else if( GetCarToLobbyRecallFlag() )
         {
            ucParkingFloor = Param_ReadValue_8Bit(enPARAM8__CarToLobbyFloor);
            eRecallDoorCommand = RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR;
         }
      }
   }
}
/*-----------------------------------------------------------------------------
   After parking with doors open, door dwell time will delay the doors from
   closing to take the new destination. This function checks if parking with doors open
   has just been canceled and attempts to close the doors
 -----------------------------------------------------------------------------*/
static void CheckForParkingWithDoorsOpenDwell(void)
{
   if( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
    && ( gstCurrentModeRules[ GetOperation_AutoMode() ].bParkingEnabled ) ) /* Flag to lockout certain auto modes such as fire where automatic car dispatching should be ignored */
   {
      if( ( ucLastParkingFloor != ucParkingFloor )
       || ( ( eLastRecallDoorCommand != RECALL_DOOR_COMMAND__OPEN_NEITHER_DOOR ) && ( eRecallDoorCommand == RECALL_DOOR_COMMAND__OPEN_NEITHER_DOOR ) ) )
      {
         CloseAnyOpenDoor();
      }
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 3000;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_IDLE_TIME_1MS;

   return 0;
}

/*-----------------------------------------------------------------------------

   In addition to heartbeat, this module checks if the last reset was caused
   by the watchdog. If so, it logs a fault for the first 5 seconds on startup.
   It also receives the watchdog status of MRB and TRC and logs their fautls.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   UpdateParkingTimer();
   UpdateDirectionChangeTimer();
   UpdateFanTimer();
   UpdateFaultedTimer();

   UpdateParkingStatus();
   CheckForParkingWithDoorsOpenDwell();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
