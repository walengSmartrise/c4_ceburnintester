/******************************************************************************
 *
 * @file     mod_heartbeat.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_a.h"
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Heartbeat =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define IDLE_COUNTS_PER_SEC            ( 2 )
#define IDLE_COUNTS_PER_MIN            ( IDLE_COUNTS_PER_SEC * 60 )
#define MAX_IDLE_TIME_500MS            ( IDLE_COUNTS_PER_MIN * 255 )

#define BUZZER_TOGGLE_RATE_500MS       (4)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t bOverloadBuzzer;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Updated buzzer output due to overload signal
   TODO move to a separate module
 -----------------------------------------------------------------------------*/
uint8_t Overload_GetBuzzerFlag(void)
{
   return bOverloadBuzzer;
}
static void Overload_UpdateBuzzerFlag(void)
{
   static uint8_t ucDelay_500ms;
   if( GetOutputValue( enOUT_Overload ) )
   {
      if(++ucDelay_500ms >= BUZZER_TOGGLE_RATE_500MS)
      {
         ucDelay_500ms = 0;
         bOverloadBuzzer = !bOverloadBuzzer;
      }
   }
   else
   {
      ucDelay_500ms = 0;
      bOverloadBuzzer = 0;
   }
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 500;
   pstThisModule->uwRunPeriod_1ms = 500;

   return 0;
}

/*-----------------------------------------------------------------------------

   In addition to heartbeat, this module checks if the last reset was caused
   by the watchdog. If so, it logs a fault for the first 5 seconds on startup.
   It also receives the watchdog status of MRB and TRC and logs their fautls.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static uint8_t bLED_IsOn;

   bLED_IsOn = !bLED_IsOn;

   SRU_Write_LED( enSRU_LED_Heartbeat, bLED_IsOn );

   Overload_UpdateBuzzerFlag();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
