/******************************************************************************
 *
 * @file     mod_run_log.c
 * @brief
 * @version  V1.00
 * @date     11, April 2017
 *
 * @note    This module stores the run data for the most recent run.
 *
 *          Timestamped:
 *             Drive HW Enable
 *             B2 Cont. Pick
 *             M Cont. Pick
 *             E Brake Pick
 *             Command Zero Speed
 *             Brake Pick
 *             Command Nonzero Speed
 *
 *             End of Accel
 *             End of Cruising
 *             Start of Leveling
 *
 *          Interval (100 ms):
 *             Car Speed
 *             Commanded Speed
 *             Position
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "sys.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_RunLog =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
/* Current */
static Run_Log_States eState;
static Transition_Points eTransitionPointIndex;

static int32_t aiTransitionTimestamp_ms[NUM_TPs];
static Run_Log_Item stAccelRunLog[MAX_PATTERN_SIZE_BYTES/(MOD_RUN_PERIOD_RUN_LOG_1MS/MOD_RUN_PERIOD_MOTION_1MS)];
static Run_Log_Item stDecelRunLog[MAX_PATTERN_SIZE_BYTES/(MOD_RUN_PERIOD_RUN_LOG_1MS/MOD_RUN_PERIOD_MOTION_1MS)];
static uint32_t uiAccelRunLogIndex;
static uint32_t uiDecelRunLogIndex;

/* Last Run */
static int32_t aiLastTransitionTimestamp_ms[NUM_TPs];
static Run_Log_Item stLastAccelRunLog[MAX_PATTERN_SIZE_BYTES/(MOD_RUN_PERIOD_RUN_LOG_1MS/MOD_RUN_PERIOD_MOTION_1MS)];
static Run_Log_Item stLastDecelRunLog[MAX_PATTERN_SIZE_BYTES/(MOD_RUN_PERIOD_RUN_LOG_1MS/MOD_RUN_PERIOD_MOTION_1MS)];
static uint32_t uiLastAccelRunLogIndex;
static uint32_t uiLastDecelRunLogIndex;

/*  */
static uint8_t bNewLog; //Signals new run in available to transmit
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t GetRunLog_NewLogFlag()
{
   return bNewLog;
}
/*-----------------------------------------------------------------------------
   Clear new log flag after completing transmission to
   prevent loading of new log while transmitting
   New log will not be loadeduntil flag is cleared
 -----------------------------------------------------------------------------*/
void ClrRunLog_NewLogFlag()
{
   bNewLog = 0;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
Run_Log_States GetRunLog_State()
{
   return eState;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint32_t GetRunLog_AccelLogSize()
{
   return uiAccelRunLogIndex;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint32_t GetRunLog_DecelLogSize()
{
   return uiDecelRunLogIndex;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t GetRunLog_AccelLogItem( Run_Log_Item *pstRunLogItem, uint32_t uiIndex )
{
   uint8_t bSuccess = 0;
   if( uiIndex < uiAccelRunLogIndex )
   {
      bSuccess = 1;
      memcpy( pstRunLogItem, &stAccelRunLog[uiIndex], sizeof( Run_Log_Item ) );
   }
   return bSuccess;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t GetRunLog_DecelLogItem( Run_Log_Item *pstRunLogItem, uint32_t uiIndex )
{
   uint8_t bSuccess = 0;
   if( uiIndex < uiDecelRunLogIndex )
   {
      bSuccess = 1;
      memcpy( pstRunLogItem, &stDecelRunLog[uiIndex], sizeof( Run_Log_Item ) );
   }
   return bSuccess;
}
/*-----------------------------------------------------------------------------
   Returns the timestamp of each transition point of the last run
 -----------------------------------------------------------------------------*/
int32_t GetRunLog_TransitionPointTimestamp( Transition_Points eTransitionPoint )
{
   int32_t iReturn = 0;
   if( eTransitionPoint < NUM_TPs )
   {
      iReturn = aiTransitionTimestamp_ms[eTransitionPoint];
   }
   return iReturn;
}

/*-----------------------------------------------------------------------------
BPS toggle might occur after start sequence so it is checked outside the run log stages
 -----------------------------------------------------------------------------*/
static void CheckForStartTimeBPS()
{
   if( Brake_GetBPS()
    && !aiTransitionTimestamp_ms[TP__Start_BPS] )
   {
      aiTransitionTimestamp_ms[TP__Start_BPS] = Sys_GetTickCount( enTickCount_1ms );
   }

   if( Brake2_GetBPS()
    && !aiTransitionTimestamp_ms[TP__Start_EB_BPS] )
   {
      aiTransitionTimestamp_ms[TP__Start_EB_BPS] = Sys_GetTickCount( enTickCount_1ms );
   }
}
static void CheckForStopTimeBPS()
{
   if( !Brake_GetBPS()
    && !aiTransitionTimestamp_ms[TP__Stop_BPS] )
   {
      aiTransitionTimestamp_ms[TP__Stop_BPS] = Sys_GetTickCount( enTickCount_1ms );
   }

   if( !Brake2_GetBPS()
    && !aiTransitionTimestamp_ms[TP__Stop_EB_BPS] )
   {
      aiTransitionTimestamp_ms[TP__Stop_EB_BPS] = Sys_GetTickCount( enTickCount_1ms );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckForIdleState()
{
   /* If stopped for 30 seconds, reset log state */
   static uint16_t uwDelay;
   if(!GetMotion_RunFlag())
   {
      if(++uwDelay >= 600)
      {
         uwDelay = 0;
         eState = RL_STATE__IDLE;
      }
   }
   else
   {
      uwDelay = 0;
   }
}
/*-----------------------------------------------------------------------------
   Await start of run
   Clear previous log values
 -----------------------------------------------------------------------------*/
static void State_Idle()
{
   if( Motion_GetMotionState() == MOTION__START_SEQUENCE )
   {
      /* Store Previous Run */
      if(!bNewLog)
      {
         bNewLog = 1;
         memcpy( &aiLastTransitionTimestamp_ms[0], &aiTransitionTimestamp_ms[0], sizeof(aiTransitionTimestamp_ms) );
         memcpy( &stLastAccelRunLog[0], &stAccelRunLog[0], sizeof(stAccelRunLog) );
         memcpy( &stLastDecelRunLog[0], &stDecelRunLog[0], sizeof(stDecelRunLog) );
         uiLastAccelRunLogIndex = uiAccelRunLogIndex;
         uiLastDecelRunLogIndex = uiDecelRunLogIndex;
      }
      /* Clear Current Run */
      uiAccelRunLogIndex = 0;
      uiDecelRunLogIndex = 0;
      eTransitionPointIndex = TP__Start_Drive_HW_EN;

      memset( &aiTransitionTimestamp_ms[0], 0, sizeof(aiTransitionTimestamp_ms));
      eState = RL_STATE__START_SEQ;
   }
}
/*-----------------------------------------------------------------------------
   Log timestamps for each stage of the motion start sequence
 -----------------------------------------------------------------------------*/
static void State_StartSequence()
{
   if( GetDriveHWEnableFlag()
    && !aiTransitionTimestamp_ms[TP__Start_Drive_HW_EN] )
   {
      aiTransitionTimestamp_ms[TP__Start_Drive_HW_EN] = Sys_GetTickCount( enTickCount_1ms );
   }

   if( GetPickB2Flag()
    && !aiTransitionTimestamp_ms[TP__Start_PickB2] )
   {
      aiTransitionTimestamp_ms[TP__Start_PickB2] = Sys_GetTickCount( enTickCount_1ms );
   }

   if( GetPickMFlag()
    && !aiTransitionTimestamp_ms[TP__Start_PickM] )
   {
      aiTransitionTimestamp_ms[TP__Start_PickM] = Sys_GetTickCount( enTickCount_1ms );
   }

   if( GetPickEBrakeFlag()
    && !aiTransitionTimestamp_ms[TP__Start_LiftEBrake] )
   {
      aiTransitionTimestamp_ms[TP__Start_LiftEBrake] = Sys_GetTickCount( enTickCount_1ms );
   }

   if( GetPickDriveFlag()
    && !aiTransitionTimestamp_ms[TP__Start_HoldZero] )
   {
      aiTransitionTimestamp_ms[TP__Start_HoldZero] = Sys_GetTickCount( enTickCount_1ms );
   }

   if( GetPickBrakeFlag()
    && !aiTransitionTimestamp_ms[TP__Start_LiftBrake] )
   {
      aiTransitionTimestamp_ms[TP__Start_LiftBrake] = Sys_GetTickCount( enTickCount_1ms );
   }

   /* Keep at end */
   if( ( Motion_GetMotionState() == MOTION__ACCELERATING )
    && !aiTransitionTimestamp_ms[TP__EndOfStart] )
   {
      aiTransitionTimestamp_ms[TP__EndOfStart] = Sys_GetTickCount( enTickCount_1ms );
      eTransitionPointIndex = TP__EndOfAccel;
      eState = RL_STATE__ACCEL;
   }
}
/*-----------------------------------------------------------------------------
   Record the car speed, commanded speed, car position at predefined interval
   Log end of accel timestamp
 -----------------------------------------------------------------------------*/
static void State_Accel()
{
   static uint8_t ucScalingTimer;
   if( Motion_GetMotionState() == MOTION__ACCELERATING )
   {
      // Log car movement
      if( ++ucScalingTimer >= Param_ReadValue_8Bit(enPARAM8__Debug_RunLogScaling) )
      {
         ucScalingTimer = 0;
         if( uiAccelRunLogIndex < ( MAX_PATTERN_SIZE_BYTES/(MOD_RUN_PERIOD_RUN_LOG_1MS/MOD_RUN_PERIOD_MOTION_1MS) ) )
         {
#if LOG_DRIVE_SPEED_FEEDBACK
            stAccelRunLog[uiAccelRunLogIndex].wCmdSpeed = GetDriveSpeedFeedback();
#else
            stAccelRunLog[uiAccelRunLogIndex].wCmdSpeed = GetMotion_SpeedCommand();
#endif
            stAccelRunLog[uiAccelRunLogIndex].wCarSpeed = GetPosition_Velocity();
            stAccelRunLog[uiAccelRunLogIndex].uiCarPosition = GetPosition_PositionCount();
            uiAccelRunLogIndex++;
         }
      }
   }
   else
   {
      // Log end of accel stage and move to cruising
      ucScalingTimer = 0;
      aiTransitionTimestamp_ms[eTransitionPointIndex] = Sys_GetTickCount( enTickCount_1ms );
      eTransitionPointIndex = TP__EndOfCruising;
      eState = RL_STATE__CRUISING;
   }
}
/*-----------------------------------------------------------------------------
   Await decel
   Log end of cruising timestamp
 -----------------------------------------------------------------------------*/
static void State_Cruising()
{
   if( Motion_GetMotionState() != MOTION__CRUISING )
   {
      // Log end of cruising stage and move to decel
      aiTransitionTimestamp_ms[eTransitionPointIndex] = Sys_GetTickCount( enTickCount_1ms );
      eTransitionPointIndex = TP__EndOfDecel;
      eState = RL_STATE__DECEL;
   }
}
/*-----------------------------------------------------------------------------
   Record the car speed, commanded speed, car position at predefined interval
   Log end of decel timestamp
 -----------------------------------------------------------------------------*/
static void State_Decel()
{
   static uint8_t ucScalingTimer;
   if( ( Motion_GetMotionState() == MOTION__DECELERATING )
    && ( !Motion_GetLevelingFlag() ) )
   {
      // Log car movement
      if( ++ucScalingTimer >= Param_ReadValue_8Bit(enPARAM8__Debug_RunLogScaling) )
      {
         ucScalingTimer = 0;
         if(uiDecelRunLogIndex < ( MAX_PATTERN_SIZE_BYTES/(MOD_RUN_PERIOD_RUN_LOG_1MS/MOD_RUN_PERIOD_MOTION_1MS) ) )
         {
#if LOG_DRIVE_SPEED_FEEDBACK
            stDecelRunLog[uiDecelRunLogIndex].wCmdSpeed = GetDriveSpeedFeedback();
#else
            stDecelRunLog[uiDecelRunLogIndex].wCmdSpeed = GetMotion_SpeedCommand();
#endif
             stDecelRunLog[uiDecelRunLogIndex].wCarSpeed = GetPosition_Velocity();
             stDecelRunLog[uiDecelRunLogIndex].uiCarPosition = GetPosition_PositionCount();
             uiDecelRunLogIndex++;
         }
      }
   }
   else
   {
      // Log end of decel stage and move to stop sequence
      ucScalingTimer = 0;
      aiTransitionTimestamp_ms[eTransitionPointIndex] = Sys_GetTickCount( enTickCount_1ms );
      eTransitionPointIndex = TP__EndOfLeveling;
      eState = RL_STATE__LEVELING;
   }
}
/*-----------------------------------------------------------------------------
   Await stop sequence
   Log end of leveling timestamp
 -----------------------------------------------------------------------------*/
static void State_Leveling()
{
   if( Motion_GetMotionState() != MOTION__DECELERATING )
   {
      // Log end of leveling stage and move to stop sequence
      aiTransitionTimestamp_ms[eTransitionPointIndex] = Sys_GetTickCount( enTickCount_1ms );
      eTransitionPointIndex = TP__Stop_HoldZero;
      eState = RL_STATE__STOP_SEQ;
   }
}
/*-----------------------------------------------------------------------------
Log timestamps for each stage of the motion stop sequence
 -----------------------------------------------------------------------------*/
static void State_StopSequence()
{
   if( !GetMotion_SpeedCommand()
    && !aiTransitionTimestamp_ms[TP__Stop_HoldZero] )
   {
      aiTransitionTimestamp_ms[TP__Stop_HoldZero] = Sys_GetTickCount( enTickCount_1ms );
   }

   if( !GetPickBrakeFlag()
    && !aiTransitionTimestamp_ms[TP__Stop_DropBrake] )
   {
      aiTransitionTimestamp_ms[TP__Stop_DropBrake] = Sys_GetTickCount( enTickCount_1ms );
   }

   if( !GetPickDriveFlag()
    && !aiTransitionTimestamp_ms[TP__Stop_DropDrive] )
   {
      aiTransitionTimestamp_ms[TP__Stop_DropDrive] = Sys_GetTickCount( enTickCount_1ms );
   }

   if( !GetPickEBrakeFlag()
    && !aiTransitionTimestamp_ms[TP__Stop_DropEBrake] )
   {
      aiTransitionTimestamp_ms[TP__Stop_DropEBrake] = Sys_GetTickCount( enTickCount_1ms );
   }

   if( !GetPickMFlag()
    && !aiTransitionTimestamp_ms[TP__Stop_DropM] )
   {
      aiTransitionTimestamp_ms[TP__Stop_DropM] = Sys_GetTickCount( enTickCount_1ms );
   }

   if( !GetPickB2Flag()
    && !aiTransitionTimestamp_ms[TP__Stop_DropB2] )
   {
      aiTransitionTimestamp_ms[TP__Stop_DropB2] = Sys_GetTickCount( enTickCount_1ms );
   }

   /* Keep at end */
   if( ( Motion_GetMotionState() == MOTION__STOPPED )
    && !aiTransitionTimestamp_ms[TP__Stop_Preflight] )
   {
      aiTransitionTimestamp_ms[TP__Stop_Preflight] = Sys_GetTickCount( enTickCount_1ms );
      eState = RL_STATE__IDLE;
   }
}


/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 1000;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_RUN_LOG_1MS;

   return 0;
}

/*-----------------------------------------------------------------------------


 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   if( GetOperation_ClassOfOp() == CLASSOP__AUTO )
   {
      CheckForIdleState();
      CheckForStartTimeBPS();
      CheckForStopTimeBPS();

      switch(eState)
      {
         case RL_STATE__IDLE:
            State_Idle();
            break;
         case RL_STATE__START_SEQ:
            State_StartSequence();
            break;
         case RL_STATE__ACCEL:
            State_Accel();
            break;
         case RL_STATE__CRUISING:
            State_Cruising();
            break;
         case RL_STATE__DECEL:
            State_Decel();
            break;
         case RL_STATE__LEVELING:
            State_Leveling();
            break;
         case RL_STATE__STOP_SEQ:
            State_StopSequence();
            break;
         default:
            eState = RL_STATE__IDLE;
            break;
      }
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
