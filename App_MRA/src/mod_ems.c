/******************************************************************************
*
* @file     mod_ems.c
* @brief    Watches ems inputs and state.
* @version  V1.00
* @date     19, March 2016
*
* @note
*
******************************************************************************/

/*----------------------------------------------------------------------------
*
* Place #include files required to compile this source file here.
*
*----------------------------------------------------------------------------*/

#include "mod.h"
#include "GlobalData.h"
#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
/*----------------------------------------------------------------------------
*
* Place function prototypes that will be used only by this file here.
* This section need only include "forward referenced" functions.
*
*----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
*
* Place #define constants and macros that will be used only by this
* source file here.
*
*----------------------------------------------------------------------------*/
#define DEFAULT_PHASE1_EXIT_DELAY_MS    (60000U) // From V2, active if exit delay not set
#define EMS_RESET_DEBOUNCE_MS           (3000U)
/*----------------------------------------------------------------------------
*
* Place typedefs, structs, unions, and enums that will be used only
* by this source file here.
*
*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*
* Place static variables that will be used only by this file here.
*
*----------------------------------------------------------------------------*/

/* Phase 1 */
static en_ems1_states eState_EMS1;
static uint32_t uiExitTimer_EMS1_ms;
/* Phase 2 */
static en_ems2_states eState_EMS2;
static uint32_t uiExitTimer_EMS2_ms;

/* Lamp */
static uint8_t bLamp;

static uint8_t bBuzzer;
static uint8_t bDisableBuzzer;//disable when at recall floor

/* If EMS 1 recall detected before Fire 1 Recall, EMS1 takes priority. Otherwise Fire1 Recall takes priority. */
static uint8_t bEMS1_Before_Fire1;
/* If EMS 2 is activated before fire phase 2 is activated, EMS2 takes priority. Otherwise Fire phase 2 takes priority. */
static uint8_t bEMS2_Before_Fire2;

/*----------------------------------------------------------------------------
*
* Define global variables that will be used by both this source file
* and by other source files here.
*
*----------------------------------------------------------------------------*/
struct st_module gstMod_EMS =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
*
* Place function bodies here.
*
*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   Between EMS 1 and Fire 1, the first detected takes priority.
   Returns 1 if EMS1 was detected first.
*----------------------------------------------------------------------------*/
uint8_t EMS_GetEMS1PrioritizedFlag(void)
{
   uint8_t bPriority = ( EMS_GetEmergencyMedicalService() == EMS_SERVICE__PHASE1 ) && ( bEMS1_Before_Fire1 );
   return bPriority;
}
/*----------------------------------------------------------------------------
   Between EMS 2 and Fire 2, the first detected takes priority.
   Returns 1 if EMS2 was detected first.
*----------------------------------------------------------------------------*/
uint8_t EMS_GetEMS2PrioritizedFlag(void)
{
   uint8_t bPriority = ( EMS_GetEmergencyMedicalService() == EMS_SERVICE__PHASE2 ) && ( bEMS2_Before_Fire2 );
   return bPriority;
}

/*----------------------------------------------------------------------------
   Return 0 if not in EMS
   Return 1 if in phase1
   Return 2 if in phase2
*----------------------------------------------------------------------------*/
en_ems_services EMS_GetEmergencyMedicalService(void)
{
   en_ems_services eService = EMS_SERVICE__INACTIVE;
   if( eState_EMS2 == EMS2__ACTIVE )
   {
      eService = EMS_SERVICE__PHASE2;
   }
   else if( eState_EMS1 == EMS1__ACTIVE )
   {
      eService = EMS_SERVICE__PHASE1;
   }

   return eService;
}
/*----------------------------------------------------------------------------

*----------------------------------------------------------------------------*/
uint8_t EMS_GetLampFlag(void)
{
   return bLamp;
}
/*----------------------------------------------------------------------------

*----------------------------------------------------------------------------*/
uint8_t EMS_GetBuzzerFlag(void)
{
   return bBuzzer;
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void UpdateLampOutput()
{
   if( GetEmergencyBit( EmergencyBF_EMS_Phase1_Active )
    || GetEmergencyBit( EmergencyBF_EMS_Phase2_Active ) )
   {
      bLamp = 1;
   }
   else
   {
      bLamp = 0;
   }
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void UpdateBuzzerOutput()
{
   uint8_t ucCallFloor_Plus1 = GetActiveMedicalCallFloor_MRB();
   if( ucCallFloor_Plus1
    && GetEmergencyBit( EmergencyBF_EMS_Phase1_Active )
    && !bDisableBuzzer )
   {
      bBuzzer = 1;
   }
   else
   {
      bBuzzer = 0;
   }
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void IdleWithDoorsOpen()
{
   ClearLatchedCarCalls();
   uint8_t ucNumOfDoors = GetNumberOfDoors();
   for (enum en_doors enDoorIndex = DOOR_FRONT; enDoorIndex < ucNumOfDoors; enDoorIndex++)
   {
      SetDoorCommand( enDoorIndex, DOOR_COMMAND__OPEN_IN_CAR_REQUEST );
   }
}

/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void ResetEMS2()
{
   eState_EMS2 = EMS2__INACTIVE;
   ClrEmergencyBit( EmergencyBF_EMS_Phase2_Active );
   uiExitTimer_EMS2_ms = 0;
   bEMS2_Before_Fire2 = 0;
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void ResetEMS1()
{
   eState_EMS1 = EMS1__INACTIVE;
   ClrEmergencyBit( EmergencyBF_EMS_Phase1_Active );
   FRAM_SetRecallFloor_Plus1(0);
   uiExitTimer_EMS1_ms = 0;
   bEMS1_Before_Fire1 = 0;
}
/*-----------------------------------------------------------------------------
   Latch first medical keyswitch detected
   Disables all other calls
   If in motion, stop at next landing
   Close doors immedialy if open
   Then recall nonstop to latched floor.
   Open doors at recall floor and await EMS2 Key
   Return to normal operation if >= 30 seconds passed without EMS2 Key
   *Disable in car stop switch at start of run
-----------------------------------------------------------------------------*/
void EMS_Phase_1 ( uint8_t bInitMode )
{
   Auto_SetSpeed(GetFP_ContractSpeed());
   Auto_Set_Limits();

   ClearLatchedCarCalls();
   ClearLatchedHallCalls();

   SetEmergencyBit( EmergencyBF_EMS_Phase1_Active );
   uint8_t ucStoredMedicalFloor_Plus1 = FRAM_GetRecallFloor_Plus1();
   uint8_t ucRequestedMedicalFloor_Plus1 = GetActiveMedicalCallFloor_MRB();

   if( ucStoredMedicalFloor_Plus1 == ucRequestedMedicalFloor_Plus1 )
   {
      uiExitTimer_EMS1_ms = 0;
   }

   uint8_t ucRecallFloor = ( ucStoredMedicalFloor_Plus1 ) ? ucStoredMedicalFloor_Plus1-1:0;
   if( Auto_Recall ( bInitMode, ucRecallFloor, RECALL_DOOR_COMMAND__OPEN_BOTH_DOORS ) )
   {
      bDisableBuzzer = 1;
      uint32_t uiExitDelay_ms = ( Param_ReadValue_8Bit( enPARAM8__EMS1_ExitDelay_1s ) )
                              ? ( Param_ReadValue_8Bit( enPARAM8__EMS1_ExitDelay_1s ) * 1000 )
                              : ( DEFAULT_PHASE1_EXIT_DELAY_MS );
      if( uiExitTimer_EMS1_ms >= uiExitDelay_ms )
      {
         ResetEMS1();
      }
      else
      {
         uiExitTimer_EMS1_ms += MOD_RUN_PERIOD_OPER_AUTO_1MS;
      }
   }
   else
   {
      bDisableBuzzer = 0;
      uiExitTimer_EMS1_ms = 0;
   }
}
/*-----------------------------------------------------------------------------
 * 1. EmergencyBF_EMS_Phase2_Active to 1
 * 2. disable hall calls
 * 3. Disable car calls unless enIN_EMS2_ON
 *
 * IF: enPARAM1__EMS_ExitPh2AtAnyFloor
 *    IF: enIN_UNUSED_EMS2_OFF && !InMotion
 *       THEN: Move to Normal & EmergencyBF_EMS_Phase2_Active to 0
 * ELSE IF: enIN_EMS2_ON & Car call
 *    THEN: Move to call
 * ELSE:
 *    1. Keep Doors open
 *    2. clear calls
-----------------------------------------------------------------------------*/
void EMS_Phase_2 ( uint8_t bInitMode )
{
   static uint8_t bWasRunning;
   gstCurrentModeRules[ MODE_A__EMS2 ].bIgnoreDCB = 1;
   gstCurrentModeRules[ MODE_A__EMS2 ].bIgnoreCarCall_F = 1;
   gstCurrentModeRules[ MODE_A__EMS2 ].bIgnoreCarCall_R = 1;
   Auto_SetSpeed(GetFP_ContractSpeed());
   Auto_Set_Limits();

   SetEmergencyBit( EmergencyBF_EMS_Phase2_Active );

   if( GetInputValue( enIN_EMS2_ON ) )
   {
      gstCurrentModeRules[ MODE_A__EMS2 ].bIgnoreDCB = 0;
      gstCurrentModeRules[ MODE_A__EMS2 ].bIgnoreCarCall_F = 0;
      gstCurrentModeRules[ MODE_A__EMS2 ].bIgnoreCarCall_R = 0;

      uiExitTimer_EMS2_ms = 0;

      /* Close doors if car call pressed */
      if( GetNextDestination_AnyDirection() != INVALID_FLOOR )
      {
         CloseAnyOpenDoor();
      }

      Auto_Oper( bInitMode );

      /* Car takes the first call and cancel the rest */
      if( AnyDoorOpen() )
      {
         if ( bWasRunning && !GetMotion_RunFlag()) {
            bWasRunning = 0;
            ClearLatchedCarCalls();
         }
      }
      else if( GetMotion_RunFlag() )
      {
         bWasRunning = 1;
      }
   }
   else // IDLE with doors open until Key switch is restored
   {
      if( !GetMotion_RunFlag()
        && InsideDeadZone() )
      {
         IdleWithDoorsOpen();
         if( ( Param_ReadValue_1Bit( enPARAM1__EMS_ExitPh2AtAnyFloor ) )
          || ( ( GetOperation_CurrentFloor()+1 ) == FRAM_GetRecallFloor_Plus1() ) )
         {
            if( uiExitTimer_EMS2_ms >= ( Param_ReadValue_8Bit( enPARAM8__EMS2_ExitDelay_1s ) * 1000 ) )
            {
               ResetEMS2();
            }
            else
            {
               uiExitTimer_EMS2_ms += MOD_RUN_PERIOD_OPER_AUTO_1MS;
            }
         }
         else
         {
            SetAlarm(ALM__EMS2_NOT_AT_RECALL);
         }
      }
      else //Allow for releveling/correction
      {
         uiExitTimer_EMS2_ms = 0;
         Auto_Oper( bInitMode );
      }
   }
}
/*-----------------------------------------------------------------------------
   Unload Medical Call & Check for call reassignment
-----------------------------------------------------------------------------*/
void EMS_UnloadMedicalCall(void)
{
   if( FRAM_EmergencyBitmapReady() && FRAM_RecallFloorReady() && Param_ReadValue_8Bit( enPARAM8__HallMedicalMask) )
   {
      uint8_t ucStoredMedicalFloor_Plus1 = FRAM_GetRecallFloor_Plus1();
      uint8_t ucRequestedMedicalFloor_Plus1 = GetActiveMedicalCallFloor_MRB();
      if( ( ucRequestedMedicalFloor_Plus1 )
       && ( ucRequestedMedicalFloor_Plus1 <= GetFP_NumFloors() ) )
      {
         uint8_t ucMedicalFloor = ucRequestedMedicalFloor_Plus1-1;
         if( gpastFloors[ucMedicalFloor].bFrontOpening || gpastFloors[ucMedicalFloor].bRearOpening )
         {
            /* Detect if EMS1 was activated before fire phase 1. */
            if( ( !GetEmergencyBit(EmergencyBF_EMS_Phase1_Active) )
             && ( Fire_GetFireService() != FIRE_SERVICE__PHASE1 )
             && ( Fire_GetFireService() != FIRE_SERVICE__PHASE2 ) )
            {
               bEMS1_Before_Fire1 = 1;
            }

            FRAM_SetRecallFloor_Plus1( ucRequestedMedicalFloor_Plus1 );
            SetEmergencyBit( EmergencyBF_EMS_Phase1_Active );
         }
      }
      /* Clear the saved state if existing ems recall assignment is lost */
      else if( ucStoredMedicalFloor_Plus1 && !ucRequestedMedicalFloor_Plus1 )
      {
         uint8_t ucRecallFloor = ( ucStoredMedicalFloor_Plus1 ) ? ucStoredMedicalFloor_Plus1-1:0;
         if( ( GetOperation_CurrentFloor() != ucRecallFloor ) || ( !InsideDeadZone() ) )
         {
            ResetEMS1();
         }
      }
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

-----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 2000;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_EMS_1MS;

   eState_EMS1 = EMS1__INACTIVE;
   eState_EMS2 = EMS2__INACTIVE;
   uiExitTimer_EMS1_ms = 0;
   uiExitTimer_EMS2_ms = 0;

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static uint8_t bFirst = 1;
   static uint16_t uwDebounceReset_ms;
   /* Check for EMS state after reset */
   if( FRAM_EmergencyBitmapReady() && FRAM_RecallFloorReady() )
   {
      if( bFirst )
      {
         if( SRU_Read_DIP_Switch( enSRU_DIP_A1 ) )
         {
            FRAM_SetRecallFloor_Plus1( 0 );
            ClrEmergencyBit( EmergencyBF_EMS_Phase1_Active );
            ClrEmergencyBit( EmergencyBF_EMS_Phase2_Active );
         }
         bFirst = 0;
      }
      else if( Param_ReadValue_8Bit ( enPARAM8__HallMedicalMask) )
      {
         /* EMS Updating states */
         if( eState_EMS2 != EMS2__ACTIVE )
         {
            if( GetEmergencyBit( EmergencyBF_EMS_Phase2_Active ) )
            {
               eState_EMS2 = EMS2__ACTIVE;

               /* Detect if EMS2 was activated before fire phase 2. */
               if( Fire_GetFireService() != FIRE_SERVICE__PHASE2 )
               {
                  bEMS2_Before_Fire2 = 1;
               }
            }
            else
            {
               if( GetInputValue( enIN_EMS2_ON ) )
               {
                  if( GetEmergencyBit( EmergencyBF_EMS_Phase1_Active ) )
                  {
                     SetEmergencyBit( EmergencyBF_EMS_Phase2_Active );
                     eState_EMS2 = EMS2__ACTIVE;

                     /* Detect if EMS2 was activated before fire phase 2. */
                     if( Fire_GetFireService() != FIRE_SERVICE__PHASE2 )
                     {
                        bEMS2_Before_Fire2 = 1;
                     }
                  }
                  else if( Param_ReadValue_1Bit( enPARAM1__EMS_AllowPh2WithoutPh1 ) )
                  {
                     uint8_t ucCurrentFloor_Plus1 = GetOperation_CurrentFloor()+1;
                     FRAM_SetRecallFloor_Plus1(ucCurrentFloor_Plus1);
                     SetEmergencyBit( EmergencyBF_EMS_Phase2_Active );
                     eState_EMS2 = EMS2__ACTIVE;

                     /* Detect if EMS2 was activated before fire phase 2. */
                     if( Fire_GetFireService() != FIRE_SERVICE__PHASE2 )
                     {
                        bEMS2_Before_Fire2 = 1;
                     }
                  }
               }
            }
         }

         if( ( eState_EMS1 != EMS1__ACTIVE )
          && ( eState_EMS2 != EMS2__ACTIVE ) )
         {
            if( GetEmergencyBit( EmergencyBF_EMS_Phase1_Active ) )
            {
               eState_EMS1 = EMS1__ACTIVE;
            }
         }
         if( ( eState_EMS1 != EMS1__ACTIVE )
          && ( eState_EMS2 != EMS2__ACTIVE ) )
         {
            if( uwDebounceReset_ms >= EMS_RESET_DEBOUNCE_MS )
            {
               uwDebounceReset_ms = 0;
               ResetEMS1();
               ResetEMS2();
            }
            else
            {
               uwDebounceReset_ms += MOD_RUN_PERIOD_EMS_1MS;
            }
         }
         else
         {
            uwDebounceReset_ms = 0;
         }

         /* EMS2 Main On should hold lamp high for a minimum of 60 sec after it goes Off */
         UpdateLampOutput();
         /* Add function for announcement of EMS signal */
         UpdateBuzzerOutput();

      }
      else
      {
         ResetEMS1();
         ResetEMS2();
      }
   }

    return 0;
}

/*----------------------------------------------------------------------------
*
* End of file.
*
*----------------------------------------------------------------------------*/
