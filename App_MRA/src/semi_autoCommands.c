// This header was created by ben. It is a really poor name
// since it really is the semi_auto command functions and
// not only used for acceptance test.
#include"acceptanceTest.h"

static uint8_t bManualMode = 0;

uint8_t GetManualMode( void )
{
    return bManualMode;
}
void MoveUpManual( void )
{
    bManualMode = 1;
    SetOperation_MotionCmd( MOCMD__RUN_UP );
}

void MoveDownManual( void )
{
    bManualMode = 1;
    SetOperation_MotionCmd( MOCMD__RUN_DN );
}

void StopManual( void )
{
    bManualMode = 0;
    SetOperation_MotionCmd( MOCMD__EMERG_STOP );
}

void MoveAutomatic( uint8_t ucDestination )
{
    bManualMode = 0;
    if ( ucDestination < GetFP_NumFloors() )
    {
        if ( ( gpastFloors[ucDestination].ulPosition > GetOperation_PositionLimit_DN() )
                && ( gpastFloors[ucDestination].ulPosition < GetOperation_PositionLimit_UP() ) )
        {

            SetOperation_RequestedDestination( gpastFloors[ucDestination].ulPosition );
            SetOperation_MotionCmd( MOCMD__GOTO_DEST );
        }
    }
}

