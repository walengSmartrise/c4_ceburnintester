#include "mod.h"
#include "sru.h"
#include "sru_a.h"
#include <stdint.h>
#include "acceptance.h"
#include "acceptanceTest.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_module gstMod_AcceptanceTest = { .pfnInit = Init, .pfnRun = Run, };
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
static AcceptanceTest activeAcceptanceTest = NO_ACTIVE_ACCEPTANCE_TEST;
static AcceptanceTestStatus status[NUM_ACCEPTANCE_TEST];
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

AcceptanceTest GetActiveAcceptanceTest( void )
{
    return activeAcceptanceTest;
}

void SetActiveAcceptanceTest( AcceptanceTest passed )
{
    activeAcceptanceTest = passed;
}

AcceptanceTestStatus GetAcceptanceTestStatus(AcceptanceTest passed)
{
    return status[passed];
}

void SetAcceptanceTestStatus(AcceptanceTest test, AcceptanceTestStatus testStatus)
{
    status[test] = testStatus;
}

static uint8_t accpetanceTestState;
void SetActiveAcceptanceTestState(uint8_t state)
{
    accpetanceTestState = state;
}

uint8_t GetActiveAcceptanceTestState( void )
{
    return accpetanceTestState;
}


/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static AcceptanceTest en_prevAcceptanceTest;

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
static void ResetTestState(AcceptanceTest test)
{
   switch(test)
   {
      case ACCEPTANCE_TEST_NTS:
         ResetAcceptanceNTS();
         break;
      case ACCEPTANCE_TEST_ETS:
         ResetAcceptanceETS();
         break;
      case ACCEPTANCE_TEST_ETSL:
         ResetAcceptanceETSL();
         break;
      case ACCEPTANCE_TEST_TOP_LIMIT:
         ResetAcceptanceTopLimit();
         break;
      case ACCEPTANCE_TEST_BOTTOM_LIMIT:
         ResetAcceptanceBottomLimit();
         break;
      case ACCEPTANCE_TEST_REDUNDANCY_CPLD:
         ResetAcceptanceRedundancyCPLD();
         break;
      case ACCEPTANCE_TEST_REDUNDANCY_MCU:
         ResetAcceptanceRedundancyMCU();
         break;
      case ACCEPTANCE_TEST_ASC_OR_DESC_OVERSPEED:
         ResetAcceptanceAscOrDescOverspeed();
         break;
      case ACCEPTANCE_TEST_INSPECTION_SPEED:
         ResetAcceptanceInspectionSpeed();
         break;
      case ACCEPTANCE_TEST_LEVELING_SPEED:
         ResetAcceptanceLevelingSpeed();
         break;
      case ACCEPTANCE_TEST_BOTTOM_FINAL_LIMIT:
         ResetAcceptanceBottomFinalLimit();
         break;
      case ACCEPTANCE_TEST_TOP_FINAL_LIMIT:
         ResetAcceptanceTopFinalLimit();
         break;
      case ACCEPTANCE_TEST_COUNTERWEIGHT_BUFFER:
         ResetAcceptanceCounterweightBuffer();
         break;
      case ACCEPTANCE_TEST_CAR_BUFFER:
         ResetAcceptanceCarBuffer();
         break;
      case ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_FRONT:
         ResetAcceptanceUnintendedMovementFront();
         break;
      case ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_REAR:
         ResetAcceptanceUnintendedMovementRear();
         break;
      case ACCEPTANCE_TEST_BRAKEBOARD_FEEDBACK:
         ResetAcceptanceBrakeBoardFeedback();
         break;
      case ACCEPTANCE_TEST_SLIDE_DISTANCE:
         ResetAcceptanceSlideDistance();
         break;
      case ACCEPTANCE_TEST_EBRAKE_SLIDE:
         ResetAcceptanceEBrkSlide();
         break;
      default:
         break;
   }
}
/*-----------------------------------------------------------------------------

 Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
    pstThisModule->uwInitialDelay_1ms = 200;
    pstThisModule->uwRunPeriod_1ms    = 25;

    SetActiveAcceptanceTest( NO_ACTIVE_ACCEPTANCE_TEST );

    return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
    static uint8_t readyForAcceptancetTest = 0;
    uint8_t state = 0;

    en_prevAcceptanceTest = GetActiveAcceptanceTest();

    // Check if there is a pending acceptance test.
    if(GetUIRequest_AcceptanceTest_MRB() && readyForAcceptancetTest)
    {
        if(GetActiveAcceptanceTest() == NO_ACTIVE_ACCEPTANCE_TEST)
        {
            SetActiveAcceptanceTest( GetUIRequest_AcceptanceTest_MRB() );
        }
        readyForAcceptancetTest = 0;
    }

    // Arm the acceptance test. This happens when there is no acceptance
    // asserted by MCUB. This helps prevent problems with MCUB accidently
    // issuing acceptance test.
    else if(!GetUIRequest_AcceptanceTest_MRB())
    {
        readyForAcceptancetTest = 1;
    }

    if( !OnAcceptanceTest() && (en_prevAcceptanceTest != NO_ACTIVE_ACCEPTANCE_TEST) )
    {
       ResetTestState(en_prevAcceptanceTest);
       Acceptance_ClearAllPickCommands();
       SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
       readyForAcceptancetTest = 1;
    }

    // Check to make sure that dip B8 is not enabled when it should not be
    if(!GetMotion_RunFlag())
    {
       uint8_t bDIP = SRU_Read_DIP_Switch( enSRU_DIP_B8 );
       uint8_t bValidDIPConditions = 0;
       bValidDIPConditions |= ( ( GetOperation_AutoMode() == MODE_A__TEST ) && ( GetActiveAcceptanceTest() == ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_FRONT ) );
       bValidDIPConditions |= ( ( GetOperation_AutoMode() == MODE_A__TEST ) && ( GetActiveAcceptanceTest() == ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_REAR ) );
       bValidDIPConditions |= Param_ReadValue_1Bit(enPARAM1__TestUnintendedMovement);
       if( ( bDIP && !bValidDIPConditions ) || ( !bDIP && bValidDIPConditions ) )
       {
          SetFault(FLT__INVALID_DIP_SW_B8);
       }
    }

    switch ( GetActiveAcceptanceTest() )
    {
        case ACCEPTANCE_TEST_NTS:
            state = AcceptanceNTS();
            break;
        case ACCEPTANCE_TEST_ETS:
            state = AcceptanceETS();
            break;
        case ACCEPTANCE_TEST_ETSL:
           state = AcceptanceETSL();
           break;
        case ACCEPTANCE_TEST_TOP_LIMIT:
            state = AcceptanceTopLimit();
            break;
        case ACCEPTANCE_TEST_BOTTOM_LIMIT:
            state = AcceptanceBottomLimit();
            break;
        case ACCEPTANCE_TEST_REDUNDANCY_CPLD:
            state = AcceptanceRedundancyCPLD();
            break;
        case ACCEPTANCE_TEST_REDUNDANCY_MCU:
            state = AcceptanceRedundancyMCU();
            break;
        case ACCEPTANCE_TEST_ASC_OR_DESC_OVERSPEED:
            state = AcceptanceAscOrDescOverspeed();
            break;
        case ACCEPTANCE_TEST_INSPECTION_SPEED:
            state = AcceptanceInspectionSpeed();
            break;
        case ACCEPTANCE_TEST_LEVELING_SPEED:
            state = AcceptanceLevelingSpeed();
            break;
        case ACCEPTANCE_TEST_BOTTOM_FINAL_LIMIT:
            state = AcceptanceBottomFinalLimit();
            break;
        case ACCEPTANCE_TEST_TOP_FINAL_LIMIT:
            state = AcceptanceTopFinalLimit();
            break;
        case ACCEPTANCE_TEST_COUNTERWEIGHT_BUFFER:
            state = AcceptanceCounterweightBuffer();
            break;
        case ACCEPTANCE_TEST_CAR_BUFFER:
            state = AcceptanceCarBuffer();
            break;
        case ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_FRONT:
            state = AcceptanceUnintendedMovementFront();
            break;
        case ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_REAR:
            state = AcceptanceUnintendedMovementRear();
            break;
        case ACCEPTANCE_TEST_BRAKEBOARD_FEEDBACK:
            state = AcceptanceBrakeBoardFeedback();
            break;
        case ACCEPTANCE_TEST_SLIDE_DISTANCE:
            state = AcceptanceSlideDistance();
            break;
        case ACCEPTANCE_TEST_EBRAKE_SLIDE:
           state = AcceptanceEBrkSlide();
           break;
        default:
           // Nothing to do
           break;
    }

    SetActiveAcceptanceTestState(state);

    Test_UM_UpdateControlSignals(gstMod_AcceptanceTest.uwRunPeriod_1ms);

    return 0;
}


static uint8_t onETS = 0;
uint8_t OnETS_AcceptanceTest( void)
{
    return onETS;
}
void SetETS_AcceptanceTest( uint8_t passed)
{
    onETS = passed;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

