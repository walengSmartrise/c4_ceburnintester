#include "acceptance.h"
#include "acceptanceTest.h"
#include "alarm_app.h"
#include "fault_app.h"
#include "mod.h"

#define CONTACTOR_TIMEOUT 500

enum en_Brake
{
   BRAKE_UNK,
   BRAKE_MAIN,
   BRAKE_SEC
};

static enum en_Brake enBrake;

static uint16_t ucContactorTimeoutCounter;

static acceptance_BrakeBoardFeedback_FSM currentState = ACCEPTANCE_BRAKEBOARD_IDLE;
static acceptance_BrakeBoardFeedback_FSM nextState = ACCEPTANCE_BRAKEBOARD_IDLE;

void ResetAcceptanceBrakeBoardFeedback( void )
{
   SetAcceptanceTestStatus(ACCEPTANCE_TEST_BRAKEBOARD_FEEDBACK, ACCEPTANCE_TEST_NOT_RUN);
   currentState = ACCEPTANCE_BRAKEBOARD_IDLE;
   nextState = currentState;
   enBrake = BRAKE_UNK;
}

uint8_t AcceptanceBrakeBoardFeedback( void )
{
   IncrementTimer();

   switch(currentState)
   {
      case ACCEPTANCE_BRAKEBOARD_IDLE:
         ucContactorTimeoutCounter = 0;
         if( (GetActiveAcceptanceTest() == ACCEPTANCE_TEST_BRAKEBOARD_FEEDBACK) && OnAcceptanceTest())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_BRAKEBOARD_FEEDBACK,ACCEPTANCE_TEST_IN_PROGRESS);
                nextState = ACCEPTANCE_BRAKEBOARD_WAIT_FOR_USER;
            }
            enBrake = BRAKE_UNK;
            break;
        case ACCEPTANCE_BRAKEBOARD_WAIT_FOR_USER:
           if( GetInputValue(enIN_MRUP) )
           {
              if(TimerExpired())
              {
                 nextState = ACCEPTANCE_BRAKEBOARD_PRESS_CONTACTOR;
              }
           }
           else if( !OnAcceptanceTest() )
           {
              nextState = ACCEPTANCE_BRAKEBOARD_IDLE;
           }
           break;
        case ACCEPTANCE_BRAKEBOARD_PRESS_CONTACTOR:
           if( ( GetInputValue(enIN_MBC) )
            || ( GetInputValue(enIN_MBC2) && Param_ReadValue_1Bit(enPARAM1__EnableSecondaryBrake) ) )
           {
              if(TimerExpired() )
              {
                 nextState = ACCEPTANCE_BRAKEBOARD_PICK_BRAKE;
              }
           }
           else if( !OnAcceptanceTest() )
           {
              nextState = ACCEPTANCE_BRAKEBOARD_FAIL;
           }
           else
           {
              if( ucContactorTimeoutCounter++ > CONTACTOR_TIMEOUT )
              {
                 nextState = ACCEPTANCE_BRAKEBOARD_FAIL;
              }
           }
           break;
      case ACCEPTANCE_BRAKEBOARD_PICK_BRAKE:
         if( !OnAcceptanceTest() )
         {
            nextState = ACCEPTANCE_BRAKEBOARD_FAIL;
         }
         else
         {
            uint8_t bPickMain = GetInputValue(enIN_MBC) && ( !GetInputValue(enIN_MBC2) || !Param_ReadValue_1Bit(enPARAM1__EnableSecondaryBrake) );
            uint8_t bPickSec = GetInputValue(enIN_MBC2) && !GetInputValue(enIN_MBC) && Param_ReadValue_1Bit(enPARAM1__EnableSecondaryBrake);

            if(bPickMain)
            {
               enBrake = BRAKE_MAIN;
            }
            else if(bPickSec)
            {
               enBrake = BRAKE_SEC;
            }

            SetAcceptanceBrakePickCommand( BRAKE_SEL__PRIMARY, bPickMain);
            SetAcceptanceBrakePickCommand( BRAKE_SEL__EMERGENCY, bPickSec);
            if(TimerExpired())
            {
               if( enBrake == BRAKE_MAIN )
               {
                  nextState = ACCEPTANCE_BRAKEBOARD_HOLD_MAIN_BRAKE;
               }
               else if( enBrake == BRAKE_SEC )
               {
                  nextState = ACCEPTANCE_BRAKEBOARD_HOLD_SEC_BRAKE;
               }

            }
         }
         break;
      case ACCEPTANCE_BRAKEBOARD_HOLD_MAIN_BRAKE:
         if(TimerExpired())
         {
            uint8_t ucMarginOfError = Param_ReadValue_8Bit(enPARAM8__BrakeHoldVoltage) * 0.15;

            if( gstBrake.ucVoltageFeedback > (Param_ReadValue_8Bit(enPARAM8__BrakeHoldVoltage) + ucMarginOfError) )
            {
               nextState = ACCEPTANCE_BRAKEBOARD_FAIL;
            }
            else if( gstBrake.ucVoltageFeedback < (Param_ReadValue_8Bit(enPARAM8__BrakeHoldVoltage) - ucMarginOfError))
            {
               nextState = ACCEPTANCE_BRAKEBOARD_FAIL;
            }
            else
            {
               nextState = ACCEPTANCE_BRAKEBOARD_PASS;
            }
         }
         break;
      case ACCEPTANCE_BRAKEBOARD_HOLD_SEC_BRAKE:
         if(TimerExpired())
         {
            uint8_t ucMarginOfError = Param_ReadValue_8Bit(enPARAM8__SecBrakeHoldVoltage) * 0.15;

            if( gstBrake2.ucVoltageFeedback > (Param_ReadValue_8Bit(enPARAM8__SecBrakeHoldVoltage) + ucMarginOfError) )
            {
               nextState = ACCEPTANCE_BRAKEBOARD_FAIL;
            }
            else if( gstBrake2.ucVoltageFeedback < (Param_ReadValue_8Bit(enPARAM8__SecBrakeHoldVoltage) - ucMarginOfError))
            {
               nextState = ACCEPTANCE_BRAKEBOARD_FAIL;
            }
            else
            {
               nextState = ACCEPTANCE_BRAKEBOARD_PASS;
            }
         }
         break;
      case ACCEPTANCE_BRAKEBOARD_PASS:
         if(TimerExtExpired())
         {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_BRAKEBOARD_FEEDBACK,ACCEPTANCE_TEST_PASS);
                nextState = ACCEPTANCE_BRAKEBOARD_COMPLETE;
         }
         SetAcceptanceBrakePickCommand( BRAKE_SEL__PRIMARY, 0);
         break;
      case ACCEPTANCE_BRAKEBOARD_FAIL:
         if(TimerExtExpired())
         {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_BRAKEBOARD_FEEDBACK,ACCEPTANCE_TEST_FAIL);
                nextState = ACCEPTANCE_BRAKEBOARD_COMPLETE;
         }
         SetAcceptanceBrakePickCommand( BRAKE_SEL__PRIMARY, 0);
         break;
      case ACCEPTANCE_BRAKEBOARD_COMPLETE:
         if( TimerExpired() )
         {
            SetAcceptanceTestStatus(ACCEPTANCE_TEST_BRAKEBOARD_FEEDBACK, ACCEPTANCE_TEST_NOT_RUN);
            SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
            nextState = ACCEPTANCE_BRAKEBOARD_IDLE;
         }
         break;
      default:
         break;
   }

   if(currentState != nextState)
    {
        ResetTimer();
    }

    currentState = nextState;
    return currentState;
}
