#include "acceptance.h"
#include "acceptanceTest.h"
#include "alarm_app.h"
#include "fault_app.h"
#include "mod.h"

static acceptance_BottomLimit_FSM currentState = ACCEPTANCE_BOTTOM_LIMIT_IDLE;
static acceptance_BottomLimit_FSM nextState = ACCEPTANCE_BOTTOM_LIMIT_IDLE;
static uint8_t paramSetup = 0;

void ResetAcceptanceBottomLimit( void )
{
    SetAcceptanceTestStatus(ACCEPTANCE_TEST_BOTTOM_LIMIT,ACCEPTANCE_TEST_NOT_RUN);

    currentState = ACCEPTANCE_BOTTOM_LIMIT_IDLE;
    nextState = currentState;

    SetAcceptanceTestBypassDecelFlag(0);
    paramSetup = 0;
}

uint8_t AcceptanceBottomLimit( void )
{
    IncrementTimer();

    switch(currentState)
    {
        case ACCEPTANCE_BOTTOM_LIMIT_IDLE:
            // Transition only of on acceptance and also this test is selected
            if( (GetActiveAcceptanceTest() == ACCEPTANCE_TEST_BOTTOM_LIMIT) && OnAcceptanceTest() )
            {
                // Go to the bottom floor
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_BOTTOM_LIMIT,ACCEPTANCE_TEST_IN_PROGRESS);
                nextState = ACCEPTANCE_BOTTOM_LIMIT_MOVE_TO_BOTTOM;
            }
            break;
        case ACCEPTANCE_BOTTOM_LIMIT_MOVE_TO_BOTTOM:
            // Wait till at the bottom floor
            if(AtBottomFloor() && TimerExpired())
            {
                nextState = ACCEPTANCE_BOTTOM_LIMIT_PARAMETER_SETUP;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_BOTTOM_LIMIT_FAIL;
            }
            else
            {
                MoveAutomatic(0);
            }
            break;
        case ACCEPTANCE_BOTTOM_LIMIT_PARAMETER_SETUP:
            if(!paramSetup)
            {
                paramSetup = 1;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_BOTTOM_LIMIT_FAIL;
            }
            else if(TimerExpired() && NoActiveFaultOrAlarm())
            {
                nextState = ACCEPTANCE_BOTTOM_LIMIT_USER_PRESS_DN;
            }

            break;
        case ACCEPTANCE_BOTTOM_LIMIT_USER_PRESS_DN:
            // User needs to press the dn button to start the test
            if(GetInputValue( enIN_MRDN ))
            {
                nextState = ACCEPTANCE_BOTTOM_LIMIT_TEST_RUNNING;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_BOTTOM_LIMIT_FAIL;
            }
            break;
        case ACCEPTANCE_BOTTOM_LIMIT_TEST_RUNNING:
            // If the person lets go of the switch then go to failed state.
            if(GetInputValue( enIN_MRDN ))
            {
                MoveDownManual();
            }
            else
            {
                nextState = ACCEPTANCE_BOTTOM_LIMIT_FAIL;
            }

            // If test runs to long the go to fail state
            // If the position limit fault occures then go to pass state.
            if (GetFault_ByNumber(FLT__POSITION_LIMIT))
            {
                nextState = ACCEPTANCE_BOTTOM_LIMIT_PASS;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_BOTTOM_LIMIT_FAIL;
            }
            /*
            else if( TimerExpired())
            {
                nextState = ACCEPTANCE_BOTTOM_LIMIT_FAIL;
            }
*/
            break;
        case ACCEPTANCE_BOTTOM_LIMIT_PASS:
            if(TimerExtExpired())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_BOTTOM_LIMIT,ACCEPTANCE_TEST_PASS);
                nextState = ACCEPTANCE_BOTTOM_LIMIT_PARAMETER_RESTORE;
            }

            break;
        case ACCEPTANCE_BOTTOM_LIMIT_FAIL:
            if(TimerExtExpired())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_BOTTOM_LIMIT,ACCEPTANCE_TEST_FAIL);
                nextState = ACCEPTANCE_BOTTOM_LIMIT_PARAMETER_RESTORE;
            }
            else
            {
                StopManual();
            }
            break;
        case ACCEPTANCE_BOTTOM_LIMIT_PARAMETER_RESTORE:
            if(paramSetup)
            {
                SetAcceptanceTestBypassDecelFlag(0);
                paramSetup = 0;
            }
            else if( TimerExpired() && NoActiveFaultOrAlarm())
            {
                SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_BOTTOM_LIMIT,ACCEPTANCE_TEST_NOT_RUN);
                nextState = ACCEPTANCE_BOTTOM_LIMIT_IDLE;
            }
            else
            {
                StopManual();
            }
            break;
        default:
            // Default to the idle state. Command an E-Stop
            StopManual();
            SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
            SetAcceptanceTestStatus(ACCEPTANCE_TEST_BOTTOM_LIMIT,ACCEPTANCE_TEST_NOT_RUN);
            nextState = ACCEPTANCE_BOTTOM_LIMIT_IDLE;
            break;
    }

    // Anytime the state is going to change the timer must be reset.
    if(currentState != nextState)
    {
        ResetTimer();
    }

    currentState = nextState;


    return currentState;
}
