#include "acceptance.h"
#include "acceptanceTest.h"
#include "alarm_app.h"
#include "fault_app.h"
#include "mod.h"

static acceptance_CounterweightBuffer_FSM currentState = ACCEPTANCE_COUNTERWEIGHT_BUFFER_IDLE;
static acceptance_CounterweightBuffer_FSM nextState = ACCEPTANCE_COUNTERWEIGHT_BUFFER_IDLE;
static uint8_t paramSetup = 0;
static uint16_t contractSpeed = 0;

// Move the car to the bottom floor.
// Record the virtual ETS parameters
////BEGIN MECHANIC INTERVENTION////
// Jump out final limit switches
// Pull the NTS wire
////END MECHANIC INTERVENTION////
// Wait for the user to press the up & enable button
// Car places call to top floor. Need to to intervene with the patter so that we can strike the buffer.

void ResetAcceptanceCounterweightBuffer( void )
{
	if(contractSpeed)
	{
		Param_WriteValue_16Bit(enPARAM16__ContractSpeed, contractSpeed);
	}
	Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 0);
	Param_WriteValue_1Bit( enPARAM1__RelevelEnabled  , 1);
	SetAcceptanceTestBypassDecelFlag(0);
	SetETS_AcceptanceTest(0);
	paramSetup = 0;

	SetAcceptanceTestStatus(ACCEPTANCE_TEST_COUNTERWEIGHT_BUFFER,ACCEPTANCE_TEST_NOT_RUN);
	currentState = ACCEPTANCE_COUNTERWEIGHT_BUFFER_PARAMETER_RESTORE;
	nextState = currentState;
}

uint8_t AcceptanceCounterweightBuffer( void )
{
    IncrementTimer();

    switch(currentState)
    {
        case ACCEPTANCE_COUNTERWEIGHT_BUFFER_IDLE:
            if( (GetActiveAcceptanceTest() == ACCEPTANCE_TEST_COUNTERWEIGHT_BUFFER) && OnAcceptanceTest())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_COUNTERWEIGHT_BUFFER,ACCEPTANCE_TEST_IN_PROGRESS);
                nextState = ACCEPTANCE_COUNTERWEIGHT_BUFFER_IN_DZ;
            }
            break;
        case ACCEPTANCE_COUNTERWEIGHT_BUFFER_IN_DZ:
            // The car is at the bottom floor.

            if(( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R)  ) && TimerExpired())
            {
                nextState = ACCEPTANCE_COUNTERWEIGHT_BUFFER_PARAMETER_SETUP;
            }
            else if(!OnAcceptanceTest() || TimerExpired())
            {
                nextState = ACCEPTANCE_COUNTERWEIGHT_BUFFER_FAIL;
            }

            break;
        case ACCEPTANCE_COUNTERWEIGHT_BUFFER_PARAMETER_SETUP:

            if(!paramSetup)
            {
                // Store contract speed;
                 contractSpeed = Param_ReadValue_16Bit(enPARAM16__ContractSpeed);
                 // Set the drive contract speed parameter
                 //TODO SetDriveParameter();

                 //Set the internal contract speed parameter
                 // Make new contract speed 130%.
                 // This is hard coded for now. This could be a user settable parameter
                 Param_WriteValue_16Bit(enPARAM16__ContractSpeed, Param_ReadValue_16Bit(enPARAM16__Acceptance_BufferSpeed));

                 SetAcceptanceTestBypassDecelFlag(1);
                 // This bypasses NTS
                 Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 1);
                 Param_WriteValue_1Bit( enPARAM1__RelevelEnabled  , 0);
                 SetETS_AcceptanceBypassFlag(1);
                 SetETS_AcceptanceTest(1);
                paramSetup = 1;
            }
            else if(TimeoutReached())
            {
                nextState = ACCEPTANCE_COUNTERWEIGHT_BUFFER_USER_PRESS_UP;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_COUNTERWEIGHT_BUFFER_FAIL;
            }
            break;
        case ACCEPTANCE_COUNTERWEIGHT_BUFFER_USER_PRESS_UP:
            // User needs to pull the NTS wire
            // User needs to jump the Final Switches
            //If the above is not done the car will not strike the buffer
            if(GetInputValue( enIN_MRUP ))
            {

                nextState = ACCEPTANCE_COUNTERWEIGHT_BUFFER_TEST_RUNNING;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_COUNTERWEIGHT_BUFFER_FAIL;
            }
            break;
        case ACCEPTANCE_COUNTERWEIGHT_BUFFER_TEST_RUNNING:
            MoveAutomatic((GetFP_NumFloors()-1));
            if ( GetFault_ByNumber(FLT__SAFETY_STR_BUF) )
            {
                nextState = ACCEPTANCE_COUNTERWEIGHT_BUFFER_PASS;
            }
            // If the user lets go of the buffer switch then go to fail
            // If at top floor and not moving then fail
            else if( !GetInputValue( enIN_MRUP ) || AtTopFloor() )
            {
                nextState = ACCEPTANCE_COUNTERWEIGHT_BUFFER_FAIL;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_COUNTERWEIGHT_BUFFER_FAIL;
            }
            break;
        case ACCEPTANCE_COUNTERWEIGHT_BUFFER_PASS:
            if(TimerExtExpired())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_COUNTERWEIGHT_BUFFER,ACCEPTANCE_TEST_PASS);
                nextState = ACCEPTANCE_COUNTERWEIGHT_BUFFER_PARAMETER_RESTORE;
            }
            break;
        case ACCEPTANCE_COUNTERWEIGHT_BUFFER_FAIL:
            if(TimerExtExpired())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_COUNTERWEIGHT_BUFFER,ACCEPTANCE_TEST_FAIL);
                nextState = ACCEPTANCE_COUNTERWEIGHT_BUFFER_PARAMETER_RESTORE;
            }
            else
            {
                StopManual();
            }


            break;
        case ACCEPTANCE_COUNTERWEIGHT_BUFFER_PARAMETER_RESTORE:

            if(paramSetup)
            {
                SetAcceptanceTestBypassDecelFlag(0);
                Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 0);
                Param_WriteValue_1Bit( enPARAM1__RelevelEnabled  , 1);
                SetETS_AcceptanceTest(0);
                paramSetup = 0;
                Param_WriteValue_16Bit(enPARAM16__ContractSpeed, contractSpeed);
            }
            else if(TimerExpired())
            {
                SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_COUNTERWEIGHT_BUFFER,ACCEPTANCE_TEST_NOT_RUN);
                nextState = ACCEPTANCE_COUNTERWEIGHT_BUFFER_IDLE;
            }

            break;
        default:
            StopManual();
            SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
            SetAcceptanceTestStatus(ACCEPTANCE_TEST_COUNTERWEIGHT_BUFFER,ACCEPTANCE_TEST_NOT_RUN);
            nextState = ACCEPTANCE_COUNTERWEIGHT_BUFFER_IDLE;
            break;
    }
    // Anytime the state is going to change the timer must be reset.
    if(currentState != nextState)
    {
        ResetTimer();
    }

    currentState = nextState;
    return currentState;
}
