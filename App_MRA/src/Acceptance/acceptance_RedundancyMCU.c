#include "acceptance.h"
#include "acceptanceTest.h"
#include "alarm_app.h"
#include "fault_app.h"
#include "mod.h"

static acceptance_RedundancyMCU_FSM currentState = ACCEPTANCE_REDUNDANCY_MCU_IDLE;
static acceptance_RedundancyMCU_FSM nextState = ACCEPTANCE_REDUNDANCY_MCU_IDLE;

void ResetAcceptanceRedundancyMCU( void )
{
	SetAcceptanceTestStatus(ACCEPTANCE_TEST_REDUNDANCY_MCU,ACCEPTANCE_TEST_NOT_RUN);
	currentState = ACCEPTANCE_REDUNDANCY_MCU_IDLE;
	nextState = currentState;
}

uint8_t AcceptanceRedundancyMCU( void )
{


    IncrementTimer();

    if( ( !GetInputValue( enIN_MM ) || GetInputValue(enIN_MRTR) ) )
    {
        SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
    }

    switch(currentState)
    {
        case ACCEPTANCE_REDUNDANCY_MCU_IDLE:
            if( (GetActiveAcceptanceTest() == ACCEPTANCE_TEST_REDUNDANCY_MCU) && OnAcceptanceTest())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_REDUNDANCY_MCU,ACCEPTANCE_TEST_IN_PROGRESS);
                nextState = ACCEPTANCE_REDUNDANCY_MCU_TEST_RUNNING;
            }
            break;
        case ACCEPTANCE_REDUNDANCY_MCU_TEST_RUNNING:
           if ( GetFault_ByRange(FLT__REDUNDANCY_LRB, FLT__REDUNDANCY_C_SFP) )
            {
                nextState = ACCEPTANCE_REDUNDANCY_MCU_PASS;
            }
            else if( TimerExpired())
            {
                nextState = ACCEPTANCE_REDUNDANCY_MCU_FAIL;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_REDUNDANCY_MCU_FAIL;
            }
            break;
        case ACCEPTANCE_REDUNDANCY_MCU_PASS:
            if(TimerExtExpired())
            {
                SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_REDUNDANCY_MCU,ACCEPTANCE_TEST_PASS);
                nextState = ACCEPTANCE_REDUNDANCY_MCU_IDLE;
            }
            break;
        case ACCEPTANCE_REDUNDANCY_MCU_FAIL:
            // Error so we want to stop the car ASAP
            if(TimerExtExpired())
            {
                SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_REDUNDANCY_MCU,ACCEPTANCE_TEST_FAIL);
                nextState = ACCEPTANCE_REDUNDANCY_MCU_IDLE;
            }
            else
            {
                StopManual();
            }

            break;
        default:
            StopManual();
            SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
            SetAcceptanceTestStatus(ACCEPTANCE_TEST_REDUNDANCY_MCU,ACCEPTANCE_TEST_FAIL);
            nextState = ACCEPTANCE_REDUNDANCY_MCU_IDLE;
            break;
    }
    // Anytime the state is going to change the timer must be reset.
    if(currentState != nextState)
    {
        ResetTimer();
    }

    currentState = nextState;
    return currentState;
}
