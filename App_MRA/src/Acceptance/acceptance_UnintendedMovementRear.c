#include "acceptance.h"
#include "acceptanceTest.h"
#include "alarm_app.h"
#include "fault_app.h"
#include "mod.h"

#define ACCEPTANCE_SIX_INCHES (300)

static uint32_t startPosition = 0;

static acceptance_UnintendedMovementREAR_FSM currentState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_IDLE;
static acceptance_UnintendedMovementREAR_FSM nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_IDLE;

static uint8_t paramSetup = 0;

void ResetAcceptanceUnintendedMovementRear( void )
{
	SetAcceptanceTestStatus(ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_REAR,ACCEPTANCE_TEST_NOT_RUN);
	currentState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_IDLE;
	currentState = nextState;
}

static uint8_t MovedMoreThan6Inches( void )
{
    uint32_t currentPosition = GetPosition_PositionCount();
    uint8_t result = 0;

    // Moved up 6 inches
    if(currentPosition > (startPosition + ACCEPTANCE_SIX_INCHES))
    {
        result = 1;
    }
    else if(currentPosition < (startPosition - ACCEPTANCE_SIX_INCHES))
    {
        result = 1;
    }
    return result;
}

uint8_t AcceptanceUnintendedMovementRear( void )
{
    IncrementTimer();

    if( !OnAcceptanceTest() && paramSetup )
    {
        currentState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PARAMETER_RESTORE;
    }
    else if( !OnAcceptanceTest() && !paramSetup)
    {
        SetAcceptanceTestStatus(ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_REAR,ACCEPTANCE_TEST_NOT_RUN);
        SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
    }

    switch(currentState)
    {
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_IDLE:
            if( (GetActiveAcceptanceTest() == ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_REAR) && OnAcceptanceTest())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_REAR,ACCEPTANCE_TEST_IN_PROGRESS);
                ResetTimer();
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_IN_DZ;
            }
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_IN_DZ:
            // The car is inside of a DZ
            if( (CheckIfInDoorZone() && gpastFloors[GetOperation_CurrentFloor()].bRearOpening) && TimerExpired() )
            {
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PARAMETER_SETUP;
            }
            else if(!OnAcceptanceTest() || !(CheckIfInDoorZone() && gpastFloors[GetOperation_CurrentFloor()].bRearOpening))
            {
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_FAIL;
            }
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PARAMETER_SETUP:
            if(!paramSetup)
            {
                // No parameters to set up

                paramSetup = 1;
            }
            // Wait for faults or alarms to clear.
            else if(TimerExpired() )
            {
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_ENABLE_DIP_SWITCH;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_FAIL;
            }

            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_ENABLE_DIP_SWITCH:
            // User must enable DipB8. This lets the CPLD know we are in this test.
            if( SRU_Read_DIP_Switch( enSRU_DIP_B8 ) && TimerExpired() )
            {
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_OPEN_DOORS;

            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_FAIL;
            }
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_OPEN_DOORS:

            if (TimerExpired() && (GetDoorState(DOOR_REAR) == DOOR__OPEN ))
            {
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_USER_PRESS_UP;
            }
            else
            {
                if(gpastFloors[GetOperation_CurrentFloor()].bRearOpening)
                {
                   SetDoorCommand( DOOR_REAR, DOOR_COMMAND__OPEN_HOLD_REQUEST );
                }
            }
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_USER_PRESS_UP:

            // Store the current position count
            startPosition = GetPosition_PositionCount();
            if(GetInputValue( enIN_MRUP ))
            {
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PICK_B2;
            }
            else if(!OnAcceptanceTest() )
            {
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_FAIL;
            }
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PICK_B2:

            if(TimerExpired()|| GetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2))
            {
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PICK_SECONDARY;
            }
            else if( MovedMoreThan6Inches() )
            {
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_FAIL;
            }
            else
            {
                // To start the test the user must press the up button
               if(gpastFloors[GetOperation_CurrentFloor()].bRearOpening)
               {
                  SetDoorCommand( DOOR_REAR, DOOR_COMMAND__OPEN_HOLD_REQUEST );
               }
                SetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2,1);
            }
            break;

        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PICK_SECONDARY:

            if(TimerExpired())
            {
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PICK_PRIMARY;
                SetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY,1);

            }
            else if( MovedMoreThan6Inches() )
            {
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_FAIL;
            }
            else
            {
               if(gpastFloors[GetOperation_CurrentFloor()].bRearOpening)
               {
                  SetDoorCommand( DOOR_REAR, DOOR_COMMAND__OPEN_HOLD_REQUEST );
               }
            }

            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PICK_PRIMARY:

            if( GetInputValue(enIN_MBC))
            {
                nextState =  ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_TEST_RUNNING;
                SetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY,1);
            }
            else
            {
               if(gpastFloors[GetOperation_CurrentFloor()].bRearOpening)
               {
                  SetDoorCommand( DOOR_REAR, DOOR_COMMAND__OPEN_HOLD_REQUEST );
               }
            }
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_TEST_RUNNING:

            if ( GetFault_ByRange(FLT__UNINTENDED_MOV, FLT__UNINTENDED_MOV_L)
              || GetFault_ByNumber(FLT__CPLD_UNINT_MOV) )
            {
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PASS;
            }
            else if( MovedMoreThan6Inches() || !GetInputValue(enIN_MBC))
            {
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_FAIL;
            }
            else if(!OnAcceptanceTest() )
            {
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_FAIL;
            }
            else
            {
               if(gpastFloors[GetOperation_CurrentFloor()].bRearOpening)
               {
                  SetDoorCommand( DOOR_REAR, DOOR_COMMAND__OPEN_HOLD_REQUEST );
               }
            }
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PASS:
            StopManual();
            // Keep secondary picked to show that it can hold the car
            SetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY ,0);
            SetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY   ,1);
            SetAcceptanceContactorPickCommand(CONTACTOR_SEL__M ,0);
            SetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2,0);
            if(TimerExtExpired() && !GetInputValue( enIN_MRUP ))
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_REAR,ACCEPTANCE_TEST_PASS);
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PARAMETER_RESTORE;
            }
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_FAIL:
            StopManual();
            SetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY ,0);
            SetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY   ,0);
            SetAcceptanceContactorPickCommand(CONTACTOR_SEL__M ,0);
            SetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2,0);
            if(TimerExtExpired())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_REAR,ACCEPTANCE_TEST_FAIL);
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PARAMETER_RESTORE;
            }
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_PARAMETER_RESTORE:
            StopManual();
            SetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY,0);
            SetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY  ,0);
            SetAcceptanceContactorPickCommand(CONTACTOR_SEL__M,0);
            SetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2,0);
            if(paramSetup)
            {

                paramSetup = 0;
            }
            else if(TimerExpired())
            {
                SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_REAR,ACCEPTANCE_TEST_NOT_RUN);
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_DISABLE_DIP_SWITCH;
            }
            else
            {
               CloseAnyOpenDoor();
            }
            break;
        case ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_DISABLE_DIP_SWITCH:
            StopManual();
            SetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY,0);
            SetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY  ,0);
            SetAcceptanceContactorPickCommand(CONTACTOR_SEL__M,0);
            SetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2,0);
            if( !SRU_Read_DIP_Switch( enSRU_DIP_B8 ) )
            {
                nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_IDLE;
            }
            else
            {
                CloseAnyOpenDoor();
            }
            break;
        default:
            StopManual();
            SetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY,0);
            SetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY  ,0);
            SetAcceptanceContactorPickCommand(CONTACTOR_SEL__M,0);
            SetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2,0);

            SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
            SetAcceptanceTestStatus(ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_REAR,ACCEPTANCE_TEST_NOT_RUN);
            CloseAnyOpenDoor();

            nextState = ACCEPTANCE_UNINTENDED_MOVEMENT_REAR_IDLE;
            break;
    }
    // Anytime the state is going to change the timer must be reset.
    if(currentState != nextState)
    {
        ResetTimer();
    }

    currentState = nextState;    return currentState;
}
