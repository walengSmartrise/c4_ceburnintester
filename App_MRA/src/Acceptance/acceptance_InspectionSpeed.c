#include "acceptance.h"
#include "acceptanceTest.h"
#include "alarm_app.h"
#include "fault_app.h"
#include "mod.h"

static acceptance_InspectionSpeed_FSM currentState = ACCEPTANCE_INSPECTION_SPEED_IDLE;
static acceptance_InspectionSpeed_FSM nextState = ACCEPTANCE_INSPECTION_SPEED_IDLE;
static uint8_t paramSetup = 0;
static uint16_t inspectionSpeed;

void ResetAcceptanceInspectionSpeed( void )
{
	inspectionSpeed = ( inspectionSpeed >= MAX_INSP_SPEED) ? 20 : inspectionSpeed;
	Param_WriteValue_16Bit(enPARAM16__InspectionSpeed,inspectionSpeed);
	paramSetup = 0;

    SetAcceptanceTestStatus(ACCEPTANCE_TEST_INSPECTION_SPEED,ACCEPTANCE_TEST_NOT_RUN);
    currentState = ACCEPTANCE_INSPECTION_SPEED_IDLE;
    nextState = currentState;
}

uint8_t AcceptanceInspectionSpeed( void )
{



    IncrementTimer();

    if( !OnAcceptanceTest() && paramSetup )
    {
        currentState = ACCEPTANCE_INSPECTION_SPEED_PARAMETER_RESTORE;
    }
    else if( !OnAcceptanceTest() && !paramSetup)
    {

        SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
    }
    
    switch(currentState)
    {
        case ACCEPTANCE_INSPECTION_SPEED_IDLE:
            if( (GetActiveAcceptanceTest() == ACCEPTANCE_TEST_INSPECTION_SPEED) && OnAcceptanceTest())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_INSPECTION_SPEED,ACCEPTANCE_TEST_IN_PROGRESS);
                nextState = ACCEPTANCE_INSPECTION_SPEED_PARAMETER_SETUP;
            }
            break;
        case ACCEPTANCE_INSPECTION_SPEED_PARAMETER_SETUP:
            if(!paramSetup)
            {
                // Save the current inspection speed.
                inspectionSpeed = Param_ReadValue_16Bit(enPARAM16__InspectionSpeed);
                // Set the inspection speed to be the max false plus 1fpm. this
                Param_WriteValue_16Bit(enPARAM16__InspectionSpeed,MAX_INSP_SPEED+1);
                paramSetup = 1;
            }
            if( TimerExpired() )
            {
                nextState = ACCEPTANCE_INSPECTION_SPEED_TEST_RUNNING;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_INSPECTION_SPEED_FAIL;
            }
            break;
        case ACCEPTANCE_INSPECTION_SPEED_TEST_RUNNING:
            // If the timer expires then this test fails. If the invalid inspection speed
            // fault is issued this test passes.
            if ( GetFault_ByNumber(FLT__INVALID_INSP_SPD) )
            {
                nextState = ACCEPTANCE_INSPECTION_SPEED_PASS;
            }
            else if( TimerExpired())
            {
                nextState = ACCEPTANCE_INSPECTION_SPEED_FAIL;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_INSPECTION_SPEED_FAIL;
            }
            break;
        case ACCEPTANCE_INSPECTION_SPEED_PASS:
            if(TimerExtExpired())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_INSPECTION_SPEED,ACCEPTANCE_TEST_PASS);
                nextState = ACCEPTANCE_INSPECTION_SPEED_PARAMETER_RESTORE;
            }
            break;
        case ACCEPTANCE_INSPECTION_SPEED_FAIL:
            if(TimerExtExpired())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_INSPECTION_SPEED,ACCEPTANCE_TEST_FAIL);
                nextState = ACCEPTANCE_INSPECTION_SPEED_PARAMETER_RESTORE;
            }
            else
            {
                StopManual();
            }
            break;
        case ACCEPTANCE_INSPECTION_SPEED_PARAMETER_RESTORE:

            if(paramSetup)
            {
                // Just in case the system accidently made inspection speed greater than
                // the max speed set inspection speed to 20 fpm.
                inspectionSpeed = ( inspectionSpeed >= MAX_INSP_SPEED) ? 20 : inspectionSpeed;
                Param_WriteValue_16Bit(enPARAM16__InspectionSpeed,inspectionSpeed);
                paramSetup = 0;
            }
            else if(TimerExpired())
            {
                SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_INSPECTION_SPEED,ACCEPTANCE_TEST_NOT_RUN);
                nextState = ACCEPTANCE_INSPECTION_SPEED_IDLE;
            }

            break;
        default:
            StopManual();
            SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
            SetAcceptanceTestStatus(ACCEPTANCE_TEST_INSPECTION_SPEED,ACCEPTANCE_TEST_NOT_RUN);
            nextState = ACCEPTANCE_INSPECTION_SPEED_IDLE;
            break;
    }
    // Anytime the state is going to change the timer must be reset.
    if(currentState != nextState)
    {
        ResetTimer();
    }

    currentState = nextState;
    return currentState;
}
