#include "acceptance.h"
#include "acceptanceTest.h"
#include "alarm_app.h"
#include "fault_app.h"
#include "mod.h"

static acceptance_RedundancyCPLD_FSM currentState = ACCEPTANCE_REDUNDANCY_CPLD_IDLE;
static acceptance_RedundancyCPLD_FSM nextState = ACCEPTANCE_REDUNDANCY_CPLD_IDLE;

void ResetAcceptanceRedundancyCPLD( void )
{
	SetAcceptanceTestStatus(ACCEPTANCE_TEST_REDUNDANCY_CPLD,ACCEPTANCE_TEST_NOT_RUN);
}

uint8_t AcceptanceRedundancyCPLD( void )
{
    IncrementTimer();

    switch(currentState)
    {
        case ACCEPTANCE_REDUNDANCY_CPLD_IDLE:
            if( (GetActiveAcceptanceTest() == ACCEPTANCE_TEST_REDUNDANCY_CPLD) && OnAcceptanceTest())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_REDUNDANCY_CPLD,ACCEPTANCE_TEST_IN_PROGRESS);
                nextState = ACCEPTANCE_REDUNDANCY_CPLD_TEST_RUNNING;
            }
            break;
        case ACCEPTANCE_REDUNDANCY_CPLD_TEST_RUNNING:
            if ( GetFault_ByRange(FLT__REDUNDANCY_LRB, FLT__REDUNDANCY_C_SFP) )
            {
                nextState = ACCEPTANCE_REDUNDANCY_CPLD_PASS;
            }
            else if( TimerExpired())
            {
                nextState = ACCEPTANCE_REDUNDANCY_CPLD_FAIL;
            }
            else if (!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_REDUNDANCY_CPLD_FAIL;
            }
            break;
        case ACCEPTANCE_REDUNDANCY_CPLD_PASS:
            if(TimerExtExpired())
            {
                SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_REDUNDANCY_CPLD,ACCEPTANCE_TEST_PASS);
                nextState = ACCEPTANCE_REDUNDANCY_CPLD_IDLE;
            }

            break;
        case ACCEPTANCE_REDUNDANCY_CPLD_FAIL:
            if(TimerExtExpired())
            {
                SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_REDUNDANCY_CPLD,ACCEPTANCE_TEST_FAIL);
                nextState = ACCEPTANCE_REDUNDANCY_CPLD_IDLE;
            }
            else
            {
                StopManual();
            }
            break;
        default:
            StopManual();
            SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
            SetAcceptanceTestStatus(ACCEPTANCE_TEST_REDUNDANCY_CPLD,ACCEPTANCE_TEST_FAIL);
            nextState = ACCEPTANCE_REDUNDANCY_CPLD_IDLE;
            break;
    }
    // Anytime the state is going to change the timer must be reset.
    if(currentState != nextState)
    {
        ResetTimer();
    }

    currentState = nextState;
    return currentState;
}
