#include "acceptance.h"
#include "acceptanceTest.h"
#include "alarm_app.h"
#include "fault_app.h"
#include "mod.h"

static acceptance_CarBuffer_FSM currentState = ACCEPTANCE_CAR_BUFFER_IDLE;
static acceptance_CarBuffer_FSM nextState = ACCEPTANCE_CAR_BUFFER_IDLE;

static uint8_t paramSetup = 0;
static uint16_t contractSpeed = 0;

// Move the car to the bottom floor.
// Record the virtual ETS parameters
////BEGIN MECHANIC INTERVENTION////
// Jump out final limit switches
// Pull the NTS wire
////END MECHANIC INTERVENTION////
// Wait for the user to press the up & enable button
// Car places call to top floor. Need to to intervene with the patter so that we can strike the buffer.

void ResetAcceptanceCarBuffer( void )
{
	if(contractSpeed)
	{
		Param_WriteValue_16Bit(enPARAM16__ContractSpeed, contractSpeed);
	}

	Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 0);
	Param_WriteValue_1Bit( enPARAM1__RelevelEnabled  , 1);

	SetAcceptanceTestBypassDecelFlag(0);
	SetETS_AcceptanceBypassFlag(0);
	SetETS_AcceptanceTest(0);

	paramSetup = 0;

    SetAcceptanceTestStatus(ACCEPTANCE_TEST_CAR_BUFFER,ACCEPTANCE_TEST_NOT_RUN);

	currentState = ACCEPTANCE_CAR_BUFFER_IDLE;
	nextState = currentState;
}
uint8_t AcceptanceCarBuffer( void )
{
    IncrementTimer();

    switch(currentState)
    {
        case ACCEPTANCE_CAR_BUFFER_IDLE:
            if( (GetActiveAcceptanceTest() == ACCEPTANCE_TEST_CAR_BUFFER) && OnAcceptanceTest())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_CAR_BUFFER,ACCEPTANCE_TEST_IN_PROGRESS);
                nextState = ACCEPTANCE_CAR_BUFFER_IN_DZ;
            }
            break;
        case ACCEPTANCE_CAR_BUFFER_IN_DZ:
            // The car is at the bottom floor.
            if( (GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R)) && TimerExpired())
            {
                nextState = ACCEPTANCE_CAR_BUFFER_PARAMETER_SETUP;
            }
            else if(!OnAcceptanceTest() || TimerExpired())
            {
                nextState = ACCEPTANCE_CAR_BUFFER_FAIL;
            }
            break;
        case ACCEPTANCE_CAR_BUFFER_PARAMETER_SETUP:
            if(!paramSetup)
            {
                // Store contract speed;
                contractSpeed = Param_ReadValue_16Bit(enPARAM16__ContractSpeed);
                // Set the drive contract speed parameter
                //TODO SetDriveParameter();

                //Set the internal contract speed parameter
                // Make new contract speed 130%.
                // This is hard coded for now. This could be a user settable parameter
                Param_WriteValue_16Bit(enPARAM16__ContractSpeed, Param_ReadValue_16Bit(enPARAM16__Acceptance_BufferSpeed));

                SetAcceptanceTestBypassDecelFlag(1);
                // This bypasses NTS
                Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 1);
                Param_WriteValue_1Bit( enPARAM1__RelevelEnabled  , 0);
                SetETS_AcceptanceBypassFlag(1);
                SetETS_AcceptanceTest(1);


                paramSetup = 1;
            }
            else if(TimeoutReached())
            {
                nextState = ACCEPTANCE_CAR_BUFFER_USER_PRESS_DOWN;
            }
            else if(!NoActiveFaultOrAlarm())
            {
            	ResetTimer();
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_CAR_BUFFER_FAIL;
            }
            break;
        case ACCEPTANCE_CAR_BUFFER_USER_PRESS_DOWN:
            // User needs to pull the NTS wire
            // User needs to jump the Final Switches
            //If the above is not done the car will not strike the buffer
            if(GetInputValue( enIN_MRDN ))
            {
                nextState = ACCEPTANCE_CAR_BUFFER_TEST_RUNNING;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_CAR_BUFFER_FAIL;
            }
            break;
        case ACCEPTANCE_CAR_BUFFER_TEST_RUNNING:
            MoveAutomatic(0);
            if ( GetFault_ByNumber(FLT__SAFETY_STR_BUF) )
            {
                nextState = ACCEPTANCE_CAR_BUFFER_PASS;
            }
            // If the user lets go of the buffer switch then go to fail
            // If at top floor and not moving then fail
            else if( !GetInputValue( enIN_MRDN ) || AtBottomFloor() )
            {
                nextState = ACCEPTANCE_CAR_BUFFER_FAIL;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_CAR_BUFFER_FAIL;
            }
            break;
        case ACCEPTANCE_CAR_BUFFER_PASS:
            if(TimerExtExpired())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_CAR_BUFFER,ACCEPTANCE_TEST_PASS);
                nextState = ACCEPTANCE_CAR_BUFFER_PARAMETER_RESTORE;
            }
            break;
        case ACCEPTANCE_CAR_BUFFER_FAIL:
            StopManual();
            if(TimerExtExpired())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_CAR_BUFFER,ACCEPTANCE_TEST_FAIL);
                nextState = ACCEPTANCE_CAR_BUFFER_PARAMETER_RESTORE;
            }


            break;
        case ACCEPTANCE_CAR_BUFFER_PARAMETER_RESTORE:

            if(paramSetup)
            {
                Param_WriteValue_16Bit(enPARAM16__ContractSpeed, contractSpeed);

                SetAcceptanceTestBypassDecelFlag(0);
                Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 0);
                Param_WriteValue_1Bit( enPARAM1__RelevelEnabled  , 1);
                SetETS_AcceptanceBypassFlag(0);
                SetETS_AcceptanceTest(0);
                paramSetup = 0;


            }
            else if(TimerExpired())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_CAR_BUFFER,ACCEPTANCE_TEST_NOT_RUN);
                SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
                nextState = ACCEPTANCE_CAR_BUFFER_IDLE;
            }

            break;
        default:
            StopManual();
            SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
            SetAcceptanceTestStatus(ACCEPTANCE_TEST_CAR_BUFFER,ACCEPTANCE_TEST_NOT_RUN);
            nextState = ACCEPTANCE_CAR_BUFFER_IDLE;
            break;
    }
    // Anytime the state is going to change the timer must be reset.
    if(currentState != nextState)
    {
        ResetTimer();
    }

    currentState = nextState;
    return currentState;
}
