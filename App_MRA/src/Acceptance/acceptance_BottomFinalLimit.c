#include "acceptance.h"
#include "acceptanceTest.h"
#include "alarm_app.h"
#include "fault_app.h"
#include "mod.h"

static acceptance_BottomFinalLimit_FSM currentState = ACCEPTANCE_BOTTOM_FINAL_LIMIT_IDLE;
static acceptance_BottomFinalLimit_FSM nextState = ACCEPTANCE_BOTTOM_FINAL_LIMIT_IDLE;
static uint8_t paramSetup = 0;

void ResetAcceptanceBottomFinalLimit( void )
{
	currentState = ACCEPTANCE_BOTTOM_FINAL_LIMIT_IDLE;
	nextState = currentState;
	paramSetup = 0;

	Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 0 );
	SetAcceptanceTestBypassDecelFlag(0);

	SetAcceptanceTestStatus(ACCEPTANCE_TEST_BOTTOM_FINAL_LIMIT,ACCEPTANCE_TEST_NOT_RUN);
}

uint8_t AcceptanceBottomFinalLimit( void )
{
    IncrementTimer();

    switch(currentState)
    {
        case ACCEPTANCE_BOTTOM_FINAL_LIMIT_IDLE:
            if( (GetActiveAcceptanceTest() == ACCEPTANCE_TEST_BOTTOM_FINAL_LIMIT) && OnAcceptanceTest() )
            {
                // Go to the bottom floor
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_BOTTOM_FINAL_LIMIT,ACCEPTANCE_TEST_IN_PROGRESS);
                nextState = ACCEPTANCE_BOTTOM_FINAL_LIMIT_MOVE_TO_BOTTOM;
            }
            break;
        case ACCEPTANCE_BOTTOM_FINAL_LIMIT_MOVE_TO_BOTTOM:
            // Wait till at the bottom floor
            if(AtBottomFloor() && TimerExpired())
            {
                nextState = ACCEPTANCE_BOTTOM_FINAL_LIMIT_PARAMETER_SETUP;
            }
            // Not at bottom floor and are not moving
            else if ( !AtBottomFloor() &&  !GetMotion_RunFlag())
            {
                MoveAutomatic(0);
            }
            // Not moving and have been in this state for to long
            else if(TimerExpired() && !GetMotion_RunFlag())
            {
                nextState = ACCEPTANCE_BOTTOM_FINAL_LIMIT_FAIL;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_BOTTOM_FINAL_LIMIT_FAIL;
            }
            break;
        case ACCEPTANCE_BOTTOM_FINAL_LIMIT_PARAMETER_SETUP:
            if(!paramSetup)
            {
                Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 1);
                SetAcceptanceTestBypassDecelFlag(1);
                paramSetup = 1;
            }
            else if(TimerExpired() && NoActiveFaultOrAlarm())
            {
                nextState = ACCEPTANCE_BOTTOM_FINAL_LIMIT_USER_PRESS_DOWN;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_BOTTOM_FINAL_LIMIT_FAIL;
            }
            break;
        case ACCEPTANCE_BOTTOM_FINAL_LIMIT_USER_PRESS_DOWN:
            // User needs to press the dn button to start the test
            if(GetInputValue( enIN_MRDN ))
            {
                nextState = ACCEPTANCE_BOTTOM_FINAL_LIMIT_TEST_RUNNING;
            }
            break;
        case ACCEPTANCE_BOTTOM_FINAL_LIMIT_TEST_RUNNING:
            // If the person lets go of the switch then go to failed state.
            if(GetInputValue( enIN_MRDN ))
            {
                MoveDownManual();
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_BOTTOM_FINAL_LIMIT_FAIL;
            }
            else
            {
                nextState = ACCEPTANCE_BOTTOM_FINAL_LIMIT_FAIL;
            }

            if ( GetFault_ByRange(FLT__SAFETY_STR_SFH, FLT__SAFETY_STR_CAR_SAFE) )
            {
                nextState = ACCEPTANCE_BOTTOM_FINAL_LIMIT_PASS;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_BOTTOM_FINAL_LIMIT_FAIL;
            }

            break;
        case ACCEPTANCE_BOTTOM_FINAL_LIMIT_PASS:
            // Make sure the car has stopped moving
            if(!GetMotion_RunFlag() && TimerExtExpired())
            {
                StopManual();
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_BOTTOM_FINAL_LIMIT,ACCEPTANCE_TEST_PASS);
                nextState = ACCEPTANCE_BOTTOM_FINAL_LIMIT_PARAMETER_RESTORE;
            }
            break;
        case ACCEPTANCE_BOTTOM_FINAL_LIMIT_FAIL:
            if(TimerExtExpired())
            {

                SetAcceptanceTestStatus(ACCEPTANCE_TEST_BOTTOM_FINAL_LIMIT,ACCEPTANCE_TEST_FAIL);
                nextState = ACCEPTANCE_TOP_FINAL_LIMIT_PARAMETER_RESTORE;
            }
            else
            {
                StopManual();
            }

            break;
        case ACCEPTANCE_BOTTOM_FINAL_LIMIT_PARAMETER_RESTORE:
            if(paramSetup)
            {
                StopManual();
                Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 0 );
                SetAcceptanceTestBypassDecelFlag(0);
                paramSetup = 0;
            }
            else if(TimerExpired() && NoActiveFaultOrAlarm())
            {
                SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_BOTTOM_FINAL_LIMIT,ACCEPTANCE_TEST_NOT_RUN);
                nextState = ACCEPTANCE_BOTTOM_FINAL_LIMIT_IDLE;
            }
            break;
        default:
            // Default to the idle state. Command an E-Stop
            StopManual();
            SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
            SetAcceptanceTestStatus(ACCEPTANCE_TEST_BOTTOM_FINAL_LIMIT,ACCEPTANCE_TEST_NOT_RUN);
            nextState = ACCEPTANCE_BOTTOM_FINAL_LIMIT_IDLE;
            break;
    }
    // Anytime the state is going to change the timer must be reset.
    if(currentState != nextState)
    {
        ResetTimer();
    }

    currentState = nextState;
    return currentState;
}
