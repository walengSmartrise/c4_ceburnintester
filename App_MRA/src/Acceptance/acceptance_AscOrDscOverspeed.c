#include "acceptance.h"
#include "acceptanceTest.h"
#include "alarm_app.h"
#include "fault_app.h"
#include "mod.h"

typedef enum
{
    TEST_NONE,
    TEST_ASCENDING,
    TEST_DESCENDING
} TestType;

static uint16_t contractSpeed = 0;
static TestType ascendingOrDescending = TEST_NONE;
static acceptance_AscOrDscOverspeed_FSM currentState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_IDLE;
static acceptance_AscOrDscOverspeed_FSM nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_IDLE;
static uint8_t paramSetup = 0;

// This is testing the electrical contact on the governor.
// This should trip between 125-150% of contract speed.
// When tripped the governor input on the controller will go low.

// Steps
// 1: Move car to bottom of hoistway
// 2: Save contract speed parameter
//    Set new parameter as 130% of contract speed
// 3: issue command to top floor
// 4: If GOV trips then go to pass, otherwise if
//    arrive at top floor then test failed.



void ResetAcceptanceAscOrDescOverspeed( void )
{
   if(contractSpeed)
   {
      Param_WriteValue_16Bit(enPARAM16__ContractSpeed,contractSpeed);
   }

   Param_WriteValue_1Bit( enPARAM1__RelevelEnabled  , 1);
   paramSetup = 0;

   SetAcceptanceTestStatus(ACCEPTANCE_TEST_ASC_OR_DESC_OVERSPEED,ACCEPTANCE_TEST_NOT_RUN);
   ascendingOrDescending = TEST_NONE;

    currentState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_IDLE;
    nextState = currentState;
}

// Move the car in the up direction. This way the car safities do not engage.
uint8_t AcceptanceAscOrDescOverspeed( void )
{
    IncrementTimer();

    switch(currentState)
   {
      case ACCEPTANCE_ASC_OR_DESC_OVERSPEED_IDLE:
         if( (GetActiveAcceptanceTest() == ACCEPTANCE_TEST_ASC_OR_DESC_OVERSPEED) && OnAcceptanceTest())
         {
            SetAcceptanceTestStatus(ACCEPTANCE_TEST_ASC_OR_DESC_OVERSPEED,ACCEPTANCE_TEST_IN_PROGRESS);
            nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_IN_DZ;
         }
         break;
      case ACCEPTANCE_ASC_OR_DESC_OVERSPEED_IN_DZ:
         if( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R)  )
         {
            nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_PARAMETER_SETUP;
         }
         else if(!OnAcceptanceTest() || !(GetInputValue(enIN_DZ_F) && GetInputValue(enIN_DZ_R)))
         {
            nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_FAIL;
         }

         break;
      case ACCEPTANCE_ASC_OR_DESC_OVERSPEED_PARAMETER_SETUP:
         if(!paramSetup)
         {
            // Store contract speed;
            contractSpeed = Param_ReadValue_16Bit(enPARAM16__ContractSpeed);
            // Set the drive contract speed parameter
            //TODO SetDriveParameter();

            //Set the internal contract speed parameter
            // Make new contract speed 130%.
            // This is hard coded for now. This could be a user settable parameter
            Param_WriteValue_16Bit(enPARAM16__ContractSpeed, Param_ReadValue_16Bit(enPARAM16__Acceptance_AscOrDescSpeed));

            paramSetup = 1;
         }
         else if(TimeoutReached() && NoActiveFaultOrAlarm())
         {
            nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_WAIT_FOR_USER;
         }
            else if(!NoActiveFaultOrAlarm())
            {
               ResetTimer();
            }
         else if(!OnAcceptanceTest() && TimerExpired())
         {
            nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_FAIL;
         }

         break;
      case ACCEPTANCE_ASC_OR_DESC_OVERSPEED_WAIT_FOR_USER:
         // Press the MR down button to start the test. This button must be held.
         // DN is used since the car is going towards the top floor.

         //TODO: Should use two different states here instead of a flag variable
         if(GetInputValue( enIN_MRUP ) )
         {
            // Current floor must be 2 floors away from the top
            if( GetOperation_CurrentFloor() <= (GetFP_NumFloors()-2 ) )
            {
               ascendingOrDescending = TEST_ASCENDING;
               nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_TEST_RUNNING;
            }
            else
            {
               nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_FAIL;

            }
         }
         else if(GetInputValue( enIN_MRDN ))
         {
            // Current floor must be 2 floors away from the bottom
            if(GetOperation_CurrentFloor() >= 2)
            {
               ascendingOrDescending = TEST_DESCENDING;
               nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_TEST_RUNNING;
            }
            else
            {
               nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_FAIL;
            }

         }
         else if(!OnAcceptanceTest())
         {
            nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_FAIL;
         }
         break;
      case ACCEPTANCE_ASC_OR_DESC_OVERSPEED_TEST_RUNNING:

         if(ascendingOrDescending ==  TEST_ASCENDING)
         {
            MoveAutomatic((GetFP_NumFloors()-1));
            // Check if the governor fault tripped. If it did the test passed
            if ( GetFault_ByRange(FLT__GOV, FLT__GOV_L) )
            {
               nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_PASS;
                  if(!GetInputValue(enIN_MBC))
                  {
                      SetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY ,0);
                      SetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY   ,0);
                      SetAcceptanceContactorPickCommand(CONTACTOR_SEL__M ,0);
                      SetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2,0);
                }
                else
                {
                    SetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY ,1);
                    SetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY   ,1);
                    SetAcceptanceContactorPickCommand(CONTACTOR_SEL__M ,0);
                    SetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2,0);
                }
            }
            // Fail the test if on of three things happen
            // 1) A fault occured
            // 2) Car is at top floor
            // 3) User stops holding down button
            else if( !GetInputValue( enIN_MRUP )
                  || AtTopFloor()
            )
            {
               nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_FAIL;
            }
            else if(!OnAcceptanceTest())
            {
               nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_FAIL;
            }
         }
         else // TEST_DESENDING
         {
            MoveAutomatic(0);
            // Check if the governor fault tripped. If it did the test passed
            if ( GetFault_ByRange(FLT__GOV, FLT__GOV_L) )
            {
               nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_PASS;
                  if(!GetInputValue(enIN_MBC) || !GetInputValue(enIN_MBC2))
                  {
                      SetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY ,0);
                      SetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY   ,0);
                      SetAcceptanceContactorPickCommand(CONTACTOR_SEL__M ,0);
                      SetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2,0);
                }
                else
                {
                    SetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY ,1);
                    SetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY   ,1);
                    SetAcceptanceContactorPickCommand(CONTACTOR_SEL__M ,0);
                    SetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2,0);
                }
            }
            // Fail the test if on of three things happen
            // 1) A fault occured
            // 2) Car is at top floor
            // 3) User stops holding down button
            else if(  !GetInputValue( enIN_MRDN )
                  || AtBottomFloor()
            )
            {
               nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_FAIL;
            }
            else if(!OnAcceptanceTest())
            {
               nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_FAIL;
            }
         }

         break;
      case ACCEPTANCE_ASC_OR_DESC_OVERSPEED_PASS:
         //Show ebrake can hold
          if( TimeoutReached_AscDescOvsp() )
          {
              SetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY ,0);
              SetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY   ,0);
              SetAcceptanceContactorPickCommand(CONTACTOR_SEL__M ,0);
              SetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2,0);
          }
          else if(!GetInputValue(enIN_MBC2) && (ascendingOrDescending == TEST_DESCENDING))
          {
              SetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY ,0);
              SetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY   ,0);
              SetAcceptanceContactorPickCommand(CONTACTOR_SEL__M ,0);
              SetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2,0);
          }
          else
          {
             if(ascendingOrDescending == TEST_DESCENDING)
             {
                 SetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY, GetInputValue(enIN_MBC2));
             }
              SetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY, GetInputValue(enIN_MBC));
              SetAcceptanceContactorPickCommand(CONTACTOR_SEL__M ,0);
              SetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2,0);
          }
         if( TimeoutReached_AscDescOvsp() || ( !GetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY) && !GetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY ) ) )
         {
            SetAcceptanceTestStatus(ACCEPTANCE_TEST_ASC_OR_DESC_OVERSPEED,ACCEPTANCE_TEST_PASS);
            nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_PARAMETER_RESTORE;
         }
         break;
      case ACCEPTANCE_ASC_OR_DESC_OVERSPEED_FAIL:
         // Error so we want to stop the car ASAP
            SetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY ,0);
            SetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY   ,0);
            SetAcceptanceContactorPickCommand(CONTACTOR_SEL__M ,0);
            SetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2,0);
         if(TimerExtExpired())
         {
            SetAcceptanceTestStatus(ACCEPTANCE_TEST_ASC_OR_DESC_OVERSPEED,ACCEPTANCE_TEST_FAIL);
            nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_PARAMETER_RESTORE;
         }
         else
         {
            StopManual();
         }
         break;
      case ACCEPTANCE_ASC_OR_DESC_OVERSPEED_PARAMETER_RESTORE:
         if(paramSetup)
         {
            // Restore the contract speed parameter
            Param_WriteValue_16Bit(enPARAM16__ContractSpeed,contractSpeed);
            paramSetup = 0;
            ascendingOrDescending = TEST_NONE;
         }
         else if(TimerExpired() && NoActiveFaultOrAlarm())
         {
            SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
            SetAcceptanceTestStatus(ACCEPTANCE_TEST_ASC_OR_DESC_OVERSPEED,ACCEPTANCE_TEST_NOT_RUN);
            nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_IDLE;
         }
         break;
      default:
         StopManual();
         SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
         SetAcceptanceTestStatus(ACCEPTANCE_TEST_ASC_OR_DESC_OVERSPEED,ACCEPTANCE_TEST_NOT_RUN);
         nextState = ACCEPTANCE_ASC_OR_DESC_OVERSPEED_IDLE;
         break;
   }
    // Anytime the state is going to change the timer must be reset.
    if(currentState != nextState)
    {
        ResetTimer();
    }

    currentState = nextState;
    return currentState;
}

