#include "acceptance.h"
#include "mod.h"
#include "sys.h"
#include "operation.h"

#define TIMER_REDUCED_MAX_25MS                  (100)  // 2.5s
#define TIMER_MAX_25MS                          (200)  // 5s
#define TIMER_EXT_MAX_25MS                      (360)  // 9s
#define TIMEOUT_25MS                            (600)  // 15s
#define TIMEOUT_COUNTER_MAX_25MS                (1200) // 30s

#define ASC_DESC_OVSP_TIMEOUT_25MS              (1200) // 30s

static uint16_t timer_25ms = 0;
static uint16_t timerExt_25ms = 0;
static uint16_t uwTimeoutCounter_25ms = 0;
static uint8_t abAcceptancePickContactor[MAX_CONTACTOR_SEL];
static uint8_t abAcceptancePickBrake[MAX_BRAKE_SEL];

// Functions for working with the acceptance test timer
uint8_t TimerReducedExpired(void)
{
    return (timer_25ms >= TIMER_REDUCED_MAX_25MS);
}
uint8_t TimerExpired(void)
{
    return (timer_25ms >= TIMER_MAX_25MS);
}

uint8_t TimerExtExpired(void)
{
    return (timerExt_25ms >= TIMER_EXT_MAX_25MS);
}

uint8_t TimeoutReached( void )
{
  return (uwTimeoutCounter_25ms >= TIMEOUT_25MS);
}

uint8_t TimeoutReached_AscDescOvsp( void )
{
  return (uwTimeoutCounter_25ms >= ASC_DESC_OVSP_TIMEOUT_25MS);
}

void ResetTimer( void )
{
    timer_25ms = 0;
    timerExt_25ms = 0;
    uwTimeoutCounter_25ms = 0;
}

void IncrementTimer()
{
    // Saturate the timer_25ms if it expires
    timer_25ms = (timer_25ms >= TIMER_MAX_25MS) ? timer_25ms : timer_25ms + 1;
    timerExt_25ms = (timerExt_25ms >= TIMER_EXT_MAX_25MS) ? timerExt_25ms : timerExt_25ms + 1;
    uwTimeoutCounter_25ms = (uwTimeoutCounter_25ms >= TIMEOUT_COUNTER_MAX_25MS) ? uwTimeoutCounter_25ms : uwTimeoutCounter_25ms + 1;
}


// Helper functions for top and bottom floor.
// At the top floor and car stopped moving
 uint8_t AtTopFloor( void )
{
    return ((GetOperation_CurrentFloor() == GetFP_NumFloors()-1) && !GetMotion_RunFlag());
}

// At the top floor and car stopped moving
 uint8_t AtBottomFloor( void )
{
    return ( (GetOperation_CurrentFloor() == 0) && !GetMotion_RunFlag());
}


uint8_t NoActiveFaultOrAlarm( void )
{
    // 0 if alarm/fault not active
    // 1 if alarm/fault is active
    uint8_t activeAlarm = gstAlarm.bActiveAlarm;
    uint8_t activeFault = gstFault.bActiveFault;

    return (!activeAlarm && !activeFault);
}

/*
 * Check if the system is on acceptance test mode.
 * Two conditions must be met.
 * 1) Class-Op must be auto    TODO: Acceptance test should be semi auto mode
 * 2) Mode of pp must be test  TODO: Acceptance test should be MODE_A__ACCEPTANCE
 */
uint8_t OnAcceptanceTest( void )
{
    uint8_t result = 0;

    if( (GetOperation_AutoMode() == MODE_A__TEST ) && (GetOperation_ClassOfOp() == CLASSOP__AUTO))
    {
        result = 1;
    }
    return result;
}

/*----------------------------------------------------------------------------
 Clear all pick requests when test is canceled
 *----------------------------------------------------------------------------*/
void Acceptance_ClearAllPickCommands(void)
{
   abAcceptancePickContactor[CONTACTOR_SEL__M] = 0;
   abAcceptancePickContactor[CONTACTOR_SEL__B2] = 0;
   abAcceptancePickBrake[BRAKE_SEL__PRIMARY] = 0;
   abAcceptancePickBrake[BRAKE_SEL__EMERGENCY] = 0;

   SetAcceptanceTestBypassDecelFlag(0);
   SetETS_AcceptanceBypassFlag(0);
}
/*----------------------------------------------------------------------------
   For manually picking contactors during acceptance test
   0 = CONTACTOR_SEL__M
   1 = CONTACTOR_SEL__B2
 *----------------------------------------------------------------------------*/
void SetAcceptanceContactorPickCommand( ContactorSelect eContactorSelect, uint8_t bPick )
{
   if( eContactorSelect < MAX_CONTACTOR_SEL )
   {
      if( GetOperation_AutoMode() == MODE_A__TEST )
      {
         abAcceptancePickContactor[ eContactorSelect ] = bPick;
      }
   }
}
uint8_t GetAcceptanceContactorPickCommand( ContactorSelect eContactorSelect )
{
   uint8_t bPick = 0;
   if( eContactorSelect < MAX_CONTACTOR_SEL )
   {
      if( abAcceptancePickContactor[eContactorSelect]
      && ( GetOperation_AutoMode() == MODE_A__TEST ) )
      {
         bPick = 1;
      }
   }

   return bPick;
}

/*----------------------------------------------------------------------------
   For manually picking brake during acceptance test
   0 = Primary brake
   1 = secondary brake
 *----------------------------------------------------------------------------*/
void SetAcceptanceBrakePickCommand( BrakeSelect eBrakeSelect, uint8_t bPick )
{
   if( eBrakeSelect < MAX_BRAKE_SEL )
   {
      if( GetOperation_AutoMode() == MODE_A__TEST )
      {
         abAcceptancePickBrake[ eBrakeSelect ] = bPick;
      }
   }
}

/*----------------------------------------------------------------------------
   For manually picking brake during acceptance test
   0 = Primary brake
   1 = secondary brake
 *----------------------------------------------------------------------------*/
uint8_t GetAcceptanceBrakePickCommand( BrakeSelect eBrakeSelect )
{
   uint8_t bPick = 0;
   if( eBrakeSelect < MAX_BRAKE_SEL )
   {
      if( abAcceptancePickBrake[eBrakeSelect]
      && ( GetOperation_AutoMode() == MODE_A__TEST ) )
      {
         bPick = 1;
      }
   }

   return bPick;
}


/*----------------------------------------------------------------------------
The following handles control signals for the unintended movement test feature requested by NYC.
This feature allows testing of unintended movement in any mode of operation.
 *----------------------------------------------------------------------------*/
#define MAX_UNINTENDED_MOV_TEST_ACTIVE_TIME__MSEC        (5 * 60 * 1000) // 5min
uint8_t Test_UM_GetTestActiveFlag(void)
{
   uint8_t bTestActive = Param_ReadValue_1Bit(enPARAM1__TestUnintendedMovement) && SRU_Read_DIP_Switch(enSRU_DIP_B8);
   return bTestActive;
}
void Test_UM_UpdateControlSignals(uint16_t uwRunPeriod__1ms)
{
   static uint32_t uiCounter__ms;
   static uint8_t bLastTestActive;
   uint8_t bTestActive = Test_UM_GetTestActiveFlag();
   if(bTestActive)
   {
      SetAlarm(ALM__UNINTENDED_MOV_TEST_ACTIVE);
      if(bTestActive != bLastTestActive)
      {
         uiCounter__ms = 0;
      }

      if( uiCounter__ms >= MAX_UNINTENDED_MOV_TEST_ACTIVE_TIME__MSEC )
      {
         Param_WriteValue_1Bit(enPARAM1__TestUnintendedMovement, 0);
      }
      else
      {
         uiCounter__ms += uwRunPeriod__1ms;
      }
   }
   else
   {
      uiCounter__ms = 0;
   }

   bLastTestActive = bTestActive;
}
uint8_t Test_UM_GetPickBrakeFlag(void)
{
   uint8_t bPick = 0;
   uint8_t bTestActive = Test_UM_GetTestActiveFlag();
   if(bTestActive)
   {
      // Pick brake when mechanic manually picks the B contactor
      bPick = GetInputValue(enIN_MBC);
   }
   return bPick;
}
uint8_t Test_UM_GetPickEBrakeFlag(void)
{
   uint8_t bPick = 0;
   uint8_t bTestActive = Test_UM_GetTestActiveFlag();
   if(bTestActive)
   {
      // Pick ebrake when the B2 contactor has picked
      bPick = GetInputValue(enIN_MBC2);
   }
   return bPick;
}
uint8_t Test_UM_GetPickB2Flag(void)
{
   uint8_t bPick = 0;
   uint8_t bTestActive = Test_UM_GetTestActiveFlag();
   if(bTestActive)
   {
      // Pick as long as the test is active
      bPick = 1;
   }
   return bPick;
}
