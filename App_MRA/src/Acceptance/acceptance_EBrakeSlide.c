#include "acceptance.h"
#include "acceptanceTest.h"
#include "alarm_app.h"
#include "fault_app.h"
#include "mod.h"

#define MAX_ACCEL_TIME (500)

enum en_Test_Direction
{
    DIR_NONE,
    DIR_UP,
    DIR_DOWN
};

static enum en_Test_Direction enSlideDir;

static uint16_t uwAccelTimer;
static uint32_t uiPositionInitial;
static uint32_t uiPositionFinal;
static uint16_t uwSlideDistance;
//static uint32_t uiZeroPos;

static acceptance_EBrake_Slide_FSM currentState = ACCEPTANCE_ESLIDE_IDLE;
static acceptance_EBrake_Slide_FSM nextState = ACCEPTANCE_ESLIDE_IDLE;

void ResetAcceptanceEBrkSlide( void )
{
   SetAcceptanceTestStatus(ACCEPTANCE_TEST_EBRAKE_SLIDE,ACCEPTANCE_TEST_NOT_RUN);
   currentState = ACCEPTANCE_ESLIDE_IDLE;

   uwAccelTimer = 0;
   uiPositionInitial = 0;
   uiPositionFinal = 0;
   uwSlideDistance = 0;
}

uint8_t AcceptanceEBrkSlide( void )
{
    IncrementTimer();

    switch(currentState)
    {
        case ACCEPTANCE_ESLIDE_IDLE:
            if( (GetActiveAcceptanceTest() == ACCEPTANCE_TEST_EBRAKE_SLIDE) && OnAcceptanceTest())
            {
                SetAcceptanceTestStatus( ACCEPTANCE_TEST_EBRAKE_SLIDE, ACCEPTANCE_TEST_IN_PROGRESS );
                nextState = ACCEPTANCE_ESLIDE_CHOOSE_DIR;
            }
            uwAccelTimer = 0;
            break;
        case ACCEPTANCE_ESLIDE_CHOOSE_DIR:
            if( GetInputValue( enIN_MRDN ))
            {
                nextState = ACCEPTANCE_ESLIDE_MOVE_TO_TOP;
                enSlideDir = DIR_DOWN;
            }
            else if ( GetInputValue( enIN_MRUP ) )
            {
                nextState = ACCEPTANCE_ESLIDE_MOVE_TO_BOTTOM;
                enSlideDir = DIR_UP;
            }
            break;
        case ACCEPTANCE_ESLIDE_MOVE_TO_TOP:
            if( AtTopFloor() )
            {
                nextState = ACCEPTANCE_ESLIDE_WAIT_FOR_USER;
            }
            else if( !OnAcceptanceTest() )
            {
                nextState = ACCEPTANCE_ESLIDE_FAIL;
            }
            else
            {
                MoveAutomatic(GetFP_NumFloors() -1);
            }
            break;
        case ACCEPTANCE_ESLIDE_MOVE_TO_BOTTOM:
            if( AtBottomFloor() )
            {
                nextState = ACCEPTANCE_ESLIDE_WAIT_FOR_USER;
            }
            else if( !OnAcceptanceTest() )
            {
                nextState = ACCEPTANCE_ESLIDE_FAIL;
            }
            else
            {
                MoveAutomatic(0);
            }
            break;
        case ACCEPTANCE_ESLIDE_WAIT_FOR_USER:
            if(GetInputValue( enIN_MRDN ) && (enSlideDir == DIR_DOWN) )
            {
                if(TimerExpired())
                {
                    MoveAutomatic(0);
                    nextState = ACCEPTANCE_ESLIDE_GET_TO_SPEED;
                }
            }
            else if(GetInputValue( enIN_MRUP ) && (enSlideDir == DIR_UP) )
            {
                if(TimerExpired())
                {
                    MoveAutomatic(GetFP_NumFloors() - 1);
                    nextState = ACCEPTANCE_ESLIDE_GET_TO_SPEED;
                }
            }
            else if( !OnAcceptanceTest() )
            {
                nextState = ACCEPTANCE_SLIDE_FAIL;
            }
            break;
        case ACCEPTANCE_ESLIDE_GET_TO_SPEED:
            if( Motion_GetMotionState() == MOTION__CRUISING )
            {
                if(TimerExpired())
                {
                    nextState = ACCEPTANCE_ESLIDE_TRIP_EBRAKE;
                }
            }
            else if( uwAccelTimer++ > MAX_ACCEL_TIME )
            {
                nextState = ACCEPTANCE_ESLIDE_FAIL;
            }
            break;
        case ACCEPTANCE_ESLIDE_TRIP_EBRAKE:
            SetOperation_MotionCmd(MOCMD__EMERG_STOP);
            /* Allow picking of one brake and not the others to show the single brake can hold the car */
            uint8_t bPickMain = GetInputValue(enIN_MBC);
            uint8_t bPickSec = GetInputValue(enIN_MBC2);
            SetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY ,bPickSec);
            SetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY   ,bPickMain);
            SetAcceptanceContactorPickCommand(CONTACTOR_SEL__M ,0);
            SetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2,0);

            uiPositionInitial = GetPosition_PositionCount();
            nextState = ACCEPTANCE_ESLIDE_MEASURE_SLIDE;
            break;
        case ACCEPTANCE_ESLIDE_MEASURE_SLIDE:
            if(TimeoutReached() )
            {
                SetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY ,0);
                SetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY   ,0);
                SetAcceptanceContactorPickCommand(CONTACTOR_SEL__M ,0);
                SetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2,0);
                nextState = ACCEPTANCE_ESLIDE_FAIL;
            }
            else
            {
                /* Allow picking of one brake and not the others to show the single brake can hold the car */
               uint8_t bPickMain = GetInputValue(enIN_MBC);
               uint8_t bPickSec = GetInputValue(enIN_MBC2);
                SetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY ,bPickSec);
                SetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY   ,bPickMain);
                SetAcceptanceContactorPickCommand(CONTACTOR_SEL__M ,0);
                SetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2,0);
            }

            if(GetPosition_Velocity() == 0)
            {
                uiPositionFinal = GetPosition_PositionCount();
                if(enSlideDir == DIR_DOWN)
                {
                    uwSlideDistance = uiPositionInitial - uiPositionFinal;
                }
                else if(enSlideDir == DIR_UP)
                {
                    uwSlideDistance = uiPositionFinal - uiPositionInitial;
                }
                
                Param_WriteValue_16Bit(enPARAM16__Acceptance_EBrk_SlideDistance, uwSlideDistance);
                nextState = ACCEPTANCE_ESLIDE_PASS;
            }
            break;
        case ACCEPTANCE_ESLIDE_PASS:
            if(TimerExtExpired())
            {
               /* Allow picking of one brake and not the others to show the single brake can hold the car */
               uint8_t bPickMain = GetInputValue(enIN_MBC);
               uint8_t bPickSec = GetInputValue(enIN_MBC2);
               SetAcceptanceBrakePickCommand(BRAKE_SEL__EMERGENCY ,bPickSec);
               SetAcceptanceBrakePickCommand(BRAKE_SEL__PRIMARY   ,bPickMain);
               SetAcceptanceContactorPickCommand(CONTACTOR_SEL__M ,0);
               SetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2,0);

               SetAcceptanceTestStatus(ACCEPTANCE_TEST_EBRAKE_SLIDE,ACCEPTANCE_TEST_PASS);
               nextState = ACCEPTANCE_ESLIDE_COMPLETE;
            }
            break;
        case ACCEPTANCE_ESLIDE_FAIL:
           if(TimerExtExpired())
           {
              SetAcceptanceTestStatus(ACCEPTANCE_TEST_EBRAKE_SLIDE,ACCEPTANCE_TEST_FAIL);
              nextState = ACCEPTANCE_ESLIDE_COMPLETE;
           }
           break;
        case ACCEPTANCE_ESLIDE_COMPLETE:
            if(TimerExpired())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_EBRAKE_SLIDE, ACCEPTANCE_TEST_NOT_RUN);
                SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
                nextState = ACCEPTANCE_ESLIDE_IDLE;
            }
            Acceptance_ClearAllPickCommands();
            break;
        default:
          SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
          SetAcceptanceTestStatus(ACCEPTANCE_TEST_EBRAKE_SLIDE,ACCEPTANCE_TEST_NOT_RUN);
          break;
    }

    if(currentState != nextState)
    {
        ResetTimer();
    }

    currentState = nextState;
    return currentState;
}
