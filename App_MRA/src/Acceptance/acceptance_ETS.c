#include "acceptance.h"
#include "acceptanceTest.h"
#include "alarm_app.h"
#include "fault_app.h"
#include "mod.h"

static acceptance_ETS_FSM currentState = ACCEPTANCE_ETS_IDLE;
static acceptance_ETS_FSM nextState = ACCEPTANCE_ETS_IDLE;

static uint8_t paramSetup = 0;

void ResetAcceptanceETS( void )
{
	Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 0);
	Param_WriteValue_1Bit( enPARAM1__RelevelEnabled  , 1);
	SetAcceptanceTestBypassDecelFlag(0);
	SetETS_AcceptanceTest(0);
	paramSetup = 0;

	SetAcceptanceTestStatus(ACCEPTANCE_TEST_ETS,ACCEPTANCE_TEST_NOT_RUN);

	currentState = ACCEPTANCE_ETS_IDLE;
	nextState = currentState;
}

// This test the down ETS. If at contract speed and pass the ETS position the
// system issues an emergency stop. The user must remove the NTS wire.
// TODO: better method would be to send the pattern to a higher position
uint8_t AcceptanceETS( void )
{
    IncrementTimer();

    switch(currentState)
    {
        case ACCEPTANCE_ETS_IDLE:
            if((GetActiveAcceptanceTest() == ACCEPTANCE_TEST_ETS) && OnAcceptanceTest())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_ETS,ACCEPTANCE_TEST_IN_PROGRESS);
                nextState = ACCEPTANCE_ETS_IN_DZ;
            }
            break;
        case ACCEPTANCE_ETS_IN_DZ:
            if(TimerExpired())
            {
                if(( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R)  ))
                {
                    nextState = ACCEPTANCE_ETS_SELECT_TYPE;
                }
                else
                {
                    nextState = ACCEPTANCE_ETS_FAIL;
                }

            }
            else if(!NoActiveFaultOrAlarm())
            {
                nextState = ACCEPTANCE_ETS_FAIL;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_ETS_FAIL;
            }
            break;
        case ACCEPTANCE_ETS_SELECT_TYPE:
            if(GetInputValue( enIN_MRUP ))
            {
                nextState = ACCEPTANCE_ETS_PARAMETER_SETUP_UETS;
            }
            else if(GetInputValue( enIN_MRDN ))
            {
                nextState = ACCEPTANCE_ETS_PARAMETER_SETUP_DETS;
            }
            break;
        case ACCEPTANCE_ETS_PARAMETER_SETUP_UETS:
            if(!paramSetup)
            {
			   //Need both of these to compleatly bypass decel, bypass dcel and bypass NTS
			    SetAcceptanceTestBypassDecelFlag(1);
			    Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 1);
			    Param_WriteValue_1Bit( enPARAM1__RelevelEnabled  , 0);
			    SetETS_AcceptanceBypassFlag(0);
			    SetETS_AcceptanceTest(1);

			    paramSetup = 1;
            }
            else if(TimerExpired())
            {
                nextState = ACCEPTANCE_ETS_WAIT_FOR_USER_UETS;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_ETS_FAIL;
            }
            break;
        case ACCEPTANCE_ETS_PARAMETER_SETUP_DETS:
            if(!paramSetup)
            {
			   SetAcceptanceTestBypassDecelFlag(1);
			   // This bypasses NTS
			   Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 1);
			   Param_WriteValue_1Bit( enPARAM1__RelevelEnabled  , 0);
			   SetETS_AcceptanceBypassFlag(0);
			   SetETS_AcceptanceTest(1);

			   paramSetup = 1;
            }
            else if(TimerExpired())
            {
                nextState = ACCEPTANCE_ETS_WAIT_FOR_USER_DETS;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_ETS_FAIL;
            }
            break;
        case ACCEPTANCE_ETS_WAIT_FOR_USER_UETS:
            // USer has to press the
            if(GetInputValue( enIN_MRUP ))
            {
               nextState = ACCEPTANCE_ETS_TEST_RUNNING_UETS;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_ETS_FAIL;
            }
            break;
        case ACCEPTANCE_ETS_WAIT_FOR_USER_DETS:
            // User has to press the
            if(GetInputValue( enIN_MRDN ))
            {
               nextState = ACCEPTANCE_ETS_TEST_RUNNING_DETS;
            }
            else if (!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_ETS_FAIL;
            }
            break;
        case ACCEPTANCE_ETS_TEST_RUNNING_UETS:
            MoveAutomatic((GetFP_NumFloors()-1));
            // Did  UETS fault happen?
            if ( GetFault_ByRange(FLT__OVERSPEED_UETS_1, FLT__OVERSPEED_UETS_8) )
            {
                nextState = ACCEPTANCE_ETS_PASS;
            }
            // Fail the test if on of three things happen
            // 1) User lets go of the dn switch
            // 2) A fault occured
            // 3) Car is no longer moving
            else if( !GetInputValue( enIN_MRUP ) )
            {
                nextState = ACCEPTANCE_ETS_FAIL;
            }
            else if (!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_ETS_FAIL;
            }
            break;
        case ACCEPTANCE_ETS_TEST_RUNNING_DETS:
            MoveAutomatic(0);
            // Did D ETS fault happen?
            if ( GetFault_ByRange(FLT__OVERSPEED_DETS_1, FLT__OVERSPEED_DETS_8) )
            {
                nextState = ACCEPTANCE_ETS_PASS;
            }
            // Fail the test if on of three things happen
            // 1) User lets go of the dn switch
            // 2) A fault occured
            // 3) Car is no longer moving
            //else if( (GetFault_ByNode( enFAULT_NODE__MRA ) != FLT__NONE) || !GetInputValue( enIN_MRDN ) )
            else if( !GetInputValue( enIN_MRDN ) )
            {
                nextState = ACCEPTANCE_ETS_FAIL;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_ETS_FAIL;
            }
            break;
        case ACCEPTANCE_ETS_PASS:
            if(TimerExtExpired())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_ETS,ACCEPTANCE_TEST_PASS);
                nextState = ACCEPTANCE_ETS_PARAMETER_RESTORE;
            }

            break;
        case ACCEPTANCE_ETS_FAIL:
            if(TimerExtExpired())
            {
                // Error so we want to stop the car ASAP
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_ETS,ACCEPTANCE_TEST_FAIL);
                nextState = ACCEPTANCE_ETS_PARAMETER_RESTORE;
            }
            else
            {
                StopManual();
            }
            break;
        case ACCEPTANCE_ETS_PARAMETER_RESTORE:
            if(paramSetup)
            {
                SetAcceptanceTestBypassDecelFlag(0);
                Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 0);
                Param_WriteValue_1Bit( enPARAM1__RelevelEnabled  , 1);
                SetETS_AcceptanceTest(0);
                paramSetup = 0;
            }
            else if(TimerExpired())
            {
            	MoveAutomatic(GetClosestValidOpening(GetMotion_Direction(), DOOR_ANY));
                SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_ETS,ACCEPTANCE_TEST_NOT_RUN);
                nextState = ACCEPTANCE_ETS_IDLE;
            }
            break;

        default:
            StopManual();
            SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
            SetAcceptanceTestStatus(ACCEPTANCE_TEST_ETS,ACCEPTANCE_TEST_NOT_RUN);
            nextState = ACCEPTANCE_ETS_IDLE;
            break;
    }
    // Anytime the state is going to change the timer must be reset.
    if(currentState != nextState)
    {
        ResetTimer();
    }

    currentState = nextState;
    return currentState;
}
