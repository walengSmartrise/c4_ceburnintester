#include "acceptance.h"
#include "acceptanceTest.h"
#include "alarm_app.h"
#include "fault_app.h"
#include "mod.h"

static acceptance_TopLimit_FSM currentState = ACCEPTANCE_TOP_LIMIT_IDLE;
static acceptance_TopLimit_FSM nextState = ACCEPTANCE_TOP_LIMIT_IDLE;

static uint8_t paramSetup = 0;

void ResetAcceptanceTopLimit( void )
{
    SetAcceptanceTestStatus(ACCEPTANCE_TEST_TOP_LIMIT,ACCEPTANCE_TEST_NOT_RUN);

    currentState = ACCEPTANCE_TOP_LIMIT_IDLE;
    nextState = currentState;
}

uint8_t AcceptanceTopLimit( void )
{
	IncrementTimer();

    switch(currentState)
    {
        case ACCEPTANCE_TOP_LIMIT_IDLE:
            if( (GetActiveAcceptanceTest() == ACCEPTANCE_TEST_TOP_LIMIT) && OnAcceptanceTest())
            {
                // Go to the bottom floor
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_TOP_LIMIT,ACCEPTANCE_TEST_IN_PROGRESS);
                nextState = ACCEPTANCE_TOP_LIMIT_MOVE_TO_TOP;
            }
            break;
        case ACCEPTANCE_TOP_LIMIT_MOVE_TO_TOP:
            // Wait till at the bottom floor
            if( AtTopFloor() && TimerExpired())
            {
                nextState = ACCEPTANCE_TOP_LIMIT_PARAMETER_SETUP;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_TOP_LIMIT_FAIL;
            }
            else
            {
                MoveAutomatic((GetFP_NumFloors()-1));
            }
            break;
        case ACCEPTANCE_TOP_LIMIT_PARAMETER_SETUP:
            if(!paramSetup)
            {
//                SetAcceptanceTestBypassDecelFlag(1);
                paramSetup = 1;
            }
            else if(TimerExpired())
            {
                nextState = ACCEPTANCE_TOP_LIMIT_USER_PRESS_UP;
            }
            break;
        case ACCEPTANCE_TOP_LIMIT_USER_PRESS_UP:
            // User needs to press the dn button to start the test
            if(GetInputValue( enIN_MRUP ))
            {
                nextState = ACCEPTANCE_TOP_LIMIT_TEST_RUNNING;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_TOP_LIMIT_FAIL;
            }
            break;
        case ACCEPTANCE_TOP_LIMIT_TEST_RUNNING:
            // Keep asserting a move up command as long as the up switch is pressed.
            // If the user lets go then immediately issue a fail.
            if(GetInputValue( enIN_MRUP ))
            {
                MoveUpManual();
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_TOP_LIMIT_FAIL;
            }
            else
            {
                nextState = ACCEPTANCE_TOP_LIMIT_FAIL;
            }

            // Tests passes if a position limit fault is issued.
            if ( GetFault_ByNumber(FLT__POSITION_LIMIT) )
            {
                nextState = ACCEPTANCE_TOP_LIMIT_PASS;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_TOP_LIMIT_FAIL;
            }
            break;
        case ACCEPTANCE_TOP_LIMIT_PASS:
            if(TimerExtExpired())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_TOP_LIMIT,ACCEPTANCE_TEST_PASS);
                nextState = ACCEPTANCE_TOP_LIMIT_PARAMETER_RESTORE;
            }
            else
            {
                StopManual();
            }
            break;
        case ACCEPTANCE_TOP_LIMIT_FAIL:
            if(TimerExtExpired())
            {

                SetAcceptanceTestStatus(ACCEPTANCE_TEST_TOP_LIMIT,ACCEPTANCE_TEST_FAIL);
                nextState = ACCEPTANCE_TOP_LIMIT_PARAMETER_RESTORE;
            }
            else
            {
                StopManual();
            }

            break;
        case ACCEPTANCE_TOP_LIMIT_PARAMETER_RESTORE:
            if(paramSetup)
            {
                StopManual();
               // Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 0 );
                //SetAcceptanceTestBypassDecelFlag(0);
                paramSetup = 0;
            }
            else if(TimerExpired())
            {
                SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_TOP_LIMIT,ACCEPTANCE_TEST_NOT_RUN);
                nextState = ACCEPTANCE_TOP_LIMIT_IDLE;
            }
            break;
        default:
            // Default to the idle state. Command an E-Stop
            StopManual();
            SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
            SetAcceptanceTestStatus(ACCEPTANCE_TEST_TOP_LIMIT,ACCEPTANCE_TEST_NOT_RUN);
            ResetTimer();
            nextState = ACCEPTANCE_TOP_LIMIT_IDLE;
            break;
    }
    // Anytime the state is going to change the timer must be reset.
    if(currentState != nextState)
    {
        ResetTimer();
    }

    currentState = nextState;
    return currentState;
}
