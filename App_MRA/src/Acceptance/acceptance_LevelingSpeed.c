#include "acceptance.h"
#include "acceptanceTest.h"
#include "alarm_app.h"
#include "fault_app.h"
#include "mod.h"

static acceptance_LevelingSpeed_FSM currentState = ACCEPTANCE_LEVELING_SPEED_IDLE;
static acceptance_LevelingSpeed_FSM nextState =  ACCEPTANCE_LEVELING_SPEED_IDLE;
static uint8_t paramSetup = 0;
static uint16_t levelingSpeed;

void ResetAcceptanceLevelingSpeed( void )
{
    levelingSpeed = (levelingSpeed >= MAX_LEVELING_SPEED) ? 20 : levelingSpeed;
    Param_WriteValue_16Bit(enPARAM16__LevelingSpeed,levelingSpeed);
	paramSetup = 0;

    SetAcceptanceTestStatus(ACCEPTANCE_TEST_LEVELING_SPEED,ACCEPTANCE_TEST_NOT_RUN);
    currentState = ACCEPTANCE_LEVELING_SPEED_IDLE;
    nextState = currentState;
}

uint8_t AcceptanceLevelingSpeed( void )
{
    static acceptance_LevelingSpeed_FSM currentState;
    acceptance_LevelingSpeed_FSM nextState = currentState;
    static uint8_t paramSetup = 0;
    static uint16_t levelingSpeed;

    IncrementTimer();

    if(  !OnAcceptanceTest() && paramSetup )
    {
        currentState = ACCEPTANCE_LEVELING_SPEED_PARAMETER_RESTORE;
    }
    else if( !OnAcceptanceTest() && !paramSetup)
    {
        SetAcceptanceTestStatus(ACCEPTANCE_TEST_LEVELING_SPEED,ACCEPTANCE_TEST_NOT_RUN);
        SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
    }

    switch(currentState)
    {
        case ACCEPTANCE_LEVELING_SPEED_IDLE:
            if( (GetActiveAcceptanceTest() == ACCEPTANCE_TEST_LEVELING_SPEED) && OnAcceptanceTest())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_LEVELING_SPEED,ACCEPTANCE_TEST_IN_PROGRESS);
                nextState = ACCEPTANCE_LEVELING_SPEED_PARAMETER_SETUP;
            }
            break;
        case ACCEPTANCE_LEVELING_SPEED_PARAMETER_SETUP:

            if(!paramSetup)
            {
                // Save the current inspection speed.
                levelingSpeed = Param_ReadValue_16Bit(enPARAM16__LevelingSpeed);
                // Set the inspection speed to be the max false plus 1fpm. this
                Param_WriteValue_16Bit(enPARAM16__LevelingSpeed,MAX_LEVELING_SPEED+1);
                paramSetup = 1;
            }
            else if(TimerExpired())
            {
                nextState = ACCEPTANCE_LEVELING_SPEED_TEST_RUNNING;
            }
            else if( !OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_LEVELING_SPEED_FAIL;
            }

            break;
        case ACCEPTANCE_LEVELING_SPEED_TEST_RUNNING:
            // If the timer expires then this test fails. If the invalid inspection speed
            // fault is issued this test passes.
            if ( GetFault_ByNumber(FLT__INVALID_LEVEL_SPD) )
            {
                nextState = ACCEPTANCE_LEVELING_SPEED_PASS;
            }
            else if( TimerExpired() )
            {
                nextState = ACCEPTANCE_LEVELING_SPEED_FAIL;
            }
            else if( !OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_LEVELING_SPEED_FAIL;
            }
            break;
        case ACCEPTANCE_LEVELING_SPEED_PASS:
            if(TimerExtExpired())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_LEVELING_SPEED,ACCEPTANCE_TEST_PASS);
                nextState = ACCEPTANCE_LEVELING_SPEED_PARAMETER_RESTORE;
            }
            break;
        case ACCEPTANCE_LEVELING_SPEED_FAIL:
            if(TimerExtExpired())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_LEVELING_SPEED,ACCEPTANCE_TEST_FAIL);
                nextState = ACCEPTANCE_LEVELING_SPEED_PARAMETER_RESTORE;
            }
            else
            {
                StopManual();
            }
            // Error so we want to stop the car ASAP

            break;
        case ACCEPTANCE_LEVELING_SPEED_PARAMETER_RESTORE:
            if(paramSetup)
            {
                // If the system stored a value greater than the max speed then the leveling speed
                // will be restored to 20 fpm.
                levelingSpeed = (levelingSpeed >= MAX_LEVELING_SPEED) ? 20 : levelingSpeed;
                Param_WriteValue_16Bit(enPARAM16__LevelingSpeed,levelingSpeed);
                paramSetup = 0;
            }
            else if(TimerExpired())
            {
                SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_LEVELING_SPEED,ACCEPTANCE_TEST_NOT_RUN);
                nextState = ACCEPTANCE_LEVELING_SPEED_IDLE;
            }
            break;
        default:
            StopManual();
            SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
            SetAcceptanceTestStatus(ACCEPTANCE_TEST_LEVELING_SPEED,ACCEPTANCE_TEST_NOT_RUN);
            nextState = ACCEPTANCE_LEVELING_SPEED_IDLE;
            break;
    }
    // Anytime the state is going to change the timer must be reset.
    if(currentState != nextState)
    {
        ResetTimer();
    }

    currentState = nextState;
    return currentState;
}
