#include "acceptance.h"
#include "acceptanceTest.h"
#include "alarm_app.h"
#include "fault_app.h"
#include "mod.h"

static acceptance_ETSL_FSM currentState = ACCEPTANCE_ETSL_IDLE;
static acceptance_ETSL_FSM nextState = ACCEPTANCE_ETSL_IDLE;
static uint8_t paramSetup = 0;

void ResetAcceptanceETSL( void )
{
	SetAcceptanceTestBypassDecelFlag(0);
	Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 0);
	Param_WriteValue_1Bit( enPARAM1__RelevelEnabled  , 1);
	paramSetup = 0;

    SetAcceptanceTestStatus(ACCEPTANCE_TEST_ETSL,ACCEPTANCE_TEST_NOT_RUN);
    currentState = ACCEPTANCE_ETSL_IDLE;
    nextState = currentState;
}
// This test the down ETSL. If at contract speed and pass the ETSL position the
// system issues an emergency stop. The user must remove the NTS wire.
// TODO: better method would be to send the pattern to a higher position
uint8_t AcceptanceETSL( void )
{
    IncrementTimer();

    switch(currentState)
    {
        case ACCEPTANCE_ETSL_IDLE:
            if((GetActiveAcceptanceTest() == ACCEPTANCE_TEST_ETSL) && OnAcceptanceTest())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_ETSL,ACCEPTANCE_TEST_IN_PROGRESS);
                nextState = ACCEPTANCE_ETSL_IN_DZ;
            }
            break;
        case ACCEPTANCE_ETSL_IN_DZ:
            if(TimerExpired())
            {
                if(( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R)  ))
                {
                    nextState = ACCEPTANCE_ETSL_SELECT_TYPE;
                }
                else
                {
                    nextState = ACCEPTANCE_ETSL_FAIL;
                }

            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_ETSL_FAIL;
            }
            break;
        case ACCEPTANCE_ETSL_SELECT_TYPE:
            if(GetInputValue( enIN_MRUP ))
            {
                nextState = ACCEPTANCE_ETSL_PARAMETER_SETUP_UETSL;
            }
            else if(GetInputValue( enIN_MRDN ))
            {
                nextState = ACCEPTANCE_ETSL_PARAMETER_SETUP_DETSL;
            }
            break;
        case ACCEPTANCE_ETSL_PARAMETER_SETUP_UETSL:
            if(!paramSetup)
            {
                   //Need both of these to compleatly bypass decel, bypass dcel and bypass NTS

                   SetAcceptanceTestBypassDecelFlag(1);
                   Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 1);
                   Param_WriteValue_1Bit( enPARAM1__RelevelEnabled  , 0);
                   SetETS_AcceptanceBypassFlag(0);
                   paramSetup = 1;
            }
            else if(TimerExpired())
            {
                nextState = ACCEPTANCE_ETSL_WAIT_FOR_USER_UETSL;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_ETSL_FAIL;
            }
            break;
        case ACCEPTANCE_ETSL_PARAMETER_SETUP_DETSL:
            if(!paramSetup)
            {
			   //SetAcceptanceTestBypassDecelFlag(1);
			   // This bypasses NTS
			   Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 1);
			   Param_WriteValue_1Bit( enPARAM1__RelevelEnabled  , 0);
			   SetETS_AcceptanceBypassFlag(0);

			   paramSetup = 1;
            }
            else if(TimerExpired())
            {
                nextState = ACCEPTANCE_ETSL_WAIT_FOR_USER_DETSL;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_ETSL_FAIL;
            }
            break;
        case ACCEPTANCE_ETSL_WAIT_FOR_USER_UETSL:
            // USer has to press the
            if(GetInputValue( enIN_MRUP ))
            {
               nextState = ACCEPTANCE_ETSL_TEST_RUNNING_UETSL;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_ETSL_FAIL;
            }
            break;
        case ACCEPTANCE_ETSL_WAIT_FOR_USER_DETSL:
            // User has to press the
            if(GetInputValue( enIN_MRDN ))
            {
               nextState = ACCEPTANCE_ETSL_TEST_RUNNING_DETSL;
            }
            else if (!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_ETSL_FAIL;
            }
            break;
        case ACCEPTANCE_ETSL_TEST_RUNNING_UETSL:
            MoveAutomatic((GetFP_NumFloors()-1));
            // Did  UETSL fault happen?
            if ( GetFault_ByRange(FLT__OVERSPEED_UETSL_1, FLT__OVERSPEED_UETSL_8) )
            {
                nextState = ACCEPTANCE_ETSL_PASS;
            }
            // Fail the test if on of three things happen
            // 1) User lETSL go of the dn switch
            // 2) A fault occured
            // 3) Car is no longer moving
            else if( !GetInputValue( enIN_MRUP ) )
            {
                nextState = ACCEPTANCE_ETSL_FAIL;
            }
            else if (!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_ETSL_FAIL;
            }
            break;
        case ACCEPTANCE_ETSL_TEST_RUNNING_DETSL:
            MoveAutomatic(0);
            // Did D ETSL fault happen?
            if ( GetFault_ByRange(FLT__OVERSPEED_DETSL_1, FLT__OVERSPEED_DETSL_8) )
            {
                nextState = ACCEPTANCE_ETSL_PASS;
            }
            // Fail the test if on of three things happen
            // 1) User lets go of the dn switch
            // 2) A fault occured
            // 3) Car is no longer moving
            //else if( (GetFault_ByNode( enFAULT_NODE__MRA ) != FLT__NONE) || !GetInputValue( enIN_MRDN ) )
            else if( !GetInputValue( enIN_MRDN ) )
            {
                nextState = ACCEPTANCE_ETSL_FAIL;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_ETSL_FAIL;
            }
            break;
        case ACCEPTANCE_ETSL_PASS:
            if(TimerExtExpired())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_ETSL,ACCEPTANCE_TEST_PASS);
                nextState = ACCEPTANCE_ETSL_PARAMETER_RESTORE;
            }

            break;
        case ACCEPTANCE_ETSL_FAIL:
            if(TimerExtExpired())
            {
                // Error so we want to stop the car ASAP
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_ETSL,ACCEPTANCE_TEST_FAIL);
                nextState = ACCEPTANCE_ETSL_PARAMETER_RESTORE;
            }
            else
            {
                StopManual();
            }
            break;
        case ACCEPTANCE_ETSL_PARAMETER_RESTORE:
            if(paramSetup)
            {
                SetAcceptanceTestBypassDecelFlag(0);
                Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 0);
                Param_WriteValue_1Bit( enPARAM1__RelevelEnabled  , 1);
                paramSetup = 0;
            }
            else if(TimerExpired())
            {
            	MoveAutomatic(GetClosestValidOpening(GetMotion_Direction(), DOOR_ANY));
                SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_ETSL,ACCEPTANCE_TEST_NOT_RUN);
                nextState = ACCEPTANCE_ETSL_IDLE;
            }
            break;

        default:
            StopManual();
            SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
            SetAcceptanceTestStatus(ACCEPTANCE_TEST_ETSL,ACCEPTANCE_TEST_NOT_RUN);
            nextState = ACCEPTANCE_ETSL_IDLE;
            break;
    }
    // Anytime the state is going to change the timer must be reset.
    if(currentState != nextState)
    {
        ResetTimer();
    }

    currentState = nextState;
    return currentState;
}
