#include "acceptance.h"
#include "acceptanceTest.h"
#include "alarm_app.h"
#include "fault_app.h"
#include "mod.h"

static uint8_t paramSetup = 0;

static acceptance_NTS_FSM currentState = ACCEPTANCE_NTS_IDLE;
static acceptance_NTS_FSM nextState = ACCEPTANCE_NTS_IDLE;

void ResetAcceptanceNTS( void )
{
    SetAcceptanceTestBypassDecelFlag(0);
    SetETS_AcceptanceBypassFlag(0);

    paramSetup = 0;
    SetAcceptanceTestStatus(ACCEPTANCE_TEST_NTS,ACCEPTANCE_TEST_NOT_RUN);

    currentState = ACCEPTANCE_NTS_IDLE;
    nextState = currentState;
}
uint8_t AcceptanceNTS( void )
{
    IncrementTimer();

    if( !OnAcceptanceTest() && paramSetup)
    {
        currentState = ACCEPTANCE_NTS_PARAMETER_RESTORE;
    }
    else if( !OnAcceptanceTest() && !paramSetup)
    {
        SetAcceptanceTestStatus(ACCEPTANCE_TEST_NTS,ACCEPTANCE_TEST_NOT_RUN);
        SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
    }

    switch(currentState)
    {
        case ACCEPTANCE_NTS_IDLE:
            if( (GetActiveAcceptanceTest() == ACCEPTANCE_TEST_NTS) && OnAcceptanceTest())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_NTS,ACCEPTANCE_TEST_IN_PROGRESS);
                nextState = ACCEPTANCE_NTS_IN_DZ;
            }
            break;

        case ACCEPTANCE_NTS_IN_DZ:
            if(TimerExpired())
            {
                if(( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R)  ))
                {
                    nextState = ACCEPTANCE_NTS_SELECT_TYPE;
                }
                else
                {
                    nextState = ACCEPTANCE_NTS_FAIL;
                }

            }
            else if(GetNTS_StopFlag_CTB() || gstFault.bActiveFault)
            {
                nextState = ACCEPTANCE_NTS_FAIL;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_NTS_FAIL;
            }
            break;

        case ACCEPTANCE_NTS_SELECT_TYPE:
            if(GetInputValue( enIN_MRUP ))
            {
                nextState = ACCEPTANCE_NTS_PARAMETER_SETUP_UNTS;
            }
            else if(GetInputValue( enIN_MRDN ))
            {
                nextState = ACCEPTANCE_NTS_PARAMETER_SETUP_DNTS;
            }
            break;

        case ACCEPTANCE_NTS_PARAMETER_SETUP_UNTS:
            // set up the parameters the first time this module is run.
            // After wait for the timer to expire.
            if(!paramSetup)
            {
                SetAcceptanceTestBypassDecelFlag(0);
                SetETS_AcceptanceBypassFlag(1);
                paramSetup = 1;
            }
            // Wait for faults to clear.
            else if(TimerExpired() && (!gstFault.bActiveFault) && (!GetNTS_StopFlag_CTB())) // check for an active fault
            {
                nextState = ACCEPTANCE_NTS_TEST_RUNNING_UNTS;
            }
            break;

        case ACCEPTANCE_NTS_PARAMETER_SETUP_DNTS:
            if(!paramSetup)
            {
                SetAcceptanceTestBypassDecelFlag(0);
                SetETS_AcceptanceBypassFlag(1);
                paramSetup = 1;
            }
            else if(TimerExpired() && (!gstFault.bActiveFault) && (!GetNTS_StopFlag_CTB())) // check for an active fault
            {
                nextState = ACCEPTANCE_NTS_TEST_RUNNING_DNTS;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_NTS_FAIL;
            }
            break;

        case ACCEPTANCE_NTS_TEST_RUNNING_UNTS:
            MoveAutomatic((GetFP_NumFloors()-1));
            if (GetNTS_StopFlag_CTB())
            {
                nextState = ACCEPTANCE_NTS_MAYBE_PASS_UNTS;
            }
            // Fail the test if on of three things happen
            // 1) Test timeout expired
            // 2) A fault occured
            // 3) Car is no longer moving
            else if( !GetMotion_RunFlag() && TimerExpired())
            {
                nextState = ACCEPTANCE_NTS_FAIL;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_NTS_FAIL;
            }
            break;

        case ACCEPTANCE_NTS_TEST_RUNNING_DNTS:
            MoveAutomatic(0);
            if (GetNTS_StopFlag_CTB())
            {
                nextState = ACCEPTANCE_NTS_MAYBE_PASS_DNTS;
            }
            // Fail the test if on of three things happen
            // 1) Test timeout expired
            // 2) A fault occured
            // 3) Car is no longer moving
            else if( !GetMotion_RunFlag() && TimerExpired())
            {
                nextState = ACCEPTANCE_NTS_FAIL;
            }
            else if(!OnAcceptanceTest())
            {
                nextState = ACCEPTANCE_NTS_FAIL;
            }
            break;

        case ACCEPTANCE_NTS_MAYBE_PASS_UNTS:
        	StopNextAvailableLanding();
            if(!GetMotion_RunFlag())
            {
                nextState = ACCEPTANCE_NTS_PASS;
            }
            else if(!OnAcceptanceTest())
            {
            	StopManual();
                nextState = ACCEPTANCE_NTS_FAIL;
            }
            break;
        case ACCEPTANCE_NTS_MAYBE_PASS_DNTS:
        	StopNextAvailableLanding();
            if(!GetMotion_RunFlag())
            {
                nextState = ACCEPTANCE_NTS_PASS;
            }
            else if(!OnAcceptanceTest())
            {
            	StopManual();
                nextState = ACCEPTANCE_NTS_FAIL;
            }
            break;
        case ACCEPTANCE_NTS_PASS:
            if(TimerExtExpired())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_NTS,ACCEPTANCE_TEST_PASS);
                nextState = ACCEPTANCE_NTS_PARAMETER_RESTORE;
            }
            break;

        case ACCEPTANCE_NTS_FAIL:
            if(TimerExtExpired())
            {
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_NTS,ACCEPTANCE_TEST_FAIL);
                nextState = ACCEPTANCE_NTS_PARAMETER_RESTORE;
            }
            else
            {
                StopManual();
            }
            break;

        case ACCEPTANCE_NTS_PARAMETER_RESTORE:
            if(paramSetup)
            {
                SetAcceptanceTestBypassDecelFlag(0);
                SetETS_AcceptanceBypassFlag(0);

                paramSetup = 0;
            }
            else if(TimerExpired())
            {
                SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
                SetAcceptanceTestStatus(ACCEPTANCE_TEST_NTS,ACCEPTANCE_TEST_NOT_RUN);
                nextState = ACCEPTANCE_NTS_IDLE;
            }
            break;
        
        default:
            StopManual();
            SetActiveAcceptanceTest(NO_ACTIVE_ACCEPTANCE_TEST);
            SetAcceptanceTestStatus(ACCEPTANCE_TEST_NTS,ACCEPTANCE_TEST_NOT_RUN);
            nextState = ACCEPTANCE_NTS_IDLE;
            break;
    }
    // Anytime the state is going to change the timer must be reset.
    if(currentState != nextState)
    {
        ResetTimer();
    }

    currentState = nextState;
    return currentState;
}
