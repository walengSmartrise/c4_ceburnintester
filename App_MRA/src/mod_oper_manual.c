/******************************************************************************
 *
 * @file     mod_heartbeat.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
#include "position.h"
#include "mod_rescue.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Oper_Manual =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define DIRECTION_INPUT_DEBOUNCE_100MS          (5)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static enum en_subalarm_invalid_manual_run eSubAlarm_Plus1;
static uint8_t bEnableInspectionControl = 0;

static uint8_t ucDebounceUp_100ms;
static uint8_t ucDebounceDown_100ms;

static uint8_t bHallDoorsBypassed;
static uint8_t bCarDoorsBypassed;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   Checks for valid door state in hoistway access.
   Returns 1 if safe
 *----------------------------------------------------------------------------*/
uint8_t GetCarDoorSafeHoistwayAccess()
{
   uint8_t bSafe = 1;

   if( GetFP_RearDoors() )
   {
      /* If opening is rear, but front door is open, then car is unsafe */
      if( ( ( GetOperation_ManualMode() == MODE_M__INSP_HA_BOTTOM ) && ( Param_ReadValue_8Bit( enPARAM8__HA_BottomOpening ) ) )
       || ( ( GetOperation_ManualMode() == MODE_M__INSP_HA_TOP ) && ( Param_ReadValue_8Bit( enPARAM8__HA_TopOpening ) ) ) )
      {
         if( GetDoorState( DOOR_FRONT ) != DOOR__CLOSED )
         {
            bSafe = 0;
         }
      }
      /* If opening is front, but rear door is open, then car is unsafe */
      else if( ( ( GetOperation_ManualMode() == MODE_M__INSP_HA_BOTTOM ) && ( !Param_ReadValue_8Bit( enPARAM8__HA_BottomOpening ) ) )
            || ( ( GetOperation_ManualMode() == MODE_M__INSP_HA_TOP ) && ( !Param_ReadValue_8Bit( enPARAM8__HA_TopOpening ) ) ) )
      {
         if( GetDoorState( DOOR_REAR ) != DOOR__CLOSED )
         {
            bSafe = 0;
         }
      }
      if( !bSafe )
      {
         eSubAlarm_Plus1 = SUBALARM_INVALID_MANUAL_RUN__HA_OPENING+1;
      }
   }

   return bSafe;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint8_t GetHallDoorSafeHoistwayAccess( void )
{
   uint8_t bSafe = AllHallDoorsClosed();
   if( GetOperation_ManualMode() == MODE_M__INSP_HA_TOP )
   {
      uint8_t ucFloorIndex = Param_ReadValue_8Bit(enPARAM8__HA_TopFloor);
      if( ucFloorIndex >= GetFP_NumFloors() )
      {
         ucFloorIndex = GetFP_NumFloors()-1;
      }
      enum en_doors enSafeDoorIndex = ( !Param_ReadValue_8Bit(enPARAM8__HA_TopOpening) || !GetFP_RearDoors() ) ? DOOR_FRONT:DOOR_REAR;
      enum en_doors enUnsafeDoorIndex = ( !!Param_ReadValue_8Bit(enPARAM8__HA_TopOpening) || !GetFP_RearDoors() ) ? DOOR_FRONT:DOOR_REAR;
      uint8_t bLock_SafeDoor;
      uint8_t bLock_UnsafeDoor;
      // If front swing closed contacts are used, check those instead of hall interlock signals
      if( Door_GetSwingClosedContactsProgrammedFlag(enSafeDoorIndex) && Doors_CheckIfManualSwingDoor(enSafeDoorIndex, ucFloorIndex) )
      {
         bLock_SafeDoor = !GetCloseContact_Top(enSafeDoorIndex) && GetCloseContact_Mid(enSafeDoorIndex) && GetCloseContact_Bot(enSafeDoorIndex);
      }
      else
      {
         bLock_SafeDoor = !GetLock_Top(enSafeDoorIndex) && GetLock_Mid(enSafeDoorIndex) && GetLock_Bot(enSafeDoorIndex);
      }

      if( !GetFP_RearDoors() )
      {
         bLock_UnsafeDoor = 1;
      }
      else
      {
         // If rear swing closed contacts are used, check those instead of hall interlock signals, for cars with only front doors, this may be a duplicate of the SafeDoor check
         if( Door_GetSwingClosedContactsProgrammedFlag(enUnsafeDoorIndex) && Doors_CheckIfManualSwingDoor(enUnsafeDoorIndex, ucFloorIndex) )
         {
            bLock_UnsafeDoor = GetCloseContact_Top(enUnsafeDoorIndex) && GetCloseContact_Mid(enUnsafeDoorIndex) && GetCloseContact_Bot(enUnsafeDoorIndex);
         }
         else
         {
            bLock_UnsafeDoor = GetLock_Top(enUnsafeDoorIndex) && GetLock_Mid(enUnsafeDoorIndex) && GetLock_Bot(enUnsafeDoorIndex);
         }
      }

      if( ( bLock_SafeDoor )
       && ( bLock_UnsafeDoor ) )
      {
         bSafe = 1;
      }
   }
   else if( GetOperation_ManualMode() == MODE_M__INSP_HA_BOTTOM )
   {
      uint8_t ucFloorIndex = Param_ReadValue_8Bit(enPARAM8__HA_BottomFloor);
      enum en_doors enSafeDoorIndex = ( !Param_ReadValue_8Bit(enPARAM8__HA_BottomOpening) || !GetFP_RearDoors() ) ? DOOR_FRONT:DOOR_REAR;
      enum en_doors enUnsafeDoorIndex = ( !!Param_ReadValue_8Bit(enPARAM8__HA_BottomOpening) || !GetFP_RearDoors() ) ? DOOR_FRONT:DOOR_REAR;
      uint8_t bLock_SafeDoor;
      uint8_t bLock_UnsafeDoor;
      // If front swing closed contacts are used, check those instead of hall interlock signals
      if( Door_GetSwingClosedContactsProgrammedFlag(enSafeDoorIndex) && Doors_CheckIfManualSwingDoor(enSafeDoorIndex, ucFloorIndex) )
      {
         bLock_SafeDoor = GetCloseContact_Top(enSafeDoorIndex) && GetCloseContact_Mid(enSafeDoorIndex) && !GetCloseContact_Bot(enSafeDoorIndex);
      }
      else
      {
         bLock_SafeDoor = GetLock_Top(enSafeDoorIndex) && GetLock_Mid(enSafeDoorIndex) && !GetLock_Bot(enSafeDoorIndex);
      }

      if( !GetFP_RearDoors() )
      {
         bLock_UnsafeDoor = 1;
      }
      else
      {
         // If rear swing closed contacts are used, check those instead of hall interlock signals, for cars with only front doors, this may be a duplicate of the SafeDoor check
         if( Door_GetSwingClosedContactsProgrammedFlag(enUnsafeDoorIndex) && Doors_CheckIfManualSwingDoor(enUnsafeDoorIndex, ucFloorIndex) )
         {
            bLock_UnsafeDoor = GetCloseContact_Top(enUnsafeDoorIndex) && GetCloseContact_Mid(enUnsafeDoorIndex) && GetCloseContact_Bot(enUnsafeDoorIndex);
         }
         else
         {
            bLock_UnsafeDoor = GetLock_Top(enUnsafeDoorIndex) && GetLock_Mid(enUnsafeDoorIndex) && GetLock_Bot(enUnsafeDoorIndex);
         }
      }

      if( ( bLock_SafeDoor )
       && ( bLock_UnsafeDoor ) )
      {
         bSafe = 1;
      }
   }

   return bSafe;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static uint8_t Inspection_HallDoorsSafe ( void )
{
   bHallDoorsBypassed = 0;
   uint8_t bHallDoorSafeToRun = 0;
   if( Doors_GetBypassHallDoors() )
   {
      bHallDoorSafeToRun = 1;
      bHallDoorsBypassed = 1;
   }
   else if ( AllHallDoorsClosed_Inspection() )
   {
      bHallDoorSafeToRun = 1;
   }
   else if ( ( GetOperation_ManualMode() == MODE_M__INSP_HA_TOP )
          || ( GetOperation_ManualMode() == MODE_M__INSP_HA_BOTTOM ) )
   {
      bHallDoorSafeToRun = GetHallDoorSafeHoistwayAccess();
      bHallDoorsBypassed = bHallDoorSafeToRun;
   }
   if(!bHallDoorSafeToRun && !eSubAlarm_Plus1)
   {
      eSubAlarm_Plus1 = SUBALARM_INVALID_MANUAL_RUN__HALL_DOOR+1;
   }

   return bHallDoorSafeToRun;
}

/*-----------------------------------------------------------------------------
   Checking if the car doors are safe to run
 -----------------------------------------------------------------------------*/
static uint8_t Inspection_CarDoorsSafe ( void )
{
   bCarDoorsBypassed = 0;
   uint8_t bCarDoorSafeToRun = 0;
   if( Doors_GetBypassCarDoors() )
   {
      bCarDoorSafeToRun = 1;
      bCarDoorsBypassed = 1;
   }
   else if( CarDoorsClosed() )
   {
      bCarDoorSafeToRun = 1;
   }
   else if (  GetOperation_ManualMode() == MODE_M__INSP_HA_TOP
           || GetOperation_ManualMode() == MODE_M__INSP_HA_BOTTOM)
   {
      bCarDoorSafeToRun = GetCarDoorSafeHoistwayAccess();
      /* For manual swing hall doors, if the interlocks are controlled by the controller's
       * CAM output, the is a delay between when run starts and the doors are seen as fully closed.
       * Suppress the invalid doors alarm temporarily and rely on the motion start sequence checks
       * to flag this event if the locks are still open when a run begins. */
      uint8_t bManualSwingDoors_F = Doors_CheckIfManualSwingDoor(DOOR_FRONT, INVALID_FLOOR);
      uint8_t bManualSwingDoors_R = Doors_CheckIfManualSwingDoor(DOOR_REAR, INVALID_FLOOR);
      if( ( Doors_CheckIfManualSwingDoor(DOOR_FRONT, INVALID_FLOOR) || Doors_CheckIfManualSwingDoor(DOOR_REAR, INVALID_FLOOR) )
       && ( GetMotion_RunFlag() ) )
      {
         bCarDoorSafeToRun = 1;
         /* Suppress the alarm flagging a door is open */
         if( eSubAlarm_Plus1 == ( SUBALARM_INVALID_MANUAL_RUN__HA_OPENING+1 ) )
         {
            eSubAlarm_Plus1 = 0;
         }
      }

      bCarDoorsBypassed = bCarDoorSafeToRun;
   }
   if(!bCarDoorSafeToRun && !eSubAlarm_Plus1)
   {
      eSubAlarm_Plus1 = SUBALARM_INVALID_MANUAL_RUN__CAR_DOOR+1;
   }

   return bCarDoorSafeToRun;
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Inspection_UpdateDebouncedDirections( uint8_t ucUP, uint8_t ucDN )
{
   if(ucUP)
   {
      if(ucDebounceUp_100ms <= DIRECTION_INPUT_DEBOUNCE_100MS)
      {
         ucDebounceUp_100ms++;
      }
   }
   else
   {
      ucDebounceUp_100ms = 0;
   }

   if(ucDN)
   {
      if(ucDebounceDown_100ms <= DIRECTION_INPUT_DEBOUNCE_100MS)
      {
         ucDebounceDown_100ms++;
      }
   }
   else
   {
      ucDebounceDown_100ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t Inspection_GetDebouncedUp()
{
   uint8_t bUp = (ucDebounceUp_100ms >= DIRECTION_INPUT_DEBOUNCE_100MS) ? 1:0;
   return bUp;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t Inspection_GetDebouncedDown()
{
   uint8_t bDown = (ucDebounceDown_100ms >= DIRECTION_INPUT_DEBOUNCE_100MS) ? 1:0;
   return bDown;
}
/*
 * Purpose: Controls when the car can move up or down
 * Input: ucUP -> 1 when up is pressed, 0 otherwise
 * Input: ucDN -> 1 when down is pressed, 0 otherwise
 *
 * On there may be no locks and doors. When the operator presss up
 * or down the car should move in that direction.
 */

static void InspectionMotion( uint8_t ucUP, uint8_t ucDN )
{
   eSubAlarm_Plus1 = 0;
   uint8_t bCarDoorSafeToRun = Inspection_CarDoorsSafe();
   uint8_t bHallDoorSafeToRun = Inspection_HallDoorsSafe();
   //Door Controls Buttons
   for (enum en_doors enDoorIndex = DOOR_FRONT; enDoorIndex < GetNumberOfDoors(); enDoorIndex++)
   {
      uint8_t ucDCB = GetDoorCloseButton( enDoorIndex );
      uint8_t ucDOB = GetDoorOpenButton( enDoorIndex );
      if ( ucDOB && !GetMotion_RunFlag() )
      {
         SetDoorCommand( enDoorIndex, DOOR_COMMAND__OPEN_CONSTANT_PRESSURE );
         if( !bCarDoorsBypassed )
         {
            bCarDoorSafeToRun = 0;
         }
         if( !bHallDoorsBypassed )
         {
            bHallDoorSafeToRun = 0;
         }
         if(!eSubAlarm_Plus1)
         {
            eSubAlarm_Plus1 = SUBALARM_INVALID_MANUAL_RUN__DOB_F+enDoorIndex+1;
         }
      }
      else if ( ucDCB )
      {
         if (ucDCB & 0x10) // MR
         {
            SetDoorCommand( enDoorIndex, DOOR_COMMAND__NUDGE_CONSTANT_PRESSURE );
         } else {
            SetDoorCommand( enDoorIndex, DOOR_COMMAND__CLOSE_CONSTANT_PRESSURE );
         }
      }
      else
      {
         SetDoorCommand( enDoorIndex, DOOR_COMMAND__NONE );
      }
   }

   Inspection_UpdateDebouncedDirections(ucUP, ucDN);

   uint8_t bUP = Inspection_GetDebouncedUp();
   uint8_t bDN = Inspection_GetDebouncedDown();

   if ( bCarDoorSafeToRun
         && bHallDoorSafeToRun
         && bEnableInspectionControl )
   {
      if( !GetInputValue(enIN_CTEN)
       && ( GetOperation_ManualMode() == MODE_M__INSP_CT )
       && ( bDN || bUP ) )
      {
         SetAlarm(ALM__INVALID_MAN_RUN_CT_EN);
         SetOperation_MotionCmd(MOCMD__NORMAL_STOP);
      }
      else if ( (bDN)
            && (GetOperation_MotionCmd() != MOCMD__RUN_UP)
            && (!bUP) )
      {
         SetOperation_MotionCmd( MOCMD__RUN_DN);
      }
      else if ( (bUP)
            && (GetOperation_MotionCmd()) != MOCMD__RUN_DN
            && (!bDN) )
      {
         SetOperation_MotionCmd(MOCMD__RUN_UP);
      }
      else
      {
         if ( (bDN && bUP) )
         {
            bEnableInspectionControl = 0;
         }
         SetOperation_MotionCmd(MOCMD__NORMAL_STOP);
      }
   }
   else
   {
     //Arm controls once you see up and down both off.
     if ( !bEnableInspectionControl )
     {
        /* Use raw inputs for this check otherwise any time a car goes from
         * auto to inspction this check will be bypassed due to the debounce. */
        if( !ucUP && !ucDN )
        {
           bEnableInspectionControl = 1;
        }
        else
        {
           SetAlarm(ALM__INVALID_MAN_RUN_ARM);
        }
     }
     if( GetMotion_RunFlag() )
     {
        SetOperation_MotionCmd(MOCMD__EMERG_STOP);
     }

     /* Set alarm if attempting to run with invalid door state */
     if( bEnableInspectionControl && ( bUP || bDN ) )
     {
        if( eSubAlarm_Plus1 )
        {
            en_alarms eAlarm = ALM__INVALID_MAN_RUN_DOOR + eSubAlarm_Plus1 - 1;
            if(eAlarm > ALM__INVALID_MAN_RUN_CT_EN)
            {
               eAlarm = ALM__INVALID_MAN_RUN_CT_EN;
            }
            SetAlarm(eAlarm);
        }
     }
   }
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void SetInspectionSpeedLimit()
{
   if( Rescue_GetManualTractionRescue() )
   {
      uint16_t uwLevelingSpeed = Param_ReadValue_16Bit(enPARAM16__LevelingSpeed);
      SetOperation_SpeedLimit(uwLevelingSpeed);
   }
   else if( ( GetOperation_ManualMode() == MODE_M__INSP_HA )
         || ( GetOperation_ManualMode() == MODE_M__INSP_HA_BOTTOM )
         || ( GetOperation_ManualMode() == MODE_M__INSP_HA_TOP ) )
   {
      SetOperation_SpeedLimit(Param_ReadValue_8Bit( enPARAM8__AccessSpeed_fpm ));
   }
   else
   {
      SetOperation_SpeedLimit(Param_ReadValue_16Bit( enPARAM16__InspectionSpeed ));
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 100;
   pstThisModule->uwRunPeriod_1ms = 100;

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static enum en_mode_manual eLastManualMode;
   if(!GetPreflightFlag())
   {
      if (GetOperation_ManualMode() != eLastManualMode)
      {
         if(Param_ReadValue_1Bit(enPARAM1__EnableOpModeAlarm))
         {
            SetAlarm(ALM__MODE_CHANGE);
         }
         bEnableInspectionControl = 0;
         ucDebounceUp_100ms = 0;
         ucDebounceDown_100ms = 0;
      }

      if( !GetMotion_RunFlag() )
      {
         if ( SRU_Read_DIP_Switch( enSRU_DIP_B4 ) != Param_ReadValue_1Bit(enPARAM1__Enable_Pit_Inspection) ) {
            SetFault(FLT__INVALID_DIP_SW_B4);
         }
         if ( SRU_Read_DIP_Switch( enSRU_DIP_B3 ) != Param_ReadValue_1Bit(enPARAM1__Enable_Landing_Inspection) ) {
            SetFault(FLT__INVALID_DIP_SW_B3);
         }
      }

      if ( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
      {
         if (GetOperation_ManualMode() == MODE_M__INSP_HA_TOP)
         {
            uint8_t ucHA_Floor = Param_ReadValue_8Bit(enPARAM8__HA_TopFloor);
            if ( ucHA_Floor >= GetFP_NumFloors())
            {
               ucHA_Floor = GetFP_NumFloors() - 1;
            }

            uint32_t accessDistance = (uint32_t) Param_ReadValue_8Bit(enPARAM8__HA_AllowedDist_Top_FT) * ONE_FEET;
            SetOperation_PositionLimit_DN( gpastFloors[ ucHA_Floor ].ulPosition - accessDistance );
            SetOperation_PositionLimit_UP( gpastFloors[ ucHA_Floor ].ulPosition + ONE_INCH );
         }
         else if (GetOperation_ManualMode() == MODE_M__INSP_HA_BOTTOM)
         {
            uint8_t ucHA_Floor = Param_ReadValue_8Bit(enPARAM8__HA_BottomFloor);
            if ( ucHA_Floor >= GetFP_NumFloors())
            {
               ucHA_Floor = 0;
            }

            uint32_t accessDistance = (uint32_t) Param_ReadValue_8Bit(enPARAM8__HA_AllowedDist_Bot_FT) * ONE_FEET;
            SetOperation_PositionLimit_DN( gpastFloors[ ucHA_Floor ].ulPosition - ONE_INCH );
            SetOperation_PositionLimit_UP( gpastFloors[ ucHA_Floor ].ulPosition + accessDistance );
         }
         else if (GetOperation_ManualMode() > MODE_M__INVALID )
         {
             SetOperation_PositionLimit_DN( gpastFloors[ 0 ].ulPosition - ONE_INCH);
             SetOperation_PositionLimit_UP(gpastFloors[ GetFP_NumFloors() - 1 ].ulPosition + ONE_INCH);
         }

         uint8_t bUp = 0;
         uint8_t bDn = 0;

         SetOperation_BypassTermLimits(Param_ReadValue_1Bit( enPARAM1__BypassTermLimits ));

         SetInspectionSpeedLimit();

         switch ( GetOperation_ManualMode() )
         {
            case MODE_M__INSP_HA:
               SetOperation_BypassTermLimits(0); // A17 (2016) 2.12.7.3.3(3)(d)
               bUp = GetInputValue(enIN_ATU) || GetInputValue(enIN_ABU);
               bDn = GetInputValue(enIN_ATD) || GetInputValue(enIN_ABD);
               InspectionMotion( bUp, bDn );
               break;

            case MODE_M__INSP_HA_TOP:
               SetOperation_BypassTermLimits(0); // A17 (2016) 2.12.7.3.3(3)(d)
               bUp = GetInputValue(enIN_ATU);
               bDn = GetInputValue(enIN_ATD);
               InspectionMotion( bUp, bDn );
               break;

            case MODE_M__INSP_HA_BOTTOM:
               SetOperation_BypassTermLimits(0); // A17 (2016) 2.12.7.3.3(3)(d)
               bUp = GetInputValue(enIN_ABU);
               bDn = GetInputValue(enIN_ABD);
               InspectionMotion( bUp, bDn );
               break;

            case MODE_M__INSP_CT:
               bUp = GetInputValue(enIN_CTUP);
               bDn = GetInputValue(enIN_CTDN);
               InspectionMotion( bUp, bDn );
               break;

            case MODE_M__CONSTRUCTION:
               SetOperation_BypassTermLimits(1);
               if( Param_ReadValue_1Bit( enPARAM1__EnableConstructionRunBox )
               && !Rescue_GetManualTractionRescue() )
               {
                  bUp = GetInputValue(enIN_CONST_UP) & GetInputValue(enIN_CONST_EN);
                  bDn = GetInputValue(enIN_CONST_DN) & GetInputValue(enIN_CONST_EN);
               }
               else
               {
                  bUp = GetInputValue(enIN_MRUP);
                  bDn = GetInputValue(enIN_MRDN);
               }
               InspectionMotion( bUp, bDn );
               break;
            case MODE_M__INSP_MR:
               bUp = GetInputValue(enIN_MRUP);
               bDn = GetInputValue(enIN_MRDN);
               InspectionMotion( bUp, bDn );
               break;

            case MODE_M__INSP_PIT:
               bUp = GetInputValue(enIN_IPUP);
               bDn = GetInputValue(enIN_IPDN);
               InspectionMotion( bUp, bDn );
               break;

            case MODE_M__INSP_IC:
               bUp = GetInputValue(enIN_ICUP);
               bDn = GetInputValue(enIN_ICDN);
               InspectionMotion( bUp, bDn );
               break;

            case MODE_M__INSP_LND:
               bUp = GetInputValue(enIN_ILUP);
               bDn = GetInputValue(enIN_ILDN);
               InspectionMotion( bUp, bDn );
               break;

            case MODE_M__INVALID:
            default:
               ucDebounceUp_100ms = 0;
               ucDebounceDown_100ms = 0;
               //STOP!
               if(Param_ReadValue_1Bit(enPARAM1__EnableEStopAlarms))
               {
                  SetAlarm(ALM__ESTOP_INVALID_INSP);
               }
               SetOperation_MotionCmd(MOCMD__EMERG_STOP);
               break;
         }
      }
      else
      {
         bEnableInspectionControl = 0;
         ucDebounceUp_100ms = 0;
         ucDebounceDown_100ms = 0;
         SetOperation_ManualMode(MODE_M__NONE);
      }
   }
   if ( gstFault.bActiveFault )
   {
       SetOperation_MotionCmd( MOCMD__EMERG_STOP );
   }
   eLastManualMode = GetOperation_ManualMode();
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
