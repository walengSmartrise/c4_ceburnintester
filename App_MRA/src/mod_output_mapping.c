/******************************************************************************
 *
 * @file     mod_local_outputs.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "sru.h"
#include "sru_a.h"
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
#include "mod_doors.h"
#include "automatic_operation.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_OutputMapping =
{
   .pfnInit = Init,
   .pfnRun = Run,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MAX_TERMINALS_PER_OUTPUT_FUNCTION           (2)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
typedef struct
{
   uint16_t *pauwActiveTerm_Plus1[MAX_TERMINALS_PER_OUTPUT_FUNCTION];
   uint16_t *pauwTermToCheck[MAX_TERMINALS_PER_OUTPUT_FUNCTION];
   enum_output_groups eCOG;
   uint8_t ucFirstIndex;
   uint8_t ucLastIndex;
}st_output_group_ctrl;
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint32_t auiBF_RawTerminalOutputs[ BITMAP32_SIZE(MAX_NUM_IO_TERMINALS) ];

static uint32_t gaulBitMappedOutputs[ BITMAP32_SIZE ( NUM_OUTPUT_FUNCTIONS ) ];

/* CAR CALL LAMPS */
static uint16_t * pauwActiveTerm_Plus1_CCL_Front[MAX_TERMINALS_PER_OUTPUT_FUNCTION];
static uint16_t * pauwActiveTerm_Plus1_CCL_Rear[MAX_TERMINALS_PER_OUTPUT_FUNCTION];
static uint16_t * pauwTermToCheck_CCL_Front[MAX_TERMINALS_PER_OUTPUT_FUNCTION];
static uint16_t * pauwTermToCheck_CCL_Rear[MAX_TERMINALS_PER_OUTPUT_FUNCTION];

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint16_t GetLocalOutputs(enum en_io_boards eBoard)
{
   uint16_t uwReturn = 0;
   switch( eBoard )
   {
      case IO_BOARDS__MRA:
         uwReturn = Sys_Byte_Get_Array32( &auiBF_RawTerminalOutputs[0],
                                          (TERMINAL_INDEX_START__MRA/MIN_TERMINALS_PER_IO_BOARD),
                                          BITMAP32_SIZE( MAX_NUM_IO_TERMINALS ) );
         break;
      case IO_BOARDS__CTA:
         uwReturn = Sys_Byte_Get_Array32(&auiBF_RawTerminalOutputs[0],
                                         (TERMINAL_INDEX_START__CTA/MIN_TERMINALS_PER_IO_BOARD),
                                         BITMAP32_SIZE( MAX_NUM_IO_TERMINALS ));
         uwReturn |= Sys_Byte_Get_Array32(&auiBF_RawTerminalOutputs[0],
                                          (TERMINAL_INDEX_START__CTA/MIN_TERMINALS_PER_IO_BOARD)+1,
                                          BITMAP32_SIZE( MAX_NUM_IO_TERMINALS )) << 8;
         break;
      case IO_BOARDS__COP:
         uwReturn = Sys_Byte_Get_Array32(&auiBF_RawTerminalOutputs[0],
                                         (TERMINAL_INDEX_START__COP/MIN_TERMINALS_PER_IO_BOARD),
                                         BITMAP32_SIZE( MAX_NUM_IO_TERMINALS ));
         uwReturn |= Sys_Byte_Get_Array32(&auiBF_RawTerminalOutputs[0],
                                          (TERMINAL_INDEX_START__COP/MIN_TERMINALS_PER_IO_BOARD)+1,
                                          BITMAP32_SIZE( MAX_NUM_IO_TERMINALS )) << 8;
         break;
      case IO_BOARDS__RISER1:
      case IO_BOARDS__RISER2:
      case IO_BOARDS__RISER3:
      case IO_BOARDS__RISER4:
         uwReturn = Sys_Byte_Get_Array32(&auiBF_RawTerminalOutputs[0],
                                         (TERMINAL_INDEX_START__RIS/MIN_TERMINALS_PER_IO_BOARD)+(eBoard-IO_BOARDS__RISER1),
                                         BITMAP32_SIZE( MAX_NUM_IO_TERMINALS ));
         break;
      case IO_BOARDS__EXP1:
      case IO_BOARDS__EXP2:
      case IO_BOARDS__EXP3:
      case IO_BOARDS__EXP4:
      case IO_BOARDS__EXP5:
      case IO_BOARDS__EXP6:
      case IO_BOARDS__EXP7:
      case IO_BOARDS__EXP8:
      case IO_BOARDS__EXP9:
      case IO_BOARDS__EXP10:
      case IO_BOARDS__EXP11:
      case IO_BOARDS__EXP12:
      case IO_BOARDS__EXP13:
      case IO_BOARDS__EXP14:
      case IO_BOARDS__EXP15:
      case IO_BOARDS__EXP16:
      case IO_BOARDS__EXP17:
      case IO_BOARDS__EXP18:
      case IO_BOARDS__EXP19:
      case IO_BOARDS__EXP20:
      case IO_BOARDS__EXP21:
      case IO_BOARDS__EXP22:
      case IO_BOARDS__EXP23:
      case IO_BOARDS__EXP24:
      case IO_BOARDS__EXP25:
      case IO_BOARDS__EXP26:
      case IO_BOARDS__EXP27:
      case IO_BOARDS__EXP28:
      case IO_BOARDS__EXP29:
      case IO_BOARDS__EXP30:
      case IO_BOARDS__EXP31:
      case IO_BOARDS__EXP32:
      case IO_BOARDS__EXP33:
      case IO_BOARDS__EXP34:
      case IO_BOARDS__EXP35:
      case IO_BOARDS__EXP36:
      case IO_BOARDS__EXP37:
      case IO_BOARDS__EXP38:
      case IO_BOARDS__EXP39:
      case IO_BOARDS__EXP40:
         uwReturn = Sys_Byte_Get_Array32(&auiBF_RawTerminalOutputs[0],
                                         (TERMINAL_INDEX_START__EXP/MIN_TERMINALS_PER_IO_BOARD)+(eBoard-IO_BOARDS__EXP1),
                                         BITMAP32_SIZE( MAX_NUM_IO_TERMINALS ));
         break;
      default: break;
   }

   return uwReturn;
}

/*-----------------------------------------------------------------------------


 -----------------------------------------------------------------------------*/
void SetOutputValue( enum en_output_functions eOutput, uint8_t bValue )
{
   Sys_Bit_Set_Array32( gaulBitMappedOutputs,
                        eOutput,
                        NUM_ARRAY_ELEMENTS( gaulBitMappedOutputs ),
                        bValue
                      );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
inline uint8_t GetOutputValue( enum en_output_functions eOutput )
{
   return GetOutputBitmapValue(eOutput);
}

/*-----------------------------------------------------------------------------
   Returns 1 if the given Term is programmed with the arguement function.

   Terms programmed in EEPROM as follows:
      Group Index:      Bit [8:15]
      Inversion:        Bit [7]
      Function Index:   Bit [0:6]
 -----------------------------------------------------------------------------*/
static uint8_t CheckIf_TermProgrammedAsGroupFunc( uint16_t uwTerm, enum_output_groups eCOG, uint8_t ucFunctionIndex )
{
   uint8_t bProgrammed = 0;
   if( ( uwTerm < MAX_NUM_IO_TERMINALS )
    && ( eCOG && ( eCOG < NUM_OUTPUT_GROUPS ) )
    && ( ucFunctionIndex < SysIO_GetSizeOfCOG(eCOG) ) )
   {
      uint16_t uwParam = Param_ReadValue_16Bit( enPARAM16__MR_Outputs_601 + uwTerm );
      if( ( eCOG == ( ( uwParam >> 8 ) & 0xFF ) )
       && ( ucFunctionIndex == ( uwParam & 0x7F ) ) )
      {
         bProgrammed = 1;
      }
   }

   return bProgrammed;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateGlobalOutputBitMap(void)
{
   for(uint8_t i = 0; i < NUM_ARRAY_ELEMENTS( gaulBitMappedOutputs ); i++)
   {
      if(GetOutputBitMap(i) != gaulBitMappedOutputs[i])
      {
         SetOutputBitMap(gaulBitMappedOutputs[i], i);
      }
   }
}

/*-----------------------------------------------------------------------------
//If idle for greater than X seconds, turn off
 -----------------------------------------------------------------------------*/
static void Update_FanAndLight(void)
{
   uint8_t bOn = 0;
   if(GetInputValue(enIN_FAN_AND_LIGHT))
   {
      bOn = 1;
   }
   else if( IdleTime_GetFanAndLightTime_Seconds() < Param_ReadValue_8Bit(enPARAM8__FanAndLight_1s) )
   {
      bOn = 1;
   }

   SetOutputValue(enOUT_LIGHTFAN, bOn);
}
/*-----------------------------------------------------------------------------
//If idle for greater than X seconds, turn off
 -----------------------------------------------------------------------------*/
static void Update_MR_Fan(void)
{
   uint8_t bOn = 0;
   if( IdleTime_GetFanAndLightTime_Minutes() < Param_ReadValue_8Bit(enPARAM8__MRFanTimer_min) )
   {
      bOn = 1;
   }

   SetOutputValue(enOUT_MR_FAN, bOn);
}
/*-----------------------------------------------------------------------------
Update regen outputs
   From V2: If car stopped for atleast 2 seconds and regen fault is high, enOUT_REGEN_RESET
   //TODO also check that not on battery lowering
   enOUT_REGEN_ENABLE directly tied to SAFE output
 -----------------------------------------------------------------------------*/
static void Update_RegenOutputs()
{
   static uint16_t uwTimer_ms;
   uint8_t bResetRegen = 0;
   if( !GetMotion_RunFlag() )
   {
      if( uwTimer_ms < 2000 ) //2 seconds
      {
         uwTimer_ms += MOD_RUN_PERIOD_OUTPUT_MAPPING_1MS;
      }
   }
   else
   {
      uwTimer_ms = 0;
   }

   if( GetInputValue(enIN_REGEN_FLT) )
   {
      //Set fault
      if( uwTimer_ms >= 2000 )// 2 sec
      {
         bResetRegen = 1;
      }
   }
   /* Regen unit requires pulsed reset */
   SetOutputValue( enOUT_REGEN_RESET, bResetRegen && SRU_Read_LED(enSRU_LED_Heartbeat) );
   uint8_t bRegenEnable = ( GetOperation_AutoMode() != MODE_A__BATT_RESQ )
                       && ( ( GetEmergencyPowerCommand() == EPC__OFF ) || ( Param_ReadValue_1Bit(enPARAM1__EnableRegenOnEP) ) )
                       && ( GetPickMFlag() || Drive_GetMotorLearnFlag() );
   SetOutputValue( enOUT_REGEN_ENABLE, bRegenEnable );
}
/*-----------------------------------------------------------------------------
FOR DSD drive
Update Drive HW enable output.
   Currently tied to picking of M Contactor
 -----------------------------------------------------------------------------*/
static void Update_DriveHWEnable()
{
   SetOutputValue( enOUT_DRV_HW_ENABLE, GetDriveHWEnableFlag() );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t LoadWeigher_GetBuzzerFlag()
{
   uint8_t bBuzzer = 0;
   if( !Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect)
     && GetInputValue(enIN_OVER_LOAD)
     && ( GetOperation_AutoMode() == MODE_A__NORMAL )
     && !GetMotion_RunFlag() )
   {
      bBuzzer = 1;
   }
   return bBuzzer;
}
 /*-----------------------------------------------------------------------------
In use output
From V2:
    bInUse =    (gb4Control_PrepareToRun == B4_TRUE)
             || (gb4Control_RunFlag == B4_TRUE)
             || (gucDisplayModeOfOperation != MOP_AUTO_NORMAL)
             || !gbCarDoors_AllGSW_Made;
 -----------------------------------------------------------------------------*/
static void Update_InUseLamp()
{
   uint8_t bInUse = ( ( GetMotion_RunFlag() )
                   || ( AnyDoorOpen() )
                   || ( GetOperation_AutoMode() != MODE_A__NORMAL )
                   || ( GetPreflightFlag() ) );
   SetOutputValue( enOUT_IN_USE, bInUse );
}
/*-----------------------------------------------------------------------------
update EPWR outputs
 -----------------------------------------------------------------------------*/
static void Update_EPowerOutputs()
{
   uint8_t bEP = ( GetEmergencyPowerCommand() != EPC__OFF ) ? 1:0;
   SetOutputValue( enOUT_LMP_EPWR, bEP );

   for(uint8_t i = 0; i < MAX_GROUP_CARS; i++)
   {
      uint8_t bLamp = 0;

      uint8_t bEPC_Recall = (
            ( GetOperation_AutoMode() == MODE_A__EPOWER ) &&
            ( GetEmergencyPowerCommand() == EPC__RECALL ) &&
            GetMotion_RunFlag()
      );

      uint8_t bEPC_Auto = ( GetEmergencyPowerCommand() == EPC__RUN_AUTO );

      if( ( i == Param_ReadValue_8Bit(enPARAM8__GroupCarIndex) ) && (bEPC_Recall || bEPC_Auto) )
      {
         bLamp = 1;
      }
      SetOutputValue(enOUT_LMP_Select1+i, bLamp);
   }
}
/*-----------------------------------------------------------------------------
 Update chime output
 -----------------------------------------------------------------------------*/
#define CHIME_DURATION_10MS      (50)
static void Update_ChimeOutput()
{
   static uint8_t ucFloor_Last;
   static uint8_t ucChimeCounter_10MS;
   static uint8_t bChime_Out;
   if( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
    && ( !GetInputValue(enIN_PASSING_CHIME_DISABLE) ) )
   {
      if( GetOperation_CurrentFloor() != ucFloor_Last )
      {
         // Store last floor for arrival up/dn
         ucFloor_Last = GetOperation_CurrentFloor();
         ucChimeCounter_10MS = 0;

         bChime_Out = 1;
      }

      if(ucChimeCounter_10MS > CHIME_DURATION_10MS)
      {
         bChime_Out = 0;
      }
      else
      {
         ucChimeCounter_10MS++;
      }
   }
   else
   {
      bChime_Out = 0;
   }

   SetOutputValue(enOUT_CHIME, bChime_Out);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_OverloadOutput()
{
   uint8_t bOverload = ( GetOperation_ClassOfOp() == CLASSOP__AUTO ) &&
                       ( GetFault_ByNumber(FLT__OVERLOADED)
                      || GetOverLoadFlag() );
   SetOutputValue( enOUT_Overload, bOverload );
}
/*-----------------------------------------------------------------------------
update at recall output
 -----------------------------------------------------------------------------*/
static void Update_AtRecallOutput(void)
{
   enum en_doors enRecallDoor = Param_ReadValue_1Bit(enPARAM1__FireRecallDoorM);
   uint8_t bDoorsOpen = GetDoorState(enRecallDoor) == DOOR__OPEN;
   uint8_t ucRecallFloor = Param_ReadValue_8Bit( enPARAM8__FireRecallFloorM );

   uint8_t bOutput = (GetOperation_CurrentFloor() == ucRecallFloor) && bDoorsOpen;
   SetOutputValue( enOUT_At_Recall, bOutput );
}
/*-----------------------------------------------------------------------------
update discrete arrival lantern outputs
 -----------------------------------------------------------------------------*/
static void Update_DiscreteArrivalLanterns(void)
{
   SetOutputValue(enOUT_ARV_UP_1, 0);
   SetOutputValue(enOUT_ARV_DN_1, 0);
   SetOutputValue(enOUT_ARV_UP_2, 0);
   SetOutputValue(enOUT_ARV_DN_2, 0);
   SetOutputValue(enOUT_ARV_UP_3, 0);
   SetOutputValue(enOUT_ARV_DN_3, 0);
   SetOutputValue(enOUT_ARV_UP_4, 0);
   SetOutputValue(enOUT_ARV_DN_4, 0);
   SetOutputValue(enOUT_ARV_UP_5, 0);
   SetOutputValue(enOUT_ARV_DN_5, 0);
   if( GetArrivalLamp_Front() )
   {
      for(uint8_t i = 0; i < 5; i++)
      {
         uint8_t ucFloor = Param_ReadValue_8Bit(enPARAM8__ArvLanternFloor_1+i);
         uint8_t bRear = Param_ReadValue_1Bit(enPARAM1__ArvLanternDoor_1+i);
         if( ( !bRear ) &&
             ( GetOperation_DestinationFloor() == ucFloor ) )
         {
            enum en_output_functions eOutput = enOUT_ARV_UP_1 + i*2;
            if( GetArrivalLamp_Front() == DIR__DN )
            {
               eOutput += 1;
            }
            SetOutputValue(eOutput, 1);
         }
      }
   }
   if( GetArrivalLamp_Rear() )
   {
      for(uint8_t i = 0; i < 5; i++)
      {
         uint8_t ucFloor = Param_ReadValue_8Bit(enPARAM8__ArvLanternFloor_1+i);
         uint8_t bRear = Param_ReadValue_1Bit(enPARAM1__ArvLanternDoor_1+i);
         if( ( bRear ) &&
             ( GetOperation_DestinationFloor() == ucFloor ) )
         {
            enum en_output_functions eOutput = enOUT_ARV_UP_1 + i*2;
            if( GetArrivalLamp_Rear() == DIR__DN )
            {
               eOutput += 1;
            }
            SetOutputValue(eOutput, 1);
         }
      }
   }
}
/*-----------------------------------------------------------------------------
   Update distress lamp/buzzer outputs

   At a central control console, a distress
   and light buzzer will be provided for each
   elevator and an acknowledge button common
   to all elevators. Pressing the distress
   alarm button, triggering the emergency
   stop switch, if a dispatched car remains at a landing for >30 seconds, or if the electrical safety circuit is open will turn on the distress light, and pulse the distress buzzer. The Distress light will remain lit until the acknowledge button is pressed.

 -----------------------------------------------------------------------------*/
#define DISTRESS_STALLED_CAR_TIMER__SEC         (30)
#define DISTRESS_BUZZER_TOGGLE_INTERVAL__10MS   (150)
static void UpdateDistressLampAndBuzzer(void)
{
   static uint8_t bDistressLamp;
   static uint8_t ucBuzzerCounter_10ms;
   uint8_t bBuzzer = 0;
   uint8_t bLamp = 0;
   if( CheckIfInputIsProgrammed(enIN_DISTRESS_BUTTON) )
   {
      /* Keep distress lamp illuminated until acknowledge button pressed */
      if( GetInputValue(enIN_DISTRESS_ACKNOWLEDGE) )
      {
         bDistressLamp = 0;
      }

      if( ( !GetInputValue(enIN_ICST) )
       || ( GetInputValue(enIN_DISTRESS_BUTTON) )
       || ( IdleTime_GetFaultedTime_Seconds() >= DISTRESS_STALLED_CAR_TIMER__SEC ))
      {
         bBuzzer = GetOutputValue(enOUT_DISTRESS_BUZZER);
         bDistressLamp = 1;
         if(++ucBuzzerCounter_10ms >= DISTRESS_BUZZER_TOGGLE_INTERVAL__10MS)
         {
            ucBuzzerCounter_10ms = 0;
            bBuzzer = !bBuzzer;
         }
      }

      bLamp = bDistressLamp;
   }

   SetOutputValue(enOUT_DISTRESS_BUZZER, bBuzzer);
   SetOutputValue(enOUT_DISTRESS_LAMP, bLamp);
}
/*-----------------------------------------------------------------------------
Update Outputs
 -----------------------------------------------------------------------------*/
static void Update_Outputs(void)
{
   //--------------------------------------------------------------------
   /* Fire */
   SetOutputValue( enOUT_LMP_FIRE, gbInCarFireLamp );
   /* Only allow the master dispatcher to set the lobby lamp, otherwise the flashing
    * fire hat may be erratic from being commanded by all group cars. */
   SetOutputValue( enOUT_FIRE_LOBBY_LAMP, gbLobbyFireLamp && GetMasterDispatcherFlag_MRB() );

   //--------------------------------------------------------------------
   /* Buzzer */
   uint8_t bBuzzer = EMS_GetBuzzerFlag()
                  || Attendant_GetBuzzerFlag()
                  || Doors_GetBuzzerFlag()
                  || LoadWeigher_GetBuzzerFlag()
                  || Fire_GetBuzzer()
                  || Overload_GetBuzzerFlag()
                  || ( Param_ReadValue_1Bit( enPARAM1__EnableEarthQuake )
                      &&  Param_ReadValue_1Bit(enPARAM1__EQ_Buzzer )
                      && ( GetEmergencyBit(EmergencyBF_CW_Derail_InProgress) || GetEmergencyBit(EmergencyBF_EQ_Seismic_InProgress))
                      );

   SetOutputValue( enOUT_BUZZER, bBuzzer );

   //--------------------------------------------------------------------
   /* Inspection */
   if ( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
   {
      SetOutputValue( enOUT_LMP_INSP , 1 );
   }
   else
   {
      SetOutputValue( enOUT_LMP_INSP , 0 );
   }

   //--------------------------------------------------------------------
   /* EQ */
   if( Param_ReadValue_1Bit( enPARAM1__EQ_OldJobSupport ) )
   {
      SetOutputValue(enOUT_LMP_EQ, OnEarthquake());
   }

   //--------------------------------------------------------------------

   /* TRV UP/DN  */
   if( GetMotion_Direction() > 0 )
   {
      SetOutputValue(enOUT_TRV_UP, 1);
      SetOutputValue(enOUT_TRV_DN, 0);
   }
   else if( GetMotion_Direction() < 0 )
   {
      SetOutputValue(enOUT_TRV_UP, 0);
      SetOutputValue(enOUT_TRV_DN, 1);
   }
   else // == 0
   {
      SetOutputValue(enOUT_TRV_UP, 0);
      SetOutputValue(enOUT_TRV_DN, 0);
   }

   //--------------------------------------------------------------------
   /* ARV UP/DN */
   SetOutputValue(enOUT_ARV_UP_F, ( GetArrivalLamp_Front() == DIR__UP ) );
   SetOutputValue(enOUT_ARV_DN_F, ( GetArrivalLamp_Front() == DIR__DN ) );
   SetOutputValue(enOUT_ARV_UP_R, ( GetArrivalLamp_Rear() == DIR__UP ) );
   SetOutputValue(enOUT_ARV_DN_R, ( GetArrivalLamp_Rear() == DIR__DN ) );
   //--------------------------------------------------------------------
   /* Chime */
   Update_ChimeOutput();

   /* Car To Lobby Lamp */
   SetOutputValue( enOUT_CAR_TO_LOBBY, GetCarToLobbyLamp() );

   /* In Service Lamp */
   uint8_t bInService = !HallCallsDisabled() && !gstFault.bActiveFault;
   SetOutputValue( enOUT_IN_SERVICE, bInService );

   Update_FanAndLight();

   Update_MR_Fan();

   Update_RegenOutputs();

   Update_DriveHWEnable();

   /* EMS */
   SetOutputValue( enOUT_LMP_EMS, EMS_GetLampFlag() );

   /* Attendant */
   SetOutputValue( enOUT_LMP_ATTD_ABOVE, Attendant_GetHallCallLamp_Above() );
   SetOutputValue( enOUT_LMP_ATTD_BELOW, Attendant_GetHallCallLamp_Below() );

   /* Independent Srv */
   SetOutputValue( enOUT_LMP_INDP_SRV, ( GetOperation_AutoMode() == MODE_A__INDP_SRV ) );

   /* Accelerating/Decelerating */
   SetOutputValue( enOUT_ACCELERATING, Motion_GetMotionState()==MOTION__ACCELERATING );
   SetOutputValue( enOUT_DECELERATING, Motion_GetMotionState()==MOTION__DECELERATING );

   /* Emergency Power */
   Update_EPowerOutputs();

   Update_InUseLamp();

   /* Door hold lamps */
   SetOutputValue(enOUT_DoorHold_F, GetInputValue(enIN_DoorHold_F));
   SetOutputValue(enOUT_DoorHold_R, GetInputValue(enIN_DoorHold_R));

   /* Rescue */
   SetOutputValue(enOUT_RecTrvDir, Rescue_GetRecTrvDirCommand_HPV());
   SetOutputValue(enOUT_BatteryPwr, Rescue_GetActiveFlag());

   Update_OverloadOutput();

   Update_AtRecallOutput();

   /* Phone Failure */
   SetOutputValue(enOUT_PhoneFailLamp, PhoneFailure_GetLamp());
   SetOutputValue(enOUT_PhoneFailBuzzer, PhoneFailure_GetBuzzer());

   SetOutputValue(enOUT_LMP_Parking, GetParkingLamp());

   /* Discrete Arrival Lanterns */
   Update_DiscreteArrivalLanterns();

   /* Auto Rescue Output */
   SetOutputValue(enOUT_AUTO_RESCUE, ( GetOperation_AutoMode() == MODE_A__BATT_RESQ ) );

   UpdateDistressLampAndBuzzer();
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckFor_InvalidTerminalAlarm( st_output_group_ctrl *pstOutputGroupCtrl, uint8_t ucFunctionIndex )
{
   uint16_t uwTerminal;
   /* If two active terminals have been detected, check for third and set alarm if found */
   if( pstOutputGroupCtrl->pauwActiveTerm_Plus1[0][ucFunctionIndex]
    && pstOutputGroupCtrl->pauwActiveTerm_Plus1[1][ucFunctionIndex] )
   {
      if( ++pstOutputGroupCtrl->pauwTermToCheck[0][ucFunctionIndex] >= MAX_NUM_IO_TERMINALS )
      {
         pstOutputGroupCtrl->pauwTermToCheck[0][ucFunctionIndex] = 0;
      }
      uwTerminal = pstOutputGroupCtrl->pauwTermToCheck[0][ucFunctionIndex];
      if( ( uwTerminal != pstOutputGroupCtrl->pauwActiveTerm_Plus1[0][ucFunctionIndex]-1 )
       && ( uwTerminal != pstOutputGroupCtrl->pauwActiveTerm_Plus1[1][ucFunctionIndex]-1 )
       && CheckIf_TermProgrammedAsGroupFunc( uwTerminal, pstOutputGroupCtrl->eCOG, ucFunctionIndex ) )
      {
         en_alarms eAlarm = ALM__DUPLICATE_MR_601 + uwTerminal;
         if(eAlarm > ALM__DUPLICATE_EXP40_608)
         {
            eAlarm = ALM__DUPLICATE_EXP40_608;
         }
         SetAlarm(eAlarm);
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableOutputGroup( st_output_group_ctrl *pstOutputGroupCtrl )
{
   uint16_t uwTerminal;
   uint8_t bValue, bInvert;
   enum en_output_functions eOutput;
   for ( uint8_t ucIndex = pstOutputGroupCtrl->ucFirstIndex; ucIndex < pstOutputGroupCtrl->ucLastIndex; ucIndex++ )
   {
      eOutput = SysIO_GetOutputFunctionFromCOG( pstOutputGroupCtrl->eCOG, ucIndex );
      bValue = GetOutputValue( eOutput );
      for( uint8_t ucSource = 0; ucSource < MAX_TERMINALS_PER_OUTPUT_FUNCTION; ucSource++ )//Functions can be assigned to up to 2 Terms
      {
         if( pstOutputGroupCtrl->pauwActiveTerm_Plus1[ucSource][ucIndex] )
         {
            uwTerminal = pstOutputGroupCtrl->pauwActiveTerm_Plus1[ucSource][ucIndex]-1;
            //Check that Term is still set
            if( CheckIf_TermProgrammedAsGroupFunc( uwTerminal, pstOutputGroupCtrl->eCOG, ucIndex ) )
            {
               bInvert = ( Param_ReadValue_16Bit( enPARAM16__MR_Outputs_601 + uwTerminal ) & MASK_IO_PARAM_TERMINAL_INVERSION ) != 0;
               Sys_Bit_Set( &auiBF_RawTerminalOutputs, uwTerminal, bValue ^ bInvert );
            }
            else
            {
               pstOutputGroupCtrl->pauwActiveTerm_Plus1[ucSource][ucIndex] = 0;
            }
         }
         else // Check a new terminal for specified function
         {
            if( ++pstOutputGroupCtrl->pauwTermToCheck[ucSource][ucIndex] >= MAX_NUM_IO_TERMINALS )
            {
               pstOutputGroupCtrl->pauwTermToCheck[ucSource][ucIndex] = 0;
            }
            uwTerminal = pstOutputGroupCtrl->pauwTermToCheck[ucSource][ucIndex];
            if( CheckIf_TermProgrammedAsGroupFunc( uwTerminal, pstOutputGroupCtrl->eCOG, ucIndex ) )
            {
               bInvert = ( Param_ReadValue_16Bit( enPARAM16__MR_Outputs_601 + uwTerminal ) & MASK_IO_PARAM_TERMINAL_INVERSION ) != 0;
               Sys_Bit_Set( &auiBF_RawTerminalOutputs, uwTerminal, bValue ^ bInvert );
               pstOutputGroupCtrl->pauwActiveTerm_Plus1[ucSource][ucIndex] = uwTerminal + 1;
            }
         }
      }
      /* Don't allow both active terminals to be the same */
      if( pstOutputGroupCtrl->pauwActiveTerm_Plus1[0][ucIndex] == pstOutputGroupCtrl->pauwActiveTerm_Plus1[1][ucIndex] )
      {
         pstOutputGroupCtrl->pauwActiveTerm_Plus1[1][ucIndex] = 0;
      }

      CheckFor_InvalidTerminalAlarm( pstOutputGroupCtrl, ucIndex );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableOutputGroup_Auto()
{
   static uint16_t auwActiveTerm_Plus1[MAX_TERMINALS_PER_OUTPUT_FUNCTION][NUM_COG_AUTO_OPER];
   static uint16_t auwTermToCheck[MAX_TERMINALS_PER_OUTPUT_FUNCTION][NUM_COG_AUTO_OPER];
   static st_output_group_ctrl stOutputGroupCtrl =
   {
         .pauwActiveTerm_Plus1[0] = &auwActiveTerm_Plus1[0][0],
         .pauwActiveTerm_Plus1[1] = &auwActiveTerm_Plus1[1][0],
         .pauwTermToCheck[0] = &auwTermToCheck[0][0],
         .pauwTermToCheck[1] = &auwTermToCheck[1][0],
         .eCOG = OUTPUT_GROUP__AUTO_OPER,
         .ucFirstIndex = COG_AUTO_OPER__TRV_UP,
         .ucLastIndex = NUM_COG_AUTO_OPER,
   };
   UpdateProgrammableOutputGroup(&stOutputGroupCtrl);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableOutputGroup_DoorsFront()
{
   static uint16_t auwActiveTerm_Plus1[MAX_TERMINALS_PER_OUTPUT_FUNCTION][NUM_COG_DOORS];
   static uint16_t auwTermToCheck[MAX_TERMINALS_PER_OUTPUT_FUNCTION][NUM_COG_DOORS];
   static st_output_group_ctrl stOutputGroupCtrl =
   {
         .pauwActiveTerm_Plus1[0] = &auwActiveTerm_Plus1[0][0],
         .pauwActiveTerm_Plus1[1] = &auwActiveTerm_Plus1[1][0],
         .pauwTermToCheck[0] = &auwTermToCheck[0][0],
         .pauwTermToCheck[1] = &auwTermToCheck[1][0],
         .eCOG = OUTPUT_GROUP__DOORS_F,
         .ucFirstIndex = COG_DOORS__DO,
         .ucLastIndex = NUM_COG_DOORS,
   };
   UpdateProgrammableOutputGroup(&stOutputGroupCtrl);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableOutputGroup_DoorsRear()
{
   static uint16_t auwActiveTerm_Plus1[MAX_TERMINALS_PER_OUTPUT_FUNCTION][NUM_COG_DOORS];
   static uint16_t auwTermToCheck[MAX_TERMINALS_PER_OUTPUT_FUNCTION][NUM_COG_DOORS];
   static st_output_group_ctrl stOutputGroupCtrl =
   {
         .pauwActiveTerm_Plus1[0] = &auwActiveTerm_Plus1[0][0],
         .pauwActiveTerm_Plus1[1] = &auwActiveTerm_Plus1[1][0],
         .pauwTermToCheck[0] = &auwTermToCheck[0][0],
         .pauwTermToCheck[1] = &auwTermToCheck[1][0],
         .eCOG = OUTPUT_GROUP__DOORS_R,
         .ucFirstIndex = COG_DOORS__DO,
         .ucLastIndex = NUM_COG_DOORS,
   };
   UpdateProgrammableOutputGroup(&stOutputGroupCtrl);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableOutputGroup_Fire()
{
   static uint16_t auwActiveTerm_Plus1[MAX_TERMINALS_PER_OUTPUT_FUNCTION][NUM_COG_FIRE_EQ];
   static uint16_t auwTermToCheck[MAX_TERMINALS_PER_OUTPUT_FUNCTION][NUM_COG_FIRE_EQ];
   static st_output_group_ctrl stOutputGroupCtrl =
   {
         .pauwActiveTerm_Plus1[0] = &auwActiveTerm_Plus1[0][0],
         .pauwActiveTerm_Plus1[1] = &auwActiveTerm_Plus1[1][0],
         .pauwTermToCheck[0] = &auwTermToCheck[0][0],
         .pauwTermToCheck[1] = &auwTermToCheck[1][0],
         .eCOG = OUTPUT_GROUP__FIRE_EQ,
         .ucFirstIndex = COG_FIRE_EQ__LMP_FIRE,
         .ucLastIndex = NUM_COG_FIRE_EQ,
   };
   UpdateProgrammableOutputGroup(&stOutputGroupCtrl);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableOutputGroup_Insp()
{
   static uint16_t auwActiveTerm_Plus1[MAX_TERMINALS_PER_OUTPUT_FUNCTION][NUM_COG_INSP];
   static uint16_t auwTermToCheck[MAX_TERMINALS_PER_OUTPUT_FUNCTION][NUM_COG_INSP];
   static st_output_group_ctrl stOutputGroupCtrl =
   {
         .pauwActiveTerm_Plus1[0] = &auwActiveTerm_Plus1[0][0],
         .pauwActiveTerm_Plus1[1] = &auwActiveTerm_Plus1[1][0],
         .pauwTermToCheck[0] = &auwTermToCheck[0][0],
         .pauwTermToCheck[1] = &auwTermToCheck[1][0],
         .eCOG = OUTPUT_GROUP__INSP,
         .ucFirstIndex = COG_INSP__LMP_INSP,
         .ucLastIndex = NUM_COG_INSP,
   };
   UpdateProgrammableOutputGroup(&stOutputGroupCtrl);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableOutputGroup_Ctrl()
{
   static uint16_t auwActiveTerm_Plus1[MAX_TERMINALS_PER_OUTPUT_FUNCTION][NUM_COG_CTRL];
   static uint16_t auwTermToCheck[MAX_TERMINALS_PER_OUTPUT_FUNCTION][NUM_COG_CTRL];
   static st_output_group_ctrl stOutputGroupCtrl =
   {
         .pauwActiveTerm_Plus1[0] = &auwActiveTerm_Plus1[0][0],
         .pauwActiveTerm_Plus1[1] = &auwActiveTerm_Plus1[1][0],
         .pauwTermToCheck[0] = &auwTermToCheck[0][0],
         .pauwTermToCheck[1] = &auwTermToCheck[1][0],
         .eCOG = OUTPUT_GROUP__CTRL,
         .ucFirstIndex = COG_CTRL__LIGHT_FAN,
         .ucLastIndex = NUM_COG_CTRL,
   };
   UpdateProgrammableOutputGroup(&stOutputGroupCtrl);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableOutputGroup_Safety()
{
   static uint16_t auwActiveTerm_Plus1[MAX_TERMINALS_PER_OUTPUT_FUNCTION][NUM_COG_SAFETY];
   static uint16_t auwTermToCheck[MAX_TERMINALS_PER_OUTPUT_FUNCTION][NUM_COG_SAFETY];
   static st_output_group_ctrl stOutputGroupCtrl =
   {
         .pauwActiveTerm_Plus1[0] = &auwActiveTerm_Plus1[0][0],
         .pauwActiveTerm_Plus1[1] = &auwActiveTerm_Plus1[1][0],
         .pauwTermToCheck[0] = &auwTermToCheck[0][0],
         .pauwTermToCheck[1] = &auwTermToCheck[1][0],
         .eCOG = OUTPUT_GROUP__SAFETY,
         .ucFirstIndex = COG_SAFETY__LMP_FLOOD,
         .ucLastIndex = NUM_COG_SAFETY,
   };
   UpdateProgrammableOutputGroup(&stOutputGroupCtrl);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableOutputGroup_CarCallLamp_Front()
{
   uint16_t uwTerminal;
   uint8_t bValue, bInvert;
   enum en_output_functions eOutput;
   static st_output_group_ctrl stOutputGroupCtrl =
   {
         .eCOG = OUTPUT_GROUP__CCL_F,
         .ucFirstIndex = 0,
   };
   stOutputGroupCtrl.pauwActiveTerm_Plus1[0] = pauwActiveTerm_Plus1_CCL_Front[0];
   stOutputGroupCtrl.pauwActiveTerm_Plus1[1] = pauwActiveTerm_Plus1_CCL_Front[1];
   stOutputGroupCtrl.pauwTermToCheck[0] = pauwTermToCheck_CCL_Front[0];
   stOutputGroupCtrl.pauwTermToCheck[1] = pauwTermToCheck_CCL_Front[1];
   stOutputGroupCtrl.ucLastIndex = GetFP_NumFloors();

   for ( uint8_t ucIndex = stOutputGroupCtrl.ucFirstIndex; ucIndex < stOutputGroupCtrl.ucLastIndex; ucIndex++ )
   {
      bValue = gpastCarCalls_F[ ucIndex ].bLamp;
      for( uint8_t ucSource = 0; ucSource < MAX_TERMINALS_PER_OUTPUT_FUNCTION; ucSource++ )//Functions can be assigned to up to 2 Terms
      {
         if( stOutputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex] )
         {
            uwTerminal = stOutputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex]-1;
            //Check that Term is still set
            if( CheckIf_TermProgrammedAsGroupFunc( uwTerminal, stOutputGroupCtrl.eCOG, ucIndex ) )
            {
               bInvert = ( Param_ReadValue_16Bit( enPARAM16__MR_Outputs_601 + uwTerminal ) & MASK_IO_PARAM_TERMINAL_INVERSION ) != 0;
               Sys_Bit_Set( &auiBF_RawTerminalOutputs, uwTerminal, bValue ^ bInvert );
            }
            else
            {
               stOutputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex] = 0;
            }
         }
         else // Check a new terminal for specified function
         {
            if( ++stOutputGroupCtrl.pauwTermToCheck[ucSource][ucIndex] >= MAX_NUM_IO_TERMINALS )
            {
               stOutputGroupCtrl.pauwTermToCheck[ucSource][ucIndex] = 0;
            }
            uint16_t uwTerminal = stOutputGroupCtrl.pauwTermToCheck[ucSource][ucIndex];
            if( CheckIf_TermProgrammedAsGroupFunc( uwTerminal, stOutputGroupCtrl.eCOG, ucIndex ) )
            {
               bInvert = ( Param_ReadValue_16Bit( enPARAM16__MR_Outputs_601 + uwTerminal ) & MASK_IO_PARAM_TERMINAL_INVERSION ) != 0;
               Sys_Bit_Set( &auiBF_RawTerminalOutputs, uwTerminal, bValue ^ bInvert );
               stOutputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex] = uwTerminal + 1;
            }
         }
      }
      /* Don't allow both active terminals to be the same */
      if( stOutputGroupCtrl.pauwActiveTerm_Plus1[0][ucIndex] == stOutputGroupCtrl.pauwActiveTerm_Plus1[1][ucIndex] )
      {
         stOutputGroupCtrl.pauwActiveTerm_Plus1[1][ucIndex] = 0;
      }
      CheckFor_InvalidTerminalAlarm( &stOutputGroupCtrl, ucIndex );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableOutputGroup_CarCallLamp_Rear()
{
   uint16_t uwTerminal;
   uint8_t bValue, bInvert;
   enum en_output_functions eOutput;
   static st_output_group_ctrl stOutputGroupCtrl =
   {
         .eCOG = OUTPUT_GROUP__CCL_R,
         .ucFirstIndex = 0,
   };
   stOutputGroupCtrl.pauwActiveTerm_Plus1[0] = pauwActiveTerm_Plus1_CCL_Rear[0];
   stOutputGroupCtrl.pauwActiveTerm_Plus1[1] = pauwActiveTerm_Plus1_CCL_Rear[1];
   stOutputGroupCtrl.pauwTermToCheck[0] = pauwTermToCheck_CCL_Rear[0];
   stOutputGroupCtrl.pauwTermToCheck[1] = pauwTermToCheck_CCL_Rear[1];
   stOutputGroupCtrl.ucLastIndex = GetFP_NumFloors();

   for ( uint8_t ucIndex = stOutputGroupCtrl.ucFirstIndex; ucIndex < stOutputGroupCtrl.ucLastIndex; ucIndex++ )
   {
      bValue = gpastCarCalls_R[ ucIndex ].bLamp;
      for( uint8_t ucSource = 0; ucSource < MAX_TERMINALS_PER_OUTPUT_FUNCTION; ucSource++ )//Functions can be assigned to up to 2 Terms
      {
         if( stOutputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex] )
         {
            uwTerminal = stOutputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex]-1;
            //Check that Term is still set
            if( CheckIf_TermProgrammedAsGroupFunc( uwTerminal, stOutputGroupCtrl.eCOG, ucIndex ) )
            {
               bInvert = ( Param_ReadValue_16Bit( enPARAM16__MR_Outputs_601 + uwTerminal ) & MASK_IO_PARAM_TERMINAL_INVERSION ) != 0;
               Sys_Bit_Set( &auiBF_RawTerminalOutputs, uwTerminal, bValue ^ bInvert );
            }
            else
            {
               stOutputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex] = 0;
            }
         }
         else // Check a new terminal for specified function
         {
            if( ++stOutputGroupCtrl.pauwTermToCheck[ucSource][ucIndex] >= MAX_NUM_IO_TERMINALS )
            {
               stOutputGroupCtrl.pauwTermToCheck[ucSource][ucIndex] = 0;
            }
            uint16_t uwTerminal = stOutputGroupCtrl.pauwTermToCheck[ucSource][ucIndex];
            if( CheckIf_TermProgrammedAsGroupFunc( uwTerminal, stOutputGroupCtrl.eCOG, ucIndex ) )
            {
               bInvert = ( Param_ReadValue_16Bit( enPARAM16__MR_Outputs_601 + uwTerminal ) & MASK_IO_PARAM_TERMINAL_INVERSION ) != 0;
               Sys_Bit_Set( &auiBF_RawTerminalOutputs, uwTerminal, bValue ^ bInvert );
               stOutputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex] = uwTerminal + 1;
            }
         }
      }
      /* Don't allow both active terminals to be the same */
      if( stOutputGroupCtrl.pauwActiveTerm_Plus1[0][ucIndex] == stOutputGroupCtrl.pauwActiveTerm_Plus1[1][ucIndex] )
      {
         stOutputGroupCtrl.pauwActiveTerm_Plus1[1][ucIndex] = 0;
      }
      CheckFor_InvalidTerminalAlarm( &stOutputGroupCtrl, ucIndex );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableOutputGroup_EPower()
{
   static uint16_t auwActiveTerm_Plus1[MAX_TERMINALS_PER_OUTPUT_FUNCTION][NUM_COG_EPWR];
   static uint16_t auwTermToCheck[MAX_TERMINALS_PER_OUTPUT_FUNCTION][NUM_COG_EPWR];
   static st_output_group_ctrl stOutputGroupCtrl =
   {
         .pauwActiveTerm_Plus1[0] = &auwActiveTerm_Plus1[0][0],
         .pauwActiveTerm_Plus1[1] = &auwActiveTerm_Plus1[1][0],
         .pauwTermToCheck[0] = &auwTermToCheck[0][0],
         .pauwTermToCheck[1] = &auwTermToCheck[1][0],
         .eCOG = OUTPUT_GROUP__EPWR,
         .ucFirstIndex = COG_EPWR__LMP_EPWR,
         .ucLastIndex = NUM_COG_EPWR,
   };
   UpdateProgrammableOutputGroup(&stOutputGroupCtrl);
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   //------------------------
   pstThisModule->uwInitialDelay_1ms = 1000;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_OUTPUT_MAPPING_1MS;

   pauwActiveTerm_Plus1_CCL_Front[0] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCL_Front[0] ) );
   pauwActiveTerm_Plus1_CCL_Front[1] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCL_Front[0] ) );

   pauwTermToCheck_CCL_Front[0] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCL_Front[0] ) );
   pauwTermToCheck_CCL_Front[1] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCL_Front[0] ) );

   if ( GetFP_RearDoors() )
   {
      pauwActiveTerm_Plus1_CCL_Rear[0] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCL_Rear[0] ) );
      pauwActiveTerm_Plus1_CCL_Rear[1] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCL_Rear[0] ) );

      pauwTermToCheck_CCL_Rear[0] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCL_Rear[0] ) );
      pauwTermToCheck_CCL_Rear[1] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCL_Rear[0] ) );
   }

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   // 1. Update mapped outputs
    Update_Outputs();

   // 2. Clear auiBF_RawTerminalOutputs
    memset(&auiBF_RawTerminalOutputs[0], 0, sizeof(auiBF_RawTerminalOutputs));

   // 3. Update general programmable output terminals
    UpdateProgrammableOutputGroup_Auto();
    UpdateProgrammableOutputGroup_DoorsFront();
    UpdateProgrammableOutputGroup_Fire();
    UpdateProgrammableOutputGroup_Insp();
    UpdateProgrammableOutputGroup_Ctrl();
    UpdateProgrammableOutputGroup_Safety();
    UpdateProgrammableOutputGroup_EPower();
    UpdateProgrammableOutputGroup_CarCallLamp_Front();
    if(GetFP_RearDoors())
    {
       UpdateProgrammableOutputGroup_DoorsRear();
       UpdateProgrammableOutputGroup_CarCallLamp_Rear();
    }

    UpdateGlobalOutputBitMap();

    //TODO remove debugging test
    if( ( Param_ReadValue_1Bit(enPARAM1__InMotionOpeningAlarm) ) )
    {
       uint_fast8_t bValue = GetOutputValue( enOUT_DO_F );
       if( GetMotion_RunFlag() && bValue && !Motion_GetRelevelingFlag()

             // Preopening
     && !(  ( ( Motion_GetMotionState() == MOTION__DECELERATING ) || ( Motion_GetMotionState() == MOTION__STOP_SEQUENCE ) ) )
     )
       {
          SetAlarm(ALM__DO_DURING_RUN);
       }
    }


    return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
