/******************************************************************************
 *
 * @file     mod_local_outputs.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "sru.h"
#include "sru_a.h"
#include "sys.h"
#include "GlobalData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_InputMapping =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MAX_TERMINALS_PER_INPUT_FUNCTION           (2)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
typedef struct
{
   uint16_t *pauwActiveTerm_Plus1[MAX_TERMINALS_PER_INPUT_FUNCTION];
   uint16_t *pauwTermToCheck[MAX_TERMINALS_PER_INPUT_FUNCTION];
   enum_input_groups eCIG;
   uint8_t ucFirstIndex;
   uint8_t ucLastIndex;
}st_input_group_ctrl;

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static uint32_t auiBF_MappedInputs[ BITMAP32_SIZE ( NUM_INPUT_FUNCTIONS ) ];
static uint32_t auiBF_RawTerminalInputs[ BITMAP32_SIZE( MAX_NUM_IO_TERMINALS ) ];

/* Flags marking if each input is programmed */
static uint32_t auiBF_ProgrammedInputs[ BITMAP32_SIZE ( NUM_INPUT_FUNCTIONS ) ];
//---------------------------------------------------------------

/* Car Call Security */
static uint32_t auiBF_CarCallSecurity[NUM_OF_DOORS][ BITMAP32_SIZE ( MAX_NUM_FLOORS ) ];
static uint16_t * pauwActiveTerm_Plus1_CCSec_Front[MAX_TERMINALS_PER_INPUT_FUNCTION];
static uint16_t * pauwActiveTerm_Plus1_CCSec_Rear[MAX_TERMINALS_PER_INPUT_FUNCTION];
static uint16_t * pauwTermToCheck_CCSec_Front[MAX_TERMINALS_PER_INPUT_FUNCTION];
static uint16_t * pauwTermToCheck_CCSec_Rear[MAX_TERMINALS_PER_INPUT_FUNCTION];

/* Car Call Buttons */
static uint16_t guwTickCounts_Current;
static uint16_t * pauwTimers_F_5ms;
static uint16_t * pauwTimers_R_5ms;
static uint16_t * pauwActiveTerm_Plus1_CCB_Front[MAX_TERMINALS_PER_INPUT_FUNCTION];
static uint16_t * pauwActiveTerm_Plus1_CCB_Rear[MAX_TERMINALS_PER_INPUT_FUNCTION];
static uint16_t * pauwTermToCheck_CCB_Front[MAX_TERMINALS_PER_INPUT_FUNCTION];
static uint16_t * pauwTermToCheck_CCB_Rear[MAX_TERMINALS_PER_INPUT_FUNCTION];

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
#define CALL_CANCEL_TIMEOUT_100MS (20)
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint32_t GetCarCallSecurityBitmap(uint8_t ucBitmapIndex, uint8_t bRear)
{
   return auiBF_CarCallSecurity[bRear][ucBitmapIndex];
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint16_t CheckForClearingCarCallCommand(void)
{
   uint16_t uwClearCallIndex_Plus1 = 0;
   static uint8_t ucLastCCB_F = 0;
   static uint8_t ucLastCCB_R = 0;
   static uint8_t bReleaseFlag_F = 0;
   static uint8_t bReleaseFlag_R = 0;
   static uint8_t ucCallCancelCounterF_100MS = 0;
   static uint8_t ucCallCancelCounterR_100MS = 0;

   if( Param_ReadValue_1Bit(enPARAM1__EnableClearCC) )
   {
      for(uint8_t i = 0; i < GetFP_NumFloors(); i++)
      {
         if( pauwTimers_F_5ms[i] )
         {
            if(!bReleaseFlag_F)
            {
               ucLastCCB_F = i + 1;
               ucCallCancelCounterF_100MS = 0;
               break;
            }
            else if(i == (ucLastCCB_F - 1))
            {
               ucLastCCB_F = 0;
               uwClearCallIndex_Plus1 = i + 1;
               bReleaseFlag_F = 0;
               pauwTimers_F_5ms[i] = 0;
               gpastCarCalls_F[i].bButton_CurrentPress = 0;
               gpastCarCalls_F[i].bLatched = 0;
            }

         }
         else if(i == (ucLastCCB_F - 1))
         {
            if(ucCallCancelCounterF_100MS++ < CALL_CANCEL_TIMEOUT_100MS)
            {
               bReleaseFlag_F = 1;
               break;
            }
            else
            {
               bReleaseFlag_F = 0;
               ucLastCCB_F = 0;
               break;
            }
         }

      }

      if( !uwClearCallIndex_Plus1 && GetFP_RearDoors() )
      {
      for(uint8_t i = 0; i < GetFP_NumFloors(); i++)
      {
         if( pauwTimers_R_5ms[i] )
         {
            if(!bReleaseFlag_R)
            {
               ucLastCCB_R = i + 1;
               ucCallCancelCounterR_100MS = 0;
               break;
            }
            else if(i == (ucLastCCB_R - 1))
            {
               uwClearCallIndex_Plus1 = i+MAX_NUM_FLOORS+1;
               bReleaseFlag_R = 0;
               pauwTimers_R_5ms[i] = 0;
               gpastCarCalls_R[i].bButton_CurrentPress = 0;
               gpastCarCalls_R[i].bLatched = 0;
            }
         }
         else if(i == ucLastCCB_R - 1)
         {
            if(ucCallCancelCounterR_100MS++ < CALL_CANCEL_TIMEOUT_100MS)
            {
               bReleaseFlag_R = 1;
               break;
            }
            else
            {
               bReleaseFlag_R = 0;
               ucLastCCB_R = 0;
               break;
            }
         }

      }
      }
   }

   return uwClearCallIndex_Plus1;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t CheckIfAllDoorInputsInitialized()
{
   static uint8_t bProgrammed = 0;

   if(!bProgrammed)
   {
      uint8_t bValid = 1;
      for(enum en_doors enDoorIndex = 0; enDoorIndex < GetFP_RearDoors()+1; enDoorIndex++)
      {
         if( GetFP_DoorType(enDoorIndex) == DOOR_TYPE__MANUAL )
         {
            if( enDoorIndex == DOOR_FRONT )
            {
               /* Also flag when swing closed contacts are programmed */
               uint8_t bAnyClosedContactsProgrammed = CheckIfInputIsProgrammed(enIN_TCL_F)
                                                   || CheckIfInputIsProgrammed(enIN_MCL_F)
                                                   || CheckIfInputIsProgrammed(enIN_BCL_F);
               if( bAnyClosedContactsProgrammed )
               {
                  bValid &= CheckIfInputIsProgrammed(enIN_DZ_F)
                         && CheckIfInputIsProgrammed(enIN_GSWF)
                         && CheckIfInputIsProgrammed(enIN_DPM_F)
                         && CheckIfInputIsProgrammed(enIN_TCL_F)
                         && CheckIfInputIsProgrammed(enIN_MCL_F)
                         && CheckIfInputIsProgrammed(enIN_BCL_F);
                  Door_SetSwingClosedContactsProgrammedFlag(DOOR_FRONT);
               }
               else
               {
                  bValid &= CheckIfInputIsProgrammed(enIN_DZ_F)
                         && CheckIfInputIsProgrammed(enIN_GSWF)
                         && CheckIfInputIsProgrammed(enIN_DPM_F);
               }
            }
            else
            {
               /* Also flag when swing closed contacts are programmed */
               uint8_t bAnyClosedContactsProgrammed = CheckIfInputIsProgrammed(enIN_TCL_R)
                                                   || CheckIfInputIsProgrammed(enIN_MCL_R)
                                                   || CheckIfInputIsProgrammed(enIN_BCL_R);
               if( bAnyClosedContactsProgrammed )
               {
                  bValid &= CheckIfInputIsProgrammed(enIN_DZ_R)
                         && CheckIfInputIsProgrammed(enIN_GSWR)
                         && CheckIfInputIsProgrammed(enIN_DPM_R)
                         && CheckIfInputIsProgrammed(enIN_TCL_R)
                         && CheckIfInputIsProgrammed(enIN_MCL_R)
                         && CheckIfInputIsProgrammed(enIN_BCL_R);
                  Door_SetSwingClosedContactsProgrammedFlag(DOOR_REAR);
               }
               else
               {
                  bValid &= CheckIfInputIsProgrammed(enIN_DZ_R)
                         && CheckIfInputIsProgrammed(enIN_GSWR)
                         && CheckIfInputIsProgrammed(enIN_DPM_R);
               }
            }
         }
         else if( ( GetFP_DoorType(enDoorIndex) == DOOR_TYPE__SWING )
               || ( GetFP_DoorType(enDoorIndex) == DOOR_TYPE__FREIGHT ) )
         {
            if( enDoorIndex == DOOR_FRONT )
            {
               /* Also flag when swing closed contacts are programmed */
               uint8_t bAnyClosedContactsProgrammed = CheckIfInputIsProgrammed(enIN_TCL_F)
                                                   || CheckIfInputIsProgrammed(enIN_MCL_F)
                                                   || CheckIfInputIsProgrammed(enIN_BCL_F);
               if( bAnyClosedContactsProgrammed )
               {
                  bValid &= CheckIfInputIsProgrammed(enIN_DZ_F)
                         && CheckIfInputIsProgrammed(enIN_TCL_F)
                         && CheckIfInputIsProgrammed(enIN_MCL_F)
                         && CheckIfInputIsProgrammed(enIN_BCL_F)
                         && CheckIfInputIsProgrammed(enIN_GSWF)
                         && CheckIfInputIsProgrammed(enIN_DOL_F)
                         && CheckIfInputIsProgrammed(enIN_DCL_F)
                         && CheckIfInputIsProgrammed(enIN_PHE_F);
                  Door_SetSwingClosedContactsProgrammedFlag(DOOR_FRONT);
               }
               else
               {
                  bValid &= CheckIfInputIsProgrammed(enIN_DZ_F)
                         && CheckIfInputIsProgrammed(enIN_GSWF)
                         && CheckIfInputIsProgrammed(enIN_DOL_F)
                         && CheckIfInputIsProgrammed(enIN_DCL_F)
                         && CheckIfInputIsProgrammed(enIN_PHE_F);
               }
            }
            else
            {
               /* Also flag when swing closed contacts are programmed */
               uint8_t bAnyClosedContactsProgrammed = CheckIfInputIsProgrammed(enIN_TCL_R)
                                                   || CheckIfInputIsProgrammed(enIN_MCL_R)
                                                   || CheckIfInputIsProgrammed(enIN_BCL_R);
               if( bAnyClosedContactsProgrammed )
               {
                  bValid &= CheckIfInputIsProgrammed(enIN_DZ_R)
                         && CheckIfInputIsProgrammed(enIN_TCL_R)
                         && CheckIfInputIsProgrammed(enIN_MCL_R)
                         && CheckIfInputIsProgrammed(enIN_BCL_R)
                         && CheckIfInputIsProgrammed(enIN_GSWR)
                         && CheckIfInputIsProgrammed(enIN_DOL_R)
                         && CheckIfInputIsProgrammed(enIN_DCL_R)
                         && CheckIfInputIsProgrammed(enIN_PHE_R);
                  Door_SetSwingClosedContactsProgrammedFlag(DOOR_REAR);
               }
               else
               {
                  bValid &= CheckIfInputIsProgrammed(enIN_DZ_R)
                         && CheckIfInputIsProgrammed(enIN_GSWR)
                         && CheckIfInputIsProgrammed(enIN_DOL_R)
                         && CheckIfInputIsProgrammed(enIN_DCL_R)
                         && CheckIfInputIsProgrammed(enIN_PHE_R);
               }
            }
         }
         else if( GetFP_DoorType(enDoorIndex) == DOOR_TYPE__AUTOMATIC )
         {
            if( enDoorIndex == DOOR_FRONT )
            {
               bValid &= CheckIfInputIsProgrammed(enIN_DZ_F)
                      && CheckIfInputIsProgrammed(enIN_GSWF)
                      && CheckIfInputIsProgrammed(enIN_DOL_F)
                      && CheckIfInputIsProgrammed(enIN_DCL_F)
                      && CheckIfInputIsProgrammed(enIN_PHE_F);
            }
            else
            {
               bValid &= CheckIfInputIsProgrammed(enIN_DZ_R)
                      && CheckIfInputIsProgrammed(enIN_GSWR)
                      && CheckIfInputIsProgrammed(enIN_DOL_R)
                      && CheckIfInputIsProgrammed(enIN_DCL_R)
                      && CheckIfInputIsProgrammed(enIN_PHE_R);
            }
         }
         else
         {
            bValid = 0;
         }
      }
      bProgrammed = bValid;
   }

   return bProgrammed;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t CheckIfInputIsProgrammed( enum en_input_functions eInput )
{
   uint8_t bProgrammed = Sys_Bit_Get_Array32( &auiBF_ProgrammedInputs[0], eInput, BITMAP32_SIZE(NUM_INPUT_FUNCTIONS) );
   if( eInput < START_INPUTS_NONFIXED )
   {
      bProgrammed = 1;
   }
   return bProgrammed;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetMachineRoomExtInputs( uint32_t uiInputs, uint8_t ucIndex )
{
   if(ucIndex < BITMAP32_SIZE(EXT_NUM_INPUTS)-1)
   {
      auiBF_MappedInputs[ucIndex] = uiInputs;
   }
   else if(ucIndex < BITMAP32_SIZE(EXT_NUM_INPUTS))
   {
      auiBF_MappedInputs[ucIndex] &= ~(0x3FF);
      auiBF_MappedInputs[ucIndex] |= uiInputs & (0x3FF);
   }
}

/*-----------------------------------------------------------------------------
   Status of SRU safe inputs encoded in third byte of their local inputs
 -----------------------------------------------------------------------------*/
static void UpdateDedicatedInputs_CTA( uint8_t ucBF )
{
   uint8_t bValue;
   //Bit 0 = enIN_CT_SW
   bValue = ucBF & 1;
   SetInputValue( enIN_CT_SW, bValue );

   //Bit 1 = enIN_ESC_HATCH
   bValue = ( ucBF >> 1 )& 1;
   SetInputValue( enIN_ESC_HATCH, bValue );

   //Bit 2 = enIN_CAR_SAFE
   bValue = ( ucBF >> 2 )& 1;
   SetInputValue( enIN_CAR_SAFE, bValue );

   //Bit 3 = enIN_CTTR
   bValue = ( ucBF >> 3 )& 1;
   SetInputValue( enIN_CTTR, bValue ^ 1 ); /* INVERT TRANSFER SWITCH */
}
/*-----------------------------------------------------------------------------
   Status of SRU safe inputs encoded in third byte of their local inputs
 -----------------------------------------------------------------------------*/
static void UpdateDedicatedInputs_COP( uint8_t ucBF )
{
   uint8_t bValue;
   //Bit 0 = enIN_HATR
   bValue = ucBF & 1;
   SetInputValue( enIN_HATR, bValue ^ 1 ); /* INVERT TRANSFER SWITCH */

   //Bit 1 = enIN_ICST
   bValue = ( ucBF >> 1 )& 1;
   SetInputValue( enIN_ICST, bValue );

   //Bit 2 = enIN_FIRE_STOP_SW
   bValue = ( ucBF >> 2 )& 1;
   SetInputValue( enIN_FIRE_STOP_SW, bValue );

   //Bit 3 = enIN_ICTR
   bValue = ( ucBF >> 3 )& 1;
   SetInputValue( enIN_ICTR, bValue ^ 1 ); /* INVERT TRANSFER SWITCH */
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetLocalInputs( uint32_t uiInputs, enum en_io_boards eBoard )
{
   uint8_t ucIndex;
   uint8_t ucByte;
   switch( eBoard )
   {
      case IO_BOARDS__MRA:
         Sys_Byte_Set_Array32(&auiBF_RawTerminalInputs[0],
                              (TERMINAL_INDEX_START__MRA/MIN_TERMINALS_PER_IO_BOARD),
                              BITMAP32_SIZE( MAX_NUM_IO_TERMINALS ),
                              uiInputs & 0xFF);
         break;
      case IO_BOARDS__CTA:
         UpdateDedicatedInputs_CTA( ( uiInputs >> 16 ) & 0xFF );
         Sys_Byte_Set_Array32(&auiBF_RawTerminalInputs[0],
                              (TERMINAL_INDEX_START__CTA/MIN_TERMINALS_PER_IO_BOARD),
                              BITMAP32_SIZE( MAX_NUM_IO_TERMINALS ),
                              uiInputs & 0xFF );
         Sys_Byte_Set_Array32(&auiBF_RawTerminalInputs[0],
                              (TERMINAL_INDEX_START__CTA/MIN_TERMINALS_PER_IO_BOARD)+1,
                              BITMAP32_SIZE( MAX_NUM_IO_TERMINALS ),
                              ( uiInputs >> 8 ) & 0xFF);
         break;
      case IO_BOARDS__COP:
         UpdateDedicatedInputs_COP( ( uiInputs >> 16 ) & 0xFF );
         Sys_Byte_Set_Array32(&auiBF_RawTerminalInputs[0],
                              (TERMINAL_INDEX_START__COP/MIN_TERMINALS_PER_IO_BOARD),
                              BITMAP32_SIZE( MAX_NUM_IO_TERMINALS ),
                              uiInputs & 0xFF );
         Sys_Byte_Set_Array32(&auiBF_RawTerminalInputs[0],
                              (TERMINAL_INDEX_START__COP/MIN_TERMINALS_PER_IO_BOARD)+1,
                              BITMAP32_SIZE( MAX_NUM_IO_TERMINALS ),
                              ( uiInputs >> 8 ) & 0xFF);
         break;
      case IO_BOARDS__RISER1:
      case IO_BOARDS__RISER2:
      case IO_BOARDS__RISER3:
      case IO_BOARDS__RISER4:
         Sys_Byte_Set_Array32(&auiBF_RawTerminalInputs[0],
                              (TERMINAL_INDEX_START__RIS/MIN_TERMINALS_PER_IO_BOARD)+(eBoard-IO_BOARDS__RISER1),
                              BITMAP32_SIZE( MAX_NUM_IO_TERMINALS ),
                              uiInputs & 0xFF );
         break;
      case IO_BOARDS__EXP1:
      case IO_BOARDS__EXP2:
      case IO_BOARDS__EXP3:
      case IO_BOARDS__EXP4:
      case IO_BOARDS__EXP5:
      case IO_BOARDS__EXP6:
      case IO_BOARDS__EXP7:
      case IO_BOARDS__EXP8:
      case IO_BOARDS__EXP9:
      case IO_BOARDS__EXP10:
      case IO_BOARDS__EXP11:
      case IO_BOARDS__EXP12:
      case IO_BOARDS__EXP13:
      case IO_BOARDS__EXP14:
      case IO_BOARDS__EXP15:
      case IO_BOARDS__EXP16:
      case IO_BOARDS__EXP17:
      case IO_BOARDS__EXP18:
      case IO_BOARDS__EXP19:
      case IO_BOARDS__EXP20:
      case IO_BOARDS__EXP21:
      case IO_BOARDS__EXP22:
      case IO_BOARDS__EXP23:
      case IO_BOARDS__EXP24:
      case IO_BOARDS__EXP25:
      case IO_BOARDS__EXP26:
      case IO_BOARDS__EXP27:
      case IO_BOARDS__EXP28:
      case IO_BOARDS__EXP29:
      case IO_BOARDS__EXP30:
      case IO_BOARDS__EXP31:
      case IO_BOARDS__EXP32:
      case IO_BOARDS__EXP33:
      case IO_BOARDS__EXP34:
      case IO_BOARDS__EXP35:
      case IO_BOARDS__EXP36:
      case IO_BOARDS__EXP37:
      case IO_BOARDS__EXP38:
      case IO_BOARDS__EXP39:
      case IO_BOARDS__EXP40:
         Sys_Byte_Set_Array32(&auiBF_RawTerminalInputs[0],
                              (TERMINAL_INDEX_START__EXP/MIN_TERMINALS_PER_IO_BOARD)+(eBoard-IO_BOARDS__EXP1),
                              BITMAP32_SIZE( MAX_NUM_IO_TERMINALS ),
                              uiInputs & 0xFF );
         break;
      default: break;
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t GetInputValue( enum en_input_functions eInput )
{
   uint8_t bValue = Sys_Bit_Get_Array32( auiBF_MappedInputs,
                                         eInput,
                                         NUM_ARRAY_ELEMENTS( auiBF_MappedInputs ) );
   return bValue;
}

/*-----------------------------------------------------------------------------


 -----------------------------------------------------------------------------*/
void SetInputValue( enum en_input_functions eInput, uint8_t bValue )
{
   Sys_Bit_Set_Array32( auiBF_MappedInputs,
                               eInput,
                               NUM_ARRAY_ELEMENTS( auiBF_MappedInputs ),
                               bValue
                             );
}

/*-----------------------------------------------------------------------------


 -----------------------------------------------------------------------------*/
static void UpdateGlobalInputBitMap(void)
{
   for(uint8_t i = 0; i < NUM_ARRAY_ELEMENTS( auiBF_MappedInputs ); i++)
   {
      if(GetInputBitMap(i) != auiBF_MappedInputs[i])
      {
         SetInputBitMap(auiBF_MappedInputs[i], i);
      }
   }
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t GetCarCallEnable( uint8_t ucFloorIndex, uint8_t bRearDoor)
{
   uint8_t bValue = 0;
   if( ucFloorIndex < GetFP_NumFloors() )
   {
      if(!bRearDoor)
      {
         bValue = Sys_Bit_Get( &auiBF_CarCallSecurity[DOOR_FRONT][0], ucFloorIndex );
      }
      else if( bRearDoor && GetFP_RearDoors() )
      {
         bValue = Sys_Bit_Get( &auiBF_CarCallSecurity[DOOR_REAR][0], ucFloorIndex );
      }
   }
   return bValue;
}

/*-----------------------------------------------------------------------------
   Returns 1 if the given Term is programmed with the arguement function.

   Terms programmed in EEPROM as follows:
      Group Index:      Bit [8:15]
      Inversion:        Bit [7]
      Function Index:   Bit [0:6]
 -----------------------------------------------------------------------------*/
static uint8_t CheckIf_TermProgrammedAsGroupFunc( uint16_t uwTerm, enum_input_groups eCIG, uint8_t ucFunctionIndex )
{
   uint8_t bProgrammed = 0;
   if( ( uwTerm < MAX_NUM_IO_TERMINALS )
    && ( eCIG && ( eCIG < NUM_INPUT_GROUPS ) )
    && ( ucFunctionIndex < SysIO_GetSizeOfCIG(eCIG) ) )
   {
      uint16_t uwParam = Param_ReadValue_16Bit( enPARAM16__MR_Inputs_501 + uwTerm );
      if( ( eCIG == ( ( uwParam >> 8 ) & 0xFF ) )
       && ( ucFunctionIndex == ( uwParam & 0x7F ) ) )
      {
         bProgrammed = 1;
      }
   }
   return bProgrammed;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckFor_InvalidTerminalAlarm( st_input_group_ctrl *pstInputGroupCtrl, uint8_t ucFunctionIndex )
{
   uint16_t uwTerminal;
   /* If two active terminals have been detected, check for third and set alarm if found */
   if( pstInputGroupCtrl->pauwActiveTerm_Plus1[0][ucFunctionIndex]
    && pstInputGroupCtrl->pauwActiveTerm_Plus1[1][ucFunctionIndex] )
   {
      if( ++pstInputGroupCtrl->pauwTermToCheck[0][ucFunctionIndex] >= MAX_NUM_IO_TERMINALS )
      {
         pstInputGroupCtrl->pauwTermToCheck[0][ucFunctionIndex] = 0;
      }
      uwTerminal = pstInputGroupCtrl->pauwTermToCheck[0][ucFunctionIndex];
      if( ( uwTerminal != pstInputGroupCtrl->pauwActiveTerm_Plus1[0][ucFunctionIndex]-1 )
       && ( uwTerminal != pstInputGroupCtrl->pauwActiveTerm_Plus1[1][ucFunctionIndex]-1 )
       && CheckIf_TermProgrammedAsGroupFunc( uwTerminal, pstInputGroupCtrl->eCIG, ucFunctionIndex ) )
      {
         en_alarms eAlarm = ALM__DUPLICATE_MR_501 + uwTerminal;
         if(eAlarm > ALM__DUPLICATE_EXP40_508)
         {
            eAlarm = ALM__DUPLICATE_EXP40_508;
         }
         SetAlarm(eAlarm);
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableInputGroup( st_input_group_ctrl *pstInputGroupCtrl )
{
   uint16_t uwTerminal, uwFixedTerminal_Plus1;
   uint8_t bValue, bInvert, bPin;
   enum en_input_functions eInput;
   for ( uint8_t ucIndex = pstInputGroupCtrl->ucFirstIndex; ucIndex < pstInputGroupCtrl->ucLastIndex; ucIndex++ )
   {
      /* Clear flag marking inputs that have been programmed */
      eInput = SysIO_GetInputFunctionFromCIG( pstInputGroupCtrl->eCIG, ucIndex );
      Sys_Bit_Set_Array32( &auiBF_ProgrammedInputs[0], eInput, BITMAP32_SIZE( NUM_INPUT_FUNCTIONS ), 0 );

      /* Check for programmed terminals & set mapped input function */
      bValue = 0;
      for( uint8_t ucSource = 0; ucSource < MAX_TERMINALS_PER_INPUT_FUNCTION; ucSource++ )//Functions can be assigned to up to 2 Terms
      {
         uwFixedTerminal_Plus1 = SysIO_GetFixedTerminalFromCIG_Plus1( pstInputGroupCtrl->eCIG, ucIndex );
         if( uwFixedTerminal_Plus1 )
         {
            uwTerminal =  (uwFixedTerminal_Plus1 & ~MASK_INVERT_FIXED_TERMINAL_PLUS1 ) - 1;
            bInvert = ( uwFixedTerminal_Plus1 & MASK_INVERT_FIXED_TERMINAL_PLUS1 ) != 0;
            bValue = Sys_Bit_Get( &auiBF_RawTerminalInputs[0], uwTerminal ) ^ bInvert;
            Sys_Bit_Set_Array32( &auiBF_ProgrammedInputs[0], eInput, BITMAP32_SIZE( NUM_INPUT_FUNCTIONS ), 1 );
            break;
         }
         else if( pstInputGroupCtrl->pauwActiveTerm_Plus1[ucSource][ucIndex] )
         {
            uwTerminal = pstInputGroupCtrl->pauwActiveTerm_Plus1[ucSource][ucIndex]-1;
            //Check that Term is still set
            if( CheckIf_TermProgrammedAsGroupFunc( uwTerminal, pstInputGroupCtrl->eCIG, ucIndex ) )
            {
               bInvert = ( Param_ReadValue_16Bit( enPARAM16__MR_Inputs_501 + uwTerminal ) & MASK_IO_PARAM_TERMINAL_INVERSION ) != 0;
               bPin = Sys_Bit_Get( &auiBF_RawTerminalInputs[0], uwTerminal ) ^ bInvert;
               bValue |= bPin;
               Sys_Bit_Set_Array32( &auiBF_ProgrammedInputs[0], eInput, BITMAP32_SIZE( NUM_INPUT_FUNCTIONS ), 1 );
            }
            else
            {
               pstInputGroupCtrl->pauwActiveTerm_Plus1[ucSource][ucIndex] = 0;
            }
         }
         else // Check a new terminal for specified function
         {
            if( ++pstInputGroupCtrl->pauwTermToCheck[ucSource][ucIndex] >= MAX_NUM_IO_TERMINALS )
            {
               pstInputGroupCtrl->pauwTermToCheck[ucSource][ucIndex] = 0;
            }
            uwTerminal = pstInputGroupCtrl->pauwTermToCheck[ucSource][ucIndex];
            if( CheckIf_TermProgrammedAsGroupFunc( uwTerminal, pstInputGroupCtrl->eCIG, ucIndex ) )
            {
               bInvert = ( Param_ReadValue_16Bit( enPARAM16__MR_Inputs_501 + uwTerminal ) & MASK_IO_PARAM_TERMINAL_INVERSION ) != 0;
               bPin = Sys_Bit_Get( &auiBF_RawTerminalInputs[0], uwTerminal ) ^ bInvert;
               bValue |= bPin;
               pstInputGroupCtrl->pauwActiveTerm_Plus1[ucSource][ucIndex] = uwTerminal + 1;
               Sys_Bit_Set_Array32( &auiBF_ProgrammedInputs[0], eInput, BITMAP32_SIZE( NUM_INPUT_FUNCTIONS ), 1 );
            }
         }
      }
      SetInputValue( eInput, bValue );
      /* Don't allow both active terminals to be the same */
      if( pstInputGroupCtrl->pauwActiveTerm_Plus1[0][ucIndex] == pstInputGroupCtrl->pauwActiveTerm_Plus1[1][ucIndex] )
      {
         pstInputGroupCtrl->pauwActiveTerm_Plus1[1][ucIndex] = 0;
      }
      CheckFor_InvalidTerminalAlarm( pstInputGroupCtrl, ucIndex );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableInputGroup_Auto()
{
   static uint16_t auwActiveTerm_Plus1[MAX_TERMINALS_PER_INPUT_FUNCTION][NUM_CIG_AUTO_OPER];
   static uint16_t auwTermToCheck[MAX_TERMINALS_PER_INPUT_FUNCTION][NUM_CIG_AUTO_OPER];
   static st_input_group_ctrl stInputGroupCtrl =
   {
         .pauwActiveTerm_Plus1[0] = &auwActiveTerm_Plus1[0][0],
         .pauwActiveTerm_Plus1[1] = &auwActiveTerm_Plus1[1][0],
         .pauwTermToCheck[0] = &auwTermToCheck[0][0],
         .pauwTermToCheck[1] = &auwTermToCheck[1][0],
         .eCIG = INPUT_GROUP__AUTO_OP,
         .ucFirstIndex = CIG_AUTO_OPER__INDP,
         .ucLastIndex = NUM_CIG_AUTO_OPER,
   };
   UpdateProgrammableInputGroup(&stInputGroupCtrl);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableInputGroup_DoorsFront()
{
   static uint16_t auwActiveTerm_Plus1[MAX_TERMINALS_PER_INPUT_FUNCTION][NUM_CIG_DOORS];
   static uint16_t auwTermToCheck[MAX_TERMINALS_PER_INPUT_FUNCTION][NUM_CIG_DOORS];
   static st_input_group_ctrl stInputGroupCtrl =
   {
         .pauwActiveTerm_Plus1[0] = &auwActiveTerm_Plus1[0][0],
         .pauwActiveTerm_Plus1[1] = &auwActiveTerm_Plus1[1][0],
         .pauwTermToCheck[0] = &auwTermToCheck[0][0],
         .pauwTermToCheck[1] = &auwTermToCheck[1][0],
         .eCIG = INPUT_GROUP__DOORS_F,
         .ucFirstIndex = CIG_DOORS__GSW,
         .ucLastIndex = NUM_CIG_DOORS,
   };
   UpdateProgrammableInputGroup(&stInputGroupCtrl);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableInputGroup_DoorsRear()
{
   static uint16_t auwActiveTerm_Plus1[MAX_TERMINALS_PER_INPUT_FUNCTION][NUM_CIG_DOORS];
   static uint16_t auwTermToCheck[MAX_TERMINALS_PER_INPUT_FUNCTION][NUM_CIG_DOORS];
   static st_input_group_ctrl stInputGroupCtrl =
   {
         .pauwActiveTerm_Plus1[0] = &auwActiveTerm_Plus1[0][0],
         .pauwActiveTerm_Plus1[1] = &auwActiveTerm_Plus1[1][0],
         .pauwTermToCheck[0] = &auwTermToCheck[0][0],
         .pauwTermToCheck[1] = &auwTermToCheck[1][0],
         .eCIG = INPUT_GROUP__DOORS_R,
         .ucFirstIndex = CIG_DOORS__GSW,
         .ucLastIndex = NUM_CIG_DOORS,
   };
   UpdateProgrammableInputGroup(&stInputGroupCtrl);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableInputGroup_Fire()
{
   static uint16_t auwActiveTerm_Plus1[MAX_TERMINALS_PER_INPUT_FUNCTION][NUM_CIG_FIRE_EQ];
   static uint16_t auwTermToCheck[MAX_TERMINALS_PER_INPUT_FUNCTION][NUM_CIG_FIRE_EQ];
   static st_input_group_ctrl stInputGroupCtrl =
   {
         .pauwActiveTerm_Plus1[0] = &auwActiveTerm_Plus1[0][0],
         .pauwActiveTerm_Plus1[1] = &auwActiveTerm_Plus1[1][0],
         .pauwTermToCheck[0] = &auwTermToCheck[0][0],
         .pauwTermToCheck[1] = &auwTermToCheck[1][0],
         .eCIG = INPUT_GROUP__FIRE,
         .ucFirstIndex = CIG_FIRE_EQ__SMOKE_HA,
         .ucLastIndex = NUM_CIG_FIRE_EQ,
   };
   UpdateProgrammableInputGroup(&stInputGroupCtrl);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableInputGroup_Insp()
{
   static uint16_t auwActiveTerm_Plus1[MAX_TERMINALS_PER_INPUT_FUNCTION][NUM_CIG_INSP];
   static uint16_t auwTermToCheck[MAX_TERMINALS_PER_INPUT_FUNCTION][NUM_CIG_INSP];
   static st_input_group_ctrl stInputGroupCtrl =
   {
         .pauwActiveTerm_Plus1[0] = &auwActiveTerm_Plus1[0][0],
         .pauwActiveTerm_Plus1[1] = &auwActiveTerm_Plus1[1][0],
         .pauwTermToCheck[0] = &auwTermToCheck[0][0],
         .pauwTermToCheck[1] = &auwTermToCheck[1][0],
         .eCIG = INPUT_GROUP__INSP,
         .ucFirstIndex = CIG_INSP__IP_TR,
         .ucLastIndex = NUM_CIG_INSP,
   };
   UpdateProgrammableInputGroup(&stInputGroupCtrl);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableInputGroup_Ctrl()
{
   static uint16_t auwActiveTerm_Plus1[MAX_TERMINALS_PER_INPUT_FUNCTION][NUM_CIG_CTRL];
   static uint16_t auwTermToCheck[MAX_TERMINALS_PER_INPUT_FUNCTION][NUM_CIG_CTRL];
   static st_input_group_ctrl stInputGroupCtrl =
   {
         .pauwActiveTerm_Plus1[0] = &auwActiveTerm_Plus1[0][0],
         .pauwActiveTerm_Plus1[1] = &auwActiveTerm_Plus1[1][0],
         .pauwTermToCheck[0] = &auwTermToCheck[0][0],
         .pauwTermToCheck[1] = &auwTermToCheck[1][0],
         .eCIG = INPUT_GROUP__CTRL,
         .ucFirstIndex = CIG_CTRL__UNUSED0,
         .ucLastIndex = NUM_CIG_CTRL,
   };
   UpdateProgrammableInputGroup(&stInputGroupCtrl);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableInputGroup_Safety()
{
   static uint16_t auwActiveTerm_Plus1[MAX_TERMINALS_PER_INPUT_FUNCTION][NUM_CIG_SAFETY];
   static uint16_t auwTermToCheck[MAX_TERMINALS_PER_INPUT_FUNCTION][NUM_CIG_SAFETY];
   static st_input_group_ctrl stInputGroupCtrl =
   {
         .pauwActiveTerm_Plus1[0] = &auwActiveTerm_Plus1[0][0],
         .pauwActiveTerm_Plus1[1] = &auwActiveTerm_Plus1[1][0],
         .pauwTermToCheck[0] = &auwTermToCheck[0][0],
         .pauwTermToCheck[1] = &auwTermToCheck[1][0],
         .eCIG = INPUT_GROUP__SAFETY,
         .ucFirstIndex = CIG_SAFETY__OVER_LOAD,
         .ucLastIndex = NUM_CIG_SAFETY,
   };
   UpdateProgrammableInputGroup(&stInputGroupCtrl);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableInputGroup_CarCallSecurity_Front()
{
   static st_input_group_ctrl stInputGroupCtrl =
   {
         .eCIG = INPUT_GROUP__CC_ENABLE_F,
         .ucFirstIndex = 0,
   };
   stInputGroupCtrl.pauwActiveTerm_Plus1[0] = pauwActiveTerm_Plus1_CCSec_Front[0];
   stInputGroupCtrl.pauwActiveTerm_Plus1[1] = pauwActiveTerm_Plus1_CCSec_Front[1];
   stInputGroupCtrl.pauwTermToCheck[0] = pauwTermToCheck_CCSec_Front[0];
   stInputGroupCtrl.pauwTermToCheck[1] = pauwTermToCheck_CCSec_Front[1];
   stInputGroupCtrl.ucLastIndex = GetFP_NumFloors();

   for ( uint8_t ucIndex = stInputGroupCtrl.ucFirstIndex; ucIndex < stInputGroupCtrl.ucLastIndex; ucIndex++ )
   {
      uint8_t bValue = 0;
      for( uint8_t ucSource = 0; ucSource < MAX_TERMINALS_PER_INPUT_FUNCTION; ucSource++ )//Functions can be assigned to up to 2 Terms
      {
         if( stInputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex] )
         {
            uint16_t uwTerminal = stInputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex]-1;
            //Check that Term is still set
            if( CheckIf_TermProgrammedAsGroupFunc( uwTerminal, stInputGroupCtrl.eCIG, ucIndex ) )
            {
               uint8_t bInvert = ( Param_ReadValue_16Bit( enPARAM16__MR_Inputs_501 + uwTerminal ) & MASK_IO_PARAM_TERMINAL_INVERSION ) != 0;
               uint8_t bPin = Sys_Bit_Get( &auiBF_RawTerminalInputs[0], uwTerminal ) ^ bInvert;
               bValue |= bPin;
            }
            else
            {
               stInputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex] = 0;
            }
         }
         else // Check a new terminal for specified function
         {
            if( ++stInputGroupCtrl.pauwTermToCheck[ucSource][ucIndex] >= MAX_NUM_IO_TERMINALS )
            {
               stInputGroupCtrl.pauwTermToCheck[ucSource][ucIndex] = 0;
            }
            uint16_t uwTerminal = stInputGroupCtrl.pauwTermToCheck[ucSource][ucIndex];
            if( CheckIf_TermProgrammedAsGroupFunc( uwTerminal, stInputGroupCtrl.eCIG, ucIndex ) )
            {
               uint8_t bInvert = ( Param_ReadValue_16Bit( enPARAM16__MR_Inputs_501 + uwTerminal ) & MASK_IO_PARAM_TERMINAL_INVERSION ) != 0;
               uint8_t bPin = Sys_Bit_Get( &auiBF_RawTerminalInputs[0], uwTerminal ) ^ bInvert;
               bValue |= bPin;
               stInputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex] = uwTerminal + 1;
            }
         }
      }
      Sys_Bit_Set( &auiBF_CarCallSecurity[DOOR_FRONT][0], ucIndex, bValue );
      /* Don't allow both active terminals to be the same */
      if( stInputGroupCtrl.pauwActiveTerm_Plus1[0][ucIndex] == stInputGroupCtrl.pauwActiveTerm_Plus1[1][ucIndex] )
      {
         stInputGroupCtrl.pauwActiveTerm_Plus1[1][ucIndex] = 0;
      }
      CheckFor_InvalidTerminalAlarm( &stInputGroupCtrl, ucIndex );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableInputGroup_CarCallButton_Front()
{
   static st_input_group_ctrl stInputGroupCtrl =
   {
         .eCIG = INPUT_GROUP__CCB_F,
         .ucFirstIndex = 0,
   };
   stInputGroupCtrl.pauwActiveTerm_Plus1[0] = pauwActiveTerm_Plus1_CCB_Front[0];
   stInputGroupCtrl.pauwActiveTerm_Plus1[1] = pauwActiveTerm_Plus1_CCB_Front[1];
   stInputGroupCtrl.pauwTermToCheck[0] = pauwTermToCheck_CCB_Front[0];
   stInputGroupCtrl.pauwTermToCheck[1] = pauwTermToCheck_CCB_Front[1];
   stInputGroupCtrl.ucLastIndex = GetFP_NumFloors();

   for ( uint8_t ucIndex = stInputGroupCtrl.ucFirstIndex; ucIndex < stInputGroupCtrl.ucLastIndex; ucIndex++ )
   {
      uint8_t bValue = 0;
      for( uint8_t ucSource = 0; ucSource < MAX_TERMINALS_PER_INPUT_FUNCTION; ucSource++ )//Functions can be assigned to up to 2 Terms
      {
         if( stInputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex] )
         {
            uint16_t uwTerminal = stInputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex]-1;
            //Check that Term is still set
            if( CheckIf_TermProgrammedAsGroupFunc( uwTerminal, stInputGroupCtrl.eCIG, ucIndex ) )
            {
               uint8_t bInvert = ( Param_ReadValue_16Bit( enPARAM16__MR_Inputs_501 + uwTerminal ) & MASK_IO_PARAM_TERMINAL_INVERSION ) != 0;
               uint8_t bPin = Sys_Bit_Get( &auiBF_RawTerminalInputs[0], uwTerminal ) ^ bInvert;
               bValue |= bPin;
            }
            else
            {
               stInputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex] = 0;
            }
         }
         else // Check a new terminal for specified function
         {
            if( ++stInputGroupCtrl.pauwTermToCheck[ucSource][ucIndex] >= MAX_NUM_IO_TERMINALS )
            {
               stInputGroupCtrl.pauwTermToCheck[ucSource][ucIndex] = 0;
            }
            uint16_t uwTerminal = stInputGroupCtrl.pauwTermToCheck[ucSource][ucIndex];
            if( CheckIf_TermProgrammedAsGroupFunc( uwTerminal, stInputGroupCtrl.eCIG, ucIndex ) )
            {
               uint8_t bInvert = ( Param_ReadValue_16Bit( enPARAM16__MR_Inputs_501 + uwTerminal ) & MASK_IO_PARAM_TERMINAL_INVERSION ) != 0;
               uint8_t bPin = Sys_Bit_Get( &auiBF_RawTerminalInputs[0], uwTerminal ) ^ bInvert;
               bValue |= bPin;
               stInputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex] = uwTerminal + 1;
            }
         }
      }
      if(bValue)
      {
         pauwTimers_F_5ms[ucIndex] = guwTickCounts_Current;
      }
      /* Don't allow both active terminals to be the same */
      if( stInputGroupCtrl.pauwActiveTerm_Plus1[0][ucIndex] == stInputGroupCtrl.pauwActiveTerm_Plus1[1][ucIndex] )
      {
         stInputGroupCtrl.pauwActiveTerm_Plus1[1][ucIndex] = 0;
      }
      CheckFor_InvalidTerminalAlarm( &stInputGroupCtrl, ucIndex );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableInputGroup_CarCallSecurity_Rear()
{
   static st_input_group_ctrl stInputGroupCtrl =
   {
         .eCIG = INPUT_GROUP__CC_ENABLE_R,
         .ucFirstIndex = 0,
   };
   stInputGroupCtrl.pauwActiveTerm_Plus1[0] = pauwActiveTerm_Plus1_CCSec_Rear[0];
   stInputGroupCtrl.pauwActiveTerm_Plus1[1] = pauwActiveTerm_Plus1_CCSec_Rear[1];
   stInputGroupCtrl.pauwTermToCheck[0] = pauwTermToCheck_CCSec_Rear[0];
   stInputGroupCtrl.pauwTermToCheck[1] = pauwTermToCheck_CCSec_Rear[1];
   stInputGroupCtrl.ucLastIndex = GetFP_NumFloors();

   for ( uint8_t ucIndex = stInputGroupCtrl.ucFirstIndex; ucIndex < stInputGroupCtrl.ucLastIndex; ucIndex++ )
   {
      uint8_t bValue = 0;
      for( uint8_t ucSource = 0; ucSource < MAX_TERMINALS_PER_INPUT_FUNCTION; ucSource++ )//Functions can be assigned to up to 2 Terms
      {
         if( stInputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex] )
         {
            uint16_t uwTerminal = stInputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex]-1;
            //Check that Term is still set
            if( CheckIf_TermProgrammedAsGroupFunc( uwTerminal, stInputGroupCtrl.eCIG, ucIndex ) )
            {
               uint8_t bInvert = ( Param_ReadValue_16Bit( enPARAM16__MR_Inputs_501 + uwTerminal ) & MASK_IO_PARAM_TERMINAL_INVERSION ) != 0;
               uint8_t bPin = Sys_Bit_Get( &auiBF_RawTerminalInputs[0], uwTerminal ) ^ bInvert;
               bValue |= bPin;
            }
            else
            {
               stInputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex] = 0;
            }
         }
         else // Check a new terminal for specified function
         {
            if( ++stInputGroupCtrl.pauwTermToCheck[ucSource][ucIndex] >= MAX_NUM_IO_TERMINALS )
            {
               stInputGroupCtrl.pauwTermToCheck[ucSource][ucIndex] = 0;
            }
            uint16_t uwTerminal = stInputGroupCtrl.pauwTermToCheck[ucSource][ucIndex];
            if( CheckIf_TermProgrammedAsGroupFunc( uwTerminal, stInputGroupCtrl.eCIG, ucIndex ) )
            {
               uint8_t bInvert = ( Param_ReadValue_16Bit( enPARAM16__MR_Inputs_501 + uwTerminal ) & MASK_IO_PARAM_TERMINAL_INVERSION ) != 0;
               uint8_t bPin = Sys_Bit_Get( &auiBF_RawTerminalInputs[0], uwTerminal ) ^ bInvert;
               bValue |= bPin;
               stInputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex] = uwTerminal + 1;
            }
         }
      }
      Sys_Bit_Set( &auiBF_CarCallSecurity[DOOR_REAR][0], ucIndex, bValue );
      /* Don't allow both active terminals to be the same */
      if( stInputGroupCtrl.pauwActiveTerm_Plus1[0][ucIndex] == stInputGroupCtrl.pauwActiveTerm_Plus1[1][ucIndex] )
      {
         stInputGroupCtrl.pauwActiveTerm_Plus1[1][ucIndex] = 0;
      }
      CheckFor_InvalidTerminalAlarm( &stInputGroupCtrl, ucIndex );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableInputGroup_CarCallButton_Rear()
{
   static st_input_group_ctrl stInputGroupCtrl =
   {
         .eCIG = INPUT_GROUP__CCB_R,
         .ucFirstIndex = 0,
   };
   stInputGroupCtrl.pauwActiveTerm_Plus1[0] = pauwActiveTerm_Plus1_CCB_Rear[0];
   stInputGroupCtrl.pauwActiveTerm_Plus1[1] = pauwActiveTerm_Plus1_CCB_Rear[1];
   stInputGroupCtrl.pauwTermToCheck[0] = pauwTermToCheck_CCB_Rear[0];
   stInputGroupCtrl.pauwTermToCheck[1] = pauwTermToCheck_CCB_Rear[1];
   stInputGroupCtrl.ucLastIndex = GetFP_NumFloors();

   for ( uint8_t ucIndex = stInputGroupCtrl.ucFirstIndex; ucIndex < stInputGroupCtrl.ucLastIndex; ucIndex++ )
   {
      uint8_t bValue = 0;
      for( uint8_t ucSource = 0; ucSource < MAX_TERMINALS_PER_INPUT_FUNCTION; ucSource++ )//Functions can be assigned to up to 2 Terms
      {
         if( stInputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex] )
         {
            uint16_t uwTerminal = stInputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex]-1;
            //Check that Term is still set
            if( CheckIf_TermProgrammedAsGroupFunc( uwTerminal, stInputGroupCtrl.eCIG, ucIndex ) )
            {
               uint8_t bInvert = ( Param_ReadValue_16Bit( enPARAM16__MR_Inputs_501 + uwTerminal ) & MASK_IO_PARAM_TERMINAL_INVERSION ) != 0;
               uint8_t bPin = Sys_Bit_Get( &auiBF_RawTerminalInputs[0], uwTerminal ) ^ bInvert;
               bValue |= bPin;
            }
            else
            {
               stInputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex] = 0;
            }
         }
         else // Check a new terminal for specified function
         {
            if( ++stInputGroupCtrl.pauwTermToCheck[ucSource][ucIndex] >= MAX_NUM_IO_TERMINALS )
            {
               stInputGroupCtrl.pauwTermToCheck[ucSource][ucIndex] = 0;
            }
            uint16_t uwTerminal = stInputGroupCtrl.pauwTermToCheck[ucSource][ucIndex];
            if( CheckIf_TermProgrammedAsGroupFunc( uwTerminal, stInputGroupCtrl.eCIG, ucIndex ) )
            {
               uint8_t bInvert = ( Param_ReadValue_16Bit( enPARAM16__MR_Inputs_501 + uwTerminal ) & MASK_IO_PARAM_TERMINAL_INVERSION ) != 0;
               uint8_t bPin = Sys_Bit_Get( &auiBF_RawTerminalInputs[0], uwTerminal ) ^ bInvert;
               bValue |= bPin;
               stInputGroupCtrl.pauwActiveTerm_Plus1[ucSource][ucIndex] = uwTerminal + 1;
            }
         }
      }
      if(bValue)
      {
         pauwTimers_R_5ms[ucIndex] = guwTickCounts_Current;
      }
      /* Don't allow both active terminals to be the same */
      if( stInputGroupCtrl.pauwActiveTerm_Plus1[0][ucIndex] == stInputGroupCtrl.pauwActiveTerm_Plus1[1][ucIndex] )
      {
         stInputGroupCtrl.pauwActiveTerm_Plus1[1][ucIndex] = 0;
      }
      CheckFor_InvalidTerminalAlarm( &stInputGroupCtrl, ucIndex );
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateProgrammableInputGroup_EPWR()
{
   static uint16_t auwActiveTerm_Plus1[MAX_TERMINALS_PER_INPUT_FUNCTION][NUM_CIG_EPWR];
   static uint16_t auwTermToCheck[MAX_TERMINALS_PER_INPUT_FUNCTION][NUM_CIG_EPWR];
   static st_input_group_ctrl stInputGroupCtrl =
   {
         .pauwActiveTerm_Plus1[0] = &auwActiveTerm_Plus1[0][0],
         .pauwActiveTerm_Plus1[1] = &auwActiveTerm_Plus1[1][0],
         .pauwTermToCheck[0] = &auwTermToCheck[0][0],
         .pauwTermToCheck[1] = &auwTermToCheck[1][0],
         .eCIG = INPUT_GROUP__EPWR,
         .ucFirstIndex = CIG_EPWR__ON,
         .ucLastIndex = NUM_CIG_EPWR,
   };
   UpdateProgrammableInputGroup(&stInputGroupCtrl);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateCCButtonPress_Front()
{
   for ( uint8_t ucFloorIndex = 0; ucFloorIndex < GetFP_NumFloors(); ucFloorIndex++ )
   {
      if ( pauwTimers_F_5ms[ ucFloorIndex ] )
      {
         gpastCarCalls_F[ ucFloorIndex ].bButton_CurrentPress = 1;
         pauwTimers_F_5ms[ ucFloorIndex ]--;
      }
      else
      {
         gpastCarCalls_F[ ucFloorIndex ].bButton_CurrentPress = 0;
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateCCButtonPress_Rear()
{
   for ( uint8_t ucFloorIndex = 0; ucFloorIndex < GetFP_NumFloors(); ucFloorIndex++ )
   {
      if ( pauwTimers_R_5ms[ ucFloorIndex ] )
      {
         gpastCarCalls_R[ ucFloorIndex ].bButton_CurrentPress = 1;
         pauwTimers_R_5ms[ ucFloorIndex ]--;
      }
      else
      {
         gpastCarCalls_R[ ucFloorIndex ].bButton_CurrentPress = 0;
      }
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   //------------------------
   pstThisModule->uwInitialDelay_1ms = 250;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_INPUT_MAPPING_1MS;

   if( Param_ReadValue_8Bit(enPARAM8__CCB_RecentPressTimer_100ms) )
   {
      guwTickCounts_Current = ( Param_ReadValue_8Bit(enPARAM8__CCB_RecentPressTimer_100ms)*100 ) / pstThisModule->uwRunPeriod_1ms;
   }
   else
   {
      guwTickCounts_Current = 100 / pstThisModule->uwRunPeriod_1ms;
   }

   pauwTimers_F_5ms = calloc( GetFP_NumFloors(), sizeof( *pauwTimers_F_5ms ) );

   pauwActiveTerm_Plus1_CCB_Front[0] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCB_Front[0] ) );
   pauwActiveTerm_Plus1_CCB_Front[1] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCB_Front[0] ) );
   pauwActiveTerm_Plus1_CCSec_Front[0] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCB_Front[0] ) );
   pauwActiveTerm_Plus1_CCSec_Front[1] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCB_Front[0] ) );

   pauwTermToCheck_CCB_Front[0] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCB_Front[0] ) );
   pauwTermToCheck_CCB_Front[1] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCB_Front[0] ) );
   pauwTermToCheck_CCSec_Front[0] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCB_Front[0] ) );
   pauwTermToCheck_CCSec_Front[1] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCB_Front[0] ) );

   if ( GetFP_RearDoors() )
   {
      pauwTimers_R_5ms = calloc( GetFP_NumFloors(), sizeof( *pauwTimers_R_5ms ) );

      pauwActiveTerm_Plus1_CCB_Rear[0] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCB_Rear[0] ) );
      pauwActiveTerm_Plus1_CCB_Rear[1] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCB_Rear[0] ) );
      pauwActiveTerm_Plus1_CCSec_Rear[0] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCB_Rear[0] ) );
      pauwActiveTerm_Plus1_CCSec_Rear[1] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCB_Rear[0] ) );

      pauwTermToCheck_CCB_Rear[0] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCB_Rear[0] ) );
      pauwTermToCheck_CCB_Rear[1] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCB_Rear[0] ) );
      pauwTermToCheck_CCSec_Rear[0] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCB_Rear[0] ) );
      pauwTermToCheck_CCSec_Rear[1] = calloc( GetFP_NumFloors(), sizeof( *pauwActiveTerm_Plus1_CCB_Rear[0] ) );
   }

   return 0;
}

/*-----------------------------------------------------------------------------

   This module reads 8 local inputs from all the boards on the car network,
   and maps them to the correct functions, retrieved from flash memory.
   Inputs are stored in memory by function, where every functions is allocated
   an 8-bit parameter, in which the deployment and Term number are stored.
   GetDeploymentAndTerm_Input() returns false if the function is not programmed,
   or true if the deployment number in non-zero. The Term number, however, is
   zero-based (in order to fit all 16 Terms in one byte).

   The input to this module is : gauiLocalInputs[]; where all the
   16 inputs of every board are stored.
   The output of this module is auiBF_MappedInputs[ BITMAP32_SIZE ( NUM_INPUT_FUNCTIONS ) ]
   This array is globally accessible to MRA. To get or set any input from any
   module, you can simply call GetInputValue( enIN_Inputx ) or SetInputValue( enIN_Inputx )
   SetInputValue() is generally only used only here to set the value after sorting
   the raw inputs, while GetInputValue() is called from outside this module to read the
   input state.

   This module is also responsible for all car call buttons, which as treated as
   a special type of input funtions because the number of inputs is per-floor.
   than the number of available parameters at the time.
   CCBs are not saved to auiBF_MappedInputs[], instead they laatch in the
   gpastCarCalls_F/R structure. If an CCBs is detected high, it is latched to
   gpastCarCalls_F/R[].bButton_CurrentPress for half a second and to
   gpastCarCalls_F/R[].bButton_RecentPress for two seconds from this module.
   bCurrentPress and bRecentPress are being used in the car call module to
   update other struct memebers such as bLatched and bLamp.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   // Note: Terminal indexes are virtual and extend between boards in the order defined in sys_io.h
   /* 1. Check each input group - TODO split  */
   UpdateProgrammableInputGroup_Auto();
   UpdateProgrammableInputGroup_DoorsFront();
   UpdateProgrammableInputGroup_Fire();
   UpdateProgrammableInputGroup_Insp();
   UpdateProgrammableInputGroup_Ctrl();
   UpdateProgrammableInputGroup_Safety();
   UpdateProgrammableInputGroup_CarCallButton_Front();
   UpdateProgrammableInputGroup_CarCallSecurity_Front();
   UpdateProgrammableInputGroup_EPWR();
   if(GetFP_RearDoors())
   {
      UpdateProgrammableInputGroup_DoorsRear();
      UpdateProgrammableInputGroup_CarCallButton_Rear();
      UpdateProgrammableInputGroup_CarCallSecurity_Rear();
   }

   /* 2. Update button press timers */
   UpdateCCButtonPress_Front();
   if(GetFP_RearDoors())
   {
      UpdateCCButtonPress_Rear();
   }

   UpdateGlobalInputBitMap();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
