/******************************************************************************
 *
 * @file     mod_brake.c
 * @brief    Primary brake
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "motion.h"
#include "acceptance.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Brake =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

struct st_brake gstBrake;
struct st_brake gstBrake2;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static const uint8_t gaucRampDownTime_10ms[ NUM_RAMP_DOWN_TIMES ] =
{
   0,
   5,
   10,
};

static uint8_t bDropEB = 0;  // Flag to drop emergency brake when BPS stuck open
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Returns 1 when ebrake (rope gripper) should be dropped
 -----------------------------------------------------------------------------*/
uint8_t Brake_GetDropEBFlag(void)
{
   return Param_ReadValue_1Bit(enPARAM1__BPSStuckOpenDropsEBrake) && ( bDropEB || GetFault_ByNumber(FLT__BPS_STUCK_OPEN) );
}
/*-----------------------------------------------------------------------------
   Return BPS status
 -----------------------------------------------------------------------------*/
uint8_t Brake_GetBPS()
{
   uint8_t bBPS = gstBrake.bBPS;
   if( CheckIfInputIsProgrammed(enIN_BRAKE1_BPS) )
   {
      bBPS = GetInputValue(enIN_BRAKE1_BPS);
   }
   return bBPS;
}
uint8_t Brake2_GetBPS()
{
   uint8_t bBPS = (Param_ReadValue_1Bit(enPARAM1__EnableSecondaryBrake)) ? gstBrake2.bBPS:1;
   if( CheckIfInputIsProgrammed(enIN_BRAKE2_BPS) )
   {
      bBPS = (Param_ReadValue_1Bit(enPARAM1__EnableSecondaryBrake)) ? GetInputValue(enIN_BRAKE2_BPS):1;
   }
   return bBPS;
}

/*-----------------------------------------------------------------------------
Ramp/Delay lower bounds handled by brake
 -----------------------------------------------------------------------------*/
static void UpdateParameters_Brake()
{
   uint8_t ucPickVoltage = Param_ReadValue_8Bit(enPARAM8__BrakePickVoltage);
   uint8_t ucHoldVoltage = Param_ReadValue_8Bit(enPARAM8__BrakeHoldVoltage);
   uint8_t ucRampUpTime = Param_ReadValue_8Bit( enPARAM8__BrakeRampTime_Auto_10ms );
   uint8_t ucPickDelay = Param_ReadValue_8Bit(enPARAM8__BrakePickDelay_10ms);
   uint8_t ucRelevelVoltage = Param_ReadValue_8Bit(enPARAM8__BrakeRelevelVoltage);

   if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
   {
      ucRampUpTime = Param_ReadValue_8Bit( enPARAM8__BrakeRampTime_Insp_10ms );
   }

   if(ucPickVoltage != gstBrake.ucFP_PickVoltage)
   {
      gstBrake.ucFP_PickVoltage = ucPickVoltage;
   }
   if(ucHoldVoltage != gstBrake.ucFP_HoldVoltage)
   {
      gstBrake.ucFP_HoldVoltage = ucHoldVoltage;
   }
   if(ucPickDelay != gstBrake.ucFP_PickTime_10ms)
   {
      gstBrake.ucFP_PickTime_10ms = ucPickDelay;
   }
   if(ucRampUpTime != gstBrake.ucFP_RampTime_10ms)
   {
      gstBrake.ucFP_RampTime_10ms = ucRampUpTime;
   }
   if( ucRelevelVoltage != gstBrake.ucFP_RelVoltage )
   {
      gstBrake.ucFP_RelVoltage = ucRelevelVoltage;
   }
   gstBrake.ucRampDownTime_10ms = gaucRampDownTime_10ms[ NORMAL_STOP_RAMP_TIME ];
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateParameters_Brake2()
{
   uint8_t ucPickVoltage = Param_ReadValue_8Bit(enPARAM8__SecBrakePickVoltage);
   uint8_t ucHoldVoltage = Param_ReadValue_8Bit(enPARAM8__SecBrakeHoldVoltage);
   uint8_t ucRampUpTime = Param_ReadValue_8Bit(enPARAM8__SecBrakeRampTime_10ms);
   uint8_t ucPickDelay = Param_ReadValue_8Bit(enPARAM8__SecBrakePickDelay_10ms);
   uint8_t ucRelevelVoltage = Param_ReadValue_8Bit(enPARAM8__SecBrakeRelevelVoltage);

   if(ucPickVoltage != gstBrake2.ucFP_PickVoltage)
   {
      gstBrake2.ucFP_PickVoltage = ucPickVoltage;
   }
   if(ucHoldVoltage != gstBrake2.ucFP_HoldVoltage)
   {
      gstBrake2.ucFP_HoldVoltage = ucHoldVoltage;
   }
   if(ucPickDelay != gstBrake2.ucFP_PickTime_10ms)
   {
      gstBrake2.ucFP_PickTime_10ms = ucPickDelay;
   }
   if(ucRampUpTime != gstBrake2.ucFP_RampTime_10ms)
   {
      gstBrake2.ucFP_RampTime_10ms = ucRampUpTime;
   }
   if(ucRelevelVoltage != gstBrake2.ucFP_RelVoltage)
   {
      gstBrake2.ucFP_RelVoltage = ucRelevelVoltage;
   }
   gstBrake2.ucRampDownTime_10ms = gaucRampDownTime_10ms[ NORMAL_STOP_RAMP_TIME ];
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateBPS()
{
   uint8_t bInvertPrimary = Param_ReadValue_1Bit(enPARAM1__BPS_PRIM_NC);
   gstBrake.bBPS = (bInvertPrimary) ? !gstBrake.bBPS_Feedback:gstBrake.bBPS_Feedback;
}
/*-----------------------------------------------------------------------------
process bps
 -----------------------------------------------------------------------------*/
static void UpdateBPS2()
{
   uint8_t bInvertSecondary = Param_ReadValue_1Bit(enPARAM1__BPS_SEC_NC);
   gstBrake2.bBPS = (bInvertSecondary) ? !gstBrake2.bBPS_Feedback:gstBrake2.bBPS_Feedback;
}

/*-----------------------------------------------------------------------------
 Monitor BPS for two faults.
    1. Picked when stopped
    2. Not picked when running

 For 1, car will finish run then fault.

 Both faults require board reset to clear for main brake.

 Disabled via 1 bit parameter
 -----------------------------------------------------------------------------*/
static void CheckFor_BPSFault()
{
   static uint16_t auwDebounceCounter[ NUM_SUBF__INVALID_BPS ];
   static uint8_t ucSubfault_Plus1;

   if(!inReleveling()
   && !Motion_GetRelevelingFlag() )
   {
      uint16_t uwTimeout_10ms = MIN_BPS_ERROR_TIMEOUT_1MS/MOD_RUN_PERIOD_BRAKE_1MS;
      uwTimeout_10ms += Param_ReadValue_8Bit(enPARAM8__BPS_Timeout_100ms) * (100/MOD_RUN_PERIOD_BRAKE_1MS);

      uint8_t bBPS = Brake_GetBPS();
      if( !ucSubfault_Plus1
       && !Param_ReadValue_1Bit( enPARAM1__DisableBPS_StuckLow )
       && !bBPS
       && GetPickBrakeFlag() )
      {
         if( auwDebounceCounter[ SUBF__INVALID_BPS_RUN_ON ] >= uwTimeout_10ms )
         {
            ucSubfault_Plus1 = SUBF__INVALID_BPS_RUN_ON + 1;
         }
         else
         {
            auwDebounceCounter[ SUBF__INVALID_BPS_RUN_ON ]++;
         }
      }
      else if( !ucSubfault_Plus1
            && !Param_ReadValue_1Bit( enPARAM1__DisableBPS_StuckHigh )
            && bBPS
            && !GetPickBrakeFlag() )
      {
         if( auwDebounceCounter[ SUBF__INVALID_BPS_STOPPED ] >= uwTimeout_10ms )
         {
            ucSubfault_Plus1 = SUBF__INVALID_BPS_STOPPED + 1;
         }
         else
         {
            auwDebounceCounter[ SUBF__INVALID_BPS_STOPPED ]++;
         }
      }
      else
      {
         auwDebounceCounter[ SUBF__INVALID_BPS_RUN_ON ] = 0;
         auwDebounceCounter[ SUBF__INVALID_BPS_STOPPED ] = 0;
      }
   }

   /* Latch BPS fault after run complete */
   if( !GetMotion_RunFlag() )
   {
      /* Clear fault if BPS check is disabled */
      if( Param_ReadValue_1Bit( enPARAM1__DisableBPS_StuckHigh )
     && ( ( ucSubfault_Plus1 == SUBF__INVALID_BPS_STOPPED+1 ) || ( GetFault_ByNode(FaultApp_GetNode()) == FLT__BPS_STUCK_OPEN ) ) )
      {
         ucSubfault_Plus1 = 0;
         if( GetFault_ByNode(FaultApp_GetNode()) == FLT__BPS_STUCK_OPEN )
         {
            ManuallyClearLocalFault();
            bDropEB = 0;
         }
      }
      else if( Param_ReadValue_1Bit( enPARAM1__DisableBPS_StuckLow )
          && ( ( ucSubfault_Plus1 == SUBF__INVALID_BPS_RUN_ON+1 ) || ( GetFault_ByNode(FaultApp_GetNode()) == FLT__BPS_STUCK_CLOSED ) ) )
      {
         ucSubfault_Plus1 = 0;
         if( GetFault_ByNode(FaultApp_GetNode()) == FLT__BPS_STUCK_CLOSED )
         {
            ManuallyClearLocalFault();
         }
      }

      if( ucSubfault_Plus1 )
      {
         SetFault(FLT__BPS_STUCK_CLOSED + ucSubfault_Plus1 - 1 );
         if(ucSubfault_Plus1 == (SUBF__INVALID_BPS_STOPPED+1))
         {
            bDropEB = 1;
         }
      }
   }
}
/*-----------------------------------------------------------------------------
 Monitor BPS for two faults.
    1. Picked when stopped (SUBF__INVALID_BPS_RUN_ON)
    2. Not picked when running

 For 1, car will finish run then fault. This fault will require board reset to clear.

 Disabled via 1 bit parameter
 -----------------------------------------------------------------------------*/
static void CheckFor_BPS2Fault()
{
   static uint16_t auwDebounceCounter[ NUM_SUBF__INVALID_BPS ];
   static uint8_t ucSubfault_Plus1;

   if(!inReleveling()
   && !Motion_GetRelevelingFlag() )
   {
      uint16_t uwTimeout_10ms = MIN_BPS_ERROR_TIMEOUT_1MS/MOD_RUN_PERIOD_BRAKE_1MS;
      uwTimeout_10ms += Param_ReadValue_8Bit(enPARAM8__BPS2_Timeout_100ms) * (100/MOD_RUN_PERIOD_BRAKE_1MS);

      uint8_t bBPS = Brake2_GetBPS();
      if( !ucSubfault_Plus1
       && !Param_ReadValue_1Bit( enPARAM1__DisableBPS2_StuckLow )
       && !bBPS
       && GetPickEBrakeFlag() )
      {
         if( auwDebounceCounter[ SUBF__INVALID_BPS_RUN_ON ] >= uwTimeout_10ms )
         {
            ucSubfault_Plus1 = SUBF__INVALID_BPS_RUN_ON + 1;
         }
         else
         {
            auwDebounceCounter[ SUBF__INVALID_BPS_RUN_ON ]++;
         }
      }
      else if( !ucSubfault_Plus1
            && !Param_ReadValue_1Bit( enPARAM1__DisableBPS2_StuckHigh )
            && bBPS
            && !GetPickEBrakeFlag() )
      {
         if( auwDebounceCounter[ SUBF__INVALID_BPS_STOPPED ] >= uwTimeout_10ms )
         {
            ucSubfault_Plus1 = SUBF__INVALID_BPS_STOPPED + 1;
         }
         else
         {
            auwDebounceCounter[ SUBF__INVALID_BPS_STOPPED ]++;
         }
      }
      else
      {
         auwDebounceCounter[ SUBF__INVALID_BPS_RUN_ON ] = 0;
         auwDebounceCounter[ SUBF__INVALID_BPS_STOPPED ] = 0;
      }
   }


   /* Latch BPS fault after run complete */
   if( !GetMotion_RunFlag() )
   {
      /* Clear fault if BPS check is disabled */
      if( Param_ReadValue_1Bit( enPARAM1__DisableBPS2_StuckHigh )
     && ( ucSubfault_Plus1 == SUBF__INVALID_BPS_STOPPED+1 ) )
      {
         ucSubfault_Plus1 = 0;
      }
      else if( Param_ReadValue_1Bit( enPARAM1__DisableBPS2_StuckLow )
          && ( ucSubfault_Plus1 == SUBF__INVALID_BPS_RUN_ON+1 ) )
      {
         ucSubfault_Plus1 = 0;
      }

      if( ucSubfault_Plus1 )
      {
         SetFault(FLT__EBPS_STUCK_CLOSED+ucSubfault_Plus1-1);
         if( ( ucSubfault_Plus1 - 1 ) == SUBF__INVALID_BPS_STOPPED )
         {
            ucSubfault_Plus1 = 0;
         }
      }
   }
}
/*-----------------------------------------------------------------------------
 Counter will be reset when packet is received
 Trigger a fault if we don't receive from drive
 -----------------------------------------------------------------------------*/
static void CheckFor_BrakeFault()
{
   static uint16_t uwStartupDelay_10ms = 500;
   static uint8_t bLatchingMosfetFault; // Latch mosfet fault locally
   static uint8_t bEndOfRunMosfetFault; // Flag to trigger mosfet fault at end of run

   static enum en_brake_faults eLastFault;
   static uint8_t bSuccessfulRun;
   static uint8_t bLastRunFlag;
   static uint8_t ucConsecutiveFaultCounter;// Tracks num of consecutive mosfet faults

   /* Detect successful runs */
   if( GetMotion_RunFlag() && !bLastRunFlag )
   {
      bSuccessfulRun = 1;
   }
   else if( !GetMotion_RunFlag() && bLastRunFlag )
   {
      if(bSuccessfulRun)
      {
         ucConsecutiveFaultCounter = 0;
      }
   }
   bLastRunFlag = GetMotion_RunFlag();

   if( !uwStartupDelay_10ms )
   {
      if( !Param_ReadValue_1Bit(enPARAM1__DisableBrakeboard) && !CheckIfInputIsProgrammed(enIN_BRAKE1_BPS) )
      {
         if ( gstBrake.uwOfflineCounter_1ms > BRAKE_OFFLINE_TIMEOUT_1MS )
         {
            SetFault(FLT__BRAKE_OFFLINE);
         }
         else
         {
            gstBrake.uwOfflineCounter_1ms += MOD_RUN_PERIOD_BRAKE_1MS;
            if( ( !GetMotion_RunFlag() )
             && ( ( bEndOfRunMosfetFault )
               || ( bLatchingMosfetFault && getDoorZone(DOOR_ANY) ) ) )
            {
               SetFault(FLT__BRAKE_MOSFET);
               bEndOfRunMosfetFault = 0;
            }
            else if( gstBrake.eError != BRAKE_ERROR__NONE )
            {
               if( gstBrake.eError != BRAKE_ERROR__MOSFET_FAILURE )
               {
                  if( gstBrake.eError == BRAKE_ERROR__OVERHEAT )
                  {
                     if( !Param_ReadValue_1Bit(enPARAM1__DisableBrakeOverheat) && !GetMotion_RunFlag() && getDoorZone(DOOR_ANY) )
                     {
                        SetFault(FLT__BRAKE_OVERHEAT);
                     }
                  }
                  else
                  {
                     en_faults eFaultNum = FLT__BRAKE_UNK;
                     if( gstBrake.eError > BRAKE_ERROR__NONE )
                     {
                        eFaultNum += gstBrake.eError-BRAKE_ERROR__NONE;
                     }
                     if(eFaultNum > FLT__BRAKE_AC_LOSS)
                     {
                        eFaultNum = FLT__BRAKE_AC_LOSS;
                     }
                     SetFault(eFaultNum);
                  }
               }
               else if( ( eLastFault != gstBrake.eError )
                     && ( Motion_GetMotionState() >= MOTION__MANUAL_RUN )
                     && ( Motion_GetMotionState() <= MOTION__ACCELERATING )
                     && ( GetInputValue(enIN_MBC) ) )
               {
                  bEndOfRunMosfetFault = 1;
                  bSuccessfulRun = 0;
                  if( ucConsecutiveFaultCounter < MOSFET_FAULT_CONSECUTIVE_LIMIT )
                  {
                     ucConsecutiveFaultCounter++;
                  }
                  else
                  {
                     /* Do not latch if the fault occurred after the start sequence or acceleration stage */
                     bLatchingMosfetFault = 1;
                  }
               }
            }
         }
      }
      else
      {
         bLatchingMosfetFault = 0;
         bEndOfRunMosfetFault = 0;
      }
   }
   else
   {
      uwStartupDelay_10ms--;
   }

   if( Param_ReadValue_1Bit(enPARAM1__DisableLatchingBrakeFault) )
   {
      bLatchingMosfetFault = 0;
      ucConsecutiveFaultCounter = 0;
   }
   eLastFault = gstBrake.eError;
}
/*-----------------------------------------------------------------------------
 Counter will be reset when packet is received
 Trigger a fault if we don't receive from drive
 -----------------------------------------------------------------------------*/
static void CheckFor_Brake2Fault()
{
   static uint16_t uwStartupDelay_10ms = 500;
   static uint8_t bLatchingMosfetFault; // Latch mosfet fault locally
   static uint8_t bEndOfRunMosfetFault; // Flag to trigger mosfet fault at end of run

   static enum en_brake_faults eLastFault;
   static uint8_t bSuccessfulRun;
   static uint8_t bLastRunFlag;
   static uint8_t ucConsecutiveFaultCounter;// Tracks num of consecutive mosfet faults

   /* Detect successful runs */
   if( GetMotion_RunFlag() && !bLastRunFlag )
   {
      bSuccessfulRun = 1;
   }
   else if( !GetMotion_RunFlag() && bLastRunFlag )
   {
      if(bSuccessfulRun)
      {
         ucConsecutiveFaultCounter = 0;
      }
   }
   bLastRunFlag = GetMotion_RunFlag();

   if( !uwStartupDelay_10ms )
   {
      if( !Param_ReadValue_1Bit(enPARAM1__DisableBrakeboard) && !CheckIfInputIsProgrammed(enIN_BRAKE2_BPS) )
      {
         if ( gstBrake2.uwOfflineCounter_1ms > BRAKE_OFFLINE_TIMEOUT_1MS )
         {
            SetFault(FLT__EBRAKE_OFFLINE);
         }
         else
         {
            gstBrake2.uwOfflineCounter_1ms += MOD_RUN_PERIOD_BRAKE_1MS;
            if( ( !GetMotion_RunFlag() )
             && ( ( bEndOfRunMosfetFault )
               || ( bLatchingMosfetFault && getDoorZone(DOOR_ANY) ) ) )
            {
               SetFault(FLT__EBRAKE_MOSFET);
               bEndOfRunMosfetFault = 0;
            }
            else if( gstBrake2.eError != BRAKE_ERROR__NONE )
            {
               if( gstBrake2.eError != BRAKE_ERROR__MOSFET_FAILURE )
               {
                  if( gstBrake2.eError == BRAKE_ERROR__OVERHEAT )
                  {
                     if( !Param_ReadValue_1Bit(enPARAM1__DisableBrakeOverheat) && !GetMotion_RunFlag() && getDoorZone(DOOR_ANY) )
                     {
                        SetFault(FLT__EBRAKE_OVERHEAT);
                     }
                  }
                  else
                  {
                     en_faults eFaultNum = FLT__EBRAKE_UNK;
                     if( gstBrake2.eError > BRAKE_ERROR__NONE )
                     {
                        eFaultNum += gstBrake2.eError-BRAKE_ERROR__NONE;
                     }
                     if(eFaultNum > FLT__EBRAKE_AC_LOSS)
                     {
                        eFaultNum = FLT__EBRAKE_AC_LOSS;
                     }
                     SetFault(eFaultNum);
                  }
               }
               else if( ( eLastFault != gstBrake2.eError )
                     && ( Motion_GetMotionState() >= MOTION__MANUAL_RUN )
                     && ( Motion_GetMotionState() <= MOTION__ACCELERATING )
                     && ( GetInputValue(enIN_MBC2) ) )
               {
                  bEndOfRunMosfetFault = 1;
                  bSuccessfulRun = 0;
                  if( ucConsecutiveFaultCounter < MOSFET_FAULT_CONSECUTIVE_LIMIT )
                  {
                     ucConsecutiveFaultCounter++;
                  }
                  else
                  {
                     /* Do not latch if the fault occurred after the start sequence or acceleration stage */
                     bLatchingMosfetFault = 1;
                  }
               }
            }
         }
      }
      else
      {
         bLatchingMosfetFault = 0;
         bEndOfRunMosfetFault = 0;
      }
   }
   else
   {
      uwStartupDelay_10ms--;
   }

   if( Param_ReadValue_1Bit(enPARAM1__DisableLatchingBrakeFault) )
   {
      bLatchingMosfetFault = 0;
      ucConsecutiveFaultCounter = 0;
   }
   eLastFault = gstBrake2.eError;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t LiftBrake()
{
   uint8_t bLiftBrake = 0;
   /* Allow brake to remain lifted as long as mechanic is holding the B contactor. Even after faulted, to confirm secondary brake drop will stop the car.*/
   if( Test_UM_GetPickBrakeFlag() )
   {
      bLiftBrake = 1;
   }
   else if( (!gstFault.bActiveFault || (GetOperation_AutoMode() == MODE_A__TEST) ) && ( GetPickBrakeFlag() || GetAcceptanceBrakePickCommand( BRAKE_SEL__PRIMARY ) ) )
   {
      bLiftBrake = 1;
   }
   return bLiftBrake;
}
static uint8_t LiftBrake2()
{
   uint8_t bLiftBrake = 0;
   if( (!gstFault.bActiveFault || (GetOperation_AutoMode() == MODE_A__TEST)) && ( GetPickEBrakeFlag() || GetAcceptanceBrakePickCommand( BRAKE_SEL__EMERGENCY ) || Test_UM_GetPickEBrakeFlag() ) )
   {
      bLiftBrake = 1;
   }
   return bLiftBrake;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ControlBrake()
{
   if ( LiftBrake() )
   {
      gstBrake.ucCommandByte = 1;
      gstBrake.ucRampUpTime_10ms = gstBrake.ucFP_RampTime_10ms;
      gstBrake.ucPickTime_10ms = gstBrake.ucFP_PickTime_10ms;
      if( Motion_GetRelevelingFlag() )
      {
         gstBrake.ucPickVoltageCommand = gstBrake.ucFP_RelVoltage;
         gstBrake.ucHoldVoltageCommand = gstBrake.ucFP_RelVoltage;
      }
      else
      {
         gstBrake.ucPickVoltageCommand = gstBrake.ucFP_PickVoltage;
         gstBrake.ucHoldVoltageCommand = gstBrake.ucFP_HoldVoltage;
      }

      /* AC brake control via discrete pick output */
      SetOutputValue(enOUT_BRAKE1_PICK, 1);
   }
   else
   {
      UpdateParameters_Brake();
      gstBrake.ucCommandByte = 0;
      if ( GetOperation_MotionCmd() == MOCMD__NORMAL_STOP )
      {
         gstBrake.ucRampDownTime_10ms = gaucRampDownTime_10ms[ NORMAL_STOP_RAMP_TIME ];
      }
      else if ( GetOperation_MotionCmd() == MOCMD__QUICK_STOP )
      {
         gstBrake.ucRampDownTime_10ms = gaucRampDownTime_10ms[ QUICK_STOP_RAMP_TIME ];
      }
      else
      {
         gstBrake.ucRampDownTime_10ms = gaucRampDownTime_10ms[ EMERGENCY_STOP_RAMP_TIME ];
      }

      /* AC brake control via discrete pick output */
      SetOutputValue(enOUT_BRAKE1_PICK, 0);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ControlBrake2()
{
   if ( LiftBrake2() )
   {
      gstBrake2.ucCommandByte = 1;
      gstBrake2.ucRampUpTime_10ms = gstBrake2.ucFP_RampTime_10ms;
      gstBrake2.ucPickTime_10ms = gstBrake2.ucFP_PickTime_10ms;
      if( Motion_GetRelevelingFlag() )
      {
         gstBrake2.ucPickVoltageCommand = gstBrake2.ucFP_RelVoltage;
         gstBrake2.ucHoldVoltageCommand = gstBrake2.ucFP_RelVoltage;
      }
      else
      {
         gstBrake2.ucPickVoltageCommand = gstBrake2.ucFP_PickVoltage;
         gstBrake2.ucHoldVoltageCommand = gstBrake2.ucFP_HoldVoltage;
      }

      /* AC brake control via discrete pick output */
      SetOutputValue(enOUT_BRAKE2_PICK, 1);
   }
   else
   {
      UpdateParameters_Brake2();
      gstBrake2.ucCommandByte = 0;
      if ( GetOperation_MotionCmd() == MOCMD__NORMAL_STOP )
      {
         gstBrake2.ucRampDownTime_10ms = gaucRampDownTime_10ms[ NORMAL_STOP_RAMP_TIME ];
      }
      else if ( GetOperation_MotionCmd() == MOCMD__QUICK_STOP )
      {
         gstBrake2.ucRampDownTime_10ms = gaucRampDownTime_10ms[ QUICK_STOP_RAMP_TIME ];
      }
      else
      {
         gstBrake2.ucRampDownTime_10ms = gaucRampDownTime_10ms[ EMERGENCY_STOP_RAMP_TIME ];
      }

      /* AC brake control via discrete pick output */
      SetOutputValue(enOUT_BRAKE2_PICK, 0);
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_BRAKE_1MS;

   UpdateParameters_Brake();
   UpdateParameters_Brake2();
   gstBrake.ucCommandByte = 0;
   gstBrake.ucRampUpTime_10ms = gstBrake.ucFP_RampTime_10ms;
   gstBrake.ucPickTime_10ms = gstBrake.ucFP_PickTime_10ms;
   gstBrake.ucPickVoltageCommand = gstBrake.ucFP_PickVoltage;
   gstBrake.ucHoldVoltageCommand = gstBrake.ucFP_HoldVoltage;
   gstBrake.ucRampDownTime_10ms = gaucRampDownTime_10ms[ EMERGENCY_STOP_RAMP_TIME ];
   gstBrake.bBPS = 0;

   gstBrake2.ucCommandByte = 0;
   gstBrake2.ucRampUpTime_10ms = gstBrake2.ucFP_RampTime_10ms;
   gstBrake2.ucPickTime_10ms = gstBrake2.ucFP_PickTime_10ms;
   gstBrake2.ucPickVoltageCommand = gstBrake2.ucFP_PickVoltage;
   gstBrake2.ucHoldVoltageCommand = gstBrake2.ucFP_HoldVoltage;
   gstBrake2.ucRampDownTime_10ms = gaucRampDownTime_10ms[ EMERGENCY_STOP_RAMP_TIME ];
   gstBrake2.bBPS = 0;
   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   UpdateBPS();
   CheckFor_BrakeFault();
   uint8_t bActiveBrakeTest = ( GetOperation_AutoMode() == MODE_A__TEST )
                           && ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
                           && ( ( GetActiveAcceptanceTest() == ACCEPTANCE_TEST_SLIDE_DISTANCE )
                             || ( GetActiveAcceptanceTest() == ACCEPTANCE_TEST_EBRAKE_SLIDE )
                             || ( GetActiveAcceptanceTest() == ACCEPTANCE_TEST_BRAKEBOARD_FEEDBACK ) );
   bActiveBrakeTest |= Test_UM_GetTestActiveFlag();
   if( !bActiveBrakeTest )
   {
      CheckFor_BPSFault();
   }

   ControlBrake();

   if( Param_ReadValue_1Bit(enPARAM1__EnableSecondaryBrake) )
   {
      UpdateBPS2();
      CheckFor_Brake2Fault();
      if( !bActiveBrakeTest )
      {
         CheckFor_BPS2Fault();
      }
      ControlBrake2();
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
