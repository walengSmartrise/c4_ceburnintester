/******************************************************************************
 *
 * @file     mod_earthquake.c
 * @brief    Logic that controls how earthquake and seismic operations work.
 * @version  V1.00
 * @date     28, July 2016
 *
 * @note     Operation outlined in A17 (2016) 8.4.10
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "GlobalData.h"
#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

static void EQ_Fault_With_Doors_Open( void );
static void EQ_Wait_To_Activate_Hoistway_Check( void );
static void EQ_Move_To_Top( void );
static void EQ_Move_To_Bottom( void );
static void EQ_Move_To_Original_Floor( void );
static void EQ_Wait_For_Hoistway_Check_Activation( void );
static void EQ_Reduced_Speed_Auto( void );

static void EQ_Lamps( void );
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Earthquake = { .pfnInit = Init, .pfnRun = Run, };

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MAX_EQ_SPEED__FPM           (75)
#define MAX_EQ_HOISTWAY_SCAN__FPM 150
#define MIN_EQ_HOISTWAY_SCAN__FPM 10

#define INITIAL_WAIT_TIME_50MS 15*20
#define MAX_WAIT_RESCAN_50MS 60*20
#define MOD_RUN_TIME_1MS 50
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
enum en_eq_states
{
   EQ_FAULT_WITH_DOORS_OPEN,
   EQ_WAIT_TO_ACTIVATE_HOISTWAY_CHECK,
   EQ_MOVE_TO_TOP,
   EQ_MOVE_TO_BOTTOM,
   EQ_MOVE_TO_ORIGINAL_FLOOR,
   EQ_WAIT_FOR_HOISTWAY_CHECK_ACTIVATION,
   EQ_REDUCED_SPEED_AUTO

};
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
/* Add single cycle delay before floor selection, otherwise reachable floor assessment will be
 * calculated prior to speed limit change  */
static uint8_t bDelayFloorSelection;
static uint8_t ucRecallFloor = 0xFF;

static enum en_eq_states enEQStates = EQ_FAULT_WITH_DOORS_OPEN;
static uint16_t uwInitialWait_50MS; //waits up to 15 seconds
static uint16_t uwTerminalWait_50MS; //waits up to 60 seconds
static uint16_t uwEQModeSlowDownTimer_1MS;
static uint16_t uwEQModeIndicatorTimer_1MS;
static uint16_t uwEQModeStatusTimer_1MS;

static uint8_t original_max_run_time_1SEC;

static uint8_t bCW_Derail_Input_Active;
static uint8_t bSeismic_Input_Active;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t Auto_CW_GetState( void )
{
   return GetRecallState();
}
/*-----------------------------------------------------------------------------
   Sets position limit bounds & determines recall floor
 -----------------------------------------------------------------------------*/
static uint8_t GetCounterweightRecallFloor()
{
   uint8_t ucDestFloor = INVALID_FLOOR;
   enum direction_enum eDir = GetMotion_Direction();
   uint32_t uiCurrentPos = GetPosition_PositionCount();
   uint32_t uiCW_Pos = Param_ReadValue_24Bit( enPARAM24__COUNTER_WEIGHT_MID_POINT );

   /* Created a 5 foot no go zone around the learned CW position */
   uint32_t uiCW_UpperPosLimit = uiCW_Pos + TWO_FEET;
   uint32_t uiCW_LowerPosLimit = uiCW_Pos - TWO_FEET;
   if ( GetPosition_PositionCount() > uiCW_Pos )
   {
      SetOperation_PositionLimit_DN(uiCW_UpperPosLimit);
      SetOperation_PositionLimit_UP(gpastFloors[ GetFP_NumFloors() - 1 ].ulPosition + ONE_INCH);
   }
   else
   {
      SetOperation_PositionLimit_DN (gpastFloors[ 0 ].ulPosition - ONE_INCH);
      SetOperation_PositionLimit_UP(uiCW_LowerPosLimit);
   }

   if( eDir == DIR__UP )
   {
      uint8_t ucAboveFloor = GetClosestValidOpening( DIR__UP, DOOR_ANY );
      if( ucAboveFloor < GetFP_NumFloors() )
      {
         if( ( ( uiCurrentPos >= uiCW_Pos ) && ( gpastFloors[ucAboveFloor].ulPosition >= uiCurrentPos ) && ( gpastFloors[ucAboveFloor].ulPosition > uiCW_UpperPosLimit ) )
          || ( ( uiCurrentPos <= uiCW_Pos ) && ( gpastFloors[ucAboveFloor].ulPosition <= uiCurrentPos ) && ( gpastFloors[ucAboveFloor].ulPosition < uiCW_LowerPosLimit ) ) )
         {
            ucDestFloor = ucAboveFloor;
         }
      }
   }
   else if( eDir == DIR__DN )
   {
      uint8_t ucBelowFloor = GetClosestValidOpening( DIR__DN, DOOR_ANY );
      if( ucBelowFloor < GetFP_NumFloors() )
      {
         if( ( ( uiCurrentPos >= uiCW_Pos ) && ( gpastFloors[ucBelowFloor].ulPosition >= uiCurrentPos ) && ( gpastFloors[ucBelowFloor].ulPosition > uiCW_UpperPosLimit ) )
          || ( ( uiCurrentPos <= uiCW_Pos ) && ( gpastFloors[ucBelowFloor].ulPosition <= uiCurrentPos ) && ( gpastFloors[ucBelowFloor].ulPosition < uiCW_LowerPosLimit ) ) )
         {
            ucDestFloor = ucBelowFloor;
         }
      }
   }
   else
   {
      uint8_t ucAboveFloor = GetClosestValidOpening( DIR__UP, DOOR_ANY );
      uint8_t ucBelowFloor = GetClosestValidOpening( DIR__DN, DOOR_ANY );
      if( ucAboveFloor < GetFP_NumFloors() )
      {
         if( ( ( uiCurrentPos >= uiCW_Pos ) && ( gpastFloors[ucAboveFloor].ulPosition >= uiCurrentPos ) && ( gpastFloors[ucAboveFloor].ulPosition > uiCW_UpperPosLimit ) )
          || ( ( uiCurrentPos <= uiCW_Pos ) && ( gpastFloors[ucAboveFloor].ulPosition <= uiCurrentPos ) && ( gpastFloors[ucAboveFloor].ulPosition < uiCW_LowerPosLimit ) ) )
         {
            ucDestFloor = ucAboveFloor;
         }
      }
      if( ucDestFloor == INVALID_FLOOR )
      {
         if( ucBelowFloor < GetFP_NumFloors() )
         {
            if( ( ( uiCurrentPos >= uiCW_Pos ) && ( gpastFloors[ucBelowFloor].ulPosition >= uiCurrentPos ) && ( gpastFloors[ucBelowFloor].ulPosition > uiCW_UpperPosLimit ) )
             || ( ( uiCurrentPos <= uiCW_Pos ) && ( gpastFloors[ucBelowFloor].ulPosition <= uiCurrentPos ) && ( gpastFloors[ucBelowFloor].ulPosition < uiCW_LowerPosLimit ) ) )
            {
               ucDestFloor = ucBelowFloor;
            }
         }
      }

      /* Exception if the car is within 2 inches of the current floor.
       * Fixes car recalling to new floor on reset for modes like EQ */
      uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
      if( ucCurrentFloor < GetFP_NumFloors() )
      {
         uint32_t uiUpperLimit = gpastFloors[ucCurrentFloor].ulPosition + TWO_INCHES;
         uint32_t uiLowerLimit = gpastFloors[ucCurrentFloor].ulPosition - TWO_INCHES;
         if( ( uiCurrentPos > uiLowerLimit )
          && ( uiCurrentPos < uiUpperLimit ) )
         {
            if( gpastFloors[ucCurrentFloor].bFrontOpening
           || ( GetFP_RearDoors() && gpastFloors[ucCurrentFloor].bRearOpening ) )
            {
               ucDestFloor = ucCurrentFloor;
            }
         }
      }
   }

   return ucDestFloor;
}

/*-----------------------------------------------------------------------------

   Called from Oper_Auto

 -----------------------------------------------------------------------------*/
//void Auto_CW_Derail( uint8_t bInit )
void Auto_Seismc( uint8_t bInit )
{
   //limit speed
   Auto_SetSpeed(MAX_EQ_SPEED__FPM);

   if( bInit )
   {
      bDelayFloorSelection = 1;
      ResetRecallState();
      ucRecallFloor = INVALID_FLOOR;
      if( GetMotion_RunFlag() && ( GetOperation_AutoMode() == MODE_A__CW_DRAIL ) )
      {
         SetOperation_MotionCmd( MOCMD__EMERG_STOP );
      }

      ClearLatchedCarCalls();
      ClearLatchedHallCalls();

      uwInitialWait_50MS = 0;
      uwTerminalWait_50MS = 0;
      enEQStates = EQ_FAULT_WITH_DOORS_OPEN;

      original_max_run_time_1SEC = Param_ReadValue_8Bit(enPARAM8__Max_Runtime_1sec);
      Param_WriteValue_8Bit(enPARAM8__Max_Runtime_1sec, 0);

      /* If heading to a floor that will require passing the CW position, stop immediately. Added guard */
      if( GetMotion_RunFlag() && ( GetOperation_AutoMode() == MODE_A__CW_DRAIL ) )
      {
         uint32_t uiCurrentPos = GetPosition_PositionCount();
         uint32_t uiDestPos = GetMotion_Destination();
         uint32_t uiCW_Pos = Param_ReadValue_24Bit( enPARAM24__COUNTER_WEIGHT_MID_POINT );
         if( ( ( uiCurrentPos >= uiCW_Pos ) && ( uiDestPos <= uiCW_Pos ) )
          || ( ( uiCurrentPos <= uiCW_Pos ) && ( uiDestPos >= uiCW_Pos ) ) )
         {
            SetOperation_MotionCmd( MOCMD__EMERG_STOP );
         }
      }
   }

   if( bDelayFloorSelection )
   {
      bDelayFloorSelection--;
   }
   else if( GetOperation_AutoMode() == MODE_A__CW_DRAIL )
   {
      ucRecallFloor = GetCounterweightRecallFloor();
      if( Auto_Recall(bInit, ucRecallFloor, DOOR_ANY) )
      {
         SetFault(FLT__EQ);
      }
      SetOutputValue(enOUT_LMP_EQ, 1);
      SetOutputValue( enOUT_EQ_SLOW_LAMP, 0);
      if( ( uwEQModeStatusTimer_1MS > MOD_RUN_TIME_1MS * 2 ) )
      {
         SetOutputValue( enOUT_LMP_SEISMIC_STATUS, !GetOutputValue(enOUT_LMP_SEISMIC_STATUS) );
         uwEQModeStatusTimer_1MS = 0;
      }
      else
      {
         uwEQModeStatusTimer_1MS += MOD_RUN_TIME_1MS;
      }
   }
   else
   {
      Auto_EQ_Hoistway_Scan();
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t OnEarthquake ( void ) {
   uint8_t bOnEarthquake = 0;
   if ( Param_ReadValue_1Bit( enPARAM1__EnableEarthQuake )
    && ( GetEmergencyBit(EmergencyBF_CW_Derail_InProgress) || GetEmergencyBit(EmergencyBF_EQ_Seismic_InProgress)
         )
      )
   {
      bOnEarthquake = 1;
   }
   return bOnEarthquake;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
//TODO: Merge this and CW above into a single function.
//void Auto_Seismc( uint8_t bInit )
//{
   //limit speed
//   Auto_SetSpeed(MAX_EQ_SPEED__FPM);
//   ClearLatchedCarCalls();
//   ClearLatchedHallCalls();

//   if(bInit)
//   {
//      bDelayFloorSelection = 1;
//      ResetRecallState();
//      ucRecallFloor = INVALID_FLOOR;
//   }
   // ---------------------------------------------------
//   if(bDelayFloorSelection)
//   {
//      bDelayFloorSelection--;
//   }
//   else
//   {
//      ucRecallFloor = GetClosestValidOpening( GetMotion_Direction(), DOOR_ANY );
//      if( Auto_Recall(bInit, ucRecallFloor, RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR) )
//      {
//         SetFault(FLT__EQ);
//      }
//   }
//}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void Auto_EQ_Hoistway_Scan(void)
{
   EQ_Lamps();

   switch(enEQStates)
   {
      case EQ_FAULT_WITH_DOORS_OPEN:
         EQ_Fault_With_Doors_Open();
         break;

      case EQ_WAIT_TO_ACTIVATE_HOISTWAY_CHECK:
         EQ_Wait_To_Activate_Hoistway_Check();
         break;

      case EQ_MOVE_TO_TOP:
         EQ_Move_To_Top();
         break;

      case EQ_MOVE_TO_BOTTOM:
         EQ_Move_To_Bottom();
         break;

      case EQ_MOVE_TO_ORIGINAL_FLOOR:
         EQ_Move_To_Original_Floor();
         break;

      case EQ_WAIT_FOR_HOISTWAY_CHECK_ACTIVATION:
         EQ_Wait_For_Hoistway_Check_Activation();
         break;

      case EQ_REDUCED_SPEED_AUTO:
         EQ_Reduced_Speed_Auto();
         break;

      default:
         enEQStates = EQ_FAULT_WITH_DOORS_OPEN;
      break;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void EQ_Fault_With_Doors_Open( void )
{/*
   gstCurrentModeRules[MODE_A__CW_DRAIL].bParkingEnabled = 0;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreHallCall = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bAutoDoorOpen = 0;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreCarCall_F = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreCarCall_R = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bDoorHold = 0;*/

   gstCurrentModeRules[MODE_A__SEISMC].bParkingEnabled = 0;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreHallCall = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bAutoDoorOpen = 0;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreCarCall_F = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreCarCall_R = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bDoorHold = 0;

   ucRecallFloor = GetClosestValidOpening(GetMotion_Direction(), DOOR_ANY);
   if( Param_ReadValue_1Bit( enPARAM1__EnableEarthQuake ) && GetEmergencyBit(EmergencyBF_EQ_AutoMode_InProgress) )
   {
      uwInitialWait_50MS = 0;
      uwTerminalWait_50MS = 0;
      enEQStates = EQ_REDUCED_SPEED_AUTO;
   }
   else if( Auto_Recall(0, ucRecallFloor, RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR ) )
   {
      SetFault(FLT__EQ);
      if( GetInputValue( enIN_EQ_HOISTWAY_SCAN) && !bCW_Derail_Input_Active && !bSeismic_Input_Active )
      {
         uwInitialWait_50MS = 0;
         uwTerminalWait_50MS = 0;
         enEQStates = EQ_WAIT_TO_ACTIVATE_HOISTWAY_CHECK;
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void EQ_Wait_To_Activate_Hoistway_Check( void )
{/*
   gstCurrentModeRules[MODE_A__CW_DRAIL].bParkingEnabled = 0;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreHallCall = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bAutoDoorOpen = 0;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreCarCall_F = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreCarCall_R = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bDoorHold = 0;*/

   gstCurrentModeRules[MODE_A__SEISMC].bParkingEnabled = 0;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreHallCall = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bAutoDoorOpen = 0;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreCarCall_F = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreCarCall_R = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bDoorHold = 0;

   if( Param_ReadValue_1Bit( enPARAM1__EnableEarthQuake ) && GetEmergencyBit(EmergencyBF_EQ_AutoMode_InProgress) )
   {
      uwInitialWait_50MS = 0;
      uwTerminalWait_50MS = 0;
      enEQStates = EQ_REDUCED_SPEED_AUTO;
   }
   else if( uwInitialWait_50MS > INITIAL_WAIT_TIME_50MS )
   {
      CloseAnyOpenDoor();

      if( CarDoorsClosed() )
      {
         uwInitialWait_50MS = 0;
         uwTerminalWait_50MS = 0;
         enEQStates = EQ_MOVE_TO_TOP;
      }
   }
   else
   {
      Auto_Recall(0, ucRecallFloor, RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR );
      uwInitialWait_50MS++;
   }

//   Auto_Oper( 0 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void EQ_Move_To_Top( void )
{/*
   gstCurrentModeRules[MODE_A__CW_DRAIL].bParkingEnabled = 0;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreHallCall = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bAutoDoorOpen = 0;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreCarCall_F = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreCarCall_R = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bDoorHold = 0;*/

   gstCurrentModeRules[MODE_A__SEISMC].bParkingEnabled = 0;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreHallCall = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bAutoDoorOpen = 0;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreCarCall_F = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreCarCall_R = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bDoorHold = 0;

   SetInDestinationDoorzone(0); // prevent door reopening
   Auto_Set_Limits(); // set the position limits where the car can move

   uint8_t ucSpeedLimit = Param_ReadValue_8Bit(enPARAM8__EQ_Hoistway_Scan_Speed);
   if( ucSpeedLimit < MIN_EQ_HOISTWAY_SCAN__FPM )
   {
      ucSpeedLimit = MIN_EQ_HOISTWAY_SCAN__FPM;
   }
   else if( ucSpeedLimit > 150 )
   {
       ucSpeedLimit = MAX_EQ_HOISTWAY_SCAN__FPM;
   }
   Auto_SetSpeed(ucSpeedLimit);

   if( Param_ReadValue_1Bit( enPARAM1__EnableEarthQuake ) && GetEmergencyBit(EmergencyBF_EQ_AutoMode_InProgress) )
   {
      uwInitialWait_50MS = 0;
      uwTerminalWait_50MS = 0;
      enEQStates = EQ_REDUCED_SPEED_AUTO;
   }
   else if( getDoorZone(DOOR_ANY) && !GetMotion_RunFlag() && ( GetOperation_CurrentFloor() == GetFP_NumFloors()-1 ) )
   {
      uwInitialWait_50MS = 0;
      uwTerminalWait_50MS = 0;
      enEQStates = EQ_MOVE_TO_BOTTOM;
   }
   else
   {
      SetDestination(GetFP_NumFloors()-1);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void EQ_Move_To_Bottom( void )
{/*
   gstCurrentModeRules[MODE_A__CW_DRAIL].bParkingEnabled = 0;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreHallCall = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bAutoDoorOpen = 0;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreCarCall_F = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreCarCall_R = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bDoorHold = 0;*/

   gstCurrentModeRules[MODE_A__SEISMC].bParkingEnabled = 0;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreHallCall = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bAutoDoorOpen = 0;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreCarCall_F = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreCarCall_R = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bDoorHold = 0;

   SetInDestinationDoorzone(0); // prevent door reopening
   Auto_Set_Limits(); // set the position limits where the car can move

   uint8_t ucSpeedLimit = Param_ReadValue_8Bit(enPARAM8__EQ_Hoistway_Scan_Speed);
   if( ucSpeedLimit < MIN_EQ_HOISTWAY_SCAN__FPM )
   {
      ucSpeedLimit = MIN_EQ_HOISTWAY_SCAN__FPM;
   }
   else if( ucSpeedLimit > 150 )
   {
       ucSpeedLimit = MAX_EQ_HOISTWAY_SCAN__FPM;
   }
   Auto_SetSpeed(ucSpeedLimit);

   if( Param_ReadValue_1Bit( enPARAM1__EnableEarthQuake ) && GetEmergencyBit(EmergencyBF_EQ_AutoMode_InProgress) )
   {
      uwInitialWait_50MS = 0;
      uwTerminalWait_50MS = 0;
      enEQStates = EQ_REDUCED_SPEED_AUTO;
   }
   else if( getDoorZone(DOOR_ANY) && !GetMotion_RunFlag() && ( GetOperation_CurrentFloor() == 0 ) )
   {
      uwInitialWait_50MS = 0;
      uwTerminalWait_50MS = 0;
      enEQStates = EQ_MOVE_TO_ORIGINAL_FLOOR;
   }
   else
   {
      SetDestination( 0 );
   }

//   Auto_Oper( 0 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void EQ_Move_To_Original_Floor( void )
{/*
   gstCurrentModeRules[MODE_A__CW_DRAIL].bParkingEnabled = 0;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreHallCall = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bAutoDoorOpen = 0;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreCarCall_F = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreCarCall_R = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bDoorHold = 0;*/

   gstCurrentModeRules[MODE_A__SEISMC].bParkingEnabled = 0;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreHallCall = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bAutoDoorOpen = 0;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreCarCall_F = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreCarCall_R = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bDoorHold = 0;

   if( Param_ReadValue_1Bit( enPARAM1__EnableEarthQuake ) && GetEmergencyBit(EmergencyBF_EQ_AutoMode_InProgress) )
   {
      uwInitialWait_50MS = 0;
      uwTerminalWait_50MS = 0;
      enEQStates = EQ_REDUCED_SPEED_AUTO;
   }
   else if( Auto_Recall(0, ucRecallFloor, RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR) ) // set the speed limit ks note: bound speed to >= 75, and <= 150 fpm
   {
      uwInitialWait_50MS = 0;
      uwTerminalWait_50MS = 0;
      enEQStates = EQ_WAIT_FOR_HOISTWAY_CHECK_ACTIVATION;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void EQ_Wait_For_Hoistway_Check_Activation( void )
{/*
   gstCurrentModeRules[MODE_A__CW_DRAIL].bParkingEnabled = 0;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreHallCall = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bAutoDoorOpen = 0;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreCarCall_F = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreCarCall_R = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bDoorHold = 0;*/

   gstCurrentModeRules[MODE_A__SEISMC].bParkingEnabled = 0;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreHallCall = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bAutoDoorOpen = 0;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreCarCall_F = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreCarCall_R = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bDoorHold = 0;

   if( Param_ReadValue_1Bit( enPARAM1__EnableEarthQuake ) && GetEmergencyBit(EmergencyBF_EQ_AutoMode_InProgress) )
   {
      uwInitialWait_50MS = 0;
      uwTerminalWait_50MS = 0;
      enEQStates = EQ_REDUCED_SPEED_AUTO;
   }
   else if( uwTerminalWait_50MS >= MAX_WAIT_RESCAN_50MS )
   {
      uwInitialWait_50MS = 0;
      uwTerminalWait_50MS = 0;
      enEQStates = EQ_FAULT_WITH_DOORS_OPEN;
   }
   else
   {
      if( GetInputValue( enIN_EQ_HOISTWAY_SCAN ) )
      {
         uwInitialWait_50MS = 0;
         uwTerminalWait_50MS = 0;
         enEQStates = EQ_REDUCED_SPEED_AUTO;
      }
      if( Auto_Recall(0, ucRecallFloor, RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR) )
      {
         SetFault(FLT__EQ);
      }
      uwTerminalWait_50MS++;
   }

   //Auto_Oper( 0 );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void EQ_Reduced_Speed_Auto( void )
{/*
   gstCurrentModeRules[MODE_A__CW_DRAIL].bParkingEnabled = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreHallCall = 0;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bAutoDoorOpen = 1;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreCarCall_F = 0;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreCarCall_R = 0;
   gstCurrentModeRules[MODE_A__CW_DRAIL].bDoorHold = Param_ReadValue_1Bit(enPARAM1__Enable_Freight_Doors) &&
      !Param_ReadValue_1Bit(enPARAM1__Enable_Freight_AutoClose);*/

   gstCurrentModeRules[MODE_A__SEISMC].bParkingEnabled = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreHallCall = 0;
   gstCurrentModeRules[MODE_A__SEISMC].bAutoDoorOpen = 1;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreCarCall_F = 0;
   gstCurrentModeRules[MODE_A__SEISMC].bIgnoreCarCall_R = 0;
   gstCurrentModeRules[MODE_A__SEISMC].bDoorHold = Param_ReadValue_1Bit(enPARAM1__Enable_Freight_Doors) &&
      !Param_ReadValue_1Bit(enPARAM1__Enable_Freight_AutoClose);

   uint8_t ucSpeedLimit = Param_ReadValue_8Bit(enPARAM8__EQ_Hoistway_Scan_Speed);
   if( ucSpeedLimit < MIN_EQ_HOISTWAY_SCAN__FPM )
   {
      ucSpeedLimit = MIN_EQ_HOISTWAY_SCAN__FPM;
   }
   else if( ucSpeedLimit > 150 )
   {
       ucSpeedLimit = MAX_EQ_HOISTWAY_SCAN__FPM;
   }
   Auto_SetSpeed(ucSpeedLimit);

   SetEmergencyBit(EmergencyBF_EQ_AutoMode_InProgress);

   Auto_Oper(0);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void EQ_Lamps( void )
{
   if(bCW_Derail_Input_Active)
   {
      SetOutputValue( enOUT_EQ_SLOW_LAMP, 0);
      SetOutputValue(enOUT_LMP_EQ, 1);
      if( ( uwEQModeStatusTimer_1MS > MOD_RUN_TIME_1MS * 2 ) )
      {
         SetOutputValue( enOUT_LMP_SEISMIC_STATUS, !GetOutputValue(enOUT_LMP_SEISMIC_STATUS) );
         uwEQModeStatusTimer_1MS = 0;
      }
      else
      {
         uwEQModeStatusTimer_1MS += MOD_RUN_TIME_1MS;
      }
   }
   else if( bSeismic_Input_Active )
   {
      SetOutputValue( enOUT_EQ_SLOW_LAMP, 0);
      if( ( uwEQModeIndicatorTimer_1MS > MOD_RUN_TIME_1MS * 2 ) )
      {
         SetOutputValue( enOUT_LMP_EQ, !GetOutputValue(enOUT_LMP_EQ) );
         uwEQModeIndicatorTimer_1MS = 0;
      }
      else
      {
         uwEQModeIndicatorTimer_1MS += MOD_RUN_TIME_1MS;
      }
      SetOutputValue( enOUT_LMP_SEISMIC_STATUS, 1);
   }
   else
   {
      if( enEQStates == EQ_FAULT_WITH_DOORS_OPEN )
      {
         if( ( uwEQModeSlowDownTimer_1MS > MOD_RUN_TIME_1MS * 2 ) )
         {
            SetOutputValue( enOUT_EQ_SLOW_LAMP, !GetOutputValue(enOUT_EQ_SLOW_LAMP) );
            uwEQModeSlowDownTimer_1MS = 0;
         }
         else
         {
            uwEQModeSlowDownTimer_1MS += MOD_RUN_TIME_1MS;
         }
         if( ( uwEQModeIndicatorTimer_1MS > MOD_RUN_TIME_1MS * 2 ) )
         {
            SetOutputValue( enOUT_LMP_EQ, !GetOutputValue(enOUT_LMP_EQ) );
            uwEQModeIndicatorTimer_1MS = 0;
         }
         else
         {
            uwEQModeIndicatorTimer_1MS += MOD_RUN_TIME_1MS;
         }
         SetOutputValue( enOUT_LMP_SEISMIC_STATUS, 1);

      }
      else if( enEQStates == EQ_WAIT_TO_ACTIVATE_HOISTWAY_CHECK )
      {
         SetOutputValue(enOUT_EQ_SLOW_LAMP, 1);

         if( ( uwEQModeIndicatorTimer_1MS > MOD_RUN_TIME_1MS * 2 ) )
         {
            SetOutputValue( enOUT_LMP_EQ, !GetOutputValue(enOUT_LMP_EQ) );
            uwEQModeIndicatorTimer_1MS = 0;
         }
         else
         {
            uwEQModeIndicatorTimer_1MS += MOD_RUN_TIME_1MS;
         }
         SetOutputValue( enOUT_LMP_SEISMIC_STATUS, 1);

      }
      else if( ( enEQStates == EQ_MOVE_TO_TOP )
        ||( enEQStates == EQ_MOVE_TO_BOTTOM )
        ||( enEQStates == EQ_MOVE_TO_ORIGINAL_FLOOR ) )
      {
         SetOutputValue(enOUT_EQ_SLOW_LAMP, 1);

         if( ( uwEQModeIndicatorTimer_1MS > MOD_RUN_TIME_1MS * 2 ) )
         {
            SetOutputValue( enOUT_LMP_EQ, !GetOutputValue(enOUT_LMP_EQ) );
            uwEQModeIndicatorTimer_1MS = 0;
         }
         else
         {
            uwEQModeIndicatorTimer_1MS += MOD_RUN_TIME_1MS;
         }
         SetOutputValue( enOUT_LMP_SEISMIC_STATUS, 1);
      }
      else if( enEQStates == EQ_WAIT_FOR_HOISTWAY_CHECK_ACTIVATION )
      {
         if( ( uwEQModeSlowDownTimer_1MS > MOD_RUN_TIME_1MS * 2 ) )
         {
            SetOutputValue( enOUT_EQ_SLOW_LAMP, !GetOutputValue(enOUT_EQ_SLOW_LAMP) );
            uwEQModeSlowDownTimer_1MS = 0;
         }
         else
         {
            uwEQModeSlowDownTimer_1MS += MOD_RUN_TIME_1MS;
         }
         SetOutputValue(enOUT_LMP_EQ,1);
         SetOutputValue( enOUT_LMP_SEISMIC_STATUS, 1);

      }
      else if( enEQStates == EQ_REDUCED_SPEED_AUTO )
      {
         SetOutputValue(enOUT_EQ_SLOW_LAMP, 1);
         SetOutputValue(enOUT_LMP_EQ,1);
         SetOutputValue( enOUT_LMP_SEISMIC_STATUS, 1);
      }
   }
}
/*-----------------------------------------------------------------------------


/*-----------------------------------------------------------------------------

 Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 1500;
   pstThisModule->uwRunPeriod_1ms = 200;

   return 0;
}

/*-----------------------------------------------------------------------------
Earthquake Specifications from Functional Test Procedures Document for 2.41c

   Set the counterweight height in the fire/earthquake menu and bring the car near it.

   While the car is not in a door zone, turn off the counterweight input. Verify the car moves away from the counterweight point and stops at the next landing.

   Put back in the counterweight signal and reset earthquake. Turn off the seismic input and verify the seismic fault shows up.

   Put back the seismic signal and reset earthquake.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   if( SRU_Read_ResetButton(eRESET_BUTTON_CW) || !Param_ReadValue_1Bit( enPARAM1__EnableEarthQuake ) )
   {
      ClrEmergencyBit(EmergencyBF_CW_Derail_InProgress);
      ClrEmergencyBit(EmergencyBF_EQ_Seismic_InProgress);
      ClrEmergencyBit(EmergencyBF_CW_Recall_Complete);
      ClrEmergencyBit(EmergencyBF_EQ_AutoMode_InProgress);
/*
      gstCurrentModeRules[MODE_A__CW_DRAIL].bParkingEnabled = 0;
      gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreHallCall = 1;
      gstCurrentModeRules[MODE_A__CW_DRAIL].bAutoDoorOpen = 0;
      gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreCarCall_F = 1;
      gstCurrentModeRules[MODE_A__CW_DRAIL].bIgnoreCarCall_R = 1;
      gstCurrentModeRules[MODE_A__CW_DRAIL].bDoorHold = 0;
*/
      gstCurrentModeRules[MODE_A__SEISMC].bParkingEnabled = 0;
      gstCurrentModeRules[MODE_A__SEISMC].bIgnoreHallCall = 1;
      gstCurrentModeRules[MODE_A__SEISMC].bAutoDoorOpen = 0;
      gstCurrentModeRules[MODE_A__SEISMC].bIgnoreCarCall_F = 1;
      gstCurrentModeRules[MODE_A__SEISMC].bIgnoreCarCall_R = 1;
      gstCurrentModeRules[MODE_A__SEISMC].bDoorHold = 0;

      Param_WriteValue_8Bit(enPARAM8__Max_Runtime_1sec, original_max_run_time_1SEC);
      SetOutputValue(enOUT_EQ_SLOW_LAMP, 0);
      SetOutputValue(enOUT_LMP_EQ,0);
      SetOutputValue( enOUT_LMP_SEISMIC_STATUS, 0);
   }

   if ( Param_ReadValue_1Bit( enPARAM1__EnableEarthQuake ) )
   {
      if ( ( !Param_ReadValue_1Bit(enPARAM1__CW_Derail_NO) && !GetInputValue( enIN_CW_DERAIL ) )
        || (  Param_ReadValue_1Bit(enPARAM1__CW_Derail_NO) &&  GetInputValue( enIN_CW_DERAIL ) ) )
      {
         SetEmergencyBit(EmergencyBF_CW_Derail_InProgress);
         bCW_Derail_Input_Active = 1;
      }
      else
      {
         bCW_Derail_Input_Active = 0;
      }

      if ( !GetInputValue( enIN_SEISMIC ) )
      {
         SetEmergencyBit(EmergencyBF_EQ_Seismic_InProgress);
         bSeismic_Input_Active = 1;
      }
      else
      {
         bSeismic_Input_Active = 0;
      }
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
