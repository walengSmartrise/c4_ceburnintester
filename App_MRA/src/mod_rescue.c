/******************************************************************************
*
* @file     mod_rescue.c
* @brief    Battery rescue operation
* @version  V1.00
* @date     1, Feb 2017
*
* @note
*
******************************************************************************/

/*----------------------------------------------------------------------------
*
* Place #include files required to compile this source file here.
*
*----------------------------------------------------------------------------*/

#include "mod.h"
#include "GlobalData.h"
#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
/*----------------------------------------------------------------------------
*
* Place function prototypes that will be used only by this file here.
* This section need only include "forward referenced" functions.
*
*----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
*
* Place #define constants and macros that will be used only by this
* source file here.
*
*----------------------------------------------------------------------------*/
// Minimum position diff to override light load
#define MIN_POS_DIFF                            (TWO_FEET)

#define DELAY_BEFORE_STARTING_RESCUE_50MS      (40)
#define DELAY_IN_DZ_FAULT_50MS                 (80)
#define INPUT_DEBOUNCE_50MS                    (20)
#define INPUT_DEBOUNCE_250MS                   (4)
/*----------------------------------------------------------------------------
*
* Place typedefs, structs, unions, and enums that will be used only
* by this source file here.
*
*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*
* Place static variables that will be used only by this file here.
*
*----------------------------------------------------------------------------*/
static uint8_t bRescueActive; // Mode flag

static uint8_t ucRescueFloor = INVALID_FLOOR;
static enum en_doors eRescueDoor;

static uint8_t ucBatteryOnTimer_50ms;
static uint8_t ucInDoorzoneTimer_50ms;
static uint8_t bButtonsArmed;
static uint8_t ucCounterUp_50ms;
static uint8_t ucCounterDown_50ms;

static uint8_t bRecTrvDirCommand; // flag for running HPV recommended travel direction check
static enum direction_enum eRecTrvDir; // flag signaling if HPV recommended travel direction check completed

static uint8_t bSafetyRescue;

static uint8_t ucDirectionCounter_50ms;
/*----------------------------------------------------------------------------
*
* Define global variables that will be used by both this source file
* and by other source files here.
*
*----------------------------------------------------------------------------*/
struct st_module gstMod_Rescue = { .pfnInit = Init, .pfnRun = Run, };

/*----------------------------------------------------------------------------
*
* Place function bodies here.
*
*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

*----------------------------------------------------------------------------*/
uint8_t Rescue_GetActiveFlag()
{
   return bRescueActive;
}
uint8_t Rescue_GetManualTractionRescue()
{
   return ( bRescueActive )
       && ( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
       && ( GetOperation_ManualMode() == MODE_M__CONSTRUCTION );
}
uint8_t Rescue_GetAutoTractionRescue()
{
   return ( bRescueActive )
       && ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
       && ( GetOperation_AutoMode() == MODE_A__BATT_RESQ );
}
/*----------------------------------------------------------------------------
Returns 1 when the c4 controller is relying on the drive to determine its
movement direction rather than determining direction by distance alone.
This should be the default option
*----------------------------------------------------------------------------*/
uint8_t Rescue_NeedToGetRecTrvDir_HPV(void)
{
   return ( bRescueActive )
       && ( Param_ReadValue_1Bit( enPARAM1__Rescue_RecTrvDir ) )
       && ( Param_ReadValue_8Bit( enPARAM8__DriveSelect ) == enDriveType__HPV );
}
uint8_t Rescue_NeedToGetRecTrvDir_KEB(void)
{
   return ( bRescueActive )
       && ( Param_ReadValue_1Bit( enPARAM1__Rescue_RecTrvDir ) )
       && ( Param_ReadValue_8Bit( enPARAM8__DriveSelect ) == enDriveType__KEB );
}
/*----------------------------------------------------------------------------
From HPV Series 2 manual on Recommended travel direction
"
6. When the Rec Travel On (C4) output is
high, the state of Rec Travl Dir (C3) will be
the direction that the drive suggests the
elevator should go for the lightest load.
   a. If Rec Travl Dir (C3) remains low, the
   controller should continue running in the
   same direction.
   b. If Rec Travl Dir (C3) goes high, the
   controller should immediately drop the
   original direction input and instead issue
   the opposite direction.
"
The commanded direction is DIR__UP
-----------------------------------------------
From KEB Manual on UPS operation
"
UPS Operation
During UPS Operation there is the option for the drive to determine the
direction of travel based on the load to utilize the least amount of power under
a UPS supply.
To utilize this option, both direction inputs must be given for a run during UPS
Operation mode.  During the brake release sequence the drive will measure
the motor torque and travel in the direction of least resistance.  The conditions
for determining direction of travel are:

•  Less than Balanced = Up
• Greater than Balanced = Down

During UPS Operation mode, if a single direction input is given, the direction
of travel will be normal, as directed.
During regular operation (not UPS mode), if both direction inputs are signaled
simultaneously, a ‘Direction Selection Failure’ fault will occur.
"
Note: UPS speed on drive must be set

*----------------------------------------------------------------------------*/
#define KEB_DIRECTION_SELECT_SPEED_THRESHOLD_FPM      (2)
#define KEB_DIRECTION_SELECT_SPEED_DELAY_50MS         (5)
static void Rescue_UpdateRecTrvDir()
{
   if( ( Rescue_GetRecTrvDirCommand_HPV() )
    && ( GetInputValue( enIN_RecTrvOn ) )
    && ( !eRecTrvDir ) )
   {
      eRecTrvDir = ( GetInputValue( enIN_RecTrvDir ) ) ? DIR__DN:DIR__UP;
   }
   else if( ( Rescue_GetRecTrvDirCommand_KEB() )
         && ( GetMotion_RunFlag() )
         && ( !eRecTrvDir ) )
   {
      int16_t wSpeed = GetPosition_Velocity();
      if( wSpeed >= KEB_DIRECTION_SELECT_SPEED_THRESHOLD_FPM )
      {
         if( ++ucDirectionCounter_50ms >= KEB_DIRECTION_SELECT_SPEED_DELAY_50MS )
         {
            eRecTrvDir = DIR__UP;
         }
      }
      else if( wSpeed <= (-1*KEB_DIRECTION_SELECT_SPEED_THRESHOLD_FPM) )
      {
         if( ++ucDirectionCounter_50ms >= KEB_DIRECTION_SELECT_SPEED_DELAY_50MS )
         {
            eRecTrvDir = DIR__DN;
         }
      }
      else
      {
         ucDirectionCounter_50ms = 0;
      }
   }
}

/*----------------------------------------------------------------------------
mod_motion Process_Stopped

enPARAM1__Rescue_RecTrvDir
*----------------------------------------------------------------------------*/
uint8_t Rescue_GetRecTrvDirCommand_HPV()
{
   return ( bRecTrvDirCommand )
       && ( bRescueActive )
       && ( Rescue_NeedToGetRecTrvDir_HPV() )
       && ( GetOperation_AutoMode() == MODE_A__BATT_RESQ );
}
uint8_t Rescue_GetRecTrvDirCommand_KEB()
{
   return ( bRecTrvDirCommand )
       && ( bRescueActive )
       && ( Rescue_NeedToGetRecTrvDir_KEB() )
       && ( GetOperation_AutoMode() == MODE_A__BATT_RESQ );
}

/*-----------------------------------------------------------------------------
   Moves to closest floor. If light load is active, goes to closest floor above
   Improvements: check current draw in either direction determine direction
-----------------------------------------------------------------------------*/
static uint8_t GetRescueRecallFloor()
{
   uint8_t ucFloor_Above = GetClosestValidOpening( DIR__UP, DOOR_ANY );
   uint8_t ucFloor_Below = GetClosestValidOpening( DIR__DN, DOOR_ANY );
   uint8_t ucRecallFloor = ucFloor_Below;

   uint32_t uiCurrentPos = GetPosition_PositionCount();
   uint32_t uiFloorPos_Above = (ucFloor_Above != INVALID_FLOOR) ? (gpastFloors[ucFloor_Above].ulPosition):MAX_24BIT_VALUE;
   uint32_t uiFloorPos_Below = (ucFloor_Below != INVALID_FLOOR) ? (gpastFloors[ucFloor_Below].ulPosition):0;
   uint32_t uiDistance_Above = uiFloorPos_Above-uiCurrentPos;
   uint32_t uiDistance_Below = uiCurrentPos-uiFloorPos_Below;
   /* Destination above is closer */
   if( uiDistance_Above <= uiDistance_Below )
   {
      ucRecallFloor = ucFloor_Above;
   }
   /* If light load active, go to the nearest floor above */
   else if( GetLightLoadFlag() )
   {
      ucRecallFloor = ucFloor_Above;
      /* Unless the floor below is much closer */
      if( (uiDistance_Below-uiDistance_Above) < (uint32_t)MIN_POS_DIFF )
      {
         ucRecallFloor = ucFloor_Below;
      }
   }
   /* Otherwise, move to the lower floor */

   return ucRecallFloor;
}
/*-----------------------------------------------------------------------------
   If automatic rescue is disabled, the car is in manual traction rescue mode.

   Manual rescue is initiated by holding the brake release button. This will pass three signals.

      MANUAL_PICK input to the MR board. If ON, in construction, and hall doors are correct…
         Turns SAFETY_RESCUE output ON.
            Picks the BS Relay, which picks the BL contactor if the M contactor is low.

      BRAKE_RELEASE output from the drive, this will pick the B1 contactor once the BL contactor is picked. This output is called BRAKE CONTROL on the KEB and typically comes from Output RLY 1.

      MANUAL_RESCUE

   When the MR UP or DN signals are on, the BRAKE_RELEASE output will pick the B1 contactor. And pick the brake.
-----------------------------------------------------------------------------*/
static void Rescue_Manual( uint8_t bInit )
{
   /* Set speed limit - Moved to mod_oper_manual.c */

   if( getDoorZone(DOOR_ANY) )
   {
      /* Command Stop */
      SetOperation_MotionCmd(MOCMD__EMERG_STOP);

      /* Set Fault */
      SetFault(FLT__RESCUE_IN_DZ);

      /* User must command doors open... */
   }
   else if( ( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
         && ( GetOperation_ManualMode() == MODE_M__CONSTRUCTION )
         && ( AllHallDoorsClosed() )
         && ( GetInputValue( enIN_MANUAL_PICK ) ) )
   {
      bSafetyRescue = 1;
      CloseAnyOpenDoor();
   }
   else
   {
      /* Command Stop */
      SetOperation_MotionCmd(MOCMD__EMERG_STOP);

      /* Set Fault */
      SetFault(FLT__RESCUE_INVALID);

      bSafetyRescue = 0;
      ucCounterUp_50ms = 0;
      ucCounterDown_50ms = 0;
      CloseAnyOpenDoor();
   }
}
/*-----------------------------------------------------------------------------
   If automatic rescue is not disabled,
-----------------------------------------------------------------------------*/
static void Rescue_Automatic()
{
   static uint8_t bInit;
   uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
   uint8_t bValidOpening = GetFloorOpening_Front(ucCurrentFloor) || GetFloorOpening_Rear(ucCurrentFloor);
   SetInDestinationDoorzone(0); // Don't allow doors to open
   /* If in DZ, hold doors open and go OOS */
   if( getDoorZone(DOOR_ANY) && InsideDeadZone() && bValidOpening )
   {
      /* Command Stop */
      SetOperation_MotionCmd(MOCMD__EMERG_STOP);

      /* Set Fault */
      SetFault(FLT__RESCUE_IN_DZ);

      /* Hold open doors */
      if( !GetMotion_RunFlag() )
      {
         SetInDestinationDoorzone(1);
         if( GetFloorOpening_Front(ucCurrentFloor) )
         {
            SetDoorCommand( DOOR_FRONT, DOOR_COMMAND__OPEN_UI_REQUEST );
         }
         else
         {
            SetDoorCommand( DOOR_REAR, DOOR_COMMAND__OPEN_UI_REQUEST );
         }
      }
   }
   else if( ucRescueFloor >= GetFP_NumFloors() )
   {
      bInit = 1;
      if( Rescue_NeedToGetRecTrvDir_HPV() )
      {
         SetOperation_MotionCmd(MOCMD__NORMAL_STOP);
         bRecTrvDirCommand = 1;
         if( eRecTrvDir != DIR__NONE )// If recommended travel direction has been received
         {
            ucRescueFloor = GetClosestValidOpening( eRecTrvDir, DOOR_ANY );
            if( ucRescueFloor >= GetFP_NumFloors() )
            {
               ucRescueFloor = GetClosestValidOpening( ( -1*eRecTrvDir ), DOOR_ANY );
            }
         }
      }
      else if( Rescue_NeedToGetRecTrvDir_KEB() )
      {
         /* For KEB, Easy Travel Direction option requires that the drive be given both up and down signals
          * and the drive will automatically choose its direction. No separate direction determining stage required */
         bRecTrvDirCommand = 1;
         uint8_t ucFloor = GetClosestValidOpening( eRecTrvDir, DOOR_ANY );
         if( GetMotion_RunFlag() )
         {
            if( ( ( GetMotion_SpeedCommand() > 0 ) && ( ucFloor < GetOperation_DestinationFloor() ) )
             || ( ( GetMotion_SpeedCommand() < 0 ) && ( ucFloor > GetOperation_DestinationFloor() ) ) )
            {
               SetFault(FLT__RESCUE_INVALID);
            }
         }
         else if( !gstFault.bActiveFault )
         {
//            //todo rm test
//            if( !eRecTrvDir )
//            {
//               if( GetPosition_PositionCount() >= gpastFloors[ucFloor].ulPosition )
//               {
//                  eRecTrvDir = DIR__UP;
//               }
//               else
//               {
//                  eRecTrvDir = DIR__DN;
//               }
//            }
            SetDestination(ucFloor);
         }
      }
      else
      {
         ucRescueFloor = GetRescueRecallFloor();
         SetFault(FLT__RESCUE_INVALID);
      }
   }
   else
   {
      bRecTrvDirCommand = 0;
      eRescueDoor = (gpastFloors[ucRescueFloor].bFrontOpening) ? DOOR_FRONT:DOOR_REAR;
      en_recall_door_command eRecallDoorCommand = ( eRescueDoor == DOOR_FRONT ) ? RECALL_DOOR_COMMAND__OPEN_FRONT_DOOR:RECALL_DOOR_COMMAND__OPEN_REAR_DOOR;
      if( ( getDoorZone(DOOR_ANY) )
       && ( !GetMotion_RunFlag() )
       && ( ucRescueFloor == ucCurrentFloor ) )
      {
         SetInDestinationDoorzone(1);
      }
      if(Auto_Recall( bInit,ucRescueFloor, eRecallDoorCommand ))
      {
         if(ucInDoorzoneTimer_50ms >= DELAY_IN_DZ_FAULT_50MS)
         {
            SetFault(FLT__RESCUE_IN_DZ);
         }
         else
         {
            ucInDoorzoneTimer_50ms++;
         }
      }
      bInit = 0;
   }
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
void Auto_Rescue( uint8_t bInitMode )
{
   static uint8_t ucDebounce_50ms;
   /* Set speed limit */
   uint16_t uwLevelingSpeed = Param_ReadValue_8Bit(enPARAM8__AutoRescueSpeed_fpm);
   SetOperation_SpeedLimit(uwLevelingSpeed);

   if( bInitMode )
   {
      ucRescueFloor = INVALID_FLOOR;
      bButtonsArmed = 0;
      ucCounterUp_50ms = 0;
      ucCounterDown_50ms = 0;
      ucInDoorzoneTimer_50ms = 0;
      ucBatteryOnTimer_50ms = 0;
      ucDebounce_50ms = 0;
      bRecTrvDirCommand = 0;
      eRecTrvDir = DIR__NONE;
      ucDirectionCounter_50ms = 0;
   }

   if( GetInputValue(enIN_AUTO_RESCUE) )
   {
      if( ucDebounce_50ms < INPUT_DEBOUNCE_50MS )
      {
         ucDebounce_50ms++;
      }
   }
   else
   {
      ucDebounce_50ms = 0;
   }

   if(ucBatteryOnTimer_50ms >= DELAY_BEFORE_STARTING_RESCUE_50MS)
   {
      uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
      uint8_t bValidOpening = GetFloorOpening_Front(ucCurrentFloor) || GetFloorOpening_Rear(ucCurrentFloor);
      if( ucDebounce_50ms >= INPUT_DEBOUNCE_50MS )
      {
         Rescue_Automatic();
         bSafetyRescue = 0;
      }
      else if( getDoorZone(DOOR_ANY) && InsideDeadZone() && bValidOpening )
      {
         /* Command Stop */
         SetOperation_MotionCmd(MOCMD__EMERG_STOP);

         /* Set Fault */
         SetFault(FLT__RESCUE_IN_DZ);

         /* Hold open doors */
         if( !GetMotion_RunFlag() && !GetPosition_Velocity() )
         {
            if( GetFloorOpening_Front(ucCurrentFloor) )
            {
               SetDoorCommand( DOOR_FRONT, DOOR_COMMAND__OPEN_HOLD_REQUEST );
            }
            else
            {
               SetDoorCommand( DOOR_REAR, DOOR_COMMAND__OPEN_HOLD_REQUEST );
            }
         }
      }
      else
      {
         SetFault(FLT__RESCUE_START);
      }
   }
   else
   {
      ucBatteryOnTimer_50ms++;
      SetFault(FLT__RESCUE_START);
   }
}
/*-----------------------------------------------------------------------------
   Check for battery fault input going high
 -----------------------------------------------------------------------------*/
#define BATTERY_FAULT_DEBOUNCE_LIMIT     (5)
static void CheckFor_BatteryFault()
{
   static uint8_t ucDebounce;
   if( GetInputValue(enIN_BATTERY_FLT)
   && !GetMotion_RunFlag()
   && getDoorZone(DOOR_ANY) )
   {
      if(ucDebounce >= BATTERY_FAULT_DEBOUNCE_LIMIT)
      {
         SetFault(FLT__BATTERY_FAULT);
      }
      else
      {
         ucDebounce++;
      }
   }
   else
   {
      ucDebounce = 0;
   }
}
/*-----------------------------------------------------------------------------

 Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 2000;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_RESCUE_1MS;

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static uint8_t ucDebounceRescue_250ms;

   CheckFor_BatteryFault();

   if( GetInputValue( enIN_BATTERY_POWER ) )
   {
      if( ucDebounceRescue_250ms < INPUT_DEBOUNCE_50MS )
      {
         ucDebounceRescue_250ms++;
      }
   }
   else
   {
      ucDebounceRescue_250ms = 0;
      bSafetyRescue = 0;
   }
   bRescueActive = ( ucDebounceRescue_250ms >= INPUT_DEBOUNCE_250MS ) ? 1:0;

   static enum en_classop eLastClassOp;
   if( bRescueActive && !Rescue_GetManualTractionRescue() && !Rescue_GetAutoTractionRescue() )
   {
      SetFault(FLT__RESCUE_START);
   }

   if( ( Rescue_GetManualTractionRescue() )
    && ( !GetInputValue(enIN_AUTO_RESCUE) ) )
   {
      Rescue_Manual( eLastClassOp != GetOperation_ClassOfOp() );
   }

   eLastClassOp = GetOperation_ClassOfOp();

   Rescue_UpdateRecTrvDir();

   SetOutputValue(enOUT_SAFETY_RESCUE, bSafetyRescue);
   return 0;
}
/*----------------------------------------------------------------------------
*
* End of file.
*
*----------------------------------------------------------------------------*/
