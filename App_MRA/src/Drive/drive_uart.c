/******************************************************************************
 *
 * @file     mod_drive.c
 * @brief
 * @version  V1.00
 * @date     23, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"

#include "sru_a.h"
#include <stdint.h>
#include <string.h>
#include "sys.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define DRIVE_UART   (LPC_UART1)
#define DRIVE_UART_IRQ   (UART1_IRQn)

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
/* Transmit and receive ring buffers */
STATIC RINGBUFF_T txring, rxring;

/* Ring buffer size */
#define UART_RB_SIZE 128

/* Transmit and receive buffers */
static uint8_t rxbuff[UART_RB_SIZE], txbuff[UART_RB_SIZE];

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
uint32_t UART_SendRB(const void *data, int bytes)
{
   uint32_t uiReturn = 0;
   if(RingBuffer_GetFree(&txring) >= bytes)
   {
      Chip_UART_SendRB(DRIVE_UART, &txring, data, bytes);
   }
   return uiReturn;
}
uint8_t UART_ReadByte(uint8_t *pucByte)
{
   if(!RingBuffer_IsEmpty(&rxring))
   {
      if(RingBuffer_Pop(&rxring, pucByte))
      {
         return 1;
      }
   }
   return 0;
}
static uint32_t GetDebugKEBBaudRate()//todo remove
{
   uint32_t uiBaudRate = 115200;//Default
   uint8_t ucDebugParam = Param_ReadValue_8Bit(enPARAM8__DEBUG_KEB_BaudRate);
   if( ucDebugParam == 1 )// 9600
   {
      uiBaudRate = 9600;
   }
   else if( ucDebugParam == 2 )// 19200
   {
      uiBaudRate = 19200;
   }
   else if( ucDebugParam == 3 )// 38400
   {
      uiBaudRate = 38400;
   }
   else if( ucDebugParam == 4 )// 55500
   {
      uiBaudRate = 55500;
   }
   return uiBaudRate;
}
void Init_UART_RingBuffer(enum en_drive_type eDrive)
{
   Chip_UART_Init(DRIVE_UART);
   switch(eDrive)
   {
      case enDriveType__KEB:
         /* Setup UART for 115.2K7N1 */
         Chip_UART_SetBaudFDR(DRIVE_UART, GetDebugKEBBaudRate());
         Chip_UART_ConfigData(DRIVE_UART, (UART_LCR_WLEN7
                                        | UART_LCR_SBS_1BIT
                                        | UART_LCR_PARITY_EVEN
                                        | UART_LCR_PARITY_EN));
         Chip_UART_SetupFIFOS(DRIVE_UART, (UART_FCR_FIFO_EN
                                        | UART_FCR_TRG_LEV0 ));
         Chip_UART_ClearModemControl(DRIVE_UART, UART_MCR_AUTO_RTS_EN);
         /* RTS output is active low. Tranceiver IC req's active high */
         Chip_UART_ClearModemControl(DRIVE_UART, UART_MCR_RTS_CTRL);
         Chip_UART_TXEnable(DRIVE_UART);
         break;

      case enDriveType__HPV:
         /* Setup UART for 19.2K8N1 */
//         Chip_UART_SetBaud(DRIVE_UART, 19200);
         Chip_UART_SetBaudFDR(DRIVE_UART, 19200);
         Chip_UART_ConfigData(DRIVE_UART, (UART_LCR_WLEN8
                                        | UART_LCR_SBS_1BIT));
         Chip_UART_SetupFIFOS(DRIVE_UART, (UART_FCR_FIFO_EN
                                        | UART_FCR_TRG_LEV0 ));
         Chip_UART_ClearModemControl(DRIVE_UART, UART_MCR_AUTO_RTS_EN);
         /* RTS output is active low. Tranceiver IC req's active high */
         Chip_UART_ClearModemControl(DRIVE_UART, UART_MCR_RTS_CTRL);
         Chip_UART_TXEnable(DRIVE_UART);
         break;
      case enDriveType__DSD:
         /* Setup UART for 19.2K8N1 */
//         Chip_UART_SetBaud(DRIVE_UART, 19200);
         Chip_UART_SetBaudFDR(DRIVE_UART, 19200);
         Chip_UART_ConfigData(DRIVE_UART, (UART_LCR_WLEN8
                                        | UART_LCR_SBS_1BIT));
         Chip_UART_SetupFIFOS(DRIVE_UART, (UART_FCR_FIFO_EN
                                        | UART_FCR_TRG_LEV0 ));
         Chip_UART_ClearModemControl(DRIVE_UART, UART_MCR_AUTO_RTS_EN);
         /* RTS output is active low. Tranceiver IC req's active high */
         Chip_UART_ClearModemControl(DRIVE_UART, UART_MCR_RTS_CTRL);
         Chip_UART_TXEnable(DRIVE_UART);
         break;
      default:
         break;
   }
   /* Before using the ring buffers, initialize them using the ring
      buffer init function */
   RingBuffer_Init(&rxring, rxbuff, 1, UART_RB_SIZE);
   RingBuffer_Init(&txring, txbuff, 1, UART_RB_SIZE);

   Chip_UART_IntEnable(DRIVE_UART, (UART_IER_THREINT
                           | UART_IER_RBRINT));

   /* preemption = 1, sub-priority = 1 */
#if INCREASED_PRIORITY_FOR_BITBANGED_CPLD_SPI
   NVIC_SetPriority(DRIVE_UART_IRQ, 1);
#endif
   NVIC_EnableIRQ(DRIVE_UART_IRQ);
}

void UART1_IRQHandler(void)
{
   // handles tx - same as default handler
	uint32_t Gets = ( Chip_UART_GetIntsEnabled(DRIVE_UART) & UART_IER_THREINT );
	uint32_t Rets =( Chip_UART_ReadLineStatus(DRIVE_UART) & UART_LSR_THRE );
   if ( ( Gets  != 0 )
     && ( Rets != 0 ) )
   {
      Chip_UART_TXIntHandlerRB(DRIVE_UART, &txring);
   }

   if (RingBuffer_IsEmpty(&txring)) {
      Chip_UART_IntDisable(DRIVE_UART, UART_IIR_INTID_THRE);
   }

   Chip_UART_RXIntHandlerRB(DRIVE_UART, &rxring);

}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
