/******************************************************************************
 *
 * @file     drive_keb_f5.c
 * @brief    
 * @version  V1.00
 * @date     10 October 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "sru_a.h"
#include <stdint.h>
#include <sys_drive.h>
#include "sys.h"
#include "motion.h"
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define KEB_MIN_CHAR   0x20
#define KEB_ZERO_CHAR  0x30
#define KEB_SRV_CHAR   (0x78)// Runtime service character
#define KEB_SRV_CHAR__RUNTIME   (0x78)
#define KEB_SRV_CHAR__WRITE     (0x48)
#define KEB_SRV_CHAR__READ      (0x47)
#define KEB_IID_CHAR   (KEB_ZERO_CHAR + 15U)
#define KEB_EOT_CHAR   0x04
#define KEB_ENQ_CHAR   0x05
#define KEB_STX_CHAR   0x02
#define KEB_ETX_CHAR   0x03
#define KEB_ACK_CHAR   0x06
#define KEB_NAK_CHAR   0x15

#define KEB_BCC        0x20//Logical XOR of packet + 0x20 if less than 0x20

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t ucKEB_IID = '1';
static int ucBytesToSend;
static uint8_t ucServiceChar;
static uint16_t uwRxErrorCounter;//Counts number of bad packets received
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Returns the number of bad bytes/packets detected
-----------------------------------------------------------------------------*/
uint16_t GetDriveRxErrorCount_KEB(void)
{
   return uwRxErrorCounter;
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static uint8_t CalculateChecksum( uint8_t* ucBuffer, uint8_t ucLength )
{
   uint8_t ucBCC = ucBuffer[0];

   for( uint8_t ucIndex = 1; ucIndex < ucLength; ucIndex++ )
   {
      ucBCC ^= ucBuffer[ ucIndex ];
   }

   if( ucBCC < KEB_MIN_CHAR )
   {
      ucBCC += KEB_MIN_CHAR;
   }

   return ucBCC;
}
/*-----------------------------------------------------------------------------


-----------------------------------------------------------------------------*/
static void ConvertWordToASCII( uint8_t *pucASCII, uint16_t uwValue )
{
   uint8_t ucLength = 4;
   uint8_t ucNibble;
   for(uint8_t i = 0; i < ucLength; i++)
   {
      ucNibble = uwValue >> (i*4) & 0xF;
      if(ucNibble <= 9)
      {
         ucNibble += '0';
      }
      else
      {
         ucNibble += 'A' - 10;
      }

      *(pucASCII + ucLength - 1 - i ) =ucNibble;
   }
}
/*-----------------------------------------------------------------------------


-----------------------------------------------------------------------------*/
static void ConvertDWordToASCII( uint8_t *pucASCII, uint32_t uiValue )
{
   uint8_t ucLength = 8;
   uint8_t ucNibble;

   for(uint8_t i = 0; i < ucLength; i++)
   {
      ucNibble = uiValue >> (i*4) & 0xF;
      if(ucNibble <= 9)
      {
         ucNibble += '0';
      }
      else
      {
         ucNibble += 'A' - 10;
      }

      *(pucASCII + ucLength - 1 - i ) =ucNibble;
   }
}

/*-----------------------------------------------------------------------------

   Converts a hex char '0', '1', ... 'F' to an unsigned number between 0 and 15.
   Returns 0xFF if ucChar is not a hex character.

-----------------------------------------------------------------------------*/
static uint8_t CharToNibble( uint8_t ucChar )
{
   uint8_t ucNibble;

   if ( (ucChar >= '0') && (ucChar <= '9') )
   {
      ucNibble = ucChar - '0';
   }
   else if ( (ucChar >= 'A') && (ucChar <= 'F') )
   {
      ucNibble = ucChar - 'A' + 10;
   }
   else
   {
      ucNibble = 0xFF;
   }

   return ucNibble;
}

/*-----------------------------------------------------------------------------
   Request to read parameter currently being written.
 -----------------------------------------------------------------------------*/
static void LoadParamReadRequest( void )
{
   uint8_t ucChecksum_Start = 3;
   uint8_t ucChecksum_End = 12;

   gstDrive.stTxBuffer.aucData[ 0 ] = ASCII_EOT_CHAR;
   gstDrive.stTxBuffer.aucData[ 1 ] = 'F';//ASCII_ZERO_CHAR;
   gstDrive.stTxBuffer.aucData[ 2 ] = 'F';//ASCII_ZERO_CHAR + 1;
   /* Start of BCC calc */
   gstDrive.stTxBuffer.aucData[ 3 ] = KEB_SRV_CHAR__READ;
   //TODO investigate why drive won't respond when IID = KEB_IID_CHAR
   gstDrive.stTxBuffer.aucData[ 4 ] = '1';//KEB_IID_CHAR;

   /* Fetch ID of parameter currently being updated */
   uint32_t uiParameterID = GetDriveParameterID();
   //5.1 Service 0/1: Reading a Parameter Value (extended/standard set addressing)
   ConvertWordToASCII(&gstDrive.stTxBuffer.aucData[ 5 ], (uint16_t) uiParameterID );
   ucBytesToSend += 4;

   gstDrive.stTxBuffer.aucData[ 9 ] = '0';
   gstDrive.stTxBuffer.aucData[ 10 ] = '1';

   gstDrive.stTxBuffer.aucData[ 11 ] = ASCII_ENQ_CHAR;

   gstDrive.stTxBuffer.aucData[ 12 ] = CalculateChecksum( &gstDrive.stTxBuffer.aucData[ucChecksum_Start], ucChecksum_End - ucChecksum_Start );

   gstDrive.stTxBuffer.uiIndex = 13;
   ucBytesToSend = 13;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadParamWriteRequest( void )
{
   Drive_Parameter_Object stParamToWrite;
   uint8_t ucChecksum_Start = 4;
   uint8_t ucChecksum_End = 21;

   /* Prefix packet with inverter addresssing */
   gstDrive.stTxBuffer.aucData[ 0 ] = ASCII_EOT_CHAR;
   gstDrive.stTxBuffer.aucData[ 1 ] = 'F';//ASCII_ZERO_CHAR;
   gstDrive.stTxBuffer.aucData[ 2 ] = 'F';//ASCII_ZERO_CHAR + 1;
   gstDrive.stTxBuffer.aucData[ 3 ] = ASCII_STX_CHAR;
   /* Start of BCC calc */
   gstDrive.stTxBuffer.aucData[ 4 ] = KEB_SRV_CHAR__WRITE;
   gstDrive.stTxBuffer.aucData[ 5 ] = '2';//KEB_IID_CHAR;

   stParamToWrite.uiParameterID = GetDriveParameterID();
   stParamToWrite.uiValue = GetDriveParameterValue();

   ConvertWordToASCII(&gstDrive.stTxBuffer.aucData[ 6 ], (uint16_t) stParamToWrite.uiParameterID );
   ucBytesToSend += 4;
   ConvertDWordToASCII(&gstDrive.stTxBuffer.aucData[ 10 ], stParamToWrite.uiValue );
   ucBytesToSend += 8;
   gstDrive.stTxBuffer.aucData[ 18 ] = '0';
   gstDrive.stTxBuffer.aucData[ 19 ] = '1';

   gstDrive.stTxBuffer.aucData[ 20 ] = ASCII_ETX_CHAR;

   gstDrive.stTxBuffer.aucData[ 21 ] = CalculateChecksum( &gstDrive.stTxBuffer.aucData[ucChecksum_Start], ucChecksum_End - ucChecksum_Start );
   gstDrive.stTxBuffer.uiIndex = 22;
   ucBytesToSend = 22;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadRuntimeMessage( void )
{
   gstDrive.stTxBuffer.aucData[ 0 ] = KEB_EOT_CHAR;
   gstDrive.stTxBuffer.aucData[ 1 ] = 'F';//KEB_ZERO_CHAR;
   gstDrive.stTxBuffer.aucData[ 2 ] = 'F';//KEB_ZERO_CHAR + 1;
   gstDrive.stTxBuffer.aucData[ 3 ] = KEB_STX_CHAR;
   //5.28 Service 17: Writing Process Data Type 2 (4*16bit)
   gstDrive.stTxBuffer.aucData[ 4 ] = 'x';
   gstDrive.stTxBuffer.aucData[ 5 ] = ucKEB_IID;

   //FB01 - Field bus control word
   uint16_t uwControlCommand = 0;
   int16_t wSpeed = GetMotion_SpeedCommand();
   if(gstDrive.bTriggerDriveReset)
   {
      uwControlCommand |= 1 << 1;//reset

      // Special function 2 (Fault reset) - Control Word Bit 10. Drive FB22 set to 16.
      uwControlCommand |= 1 << 10;
   }

   if( GetOperation_AutoMode() == MODE_A__BATT_RESQ )
   {
      // Special function 3 (UPS) - Control Word Bit 11. Drive FB23 set to 1.
      uwControlCommand |= 1 << 11;
   }

   if ( !gstFault.bActiveFault )
   {
      if( GetPickMFlag() )
      {
         uwControlCommand |= 0x01;  // enable
      }
      if ( GetPickDriveFlag() )
      {
         // mod_rescue.c When detecting travel direction of least resistance, give KEB both directions.
         if( Rescue_GetRecTrvDirCommand_KEB() )
         {
            uwControlCommand |= 0x04;
            uwControlCommand |= 0x08;
         }
         else if(GetMotion_Direction() == DIR__UP)
         {
            uwControlCommand |= 0x04;
         }
         else if(GetMotion_Direction() == DIR__DN)
         {
            uwControlCommand |= 0x08;
         }
      }
   }

   //Test DIP switch for running zero speed on KEB
   if( Drive_GetMotorLearnFlag() )
   {
#if 0//test
      // Spectial function 1 (Inspection Speed) - Control Word Bit 9
//            uwControlCommand |= 1 << 9;
#else
      uwControlCommand |= 1;
      uwControlCommand |= 0x4;
#endif
   }

   ConvertWordToASCII(&gstDrive.stTxBuffer.aucData[6], uwControlCommand);

   //FB02 - Field bus speed
   ConvertWordToASCII(&gstDrive.stTxBuffer.aucData[10], (uint16_t)wSpeed);//uwSpeed);

   //FB03 - Field bus pretorq
   ConvertWordToASCII(&gstDrive.stTxBuffer.aucData[14], gstDrive.wPretorquePercentage);

   //FB04 - Field bus target pos
   ConvertWordToASCII(&gstDrive.stTxBuffer.aucData[18], 0);

   gstDrive.stTxBuffer.aucData[ 22 ] = KEB_ETX_CHAR;

   uint8_t ucBCC = gstDrive.stTxBuffer.aucData[4];
   for(uint8_t j = 5; j < 23; j++)
   {
      ucBCC ^= gstDrive.stTxBuffer.aucData[ j ];
   }
   if(ucBCC < KEB_MIN_CHAR)
   {
      ucBCC += 0x20;
   }
   gstDrive.stTxBuffer.aucData[ 23 ] = ucBCC;

   //todo rm
   if(gstDrive.ucBadPacketCountdown)
   {
      gstDrive.stTxBuffer.aucData[ 23 ]++;
      gstDrive.ucBadPacketCountdown--;
   }

   gstDrive.stTxBuffer.uiIndex = 24;

   ucBytesToSend = 24;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadTxBuffer( void )
{
   static Drive_Msg_Type eMessageType = DRIVE_MSG__RUNTIME;//Variable controlling which message to transmit
   static uint8_t ucMotionCountdown; // continue sending runtime message after run
   if( GetMotion_RunFlag() )
   {
      eMessageType = DRIVE_MSG__RUNTIME;
      ucMotionCountdown = 200;
   }
   else if( ucMotionCountdown )
   {
      eMessageType = DRIVE_MSG__RUNTIME;
      ucMotionCountdown--;
   }
   else if( GetOperation_ClassOfOp() != CLASSOP__MANUAL )
   {
      eMessageType = DRIVE_MSG__RUNTIME;
   }

   //-------------------------------------
   if(Param_ReadValue_1Bit(enPARAM1__EnableUIDriveEdit))
   {
       if( eMessageType == DRIVE_MSG__READ_PARAM )
       {
          LoadParamReadRequest();
       }
       else if( eMessageType == DRIVE_MSG__WRITE_PARAM )
       {
          LoadParamWriteRequest();
       }
       else //if( eMessageType == DRIVE_MSG__RUNTIME )
       {
          LoadRuntimeMessage();
          eMessageType = DRIVE_MSG__RUNTIME;
       }

       en_drive_commands eNewCommand = GetDriveParameterCommand();
       /* Check for change in drive parameter command */
       if( eNewCommand == DRIVE_CMD__READ )
       {
          eMessageType = DRIVE_MSG__READ_PARAM;
       }
       else if( eNewCommand == DRIVE_CMD__WRITE )
       {
          eMessageType = DRIVE_MSG__WRITE_PARAM;
       }
       else
       {
          DriveParam_ClrDriveEditConfirm();
          eMessageType = DRIVE_MSG__RUNTIME;
       }
   }
   else
   {
       LoadRuntimeMessage();
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadRxBuffer( void )
{
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadParameterReadData()
{
   uint32_t uiParameterValue = gstDrive.stRxBuffer.aucData[6]
                             | gstDrive.stRxBuffer.aucData[5] << 8
                             | gstDrive.stRxBuffer.aucData[4] << 16
                             | gstDrive.stRxBuffer.aucData[3] << 24;

   if(Param_ReadValue_1Bit(enPARAM1__EnableUIDriveEdit))
   {
      DriveParam_UnloadDriveResponse( GetDriveParameterID(), uiParameterValue );
   }
}
/*-----------------------------------------------------------------------------
   Takes the inverter status value and extracts valid fault

   See Inverter Status DG02 P.235 in KEB manual
 -----------------------------------------------------------------------------*/
static void CheckFor_ReportedDriveFault( uint16_t uwInverterStatus, uint16_t uwOutputs )
{
   enum en_drive_error_keb eErrorKEB = DRIVE_ERROR_KEB__NONE;
   uwInverterStatus &= 0xFF; // Valid status values up to 255
   if(uwInverterStatus)
   {
      if( ( uwInverterStatus >= 1 )
       && ( uwInverterStatus <= 6 ) )
      {
         eErrorKEB = uwInverterStatus;
      }
      else if( ( uwInverterStatus >= 8 )
            && ( uwInverterStatus <= 9 ) )
      {
         eErrorKEB = uwInverterStatus;
      }
      else if( ( uwInverterStatus >= 8 )
            && ( uwInverterStatus <= 9 ) )
      {
         eErrorKEB = uwInverterStatus;
      }
      else if( ( uwInverterStatus >= 12 )
            && ( uwInverterStatus <= 16 ) )
      {
         eErrorKEB = uwInverterStatus;
      }
      else if( ( uwInverterStatus >= 18 )
            && ( uwInverterStatus <= 19 ) )
      {
         eErrorKEB = uwInverterStatus;
      }
      else if( ( uwInverterStatus >= 23 )
            && ( uwInverterStatus <= 35 ) )
      {
         eErrorKEB = uwInverterStatus;
      }
      else if( ( uwInverterStatus >= 39 )
            && ( uwInverterStatus <= 60 ) )
      {
         eErrorKEB = uwInverterStatus;
      }
      else if( uwInverterStatus == 93 )
      {
         eErrorKEB = uwInverterStatus;
      }
      //TODO 79 is quick stop
      else if( ( uwInverterStatus >= 150 )
            && ( uwInverterStatus <= 154 ) )
      {
         eErrorKEB = uwInverterStatus;
      }
      else if( ( uwInverterStatus >= 156 )
            && ( uwInverterStatus <= 166 ) )
      {
         eErrorKEB = uwInverterStatus;
      }

      //Optional
   #if 1
      else if( uwInverterStatus == 200 )//no card COM
      {
         eErrorKEB = uwInverterStatus;
      }
      else if( uwInverterStatus == 202 )//undefined encoder
      {
         eErrorKEB = uwInverterStatus;
      }
      else if( uwInverterStatus == 206 )//no encoder com
      {
         eErrorKEB = uwInverterStatus;
      }
      else if( uwInverterStatus == 208 )//PPR doesnt match LE01 encoder interface
      {
         eErrorKEB = uwInverterStatus;
      }
      else if( uwInverterStatus == 216 )//Internal encoder error
      {
         eErrorKEB = uwInverterStatus;
      }
      else if( uwInverterStatus == 222 )//undef encod error
      {
         eErrorKEB = uwInverterStatus;
      }
   #endif
   }

   if( eErrorKEB == DRIVE_ERROR_KEB__NONE )
   {
      /* If drive ready output is off, set unknow drive fault */
      if(!( uwOutputs & 0x04 ))
      {
         eErrorKEB = DRIVE_ERROR_KEB__UNK;
      }
   }

   gstDrive.eFault_KEB = eErrorKEB;
}
/*-----------------------------------------------------------------------------
Input Status:
   B0 = I7 (Drive Enable)
   B1 = I8
   B2 = I5 (Up Direction)
   B3 = I6 (Down Direction)
   B4 = I1
   B5 = I2
   B6 = I3
   B7 = I4
Output Status:
   B0 = O1
   B1 = O2
   B2 = RLY1 (Drive Ready)
   B3 = RLY2 (Drive On)
 -----------------------------------------------------------------------------*/
static void UnloadRuntimeData()
{
   uint32_t ulPDO1 = gstDrive.stRxBuffer.aucData[4] | gstDrive.stRxBuffer.aucData[3] << 8;
   uint32_t ulPDO2 = gstDrive.stRxBuffer.aucData[6] | gstDrive.stRxBuffer.aucData[5] << 8;
   uint32_t ulPDO3 = gstDrive.stRxBuffer.aucData[8] | gstDrive.stRxBuffer.aucData[7] << 8;
   uint32_t ulPDO4 = gstDrive.stRxBuffer.aucData[10] | gstDrive.stRxBuffer.aucData[9] << 8;
//      uint8_t ucFault = gstDrive.stRxBuffer.aucData[11] - 0x30;
//      uint8_t ucETC = gstDrive.stRxBuffer.aucData[12];

   uint16_t uwInverterStatus = ulPDO1;
   int16_t wDriveSpeed = ulPDO2;
   uint16_t uwInputs = ulPDO3;
   uint16_t uwOutputs = ulPDO4;

   if( uwOutputs & 0x08 ) // DRIVE ON
   {
      gstDrive.bSpeedRegRelease = 1;
   }
   else
   {
      gstDrive.bSpeedRegRelease = 0;
   }

   gstDrive.wSpeedFeedback = wDriveSpeed;

   CheckFor_ReportedDriveFault( uwInverterStatus, uwOutputs );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadNewData()
{
   if( ucServiceChar == KEB_SRV_CHAR__RUNTIME )
   {
      UnloadRuntimeData();
   }
   else if( ucServiceChar == KEB_SRV_CHAR__READ )
   {
      UnloadParameterReadData();
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Receive( void )
{
   static uint8_t ucHalfWord;
   uint8_t ucByteReceived;
   for(uint8_t i = 0; i < DRIVE_UART_RX_BYTE_LIMIT; i++)
   {
      if(UART_ReadByte(&ucByteReceived))
      {
         if ( gstDrive.stRxBuffer.uiIndex >= sizeof( gstDrive.stRxBuffer.aucData ) )
         {
            gstDrive.stRxBuffer.uiIndex = 0;
            gstDrive.stRxBuffer.ucState = 0;
            uwRxErrorCounter++;
         }
         else
         {
            ucByteReceived &= 0x7F; /*  filter out parity bit (0x7f = 0111 1111) */

            if ( gstDrive.stRxBuffer.ucState == 0 )  // awaiting STX
            {
               if ( ucByteReceived == KEB_STX_CHAR )
               {
                  gstDrive.stRxBuffer.aucData[ gstDrive.stRxBuffer.uiIndex ] = ucByteReceived;

                  ++gstDrive.stRxBuffer.ucState;
               }
               else
               {
                  gstDrive.stRxBuffer.uiIndex = 0;
               }
            }
            else if ( gstDrive.stRxBuffer.ucState == 1 )  // awaiting service byte
            {
               if ( ucByteReceived == KEB_SRV_CHAR__RUNTIME )
               {
                  ucServiceChar = ucByteReceived;
                  gstDrive.stRxBuffer.ucBytesLeft = 16;

                  if(++gstDrive.stRxBuffer.uiIndex >= DRIVE_BUFFER_SIZE)
                  {
                     gstDrive.stRxBuffer.uiIndex = 0;
                     uwRxErrorCounter++;
                  }
                  gstDrive.stRxBuffer.aucData[ gstDrive.stRxBuffer.uiIndex ] = ucByteReceived;

                  gstDrive.stRxBuffer.ucChecksum = ucByteReceived;

                  ++gstDrive.stRxBuffer.ucState;
               }
               else if ( ucByteReceived == KEB_SRV_CHAR__READ )
               {
                  ucServiceChar = ucByteReceived;

                  gstDrive.stRxBuffer.ucBytesLeft = 8;

                  if(++gstDrive.stRxBuffer.uiIndex >= DRIVE_BUFFER_SIZE)
                  {
                     gstDrive.stRxBuffer.uiIndex = 0;
                     uwRxErrorCounter++;
                  }
                  gstDrive.stRxBuffer.aucData[ gstDrive.stRxBuffer.uiIndex ] = ucByteReceived;

                  gstDrive.stRxBuffer.ucChecksum = ucByteReceived;

                  ++gstDrive.stRxBuffer.ucState;
               }
               else
               {
                  ucServiceChar = 0;
                  gstDrive.stRxBuffer.uiIndex = 0;
                  gstDrive.stRxBuffer.ucState = 0;
                  //uwRxErrorCounter++;
               }
            }
            else if ( gstDrive.stRxBuffer.ucState == 2 )  // awaiting invoke ID
            {
               if ( ucByteReceived >= KEB_ZERO_CHAR
                 && ucByteReceived <= KEB_IID_CHAR )
               {
                  if(++gstDrive.stRxBuffer.uiIndex >= DRIVE_BUFFER_SIZE)
                  {
                     gstDrive.stRxBuffer.uiIndex = 0;
                     uwRxErrorCounter++;
                  }
                  gstDrive.stRxBuffer.aucData[ gstDrive.stRxBuffer.uiIndex ] = ucByteReceived;

                  gstDrive.stRxBuffer.ucChecksum ^= ucByteReceived;

                  ++gstDrive.stRxBuffer.ucState;
               }
               else
               {
                  gstDrive.stRxBuffer.uiIndex = 0;
                  gstDrive.stRxBuffer.ucState = 0;
                  uwRxErrorCounter++;
               }
            }
            else if ( gstDrive.stRxBuffer.ucState == 3 )  // awaiting PDI //PDO data
            {
               uint8_t ucNibble = CharToNibble( ucByteReceived );

               if ( ucNibble <= 0xF )
               {
                  gstDrive.stRxBuffer.ucChecksum ^= ucByteReceived;

                  if ( gstDrive.stRxBuffer.ucBytesLeft % 2 ) /* save to buffer every other time (every two nibbles) */
                  {
                     ucHalfWord <<= 4;

                     ucHalfWord |= ucNibble;

                     if(++gstDrive.stRxBuffer.uiIndex >= DRIVE_BUFFER_SIZE)
                     {
                        gstDrive.stRxBuffer.uiIndex = 0;
                        uwRxErrorCounter++;
                     }
                     gstDrive.stRxBuffer.aucData[ gstDrive.stRxBuffer.uiIndex ] = ucHalfWord;
                  }
                  else
                  {
                     ucHalfWord = ucNibble;
                  }

                  if ( --gstDrive.stRxBuffer.ucBytesLeft == 0 )
                  {
                     /* Skip state 4 if reading parameters */
                     if( ucServiceChar == KEB_SRV_CHAR__READ )
                     {
                        gstDrive.stRxBuffer.ucState = 5;
                     }
                     else
                     {
                        ++gstDrive.stRxBuffer.ucState;
                     }
                  }
               }
               else
               {
                  gstDrive.stRxBuffer.uiIndex = 0;
                  gstDrive.stRxBuffer.ucState = 0;
                  uwRxErrorCounter++;
               }
            }
            else if ( gstDrive.stRxBuffer.ucState == 4 )  // awaiting result code
            {

               if ( ucByteReceived == KEB_ZERO_CHAR )
               {
                  if(++gstDrive.stRxBuffer.uiIndex >= DRIVE_BUFFER_SIZE)
                  {
                     gstDrive.stRxBuffer.uiIndex = 0;
                     uwRxErrorCounter++;
                  }
                  gstDrive.stRxBuffer.aucData[ gstDrive.stRxBuffer.uiIndex ] = ucByteReceived;

                  gstDrive.stRxBuffer.ucChecksum ^= ucByteReceived;

                  ++gstDrive.stRxBuffer.ucState;
               }
               else
               {
                  gstDrive.stRxBuffer.uiIndex = 0;
                  gstDrive.stRxBuffer.ucState = 0;
                  uwRxErrorCounter++;
               }
            }
            else if ( gstDrive.stRxBuffer.ucState == 5 )  // awaiting ETX
            {
               if ( ucByteReceived == KEB_ETX_CHAR )
               {
                  if(++gstDrive.stRxBuffer.uiIndex >= DRIVE_BUFFER_SIZE)
                  {
                     gstDrive.stRxBuffer.uiIndex = 0;
                     uwRxErrorCounter++;
                  }
                  gstDrive.stRxBuffer.aucData[ gstDrive.stRxBuffer.uiIndex ] = ucByteReceived;

                  gstDrive.stRxBuffer.ucChecksum ^= ucByteReceived;

                  ++gstDrive.stRxBuffer.ucState;
               }
               else
               {
                  gstDrive.stRxBuffer.uiIndex = 0;
                  gstDrive.stRxBuffer.ucState = 0;
                  uwRxErrorCounter++;
               }
            }
            else // awaiting checksum
            {
               if ( gstDrive.stRxBuffer.ucChecksum < KEB_MIN_CHAR )
               {
                  gstDrive.stRxBuffer.ucChecksum += KEB_MIN_CHAR;
               }

               if ( gstDrive.stRxBuffer.ucChecksum != ucByteReceived )  // bad checksum?
               {
                  uwRxErrorCounter++;
               }
               else  // good checksum
               {
                  UnloadNewData();

                  gstDrive.uiOfflineTime_5ms = 0;

                  if(++gstDrive.stRxBuffer.uiIndex >= DRIVE_BUFFER_SIZE)
                  {
                     gstDrive.stRxBuffer.uiIndex = 0;
                  }
                  gstDrive.stRxBuffer.aucData[ gstDrive.stRxBuffer.uiIndex ] = ucByteReceived;
               }

               gstDrive.stRxBuffer.uiIndex = 0;
               gstDrive.stRxBuffer.ucState = 0;
            }
         }
      }
      else
      {
         i = DRIVE_UART_RX_BYTE_LIMIT;
         break;
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Transmit( void )
{
   //------------------------
   // Transmit the data to the drive.
   uint8_t ucBytesWritten = UART_SendRB(&gstDrive.stTxBuffer.aucData[0], ucBytesToSend);
   if(ucBytesWritten != ucBytesToSend)
   {
      //SetAlarm
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void LoadInterface_KEB( struct st_drive * pstDrive )
{
   pstDrive->eType = enDriveType__KEB;

   pstDrive->stInterface.pfnLoadTxBuffer = LoadTxBuffer;
   pstDrive->stInterface.pfnUnloadRxBuffer = UnloadRxBuffer;

   pstDrive->stInterface.pfnTransmit = Transmit;
   pstDrive->stInterface.pfnReceive = Receive;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

