/******************************************************************************
 *
 * @file     mod_drive.c
 * @brief
 * @version  V1.00
 * @date     23, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "sru_a.h"
#include <stdint.h>
#include <string.h>
#include <sys_drive.h>
#include "sys.h"
#include "motion.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
static en_drive_commands eConfirmCmd;
static uint16_t uwConfirmIndex;
static uint32_t uiConfirmValue;
/*----------------------------------------------------------------------------
   Unload drive response for updating feedback to R/W requesting node
 *----------------------------------------------------------------------------*/
void DriveParam_UnloadDriveResponse( uint16_t uwID, uint32_t uiValue )
{
   static uint32_t uiPrevValue;
   /* Make sure the value, the ID and command check out before updating the confirmation channel */
   if( ( uiValue == uiPrevValue )
    && ( uwID == GetDriveParameterID() ) )
   {
      eConfirmCmd = GetDriveParameterCommand();
      uwConfirmIndex = uwID;
      uiConfirmValue = uiValue;
   }
   uiPrevValue = uiValue;
}
void DriveParam_ClrDriveEditConfirm()
{
   eConfirmCmd = DRIVE_CMD__NONE;
   uiConfirmValue = 0;
   uwConfirmIndex = 0;
}

en_drive_commands DriveParam_GetConfirmCmd()
{
   return eConfirmCmd;
}
uint16_t DriveParam_GetConfirmID()
{
   return uwConfirmIndex;
}
uint32_t DriveParam_GetConfirmValue()
{
   return uiConfirmValue;
}


/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
