/******************************************************************************
 *
 * @file     drive_hpv900_s2.c
 * @brief    
 * @version  V1.00
 * @date     1, April 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "drive.h"
#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "motion.h"
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define CYCLES_TO_SUPRESS_SPEED_FEEDBACK        (20)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint16_t uwRxErrorCounter;//Counts number of bad packets received

/* For a certain number of cycles after sending over pretorque,
   the speed feedback field sent by the drive will contain the
   pretorque value instead of the speed so supress unload of this field.
*/
static uint16_t uwSuppressSpeedFeedbackCountdown;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Returns the number of bad bytes/packets detected
-----------------------------------------------------------------------------*/
uint16_t GetDriveRxErrorCount_DSD(void)
{
   return uwRxErrorCounter;
}
/*-----------------------------------------------------------------------------
   Returns speed command in DSD412 format (as a percentage of stored contract speed)
 -----------------------------------------------------------------------------*/
static int16_t ConvertSpeedCommandToDSD412Format( int16_t wSpeedCommand )
{
   float fSpeedPercentage = 0;
   if( wSpeedCommand )
   {
      fSpeedPercentage = wSpeedCommand;
      if( wSpeedCommand < 0 )
      {
         fSpeedPercentage *= 32768;// DSD412 value for 100% of contract speed

         fSpeedPercentage /= Param_ReadValue_16Bit( enPARAM16__ContractSpeed );
         fSpeedPercentage -= 0.5f;
      }
      else
      {
         fSpeedPercentage *= 32767;// DSD412 value for 100% of contract speed

         fSpeedPercentage /= Param_ReadValue_16Bit( enPARAM16__ContractSpeed );
         fSpeedPercentage += 0.5f;
      }
   }

   return (int16_t) fSpeedPercentage;
}
/*-----------------------------------------------------------------------------
   Takes speed in DSD412 format and converts it to fpm
 -----------------------------------------------------------------------------*/
static int16_t ConvertDSD412FormatToSpeedCommand( int16_t wDriveSpeed )
{
   float fSpeedFeedback = 0;
   if( wDriveSpeed )
   {
      if( wDriveSpeed < 0 )
      {
         fSpeedFeedback = wDriveSpeed * Param_ReadValue_16Bit( enPARAM16__ContractSpeed );
         fSpeedFeedback /= 32768;
         fSpeedFeedback -= 0.5f;
      }
      else
      {
         fSpeedFeedback = wDriveSpeed * Param_ReadValue_16Bit( enPARAM16__ContractSpeed );
         fSpeedFeedback /= 32767;
         fSpeedFeedback += 0.5f;
      }
   }

   return (int16_t) fSpeedFeedback;
}
/*-----------------------------------------------------------------------------
   Returns speed command in DSD412 format (as a percentage of stored contract speed)
 -----------------------------------------------------------------------------*/
static int16_t ConvertPretorqueCommandToDSD412Format( int16_t wPretorque )
{
   float fPercentage = 0;
   if( wPretorque )
   {
      fPercentage = wPretorque;
      if( wPretorque < 0 )
      {
         fPercentage *= 32768;// DSD412 value for 100% of max pretorque

         fPercentage /= 100.0f;
         fPercentage -= 0.5f;
      }
      else
      {
         fPercentage *= 32767;// DSD412 value for 100% of max pretorque

         fPercentage /= 100.0f;
         fPercentage += 0.5f;
      }
   }

   return (int16_t) fPercentage;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadTxBuffer( void )
{
   static uint8_t ucPretorqueDelayCounter_10ms = 0;
   static uint8_t ucPretorqueHoldCounter_10ms = 0;
   int16_t wSpeedCommand = GetMotion_SpeedCommand();
   int16_t wDriveSpeedCommand = ConvertSpeedCommandToDSD412Format( wSpeedCommand );
   int16_t wDrivePretorqueCommand = ConvertPretorqueCommandToDSD412Format( gstDrive.wPretorquePercentage );

   /* DSD protocol is empty. Requires pretorque latch flag to be asserted after an
    * undefined delay after receiving the full field flag, a direction, and the
    * hardware drive enable output. The pretorque latch flag must be held for
    * a short period while asserting the desired pretorque value. The drive
    * then requires the pretorque value to be held for another undefined short
    * period, while the pretorque latch flag is deasserted.
    *
    * CAUTION: Also not included in the DSD documentation, but the run command takes precedent over the
    * pretorque latch command. Meaning if the run command is given while attempting to assert pretorque,
    * the pretorque command will be interpreted as a speed. Seems unsafe to not include in the document.*/

   /* Assumes enPARAM8__ResendDriveTimer_5ms of 2. */
   uint8_t ucPretorqueDelayLimit_10ms = 5; // Tested in warehouse at 2 and doubled to account for in field packet loss
   uint8_t ucPretorqueHoldLimit_10ms = 10; // Tested in warehouse at 3 and doubled to account for in field packet loss

   //------------------------
   // Byte 1 - Sync Byte 1 (0xFA)
   gstDrive.stTxBuffer.aucData[ 0 ] = 0xFA;

   //------------------------
   // Byte 2 - Sync Byte 2 (0x05)
   gstDrive.stTxBuffer.aucData[ 1 ] = 0x05;

   //------------------------
   // Byte 3 - Logic Byte 1
   //    Bit0 = Sync bit (1)
   //    Bit1 = Sync bit (1)
   //    Bit2 = Run Command
   //    Bit3 = Clear Fault
   //    Bit4 = Direction DOWN
   //    Bit5 = Direction UP
   //    Bit6 = Field Command
   //    Bit7 = Pre-Torque Command (on rising edge)
   uint8_t uc = 0x03;
   if ( !gstFault.bActiveFault )
   {
      if( GetMotion_RunFlag() )
      {
         /* Full field enable bit for quicker speed reg feedback and faster take off time. */
         if( Param_ReadValue_1Bit(enPARAM1__EnableDSDFullField) )
         {
            uc |= 0x40;
         }

         if ( GetMotion_Direction() == -1 )
         {
            uc |= 0x10;  // direction DN
         }
         else if ( GetMotion_Direction() == 1 )
         {
            uc |= 0x20;  // direction UP
         }

         if ( GetPickDriveFlag() )
         {
            uc |= 0x04;  // run
         }

         if( GetMotion_RunFlag()
        && ( Motion_GetStartState() == SEQUENCE_START__DSD_PRETORQUE ) )
         {

            if(ucPretorqueDelayCounter_10ms >= ucPretorqueDelayLimit_10ms)
            {
               if(ucPretorqueHoldCounter_10ms < ucPretorqueHoldLimit_10ms)
               {
                  uc |= 0x80;
                  ucPretorqueHoldCounter_10ms++;
               }
            }
            else
            {
               ucPretorqueDelayCounter_10ms++;
            }
         }
         else
         {
            ucPretorqueDelayCounter_10ms = 0;
            ucPretorqueHoldCounter_10ms = 0;
         }
      }
      else
      {
         ucPretorqueDelayCounter_10ms = 0;
         ucPretorqueHoldCounter_10ms = 0;
      }
   }

   if(gstDrive.bTriggerDriveReset)
   {
      uc |= 0x08;  // fault reset
   }

   gstDrive.stTxBuffer.aucData[ 2 ] = uc;

   //------------------------
   // Byte 4 - Logic Byte 2
   //    Bit0 = Velocity Loop Gain Reduction
   //    Bit1 = Not Used
   //    Bit2 = Not Used
   //    Bit3 = Not Used
   //    Bit4 = Not Used
   //    Bit5 = Not Used
   //    Bit6 = Not Used
   //    Bit7 = Not Used
   gstDrive.stTxBuffer.aucData[ 3 ] = 0;

   /* According to magnetek, the run command overrides the pretorque command.
    * If sending the pretorque value with the run command set, even if the
    * pretorque bit is set, the value will be interpreted as a speed command. */
   /* At start of run before ramp up, sent pretorque instead speed */
   if( ( uc & 0x80 )
   && !( uc & 0x04 ) )
   {
      uwSuppressSpeedFeedbackCountdown = CYCLES_TO_SUPRESS_SPEED_FEEDBACK;
      //------------------------
      // Byte 5 - Torque Hi Byte
      gstDrive.stTxBuffer.aucData[ 4 ] = (uint8_t) (wDrivePretorqueCommand >> 8);

      //------------------------
      // Byte 6 - Torque Lo Byte
      gstDrive.stTxBuffer.aucData[ 5 ] = (uint8_t) (wDrivePretorqueCommand & 0xff);
   }
   else if( ( Motion_GetStartState() == SEQUENCE_START__DSD_PRETORQUE )
         && ( ucPretorqueDelayCounter_10ms >= ucPretorqueDelayLimit_10ms )
         && !( uc & 0x04 ))
   {
      uwSuppressSpeedFeedbackCountdown = CYCLES_TO_SUPRESS_SPEED_FEEDBACK;
      //------------------------
      // Byte 5 - Torque Hi Byte
      gstDrive.stTxBuffer.aucData[ 4 ] = (uint8_t) (wDrivePretorqueCommand >> 8);

      //------------------------
      // Byte 6 - Torque Lo Byte
      gstDrive.stTxBuffer.aucData[ 5 ] = (uint8_t) (wDrivePretorqueCommand & 0xff);
   }
   else
   {
      //------------------------
      // Byte 5 - Speed Hi Byte
      gstDrive.stTxBuffer.aucData[ 4 ] = (uint8_t) (wDriveSpeedCommand >> 8);

      //------------------------
      // Byte 6 - Speed Lo Byte
      gstDrive.stTxBuffer.aucData[ 5 ] = (uint8_t) (wDriveSpeedCommand & 0xff);
   }

   //------------------------
   // Byte 7 - PCDU Command (Not Applicable)
   gstDrive.stTxBuffer.aucData[ 6 ] = 0;

   gstDrive.stTxBuffer.uiIndex = 7;  // 7 bytes placed in buffer
   // Possibly missing The checksum modulo 256 sum of bytes 1-7
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadRxBuffer( void )
{
}
/*-----------------------------------------------------------------------------

Bytes 1&2: These bytes will always be set to FAh and 05h respectively. They are used for
synchronizing the host to the start of an incoming Demand Velocity Response message from the DSD
412.
Byte 3: {N259_C010} This byte contains 8 logic status signals as defined below:
   B0: =1 to act as a synchronization bit for the host
   B1: =1 to act as a synchronization bit for the host
   B2: 0=Tach direction is not UP 1=Tach direction is UP
   B3: 0=Tach direction is not DOWN 1=Tach direction is DOWN
   B4: 0=No tach overspeed fault 1=Tach overspeed fault F97
   B5: 0=No tach loss fault 1=Tach loss fault F98
   B6: 0=No reverse tach fault 1=Reverse tach fault F99
   B7: 0=No Serial Communications Fault 1=Serial Communications Fault
Byte 4: {N259_C011} This byte contains 8 logic signals to indicate drive faults.
   B0: 0=No Motor over-load 1=Motor over-load fault F400
   B1: 0=No excessive field current 1=Excessive field current F401
   B2: 0=No contactor failure 1=Contactor failure F402
   B3: 0=Drive is not at CEMF limit 1=Drive is at CEMF limit F407 or F408
   B4: 0=E-STOP circuit is closed 1=E-STOP circuit is open
   B5: 0=No E-STOP fault 1=E-STOP fault F405
   B6: 1=No drive fault exists, READY 0=A drive fault exists, NOT READY
   B7: 1=Drive NOT READY 0=Drive is READY
Byte 5: {N259_C012} This byte contains 8 logic signals to indicate additional faults:
   B0: 0=No Loop fault exists 1=Loop fault exists F900
   B1: 0=No PCU IST fault 1=PCU IST fault F901
   B2: 0=No line synchronization failure 1=Line synchronization failure F903
   B3: 0=No low line fault 1=Low line fault F904
   B4: 0=No field loss fault 1=Field loss fault F905
   B5: 0=No Line Droop 1=Line Droop below 90% of nominal F406
   B6: 0=Speed Regulator not Released 1=Speed Regulator is Released
   B7: Spare
Bytes 6&7: These two bytes contain an echo of the most recent Numeric Command target value
from the host. The format of the value is identical to that previously defined in the Demand Velocity
message.
 -----------------------------------------------------------------------------*/
static void UnloadNewData()
{
   if( !( gstDrive.stRxBuffer.aucData[ 3 ] & 0x40 ) )// If drive fault exists
   {
      if( gstDrive.stRxBuffer.aucData[ 2 ] & 0x10 )
      {
         gstDrive.eFault_DSD = DRIVE_ERROR_DSD__TACH_OVERSPEED_F97;
      }
      else if( gstDrive.stRxBuffer.aucData[ 2 ] & 0x20 )
      {
         gstDrive.eFault_DSD = DRIVE_ERROR_DSD__TACH_LOSS_F98;
      }
      else if( gstDrive.stRxBuffer.aucData[ 2 ] & 0x40 )
      {
         gstDrive.eFault_DSD = DRIVE_ERROR_DSD__TACH_REV_F99;
      }
      else if( gstDrive.stRxBuffer.aucData[ 2 ] & 0x80 )
      {
         gstDrive.eFault_DSD = DRIVE_ERROR_DSD__SERIAL_COM;
      }
      else if( gstDrive.stRxBuffer.aucData[ 3 ] & 0x01 )
      {
         gstDrive.eFault_DSD = DRIVE_ERROR_DSD__OVERLOAD_F400;
      }
      else if( gstDrive.stRxBuffer.aucData[ 3 ] & 0x02 )
      {
         gstDrive.eFault_DSD = DRIVE_ERROR_DSD__FIELD_CURR_F401;
      }
      else if( gstDrive.stRxBuffer.aucData[ 3 ] & 0x04 )
      {
         gstDrive.eFault_DSD = DRIVE_ERROR_DSD__CONTACTOR_F402;
      }
      else if( gstDrive.stRxBuffer.aucData[ 3 ] & 0x08 )
      {
         gstDrive.eFault_DSD = DRIVE_ERROR_DSD__CEMF_F407_8;
      }
      else if( gstDrive.stRxBuffer.aucData[ 3 ] & 0x20 )
      {
         gstDrive.eFault_DSD = DRIVE_ERROR_DSD__ESTOP_F405;
      }
      else if( gstDrive.stRxBuffer.aucData[ 4 ] & 0x01 )
      {
         gstDrive.eFault_DSD = DRIVE_ERROR_DSD__LOOP_F900;
      }
      else if( gstDrive.stRxBuffer.aucData[ 4 ] & 0x02 )
      {
         gstDrive.eFault_DSD = DRIVE_ERROR_DSD__PCU_IST_F901;
      }
      else if( gstDrive.stRxBuffer.aucData[ 4 ] & 0x04 )
      {
         gstDrive.eFault_DSD = DRIVE_ERROR_DSD__LINE_SYNC_F903;
      }
      else if( gstDrive.stRxBuffer.aucData[ 4 ] & 0x08 )
      {
         gstDrive.eFault_DSD = DRIVE_ERROR_DSD__LINE_LOW_F904;
      }
      else if( gstDrive.stRxBuffer.aucData[ 4 ] & 0x01 )
      {
         gstDrive.eFault_DSD = DRIVE_ERROR_DSD__FIELD_LOSS_F905;
      }
      else if( gstDrive.stRxBuffer.aucData[ 4 ] & 0x02 )
      {
         gstDrive.eFault_DSD = DRIVE_ERROR_DSD__LINE_DROOP_F406;
      }
      else
      {
         gstDrive.eFault_DSD = DRIVE_ERROR_DSD__NOT_READY;
      }
   }
   else if(!( gstDrive.stRxBuffer.aucData[ 3 ] & 0x80 ))// If drive not ready
   {
      /* KS Note, this logic is inverted compared to the documentation. Why?? */
      gstDrive.eFault_DSD = DRIVE_ERROR_DSD__NOT_READY;
   }
   else
   {
      gstDrive.eFault_DSD = DRIVE_ERROR_DSD__NONE;
   }

   /* From the DSD manual:
    *    Bytes 6&7: These two bytes contain an echo of the most recent Numeric Command target value
    *    from the host. The format of the value is identical to that previously defined in the Demand Velocity
    *    message. */
   /* When nonzero, the speed feedback field is expected to hold the pretorque signal instead of speed */
   if(uwSuppressSpeedFeedbackCountdown)
   {
      /* When nonzero, the speed feedback field is expected to hold the pretorque signal instead of speed */
      uwSuppressSpeedFeedbackCountdown--;
   }
   else
   {
      int16_t wDriveSpeed = ( gstDrive.stRxBuffer.aucData[ 5 ] << 8 ) | gstDrive.stRxBuffer.aucData[ 6 ];
      gstDrive.wSpeedFeedback = ConvertDSD412FormatToSpeedCommand( wDriveSpeed );
   }
   gstDrive.bSpeedRegRelease = (gstDrive.stRxBuffer.aucData[ 4 ] & 0x40) ? 1:0;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Receive( void )
{
   uint8_t ucByteReceived;
   uint8_t ucByteLimit = DRIVE_UART_RX_BYTE_LIMIT;
   while ( UART_ReadByte( &ucByteReceived ) && ucByteLimit)
   {
      ucByteLimit--;
      if ( gstDrive.stRxBuffer.uiIndex >= sizeof( gstDrive.stRxBuffer.aucData ) )
      {
         gstDrive.stRxBuffer.uiIndex = 0;
         gstDrive.stRxBuffer.ucState = 0;
         uwRxErrorCounter++;
      }
      else
      {
         gstDrive.stRxBuffer.aucData[ gstDrive.stRxBuffer.uiIndex++ ] = ucByteReceived;

         if ( gstDrive.stRxBuffer.ucState == 0 )  // awaiting 0xFA
         {
            if ( ucByteReceived == 0xFA )
            {
               gstDrive.stRxBuffer.ucChecksum = ucByteReceived;

               ++gstDrive.stRxBuffer.ucState;
            }
            else
            {
               gstDrive.stRxBuffer.uiIndex = 0;
               gstDrive.stRxBuffer.ucState = 0;
            }
         }
         else if ( gstDrive.stRxBuffer.ucState == 1 )  // awaiting message ID
         {
            if ( ucByteReceived == 0x05 )
            {
               gstDrive.stRxBuffer.ucChecksum += ucByteReceived;

               ++gstDrive.stRxBuffer.ucState;

               // 11 bytes in message. Subtract 2 already received and don't
               // count the checksum.
               gstDrive.stRxBuffer.ucBytesLeft = 11 - 3;
            }
            else
            {
               gstDrive.stRxBuffer.uiIndex = 0;
               gstDrive.stRxBuffer.ucState = 0;
               uwRxErrorCounter++;
            }
         }
         else // ucState == 2
         {
            if ( gstDrive.stRxBuffer.ucBytesLeft )
            {
               gstDrive.stRxBuffer.ucChecksum += ucByteReceived;

               --gstDrive.stRxBuffer.ucBytesLeft;
            }
            else
            {
               if ( gstDrive.stRxBuffer.ucChecksum != ucByteReceived )  // bad checksum?
               {
                  uwRxErrorCounter++;
               }
               else  // good checksum
               {
                  UnloadNewData();
                  gstDrive.uiOfflineTime_5ms = 0;
               }

               gstDrive.stRxBuffer.uiIndex = 0;
               gstDrive.stRxBuffer.ucState = 0;
            }
         }
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Transmit( void )
{
   //------------------------
   // Calculate the checksum on the data to send.
   uint8_t ucChecksum = 0;

   for ( uint8_t i = 0; i < gstDrive.stTxBuffer.uiIndex; ++i )
   {
      ucChecksum += gstDrive.stTxBuffer.aucData[ i ];
   }

   //todo rm
   if(gstDrive.ucBadPacketCountdown)
   {
      ucChecksum++;
      gstDrive.ucBadPacketCountdown--;
   }

   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = ucChecksum;

   //------------------------
   // Transmit the data to the drive.
   UART_SendRB( &gstDrive.stTxBuffer.aucData, gstDrive.stTxBuffer.uiIndex );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void LoadInterface_DSD( struct st_drive * pstDrive )
{
   pstDrive->eType = enDriveType__DSD;

   pstDrive->stInterface.pfnLoadTxBuffer = LoadTxBuffer;
   pstDrive->stInterface.pfnUnloadRxBuffer = UnloadRxBuffer;

   pstDrive->stInterface.pfnTransmit = Transmit;
   pstDrive->stInterface.pfnReceive = Receive;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
