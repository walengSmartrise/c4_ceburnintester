/******************************************************************************
 *
 * @file     mod_drive.c
 * @brief
 * @version  V1.00
 * @date     23, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "drive.h"
#include "sru_a.h"
#include <stdint.h>
#include <string.h>
#include "sys.h"
#include "motion.h"
#include "acceptance.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Drive =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

struct st_drive gstDrive;

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/* This is the duration mod_drive will wait in the discovery state before it
 * switches to look for another drive */
#define DRIVE_DISCOVERY_TIMEOUT     200 /* 20 x 5ms = 1s */

/* This is the duration mod_drive will allow a comm loss before it latches a
 * drive comm fault */
#define DRIVE_COMM_TIMEOUT          200 /* 20 x 5ms = 1s */

#define DRIVE_RESET_LIMIT               (10)
/*----------------------------------------------------------------------------
 *
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static void ( *gapfnLoadInterface[] )( struct st_drive * pstDrive ) =
{
   &LoadInterface_HPV,
   &LoadInterface_KEB,
   &LoadInterface_DSD,
};

static uint8_t bResetLimitExceeded = 0;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Returns 1 if the argument drive type matches the configured drive
 -----------------------------------------------------------------------------*/
uint8_t CheckDriveType( enum en_drive_type eType )
{
   return ( gstDrive.eType == eType );
}
/*-----------------------------------------------------------------------------
Returns the receive error count for drive messages
 -----------------------------------------------------------------------------*/
uint16_t Drive_GetRxErrorCount(void)
{
   uint16_t uwCount = 0;
   switch( gstDrive.eType )
   {
      case enDriveType__HPV:
         uwCount = GetDriveRxErrorCount_HPV();
         break;
      case enDriveType__KEB:
         uwCount = GetDriveRxErrorCount_KEB();
         break;
      case enDriveType__DSD:
         uwCount = GetDriveRxErrorCount_DSD();
         break;
   }
   return uwCount;
}
/*-----------------------------------------------------------------------------
   Flag to command zero speed to drive and pick M contactor for KEB PM motor learn
 -----------------------------------------------------------------------------*/
uint8_t Drive_GetMotorLearnFlag()
{
   return gstDrive.bLearnMotor;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckFor_MotorLearn()
{
   gstDrive.bLearnMotor = 0;
   if( SRU_Read_DIP_Switch(enSRU_DIP_A6) )
   {
      if( ( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
       && ( GetOperation_ManualMode() == MODE_M__CONSTRUCTION ) )
      {
         gstDrive.bLearnMotor = 1;
      }
      else if( !GetMotion_RunFlag() )
      {
         SetFault(FLT__INVALID_DIP_SW_A6);
      }
   }
}
/*-----------------------------------------------------------------------------
   TODO rm, on rising edge of MR DIP A2, sends adjustable number of sequential bad packets to the drive.
 -----------------------------------------------------------------------------*/
static void CheckFor_BadPacketUpdates()
{
#if 0
   static uint8_t bLastDIP;
   if( SRU_Read_DIP_Switch(enSRU_DIP_A2) && !bLastDIP )
   {
      gstDrive.ucBadPacketCountdown = Param_ReadValue_8Bit(enPARAM8__DEBUG_NumInvalidDrivePackets);
   }

   bLastDIP = SRU_Read_DIP_Switch(enSRU_DIP_A2);
#else
   gstDrive.ucBadPacketCountdown = 0;
#endif
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckFor_Pretorque()
{
   gstDrive.wPretorquePercentage = 0;
   if( GetMotion_RunFlag() )
   {
      uint8_t ucSelect = Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect);
      if( ucSelect == LWS__MR )
      {
         gstDrive.wPretorquePercentage = GetLW_AverageTorquePercent_MRB();
      }
      else if( ucSelect == LWS__CT )
      {
         gstDrive.wPretorquePercentage = GetLW_AverageTorquePercent_CTB();
      }
      else /* Discrete LW */
      {
         if( Param_ReadValue_1Bit(enPARAM1__EnablePretorqueTest) )
         {
            un_type_conv unConv;
            unConv.auc[0] = Param_ReadValue_8Bit(enPARAM8__LWD_TorqueOffset);
            gstDrive.wPretorquePercentage = unConv.ac[0];
         }
         else
         {
            gstDrive.wPretorquePercentage = 0;
         }
      }
   }

   /* Bound pretorque value [-100,100] */
   if( gstDrive.wPretorquePercentage > 100 )
   {
      gstDrive.wPretorquePercentage = 100;
   }
   else if( gstDrive.wPretorquePercentage < -100 )
   {
      gstDrive.wPretorquePercentage = -100;
   }
}

/*-----------------------------------------------------------------------------
   Checks if a run has been completed without fault
 -----------------------------------------------------------------------------*/
static uint8_t CheckFor_SuccessfulRun()
{
   static uint8_t bNewRun;
   uint8_t bRunSuccessful = 0;
   if(gstFault.bActiveFault)
   {
      bNewRun = 0;
   }
   else if(GetMotion_RunFlag() && !bNewRun)
   {
      bNewRun = 1;
   }
   else if(bNewRun && !GetMotion_RunFlag())
   {
      bNewRun = 0;
      bRunSuccessful = 1;
   }
   return bRunSuccessful;
}
/*-----------------------------------------------------------------------------
   Clear the reset drive counter after 2 successful runs
 -----------------------------------------------------------------------------*/
static void ClearResetDriveCounter()
{
   static uint8_t ucSuccessfulRunCounter = 0;
   if(CheckFor_SuccessfulRun()
   && gstDrive.ucResetCounter < DRIVE_RESET_LIMIT)
   {
      if(++ucSuccessfulRunCounter >= 2)
      {
         ucSuccessfulRunCounter = 0;
         gstDrive.ucResetCounter = 0;
      }
   }
}
/*-----------------------------------------------------------------------------
   Checks if a drive reset should be sent
   Drive must be reporting a fault  5 sec before a drive fault is triggered
   Increments drive reset count only if during run
   TODO add reset for DSD at startup
 -----------------------------------------------------------------------------*/
static void CheckIfDriveResetNeeded()
{
   static uint16_t uwCounter_5ms = 0;
   static uint8_t bLastTriggerDriveReset;
   if(!Param_ReadValue_1Bit(enPARAM1__DisableAutoDriveReset))
   {
      gstDrive.bTriggerDriveReset = 0;
      if(!bResetLimitExceeded)
      {
            if(gstDrive.eFault == DRIVE_FAULT__DRIVE_REPORTED_FAULT)
            {
               if(uwCounter_5ms >= 1200)//6s
               {
                  gstDrive.bTriggerDriveReset = 1;
               }
               else
               {
                  uwCounter_5ms++;
               }
            }
            else
            {
               uwCounter_5ms = 0;
            }
      }
      else
      {
         // Drive reset limit exceeded
         SetAlarm(ALM__DRIVE_RESET_LIMIT);
      }

      if( !bLastTriggerDriveReset
       && gstDrive.bTriggerDriveReset)
      {
         if(gstDrive.ucResetCounter >= DRIVE_RESET_LIMIT)
         {
            bResetLimitExceeded = 1;
         }
         else
         {
            gstDrive.ucResetCounter++;
         }
      }
      bLastTriggerDriveReset = gstDrive.bTriggerDriveReset;
      ClearResetDriveCounter();
   }
   /* If DSD, assert reset at startup to clear F118 */
   static uint16_t uwStartupCountdown_5ms = 400;//2 sec time to hold drive reset
   static uint16_t uwStartupDelay_5ms = 600;//3 sec time after startup to delay drive reset
   if(gstDrive.eType == enDriveType__DSD )
   {
      if(uwStartupDelay_5ms)//DSD reports "PROT" without this delay
      {
         uwStartupDelay_5ms--;
      }
      else if(uwStartupCountdown_5ms)
      {
         uwStartupCountdown_5ms--;
         gstDrive.bTriggerDriveReset = 1;
      }
   }

   if(gstDrive.bTriggerDriveReset)
   {
      SetAlarm(ALM__DRIVE_RESET);
   }
}
/*-----------------------------------------------------------------------------
   Drive access functions

 -----------------------------------------------------------------------------*/
uint8_t GetSpeedRegReleased()
{
   return gstDrive.bSpeedRegRelease;
}
uint8_t GetDriveFault()
{
   return gstDrive.eFault;
}
int16_t GetDriveSpeedFeedback()
{
   int16_t wFeedback = 0;
   wFeedback = gstDrive.wSpeedFeedback;

   /* Fix for missing speed feedback polarity on KEB */
   if( ( !Param_ReadValue_1Bit(enPARAM1__DisableInvertKEBSpeed) )
    && ( Param_ReadValue_8Bit(enPARAM8__DriveSelect) == enDriveType__KEB )
    && ( GetMotion_Direction() == DIR__DN ) )
   {
      if( wFeedback > 0 )
      {
         wFeedback *= -1;
      }
   }
   return wFeedback;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
int16_t GetMagnetekPretorqueCommand()
{
   int iPretorque;
   float flPercent;

   if ( gstDrive.wPretorquePercentage < 0 )
   {
      flPercent = gstDrive.wPretorquePercentage * -1;
      flPercent /= 100;

      iPretorque = (int) -8192 * flPercent;
   }
   else
   {
      flPercent = gstDrive.wPretorquePercentage;
      flPercent /= 100;

      iPretorque = (int) 8192 * flPercent;
   }

   return (int16_t) iPretorque;
}
/*-----------------------------------------------------------------------------

   Helper function used by drive_hpv and drive_dsd

 -----------------------------------------------------------------------------*/
int16_t GetMagnetekSpeedCommand( void )
{
   int iSpeed;
   float flPercent;

   uint16_t uwContract = Param_ReadValue_16Bit( enPARAM16__ContractSpeed );

   if ( GetMotion_SpeedCommand() < 0 )
   {
      flPercent = GetMotion_SpeedCommand() * -100;
      flPercent /= uwContract;

      iSpeed = (int) -8192 * flPercent;
      iSpeed /= 100;
   }
   else
   {
      flPercent = GetMotion_SpeedCommand() * 100;
      flPercent /= uwContract;

      iSpeed = (int) 8192 * flPercent;
      iSpeed /= 100;
   }

   return (int16_t) iSpeed;
}
/*-----------------------------------------------------------------------------

   Helper function to convert a magnetek command speed to FPM

 -----------------------------------------------------------------------------*/
int16_t ConvertMagnetekSpeedToFPM( int16_t wMagnetekSpeed )
{
   float fSpeed_Contract, fSpeed_FPM, fSpeed_Magnetek;

   fSpeed_Contract = Param_ReadValue_16Bit( enPARAM16__ContractSpeed );
   fSpeed_Magnetek = wMagnetekSpeed;
   if ( fSpeed_Magnetek < 0 )
   {
      fSpeed_FPM = fSpeed_Magnetek * -100;
      fSpeed_FPM *= fSpeed_Contract;
      fSpeed_FPM /= MIN_MAGNETEK_SPEED;
      fSpeed_FPM /= 100;
   }
   else
   {
      fSpeed_FPM = fSpeed_Magnetek * 100;
      fSpeed_FPM *= fSpeed_Contract;
      fSpeed_FPM /= MAX_MAGNETEK_SPEED;
      fSpeed_FPM /= 100;
   }

   return (int16_t) fSpeed_FPM;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ClearBuffers( void )
{
   memset( &gstDrive.stRxBuffer.aucData, 0, DRIVE_BUFFER_SIZE );
   memset( &gstDrive.stTxBuffer.aucData, 0, DRIVE_BUFFER_SIZE );

   gstDrive.stTxBuffer.ucState = 0;
   gstDrive.stTxBuffer.uiIndex = 0;

   gstDrive.stRxBuffer.ucState = 0;
   gstDrive.stRxBuffer.uiIndex = 0;

   gstDrive.eFault = 0;
   gstDrive.wSpeedFeedback = 0;
   gstDrive.bSpeedRegRelease = 0;
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Process_Running( void )
{
   static uint8_t ucCounter_5ms;

   gstDrive.stInterface.pfnReceive();
   gstDrive.stInterface.pfnUnloadRxBuffer();

   uint8_t ucResendDelay = Param_ReadValue_8Bit(enPARAM8__ResendDriveTimer_5ms);

   if ( ++ucCounter_5ms >= ucResendDelay )
   {
      ucCounter_5ms = 0;
      gstDrive.stInterface.pfnLoadTxBuffer();
      gstDrive.stInterface.pfnTransmit();
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Process_Initializing( void )
{
   gstDrive.eState = enDriveState__RUNNING;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Process_Offline( void )
{
   gstDrive.eState = enDriveState__INITIALIZING;
}
/*-----------------------------------------------------------------------------
   Check if motion is in state where speed reg should be high
 -----------------------------------------------------------------------------*/
static uint8_t CheckIfValidBContactorHighState()
{
   uint8_t bValid = 1;
   uint8_t ucMotionState = Motion_GetMotionState();
   if( ( ucMotionState == MOTION__STOPPED )
    || ( ucMotionState == MOTION__STOP_SEQUENCE )
    || ( ( ucMotionState == MOTION__START_SEQUENCE ) && !GetPickBrakeFlag() ) )
   {
      bValid = 0;
   }
   else if( GetAcceptanceContactorPickCommand( CONTACTOR_SEL__B2 ) )
   {
      bValid = 0;
   }
   return bValid;
}
/*-----------------------------------------------------------------------------
   Check if motion is in state where speed reg should be low
 -----------------------------------------------------------------------------*/
static uint8_t CheckIfValidBContactorLowState()
{
   uint8_t bValid = 0;
   uint8_t ucMotionState = Motion_GetMotionState();
   if( ( ucMotionState == MOTION__STOPPED )
    || ( ( ucMotionState == MOTION__STOP_SEQUENCE ) && ( !GetPickMFlag() ) )
    || ( ( ucMotionState == MOTION__START_SEQUENCE ) && ( !GetPickDriveFlag() ) ) )
   {
      if( !GetAcceptanceContactorPickCommand( CONTACTOR_SEL__B2 ) )
      {
         bValid = 1;
      }
   }
   return bValid;
}
/*-----------------------------------------------------------------------------
   Checks for invalid B contactor state
 -----------------------------------------------------------------------------*/
#define DEBOUNCE_B_CONT_STUCK_LOW_5MS     (400)
#define DEBOUNCE_B_CONT_STUCK_HIGH_5MS    (200)
static void CheckFor_BContactorFault()
{
   static uint16_t uwDebounceCounterLow_5ms;
   static uint16_t uwDebounceCounterHigh_5ms;
   uint8_t ucMotionState = Motion_GetMotionState();
   uint8_t bPickDrive = GetPickDriveFlag();
   uint8_t bSpeedRegHW = GetInputValue(enIN_MBC);
   uint8_t bActiveBrakeTest = ( GetOperation_AutoMode() == MODE_A__TEST )
                           && ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
                           && ( ( GetActiveAcceptanceTest() == ACCEPTANCE_TEST_SLIDE_DISTANCE )
                             || ( GetActiveAcceptanceTest() == ACCEPTANCE_TEST_EBRAKE_SLIDE )
                             || ( GetActiveAcceptanceTest() == ACCEPTANCE_TEST_BRAKEBOARD_FEEDBACK ) );
   bActiveBrakeTest |= Test_UM_GetTestActiveFlag();
   if( ( Drive_GetMotorLearnFlag() )
    || ( Rescue_GetManualTractionRescue() ) )
   {
      //Bypass B contactor check if learning motor or in manual traction rescue
   }
   /* Check for B contactor stuck low */
   else if( bPickDrive
         && CheckIfValidBContactorHighState() )
   {
      uwDebounceCounterHigh_5ms = 0;
      if( !bSpeedRegHW && !bActiveBrakeTest )
      {
         if( ++uwDebounceCounterLow_5ms >= DEBOUNCE_B_CONT_STUCK_LOW_5MS )
         {
            uwDebounceCounterLow_5ms = 0;
            SetFault(FLT__B_CONT_LO_HW);
         }
      }
      else if( !gstDrive.bSpeedRegRelease )
      {
         if( ++uwDebounceCounterLow_5ms >= DEBOUNCE_B_CONT_STUCK_LOW_5MS )
         {
            uwDebounceCounterLow_5ms = 0;
            SetFault(FLT__SPEED_REG_LO);
         }
      }
      else
      {
         uwDebounceCounterLow_5ms = 0;
      }
   }
   /* Check for B contactor stuck high */
   else if( !bPickDrive
         && CheckIfValidBContactorLowState() )
   {
      uwDebounceCounterLow_5ms = 0;
      if( bSpeedRegHW && !bActiveBrakeTest  )
      {
         if( ++uwDebounceCounterHigh_5ms >= DEBOUNCE_B_CONT_STUCK_HIGH_5MS )
         {
            uwDebounceCounterHigh_5ms = 0;
            SetFault(FLT__B_CONT_HI_HW);
         }
      }
      else if( gstDrive.bSpeedRegRelease )
      {
         if( ++uwDebounceCounterHigh_5ms >= DEBOUNCE_B_CONT_STUCK_HIGH_5MS )
         {
            uwDebounceCounterHigh_5ms = 0;
            SetFault(FLT__SPEED_REG_HI);
         }
      }
      else
      {
         uwDebounceCounterHigh_5ms = 0;
      }
   }
   else
   {
      uwDebounceCounterLow_5ms = 0;
      uwDebounceCounterHigh_5ms = 0;
   }
}

/*-----------------------------------------------------------------------------
 Counter will be reset when packet is received
 Trigger a fault if we don't receive from drive
 -----------------------------------------------------------------------------*/
static void CheckFor_DriveFault()
{
   if ( gstDrive.uiOfflineTime_5ms > DRIVE_COMM_TIMEOUT )
   {
      SetFault(FLT__DRIVE_OFFLINE);
      gstDrive.eFault = DRIVE_FAULT__COMM_TIMEOUT;
      ClearBuffers();
      gstDrive.uiOfflineTime_5ms = 0;
   }
   else
   {
      gstDrive.uiOfflineTime_5ms++;
      if( gstDrive.eFault_HPV
     && ( gstDrive.eType == enDriveType__HPV ) )
      {
         en_faults eFaultNum = FLT__MAG_FLT_1 + gstDrive.eFault_HPV - 1;
         if(eFaultNum > FLT__MAG_FLT_72)
         {
            eFaultNum = FLT__DRIVE_FLT_UNKNOWN;
         }
         SetFault(eFaultNum);
         gstDrive.eFault = DRIVE_FAULT__DRIVE_REPORTED_FAULT;
      }
      else if( gstDrive.eFault_KEB
          && ( gstDrive.eType == enDriveType__KEB ) )
      {
         en_faults eFaultNum = FLT__KEB_FLT_1 + gstDrive.eFault_KEB - 1;
         if(eFaultNum > FLT__KEB_FLT_223)
         {
            eFaultNum = FLT__DRIVE_FLT_UNKNOWN;
         }
         SetFault(eFaultNum);
         gstDrive.eFault = DRIVE_FAULT__DRIVE_REPORTED_FAULT;
      }
      else if( gstDrive.eFault_DSD
          && ( gstDrive.eType == enDriveType__DSD ) )
      {
         en_faults eFaultNum = FLT__DSD_NOT_RDY + gstDrive.eFault_DSD - 1;
         if(eFaultNum > FLT__DSD_COMM)
         {
            eFaultNum = FLT__DRIVE_FLT_UNKNOWN;
         }
         SetFault(eFaultNum);
         gstDrive.eFault = DRIVE_FAULT__DRIVE_REPORTED_FAULT;
      }
      else
      {
         gstDrive.eFault = DRIVE_FAULT__NONE;
         CheckFor_BContactorFault();
      }
   }
}
/*-----------------------------------------------------------------------------
   Check for regen fault input going high
 -----------------------------------------------------------------------------*/
#define REGEN_FAULT_DEBOUNCE_LIMIT_5MS     (60)
static void CheckFor_RegenFault()
{
   static uint16_t uwDebounce_5ms;
   if( GetInputValue(enIN_REGEN_FLT) )
   {
      if(uwDebounce_5ms >= REGEN_FAULT_DEBOUNCE_LIMIT_5MS)
      {
         SetFault(FLT__REGEN_FAULT);
      }
      else
      {
         uwDebounce_5ms++;
      }
   }
   else
   {
      uwDebounce_5ms = 0;
   }
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   //------------------------
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = 5;

   gstDrive.uiOfflineTime_5ms = 0;
   gstDrive.ucResetCounter = 0;
   gstDrive.ucBadPacketCountdown = 0;
   gstDrive.eType = Param_ReadValue_8Bit(enPARAM8__DriveSelect);
   if(gstDrive.eType >= NUM_DRIVE_TYPES)
   {
      gstDrive.eType = enDriveType__HPV;
   }

   ( *gapfnLoadInterface[ gstDrive.eType ] )( &gstDrive );
   Init_UART_RingBuffer(gstDrive.eType);

   return 0;
}

/*-----------------------------------------------------------------------------

    Controller searches for a drive at startup. It loads one of the
    interfaces and waits for a valid response. If no response within
    a certain amount of time, it resets the comm port and tries a different
    drive model. It rotates between all drive models. If a drive_x is found,
    drive_x_config() is called once to register it. then the state is
    transitioned to init. while in this state, drive_x_init() is called
    continuously until all initial parameters to be read/written to/from
    drive done. then state transitions to running and drive_x_run() is
    called continuously while drive is online. if drive goes offline,
    drive_x_clear() is called once then state transitions to offline where
    discoverDrive() is called continuously.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   CheckFor_Pretorque();
   switch ( gstDrive.eState )
   {
      case enDriveState__RUNNING:
         Process_Running();
         break;

      case enDriveState__INITIALIZING:
         Process_Initializing();
         break;
      case enDriveState__OFFLINE:
      default:
         Process_Offline();
         break;
   }
   if(!Rescue_GetManualTractionRescue())
   {
      CheckFor_DriveFault();
      CheckFor_RegenFault();
   }
   CheckIfDriveResetNeeded();
   CheckFor_MotorLearn();
   CheckFor_BadPacketUpdates();
   return 0;
}


/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
