/******************************************************************************
 *
 * @file     drive_hpv900_s2.c
 * @brief    
 * @version  V1.00
 * @date     1, April 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "motion.h"
#include <string.h>
#include <sys_drive.h>
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

enum en_hpv_message_types
{
   enHPV_MSG_TYPE__DRIVE_PARAM_READ = 1,
   enHPV_MSG_TYPE__RUNTIME_MESSAGE,
   enHPV_MSG_TYPE__DRIVE_PARAM_WRITE,
   enHPV_MSG_TYPE__HANDHELD_OPERATOR,

   NUM_HPV_MESSAGE_TYPES
};

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint16_t uwRxErrorCounter;//Counts number of bad packets received
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Returns the number of bad bytes/packets detected
-----------------------------------------------------------------------------*/
uint16_t GetDriveRxErrorCount_HPV(void)
{
   return uwRxErrorCounter;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadParamReadResponse( void )
{
   if(Param_ReadValue_1Bit(enPARAM1__EnableUIDriveEdit))
   {
      if( gstDrive.stRxBuffer.aucData[2] == HPV_SETUP_ID__READ_16BIT_PARAM ) // setup parameters?
      {
         if( gstDrive.stRxBuffer.aucData[3] == 0 ) // valid parameter requested?
         {
            uint16_t uwParameterID = gstDrive.stRxBuffer.aucData[4] << 8;
            uwParameterID |= gstDrive.stRxBuffer.aucData[5];
            uint32_t uiData = gstDrive.stRxBuffer.aucData[6] << 16;
            uiData |= gstDrive.stRxBuffer.aucData[7] << 8;
            uiData |= gstDrive.stRxBuffer.aucData[8];
            DriveParam_UnloadDriveResponse( uwParameterID, uiData );
         }
      }
   }
}
/*-----------------------------------------------------------------------------
Byte0:  Sync Byte
Byte1:  Msg ID
Byte2:  Setup ID (0x06)
Byte3:  Data0
            0x00 = Change accepted
            0x02 = Parameter ID out of range
            0x03 = Parameter data out of range
Byte4:  Parameter ID (MSB)
Byte5:  Parameter ID (LSB)
Byte6:  Parameter Data (MSB)
Byte7:  Parameter Data (Middle Byte)
Byte8:  Parameter Data (LSB)
Byte9:  Checksum
 -----------------------------------------------------------------------------*/
static void UnloadParamWriteResponse( void )
{
   if(Param_ReadValue_1Bit(enPARAM1__EnableUIDriveEdit))
   {
      if( gstDrive.stRxBuffer.aucData[2] == HPV_SETUP_ID__WRITE_16BIT_PARAM ) // setup parameters?
      {
         if( gstDrive.stRxBuffer.aucData[3] == 0 ) // valid parameter requested?
         {
            uint16_t uwParameterID = gstDrive.stRxBuffer.aucData[4] << 8;
            uwParameterID |= gstDrive.stRxBuffer.aucData[5];
            uint32_t uiData = gstDrive.stRxBuffer.aucData[6] << 16;
            uiData |= gstDrive.stRxBuffer.aucData[7] << 8;
            uiData |= gstDrive.stRxBuffer.aucData[8];

            DriveParam_UnloadDriveResponse( uwParameterID, uiData );
         }
      }
   }
}
/*-----------------------------------------------------------------------------

   Extacting data from the drive's serial response.
   Bit1 of Byte3 contains fault status.
   Bit2 of Byte3 contains speed reg release which indicates that the drive
   is ready to command non-zero speed. In the motion module I command zero speed
   at the beginning of each run until the drive sends a speed reg release.

 -----------------------------------------------------------------------------*/
static void UnloadRuntimeMessage( void )
{
   if ( gstDrive.stRxBuffer.aucData[ 2 ] & 1 )
   {
      if( gstDrive.stRxBuffer.aucData[ 5 ] )
      {
         gstDrive.eFault_HPV = gstDrive.stRxBuffer.aucData[ 5 ];
      }
      else
      {
         gstDrive.eFault_HPV = DRIVE_ERROR_HPV__UNK;
      }
   }
   else
   {
      gstDrive.eFault_HPV = DRIVE_ERROR_HPV__NONE;
   }

   gstDrive.bSpeedRegRelease = gstDrive.stRxBuffer.aucData[ 2 ] >> 1 & 1;

   int16_t wMagnetekSpeed = gstDrive.stRxBuffer.aucData[ 7 ] << 8
                          | gstDrive.stRxBuffer.aucData[ 8 ];
   gstDrive.wSpeedFeedback = ConvertMagnetekSpeedToFPM( wMagnetekSpeed );
}

/*-----------------------------------------------------------------------------
3.1 Setup Message to Read (Request) Info from the Drive (Msg ID = 0x01)
 -----------------------------------------------------------------------------*/
static void LoadParamReadRequest( void )
{
   uint16_t uwParamID = GetDriveParameterID();

   gstDrive.stTxBuffer.uiIndex = 0;

   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = HPV_SYNC_BYTE;
   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = enHPV_MSG_TYPE__DRIVE_PARAM_READ;

   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = HPV_SETUP_ID__READ_16BIT_PARAM;

   /* Data = 0 */
   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = 0;
   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = 0;
   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = 0;
   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = 0;

   /* Parameter ID */
   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = (uwParamID >> 8) & 0xFF;
   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = (uwParamID) & 0xFF;
}

/*-----------------------------------------------------------------------------
3.2. Setup Message to Set Variables in the Drive (Msg ID = 0x03)
 -----------------------------------------------------------------------------*/
static void LoadParamWriteRequest( void )
{
   uint16_t uwParamID = GetDriveParameterID();
   uint32_t uiData = GetDriveParameterValue();

   gstDrive.stTxBuffer.uiIndex = 0;

   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = HPV_SYNC_BYTE;
   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = enHPV_MSG_TYPE__DRIVE_PARAM_WRITE;

   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = HPV_SETUP_ID__WRITE_16BIT_PARAM;

   /* Parameter ID */
   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = (uwParamID >> 8) & 0xFF;
   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = (uwParamID) & 0xFF;

   /* Data */
   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = (uiData >> 24) & 0xFF;
   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = (uiData >> 16) & 0xFF;
   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = (uiData >> 8) & 0xFF;
   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = (uiData) & 0xFF;
}

/*-----------------------------------------------------------------------------

   I am using GetPickDriveFlag() to determine when to start/stop sending the run
   command to the drive. GetPickDriveFlag() is being set in the start sequence and
   cleared in the stop sequence in the motion module.

 -----------------------------------------------------------------------------*/
static void LoadRuntimeMessage( void )
{
   int16_t wSpeedCommand = GetMagnetekSpeedCommand();
   int16_t wPretorqueCommand = GetMagnetekPretorqueCommand();
   //------------------------
   // Byte 1 - Sync Byte (0xFA)
   gstDrive.stTxBuffer.aucData[ 0 ] = 0xFA;

   //------------------------
   // Byte 2 - Msg ID (2 = Runtime message)
   gstDrive.stTxBuffer.aucData[ 1 ] = enHPV_MSG_TYPE__RUNTIME_MESSAGE;

   //------------------------
   // Byte 3 - Logic Byte 1
   //    Bit0 = Run requested
   //    Bit1 = Fault reset request (on rising edge)
   //    Bit2 = Pretorque latch bit
   //    Bit3 = Closed loop gain reduction used
   //    Bit4 = Torque ramp down enabled
   //    Bit5 = Brake pick command
   //    Bit6 = Brake hold command
   //    Bit7 = Overspeed test request (on rising edge)
   uint8_t uc = 0;

   uc |= 0x04;//Latch pretorque

   if ( !gstFault.bActiveFault )
   {
      if ( GetPickDriveFlag() )
      {
         uc |= 0x01;  // run
         uc |= 0x20;  // pick brake
      }
   }

   if(gstDrive.bTriggerDriveReset)
   {
      uc |= 0x02;  // fault reset
   }

   gstDrive.stTxBuffer.aucData[ 2 ] = uc;

   //------------------------
   // Byte 4 - Logic Byte 2 (Reserved - write 0's only)
   gstDrive.stTxBuffer.aucData[ 3 ] = 0;

   //------------------------
   // Byte 5 - Speed Hi Byte
   gstDrive.stTxBuffer.aucData[ 4 ] = (uint8_t) (wSpeedCommand >> 8);

   //------------------------
   // Byte 6 - Speed Lo Byte
   gstDrive.stTxBuffer.aucData[ 5 ] = (uint8_t) (wSpeedCommand & 0xff);
#if 0
   //------------------------
   // Byte 7 - Cmd ID (3 = Monitor)
   gstDrive.stTxBuffer.aucData[ 6 ] = 3;

   //------------------------
   // Byte 8 - Numeric Hi Byte (12 = Logic outputs)
   gstDrive.stTxBuffer.aucData[ 7 ] = 12;

   //------------------------
   // Byte 9 - Numeric Lo Byte (11 = Logic inputs)
   gstDrive.stTxBuffer.aucData[ 8 ] = 11;
#else
   //------------------------
   // Byte 7 - Cmd ID (1 = Pretorque)
   gstDrive.stTxBuffer.aucData[ 6 ] = 1;

   //------------------------
   // Byte 8 - Numeric Hi Byte (Pretorque MSB)
   gstDrive.stTxBuffer.aucData[ 7 ] = (uint8_t) (wPretorqueCommand >> 8);

   //------------------------
   // Byte 9 - Numeric Lo Byte (Pretorque LSB)
   gstDrive.stTxBuffer.aucData[ 8 ] = (uint8_t) (wPretorqueCommand & 0xff);
#endif
   gstDrive.stTxBuffer.uiIndex = 9;  // 9 bytes placed in buffer
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadTxBuffer( void )
{
   static Drive_Msg_Type eMessageType = DRIVE_MSG__RUNTIME;//Variable controlling which message to transmit
   static uint8_t ucMotionCountdown; // continue sending runtime message after run
   if( GetMotion_RunFlag() )
   {
      eMessageType = DRIVE_MSG__RUNTIME;
      ucMotionCountdown = 200;
   }
   else if( ucMotionCountdown )
   {
      eMessageType = DRIVE_MSG__RUNTIME;
      ucMotionCountdown--;
   }
   else if( GetOperation_ClassOfOp() != CLASSOP__MANUAL )
   {
      eMessageType = DRIVE_MSG__RUNTIME;
   }

   //-------------------------------------
   if(Param_ReadValue_1Bit(enPARAM1__EnableUIDriveEdit))
   {
      if( eMessageType == DRIVE_MSG__READ_PARAM )
      {
         LoadParamReadRequest();
      }
      else if( eMessageType == DRIVE_MSG__WRITE_PARAM )
      {
         LoadParamWriteRequest();
         //TODO set alarm
      }
      else //if( eMessageType == DRIVE_MSG__RUNTIME )
      {
         LoadRuntimeMessage();
         eMessageType = DRIVE_MSG__RUNTIME;
      }

      en_drive_commands eNewCommand = GetDriveParameterCommand();
      /* Check for change in drive parameter command */
      if( eNewCommand == DRIVE_CMD__READ )
      {
         eMessageType = DRIVE_MSG__READ_PARAM;
      }
      else if( eNewCommand == DRIVE_CMD__WRITE )
      {
         eMessageType = DRIVE_MSG__WRITE_PARAM;
      }
      else
      {
         DriveParam_ClrDriveEditConfirm();
         eMessageType = DRIVE_MSG__RUNTIME;
      }
   }
   else
   {
      LoadRuntimeMessage();
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadRxBuffer( void )
{
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadNewData()
{
   switch ( gstDrive.stRxBuffer.aucData[ 1 ] )  // message type
   {
      case enHPV_MSG_TYPE__DRIVE_PARAM_READ:  // response to read info from the drive
         UnloadParamReadResponse();
         break;
      case enHPV_MSG_TYPE__RUNTIME_MESSAGE:  // Runtime message
         UnloadRuntimeMessage();
         break;
      case enHPV_MSG_TYPE__DRIVE_PARAM_WRITE:  // Drive's response to a write request
         UnloadParamWriteResponse();
         break;
      default:
         break;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

static void Receive( void )
{
   uint8_t ucByteReceived;
   for(uint8_t i = 0; i < DRIVE_UART_RX_BYTE_LIMIT; i++)
   {
      if(UART_ReadByte(&ucByteReceived))
      {
         if ( gstDrive.stRxBuffer.uiIndex >= sizeof( gstDrive.stRxBuffer.aucData ) )
         {
            gstDrive.stRxBuffer.uiIndex = 0;
            gstDrive.stRxBuffer.ucState = 0;
            uwRxErrorCounter++;
         }
         else
         {
            gstDrive.stRxBuffer.aucData[ gstDrive.stRxBuffer.uiIndex++ ] = ucByteReceived;

            if ( gstDrive.stRxBuffer.ucState == 0 )  // awaiting 0xFA
            {
               if ( ucByteReceived == 0xFA )
               {
                  gstDrive.stRxBuffer.ucChecksum = ucByteReceived;

                  ++gstDrive.stRxBuffer.ucState;
               }
               else
               {
                  gstDrive.stRxBuffer.uiIndex = 0;
               }
            }
            else if ( gstDrive.stRxBuffer.ucState == 1 )  // awaiting message ID
            {
               gstDrive.stRxBuffer.ucChecksum += ucByteReceived;

               ++gstDrive.stRxBuffer.ucState;

               switch ( ucByteReceived )
               {
                  case enHPV_MSG_TYPE__DRIVE_PARAM_READ:  // Setup message response to request info from the drive
                  case enHPV_MSG_TYPE__DRIVE_PARAM_WRITE:  // Setup message response to request to set variables in the drive

                     // 10 bytes in message. Subtract 2 already received and don't
                     // count the checksum.
                     gstDrive.stRxBuffer.ucBytesLeft = 10 - 3;
                     break;

                  case enHPV_MSG_TYPE__RUNTIME_MESSAGE:  // Runtime message?
                  case enHPV_MSG_TYPE__HANDHELD_OPERATOR:  // Handheld operator display response (should never receive this)

                     // 16 bytes in message. Subtract 2 already received and don't
                     // count the checksum.
                     gstDrive.stRxBuffer.ucBytesLeft = 16 - 3;
                     break;

                  default:
                     gstDrive.stRxBuffer.uiIndex = 0;
                     gstDrive.stRxBuffer.ucState = 0;
                     uwRxErrorCounter++;
                     break;
               }
            }
            else // ucState = 2
            {
               if ( gstDrive.stRxBuffer.ucBytesLeft )
               {
                  gstDrive.stRxBuffer.ucChecksum += ucByteReceived;

                  --gstDrive.stRxBuffer.ucBytesLeft;
               }
               else
               {
                  if ( gstDrive.stRxBuffer.ucChecksum != ucByteReceived )  // bad checksum?
                  {
                     uwRxErrorCounter++;
                  }
                  else  // good checksum
                  {
                     UnloadNewData();
                     gstDrive.uiOfflineTime_5ms = 0;
                  }

                  gstDrive.stRxBuffer.uiIndex = 0;
                  gstDrive.stRxBuffer.ucState = 0;
               }
            }
         }
      }
      else
      {
         i = DRIVE_UART_RX_BYTE_LIMIT;
         break;
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Transmit( void )
{
   //------------------------
   // Calculate the checksum on the data to send.
   uint8_t ucChecksum = 0;
   uint8_t i;

   for ( i = 0; i < gstDrive.stTxBuffer.uiIndex; ++i )
   {
      ucChecksum += gstDrive.stTxBuffer.aucData[ i ];
   }

   //todo rm
   if(gstDrive.ucBadPacketCountdown)
   {
      ucChecksum++;
      gstDrive.ucBadPacketCountdown--;
   }

   gstDrive.stTxBuffer.aucData[ gstDrive.stTxBuffer.uiIndex++ ] = ucChecksum;

   //------------------------
   // Transmit the data to the drive.
   if(gstDrive.stTxBuffer.uiIndex )
   {
      uint32_t uiBytesSent = UART_SendRB(&gstDrive.stTxBuffer.aucData[0],
                                          gstDrive.stTxBuffer.uiIndex );
      if(gstDrive.stTxBuffer.uiIndex != uiBytesSent)
      {
         uiBytesSent = gstDrive.stTxBuffer.uiIndex;
         //todo set fault
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void LoadInterface_HPV( struct st_drive * pstDrive )
{
   pstDrive->eType = enDriveType__HPV;

   pstDrive->stInterface.pfnLoadTxBuffer = LoadTxBuffer;
   pstDrive->stInterface.pfnUnloadRxBuffer = UnloadRxBuffer;

   pstDrive->stInterface.pfnTransmit = Transmit;
   pstDrive->stInterface.pfnReceive = Receive;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

