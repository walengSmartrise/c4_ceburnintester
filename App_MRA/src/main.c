/******************************************************************************
 *
 * @file     main.c
 * @brief    Program start point and main loop.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
#include "board.h"

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

//------------------------------------------------------------------------------
static struct st_module * gastModules_MRA[] =
{
         &gstSys_Mod,  // must include the system module in addition to app modules
         &gstMod_Fault,
         &gstMod_FaultApp,
         &gstMod_Heartbeat,
         &gstMod_Watchdog,

         &gstMod_LocalInputs,
         &gstMod_CAN,
         &gstMod_UART,
         &gstMod_SData,
         &gstMod_ParamEEPROM,

         &gstMod_ParamApp,
         &gstMod_ParamMaster,
         &gstMod_ParamSlave,
         &gstMod_ModeOfOper,
         &gstMod_Oper_Auto,

         &gstMod_Oper_Manual,
         &gstMod_Oper_Learn,
         &gstMod_Fire,
         &gstMod_Flood_Op,
         &gstMod_Sabbath,

         &gstMod_CarCalls,
         &gstMod_Dispatch,
         &gstMod_Earthquake,
         &gstMod_Capture,
         &gstMod_CarToLobby,

         &gstMod_MaxRuntime,
         &gstMod_Releveling,
         &gstMod_Doors,
         &gstMod_InputMapping,
         &gstMod_OutputMapping,

         &gstMod_RTC,
         &gstMod_AlarmApp,
         &gstMod_TemporaryTest,
         &gstMod_Position,
         &gstMod_Motion,

         &gstMod_Brake,
         &gstMod_Drive,
         &gstMod_Safety,
         &gstMod_FRAM,
//         &gstMod_Runtimer,

         &gstMod_SData_CtrlNet,
         &gstMod_FPGA,
         &gstMod_AcceptanceTest,
         &gstMod_RunLog,
         &gstMod_ETS,

         &gstMod_CarCallSecurity,
         &gstMod_EMS,
         &gstMod_Rescue,
         &gstMod_PhoneFailure,
         &gstMod_IdleTime,
};

static struct st_module_control gstModuleControl_MRA =
{
   .uiNumModules = sizeof(gastModules_MRA) / sizeof(gastModules_MRA[ 0 ]),

   .pastModules = &gastModules_MRA,

   .enMCU_ID = enMCUA_SRU_Base,

   .enDeployment = enSRU_DEPLOYMENT__MR,
};

static struct st_module * gastModules_MRA_Pre[] =
{
         &gstSys_Mod,  // must include the system module in addition to app modules

         &gstMod_ParamEEPROM,
         &gstMod_ParamApp,
         &gstMod_ParamMaster,
         &gstMod_ParamSlave,
};

static struct st_module_control gstModuleControl_MRA_Pre =
{
   .uiNumModules = sizeof(gastModules_MRA_Pre) / sizeof(gastModules_MRA_Pre[ 0 ]),

   .pastModules = &gastModules_MRA_Pre,

   .enMCU_ID = enMCUA_SRU_Base,

   .enDeployment = enSRU_DEPLOYMENT__MR,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
struct st_module_control *gpstPreModuleControl_ThisDeployment;
struct st_module_control *gpstModuleControl_ThisDeployment;


/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
uint8_t gbBackupSync;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void Deployment_Init( void )
{
   int iError = 1;  // assume error
   en_sru_deployments deplyment = GetSRU_Deployment();

   if ( deplyment == enSRU_DEPLOYMENT__MR )
   {
      gpstModuleControl_ThisDeployment = &gstModuleControl_MRA;

      gpstPreModuleControl_ThisDeployment = &gstModuleControl_MRA_Pre;

      gbBackupSync = SRU_Read_DIP_Switch( enSRU_DIP_B5 );

      SetSystemNodeID(SYS_NODE__MRA);
      SharedData_Init();
      SharedData_CtrlNet_Init();
      iError = 0;
   }

   if ( iError )
   {
      while ( 1 )
      {
         static uint32_t uiCounter, bState;

         if( uiCounter >= 0xfffff )
         {
            uiCounter = 0;

            bState = ( bState )? 0: 1;

            SRU_Write_LED( enSRU_LED_Heartbeat, bState );
            SRU_Write_LED( enSRU_LED_Alarm,     bState );
            SRU_Write_LED( enSRU_LED_Fault,     bState );
         }
         else
         {
            uiCounter++;
         }
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
int main( void )
{
   SystemCoreClockUpdate();
   Sys_Init();

   MCU_A_Init();

   Deployment_Init();

   // These are the modules that need to be started before the main application.
   // Right now this is just parameters. This is because most modules require
   // parameters and the parameters are not valid till they are loaded into RAM
   __enable_irq();

#if ENABLE_SEGGER_DEBUG
      SEGGER_SYSVIEW_Conf();
      SEGGER_RTT_Init();
   #if RTT_ENABLE_MOTION_LOGGING
      SEGGER_RTT_WriteString(SEGGER_CHANNEL_IN_USE, "RTT Log - Motion\r\n");
   #elif RTT_ENABLE_MODULE_CALL_PERIOD_LOGGING
      SEGGER_RTT_WriteString(SEGGER_CHANNEL_IN_USE, "RTT Log - Module Run Period\r\n");
   #elif RTT_ENABLE_BRAKE_COM_LOGGING
      SEGGER_RTT_WriteString(SEGGER_CHANNEL_IN_USE, "RTT Log - Brake COM\r\n");
   #endif
#endif

   Mod_InitAllModules( gpstPreModuleControl_ThisDeployment );
   while(!GetFP_FlashParamsReady())
   {
       Mod_RunOneModule( gpstPreModuleControl_ThisDeployment );
   }

   Mod_InitAllModules( gpstModuleControl_ThisDeployment );



   // This is really a while(1) loop. Sys_Shutdown() always returns 0. The
   // function is being provided in anticipation of a future time when this
   // application might be running on a PC simulator and we would need a
   // mechanism to terminate this application.

   while ( !Sys_Shutdown() )
   {
      Mod_RunOneModule( gpstModuleControl_ThisDeployment );

      uint8_t ucCurrModIndex = gpstModuleControl_ThisDeployment->uiCurrModIndex;
      uint8_t ucNumOfMods = gpstModuleControl_ThisDeployment->uiNumModules;

      struct st_module* pstCurrentModule = gastModules_MRA[ucCurrModIndex];

      if( ( pstCurrentModule->bTimeViolation )
       && ( ucCurrModIndex < ucNumOfMods )
       && ( ( Param_ReadValue_8Bit(enPARAM8__TimeViolationModule) == MRC__ALL )
         || ( ( Param_ReadValue_8Bit(enPARAM8__TimeViolationModule)-MRC__MRA_START ) == ucCurrModIndex ) )  )
      {
         SetAlarm( ALM__RUN_TIME_FAULT + ucCurrModIndex - 1);
      }

      Debug_CAN_MonitorUtilization();
   }
   
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
