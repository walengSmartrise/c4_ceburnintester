/******************************************************************************
 *
 * @version  V1.00
 * @date     5, September 2018
 *
 * @note    This module monitors the phone failure contact and in case of failure
 *          triggers buzzer and visual signals.
 *          This feature is guided by the document A17-2013, 2.27.1.1.6
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "drive.h"
#include "mod.h"
#include "sru_a.h"
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_module gstMod_PhoneFailure =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MODULE_TIME_TO_SECONDS            (10)
#define MODULE_TIME_TO_MINUTES            (60*MODULE_TIME_TO_SECONDS)
#define MODULE_TIME_TO_HOURS              (60*MODULE_TIME_TO_MINUTES)

#define INPUT_DEBOUNCE_LIMIT_100MS        (5)

#define BUZZER_MIN_DURATION_100MS         (MODULE_TIME_TO_SECONDS) // > 500 msec
#define BUZZER_MIN_INTERVAL_100MS         (15*MODULE_TIME_TO_SECONDS) // < 30 sec
#define BUZZER_RESET_TIMEOUT_100MS        (12*MODULE_TIME_TO_HOURS)

#define LAMP_TOGGLE_RATE_100MS            (5)

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t bBuzzerResetFlag;
static uint8_t bPhoneFailure;

/* Module Outputs */
static uint8_t bBuzzer;
static uint8_t bLamp;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Module Outputs
 -----------------------------------------------------------------------------*/
uint8_t PhoneFailure_GetBuzzer(void)
{
   return bBuzzer;
}
uint8_t PhoneFailure_GetLamp(void)
{
   return bLamp;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdatePhoneFailureSignal(void)
{
   static uint8_t ucDebounce_100ms;
   if( bPhoneFailure != GetInputValue(enIN_PhoneFailActive) )
   {
      if( ++ucDebounce_100ms >= INPUT_DEBOUNCE_LIMIT_100MS )
      {
         ucDebounce_100ms = 0;
         bPhoneFailure = GetInputValue(enIN_PhoneFailActive);
      }
   }
   else
   {
      ucDebounce_100ms = 0;
   }
}
/*-----------------------------------------------------------------------------
   This updates the reset signal used to suppress the phone buzzer
   output when silenced by authorized personnel.
   2.27.1.1.6 (b) (3)
 -----------------------------------------------------------------------------*/
static void UpdateBuzzerResetSignal(void)
{
   static uint8_t bResetInput;
   static uint8_t ucDebounce_100ms;
   static uint32_t uiResetCountdown_100ms;

   if( bResetInput != GetInputValue(enIN_PhoneFailReset) )
   {
      if( ++ucDebounce_100ms >= INPUT_DEBOUNCE_LIMIT_100MS )
      {
         ucDebounce_100ms = 0;
         bResetInput = GetInputValue(enIN_PhoneFailReset);
      }
   }
   else
   {
      ucDebounce_100ms = 0;
   }

   if( bResetInput )
   {
      uiResetCountdown_100ms = BUZZER_RESET_TIMEOUT_100MS;
      bBuzzerResetFlag = 1;
   }
   else if( uiResetCountdown_100ms )
   {
      uiResetCountdown_100ms--;
      bBuzzerResetFlag = 1;
   }
   else
   {
      bBuzzerResetFlag = 0;
   }
}
/*-----------------------------------------------------------------------------
A17 2.27.1.1.6 (b) (2b)
 -----------------------------------------------------------------------------*/
static void UpdateBuzzerOutput(void)
{
   static uint16_t uwIntervalCountdown_100ms;
   static uint8_t ucDurationTimer_100ms;

   bBuzzer = 0;

   // Combine with phone failure input from DL20
   uint8_t bPhoneFailure_Combined = bPhoneFailure || DL20_GetPhoneFailureFlag();

   if( bPhoneFailure_Combined && !bBuzzerResetFlag )
   {
      if( !uwIntervalCountdown_100ms )
      {
         if( ucDurationTimer_100ms < BUZZER_MIN_DURATION_100MS )
         {
            ucDurationTimer_100ms++;
            bBuzzer = 1;
         }
         else
         {
            uwIntervalCountdown_100ms = BUZZER_MIN_INTERVAL_100MS;
         }
      }
      else
      {
         uwIntervalCountdown_100ms--;
         ucDurationTimer_100ms = 0;
      }
   }
   else
   {
      uwIntervalCountdown_100ms = 0;
      ucDurationTimer_100ms = 0;
   }
}

/*-----------------------------------------------------------------------------
A17 2.27.1.1.6 (b) (2b)
 -----------------------------------------------------------------------------*/
static void UpdateLampOutput(void)
{
   static uint8_t ucToggleTimer_100ms;
   // Combine with phone failure input from DL20
   uint8_t bPhoneFailure_Combined = bPhoneFailure || DL20_GetPhoneFailureFlag();
   if( bPhoneFailure_Combined )
   {
      if( ++ucToggleTimer_100ms >= LAMP_TOGGLE_RATE_100MS )
      {
         ucToggleTimer_100ms = 0;
         bLamp = !bLamp;
      }
   }
   else
   {
      ucToggleTimer_100ms = 0;
      bLamp = 0;
   }
}
/*-----------------------------------------------------------------------------

 Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
    pstThisModule->uwInitialDelay_1ms = 10000;
    pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_PHONE_FAILURE_1MS;

    return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   UpdatePhoneFailureSignal();
   UpdateBuzzerResetSignal();

   UpdateBuzzerOutput();
   UpdateLampOutput();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
