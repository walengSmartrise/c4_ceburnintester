/******************************************************************************
 *
 * @file     mod_heartbeat.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
#include "position.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

static enum en_mode_manual GetInspectionMode();

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_ModeOfOper =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
/* Used to limit repeat calling of GetInspectionMode() */
static enum en_mode_manual eCurrentInspectionMode;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Returns 1 after inspection signals from all boards have been received.
 -----------------------------------------------------------------------------*/
uint8_t CheckIf_AllStartupSignalsReceived()
{
   static uint8_t bReturn = 0;
   if( !bReturn
    && ( GetDatagramReceivedCount(DATAGRAM_ID_28) > 2 ) //RISER INPUTS
    && ( GetDatagramReceivedCount(DATAGRAM_ID_26) > 2 ) // CT INPUTS
    && ( GetDatagramReceivedCount(DATAGRAM_ID_27) > 2 ) // COP INPUTS
    )
    {
      bReturn = 1;
    }
   else if( !bReturn && ( eCurrentInspectionMode == MODE_M__CONSTRUCTION ) )
   {
      return 1;
   }
   return bReturn;
}
/*-----------------------------------------------------------------------------


 -----------------------------------------------------------------------------*/

uint8_t GetFloorIndex( uint32_t ulPosCount )
{
   uint8_t ucFloor = INVALID_FLOOR;

   if (ulPosCount)
   {
      for (uint8_t i = 0; i < GetFP_NumFloors(); i++ )
      {
         if ( ulPosCount < ( gpastFloors[ i ].ulPosition ) )
         {
            ucFloor = i;
            if ( ucFloor )
            {
               uint32_t uwHalfway = ((gpastFloors[ i ].ulPosition - gpastFloors[ i-1 ].ulPosition)/2) + gpastFloors[ (i-1) ].ulPosition;
               if (ulPosCount < uwHalfway)
               {
                  ucFloor--;
               }
            }
            break;
         } else if ( (i+1) >= GetFP_NumFloors() ) {
            ucFloor = i;
         }
      }
   }
   return ucFloor;
}
/*-----------------------------------------------------------------------------
 Get debounced learn mode signal
 -----------------------------------------------------------------------------*/
static uint8_t GetLearnModeFlag()
{
   static uint8_t ucDebounce_50ms;
   uint8_t bActive = 0;
   if( SRU_Read_DIP_Switch(enSRU_DIP_A5) )
   {
      if(ucDebounce_50ms < MODE_SIGNALS_DEBOUNCE_50MS)
      {
         ucDebounce_50ms++;
      }
      else
      {
         bActive = 1;
      }
   }
   else
   {
      ucDebounce_50ms = 0;
   }
   return bActive;
}
/*-----------------------------------------------------------------------------
   Debounced inspection signals
 -----------------------------------------------------------------------------*/
static uint8_t GetCarTopInspectionFlag()
{
   static uint8_t ucDebounce_50ms;
   uint8_t bActive = 0;
   if( GetInputValue(enIN_CTTR) )
   {
      if(ucDebounce_50ms < MODE_SIGNALS_DEBOUNCE_50MS)
      {
         ucDebounce_50ms++;
      }
      else
      {
         bActive = 1;
      }
   }
   else
   {
      ucDebounce_50ms = 0;
   }
   return bActive;
}
static uint8_t GetInCarInspectionFlag()
{
   static uint8_t ucDebounce_50ms;
   uint8_t bActive = 0;
   if( GetInputValue(enIN_ICTR) )
   {
      if(ucDebounce_50ms < MODE_SIGNALS_DEBOUNCE_50MS)
      {
         ucDebounce_50ms++;
      }
      else
      {
         bActive = 1;
      }
   }
   else
   {
      ucDebounce_50ms = 0;
   }
   return bActive;
}
static uint8_t GetHoistwayAccessInspectionFlag()
{
   static uint8_t ucDebounce_50ms;
   uint8_t bActive = 0;
   if( GetInputValue(enIN_HATR) )
   {
      if(ucDebounce_50ms < MODE_SIGNALS_DEBOUNCE_50MS)
      {
         ucDebounce_50ms++;
      }
      else
      {
         bActive = 1;
      }
   }
   else
   {
      ucDebounce_50ms = 0;
   }
   return bActive;
}
static uint8_t GetMachineRoomInspectionFlag()
{
   static uint8_t ucDebounce_50ms;
   uint8_t bActive = 0;
   if( GetInputValue(enIN_MRTR) )
   {
      if(ucDebounce_50ms < MODE_SIGNALS_DEBOUNCE_50MS)
      {
         ucDebounce_50ms++;
      }
      else
      {
         bActive = 1;
      }
   }
   else
   {
      ucDebounce_50ms = 0;
   }
   return bActive;
}
static uint8_t GetPitInspectionFlag()
{
   static uint8_t ucDebounce_50ms;
   uint8_t bActive = 0;
   if( GetInputValue(enIN_IPTR)
    && Param_ReadValue_1Bit(enPARAM1__Enable_Pit_Inspection)
    && SRU_Read_DIP_Switch( enSRU_DIP_B4 ) )
   {
      if(ucDebounce_50ms < MODE_SIGNALS_DEBOUNCE_50MS)
      {
         ucDebounce_50ms++;
      }
      else
      {
         bActive = 1;
      }
   }
   else
   {
      ucDebounce_50ms = 0;
   }
   return bActive;
}
static uint8_t GetLandingInspectionFlag()
{
   static uint8_t ucDebounce_50ms;
   uint8_t bActive = 0;
   if( GetInputValue(enIN_ILTR)
    && Param_ReadValue_1Bit(enPARAM1__Enable_Landing_Inspection)
    && SRU_Read_DIP_Switch( enSRU_DIP_B3 ) )
   {
      if(ucDebounce_50ms < MODE_SIGNALS_DEBOUNCE_50MS)
      {
         ucDebounce_50ms++;
      }
      else
      {
         bActive = 1;
      }
   }
   else
   {
      ucDebounce_50ms = 0;
   }
   return bActive;
}
static uint8_t GetMechanicsModeFlag()
{
   static uint8_t ucDebounce_50ms;
   uint8_t bActive = 0;
   if( GetInputValue(enIN_MM) )
   {
      if(ucDebounce_50ms < MODE_SIGNALS_DEBOUNCE_50MS)
      {
         ucDebounce_50ms++;
      }
      else
      {
         bActive = 1;
      }
   }
   else
   {
      ucDebounce_50ms = 0;
   }
   return bActive;
}
/*-----------------------------------------------------------------------------
   TODO: Fix priority.

   See A17.1 2.26.1.4.4
 -----------------------------------------------------------------------------*/
static enum en_mode_manual GetInspectionMode()
{
   uint8_t bCartopInspection = GetCarTopInspectionFlag();
   uint8_t bInCarInspection = GetInCarInspectionFlag();
   uint8_t bHoistwayAccessInspection = GetHoistwayAccessInspectionFlag();
   uint8_t bMachineRoomInspection = GetMachineRoomInspectionFlag();
   uint8_t bLandingInspection = GetLandingInspectionFlag();
   uint8_t bPitInspection = GetPitInspectionFlag();
   uint8_t bMechanicsMode = GetMechanicsModeFlag();

   enum en_mode_manual eInspectionMode = MODE_M__NONE;

   uint8_t bAnyInspectionSignals = bCartopInspection
                                 | bInCarInspection
                                 | bHoistwayAccessInspection
                                 | bMachineRoomInspection
                                 | bLandingInspection
                                 | bPitInspection;
   if ( bAnyInspectionSignals )
   {
      eInspectionMode = MODE_M__INVALID;

      /* Per A17.1: if MR Insp and LND Insp ON at the same time or PIT Insp
       * with ANY other Insp switch ON then "No Insp Operation Allowed" */
      if ( bMechanicsMode )
      {
          eInspectionMode = MODE_M__INVALID;

         if ( bMachineRoomInspection ) //check for machine room inspection
         {
             eInspectionMode = MODE_M__CONSTRUCTION;
         }
      }
      else if ( bMachineRoomInspection
             && bLandingInspection ) /* MR and LND Insp ON */
      {
         eInspectionMode = MODE_M__INVALID;
      }
      else if ( ( bPitInspection )
             && ( bCartopInspection
               || bInCarInspection
               || bHoistwayAccessInspection
               || bMachineRoomInspection
               || bLandingInspection  ) ) /* PIT and another Insp ON */
   {
          eInspectionMode = MODE_M__INVALID;
      }
      else if(bCartopInspection)
      {
        eInspectionMode = MODE_M__INSP_CT;
        if (Param_ReadValue_1Bit(enPARAM1__ICInspectionRequiredForCT)
                && !bInCarInspection
                && !bHoistwayAccessInspection)
        {
           //FAULT!
           eInspectionMode = MODE_M__INVALID;
           if( !GetPreflightFlag() )
           {
              SetFault(FLT__INSP_IC_KEY_REQD);
           }
        }

      }
      else if(bInCarInspection)
      {
         eInspectionMode = MODE_M__INSP_IC;
      }
      else if(bHoistwayAccessInspection)
      {
         if ( AllHallDoorsClosed() )
         {
            if ( (GetOperation_ManualMode() == MODE_M__INSP_HA_BOTTOM)
              || (GetOperation_ManualMode() == MODE_M__INSP_HA_TOP) )
            {
               //If it was in a specific HA before, continue it on
               eInspectionMode = GetOperation_ManualMode();
            }
            else
            {
               eInspectionMode = MODE_M__INSP_HA;
            }
         }
         else
         {
            uint8_t ucNumLocksClosed = 0;
            uint8_t ucStopPoint = ((GetFP_RearDoors())?enIN_LBR:enIN_LBF);
            eInspectionMode = MODE_M__INSP_HA;

            for (uint8_t lock = enIN_LTF; lock <= ucStopPoint; lock++)
            {
               if ( GetInputValue ( lock ) )
               {
                  ucNumLocksClosed++; //count the number of closed locks
               }
            }

            // if 2 locks are closed for front door only, or 5 closed on rear doors
            if ( (!GetFP_RearDoors() && ucNumLocksClosed == 2) ||
                 ( GetFP_RearDoors() && ucNumLocksClosed == 5) )
            {
               if ( !GetInputValue( enIN_LBF ) ||
                  ( GetFP_RearDoors() && !GetInputValue( enIN_LBR ) ) )
               {
                  eInspectionMode = MODE_M__INSP_HA_BOTTOM;
               }
               else if ( !GetInputValue( enIN_LTF ) ||
                       ( GetFP_RearDoors() && !GetInputValue( enIN_LTR ) ) )
               {
                  eInspectionMode = MODE_M__INSP_HA_TOP;
               }
               else
               {
                  eInspectionMode = MODE_M__INSP_HA;
               }
            }
         }
         /* Exception for HA with manual swing doors.*/
         uint8_t bManualSwingDoors_F = Doors_CheckIfManualSwingDoor(DOOR_FRONT, INVALID_FLOOR);
         uint8_t bManualSwingDoors_R = Doors_CheckIfManualSwingDoor(DOOR_REAR,  INVALID_FLOOR);
         if( bManualSwingDoors_F && Door_GetSwingClosedContactsProgrammedFlag(DOOR_FRONT) )
         {
            if( !GetInputValue(enIN_TCL_F) && GetInputValue(enIN_MCL_F) && GetInputValue(enIN_BCL_F) )
            {
               eInspectionMode = MODE_M__INSP_HA_TOP;
            }
            else if( GetInputValue(enIN_TCL_F) && GetInputValue(enIN_MCL_F) && !GetInputValue(enIN_BCL_F) )
            {
               eInspectionMode = MODE_M__INSP_HA_BOTTOM;
            }
         }
         if( bManualSwingDoors_R && Door_GetSwingClosedContactsProgrammedFlag(DOOR_REAR) )
         {
            if( !GetInputValue(enIN_TCL_R) && GetInputValue(enIN_MCL_R) && GetInputValue(enIN_BCL_R) )
            {
               eInspectionMode = MODE_M__INSP_HA_TOP;
            }
            else if( GetInputValue(enIN_TCL_R) && GetInputValue(enIN_MCL_R) && !GetInputValue(enIN_BCL_R) )
            {
               eInspectionMode = MODE_M__INSP_HA_BOTTOM;
            }
         }
      }
      else if(bMachineRoomInspection)
      {
         eInspectionMode = MODE_M__INSP_MR;
      }
      else if(bPitInspection)
      {
         eInspectionMode = MODE_M__INSP_PIT;
      }
      else if(bLandingInspection)
      {
         eInspectionMode = MODE_M__INSP_LND;
      }
   }
   if( !GetPreflightFlag() )
   {
      SetOperation_ManualMode(eInspectionMode);
   }

   return eInspectionMode;
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   //------------------------
    SetOperation_ClassOfOp(CLASSOP__UNKNOWN);

    SetOperation_ManualMode(MODE_M__UNKNOWN);

   //------------------------
   pstThisModule->uwInitialDelay_1ms = 50;
   pstThisModule->uwRunPeriod_1ms = 50;

   return 0;
}

/*-----------------------------------------------------------------------------

   This module is responsible for determining the current class of operation.
   This is where gstOperation.eClassOfOperation is written.
   TODO: Rename to mod_class_of_oper.c
   This module also determines the mode of operation if the current classop
   is Manual. To make this easier, all inspection transfer switches are listed
   in order in the input function enum. So if adding any additional input
   functions, they must be added at the end of the list.
   This is also where gstOperation.eManualMode is written.

   If Car Door Bypass or Hoistway Door Bypass is on, OR in CT, IC or HA, Inspection mode is invalid (A17.1 2.26.1.4.4 (5))
 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static enum en_classop ePrevClassOfOperation = CLASSOP__UNKNOWN;
   eCurrentInspectionMode = GetInspectionMode();
   if ( ( FRAM_EmergencyBitmapReady() || ( eCurrentInspectionMode == MODE_M__CONSTRUCTION ) )
     && ( !GetPreflightFlag() )
     && ( CheckIf_AllStartupSignalsReceived() )
     && ( !gbBackupSync ) )
   {
     if ( eCurrentInspectionMode != MODE_M__NONE )
      {
          SetOperation_ClassOfOp(CLASSOP__MANUAL);
      }
     else if( gbBackupSync )
     {
        SetOperation_ClassOfOp(CLASSOP__MANUAL);
     }
      else //Not on Inspection
      {
          SetOperation_ManualMode( MODE_M__NONE);

         if ( GetLearnModeFlag() )

         {
             SetOperation_ClassOfOp( CLASSOP__SEMI_AUTO);
         }
         else /* Assume CLASSOP__AUTOMATIC */
         {
             SetOperation_ClassOfOp(CLASSOP__AUTO);
         }
      }

      SetOperation_CurrentFloor( GetFloorIndex( GetPosition_PositionCount() ));

      SetOperation_DestinationFloor( GetFloorIndex( GetMotion_Destination() ));
   }

   if ( ePrevClassOfOperation != GetOperation_ClassOfOp() )
   {
      ClearLatchedCarCalls();
      ClearLatchedHallCalls();

      if( GetMotion_RunFlag() )
      {
         SetOperation_MotionCmd( MOCMD__EMERG_STOP);
         if(Param_ReadValue_1Bit(enPARAM1__EnableEStopAlarms))
         {
            SetAlarm(ALM__ESTOP_CLASS_OP);
         }
      }

      /* Fix for bug where if doors were opening when moving from auto to insp, they would continue opening */
      if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
      {
         ResetDoors();
      }

      ePrevClassOfOperation = GetOperation_ClassOfOp();
   }

   if(Learn_GetNeedToLearnFlag() && !Param_ReadValue_1Bit(enPARAM1__BypassTermLimits))
   {
	  Param_WriteValue_1Bit(enPARAM1__BypassTermLimits, 1);
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
