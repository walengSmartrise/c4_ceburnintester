/******************************************************************************
 *
 * @file     mod_heartbeat.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "acceptance.h"
#include "remoteCommands.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Oper_Auto =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define FRIDAY 5
#define SATURDAY 6
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t bPreopeningFlag;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   Returns 1 when preopening is active
 *----------------------------------------------------------------------------*/
uint8_t Auto_GetPreopeningFlag(void)
{
   return bPreopeningFlag;
}
/*----------------------------------------------------------------------------
   Returns 1 if the current mode calls Auto_Recall
   Used to clear recall variables
 *----------------------------------------------------------------------------*/
static uint8_t CheckIfValidRecallMode()
{
   uint8_t bValid = 0;
   if( ( GetOperation_AutoMode() == MODE_A__FIRE1 )
    || ( GetOperation_AutoMode() == MODE_A__FIRE2 )
    || ( GetOperation_AutoMode() == MODE_A__EMS1 )
    || ( GetOperation_AutoMode() == MODE_A__OOS )
    || ( GetOperation_AutoMode() == MODE_A__SEISMC )
    || ( GetOperation_AutoMode() == MODE_A__CW_DRAIL )
    || ( ( GetOperation_AutoMode() == MODE_A__EPOWER )
      && ( ( GetEmergencyPowerCommand() == EPC__PRETRANSFER ) || ( GetEmergencyPowerCommand() == EPC__RECALL ) ) )
    || ( GetOperation_AutoMode() == MODE_A__BATT_RESQ )
    || ( GetOperation_AutoMode() == MODE_A__FLOOD_OP )
    || ( GetOperation_AutoMode() == MODE_A__SABBATH )
    || ( IdleTime_GetParkingFloor_Plus1() && IdleTime_GetParkingDoorCommand() )
    || ( GetOperation_AutoMode() == MODE_A__WANDERGUARD )
    )
   {
      bValid = 1;
   }
   return bValid;
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 50;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_OPER_AUTO_1MS;

   return 0;
}

/*-----------------------------------------------------------------------------

   This module is scheduled to run even if gstOperation.eClassOfOperation is not
   Automatic. If on Automatic, this module updates the mode of operation eAutoMode
   and calls the appropriate mode function such as Auto_Normal() or Auto_Fire().
   These mode functions, therefore, are only called if they are the current
   mode. This module checks for all modes of operations in order of precedence,
   and assume Auto_Normal if no other mode is activated.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   uint32_t timeOfDay = RTC_GetTimeOfDay_TotalSeconds();
   uint8_t bIsFriday = ( RTC_GetDayOfWeek() == FRIDAY );
   uint8_t bIsSaturday = ( RTC_GetDayOfWeek() == SATURDAY );

   EPower_ResetCarRecalledFlag();

    static enum en_mode_auto ePrevAutoMode;

    if ( ( FRAM_EmergencyBitmapReady() )
      && ( GetOperation_ClassOfOp() == CLASSOP__AUTO ) )
    {
        SetOperation_BypassTermLimits( Param_ReadValue_1Bit( enPARAM1__BypassTermLimits ) );

       /* Generic function set the top and bottom limits of the car. */
       Auto_Set_Limits();

       bPreopeningFlag = GetMotion_RunFlag() && PreOpeningCheck();

       /*
        * Acceptance Test Mode
        */
       if( ( ( GetInputValue( enIN_MM )
          || ( GetActiveAcceptanceTest() != NO_ACTIVE_ACCEPTANCE_TEST ) ) )
          && ( !GetEmergencyPowerLockout() ) )
       {
          SetOperation_AutoMode( MODE_A__TEST);

          /* If there is an active acceptance test, disable releveling/correction runs to prevent test interference */
          if( GetActiveAcceptanceTest() != NO_ACTIVE_ACCEPTANCE_TEST )
          {
             gstCurrentModeRules[ MODE_A__TEST ].bAllowedOutsideDoorZone = 1;
          }
          else
          {
             gstCurrentModeRules[ MODE_A__TEST ].bAllowedOutsideDoorZone = 0;
          }

          Auto_Oper( ( ePrevAutoMode != GetOperation_AutoMode() ) );
       }
       /*
        * Earthquake modes
        */
       else if (  ( Param_ReadValue_1Bit( enPARAM1__EnableEarthQuake ) )
              &&  ( GetEmergencyBit( EmergencyBF_CW_Derail_InProgress ) )
              &&  ( !GetEmergencyPowerLockout() ) )
       {
          //SetOperation_AutoMode(MODE_A__CW_DRAIL);
          //Auto_CW_Derail( ( ePrevAutoMode != GetOperation_AutoMode() ) );
          SetOperation_AutoMode(MODE_A__CW_DRAIL);
          Auto_Seismc( ( ePrevAutoMode != GetOperation_AutoMode() ) );
       }
       else if ( ( Param_ReadValue_1Bit( enPARAM1__EnableEarthQuake ) )
              && ( GetEmergencyBit( EmergencyBF_EQ_Seismic_InProgress ) )
              && ( !GetEmergencyPowerLockout() ) )
       {
          SetOperation_AutoMode(MODE_A__SEISMC);
          Auto_Seismc( ( ePrevAutoMode != GetOperation_AutoMode() ) );
       }
       /*
        * Battery Rescue
        */
       else if( ( Rescue_GetActiveFlag() )
             && ( !GetEmergencyPowerLockout() ) )
       {
          SetOperation_AutoMode( MODE_A__BATT_RESQ );
          Auto_Rescue( ( ePrevAutoMode != GetOperation_AutoMode() ) );
       }
       /*
        * Fire Phase 2 In Car
        */
       else if( ( Fire_GetFireService() == FIRE_SERVICE__PHASE2 )
             && ( !GetEmergencyPowerLockout() )
             && ( !EMS_GetEMS2PrioritizedFlag() )
             && ( !Fire_CheckIfFireRecallDelayed() ) )
       {
          SetOperation_AutoMode( MODE_A__FIRE2);
          Fire_Phase_2( (ePrevAutoMode != GetOperation_AutoMode()) );
       }
       /*
        * EMS Phase 2 In Car
        */
       else if( ( EMS_GetEmergencyMedicalService() == EMS_SERVICE__PHASE2 )
             && ( !GetEmergencyPowerLockout() ) )
       {
          SetOperation_AutoMode( MODE_A__EMS2 );
          EMS_Phase_2( (ePrevAutoMode != GetOperation_AutoMode()) );
       }
       /*
        * Emergency Power
        */
       else if ( ( GetEmergencyPowerCommand() ) && ( GetEmergencyPowerCommand() != EPC__RUN_AUTO ) ) {
          SetOperation_AutoMode(MODE_A__EPOWER);
          Auto_Emergency_Power( ( ePrevAutoMode != GetOperation_AutoMode() ) );
       }
       /*
        * Fire Phase 1 (Recall)
        */
       else if( ( Fire_GetFireService() == FIRE_SERVICE__PHASE1 )
             && ( !GetEmergencyPowerLockout() )
             && ( !EMS_GetEMS1PrioritizedFlag() )
             && ( !Fire_CheckIfFireRecallDelayed() ) )
       {
           SetOperation_AutoMode( MODE_A__FIRE1);
           Fire_Phase_1( (ePrevAutoMode != GetOperation_AutoMode()) );
       }
       /*
        * EMS Phase 1 (Recall)
        */
       else if( ( EMS_GetEmergencyMedicalService() == EMS_SERVICE__PHASE1 )
             && ( !GetEmergencyPowerLockout() ) )
       {
          SetOperation_AutoMode( MODE_A__EMS1 );
          EMS_Phase_1( (ePrevAutoMode != GetOperation_AutoMode()) );
       }

       /*
        * Out of Service
        */
       else if ( ( !Param_ReadValue_1Bit(enPARAM1__OOS_Disable) )
              && ( GetInputValue(enIN_OOS) || GetOOSFault() || DL20_GetOOSFlag() ) )
       {
          SetOperation_AutoMode(MODE_A__OOS);
          Auto_OOS( ( ePrevAutoMode != GetOperation_AutoMode() ) );
       }
       /*
        * Flood
        */
       else if( Flood_GetActiveFlag() )
       {
          SetOperation_AutoMode(MODE_A__FLOOD_OP);
          Auto_Flood( ( ePrevAutoMode != GetOperation_AutoMode() ) );
       }
       /*
        * Inp Service
        */
       else if ( GetInputValue( enIN_INDP ) && !SRU_Read_DIP_Switch( enSRU_DIP_A3 ) )
       {
          SetOperation_AutoMode( MODE_A__INDP_SRV);
          Auto_Indp_Srv( ( ePrevAutoMode != GetOperation_AutoMode() ) );
       }

       /*
        * Active Shooter
        */
       else if( GetInputValue(enIN_ACTIVE_SHOOTER_MODE)){
             SetOperation_AutoMode( MODE_A__ACTIVE_SHOOTER_MODE);
             Auto_Active_Shooter_Mode( ( ePrevAutoMode != GetOperation_AutoMode()));
       }
       /*
        * Marshal Mode
        */
      else if ( GetInputValue( enIN_MARSHAL_MODE ) || bMarshalModeStatus() )
      {
        SetOperation_AutoMode( MODE_A__MARSHAL_MODE);
        Auto_Marshal_Mode( ( ePrevAutoMode != GetOperation_AutoMode() ) );
      }
       /*
        * Sabbath
        */

       else if ( GetInputValue(enIN_SABBATH) &&
               ( Param_ReadValue_1Bit( enPARAM1__Sabbath_KeyEnable_Only) || Param_ReadValue_1Bit( enPARAM1__Sabbath_KeyOrTimer_Enable) ) &&
               !SRU_Read_DIP_Switch( enSRU_DIP_A3 ) )
       {
          SetOperation_AutoMode(MODE_A__SABBATH);
          Auto_Sabbath( ( ePrevAutoMode != GetOperation_AutoMode() ) );
       }
       else if( ( ( bIsFriday && (timeOfDay > GetSabbathParamTime( enPARAM24__Sabbath_Start_Time ) ) ) ||
                  ( bIsSaturday && ( timeOfDay < GetSabbathParamTime( enPARAM24__Sabbath_End_Time) ) ) )
             && ( Param_ReadValue_1Bit( enPARAM1__Sabbath_TimerEnable_Only) || Param_ReadValue_1Bit( enPARAM1__Sabbath_KeyOrTimer_Enable) ) &&
             !SRU_Read_DIP_Switch( enSRU_DIP_A3 ) )
       {
          SetOperation_AutoMode(MODE_A__SABBATH);
          Auto_Sabbath( ( ePrevAutoMode != GetOperation_AutoMode() ) );
       }
       /* Emergency dispatch aka wild dispatch currently activates by enabling sabbath mode */
       else if( Dispatch_GetEmergencyDispatchFlag_MRB() && Param_ReadValue_1Bit(enPARAM1__EnableEmergencyDispatch) && !SRU_Read_DIP_Switch( enSRU_DIP_A3  ) )
       {
          SetOperation_AutoMode(MODE_A__SABBATH);
          Auto_Sabbath( ( ePrevAutoMode != GetOperation_AutoMode() ) );
       }
       /*
        * VIP Mode
        */
       else if( VIP_GetSwingService() && Param_ReadValue_1Bit(enPARAM1__VIP_Priority_Dispatching) )
       {
          SetOperation_AutoMode(MODE_A__VIP_MODE);
          Auto_VIP_Mode( ( ePrevAutoMode != GetOperation_AutoMode() ) );
       }
       /*
        * Attendant
        */
       else if ( GetInputValue( enIN_ATTD_ON ) )
       {
          SetOperation_AutoMode(MODE_A__ATTENDANT);

          uint8_t bInitMode = 0;
          if ( (ePrevAutoMode != MODE_A__ATTENDANT) )
          {
             ClearLatchedCarCalls();
             ClearLatchedHallCalls();
             bInitMode = 1;
          }

          Auto_Attendant( bInitMode );
       }
       /*
        * Swing
        */
       else if( GetSwingService() && !Param_ReadValue_1Bit(enPARAM1__VIP_Priority_Dispatching) )
       {
          SetOperation_AutoMode(MODE_A__SWING);
          Auto_Swing( ( ePrevAutoMode != GetOperation_AutoMode() ) );
       }
       else if( GetInputValue( enIN_WANDER_GUARD ) )
       {
          SetOperation_AutoMode(MODE_A__WANDERGUARD);
          Auto_Code_Pink( ( ePrevAutoMode != GetOperation_AutoMode() ) );
       }
       /*
       * Custom Mode
       */
       else if ( (!GetInputValue( enIN_CUSTOM )) !=  (!Param_ReadValue_1Bit(enPARAM1__NC_INPUT_CustomMode)))
       {
          SetOperation_AutoMode( MODE_A__CUSTOM);
          uint8_t bInitMode = ( ePrevAutoMode != MODE_A__CUSTOM )?1:0;
          Auto_InitCustomSettings( bInitMode );
          Auto_Oper( bInitMode );
       }
       else
       {
          SetOperation_AutoMode(MODE_A__NORMAL);

          uint8_t bInitMode = 0;
          if ( ePrevAutoMode == MODE_A__EPOWER )
          {
             Init_ADARequest();
             ClearLatchedCarCalls();
             ClearLatchedHallCalls();
             bInitMode = 1;
          }
          Auto_Oper( ( ePrevAutoMode != GetOperation_AutoMode() ) );
       }


       if ( ( Param_ReadValue_1Bit( enPARAM1__BypassTermLimits ) ) &&
            ( GetOperation_AutoMode() != MODE_A__TEST ) )
       {
          Param_WriteValue_1Bit( enPARAM1__BypassTermLimits, 0 );
       }

       if ( !gstFault.bActiveFault && GetNTS_StopFlag_CTB() && GetPosition_Velocity() )
       {
          SetOperation_MotionCmd( MOCMD__QUICK_STOP);
       }
    }
    else
    {
        SetOperation_AutoMode(MODE_A__NONE);
        bPreopeningFlag = 0;
    }


    if(ePrevAutoMode != GetOperation_AutoMode())
    {
       if(Param_ReadValue_1Bit(enPARAM1__EnableOpModeAlarm))
       {
          SetAlarm(ALM__MODE_CHANGE);
       }
    }
    ePrevAutoMode = GetOperation_AutoMode();

    if(!CheckIfValidRecallMode())
    {
       /* Recall status must be cleared prior to that start of EP or the car will be considered recalled */
       ResetRecallState();
    }
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
