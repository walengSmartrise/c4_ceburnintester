/******************************************************************************
 *
 * @brief    
 * @version  V1.00
 * @date     2, May 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "drive.h"
#include "mod.h"
#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "acceptance.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "fpga_api.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_module gstMod_Safety = { .pfnInit = Init, .pfnRun = Run, };

uint8_t gbRunDisabled;

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

#define MAX_TRACTION_LOSS_TOLERANCE  (60)
#define TRACTION_LOSS_FAILED_TO_MOVE_ZONE_05MM        (QUARTER_INCH)

#define RELAY_DEBOUNCE_TIMES (10)
#define MIN_DEBOUNCE_CYCLES   (3)
#define MIN_DEBOUNCE_CYCLES_SFP_10MS   (100)

#define UNINTENDED_MOV_LATCHING_GSW_AND_LOCK_OPEN_DEBOUNCE_10MS       ( 20 )
#define UNINTENDED_MOV_NONLATCHING_GSW_AND_LOCK_OPEN_DEBOUNCE_10MS    ( 50 )

#define DEBOUNCE_FUSE_SIGNALS_10MS           (25)
#define DEBOUNCE_SAFETY_ZONE_SIGNALS_10MS    (50)

// Module runs at 10ms. 1s / 10ms = 1000ms / 10ms = 100
// Hall locks can wait up to 5 seconds before a hall lock fault is issued
// 5s = 5s * 100
#define TIME_BETWEEN_GSW_AND_HALL_LOCK_10MS                 (700)

#define TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_DEFAULT_10MS   (700)
#define TIME_BETWEEN_GSW_AND_SWING_CONTACTS_DEFAULT_10MS    (500)

#define MAX_TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_10MS       (3000)
#define MAX_TIME_BETWEEN_GSW_AND_SWING_CONTACTS_10MS        (3000)

#define SECOND_SCALING_FACTOR_10MS                          (100)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static uint8_t gucFP_LockClipTime_10ms;
static uint8_t bStopSwitchBypassed;

static uint8_t bBypassCarDoors;
static uint8_t bBypassHallDoors_All;
static uint8_t bBypassHallDoors_OnlyTop;
static uint8_t bBypassHallDoors_OnlyBot;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateBypassDoorFlags(void)
{
   bBypassHallDoors_All = 0;
   bBypassHallDoors_OnlyTop = 0;
   bBypassHallDoors_OnlyBot = 0;
   bBypassCarDoors = 0;
   enum en_mode_manual eManual = GetOperation_ManualMode();
   if( ( eManual == MODE_M__INSP_IC )
    || ( eManual == MODE_M__INSP_CT ) )
   {
      bBypassHallDoors_All = GetInputValue( enIN_BYPH );
      bBypassCarDoors = GetInputValue( enIN_BYPC );
   }
   else if( eManual == MODE_M__INSP_HA_TOP )
   {
      bBypassHallDoors_OnlyTop = 1;
      bBypassCarDoors = 1;
   }
   else if( eManual == MODE_M__INSP_HA_BOTTOM )
   {
      bBypassHallDoors_OnlyBot = 1;
      bBypassCarDoors = 1;
   }
   else if( eManual == MODE_M__INSP_HA )
   {
      bBypassHallDoors_OnlyTop = GetOperation_CurrentFloor() == (GetFP_NumFloors()-1);
      bBypassHallDoors_OnlyBot = GetOperation_CurrentFloor() == 0;
      bBypassCarDoors = 1;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t InVirtualDZ(uint32_t top, uint32_t bottom)
{
   // In a virtual door zone as long as the current position is between the
   // two inches of the current learned floor.
   return ((bottom <= GetPosition_PositionCount()) && (GetPosition_PositionCount() <= top));
}
uint8_t CheckIfInDoorZone(void)
{
   // Get the learned position of the floor we are nearest.
   uint32_t learnedFloorPosition = Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0+GetOperation_CurrentFloor());

   // Add the number of CEDES counts equal to 2.5 inches
   // These signals give the system a virtual door zone magnet
   uint32_t virtualDoorZone_Top    = learnedFloorPosition + TWO_HALF_INCHES;
   uint32_t virtualDoorZone_Bottom = learnedFloorPosition - TWO_HALF_INCHES;

   return (InVirtualDZ(virtualDoorZone_Top,virtualDoorZone_Bottom) ? 1 : 0);

}
/*----------------------------------------------------------------------------
   returns 1 when IC stop switch is bypassed
 *----------------------------------------------------------------------------*/
uint8_t Safety_GetInCarStopBypass()
{
   return bStopSwitchBypassed;
}

/*-----------------------------------------------------------------------------
Check for fault from discrete input
 -----------------------------------------------------------------------------*/
#define FAULT_INPUT_DEBOUNCE_10MS      (25)
static void CheckForFaultInput(void)
{
   static uint8_t ucCounter_10ms;
   if( GetInputValue(enIN_FAULT) )
   {
      if(ucCounter_10ms < FAULT_INPUT_DEBOUNCE_10MS)
      {
         ucCounter_10ms++;
      }
      else
      {
         SetFault(FLT__FAULT_INPUT);
      }
   }
   else
   {
      ucCounter_10ms = 0;
   }
}
/*----------------------------------------------------------------------------
   Checks if the car is moving beyond the position limits set for the run.
 *----------------------------------------------------------------------------*/
#define DEBOUNCE_COUNT_POSITION_LIMIT_FAULT_10MS     (50)//(10)
static void CheckFor_PositionLimitFault()
{
   static uint8_t ucDebounceCounter;
   uint8_t bLimitExceeded = 0;
   // On Auto and not bypassing term limits
   if( (GetOperation_ClassOfOp() == CLASSOP__AUTO) && !GetOperation_BypassTermLimits())
   {
      if(GetPosition_Velocity() > 0)
      {
         if(GetPosition_PositionCount() > GetOperation_PositionLimit_UP())
         {
            bLimitExceeded = 1;
         }
      }
      else if(GetPosition_Velocity() < 0)
      {
         if(GetPosition_PositionCount() < GetOperation_PositionLimit_DN())
         {
            bLimitExceeded = 1;
         }
      }

      if(bLimitExceeded)
      {
         if(ucDebounceCounter >= DEBOUNCE_COUNT_POSITION_LIMIT_FAULT_10MS)
         {
            SetFault(FLT__POSITION_LIMIT);
         }
         else
         {
            ucDebounceCounter++;
         }
      }
      else
      {
         ucDebounceCounter = 0;
      }
   }
}
/*-----------------------------------------------------------------------------

 Check for power going out to the 4 safety zone.

 Otherwise check for power coming back on the 4 zone inputs.

 If any zone input is not powered then log a "safety string" fault.

 -----------------------------------------------------------------------------*/
static void CheckZone_SFM( void )
{
   static uint8_t ucDebounce_10ms;
   if ( !GetInputValue( enIN_ZMIN ) ) // MR Input SFM
   {
      if(++ucDebounce_10ms >= DEBOUNCE_SAFETY_ZONE_SIGNALS_10MS)
      {
         SetFault(FLT__SAFETY_STR_SFM);
         ucDebounce_10ms = 0;
      }
   }
   else
   {
      ucDebounce_10ms = 0;
   }
}

static void CheckZone_SFH( void )
{
   static uint8_t ucDebounce_10ms;
   if ( !GetInputValue( enIN_ZHIN ) ) // MR Input SFH
   {
      if(++ucDebounce_10ms >= DEBOUNCE_SAFETY_ZONE_SIGNALS_10MS)
      {
         SetFault(FLT__SAFETY_STR_SFH);
         ucDebounce_10ms = 0;
      }
   }
   else
   {
      ucDebounce_10ms = 0;
   }
}
static void CheckZone_PIT( void )
{
   static uint8_t ucDebounce_10ms;
   if ( !GetInputValue( enIN_PIT ) )
   {
      if(++ucDebounce_10ms >= DEBOUNCE_SAFETY_ZONE_SIGNALS_10MS)
      {
         SetFault(FLT__SAFETY_STR_PIT);
         ucDebounce_10ms = 0;
      }
   }
   else
   {
      ucDebounce_10ms = 0;
   }
}
static void CheckZone_BUF( void )
{
   static uint8_t ucDebounce_10ms;
   if ( !GetInputValue( enIN_BUF ) )
   {
      if(++ucDebounce_10ms >= DEBOUNCE_SAFETY_ZONE_SIGNALS_10MS)
      {
         SetFault(FLT__SAFETY_STR_BUF);
         ucDebounce_10ms = 0;
      }
   }
   else
   {
      ucDebounce_10ms = 0;
   }
}
static void CheckZone_TFL( void )
{
   static uint8_t ucDebounce_10ms;
   if ( !GetInputValue( enIN_TFL ) )
   {
      if(++ucDebounce_10ms >= DEBOUNCE_SAFETY_ZONE_SIGNALS_10MS)
      {
         SetFault(FLT__SAFETY_STR_TFL);
         ucDebounce_10ms = 0;
      }
   }
   else
   {
      ucDebounce_10ms = 0;
   }
}
static void CheckZone_BFL( void )
{
   static uint8_t ucDebounce_10ms;
   if ( !GetInputValue( enIN_BFL ) )
   {
      if(++ucDebounce_10ms >= DEBOUNCE_SAFETY_ZONE_SIGNALS_10MS)
      {
         SetFault(FLT__SAFETY_STR_BFL);
         ucDebounce_10ms = 0;
      }
   }
   else
   {
      ucDebounce_10ms = 0;
   }
}
static void CheckZone_CT_SW( void )
{
   static uint8_t ucDebounce_10ms;
   if ( !GetInputValue( enIN_CT_SW ) )
   {
      if(++ucDebounce_10ms >= DEBOUNCE_SAFETY_ZONE_SIGNALS_10MS)
      {
         SetFault(FLT__SAFETY_STR_CT_SW);
         ucDebounce_10ms = 0;
      }
   }
   else
   {
      ucDebounce_10ms = 0;
   }
}
static void CheckZone_ESC_HATCH( void )
{
   static uint8_t ucDebounce_10ms;
   if ( !GetInputValue( enIN_ESC_HATCH ) )
   {
      if(++ucDebounce_10ms >= DEBOUNCE_SAFETY_ZONE_SIGNALS_10MS)
      {
         SetFault(FLT__SAFETY_STR_ESC_HATCH);
         ucDebounce_10ms = 0;
      }
   }
   else
   {
      ucDebounce_10ms = 0;
   }
}
static void CheckZone_CAR_SAFE( void )
{
   static uint8_t ucDebounce_10ms;
   if ( !GetInputValue( enIN_CAR_SAFE ) )
   {
      if(++ucDebounce_10ms >= DEBOUNCE_SAFETY_ZONE_SIGNALS_10MS)
      {
         SetFault(FLT__SAFETY_STR_CAR_SAFE);
         ucDebounce_10ms = 0;
      }
   }
   else
   {
      ucDebounce_10ms = 0;
   }
}
static void CheckFor_SafetyZone( void )
{
   CheckZone_SFM();

   CheckZone_SFH();

   CheckZone_PIT();

   CheckZone_BUF();

   CheckZone_TFL();

   CheckZone_BFL();

   /* Ignore CT/COP safeties in construction */
   if( GetOperation_ManualMode() != MODE_M__CONSTRUCTION )
   {
      CheckZone_CT_SW();

      CheckZone_ESC_HATCH();

      CheckZone_CAR_SAFE();
   }
}
/*-----------------------------------------------------------------------------

 Read the In-Car Stop Switch.

 Switch becomes bypassed when on Fire Phase 1 AND car is in motion.

 Once bypassed, switch remains bypassed until no longer on any fire service.

 If switch is open AND not bypassed then log a fault.

 -----------------------------------------------------------------------------*/
#define DEBOUNCE_STOP_SWITCHES_10MS    (50)
static void CheckFor_StopSwitches( void )
{
   static uint8_t ucDebounce_10ms;

   enum en_fire_2_states eState = Fire_GetFireService_2_State();
   enum en_mode_auto eMode = GetOperation_AutoMode();
   int16_t wSpeed = GetMotion_SpeedCommand();
   uint8_t bDoor = CarDoorsClosed();
   uint8_t bAtRecall = Fire_AtPhase2RecallFloor();

   /* IC Stop should also be bypassed when recalling after Phase 2 is switched off
    * A17.1 2013 - 2.27.3.3.4*/
   uint8_t bPh2BypassMode = ( GetOperation_AutoMode() == MODE_A__FIRE2 )
                         && ( Fire_GetFireService_2_State() == FIRE_II__OFF );
   uint8_t bPh2Bypass = bPh2BypassMode && CarDoorsClosed();
   if( Param_ReadValue_1Bit(enPARAM1__DisableBypassICStop) )
   {
      bStopSwitchBypassed = 0;
   }
   else if ( ( GetMotion_SpeedCommand() || GetMotion_RunFlag() )
          && ( ( GetOperation_AutoMode() == MODE_A__FIRE1 ) ||
               ( GetOperation_AutoMode() == MODE_A__EMS1 ) ||
               ( bPh2Bypass ) ) )
   {
      bStopSwitchBypassed = 1;
   }
   else if( ( GetOperation_AutoMode() != MODE_A__FIRE1 )
         && ( GetOperation_AutoMode() != MODE_A__EMS1 )
         && ( !bPh2BypassMode || ( Fire_AtPhase2RecallFloor() && ( GetOperation_AutoMode() == MODE_A__FIRE2 ) ) ) )
   {
      bStopSwitchBypassed = 0;
   }

   if ( !GetInputValue( enIN_ICST ) && !bStopSwitchBypassed )
   {
      if(++ucDebounce_10ms >= DEBOUNCE_STOP_SWITCHES_10MS)
      {
         SetFault(FLT__IC_STOP_SW);
         ucDebounce_10ms = 0;
      }
   }
   else
   {
      ucDebounce_10ms = 0;
   }
}

/*-----------------------------------------------------------------------------

 If the machine room nonBypass safety is low the the governor has tripped. Set a
 governor fault.
 If the governor input is low, set a
 governor fault
 -----------------------------------------------------------------------------*/

static void CheckFor_GovernorLost( void )
{
   static uint16_t uwStartupDelay_10ms = 300;
   static uint8_t ucDebounceTimer_GOV_10ms;
   uint8_t bGOV = GetInputValue( enIN_GOV );
   uint8_t bM120VAC = GetInputValue( enIN_M120VAC );
   /* Wait 3 sec after startup before dropping RGM.
    * For cases with rope gripper FB wired to SFM (enIN_ZMIN) */
   if(uwStartupDelay_10ms)
   {
      uwStartupDelay_10ms--;
   }

   if ( !bGOV && bM120VAC && !uwStartupDelay_10ms )
   {
      if ( ucDebounceTimer_GOV_10ms > 50 )
      {
         SetFault(FLT__GOV);
      }
      else
      {
         ucDebounceTimer_GOV_10ms++;
      }
   }
   else
   {
      ucDebounceTimer_GOV_10ms = 0;
   }
}

/*-----------------------------------------------------------------------------

 If car exceeds contract speed by more than 10% at any time then fault.

 Have a parameter to optionally drop the e-brake when this fault occurs.

 -----------------------------------------------------------------------------*/
#define MAX_DEBOUNCE_LIMIT_OVERSPEED_GENERAL        (100)
static void CheckFor_Overspeed_General( void )
{
   static uint8_t ucDebounce;
   float fSpeedLimit_General;
   fSpeedLimit_General = ( float ) Param_ReadValue_16Bit( enPARAM16__ContractSpeed );
   fSpeedLimit_General *= 1.1f;
   int16_t wSpeedLimit_General = fSpeedLimit_General + 0.5f;
   int16_t wCarSpeed = GetPosition_Velocity();
   uint8_t ucGeneralOverspeedDebounceLimit = Param_ReadValue_8Bit(enPARAM8__GeneralOverspeedDebounceLimit);
   ucGeneralOverspeedDebounceLimit =
         (ucGeneralOverspeedDebounceLimit > MAX_DEBOUNCE_LIMIT_OVERSPEED_GENERAL)
         ? MAX_DEBOUNCE_LIMIT_OVERSPEED_GENERAL:ucGeneralOverspeedDebounceLimit;
   if ( wCarSpeed < ( -1 * wSpeedLimit_General )
         || wCarSpeed > wSpeedLimit_General )
   {
      if( ucDebounce >= ucGeneralOverspeedDebounceLimit )
      {
         SetFault(FLT__OVERSPEED_GENERAL);
      }
      else
      {
         ucDebounce++;
      }
   }
   else
   {
      ucDebounce = 0;
   }
}

/*-----------------------------------------------------------------------------

 If car exceeds 150 fpm in any mode of inspection then fault.

 NOTE: We have had a Canadian inspector who ran the car at 151 fpm and
 expected to see it fault so we can't assume they'll allow a 10%
 margin.

 -----------------------------------------------------------------------------*/
#define MAX_DEBOUNCE_LIMIT_OVERSPEED_INSP        (100)
static void CheckFor_Overspeed_Inspection( void )
{
   static uint8_t ucCounter;
   uint8_t ucOverspeedDebounceLimit = Param_ReadValue_8Bit(enPARAM8__InspectionOverspeedDebounceLimit);
   ucOverspeedDebounceLimit = ( ucOverspeedDebounceLimit >= MAX_DEBOUNCE_LIMIT_OVERSPEED_INSP )
               ? (MAX_DEBOUNCE_LIMIT_OVERSPEED_INSP):(ucOverspeedDebounceLimit);
   if ( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
   {
      if ( GetPosition_Velocity() > MAX_INSP_SPEED
            || GetPosition_Velocity() < (-1 * MAX_INSP_SPEED) )
      {
         if( ucCounter >= ucOverspeedDebounceLimit )
         {
            SetFault(FLT__OVERSPEED_INSPECTION);
         }
         else
         {
            ucCounter++;
         }
      }
      else
      {
         ucCounter = 0;
      }
   }
}

/*-----------------------------------------------------------------------------

 If car exceeds 150 fpm in any mode of operation while any lock or any
 gate switch is open then fault.

 If any lock is open AND any gate switch is open AND the command speed is
 more than 150 fpm then fault immediately.
//todo add exception for fixed CAM
 -----------------------------------------------------------------------------*/
#define MAX_DEBOUNCE_LIMIT_OVERSPEED_DOORS        (100)
static void CheckFor_Overspeed_DoorsOpen( void )
{
   int16_t wCarSpeed;
   int16_t wCmdSpeed;
   uint8_t bAnyGateswitchOpen;
   uint8_t bAnyLockOpen;
   uint8_t bAnyDPMOpen;
   static uint8_t ucCounter;
   uint8_t ucOverspeedDebounceLimit = Param_ReadValue_8Bit(enPARAM8__DoorOpenOverspeedDebounceLimit);
   ucOverspeedDebounceLimit = ( ucOverspeedDebounceLimit >= MAX_DEBOUNCE_LIMIT_OVERSPEED_DOORS )
                            ? (MAX_DEBOUNCE_LIMIT_OVERSPEED_DOORS):(ucOverspeedDebounceLimit);

   bAnyGateswitchOpen = !GetInputValue( enIN_GSWF );
   bAnyLockOpen = !GetInputValue( enIN_LTF ) || !GetInputValue( enIN_LMF ) || !GetInputValue( enIN_LBF );
   //todo review this option taken from existing software which only enables DPM overspeed checks for manual door types
   bAnyDPMOpen = !GetInputValue(enIN_DPM_F) && CheckIfInputIsProgrammed(enIN_DPM_F) && ( GetFP_DoorType(DOOR_FRONT) != DOOR_TYPE__MANUAL );

   if ( GetFP_RearDoors() )
   {
      bAnyGateswitchOpen |= !GetInputValue( enIN_GSWR );
      bAnyLockOpen |= !GetInputValue( enIN_LTR ) || !GetInputValue( enIN_LMR ) || !GetInputValue( enIN_LBR );
      //todo review this option taken from existing software which only enables DPM overspeed checks for manual door types
      bAnyDPMOpen |= !GetInputValue(enIN_DPM_R) && CheckIfInputIsProgrammed(enIN_DPM_R) && ( GetFP_DoorType(DOOR_REAR) != DOOR_TYPE__MANUAL );
   }
   wCarSpeed = GetPosition_Velocity();
   wCmdSpeed = GetMotion_SpeedCommand();
   if ( ( wCarSpeed > 150 )
     || ( wCarSpeed < -150 )
     || ( wCmdSpeed > 150 )
     || ( wCmdSpeed < -150 ) )
   {
      if ( bAnyGateswitchOpen || bAnyLockOpen || bAnyDPMOpen )
      {
         if( ucCounter >= ucOverspeedDebounceLimit )
         {
            if(!GetInputValue( enIN_GSWF ))
            {
               SetFault(FLT__OVERSPEED_DOOR_GSWF);
            }
            else if(!GetInputValue( enIN_LTF ))
            {
               SetFault(FLT__OVERSPEED_DOORS_LFT);
            }
            else if(!GetInputValue( enIN_LMF ))
            {
               SetFault(FLT__OVERSPEED_DOORS_LFM);
            }
            else if(!GetInputValue( enIN_LBF ))
            {
               SetFault(FLT__OVERSPEED_DOORS_LFB);
            }
            else if(!GetInputValue( enIN_GSWR ))
            {
               SetFault(FLT__OVERSPEED_DOORS_GSWR);
            }
            else if(!GetInputValue( enIN_LTR ))
            {
               SetFault(FLT__OVERSPEED_DOORS_LRT);
            }
            else if(!GetInputValue( enIN_LMR ))
            {
               SetFault(FLT__OVERSPEED_DOORS_LRM);
            }
            else if(!GetInputValue( enIN_LBR ))
            {
               SetFault(FLT__OVERSPEED_DOORS_LRB);
            }
            else if(!GetInputValue( enIN_DPM_F ))
            {
               SetFault(FLT__OVERSPEED_DOOR_DPM_F);
            }
            else if(!GetInputValue( enIN_DPM_R ))
            {
               SetFault(FLT__OVERSPEED_DOOR_DPM_R);
            }
         }
         else
         {
            ucCounter++;
         }
      }
      else
      {
         ucCounter = 0;
      }
   }
   else
   {
      ucCounter = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define MAX_DEBOUNCE_LIMIT_OVERSPEED_CONST_10MS        (100)
#define CONSTRUCTION_OVERSPEED_COMMAND_OFFSET_FPM      (25)
static void CheckFor_Overspeed_Construction( void )
{
   static uint8_t ucCounter;
   uint8_t bDisableOverspeed = Param_ReadValue_1Bit(enPARAM1__DisableConstructionOverspeed);
   uint8_t ucOverspeedDebounceLimit = Param_ReadValue_8Bit(enPARAM8__ConstructionOverspeedDebounce_10ms);
   ucOverspeedDebounceLimit = ( ucOverspeedDebounceLimit >= MAX_DEBOUNCE_LIMIT_OVERSPEED_CONST_10MS )
                                  ? ( MAX_DEBOUNCE_LIMIT_OVERSPEED_CONST_10MS )
                                        : ( ucOverspeedDebounceLimit );
   int16_t wDriveSpeed_fpm = GetDriveSpeedFeedback();
   if ( ( GetOperation_ManualMode() == MODE_M__CONSTRUCTION )
         && ( !bDisableOverspeed )
         && ( wDriveSpeed_fpm )
         && ( !Rescue_GetManualTractionRescue() ) )
   {
      int16_t wUpperSpeed_fpm = GetMotion_SpeedCommand() + CONSTRUCTION_OVERSPEED_COMMAND_OFFSET_FPM;
      int16_t wLowerSpeed_fpm = GetMotion_SpeedCommand() - CONSTRUCTION_OVERSPEED_COMMAND_OFFSET_FPM;

      if( ( wDriveSpeed_fpm > wUpperSpeed_fpm )
            || ( wDriveSpeed_fpm < wLowerSpeed_fpm ) )
      {
         if( ucCounter >= ucOverspeedDebounceLimit )
         {
            SetFault(FLT__OVERSPEED_CONST);
         }
         else
         {
            ucCounter++;
         }
      }
      else
      {
         ucCounter = 0;
      }
   }
   else
   {
      ucCounter = 0;
   }
}
/*-----------------------------------------------------------------------------

 If any lock is open outside of a door zone for more than the lock clip time
 parameter (not to exceed 500 ms) then fault.

 -----------------------------------------------------------------------------*/
static void CheckFor_LocksOpen( void )
{
   static uint32_t uiLTF_Timer_10ms;
   static uint32_t uiLMF_Timer_10ms;
   static uint32_t uiLBF_Timer_10ms;
   static uint32_t uiLTR_Timer_10ms;
   static uint32_t uiLMR_Timer_10ms;
   static uint32_t uiLBR_Timer_10ms;

   uint8_t bSwingHallDoor_F = Doors_CheckIfManualSwingDoor(DOOR_FRONT, INVALID_FLOOR);
   uint8_t bSwingHallDoor_R = Doors_CheckIfManualSwingDoor(DOOR_REAR, INVALID_FLOOR);
   uint32_t uiTimeBetweenGSW_and_Lock_10ms_F = TIME_BETWEEN_GSW_AND_HALL_LOCK_10MS;
   uint32_t uiTimeBetweenGSW_and_Lock_10ms_R = TIME_BETWEEN_GSW_AND_HALL_LOCK_10MS;
   if( bSwingHallDoor_F )
   {
      uiTimeBetweenGSW_and_Lock_10ms_F = Param_ReadValue_8Bit(enPARAM8__Swing_GSW_Locks_Timeout_1s) * SECOND_SCALING_FACTOR_10MS;
      if( !uiTimeBetweenGSW_and_Lock_10ms_F )
      {
         uiTimeBetweenGSW_and_Lock_10ms_F = TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_DEFAULT_10MS;
      }
      else if( uiTimeBetweenGSW_and_Lock_10ms_F > MAX_TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_10MS )
      {
         uiTimeBetweenGSW_and_Lock_10ms_F = MAX_TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_10MS;
      }
   }
   if( bSwingHallDoor_R )
   {
      uiTimeBetweenGSW_and_Lock_10ms_R = Param_ReadValue_8Bit(enPARAM8__Swing_GSW_Locks_Timeout_1s) * SECOND_SCALING_FACTOR_10MS;
      if( !uiTimeBetweenGSW_and_Lock_10ms_R )
      {
         uiTimeBetweenGSW_and_Lock_10ms_R = TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_DEFAULT_10MS;
      }
      else if( uiTimeBetweenGSW_and_Lock_10ms_R > MAX_TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_10MS )
      {
         uiTimeBetweenGSW_and_Lock_10ms_R = MAX_TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_10MS;
      }
   }
   uint8_t bSwingLockExpectedClosed_F = Doors_CheckIfSwingLocksExpectedClosed(DOOR_FRONT);
   uint8_t bSwingLockExpectedClosed_R = Doors_CheckIfSwingLocksExpectedClosed(DOOR_REAR);

   /*Locks can be open until CAM is active on decoupled doors*/
   if(!bBypassHallDoors_All)
   {
      if(!bBypassHallDoors_OnlyTop)
      {
         if ( !GetInputValue( enIN_LTF ) && !GetInputValue( enIN_DZ_F ) )
         {
            if ( uiLTF_Timer_10ms > gucFP_LockClipTime_10ms )
            {
               SetFault(FLT__LTF_OPEN);
            }
            else
            {
               uiLTF_Timer_10ms++;
            }
         }
         else if ( !GetInputValue( enIN_LTF ) && GetInputValue( enIN_GSWF ) )
         {
            /*Coupled doors (i.e. automatic)*/
            if( !bSwingHallDoor_F )
            {
               if ( uiLTF_Timer_10ms > uiTimeBetweenGSW_and_Lock_10ms_F )
               {
                  SetFault(FLT__LTF_OPEN);
               }
               else
               {
                  uiLTF_Timer_10ms++;
               }
            }
            /*Decoupled doors and expected closed */
            else if( bSwingLockExpectedClosed_F )
            {
               if ( uiLTF_Timer_10ms > uiTimeBetweenGSW_and_Lock_10ms_F )
               {
                  SetFault(FLT__LTF_OPEN);
               }
               else
               {
                  uiLTF_Timer_10ms++;
               }
            }
            else
            {
               uiLTF_Timer_10ms = 0;
            }
         }
         else
         {
            uiLTF_Timer_10ms = 0;
         }
      }

      //-------------------------------------------------
      if ( !GetInputValue( enIN_LMF ) && !GetInputValue( enIN_DZ_F ) )
      {
         if ( uiLMF_Timer_10ms > gucFP_LockClipTime_10ms )
         {
            SetFault(FLT__LMF_OPEN);
         }
         else
         {
            uiLMF_Timer_10ms++;
         }
      }
      else if ( !GetInputValue( enIN_LMF ) && GetInputValue( enIN_GSWF ) )
      {
            /*Coupled doors (i.e. automatic)*/
            if( !bSwingHallDoor_F )
            {
               if ( uiLMF_Timer_10ms > uiTimeBetweenGSW_and_Lock_10ms_F )
               {
                  SetFault(FLT__LMF_OPEN);
               }
               else
               {
                  uiLMF_Timer_10ms++;
               }
            }
            /*Decoupled doors and expected closed */
            else if( bSwingLockExpectedClosed_F )
            {
               if ( uiLMF_Timer_10ms > uiTimeBetweenGSW_and_Lock_10ms_F )
               {
                  SetFault(FLT__LMF_OPEN);
               }
               else
               {
                  uiLMF_Timer_10ms++;
               }
            }
            else
            {
               uiLMF_Timer_10ms = 0;
            }
      }
      else
      {
         uiLMF_Timer_10ms = 0;
      }

      //-------------------------------------------------
      if(!bBypassHallDoors_OnlyBot)
      {
         if ( !GetInputValue( enIN_LBF ) && !GetInputValue( enIN_DZ_F ) )
         {
            if ( uiLBF_Timer_10ms > gucFP_LockClipTime_10ms )
            {
               SetFault(FLT__LBF_OPEN);
            }
            else
            {
               uiLBF_Timer_10ms++;
            }
         }
         else if ( !GetInputValue( enIN_LBF ) && GetInputValue( enIN_GSWF ) )
         {
            /*Coupled doors (i.e. automatic)*/
            if( !bSwingHallDoor_F )
            {
               if ( uiLBF_Timer_10ms > uiTimeBetweenGSW_and_Lock_10ms_F )
               {
                  SetFault(FLT__LBF_OPEN);
               }
               else
               {
                  uiLBF_Timer_10ms++;
               }
            }
            /*Decoupled doors and expected closed */
            else if( bSwingLockExpectedClosed_F )
            {
               if ( uiLBF_Timer_10ms > uiTimeBetweenGSW_and_Lock_10ms_F )
               {
                  SetFault(FLT__LBF_OPEN);
               }
               else
               {
                  uiLBF_Timer_10ms++;
               }
            }
            else
            {
               uiLBF_Timer_10ms = 0;
            }
         }
         else
         {
            uiLBF_Timer_10ms = 0;
         }
      }

      if ( GetFP_RearDoors() )
      {
         if(!bBypassHallDoors_OnlyTop)
         {
            if ( !GetInputValue( enIN_LTR ) && !GetInputValue( enIN_DZ_R ) )
            {
               if ( uiLTR_Timer_10ms > gucFP_LockClipTime_10ms )
               {
                  SetFault(FLT__LTR_OPEN);
               }
               else
               {
                  uiLTR_Timer_10ms++;
               }
            }
            else if ( !GetInputValue( enIN_LTR ) && GetInputValue( enIN_GSWR ) )
            {
               /*Coupled doors (i.e. automatic)*/
               if( !bSwingHallDoor_R )
               {
                  if ( uiLTR_Timer_10ms > uiTimeBetweenGSW_and_Lock_10ms_R )
                  {
                     SetFault(FLT__LTR_OPEN);
                  }
                  else
                  {
                     uiLTR_Timer_10ms++;
                  }
               }
               /*Decoupled doors and expected closed */
               else if( bSwingLockExpectedClosed_R )
               {
                  if ( uiLTR_Timer_10ms > uiTimeBetweenGSW_and_Lock_10ms_R )
                  {
                     SetFault(FLT__LTR_OPEN);
                  }
                  else
                  {
                     uiLTR_Timer_10ms++;
                  }
               }
               else
               {
                  uiLTR_Timer_10ms = 0;
               }
            }
            else
            {
               uiLTR_Timer_10ms = 0;
            }

         }
         //-------------------------------------------------
         if ( !GetInputValue( enIN_LMR ) && !GetInputValue( enIN_DZ_R ) )
         {
            if ( uiLMR_Timer_10ms > gucFP_LockClipTime_10ms )
            {
               SetFault(FLT__LMR_OPEN);
            }
            else
            {
               uiLMR_Timer_10ms++;
            }
         }
         else if ( !GetInputValue( enIN_LMR ) && GetInputValue( enIN_GSWR ) )
         {
            /*Coupled doors (i.e. automatic)*/
            if( !bSwingHallDoor_R )
            {
               if ( uiLMR_Timer_10ms > uiTimeBetweenGSW_and_Lock_10ms_R )
               {
                  SetFault(FLT__LMR_OPEN);
               }
               else
               {
                  uiLMR_Timer_10ms++;
               }
            }
            /*Decoupled doors and expected closed */
            else if( bSwingLockExpectedClosed_R )
            {
               if ( uiLMR_Timer_10ms > uiTimeBetweenGSW_and_Lock_10ms_R )
               {
                  SetFault(FLT__LMR_OPEN);
               }
               else
               {
                  uiLMR_Timer_10ms++;
               }
            }
            else
            {
               uiLMR_Timer_10ms = 0;
            }
         }
         else
         {
            uiLMR_Timer_10ms = 0;
         }

         //-------------------------------------------------
         if(!bBypassHallDoors_OnlyBot)
         {
            if ( !GetInputValue( enIN_LBR ) && !GetInputValue( enIN_DZ_R ) )
            {
               if ( uiLBR_Timer_10ms > gucFP_LockClipTime_10ms )
               {
                  SetFault(FLT__LBR_OPEN);
               }
               else
               {
                  uiLBR_Timer_10ms++;
               }
            }
            else if ( !GetInputValue( enIN_LBR ) && GetInputValue( enIN_GSWR ) )
            {
               /*Coupled doors (i.e. automatic)*/
               if( !bSwingHallDoor_R )
               {
                  if ( uiLBR_Timer_10ms > uiTimeBetweenGSW_and_Lock_10ms_R )
                  {
                     SetFault(FLT__LBR_OPEN);
                  }
                  else
                  {
                     uiLBR_Timer_10ms++;
                  }
               }
               /*Decoupled doors and expected closed */
               else if( bSwingLockExpectedClosed_R )
               {
                  if ( uiLBR_Timer_10ms > uiTimeBetweenGSW_and_Lock_10ms_R )
                  {
                     SetFault(FLT__LBR_OPEN);
                  }
                  else
                  {
                     uiLBR_Timer_10ms++;
                  }
               }
               else
               {
                  uiLBR_Timer_10ms = 0;
               }
            }
            else
            {
               uiLBR_Timer_10ms = 0;
            }
         }
      }
   }
   else
   {
      uiLTF_Timer_10ms = 0;
      uiLMF_Timer_10ms = 0;
      uiLBF_Timer_10ms = 0;
      uiLTR_Timer_10ms = 0;
      uiLMR_Timer_10ms = 0;
      uiLBR_Timer_10ms = 0;
   }
}

/*-----------------------------------------------------------------------------

 If any closed contact lock is open outside of a door zone for more than the lock clip time
 parameter (not to exceed 500 ms) then fault.

 -----------------------------------------------------------------------------*/
static void CheckFor_CloseContactsOpen( void )
{
   static uint32_t uiTCLF_Timer_10ms;
   static uint32_t uiMCLF_Timer_10ms;
   static uint32_t uiBCLF_Timer_10ms;
   static uint32_t uiTCLR_Timer_10ms;
   static uint32_t uiMCLR_Timer_10ms;
   static uint32_t uiBCLR_Timer_10ms;

   uint8_t bSwingHallDoor_F = Doors_CheckIfManualSwingDoor(DOOR_FRONT, INVALID_FLOOR) && Door_GetSwingClosedContactsProgrammedFlag(DOOR_FRONT); // Suppress closed contact checks if they arent programmed
   uint8_t bSwingHallDoor_R = Doors_CheckIfManualSwingDoor(DOOR_REAR, INVALID_FLOOR) && Door_GetSwingClosedContactsProgrammedFlag(DOOR_REAR); // Suppress closed contact checks if they arent programmed
   uint32_t uiTimeBetweenGSW_and_CloseContact_F = TIME_BETWEEN_GSW_AND_HALL_LOCK_10MS;
   uint32_t uiTimeBetweenGSW_and_CloseContact_R = TIME_BETWEEN_GSW_AND_HALL_LOCK_10MS;
   if( bSwingHallDoor_F )
   {
      uiTimeBetweenGSW_and_CloseContact_F = Param_ReadValue_8Bit(enPARAM8__Swing_Contacts_Timeout_1s) * SECOND_SCALING_FACTOR_10MS;
      if( !uiTimeBetweenGSW_and_CloseContact_F )
      {
         uiTimeBetweenGSW_and_CloseContact_F = TIME_BETWEEN_GSW_AND_SWING_CONTACTS_DEFAULT_10MS;
      }
      else if( uiTimeBetweenGSW_and_CloseContact_F > MAX_TIME_BETWEEN_GSW_AND_SWING_CONTACTS_10MS )
      {
         uiTimeBetweenGSW_and_CloseContact_F = MAX_TIME_BETWEEN_GSW_AND_SWING_CONTACTS_10MS;
      }
   }
   if( bSwingHallDoor_R )
   {
      uiTimeBetweenGSW_and_CloseContact_R = Param_ReadValue_8Bit(enPARAM8__Swing_Contacts_Timeout_1s) * SECOND_SCALING_FACTOR_10MS;
      if( !uiTimeBetweenGSW_and_CloseContact_R )
      {
         uiTimeBetweenGSW_and_CloseContact_R = TIME_BETWEEN_GSW_AND_SWING_CONTACTS_DEFAULT_10MS;
      }
      else if( uiTimeBetweenGSW_and_CloseContact_R > MAX_TIME_BETWEEN_GSW_AND_SWING_CONTACTS_10MS )
      {
         uiTimeBetweenGSW_and_CloseContact_R = MAX_TIME_BETWEEN_GSW_AND_SWING_CONTACTS_10MS;
      }
   }

   if(!bBypassHallDoors_All)
   {
      if( bSwingHallDoor_F )
      {
         if(!bBypassHallDoors_OnlyTop)
         {
            if(!GetInputValue(enIN_TCL_F) && !GetInputValue(enIN_DZ_F) )
            {
               if ( uiTCLF_Timer_10ms > gucFP_LockClipTime_10ms )
               {
                  SetFault(FLT__TCL_F_OPEN);
               }
               else
               {
                  uiTCLF_Timer_10ms++;
               }
            }
            else if(!GetInputValue(enIN_TCL_F) && GetInputValue(enIN_GSWF))
            {
               if (uiTCLF_Timer_10ms > uiTimeBetweenGSW_and_CloseContact_F)
               {
                  SetFault(FLT__TCL_F_OPEN);
               }
               else
               {
                  uiTCLF_Timer_10ms++;
               }
            }
            else
            {
               uiTCLF_Timer_10ms = 0;
            }
         }
         else
         {
            uiTCLF_Timer_10ms = 0;
         }

         if(!GetInputValue(enIN_MCL_F) && !GetInputValue(enIN_DZ_F) )
         {
            if ( uiMCLF_Timer_10ms > gucFP_LockClipTime_10ms )
            {
               SetFault(FLT__MCL_F_OPEN);
            }
            else
            {
               uiMCLF_Timer_10ms++;
            }
         }
         else if(!GetInputValue(enIN_MCL_F) && GetInputValue(enIN_GSWF))
         {
            if (uiMCLF_Timer_10ms > uiTimeBetweenGSW_and_CloseContact_F)
            {
               SetFault(FLT__MCL_F_OPEN);
            }
            else
            {
               uiMCLF_Timer_10ms++;
            }
         }
         else
         {
            uiMCLF_Timer_10ms = 0;
         }

         if(!bBypassHallDoors_OnlyBot)
         {
            if(!GetInputValue(enIN_BCL_F) && !GetInputValue(enIN_DZ_F) )
            {
               if ( uiBCLF_Timer_10ms > gucFP_LockClipTime_10ms )
               {
                  SetFault(FLT__BCL_F_OPEN);
               }
               else
               {
                  uiBCLF_Timer_10ms++;
               }
            }
            else if(!GetInputValue(enIN_BCL_F) && GetInputValue(enIN_GSWF))
            {
               if (uiBCLF_Timer_10ms > uiTimeBetweenGSW_and_CloseContact_F)
               {
                  SetFault(FLT__BCL_F_OPEN);
               }
               else
               {
                  uiBCLF_Timer_10ms++;
               }
            }
            else
            {
               uiBCLF_Timer_10ms = 0;
            }
         }
         else
         {
            uiBCLF_Timer_10ms = 0;
         }
      }
      if(GetFP_RearDoors() && bSwingHallDoor_R)
      {
         if(!bBypassHallDoors_OnlyTop)
         {
            if(!GetInputValue(enIN_TCL_R) && !GetInputValue(enIN_DZ_R) )
            {
               if ( uiTCLR_Timer_10ms > gucFP_LockClipTime_10ms )
               {
                  SetFault(FLT__TCL_R_OPEN);
               }
               else
               {
                  uiTCLR_Timer_10ms++;
               }
            }
            else if(!GetInputValue(enIN_TCL_R) && GetInputValue(enIN_GSWR))
            {
               if (uiTCLR_Timer_10ms > uiTimeBetweenGSW_and_CloseContact_R)
               {
                  SetFault(FLT__TCL_R_OPEN);
               }
               else
               {
                  uiTCLR_Timer_10ms++;
               }
            }
            else
            {
               uiTCLR_Timer_10ms = 0;
            }
         }
         else
         {
            uiTCLR_Timer_10ms = 0;
         }

         if(!GetInputValue(enIN_MCL_R) && !GetInputValue(enIN_DZ_R) )
         {
            if ( uiMCLR_Timer_10ms > gucFP_LockClipTime_10ms )
            {
               SetFault(FLT__MCL_R_OPEN);
            }
            else
            {
               uiMCLR_Timer_10ms++;
            }
         }
         else if(!GetInputValue(enIN_MCL_R) && GetInputValue(enIN_GSWR))
         {
            if (uiMCLR_Timer_10ms > uiTimeBetweenGSW_and_CloseContact_R)
            {
               SetFault(FLT__MCL_R_OPEN);
            }
            else
            {
               uiMCLR_Timer_10ms++;
            }
         }
         else
         {
            uiMCLR_Timer_10ms = 0;
         }

         if(!bBypassHallDoors_OnlyBot)
         {
            if(!GetInputValue(enIN_BCL_R) && !GetInputValue(enIN_DZ_R) )
            {
               if ( uiBCLR_Timer_10ms > gucFP_LockClipTime_10ms )
               {
                  SetFault(FLT__BCL_R_OPEN);
               }
               else
               {
                  uiBCLR_Timer_10ms++;
               }
            }
            else if(!GetInputValue(enIN_BCL_R) && GetInputValue(enIN_GSWR))
            {
               if (uiBCLR_Timer_10ms > uiTimeBetweenGSW_and_CloseContact_R)
               {
                  SetFault(FLT__BCL_R_OPEN);
               }
               else
               {
                  uiBCLR_Timer_10ms++;
               }
            }
            else
            {
               uiBCLR_Timer_10ms = 0;
            }
         }
         else
         {
            uiBCLR_Timer_10ms = 0;
         }
      }
   }
   else
   {
      uiTCLF_Timer_10ms = 0;
      uiMCLF_Timer_10ms = 0;
      uiBCLF_Timer_10ms = 0;
      uiTCLR_Timer_10ms = 0;
      uiMCLR_Timer_10ms = 0;
      uiBCLR_Timer_10ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 If any gate switch is open outside of a door zone for more than the lock clip
 time parameter (not to exceed 500 ms) then fault.

 -----------------------------------------------------------------------------*/
static void CheckFor_GateSwitchOpen( void )
{
   static uint32_t uiGSWF_Timer_10ms, uiGSWR_Timer_10ms;
   uint8_t bBypassJumperOnLockDetection = ( Param_ReadValue_1Bit(enPARAM1__Fire__IgnoreLocksJumpedOnPhase2) )
                                       && ( GetEmergencyBit( EmergencyBF_FirePhaseII_Active ) )
                                       && ( GetOperation_ClassOfOp() == CLASSOP__AUTO );
   uint8_t bSwingHallDoor_F = Doors_CheckIfManualSwingDoor(DOOR_FRONT, INVALID_FLOOR);
   uint8_t bSwingHallDoor_R = Doors_CheckIfManualSwingDoor(DOOR_REAR, INVALID_FLOOR);
   uint32_t uiTimeBetweenGSW_and_Lock_10ms_F = TIME_BETWEEN_GSW_AND_HALL_LOCK_10MS;
   uint32_t uiTimeBetweenGSW_and_Lock_10ms_R = TIME_BETWEEN_GSW_AND_HALL_LOCK_10MS;
   if( bSwingHallDoor_F )
   {
      uiTimeBetweenGSW_and_Lock_10ms_F = Param_ReadValue_8Bit(enPARAM8__Swing_GSW_Locks_Timeout_1s) * SECOND_SCALING_FACTOR_10MS;
      if( !uiTimeBetweenGSW_and_Lock_10ms_F )
      {
         uiTimeBetweenGSW_and_Lock_10ms_F = TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_DEFAULT_10MS;
      }
      else if( uiTimeBetweenGSW_and_Lock_10ms_F > MAX_TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_10MS )
      {
         uiTimeBetweenGSW_and_Lock_10ms_F = MAX_TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_10MS;
      }
   }
   if( bSwingHallDoor_R )
   {
      uiTimeBetweenGSW_and_Lock_10ms_R = Param_ReadValue_8Bit(enPARAM8__Swing_GSW_Locks_Timeout_1s) * SECOND_SCALING_FACTOR_10MS;
      if( !uiTimeBetweenGSW_and_Lock_10ms_R )
      {
         uiTimeBetweenGSW_and_Lock_10ms_R = TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_DEFAULT_10MS;
      }
      else if( uiTimeBetweenGSW_and_Lock_10ms_R > MAX_TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_10MS )
      {
         uiTimeBetweenGSW_and_Lock_10ms_R = MAX_TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_10MS;
      }
   }
   uint8_t bSwingLockExpectedClosed_F = Doors_CheckIfSwingLocksExpectedClosed(DOOR_FRONT) && !Auto_GetPreopeningFlag();
   uint8_t bSwingLockExpectedClosed_R = Doors_CheckIfSwingLocksExpectedClosed(DOOR_REAR) && !Auto_GetPreopeningFlag();

   if( !GetInputValue( enIN_GSWF ) )
   {
      if( !GetInputValue( enIN_DZ_F ) )
      {
         if ( uiGSWF_Timer_10ms > gucFP_LockClipTime_10ms )
         {
            SetFault(FLT__GSWF_OPEN);
         }
         else
         {
            uiGSWF_Timer_10ms++;
         }
      }
      else if( !bBypassJumperOnLockDetection )
      {
         if( GetFP_DoorType(DOOR_FRONT) == DOOR_TYPE__MANUAL )
         {
            if( GetCloseContacts(DOOR_FRONT) && Door_GetSwingClosedContactsProgrammedFlag(DOOR_FRONT) )
            {
               if ( uiGSWF_Timer_10ms > uiTimeBetweenGSW_and_Lock_10ms_F )
               {
                  SetFault(FLT__GSWF_OPEN);
               }
               else
               {
                  uiGSWF_Timer_10ms++;
               }
            }
         }
         else if( ( GetFP_DoorType(DOOR_FRONT) == DOOR_TYPE__AUTOMATIC ) || ( bSwingLockExpectedClosed_F ) )
         {
            if( GetLocks(DOOR_FRONT) )
            {
               if ( uiGSWF_Timer_10ms > uiTimeBetweenGSW_and_Lock_10ms_F )
               {
                  SetFault(FLT__GSWF_OPEN);
               }
               else
               {
                  uiGSWF_Timer_10ms++;
               }
            }
         }
      }
      else
      {
         uiGSWF_Timer_10ms = 0;
      }
   }
   else
   {
      uiGSWF_Timer_10ms = 0;
   }

   if( GetFP_RearDoors() && !GetInputValue( enIN_GSWR ) )
   {
      if( !GetInputValue( enIN_DZ_R ) )
      {
         if ( uiGSWR_Timer_10ms > gucFP_LockClipTime_10ms )
         {
            SetFault(FLT__GSWR_OPEN);
         }
         else
         {
            uiGSWR_Timer_10ms++;
         }
      }
      else if( !bBypassJumperOnLockDetection )
      {
         if( GetFP_DoorType(DOOR_REAR) == DOOR_TYPE__MANUAL )
         {
            if( GetCloseContacts(DOOR_REAR) && Door_GetSwingClosedContactsProgrammedFlag(DOOR_REAR) )
            {
               if ( uiGSWR_Timer_10ms > uiTimeBetweenGSW_and_Lock_10ms_R )
               {
                  SetFault(FLT__GSWR_OPEN);
               }
               else
               {
                  uiGSWR_Timer_10ms++;
               }
            }
         }
         else if( ( GetFP_DoorType(DOOR_REAR) == DOOR_TYPE__AUTOMATIC ) || ( bSwingLockExpectedClosed_R ) )
         {
            if( GetLocks(DOOR_REAR) )
            {
               if ( uiGSWR_Timer_10ms > uiTimeBetweenGSW_and_Lock_10ms_R )
               {
                  SetFault(FLT__GSWR_OPEN);
               }
               else
               {
                  uiGSWR_Timer_10ms++;
               }
            }
         }
      }
      else
      {
         uiGSWR_Timer_10ms = 0;
      }
   }
   else
   {
      uiGSWR_Timer_10ms = 0;
   }
}

/*-----------------------------------------------------------------------------

 If any DPM is open outside of a door zone for more than the lock clip
 time parameter (not to exceed 500 ms) then fault.

 -----------------------------------------------------------------------------*/
static void CheckFor_DPMOpen( void )
{
   static uint32_t uiDPMF_Timer_10ms, uiDPMR_Timer_10ms;
   uint8_t bBypassJumperOnLockDetection = ( Param_ReadValue_1Bit(enPARAM1__Fire__IgnoreLocksJumpedOnPhase2) )
                                       && ( GetEmergencyBit( EmergencyBF_FirePhaseII_Active ) )
                                       && ( GetOperation_ClassOfOp() == CLASSOP__AUTO );
   uint8_t bSwingHallDoor_F = Doors_CheckIfManualSwingDoor(DOOR_FRONT, INVALID_FLOOR);
   uint8_t bSwingHallDoor_R = Doors_CheckIfManualSwingDoor(DOOR_REAR, INVALID_FLOOR);
   uint32_t uiTimeBetweenDPM_and_Lock_10ms_F = TIME_BETWEEN_GSW_AND_HALL_LOCK_10MS;
   uint32_t uiTimeBetweenDPM_and_Lock_10ms_R = TIME_BETWEEN_GSW_AND_HALL_LOCK_10MS;
   if( bSwingHallDoor_F )
   {
      uiTimeBetweenDPM_and_Lock_10ms_F = Param_ReadValue_8Bit(enPARAM8__Swing_GSW_Locks_Timeout_1s) * SECOND_SCALING_FACTOR_10MS;
      if( !uiTimeBetweenDPM_and_Lock_10ms_F )
      {
         uiTimeBetweenDPM_and_Lock_10ms_F = TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_DEFAULT_10MS;
      }
      else if( uiTimeBetweenDPM_and_Lock_10ms_F > MAX_TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_10MS )
      {
         uiTimeBetweenDPM_and_Lock_10ms_F = MAX_TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_10MS;
      }
   }
   if( bSwingHallDoor_R )
   {
      uiTimeBetweenDPM_and_Lock_10ms_R = Param_ReadValue_8Bit(enPARAM8__Swing_GSW_Locks_Timeout_1s) * SECOND_SCALING_FACTOR_10MS;
      if( !uiTimeBetweenDPM_and_Lock_10ms_R )
      {
         uiTimeBetweenDPM_and_Lock_10ms_R = TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_DEFAULT_10MS;
      }
      else if( uiTimeBetweenDPM_and_Lock_10ms_R > MAX_TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_10MS )
      {
         uiTimeBetweenDPM_and_Lock_10ms_R = MAX_TIME_BETWEEN_GSW_AND_HALL_LOCK_SWING_10MS;
      }
   }
   uint8_t bSwingLockExpectedClosed_F = Doors_CheckIfSwingLocksExpectedClosed(DOOR_FRONT) && !Auto_GetPreopeningFlag();
   uint8_t bSwingLockExpectedClosed_R = Doors_CheckIfSwingLocksExpectedClosed(DOOR_REAR) && !Auto_GetPreopeningFlag();
   uint8_t bDPM_F_Valid = CheckIfInputIsProgrammed(enIN_DPM_F);
   uint8_t bDPM_R_Valid = CheckIfInputIsProgrammed(enIN_DPM_R);

   if( bDPM_F_Valid && !GetInputValue( enIN_DPM_F ) )
   {
      if ( !GetInputValue( enIN_DZ_F ) )
      {
         if ( uiDPMF_Timer_10ms > gucFP_LockClipTime_10ms )
         {
            SetFault(FLT__DPMF_OPEN);
         }
         else
         {
            uiDPMF_Timer_10ms++;
         }
      }
      else if( !bBypassJumperOnLockDetection )
      {
         if( GetFP_DoorType(DOOR_FRONT) == DOOR_TYPE__MANUAL )
         {
            if( GetCloseContacts(DOOR_FRONT) && Door_GetSwingClosedContactsProgrammedFlag(DOOR_FRONT) )
            {
               if ( uiDPMF_Timer_10ms > uiTimeBetweenDPM_and_Lock_10ms_F )
               {
                  SetFault(FLT__DPMF_OPEN);
               }
               else
               {
                  uiDPMF_Timer_10ms++;
               }
            }
         }
         else if( ( GetFP_DoorType(DOOR_FRONT) == DOOR_TYPE__AUTOMATIC ) || ( bSwingLockExpectedClosed_F ) )
         {
            if( GetLocks(DOOR_FRONT) )
            {
               if ( uiDPMF_Timer_10ms > uiTimeBetweenDPM_and_Lock_10ms_F )
               {
                  SetFault(FLT__DPMF_OPEN);
               }
               else
               {
                  uiDPMF_Timer_10ms++;
               }
            }
         }
      }
      else
      {
         uiDPMF_Timer_10ms = 0;
      }
   }
   else
   {
      uiDPMF_Timer_10ms = 0;
   }

   if ( GetFP_RearDoors() && bDPM_R_Valid && !GetInputValue( enIN_DPM_R ) )
   {
      if ( !GetInputValue( enIN_DZ_R ) )
      {
         if ( uiDPMR_Timer_10ms > gucFP_LockClipTime_10ms )
         {
            SetFault(FLT__DPMR_OPEN);
         }
         else
         {
            uiDPMR_Timer_10ms++;
         }
      }
      else if( !bBypassJumperOnLockDetection )
      {
         if( GetFP_DoorType(DOOR_REAR) == DOOR_TYPE__MANUAL )
         {
            if( GetCloseContacts(DOOR_REAR) && Door_GetSwingClosedContactsProgrammedFlag(DOOR_REAR) )
            {
               if ( uiDPMR_Timer_10ms > uiTimeBetweenDPM_and_Lock_10ms_R )
               {
                  SetFault(FLT__DPMR_OPEN);
               }
               else
               {
                  uiDPMR_Timer_10ms++;
               }
            }
         }
         else if( ( GetFP_DoorType(DOOR_REAR) == DOOR_TYPE__AUTOMATIC ) || ( bSwingLockExpectedClosed_R ) )
         {
            if( GetLocks(DOOR_REAR) )
            {
               if ( uiDPMR_Timer_10ms > uiTimeBetweenDPM_and_Lock_10ms_R )
               {
                  SetFault(FLT__DPMR_OPEN);
               }
               else
               {
                  uiDPMR_Timer_10ms++;
               }
            }
         }
      }
      else
      {
         uiDPMR_Timer_10ms = 0;
      }
   }
   else
   {
      uiDPMR_Timer_10ms = 0;
   }
}

/*-----------------------------------------------------------------------------

Return =
   DIR__UP  : If the virtual DZ was lost and the car has exceeded the upper position limit
   DIR__DN  : If the virtual DZ was lost and the car has exceeded the upper position limit
   DIR__NONE: If the DZ has not been lost
 -----------------------------------------------------------------------------*/
static enum direction_enum UpdateDoorZoneLossDir(void)
{
   static uint8_t bLastDZ;
   uint8_t bDZ = 0;

   enum direction_enum eDir = DIR__NONE;

   uint32_t uiCurrentPosition = GetPosition_PositionCount();
   uint32_t uiFloorPosition = Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0+GetOperation_CurrentFloor() );
   uint32_t uiDoorZoneLimit_Top = uiFloorPosition + TWO_HALF_INCHES;
   uint32_t uiDoorZoneLimit_Bot = uiFloorPosition - TWO_HALF_INCHES;
   if( uiCurrentPosition > uiDoorZoneLimit_Top )
   {
      bDZ = 0;
      if( bLastDZ && !bDZ )
      {
         eDir = DIR__UP;
      }
   }
   else if( uiCurrentPosition < uiDoorZoneLimit_Bot )
   {
      bDZ = 0;
      if( bLastDZ && !bDZ )
      {
         eDir = DIR__DN;
      }
   }
   else
   {
      bDZ = 1;
   }

   bLastDZ = bDZ;

   return eDir;
}
/*-----------------------------------------------------------------------------

Unintended Movement checks broken into two different checks.
Both check see if a LOCK and GSW are open at the same time,
while the virtual DZ signal is lost. The virtual DZ is defined
as two and a half inches above and below the nearest learned
floor position.
and the car has moved two and a half inches from the
nearest learned floor position:

      1. FLT__UNINTENDED_MOV: If the car leaves DZ in a direction that does not agree with the direction command
      2. FLT__UNINTENDED_LCK_AND_GSW: If the car leaves DZ in the commanded direction.

 -----------------------------------------------------------------------------*/
static uint8_t AnyGateSwitchOpen( void )
{
   uint8_t result = !GetInputValue( enIN_GSWF );

   if ( GetFP_RearDoors() )
   {
      result |= !GetInputValue( enIN_GSWR );
   }

   return result;
}

static uint8_t AnyLockOpen( void )
{
   uint8_t result = !GetInputValue( enIN_LTF ) || !GetInputValue( enIN_LMF )
                  || !GetInputValue( enIN_LBF );

   if ( GetFP_RearDoors() )
   {
      result |= !GetInputValue( enIN_LTR ) || !GetInputValue( enIN_LMR )
                      || !GetInputValue( enIN_LBR );
   }

   return result;
}

static void CheckFor_UnintendedMovement()
{
   static uint8_t ucLockAndGSWOpenDebounce_10ms;
   /* Suppress fault if no valid position has been received.
    * Or if the current floor is unknown.
    * Both are needed to determine virtual DZ. */
   if( ( GetPosition_PositionCount() )
    && ( GetOperation_CurrentFloor() < GetFP_NumFloors() ) )
   {
      uint8_t bOpenLockAndGSW_Debounced_Latching = 0;
      uint8_t bOpenLockAndGSW_Debounced_NonLatching = 0;
      uint8_t bOpenLockAndGSW_New = AnyGateSwitchOpen() && AnyLockOpen();
      if( bOpenLockAndGSW_New )
      {
         if( ucLockAndGSWOpenDebounce_10ms >= UNINTENDED_MOV_NONLATCHING_GSW_AND_LOCK_OPEN_DEBOUNCE_10MS )
         {
            bOpenLockAndGSW_Debounced_NonLatching = 1;
            bOpenLockAndGSW_Debounced_Latching = 1;
         }
         else if( ucLockAndGSWOpenDebounce_10ms >= UNINTENDED_MOV_LATCHING_GSW_AND_LOCK_OPEN_DEBOUNCE_10MS )
         {
            bOpenLockAndGSW_Debounced_Latching = 1;
            ucLockAndGSWOpenDebounce_10ms++;
         }
         else
         {
            ucLockAndGSWOpenDebounce_10ms++;
         }

         //Todo rm test
         if( Param_ReadValue_1Bit(enPARAM1__EnableOpenDoorsAlarm)
          && GetMotion_RunFlag()
          && !Auto_GetPreopeningFlag() )
         {
            SetAlarm(ALM__DOOR_OPEN_TEST);
         }
      }
      else
      {
         ucLockAndGSWOpenDebounce_10ms = 0;
      }

      enum direction_enum eDZLossDir = UpdateDoorZoneLossDir();
      if( !bBypassCarDoors && !(bBypassHallDoors_All || bBypassHallDoors_OnlyTop || bBypassHallDoors_OnlyBot) && eDZLossDir )
      {
         enum direction_enum eCommandDir = GetMotion_Direction();
         if( ( eDZLossDir == eCommandDir )
          && ( bOpenLockAndGSW_Debounced_NonLatching ) )
         {
            SetFault(FLT__UNINTENDED_LCK_AND_GSW);
         }
         else if( ( eDZLossDir != eCommandDir )
               && ( bOpenLockAndGSW_Debounced_Latching ) )
         {
            SetFault(FLT__UNINTENDED_MOV);
         }
      }
      /* Exception to flag UM when car doors or hall doors are bypassed */
      else if( eDZLossDir && !GetMotion_RunFlag() )
      {
         enum direction_enum eCommandDir = GetMotion_Direction();
         if( ( eDZLossDir == eCommandDir )
          && ( bOpenLockAndGSW_Debounced_NonLatching ) )
         {
            SetFault(FLT__UNINTENDED_LCK_AND_GSW);
         }
         else if( ( eDZLossDir != eCommandDir )
               && ( bOpenLockAndGSW_Debounced_Latching ) )
         {
            SetFault(FLT__UNINTENDED_MOV);
         }
      }
   }
   else
   {
      ucLockAndGSWOpenDebounce_10ms = 0;
   }
}

/*-----------------------------------------------------------------------------
   Check M Contactor and B contactor for invalid state
 -----------------------------------------------------------------------------*/
static void CheckFor_ContactorFeedback( void )
{
   static uint16_t uwMMC_Delay_10ms, uwMB2C_Delay_10ms;
   if(!Rescue_GetManualTractionRescue())
   {
      uint8_t ucSFM = GetInputValue( enIN_MSFM );
      uint8_t bMMC = GetInputValue( enIN_MMC );
      uint8_t bTestBypass = (
            (GetActiveAcceptanceTest() == ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_FRONT) ||
            (GetActiveAcceptanceTest() == ACCEPTANCE_TEST_UNINTENDED_MOVEMENT_REAR)
      );

      if ( ucSFM ^ !bMMC )
      {
         if ( (uwMMC_Delay_10ms > 500) && !bTestBypass )
         {
            en_faults eFault = ( bMMC ) ? FLT__M_CONT_STUCK_LO:FLT__M_CONT_STUCK_HI;
            SetFault(eFault);
         }
         else
         {
            uwMMC_Delay_10ms++;
         }
      }
      else
      {
         uwMMC_Delay_10ms = 0;
      }
   }

   //-----------------------------------------------
   if( Param_ReadValue_1Bit(enPARAM1__EnableSecondaryBrake) )
   {
      uint8_t bRGM = GetInputValue( enIN_MRGM );//EB2
      uint8_t bRGP = GetInputValue( enIN_MRGP );//EB1
      uint8_t bEB = bRGM && bRGP; // Expected EB board output
      uint8_t bMB2C = GetInputValue( enIN_MBC2 );
      uint8_t bActiveBrakeTest = ( GetOperation_AutoMode() == MODE_A__TEST )
                              && ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
                              && ( ( GetActiveAcceptanceTest() == ACCEPTANCE_TEST_SLIDE_DISTANCE )
                                || ( GetActiveAcceptanceTest() == ACCEPTANCE_TEST_EBRAKE_SLIDE )
                                || ( GetActiveAcceptanceTest() == ACCEPTANCE_TEST_BRAKEBOARD_FEEDBACK ) );
      bActiveBrakeTest |= Test_UM_GetTestActiveFlag();
      if( ( bEB ^ bMB2C ) && !bActiveBrakeTest )
      {
         if ( uwMB2C_Delay_10ms > 500 )
         {
            SetFault(FLT__B2_CONT_STUCK_HI+!bMB2C);
         }
         else
         {
            uwMB2C_Delay_10ms++;
         }
      }
      else
      {
         uwMB2C_Delay_10ms = 0;
      }
   }
}
/*-----------------------------------------------------------------------------
 In order to check for speed dev the first thing that is check is if the car
 is traveling above a specific threshold. At low speeds speed dev is not checked.
 The speed that is checked here is the actual speed the car is traveling is checked
 here, this is the feedback from cedes.

 If the car is moving and its speed is above the threshold then two bounds are
 calculated. these are an upper and lower bound which is calculated by looking
 at the commanded speed. The following formula determines the upper and lower
 bounds.

 bound = +/- [ (1 - CMD_Speed/contractSpeed) + (X * constratSpeed) ] + CMD_Speed

 where X is the percent that determions the offset.
 The first term when the car is at threhiold will make the bounds higher.
 As the CMD speed increases the first term will go away leaving the bound to be
 the offset value. The reasoning here is that the motor is able to better
 control the car at higher speeds.

 If the current speed of the car is outside of either of the bounds then a timer
 will start. If the timer expires then the car will issue a speed dev fault.
 -----------------------------------------------------------------------------*/
static void CheckFor_SpeedDev( void )
{
   static uint16_t uwTimer_10ms;

   // Check speed deviation only when in in auto operation
   if ( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
         && !CheckIfQuickStop() )
   {
      uint16_t contractSpeed = Param_ReadValue_16Bit( enPARAM16__ContractSpeed );
      uint16_t tripThreshold = Param_ReadValue_16Bit( enPARAM16__SpeedDev_Threshold );
      uint16_t speedDevTimer_10ms = Param_ReadValue_16Bit( enPARAM16__SpeedDev_Timeout_10ms );
      uint16_t offsetPercent = Param_ReadValue_16Bit( enPARAM16__SpeedDev_OffsetPercent );

      // Do not check for speed dev when the car is traveling at low speeds
      // It is hard for motors to maintain lower speeds.
      uint16_t velocity = ( GetPosition_Velocity() >= 0 ) ? GetPosition_Velocity() : -GetPosition_Velocity();
      uint16_t cmdSpeed = ( GetMotion_SpeedCommand()   >= 0 ) ? GetMotion_SpeedCommand()   : -GetMotion_SpeedCommand();

      if ( tripThreshold < velocity )
      {
         float lhs = ( 1.0 - ( ( float ) cmdSpeed / ( float ) contractSpeed ) );

         float delta = ( lhs + ( float ) offsetPercent / 100.0 ) * ( float ) cmdSpeed;
         float upperBound = cmdSpeed + delta;
         float lowerBound = cmdSpeed - delta;
         if ( upperBound < velocity )
         {
            if ( uwTimer_10ms >= speedDevTimer_10ms )
            {
               SetFault(FLT__SPEED_DEV);
            }
            else
            {
               uwTimer_10ms++;
            }
         }
         else if ( lowerBound > velocity )
         {
            if ( uwTimer_10ms >= speedDevTimer_10ms )
            {
               SetFault(FLT__SPEED_DEV);
            }
            else
            {
               uwTimer_10ms++;
            }
         }
         else
         {
            uwTimer_10ms = 0;
         }
      }
      else if ( velocity == 0 )
      {
         uwTimer_10ms = 0;
      }
   }
}
/*-----------------------------------------------------------------------------

 Monitor the encoder speed. If the encoder speed is non-zero but the FPM
 feedback indicates no motion for more than (traction loss parameter) time
 then fault. This fault requires pressing the "Traction Loss Reset" button
 to clear.
 If drive commanded to move, but cedes speed differs from the
 drive feedback speed by greater than the offset percentage, set traction loss fault

 -----------------------------------------------------------------------------*/
static void CheckFor_TractionLoss( void )
{
   static uint32_t uiTrcLossCounter_10ms;

   float fCarSpeed = GetPosition_Velocity();
   if( Param_ReadValue_1Bit(enPARAM1__TestTrcLoss) )
   {
      fCarSpeed = 0;
   }
   float fDriveSpeed = GetDriveSpeedFeedback();

   uint16_t uwOffsetPercent = Param_ReadValue_16Bit( enPARAM16__TractionLoss_OffsetPercent );
   uint16_t uwTrcLossTimeout_10ms = Param_ReadValue_16Bit( enPARAM16__TractionLoss_Timeout_10ms );
   int32_t iSpeedThreshold = Param_ReadValue_16Bit( enPARAM16__TractionLoss_Threshold );
   if(uwOffsetPercent >= MAX_TRACTION_LOSS_TOLERANCE)
   {
      uwOffsetPercent = MAX_TRACTION_LOSS_TOLERANCE;
   }
   float fUpperBound = fCarSpeed * ( 100.0f + uwOffsetPercent ) / 100.0f;
   float fLowerBound = fCarSpeed * ( 100.0f - uwOffsetPercent ) / 100.0f;
   if( fCarSpeed >= 0 )
   {
      if( ( ( fDriveSpeed >= (float) iSpeedThreshold ) || ( fDriveSpeed <= (float) -1*iSpeedThreshold ) )
       && ( fDriveSpeed > fUpperBound || fDriveSpeed < fLowerBound ) )
      {
         if( uiTrcLossCounter_10ms > uwTrcLossTimeout_10ms )
         {
            SetFault(FLT__TRACTION_LOSS);
         }
         else
         {
            uiTrcLossCounter_10ms++;
         }
      }
      else
      {
         uiTrcLossCounter_10ms = 0;
      }
   }
   else if( fCarSpeed < 0 )
   {
      if( ( ( fDriveSpeed >= (float) iSpeedThreshold ) || ( fDriveSpeed <= (float) -1*iSpeedThreshold ) )
       && ( fDriveSpeed < fUpperBound || fDriveSpeed > fLowerBound ) )
      {
         if( uiTrcLossCounter_10ms > uwTrcLossTimeout_10ms )
         {
            SetFault(FLT__TRACTION_LOSS);
         }
         else
         {
            uiTrcLossCounter_10ms++;
         }
      }
      else
      {
         uiTrcLossCounter_10ms = 0;
      }
   }

   /* Additional case for test where mechanic will drop the rope gripper and run the car.
    * The car will not move, but the speed command and encoder speed will be nonzero.
    * To compensate, if the car fails to move outside a quarter inch zone of its acceleration start
    * position, X amount of time from when it first commanded a nonzero speed, then also trip traction loss. */
   static uint16_t uwAltTractionLossTimer_10ms;
   uint32_t uiAltTractionLossUpperPos_05mm = Motion_GetStartPosition_Accel() + TRACTION_LOSS_FAILED_TO_MOVE_ZONE_05MM;
   uint32_t uiAltTractionLossLowerPos_05mm = Motion_GetStartPosition_Accel() - TRACTION_LOSS_FAILED_TO_MOVE_ZONE_05MM;
   if( ( GetMotion_SpeedCommand() )
    && ( GetPosition_PositionCount() < uiAltTractionLossUpperPos_05mm )
    && ( GetPosition_PositionCount() > uiAltTractionLossLowerPos_05mm )
    )
   {
      if( ++uwAltTractionLossTimer_10ms > uwTrcLossTimeout_10ms )
      {
         SetFault(FLT__TRACTION_LOSS);
      }
   }
   else
   {
      uwAltTractionLossTimer_10ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 Read all the inspection transfer inputs.

 Verify that the current Mode of Operation does not violate the hierarchy of
 inspection modes.

 Fault if any invalid combination of inspection transfer inputs is active.

 See NONMANDATORY APPENDIX R of the A17.1 code for hierarchy of inspection
 modes.

 -----------------------------------------------------------------------------*/
static void CheckFor_InvalidInspectionMode( void )
{
   /* Will add this later. It is currently done in mod_mode_of_oper.c in MCUA */
}
/*-----------------------------------------------------------------------------

 Check if the Hoistway Door Bypass Switch input or Car Door Bypass Switch
 input is powered but the car is not on CT or IC inspection. In such a case
 fault.

 -----------------------------------------------------------------------------*/
#define DEBOUNCE_BYPASS_SWITCHES_10MS     (50)
static void CheckFor_BypassSwitchesOn( void )
{
   static uint8_t ucCounterH_10ms;
   static uint8_t ucCounterC_10ms;
   uint8_t bIgnoreBypassSwitchHA =
         ( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
         && ( GetOperation_ManualMode() == MODE_M__INSP_CT
               || GetOperation_ManualMode() == MODE_M__INSP_IC );
   if ( GetInputValue( enIN_BYPH ) && !bIgnoreBypassSwitchHA )
   {
      if( ucCounterH_10ms >= DEBOUNCE_BYPASS_SWITCHES_10MS )
      {
         SetFault(FLT__HA_BYPASS);
      }
      else
      {
         ucCounterH_10ms++;
      }
   }
   else
   {
      ucCounterH_10ms = 0;
   }


   uint8_t bIgnoreBypassSwitchCar =
         ( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
         && ( GetOperation_ManualMode() == MODE_M__INSP_CT
               || GetOperation_ManualMode() == MODE_M__INSP_IC );
   if ( GetInputValue( enIN_BYPC ) && !bIgnoreBypassSwitchCar )
   {
      if( ucCounterC_10ms >= DEBOUNCE_BYPASS_SWITCHES_10MS )
      {
         SetFault(FLT__CAR_BYPASS);
      }
      else
      {
         ucCounterC_10ms++;
      }
   }
   else
   {
      ucCounterC_10ms = 0;
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t GetNeedToDropEBrakeFlag(void)
{
   uint8_t bDropBrake = 0;
   if( gstFault.bActiveFault )
   {
      if( GetFault_ByNumber(FLT__GOV) || GetFault_ByNumber(FLT__GOV_L) )
      {
         bDropBrake = 1;
      }
      else if( GetFault_ByNumber(FLT__UNINTENDED_MOV) || GetFault_ByNumber(FLT__UNINTENDED_MOV_L) )
      {
         bDropBrake = 1;
      }
      else if( ( GetFault_ByNumber(FLT__OVERSPEED_GENERAL) || GetFault_ByNumber(FLT__OVERSPEED_GENERAL_L) )
            && ( Param_ReadValue_1Bit( enPARAM1__EBrakeOnOverspeed ) ) )
      {
         bDropBrake = 1;
      }
      else if( Brake_GetDropEBFlag() )
      {
         bDropBrake = 1;
      }
   }
   return bDropBrake;
}
/*-----------------------------------------------------------------------------

 Monitor the status of RGP and DZP. If the FPGA drops them both, that means
 it detected a fault that requires E Brake Reset. The FPGA has no way of
 saving the state of its fault to NVM, so latch it in software to require
 a reset on the E Brake Reset pushbutton.
 RGP marked EB1 in silkscreen.
 -----------------------------------------------------------------------------*/
static void CheckFor_FPGA_E_Brake( void )
{
   static uint8_t ucTimer_10ms;
   static uint16_t uwStartupCountdown_10ms = 200;
   if(uwStartupCountdown_10ms)
   {
      uwStartupCountdown_10ms--;
   }
   else
   {
      // When there is a secondary break the the rgp and dzp relays will drop at the
      // end of a run. So we should not issue a fault in this case.
      if ( !GetInputValue( enIN_MRGP ) && !GetNeedToDropEBrakeFlag() ) // EB1
      {
         // Only issue the fault when teher is no secondary break
         if ( !Param_ReadValue_1Bit(enPARAM1__EnableSecondaryBrake) )
         {
            if ( ucTimer_10ms > 100 )
            {
               SetFault(FLT__EB1_DROP);
            }
            else
            {
               ucTimer_10ms++;
            }
         }
      }
      else
      {
         ucTimer_10ms = 0;
      }
   }
}

/*-----------------------------------------------------------------------------

 Cheks the M_120 input. If it is low for more than a second then the car will
 fault out.

 -----------------------------------------------------------------------------*/
static void CheckFor_120VACFault( void )
{
   static uint8_t timer_120VAC_10ms = 0;

   // Check if 120VAC is low
   if( !GetInputValue( enIN_M120VAC ) )
   {
      if(timer_120VAC_10ms > 200)
      {
         SetFault(FLT__120VAC);
      }
      else
      {
         timer_120VAC_10ms++;
      }
   }
   else
   {
      timer_120VAC_10ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckFor_FireStopSwitch( void )
{
   static uint8_t ucDebounce_10ms;
   if ( !GetInputValue( enIN_FIRE_STOP_SW ) )
   {
      if(++ucDebounce_10ms >= DEBOUNCE_STOP_SWITCHES_10MS)
      {
         SetFault(FLT__FIRE_STOP_SW);
         ucDebounce_10ms = 0;
      }
   }
   else
   {
      ucDebounce_10ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 If using ropegripper, RGM drop will trigger faults.
 If using a secondary brake instead, the relays will not be checked.
 -----------------------------------------------------------------------------*/
static void CheckFor_RopeGripperDropped( void )
{
   static uint8_t ucTimer_RGM;
   uint8_t bSecondaryBrake = Param_ReadValue_1Bit( enPARAM1__EnableSecondaryBrake );

   //------------------------------
   if(!bSecondaryBrake)// If rope gripper type ebrake
   {
      // If RGM is low issue a fault
      if( !SRU_Read_ExtInput(eEXT_Input_MRGM) ) //EB2
      {
         if ( ucTimer_RGM > RELAY_DEBOUNCE_TIMES )
         {
            SetFault(FLT__EB2_DROPPED);
         }
         else
         {
            ucTimer_RGM++;
         }
      }
      else
      {
         ucTimer_RGM = 0;
      }
   }
}
/*-----------------------------------------------------------------------------
   Checks that relay command matches the relay monitoring
   Checks that SFP is not low
 -----------------------------------------------------------------------------*/
static void CheckFor_SFP_FeedbackFault()
{
   static uint8_t ucTimer_SFP1;
   static uint8_t ucTimer_SFP2;

   // Check SFP
   uint8_t ucSFP_DebounceLimit = Param_ReadValue_8Bit( enPARAM8__SFP_DebounceLimit );
   if(ucSFP_DebounceLimit < MIN_DEBOUNCE_CYCLES_SFP_10MS)
   {
      ucSFP_DebounceLimit = MIN_DEBOUNCE_CYCLES_SFP_10MS;
   }
   uint8_t bCSFP = SRU_Read_ExtInput( eEXT_Input_CSFP );
   uint8_t bMSFP = SRU_Read_ExtInput( eEXT_Input_MSFP );
   if( bCSFP ^ bMSFP )
   {
      if ( ucTimer_SFP2 > RELAY_DEBOUNCE_TIMES )
      {
         en_faults eFault = (bMSFP) ? FLT__SFP_STUCK_HI:FLT__SFP_STUCK_LO;
         SetFault(eFault);
      }
      else
      {
         ucTimer_SFP2++;
      }
   }
   else
   {
      ucTimer_SFP2 = 0;
   }

   if(!bMSFP)
   {
      if ( ucTimer_SFP1 > ucSFP_DebounceLimit )
      {
         SetFault(FLT__SFP_DROPPED);
      }
      else
      {
         ucTimer_SFP1++;
      }
   }
   else
   {
      ucTimer_SFP1 = 0;
   }
}
/*-----------------------------------------------------------------------------
   Checks that relay command matches the relay monitoring
 -----------------------------------------------------------------------------*/
static void CheckFor_SFM_FeedbackFault()
{
   static uint8_t ucTimer_SFM;
   uint8_t bCSFM = SRU_Read_ExtOutput( eExt_Output_CSFM );
   uint8_t bMSFM = SRU_Read_ExtInput( eEXT_Input_MSFM );
   if( bCSFM ^ bMSFM )
   {
      if ( ucTimer_SFM > RELAY_DEBOUNCE_TIMES )
      {
         SetFault(FLT__SFM_STUCK);
      }
      else
      {
         ucTimer_SFM++;
      }
   }
   else
   {
      ucTimer_SFM = 0;
   }
}
/*-----------------------------------------------------------------------------
   Checks that relay command matches the relay monitoring
   EB1
 -----------------------------------------------------------------------------*/
static void CheckFor_RGP_FeedbackFault()
{
   static uint8_t ucTimer_RGP;
   uint8_t bCRGP = SRU_Read_ExtInput( eEXT_Input_CRGP );
   uint8_t bMRGP = SRU_Read_ExtInput( eEXT_Input_MRGP );
   if( bCRGP ^ bMRGP )
   {
      if ( ucTimer_RGP > RELAY_DEBOUNCE_TIMES )
      {
         SetFault(FLT__EB1_STUCK);
      }
      else
      {
         ucTimer_RGP++;
      }
   }
   else
   {
      ucTimer_RGP = 0;
   }
}
/*-----------------------------------------------------------------------------
   Checks that relay command matches the relay monitoring
   EB2
 -----------------------------------------------------------------------------*/
static void CheckFor_RGM_FeedbackFault()
{
   static uint8_t ucTimer_RGM;
   uint8_t bCRGM = SRU_Read_ExtOutput( eExt_Output_CRGM );
   uint8_t bMRGM = SRU_Read_ExtInput( eEXT_Input_MRGM );
   if( bCRGM ^ bMRGM )
   {
      if ( ucTimer_RGM > RELAY_DEBOUNCE_TIMES )
      {
         SetFault(FLT__EB2_STUCK);
      }
      else
      {
         ucTimer_RGM++;
      }
   }
   else
   {
      ucTimer_RGM = 0;
   }
}
/*-----------------------------------------------------------------------------
   Checks that relay command matches the relay monitoring
 -----------------------------------------------------------------------------*/
#define RGB_LOW_FEEDBACK_DEBOUNCE_10MS        (100)
static void CheckFor_RGB_FeedbackFault()
{
   static uint8_t ucTimer_RGBP1;
   static uint8_t ucTimer_RGBM1;
   static uint8_t ucTimer_RGBP2;
   static uint8_t ucTimer_RGBM2;
   // Check RGB CPLD EB3
   uint8_t bCRGBP = SRU_Read_ExtInput( eEXT_Input_CRGB );
   uint8_t bMRGBP = SRU_Read_ExtInput( eEXT_Input_MRGBP );
   if( bCRGBP ^ bMRGBP )
   {
      if ( ucTimer_RGBP1 > RELAY_DEBOUNCE_TIMES )
      {
         SetFault(FLT__EB3_STUCK_LO+bMRGBP);
      }
      else
      {
         ucTimer_RGBP1++;
      }
   }
   else
   {
      ucTimer_RGBP1 = 0;
   }

   if( !GetPreflightFlag()
         && bMRGBP )
   {
      if( ucTimer_RGBP2 > RGB_LOW_FEEDBACK_DEBOUNCE_10MS )
      {
         SetFault(FLT__EB3_STUCK_HI);
      }
      else
      {
         ucTimer_RGBP2++;
      }
   }
   else
   {
      ucTimer_RGBP2 = 0;
   }
   //------------------------------------------------------------------

   // Check RGB MCU EB4
   uint8_t bCRGBM = SRU_Read_ExtOutput( eExt_Output_CRGBM );
   uint8_t bMRGBM = SRU_Read_ExtInput( eEXT_Input_MRGBM );
   if( bCRGBM ^ bMRGBM )
   {
      if ( ucTimer_RGBM1 > RELAY_DEBOUNCE_TIMES )
      {
         en_faults eFault = (bMRGBM) ? FLT__EB4_STUCK_HI:FLT__EB4_STUCK_LO;
         SetFault(eFault);
      }
      else
      {
         ucTimer_RGBM1++;
      }
   }
   else
   {
      ucTimer_RGBM1 = 0;
   }

   if( !GetPreflightFlag()
         && bMRGBM )
   {
      if ( ucTimer_RGBM2 > RGB_LOW_FEEDBACK_DEBOUNCE_10MS )
      {
         SetFault(FLT__EB4_STUCK_HI);
      }
      else
      {
         ucTimer_RGBM2++;
      }
   }
   else
   {
      ucTimer_RGBM2 = 0;
   }
}

/*-----------------------------------------------------------------------------
   If both EB4 & EB3 are picked when not in preflight set a POR fault
 -----------------------------------------------------------------------------*/
#define EB_BYPASSED_DEBOUNCE_10MS      (20U)
static void CheckFor_EB_Bypassed()
{
   static uint8_t ucCounter;
   static uint8_t bLatchFault;// Fault should be latched until reset.
   if( bLatchFault )
   {
      SetFault(FLT__EB_BYPASSED);
   }
   else if( !GetPreflightFlag() )
   {
      uint8_t bBypass_CPLD = GetInputValue( enIN_MRGBP );
      uint8_t bBypass_MR = GetInputValue( enIN_MRGBM );
      if( bBypass_CPLD && bBypass_MR )
      {
         if( ucCounter >= EB_BYPASSED_DEBOUNCE_10MS )
         {
            ucCounter = 0;
            bLatchFault = 1;
         }
      }
      else
      {
         ucCounter = 0;
      }
   }
}

/*-----------------------------------------------------------------------------
   EB2
 -----------------------------------------------------------------------------*/
static void Control_RGM_Relay()
{
   uint8_t bEnergizeRelays_EBrake = 0;

   uint8_t bEBrakeEnabled = Param_ReadValue_1Bit( enPARAM1__EnableSecondaryBrake );
   if( GetNeedToDropEBrakeFlag() )
   {
      bEnergizeRelays_EBrake = 0;
   }
   else if( bEBrakeEnabled )
   {
      if( gstFault.bActiveFault )
      {
         bEnergizeRelays_EBrake = 0;
      }
      else if( GetPickB2Flag() || GetAcceptanceContactorPickCommand(CONTACTOR_SEL__B2) || Test_UM_GetPickB2Flag() )
      {
         bEnergizeRelays_EBrake = 1;
      }
   }
   else if( GetFault_ByRange(FLT__OVERSPEED_UETS_1,FLT__OVERSPEED_DETSL_8) &&
            Param_ReadValue_1Bit(enPARAM1__EBrakeOnETSandETSL) )
   {
      bEnergizeRelays_EBrake = 0;
   }
   else
   {
      bEnergizeRelays_EBrake = 1;
   }

   /* Surrender control to CPLD during preflight */
   uint8_t bPreflightPickCmd = 0;
   if( ( GetPreflightFlag() )
    && ( FPGA_GetPreflightStatus(FPGA_LOC__MRA) != PREFLIGHT_STATUS__INACTIVE ) )
   {
      if( FPGA_GetPreflightCommand(FPGA_LOC__MRA) != PREFLIGHT_CMD__DROP_GRIPPER )
      {
         bPreflightPickCmd = 1;
      }
      bEnergizeRelays_EBrake = bPreflightPickCmd;
   }


   SRU_Write_ExtOutput( eExt_Output_CRGM, bEnergizeRelays_EBrake );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Control_SFM_Relay()
{
   uint8_t bEnergizeRelays_Safety = 0;
   if ( !gstFault.bActiveFault )
   {
      if ( GetPickMFlag() || GetAcceptanceContactorPickCommand( CONTACTOR_SEL__M ) || Drive_GetMotorLearnFlag() )
      {
         bEnergizeRelays_Safety = 1;
      }
   }

   SRU_Write_ExtOutput( eExt_Output_CSFM, bEnergizeRelays_Safety );
}
/*-----------------------------------------------------------------------------
 Marked EB4 in silk
 Commanded by CPLD during preflight

 -----------------------------------------------------------------------------*/
static void Control_Bypass_Relay()
{
   uint8_t bPickRelay = 0;
   static uint8_t bPreflightPickCmd;
   PreflightStatus eStatus = FPGA_GetPreflightStatus(FPGA_LOC__MRA);
   if( ( GetPreflightFlag() )
    && ( eStatus != PREFLIGHT_STATUS__INACTIVE ) )
   {
      PreflightMCU_Command eCommand = FPGA_GetPreflightCommand(FPGA_LOC__MRA);
      if( eCommand == PREFLIGHT_CMD__PICK_BYPASS )
      {
         bPreflightPickCmd = 1;
      }
      else if( eCommand == PREFLIGHT_CMD__DROP_BYPASS )
      {
         bPreflightPickCmd = 0;
      }
   }
   else
   {
      bPreflightPickCmd = 0;
   }

   bPickRelay = bPreflightPickCmd;

   SRU_Write_ExtOutput( eExt_Output_CRGBM, bPickRelay );
}
/*-----------------------------------------------------------------------------

 Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 1000;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_SAFETY_1MS;

   gucFP_LockClipTime_10ms = Param_ReadValue_16Bit( enPARAM16__LockClipTime_10ms );

   if ( gucFP_LockClipTime_10ms > 50 || gucFP_LockClipTime_10ms == 0 )
   {
      gucFP_LockClipTime_10ms = 50;
   }

   SRU_Write_ExtOutput( eExt_Output_CSFM, 0 );

   return 0;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t CheckIfOnConstruction( void )
{
   uint8_t result = 0; // Assume false
   if ( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
   {
      if( GetOperation_ManualMode() == MODE_M__CONSTRUCTION )
      {
         result = 1;
      }
   }

   return result;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   if(GetPreflightFlag())
   {

   }
   else
   {
      UpdateBypassDoorFlags();
      CheckForFaultInput();
      if ( CheckIfOnConstruction() )
      {
         // Safety one & locks
         CheckFor_LocksOpen();
//         CheckFor_CloseContactsOpen();
         CheckFor_Overspeed_Construction();

         // Temporarily enable bypass switch checks on construction since the CPLD flags this fault
         CheckFor_BypassSwitchesOn();
      }
      else
      {
         //--------------------------------------------------
         // Performing all safety checks here
         CheckFor_FPGA_E_Brake();
         CheckFor_RopeGripperDropped();

         //--------------------------------------------------

         // Speed/Position Feedback related safety checks. Must be bypassed on
         // Construction since landing system is not required on this mode
         CheckFor_Overspeed_General();
         CheckFor_Overspeed_Inspection();
         CheckFor_Overspeed_DoorsOpen();

         CheckFor_SpeedDev();

         CheckFor_TractionLoss();

         if( CheckIf_AllStartupSignalsReceived() ) // NYC 21-41 28th Job site: unintended movement triggered at startup
         {
            CheckFor_UnintendedMovement();
         }

         if ( !bBypassCarDoors )
         {
            CheckFor_GateSwitchOpen();
            CheckFor_DPMOpen();
         }

         CheckFor_LocksOpen();
         CheckFor_CloseContactsOpen();

         CheckFor_StopSwitches();
         // Temporarily enable bypass switch checks on construction since the CPLD flags this fault
         CheckFor_BypassSwitchesOn();

         CheckFor_InvalidInspectionMode();

         CheckFor_FireStopSwitch();

         CheckFor_EB_Bypassed();

         CheckFor_PositionLimitFault();
      }

      CheckFor_120VACFault();
      CheckFor_SafetyZone();
      CheckFor_GovernorLost();

      CheckFor_RGB_FeedbackFault();
      CheckFor_RGM_FeedbackFault();
      CheckFor_RGP_FeedbackFault();
      CheckFor_SFP_FeedbackFault();
      CheckFor_SFM_FeedbackFault();
      CheckFor_ContactorFeedback();

      //--------------------------------------------------
      // Actuate relays.
      Control_SFM_Relay();
   }

   Control_RGM_Relay();
   Control_Bypass_Relay();
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
