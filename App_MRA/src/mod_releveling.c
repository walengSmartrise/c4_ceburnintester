/******************************************************************************
 *
 * @file     mod_heartbeat.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Releveling =
{
      .pfnInit = Init,
      .pfnRun = Run,
};

static uint8_t ucRelevelingFlag = 0;

uint8_t inReleveling ( void )
{
   return ucRelevelingFlag;
}

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/


static uint8_t CheckReleveling( void ) {
   uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
   uint8_t bDisableSabbathReleveling = (
      (GetOperation_AutoMode() == MODE_A__SABBATH) &&
      Param_ReadValue_1Bit(enPARAM1__DisableSabbathReleveling)
   );
   uint8_t bEnableReleveling = (
      Param_ReadValue_1Bit(enPARAM1__RelevelEnabled) &&
      !bDisableSabbathReleveling
   );


   return  (!gstFault.bActiveFault) //no active fault
         && (!GetMotion_RunFlag()) //not in motion
         && bEnableReleveling //releveling enabled
         && (
               ( gpastFloors[ucCurrentFloor].bFrontOpening && getDoorZone(DOOR_FRONT)) //at a valid doorzone (front)
               ||
               ( gpastFloors[ucCurrentFloor].bRearOpening && getDoorZone(DOOR_REAR)) //at a valid doorzone (rear)
         )
         && (   InDestinationDoorzone()   //being flagged as in dest door zone and or doors are open
               || (    getDoorZone( DOOR_ANY )
                     &&  AnyDoorOpen()
               )
         );
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 500;
   pstThisModule->uwRunPeriod_1ms = 50;
   return 0;
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static uint16_t uwRelevelTimeout_50ms;
   static uint8_t bArmReleveling;
   static enum en_mode_auto eLastAutoMode;


   if (     ( GetOperation_ClassOfOp() == CLASSOP__AUTO)
         && ( GetOperation_AutoMode() == eLastAutoMode)
         && ( GetOperation_AutoMode() != MODE_A__TEST)) //never enable if on acceptance test
   {
      if( ( !bArmReleveling )
       && ( ( DoorsSafeAndReadyToRun() )
         || ( !GetMotion_RunFlag() && getDoorZone(DOOR_ANY) ) ))
      {
         bArmReleveling = 1;
      }
   }
   else
   {
      bArmReleveling = 0;
      ucRelevelingFlag = 0;
   }

   if ( ( bArmReleveling )
     && ( CheckReleveling() )
     && ( GetPosition_Velocity() <= 1 )
     && ( GetPosition_Velocity() >= -1 ) )
   {
      if ( !InsideDeadZone() )
      {
         if(uwRelevelTimeout_50ms < Param_ReadValue_8Bit(enPARAM8__RelevelingDelay_50ms) )
         {
            uwRelevelTimeout_50ms++;
         }
         else
         {
            SetAlarm(ALM__RELEVELING);
            ucRelevelingFlag = 1;
            StartReleveling( GetOperation_CurrentFloor() );
         }
      }
      else
      {
         ucRelevelingFlag = 0;
      }
   }
   else
   {
      uwRelevelTimeout_50ms = 0;
      ucRelevelingFlag = 0;
   }

   eLastAutoMode = GetOperation_AutoMode();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
