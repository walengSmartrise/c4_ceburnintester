/******************************************************************************
 *
 * @file     mod_fire.c
 * @brief    Watches fire inputs and state.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "GlobalData.h"
#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

static uint8_t CheckForActiveSmoke ( void );
static uint8_t GetRecallFloor( void );
static enum en_doors GetRecallDoor( void );
static void ResetFireService ( void );

uint8_t gbLobbyFireLamp;
uint8_t gbInCarFireLamp;
uint8_t gbShuntFlag;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define RUN_INTERVAL_TO_SECONDS                    (20)
#define FIRE_INPUT_DEBOUNCE__50MS                  (10)
#define FIRE_INPUT_REDUCED_DEBOUNCE__50MS          (2)
#define FIRE_SWITCH_DEBOUNCE_TIMER__50MS           (10) //timer used for debouce the toggling of the fire recall switch input

#define MIN_ATTENDANT_FIRE_RECALL_DELAY__50MS      (10*RUN_INTERVAL_TO_SECONDS)
#define MAX_ATTENDANT_FIRE_RECALL_DELAY__50MS      (30*RUN_INTERVAL_TO_SECONDS)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   FIRE_INPUT__KEY_RECALL_MAIN,
   FIRE_INPUT__KEY_RECALL_REMOTE,
   FIRE_INPUT__SMOKE_MAIN,
   FIRE_INPUT__SMOKE_ALT,
   FIRE_INPUT__SMOKE_MR,
   FIRE_INPUT__SMOKE_HA,
   FIRE_INPUT__SMOKE_PIT,
   FIRE_INPUT__SMOKE_MR_2,
   FIRE_INPUT__SMOKE_HA_2,
   FIRE_INPUT__KEY_RECALL_RESET,
   FIRE_INPUT__REDUCED_DEBOUNCE_INPUTS,
   FIRE_INPUT__FIRE_II_ON = FIRE_INPUT__REDUCED_DEBOUNCE_INPUTS,
   FIRE_INPUT__FIRE_II_OFF,
   FIRE_INPUT__FIRE_II_HOLD,
   FIRE_INPUT__FIRE_II_CNCL,

   NUM_FIRE_INPUTS
}en_fire_inputs;
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static enum en_Smokes enActiveFire;
static enum en_fire_2_states enFirePhase2;
static enum en_FireKeyState enRecallKeySwitch = KEY_OFF;
static enum en_FireKeyState enRemoteKeySwitch = KEY_OFF;

static uint8_t ucCheckReset = 0;

static uint8_t bBuzzer;
static uint8_t ucRecentSmokeReset = 20;
static uint16_t uwStartupTimeout_50ms = 80; //delay before reading fire inputs, 4 seconds

/* This flag delays running of Fire_Phase_2() until the first updating of
 * enFirePhase2 to address race condition where car can clear the fire phase 2
 * emergency bit before its had a chance to check its fire 2 inputs  */
static uint8_t bDelayFirePhase2 = 1;

static uint8_t bDoorOpened_ManualDoors = 0;

/* A17 (2016) - 2.27.5.2(a) requires that if the car is parked at a
 * floor on attendant or independent service when fire recall is activated,
 * the recall will be delayed no more than 30 seconds and no less than 10 seconds. */
static uint16_t uwAttendantFireRecallDelay_50ms;
static uint16_t uwAttendantFireRecallLimit_50ms;

static uint8_t abDebouncedFireInputs[BITMAP8_SIZE(NUM_FIRE_INPUTS)];
static uint8_t aucInputDebounceTimers_50ms[NUM_FIRE_INPUTS];
static const enum en_input_functions aeFireInputs[NUM_FIRE_INPUTS] =
{
      enIN_FIRE_RECALL_OFF, //FIRE_INPUT__KEY_RECALL_MAIN,
      enIN_REMOTE_FIRE_KEY_SWITCH, //FIRE_INPUT__KEY_RECALL_REMOTE,
      enIN_SMOKE_MAIN, //FIRE_INPUT__SMOKE_MAIN,
      enIN_SMOKE_ALT, //FIRE_INPUT__SMOKE_ALT,
      enIN_SMOKE_MR, //FIRE_INPUT__SMOKE_MR,
      enIN_SMOKE_HA, //FIRE_INPUT__SMOKE_HA,
      enIN_SMOKE_PIT, //FIRE_INPUT__SMOKE_PIT,
      enIN_SMOKE_MR_2, //FIRE_INPUT__SMOKE_MR_2,
      enIN_SMOKE_HA_2, //FIRE_INPUT__SMOKE_HA_2,
      //FIRE_INPUT__REDUCED_DEBOUNCE_INPUTS,
      enIN_FIRE_RECALL_RESET, //FIRE_INPUT__KEY_RECALL_RESET,
      enIN_FIRE_II_ON, //FIRE_INPUT__FIRE_II_ON,
      enIN_FIRE_II_OFF, //FIRE_INPUT__FIRE_II_OFF,
      enIN_FIRE_II_HOLD, //FIRE_INPUT__FIRE_II_HOLD,
      enIN_FIRE_II_CNCL, //FIRE_INPUT__FIRE_II_CNCL,
};
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Fire =
{
      .pfnInit = Init,
      .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   @fn uint8_t Fire_CheckIfFireRecallDelayed( void )
   @return Returns 1 if transition to fire operation should be delayed
   @brief This function is used to delay the transition to fire phase 1/2 for a fixed period when on independent or attendant service
 *----------------------------------------------------------------------------*/
uint8_t Fire_CheckIfFireRecallDelayed( void )
{
   uint8_t bDelayed = uwAttendantFireRecallDelay_50ms <= uwAttendantFireRecallLimit_50ms;
   return bDelayed;
}
/*----------------------------------------------------------------------------
   @fn static void Fire_UpdateAttendantFireRecallDelay( void )
   @return none
   @brief This function updates timers used to delay the start of fire recall when activated and the car is parked at a non recall floor on attendant or independent service.
 *----------------------------------------------------------------------------*/
static void Fire_UpdateAttendantFireRecallDelay( void )
{
   uwAttendantFireRecallLimit_50ms = Param_ReadValue_8Bit(enPARAM8__ATTD_FireRecallDelay_1s) * RUN_INTERVAL_TO_SECONDS;
   if( uwAttendantFireRecallLimit_50ms < MIN_ATTENDANT_FIRE_RECALL_DELAY__50MS )
   {
      uwAttendantFireRecallLimit_50ms = MIN_ATTENDANT_FIRE_RECALL_DELAY__50MS;
   }
   else if( uwAttendantFireRecallLimit_50ms > MAX_ATTENDANT_FIRE_RECALL_DELAY__50MS )
   {
      uwAttendantFireRecallLimit_50ms = MAX_ATTENDANT_FIRE_RECALL_DELAY__50MS;
   }

   if( uwAttendantFireRecallDelay_50ms <= uwAttendantFireRecallLimit_50ms )
   {
      uwAttendantFireRecallDelay_50ms++;
   }

   /* IF On neither independent service nor attendant service,
    * OR in motion
    * OR at fire recall floor
    *    THEN bypass the fire recall delay timer */
   enum en_mode_auto eAutoMode = GetOperation_AutoMode();
   if( ( ( eAutoMode != MODE_A__INDP_SRV ) && ( eAutoMode != MODE_A__ATTENDANT ) )
    || ( GetMotion_RunFlag() )
    || ( GetOperation_CurrentFloor() == GetRecallFloor() ) )
   {
      uwAttendantFireRecallDelay_50ms = uwAttendantFireRecallLimit_50ms+1;
   }
   /* Reset the delay timer if in independent service or attendant AND neither fire phase is active */
   else if( ( eAutoMode == MODE_A__INDP_SRV ) || ( eAutoMode == MODE_A__ATTENDANT ) )
   {
      if( Fire_GetFireService() == FIRE_SERVICE__INACTIVE )
      {
         uwAttendantFireRecallDelay_50ms = 0;
      }
   }
}
/*----------------------------------------------------------------------------
   @fn static void Fire_UpdateDebouncedInputs( void )
   @return none
   @brief This function updates the debounced fire input flags used by the fire module.
 *----------------------------------------------------------------------------*/
static void Fire_UpdateDebouncedInputs(void)
{
   for(uint8_t i = 0; i < NUM_FIRE_INPUTS; i++)
   {
      uint8_t bNewInput = GetInputValue(aeFireInputs[i]);
      uint8_t bOldInput = Sys_Bit_Get(&abDebouncedFireInputs[0], i);
      if( bNewInput != bOldInput )
      {
         if( i >= FIRE_INPUT__REDUCED_DEBOUNCE_INPUTS )
         {
            if( ++aucInputDebounceTimers_50ms[i] >= FIRE_INPUT_REDUCED_DEBOUNCE__50MS )
            {
               Sys_Bit_Set(&abDebouncedFireInputs[0], i, bNewInput);
            }
         }
         else
         {
            if( ++aucInputDebounceTimers_50ms[i] >= FIRE_INPUT_DEBOUNCE__50MS )
            {
               Sys_Bit_Set(&abDebouncedFireInputs[0], i, bNewInput);
            }
         }
      }
      else
      {
         aucInputDebounceTimers_50ms[i] = 0;
      }
   }
}
/*----------------------------------------------------------------------------
   @fn static uint8_t Fire_GetDebouncedInput(en_fire_inputs eInput)
   @return Returns the state of the specified debounced fire input (1 if active)
   @brief This function updates the debounced fire input flags used by the fire module.
 *----------------------------------------------------------------------------*/
static uint8_t Fire_GetDebouncedInput(en_fire_inputs eInput)
{
   uint8_t bReturn = Sys_Bit_Get(&abDebouncedFireInputs[0], eInput);
   return bReturn;
}
/*----------------------------------------------------------------------------
   @fn static void Fire_UpdateVirtualKeyswitches(void)
   @return none
   @brief This function updates the virtual keyswitch variables used by the fire module.
 *----------------------------------------------------------------------------*/
static void Fire_UpdateVirtualKeyswitches(void)
{
   static uint8_t ucKeySwapTimeout_50ms;
   //used to make a virtual phase 1 keyswitch, cleans up looking for state to single location
   if( Fire_GetDebouncedInput(FIRE_INPUT__KEY_RECALL_RESET) ) {//if ( GetInputValue( enIN_FIRE_RECALL_RESET ) ) {
      ucKeySwapTimeout_50ms = 0;
      enRecallKeySwitch = KEY_RESET;
   } else if( Fire_GetDebouncedInput(FIRE_INPUT__KEY_RECALL_MAIN) ) {//} else if ( GetInputValue( enIN_FIRE_RECALL_OFF ) ) {
      ucKeySwapTimeout_50ms = 0;
      enRecallKeySwitch = KEY_OFF;
   } else if (ucKeySwapTimeout_50ms > FIRE_SWITCH_DEBOUNCE_TIMER__50MS) {
      enRecallKeySwitch = KEY_ON;
   } else {
      ucKeySwapTimeout_50ms++;
   }

   //used to make the remote fire keyswitch, this check makes sure its programmed to a board
   //so the input has to be programmed and low to go into fire service.
   if ( ( CheckIfInputIsProgrammed(enIN_REMOTE_FIRE_KEY_SWITCH) )
     && ( !Fire_GetDebouncedInput(FIRE_INPUT__KEY_RECALL_REMOTE) ) )//  && ( !GetInputValue( enIN_REMOTE_FIRE_KEY_SWITCH ) ) )
   {
      enRemoteKeySwitch = KEY_ON;
   } else {
      enRemoteKeySwitch = KEY_OFF;
   }
}
/*----------------------------------------------------------------------------
   Checks if fire phase 2 door functionality should be enabled
   Used to ensure fire phase 2 door handling should be active
   even if fire phase 2 isn't the active mode of operation
 *----------------------------------------------------------------------------*/
uint8_t Fire_CheckIfFire2DoorsEnabled(void)
{
   uint8_t bEnabled = ( enFirePhase2 == FIRE_II__ON ) && ( GetEmergencyBit( EmergencyBF_FirePhaseII_Active ) ) && ( GetOperation_ClassOfOp() == CLASSOP__AUTO );
   return bEnabled;
}
/*----------------------------------------------------------------------------
   @fn uint8_t Fire_CheckIfFire1Recall(void)
   @return Returns 1 if car is attempting a fire phase 1 recall
   @brief This function checks if car is attempting a fire phase 1 recall
 *----------------------------------------------------------------------------*/
uint8_t Fire_CheckIfFire1Recall(void)
{
   uint8_t bEnabled = ( GetOperation_AutoMode() == MODE_A__FIRE1 ) && ( GetEmergencyBit( EmergencyBF_FirePhaseI_Active ) ) && ( GetRecallState() != RECALL__RECALL_FINISHED );
   return bEnabled;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint8_t Fire_GetBuzzer(void)
{
   return bBuzzer;
}
void Fire_SetBuzzer(uint8_t bOn)
{
   bBuzzer = bOn;
}

static void UpdateBuzzer(void)
{
   if( ( GetRecallState() == RECALL__RECALL_FINISHED )
    && ( Fire_GetFireService() ) )
   {
      Fire_SetBuzzer(0);
   }
   else if( ( Fire_GetFireService() == FIRE_SERVICE__PHASE2 )
         && ( enFirePhase2 != FIRE_II__OFF ) )
   {
      Fire_SetBuzzer(0);
   }
   else if( ( Fire_GetFireService() == FIRE_SERVICE__PHASE1 )
         || ( Fire_GetFireService() == FIRE_SERVICE__PHASE2 ) )
   {
      Fire_SetBuzzer(1);
   }
   else
   {
      Fire_SetBuzzer(0);
   }
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static uint8_t GetBypassFireService()
{
   uint8_t bBypass = SRU_Read_DIP_Switch(enSRU_DIP_B6)
                        && Param_ReadValue_1Bit(enPARAM1__BypassFireSrv);

   bBypass |= SRU_Read_DIP_Switch(enSRU_DIP_A3);
   /* Also bypass fire when on learn mode */
   bBypass |= GetOperation_ClassOfOp() == CLASSOP__SEMI_AUTO;
   return bBypass;
}
en_fire_services Fire_GetFireService( void )
{
   en_fire_services eService = FIRE_SERVICE__INACTIVE;
   if( GetBypassFireService() )
   {
      eService = FIRE_SERVICE__INACTIVE;/* Bypass fire  */
   }
   else if( GetEmergencyBit( EmergencyBF_FirePhaseII_Active ) || enFirePhase2 != FIRE_II__OFF )
   {
      eService = FIRE_SERVICE__PHASE2;
   }
   else if( GetEmergencyBit( EmergencyBF_FirePhaseI_Active ) || enActiveFire != SMOKE_NO_SMOKE_ACTIVE)
   {
      eService = FIRE_SERVICE__PHASE1;
   }

   if( ( eService != FIRE_SERVICE__INACTIVE )
    && ( Flood_GetActiveFlag() )
    && ( Param_ReadValue_1Bit(enPARAM1__FLOOD_OverrideFire) ) )
   {
      eService = FIRE_SERVICE__FLOOD_BYP;
   }

   return eService;
}

enum en_fire_2_states Fire_GetFireService_2_State ( void )
{
   return enFirePhase2;
}

void Fire_Phase_1 ( uint8_t bInitMode )
{
   Auto_SetSpeed(GetFP_ContractSpeed());
   Auto_Set_Limits();

   if ( bInitMode )
   {
      ClearLatchedCarCalls();
      ClearLatchedHallCalls();
      bDoorOpened_ManualDoors = 0;
   }

   uint8_t ucRecallFloor = GetRecallFloor();
   enum en_doors enCurrentRecallDoor = GetRecallDoor();
   en_recall_door_command eRecallDoorCommand = ( enCurrentRecallDoor == DOOR_FRONT ) ? RECALL_DOOR_COMMAND__OPEN_FRONT_DOOR:RECALL_DOOR_COMMAND__OPEN_REAR_DOOR;
   Fire_SetBuzzer(1);

   if(!uwStartupTimeout_50ms)
   {
      if (Auto_Recall ( bInitMode, ucRecallFloor, eRecallDoorCommand ))
      {
         Fire_SetBuzzer(0);
         /*
          *  If i have my reset armed, and i am in a state i can reset fire service
          */
         if( ( GetEmergencyBit( EmergencyBF_FirePhaseI_ArmReset ) )
          && ( ( Param_ReadValue_1Bit( enPARAM1__Fire__AllowResetWithActiveSmoke ) ) || ( CheckForActiveSmoke() == SMOKE_NO_SMOKE_ACTIVE ) )
         )
         {
            ResetFireService();
            SetDoorCommand( enCurrentRecallDoor, DOOR_COMMAND__OPEN_IN_CAR_REQUEST ); // Reset door hold command asserted by Auto_Recall
         }

         SetOutputValue(enOUT_FIRE_SHUNT,gbShuntFlag);
      }
   }

   if( (GetDoorState(enCurrentRecallDoor) == DOOR__OPEN) && (GetFP_DoorType(enCurrentRecallDoor) == DOOR_TYPE__MANUAL) )
   {
      bDoorOpened_ManualDoors = 1;
   }
}

void Fire_Phase_2 ( uint8_t bInitMode )
{
   static uint8_t bWasRunning = 0;
   static enum en_fire_2_states eLastState;
   static uint8_t ucDelayCounter_50ms;
   Fire_SetBuzzer(0);
   Auto_SetSpeed(GetFP_ContractSpeed());
   Auto_Set_Limits();

   SetOperation_PositionLimit_UP( gpastFloors[ GetFP_NumFloors() - 1 ].ulPosition + ONE_INCH);

   if ( bWasRunning && !GetMotion_RunFlag()) {
      ClearLatchedCarCalls();
   }
   bWasRunning = GetMotion_RunFlag();

   //Check for Call Cancel Key.
   if( Fire_GetDebouncedInput(FIRE_INPUT__FIRE_II_CNCL) )//if ( GetInputValue( enIN_FIRE_II_CNCL ) )
   {
      //while the call cancel button is pressed, also disable being able to latch or answer car calls
      gstCurrentModeRules[ MODE_A__FIRE2 ].bIgnoreCarCall_F = 1;
      gstCurrentModeRules[ MODE_A__FIRE2 ].bIgnoreCarCall_R = 1;
      ClearLatchedCarCalls();
      StopNextAvailableLanding();
   }
   else
   {
      //re enable latching/answering car calls
      gstCurrentModeRules[ MODE_A__FIRE2 ].bIgnoreCarCall_F = 0;
      gstCurrentModeRules[ MODE_A__FIRE2 ].bIgnoreCarCall_R = 0;
   }

   if( !bDelayFirePhase2 )
   {
      switch (enFirePhase2)
      {
         case FIRE_II__OFF:
        {
            if(Param_ReadValue_1Bit(enPARAM1__Fire__RecallToMainAfterFirePhase2))
            {
               uint8_t ucRecallFloor = Param_ReadValue_8Bit( enPARAM8__FireRecallFloorM );
               enum en_doors enCurrentRecallDoor;

               ClearLatchedCarCalls();

               if(Param_ReadValue_1Bit(enPARAM1__FireRecallDoorM))
               {
                  enCurrentRecallDoor = DOOR_REAR;
               }
               else
               {
                  enCurrentRecallDoor = DOOR_FRONT;
               }

              Fire_SetBuzzer(1);
              en_recall_door_command eRecallDoorCommand = ( enCurrentRecallDoor == DOOR_FRONT ) ? RECALL_DOOR_COMMAND__OPEN_FRONT_DOOR:RECALL_DOOR_COMMAND__OPEN_REAR_DOOR;
              if (Auto_Recall ( eLastState != enFirePhase2, ucRecallFloor, eRecallDoorCommand ))
              {
                 //revert back to phase 1
                 ClrEmergencyBit(EmergencyBF_FirePhaseII_Active);
                 Fire_SetBuzzer(0);
              }
            }
            else
            {
                 uint8_t ucRecallFloor = GetRecallFloor();
                 ClearLatchedCarCalls();

                 enum en_doors enCurrentRecallDoor = GetRecallDoor();

                 Fire_SetBuzzer(1);
                 en_recall_door_command eRecallDoorCommand = ( enCurrentRecallDoor == DOOR_FRONT ) ? RECALL_DOOR_COMMAND__OPEN_FRONT_DOOR:RECALL_DOOR_COMMAND__OPEN_REAR_DOOR;
                 if (Auto_Recall ( eLastState != enFirePhase2, ucRecallFloor, eRecallDoorCommand ))
                 {
                    //revert back to phase 1
                    ClrEmergencyBit(EmergencyBF_FirePhaseII_Active);
                    Fire_SetBuzzer(0);
                 }
             }
          break;
        }
         case FIRE_II__ON:
            /* Prevent issue where on reset, a fully open door will start to close */
            if(bInitMode)
            {
               for (enum en_doors enDoorIndex = DOOR_FRONT; enDoorIndex < GetNumberOfDoors(); enDoorIndex++)
               {
                  Doors_ForceDoorsFullyOpen(enDoorIndex);
               }
            }

            Auto_Oper( 0 ); // 0 means do not INIT auto Oper
            break;
         default:
         case FIRE_II__HOLD:
            if ( AnyDoorFullyOpen() || bDoorOpened_ManualDoors )
            {
               ClearLatchedCarCalls();

               //Disable latching or answering car calls while on fire 2 hold
               gstCurrentModeRules[ MODE_A__FIRE2 ].bIgnoreCarCall_F = 1;
               gstCurrentModeRules[ MODE_A__FIRE2 ].bIgnoreCarCall_R = 1;
            }
            else if ( CarDoorsClosed() )
            {
               enFirePhase2 = FIRE_II__ON;
            }
            else
            {
               for (enum en_doors enDoorIndex = DOOR_FRONT; enDoorIndex < GetNumberOfDoors(); enDoorIndex++)
               {
                  SetInDestinationDoorzone(getDoorZone(DOOR_ANY));
                  if( ( Param_ReadValue_1Bit(enPARAM1__Fire__DoorOpenOnHold) )
                   || ( GetDoorState(enDoorIndex) != DOOR__CLOSED ) )
                  {
                     SetDoorCommand( enDoorIndex, DOOR_COMMAND__OPEN_HOLD_REQUEST );
                  }
               }
            }
            break;
      }

      eLastState = enFirePhase2;
   }
}


/*
 *  Hat flash update system looks to see if the hat flash is on,
 *  and if it should be trying to update this setting before
 *  checking flags from inputs
 */
static void CheckHatFlashUpdate ( void ) {
   if (   ( Param_ReadValue_1Bit(enPARAM1__Fire__HatFlashIgnoreOrder) )
         && ( !GetEmergencyBit(EmergencyBF_FirePhaseI_ArmReset) )
         && (  ( !Fire_GetDebouncedInput(FIRE_INPUT__SMOKE_MAIN) && Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Main_FlashFireHat) && CheckIfInputIsProgrammed(enIN_SMOKE_MAIN) )
            || ( !Fire_GetDebouncedInput(FIRE_INPUT__SMOKE_ALT)  && Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Alt_FlashFireHat) )
            || ( !Fire_GetDebouncedInput(FIRE_INPUT__SMOKE_HA)   && Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Hoistway_FlashFireHat) )
            || ( !Fire_GetDebouncedInput(FIRE_INPUT__SMOKE_MR)   && Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_MachineRoom_FlashFireHat) )
            || ( !Fire_GetDebouncedInput(FIRE_INPUT__SMOKE_PIT)  && Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Pit_FlashFireHat) )
            || ( !Fire_GetDebouncedInput(FIRE_INPUT__SMOKE_HA_2) && Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Hoistway2_FlashFireHat) )
            || ( !Fire_GetDebouncedInput(FIRE_INPUT__SMOKE_MR_2) && Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_MachineRoom2_FlashFireHat) ) )
//         && (  ( !GetInputValue( enIN_SMOKE_MAIN )  && Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Main_FlashFireHat) )
//            || ( !GetInputValue( enIN_SMOKE_ALT ) && Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Alt_FlashFireHat) )
//            || ( !GetInputValue( enIN_SMOKE_HA )    && Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Hoistway_FlashFireHat) )
//            || ( !GetInputValue( enIN_SMOKE_MR )    && Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_MachineRoom_FlashFireHat) )
//            || ( !GetInputValue( enIN_SMOKE_PIT)    && Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Pit_FlashFireHat) )
//            || ( !GetInputValue( enIN_SMOKE_HA_2 )  && Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Hoistway2_FlashFireHat) )
//            || ( !GetInputValue( enIN_SMOKE_MR_2 )  && Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_MachineRoom2_FlashFireHat) ) )
   )
   {
      //Set Flash Fire Hat Bit
      SetEmergencyBit(EmergencyBF_FirePhaseI_FlashFireHat);
   }
}

static uint8_t CheckForActiveSmoke ( void )
{
   static uint8_t bContactedRiser = 0;

   enum en_Smokes enTempActiveFire = SMOKE_NO_SMOKE_ACTIVE;

   if (!bContactedRiser)
   {
      if (Sys_GetTickCount( enTickCount_1s ) > 30)
      {
         bContactedRiser |= 1;
      }
   }
   else if ( enRecallKeySwitch == KEY_ON || enRemoteKeySwitch == KEY_ON )
   {
      enTempActiveFire = SMOKE_FIRE_RECALL;
   }
   else if( !Fire_GetDebouncedInput(FIRE_INPUT__SMOKE_MAIN) && CheckIfInputIsProgrammed(enIN_SMOKE_MAIN) )//else if ( GetInputValue( enIN_SMOKE_MAIN ) == 0 )
   {
      enTempActiveFire = SMOKE_MAIN;
   }
   else if( !Fire_GetDebouncedInput(FIRE_INPUT__SMOKE_ALT) )//else if ( GetInputValue( enIN_SMOKE_ALT ) == 0 )
   {
      enTempActiveFire = SMOKE_ALT;
   }
   else if( !Fire_GetDebouncedInput(FIRE_INPUT__SMOKE_HA) )//else if ( GetInputValue( enIN_SMOKE_HA ) == 0 )
   {
      enTempActiveFire = SMOKE_HA;
   }
   else if( !Fire_GetDebouncedInput(FIRE_INPUT__SMOKE_MR) )//else if ( GetInputValue( enIN_SMOKE_MR ) == 0 )
   {
      enTempActiveFire = SMOKE_MR;
   }
   else if ( ( Param_ReadValue_1Bit(enPARAM1__EnableAltMachineRoom) )
          && ( !Fire_GetDebouncedInput(FIRE_INPUT__SMOKE_HA_2) ) )//&& ( GetInputValue( enIN_SMOKE_HA_2 ) == 0 ) )
   {
      enTempActiveFire = SMOKE_HA_2;
   }
   else if ( ( Param_ReadValue_1Bit(enPARAM1__EnableAltMachineRoom) )
          && ( !Fire_GetDebouncedInput(FIRE_INPUT__SMOKE_MR_2) ) )//&& ( GetInputValue( enIN_SMOKE_MR_2 ) == 0 ) )
   {
      enTempActiveFire = SMOKE_MR_2;
   }
   else if( ( CheckIfInputIsProgrammed(enIN_SMOKE_PIT) )
         && ( !Fire_GetDebouncedInput(FIRE_INPUT__SMOKE_PIT) ) )//&& ( GetInputValue(enIN_SMOKE_PIT) == 0 ) )
   {
      enTempActiveFire = SMOKE_PIT;
   }

   return enTempActiveFire;
}

/*
 * Function: GetRecallFloor
 * Input: None
 * Output: uint8_t    Selected Recall Floor
 * Side Effects: None
 * Description: Select what recall floor the car should go to during fire.
 *              For a pit smoke the car will recall to the higher of the main
 *              and alternate floor. If pit smoke is not enabled then normal
 *              recall logic is used.
 */
static uint8_t GetRecallFloor( void )
{
   uint8_t mainRecall = Param_ReadValue_8Bit( enPARAM8__FireRecallFloorM );
   uint8_t altRecall  = Param_ReadValue_8Bit( enPARAM8__FireRecallFloorA );

   uint8_t selectedRecallFloor  = mainRecall;

   if( GetEmergencyBit( EmergencyBF_FirePhaseI_RecallToAltFloor ) )
   {
      selectedRecallFloor = Param_ReadValue_8Bit( enPARAM8__FireRecallFloorA );
   }

   return selectedRecallFloor;
}

static enum en_doors GetRecallDoor( void )
{
   enum en_doors enCurrentRecallDoor = DOOR_FRONT;
   if ( (  GetEmergencyBit(EmergencyBF_FirePhaseI_RecallToAltFloor) && Param_ReadValue_1Bit(enPARAM1__FireRecallDoorA) )
     || ( !GetEmergencyBit(EmergencyBF_FirePhaseI_RecallToAltFloor) && Param_ReadValue_1Bit(enPARAM1__FireRecallDoorM) ) )
   {
      enCurrentRecallDoor = DOOR_REAR;
   }
   return enCurrentRecallDoor;
}

static void SetActiveSmokeSettings ( void )
{
   // Default: do not recall to alt
   uint8_t bRecallToAlt  = 0;
   // Default: Do not flash fore hat
   uint8_t bFlashFireHat = 0;

   switch (enActiveFire)
   {
      default:
      case SMOKE_FIRE_RECALL:
      case SMOKE_MAIN:
         bRecallToAlt  = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Main_UseAltFloor);
         bFlashFireHat = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Main_FlashFireHat);
         gbShuntFlag   = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Main_ShuntOnRecall);
         break;
      case SMOKE_ALT:
         bRecallToAlt  = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Alt_UseAltFloor);
         bFlashFireHat = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Alt_FlashFireHat);
         gbShuntFlag   = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Alt_ShuntOnRecall);
         break;
      case SMOKE_MR:
         bRecallToAlt  =  Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_MachineRoom_UseAltFloor);
         bFlashFireHat = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_MachineRoom_FlashFireHat);
         gbShuntFlag   = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_MachineRoom_ShuntOnRecall);
         break;
      case SMOKE_HA:
         bRecallToAlt  = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Hoistway_UseAltFloor);
         bFlashFireHat = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Hoistway_FlashFireHat);
         gbShuntFlag   = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Hoistway_ShuntOnRecall);
         break;
      case SMOKE_PIT:
         bRecallToAlt  = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Pit_UseAltFloor);
         bFlashFireHat = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Pit_FlashFireHat);
         gbShuntFlag   = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Pit_ShuntOnRecall);
         break;
      case SMOKE_MR_2:
         bRecallToAlt  =  Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_MachineRoom2_UseAltFloor);
         bFlashFireHat = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_MachineRoom2_FlashFireHat);
         gbShuntFlag   = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_MachineRoom2_ShuntOnRecall);
         break;
      case SMOKE_HA_2:
         bRecallToAlt  = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Hoistway2_UseAltFloor);
         bFlashFireHat = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Hoistway2_FlashFireHat);
         gbShuntFlag   = Param_ReadValue_1Bit(enPARAM1__Fire__Smoke_Hoistway2_ShuntOnRecall);
         break;
   }
   //Set Main or Alt Recall Floor
   if(bRecallToAlt)
   {
      SetEmergencyBit(EmergencyBF_FirePhaseI_RecallToAltFloor);
   }
   else
   {
      ClrEmergencyBit(EmergencyBF_FirePhaseI_RecallToAltFloor);
   }

   //Set Flash Fire Hat Bit
   if(bFlashFireHat)
   {
      SetEmergencyBit(EmergencyBF_FirePhaseI_FlashFireHat);
   }
   else
   {
      ClrEmergencyBit(EmergencyBF_FirePhaseI_FlashFireHat);
   }
}

static void ResetFireService ( void )
{
   ucRecentSmokeReset = 20;
   gbShuntFlag = 0;
   Fire_SetBuzzer(0);
   enActiveFire = SMOKE_NO_SMOKE_ACTIVE;
   enFirePhase2 = FIRE_II__OFF;
   ClrEmergencyBit(EmergencyBF_FirePhaseI_ArmReset);
   ClrEmergencyBit(EmergencyBF_FirePhaseI_Active);
   ClrEmergencyBit(EmergencyBF_FirePhaseII_Active);
   SetOutputValue(enOUT_FIRE_SHUNT,gbShuntFlag);
}

uint8_t Fire_AtPhase2RecallFloor()
{
   uint8_t bAtRecallFloor = 0;
   if( ( GetOperation_CurrentFloor() == GetRecallFloor() )
    && ( getDoorZone(DOOR_ANY) ) )
   {
      bAtRecallFloor = 1;
   }
   return bAtRecallFloor;
}
/*-----------------------------------------------------------------------------
   If the car is in a mode of operation that is higher priority than fire (EPWR)
   the car will not respond to a fire reset command without this function.
-----------------------------------------------------------------------------*/
static void UpdateFireResetNotInFireOperation(void)
{
   enum en_doors eRecallDoor = GetRecallDoor();
   uint8_t ucRecallFloor = GetRecallFloor();
   /* If not in fire op
    * and fully recalled
    * and fire phase 1 reset is armed
    * and Fire 2 off is set
    * */
   if( ( ( !GetMotion_RunFlag() ) && ( GetOperation_CurrentFloor() == ucRecallFloor ) && ( GetDoorState(eRecallDoor) == DOOR__OPEN ) ) // If fully recalled
      && ( GetEmergencyBit( EmergencyBF_FirePhaseI_ArmReset ) )
      && ( ( GetOperation_AutoMode() != MODE_A__FIRE1 ) && ( GetOperation_AutoMode() != MODE_A__FIRE2 ) )
      && ( !Fire_GetDebouncedInput(FIRE_INPUT__FIRE_II_ON) && Fire_GetDebouncedInput(FIRE_INPUT__FIRE_II_OFF) )//&& ( ( !GetInputValue( enIN_FIRE_II_ON ) ) && ( GetInputValue( enIN_FIRE_II_OFF ) ) )
      && ( ( Param_ReadValue_1Bit( enPARAM1__Fire__AllowResetWithActiveSmoke ) ) || ( CheckForActiveSmoke() == SMOKE_NO_SMOKE_ACTIVE ) )
   )
   {
      ResetFireService();
      SetDoorCommand( eRecallDoor, DOOR_COMMAND__OPEN_IN_CAR_REQUEST );
   }

}
/*-----------------------------------------------------------------------------
   Update lamps
-----------------------------------------------------------------------------*/
static void UpdateFireLampAndBuzzer(void)
{
   static uint8_t ucFlashingTimer_50ms;
   if ( Fire_GetFireService() )
   {
      if( GetEmergencyBit( EmergencyBF_FirePhaseI_FlashFireHat ) )
      {
         if (ucFlashingTimer_50ms > 10)
         {
            ucFlashingTimer_50ms = 0;
            gbInCarFireLamp = ! gbInCarFireLamp;
            if ( Param_ReadValue_1Bit( enPARAM1__Fire__FlashLobbyFireLamp ) )
            {
               gbLobbyFireLamp = gbInCarFireLamp;
            }
            else
            {
               gbLobbyFireLamp = 1;
            }
         }
         else
         {
            ucFlashingTimer_50ms++;
         }
      }
      else
      {
         gbInCarFireLamp = 1;
         gbLobbyFireLamp = 1;
      }
   }
   else
   {
      gbInCarFireLamp = 0;
      gbLobbyFireLamp = 0;
      Fire_SetBuzzer(0);
   }
}
/*-----------------------------------------------------------------------------
   @fn static void Fire_UpdateSystemAlarm( void )
   @return none
   @brief This function checks if fire phase 1 has been activated.
          If it has, it asserts a system alarm marking which fire
          input caused its activation.
-----------------------------------------------------------------------------*/
static void Fire_UpdateSystemAlarm(void)
{
   if( ( ( enActiveFire != SMOKE_NO_SMOKE_ACTIVE ) || GetEmergencyBit(EmergencyBF_FirePhaseI_Active) )
    && ( !GetBypassFireService() ) )
   {
      switch(enActiveFire)
      {
         case SMOKE_FIRE_RECALL:
            if( enRecallKeySwitch == KEY_ON )
            {
               SetAlarm(ALM__FIRE_KEY_MAIN);
            }
            else// if( enRemoteKeySwitch == KEY_ON )
            {
               SetAlarm(ALM__FIRE_KEY_REMOTE);
            }
            break;
         case SMOKE_MAIN:
            SetAlarm(ALM__FIRE_SMOKE_MAIN);
            break;
         case SMOKE_ALT:
            SetAlarm(ALM__FIRE_SMOKE_ALT);
            break;
         case SMOKE_MR:
            SetAlarm(ALM__FIRE_SMOKE_MR);
            break;
         case SMOKE_HA:
            SetAlarm(ALM__FIRE_SMOKE_HA);
            break;
         case SMOKE_SAVED_SMOKE:
            SetAlarm(ALM__FIRE_SMOKE_SAVED);
            break;
         case SMOKE_PIT:
            SetAlarm(ALM__FIRE_SMOKE_PIT);
            break;
         case SMOKE_MR_2:
            SetAlarm(ALM__FIRE_SMOKE_MR_2);
            break;
         case SMOKE_HA_2:
            SetAlarm(ALM__FIRE_SMOKE_HA_2);
            break;
         default: break;
      }
   }
}
/*-----------------------------------------------------------------------------
   @fn static void Fire_UpdateStatePhase2( void )
   @return none
   @brief This function updates the fire phase 2 state, enFirePhase2
-----------------------------------------------------------------------------*/
static void Fire_UpdateStatePhase2(void)
{
   bDelayFirePhase2 = 0;
   uint8_t bPhase2Active = ( enFirePhase2 != FIRE_II__OFF ) || ( GetEmergencyBit( EmergencyBF_FirePhaseII_Active ) );
   if ( ( AnyDoorFullyOpen() || bDoorOpened_ManualDoors )
     && ( ( GetRecallFloor() == GetOperation_CurrentFloor() ) || bPhase2Active ) )
   {
      //Update phase 2 mode input status or reset Fire Service
      if( Fire_GetDebouncedInput(FIRE_INPUT__FIRE_II_ON) )//if ( GetInputValue( enIN_FIRE_II_ON ) )
      {
         enFirePhase2 = FIRE_II__ON;
         SetEmergencyBit(EmergencyBF_FirePhaseII_Active);
      }
      else if( Fire_GetDebouncedInput(FIRE_INPUT__FIRE_II_OFF) )//else if ( GetInputValue( enIN_FIRE_II_OFF ) )
      {
         enFirePhase2 = FIRE_II__OFF;

         if ( ( Param_ReadValue_1Bit( enPARAM1__Fire__Group3TypeHoldSwitch ) )
           && ( CheckForActiveSmoke() == SMOKE_NO_SMOKE_ACTIVE )
           && ( GetRecallFloor() != GetOperation_CurrentFloor() ) )
         {
            enFirePhase2 = FIRE_II__HOLD;
         }
         else if( ( CheckForActiveSmoke() == SMOKE_NO_SMOKE_ACTIVE )
               && ( GetRecallFloor() == GetOperation_CurrentFloor() ) )
         {
            ClrEmergencyBit(EmergencyBF_FirePhaseII_Active);
         }
      }
      else if( Fire_GetDebouncedInput(FIRE_INPUT__FIRE_II_HOLD) )//else if ( GetInputValue( enIN_FIRE_II_HOLD ) )
      {
         enFirePhase2 = FIRE_II__HOLD;
      }
      else //Unknown state
      {
         enFirePhase2 = FIRE_II__HOLD;
      }
   }
   else if ( bPhase2Active )
   {
      if( ( Fire_GetDebouncedInput(FIRE_INPUT__FIRE_II_OFF) )//( GetInputValue( enIN_FIRE_II_OFF ) )
       && ( !Param_ReadValue_1Bit( enPARAM1__Fire__DOL_toExitPhase2) ) )
      {
         enFirePhase2 = FIRE_II__OFF;

         if ( ( Param_ReadValue_1Bit( enPARAM1__Fire__Group3TypeHoldSwitch ) )
           && ( CheckForActiveSmoke() == SMOKE_NO_SMOKE_ACTIVE )
           && ( GetRecallFloor() != GetOperation_CurrentFloor() ) )
         {
            enFirePhase2 = FIRE_II__HOLD;
         }
      }
      /* To allow aborting of fire phase 2 recall before takeoff */
      else if( ( enFirePhase2 == FIRE_II__OFF ) && !GetMotion_RunFlag() )
      {
         if( Fire_GetDebouncedInput(FIRE_INPUT__FIRE_II_ON) )//if( GetInputValue( enIN_FIRE_II_ON ) )
         {
            enFirePhase2 = FIRE_II__ON;
         }
         else if( Fire_GetDebouncedInput(FIRE_INPUT__FIRE_II_HOLD) )//else if ( GetInputValue( enIN_FIRE_II_HOLD ) )
         {
            enFirePhase2 = FIRE_II__HOLD;
         }
      }
   }
}

/*-----------------------------------------------------------------------------
   Update enActiveFire which tracks the signal that triggering fire phase 1
   and related flags
-----------------------------------------------------------------------------*/
static void UpdateActiveFireSignal(void)
{
   uint8_t bReassessActiveSmoke = ( enActiveFire == SMOKE_NO_SMOKE_ACTIVE );

   /* Reassess if the current active fire source is either fire recall switch and the fire recall switches should not be latched */
   bReassessActiveSmoke |= ( ( Param_ReadValue_1Bit(enPARAM1__Fire__DisableLatchLobbyKey) )
                          && ( enActiveFire == SMOKE_FIRE_RECALL ) );

   /* Reassess if the current recall floor is the main floor and the main recall floor should not be latched */
   bReassessActiveSmoke |= ( ( Param_ReadValue_1Bit(enPARAM1__Fire__DisableLatchMainRecallFloor) )
                          && ( !GetEmergencyBit(EmergencyBF_FirePhaseI_RecallToAltFloor) ) );

   /* Reassess if smokes should not be latched and the active fire source is a smoke signal */
   bReassessActiveSmoke |= ( ( Param_ReadValue_1Bit(enPARAM1__Fire__DisableLatchSmokes) )
                          && ( enActiveFire != SMOKE_NO_SMOKE_ACTIVE )
                          && ( enActiveFire != SMOKE_FIRE_RECALL ) );

   // After power on check if there were saved smoke settings
   if( ( enActiveFire == SMOKE_NO_SMOKE_ACTIVE )
    && ( GetEmergencyBit( EmergencyBF_FirePhaseI_Active ) )
    && ( ucRecentSmokeReset ) )
   {
      ucRecentSmokeReset--;
      if( !ucRecentSmokeReset )
      {
         enActiveFire = SMOKE_SAVED_SMOKE;

         if (enActiveFire)
         {
            gbInCarFireLamp = 1;
            //Set Phase 1 active flag
            SetEmergencyBit(EmergencyBF_FirePhaseI_Active);
            //if this doesn't return the Saved_smoke_state (power cycle after a fire has been activated) it sets the defaults based on the smoke
            if (enActiveFire != SMOKE_SAVED_SMOKE)
            {
               SetActiveSmokeSettings();
            }
         }
         else
         {
            ClrEmergencyBit(EmergencyBF_FirePhaseI_ArmReset); //Clear Reset Flag
//            ClrEmergencyBit(EmergencyBF_FirePhaseII_Active);  //Clear Phase 2 active Flag // todo rm?
         }
      }
   }
   else if( bReassessActiveSmoke )
   {
      enActiveFire = CheckForActiveSmoke();

      if (enActiveFire)
      {
         gbInCarFireLamp = 1;
         //Set Phase 1 active flag
         SetEmergencyBit(EmergencyBF_FirePhaseI_Active);
         //if this doesn't return the Saved_smoke_state (power cycle after a fire has been activated) it sets the defaults based on the smoke
         if (enActiveFire != SMOKE_SAVED_SMOKE)
         {
            SetActiveSmokeSettings();
         }
      }
      else
      {
         ClrEmergencyBit(EmergencyBF_FirePhaseI_ArmReset); //Clear Reset Flag
//         ClrEmergencyBit(EmergencyBF_FirePhaseII_Active);  //Clear Phase 2 active Flag
      }
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

-----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   gbLobbyFireLamp = 0;
   gbShuntFlag = 0;
   pstThisModule->uwInitialDelay_1ms = 2000;
   pstThisModule->uwRunPeriod_1ms = 50;

   enActiveFire = SMOKE_NO_SMOKE_ACTIVE;
   enFirePhase2 = FIRE_II__OFF;
   return 0;
}


static uint32_t Run( struct st_module *pstThisModule )
{
   //Flag for setting up fire service bits
   static uint8_t ucKeySwapTimeout = 0;



   if ( uwStartupTimeout_50ms )
   {
      uwStartupTimeout_50ms--;

      //This section covers resetting fire from a dip 1 reset.
      if ( ( GetFault_ByRange(FLT__CPU_STOP_SW_MRA, FLT__CPU_STOP_SW_COPB) )
        || ( SRU_Read_DIP_Switch( enSRU_DIP_A1 ) ) )
      {
         ResetFireService();
      }
   }
   else if( ( GetOperation_ManualMode() != MODE_M__CONSTRUCTION )
         && ( FRAM_EmergencyBitmapReady() ) )
   {
      Fire_UpdateVirtualKeyswitches();

      UpdateActiveFireSignal();

      if( ( GetEmergencyBit( EmergencyBF_FirePhaseI_Active ) )
       || ( enActiveFire != SMOKE_NO_SMOKE_ACTIVE ) )
      {
         //watch for Reset
         if( Param_ReadValue_1Bit( enPARAM1__Fire__ResetOnTransitionFire1 ) )
         {
            if( enRecallKeySwitch == KEY_ON )
            {
               ucCheckReset = 0;
            }
            // checks if reset is used, and sets the reset flag
            else if ( ( ( enRecallKeySwitch == KEY_RESET ) || ( Param_ReadValue_1Bit( enPARAM1__Fire__ResetToExitPhase1 ) == 0) )
                  && ( ( Param_ReadValue_1Bit( enPARAM1__Fire__AllowResetWithActiveSmoke ) ) || ( CheckForActiveSmoke() == SMOKE_NO_SMOKE_ACTIVE ) ) && !ucCheckReset )
            {
               ucCheckReset = 1;
            }
            // checks if off is high, and reset flag is 1, then reset fire.
            else if (( enRecallKeySwitch == KEY_OFF ) && (!Fire_GetDebouncedInput(FIRE_INPUT__KEY_RECALL_RESET)) && ucCheckReset )
            {
               SetEmergencyBit(EmergencyBF_FirePhaseI_ArmReset);
               ucCheckReset = 0;
            }
         }
         else
         {
            if ( ( ( enRecallKeySwitch == KEY_RESET ) || ( Param_ReadValue_1Bit( enPARAM1__Fire__ResetToExitPhase1 ) == 0) )
              && ( ( Param_ReadValue_1Bit( enPARAM1__Fire__AllowResetWithActiveSmoke ) ) || ( CheckForActiveSmoke() == SMOKE_NO_SMOKE_ACTIVE ) ) )
            {
               SetEmergencyBit(EmergencyBF_FirePhaseI_ArmReset);
            }
         }

      }
      uint8_t bPhase2Active = ( enFirePhase2 != FIRE_II__OFF ) || ( GetEmergencyBit( EmergencyBF_FirePhaseII_Active ) );
      if( ( enActiveFire != SMOKE_NO_SMOKE_ACTIVE ) //Active Fire Service
       || ( bPhase2Active ) )
      {
         CheckHatFlashUpdate();

         //CHECK IF I SHOULD UPDATE TO THE MAIN RECALL FLOOR OR NOT.
         if( GetEmergencyBit( EmergencyBF_FirePhaseI_RecallToAltFloor ) )
         {
            //If main recall switch is on, then check if we can goto main now, or if we need remote key as well
            if ( ( enRecallKeySwitch == KEY_ON )
              && ( ( !Param_ReadValue_1Bit( enPARAM1__Fire__RemoteAndMainToOverrideSmoke ) )
                || ( enRemoteKeySwitch == KEY_ON ) ) )
            {
               ClrEmergencyBit(EmergencyBF_FirePhaseI_RecallToAltFloor);
            }
         }

         Fire_UpdateStatePhase2();
      }

      UpdateFireResetNotInFireOperation();
      UpdateFireLampAndBuzzer();
      UpdateBuzzer();

      Fire_UpdateSystemAlarm();
   }
   Fire_UpdateDebouncedInputs();
   /* Bypass fire srv enforcement */
   if(!SRU_Read_DIP_Switch(enSRU_DIP_B6)
   && Param_ReadValue_1Bit(enPARAM1__BypassFireSrv))
   {
      Param_WriteValue_1Bit(enPARAM1__BypassFireSrv, 0);
   }
   if(GetBypassFireService())
   {
      ResetFireService();
   }

   /* Pelle door operator outputs */
   SetOutputValue(enOUT_FIRE_I_ACTIVE, Fire_CheckIfFire1Recall() );
   SetOutputValue(enOUT_FIRE_II_ACTIVE, Fire_CheckIfFire2DoorsEnabled() );
   SetOutputValue(enOUT_FIRE_II_HOLD, ( enFirePhase2 == FIRE_II__HOLD ) && ( GetOperation_ClassOfOp() == CLASSOP__AUTO ));

   Fire_UpdateAttendantFireRecallDelay();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
