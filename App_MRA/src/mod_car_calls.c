/******************************************************************************
 *
 * @file
 * @brief
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_a.h"
#include <stdint.h>
#include <stdlib.h>
#include "sys.h"
#include "operation.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

static uint8_t CheckIfAccessCodeCorrect( uint8_t ucFloorIndex, uint8_t ucRear );
static void CheckAccessCode( uint8_t bLastButtonPress, uint8_t ucFloorIndex, enum en_doors enDoorIndex, struct st_carcalls* gpastCarCalls );
static void AccessCode_Start( uint8_t bLastButtonPress, uint8_t ucFloorIndex, enum en_doors enDoorIndex, struct st_carcalls* gpastCarCalls );
static void AccessCode_CCBRecord( uint8_t bLastButtonPress, uint8_t ucFloorIndex, enum en_doors enDoorIndex, struct st_carcalls* gpastCarCalls );
static void AccessCode_CheckPattern( uint8_t bLastButtonPress, uint8_t ucFloorIndex, enum en_doors enDoorIndex, struct st_carcalls* gpastCarCalls );

static void CheckCCAcknowledge( uint8_t bLastButtonPress, uint8_t ucFloorIndex, struct st_carcalls* gpastCarCalls );
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_CarCalls =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

struct st_carcalls * gpastCarCalls_F;
struct st_carcalls * gpastCarCalls_R;

uint32_t gaulCCB_Latched[ NUM_OF_DOORS ][BITMAP32_SIZE(MAX_NUM_FLOORS)];

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define INVALID_FLOOR_ACCESS_CODE 255
#define MOD_RUN_TIME 100
#define MAX_ACCESS_CODE_FLOORS 8
#define MAX_NUM_OF_ENTRIES 4

#define MAX_FLASHES_CC_ACK 3
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
enum en_access_code_states
{
   ACCESS_CODE_START,
   ACCESS_CODE_CCB_RECORD,
   ACCESS_CODE_CHECK_PATTERN,

   ACCESS_CODE_NUMOFSTATES
};
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t ucNumOpensWithoutPHE;
static uint8_t ucNumLatchedCarCalls;
static uint32_t aauiBF_LastButtonPress[ NUM_OF_DOORS ][ BITMAP32_SIZE(MAX_NUM_FLOORS) ];
static uint32_t aauiBF_CurrentButtonPress[ NUM_OF_DOORS ][ BITMAP32_SIZE(MAX_NUM_FLOORS) ];

static uint16_t uiAccessCode_ButtonsPressed = 0;
static uint8_t ucFirstCCBPressed = INVALID_FLOOR_ACCESS_CODE;
static enum en_doors ucFirstCCBPressed_Door = NUM_OF_DOORS;
static uint16_t ucLampTimer_1ms;
static uint16_t ucCCBTimer = 0xFFFF;
static enum en_access_code_states enAccessCodeState = ACCESS_CODE_START;
static uint8_t ucAccessCodeCounter = 0;

static uint16_t uwTimeout_CCAck_1MS;
static uint8_t ucCCBLatched_CCAck;
static uint8_t ucFlashCount_CCAck;
static uint8_t ucMaxFlashCount_CCAck;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/\
 /*-----------------------------------------------------------------------------
   Updates variable storing the current number of latched CCs
 -----------------------------------------------------------------------------*/
static void UpdateNumberOfLatchedCarCalls(void)
{
    ucNumLatchedCarCalls = 0;
    for(uint8_t i = 0; i < GetFP_NumFloors(); i++)
    {
       if( gpastCarCalls_F[i].bLatched )
       {
          ucNumLatchedCarCalls++;
       }
       if( GetFP_RearDoors() && gpastCarCalls_R[i].bLatched )
       {
          ucNumLatchedCarCalls++;
       }
    }
}
/*-----------------------------------------------------------------------------
   Checks if the number of latched car calls exceeds the max calls allowed
   for the car's current weight reading.
   If exceeding the max limit, all car calls are cleared.
 -----------------------------------------------------------------------------*/
static void AntiNuisance_CheckForMaxCalls_ByWeight(void)
{
    uint8_t ucCallLimitPer250lb = Param_ReadValue_8Bit(enPARAM8__AN_MaxCarCallsPer_250lb);
    en_lws eType = Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect);
    if( ( GetOperation_AutoMode() == MODE_A__NORMAL )
     && ( ucCallLimitPer250lb )
     && ( eType != LWS__DISCRETE ) )
    {
       uint16_t uwWeight_lb = GetInCarWeight_lb();
       uint8_t ucTotalCallLimit = ( ( uwWeight_lb/250 ) + 1 ) * ucCallLimitPer250lb;

       if( ucNumLatchedCarCalls > ucTotalCallLimit )
       {
          ClearLatchedCarCalls();
       }
    }
}
/*-----------------------------------------------------------------------------
   Checks if the car has come into a floor and failed without detecting a PHE break
   If exceeding the max limit, all car calls are cleared.
 -----------------------------------------------------------------------------*/
void AntiNuissance_CheckMaxOpensWithoutPHE(void)
{
   static uint8_t bArmed;
   uint8_t ucMaxOpensWithoutPHE = Param_ReadValue_8Bit(enPARAM8__AN_MaxOpensWithoutPHE);

   if( ( GetOperation_AutoMode() == MODE_A__NORMAL )
    && ( ucMaxOpensWithoutPHE ) )
   {
      uint8_t bAnyDOL = !GetInputValue(enIN_DOL_F) || ( GetFP_RearDoors() && !GetInputValue(enIN_DOL_R) );
      if( !getDoorZone(DOOR_ANY) )
      {
         bArmed = 1;
      }
      if( bAnyDOL )
      {
         /* Only increment on first opening instance at a floor */
         if( bArmed )
         {
            bArmed = 0;
            if( ucNumOpensWithoutPHE < ucMaxOpensWithoutPHE )
            {
               ucNumOpensWithoutPHE++;
            }
         }
         uint8_t bAnyPHE = !GetInputValue(enIN_PHE_F) || ( GetFP_RearDoors() && !GetInputValue(enIN_PHE_R) );
         if( bAnyPHE )
         {
            ucNumOpensWithoutPHE = 0;
         }
      }

      if( ucNumOpensWithoutPHE >= ucMaxOpensWithoutPHE )
      {
         ClearLatchedCarCalls();
         ucNumOpensWithoutPHE = 0;
      }
   }
   else
   {
      ucNumOpensWithoutPHE = 0;
      bArmed = 0;
   }
}
/*-----------------------------------------------------------------------------
   Checks if the car should clear out car calls if X number of calls
   are latched in Light Load
 -----------------------------------------------------------------------------*/
static void AntiNuisance_CheckMaxLatchedCalls_LightLoad( void )
{
   uint8_t ucLightLoadMaxCarCalls = Param_ReadValue_8Bit(enPARAM8__AN_MaxCarCallsLightLoad);
   if( ( GetOperation_AutoMode() == MODE_A__NORMAL )
    && ( ucLightLoadMaxCarCalls )
    && ( GetLightLoadFlag() ) )
   {
      if( ucNumLatchedCarCalls > ucLightLoadMaxCarCalls )
      {
         ClearLatchedCarCalls();
      }
   }
}
/*-----------------------------------------------------------------------------
   Clears out car calls that are in the direction opposite the car's current movement direction.
   For exampl, a car moving up will have all the car calls below it cleared.
 -----------------------------------------------------------------------------*/
static void AntiNuisance_ClearReverseDirectionCarCalls( void )
{
   if( ( Param_ReadValue_1Bit(enPARAM1__AN_ClrReverseDirCC) )
    && ( GetOperation_AutoMode() == MODE_A__NORMAL ) )
   {
      enum direction_enum eDir = GetMotion_Direction();
      if( eDir == DIR__UP )
      {
         for( uint8_t ucFloor = 0; ucFloor < GetOperation_CurrentFloor(); ucFloor++ )
         {
            gpastCarCalls_F[ucFloor].bLatched = 0;

            if (GetFP_RearDoors()) {
              gpastCarCalls_R[ucFloor].bLatched = 0;
            }
         }
      }
      else if( eDir == DIR__DN )
      {
         for( uint8_t ucFloor = GetOperation_CurrentFloor()+1; ucFloor < GetFP_NumFloors(); ucFloor++ )
         {
            gpastCarCalls_F[ucFloor].bLatched = 0;

            if (GetFP_RearDoors()) {
              gpastCarCalls_R[ucFloor].bLatched = 0;
            }
         }
      }
   }
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
uint8_t LatchCarCall ( uint8_t ucFloorIndex, uint8_t ucDoorIndex)
{
   if (ucDoorIndex && GetFP_RearDoors() &&
       gpastCarCalls_R[ ucFloorIndex ].bValidOpening)
   {
      gpastCarCalls_R[ ucFloorIndex ].bLatched = 1;
   }
   else if (ucDoorIndex == 0 &&
         gpastCarCalls_F[ ucFloorIndex ].bValidOpening)
   {
      gpastCarCalls_F[ ucFloorIndex ].bLatched = 1;
   }

   return 1;
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void SetOpeningMap ( void )
{
   uint8_t ucBlock = 0;
   uint8_t ucIndex = 0;

   for (uint8_t ucFloorIndex = 0; ucFloorIndex < GetFP_NumFloors(); ucFloorIndex++ )
   {
      ucBlock = (ucFloorIndex/32);
      ucIndex = (ucFloorIndex%32);

      gpastCarCalls_F[ ucFloorIndex ].bValidOpening =
         (Param_ReadValue_32Bit(enPARAM32__OpeningBitmapF_0 + ucBlock) >> ucIndex) & 1;

      gpastCarCalls_F[ ucFloorIndex ].bSecured =
         (Param_ReadValue_32Bit(enPARAM32__SecureKeyedBitmapF_0 + ucBlock) >> ucIndex) & 1;

      if ( GetFP_RearDoors() )
      {

         gpastCarCalls_R[ ucFloorIndex ].bValidOpening =
            (Param_ReadValue_32Bit(enPARAM32__OpeningBitmapR_0 + ucBlock) >> ucIndex) & 1;

         gpastCarCalls_R[ ucFloorIndex ].bSecured =
            (Param_ReadValue_32Bit(enPARAM32__SecureKeyedBitmapR_0 + ucBlock) >> ucIndex) & 1;
      }
   }
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void SetLatchedCarCall( uint8_t ucFloorIndex, enum en_doors enDoorIndex, struct st_carcalls* gpastCarCalls) {
   /* Valid opening,
    * in Auto Operation
    * not being ignored by mode of operation,
    * and is pressed
    * AND (is not secured or is set to ignore security)
    */
   uint8_t bLastButtonPress = Sys_Bit_Get_Array32( &aauiBF_LastButtonPress[ enDoorIndex ][ 0 ],
                                                   ucFloorIndex,
                                                   ( 4 * BITMAP32_SIZE(MAX_NUM_FLOORS) ) );
   uint8_t bCarCallEnable = GetCarCallEnable( ucFloorIndex, enDoorIndex );

   uint16_t uwMaxCCBTime_1ms = Param_ReadValue_8Bit(enPARAM8__AccessCode_CCB_Time_1s)*1000;

   /* Validate mode, opening and mode rules */
   if( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
    && ( ( ( enDoorIndex == DOOR_FRONT ) && gpastFloors[ ucFloorIndex ].bFrontOpening && !gstCurrentModeRules[GetOperation_AutoMode()].bIgnoreCarCall_F )
      || ( ( enDoorIndex == DOOR_REAR  ) && gpastFloors[ ucFloorIndex ].bRearOpening && !gstCurrentModeRules[GetOperation_AutoMode()].bIgnoreCarCall_R ) )
    )
   {
      CheckAccessCode( bLastButtonPress, ucFloorIndex, enDoorIndex, gpastCarCalls );
      //logic for latching car calls separate from access code
      if( ucFirstCCBPressed == INVALID_FLOOR_ACCESS_CODE )
      {
         /* Detect only the rising edge of a button press signal & security */
         if( ( !bLastButtonPress )
          && ( gpastCarCalls[ucFloorIndex].bButton_CurrentPress )
          && ( !gpastCarCalls[ucFloorIndex].bSecured || gstCurrentModeRules[GetOperation_AutoMode()].bIgnoreCarCallSecurity ) )
         {
            if(CheckInRequired(ucFloorIndex, enDoorIndex) && !gstCurrentModeRules[GetOperation_AutoMode()].bIgnoreCarCallSecurity)
            {
               ucFloorIndex = Param_ReadValue_8Bit(enPARAM8__CheckInFloor);
            }
            gpastCarCalls[ ucFloorIndex ].bLatched = 1;
            ucCCBLatched_CCAck = 1;
         }
         else if( ( Param_ReadValue_1Bit(enPARAM1__EnableLatchesCC) )
               && ( !bLastButtonPress )
               && ( bCarCallEnable ) )
         {
            gpastCarCalls[ ucFloorIndex ].bLatched = 1;
            ucCCBLatched_CCAck = 1;
         }
         CheckCCAcknowledge( bLastButtonPress,ucFloorIndex, gpastCarCalls );
      }
   }

   if ( gpastCarCalls[ ucFloorIndex ].bLatched
     || gpastCarCalls[ ucFloorIndex ].bButton_CurrentPress
      )
   {
      gpastCarCalls[ ucFloorIndex ].bLamp = 1;
   }
   else
   {
      gpastCarCalls[ ucFloorIndex ].bLamp = 0;
   }

   //Inverts the logic of the CCL every cycle as long as a valid access code CCB was initially pressed
   if( ( ucCCBTimer < uwMaxCCBTime_1ms ) //ks note: can be placed as the first in a series of if/elseif statements before line 414.
         && ( ucFirstCCBPressed == ucFloorIndex )
         && ( ( ucFirstCCBPressed_Door == DOOR_REAR) || ( ucFirstCCBPressed_Door == DOOR_FRONT ) ) )
   {
      if( ( ucLampTimer_1ms > MOD_RUN_TIME ) ) //ks note: use #define
      {
         if( ucFirstCCBPressed_Door == DOOR_REAR )
         {
            gpastCarCalls_R[ucFirstCCBPressed].bLamp = ~gpastCarCalls_R[ucFirstCCBPressed].bLamp;
         }
         else if( ucFirstCCBPressed_Door == DOOR_FRONT )
         {
            gpastCarCalls_F[ucFirstCCBPressed].bLamp = ~gpastCarCalls_F[ucFirstCCBPressed].bLamp;
         }
         ucLampTimer_1ms = 0;
      }
      else
      {
         ucLampTimer_1ms += MOD_RUN_TIME; //ks note: use #define
      }
   }
   else if( ( ucCCBTimer >= uwMaxCCBTime_1ms )
         || ( ucFirstCCBPressed == INVALID_FLOOR_ACCESS_CODE )
         || ( ucFirstCCBPressed_Door == NUM_OF_DOORS ) )
   {
      ucLampTimer_1ms = 0;
   }

   Sys_Bit_Set_Array32( &aauiBF_LastButtonPress[ enDoorIndex ][ 0 ],
                        ucFloorIndex,
                        ( 4 * BITMAP32_SIZE(MAX_NUM_FLOORS) ),
                        gpastCarCalls[ucFloorIndex].bButton_CurrentPress );
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static uint8_t CheckIfAccessCodeCorrect( uint8_t ucFloorIndex, uint8_t ucRear )
{
   //ks note: if the code 0x0000 is a floor with security disabled then it should be covered.
   //ks note: on first button press reset static variable that tracks the number of access code digits recieved. only when the number of digits recieved equals the expected number, should the uiAccessCode_ButtonsPressed value be compared to the parameter vaalue for that floor.
   uint8_t bCorrect = 0;
if( ( ucFloorIndex != INVALID_FLOOR_ACCESS_CODE ) && ( ucFloorIndex < 8 ) )
{
   if( ucRear && GetFP_RearDoors() )
   {
      if( uiAccessCode_ButtonsPressed == Param_ReadValue_16Bit(enPARAM16__AccessCodeFloor_1R + ucFloorIndex) )
      {
         bCorrect = 1;
      }
   }
   else
   {
      if( uiAccessCode_ButtonsPressed == Param_ReadValue_16Bit(enPARAM16__AccessCodeFloor_1F + ucFloorIndex) )
      {
         bCorrect = 1;
      }
   }
}

   return bCorrect;
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void AccessCode_Start( uint8_t bLastButtonPress, uint8_t ucFloorIndex, enum en_doors enDoorIndex, struct st_carcalls* gpastCarCalls )
{
   uint16_t uwMaxCCBTime_1ms = Param_ReadValue_8Bit(enPARAM8__AccessCode_CCB_Time_1s)*1000;

   //checks if the enable bit for access code for rear floors, if true, initializes state/variables
   if( ( enDoorIndex == DOOR_REAR )
         && Param_ReadValue_16Bit(enPARAM16__AccessCodeFloor_1R + ucFloorIndex ) )
   {
      /* Detect only the rising edge of a button press signal */
      if( ( !bLastButtonPress )
            && ( gpastCarCalls[ucFloorIndex].bButton_CurrentPress ) )
      {
         if( ucFirstCCBPressed == INVALID_FLOOR_ACCESS_CODE )
         {
            ucFirstCCBPressed = ucFloorIndex;
            ucFirstCCBPressed_Door = DOOR_REAR;
            ucCCBTimer = 0;
            enAccessCodeState = ACCESS_CODE_CCB_RECORD;
            uiAccessCode_ButtonsPressed = 0;
         }
      }
   }
   //checks if the enable bit for access code for front floors, if true, initializes state/variables
   else if( ( enDoorIndex == DOOR_FRONT )
         && Param_ReadValue_16Bit(enPARAM16__AccessCodeFloor_1F + ucFloorIndex ) )
   {
      if( ( !bLastButtonPress )
            && ( gpastCarCalls[ucFloorIndex].bButton_CurrentPress ) )
      {
         if( ucFirstCCBPressed == INVALID_FLOOR_ACCESS_CODE )
         {
            ucFirstCCBPressed = ucFloorIndex;
            ucFirstCCBPressed_Door = DOOR_FRONT;
            ucCCBTimer = 0;
            enAccessCodeState = ACCESS_CODE_CCB_RECORD;
            uiAccessCode_ButtonsPressed = 0;
         }
      }
   }
   else
   {
      enAccessCodeState = ACCESS_CODE_START;
      ucFirstCCBPressed = INVALID_FLOOR_ACCESS_CODE;
      uiAccessCode_ButtonsPressed = 0;
      ucFirstCCBPressed_Door = NUM_OF_DOORS;
      ucCCBTimer = 0xFFFF;
      ucAccessCodeCounter = 0;
   }
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void AccessCode_CCBRecord( uint8_t bLastButtonPress, uint8_t ucFloorIndex, enum en_doors enDoorIndex, struct st_carcalls* gpastCarCalls )
{
   uint16_t uwMaxCCBTime_1ms = Param_ReadValue_8Bit(enPARAM8__AccessCode_CCB_Time_1s)*1000;

   if( ucCCBTimer >= uwMaxCCBTime_1ms)
   {
      enAccessCodeState = ACCESS_CODE_START;
   }
   else if( ( ucFirstCCBPressed == ucFloorIndex ) && ( ucFirstCCBPressed_Door == enDoorIndex ) )
   {
      ucCCBTimer += MOD_RUN_TIME;//ks note: use #define, and wrong value
      enAccessCodeState = ACCESS_CODE_CCB_RECORD;
      if( ucAccessCodeCounter == MAX_NUM_OF_ENTRIES )
      {
         enAccessCodeState = ACCESS_CODE_CHECK_PATTERN;
      }
   }
   //On rising edge, checks CCB and shifts them to the left by 4
   //for case when CCB is rear, adds an offset of 8
   if( ( !bLastButtonPress )
         && ( gpastCarCalls[ucFloorIndex].bButton_CurrentPress ) )
   {
      //ks note: use #define
      if( ucFloorIndex < MAX_ACCESS_CODE_FLOORS )
      {
         if( enDoorIndex == DOOR_REAR )
         {
            uiAccessCode_ButtonsPressed = ( uiAccessCode_ButtonsPressed << 4 );
            uiAccessCode_ButtonsPressed |= ( ucFloorIndex + MAX_ACCESS_CODE_FLOORS ) & 0xF; //ks note: use #define
         }
         else if( enDoorIndex == DOOR_FRONT )
         {
            uiAccessCode_ButtonsPressed = ( uiAccessCode_ButtonsPressed << 4 );
            uiAccessCode_ButtonsPressed |= ucFloorIndex & 0xF;
         }
         ucAccessCodeCounter += 1;
         ucCCBTimer = 0;
         enAccessCodeState = ACCESS_CODE_CCB_RECORD;
         if( ucAccessCodeCounter == MAX_NUM_OF_ENTRIES )
         {
            enAccessCodeState = ACCESS_CODE_CHECK_PATTERN;
         }
      }
      else
      {
         ucAccessCodeCounter += 1;
         ucCCBTimer += MOD_RUN_TIME;//ks note: use #define, and wrong value
         enAccessCodeState = ACCESS_CODE_CCB_RECORD;
         if( ucAccessCodeCounter == MAX_NUM_OF_ENTRIES )
         {
            enAccessCodeState = ACCESS_CODE_CHECK_PATTERN;
         }
      }
   }
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void AccessCode_CheckPattern( uint8_t bLastButtonPress, uint8_t ucFloorIndex, enum en_doors enDoorIndex, struct st_carcalls* gpastCarCalls )
{
   uint16_t uwMaxCCBTime_1ms = Param_ReadValue_8Bit(enPARAM8__AccessCode_CCB_Time_1s)*1000;

   if( ucCCBTimer >= uwMaxCCBTime_1ms)
   {
      enAccessCodeState = ACCESS_CODE_START;
   }
   else if( ( ucFirstCCBPressed == ucFloorIndex ) && ( ucFirstCCBPressed_Door == enDoorIndex ) )
   {
      ucCCBTimer += MOD_RUN_TIME;//ks note: use #define, and wrong value
      enAccessCodeState = ACCESS_CODE_CCB_RECORD;
   }

   if( ucCCBTimer >= uwMaxCCBTime_1ms)
   {
      enAccessCodeState = ACCESS_CODE_START;
   }
   //checks if CCB pattern matches saved access code
   //if true, latches the CC and resets state/variables
   else if( ucAccessCodeCounter == MAX_NUM_OF_ENTRIES )
   {
      if( CheckIfAccessCodeCorrect( ucFirstCCBPressed, ucFirstCCBPressed_Door ) )
      {
         if( ucFirstCCBPressed_Door == DOOR_REAR )
         {
            gpastCarCalls_R[ucFirstCCBPressed].bLatched = 1;
         }
         else if( ucFirstCCBPressed_Door == DOOR_FRONT )
         {
            gpastCarCalls_F[ucFirstCCBPressed].bLatched = 1;
         }
         enAccessCodeState = ACCESS_CODE_START;
      }

         enAccessCodeState = ACCESS_CODE_START;

   }

}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void CheckAccessCode( uint8_t bLastButtonPress, uint8_t ucFloorIndex, enum en_doors enDoorIndex, struct st_carcalls* gpastCarCalls )
{
   switch( enAccessCodeState )
   {
      case ACCESS_CODE_START:
         AccessCode_Start( bLastButtonPress, ucFloorIndex, enDoorIndex, gpastCarCalls );
         break;
      case ACCESS_CODE_CCB_RECORD:
         AccessCode_CCBRecord( bLastButtonPress,ucFloorIndex, enDoorIndex, gpastCarCalls );
         break;
      case ACCESS_CODE_CHECK_PATTERN:
         AccessCode_CheckPattern( bLastButtonPress,ucFloorIndex, enDoorIndex, gpastCarCalls );
         break;
      default:
         enAccessCodeState = ACCESS_CODE_START;
         break;
   }
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void CheckCCAcknowledge( uint8_t bLastButtonPress, uint8_t ucFloorIndex, struct st_carcalls* gpastCarCalls )
{
   if( Param_ReadValue_1Bit(enPARAM1__CC_Acknowledge) )
   {
      if( ucCCBLatched_CCAck )
      {
         ucMaxFlashCount_CCAck = 1;
      }
      else if( !bLastButtonPress && gpastCarCalls[ucFloorIndex].bButton_CurrentPress )
      {
         ucMaxFlashCount_CCAck = 3;
      }
   }
}
/*-----------------------------------------------------------------------------

-----------------------------------------------------------------------------*/
static void CCAcknowledgeOutput( void )
{
   if(ucMaxFlashCount_CCAck)
   {
      if( uwTimeout_CCAck_1MS >= MOD_RUN_TIME)
      {
         SetOutputValue(enOUT_CC_ACKNOWLEDGE, 0);
         ucFlashCount_CCAck++;
         if( ucFlashCount_CCAck == ucMaxFlashCount_CCAck )
         {
            ucCCBLatched_CCAck = 0;
            ucFlashCount_CCAck = 0;
            ucMaxFlashCount_CCAck = 0;
         }
         else if( ucFlashCount_CCAck > MAX_FLASHES_CC_ACK )
         {
            ucCCBLatched_CCAck = 0;
            ucFlashCount_CCAck = 0;
            ucMaxFlashCount_CCAck = 0;
         }
         uwTimeout_CCAck_1MS = 0;
      }
      else
      {
         SetOutputValue(enOUT_CC_ACKNOWLEDGE, 1);
         uwTimeout_CCAck_1MS += MOD_RUN_TIME;
      }
   }
   else
   {
      SetOutputValue(enOUT_CC_ACKNOWLEDGE, 0);
      ucCCBLatched_CCAck = 0;
      ucFlashCount_CCAck = 0;
      ucMaxFlashCount_CCAck = 0;
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 100;
   pstThisModule->uwRunPeriod_1ms = 100;

   //prep opening into the opening Map
   SetOpeningMap();

   return 0;
}
/*-----------------------------------------------------------------------------

   Scan all CCBs and latch them if on MODE_A__NORMAL.
   Update CCLs

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   for ( uint8_t ucFloorIndex = 0; ucFloorIndex < GetFP_NumFloors(); ucFloorIndex++ )
   {
      SetLatchedCarCall(ucFloorIndex, DOOR_FRONT, gpastCarCalls_F);
      Sys_Bit_Set(&gaulCCB_Latched[ DOOR_FRONT ][0], ucFloorIndex, gpastCarCalls_F[ ucFloorIndex ].bLatched );
      Sys_Bit_Set( &aauiBF_CurrentButtonPress[ DOOR_FRONT ][ 0 ], ucFloorIndex, gpastCarCalls_F[ ucFloorIndex ].bButton_CurrentPress );
      if ( GetFP_RearDoors() )
      {
         SetLatchedCarCall(ucFloorIndex, DOOR_REAR, gpastCarCalls_R);
         Sys_Bit_Set(&gaulCCB_Latched[ DOOR_REAR ][0], ucFloorIndex, gpastCarCalls_R[ ucFloorIndex ].bLatched );
         Sys_Bit_Set( &aauiBF_CurrentButtonPress[ DOOR_REAR ][ 0 ], ucFloorIndex, gpastCarCalls_R[ ucFloorIndex ].bButton_CurrentPress );
      }
   }

   UpdateNumberOfLatchedCarCalls();

   AntiNuisance_CheckForMaxCalls_ByWeight();

   AntiNuissance_CheckMaxOpensWithoutPHE();

   AntiNuisance_CheckMaxLatchedCalls_LightLoad();

   AntiNuisance_ClearReverseDirectionCarCalls();

   CCAcknowledgeOutput();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
