/******************************************************************************
 *
 * @file     motion_speedLimit.c
 * @brief
 * @version  V1.00
 * @date     4 August, 2016
 *
 * @note     Functions handling motion when the speed limit is reduced mid run.
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"

#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "motion.h"
#include "pattern.h"
#include "mod_motion.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_motion gstMotion;
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
Midflight Speed Limit Change Protocol:

   New speed limit != Old speed limit && Max run speed > speed limit
      Accelerating
         If speed <= limit
            Set new max speed
            Set new cruising start/stop pos
            Set new decel pattern

         If speed > limit
            Set emergency decel pattern,
            Set new max speed
            Set new cruising start/stop pos
            Set new decel pattern

      Cruising
         If speed > limit
            Set emergency decel pattern,
            Set new max speed
            Set new cruising start/stop pos
            Set new decel pattern

      Decelerating
         Do nothing

 -----------------------------------------------------------------------------*/
uint8_t CheckIfReducedSpeedLimit( Motion_Control *pstMotionCtrl )
{
   uint8_t bReturn = 0;
   if( !pstMotionCtrl->bSpeedLimitChanged
  && ( pstMotionCtrl->uwMaxSpeedOfCurrentRun_FPM > GetOperation_SpeedLimit() )
  && ( (gstMotion.wSpeedCMD*gstMotion.cDirection) >= GetOperation_SpeedLimit() )
  )
   {
      pstMotionCtrl->bSpeedLimitChanged = 1;
      bReturn = 1;
   }
   return bReturn;
}

/*-----------------------------------------------------------------------------
Accelerating

   If speed > limit
      Set emergency decel pattern,
      Set new max speed
      Set new cruising start/stop pos
      Set new decel pattern

   If speed <= limit
      Set new max speed
      Add flag to set
      Set new cruising start/stop pos
      Set new decel pattern
 -----------------------------------------------------------------------------*/
void SpeedLimitChangeDuringAccel( Motion_Control *pstMotionCtrl )
{
   int16_t wSpeedCMD = GetMotion_SpeedCommand() * GetMotion_Direction();
   int16_t wSpeedLimit = GetOperation_SpeedLimit();
   if( wSpeedCMD > wSpeedLimit )
   {
      pstMotionCtrl->uwMaxSpeedOfCurrentRun_FPM = wSpeedLimit;
      Pattern_GenerateReducedSpdRun( wSpeedCMD/60.0f, wSpeedLimit/60.0f );
      Pattern_GenerateDecelRun(wSpeedLimit/60.0f);
      /* Change starting positions for reduced speed decel, cruising, and decel */

      pstMotionCtrl->stRunParameters.uiStartPosition_RSL = GetPosition_PositionCount();
      if( GetMotion_Direction() == DIR__UP )
      {
         // Todo add offset factor
         pstMotionCtrl->stRunParameters.uiStartPosition_Cruise = pstMotionCtrl->stRunParameters.uiStartPosition_RSL
                                                               + pstMotionCtrl->stRunParameters.uiRSLDecelDistance;
         pstMotionCtrl->stRunParameters.uiStartPosition_Decel = GetMotion_Destination()-pstMotionCtrl->stRunParameters.uiSlowdownDistance;
      }
      else
      {
         // Todo add offset factor
         pstMotionCtrl->stRunParameters.uiStartPosition_Cruise = pstMotionCtrl->stRunParameters.uiStartPosition_RSL
                                                               - pstMotionCtrl->stRunParameters.uiRSLDecelDistance;
         pstMotionCtrl->stRunParameters.uiStartPosition_Decel = GetMotion_Destination()+pstMotionCtrl->stRunParameters.uiSlowdownDistance;
      }
      pstMotionCtrl->uiPrevTime_RSLDecel = pstMotionCtrl->stRunParameters.uiRSLTime-1;

      pstMotionCtrl->eMotionState = MOTION__RSL_DECEL;
   }
   else
   {
      pstMotionCtrl->uwMaxSpeedOfCurrentRun_FPM = wSpeedLimit;
      Pattern_GenerateDecelRun(wSpeedLimit/60.0f);
      // Todo add offset factor
      pstMotionCtrl->stRunParameters.uiStartPosition_Cruise = GetPosition_PositionCount();
      pstMotionCtrl->stRunParameters.uiStartPosition_Decel = GetMotion_Destination()-pstMotionCtrl->stRunParameters.uiSlowdownDistance;
      pstMotionCtrl->eMotionState = MOTION__CRUISING;
   }
}

/*-----------------------------------------------------------------------------
Cruising
   If speed > limit
      Set emergency decel pattern,
      Set new max speed
      Set new cruising start/stop pos
      Set new decel pattern
 -----------------------------------------------------------------------------*/
void SpeedLimitChangeDuringCruising( Motion_Control *pstMotionCtrl )
{
   int16_t wSpeedCMD = GetMotion_SpeedCommand() * GetMotion_Direction();
   int16_t wSpeedLimit = GetOperation_SpeedLimit();
   if( wSpeedCMD > wSpeedLimit )
   {
      pstMotionCtrl->uwMaxSpeedOfCurrentRun_FPM = wSpeedLimit;
      Pattern_GenerateReducedSpdRun( wSpeedCMD/60.0f, wSpeedLimit/60.0f );
      Pattern_GenerateDecelRun(wSpeedLimit/60.0f);
      /* Change starting positions for reduced speed decel, cruising, and decel */
      pstMotionCtrl->stRunParameters.uiStartPosition_RSL = GetPosition_PositionCount();
      if( GetMotion_Direction() == DIR__UP )
      {
         // Todo add offset factor
         pstMotionCtrl->stRunParameters.uiStartPosition_Cruise = pstMotionCtrl->stRunParameters.uiStartPosition_RSL
                                                               + pstMotionCtrl->stRunParameters.uiRSLDecelDistance;
         pstMotionCtrl->stRunParameters.uiStartPosition_Decel = GetMotion_Destination()-pstMotionCtrl->stRunParameters.uiSlowdownDistance;
      }
      else
      {
         // Todo add offset factor
         pstMotionCtrl->stRunParameters.uiStartPosition_Cruise = pstMotionCtrl->stRunParameters.uiStartPosition_RSL
                                                               - pstMotionCtrl->stRunParameters.uiRSLDecelDistance;
         pstMotionCtrl->stRunParameters.uiStartPosition_Decel = GetMotion_Destination()+pstMotionCtrl->stRunParameters.uiSlowdownDistance;
      }
      pstMotionCtrl->uiPrevTime_RSLDecel = pstMotionCtrl->stRunParameters.uiRSLTime-1;

      pstMotionCtrl->eMotionState = MOTION__RSL_DECEL;
   }
   else
   {
      // Should not trigger
   }
}
/*-----------------------------------------------------------------------------
Reduced
 -----------------------------------------------------------------------------*/
void MotionState_ReducedSpeedLimit( Motion_Control *pstMotionCtrl )
{
   Motion_Profile eProfile = gstMotion.eProfile;
   enum direction_enum eDir = gstMotion.cDirection;
   uint32_t uiDestinationPosition = gstMotion.ulActualDest;
   uint32_t uiCruisePosition = pstMotionCtrl->stRunParameters.uiStartPosition_Cruise;
   uint32_t uiCurrentPosition = GetPosition_PositionCount();
   uint32_t uiTimeIndex = 0;
   gstMotion.bRunFlag = 1;

   if ( eDir == DIR__UP )
   {
      if  ( ( uiCurrentPosition < uiDestinationPosition )
         && ( uiCurrentPosition < uiCruisePosition ) )
      {
         for ( uiTimeIndex = pstMotionCtrl->uiPrevTime_RSLDecel; uiTimeIndex > 0 ; uiTimeIndex-- )
         {
            if ( ( uiCurrentPosition >= ( uiCruisePosition - Pattern_GetReducedSpdPosition( uiTimeIndex ) ) )
              && ( uiCurrentPosition <  ( uiCruisePosition - Pattern_GetReducedSpdPosition( uiTimeIndex - 1 ) ) ) )
            {
               pstMotionCtrl->uiPrevTime_RSLDecel = uiTimeIndex;
               break;
            }
         }

         gstMotion.wSpeedCMD = Pattern_GetReducedSpdCommandSpeed( pstMotionCtrl->uiPrevTime_RSLDecel );
      }
      else if( uiCurrentPosition < uiDestinationPosition )
      {
         pstMotionCtrl->eMotionState = MOTION__CRUISING;
      }
      else
      {
         pstMotionCtrl->eMotionState = MOTION__STOP_SEQUENCE;
      }
   }
   else /* DIR__DN */
   {
      if  ( ( uiCurrentPosition > uiDestinationPosition )
         && ( uiCurrentPosition > uiCruisePosition ) )
      {
         for ( uiTimeIndex = pstMotionCtrl->uiPrevTime_RSLDecel; uiTimeIndex > 0; uiTimeIndex-- )
         {
            if ( ( uiCurrentPosition <= ( uiCruisePosition + Pattern_GetReducedSpdPosition( uiTimeIndex ) ) )
              && ( uiCurrentPosition >  ( uiCruisePosition + Pattern_GetReducedSpdPosition( uiTimeIndex - 1 ) ) ) )
            {
               pstMotionCtrl->uiPrevTime_RSLDecel = uiTimeIndex;
               break;
            }
         }

         gstMotion.wSpeedCMD = -1 * Pattern_GetReducedSpdCommandSpeed( pstMotionCtrl->uiPrevTime_RSLDecel );
      }
      else if( uiCurrentPosition > uiDestinationPosition )
      {
         pstMotionCtrl->eMotionState = MOTION__CRUISING;
      }
      else
      {
         pstMotionCtrl->eMotionState = MOTION__STOP_SEQUENCE;
      }
   }

   pstMotionCtrl->wEndOfRunSpeed_FPM = gstMotion.wSpeedCMD;
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
