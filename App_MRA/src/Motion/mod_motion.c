/******************************************************************************
 *
 * @file     mod_motion.c
 * @brief
 * @version  V1.00
 * @date     23, March 2016
 *
 * @note    This module uses the motion commad received from MRA to determine
 *          the motion of the car, including the speed command and the srart
 *          and stop sequence.
 *
 *          When a motion command is received, the motion module generates a
 *          pattern based on the distance to be traveled, then goes through the
 *          following states in order: StartSequence, Accelerating, Cruising,
 *          Decelerating, StopSequence, then Stopped.
 *
 *          Inputs to this module are gstOperation.eCommand
 *                                    gstOperation.ulRequestedDest
 *                                    gstOperation.ulPosLimit_UP
 *                                    gstOperation.ulPosLimit_DN
 *                                    gstOperation.uwSpeedLimit
*
*           Outputs of this module are gpstMotionCtrl->stFlags.bPickM, gpstMotionCtrl->stFlags.bPickDrive, gpstMotionCtrl->stFlags.bPickBrake
*           and the motion structure, which contains the current speed, direction,
*           destination, and run flag.
*
*           gpstMotionCtrl->stFlags.bPickM, gpstMotionCtrl->stFlags.bPickDrive, and gpstMotionCtrl->stFlags.bPickBrake are used to time the
*           run and stop sequence.
*
*           The start sequence is as follows:
*           - gpstMotionCtrl->stFlags.bPickM is set (and used in the safety module to energize the M contactor)
*           - Wait for Motor Energize Delay to expire
*           - gpstMotionCtrl->stFlags.bPickDrive is set (and used in the drive module to command zero speed to the drive)
*           - Wait for Speed Reg Release flag from drive (no more than 3s, otherwise fault)
*           - Wait for Brake Pick Delay to expire (after bSpeedRegReleased is received)
*           - gpstMotionCtrl->stFlags.bPickBrake is set (and sent serially to MRA to output a discrete
*           Brake Pick output)
*           - Wait for Motor Run Delay to expire
*           - End the start squence. Command non-zero speed.
*
*           The stop sequence is as follows:
*           - Hold the zero speed as soon as the car arrives at intended position
*           - Wait for Brake Drop Delay to expire
*           - gpstMotionCtrl->stFlags.bPickBrake is cleared (brake will drop)
*           - Wait for Drive Drop Delay to expire (Continue to command zero speed)
*           - gpstMotionCtrl->stFlags.bPickDrive is cleared (serial drive run command is dropped)
*           - Wait for Motor Drop Delay to expire
*           - gpstMotionCtrl->stFlags.bPickM is cleared (Drop the M contactor)
*
*           Pattern:
*           The speed curve has 3 regions: jerk in
*                                          accel (linear speed)
*                                          jerk out
*           At the end of the jerk out region, the curve reaches contract speed.
*           If the required speed is below contract, or the distance to be traveled
*           is below the minumum distance needed to achieve contract, the linear
*           region is shortened until the desired distance/speed is obtained.
*           I call this a Short Pattern (same jerk in and jerk out as full pattern,
*           but linear region is shorter)
*           For even shorter speed/distance, and when the linear region is completely
*           eliminated, both the jerk in and jerk out regions will be shortened equally
*           and this pattern is called Vert Short Pattern.
*           The cutoff speeds and distances for the Full Pattern and Short Pattern
*           depend on the jerk, accel, and contract speed, and will be calculated
*           at startup. Anything below the Short Pattern speed/distance cutoff will
*           use the Very Short Pattern.
*           Switching between these three pattern profiles simply requires recalculating
*           the jerk in time, accel time, and jerk out time.
*           Full Pattern uses max jerk in time, max accel time, and max jerk out time
*           Short Pattern uses max jerk in time, shorter accel time, and max jerk out time
*           Very Short Pattern uses shorter jerk in time, zero accel time, and shorter jerk out time
*
*           When a destination is requested by MRA, the motion module calculates
*           the distance to be traveled, then calculates the max speed that could
*           be achieved during that distance. It then generates a pattern based on
*           that speed. The generated pattern is two arrays (of speeds and positions)
*           with respect to time.
*           During the acceleration state, the motion module uses the position feedback
*           to find the closest time index from the array gauiPattern_Position[].
*           It then uses that time index to determine what the next speed should be
*           in the array gauwPattern_Speed[].
*
*           NOTE: All JerkOutDecel parameters are referred to as JerkInDecel in this file, and vice versa.
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "sru_a.h"
#include <stdlib.h>
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "acceptanceTest.h"
#include "acceptance.h"
#include "pattern.h"
#define ARM_MATH

#ifdef ARM_MATH
#include "arm_math.h"
#else
#include <math.h>
#endif

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );
static uint8_t CheckIfEmergencyStop();
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Motion =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

struct st_motion gstMotion;

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static uint32_t guiSoftLimitPosition;
static uint16_t guwSoftLimitSpeed;
static uint8_t gbSoftLimitPatternMidflight;
static uint8_t gbSoftLimitPatternAtStart;

static uint8_t bCarStopped; //Car fully stopped

static uint8_t ucMaxFloorToFloorTime_s;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

Truncated accel Test function - TODO rm
 -----------------------------------------------------------------------------*/
#if 0
static void Test_TruncatedAccel()
{
#if 1
   static uint32_t uiTestDestination;
   static enum en_motion_state eLastState;
   if( gpstMotionCtrl->eMotionState == MOTION__ACCELERATING )
   {
      // index from jerk out accel to trigger enPARAM16__UNUSED_16BIT_1158
      if( ( gpstMotionCtrl->uiPrevTime > gpstMotionCtrl->stRunParameters.uiEndTime_JerkInAccel )
       && ( gpstMotionCtrl->uiPrevTime + Param_ReadValue_16Bit(enPARAM16__UNUSED_16BIT_1063) - 2 ) >= gpstMotionCtrl->stRunParameters.uiEndTime_ConstantAccel )
      {
         uint8_t ucNextFloor = Motion_GetNextReachableFloor();
         if( gstMotion.cDirection == DIR__UP )
         {
            if( ucNextFloor < GetOperation_DestinationFloor() && ucNextFloor < GetFP_NumFloors() )
            {
               LatchCarCall(ucNextFloor, 0);
               LatchCarCall(ucNextFloor, 1);
            }
         }
         else
         {
            if( ucNextFloor > GetOperation_DestinationFloor()  && ucNextFloor < GetFP_NumFloors() )
            {
               LatchCarCall(ucNextFloor, 0);
               LatchCarCall(ucNextFloor, 1);
            }
         }
      }
   }
   else if( gpstMotionCtrl->eMotionState == MOTION__STOPPED )
   {
      uiTestDestination = 0;
      if(eLastState != MOTION__STOPPED)
      {
         ClearLatchedCarCalls();
      }
   }
   eLastState = gpstMotionCtrl->eMotionState;
#else
   static uint32_t uiTestDestination;
   if( gpstMotionCtrl->eMotionState == MOTION__ACCELERATING )
   {
      // index from jerk out accel to trigger enPARAM16__UNUSED_16BIT_1158
      if( !uiTestDestination
     && ( gpstMotionCtrl->uiPrevTime > gpstMotionCtrl->stRunParameters.uiEndTime_JerkInAccel )
     && ( gpstMotionCtrl->uiPrevTime + Param_ReadValue_16Bit(enPARAM16__UNUSED_16BIT_1158) - 2 ) >= gpstMotionCtrl->stRunParameters.uiEndTime_ConstantAccel )
      {
         // distance from current position to change to. enPARAM16__UNUSED_16BIT_1159
         if( gstMotion.cDirection == DIR__UP )
         {
            uiTestDestination = GetPosition_PositionCount()+Param_ReadValue_16Bit(enPARAM16__UNUSED_16BIT_1159);
         }
         else
         {
            uiTestDestination = GetPosition_PositionCount()-Param_ReadValue_16Bit(enPARAM16__UNUSED_16BIT_1159);
         }
      }
   }
   else if( gpstMotionCtrl->eMotionState == MOTION__STOPPED )
   {
      uiTestDestination = 0;
   }

   if( uiTestDestination )
   {
      SetOperation_RequestedDestination( uiTestDestination );
   }
#endif
}
#endif
/*----------------------------------------------------------------------------
   Dispatching functions
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   Returns index of next reachable floor
 *----------------------------------------------------------------------------*/
uint8_t Motion_GetNextReachableFloor()
{
   uint8_t ucFloor = GetOperation_DestinationFloor();
   if( gpstMotionCtrl->eMotionState == MOTION__ACCELERATING )
   {
      if( gstMotion.cDirection == DIR__UP )
      {
         uint16_t uwAccelJerkOutSpeedChange = ( gpstMotionCtrl->stAccelVars.fEndSpeed_JerkOutAccel - gpstMotionCtrl->stAccelVars.fEndSpeed_ConstantAccel ) * 60.0f + 0.5f;
         uint16_t uwMinimumTopSpeed = 1.05 * ( ( gstMotion.wSpeedCMD * gstMotion.cDirection ) + uwAccelJerkOutSpeedChange );
         uint32_t uiReachablePos = gpstMotionCtrl->stRunParameters.uiStartPosition_Accel
                                 + 1.10 * ( Pattern_CalculateRampUpDistance( uwMinimumTopSpeed )
                                          + Pattern_CalculateSlowdownDistance( uwMinimumTopSpeed ) );
         for( int16_t i = GetOperation_CurrentFloor()-1; i < GetFP_NumFloors(); i++ )
         {
            if( i >= 0 )
            {
               if( uiReachablePos <= Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 + i ) )
               {
                  ucFloor = i;
                  break;
               }
            }
         }
      }
      else
      {
         uint16_t uwAccelJerkOutSpeedChange = ( gpstMotionCtrl->stAccelVars.fEndSpeed_JerkOutAccel - gpstMotionCtrl->stAccelVars.fEndSpeed_ConstantAccel ) * 60.0f + 0.5f;
         uint16_t uwMinimumTopSpeed = 1.05 * ( ( gstMotion.wSpeedCMD * gstMotion.cDirection ) + uwAccelJerkOutSpeedChange );
         uint32_t uiReachablePos = gpstMotionCtrl->stRunParameters.uiStartPosition_Accel
                                 - 1.10 * ( Pattern_CalculateRampUpDistance( uwMinimumTopSpeed )
                                          + Pattern_CalculateSlowdownDistance( uwMinimumTopSpeed ) );
         for( int16_t i = GetOperation_CurrentFloor()+1; i >= 0; i-- )
         {
            if( i < GetFP_NumFloors() )
            {
               if( uiReachablePos >= Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 + i ) )
               {
                  ucFloor = i;
                  break;
               }
            }
         }
      }
   }
#if !RTT_ENABLE_TEST_TRUNCATED_ACCEL_ONLY
   else if( gpstMotionCtrl->eMotionState == MOTION__CRUISING )
   {
      if( gstMotion.cDirection == DIR__UP )
      {
         uint32_t uiReachablePos = GetPosition_PositionCount() + 1.10 * Motion_GetSlowdownDistance();
         for( int16_t i = GetOperation_CurrentFloor()-1; i < GetFP_NumFloors(); i++ )
         {
            if( i >= 0 )
            {
               if( uiReachablePos <= Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 + i ) )
               {
                  ucFloor = i;
                  break;
               }
            }
         }
      }
      else
      {
         uint32_t uiReachablePos = GetPosition_PositionCount()
                                 - 1.10 * Motion_GetSlowdownDistance();
         for( int16_t i = GetOperation_CurrentFloor()+1; i >= 0; i-- )
         {
            if( i < GetFP_NumFloors() )
            {
               if( uiReachablePos >= Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 + i ) )
               {
                  ucFloor = i;
                  break;
               }
            }
         }
      }
   }
#endif
   else if( gpstMotionCtrl->eMotionState == MOTION__RSL_DECEL )
   {
      uint32_t uiRSLDecelDistance = Motion_GetRSLDistance();
      if( ( gstMotion.wSpeedCMD*gstMotion.cDirection ) <= GetOperation_SpeedLimit() )
      {
         uiRSLDecelDistance = 0;
      }
      //-----------------------------------------------
      if( gstMotion.cDirection == DIR__UP )
      {
         uint32_t uiReachablePos = GetPosition_PositionCount()
                                 + 1.10 * (Motion_GetSlowdownDistance() + uiRSLDecelDistance);
         for( int16_t i = GetOperation_CurrentFloor()-1; i < GetFP_NumFloors(); i++ )
         {
            if( i >= 0 )
            {
               if( uiReachablePos <= Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 + i ) )
               {
                  ucFloor = i;
                  break;
               }
            }
         }
      }
      else
      {
         uint32_t uiReachablePos = GetPosition_PositionCount()
                                 - 1.10 * (Motion_GetSlowdownDistance() + uiRSLDecelDistance);
         for( int16_t i = GetOperation_CurrentFloor()+1; i >= 0; i-- )
         {
            if( i < GetFP_NumFloors() )
            {
               if( uiReachablePos >= Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 + i ) )
               {
                  ucFloor = i;
                  break;
               }
            }
         }
      }
   }

   if( ( gstMotion.cDirection == DIR__UP )
    && ( ucFloor > GetOperation_DestinationFloor() ) )
   {
      ucFloor = GetOperation_DestinationFloor();
   }
   else if( ( gstMotion.cDirection == DIR__DN )
         && ( ucFloor < GetOperation_DestinationFloor() ) )
   {
      ucFloor = GetOperation_DestinationFloor();
   }

   return ucFloor;
}
/*-----------------------------------------------------------------------------
   For calculating time until arriving at destination (used in arrival lantern calc)
   Only valid during decel phase
 -----------------------------------------------------------------------------*/
uint8_t Motion_GetRemainingDecelTime()
{
   uint32_t uiSum_sec = 0;
   uint8_t ucTime_sec = 255;
   enum en_motion_state eMotionState = gpstMotionCtrl->eMotionState;
   if( ( eMotionState >= MOTION__DECELERATING )
    || ( eMotionState == MOTION__STOPPED ) )
   {
      /* Time left in ramp down */
      float fDecelTime_sec = gpstMotionCtrl->uiPrevTime * gpstMotionCtrl->flPatternTimeRes_sec;
      float fLevelingTime_sec = 0;

      uint32_t uiCarPos = GetPosition_PositionCount();
      uint32_t uiDestPos = gstMotion.ulActualDest;
      Motion_Profile eProfile = GetMotion_Profile();
      uint32_t uiLevelingDist = ( gpstCurveParameters + eProfile )->uwLevelingDistance;
      uint32_t uiDistanceFromFloor = uiCarPos - uiDestPos;
      if( uiCarPos < uiDestPos )
      {
         uiDistanceFromFloor = uiDestPos - uiCarPos;
      }

      if( ( gpstCurveParameters + eProfile )->uwLevelingSpeed_FPM )
      {
         if( uiDistanceFromFloor <= uiLevelingDist )
         {
            fLevelingTime_sec =   ( uiDistanceFromFloor/HALF_MM_PER_FT )
                                / ( ( gpstCurveParameters + eProfile )->uwLevelingSpeed_FPM/60.0f );
         }
         else
         {
            fLevelingTime_sec =   ( uiLevelingDist/HALF_MM_PER_FT )
                                / ( ( gpstCurveParameters + eProfile )->uwLevelingSpeed_FPM/60.0f );
         }
      }

      uiSum_sec = fDecelTime_sec + fLevelingTime_sec + 0.5f;
      if( uiSum_sec <= 255 )
      {
         ucTime_sec = uiSum_sec;
      }
   }
   else if( eMotionState == MOTION__CRUISING )
   {
      uint32_t uiCarPos = GetPosition_PositionCount();
      uint32_t uiDecelPos = gpstMotionCtrl->stRunParameters.uiStartPosition_Decel;
      Motion_Profile eProfile = GetMotion_Profile();
      uint32_t uiDistanceLeftCruising;
      float fCruisingTime_sec;
      /* Time required for ramp down */
      float fDecelTime_sec = gpstMotionCtrl->stRunParameters.uiPatternDecelTime * gpstMotionCtrl->flPatternTimeRes_sec;
      float fLevelingTime_sec = 0;

      if( ( gpstCurveParameters + eProfile )->uwLevelingSpeed_FPM )
      {
         fLevelingTime_sec = ( ( gpstCurveParameters + eProfile )->uwLevelingDistance/HALF_MM_PER_FT )
                             / ( ( gpstCurveParameters + eProfile )->uwLevelingSpeed_FPM/60.0f );
      }

      if( uiCarPos < uiDecelPos )
      {
         uiDistanceLeftCruising = uiDecelPos - uiCarPos;
      }
      else
      {
         uiDistanceLeftCruising = uiCarPos - uiDecelPos;
      }
      fCruisingTime_sec = ( uiDistanceLeftCruising/HALF_MM_PER_FT )
                        / ( gpstMotionCtrl->stRunParameters.uwMaxRunSpeed_FPM/60.0f );

      uiSum_sec = fCruisingTime_sec + fDecelTime_sec + fLevelingTime_sec + 0.5f;
      if( uiSum_sec <= 255 )
      {
         ucTime_sec = uiSum_sec;
      }
   }

   return ucTime_sec;
}
/*----------------------------------------------------------------------------
   Flag access functions
 *----------------------------------------------------------------------------*/
uint8_t GetPreflightFlag()
{
   return gpstMotionCtrl->stFlags.bPreflight;
}
uint8_t GetDriveHWEnableFlag()
{
   return gpstMotionCtrl->stFlags.bDriveHWEnable;
}
uint8_t GetPickB2Flag()
{
   return gpstMotionCtrl->stFlags.bPickB2;
}
uint8_t GetPickMFlag()
{
   return gpstMotionCtrl->stFlags.bPickM;
}
uint8_t GetPickEBrakeFlag()
{
   return gpstMotionCtrl->stFlags.bPickEBrake;
}
uint8_t GetPickDriveFlag()
{
   return gpstMotionCtrl->stFlags.bPickDrive;
}
uint8_t GetPickBrakeFlag()
{
   return gpstMotionCtrl->stFlags.bPickBrake;
}
uint8_t Motion_GetRelevelingFlag()
{
   return gpstMotionCtrl->bReleveling;
}
/*----------------------------------------------------------------------------
   Module state functions
 *----------------------------------------------------------------------------*/
enum en_motion_stop_sequence Motion_GetStopState(void)
{
   return gpstMotionCtrl->eStopSequence;
}
enum en_motion_start_sequence Motion_GetStartState(void)
{
   return gpstMotionCtrl->eStartSequence;
}
enum en_motion_state Motion_GetMotionState(void)
{
   return gpstMotionCtrl->eMotionState;
}
enum en_patterns Motion_GetPatternState(void)
{
   return gpstMotionCtrl->stAccelVars.ePatternProfile;
}
uint8_t Motion_GetLevelingFlag()
{
   return gpstMotionCtrl->bLeveling;
}
/*----------------------------------------------------------------------------
   Debug functions
 *----------------------------------------------------------------------------*/
uint32_t Motion_GetStartPosition_Accel()
{
   return gpstMotionCtrl->stRunParameters.uiStartPosition_Accel & MAX_24BIT_VALUE;
}
uint32_t Motion_GetStartPosition_Cruise()
{
   return gpstMotionCtrl->stRunParameters.uiStartPosition_Cruise & MAX_24BIT_VALUE;
}
uint32_t Motion_GetStartPosition_Decel()
{
   return gpstMotionCtrl->stRunParameters.uiStartPosition_Decel & MAX_24BIT_VALUE;
}
uint32_t Motion_GetSlowdownDistance()
{
   return gpstMotionCtrl->stRunParameters.uiSlowdownDistance & MAX_24BIT_VALUE;
}
uint32_t Motion_GetRampUpDistance()
{
   return gpstMotionCtrl->stRunParameters.uiRampUpDistance & MAX_24BIT_VALUE;
}
uint32_t Motion_GetRSLDistance()
{
   return gpstMotionCtrl->stRunParameters.uiRSLDecelDistance & MAX_24BIT_VALUE;
}
uint8_t Motion_GetMaxFloorToFloorTime_sec(void)
{
   return ucMaxFloorToFloorTime_s;
}
uint32_t Motion_GetLastStopPosition(void)
{
   return gpstMotionCtrl->uiLastStopPos_05mm & MAX_24BIT_VALUE;
}
/*----------------------------------------------------------------------------
   Clear all Run flags
      Called at beginning of StartSequence, Stopped, and end of StopSequence
 *----------------------------------------------------------------------------*/
void ClearAllMotionRunFlags()
{
   uint8_t bEnableSecondaryBrake = Param_ReadValue_1Bit( enPARAM1__EnableSecondaryBrake );
   gstMotion.bRunFlag = 0;

   gpstMotionCtrl->stFlags.bPickM = 0;
   gpstMotionCtrl->stFlags.bPickDrive = 0;

   gpstMotionCtrl->stFlags.bPickBrake = 0;

   if( !( Param_ReadValue_1Bit(enPARAM1__DSD_EarlyFieldEnable) && ( gpstMotionCtrl->eMotionState == MOTION__STOPPED ) ) )
   {
      gpstMotionCtrl->stFlags.bDriveHWEnable = 0;
   }

//   if(!bEnableSecondaryBrake)
//   {
//      gpstMotionCtrl->stFlags.bPickEBrake = 0;
//      gpstMotionCtrl->stFlags.bPickB2 = 0;
//   }

   gpstMotionCtrl->bLeveling = 0;
}
/*----------------------------------------------------------------------------
   Clear all start sequence timers
      Called at beginning of StartSequence
 *----------------------------------------------------------------------------*/
void ClearAllStartSeqTimers()
{
   for(uint8_t i = 0; i < NUM_MOTION_START_SEQUENCE; i++)
   {
      gpstMotionCtrl->stTimers.auiStartTimers[i] = 0;
   }
   gpstMotionCtrl->stTimers.uiAccelDelayTimeout_ms = 0;
}
/*----------------------------------------------------------------------------
   Clear all stop sequence timers
      Called at beginning of StartSequence
 *----------------------------------------------------------------------------*/
void ClearAllStopSeqTimers()
{
   for(uint8_t i = 0; i < NUM_MOTION_STOP_SEQUENCE; i++)
   {
      gpstMotionCtrl->stTimers.auiStopTimers[i] = 0;
   }
}

/*----------------------------------------------------------------------------
   To set flag for acceptance test ETS/NTS runs. Bypasses decel
 *----------------------------------------------------------------------------*/
void SetAcceptanceTestBypassDecelFlag( uint8_t bBypass )
{
   gpstMotionCtrl->bAcceptanceBypassDecel = bBypass;
}
/*----------------------------------------------------------------------------
   To set flag to trigger overspeed for acceptance testing
 *----------------------------------------------------------------------------*/
void SetAcceptanceTestOverspeedFlag( uint8_t bTriggerOverspeed )
{
   gpstMotionCtrl->bAcceptanceTriggerOverspeed = bTriggerOverspeed;
}
/*----------------------------------------------------------------------------
   Get flag indicating car has not moved for parameter determined time period
 *----------------------------------------------------------------------------*/
uint8_t GetCarStoppedFlag()
{
   return bCarStopped;
}

/*----------------------------------------------------------------------------
   Returns the car's floor at the start of a run
 *----------------------------------------------------------------------------*/
uint8_t Motion_GetStartOfRunFloor()
{
   return gpstMotionCtrl->stRunParameters.ucStartOfRunFloor;
}

/*----------------------------------------------------------------------------
   Returns 1 if destination position has been passed
 *----------------------------------------------------------------------------*/
static uint8_t CheckIfDestinationReached()
{
   uint8_t bReturn = 0;
   uint32_t uiCurrentPos = GetPosition_PositionCount();
   uint32_t uiActualDest = gstMotion.ulActualDest;
   if( ( ( gstMotion.cDirection == DIR__UP ) && ( uiCurrentPos > uiActualDest ) )
    || ( ( gstMotion.cDirection == DIR__DN ) && ( uiCurrentPos < uiActualDest ) ) )
   {
      bReturn = 1;
   }
   return bReturn;
}
/*--------------------------------------------------------------------------

   Clear all gstMotion data and immediately jump to the last
   step of the stop sequence which drops all safeties and
   contactors without waiting for their timers or sequence

--------------------------------------------------------------------------*/
void EmergencyStop( Motion_Control *pstMotionCtrl )
{
   gpstMotionCtrl->stFlags.bPickB2 = 0;
   gpstMotionCtrl->stFlags.bPickEBrake = 0;

   gpstMotionCtrl->eStartSequence = SEQUENCE_START__PREPARE_TO_RUN;
   gpstMotionCtrl->eStopSequence = SEQUENCE_STOP__END;

   gpstMotionCtrl->eMotionState = MOTION__STOPPED;

   MotionState_StopSequence( gpstMotionCtrl );
}

/*--------------------------------------------------------------------------
   Returns 1 if at or below NTS speed and in DZ
--------------------------------------------------------------------------*/
uint8_t CheckIfValidDoorZoneStop(void)
{
   uint8_t bValid = 0;
   if(!Param_ReadValue_1Bit(enPARAM1__DisableNonTerminalNTS))
   {
      bValid = getDoorZone(DOOR_ANY);
      bValid &= ( gstMotion.cDirection * gstMotion.wSpeedCMD ) <= Param_ReadValue_8Bit( enPARAM8__NTSD_Speed );
   }
   return bValid;
}
/*--------------------------------------------------------------------------
   On tripping NTS via processor CTB, NTSD (Quick Stop) flag will be sent discretely to the
   drive via the CPLD network.
   This will cause the drive to independently ramp down to its configured the NTSD speed.

   In parallel, MRA will receive this flag from CTB and ramp down to the speed stored in enPARAM8__NTSD_Speed.
   Based on the decel rate stored in enPARAM8__QuickStopDecel_x10.
--------------------------------------------------------------------------*/
static void QuickStop( void )
{
   float flSpeed;
   float fSpeedReduction;
   gpstMotionCtrl->bQuickStop = 1;
   int16_t wTargetSpeed = Param_ReadValue_8Bit( enPARAM8__NTSD_Speed );
   if ( !GetPosition_Velocity()
     || ( GetPosition_PositionCount() > GetOperation_PositionLimit_UP() )
     || ( GetPosition_PositionCount() < GetOperation_PositionLimit_DN() )
     || ( CheckIfDestinationReached() )
     || ( CheckIfValidDoorZoneStop() ) )
   {
      gpstMotionCtrl->wEndOfRunSpeed_FPM = GetPosition_Velocity();

      gpstMotionCtrl->eMotionState = MOTION__STOP_SEQUENCE;
      gpstMotionCtrl->eStartSequence = SEQUENCE_START__PREPARE_TO_RUN;
      gpstMotionCtrl->eStopSequence = SEQUENCE_STOP__DROP_BRAKE;

      MotionState_StopSequence( gpstMotionCtrl );
   }
   else
   {
      int16_t wUpdatedSpeed = 0;
      float flDecel = Param_ReadValue_8Bit(enPARAM8__QuickStopDecel_x10);
      uint32_t uiMaxDecelTime_ms = ( gpstMotionCtrl->flContractSpeed_FPS / flDecel ) / 1000.0f + 0.5f;
      // Check bounds
      if ( flDecel < ( MIN_ACCEL_X10/2 ) )//feet per min^2
      {
         flDecel = ( MIN_ACCEL_X10/2 );
      }
      else if( flDecel > ( 2*MAX_ACCEL_X10 ) )
      {
         flDecel = ( 2*MAX_ACCEL_X10 );
      }
      flDecel /= 10.0f;

      fSpeedReduction = ( flDecel * gpstMotionCtrl->uiNTSDecelTime ) / 1000.0f;
      int16_t wSpeedReduction = (fSpeedReduction * 60.0f) + 0.5f;
      if( ( gstMotion.cDirection * gpstMotionCtrl->wEndOfRunSpeed_FPM ) >= wSpeedReduction )
      {
         wUpdatedSpeed = ( gstMotion.cDirection * gpstMotionCtrl->wEndOfRunSpeed_FPM ) - wSpeedReduction;
      }

      if( wUpdatedSpeed < wTargetSpeed )
      {
         wUpdatedSpeed = wTargetSpeed;
      }

      gstMotion.wSpeedCMD = gstMotion.cDirection * wUpdatedSpeed;

      if(gpstMotionCtrl->uiNTSDecelTime <= uiMaxDecelTime_ms)
      {
         gpstMotionCtrl->uiNTSDecelTime += MOD_RUN_PERIOD_MOTION_1MS;
      }

      gpstMotionCtrl->wEndOfRunSpeed_FPM = gstMotion.wSpeedCMD;
   }
}

/*--------------------------------------------------------------------------

   If no distance is specified (such as in inspection), a pattern will be
   generated to attain and maintain the requested speed.
   If a distance is specified, a pattern will be calculated to reach that
   distance at the maximum speed (but less than the speed limit specified).

--------------------------------------------------------------------------*/
static void GeneratePattern( uint32_t uiDistance, uint16_t uwSpeedLimit )
{
   float flSpeedLimit = uwSpeedLimit / 60.0f;

   if ( uiDistance )
   {
      float flMaxSpeed = Pattern_GetMaxAccelSpeed_FPS( uiDistance );

      gpstMotionCtrl->stRunParameters.uwMaxRunSpeed_FPM = ( flMaxSpeed * 60.0f ) + 0.5f;

      if ( flMaxSpeed > flSpeedLimit )
      {
         Pattern_GenerateAccelRun( flSpeedLimit );
         Pattern_GenerateDecelRun( flSpeedLimit );
      }
      else
      {
         Pattern_GenerateAccelRun( flMaxSpeed );
         Pattern_GenerateDecelRun( flMaxSpeed );
      }
   }
   else /* Inspection */
   {
      if ( flSpeedLimit > gpstMotionCtrl->flContractSpeed_FPS )
     {
        flSpeedLimit = gpstMotionCtrl->flContractSpeed_FPS;
     }

      Pattern_GenerateAccelRun( flSpeedLimit );
   }

}

/*--------------------------------------------------------------------------

   This is called when the car is accelerating and gets a request to change
   destination midflight.
   It evaluates wether the new destination is achievable, and accepts or
   rejects it. If accepted, this function updates the new critical points
   at which the motion module moves from accel to cruising to decel

--------------------------------------------------------------------------*/
static void DestinationChangeDuringAccel( void )
{
   float flSpeed;
   int16_t wOffset_05mm = ( GetOperation_RequestedDestination() >= GetPosition_PositionCount() )
                        ? ( -1*Param_ReadValue_8Bit(enPARAM8__DestOffsetUp_05mm) )
                        : ( Param_ReadValue_8Bit(enPARAM8__DestOffsetDown_05mm) );
   if( gpstMotionCtrl->bReleveling )
   {
      wOffset_05mm = ( GetOperation_RequestedDestination() >= GetPosition_PositionCount() )
                   ? ( -1*Param_ReadValue_8Bit(enPARAM8__RelevelOffsetUp_05mm) )
                   : ( Param_ReadValue_8Bit(enPARAM8__RelevelOffsetDown_05mm) );
   }
   uint32_t uiRequestedDest = GetOperation_RequestedDestination() + wOffset_05mm;
   uint32_t uiActualDest = gstMotion.ulActualDest;
   gpstMotionCtrl->bRequestRejected = 1;
   gpstMotionCtrl->uiRejectedDest = uiRequestedDest;
   if ( gstMotion.cDirection == DIR__UP )
   {
      if ( uiRequestedDest > gstMotion.ulActualDest ) /* New dest is farther than current dest */
      {
         if ( ( gpstMotionCtrl->stAccelVars.ePatternProfile != PATTERN__FULL )
//           && ( uiRequestedDest - gstMotion.ulActualDest > QUARTER_INCH ) )//TODO review removal of shortest run distance requirement
               )
         {
            /* Calculate the ADDITIONAL speed that the new requested dest would be able to reach */
            uint32_t uiRunDistance = uiRequestedDest - gpstMotionCtrl->stRunParameters.uiStartPosition_Cruise;
            flSpeed = Pattern_GetMaxAddedAccelSpeed_FPS( uiRunDistance );

            if( flSpeed >= gpstMotionCtrl->stAccelVars.fEndSpeed_JerkOutAccel )
            {
               Pattern_GenerateAddedAccelRun( flSpeed );
               gpstMotionCtrl->stRunParameters.uiStartPosition_AddedAccel = gpstMotionCtrl->stRunParameters.uiStartPosition_Accel
                                                                       + Pattern_GetAccelPosition( gpstMotionCtrl->stRunParameters.uiPatternAccelTime - 1 );
               gpstMotionCtrl->stRunParameters.uiStartPosition_Cruise = gpstMotionCtrl->stRunParameters.uiStartPosition_AddedAccel
                                                                   + Pattern_GetAddedAccelPosition( gpstMotionCtrl->stRunParameters.uiAddedAccelTime - 1 );
               Pattern_GenerateDecelRun( flSpeed );

               /* Accept new destination and adjust cruising distance */
               gpstMotionCtrl->stRunParameters.uiStartPosition_Decel = uiRequestedDest - gpstMotionCtrl->stRunParameters.uiSlowdownDistance;
               gstMotion.ulActualDest = uiRequestedDest;
               gpstMotionCtrl->bRequestRejected = 0;
            }
         }
         else
         {
            /* Accept new destination and adjust cruising distance */
            gpstMotionCtrl->stRunParameters.uiStartPosition_Decel = uiRequestedDest - gpstMotionCtrl->stRunParameters.uiSlowdownDistance;
            gstMotion.ulActualDest = uiRequestedDest;
            gpstMotionCtrl->bRequestRejected = 0;
         }
      }
      /* New dest is closer than current dest */
      else if( uiRequestedDest < gstMotion.ulActualDest )
      {
         /* And the requested destination comes after CruisingStart + slowdown */
         if( uiRequestedDest > ( gpstMotionCtrl->stRunParameters.uiStartPosition_Cruise + gpstMotionCtrl->stRunParameters.uiSlowdownDistance ) )
         {
            /* Accept new destination and adjust cruising distance */
            gpstMotionCtrl->stRunParameters.uiStartPosition_Decel = uiRequestedDest - gpstMotionCtrl->stRunParameters.uiSlowdownDistance;
            gstMotion.ulActualDest = uiRequestedDest;
            gpstMotionCtrl->bRequestRejected = 0;
         }
         /* And Jerk out accel stage has not started */
         else if( ( gpstMotionCtrl->stAccelVars.ePatternProfile != PATTERN__VERY_SHORT )
               && ( gpstMotionCtrl->stDecelVars.ePatternProfile != PATTERN__VERY_SHORT )
               && ( ( gpstMotionCtrl->uiPrevTime + ( MIN_TRUNCATED_ACCEL_TIME_OFFSET_SEC / gpstMotionCtrl->flPatternTimeRes_sec ) ) < gpstMotionCtrl->stRunParameters.uiEndTime_ConstantAccel ) )
         {
            /* Then generate a new pattern with a lower max speed */
            uint32_t uiRunDistance = uiRequestedDest - gpstMotionCtrl->stRunParameters.uiStartPosition_Accel;
            uint16_t uwAccelJerkOutSpeedChange = ( gpstMotionCtrl->stAccelVars.fEndSpeed_JerkOutAccel - gpstMotionCtrl->stAccelVars.fEndSpeed_ConstantAccel ) * 60.0f + 0.5f;
            uwAccelJerkOutSpeedChange *= 1.05f; // Scaling value for rounding inaccuracies
            /* Pattern must complete Accel Jerk Out curve */
            uint16_t uwMinimumTopSpeed = ( gstMotion.wSpeedCMD * gstMotion.cDirection ) + uwAccelJerkOutSpeedChange;
            uint16_t uwMaximumTopSpeed = ( Pattern_GetMaxAccelSpeed_FPS( uiRunDistance ) * 60.0f ) + 0.5f;
            uint16_t uwOperationSpeedLimit = GetOperation_SpeedLimit();
            uint16_t uwRunSpeed = uwMaximumTopSpeed;
            if( uwMaximumTopSpeed < uwMinimumTopSpeed )
            {
               uwRunSpeed = uwMinimumTopSpeed;
            }
            if( uwRunSpeed > uwOperationSpeedLimit )
            {
               uwRunSpeed = uwOperationSpeedLimit;
            }

            uint32_t uiReachableDistance = Pattern_CalculateRampUpDistance( uwRunSpeed ) + Pattern_CalculateSlowdownDistance( uwRunSpeed );
            uiReachableDistance *= 1.05f; // Scaling value for rounding inaccuracies
            if( uiRunDistance > uiReachableDistance )
            {
               GeneratePattern(uiRunDistance, uwRunSpeed);
               gstMotion.ulActualDest = uiRequestedDest;
               gpstMotionCtrl->stRunParameters.uiStartPosition_Cruise =  gpstMotionCtrl->stRunParameters.uiStartPosition_Accel + gpstMotionCtrl->stRunParameters.uiRampUpDistance;
               gpstMotionCtrl->stRunParameters.uiStartPosition_Decel = gstMotion.ulActualDest - gpstMotionCtrl->stRunParameters.uiSlowdownDistance;
               gpstMotionCtrl->bRequestRejected = 0;
            }
         }
      }
   }
   else if ( gstMotion.cDirection == DIR__DN )
   {
      if ( uiRequestedDest < gstMotion.ulActualDest ) /* New dest is farther than current dest */
      {
         if ( ( gpstMotionCtrl->stAccelVars.ePatternProfile != PATTERN__FULL )
//           && ( gstMotion.ulActualDest - uiRequestedDest > QUARTER_INCH ) )//TODO review removal of shortest run distance requirement
            )
         {
            /* Calculate the ADDITIONAL speed that the new requested dest would be able to reach */
            uint32_t uiRunDistance = gpstMotionCtrl->stRunParameters.uiStartPosition_Cruise - uiRequestedDest;
            flSpeed = Pattern_GetMaxAddedAccelSpeed_FPS( uiRunDistance );

            if( flSpeed >= gpstMotionCtrl->stAccelVars.fEndSpeed_JerkOutAccel )
            {
               Pattern_GenerateAddedAccelRun( flSpeed );
               gpstMotionCtrl->stRunParameters.uiStartPosition_AddedAccel = gpstMotionCtrl->stRunParameters.uiStartPosition_Accel
                                                                          - Pattern_GetAccelPosition( gpstMotionCtrl->stRunParameters.uiPatternAccelTime - 1 );
               gpstMotionCtrl->stRunParameters.uiStartPosition_Cruise = gpstMotionCtrl->stRunParameters.uiStartPosition_AddedAccel
                                                                      - Pattern_GetAddedAccelPosition( gpstMotionCtrl->stRunParameters.uiAddedAccelTime - 1 );

               Pattern_GenerateDecelRun( flSpeed );

               /* Accept new destination and adjust cruising distance */
               gpstMotionCtrl->stRunParameters.uiStartPosition_Decel = uiRequestedDest + gpstMotionCtrl->stRunParameters.uiSlowdownDistance;
               gstMotion.ulActualDest = uiRequestedDest;
               gpstMotionCtrl->bRequestRejected = 0;
            }
         }
         else
         {
            /* Accept new destination and adjust cruising distance */
            gpstMotionCtrl->stRunParameters.uiStartPosition_Decel = uiRequestedDest + gpstMotionCtrl->stRunParameters.uiSlowdownDistance;
            gstMotion.ulActualDest = uiRequestedDest;
            gpstMotionCtrl->bRequestRejected = 0;
         }
      }
      /* New dest is closer than current dest */
      else if( uiRequestedDest > gstMotion.ulActualDest )
      {
         /* And the requested destination comes after CruisingStart + slowdown */
         if( uiRequestedDest < ( gpstMotionCtrl->stRunParameters.uiStartPosition_Cruise - gpstMotionCtrl->stRunParameters.uiSlowdownDistance ) )
         {
            /* Accept new destination and adjust cruising distance */
            gpstMotionCtrl->stRunParameters.uiStartPosition_Decel = uiRequestedDest + gpstMotionCtrl->stRunParameters.uiSlowdownDistance;
            gstMotion.ulActualDest = uiRequestedDest;
            gpstMotionCtrl->bRequestRejected = 0;
         }
         /* And Jerk out accel stage has not started */
         else if( ( gpstMotionCtrl->stAccelVars.ePatternProfile != PATTERN__VERY_SHORT )
               && ( gpstMotionCtrl->stDecelVars.ePatternProfile != PATTERN__VERY_SHORT )
               && ( ( gpstMotionCtrl->uiPrevTime + ( MIN_TRUNCATED_ACCEL_TIME_OFFSET_SEC / gpstMotionCtrl->flPatternTimeRes_sec ) ) < gpstMotionCtrl->stRunParameters.uiEndTime_ConstantAccel ) )
         {
            /* Then generate a new pattern with a lower max speed */
            uint32_t uiRunDistance = gpstMotionCtrl->stRunParameters.uiStartPosition_Accel - uiRequestedDest;
            uint16_t uwAccelJerkOutSpeedChange = ( gpstMotionCtrl->stAccelVars.fEndSpeed_JerkOutAccel - gpstMotionCtrl->stAccelVars.fEndSpeed_ConstantAccel ) * 60.0f + 0.5f;
            uwAccelJerkOutSpeedChange *= 1.05f; // Scaling value for rounding inaccuracies
            /* Pattern must complete Accel Jerk Out curve */
            uint16_t uwMinimumTopSpeed = ( gstMotion.wSpeedCMD * gstMotion.cDirection ) + uwAccelJerkOutSpeedChange;
            uint16_t uwMaximumTopSpeed = ( Pattern_GetMaxAccelSpeed_FPS( uiRunDistance ) * 60.0f ) + 0.5f;
            uint16_t uwOperationSpeedLimit = GetOperation_SpeedLimit();
            uint16_t uwRunSpeed = uwMaximumTopSpeed;
            if( uwMaximumTopSpeed < uwMinimumTopSpeed )
            {
               uwRunSpeed = uwMinimumTopSpeed;
            }
            if( uwRunSpeed > uwOperationSpeedLimit )
            {
               uwRunSpeed = uwOperationSpeedLimit;
            }

            uint32_t uiReachableDistance = Pattern_CalculateRampUpDistance( uwRunSpeed ) + Pattern_CalculateSlowdownDistance( uwRunSpeed );
            uiReachableDistance *= 1.05f; // Scaling value for rounding inaccuracies
            if( uiRunDistance > uiReachableDistance )
            {
               GeneratePattern(uiRunDistance, uwRunSpeed);
               gstMotion.ulActualDest = uiRequestedDest;
               gpstMotionCtrl->stRunParameters.uiStartPosition_Cruise =  gpstMotionCtrl->stRunParameters.uiStartPosition_Accel - gpstMotionCtrl->stRunParameters.uiRampUpDistance;
               gpstMotionCtrl->stRunParameters.uiStartPosition_Decel = gstMotion.ulActualDest + gpstMotionCtrl->stRunParameters.uiSlowdownDistance;
               gpstMotionCtrl->bRequestRejected = 0;
            }
         }
      }
   }
   SetMotion_Destination( gstMotion.ulActualDest );
}

/*--------------------------------------------------------------------------

   This is called when the car is cruising (at contract) and gets a request
   to change destination midflight.
   It evaluates wether the new destination is achievable, and accepts and
   updates the cruising time, or rejects it.

--------------------------------------------------------------------------*/
static void DestinationChangeDuringCruising( void )
{
   uint32_t ulNewDestinationSlowdown = 0;
   int16_t wOffset_05mm = ( GetOperation_RequestedDestination() >= GetPosition_PositionCount() )
                        ? ( -1*Param_ReadValue_8Bit(enPARAM8__DestOffsetUp_05mm) )
                        : ( Param_ReadValue_8Bit(enPARAM8__DestOffsetDown_05mm) );
   if( gpstMotionCtrl->bReleveling )
   {
      wOffset_05mm = ( GetOperation_RequestedDestination() >= GetPosition_PositionCount() )
                   ? ( -1*Param_ReadValue_8Bit(enPARAM8__RelevelOffsetUp_05mm) )
                   : ( Param_ReadValue_8Bit(enPARAM8__RelevelOffsetDown_05mm) );
   }
   uint32_t uiRequestedDest = GetOperation_RequestedDestination() + wOffset_05mm;
   uint32_t uiCurrentPos = GetPosition_PositionCount();
   gpstMotionCtrl->uiRejectedDest = uiRequestedDest;
   if ( gstMotion.cDirection == DIR__UP )
   {
      if ( ( uiRequestedDest > gstMotion.ulActualDest )
        && ( gpstMotionCtrl->stAccelVars.ePatternProfile != PATTERN__FULL )
//        && ( uiRequestedDest - gstMotion.ulActualDest > QUARTER_INCH ) )//TODO review removal of shortest run distance requirement
         )
      {
         /* Calculate the ADDITIONAL speed that the new requested dest would be able to reach */
         /* Estimate run distance with 1 sec delay */
         uint32_t uiLagDistance = ADDED_ACCEL_TIME_DELAY_SEC * gpstMotionCtrl->stAccelVars.fEndSpeed_JerkOutAccel * HALF_MM_PER_FT;//Distance to account delay between capturing a position and generating added accel pattern
         uint32_t uiStartingPositionAddedAccel = uiCurrentPos + uiLagDistance;
         uint32_t uiRunDistance = uiRequestedDest - uiStartingPositionAddedAccel;

         float flSpeed = Pattern_GetMaxAddedAccelSpeed_FPS( uiRunDistance );

         if( flSpeed >= gpstMotionCtrl->stAccelVars.fEndSpeed_JerkOutAccel )
         {
            Pattern_GenerateAddedAccelRun( flSpeed );
            gpstMotionCtrl->stRunParameters.uiStartPosition_AddedAccel = uiStartingPositionAddedAccel;
            gpstMotionCtrl->stRunParameters.uiStartPosition_Cruise = uiStartingPositionAddedAccel
                                                                + Pattern_GetAddedAccelPosition( gpstMotionCtrl->stRunParameters.uiAddedAccelTime - 1 );
            Pattern_GenerateDecelRun( flSpeed );

            /* Accept new destination and adjust cruising distance */
            gpstMotionCtrl->stRunParameters.uiStartPosition_Decel = uiRequestedDest - gpstMotionCtrl->stRunParameters.uiSlowdownDistance;
            gstMotion.ulActualDest = uiRequestedDest;

            gpstMotionCtrl->eMotionState = MOTION__ACCELERATING;

            gpstMotionCtrl->bRequestRejected = 0;
         }
         else
         {
            gpstMotionCtrl->bRequestRejected = 1;
         }
      }
      /*
       * Check if the destination is further away than the current destination, or
       * if the new destination is far enough away to make it with the slowdown.
       */
      else if (  ( uiRequestedDest > gstMotion.ulActualDest )
         || gpstMotionCtrl->stRunParameters.uiSlowdownDistance < ( uiRequestedDest - uiCurrentPos ) )
      {
         ulNewDestinationSlowdown = uiRequestedDest - gpstMotionCtrl->stRunParameters.uiSlowdownDistance;
         gstMotion.ulActualDest = uiRequestedDest;
         gpstMotionCtrl->stRunParameters.uiStartPosition_Decel = ulNewDestinationSlowdown;
         gpstMotionCtrl->bRequestRejected = 0;
      }
      else
      {
         gpstMotionCtrl->bRequestRejected = 1;
      }
   }
   else if ( gstMotion.cDirection == DIR__DN )
   {
      if ( ( uiRequestedDest < gstMotion.ulActualDest )
        && ( gpstMotionCtrl->stAccelVars.ePatternProfile != PATTERN__FULL )
//        && ( gstMotion.ulActualDest - uiRequestedDest > QUARTER_INCH ) )//TODO review removal of shortest run distance requirement
         )
      {
         /* Calculate the ADDITIONAL speed that the new requested dest would be able to reach */
         /* Estimate run distance with 1 sec delay */
         uint32_t uiLagDistance = ADDED_ACCEL_TIME_DELAY_SEC * gpstMotionCtrl->stAccelVars.fEndSpeed_JerkOutAccel * HALF_MM_PER_FT;//Distance to account delay between capturing a position and generating added accel pattern
         uint32_t uiStartingPositionAddedAccel = uiCurrentPos - uiLagDistance;
         uint32_t uiRunDistance = uiStartingPositionAddedAccel
                                - uiRequestedDest;
         float flSpeed = Pattern_GetMaxAddedAccelSpeed_FPS( uiRunDistance );

         if( flSpeed >= gpstMotionCtrl->stAccelVars.fEndSpeed_JerkOutAccel )
         {
            Pattern_GenerateAddedAccelRun( flSpeed );

            gpstMotionCtrl->stRunParameters.uiStartPosition_AddedAccel = uiStartingPositionAddedAccel;
            gpstMotionCtrl->stRunParameters.uiStartPosition_Cruise = uiStartingPositionAddedAccel - Pattern_GetAddedAccelPosition( gpstMotionCtrl->stRunParameters.uiAddedAccelTime - 1 );

            Pattern_GenerateDecelRun( flSpeed );

            /* Accept new destination and adjust cruising distance */
            gpstMotionCtrl->stRunParameters.uiStartPosition_Decel = uiRequestedDest + gpstMotionCtrl->stRunParameters.uiSlowdownDistance;
            gstMotion.ulActualDest = uiRequestedDest;

            gpstMotionCtrl->eMotionState = MOTION__ACCELERATING;

            gpstMotionCtrl->bRequestRejected = 0;
         }
         else
         {
            gpstMotionCtrl->bRequestRejected = 1;
         }
      }
      /*
       * Check if the destination is further away than the current destination, or
       * if the new destination is far enough away to make it with the slowdown.
       */
      else if (  ( uiRequestedDest < gstMotion.ulActualDest )
              || ( gpstMotionCtrl->stRunParameters.uiSlowdownDistance < ( uiCurrentPos - uiRequestedDest ) )
               )
      {
         ulNewDestinationSlowdown = uiRequestedDest + gpstMotionCtrl->stRunParameters.uiSlowdownDistance;
         gstMotion.ulActualDest = uiRequestedDest;
         gpstMotionCtrl->stRunParameters.uiStartPosition_Decel = ulNewDestinationSlowdown;
         gpstMotionCtrl->bRequestRejected = 0;
      }
      else
      {
         gpstMotionCtrl->bRequestRejected = 1;
      }
   }
   SetMotion_Destination( gstMotion.ulActualDest );
}

/*--------------------------------------------------------------------------
   Called by StartSequence_AccelDelay,
   Captures starting position and generates pattern to reach target destination
   Returns 1 if successful
--------------------------------------------------------------------------*/
uint8_t GenerateRun_Auto()
{
   uint8_t bSuccess = 0;
   uint32_t uiDistance = 0;
   uint8_t bInvalidRun = 0;
   int16_t wOffset_05mm = ( GetOperation_RequestedDestination() >= GetPosition_PositionCount() )
                        ? ( -1*Param_ReadValue_8Bit(enPARAM8__DestOffsetUp_05mm) )
                        : ( Param_ReadValue_8Bit(enPARAM8__DestOffsetDown_05mm) );
   if( gpstMotionCtrl->bReleveling )
   {
      wOffset_05mm = ( GetOperation_RequestedDestination() >= GetPosition_PositionCount() )
                   ? ( -1*Param_ReadValue_8Bit(enPARAM8__RelevelOffsetUp_05mm) )
                   : ( Param_ReadValue_8Bit(enPARAM8__RelevelOffsetDown_05mm) );
   }
   uint32_t uiOffsetDestination_05mm = GetOperation_RequestedDestination() + wOffset_05mm;
   if ( GetOperation_RequestedDestination() > GetPosition_PositionCount() ) {
      gstMotion.cDirection = DIR__UP;
      uiDistance = uiOffsetDestination_05mm - GetPosition_PositionCount();
   }
   else if ( GetOperation_RequestedDestination() < GetPosition_PositionCount() ) {
      gstMotion.cDirection = DIR__DN;
      uiDistance = GetPosition_PositionCount() - uiOffsetDestination_05mm;
   }
   else {
      gstMotion.cDirection = DIR__NONE;
      bInvalidRun = 1;
   }

   uiDistance = ( uiDistance > MAX_24BIT_VALUE )
              ? ( 0 )
              : ( uiDistance );
   //TODO review removal of shortest run distance requirement
//   if ( uiDistance < (uint32_t)QUARTER_INCH ) {
//      gstMotion.cDirection = DIR__NONE;
//      bInvalidRun = 1;
//   }

   if( !gpstMotionCtrl->bRequestRejected && !bInvalidRun ) {
      gpstMotionCtrl->stRunParameters.uiStartPosition_Accel = GetPosition_PositionCount();

      GeneratePattern( uiDistance, GetOperation_SpeedLimit() );

      gstMotion.ulActualDest = uiOffsetDestination_05mm;

      if ( gstMotion.cDirection == DIR__UP )
      {
         gpstMotionCtrl->stRunParameters.uiStartPosition_Cruise =  gpstMotionCtrl->stRunParameters.uiStartPosition_Accel + gpstMotionCtrl->stRunParameters.uiRampUpDistance;
         gpstMotionCtrl->stRunParameters.uiStartPosition_Decel = gstMotion.ulActualDest - gpstMotionCtrl->stRunParameters.uiSlowdownDistance;
      }
      else
      {
         gpstMotionCtrl->stRunParameters.uiStartPosition_Cruise =  gpstMotionCtrl->stRunParameters.uiStartPosition_Accel - gpstMotionCtrl->stRunParameters.uiRampUpDistance;
         gpstMotionCtrl->stRunParameters.uiStartPosition_Decel = gstMotion.ulActualDest + gpstMotionCtrl->stRunParameters.uiSlowdownDistance;
      }
      bSuccess = 1;
   }
   SetMotion_Destination( gstMotion.ulActualDest );
   return bSuccess;
}
/*--------------------------------------------------------------------------
   Called by StartSequence_AccelDelay,
   Captures starting position and generates pattern to reach target destination
   Returns 1 if successful
--------------------------------------------------------------------------*/
uint8_t GenerateRun_Manual()
{
   uint8_t bSuccess = 0;
   uint32_t uiDistance;
   if ( GetOperation_MotionCmd()== MOCMD__RUN_UP )
   {
      uint32_t uiPositionLimit_UP = GetOperation_PositionLimit_UP();
      uint8_t bBypassTermLimits = GetOperation_BypassTermLimits() || Learn_GetNeedToLearnFlag();
      if ( bBypassTermLimits || GetPosition_PositionCount() < uiPositionLimit_UP  )
      {
         /* If bypass term limits is set, set soft limit speed to requested inspection speed */
         guwSoftLimitSpeed = ( bBypassTermLimits ) ? GetOperation_SpeedLimit():Param_ReadValue_16Bit( enPARAM16__InspectionTerminalSpeed );

         uiDistance = (uint32_t)( HALF_MM_PER_FT*Param_ReadValue_16Bit( enPARAM16__SoftLimitDistance_UP ) + 0.5f );

         guiSoftLimitPosition = GetOperation_PositionLimit_UP() - uiDistance;
         uint16_t uwSpeedLimit = GetOperation_SpeedLimit();
         if ( uwSpeedLimit )
         {
            if ( ( GetPosition_PositionCount() > guiSoftLimitPosition )
              && ( GetOperation_SpeedLimit() > guwSoftLimitSpeed )
              && ( GetOperation_ManualMode() != MODE_M__CONSTRUCTION ) )
            {
               GeneratePattern( 0, guwSoftLimitSpeed );
               gbSoftLimitPatternAtStart = 1;
            }
            else
            {
               GeneratePattern( 0, GetOperation_SpeedLimit() );
            }
         }
         else /* This is to be able to lift the brake by setting insp speed to zero */
         {
            gpstMotionCtrl->stRunParameters.uiPatternAccelTime = 1;
         }

         /* Get floor at start of run for run timers */
         gpstMotionCtrl->stRunParameters.ucStartOfRunFloor = GetOperation_CurrentFloor();

         gstMotion.cDirection = DIR__UP;

         bSuccess = 1;
      }
      else if( OnAcceptanceTest() && (GetPosition_PositionCount() >= uiPositionLimit_UP))
      {
        SetFault(FLT__POSITION_LIMIT);
      }
      else
      {
         SetFault(FLT__INVALID_MANUAL_RUN);
      }
   }
   else if (GetOperation_MotionCmd() == MOCMD__RUN_DN )
   {
      uint32_t uiPositionLimit_DN = GetOperation_PositionLimit_DN();
      uint8_t bBypassTermLimits = GetOperation_BypassTermLimits() || Learn_GetNeedToLearnFlag();
      if ( bBypassTermLimits || GetPosition_PositionCount() > uiPositionLimit_DN  )
      {
         /* If bypass term limits is set, set soft limit speed to requested inspection speed */
         guwSoftLimitSpeed = ( bBypassTermLimits ) ? GetOperation_SpeedLimit():Param_ReadValue_16Bit( enPARAM16__InspectionTerminalSpeed );

         uiDistance = (uint32_t)( HALF_MM_PER_FT*Param_ReadValue_16Bit( enPARAM16__SoftLimitDistance_DN ) + 0.5f );

         guiSoftLimitPosition = GetOperation_PositionLimit_DN() + uiDistance;

         if (  GetOperation_SpeedLimit())
         {
            if ( ( GetPosition_PositionCount() < guiSoftLimitPosition )
              && ( GetOperation_SpeedLimit() > guwSoftLimitSpeed )
              && ( GetOperation_ManualMode() != MODE_M__CONSTRUCTION ) )
            {
               GeneratePattern( 0, guwSoftLimitSpeed );
               gbSoftLimitPatternAtStart = 1;
            }
            else
            {
               GeneratePattern( 0,  GetOperation_SpeedLimit() );
            }
         }
         else /* This is to be able to lift the brake by setting insp speed to zero */
         {
            gpstMotionCtrl->stRunParameters.uiPatternAccelTime = 1;
         }

         /* Get floor at start of run for run timers */
         gpstMotionCtrl->stRunParameters.ucStartOfRunFloor = GetOperation_CurrentFloor();

         gstMotion.cDirection = DIR__DN;
         bSuccess = 1;
      }
      else if( OnAcceptanceTest() && (GetPosition_PositionCount() <= uiPositionLimit_DN))
      {
        SetFault(FLT__POSITION_LIMIT);
      }
      else
      {
         SetFault(FLT__INVALID_MANUAL_RUN);
      }
   }

   return bSuccess;
}
/*--------------------------------------------------------------------------
   Auto operation or nts learn
--------------------------------------------------------------------------*/
static void Process_Stopped_Auto()
{
   if ( GetOperation_MotionCmd() == MOCMD__GOTO_DEST )
   {
      uint32_t uiDistance = 0;
      uint8_t bInvalidRun = 0;
      int16_t wOffset_05mm = ( GetOperation_RequestedDestination() >= GetPosition_PositionCount() )
                           ? ( -1*Param_ReadValue_8Bit(enPARAM8__DestOffsetUp_05mm) )
                           : ( Param_ReadValue_8Bit(enPARAM8__DestOffsetDown_05mm) );
      if( inReleveling() )
      {
         wOffset_05mm = ( GetOperation_RequestedDestination() >= GetPosition_PositionCount() )
                      ? ( -1*Param_ReadValue_8Bit(enPARAM8__RelevelOffsetUp_05mm) )
                      : ( Param_ReadValue_8Bit(enPARAM8__RelevelOffsetDown_05mm) );
      }
      uint32_t uiOffsetDestination_05mm = GetOperation_RequestedDestination() + wOffset_05mm;
      if ( GetOperation_RequestedDestination() > GetPosition_PositionCount() ) {
         gstMotion.cDirection = DIR__UP;
         uiDistance = uiOffsetDestination_05mm - GetPosition_PositionCount();
      }
      else if ( GetOperation_RequestedDestination() < GetPosition_PositionCount() ) {
         gstMotion.cDirection = DIR__DN;
         uiDistance = GetPosition_PositionCount() - uiOffsetDestination_05mm;
      }
      else {
         gstMotion.cDirection = DIR__NONE;
         bInvalidRun = 1;
      }

      uiDistance = ( uiDistance > MAX_24BIT_VALUE )
                 ? ( 0 )
                 : ( uiDistance );

      //TODO review removal of shortest run distance requirement
//      if ( uiDistance < (uint32_t)QUARTER_INCH ) {
//         gstMotion.cDirection = DIR__NONE;
//         bInvalidRun = 1;
//      }

      if( !gpstMotionCtrl->bRequestRejected && !bInvalidRun ) {
         gstMotion.ulActualDest = uiOffsetDestination_05mm;

         /* Get floor at start of run for run timers */
         gpstMotionCtrl->stRunParameters.ucStartOfRunFloor = GetOperation_CurrentFloor();

         /* Check for releveling run */
         gpstMotionCtrl->bReleveling = inReleveling();

         gpstMotionCtrl->eMotionState = MOTION__START_SEQUENCE;

         ClearAllStartSeqTimers();
         ClearAllStopSeqTimers();

         gstMotion.bRunFlag = 1;
      }
      else
      {
         gstMotion.cDirection = DIR__NONE;
      }
   }
}
/*--------------------------------------------------------------------------
   Manual or HA learn
--------------------------------------------------------------------------*/
static void Process_Stopped_Manual()
{
   uint32_t uiDistance;
   if ( GetOperation_MotionCmd()== MOCMD__RUN_UP )
   {
      uint32_t uiPositionLimit_UP = GetOperation_PositionLimit_UP();
      uint8_t bBypassTermLimits = GetOperation_BypassTermLimits();
      if ( bBypassTermLimits || GetPosition_PositionCount() < uiPositionLimit_UP  )
      {
         /* If bypass term limits is set, set soft limit speed to requested inspection speed */
         guwSoftLimitSpeed = ( bBypassTermLimits ) ? GetOperation_SpeedLimit():Param_ReadValue_16Bit( enPARAM16__InspectionTerminalSpeed );

         uiDistance = (uint32_t)( HALF_MM_PER_FT*Param_ReadValue_16Bit( enPARAM16__SoftLimitDistance_UP ) + 0.5f );

         guiSoftLimitPosition = GetOperation_PositionLimit_UP() - uiDistance;

         /* Get floor at start of run for run timers */
         gpstMotionCtrl->stRunParameters.ucStartOfRunFloor = GetOperation_CurrentFloor();

         gstMotion.cDirection = DIR__UP;
         gpstMotionCtrl->eMotionState = MOTION__START_SEQUENCE;
         ClearAllStartSeqTimers();
         ClearAllStopSeqTimers();
      }
      else if( OnAcceptanceTest() && (GetPosition_PositionCount() >= uiPositionLimit_UP))
      {
        SetFault(FLT__POSITION_LIMIT);
      }
      else
      {
         SetFault(FLT__INVALID_MANUAL_RUN);
      }
   }
   else if (GetOperation_MotionCmd() == MOCMD__RUN_DN )
   {
      uint32_t uiPositionLimit_DN = GetOperation_PositionLimit_DN();
      uint8_t bBypassTermLimits = GetOperation_BypassTermLimits();
      if ( bBypassTermLimits || GetPosition_PositionCount() > uiPositionLimit_DN  )
      {
         /* If bypass term limits is set, set soft limit speed to requested inspection speed */
         guwSoftLimitSpeed = ( bBypassTermLimits ) ? GetOperation_SpeedLimit():Param_ReadValue_16Bit( enPARAM16__InspectionTerminalSpeed );

         uiDistance = (uint32_t)( HALF_MM_PER_FT*Param_ReadValue_16Bit( enPARAM16__SoftLimitDistance_DN ) + 0.5f );

         guiSoftLimitPosition = GetOperation_PositionLimit_DN() + uiDistance;

         /* Get floor at start of run for run timers */
         gpstMotionCtrl->stRunParameters.ucStartOfRunFloor = GetOperation_CurrentFloor();

         gstMotion.cDirection = DIR__DN;
         gpstMotionCtrl->eMotionState = MOTION__START_SEQUENCE;
         ClearAllStartSeqTimers();
         ClearAllStopSeqTimers();
      }
      else if( OnAcceptanceTest() && (GetPosition_PositionCount() <= uiPositionLimit_DN))
      {
        SetFault(FLT__POSITION_LIMIT);
      }
      else
      {
         SetFault(FLT__INVALID_MANUAL_RUN);
      }
   }
}
/*--------------------------------------------------------------------------
   HPV recommended travel direction check
--------------------------------------------------------------------------*/
static void Process_Stopped_RecTrvDir()
{
   /* Get floor at start of run for run timers */
   gpstMotionCtrl->stRunParameters.ucStartOfRunFloor = GetOperation_CurrentFloor();

   gstMotion.cDirection = DIR__UP;
   gpstMotionCtrl->eMotionState = MOTION__START_SEQUENCE;
   ClearAllStartSeqTimers();
   ClearAllStopSeqTimers();
}
/*--------------------------------------------------------------------------
   Checks to see that the system is in a valid mode of operation to
   initiate movement to a predefined destination
--------------------------------------------------------------------------*/
uint8_t CheckIf_ValidDestinationRun()
{
   uint8_t bReturn = 0;
   if( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
    && ( ( GetOperation_AutoMode() != MODE_A__TEST )
      || ( !GetManualMode() ) ) )
   {
      bReturn = 1;
   }
   return bReturn;
}
/*--------------------------------------------------------------------------
   Checks to see that the system is in a valid mode of operation to
   initiate a manual motion run
--------------------------------------------------------------------------*/
uint8_t CheckIf_ValidManualRun()
{
   uint8_t bReturn = 0;
   if( ( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
    || ( GetOperation_ClassOfOp() == CLASSOP__SEMI_AUTO ) )
   {
      bReturn = 1;
   }
   else if( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
         && ( GetOperation_AutoMode() == MODE_A__TEST )
         && ( GetManualMode() ) )
   {
      bReturn = 1;
   }
   return bReturn;
}
/*--------------------------------------------------------------------------
   Checks that car has not moved recently
--------------------------------------------------------------------------*/
#define CAR_STABILITY_DELAY_SCALING       (10)
static void CheckIfCarIsStable()
{
   static uint16_t uwStablilityCounter_5ms = 0xFFFF;
   uint8_t bCarStable = 0;
   int16_t wCarSpeed = GetPosition_Velocity();
   if( wCarSpeed <= 2
    && wCarSpeed >= -2 )
   {
      uint16_t uwLimit_5ms = Param_ReadValue_8Bit( enPARAM8__CarStabilityDelay_50ms ) * CAR_STABILITY_DELAY_SCALING;
      if( uwStablilityCounter_5ms >= uwLimit_5ms )
      {
         bCarStable = 1;
      }
      else
      {
         uwStablilityCounter_5ms++;
      }
   }
   else
   {
      uwStablilityCounter_5ms = 0;
   }
   bCarStopped = bCarStable;
}

/*--------------------------------------------------------------------------
   Updates Drive HW enable flag early to reduce run start delays for DSD drive jobs

   From DSD manual:
   "
      Drive & Field Enable
      The Drive & Field Enable contact input enables
      the drive to run and also acts as a command to
      fully energize the motor field. Closing Drive &
      Field Enable in advance of the Hardware Run,
      or Run-Up / Run-Down, command will allow
      time for the motor field to become fully
      energized for a minimum of starting delays.
      When the Drive & Field Enable circuit is open,
      the motor field circuit will be energized at the
      stand-by current level. If Field Enable remains
      ON for more than 5 minutes without running
      the drive, an F403 will occur and the Full Field
      condition will be turned OFF.
   "
--------------------------------------------------------------------------*/
#define MOTION_MODULE_TIME_TO_SEC           ( 200 )
#define IDLE_FIELD_ENABLE_LIMIT_5MS         ( 10*MOTION_MODULE_TIME_TO_SEC )
static void UpdateEarlyFieldEnable(void)
{
   static uint16_t uwIdleTimer_5ms;
   if( ( Param_ReadValue_1Bit(enPARAM1__DSD_EarlyFieldEnable) )
    && ( Param_ReadValue_8Bit(enPARAM8__DriveSelect) == enDriveType__DSD ) )
   {
      if( gpstMotionCtrl->bResetFieldTimer )
      {
         gpstMotionCtrl->bResetFieldTimer = 0;
         uwIdleTimer_5ms = 0;
      }

      /* Track the time the HW enable signal is on while idle to prevent DSD F403 */
      if( gpstMotionCtrl->stFlags.bDriveHWEnable )
      {
         if( uwIdleTimer_5ms < IDLE_FIELD_ENABLE_LIMIT_5MS )
         {
            uwIdleTimer_5ms++;
         }
      }

      if( ( GetDoorsFieldEnableFlag() )
       && ( uwIdleTimer_5ms < IDLE_FIELD_ENABLE_LIMIT_5MS ) )
      {
         gpstMotionCtrl->stFlags.bDriveHWEnable = 1;
      }
      else
      {
         gpstMotionCtrl->stFlags.bDriveHWEnable = 0;
      }
   }
}
/*--------------------------------------------------------------------------

   This is the idle state. The car will stay in this state until a GOTO_DEST
   motion command is received while on automatic or NTS learn, or a RUN_UP or
   RUN_DN motion command is received while on inspection or HA learn.
   On automatic or NTS learn, the motion module goes to a specific destination
   On inspection or HA learn, the motion module moves the car until the motion
   command changes to an emergency stop or quick stop.
   On automatic, a normal stop means the motion module needs to stop after it
   achieved the current destination.

--------------------------------------------------------------------------*/
static void Process_Stopped( void )
{
   gpstMotionCtrl->bQuickStop = 0;
   gpstMotionCtrl->bRequestRejected = 0;
   gpstMotionCtrl->uiRejectedDest = CEDES_INVALID_POSITION;
   gpstMotionCtrl->bSpeedLimitChanged = 0;
   gpstMotionCtrl->stRunParameters.uiAddedAccelTime = 0;
   gpstMotionCtrl->stRunParameters.uiRSLTime = 0;
   gpstMotionCtrl->uiRunTimeCounter = 0;
   gpstMotionCtrl->stRunParameters.uiSlowdownDistance = 0;
   gpstMotionCtrl->stRunParameters.uiRampUpDistance = 0;
   gpstMotionCtrl->stRunParameters.uiRSLDecelDistance = 0;
   gbSoftLimitPatternMidflight = 0;
   gbSoftLimitPatternAtStart = 0;
   gpstMotionCtrl->eStartSequence = SEQUENCE_START__PREPARE_TO_RUN;
   gpstMotionCtrl->eStopSequence = SEQUENCE_STOP__RAMP_TO_ZERO;
   gstMotion.wSpeedCMD = 0;
   gstMotion.cDirection = 0;
   gpstMotionCtrl->stRunParameters.ucStartOfRunFloor = 0;
   gpstMotionCtrl->uiPrevTime = 0;
   gpstMotionCtrl->uiPrevTime_Added = 0;
   gpstMotionCtrl->uiPrevTime_RSLDecel = 0;
   gpstMotionCtrl->uiNTSDecelTime = 0;
   gpstMotionCtrl->bReleveling = 0;
   gstMotion.ulActualDest = GetPosition_PositionCount();

   ClearAllMotionRunFlags();

   UpdateEarlyFieldEnable();

   if( !GetPreflightFlag() )
   {
      /* Motion allows commands to send the car automatically to a specific
       * destination only on Auto or if Learning NTS */
      if( ( Rescue_GetRecTrvDirCommand_HPV() )
       && ( !gstFault.bActiveFault ) )
      {
         Process_Stopped_RecTrvDir();
      }
      else if ( CheckIf_ValidDestinationRun() && ( GetCarStoppedFlag() || inReleveling() ) )
      {
         Process_Stopped_Auto();
      }
      else if ( CheckIf_ValidManualRun() )
      {
         Process_Stopped_Manual();
      }
   }
}

/*--------------------------------------------------------------------------

   The motion module should get here after the start sequence and stay here
   even after the car achieves the max inspection speed until the motion command
   changes to a quick or emergency stop (which are set by MRA) or until
   gbUnsafeCondition is high (which is set in the safety module in TRC)

--------------------------------------------------------------------------*/
static void Process_ManualRun( void )
{
   static uint16_t uwMaxSpeed;
   static uint8_t ucIndexScalingCounter;
   uint8_t ucIndexScalingPerCycle = ( gpstMotionCtrl->flPatternTimeRes_sec * 1000 ) / MOD_RUN_PERIOD_MOTION_1MS;
   gstMotion.bRunFlag = 1;

   if ( GetOperation_MotionCmd() == MOCMD__RUN_UP )
   {
      gstMotion.cDirection = DIR__UP;

      if ( GetOperation_BypassTermLimits() || GetPosition_PositionCount() < GetOperation_PositionLimit_UP()  )
      {
         if(!GetOperation_SpeedLimit())
         {
            gstMotion.wSpeedCMD = 0;
         }
         else if ( gbSoftLimitPatternAtStart || GetPosition_PositionCount() < guiSoftLimitPosition )
         {
            if( ++ucIndexScalingCounter >= ucIndexScalingPerCycle )
            {
               ucIndexScalingCounter = 0;
               gpstMotionCtrl->uiRunTimeCounter++;
            }

            if ( gpstMotionCtrl->uiRunTimeCounter > gpstMotionCtrl->stRunParameters.uiPatternAccelTime-1 )
            {
               gpstMotionCtrl->uiRunTimeCounter = gpstMotionCtrl->stRunParameters.uiPatternAccelTime - 1;
            }

            gstMotion.wSpeedCMD = Pattern_GetAccelCommandSpeed( gpstMotionCtrl->uiRunTimeCounter );
         }
         else if ( !gbSoftLimitPatternMidflight )
         {
            uint16_t uwSpeed = Pattern_GetAccelCommandSpeed( gpstMotionCtrl->uiRunTimeCounter );
            if ( uwSpeed > guwSoftLimitSpeed )
            {
               uwMaxSpeed = uwSpeed;

               GeneratePattern( 0, uwMaxSpeed - guwSoftLimitSpeed );

               gpstMotionCtrl->uiRunTimeCounter = 0;

               gbSoftLimitPatternMidflight = 1;
            }
            else /* Soft limit reached, but command speed is still below max speed near terminal */
            {
               if( ++ucIndexScalingCounter >= ucIndexScalingPerCycle )
               {
                  ucIndexScalingCounter = 0;
                  gpstMotionCtrl->uiRunTimeCounter++;
               }

               if ( gpstMotionCtrl->uiRunTimeCounter > gpstMotionCtrl->stRunParameters.uiPatternAccelTime-1 )
               {
                  gpstMotionCtrl->uiRunTimeCounter = gpstMotionCtrl->stRunParameters.uiPatternAccelTime-1;
               }

               uint16_t uwSpeed = Pattern_GetAccelCommandSpeed( gpstMotionCtrl->uiRunTimeCounter );
               if ( uwSpeed < guwSoftLimitSpeed )
               {
                  gstMotion.wSpeedCMD = uwSpeed;
               }
               else
               {
                  gstMotion.wSpeedCMD = guwSoftLimitSpeed;
               }
            }
         }
         else /* Use pattern generated mid-flight to adjust speed near terminal */
         {
            if( ++ucIndexScalingCounter >= ucIndexScalingPerCycle )
            {
               ucIndexScalingCounter = 0;
               gpstMotionCtrl->uiRunTimeCounter++;
            }

            if ( gpstMotionCtrl->uiRunTimeCounter > gpstMotionCtrl->stRunParameters.uiPatternAccelTime-1 )
            {
               gpstMotionCtrl->uiRunTimeCounter = gpstMotionCtrl->stRunParameters.uiPatternAccelTime-1;
            }

            gstMotion.wSpeedCMD = uwMaxSpeed - Pattern_GetAccelCommandSpeed( gpstMotionCtrl->uiRunTimeCounter );
         }
      }
      else /* Terminal Hard Limit Reached */
      {
         EmergencyStop( gpstMotionCtrl );
      }
   }
   else if ( GetOperation_MotionCmd() == MOCMD__RUN_DN )
   {
      gstMotion.cDirection = DIR__DN;

      if ( GetOperation_BypassTermLimits() || GetPosition_PositionCount() > GetOperation_PositionLimit_DN() )
      {
         if(!GetOperation_SpeedLimit())
         {
            gstMotion.wSpeedCMD = 0;
         }
         else if ( gbSoftLimitPatternAtStart || GetPosition_PositionCount() > guiSoftLimitPosition )
         {
            if( ++ucIndexScalingCounter >= ucIndexScalingPerCycle )
            {
               ucIndexScalingCounter = 0;
               gpstMotionCtrl->uiRunTimeCounter++;
            }

            if ( gpstMotionCtrl->uiRunTimeCounter > gpstMotionCtrl->stRunParameters.uiPatternAccelTime-1 )
            {
               gpstMotionCtrl->uiRunTimeCounter = gpstMotionCtrl->stRunParameters.uiPatternAccelTime-1;
            }

            gstMotion.wSpeedCMD = Pattern_GetAccelCommandSpeed( gpstMotionCtrl->uiRunTimeCounter );
         }
         else if ( !gbSoftLimitPatternMidflight )
         {
            uint16_t uwSpeed = Pattern_GetAccelCommandSpeed( gpstMotionCtrl->uiRunTimeCounter );
            if ( uwSpeed > guwSoftLimitSpeed )
            {
               uwMaxSpeed = uwSpeed;

               GeneratePattern( 0, uwMaxSpeed - guwSoftLimitSpeed );

               gpstMotionCtrl->uiRunTimeCounter = 0;

               gbSoftLimitPatternMidflight = 1;
            }
            else /* Soft limit reached, but command speed is still below max speed near terminal */
            {
               if( ++ucIndexScalingCounter >= ucIndexScalingPerCycle )
               {
                  ucIndexScalingCounter = 0;
                  gpstMotionCtrl->uiRunTimeCounter++;
               }

               if ( gpstMotionCtrl->uiRunTimeCounter > gpstMotionCtrl->stRunParameters.uiPatternAccelTime-1 )
               {
                  gpstMotionCtrl->uiRunTimeCounter = gpstMotionCtrl->stRunParameters.uiPatternAccelTime-1;
               }

               uint16_t uwSpeed = Pattern_GetAccelCommandSpeed( gpstMotionCtrl->uiRunTimeCounter );
               if ( uwSpeed < guwSoftLimitSpeed )
               {
                  gstMotion.wSpeedCMD = uwSpeed;
               }
               else
               {
                  gstMotion.wSpeedCMD = guwSoftLimitSpeed;
               }
            }
         }
         else /* Use pattern generated mid-flight to adjust speed near terminal */
         {
            if( ++ucIndexScalingCounter >= ucIndexScalingPerCycle )
            {
               ucIndexScalingCounter = 0;
               gpstMotionCtrl->uiRunTimeCounter++;
            }

            if ( gpstMotionCtrl->uiRunTimeCounter > gpstMotionCtrl->stRunParameters.uiPatternAccelTime-1 )
            {
               gpstMotionCtrl->uiRunTimeCounter = gpstMotionCtrl->stRunParameters.uiPatternAccelTime-1;
            }

            gstMotion.wSpeedCMD = uwMaxSpeed - Pattern_GetAccelCommandSpeed( gpstMotionCtrl->uiRunTimeCounter );
         }

         gstMotion.wSpeedCMD *= -1;
      }
      else /* Terminal Hard Limit Reached */
      {
         EmergencyStop( gpstMotionCtrl );
      }
   }
   else  /* Motion CMD Changed */
   {
      gpstMotionCtrl->eMotionState = MOTION__STOP_SEQUENCE;

      gpstMotionCtrl->eStopSequence = SEQUENCE_STOP__RAMP_TO_ZERO;
   }

   gpstMotionCtrl->wEndOfRunSpeed_FPM = gstMotion.wSpeedCMD;
}
/*--------------------------------------------------------------------------
   Accel chang during run
--------------------------------------------------------------------------*/
static void Accelerating_MidflightDestinationChange()
{
   uint32_t uiTimeIndex = 0;
   uint32_t uiCurrentPosition = GetPosition_PositionCount();
   if ( gstMotion.cDirection == DIR__UP )
   {
      for ( uiTimeIndex = gpstMotionCtrl->uiPrevTime_Added; uiTimeIndex < gpstMotionCtrl->stRunParameters.uiAddedAccelTime - 1; uiTimeIndex++ )
      {
         if ( uiCurrentPosition >= ( gpstMotionCtrl->stRunParameters.uiStartPosition_AddedAccel + Pattern_GetAddedAccelPosition( uiTimeIndex ) )
           && uiCurrentPosition <  ( gpstMotionCtrl->stRunParameters.uiStartPosition_AddedAccel + Pattern_GetAddedAccelPosition( uiTimeIndex + 1 ) )
            )
         {
            gpstMotionCtrl->uiPrevTime_Added = uiTimeIndex;
            break;
         }
      }

      gstMotion.wSpeedCMD = Pattern_GetAddedAccelCommandSpeed( gpstMotionCtrl->uiPrevTime_Added );
   }
   else //if ( gstMotion.cDirection == DIR__DN )
   {
      for ( uiTimeIndex = gpstMotionCtrl->uiPrevTime_Added; uiTimeIndex < gpstMotionCtrl->stRunParameters.uiAddedAccelTime - 1; uiTimeIndex++ )
      {
         if ( uiCurrentPosition <= ( gpstMotionCtrl->stRunParameters.uiStartPosition_AddedAccel - Pattern_GetAddedAccelPosition( uiTimeIndex ) )
           && uiCurrentPosition >  ( gpstMotionCtrl->stRunParameters.uiStartPosition_AddedAccel - Pattern_GetAddedAccelPosition( uiTimeIndex + 1 ) )
            )
         {
            gpstMotionCtrl->uiPrevTime_Added = uiTimeIndex;
            break;
         }
      }
      gstMotion.wSpeedCMD = -1 * ( Pattern_GetAddedAccelCommandSpeed( gpstMotionCtrl->uiPrevTime_Added ) );
   }
}
/*--------------------------------------------------------------------------
   Starndard run w/o midflight destination change
--------------------------------------------------------------------------*/
static void Accelerating_Default()
{
   uint32_t uiTimeIndex = 0;
   uint32_t uiCurrentPosition = GetPosition_PositionCount();
   if ( gstMotion.cDirection == DIR__UP )
   {
      for ( uiTimeIndex = gpstMotionCtrl->uiPrevTime; uiTimeIndex < gpstMotionCtrl->stRunParameters.uiPatternAccelTime - 1; uiTimeIndex++ )
      {
         uint32_t uiLowerPosition = gpstMotionCtrl->stRunParameters.uiStartPosition_Accel + Pattern_GetAccelPosition( uiTimeIndex );
         uint32_t uiHigherPosition = gpstMotionCtrl->stRunParameters.uiStartPosition_Accel + Pattern_GetAccelPosition( uiTimeIndex + 1 );
         if ( ( uiCurrentPosition >= uiLowerPosition )
           && ( uiCurrentPosition <  uiHigherPosition ) )
         {
            gpstMotionCtrl->uiPrevTime = uiTimeIndex;
            break;
         }
      }
      gstMotion.wSpeedCMD = Pattern_GetAccelCommandSpeed( gpstMotionCtrl->uiPrevTime );
   }
   else //if ( gstMotion.cDirection == DIR__DN )
   {
      for ( uiTimeIndex = gpstMotionCtrl->uiPrevTime; uiTimeIndex < gpstMotionCtrl->stRunParameters.uiPatternAccelTime - 1; uiTimeIndex++ )
      {
         uint32_t uiLowerPosition = gpstMotionCtrl->stRunParameters.uiStartPosition_Accel - Pattern_GetAccelPosition( uiTimeIndex + 1 );
         uint32_t uiHigherPosition = gpstMotionCtrl->stRunParameters.uiStartPosition_Accel - Pattern_GetAccelPosition( uiTimeIndex );
         if ( ( uiCurrentPosition <= uiHigherPosition )
           && ( uiCurrentPosition >  uiLowerPosition ) )
         {
            gpstMotionCtrl->uiPrevTime = uiTimeIndex;
            break;
         }
      }

      gstMotion.wSpeedCMD = -1 * Pattern_GetAccelCommandSpeed( gpstMotionCtrl->uiPrevTime );
   }
}
/*--------------------------------------------------------------------------

   The motion module shoud get here from the start sequence and stay here
   until the position feedback reaches gpstMotionCtrl->stRunParameters.uiStartPosition_Cruise.
   Motion will move to the cruising state from here even if the car does not
   attain contract speed, unless the motion command
   changes to a quick or emergency stop (which are set by MRA) or when
   gbUnsafeCondition is high (which is set in the safety module in TRC)

--------------------------------------------------------------------------*/
static void Process_Accelerating( void )
{
   gstMotion.bRunFlag = 1;
   uint32_t uiCurrentPosition = GetPosition_PositionCount();
   if ( gstMotion.cDirection == DIR__UP )
   {
      if  ( ( uiCurrentPosition < gpstMotionCtrl->stRunParameters.uiStartPosition_Cruise )
         && ( uiCurrentPosition < gpstMotionCtrl->stRunParameters.uiStartPosition_Decel ) )
      {
         if( gpstMotionCtrl->stRunParameters.uiAddedAccelTime
        && ( uiCurrentPosition > gpstMotionCtrl->stRunParameters.uiStartPosition_AddedAccel ) )
         {
            Accelerating_MidflightDestinationChange();
         }
         else
         {
            Accelerating_Default();
         }
      }
      else
      {
         gpstMotionCtrl->eMotionState = MOTION__CRUISING;
      }
   }
   else /* DIR__DN */
   {
      if  ( ( uiCurrentPosition > gpstMotionCtrl->stRunParameters.uiStartPosition_Cruise )
         && ( uiCurrentPosition > gpstMotionCtrl->stRunParameters.uiStartPosition_Decel ) )
      {
         if( gpstMotionCtrl->stRunParameters.uiAddedAccelTime
        && ( uiCurrentPosition < gpstMotionCtrl->stRunParameters.uiStartPosition_AddedAccel ) )
         {
            Accelerating_MidflightDestinationChange();
         }
         else
         {
            Accelerating_Default();
         }
      }
      else
      {
         gpstMotionCtrl->eMotionState = MOTION__CRUISING;
      }
   }

   /* Fix for rollback during start sequence */
   if( gstMotion.wSpeedCMD == 0 )
   {
      gstMotion.wSpeedCMD = 1 * gstMotion.cDirection;
   }
   gpstMotionCtrl->wEndOfRunSpeed_FPM = gstMotion.wSpeedCMD;
}

/*--------------------------------------------------------------------------

   Motion gets here from the accel state and only stays here if the car will
   be cruising at contract speed.
   It only transitions to the decel state from here unless the motion command
   changes to a quick or emergency stop (which are set by MRA) or when
   gbUnsafeCondition is high (which is set in the safety module in TRC)

--------------------------------------------------------------------------*/
static void Process_Cruising( void )
{
   gstMotion.bRunFlag = 1;
   uint32_t uiCurrentPosition = GetPosition_PositionCount();
   if (  ( ( uiCurrentPosition <= gpstMotionCtrl->stRunParameters.uiStartPosition_Decel ) && ( gstMotion.cDirection == DIR__UP ) )
      || ( ( uiCurrentPosition >= gpstMotionCtrl->stRunParameters.uiStartPosition_Decel ) && ( gstMotion.cDirection == DIR__DN ) )
      )
   {
      if( gpstMotionCtrl->bSpeedLimitChanged )
      {
         gstMotion.wSpeedCMD = gpstMotionCtrl->uwMaxSpeedOfCurrentRun_FPM;
      }
      else if ( gpstMotionCtrl->stRunParameters.uiAddedAccelTime )
      {
         gstMotion.wSpeedCMD = Pattern_GetAddedAccelCommandSpeed( gpstMotionCtrl->stRunParameters.uiAddedAccelTime - 1 );
      }
      else
      {
         gstMotion.wSpeedCMD = Pattern_GetAccelCommandSpeed( gpstMotionCtrl->stRunParameters.uiPatternAccelTime - 1 );
      }

      if ( gstMotion.cDirection == DIR__DN )
      {
         gstMotion.wSpeedCMD *= -1;
      }
   }
   else
   {
      if( !( gpstMotionCtrl->bAcceptanceBypassDecel && ( GetOperation_AutoMode() == MODE_A__TEST ) ) )
      {
         gpstMotionCtrl->eMotionState = MOTION__DECELERATING;
         gpstMotionCtrl->uiPrevTime = gpstMotionCtrl->stRunParameters.uiPatternDecelTime - 1;
      }
      else
      {
         if (  CheckIfDestinationReached() )
         {
            EmergencyStop( gpstMotionCtrl );
         }
      }
   }
   gpstMotionCtrl->wEndOfRunSpeed_FPM = gstMotion.wSpeedCMD;
}

/*--------------------------------------------------------------------------

   Motion uses the APS speed feedback to determine which speed command to
   send to the drive.
   stCurveParameters[MOTION_PROFILE__1].uwLevelingDistance is an extra leveling distance to acount for position lag.
   Motion will hold 1/-1 fpm speed command until the car makes the exact
   floor position.
   Motion transitions to the stop sequence from here, if not interrupted by
   a quick stop or emergency stop.

--------------------------------------------------------------------------*/
static void Process_Decelerating( void )
{
   Motion_Profile eProfile = gstMotion.eProfile;
   enum direction_enum eDir = gstMotion.cDirection;
   int16_t wLevelingVelocity = eDir * ( gpstCurveParameters + eProfile )->uwLevelingSpeed_FPM;
   uint16_t uwLevelingDistance = ( gpstCurveParameters + eProfile )->uwLevelingDistance;
   uint32_t uiDestinationPosition = gstMotion.ulActualDest;
   uint32_t uiCurrentPosition = GetPosition_PositionCount();
   gstMotion.bRunFlag = 1;

   if ( eDir == DIR__UP )
   {
      if  ( uiCurrentPosition < uiDestinationPosition )
      {
         for ( int32_t iTimeIndex = gpstMotionCtrl->uiPrevTime; iTimeIndex > 0; iTimeIndex-- )
         {
            if ( uiCurrentPosition >= ( uiDestinationPosition - Pattern_GetDecelPosition( iTimeIndex ) - uwLevelingDistance )
              && uiCurrentPosition < ( uiDestinationPosition - Pattern_GetDecelPosition( iTimeIndex - 1 ) - uwLevelingDistance ) )
            {
               gpstMotionCtrl->uiPrevTime = iTimeIndex;
               break;
            }
         }

         gstMotion.wSpeedCMD = Pattern_GetDecelCommandSpeed( gpstMotionCtrl->uiPrevTime );

         if ( gstMotion.wSpeedCMD < wLevelingVelocity )
         {
            gstMotion.wSpeedCMD = wLevelingVelocity;
         }
      }
      else
      {
         gpstMotionCtrl->eMotionState = MOTION__STOP_SEQUENCE;
      }
   }
   else /* DIR__DN */
   {
      if  ( uiCurrentPosition > uiDestinationPosition )
      {
         for ( int32_t iTimeIndex = gpstMotionCtrl->uiPrevTime; iTimeIndex > 0; iTimeIndex-- )
         {
            if ( uiCurrentPosition <= ( uiDestinationPosition + Pattern_GetDecelPosition( iTimeIndex ) + uwLevelingDistance )
              && uiCurrentPosition > ( uiDestinationPosition + Pattern_GetDecelPosition( iTimeIndex - 1 ) + uwLevelingDistance ) )
            {
               gpstMotionCtrl->uiPrevTime = iTimeIndex;
               break;
            }
         }

         gstMotion.wSpeedCMD = -1 * Pattern_GetDecelCommandSpeed( gpstMotionCtrl->uiPrevTime );

         if ( gstMotion.wSpeedCMD > wLevelingVelocity )
         {
            gstMotion.wSpeedCMD = wLevelingVelocity;
         }
      }
      else
      {
         gpstMotionCtrl->eMotionState = MOTION__STOP_SEQUENCE;
      }
   }

   if( gstMotion.wSpeedCMD == wLevelingVelocity )
   {
      gpstMotionCtrl->bLeveling = 1;
   }

   gpstMotionCtrl->wEndOfRunSpeed_FPM = gstMotion.wSpeedCMD;
}
/*-----------------------------------------------------------------------------
   This is the state when performing an HPV recommended travel direction check
 -----------------------------------------------------------------------------*/
static void Process_RecTrvDir( void )
{
   gstMotion.bRunFlag = 1;
   if( Rescue_GetRecTrvDirCommand_HPV() )
   {
      gstMotion.wSpeedCMD = 0;
      gpstMotionCtrl->wEndOfRunSpeed_FPM = gstMotion.wSpeedCMD;
   }
   else if( Rescue_GetRecTrvDirCommand_KEB() )
   {
      uint16_t uwLevelingSpeed = Param_ReadValue_16Bit(enPARAM16__LevelingSpeed);
      if(uwLevelingSpeed > MAX_LEVELING_SPEED)
      {
         uwLevelingSpeed = MAX_LEVELING_SPEED;
      }
      else if(uwLevelingSpeed < MIN_LEVELING_SPEED)
      {
         uwLevelingSpeed = MIN_LEVELING_SPEED;
      }
      gstMotion.wSpeedCMD = uwLevelingSpeed;
      gpstMotionCtrl->wEndOfRunSpeed_FPM = gstMotion.wSpeedCMD;
   }
   else
   {
      gpstMotionCtrl->eMotionState = MOTION__STOP_SEQUENCE;
   }
}
/*-----------------------------------------------------------------------------

    This is to limit the difference in the speed comanded to the drive. after
    generating the pattern, the motion modules relies on the position feedback
    to determine the next speed command. In certain occasions, the position
    feedback isn't accruate and could cause a jump in the speed command

 -----------------------------------------------------------------------------*/
static void SpeedCommandDeviationControl( void )
{
   static int16_t wMaxSpeedCMD;
   if ( ( Param_ReadValue_1Bit( enPARAM1__EnableSpeedDevControl ) )
     && ( gstMotion.bRunFlag )
     && ( !CheckIfEmergencyStop() )
     && ( gpstMotionCtrl->eMotionState != MOTION__STOP_SEQUENCE )
     && ( !gpstMotionCtrl->bQuickStop ) )
   {
      if ( gstMotion.wSpeedCMD > 0 )
      {
         if ( gstMotion.wSpeedCMD > 100 )
         {
            if ( gstMotion.wSpeedCMD > gpstMotionCtrl->wLastSpeedCMD_FPM )
            {
               wMaxSpeedCMD = 1.1 * gpstMotionCtrl->wLastSpeedCMD_FPM;

               if ( gstMotion.wSpeedCMD > wMaxSpeedCMD )
               {
                  gstMotion.wSpeedCMD = wMaxSpeedCMD;
               }
            }
            else if ( gstMotion.wSpeedCMD < gpstMotionCtrl->wLastSpeedCMD_FPM )
            {
               wMaxSpeedCMD = 0.9 * gpstMotionCtrl->wLastSpeedCMD_FPM;

               if ( gstMotion.wSpeedCMD < wMaxSpeedCMD )
               {
                  gstMotion.wSpeedCMD = wMaxSpeedCMD;
               }
            }
         }
         else // gstMotion.wSpeedCMD < 100
         {
            if ( gstMotion.wSpeedCMD > gpstMotionCtrl->wLastSpeedCMD_FPM )
            {
               wMaxSpeedCMD = gpstMotionCtrl->wLastSpeedCMD_FPM + 10;

               if ( gstMotion.wSpeedCMD > wMaxSpeedCMD )
               {
                  gstMotion.wSpeedCMD = wMaxSpeedCMD;
               }
            }
            else if ( gstMotion.wSpeedCMD < gpstMotionCtrl->wLastSpeedCMD_FPM )
            {
               wMaxSpeedCMD = gpstMotionCtrl->wLastSpeedCMD_FPM - 10;

               if ( gstMotion.wSpeedCMD < wMaxSpeedCMD )
               {
                  gstMotion.wSpeedCMD = wMaxSpeedCMD;
               }
            }
         }
      }
      else if ( gstMotion.wSpeedCMD < 0 )
      {
         if ( gstMotion.wSpeedCMD < -100 )
         {
            if ( gstMotion.wSpeedCMD < gpstMotionCtrl->wLastSpeedCMD_FPM )
            {
               wMaxSpeedCMD = 1.1 * gpstMotionCtrl->wLastSpeedCMD_FPM;

               if ( gstMotion.wSpeedCMD < wMaxSpeedCMD )
               {
                  gstMotion.wSpeedCMD = wMaxSpeedCMD;
               }
            }
            else if ( gstMotion.wSpeedCMD > gpstMotionCtrl->wLastSpeedCMD_FPM )
            {
               wMaxSpeedCMD = 0.9 * gpstMotionCtrl->wLastSpeedCMD_FPM;

               if ( gstMotion.wSpeedCMD > wMaxSpeedCMD )
               {
                  gstMotion.wSpeedCMD = wMaxSpeedCMD;
               }
            }
         }
         else // gstMotion.wSpeedCMD > -100
         {
            if ( gstMotion.wSpeedCMD < gpstMotionCtrl->wLastSpeedCMD_FPM )
            {
               wMaxSpeedCMD = gpstMotionCtrl->wLastSpeedCMD_FPM - 10;

               if ( gstMotion.wSpeedCMD < wMaxSpeedCMD )
               {
                  gstMotion.wSpeedCMD = wMaxSpeedCMD;
               }
            }
            else if ( gstMotion.wSpeedCMD > gpstMotionCtrl->wLastSpeedCMD_FPM )
            {
               wMaxSpeedCMD = gpstMotionCtrl->wLastSpeedCMD_FPM + 10;

               if ( gstMotion.wSpeedCMD > wMaxSpeedCMD )
               {
                  gstMotion.wSpeedCMD = wMaxSpeedCMD;
               }
            }
         }
      }
   }
}

/*-----------------------------------------------------------------------------
   Update structure for motion variable access in the rest of the system
 -----------------------------------------------------------------------------*/
static void UpdateGlobalMotionStructure( void )
{
   SetMotion_RunFlag( gstMotion.bRunFlag );
   SetMotion_Direction( gstMotion.cDirection );
   SetMotion_SpeedCommand( gstMotion.wSpeedCMD );
   SetMotion_Destination( gstMotion.ulActualDest );
   SetMotion_Profile( gstMotion.eProfile );
}
/*-----------------------------------------------------------------------------
   Checks if the pattern size for a run has exceeded the max size
 -----------------------------------------------------------------------------*/
static void CheckFor_InvalidSCurveFault()
{
   if( gpstMotionCtrl->stRunParameters.uiPatternAccelTime >= MAX_PATTERN_SIZE_BYTES )
   {
      SetFault(FLT__INVALID_ACCEL_CURVE);
   }
   /* The decel lookup table curve is not used while in inspection, do not check it */
   else if( ( GetOperation_ClassOfOp() != CLASSOP__MANUAL ) && ( gpstMotionCtrl->stRunParameters.uiPatternDecelTime >= MAX_PATTERN_SIZE_BYTES ) )
   {
      SetFault(FLT__INVALID_DECEL_CURVE);
   }
   else if( gpstMotionCtrl->stRunParameters.uiAddedAccelTime >= MAX_PATTERN_SIZE_BYTES )
   {
      SetFault(FLT__INVALID_ADDED_CURVE);
   }
   else if( gpstMotionCtrl->stRunParameters.uiRSLTime >= MAX_PATTERN_SIZE_BYTES )
   {
      SetFault(FLT__INVALID_RSL_CURVE);
   }
}
/*-----------------------------------------------------------------------------
   Check for violations of max scurve size. Differs from
   CheckFor_InvalidSCurveFault() in that it checks for max ramp
   time violations, instead of generated ramp time violations.
 -----------------------------------------------------------------------------*/
static void CheckFor_InvalidSCurveFault_2(Motion_Profile eProfile)
{
   // Ignore very short profile bc/ we expect it is detuned, but is not a ramp length risk
   if( ( eProfile != MOTION_PROFILE__4 )
    && ( Pattern_CheckForInvalidSCurve(eProfile) ) )
   {
      SetFault(FLT__INVALID_CURVE_P1+eProfile);
   }
}
/*-----------------------------------------------------------------------------
   MUST KEEP AT BOTTOM OF MODULE JUST ABOVE SetMotion_SpeedCommand
   Checks for activation of acceptance overspeed test.
   If active, will overwrite the commanded speed to 110% of the current contract speed
 -----------------------------------------------------------------------------*/
static void CheckFor_OverspeedAcceptanceTest()
{
   if( ( gpstMotionCtrl->eMotionState == MOTION__MANUAL_RUN )
    && ( GetOperation_AutoMode() == MODE_A__TEST )
    && ( GetActiveAcceptanceTest() == ACCEPTANCE_TEST_INSPECTION_SPEED )
    && ( gpstMotionCtrl->bAcceptanceTriggerOverspeed ) )
   {
      gstMotion.wSpeedCMD = ( Param_ReadValue_16Bit( enPARAM16__InspectionSpeed ) * 1.10f ) + 0.5f;
      gstMotion.wSpeedCMD *= gstMotion.cDirection;
   }
}
/*-----------------------------------------------------------------------------
   Sanity check: If car should be decelerating, and speed has increased, then fault
   Speed should never increase while decelerating or stopped
 -----------------------------------------------------------------------------*/
static void CheckFor_InvalidCommand(void)
{
   /* Bypass this check if in leveling mode as long as the speed command is below the minimum leveling speed.
    * This is because in cases where the run distance is less than the leveling distance, the speed will jump
    * directly to the leveling stage. This will cause a one cycle jump in speed from the minimum acceleration
    * speed to the leveling speed upon entering the decel stage */
   uint16_t uwSpeedCmd = gstMotion.wSpeedCMD*gstMotion.cDirection;
   uint8_t bValidDecelStage = ( Motion_GetMotionState() == MOTION__DECELERATING )
                           && ( !gpstMotionCtrl->bLeveling || ( uwSpeedCmd > MAX_LEVELING_SPEED ) );
   if( gpstMotionCtrl->bQuickStop
  || ( Motion_GetMotionState() == MOTION__STOPPED )
  || ( Motion_GetMotionState() == MOTION__START_SEQUENCE )
  || ( Motion_GetMotionState() == MOTION__STOP_SEQUENCE )
  || ( bValidDecelStage ) )
   {
      if( gpstMotionCtrl->wLastSpeedCMD_FPM > 0 )
      {
         if( gstMotion.wSpeedCMD > gpstMotionCtrl->wLastSpeedCMD_FPM )
         {
            SetFault(FLT__MOTION_INVALID_CMD);
            EmergencyStop(gpstMotionCtrl);
         }
      }
      else if( gpstMotionCtrl->wLastSpeedCMD_FPM < 0 )
      {
         if( gstMotion.wSpeedCMD < gpstMotionCtrl->wLastSpeedCMD_FPM )
         {
            SetFault(FLT__MOTION_INVALID_CMD);
            EmergencyStop(gpstMotionCtrl);
         }
      }
      else
      {
         if( ( gstMotion.wSpeedCMD < gpstMotionCtrl->wLastSpeedCMD_FPM )
          || ( gstMotion.wSpeedCMD > gpstMotionCtrl->wLastSpeedCMD_FPM ) )
         {
            SetFault(FLT__MOTION_INVALID_CMD);
            EmergencyStop(gpstMotionCtrl);
         }
      }
   }
}
/*--------------------------------------------------------------------------
   For both the up and down directions:
      Takes 10 evenly spaced time index points from the overall ramp down time.
      These points are offset by half of their difference.
      Finds the speed and absolute position at those points and updates system parameters.

--------------------------------------------------------------------------*/
#if 0
static void UpdateNTSTripPoints( Motion_Profile eProfile )
{
   if(!Param_ReadValue_1Bit(enPARAM1__DEBUG_DisableUpdateNTS))
   {
      Motion_Profile eSavedProfile = GetMotion_Profile();
      SetMotion_Profile(eProfile);
      //Calculate the decel pattern for max speed to get the slowdown distance
      if( eProfile == MOTION_PROFILE__3 )
      {
         Pattern_GenerateDecelRun(Param_ReadValue_16Bit(enPARAM16__EPowerSpeed_fpm)/60.0f);
      }
      else
      {
         Pattern_GenerateDecelRun(gpstMotionCtrl->flContractSpeed_FPS);
      }
      uint32_t uiTimeBetweenTripPoints = gpstMotionCtrl->stRunParameters.uiPatternDecelTime/NUM_NTS_TRIP_POINTS;
      uint32_t uiTimeOffset =  uiTimeBetweenTripPoints/2;
      for(uint8_t i = 0; i < NUM_NTS_TRIP_POINTS; i++)
      {
         uint32_t uiTimeIndex = (uiTimeBetweenTripPoints * i) + uiTimeOffset;
         uint32_t uiRelativePosition = Pattern_GetDecelPosition(uiTimeIndex) + ( gpstCurveParameters + eProfile )->uwLevelingDistance;

         uint16_t uwTripSpeed = Pattern_GetDecelCommandSpeed(uiTimeIndex);

         if( eProfile == MOTION_PROFILE__1 )
         {
            Param_WriteValue_16Bit(enPARAM16__NTS_POS_P1_0 + i, uiRelativePosition/NTS_POSITION_SCALING);
            Param_WriteValue_16Bit(enPARAM16__NTS_VEL_P1_0 + i, uwTripSpeed);
         }
         else if( eProfile == MOTION_PROFILE__2 )
         {
            Param_WriteValue_16Bit(enPARAM16__NTS_POS_P2_0 + i, uiRelativePosition/NTS_POSITION_SCALING);
            Param_WriteValue_16Bit(enPARAM16__NTS_VEL_P2_0 + i, uwTripSpeed);
         }
         else if( eProfile == MOTION_PROFILE__3 )
         {
            Param_WriteValue_16Bit(enPARAM16__NTS_POS_P3_0 + i, uiRelativePosition/NTS_POSITION_SCALING);
            Param_WriteValue_16Bit(enPARAM16__NTS_VEL_P3_0 + i, uwTripSpeed);
         }
         else if( eProfile == MOTION_PROFILE__4 )
         {
            Param_WriteValue_16Bit(enPARAM16__NTS_POS_P4_0 + i, uiRelativePosition/NTS_POSITION_SCALING);
            Param_WriteValue_16Bit(enPARAM16__NTS_VEL_P4_0 + i, uwTripSpeed);
         }
      }
      SetMotion_Profile(eSavedProfile);
      Pattern_GenerateDecelRun(gpstMotionCtrl->flContractSpeed_FPS);
   }
}
#endif
/*--------------------------------------------------------------------------
   For both the up and down directions:
      Takes 10 evenly spaced time index points from the overall ramp down time.
      These points are offset by half of their difference.
      Finds the speed and absolute position at those points and updates system parameters.

--------------------------------------------------------------------------*/
static void UpdateETSTripPoints( Motion_Profile eProfile )
{
   Motion_Profile eSavedProfile = GetMotion_Profile();
   SetMotion_Profile(eProfile);
   //Calculate the decel pattern for max speed to get the slowdown distance
   if( eProfile == MOTION_PROFILE__3 )
   {
      Pattern_GenerateDecelRun(Param_ReadValue_16Bit(enPARAM16__EPowerSpeed_fpm)/60.0f);
   }
   else
   {
      Pattern_GenerateDecelRun(gpstMotionCtrl->flContractSpeed_FPS);
   }
   uint32_t uiTimeBetweenTripPoints = gpstMotionCtrl->stRunParameters.uiPatternDecelTime/NUM_NTS_TRIP_POINTS;
   uint32_t uiTimeOffset =  uiTimeBetweenTripPoints/2;
   uint16_t uwOffsetFromNTS = Param_ReadValue_8Bit( enPARAM8__ETS_OffsetFromNTS_5mm ) * 10;

   for(uint8_t i = 0; i < NUM_ETS_TRIP_POINTS; i++)
   {
      uint32_t uiTimeIndex = (uiTimeBetweenTripPoints * i) + uiTimeOffset;
      uint32_t uiRelativePosition = Pattern_GetDecelPosition(uiTimeIndex) + ( gpstCurveParameters + eProfile )->uwLevelingDistance;
      if( uwOffsetFromNTS < uiRelativePosition )
      {
         uiRelativePosition -= uwOffsetFromNTS;
      }
      else
      {
         uiRelativePosition = 0;
      }

      uint16_t uwTripSpeed = Pattern_GetDecelCommandSpeed(uiTimeIndex);

      SetETS_Speed( uwTripSpeed, i, eProfile );
      SetETS_Position( uiRelativePosition, i, eProfile );
   }
   SetMotion_Profile(eSavedProfile);
   Pattern_GenerateDecelRun(gpstMotionCtrl->flContractSpeed_FPS);
}
/*-----------------------------------------------------------------------------
Returns 1 if emergency stop is needed
   ESTOP if:
      Class of operation changes
      Operation commands and ESTOP
      Operation command switches direction
      Any fault is active

 -----------------------------------------------------------------------------*/
static uint8_t CheckIfEmergencyStop()
{
   static enum en_classop ePrevClassOfOperation;
   static enum en_mocmd eLastMotionCommand;
   uint8_t bReturn = 0;
   enum en_mocmd eCommand = GetOperation_MotionCmd();
   if( gpstMotionCtrl->eMotionState != MOTION__STOPPED )
   {
      if( ( gstFault.bActiveFault )
       || ( eCommand == MOCMD__EMERG_STOP )
       || ( ePrevClassOfOperation != GetOperation_ClassOfOp() ) )
      {
         bReturn = 1;
      }
   }
   eLastMotionCommand = GetOperation_MotionCmd();
   ePrevClassOfOperation = GetOperation_ClassOfOp();
   return bReturn;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t CheckIfQuickStop()
{
   uint8_t bQuickStop = 1;

   if( ( GetOperation_MotionCmd() != MOCMD__QUICK_STOP )
    && !gpstMotionCtrl->bQuickStop )
   {
      bQuickStop = 0;
   }
   else if( ( GetOperation_AutoMode() == MODE_A__TEST )
         && gpstMotionCtrl->bAcceptanceBypassDecel )
   {
      bQuickStop = 0;

      if( ( gstDrive.eType == enDriveType__DSD )
       && ( GetActiveAcceptanceTest() == ACCEPTANCE_TEST_NTS ) )
      {
         bQuickStop = 1;
      }
   }
   else if( ( gpstMotionCtrl->eMotionState == MOTION__STOP_SEQUENCE )
         || ( gpstMotionCtrl->eMotionState == MOTION__STOPPED ) )
   {
      bQuickStop = 0;
   }

   return bQuickStop;
}
/*-----------------------------------------------------------------------------
   Reset motion start/stop sequence timers on change of gpstMotionCtrl->eMotionState
 -----------------------------------------------------------------------------*/
static void CheckFor_ResetMotionSequenceTimers()
{
   static enum en_motion_state ePrevMotionState;
   if( ePrevMotionState != gpstMotionCtrl->eMotionState )
   {
      ePrevMotionState = gpstMotionCtrl->eMotionState;
      for( uint8_t i = 0; i < NUM_MOTION_START_SEQUENCE; i++ )
      {
         gpstMotionCtrl->stTimers.auiStartTimers[ i ] = 0;
      }
      for( uint8_t i = 0; i < NUM_MOTION_STOP_SEQUENCE; i++ )
      {
         gpstMotionCtrl->stTimers.auiStopTimers[ i ] = 0;
      }
   }
}

/*-----------------------------------------------------------------------------
   Updates estimated max floor to floor time
 -----------------------------------------------------------------------------*/
static void UpdateMaxFloorToFloorTime( Motion_Profile eProfile )
{
   if( eProfile == MOTION_PROFILE__1 )
   {
      uint32_t uiAvgFloorDistance_05mm = 0;
      for( uint8_t i = 0; i < GetFP_NumFloors()-1; i++ )
      {
         uiAvgFloorDistance_05mm += Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0+i+1) - Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0+i);
      }
      uiAvgFloorDistance_05mm /= ( GetFP_NumFloors()-1 );

      Motion_Profile eLastProfile = GetMotion_Profile();
      SetMotion_Profile(eProfile);
      GeneratePattern(uiAvgFloorDistance_05mm, GetFP_ContractSpeed());
      float fTopSpeedTime_sec = ( ( uiAvgFloorDistance_05mm - gpstMotionCtrl->stRunParameters.uiRampUpDistance - gpstMotionCtrl->stRunParameters.uiSlowdownDistance - ( gpstCurveParameters + eProfile )->uwLevelingDistance ) / HALF_MM_PER_FT )
                              / ( gpstMotionCtrl->stRunParameters.uwMaxRunSpeed_FPM / 60.0f );
      float fLevelingTime_sec = ( ( gpstCurveParameters + eProfile )->uwLevelingDistance / HALF_MM_PER_FT )
                              / ( ( gpstCurveParameters + eProfile )->uwLevelingSpeed_FPM / 60.0f );
      float fMaxFloorToFloorTime_s = gpstMotionCtrl->stAccelVars.fEndTime_JerkOutAccel + gpstMotionCtrl->stDecelVars.fEndTime_JerkOutDecel;
      fMaxFloorToFloorTime_s += fLevelingTime_sec;
      fMaxFloorToFloorTime_s += fTopSpeedTime_sec;

      ucMaxFloorToFloorTime_s = fMaxFloorToFloorTime_s + 0.5f;
      SetMotion_Profile(eLastProfile);
   }
}
/*-----------------------------------------------------------------------------
   Update curve parameters & ETS/NTS points
 -----------------------------------------------------------------------------*/
static void UpdateMotionProfiles()
{
   uint8_t bCurveUpdated = 0;
   for( Motion_Profile i = 0; i < NUM_MOTION_PROFILES; ++i )
   {
      if( Pattern_UpdateParameters( i ) )
      {
         bCurveUpdated = 1;
         Pattern_UpdateCurveLimits( i );
         UpdateETSTripPoints( i );
         CheckFor_InvalidSCurveFault_2( i );
         UpdateMaxFloorToFloorTime( i );
         break;
      }
   }
   if(bCurveUpdated)
   {
      SetFault(FLT__SCURVE_UPDATING);
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 500;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_MOTION_1MS;

   gstMotion.eProfile = MOTION_PROFILE__1;
   gstMotion.bRunFlag = 0;
   gstMotion.wSpeedCMD = 0;
   gstMotion.cDirection = 0;

   for( Motion_Profile i = 0; i < NUM_MOTION_PROFILES; ++i )
   {
      if( Pattern_UpdateParameters( i ) )
      {
         Pattern_UpdateCurveLimits( i );
         UpdateETSTripPoints( i );
         CheckFor_InvalidSCurveFault_2( i );
         UpdateMaxFloorToFloorTime( i );
      }
   }

   return 0;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t CheckIfMidflightDestChange()
{
   uint8_t bReturn = 0;
   int16_t wOffset_05mm = ( GetOperation_RequestedDestination() >= GetPosition_PositionCount() )
                        ? ( -1*Param_ReadValue_8Bit(enPARAM8__DestOffsetUp_05mm) )
                        : ( Param_ReadValue_8Bit(enPARAM8__DestOffsetDown_05mm) );
   if( gpstMotionCtrl->bReleveling )
   {
      wOffset_05mm = ( GetOperation_RequestedDestination() >= GetPosition_PositionCount() )
                   ? ( -1*Param_ReadValue_8Bit(enPARAM8__RelevelOffsetUp_05mm) )
                   : ( Param_ReadValue_8Bit(enPARAM8__RelevelOffsetDown_05mm) );
   }
   uint32_t uiOffsetDestination_05mm = GetOperation_RequestedDestination() + wOffset_05mm;
   if ( Param_ReadValue_1Bit(enPARAM1__EnableMidFlightDestinationChange)
   && ( !gpstMotionCtrl->bReleveling )
   && ( uiOffsetDestination_05mm != gstMotion.ulActualDest )
   && ( ( !gpstMotionCtrl->bRequestRejected )
     || ( uiOffsetDestination_05mm != gpstMotionCtrl->uiRejectedDest ) )
   && !gpstMotionCtrl->stRunParameters.uiAddedAccelTime )
   {
      bReturn = 1;
   }
   return bReturn;
}

/*-----------------------------------------------------------------------------
todo set fault
 -----------------------------------------------------------------------------*/
static void BoundCommandByContractSpeed()
{
   int16_t wContractSpeed = Param_ReadValue_16Bit( enPARAM16__ContractSpeed );
   if( gstMotion.wSpeedCMD > wContractSpeed ) {
      gstMotion.wSpeedCMD = wContractSpeed;
   }
   else if( gstMotion.wSpeedCMD < -1*wContractSpeed ) {
      gstMotion.wSpeedCMD = -1*wContractSpeed;
   }
}
/*-----------------------------------------------------------------------------
Emergency brake drop control removed from stop sequence
 -----------------------------------------------------------------------------*/
static void EBrakeDropControl()
{
   uint8_t bEnableSecondaryBrake = Param_ReadValue_1Bit( enPARAM1__EnableSecondaryBrake );
   uint8_t bValidState = ( gpstMotionCtrl->eMotionState == MOTION__STOPPED )
                      || ( gpstMotionCtrl->eMotionState == MOTION__STOP_SEQUENCE );
   if( bEnableSecondaryBrake && bValidState )
   {
      StopSequence_DropEBrake( gpstMotionCtrl );
      StopSequence_DropB2( gpstMotionCtrl );
   }
}
/*-----------------------------------------------------------------------------
 Preflight complete check removed from the stop sequence
 -----------------------------------------------------------------------------*/
static void PreflightFlagDropControl(void)
{
   if( GetPreflightFlag() )
   {
      StopSequence_Preflight( gpstMotionCtrl );
   }
}
/*-----------------------------------------------------------------------------
Removed brake picking from start sequence to allow running under the brake
 -----------------------------------------------------------------------------*/
static void BrakePickControl()
{
   if( ( gpstMotionCtrl->eMotionState == MOTION__MANUAL_RUN ) ||
       ( gpstMotionCtrl->eMotionState == MOTION__ACCELERATING ) ||
       ( ( gpstMotionCtrl->eMotionState == MOTION__START_SEQUENCE ) &&
         ( gpstMotionCtrl->eStartSequence > SEQUENCE_START__PICK_B2 ) ) )
   {
      uint32_t uiPickBrakeDelay_ms = Param_ReadValue_16Bit( enPARAM16__BrakePickDelay_Auto_ms );
      if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
      {
         uiPickBrakeDelay_ms = Param_ReadValue_16Bit( enPARAM16__BrakePickDelay_Insp_ms );
      }
      if( gpstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__LIFT_BRAKE] >= uiPickBrakeDelay_ms/MOD_RUN_PERIOD_MOTION_1MS )
      {
         gpstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__LIFT_BRAKE] = 0;
         gpstMotionCtrl->stFlags.bPickBrake = 1;
         gpstMotionCtrl->stFlags.bPickEBrake = 1;
      }
      else
      {
         gpstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__LIFT_BRAKE]++;
      }
   }
}
/*-----------------------------------------------------------------------------
 Test if car can change destinations during a releveling run
 -----------------------------------------------------------------------------*/
static void CheckForMidflightDestChangeDuringRelevel(void)
{
#if 0
   static uint8_t bOncePerRun;
   if( GetMotion_RunFlag() )
   {
      if( ( ( gpstMotionCtrl->eMotionState == MOTION__CRUISING ) || ( gpstMotionCtrl->eMotionState == MOTION__ACCELERATING ) )
       && ( gpstMotionCtrl->bReleveling ) )
      {
         bOncePerRun = 0;
         SetDestination( ( GetOperation_CurrentFloor()+2 ) % GetFP_NumFloors() );
      }
   }
   else
   {
      bOncePerRun = 1;
   }
#endif
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   CheckForMidflightDestChangeDuringRelevel();

#if RTT_ENABLE_TEST_TRUNCATED_ACCEL_ONLY
   Test_TruncatedAccel();//TODO rm
#endif

   //------------------------------------------------
#if RTT_ENABLE_TEST_REDUCED_SPEED_LIMIT
   /* todo rm test*/
   static uint8_t bHold = 0;
   uint16_t ucSpeedLimit = Param_ReadValue_16Bit(enPARAM16__UNUSED_16BIT_1063);
   uint16_t ucTriggerSpeed = Param_ReadValue_16Bit(enPARAM16__UNUSED_16BIT_1062);
   uint16_t uwCurrentSpeed =gstMotion.wSpeedCMD*gstMotion.cDirection;
   if(uwCurrentSpeed >= ucTriggerSpeed)
   {
      bHold = 1;
   }
   else if(gpstMotionCtrl->eMotionState == MOTION__STOPPED)
   {
      bHold = 0;
   }

   if(bHold)
   {
      SetOperation_SpeedLimit(ucSpeedLimit);
   }
#endif
   //------------------------------------------------

   CheckIfCarIsStable();
   //------------------------------------------------
   UpdateMotionProfiles();
   //------------------------------------------------

   if( CheckIfEmergencyStop() )
   {
      EmergencyStop( gpstMotionCtrl );
      gstMotion.ulActualDest = GetPosition_PositionCount();
   }
   else if( CheckIfQuickStop() )
   {
      QuickStop();
   }
   else
   {
      switch ( gpstMotionCtrl->eMotionState )
      {
         case MOTION__STOPPED:
            Process_Stopped();
            break;

         case MOTION__MANUAL_RUN:
            Process_ManualRun();
            break;

         case MOTION__ACCELERATING:
            if( CheckIfReducedSpeedLimit(gpstMotionCtrl) )
            {
               SpeedLimitChangeDuringAccel(gpstMotionCtrl);
            }
            else if(CheckIfMidflightDestChange())
            {
               DestinationChangeDuringAccel();
            }
            else
            {
               Process_Accelerating();
            }
            break;

         case MOTION__CRUISING:
            if( CheckIfReducedSpeedLimit(gpstMotionCtrl) )
            {
               SpeedLimitChangeDuringCruising(gpstMotionCtrl);
            }
#if RTT_ENABLE_TEST_TRUNCATED_ACCEL_ONLY
            else if(0)
#else
            else if(CheckIfMidflightDestChange())
#endif
            {
               DestinationChangeDuringCruising();
            }
            else
            {
               Process_Cruising();
            }
            break;

         case MOTION__DECELERATING:
            Process_Decelerating();
            break;

         case MOTION__START_SEQUENCE:
            MotionState_StartSequence( gpstMotionCtrl );
            break;

         case MOTION__STOP_SEQUENCE:
            MotionState_StopSequence( gpstMotionCtrl );
            break;

         case MOTION__RSL_DECEL:
            MotionState_ReducedSpeedLimit( gpstMotionCtrl );
            break;

         case MOTION__REC_TRV_DIR:
            Process_RecTrvDir();
            break;

         default:
            EmergencyStop( gpstMotionCtrl );
            break;
      }
   }
   //------------------------------------------------
   EBrakeDropControl();
   //------------------------------------------------
   PreflightFlagDropControl();
   //------------------------------------------------
   BrakePickControl();
   //------------------------------------------------
   SpeedCommandDeviationControl();
   //------------------------------------------------
   CheckFor_InvalidCommand();
   //------------------------------------------------
   CheckFor_InvalidSCurveFault();
   //------------------------------------------------
   BoundCommandByContractSpeed();

   //------------------------------------------------
   CheckFor_OverspeedAcceptanceTest();

   //------------------------------------------------
   UpdateGlobalMotionStructure();
   //------------------------------------------------
   CheckFor_ResetMotionSequenceTimers();
   //------------------------------------------------
   // Preflight Test DIP
   if( Param_ReadValue_1Bit(enPARAM1__EnablePreflightTestDIP)
    && SRU_Read_DIP_Switch(enSRU_DIP_B7))
   {
      gpstMotionCtrl->stFlags.bPreflight |= 1;
      gpstMotionCtrl->stTimers.uiPreflightCounter_ms = 0;
      gpstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__PREFLIGHT] = 0;
   }
   //------------------------------------------------
   if( GetOperation_ManualMode() != MODE_M__CONSTRUCTION )
   {
      SRU_Write_ExtOutput(eExt_Output_MCUA_X, gpstMotionCtrl->stFlags.bPreflight);
   }
   else
   {
      SRU_Write_ExtOutput(eExt_Output_MCUA_X, 0);
   }
   //------------------------------------------------
   /* Update variable used for detecting jumps in speed command,
    * and inspection/leveling speed ramp down */
   gpstMotionCtrl->wLastSpeedCMD_FPM = gstMotion.wSpeedCMD;

   //------------------------------------------------
   RTTLog_MotionStart( gpstMotionCtrl, ( gpstCurveParameters + gstMotion.eProfile ));
   RTTLog_InMotion( gpstMotionCtrl );
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
