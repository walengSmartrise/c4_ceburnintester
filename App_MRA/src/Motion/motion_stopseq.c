/******************************************************************************
 *
 * @file     mod_motion.c
 * @brief
 * @version  V1.00
 * @date     23, March 2016
 *
 * @note
*
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "sru_a.h"
#include <stdlib.h>
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "acceptanceTest.h"
#include "acceptance.h"
#include "pattern.h"
#include "fpga_api.h"
#define ARM_MATH

#ifdef ARM_MATH
#include "arm_math.h"
#else
#include <math.h>
#endif

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_motion gstMotion;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------
   Returns 1 if preflight completion flag returned from MR CPLD, CT MCUA, and COP MCUA
--------------------------------------------------------------------------*/
static uint8_t GetPreflightCompleteFlag()
{
   uint8_t bReturn;
   uint8_t bMR = FPGA_GetPreflightStatus(FPGA_LOC__MRA) >= PREFLIGHT_STATUS__PASS;
   uint8_t bCT = GetPreflightState_CTA() >= PREFLIGHT_STATUS__PASS;
   uint8_t bCOP = GetPreflightState_COPA() >= PREFLIGHT_STATUS__PASS;
   if(bMR && bCT && bCOP)
   {
      bReturn = 1;
   }
   else
   {
      bReturn = 0;
   }

   return bReturn;
}
/*--------------------------------------------------------------------------
   Ramp down from leveling speed to zero.
--------------------------------------------------------------------------*/
static void StopSequence_RampToZero( Motion_Control *pstMotionCtrl )
{
   if( Param_ReadValue_1Bit(enPARAM1__StopSeq_DisableHoldZero)
    && Param_ReadValue_1Bit(enPARAM1__StopSeq_DisableRampZero) )
   {
      pstMotionCtrl->eStopSequence = SEQUENCE_STOP__DROP_BRAKE;
   }
   else if( ( Param_ReadValue_1Bit(enPARAM1__StopSeq_DisableRampZero) )
    || ( Rescue_GetManualTractionRescue() ) )
   {
      pstMotionCtrl->eStopSequence = SEQUENCE_STOP__HOLD_ZERO;
   }
   else if( pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__RAMP_TO_ZERO] >= TIMEOUT_RAMP_TO_ZERO_1MS )
   {
      SetFault(FLT__MOTION_RAMP_TO_ZERO);
      EmergencyStop( pstMotionCtrl );
   }
   else
   {
      float fDecel_fps2 = (float)Param_ReadValue_8Bit(enPARAM8__LevelingDecel_01fps)/10.0f;

      if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
      {
         fDecel_fps2 = (float)Param_ReadValue_8Bit(enPARAM8__P2_Decel_x10)/10.0f;
      }

      if(fDecel_fps2 > (MAX_ACCEL*2))
      {
         fDecel_fps2 = MAX_ACCEL*2;
      }
      else if(fDecel_fps2 < 2*MIN_ACCEL)
      {
         fDecel_fps2 = 2*MIN_ACCEL;
      }

      float fTime_sec = pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__RAMP_TO_ZERO]/1000.0f;
      int16_t wSpeedReduction = (fDecel_fps2 * fTime_sec * 60.0f ) + 0.5f;
      int16_t wNewSpeed = (pstMotionCtrl->wEndOfRunSpeed_FPM * gstMotion.cDirection) - wSpeedReduction;

      if(!pstMotionCtrl->wEndOfRunSpeed_FPM)
      {
         gstMotion.wSpeedCMD = 0;
         pstMotionCtrl->eStopSequence = SEQUENCE_STOP__HOLD_ZERO;
      }
      else if(wNewSpeed <= 1)
      {
         gstMotion.wSpeedCMD = 1 * gstMotion.cDirection;
         pstMotionCtrl->eStopSequence = SEQUENCE_STOP__HOLD_ZERO;
      }
      else
      {
         gstMotion.wSpeedCMD = wNewSpeed * gstMotion.cDirection;
         pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__RAMP_TO_ZERO] += MOD_RUN_PERIOD_MOTION_1MS;
      }
   }

}
/*--------------------------------------------------------------------------
   Wait for drive to ramp down to 1 fpm.
   Necessary for KEB internal ramp down curve when zero speed is commanded
--------------------------------------------------------------------------*/
static void StopSequence_HoldZero( Motion_Control *pstMotionCtrl )
{
   if(!pstMotionCtrl->wEndOfRunSpeed_FPM)
   {
      gstMotion.wSpeedCMD = 0;
   }
   else
   {
      gstMotion.wSpeedCMD = 1 * GetMotion_Direction();
   }

   if( ( Param_ReadValue_1Bit(enPARAM1__StopSeq_DisableHoldZero) )
    || ( Rescue_GetManualTractionRescue() ) )
   {
      pstMotionCtrl->eStopSequence = SEQUENCE_STOP__DROP_BRAKE;
   }
   else if( ( GetDriveSpeedFeedback() <= 1 )
         && ( GetDriveSpeedFeedback() >= -1 ) )
   {
      pstMotionCtrl->eStopSequence = SEQUENCE_STOP__DROP_BRAKE;
   }
   else if ( pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__HOLD_ZERO] > TIMEOUT_HOLD_ZERO_1MS/MOD_RUN_PERIOD_MOTION_1MS)
   {
      SetFault(FLT__MOTION_HOLD_ZERO);
      EmergencyStop( pstMotionCtrl );
   }
   else
   {
      pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__HOLD_ZERO]++;
   }
}
/*--------------------------------------------------------------------------

--------------------------------------------------------------------------*/
static void StopSequence_DropBrake( Motion_Control *pstMotionCtrl )
{
   uint32_t uiDropDelay_ms = 0;
   gstMotion.wSpeedCMD = 0;

   pstMotionCtrl->uiLastStopPos_05mm = GetPosition_PositionCount();

   if( GetOperation_ClassOfOp() == CLASSOP__AUTO )
   {
      uiDropDelay_ms = Param_ReadValue_16Bit(enPARAM16__BrakeDropDelay_Auto_1ms);
   }
   else //manual op
   {
      uiDropDelay_ms = Param_ReadValue_16Bit(enPARAM16__BrakeDropDelay_Insp_1ms);
   }
   if( uiDropDelay_ms > 3000 )
   {
      uiDropDelay_ms = 3000;
   }
   //-------------------------------------------------------
   if ( pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__DROP_BRAKE] >= uiDropDelay_ms/MOD_RUN_PERIOD_MOTION_1MS )
   {
      pstMotionCtrl->stFlags.bPickBrake = 0;
      pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__CHECK_BPS] = 0;
      pstMotionCtrl->eStopSequence = SEQUENCE_STOP__CHECK_BPS;
   }
   else
   {
      pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__DROP_BRAKE]++;
   }
}

/*--------------------------------------------------------------------------

--------------------------------------------------------------------------*/
static void StopSequence_Check_BPS( Motion_Control *pstMotionCtrl )
{
   pstMotionCtrl->stFlags.bPickBrake = 0;
   gstMotion.wSpeedCMD = 0;
   //-------------------------------------------------------
   if( Param_ReadValue_1Bit( enPARAM1__DisableBPS_StopSeq )
    || !Brake_GetBPS() )
   {
      pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__DELAY_DEENERGIZE] = 0;
      pstMotionCtrl->eStopSequence = SEQUENCE_STOP__DELAY_DEENERGIZE;
   }
   else if ( pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__CHECK_BPS] >= TIMEOUT_DROP_BRAKE_1MS/MOD_RUN_PERIOD_MOTION_1MS )
   {
      pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__CHECK_BPS] = 0;
      SetFault(FLT__MOTION_CHECK_BPS);
      EmergencyStop( pstMotionCtrl );
   }
   else
   {
      pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__CHECK_BPS]++;
   }
}

/*--------------------------------------------------------------------------

--------------------------------------------------------------------------*/
static void StopSequence_DelayDeenergize( Motion_Control *pstMotionCtrl )
{
   uint32_t uiDropDelay_ms = 0;
   pstMotionCtrl->stFlags.bPickBrake = 0;
   gstMotion.wSpeedCMD = 0;

   if( GetOperation_ClassOfOp() == CLASSOP__AUTO )
   {
      uiDropDelay_ms = Param_ReadValue_16Bit(enPARAM16__DriveDropDelay_Auto_1ms);
   }
   else //manual op
   {
      uiDropDelay_ms = Param_ReadValue_16Bit(enPARAM16__DriveDropDelay_Insp_1ms);
   }
   //-------------------------------------------------------
   if ( pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__DELAY_DEENERGIZE] >= uiDropDelay_ms/MOD_RUN_PERIOD_MOTION_1MS )
   {
      pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__DELAY_DROP_M] = 0;
      pstMotionCtrl->eStopSequence = SEQUENCE_STOP__DELAY_DROP_M;
   }
   else
   {
      pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__DELAY_DEENERGIZE]++;
   }
}
#if 0
/*--------------------------------------------------------------------------
todo obsolete state
--------------------------------------------------------------------------*/
static void StopSequence_MotorDeenergize( Motion_Control *pstMotionCtrl )
{
   uint32_t uiDropDelay_ms = 0;
   gstMotion.cDirection = 0;
   pstMotionCtrl->stFlags.bPickDrive = 0;
   pstMotionCtrl->stFlags.bPickBrake = 0;
   pstMotionCtrl->stFlags.bDriveHWEnable = 0;
   gstMotion.wSpeedCMD = 0;

   if( 1 )
   {
      pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__DELAY_DROP_M] = 0;
      pstMotionCtrl->eStopSequence = SEQUENCE_STOP__DELAY_DROP_M;
   }
   else if ( pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__MOTOR_DEENERGIZE] >= TIMEOUT_MOTOR_DEENERGIZE_1MS/MOD_RUN_PERIOD_MOTION_1MS )
   {
      pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__MOTOR_DEENERGIZE] = 0;
      SetFault(FLT__MOTION_DEENERGIZE);
      EmergencyStop( pstMotionCtrl );
   }
   else
   {
      pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__MOTOR_DEENERGIZE]++;
   }
}
#endif
/*--------------------------------------------------------------------------

--------------------------------------------------------------------------*/
static void StopSequence_DelayDropM( Motion_Control *pstMotionCtrl )
{
   uint32_t uiDropDelay_ms = 0;
   gstMotion.cDirection = 0;
   pstMotionCtrl->stFlags.bPickDrive = 0;
   pstMotionCtrl->stFlags.bPickBrake = 0;
   pstMotionCtrl->stFlags.bDriveHWEnable = 0;
   gstMotion.wSpeedCMD = 0;

   if( GetOperation_ClassOfOp() == CLASSOP__AUTO )
   {
      uiDropDelay_ms = Param_ReadValue_16Bit(enPARAM16__MotorDropDelay_Auto_1ms);
   }
   else //manual op
   {
      uiDropDelay_ms = Param_ReadValue_16Bit(enPARAM16__MotorDropDelay_Insp_1ms);
   }

   if( pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__DELAY_DROP_M] >= uiDropDelay_ms )
   {
      pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__DELAY_DROP_M] = 0;
      pstMotionCtrl->eStopSequence = SEQUENCE_STOP__DROP_M;
   }
   else
   {
      pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__DELAY_DROP_M] += MOD_RUN_PERIOD_MOTION_1MS;
   }
}

/*--------------------------------------------------------------------------
   @fn static uint8_t CheckIf_ValidPreflightMode( void )
   @return Returns 1 if preflight checks should be performed, 0 otherwise.
   @brief This function checks if preflight should be performed.
--------------------------------------------------------------------------*/
static uint8_t CheckIf_ValidPreflightMode(void)
{
   uint8_t bValid = 0;
   /* Matched with V2, perform preflight all automatic operation modes, but only if in DZ */
   if( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
    && ( getDoorZone(DOOR_ANY) )
    && ( GetOperation_AutoMode() != MODE_A__TEST ) )
   {
      bValid = 1;
   }
   return bValid;
}
/*--------------------------------------------------------------------------
Drop m contactor
--------------------------------------------------------------------------*/
static void StopSequence_DropM( Motion_Control *pstMotionCtrl )
{
   uint32_t uiDropDelay_ms = 0;
   pstMotionCtrl->stFlags.bPickM = 0;
   gstMotion.cDirection = 0;
   pstMotionCtrl->stFlags.bPickDrive = 0;
   pstMotionCtrl->stFlags.bPickBrake = 0;
   pstMotionCtrl->stFlags.bDriveHWEnable = 0;
   gstMotion.wSpeedCMD = 0;

   //----------------------------------------------------------
   uint8_t bBContFeedback = GetInputValue(enIN_MBC);
   if( ( CheckIfInputIsProgrammed(enIN_DSD_RunEngaged) )
    && ( gstDrive.eType == enDriveType__DSD ) )
   {
      bBContFeedback &= GetInputValue(enIN_DSD_RunEngaged);
   }
   uint8_t bSpeedReg = GetSpeedRegReleased();
   if( ( GetInputValue(enIN_MMC) && !(bBContFeedback && bSpeedReg) )
    || ( Rescue_GetManualTractionRescue() ) )
   {
      pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__PREFLIGHT] = 0;
      pstMotionCtrl->stTimers.uiPreflightCounter_ms = 0;
      pstMotionCtrl->eStopSequence = SEQUENCE_STOP__END;
      if(    ( CheckIf_ValidPreflightMode() )
          && ( !Param_ReadValue_1Bit(enPARAM1__DEBUG_DisablePreflight) )
          && ( !pstMotionCtrl->bReleveling )
        )
      {
         pstMotionCtrl->stFlags.bPreflight = 1;
      }
      else
      {
         pstMotionCtrl->stFlags.bPreflight = 0;
      }
   }
   else if( pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__DROP_M] >= TIMEOUT_DROP_M_1MS )
   {
      pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__DROP_M] = 0;
      SetFault(FLT__MOTION_DROP_M);
      EmergencyStop( pstMotionCtrl );
   }
   else
   {
      pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__DROP_M]+=MOD_RUN_PERIOD_MOTION_1MS;
   }
}
/*--------------------------------------------------------------------------
Skip if not in valid auto mode.
Sets motion fault if preflight complete confirmation not received from CPLD

--------------------------------------------------------------------------*/
void StopSequence_Preflight( Motion_Control *pstMotionCtrl )
{
   if( !Param_ReadValue_1Bit(enPARAM1__DEBUG_DisablePreflight) )
   {
      if(GetPreflightCompleteFlag())//preflight complete
      {
         uint16_t uwDebounceLimit_ms = DEBOUNCE_PREFLIGHT_COMPLETE_MS * MOD_RUN_PERIOD_MOTION_1MS;
         if( pstMotionCtrl->stTimers.uiPreflightCounter_ms >= uwDebounceLimit_ms )
         {
            pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__PREFLIGHT] = 0;
            pstMotionCtrl->stTimers.uiPreflightCounter_ms = 0;
            pstMotionCtrl->stFlags.bPreflight = 0;
         }
         else
         {
            pstMotionCtrl->stTimers.uiPreflightCounter_ms += MOD_RUN_PERIOD_MOTION_1MS;
         }
      }
      else//preflight incomplete
      {
         pstMotionCtrl->stTimers.uiPreflightCounter_ms = 0;
         if( pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__PREFLIGHT] >= TIMEOUT_PREFLIGHT_1MS/MOD_RUN_PERIOD_MOTION_1MS )
         {
            SetFault(FLT__MOTION_PREFLIGHT);
            pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__PREFLIGHT] = 0;
            pstMotionCtrl->stFlags.bPreflight = 0;
         }
         else
         {
            pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__PREFLIGHT]++;
         }
      }
   }
   else
   {
      pstMotionCtrl->stTimers.auiStopTimers[SEQUENCE_STOP__PREFLIGHT] = 0;
      pstMotionCtrl->stTimers.uiPreflightCounter_ms = 0;
      pstMotionCtrl->stFlags.bPreflight = 0;
   }
}
/*--------------------------------------------------------------------------

--------------------------------------------------------------------------*/
static void StopSequence_End( Motion_Control *pstMotionCtrl )
{
   gstMotion.bRunFlag = 0;
   gstMotion.wSpeedCMD = 0;
   gstMotion.cDirection = 0;
   pstMotionCtrl->wEndOfRunSpeed_FPM = 0;
   pstMotionCtrl->stFlags.bDriveHWEnable = 0;
   ClearAllMotionRunFlags();

   pstMotionCtrl->eStopSequence = SEQUENCE_STOP__RAMP_TO_ZERO;

   pstMotionCtrl->eMotionState = MOTION__STOPPED;
}
/*--------------------------------------------------------------------------
   Control dropping of emergency brake

   bPickEBrake flag will be dropped automatically in case of fault in
   void EmergencyStop( Motion_Control *pstMotionCtrl )
--------------------------------------------------------------------------*/
void StopSequence_DropEBrake( Motion_Control *pstMotionCtrl )
{
   static uint32_t uiEBrakeDropTimer_ms;
   uint32_t uiEBrakeDropDelay_ms = Param_ReadValue_16Bit( enPARAM16__EBrakeDropDelay_Auto_1ms );
   /* Tom recommended a minimum ebrake drop delay of 1 second.
    * This is because if the primary and secondary brake are
    * set to drop at the same time, a defective secondary
    * brake can be masked by the primary brake. */
   if( uiEBrakeDropDelay_ms < MIN_EBRAKE_DROP_TIME__MS )
   {
      uiEBrakeDropDelay_ms = MIN_EBRAKE_DROP_TIME__MS;
   }

   if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
   {
      uiEBrakeDropDelay_ms = Param_ReadValue_16Bit( enPARAM16__EBrakeDropDelay_Insp_1ms );
   }

   if( pstMotionCtrl->stFlags.bPickBrake )
   {
      uiEBrakeDropTimer_ms = 0;
   }
   else if( uiEBrakeDropTimer_ms >= uiEBrakeDropDelay_ms/MOD_RUN_PERIOD_MOTION_1MS )
   {
      uiEBrakeDropTimer_ms = 0;
      pstMotionCtrl->stFlags.bPickEBrake = 0;
   }
   else
   {
      uiEBrakeDropTimer_ms++;
   }
}
/*--------------------------------------------------------------------------
   Control dropping of B2 contactor

   bPickEBrake flag will be dropped automatically in case of fault in
   void EmergencyStop( Motion_Control *pstMotionCtrl )
--------------------------------------------------------------------------*/
void StopSequence_DropB2( Motion_Control *pstMotionCtrl )
{
   static uint32_t uiB2DropTimer_ms;
   uint32_t uiB2DropDelay_ms = Param_ReadValue_16Bit( enPARAM16__B2DropDelay_Auto_1ms );
   if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
   {
      uiB2DropDelay_ms = Param_ReadValue_16Bit( enPARAM16__B2DropDelay_Insp_1ms );
   }
   if( pstMotionCtrl->stFlags.bPickEBrake )
   {
      uiB2DropTimer_ms = 0;
   }
   else if( uiB2DropTimer_ms >= uiB2DropDelay_ms/MOD_RUN_PERIOD_MOTION_1MS )
   {
      uiB2DropTimer_ms = 0;
      pstMotionCtrl->stFlags.bPickB2 = 0;
   }
   else
   {
      uiB2DropTimer_ms++;
   }
}
/*--------------------------------------------------------------------------

   This is where pstMotionCtrl->stFlags.bPickM, pstMotionCtrl->stFlags.bPickDrive, and pstMotionCtrl->stFlags.bPickBrake are being cleared
   to drop the brake, M contactor, and the run command to the drive.

--------------------------------------------------------------------------*/
void MotionState_StopSequence( Motion_Control *pstMotionCtrl )
{
   switch ( pstMotionCtrl->eStopSequence )
   {
      case SEQUENCE_STOP__RAMP_TO_ZERO:
         StopSequence_RampToZero( pstMotionCtrl );
         break;

      case SEQUENCE_STOP__HOLD_ZERO:
         StopSequence_HoldZero( pstMotionCtrl );
         break;

      case SEQUENCE_STOP__DROP_BRAKE:
         StopSequence_DropBrake( pstMotionCtrl );
         break;

      case SEQUENCE_STOP__DELAY_DEENERGIZE:
         StopSequence_DelayDeenergize( pstMotionCtrl );
         break;

      case SEQUENCE_STOP__CHECK_BPS:
         StopSequence_Check_BPS( pstMotionCtrl );
         break;
#if 0
      case SEQUENCE_STOP__MOTOR_DEENERGIZE:
         StopSequence_MotorDeenergize( pstMotionCtrl );
         break;
#endif
      case SEQUENCE_STOP__DELAY_DROP_M:
         StopSequence_DelayDropM( pstMotionCtrl );
         break;

      case SEQUENCE_STOP__DROP_M:
         StopSequence_DropM( pstMotionCtrl );
         break;

      default:
      case SEQUENCE_STOP__END:
         StopSequence_End( pstMotionCtrl );
         break;
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
