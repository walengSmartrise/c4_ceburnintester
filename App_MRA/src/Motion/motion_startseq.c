/******************************************************************************
 *
 * @file     mod_motion.c
 * @brief
 * @version  V1.00
 * @date     23, March 2016
 *
 * @note
*
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "sru_a.h"
#include <stdlib.h>
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "acceptanceTest.h"
#include "acceptance.h"
#include "pattern.h"
#define ARM_MATH

#ifdef ARM_MATH
#include "arm_math.h"
#else
#include <math.h>
#endif

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static void CheckFor_Motion_DoorsOpen(enum en_motion_start_sequence eState);

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_motion gstMotion;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------
 Alternative motion direction assertion.
 Activated by enPARAM8__MotionDirStage_Plus1
--------------------------------------------------------------------------*/
static void EarlyUpdateMotionDirection( Motion_Control *pstMotionCtrl )
{
   uint8_t ucUpdateStage_Plus1 = Param_ReadValue_8Bit(enPARAM8__MotionDirStage_Plus1);
   if( ( ucUpdateStage_Plus1 )
    && ( ucUpdateStage_Plus1 <= NUM_MOTION_START_SEQUENCE ) )
   {
      enum en_motion_start_sequence eStage = ucUpdateStage_Plus1-1;
      if( pstMotionCtrl->eStartSequence == eStage )
      {
         if( CheckIf_ValidDestinationRun() )
         {
            if ( GetOperation_RequestedDestination() > GetPosition_PositionCount() )
            {
               gstMotion.cDirection = DIR__UP;
            }
            else if ( GetOperation_RequestedDestination() < GetPosition_PositionCount() )
            {
               gstMotion.cDirection = DIR__DN;
            }
         }
         else if( CheckIf_ValidManualRun() )
         {
            if( GetOperation_MotionCmd()== MOCMD__RUN_UP )
            {
               gstMotion.cDirection = DIR__UP;
            }
            else if( GetOperation_MotionCmd()== MOCMD__RUN_DN )
            {
               gstMotion.cDirection = DIR__DN;
            }
         }
      }
   }
}
/*--------------------------------------------------------------------------

--------------------------------------------------------------------------*/
static uint8_t Start_CheckValidOperAuto( Motion_Control *pstMotionCtrl )
{
   uint8_t bReturn = 0;
   enum en_classop eClassOfOp = GetOperation_ClassOfOp();
   if ( ( eClassOfOp == CLASSOP__AUTO )
     && ( ( pstMotionCtrl->bReleveling ) ||
          ( DoorsSafeAndReadyToRun() ) ) )
   {
      bReturn = 1;
   }

   return bReturn;
}
static uint8_t Start_CheckValidOperManual()
{
   uint8_t bReturn = 0;
   enum en_mode_manual eManualMode = GetOperation_ManualMode();
   if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
   {
      if( AllHallDoorsClosed() && CarDoorsClosed() )
      {
         bReturn = 1;
      }
      else if( ( eManualMode == MODE_M__INSP_HA_TOP )
            || ( eManualMode == MODE_M__INSP_HA_BOTTOM ) )
      {
         if( GetCarDoorSafeHoistwayAccess()
          && GetHallDoorSafeHoistwayAccess() )
         {
            bReturn = 1;
         }
      }
      else if( eManualMode == MODE_M__CONSTRUCTION )
      {
         bReturn = 1;
      }
   }
   return bReturn;
}
static uint8_t Start_CheckValidOperLearn()
{
   uint8_t bReturn = 0;
   if ( ( AllHallDoorsClosed() && CarDoorsClosed() )
     && ( GetOperation_ClassOfOp() == CLASSOP__SEMI_AUTO )
     && ( ( GetOperation_LearnMode() == MODE_L__LEARNING_UHA )
       || ( GetOperation_LearnMode() == MODE_L__LEARNING_DHA ) )
      )
   {
      bReturn = 1;
   }
   return bReturn;
}
/*--------------------------------------------------------------------------

--------------------------------------------------------------------------*/

static void StartSequence_PrepareToRun( Motion_Control *pstMotionCtrl )
{
   gstMotion.wSpeedCMD = 0;
   gstMotion.bRunFlag = 1;
   pstMotionCtrl->bResetFieldTimer = 1;

   uint32_t uiPrepareToRunTimeout_1ms = TIMEOUT_PREPARE_TO_RUN_1MS;
   uint8_t bManualSwingDoors_F = Doors_CheckIfManualSwingDoor(DOOR_FRONT, INVALID_FLOOR);
   uint8_t bManualSwingDoors_R = Doors_CheckIfManualSwingDoor(DOOR_REAR, INVALID_FLOOR);

   if( bManualSwingDoors_F || bManualSwingDoors_R )
   {
      uiPrepareToRunTimeout_1ms = Param_ReadValue_8Bit(enPARAM8__TimeoutLockAndCAM_100ms) * 100;
      if(!uiPrepareToRunTimeout_1ms)
      {
         uiPrepareToRunTimeout_1ms = DEFAULT_MANUAL_DOORS_PREPARE_TO_RUN_TIMEOUT_1MS;
      }
   }
   if((Start_CheckValidOperManual() || Start_CheckValidOperAuto(pstMotionCtrl) || Start_CheckValidOperLearn() ) )
   {
      uint8_t bDriveHardwareEnable = ( gstDrive.eType == enDriveType__DSD ) ? 1:0;
      pstMotionCtrl->eStartSequence = ( bDriveHardwareEnable) ? SEQUENCE_START__DSD_PRETORQUE:SEQUENCE_START__PICK_M;
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__DSD_PRETORQUE] = 0;
   }
   else if(pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__PREPARE_TO_RUN] >= uiPrepareToRunTimeout_1ms/MOD_RUN_PERIOD_MOTION_1MS)
   {
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__PREPARE_TO_RUN] = 0;
      CheckFor_Motion_DoorsOpen(SEQUENCE_START__PREPARE_TO_RUN);
      EmergencyStop( pstMotionCtrl );
   }
   else
   {
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__PREPARE_TO_RUN]++;
   }
}

/*--------------------------------------------------------------------------
Sends drive hardware enable output
&
Checks for LPR output from drive
--------------------------------------------------------------------------*/
static void StartSequence_DriveHWEnable( Motion_Control *pstMotionCtrl )
{
   pstMotionCtrl->stFlags.bDriveHWEnable = 1;
   gstMotion.bRunFlag = 1;
   gstMotion.wSpeedCMD = 0;

   /* This is a stage that is no longer necessary */
   if(1)
   {
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__DRIVE_HW_ENABLE] = 0;
      pstMotionCtrl->eStartSequence = SEQUENCE_START__PICK_M;
   }
   else if(pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__DRIVE_HW_ENABLE] >= TIMEOUT_DRIVE_HW_ENABLE_1MS/MOD_RUN_PERIOD_MOTION_1MS)
   {
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__DRIVE_HW_ENABLE] = 0;
      SetFault(FLT__MOTION_DRIVE_ENABLE);
      EmergencyStop( pstMotionCtrl );
   }
   else
   {
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__DRIVE_HW_ENABLE]++;
   }
}

/*--------------------------------------------------------------------------
   Pick M Contactor, switching on serial/discrete Drive Enable.
   Check feedback to proceed to next stage
--------------------------------------------------------------------------*/
static void StartSequence_PickM( Motion_Control *pstMotionCtrl )
{
   pstMotionCtrl->stFlags.bPickM = 1;
   pstMotionCtrl->stFlags.bDriveHWEnable = 1;
   gstMotion.bRunFlag = 1;
   gstMotion.wSpeedCMD = 0;
   if(!GetInputValue(enIN_MMC) || Rescue_GetManualTractionRescue())
   {
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__PICK_M] = 0;
      pstMotionCtrl->eStartSequence = SEQUENCE_START__MOTOR_ENERGIZE;
   }
   else if(pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__PICK_M] >= TIMEOUT_PICK_M_1MS/MOD_RUN_PERIOD_MOTION_1MS)
   {
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__PICK_M] = 0;
      SetFault(FLT__MOTION_PICK_M);
      EmergencyStop( pstMotionCtrl );
   }
   else
   {
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__PICK_M]++;
   }
}
/*--------------------------------------------------------------------------

--------------------------------------------------------------------------*/
static void StartSequence_MotorEnergize( Motion_Control *pstMotionCtrl )
{
   pstMotionCtrl->stFlags.bPickDrive = 1;
   pstMotionCtrl->stFlags.bPickM = 1;
   pstMotionCtrl->stFlags.bDriveHWEnable = 1;
   gstMotion.bRunFlag = 1;
   gstMotion.wSpeedCMD = 0;
   uint8_t bBContFeedback = GetInputValue(enIN_MBC);
   if( ( CheckIfInputIsProgrammed(enIN_DSD_RunEngaged) )
    && ( gstDrive.eType == enDriveType__DSD ) )
   {
      bBContFeedback &= GetInputValue(enIN_DSD_RunEngaged);
   }
   uint8_t bSpeedReg = GetSpeedRegReleased();
   if( ( bSpeedReg && bBContFeedback )
    || ( Rescue_GetManualTractionRescue() ) )
   {
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__MOTOR_ENERGIZE] = 0;
      pstMotionCtrl->eStartSequence = SEQUENCE_START__PICK_B2;
   }
   else if ( pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__MOTOR_ENERGIZE] >= TIMEOUT_SPEED_REG_RLS_1MS/MOD_RUN_PERIOD_MOTION_1MS )
   {
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__MOTOR_ENERGIZE] = 0;
      if( !bSpeedReg )
      {
         SetFault(FLT__MOTION_SPEED_REG);
      }
      else
      {
         SetFault(FLT__MOTION_PICK_B1);
      }
      EmergencyStop( pstMotionCtrl );
   }
   else
   {
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__MOTOR_ENERGIZE]++;
   }
}
/*--------------------------------------------------------------------------

--------------------------------------------------------------------------*/
static void StartSequence_PickB2( Motion_Control *pstMotionCtrl )
{
   uint8_t bEBrakeEnabled = Param_ReadValue_1Bit(enPARAM1__EnableSecondaryBrake);
   pstMotionCtrl->stFlags.bPickB2 = 1;
   pstMotionCtrl->stFlags.bPickDrive = 1;
   pstMotionCtrl->stFlags.bPickM = 1;
   pstMotionCtrl->stFlags.bDriveHWEnable = 1;
   gstMotion.bRunFlag = 1;
   gstMotion.wSpeedCMD = 0;
   if( GetInputValue(enIN_MBC2) || !bEBrakeEnabled )
   {
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__PICK_B2] = 0;
      pstMotionCtrl->eStartSequence = SEQUENCE_START__ACCEL_DELAY;
   }
   else if( pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__PICK_B2] >= TIMEOUT_PICK_B2_1MS/MOD_RUN_PERIOD_MOTION_1MS )
   {
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__PICK_B2] = 0;
      SetFault(FLT__MOTION_PICK_B2);
      EmergencyStop( pstMotionCtrl );
   }
   else
   {
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__PICK_B2]++;
   }
}

/*--------------------------------------------------------------------------
  All manual modes run profile 2
  E-Power mode runs profile 3
  For all other modes, if pattern type is very short, use profile 4
  Otherwise, use profile 1
--------------------------------------------------------------------------*/
static void SelectMotionProfile()
{
   // For now, all auto modes run profile 1, all manual modes run profile 2.
   if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
   {
      gstMotion.eProfile = MOTION_PROFILE__2;
   }
   else if( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
         && ( ( GetOperation_AutoMode() == MODE_A__EPOWER )
           || ( GetOperation_AutoMode() == MODE_A__BATT_RESQ )
           || ( EPower_GetCarRecalledFlag() )
           || ( GetEmergencyPowerCommand() != EP_OFF ) ) )
   {
      gstMotion.eProfile = MOTION_PROFILE__3;
   }
   else
   {
      /* If run is a very short pattern, run profile 4, otherwise run profile 1 */
      gstMotion.eProfile = MOTION_PROFILE__1;
      int16_t wOffset_05mm = ( GetOperation_RequestedDestination() >= GetPosition_PositionCount() )
                           ? ( -1*Param_ReadValue_8Bit(enPARAM8__DestOffsetUp_05mm) )
                           : ( Param_ReadValue_8Bit(enPARAM8__DestOffsetDown_05mm) );
      if( gpstMotionCtrl->bReleveling )
      {
         wOffset_05mm = ( GetOperation_RequestedDestination() >= GetPosition_PositionCount() )
                      ? ( -1*Param_ReadValue_8Bit(enPARAM8__RelevelOffsetUp_05mm) )
                      : ( Param_ReadValue_8Bit(enPARAM8__RelevelOffsetDown_05mm) );
      }
      uint32_t uiOffsetDestination_05mm = GetOperation_RequestedDestination() + wOffset_05mm;
      uint32_t uiRunDistance = GetPosition_PositionCount() - uiOffsetDestination_05mm;
      uint32_t uiMinShortRunDistance = 0; // Don't run profile 4 unless the enPARAM8__ShortProfileMinDist_ft is set.
      /* Allow fixed minimum distance for short run profile */
      if( Param_ReadValue_8Bit(enPARAM8__ShortProfileMinDist_ft) )
      {
         uiMinShortRunDistance = Param_ReadValue_8Bit(enPARAM8__ShortProfileMinDist_ft) * ONE_FEET;
      }

      if ( GetOperation_RequestedDestination() > GetPosition_PositionCount() )
      {
         uiRunDistance = uiOffsetDestination_05mm - GetPosition_PositionCount();
      }
      if( uiRunDistance < uiMinShortRunDistance )
      {
         gstMotion.eProfile = MOTION_PROFILE__4;
      }
   }

   SetMotion_Profile( gstMotion.eProfile );
}
/*--------------------------------------------------------------------------

--------------------------------------------------------------------------*/
static void StartSequence_AccelDelay( Motion_Control *pstMotionCtrl )
{
   pstMotionCtrl->stFlags.bPickB2 = 1;
   pstMotionCtrl->stFlags.bPickDrive = 1;
   pstMotionCtrl->stFlags.bPickM = 1;
   pstMotionCtrl->stFlags.bDriveHWEnable = 1;
   gstMotion.bRunFlag = 1;
   gstMotion.wSpeedCMD = 0;
   uint32_t uiAccelDelay = Param_ReadValue_16Bit( enPARAM16__AccelDelay_ms );
   if( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
   {
      uiAccelDelay = Param_ReadValue_16Bit( enPARAM16__AccelDelay_Insp_ms );
   }
   else if( GetOperation_AutoMode() == MODE_A__BATT_RESQ )
   {
      uiAccelDelay = Param_ReadValue_8Bit( enPARAM8__AccelDelay_Rescue_100ms ) * 100;
   }
   if ( pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__ACCEL_DELAY] >= uiAccelDelay/MOD_RUN_PERIOD_MOTION_1MS )
   {
      pstMotionCtrl->eStartSequence = SEQUENCE_START__PREPARE_TO_RUN;
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__ACCEL_DELAY] = 0;
      SelectMotionProfile();
      if(Rescue_GetRecTrvDirCommand_HPV() )
      {
         pstMotionCtrl->bRequestRejected = 0;
         pstMotionCtrl->eMotionState = MOTION__REC_TRV_DIR;
         pstMotionCtrl->stTimers.uiAccelDelayTimeout_ms = 0;
      }
      else if(( Start_CheckValidOperAuto(pstMotionCtrl) || Start_CheckValidOperLearn() )
            && CheckIf_ValidDestinationRun()
            && GenerateRun_Auto() )
      {
         pstMotionCtrl->bRequestRejected = 0;
         pstMotionCtrl->eMotionState = MOTION__ACCELERATING;
         pstMotionCtrl->stTimers.uiAccelDelayTimeout_ms = 0;
      }
      else if (CheckIf_ValidManualRun()
             && GenerateRun_Manual() )
      {
         pstMotionCtrl->eMotionState = MOTION__MANUAL_RUN;
         pstMotionCtrl->stTimers.uiAccelDelayTimeout_ms = 0;
      }
      else if(pstMotionCtrl->stTimers.uiAccelDelayTimeout_ms < TIMEOUT_ACCEL_DELAY_1MS )
      {
         pstMotionCtrl->stTimers.uiAccelDelayTimeout_ms += MOD_RUN_PERIOD_MOTION_1MS;
      }
      else
      {
         CheckFor_Motion_DoorsOpen(SEQUENCE_START__ACCEL_DELAY);
         EmergencyStop( pstMotionCtrl );
         pstMotionCtrl->stTimers.uiAccelDelayTimeout_ms = 0;
      }
   }
   else
   {
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__ACCEL_DELAY]++;
   }
}
/*--------------------------------------------------------------------------
   For DSD drive, asserts the pretorque signal prior to any run signals for at least 200ms as
   recommended by their in house timing diagram.
--------------------------------------------------------------------------*/
static void StartSequence_DSD_Pretorque( Motion_Control *pstMotionCtrl )
{
   gstMotion.bRunFlag = 1;
   gstMotion.wSpeedCMD = 0;
   pstMotionCtrl->stFlags.bDriveHWEnable = 1;
   uint16_t uwDelayLimit_5ms = ( Param_ReadValue_8Bit(enPARAM8__DSD_PretorqueDelay_50ms) )
                             ? ( Param_ReadValue_8Bit(enPARAM8__DSD_PretorqueDelay_50ms) * 10 )
                             : ( START_SEQ_DSD_PRETORQUE_ASSERTION_TIME_MS/MOD_RUN_PERIOD_MOTION_1MS );
   if( pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__DSD_PRETORQUE] >= uwDelayLimit_5ms )
   {
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__DSD_PRETORQUE] = 0;
      pstMotionCtrl->eStartSequence = SEQUENCE_START__DRIVE_HW_ENABLE;
   }
   else
   {
      pstMotionCtrl->stTimers.auiStartTimers[SEQUENCE_START__DSD_PRETORQUE]++;
   }
}
/*--------------------------------------------------------------------------

   This is where pstMotionCtrl->stFlags.bPickM, pstMotionCtrl->stFlags.bPickDrive, and pstMotionCtrl->stFlags.bPickBrake are being set
   to pick the brake, M contactor, and command zero speed to the drive.

--------------------------------------------------------------------------*/
void MotionState_StartSequence( Motion_Control *pstMotionCtrl )
{
   EarlyUpdateMotionDirection(pstMotionCtrl);

   switch ( pstMotionCtrl->eStartSequence )
   {
      case SEQUENCE_START__PREPARE_TO_RUN:
         StartSequence_PrepareToRun( pstMotionCtrl );
         break;

      case SEQUENCE_START__DSD_PRETORQUE:
         StartSequence_DSD_Pretorque( pstMotionCtrl );
         break;

      case SEQUENCE_START__DRIVE_HW_ENABLE:
         StartSequence_DriveHWEnable( pstMotionCtrl );
         break;

      case SEQUENCE_START__PICK_M:
         StartSequence_PickM( pstMotionCtrl );
         break;

      case SEQUENCE_START__MOTOR_ENERGIZE:
         StartSequence_MotorEnergize( pstMotionCtrl );
         break;

      case SEQUENCE_START__PICK_B2:
         StartSequence_PickB2( pstMotionCtrl );
         break;

      case SEQUENCE_START__ACCEL_DELAY:
         StartSequence_AccelDelay( pstMotionCtrl );
         break;

      default:
         StartSequence_PrepareToRun( pstMotionCtrl );
         break;
   }
}


/*--------------------------------------------------------------------------

   Function that checks which door signal is not made up during the start sequence.
   Specifies which door signal is open during the checks.

--------------------------------------------------------------------------*/
static void CheckFor_Motion_DoorsOpen(enum en_motion_start_sequence eState){

   uint8_t bAnyGateswitchOpen, bAnyLockOpen, bAnyDPMOpen;

   bAnyGateswitchOpen = !GetInputValue( enIN_GSWF );

   bAnyLockOpen = !GetInputValue( enIN_LTF ) || !GetInputValue( enIN_LMF ) || !GetInputValue( enIN_LBF );

   bAnyDPMOpen = !GetInputValue(enIN_DPM_F) && CheckIfInputIsProgrammed(enIN_DPM_F) && ( GetFP_DoorType(DOOR_FRONT) != DOOR_TYPE__MANUAL );

   if ( GetFP_RearDoors() )
   {
       bAnyGateswitchOpen |= !GetInputValue( enIN_GSWR );

       bAnyLockOpen |= !GetInputValue( enIN_LTR ) || !GetInputValue( enIN_LMR ) || !GetInputValue( enIN_LBR );

       bAnyDPMOpen |= !GetInputValue(enIN_DPM_R) && CheckIfInputIsProgrammed(enIN_DPM_R) && ( GetFP_DoorType(DOOR_REAR) != DOOR_TYPE__MANUAL );
   }

   if ( bAnyGateswitchOpen || bAnyLockOpen || bAnyDPMOpen )
   {
     if(eState == SEQUENCE_START__PREPARE_TO_RUN)
     {
        if(!GetInputValue( enIN_GSWF ))
        {
           SetFault(FLT__MOTION_PREPARE_GSWF);
        }
        else if(!GetInputValue( enIN_LTF ))
        {
           SetFault(FLT__MOTION_PREPARE_LFT);
        }
        else if(!GetInputValue( enIN_LMF ))
        {
           SetFault(FLT__MOTION_PREPARE_LFM);
        }
        else if(!GetInputValue( enIN_LBF ))
        {
           SetFault(FLT__MOTION_PREPARE_LFB);
        }
        else if(!GetInputValue( enIN_GSWR ))
        {
           SetFault(FLT__MOTION_PREPARE_GSWR);
        }
        else if(!GetInputValue( enIN_LTR ))
        {
           SetFault(FLT__MOTION_PREPARE_LRT);
        }
        else if(!GetInputValue( enIN_LMR ))
        {
           SetFault(FLT__MOTION_PREPARE_LRM);
        }
        else if(!GetInputValue( enIN_LBR ))
        {
           SetFault(FLT__MOTION_PREPARE_LRB);
        }
        else if(!GetInputValue( enIN_DPM_F ))
         {
            SetFault(FLT__MOTION_PREPARE_DPM_F);
         }
         else if(!GetInputValue( enIN_DPM_R ))
         {
            SetFault(FLT__MOTION_PREPARE_DPM_R);
         }
         else if(!GetInputValue( enIN_DCL_F ))
         {
            SetFault(FLT__MOTION_PREPARE_DCL_F);
         }
         else if(!GetInputValue( enIN_DCL_R ))
         {
            SetFault(FLT__MOTION_PREPARE_DCL_R);
         }
         else if(GetInputValue( enIN_DOL_F ))
         {
            SetFault(FLT__MOTION_PREPARE_DOL_F);
         }
         else if(GetInputValue( enIN_DOL_R ))
         {
            SetFault(FLT__MOTION_PREPARE_DOL_R);
         }
        else
        {
           SetFault(FLT__MOTION_PREPARE_RUN);
        }
      }
      else if (eState == SEQUENCE_START__ACCEL_DELAY)
      {
        if(!GetInputValue( enIN_GSWF ))
        {
           SetFault(FLT__MOTION_ACCEL_GSWF);
        }
        else if(!GetInputValue( enIN_LTF ))
        {
           SetFault(FLT__MOTION_ACCEL_LFT);
        }
        else if(!GetInputValue( enIN_LMF ))
        {
           SetFault(FLT__MOTION_ACCEL_LFM);
        }
        else if(!GetInputValue( enIN_LBF ))
        {
           SetFault(FLT__MOTION_ACCEL_LFB);
        }
        else if(!GetInputValue( enIN_GSWR ))
        {
           SetFault(FLT__MOTION_ACCEL_GSWR);
        }
        else if(!GetInputValue( enIN_LTR ))
        {
           SetFault(FLT__MOTION_ACCEL_LRT);
        }
        else if(!GetInputValue( enIN_LMR ))
        {
           SetFault(FLT__MOTION_ACCEL_LRM);
        }
        else if(!GetInputValue( enIN_LBR ))
        {
           SetFault(FLT__MOTION_ACCEL_LRB);
        }
        else if(!GetInputValue( enIN_DPM_F ))
        {
           SetFault(FLT__MOTION_ACCEL_DPM_F);
        }
        else if(!GetInputValue( enIN_DPM_R ))
        {
           SetFault(FLT__MOTION_ACCEL_DPM_R);
        }
        else if(!GetInputValue( enIN_DCL_F ))
        {
           SetFault(FLT__MOTION_ACCEL_DCL_F);
        }
        else if(!GetInputValue( enIN_DCL_R ))
        {
           SetFault(FLT__MOTION_ACCEL_DCL_R);
        }
        else if(GetInputValue( enIN_DOL_F ))
        {
           SetFault(FLT__MOTION_ACCEL_DOL_F);
        }
        else if(GetInputValue( enIN_DOL_R ))
        {
           SetFault(FLT__MOTION_ACCEL_DOL_R);
        }
        else
        {
          SetFault(FLT__MOTION_ACCEL_DELAY);
        }
      }
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
