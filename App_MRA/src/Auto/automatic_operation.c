/******************************************************************************
 *
 * @file     automatic_operation.c
 * @brief    Logic to control standard automatic operation (normal/Ind Srv.
 * @version  V1.00
 * @date     21, July 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
#include "remoteCommands.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static uint8_t CheckIf_MoveIdleCarTimeout(uint8_t *pucDestination);

static void AUTO_Unknown( void );
static void AUTO_Stopped( void );
static void AUTO_In_Motion( void );
static void AUTO_Correction( void );
static void AUTO_Idle( void );
static void SetParkingLamp(void);

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

#define IDLE_SCALE_TO_1SEC                            (20) //20 == 1 second
#define RECENT_DOOR_OPEN_REQ_UPPER_BOUND_50MS         (40)
#define RECENT_DOOR_OPEN_REQ_TIMEOUT_50MS             (6)

#define AUTO_STATE_MOVEMENT_TIMEOUT_50MS              (1200)
#define RECENT_DESTINATION_REQUEST_COUNTDOWN_50MS     (40)

#define MAX_EQ_SPEED__FPM           (75)
#define MAX_EQ_HOISTWAY_SCAN__FPM 150
#define MIN_EQ_HOISTWAY_SCAN__FPM 10
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t bInDestinationDoorzone = 0;
static uint8_t bParkingLamp;
static uint8_t bCloseDoorsAtStartup = 1;
static uint8_t ucDoorCheckDelay_50ms;

static uint16_t uwIdleTime_50ms;
static uint8_t ucRecentDoorOpenRequest_50ms;

static enum en_automatic_state geAutomaticState;
static enum en_automatic_state geLastAutomaticState;

static uint8_t bParkingWithDoorsOpen = 0;

static uint8_t bIndSrvDoor_F; // Tracks if the front door should be opened on independent service
static uint8_t bIndSrvDoor_R; // Tracks if the rear door should be opened on independent service

static uint8_t abFF2_MomentaryDCB[NUM_OF_DOORS]; // Tracks if there is a momentary DCB command
/* Prevents state machne from bouncing between AUTO_IDLE and AUTO_MOVING when preparing to start a run */
static uint8_t ucRecentDestinationRequestCountdown_50ms;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 Flag used to signal if doors safe to open when in automatic operation
 *----------------------------------------------------------------------------*/
uint8_t InDestinationDoorzone ( void )
{
   return bInDestinationDoorzone;
}
void SetInDestinationDoorzone ( uint8_t bDestZone )
{
   bInDestinationDoorzone = bDestZone;
}
/*----------------------------------------------------------------------------
 Get FF2 momentary DCB command
 *----------------------------------------------------------------------------*/
uint8_t GetFF2_MomentaryCloseButton(enum en_doors enDoorIndex)
{
   uint8_t bMomentaryDCB = 0;
   uint8_t Fire2MomentaryDCB = ( Fire_CheckIfFire2DoorsEnabled() )
                              && Param_ReadValue_1Bit(enPARAM1__Fire__MomentaryDoorCloseButton);
   if( Fire2MomentaryDCB )
   {
      bMomentaryDCB = abFF2_MomentaryDCB[enDoorIndex] && !GetDoorOpenButton(enDoorIndex);
   }
   return bMomentaryDCB;
}

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void Auto_Set_Limits ( void )
{
   uint32_t uiUpLimit = 0;
   uint32_t uiDownLimit = CEDES_INVALID_POSITION;
   uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
   /*
   * Setting the top and bottom limits, if the car is faulted, or the doors
   * are not closed, then the car limits its available distance to 2 inches
   * above or below the current floors position.
   */
   if ( (gstFault.bActiveFault || bInDestinationDoorzone ) && (GetOperation_AutoMode() != MODE_A__TEST))
   {
      if( ucCurrentFloor < GetFP_NumFloors() )
      {
         uiDownLimit = ( gpastFloors[ ucCurrentFloor ].ulPosition >= (ONE_INCH*2) )
                     ? ( gpastFloors[ ucCurrentFloor ].ulPosition - (ONE_INCH*2) ) : 0;
         uiUpLimit = gpastFloors[ ucCurrentFloor ].ulPosition + (ONE_INCH*2);
      }

       SetOperation_PositionLimit_DN(uiDownLimit);
       SetOperation_PositionLimit_UP(uiUpLimit);
   }
   else
   {
      uiDownLimit = ( gpastFloors[ 0 ].ulPosition >= ONE_INCH )
                  ? ( gpastFloors[ 0 ].ulPosition - ONE_INCH ) : 0;
      uiUpLimit = gpastFloors[ GetFP_NumFloors() - 1 ].ulPosition + ONE_INCH;
       SetOperation_PositionLimit_DN( uiDownLimit );
       SetOperation_PositionLimit_UP( uiUpLimit );
   }
}

void Auto_Set_Limits_Flood ( void )
{
   uint32_t uiUpLimit = 0;
   uint32_t uiDownLimit = CEDES_INVALID_POSITION;
   uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
   uint8_t uFloodFloor = Param_ReadValue_8Bit(enPARAM8__FLOOD_NumFloors_Plus1);

   /*
   * Setting the top and bottom limits, if the car is faulted, or the doors
   * are not closed, then the car limits its available distance to 2 inches
   * above or below the current floors position.
   */
   if ( (gstFault.bActiveFault || bInDestinationDoorzone ) && (GetOperation_AutoMode() != MODE_A__TEST))
   {
      if( ucCurrentFloor < GetFP_NumFloors() )
      {
         uiDownLimit = ( gpastFloors[ uFloodFloor ].ulPosition >= (ONE_INCH*2) )
                     ? ( gpastFloors[ uFloodFloor ].ulPosition - (ONE_INCH*2) ) : 0;
         uiUpLimit = gpastFloors[ ucCurrentFloor ].ulPosition + (ONE_INCH*2);
      }

       SetOperation_PositionLimit_DN(uiDownLimit);
       SetOperation_PositionLimit_UP(uiUpLimit);
   }
   else
   {
      uiDownLimit = ( gpastFloors[ uFloodFloor  ].ulPosition >= ONE_INCH )
                  ? ( gpastFloors[ uFloodFloor  ].ulPosition - ONE_INCH ) : 0;
      uiUpLimit = gpastFloors[ GetFP_NumFloors() - 1 ].ulPosition + ONE_INCH;
       SetOperation_PositionLimit_DN( uiDownLimit );
       SetOperation_PositionLimit_UP( uiUpLimit );
   }
}


void Auto_SetSpeed ( uint16_t uwSpeed ) {
   if (uwSpeed <= GetFP_ContractSpeed() ) {
      SetOperation_SpeedLimit(uwSpeed);
   }
}

/*-----------------------------------------------------------------------------
   Called from Oper_Auto
 -----------------------------------------------------------------------------*/
void Auto_Oper( uint8_t bInitMode )
{
   /* Modify speed limit if running on generator power */
   if( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
    && ( ( GetOperation_AutoMode() == MODE_A__EPOWER )
      || ( EPower_GetCarRecalledFlag() ) ) )
   {
      uint16_t uwSpeedLimit = Param_ReadValue_16Bit(enPARAM16__EPowerSpeed_fpm);
      if( uwSpeedLimit < MIN_EPOWER_SPEED_LIMIT_FPM )
      {
         uwSpeedLimit = MIN_EPOWER_SPEED_LIMIT_FPM;
      }
      Auto_SetSpeed(uwSpeedLimit);
   }
   else if(GetOperation_AutoMode() == MODE_A__SEISMC || GetOperation_AutoMode() == MODE_A__CW_DRAIL )
   {
      uint8_t ucSpeedLimit = Param_ReadValue_8Bit(enPARAM8__EQ_Hoistway_Scan_Speed);
      if( ucSpeedLimit < MIN_EQ_HOISTWAY_SCAN__FPM )
      {
         ucSpeedLimit = MIN_EQ_HOISTWAY_SCAN__FPM;
      }
      else if( ucSpeedLimit > MAX_EQ_HOISTWAY_SCAN__FPM )
      {
          ucSpeedLimit = MAX_EQ_HOISTWAY_SCAN__FPM;
      }
      Auto_SetSpeed(ucSpeedLimit);
   }
   else
   {
      Auto_SetSpeed(GetFP_ContractSpeed());
   }

   bParkingWithDoorsOpen = 0;
   if (bInitMode)
   {
      geAutomaticState = AUTO_UNKNOWN;
      geLastAutomaticState = AUTO_UNKNOWN;
      if( !Fire_CheckIfFire2DoorsEnabled() ) {
        ResetDoors();
      }
      ucRecentDestinationRequestCountdown_50ms = 0;
   }

   if( ucRecentDestinationRequestCountdown_50ms )
   {
      ucRecentDestinationRequestCountdown_50ms--;
   }

   if ( gstFault.bActiveFault )
   {
      SetOperation_MotionCmd(MOCMD__EMERG_STOP);
      if( geAutomaticState == AUTO_MOVING )
      {
         geAutomaticState = AUTO_CORRECTION;
      }
   }
   if( ucbFlag_Auto_Stop && ( bMarshalModeStatus() || GetInputValue( enIN_MARSHAL_MODE ) ) )
   {
      geAutomaticState = AUTO_STOPPED;
   }
   switch ( geAutomaticState )
   {
      case AUTO_STOPPED:
         AUTO_Stopped();
         geLastAutomaticState = AUTO_STOPPED;
         break;

      case AUTO_MOVING:
         AUTO_In_Motion();
         geLastAutomaticState = AUTO_MOVING;
         break;

      case AUTO_CORRECTION:
         AUTO_Correction();
         geLastAutomaticState = AUTO_CORRECTION;
         break;

      case AUTO_IDLE:
         AUTO_Idle();
         geLastAutomaticState = AUTO_IDLE;
         break;

      default:
      case AUTO_UNKNOWN:
         AUTO_Unknown();
         geLastAutomaticState = AUTO_UNKNOWN;
         break;
   }
   ClearCurrentLandingCalls();
   SetParkingLamp();
   SetModState_AutoState( geAutomaticState );
}


enum en_automatic_state Auto_GetState(void)
{
   return geAutomaticState;
}

/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint8_t GetParkingLamp(void)
{
   return bParkingLamp && ( GetOperation_AutoMode() == MODE_A__NORMAL );
}
static void SetParkingLamp(void)
{
   uint8_t ucParkingFloor = IdleTime_GetParkingFloor();
   if( ( !GetMotion_RunFlag() )
    && ( GetOperation_CurrentFloor() == ucParkingFloor )
    && ( ( geAutomaticState == AUTO_IDLE ) || ( geAutomaticState == AUTO_STOPPED ) ) )
   {
      bParkingLamp = 1;
   }
   else
   {
      bParkingLamp = 0;
   }
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint8_t Auto_CheckDoorOpenButtons ( void ) {
   uint8_t ucOpenDoorRequest = 0;
   uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
   if ( GetDoorOpenButton ( DOOR_FRONT ) && (ucCurrentFloor < GetFP_NumFloors()) && gpastFloors[ucCurrentFloor].bFrontOpening ) {
      ucOpenDoorRequest |= 0x01;
   }

   if ( GetFP_RearDoors() && GetDoorOpenButton ( DOOR_REAR ) && (ucCurrentFloor < GetFP_NumFloors()) && gpastFloors[ucCurrentFloor].bRearOpening ) {
      ucOpenDoorRequest |= 0x10;
   }

   return ucOpenDoorRequest;
}

/*----------------------------------------------------------------------------
 Returns 1 if in FF2, momentary DCB option is set, and DCB has been pressed
 Holds assertion of DCB after momentary press.
 ----------------------------------------------------------------------------*/
#define TIME_TO_DEBOUNCE_DOOR_STATE_50MS     (4)
static uint8_t CheckIf_Fire2MomentaryDCB( enum en_doors eDoor )
{
   static uint8_t aucDebounce_50ms[NUM_OF_DOORS];
   uint8_t bDCB = 0;
   uint8_t Fire2MomentaryDCB = Fire_CheckIfFire2DoorsEnabled()
                            && Param_ReadValue_1Bit(enPARAM1__Fire__MomentaryDoorCloseButton);
   if( !GetFP_RearDoors() && ( eDoor == DOOR_REAR ) )
   {
      abFF2_MomentaryDCB[eDoor] = 0;
      aucDebounce_50ms[eDoor] = 0;
   }
   else if( Fire2MomentaryDCB )
   {
      uint8_t bCurrentDCB = GetDoorCloseButton(eDoor) != 0;
      if( bCurrentDCB )
      {
         abFF2_MomentaryDCB[eDoor] = 1;
         aucDebounce_50ms[eDoor] = 0;
      }
      else if( ( ( GetDoorState(eDoor) == DOOR__CLOSED ) && IsDoorFullyClosed(eDoor) )
            || ( ( GetDoorState(eDoor) == DOOR__OPEN ) && IsDoorFullyOpen(eDoor) ) )
      {
         if( aucDebounce_50ms[eDoor] < TIME_TO_DEBOUNCE_DOOR_STATE_50MS )
         {
            aucDebounce_50ms[eDoor]++;
         }
         else
         {
            abFF2_MomentaryDCB[eDoor] = 0;
         }
      }
      else
      {
         aucDebounce_50ms[eDoor] = 0;
      }
   }
   else
   {
      abFF2_MomentaryDCB[eDoor] = 0;
      aucDebounce_50ms[eDoor] = 0;
   }
   bDCB = abFF2_MomentaryDCB[eDoor];
   return bDCB;
}
/*----------------------------------------------------------------------------
 Returns state of hold request inputs DOOR_COMMAND__OPEN_HOLD_DWELL_REQUEST
 Bit 0 = front
 Bit 4 = rear
 ----------------------------------------------------------------------------*/
static uint8_t CheckCurrentDoorHoldRequests()
{
   uint8_t ucHold = 0;
   if( GetOperation_AutoMode() == MODE_A__NORMAL )
   {
      if(GetInputValue(enIN_DoorHold_F))
      {
         ucHold |= 0x01;
      }
      if(GetInputValue(enIN_DoorHold_R))
      {
         ucHold |= 0x10;
      }
   }
   return ucHold;
}
/*----------------------------------------------------------------------------
 Prevents both doors from opening on Independent service
 ----------------------------------------------------------------------------*/
static void CheckIndependentServiceDoors(uint8_t ucOpenDoorRequest)
{
   if( ( GetOperation_AutoMode() == MODE_A__INDP_SRV ) || ( GetOperation_AutoMode() == MODE_A__ATTENDANT ) )
   {
      if( ucOpenDoorRequest & 0x01 )
      {
         bIndSrvDoor_F = 1;
      }
      else if( ucOpenDoorRequest & 0x10 )
      {
         bIndSrvDoor_R = 1;
      }
      else if( !bIndSrvDoor_F && !bIndSrvDoor_R )
      {
         if( gpastFloors[GetOperation_CurrentFloor()].bFrontOpening)
         {
            bIndSrvDoor_F = 1;
         }
         else
         {
            bIndSrvDoor_R = 1;
         }
      }
   }

   else
   {
      bIndSrvDoor_F = 0;
      bIndSrvDoor_R = 0;
   }
}
/*----------------------------------------------------------------------------
 Check if there is a demand out on this floor to reopen doors.
 ----------------------------------------------------------------------------*/
uint8_t Auto_CheckDoorRequests ( void )
{
   static uint8_t abLastDCB[NUM_OF_DOORS];
   uint8_t ucOpenDoorRequest = GetCurrentLandingCalls();
   uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
   enum en_mode_auto enCurrentAutoMode = GetOperation_AutoMode();
   uint8_t ucDoorHoldRequest = CheckCurrentDoorHoldRequests();

   if(GetFault_ByNumber(FLT__OVERLOADED))
   {
      if (( (ucCurrentFloor < GetFP_NumFloors()) && gpastFloors[ucCurrentFloor].bFrontOpening) && (GetFP_RearDoors() && (ucCurrentFloor < GetFP_NumFloors()) && gpastFloors[ucCurrentFloor].bRearOpening))
      {
        SetDoorCommand(DOOR_FRONT, DOOR_COMMAND__OPEN_IN_CAR_REQUEST);
        SetDoorCommand(DOOR_REAR, DOOR_COMMAND__OPEN_IN_CAR_REQUEST);
      }
      else if(( (ucCurrentFloor < GetFP_NumFloors()) && gpastFloors[ucCurrentFloor].bFrontOpening))
      {
         SetDoorCommand(DOOR_FRONT, DOOR_COMMAND__OPEN_IN_CAR_REQUEST);
      }
      else if(GetFP_RearDoors() && (ucCurrentFloor < GetFP_NumFloors()) && gpastFloors[ucCurrentFloor].bRearOpening)
      {
         SetDoorCommand(DOOR_REAR, DOOR_COMMAND__OPEN_IN_CAR_REQUEST);
      }
   }
   else
   {
      //Check if i should auto open doors, or if capture mode is in the idle phase (captured)
      if ( ( !AutoModeRules_GetAutoDoorOpenFlag() )
        || ( ( getCaptureMode() == CAPTURE_IDLE ) && ( GetOperation_AutoMode() != MODE_A__SWING ) ) )
      {
         ucOpenDoorRequest = 0;
      }
      //check door open buttons, these are after the auto door open bits for safety reasons.
      ucOpenDoorRequest |= Auto_CheckDoorOpenButtons ();

      /* Prevent both doors from opening on independent service */
      CheckIndependentServiceDoors(ucOpenDoorRequest);

      uint8_t bFire2MomentaryDCB_F = CheckIf_Fire2MomentaryDCB(DOOR_FRONT);
      uint8_t bFire2MomentaryDCB_R = CheckIf_Fire2MomentaryDCB(DOOR_REAR);

      if( AutoModeRules_GetForceDoorsOpenOrClosedFlag() )
      {
         ucRecentDoorOpenRequest_50ms = 0;
      }
      if ( ucOpenDoorRequest || ucRecentDoorOpenRequest_50ms || ucDoorHoldRequest )
      {
         if (ucOpenDoorRequest || ucDoorHoldRequest)
         {
            /* Set initial timer so once i give an open request to the door, they have time to act on it. */
            ucRecentDoorOpenRequest_50ms = RECENT_DOOR_OPEN_REQ_TIMEOUT_50MS;
         }
         else if (ucRecentDoorOpenRequest_50ms)
         {
            ucRecentDoorOpenRequest_50ms--;
         }

         //Front Door Processing
         if ( (ucCurrentFloor < GetFP_NumFloors()) && gpastFloors[ucCurrentFloor].bFrontOpening) {
           if( enCurrentAutoMode == MODE_A__SABBATH)
           {
              SetDoorCommand( DOOR_FRONT, DOOR_COMMAND__OPEN_SABBATH_MODE);
           }
           else if ( ( AutoModeRules_GetDoorHoldFlag() )
                  && ( ucOpenDoorRequest & 0x03 ) )
           {
              SetDoorCommand( DOOR_FRONT, DOOR_COMMAND__OPEN_HOLD_REQUEST );
           }
           else if ( ucOpenDoorRequest & 0x02 ) //Hall Call
           {
              if(GetADAFlag())
              {
                 SetADAFlag(0);
                 SetDoorCommand( DOOR_FRONT, DOOR_COMMAND__OPEN_ADA_MODE );
              }
              /* If the extended lobby dwell time is enabled,
               * and at the lobby floor, use the lobby dwell door command. */
              else if( ( Param_ReadValue_8Bit(enPARAM8__LobbyDwellTime_1s) )
                    && ( ucCurrentFloor == Param_ReadValue_8Bit(enPARAM8__FireRecallFloorM) ) )
              {
                 SetDoorCommand( DOOR_FRONT, DOOR_COMMAND__OPEN_LOBBY_REQUEST );
              }
              else
              {
                 SetDoorCommand( DOOR_FRONT, DOOR_COMMAND__OPEN_HALL_REQUEST );
              }
           }
           else if ( ucOpenDoorRequest & 0x01 ) //Car Call
           {
              if(GetADAFlag())
              {
                 SetADAFlag(0);
                 SetDoorCommand( DOOR_FRONT, DOOR_COMMAND__OPEN_ADA_MODE );
              }
              /* If the extended lobby dwell time is enabled,
               * and at the lobby floor,
               * and enPARAM1__EnablePassingLobbyDO,
               * then use the lobby dwell door command even though its a CC. */
              else if( ( Param_ReadValue_1Bit(enPARAM1__EnablePassingLobbyDO) )
                    && ( Param_ReadValue_8Bit(enPARAM8__LobbyDwellTime_1s) )
                    && ( ucCurrentFloor == Param_ReadValue_8Bit(enPARAM8__FireRecallFloorM) ) )
              {
                 SetDoorCommand( DOOR_FRONT, DOOR_COMMAND__OPEN_LOBBY_REQUEST );
              }
              else
              {
                 SetDoorCommand( DOOR_FRONT, DOOR_COMMAND__OPEN_IN_CAR_REQUEST );
              }
           }

           else if( ucDoorHoldRequest & 0x01 ) // Door hold with dwell
           {
              SetDoorCommand( DOOR_FRONT, DOOR_COMMAND__OPEN_HOLD_DWELL_REQUEST );
           }
         }

         //Rear Door Processing
         if (GetFP_RearDoors() && (ucCurrentFloor < GetFP_NumFloors()) && gpastFloors[ucCurrentFloor].bRearOpening) {
           if( enCurrentAutoMode == MODE_A__SABBATH)
           {
              SetDoorCommand( DOOR_REAR, DOOR_COMMAND__OPEN_SABBATH_MODE);
           }
           else if ( ( AutoModeRules_GetDoorHoldFlag() )
                  && ( ucOpenDoorRequest & 0x30 ) )
           {
              SetDoorCommand( DOOR_REAR, DOOR_COMMAND__OPEN_HOLD_REQUEST );
           }
           else if ( ucOpenDoorRequest & 0x20 ) //Hall Call
           {
             if(GetADAFlag())
             {
                SetADAFlag(0);
                SetDoorCommand( DOOR_REAR, DOOR_COMMAND__OPEN_ADA_MODE );
             }
             /* If the extended lobby dwell time is enabled,
              * and at the lobby floor, use the lobby dwell door command. */
             else if( ( Param_ReadValue_8Bit(enPARAM8__LobbyDwellTime_1s) )
                   && ( ucCurrentFloor == Param_ReadValue_8Bit(enPARAM8__FireRecallFloorM) ) )
             {
                SetDoorCommand( DOOR_REAR, DOOR_COMMAND__OPEN_LOBBY_REQUEST );
             }
             else
             {
                SetDoorCommand( DOOR_REAR, DOOR_COMMAND__OPEN_HALL_REQUEST );
             }
           }
           else if ( ucOpenDoorRequest & 0x10 ) //Car Call
           {
              if(GetADAFlag())
              {
                 SetADAFlag(0);
                 SetDoorCommand( DOOR_REAR, DOOR_COMMAND__OPEN_ADA_MODE );
              }
              /* If the extended lobby dwell time is enabled,
               * and at the lobby floor,
               * and enPARAM1__EnablePassingLobbyDO,
               * then use the lobby dwell door command even though its a CC. */
              else if( ( Param_ReadValue_1Bit(enPARAM1__EnablePassingLobbyDO) )
                    && ( Param_ReadValue_8Bit(enPARAM8__LobbyDwellTime_1s) )
                    && ( ucCurrentFloor == Param_ReadValue_8Bit(enPARAM8__FireRecallFloorM) ) )
              {
                 SetDoorCommand( DOOR_REAR, DOOR_COMMAND__OPEN_LOBBY_REQUEST );
              }
              else
              {
                 SetDoorCommand( DOOR_REAR, DOOR_COMMAND__OPEN_IN_CAR_REQUEST );
              }
           }
           else if( ucDoorHoldRequest & 0x10 ) // Door hold with dwell
           {
              SetDoorCommand( DOOR_REAR, DOOR_COMMAND__OPEN_HOLD_DWELL_REQUEST );
           }
         }

         //Else, check for door open/close buttons
         //TODO: add in code and make more universal from all doors.
      } else {
         uint8_t bHasNoDestination = (GetNextDestination_AnyDirection() == INVALID_FLOOR);

         //Front Door Buttons
        if ( (ucCurrentFloor < GetFP_NumFloors()) && (ucCurrentFloor < GetFP_NumFloors()) && gpastFloors[ucCurrentFloor].bFrontOpening) {
            if ( ( !AutoModeRules_GetIgnoreDCBFlag() )
              && ( bFire2MomentaryDCB_F || ( GetDoorCloseButton ( DOOR_FRONT ) & 0xf ) )
              && ( GetDoorState(DOOR_FRONT) != DOOR__OPENING) ) //ignore if the doors are opening
            {
               SetDoorCommand( DOOR_FRONT, DOOR_COMMAND__CLOSE );
               abLastDCB[DOOR_FRONT] = 1;
            }
            else if ( AutoModeRules_GetForceDoorsOpenOrClosedFlag() )
            {
               SetDoorCommand( DOOR_FRONT, DOOR_COMMAND__NONE );
            }
            else if ( AutoModeRules_GetAutoDoorOpenFlag() )
            {
               if ( ( AutoModeRules_GetDoorHoldFlag() )
                 && ( ( GetDoorState(DOOR_FRONT) == DOOR__CLOSING ) || ( bHasNoDestination ) )
                 && ( bIndSrvDoor_F )
                 && ( !GetMotion_RunFlag() )
               )
               {
                  SetDoorCommand( DOOR_FRONT, DOOR_COMMAND__OPEN_HOLD_REQUEST );
                  ucOpenDoorRequest = 1;
                  ucRecentDoorOpenRequest_50ms = RECENT_DOOR_OPEN_REQ_TIMEOUT_50MS;
               }
            }
            else
            {
               SetDoorCommand( DOOR_FRONT, DOOR_COMMAND__NONE );
            }
         }

         //Rear Door Buttons
         if (GetFP_RearDoors() && gpastFloors[ucCurrentFloor].bRearOpening ) {

            if ( ( !AutoModeRules_GetIgnoreDCBFlag() )
              && ( bFire2MomentaryDCB_R || GetDoorCloseButton ( DOOR_REAR ) )
              && ( GetDoorCloseButton ( DOOR_REAR ) != 0x10 )   // ignores nudging
              && ( GetDoorState(DOOR_REAR) != DOOR__OPENING) )
            {
               SetDoorCommand( DOOR_REAR, DOOR_COMMAND__CLOSE );
            }
            else if ( AutoModeRules_GetForceDoorsOpenOrClosedFlag() )
            {
               SetDoorCommand( DOOR_REAR, DOOR_COMMAND__NONE );
            }
            else if ( AutoModeRules_GetAutoDoorOpenFlag() )
            {
               if ( ( AutoModeRules_GetDoorHoldFlag() )
                 && ( ( GetDoorState(DOOR_REAR) == DOOR__CLOSING ) || ( bHasNoDestination ) )
                 && ( bIndSrvDoor_R )
                 && ( !GetMotion_RunFlag() )
               )
               {
                  SetDoorCommand( DOOR_REAR, DOOR_COMMAND__OPEN_HOLD_REQUEST );
                  ucOpenDoorRequest = 1;
                  ucRecentDoorOpenRequest_50ms = ucDoorCheckDelay_50ms;
               }
            }
            else
            {
               SetDoorCommand( DOOR_REAR, DOOR_COMMAND__NONE );
            }
         }
      }
      if( AutoModeRules_GetForceDoorsOpenOrClosedFlag() )
      {
         ucRecentDoorOpenRequest_50ms = 0;
      }
   }
   return ( ucOpenDoorRequest || ucRecentDoorOpenRequest_50ms );
}

/*-----------------------------------------------------------------------------
   Initial state, as well as the state when faulted or when its mode
   is lost or confused.
 -----------------------------------------------------------------------------*/
static void AUTO_Unknown( void )
{
   bInDestinationDoorzone = 0;
   uwIdleTime_50ms = 0;
   if ( !GetCarStoppedFlag() || GetMotion_RunFlag() )
   {
       geAutomaticState = AUTO_MOVING;
   }
   else if ( !getDoorZone(DOOR_ANY) )
   {
      geAutomaticState = AUTO_CORRECTION;
   }
   else
   {
      if( !Fire_CheckIfFire2DoorsEnabled() ) {
         ResetDoors();
      }
      SetOperation_MotionCmd(MOCMD__NORMAL_STOP);
      geAutomaticState = AUTO_IDLE;
   }
}

/*-----------------------------------------------------------------------------
   In this mode, the car has just stopped and needs to open and cycle doors.
   this mode doesn't lead to motion.
 -----------------------------------------------------------------------------*/
static void AUTO_Stopped( void )
{
   static uint16_t uwTimeout;
   bInDestinationDoorzone = 0;
   if (geLastAutomaticState != AUTO_STOPPED)
   {
      uwTimeout  = 0;
      uwIdleTime_50ms = 0;
   }

   /* Addresses case where mod_motion state machine and
    * automatic operation state machine can get out of sync */
   if( GetMotion_RunFlag() )
   {
      geAutomaticState = AUTO_MOVING;
   }
   else if ( !getDoorZone(DOOR_ANY) && !GetMotion_RunFlag() )
   {
      SetAlarm(ALM__STOP_NO_DZ);
      geAutomaticState = AUTO_CORRECTION;
   }
   else if ( ( !GetMotion_RunFlag() )
          && ( getDoorZone(DOOR_ANY) ) )
   {
      uwTimeout = 0;
      bInDestinationDoorzone = 1;

      //Check for door requests, if there are none, then clear and goto idle
      if ( Auto_CheckDoorRequests() )
      {
      }
      else if ( CarDoorsClosed() && (!ucRecentDoorOpenRequest_50ms) )
      {
         geAutomaticState = AUTO_IDLE;
      }
      else if ( ucbFlag_Auto_Stop && ( bMarshalModeStatus() || GetInputValue( enIN_MARSHAL_MODE ) ) )
      {
      }
      else if( ( !AutoModeRules_GetDoorHoldFlag() )
            && ( AnyDoorFullyOpen() )
            && ( GetNextDestination_AnyDirection() != INVALID_FLOOR ) )
      {
         /* Car can get stuck with doors open here if last command was DOOR_COMMAND__OPEN_HOLD_REQUEST */
         if( ( GetLastDoorCommand(DOOR_FRONT) == DOOR_COMMAND__OPEN_HOLD_REQUEST )
          || ( GetLastDoorCommand(DOOR_REAR) == DOOR_COMMAND__OPEN_HOLD_REQUEST )
          || ( Doors_GetDwellCompleteFlag(DOOR_ANY) )
          )
         {
            /* Doors_GetDwellCompleteFlag addresses case car powered on with doors open.
             * Car will not close doors even with demand */
            CloseAnyOpenDoor();
         }
      }
      else if( ( !AutoModeRules_GetDoorHoldFlag() )
            && ( bCloseDoorsAtStartup ) )
      {
         /* Fix for stall when restarting MR with doors open. */
         CloseAnyOpenDoor();
         bCloseDoorsAtStartup = 0;
      }
   }
   else if ( uwTimeout >= AUTO_STATE_MOVEMENT_TIMEOUT_50MS ) {
      geAutomaticState = AUTO_UNKNOWN;
      SetOperation_MotionCmd(MOCMD__EMERG_STOP);
      if(Param_ReadValue_1Bit(enPARAM1__EnableEStopAlarms))
      {
         SetAlarm(ALM__ESTOP_STOP_TIMEOUT);
      }
   }
   else
   {
      uwTimeout++;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t PreOpeningCheck(void) {
   if( ( Motion_GetMotionState() != MOTION__START_SEQUENCE ) // Allow time for the
    && ( GetOperation_DestinationFloor() == GetOperation_CurrentFloor() ) )
   {
      uint32_t ulDestPosition = gpastFloors[GetOperation_DestinationFloor()].ulPosition;
      uint32_t uiPreopeningDistance = Param_ReadValue_16Bit(enPARAM16__PreOpeningDistance);
      if(uiPreopeningDistance > (uint32_t)SIX_INCHES)
      {
         uiPreopeningDistance = SIX_INCHES;
      }

      uint32_t ulUpperFloor = ( ulDestPosition + uiPreopeningDistance);
      uint32_t ulLowerFloor = ( ulDestPosition - uiPreopeningDistance);

      int16_t uwVelocity = GetPosition_Velocity();

      return ( (getDoorZone(DOOR_ANY)) //inside the door zone
            && (CheckIfInDoorZone())
            && (Param_ReadValue_16Bit(enPARAM16__PreOpeningDistance)) //Pre opening distance is set to more than 0
            && (uwVelocity > -150) //between -150 and 150 fpm
            && (uwVelocity < 150)
            && (GetPosition_PositionCount() < ulUpperFloor) //in range of floor
            && (GetPosition_PositionCount() > ulLowerFloor)
          && (
               (Motion_GetMotionState() == MOTION__DECELERATING) ||
               (Motion_GetMotionState() == MOTION__STOP_SEQUENCE)
             ));
   }
   else
   {
      return 0;
   }
}

/*-----------------------------------------------------------------------------
    In this mode when the car is in motion, this scans and updates
    for changes in destination, either from cancelled call, or from
    new call in the direction of answering.
 -----------------------------------------------------------------------------*/
static void AUTO_In_Motion( void )
{
   uint8_t bDestDZ = 0;
   static uint16_t ucWaitingToMove_50ms;
   uwIdleTime_50ms = 0;
   if (geLastAutomaticState != AUTO_MOVING)
   {
     ucWaitingToMove_50ms = 0;
   }

   if ( GetMotion_RunFlag() )
   {
      ucWaitingToMove_50ms = 0;
      if( ( Motion_GetMotionState() != MOTION__START_SEQUENCE ) // Allow time for the
       && ( GetOperation_DestinationFloor() == GetOperation_CurrentFloor() ) )
      {
         SetOperation_MotionCmd( MOCMD__NORMAL_STOP );

         //if in realDZ and distance is under the pre-opening distance
         //  check what door to open and send open command
         if ( Auto_GetPreopeningFlag() )
         {
            bDestDZ = 1;
            bInDestinationDoorzone = 1;// Flag must be set prior to checking door reqests
            Auto_CheckDoorRequests();
         }
      }
      else
      {
         uint8_t ucDestination = GetMidflightDestination();
         uint8_t ucReachableDest = Motion_GetNextReachableFloor();
         /* Check if the destination is between the
         * current destination to the next destination
         */
         if ( ( ucDestination != INVALID_FLOOR ) &&
              ( ( ( GetMotion_Direction() == DIR__UP ) && ( ucDestination >= ucReachableDest ) ) ||
                ( ( GetMotion_Direction() == DIR__DN ) && ( ucDestination <= ucReachableDest ) ) ) )
         {
           SetDestination(ucDestination);
           ucRecentDestinationRequestCountdown_50ms = RECENT_DESTINATION_REQUEST_COUNTDOWN_50MS;
         } else {
           CheckDestination();
         }
         bIndSrvDoor_F = 0;
         bIndSrvDoor_R = 0;
      }
   }
   else if ( ucWaitingToMove_50ms >= AUTO_STATE_MOVEMENT_TIMEOUT_50MS )
   {
      geAutomaticState = AUTO_UNKNOWN;
      SetOperation_MotionCmd(MOCMD__EMERG_STOP);
      if(Param_ReadValue_1Bit(enPARAM1__EnableEStopAlarms))
      {
         SetAlarm(ALM__ESTOP_MOVE_TIMEOUT);
      }
   }
   /* If after getting an assignment in the automatic_idle mode,
    * if the car gets here before getting feedback before the motion module
    * updates the destination floor, this state machine may return to stopped.*/
   else if( ( !ucRecentDestinationRequestCountdown_50ms )
         && ( GetOperation_DestinationFloor() == GetOperation_CurrentFloor() ) )
   {
      /* Make sure the in destination DZ flag doesnt toggle off
       * during the transition from moving to stopped, otherwise the
       * DO output will toggle. */
      if ( getDoorZone(DOOR_ANY) )
      {
         bDestDZ = 1;
      }

      //If no longer moving, set mode to stopped
      geAutomaticState = AUTO_STOPPED;
   }
   else
   {
      ucWaitingToMove_50ms++;
   }
   bInDestinationDoorzone = bDestDZ;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

static void AUTO_Correction ( void )
{
   bInDestinationDoorzone = 0;
   static uint8_t ucFaultSet;
   static uint16_t uwRelevelTimeout_50ms = 0;
   uwIdleTime_50ms = 0;
   if (geLastAutomaticState != AUTO_CORRECTION)
   {
      ucFaultSet = 0; //reset fault flag
      uwRelevelTimeout_50ms = 0;
   }

   if ( GetMotion_RunFlag() )
   {
      ucFaultSet = 0;
      uwRelevelTimeout_50ms = 0;
      geAutomaticState = AUTO_MOVING;
   }
   else if ( getDoorZone(DOOR_ANY) )
   {
      if ( InsideDeadZone() || gstFault.bActiveFault )
      {
         bInDestinationDoorzone = 1;
         Auto_CheckDoorRequests();
      }
      geAutomaticState = AUTO_STOPPED;
   }
   else
   {
      CloseAnyOpenDoor();
      if ( gstCurrentModeRules[ GetOperation_AutoMode() ].bAllowedOutsideDoorZone
        || GetLWD_CaptureFlag() ) /* Bypass at floor outside DZ fault when calibrating LWD */
      {
         geAutomaticState = AUTO_IDLE;
      }
      else if ( !(ucFaultSet) )
      {
         SetAlarm(ALM__STOP_NO_DZ);
         ucFaultSet = 0x1; //set fault flag for stopped outside DZ
      }
      else if( InsideDeadZone()
          && ( GetClosestValidOpening(DIR__NONE, DOOR_ANY) == GetOperation_CurrentFloor() ) )
      {
         /* Fault, dead-zone outside of door zone */
         SetFault(FLT__AT_FLOOR_NO_DZ);
      }
      else if (uwRelevelTimeout_50ms < Param_ReadValue_8Bit(enPARAM8__RelevelingDelay_50ms) )
      {
         uwRelevelTimeout_50ms++;
      }
      else if ( ( DoorsSafeAndReadyToRun() )
             && ( !gstFault.bActiveFault ) )
      {
         uint8_t ucFloor = GetClosestValidOpening(DIR__NONE, DOOR_ANY);
         SetDestination(ucFloor);
         ucRecentDestinationRequestCountdown_50ms = RECENT_DESTINATION_REQUEST_COUNTDOWN_50MS;
         geAutomaticState = AUTO_MOVING;
      }
   }
}

static uint8_t PrepareForRun(uint8_t ucDestination)
{
   uint8_t bPreparingForRun = 0;
   if( ( IdleTime_GetParkingFloor_Plus1() )
    || ( GetLWD_LearnFloorRequest_Plus1() )
    || ( GetOperation_MotionCmd() == MOCMD__GOTO_DEST )
    || ( ( ucDestination < GetFP_NumFloors() ) && ( ucDestination != GetOperation_CurrentFloor() ) )
    )
   {
      bPreparingForRun = 1;
   }
   else if( ( !GetLWD_CaptureFlag() )
         && ( CheckIf_MoveIdleCarTimeout(&ucDestination) ) )
   {
      bPreparingForRun = 1;
   }
   return bPreparingForRun;
}

/*-----------------------------------------------------------------------------
   Update adjustable delay for pre run door checks.
   This timer will delay the start of run slightly.
 -----------------------------------------------------------------------------*/
static void UpdateDoorCheckDelay(void)
{
   /* Update adjustable delay for pre run door checks. This timer will delay the start of run slightly. */
   uint8_t ucCheckDelay_50ms = Param_ReadValue_8Bit(enPARAM8__DoorCheckTime_100ms) * 2;
   if(!ucCheckDelay_50ms)
   {
      ucCheckDelay_50ms = RECENT_DOOR_OPEN_REQ_TIMEOUT_50MS;
   }
   else if( ucCheckDelay_50ms > RECENT_DOOR_OPEN_REQ_UPPER_BOUND_50MS )
   {
      ucCheckDelay_50ms = RECENT_DOOR_OPEN_REQ_UPPER_BOUND_50MS;
   }
   ucDoorCheckDelay_50ms = ucCheckDelay_50ms;
}
/*-----------------------------------------------------------------------------
   Checks if the MoveIdleCarTimer_10min (08-203) feature is enabled
   and the car is required to move to a random floor.

   Returns 1 if required to move.
 -----------------------------------------------------------------------------*/
#define MAX_MOVE_IDLE_CAR_TIMER__1MIN     (250)
static uint8_t CheckIf_MoveIdleCarTimeout(uint8_t *pucDestination)
{
   uint8_t ucReturn = 0;
   uint16_t uwIdleLimit_1min = Param_ReadValue_8Bit(enPARAM8__MoveIdleCarTimer_10min) * 10;
   if( ( uwIdleLimit_1min )
    && ( *pucDestination >= GetFP_NumFloors() ) ) /* Car is idle with no active destination */
   {
      uwIdleLimit_1min = ( uwIdleLimit_1min > MAX_MOVE_IDLE_CAR_TIMER__1MIN ) ? MAX_MOVE_IDLE_CAR_TIMER__1MIN : uwIdleLimit_1min;
      uint16_t uwIdleTime_1min = IdleTime_GetFanAndLightTime_Minutes();
      if( uwIdleTime_1min >= uwIdleLimit_1min )
      {
         /* Determine a random floor */
         *pucDestination = ( GetOperation_CurrentFloor()+uwIdleTime_1min ) % GetFP_NumFloors();
         ucReturn = 1;
      }
   }

   return ucReturn;
}
/*-----------------------------------------------------------------------------
   Returns the next destination floor modified the following special feature:

      Ensure car stops and opens its doors (for the lobby dwell time) every time it passes the lobby

 -----------------------------------------------------------------------------*/
static uint8_t GetDestination_CurrentDirection_Modified(void)
{
   uint8_t ucDestFloor = GetDestination_CurrentDirection();
   if( ( Param_ReadValue_1Bit(enPARAM1__EnablePassingLobbyDO) )
    && ( ucDestFloor < GetFP_NumFloors() ) )
   {
      uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
      uint8_t ucLobbyFloor = Param_ReadValue_8Bit(enPARAM8__FireRecallFloorM);
      if( ucDestFloor < ucCurrentFloor )
      {
         if( ( ucLobbyFloor < ucCurrentFloor )
          && ( ucLobbyFloor > ucDestFloor ) )
         {
            SetCarCall(ucLobbyFloor, DOOR_FRONT);
            if( GetFP_RearDoors() )
            {
               SetCarCall(ucLobbyFloor, DOOR_REAR);
            }
            ucDestFloor = ucLobbyFloor;
         }
      }
      else if( ucDestFloor > ucCurrentFloor )
      {
         if( ( ucLobbyFloor > ucCurrentFloor )
          && ( ucLobbyFloor < ucDestFloor ) )
         {
            SetCarCall(ucLobbyFloor, DOOR_FRONT);
            if( GetFP_RearDoors() )
            {
               SetCarCall(ucLobbyFloor, DOOR_REAR);
            }
            ucDestFloor = ucLobbyFloor;
         }
      }
   }

   return ucDestFloor;
}

/*-----------------------------------------------------------------------------
   After the car stops it hits the idle mode, in this mode the car is
   ready to move or open the doors again if needed
 -----------------------------------------------------------------------------*/
static void AUTO_Idle( void )
{
   static uint8_t ucTimerDoorsClosed = 0;
   //Increment Idle Timer;
   if (geLastAutomaticState != AUTO_IDLE)
   {
      uwIdleTime_50ms = 0;
      ucTimerDoorsClosed = 0;
   }

   uint8_t ucDestination = GetDestination_CurrentDirection_Modified();
   if ( !GetMotion_RunFlag()
     && !getDoorZone(DOOR_ANY)
     && !( gstCurrentModeRules[ GetOperation_AutoMode() ].bAllowedOutsideDoorZone || GetLWD_CaptureFlag() ) /* Bypass at floor outside DZ fault when calibrating LWD */)
   {
      geAutomaticState = AUTO_CORRECTION;
   }
   else if( ( IdleTime_GetParkingFloor() < GetFP_NumFloors() )
         && ( IdleTime_GetParkingDoorCommand() ) )
   {
      en_recall_door_command eRecallDoorCommand = IdleTime_GetParkingDoorCommand();
      Auto_Recall(0, IdleTime_GetParkingFloor(), eRecallDoorCommand);
   }
   /* Addresses case where mod_motion state machine and
    * automatic operation state machine can get out of sync */
   else if( GetMotion_RunFlag() )
   {
      geAutomaticState = AUTO_MOVING;
   }
   /* Allow door open commands if the car is in door zone,
    * AND
    * faulted OR not preparing for a run */
   else if( ( bInDestinationDoorzone || gstFault.bActiveFault )
         && ( getDoorZone(DOOR_ANY) )
         && ( !GetMotion_RunFlag() ) )
   {
      uint8_t ucDoorRequest = Auto_CheckDoorRequests();
      if ( ucDoorRequest )
      {
         geAutomaticState = AUTO_STOPPED; //car is now not idle.

         ucTimerDoorsClosed = 0;
      }
      else
      {
         //this will close the doors if they are set to hold, but you have a demand
         if( ( !AutoModeRules_GetDoorHoldFlag() )
          && ( !AutoModeRules_GetForceDoorsOpenOrClosedFlag() )
          && ( AnyDoorFullyOpen() )
          && ( GetNextDestination_AnyDirection() != INVALID_FLOOR )
          && ( ( GetLastDoorCommand(DOOR_FRONT) == DOOR_COMMAND__OPEN_HOLD_REQUEST )
            || ( GetLastDoorCommand(DOOR_REAR) == DOOR_COMMAND__OPEN_HOLD_REQUEST ) ) )
         {
            CloseAnyOpenDoor();
         }
      }
   }
   else
   {
      if( !( AutoModeRules_GetDoorHoldFlag() && getDoorZone(DOOR_ANY) && !GetMotion_RunFlag() ) )
      {
         CloseAnyOpenDoor();
      }

      if (  DoorsSafeAndReadyToRun() )
      {
         if (ucTimerDoorsClosed > ucDoorCheckDelay_50ms)
         {
            if( !ucRecentDoorOpenRequest_50ms )
            {
               if( CheckIf_MoveIdleCarTimeout(&ucDestination) )
               {
                  // CheckIf_MoveIdleCarTimeout will update ucDestination
               }
               else if( ( IdleTime_GetParkingFloor() < GetFP_NumFloors() )
                     && ( ucDestination >= GetFP_NumFloors() ) )
               {
                  ucDestination = IdleTime_GetParkingFloor();
               }

               SetDestination(ucDestination);
               ucRecentDestinationRequestCountdown_50ms = RECENT_DESTINATION_REQUEST_COUNTDOWN_50MS;

               if( GetOperation_MotionCmd() == MOCMD__GOTO_DEST )
               {
                  if( ucDestination < GetFP_NumFloors() )
                  {
                     geAutomaticState = AUTO_MOVING;
                  }
                  else
                  {
                     SetOperation_MotionCmd( MOCMD__NORMAL_STOP );
                  }
               }
            }
         }
         else
         {
            ucTimerDoorsClosed++;
         }
      }
      else
      {
         ucTimerDoorsClosed = 0;
      }
   }

   if( !IdleTime_GetParkingDoorCommand() )
   {
      /*
       * Destination is valid
       * and not the current floor,
       * and the door is close
       * and there isn't a recent request to open the door
       */
      if ( !ucRecentDoorOpenRequest_50ms &&
           PrepareForRun(ucDestination) &&
           CarDoorsClosed() )
      {
         bInDestinationDoorzone = 0;
      }
      else if( GetMotion_RunFlag() )
      {
         bInDestinationDoorzone = 0;
      }
      else
      {
         bInDestinationDoorzone = 1;
      }
   }
   else
   {
      // bInDestinationDoorzone handled by Auto_Recall
   }

   bCloseDoorsAtStartup = 0;

   UpdateDoorCheckDelay();
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
