/******************************************************************************
 *
 * @file     auto_marshal_mode.c
 * @brief
 * @version  V1.00
 * @date     15, November 2019
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>

#include "sys.h"
#include "mod.h"
#include "motion.h"



/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
uint8_t ucbFlag_Auto_Stop = 0;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
enum en_marshal_ccbs
{
   enIN_MARSHAL_CCB1,
	enIN_MARSHAL_CCB2,
	enIN_MARSHAL_CCB3,
	enIN_MARSHAL_CCB4,
	enIN_MARSHAL_CCB5,
	enIN_MARSHAL_CCB6,
	enIN_MARSHAL_CCB7,
	enIN_MARSHAL_CCB8,
	enIN_MARSHAL_CCB9,
	enIN_MARSHAL_CCB10,
	enIN_MARSHAL_CCB11,
	enIN_MARSHAL_CCB12,
	enIN_MARSHAL_CCB13,
	enIN_MARSHAL_CCB14,
	enIN_MARSHAL_CCB15,
	enIN_MARSHAL_CCB16,
	enIN_MARSHAL_CCB17,
	enIN_MARSHAL_CCB18,
	enIN_MARSHAL_CCB19,
	enIN_MARSHAL_CCB20,
	enIN_MARSHAL_CCB21,
	enIN_MARSHAL_CCB22,
	enIN_MARSHAL_CCB23,
	enIN_MARSHAL_CCB24,
	enIN_MARSHAL_CCB25,
	enIN_MARSHAL_CCB26,
	enIN_MARSHAL_CCB27,
	enIN_MARSHAL_CCB28,
	enIN_MARSHAL_CCB29,
	enIN_MARSHAL_CCB30,
	enIN_MARSHAL_CCB31,
	enIN_MARSHAL_CCB32,
	enIN_MARSHAL_CCB33,
	enIN_MARSHAL_CCB34,
	enIN_MARSHAL_CCB35,
	enIN_MARSHAL_CCB36,
	enIN_MARSHAL_CCB37,
	enIN_MARSHAL_CCB38,
	enIN_MARSHAL_CCB39,
	enIN_MARSHAL_CCB40,
	enIN_MARSHAL_CCB41,
	enIN_MARSHAL_CCB42,
	enIN_MARSHAL_CCB43,
	enIN_MARSHAL_CCB44,
	enIN_MARSHAL_CCB45,
	enIN_MARSHAL_CCB46,
	enIN_MARSHAL_CCB47,
	enIN_MARSHAL_CCB48,
	enIN_MARSHAL_CCB49,
	enIN_MARSHAL_CCB50,
	enIN_MARSHAL_CCB51,
	enIN_MARSHAL_CCB52,
	enIN_MARSHAL_CCB53,
	enIN_MARSHAL_CCB54,
	enIN_MARSHAL_CCB55,
	enIN_MARSHAL_CCB56,
	enIN_MARSHAL_CCB57,
	enIN_MARSHAL_CCB58,
	enIN_MARSHAL_CCB59,
	enIN_MARSHAL_CCB60,
	enIN_MARSHAL_CCB61,

	MAX__MARSHAL_CCB,
};

enum en_marshal_inputs
{
   enIN_MARSHAL_DOB = 61,
   enIN_MARSHAL_DCB = 62,
   enIN_MARSHAL_ENABLE = 63,


   MAX__MARSHAL_INPUTS,
};
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t ucDCBReady_Flag = 0;
static uint8_t ucbDOBPressed_Flag = 0;
static uint8_t ucbDCBPressed_Flag = 0;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
static void Set_MarshalExpDoorInputs( void )
{
   uint8_t ucExp48Inputs = GetExpansionInputs6_LocalInputs8();
   uint8_t ucExp56Inputs = GetExpansionInputs7_LocalInputs8();

   uint8_t ucCurrentFloor = GetOperation_CurrentFloor();

   ucbDOBPressed_Flag = 0;
   ucbDCBPressed_Flag = 0;
   /*
    * Disables DOB from system while in Marshal Mode
    */
   SetInputValue( enIN_DOB_F, 0 );
   SetInputValue( enIN_DOB_R, 0 );

   /*
    * Checks if the corresponding input is high on Expansion board
    * For DCB F/R: Mode Rules temporarily set to zero when high
    * to allow DCB to be enabled
    */
   if ( ( ucExp48Inputs >> ( enIN_MARSHAL_DOB % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1 )
   {
      SetInputValue( enIN_DOB_F, 1 );
      ucbDOBPressed_Flag = 1;
   }
   else if ( ( ucExp48Inputs >> ( enIN_MARSHAL_DCB % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1 )
   {
      gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreDCB = 0;
      SetInputValue( enIN_DCB_F, 1 );
      ucbDCBPressed_Flag = 1;
   }

   if ( ( ucExp56Inputs >> ( enIN_MARSHAL_DOB % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1 )
   {
      SetInputValue( enIN_DOB_R, 1 );
      ucbDOBPressed_Flag = 1;
   }
   else if ( ( ucExp56Inputs >> ( enIN_MARSHAL_DCB % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1 )
   {
      gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreDCB = 0;
      SetInputValue( enIN_DCB_R, 1 );
      ucbDCBPressed_Flag = 1;
   }

   if ( ucbDOBPressed_Flag && ( Motion_GetMotionState() == MOTION__STOPPED ) )
   {
      if( AnyDoorFullyOpen() )
      {
         ucbFlag_Auto_Stop = 1;
      }
   }

   else if ( ucbDCBPressed_Flag && ( Motion_GetMotionState() == MOTION__STOPPED ) )
   {
      ucDCBReady_Flag = 1;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Set_MarshalExpCCBInputs( void )
{
   uint8_t ucExp41Inputs = GetExpansionInputs6_LocalInputs1();
   uint8_t ucExp42Inputs = GetExpansionInputs6_LocalInputs2();
   uint8_t ucExp43Inputs = GetExpansionInputs6_LocalInputs3();
   uint8_t ucExp44Inputs = GetExpansionInputs6_LocalInputs4();
   uint8_t ucExp45Inputs = GetExpansionInputs6_LocalInputs5();
   uint8_t ucExp46Inputs = GetExpansionInputs6_LocalInputs6();
   uint8_t ucExp47Inputs = GetExpansionInputs6_LocalInputs7();
   uint8_t ucExp48Inputs = GetExpansionInputs6_LocalInputs8();

   uint8_t ucExp49Inputs = GetExpansionInputs7_LocalInputs1();
   uint8_t ucExp50Inputs = GetExpansionInputs7_LocalInputs2();
   uint8_t ucExp51Inputs = GetExpansionInputs7_LocalInputs3();
   uint8_t ucExp52Inputs = GetExpansionInputs7_LocalInputs4();
   uint8_t ucExp53Inputs = GetExpansionInputs7_LocalInputs5();
   uint8_t ucExp54Inputs = GetExpansionInputs7_LocalInputs6();
   uint8_t ucExp55Inputs = GetExpansionInputs7_LocalInputs7();
   uint8_t ucExp56Inputs = GetExpansionInputs7_LocalInputs8();

   /*
    * Checks if input is asserted in each expansion board
    * If asserted, checks which input was asserted and
    * makes a car call corresponding to the input
    */
   if ( ucExp41Inputs )
   {
      for ( uint8_t ucInputs = enIN_MARSHAL_CCB1; ucInputs < enIN_MARSHAL_CCB9; ucInputs++ )
      {
         uint8_t ucbInputSelect = ( ucExp41Inputs >> ( ucInputs % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1;
		   if ( ucbInputSelect )
	      {
		      gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_F = 0;
		      SetCarCall( ucInputs, DOOR_FRONT );
	      }
      }
   }
   if ( ucExp42Inputs )
   {
      for ( uint8_t ucInputs = enIN_MARSHAL_CCB9; ucInputs < enIN_MARSHAL_CCB17; ucInputs++ )
      {
         uint8_t ucbInputSelect = ( ucExp42Inputs >> ( ucInputs % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1;
         if ( ucbInputSelect )
         {
            gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_F = 0;
            SetCarCall( ucInputs, DOOR_FRONT );
         }
      }
   }
   if ( ucExp43Inputs )
   {
      for ( uint8_t ucInputs = enIN_MARSHAL_CCB17; ucInputs < enIN_MARSHAL_CCB25; ucInputs++ )
      {
         uint8_t ucbInputSelect = ( ucExp43Inputs >> ( ucInputs % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1;
         if ( ucbInputSelect )
         {
            gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_F = 0;
            SetCarCall( ucInputs, DOOR_FRONT );
         }
      }
   }
   if ( ucExp44Inputs )
   {
      for ( uint8_t ucInputs = enIN_MARSHAL_CCB25; ucInputs < enIN_MARSHAL_CCB33; ucInputs++ )
      {
         uint8_t ucbInputSelect = ( ucExp44Inputs >> ( ucInputs % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1;
         if ( ucbInputSelect )
         {
            gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_F = 0;
            SetCarCall( ucInputs, DOOR_FRONT );
         }
      }
   }
   if ( ucExp45Inputs )
   {
      for ( uint8_t ucInputs = enIN_MARSHAL_CCB33; ucInputs < enIN_MARSHAL_CCB41; ucInputs++ )
      {
         uint8_t ucbInputSelect = ( ucExp45Inputs >> ( ucInputs % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1;
         if ( ucbInputSelect )
         {
            gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_F = 0;
            SetCarCall( ucInputs, DOOR_FRONT );
         }
      }
   }
   if ( ucExp46Inputs )
   {
      for ( uint8_t ucInputs = enIN_MARSHAL_CCB41; ucInputs < enIN_MARSHAL_CCB49; ucInputs++ )
      {
         uint8_t ucbInputSelect = ( ucExp46Inputs >> ( ucInputs % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1;
         if ( ucbInputSelect )
         {
            gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_F = 0;
            SetCarCall( ucInputs, DOOR_FRONT );
         }
      }
   }
   if ( ucExp47Inputs )
   {
      for ( uint8_t ucInputs = enIN_MARSHAL_CCB49; ucInputs < enIN_MARSHAL_CCB57; ucInputs++ )
      {
         uint8_t ucbInputSelect = ( ucExp47Inputs >> ( ucInputs % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1;
         if ( ucbInputSelect )
         {
            gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_F = 0;
            SetCarCall( ucInputs, DOOR_FRONT );
         }
      }
   }
   if ( ucExp48Inputs & 0x1f )
   {
      for ( uint8_t ucInputs = enIN_MARSHAL_CCB57; ucInputs < MAX__MARSHAL_CCB; ucInputs++ )
      {
         uint8_t ucbInputSelect = ( ucExp48Inputs >> ( ucInputs % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1;
         if ( ucbInputSelect )
         {
            gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_F = 0;
            SetCarCall( ucInputs, DOOR_FRONT );
         }
      }
   }
   /*-----------------------------------------------------------------------------*/
   if ( ucExp49Inputs )
   {
      for ( uint8_t ucInputs = enIN_MARSHAL_CCB1; ucInputs < enIN_MARSHAL_CCB9; ucInputs++ )
      {
         uint8_t ucbInputSelect = ( ucExp49Inputs >> ( ucInputs % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1;
         if ( ucbInputSelect )
         {
            gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_R = 0;
            SetCarCall( ucInputs, DOOR_REAR );
         }
      }
   }
   if ( ucExp50Inputs )
   {
      for ( uint8_t ucInputs = enIN_MARSHAL_CCB9; ucInputs < enIN_MARSHAL_CCB17; ucInputs++ )
      {
         uint8_t ucbInputSelect = ( ucExp50Inputs >> ( ucInputs % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1;
         if ( ucbInputSelect )
         {
            gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_R = 0;
            SetCarCall( ucInputs, DOOR_REAR );
         }
      }
   }
   if ( ucExp51Inputs )
   {
      for ( uint8_t ucInputs = enIN_MARSHAL_CCB17; ucInputs < enIN_MARSHAL_CCB25; ucInputs++ )
      {
         uint8_t ucbInputSelect = ( ucExp51Inputs >> ( ucInputs % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1;
         if ( ucbInputSelect )
         {
            gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_R = 0;
            SetCarCall( ucInputs, DOOR_REAR );
         }
      }
   }
   if ( ucExp52Inputs )
   {
      for ( uint8_t ucInputs = enIN_MARSHAL_CCB25; ucInputs < enIN_MARSHAL_CCB33; ucInputs++ )
      {
         uint8_t ucbInputSelect = ( ucExp52Inputs >> ( ucInputs % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1;
         if ( ucbInputSelect )
         {
            gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_R = 0;
            SetCarCall( ucInputs, DOOR_REAR );
         }
      }
   }
   if ( ucExp53Inputs )
   {
      for ( uint8_t ucInputs = enIN_MARSHAL_CCB33; ucInputs < enIN_MARSHAL_CCB41; ucInputs++ )
      {
         uint8_t ucbInputSelect = ( ucExp53Inputs >> ( ucInputs % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1;
         if ( ucbInputSelect )
         {
            gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_R = 0;
            SetCarCall( ucInputs, DOOR_REAR );
         }
      }
   }
   if ( ucExp54Inputs )
   {
      for ( uint8_t ucInputs = enIN_MARSHAL_CCB41; ucInputs < enIN_MARSHAL_CCB49; ucInputs++ )
      {
         uint8_t ucbInputSelect = ( ucExp54Inputs >> ( ucInputs % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1;
         if ( ucbInputSelect )
         {
            gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_R = 0;
            SetCarCall( ucInputs, DOOR_REAR );
         }
      }
   }
   if ( ucExp55Inputs )
   {
      for ( uint8_t ucInputs = enIN_MARSHAL_CCB49; ucInputs < enIN_MARSHAL_CCB57; ucInputs++ )
      {
         uint8_t ucbInputSelect = ( ucExp55Inputs >> ( ucInputs % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1;
         if ( ucbInputSelect )
         {
            gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_R = 0;
            SetCarCall( ucInputs, DOOR_REAR );
         }
      }
   }
   if ( ucExp56Inputs & 0x1f )
   {
      for ( uint8_t ucInputs = enIN_MARSHAL_CCB57; ucInputs < MAX__MARSHAL_CCB; ucInputs++ )
      {
         uint8_t ucbInputSelect = ( ucExp56Inputs >> ( ucInputs % MIN_TERMINALS_PER_IO_BOARD ) ) & 0x1;
         if ( ucbInputSelect )
         {
            gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_R = 0;
            SetCarCall( ucInputs, DOOR_REAR );
         }
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
   /*
    * Checks if the Marshal Key is asserted in Expansion board to enable Marshal Mode
    */
uint8_t bMarshalModeStatus( void )
{
   uint8_t ucbFlag = 0;

   uint8_t ucExp48Inputs = GetExpansionInputs6_LocalInputs8() & 0x80;
   uint8_t ucExp56Inputs = GetExpansionInputs7_LocalInputs8() & 0x80;

   if ( ucExp48Inputs || ucExp56Inputs )
   {
      SetInputValue(enIN_MARSHAL_MODE, 1);
      ucbFlag = 1;
   }

   return ucbFlag;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
   /*
    * Gets CCL front outputs from Expansion 41 to be used in sdata_mra.c
    */
uint8_t Get_Exp41CCLOutputs_F( void )
{
   uint8_t ucByteOutput = 0;

   for ( uint8_t ucOutputs = enIN_MARSHAL_CCB1; ucOutputs < enIN_MARSHAL_CCB9; ucOutputs++ )
   {
      if ( gpastCarCalls_F[ ucOutputs ].bLamp )
      {
         ucByteOutput |= ( gpastCarCalls_F[ ucOutputs ].bLamp << ( ucOutputs % MIN_TERMINALS_PER_IO_BOARD ) );
      }
   }
   return ucByteOutput;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
   /*
    * Gets CCL front outputs from Expansion 42 to be use in sdata_mra.c
    */
uint8_t Get_Exp42CCLOutputs_F ( void )
{
   uint8_t ucByteOutput = 0;

   for ( uint8_t ucOutputs = enIN_MARSHAL_CCB9; ucOutputs < enIN_MARSHAL_CCB17; ucOutputs++ )
   {
      if ( gpastCarCalls_F[ ucOutputs ].bLamp )
      {
         ucByteOutput |= ( gpastCarCalls_F[ ucOutputs ].bLamp << ( ucOutputs % MIN_TERMINALS_PER_IO_BOARD ) );
      }
   }
   return ucByteOutput;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
   /*
    * Gets CCL front outputs from Expansion 42 to be use in sdata_mra.c
    */
uint8_t Get_Exp43CCLOutputs_F ( void )
{
   uint8_t ucByteOutput = 0;

   for ( uint8_t ucOutputs = enIN_MARSHAL_CCB17; ucOutputs < enIN_MARSHAL_CCB25; ucOutputs++ )
   {
      if ( gpastCarCalls_F[ ucOutputs ].bLamp )
      {
         ucByteOutput |= ( gpastCarCalls_F[ ucOutputs ].bLamp << ( ucOutputs % MIN_TERMINALS_PER_IO_BOARD ) );
      }
   }
   return ucByteOutput;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
   /*
    * Gets CCL front outputs from Expansion 42 to be use in sdata_mra.c
    */
uint8_t Get_Exp44CCLOutputs_F ( void )
{
   uint8_t ucByteOutput = 0;

   for ( uint8_t ucOutputs = enIN_MARSHAL_CCB25; ucOutputs < enIN_MARSHAL_CCB33; ucOutputs++ )
   {
      if ( gpastCarCalls_F[ ucOutputs ].bLamp )
      {
         ucByteOutput |= ( gpastCarCalls_F[ ucOutputs ].bLamp << ( ucOutputs % MIN_TERMINALS_PER_IO_BOARD ) );
      }
   }
   return ucByteOutput;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
   /*
    * Gets CCL front outputs from Expansion 42 to be use in sdata_mra.c
    */
uint8_t Get_Exp45CCLOutputs_F ( void )
{
   uint8_t ucByteOutput = 0;

   for ( uint8_t ucOutputs = enIN_MARSHAL_CCB33; ucOutputs < enIN_MARSHAL_CCB41; ucOutputs++ )
   {
      if ( gpastCarCalls_F[ ucOutputs ].bLamp )
      {
         ucByteOutput |= ( gpastCarCalls_F[ ucOutputs ].bLamp << ( ucOutputs % MIN_TERMINALS_PER_IO_BOARD ) );
      }
   }
   return ucByteOutput;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
   /*
    * Gets CCL front outputs from Expansion 42 to be use in sdata_mra.c
    */
uint8_t Get_Exp46CCLOutputs_F ( void )
{
   uint8_t ucByteOutput = 0;

   for ( uint8_t ucOutputs = enIN_MARSHAL_CCB41; ucOutputs < enIN_MARSHAL_CCB49; ucOutputs++ )
   {
      if ( gpastCarCalls_F[ ucOutputs ].bLamp )
      {
         ucByteOutput |= ( gpastCarCalls_F[ ucOutputs ].bLamp << ( ucOutputs % MIN_TERMINALS_PER_IO_BOARD ) );
      }
   }
   return ucByteOutput;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
   /*
    * Gets CCL front outputs from Expansion 42 to be use in sdata_mra.c
    */
uint8_t Get_Exp47CCLOutputs_F ( void )
{
   uint8_t ucByteOutput = 0;

   for ( uint8_t ucOutputs = enIN_MARSHAL_CCB49; ucOutputs < enIN_MARSHAL_CCB57; ucOutputs++ )
   {
      if ( gpastCarCalls_F[ ucOutputs ].bLamp )
      {
         ucByteOutput |= ( gpastCarCalls_F[ ucOutputs ].bLamp << ( ucOutputs % MIN_TERMINALS_PER_IO_BOARD ) );
      }
   }
   return ucByteOutput;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
   /*
    * Gets CCL front outputs from Expansion 42 to be use in sdata_mra.c
    */
uint8_t Get_Exp48CCLOutputs_F ( void )
{
   uint8_t ucByteOutput = 0;

   for ( uint8_t ucOutputs = enIN_MARSHAL_CCB57; ucOutputs < MAX__MARSHAL_CCB; ucOutputs++ )
   {
      if ( gpastCarCalls_F[ ucOutputs ].bLamp )
      {
         ucByteOutput |= ( gpastCarCalls_F[ ucOutputs ].bLamp << ( ucOutputs % MIN_TERMINALS_PER_IO_BOARD ) );
      }
   }
   return ucByteOutput;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
   /*
    * Gets CCL rear outputs from Expansion 49 to be used in sdata_mra.c
    */
uint8_t Get_Exp49CCLOutputs_R ( void )
{
   uint8_t ucByteOutput = 0;

   for ( uint8_t ucOutputs = enIN_MARSHAL_CCB1; ucOutputs < enIN_MARSHAL_CCB9; ucOutputs++ )
   {
      if ( gpastCarCalls_R[ ucOutputs ].bLamp )
      {
         ucByteOutput |= ( gpastCarCalls_R[ ucOutputs ].bLamp << ( ucOutputs % MIN_TERMINALS_PER_IO_BOARD ) );
      }
   }
   return ucByteOutput;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
   /*
    * Gets CCL rear outputs from Expansion 49 to be used in sdata_mra.c
    */
uint8_t Get_Exp50CCLOutputs_R ( void )
{
   uint8_t ucByteOutput = 0;

   for ( uint8_t ucOutputs = enIN_MARSHAL_CCB9; ucOutputs < enIN_MARSHAL_CCB17; ucOutputs++ )
   {
      if ( gpastCarCalls_R[ ucOutputs ].bLamp )
      {
         ucByteOutput |= ( gpastCarCalls_R[ ucOutputs ].bLamp << ( ucOutputs % MIN_TERMINALS_PER_IO_BOARD ) );
      }
   }
   return ucByteOutput;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
   /*
    * Gets CCL rear outputs from Expansion 49 to be used in sdata_mra.c
    */
uint8_t Get_Exp51CCLOutputs_R ( void )
{
   uint8_t ucByteOutput = 0;

   for ( uint8_t ucOutputs = enIN_MARSHAL_CCB17; ucOutputs < enIN_MARSHAL_CCB25; ucOutputs++ )
   {
      if ( gpastCarCalls_R[ ucOutputs ].bLamp )
      {
         ucByteOutput |= ( gpastCarCalls_R[ ucOutputs ].bLamp << ( ucOutputs % MIN_TERMINALS_PER_IO_BOARD ) );
      }
   }
   return ucByteOutput;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
   /*
    * Gets CCL rear outputs from Expansion 49 to be used in sdata_mra.c
    */
uint8_t Get_Exp52CCLOutputs_R ( void )
{
   uint8_t ucByteOutput = 0;

   for ( uint8_t ucOutputs = enIN_MARSHAL_CCB25; ucOutputs < enIN_MARSHAL_CCB33; ucOutputs++ )
   {
      if ( gpastCarCalls_R[ ucOutputs ].bLamp )
      {
         ucByteOutput |= ( gpastCarCalls_R[ ucOutputs ].bLamp << ( ucOutputs % MIN_TERMINALS_PER_IO_BOARD ) );
      }
   }
   return ucByteOutput;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
   /*
    * Gets CCL rear outputs from Expansion 49 to be used in sdata_mra.c
    */
uint8_t Get_Exp53CCLOutputs_R ( void )
{
   uint8_t ucByteOutput = 0;

   for ( uint8_t ucOutputs = enIN_MARSHAL_CCB33; ucOutputs < enIN_MARSHAL_CCB41; ucOutputs++ )
   {
      if ( gpastCarCalls_R[ ucOutputs ].bLamp )
      {
         ucByteOutput |= ( gpastCarCalls_R[ ucOutputs ].bLamp << ( ucOutputs % MIN_TERMINALS_PER_IO_BOARD ) );
      }
   }
   return ucByteOutput;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
   /*
    * Gets CCL rear outputs from Expansion 49 to be used in sdata_mra.c
    */
uint8_t Get_Exp54CCLOutputs_R ( void )
{
   uint8_t ucByteOutput = 0;

   for ( uint8_t ucOutputs = enIN_MARSHAL_CCB41; ucOutputs < enIN_MARSHAL_CCB49; ucOutputs++ )
   {
      if ( gpastCarCalls_R[ ucOutputs ].bLamp )
      {
         ucByteOutput |= ( gpastCarCalls_R[ ucOutputs ].bLamp << ( ucOutputs % MIN_TERMINALS_PER_IO_BOARD ) );
      }
   }
   return ucByteOutput;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
   /*
    * Gets CCL rear outputs from Expansion 49 to be used in sdata_mra.c
    */
uint8_t Get_Exp55CCLOutputs_R ( void )
{
   uint8_t ucByteOutput = 0;

   for ( uint8_t ucOutputs = enIN_MARSHAL_CCB49; ucOutputs < enIN_MARSHAL_CCB57; ucOutputs++ )
   {
      if ( gpastCarCalls_R[ ucOutputs ].bLamp )
      {
         ucByteOutput |= ( gpastCarCalls_R[ ucOutputs ].bLamp << ( ucOutputs % MIN_TERMINALS_PER_IO_BOARD ) );
      }
   }
   return ucByteOutput;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
   /*
    * Gets CCL rear outputs from Expansion 49 to be used in sdata_mra.c
    */
uint8_t Get_Exp56CCLOutputs_R ( void )
{
   uint8_t ucByteOutput = 0;

   for ( uint8_t ucOutputs = enIN_MARSHAL_CCB57; ucOutputs < MAX__MARSHAL_CCB; ucOutputs++ )
   {
      if ( gpastCarCalls_R[ ucOutputs ].bLamp )
      {
         ucByteOutput |= ( gpastCarCalls_R[ ucOutputs ].bLamp << ( ucOutputs % MIN_TERMINALS_PER_IO_BOARD ) );
      }
   }
   return ucByteOutput;
}
/*-----------------------------------------------------------------------------

   Called from Oper_Auto

 -----------------------------------------------------------------------------*/
void Auto_Marshal_Mode( uint8_t bInit )
{
   static uint16_t uwTimeout_Auto_Oper = 0;

   if( bInit )
   {
      ClearLatchedCarCalls();
      ClearLatchedHallCalls();
      StopNextAvailableLanding();
   }
   /*
    * Disables CCB from system while in Marshal Mode
    */
   gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_F = 1;
   gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreCarCall_R = 1;
   /*
    * Disables DCB from system while in Marshal Mode
    */
   gstCurrentModeRules[MODE_A__MARSHAL_MODE].bIgnoreDCB = 1;
   /*
    * Handles logic for CCB
    */
   Set_MarshalExpCCBInputs();
   /*
    * Handles logic for DOB and DCB
    */
   Set_MarshalExpDoorInputs();
   /*
    * Prevents back and forth opening and closing of doors when DCB is pressed
    * and there's latched car calls
    */
   if ( ucDCBReady_Flag )
   {
      if( !IsDoorFullyClosed( DOOR_FRONT ) || !IsDoorFullyClosed( DOOR_REAR ) )
      {
         SetOperation_MotionCmd(MOCMD__NORMAL_STOP);
         Auto_Oper( bInit );
      }
      else
      {
         ucDCBReady_Flag = 0;
         ucbFlag_Auto_Stop = 0;
      }
   }
   /*
    * Prevents back and forth opening and closing of doors when DOB is pressed
    * and there's latched CCs.
    */
   else if( ucbFlag_Auto_Stop )
   {
      SetOperation_MotionCmd(MOCMD__NORMAL_STOP);
   }
   /*
    * Goes into Auto_Oper if DOB is pressed, otherwise waits 10 seconds before
    * dispatching latched CCs
    */
   else if( ( ( Motion_GetMotionState() == MOTION__STOPPED ) && !ucbFlag_Auto_Stop ) && ( !ucDCBReady_Flag ) )
   {
      uwTimeout_Auto_Oper += MOD_RUN_PERIOD_OPER_AUTO_1MS;
      if (ucbDOBPressed_Flag)
      {
         Auto_Oper( bInit );
         uwTimeout_Auto_Oper = 0;
      }
      else if( uwTimeout_Auto_Oper > 150 )
      {
         Auto_Oper( bInit );
         uwTimeout_Auto_Oper = 0;
      }
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
