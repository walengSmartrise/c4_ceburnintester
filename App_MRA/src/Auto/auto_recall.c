#include "mod.h"

//#include "LPC407x_8x_177x_8x.h"
#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "motion.h"
#include "remoteCommands.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

#define SECOND (20)

#define CAR_STUCK_TIMEOUT_50MS         (200)

#define MAX_EQ_SPEED__FPM           (75)
#define MAX_EQ_HOISTWAY_SCAN__FPM 150
#define MIN_EQ_HOISTWAY_SCAN__FPM 10
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

enum en_recall_states enRecallStates;
static uint8_t ucMovementTimeout_50ms;
static uint16_t uwDoorNudgeTimeout = 0;

// Timer to delay car doors opening if faulted and doors are closed but in DZ
static uint16_t uwCarStuckTimer_50ms;
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
void ResetRecallState()
{
   enRecallStates = RECALL__UNKNOWN;
   ucMovementTimeout_50ms = 0;
   uwDoorNudgeTimeout = 0;
   uwCarStuckTimer_50ms = 0;
}
enum en_recall_states GetRecallState ( void )
{
   return enRecallStates;
}

/*----------------------------------------------------------------------------
   Get closest valid opening
   eDir = DIR__UP returns closest above
   eDir = DIR__DN returns closest below
   eDir = DIR__NONE returns closest of either above or below
*----------------------------------------------------------------------------*/
uint8_t GetClosestValidOpening( enum direction_enum eDir, enum en_doors eDoor )
{
   uint8_t ucClosestFloor = INVALID_FLOOR;
   uint32_t uiCurrentPos = GetPosition_PositionCount();
   uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
   if( ucCurrentFloor < GetFP_NumFloors() )
   {
      if( eDir == DIR__UP )
      {
         ucCurrentFloor = Motion_GetNextReachableFloor();
         for( uint8_t ucFloor = ucCurrentFloor; ucFloor < GetFP_NumFloors(); ucFloor++ )
         {
            if( ( uiCurrentPos <= gpastFloors[ucFloor].ulPosition ) )
            {
               if( ( ( ( eDoor == DOOR_FRONT ) || ( eDoor == DOOR_ANY ) )&& gpastFloors[ucFloor].bFrontOpening )
                || ( ( ( eDoor == DOOR_REAR ) || ( eDoor == DOOR_ANY ) ) && gpastFloors[ucFloor].bRearOpening ) )
               {
                  ucClosestFloor = ucFloor;
                  break;
               }
            }
         }
      }
      else if( eDir == DIR__DN )
      {
         ucCurrentFloor = Motion_GetNextReachableFloor();
         for( uint8_t ucFloor_Plus1 = ucCurrentFloor+1; ucFloor_Plus1 > 0; ucFloor_Plus1-- )
         {
            if( ( uiCurrentPos >= gpastFloors[ucFloor_Plus1-1].ulPosition ) )
            {
               if( ( ( ( eDoor == DOOR_FRONT ) || ( eDoor == DOOR_ANY ) ) && gpastFloors[ucFloor_Plus1-1].bFrontOpening )
                || ( ( ( eDoor == DOOR_REAR ) || ( eDoor == DOOR_ANY ) ) && gpastFloors[ucFloor_Plus1-1].bRearOpening ) )
               {
                  ucClosestFloor = ucFloor_Plus1-1;
                  break;
               }
            }
         }
      }
      else
      {
         uint32_t uiAbovePosDiff = 0xFFFFFFFF;
         uint32_t uiBelowPosDiff = 0xFFFFFFFF;
         uint8_t ucAboveFloor = INVALID_FLOOR;
         uint8_t ucBelowFloor = INVALID_FLOOR;
         for( uint8_t ucFloor = ucCurrentFloor; ucFloor < GetFP_NumFloors(); ucFloor++ )
         {
            if( ( uiCurrentPos <= gpastFloors[ucFloor].ulPosition ) )
            {
               if( ( ( ( eDoor == DOOR_FRONT ) || ( eDoor == DOOR_ANY ) )&& gpastFloors[ucFloor].bFrontOpening )
                || ( ( ( eDoor == DOOR_REAR ) || ( eDoor == DOOR_ANY ) ) && gpastFloors[ucFloor].bRearOpening ) )
               {
                  ucAboveFloor = ucFloor;
                  uiAbovePosDiff = gpastFloors[ucFloor].ulPosition - uiCurrentPos;
                  break;
               }
            }
         }
         for( uint8_t ucFloor_Plus1 = ucCurrentFloor+1; ucFloor_Plus1 > 0; ucFloor_Plus1-- )
         {
            if( ( uiCurrentPos >= gpastFloors[ucFloor_Plus1-1].ulPosition ) )
            {
               if( ( ( ( eDoor == DOOR_FRONT ) || ( eDoor == DOOR_ANY ) ) && gpastFloors[ucFloor_Plus1-1].bFrontOpening )
                || ( ( ( eDoor == DOOR_REAR ) || ( eDoor == DOOR_ANY ) ) && gpastFloors[ucFloor_Plus1-1].bRearOpening ) )
               {
                  ucBelowFloor = ucFloor_Plus1-1;
                  uiBelowPosDiff = uiCurrentPos - gpastFloors[ucFloor_Plus1-1].ulPosition;
                  break;
               }
            }
         }

         ucClosestFloor = (uiAbovePosDiff < uiBelowPosDiff) ? ucAboveFloor:ucBelowFloor;

         /* Exception if the car is within 3 inches of the current floor.
          * Fixes car recalling to new floor on reset for modes like EQ */
         uint32_t uiUpperLimit = gpastFloors[ucCurrentFloor].ulPosition + TWO_INCHES;
         uint32_t uiLowerLimit = gpastFloors[ucCurrentFloor].ulPosition - TWO_INCHES;
         if( ( uiCurrentPos > uiLowerLimit )
          && ( uiCurrentPos < uiUpperLimit ) )
         {
            if( ( ( ( eDoor == DOOR_FRONT ) || ( eDoor == DOOR_ANY ) ) && gpastFloors[ucCurrentFloor].bFrontOpening )
             || ( ( ( eDoor == DOOR_REAR ) || ( eDoor == DOOR_ANY ) ) && gpastFloors[ucCurrentFloor].bRearOpening ) )
            {
               ucClosestFloor = ucCurrentFloor;
            }
         }
      }
   }

   return ucClosestFloor;
}

static void CloseDoor ( uint8_t ucDoorIndex, uint16_t uwDoorNudgeTimeout )
{
   if (GetDoorState(ucDoorIndex) != DOOR__CLOSED)
   {
      //set door nudge command;
      //Timer here if on door Hold setup to wait 3 seconds before closing.
      if ( ( GetLastDoorCommand(ucDoorIndex) != DOOR_COMMAND__OPEN_HOLD_REQUEST )
        || ( uwDoorNudgeTimeout >= (SECOND * 3) ) ) {
         SetDoorCommand( ucDoorIndex, DOOR_COMMAND__NUDGE );
      }
   }
}

/*----------------------------------------------------------------------------
   Check for invalid recall desination
 *----------------------------------------------------------------------------*/
#define INVALID_RECALL_DEST_ALARM_DEBOUNCE_LIMIT      (200)
static void CheckFor_InvalidRecallDestinationAlarm( uint8_t ucRecallFloor, en_recall_door_command eDoorCommand )
{
   static uint8_t ucDebounceCounter;
   if( ucRecallFloor >= GetFP_NumFloors() )
   {
      if( ucDebounceCounter < INVALID_RECALL_DEST_ALARM_DEBOUNCE_LIMIT )
      {
         ucDebounceCounter++;
      }
      else
      {
         SetAlarm(ALM__RECALL_INVALID_FLOOR);
      }
   }
   else if( eDoorCommand >= NUM_RECALL_DOOR_COMMAND )
   {
      if( ucDebounceCounter < INVALID_RECALL_DEST_ALARM_DEBOUNCE_LIMIT )
      {
         ucDebounceCounter++;
      }
      else
      {
         SetAlarm(ALM__RECALL_INVALID_DOOR);
      }
   }
   else if( ( ( eDoorCommand == RECALL_DOOR_COMMAND__OPEN_FRONT_DOOR ) && !gpastFloors[ucRecallFloor].bFrontOpening )
         || ( ( eDoorCommand == RECALL_DOOR_COMMAND__OPEN_REAR_DOOR ) && !gpastFloors[ucRecallFloor].bRearOpening )
         || ( ( ( eDoorCommand == RECALL_DOOR_COMMAND__OPEN_BOTH_DOORS ) || ( eDoorCommand == RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR ) ) && !gpastFloors[ucRecallFloor].bFrontOpening && !gpastFloors[ucRecallFloor].bRearOpening ) )
   {
      if( ucDebounceCounter < INVALID_RECALL_DEST_ALARM_DEBOUNCE_LIMIT )
      {
         ucDebounceCounter++;
      }
      else
      {
         SetAlarm(ALM__RECALL_INVALID_OPENING);
      }
   }
   else
   {
      ucDebounceCounter = 0;
   }
}
/*----------------------------------------------------------------------------
 See CheckIfValidRecallMode() to add valid recall modes of operation
   This function should be called when the car should be moved to a floor and a door should be held open.
   If the car is heading away from the recall floor when this function is called it will stop at the next available landing and reverse directions.
   If the car is faulted, doors will be held open at its current floor.
 *----------------------------------------------------------------------------*/
uint8_t Auto_Recall ( uint8_t bInitMode, uint8_t ucRecallFloor, en_recall_door_command eDoorCommand )
{
   uint8_t abRecallFinished[NUM_OF_DOORS] = {0};
   uint8_t ucNumberOfDoors = GetNumberOfDoors();

   /* Modify speed limit if running on generator power */
   if( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
    && ( ( GetOperation_AutoMode() == MODE_A__EPOWER )
      || ( EPower_GetCarRecalledFlag() ) ) )
   {
      uint16_t uwSpeedLimit = Param_ReadValue_16Bit(enPARAM16__EPowerSpeed_fpm);
      if( uwSpeedLimit < MIN_EPOWER_SPEED_LIMIT_FPM )
      {
         uwSpeedLimit = MIN_EPOWER_SPEED_LIMIT_FPM;
      }
      Auto_SetSpeed(uwSpeedLimit);
   }// set the speed limit ks note: bound speed to >= 75, and <= 150 fpm
   else if(GetOperation_AutoMode() == MODE_A__SEISMC || GetOperation_AutoMode() == MODE_A__CW_DRAIL )
   {
      uint8_t ucSpeedLimit = Param_ReadValue_8Bit(enPARAM8__EQ_Hoistway_Scan_Speed);
      if( ucSpeedLimit < MIN_EQ_HOISTWAY_SCAN__FPM )
      {
         ucSpeedLimit = MIN_EQ_HOISTWAY_SCAN__FPM;
      }
      else if( ucSpeedLimit > MAX_EQ_HOISTWAY_SCAN__FPM )
      {
          ucSpeedLimit = MAX_EQ_HOISTWAY_SCAN__FPM;
      }
      Auto_SetSpeed(ucSpeedLimit);
   }

   /* If the door command is not possible, adjust the command  */
   if( eDoorCommand == RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR )
   {
      if(GetFloorOpening_Front(ucRecallFloor))
      {
         eDoorCommand = RECALL_DOOR_COMMAND__OPEN_FRONT_DOOR;
      }
      else if(GetFloorOpening_Rear(ucRecallFloor))
      {
         eDoorCommand = RECALL_DOOR_COMMAND__OPEN_REAR_DOOR;
      }
   }
   else if( eDoorCommand == RECALL_DOOR_COMMAND__OPEN_BOTH_DOORS )
   {
      uint8_t bOpening_F = GetFloorOpening_Front(ucRecallFloor);
      uint8_t bOpening_R = GetFloorOpening_Rear(ucRecallFloor);
      if( bOpening_F && !bOpening_R )
      {
         eDoorCommand = RECALL_DOOR_COMMAND__OPEN_FRONT_DOOR;
      }
      else if( !bOpening_F && bOpening_R )
      {
         eDoorCommand = RECALL_DOOR_COMMAND__OPEN_REAR_DOOR;
      }
   }

   if ( bInitMode )
   {
      ResetRecallState();
   }

   if( GetFault_ByRange(FLT__NONE+1, NUM_FAULTS-1) )
   {
      /* To prevent entrapment when attempting a recall while car is faulted */
      if( ( !GetMotion_RunFlag() )
       && ( getDoorZone(DOOR_ANY) )  )
      {
         SetInDestinationDoorzone(1);

         /* Prevents the bRecallFinished flag from toggling when a fault is asserted while fully recalled */
         for (enum en_doors ucDoorIndex = DOOR_FRONT; ucDoorIndex < ucNumberOfDoors; ucDoorIndex++)
         {
            if ( ucRecallFloor == GetOperation_CurrentFloor() )
            {
               /* If fire phase 2 is active, don't automatically open/close doors after reaching the recall floor. Wait for constant pressure command */
               if( Fire_CheckIfFire2DoorsEnabled() )
               {
                  Auto_CheckDoorRequests();
               }
               else
               {
                  if( eDoorCommand == RECALL_DOOR_COMMAND__OPEN_NEITHER_DOOR )
                  {
                     abRecallFinished[DOOR_FRONT] = 1;
                     abRecallFinished[DOOR_REAR] = 1;
                     enRecallStates = RECALL__RECALL_FINISHED;
                  }
                  else if( eDoorCommand == RECALL_DOOR_COMMAND__OPEN_BOTH_DOORS )
                  {
                     if( ( ucDoorIndex == DOOR_FRONT )
                      && ( ( GetDoorState(ucDoorIndex) == DOOR__OPEN ) || ( ( GetFP_DoorType(ucDoorIndex) == DOOR_TYPE__MANUAL ) && GetDoorState(ucDoorIndex) == DOOR__CLOSED) ) )
                     {
                        abRecallFinished[ucDoorIndex] = 1;
                        enRecallStates = RECALL__RECALL_FINISHED;
                        SetDoorCommand( ucDoorIndex, DOOR_COMMAND__OPEN_IN_CAR_REQUEST );
                     }
                     else if( ( ucDoorIndex == DOOR_REAR )
                           && ( ( GetDoorState(ucDoorIndex) == DOOR__OPEN ) || ( ( GetFP_DoorType(ucDoorIndex) == DOOR_TYPE__MANUAL ) && GetDoorState(ucDoorIndex) == DOOR__CLOSED) ) )
                     {
                        abRecallFinished[ucDoorIndex] = 1;
                        enRecallStates = RECALL__RECALL_FINISHED;
                        SetDoorCommand( ucDoorIndex, DOOR_COMMAND__OPEN_IN_CAR_REQUEST );
                     }
                  }
                  else if( eDoorCommand == RECALL_DOOR_COMMAND__OPEN_FRONT_DOOR )
                  {
                     if( ( ucDoorIndex == DOOR_FRONT )
                      && ( ( GetDoorState(ucDoorIndex) == DOOR__OPEN ) || ( ( GetFP_DoorType(ucDoorIndex) == DOOR_TYPE__MANUAL ) && GetDoorState(ucDoorIndex) == DOOR__CLOSED) ) )
                     {
                        abRecallFinished[DOOR_FRONT] = 1;
                        abRecallFinished[DOOR_REAR] = 1;
                        enRecallStates = RECALL__RECALL_FINISHED;
                        SetDoorCommand( ucDoorIndex, DOOR_COMMAND__OPEN_IN_CAR_REQUEST );
                     }
                  }
                  else if( eDoorCommand == RECALL_DOOR_COMMAND__OPEN_REAR_DOOR )
                  {
                     if( ( ucDoorIndex == DOOR_REAR )
                      && ( ( GetDoorState(ucDoorIndex) == DOOR__OPEN ) || ( ( GetFP_DoorType(ucDoorIndex) == DOOR_TYPE__MANUAL ) && GetDoorState(ucDoorIndex) == DOOR__CLOSED) ) )
                     {
                        abRecallFinished[DOOR_FRONT] = 1;
                        abRecallFinished[DOOR_REAR] = 1;
                        enRecallStates = RECALL__RECALL_FINISHED;
                        SetDoorCommand( ucDoorIndex, DOOR_COMMAND__OPEN_IN_CAR_REQUEST );
                     }
                  }
               }
            }
         }
         /* If fire phase 2 is active, don't automatically open/close doors after reaching the recall floor. Wait for constant pressure command */
         if( !Fire_CheckIfFire2DoorsEnabled() && ( !abRecallFinished[DOOR_FRONT] || !abRecallFinished[DOOR_REAR] ) )
         {
            if( uwCarStuckTimer_50ms >= CAR_STUCK_TIMEOUT_50MS )
            {
               Doors_HoldDoorOpen(DOOR_ANY);
            }
            else
            {
               uwCarStuckTimer_50ms++;
            }
         }
         else
         {
            uwCarStuckTimer_50ms = 0;
         }
      }
      else
      {
         SetInDestinationDoorzone(0);
         uwCarStuckTimer_50ms = 0;
      }
   }
   else
   {
      uwCarStuckTimer_50ms = 0;
      uwDoorNudgeTimeout++;

      switch (enRecallStates)
      {
         case RECALL__RECALL_FINISHED:
         case RECALL__STOPPED:
               {
               uint8_t bAllowDoorOpen = 0;
               for (enum en_doors ucDoorIndex = DOOR_FRONT; ucDoorIndex < ucNumberOfDoors; ucDoorIndex++)
               {
                  if ( ucRecallFloor == GetOperation_CurrentFloor() )
                  {
                     /* If fire phase 2 is active, don't automatically open/close doors after reaching the recall floor. Wait for constant pressure command */
                     if( Fire_CheckIfFire2DoorsEnabled() )
                     {
                        uint8_t bInDoorZone = getDoorZone(DOOR_ANY);
                        uint8_t bInDeadZone = InsideDeadZone();
                        if( bInDoorZone )
                        {
                           bAllowDoorOpen = 1;
                           SetInDestinationDoorzone(1);
                           Auto_CheckDoorRequests();
                           if( bInDeadZone )
                           {
                              abRecallFinished[ucDoorIndex] = 1;
                              enRecallStates = RECALL__RECALL_FINISHED;
                              uwDoorNudgeTimeout = 0;
                           }
                        }
                        else if( bInDeadZone )
                        {
                           SetFault(FLT__AT_FLOOR_NO_DZ);
                           SetInDestinationDoorzone(0);
                        }
                        else
                        {
                           SetInDestinationDoorzone(0);
                           Auto_CheckDoorRequests();
                           SetDestination(GetOperation_CurrentFloor());
                        }
                     }
                     else
                     {
                        bAllowDoorOpen = InsideDeadZone() && getDoorZone(ucDoorIndex) && !GetMotion_RunFlag();
                        if( bAllowDoorOpen )
                        {
                           /* Added exception for parking with doors open option.
                            * To perform this operation AUTO_Idle, InDestinationDoorzone() must be manipulated.
                            * To be cleaned up later. */
                           if( IdleTime_GetParkingDoorCommand()
                            && !GetMotion_RunFlag()
                            && ( GetOperation_CurrentFloor() == GetOperation_DestinationFloor() ) )
                           {
                              SetInDestinationDoorzone(1);
                           }

                           if( eDoorCommand == RECALL_DOOR_COMMAND__OPEN_NEITHER_DOOR )
                           {
                              abRecallFinished[DOOR_FRONT] = 1;
                              abRecallFinished[DOOR_REAR] = 1;
                              enRecallStates = RECALL__RECALL_FINISHED;
                              CloseDoor ( DOOR_FRONT,  uwDoorNudgeTimeout );
                              CloseDoor ( DOOR_REAR,  uwDoorNudgeTimeout );
                           }
                           else if( eDoorCommand == RECALL_DOOR_COMMAND__OPEN_BOTH_DOORS )
                           {
                              SetDoorCommand( ucDoorIndex, DOOR_COMMAND__OPEN_IN_CAR_REQUEST );

                              if( ( ucDoorIndex == DOOR_FRONT )
                               && ( ( GetDoorState(ucDoorIndex) == DOOR__OPEN ) || ( ( GetFP_DoorType(ucDoorIndex) == DOOR_TYPE__MANUAL ) && GetDoorState(ucDoorIndex) == DOOR__CLOSED) ) )
                              {
                                 abRecallFinished[ucDoorIndex] = 1;
                                 enRecallStates = RECALL__RECALL_FINISHED;
                                 uwDoorNudgeTimeout = 0;
                              }
                              else if( ( ucDoorIndex == DOOR_REAR )
                                    && ( ( GetDoorState(ucDoorIndex) == DOOR__OPEN ) || ( ( GetFP_DoorType(ucDoorIndex) == DOOR_TYPE__MANUAL ) && GetDoorState(ucDoorIndex) == DOOR__CLOSED) ) )
                              {
                                 abRecallFinished[ucDoorIndex] = 1;
                                 enRecallStates = RECALL__RECALL_FINISHED;
                                 uwDoorNudgeTimeout = 0;
                              }
                           }
                           else if( eDoorCommand == RECALL_DOOR_COMMAND__OPEN_FRONT_DOOR )
                           {
                              if( ucDoorIndex == DOOR_FRONT )
                              {
                                 SetDoorCommand( DOOR_FRONT, DOOR_COMMAND__OPEN_IN_CAR_REQUEST );
                                 CloseDoor ( DOOR_REAR,  uwDoorNudgeTimeout );
                                 if( ( GetDoorState(ucDoorIndex) == DOOR__OPEN ) || ( ( GetFP_DoorType(ucDoorIndex) == DOOR_TYPE__MANUAL ) && GetDoorState(ucDoorIndex) == DOOR__CLOSED) )
                                 {
                                    abRecallFinished[DOOR_FRONT] = 1;
                                    abRecallFinished[DOOR_REAR] = 1;
                                    enRecallStates = RECALL__RECALL_FINISHED;
                                 }
                              }
                           }
                           else if( eDoorCommand == RECALL_DOOR_COMMAND__OPEN_REAR_DOOR )
                           {
                              if( ucDoorIndex == DOOR_REAR )
                              {
                                 SetDoorCommand( DOOR_REAR, DOOR_COMMAND__OPEN_IN_CAR_REQUEST );
                                 CloseDoor ( DOOR_FRONT,  uwDoorNudgeTimeout );
                                 if( ( GetDoorState(ucDoorIndex) == DOOR__OPEN ) || ( ( GetFP_DoorType(ucDoorIndex) == DOOR_TYPE__MANUAL ) && GetDoorState(ucDoorIndex) == DOOR__CLOSED) )
                                 {
                                    abRecallFinished[DOOR_FRONT] = 1;
                                    abRecallFinished[DOOR_REAR] = 1;
                                    enRecallStates = RECALL__RECALL_FINISHED;
                                 }
                              }
                           }
                           else
                           {
                              //should not get here
                           }
                        }
                        else if ( !getDoorZone( ucDoorIndex ) && InsideDeadZone())
                        {
                           SetInDestinationDoorzone(0);
                           CloseDoor ( ucDoorIndex,  uwDoorNudgeTimeout );
                           SetFault(FLT__AT_FLOOR_NO_DZ);
                        }
                        else if ( !getDoorZone( ucDoorIndex ) || !InsideDeadZone())
                        {
                           //Fault!
                           SetInDestinationDoorzone(0);
                           CloseDoor ( ucDoorIndex,  uwDoorNudgeTimeout );
                           SetDestination(GetOperation_CurrentFloor());
                        }
                     }
                  }
                  else if ( DoorsSafeAndReadyToRun() )
                  {
                     SetInDestinationDoorzone ( 0 );
                     SetDestination(ucRecallFloor);
                     enRecallStates = RECALL__MOVING;
                     ucMovementTimeout_50ms = 0;
                     break;
                  }
                  else
                  {
                     enRecallStates = RECALL__STOPPED;
                     /* If fire phase 2 is active, don't automatically open/close doors after reaching the recall floor. Wait for constant pressure command */
                     if( Fire_CheckIfFire2DoorsEnabled() )
                     {
                        bAllowDoorOpen = 1;
                        SetInDestinationDoorzone ( bAllowDoorOpen );
                        Auto_CheckDoorRequests();
                     }
                     else
                     {
                        if (GetDoorOpenButton ( ucDoorIndex ))
                        {
                           bAllowDoorOpen = 1;
                           SetDoorCommand( ucDoorIndex, DOOR_COMMAND__OPEN_UI_REQUEST );
                        }
                        else
                        {
                           CloseDoor ( ucDoorIndex,  uwDoorNudgeTimeout );
                        }
                     }
                  }
               } //end for
               SetInDestinationDoorzone ( bAllowDoorOpen );
            }
            break;

         case RECALL__MOVING:
            SetInDestinationDoorzone ( 0 );
            if ( GetMotion_RunFlag() )
            {
               if( ( ( GetMotion_Direction() == DIR__DN ) && ( ucRecallFloor <= GetOperation_CurrentFloor() ) )
                || ( ( GetMotion_Direction() == DIR__UP ) && ( ucRecallFloor >= GetOperation_CurrentFloor() ) ) )
               {
                  SetDestination(ucRecallFloor);
               }
               else
               {
                  //Stop Next DZ or instant depending on option;
                  if ( (GetMotion_Direction() != DIR__NONE) // Added exception to prevent ESTOP at end of recall run
                    && (gstCurrentModeRules[ GetOperation_AutoMode() ].bAllowedOutsideDoorZone) )
                  {
                     SetOperation_MotionCmd( MOCMD__EMERG_STOP );
                     if(Param_ReadValue_1Bit(enPARAM1__EnableEStopAlarms))
                     {
                        SetAlarm(ALM__ESTOP_RECALL_DEST);
                     }
                  }
                  else
                  {
                     StopNextAvailableLanding();
                     SetOperation_MotionCmd( MOCMD__NORMAL_STOP );
                  }
               }
               ucMovementTimeout_50ms = 101;
            }
            else if (ucMovementTimeout_50ms > 100)
            {
               enRecallStates = RECALL__STOPPED; //TODO: Error, could not start a run in time?
            }
            else
            {
               ucMovementTimeout_50ms++;
            }
            break;

         default:
            if (GetMotion_RunFlag())
            {
               //set state Moving;
               enRecallStates = RECALL__MOVING;
            }
            else
            {
               //set state Stopped;
               enRecallStates = RECALL__STOPPED;
            }
            break;
      }

      CheckFor_InvalidRecallDestinationAlarm( ucRecallFloor, eDoorCommand );
   }

   return abRecallFinished[DOOR_FRONT] && abRecallFinished[DOOR_REAR];
}
