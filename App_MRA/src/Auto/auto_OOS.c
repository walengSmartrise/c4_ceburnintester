/*
 * auto_OOS.c
 *
 *  Created on: Jul 21, 2017
 *      Author: ChengS
 */

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_a.h"
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t ucOOS_Fault;
static enum enOOSSubFaults eOOS_SubfaultNum;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
void SetOOSFault(enum enOOSSubFaults eOOSFault)
{
   if(eOOS_SubfaultNum != enOOS_ConsecutiveLimit)
   {
      eOOS_SubfaultNum = eOOSFault;
   }
   ucOOS_Fault = 1;
}
/*----------------------------------------------------------------------------
   Consecutive fault limit OOS should not reset every hour
   If moving to inspection this rule will be bypassed
 *----------------------------------------------------------------------------*/
void UnsetOOSFault(uint8_t bInspection)
{
   if( ( eOOS_SubfaultNum != enOOS_ConsecutiveLimit )
    || ( bInspection ) )
   {
      eOOS_SubfaultNum = enOOS_NONE;
      ucOOS_Fault = 0;
   }
}
uint8_t GetOOSFault(void)
{
   return ucOOS_Fault;
}
enum enOOSSubFaults GetOOSSubFault(void)
{
   return eOOS_SubfaultNum;
}

/*-----------------------------------------------------------------------------

   Called from Oper_Auto

 -----------------------------------------------------------------------------*/
void Auto_OOS( uint8_t bInit )
{
   uint8_t ucRecallFloor = GetClosestValidOpening( GetMotion_Direction(), DOOR_ANY );

   ClearLatchedCarCalls();
   ClearLatchedHallCalls();

   Auto_SetSpeed(GetFP_ContractSpeed());

   if( Auto_Recall(bInit, ucRecallFloor, RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR) )
   {
      // Done with Car Recall
      // If we are in OOS mode from Faulting then set fault when done with recall.
      if(GetOOSFault())
      {
         SetFault(FLT__OOS_LIMIT);
      }
   }
}


/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
