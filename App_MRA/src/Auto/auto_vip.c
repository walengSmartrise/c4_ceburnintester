/******************************************************************************
 *
 * @file     auto_vip.c
 * @brief    Logic that controls how VIP operations work.
 * @version  V1.00
 * @date     15, January 2019
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include <string.h>
#include <stdint.h>

#include "sru_a.h"
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define VIP_STUCK_EXIT_TIMEOUT (15*17)//maybe can be made into a parameter?
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static en_vip_states eSwingState = VIP_STATE__INACTIVE;
static uint8_t bVIP_CarReady;
static uint8_t bVIP_CarCapture;
static uint16_t uwVIPIdleLimit_50ms;
static uint16_t uwCaptureToVIPIdleLimit_50ms;
static uint16_t uwVIPIdleTimer_50ms = 0xFFFF;
static uint16_t uwStateTransitionTimeout_50ms;
static uint8_t ucLastValidVIPFloor_Plus1;
static uint8_t ucAcceptingHC_DO_Timer_50ms;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
uint8_t VIP_GetCarReadyFlag()
{
   return bVIP_CarReady;
}

uint8_t VIP_GetCarCaptureFlag()
{
   return bVIP_CarCapture;
}
/*-----------------------------------------------------------------------------
   Returns 1 if active
 -----------------------------------------------------------------------------*/
uint8_t VIP_GetSwingService()
{
   uint8_t bActive = 0;

   uwVIPIdleLimit_50ms = Param_ReadValue_8Bit( enPARAM8__VIP_CarCall_Timer_1s ) * 17;
   uwCaptureToVIPIdleLimit_50ms = Param_ReadValue_8Bit( enPARAM8__VIP_Idle_Time_1s ) * 17;

   uint8_t bAnyDoorOpen = ( GetDoorState(DOOR_FRONT) == DOOR__OPEN ) || ( GetDoorState(DOOR_FRONT) == DOOR__OPENING ) || ( GetDoorState(DOOR_FRONT) == DOOR__CLOSING );
   bAnyDoorOpen |= ( GetDoorState(DOOR_REAR) == DOOR__OPEN ) || ( GetDoorState(DOOR_REAR) == DOOR__OPENING ) || ( GetDoorState(DOOR_REAR) == DOOR__CLOSING );
   if( GetVIP_FloorPlus1_MRB() != 0 )
   {
      bActive = 1;
      ucLastValidVIPFloor_Plus1 = GetVIP_FloorPlus1_MRB();
   }
   else if( ( VIP_GetAutoSwingState() == VIP_STATE__ACCEPTING_CC )
         || ( VIP_GetAutoSwingState() == VIP_STATE__WAIT_FOR_IDLE ) )
   {
      bActive = 1;
   }
   // Exception for race condition where VIP HC will clear before doors
   else if( ( VIP_GetAutoSwingState() == VIP_STATE__ACCEPTING_HC )
         && ( bAnyDoorOpen )
         && ( GetOperation_CurrentFloor() == ( ucLastValidVIPFloor_Plus1-1 ) ) )
   {
      bActive = 1;
   }

   return bActive;
}
/*-----------------------------------------------------------------------------
   Check sub state of swing mode
 -----------------------------------------------------------------------------*/
en_vip_states VIP_GetAutoSwingState(void)
{
   return eSwingState;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Capturing()
{
   bVIP_CarCapture = 1;
   bVIP_CarReady = 0;

   gstCurrentModeRules[MODE_A__VIP_MODE].bIgnoreCarCall_F = 0;
   gstCurrentModeRules[MODE_A__VIP_MODE].bIgnoreCarCall_R = 0;

   if( !GetVIP_FloorPlus1_MRB() )
   {
      eSwingState = VIP_STATE__INACTIVE;
      uwStateTransitionTimeout_50ms = 0;
      uwVIPIdleTimer_50ms = 0;
   }
   // Bypass idle delay if starting capture with an already idle car
   else if( ( IdleTime_GetParkingTime_100MS()*2 ) > uwCaptureToVIPIdleLimit_50ms )
   {
      eSwingState = VIP_STATE__ACCEPTING_HC;
      uwStateTransitionTimeout_50ms = 0;
      uwVIPIdleTimer_50ms = 0;
   }
   else if( GetNextDestination_AnyDirection() == INVALID_FLOOR && !GetMotion_RunFlag() && CarDoorsClosed() )
   {
      if( uwVIPIdleTimer_50ms >= uwCaptureToVIPIdleLimit_50ms )
      {
         eSwingState = VIP_STATE__ACCEPTING_HC;
      }
      else
      {
         uwVIPIdleTimer_50ms++;
      }
      uwStateTransitionTimeout_50ms = 0;
   }
   else if( !GetMotion_RunFlag() && CarDoorsClosed() && ( GetNextDestination_AnyDirection() != INVALID_FLOOR ) && !gstFault.bActiveFault )
   {
      uwVIPIdleTimer_50ms = 0;
      if(++uwStateTransitionTimeout_50ms > VIP_STUCK_EXIT_TIMEOUT )
      {
         eSwingState = VIP_STATE__INACTIVE;
         uwStateTransitionTimeout_50ms = 0;
         if(Param_ReadValue_1Bit(enPARAM1__EnableVIPTimeoutAlarm))
         {
            SetAlarm(ALM__VIP_TIMEOUT);
         }
      }
   }
//   else if( ( GetNextDestination_AnyDirection() != INVALID_FLOOR ) && !GetMotion_RunFlag() )
//   {
//      uwVIPIdleTimer_50ms = 0;
//      if(++uwStateTransitionTimeout_50ms > VIP_STUCK_EXIT_TIMEOUT )
//      {
//         eSwingState = VIP_STATE__INACTIVE;
//         uwStateTransitionTimeout_50ms = 0;
//      }
//   }
   else
   {
      uwVIPIdleTimer_50ms = 0;
      uwStateTransitionTimeout_50ms = 0;
      uwVIPIdleTimer_50ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Accepting_VIP_HC()
{
   bVIP_CarCapture = 0;
   bVIP_CarReady = 1;

   gstCurrentModeRules[MODE_A__VIP_MODE].bIgnoreCarCall_F = 1;
   gstCurrentModeRules[MODE_A__VIP_MODE].bIgnoreCarCall_R = 1;
   uint8_t bAnyDoorOpen = ( GetDoorState(DOOR_FRONT) == DOOR__OPEN ) || ( GetDoorState(DOOR_FRONT) == DOOR__CLOSING );
   bAnyDoorOpen |= ( GetDoorState(DOOR_REAR) == DOOR__OPEN ) || ( GetDoorState(DOOR_REAR) == DOOR__CLOSING );
   // abort if assignment lost
   // At assigned VIP floor with doors open
   if( ( bAnyDoorOpen )
    && ( GetOperation_CurrentFloor() == ( ucLastValidVIPFloor_Plus1-1 ) ) )
   {
      if( ++ucAcceptingHC_DO_Timer_50ms >= Param_ReadValue_8Bit(enPARAM8__VIP_HC_TransitionDelay_50ms) )
      {
         eSwingState = VIP_STATE__ACCEPTING_CC;
         uwStateTransitionTimeout_50ms = 0;
         ucAcceptingHC_DO_Timer_50ms = 0;
      }
   }
   else if( !GetMotion_RunFlag() && CarDoorsClosed() && !gstFault.bActiveFault )
   {
      uwVIPIdleTimer_50ms = 0;
      ucAcceptingHC_DO_Timer_50ms = 0;
      if(++uwStateTransitionTimeout_50ms > VIP_STUCK_EXIT_TIMEOUT )
      {
         eSwingState = VIP_STATE__INACTIVE;
         uwStateTransitionTimeout_50ms = 0;
         if(Param_ReadValue_1Bit(enPARAM1__EnableVIPTimeoutAlarm))
         {
            SetAlarm(ALM__VIP_TIMEOUT);
         }
      }
   }
//   else if( ( GetNextDestination_AnyDirection() != INVALID_FLOOR ) && !GetMotion_RunFlag() )
//   {
//      uwStateTransitionTimeout_50ms++;
//      if(uwStateTransitionTimeout_50ms > VIP_STUCK_EXIT_TIMEOUT )
//      {
//         eSwingState = VIP_STATE__INACTIVE;
//         uwStateTransitionTimeout_50ms = 0;
//      }
//   }
   else
   {
      ucAcceptingHC_DO_Timer_50ms = 0;
      uwStateTransitionTimeout_50ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Accepting_VIP_CC()
{
   bVIP_CarCapture = 0;
   bVIP_CarReady = 0;

   gstCurrentModeRules[MODE_A__VIP_MODE].bIgnoreCarCall_F = 0;
   gstCurrentModeRules[MODE_A__VIP_MODE].bIgnoreCarCall_R = 0;

   if( ++uwStateTransitionTimeout_50ms >= uwVIPIdleLimit_50ms )
   {
      uwStateTransitionTimeout_50ms = 0;
      if( GetNextDestination_AnyDirection() != INVALID_FLOOR )
      {
         eSwingState = VIP_STATE__WAIT_FOR_IDLE;
      }
      else
      {
         eSwingState = VIP_STATE__INACTIVE;
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Wait_For_Idle()
{
   bVIP_CarCapture = 0;
   bVIP_CarReady = 0;

   gstCurrentModeRules[MODE_A__VIP_MODE].bIgnoreCarCall_F = 1;
   gstCurrentModeRules[MODE_A__VIP_MODE].bIgnoreCarCall_R = 1;
   if( ( GetNextDestination_AnyDirection() == INVALID_FLOOR ) && Doors_GetDwellCompleteFlag(DOOR_ANY) )
   {
      eSwingState = VIP_STATE__INACTIVE;
      uwStateTransitionTimeout_50ms = 0;
   }
   else if( !GetMotion_RunFlag() && CarDoorsClosed() && !gstFault.bActiveFault )
   {
      uwVIPIdleTimer_50ms = 0;
      if(++uwStateTransitionTimeout_50ms > VIP_STUCK_EXIT_TIMEOUT )
      {
         eSwingState = VIP_STATE__INACTIVE;
         uwStateTransitionTimeout_50ms = 0;
         if(Param_ReadValue_1Bit(enPARAM1__EnableVIPTimeoutAlarm))
         {
            SetAlarm(ALM__VIP_TIMEOUT);
         }
      }
   }
//   else if( ( GetNextDestination_AnyDirection() != INVALID_FLOOR ) && !GetMotion_RunFlag() )
//   {
//      uwStateTransitionTimeout_50ms++;
//      if(uwStateTransitionTimeout_50ms > VIP_STUCK_EXIT_TIMEOUT )
//      {
//         eSwingState = VIP_STATE__INACTIVE;
//         uwStateTransitionTimeout_50ms = 0;
//      }
//   }
   else
   {
      uwStateTransitionTimeout_50ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void Auto_VIP_Mode( uint8_t bInit )
{
   if( bInit )
   {
      uwVIPIdleTimer_50ms = 0;
      eSwingState = VIP_STATE__CAPTURING;
      gstCurrentModeRules[MODE_A__VIP_MODE].bIgnoreCarCall_F = 0;
      gstCurrentModeRules[MODE_A__VIP_MODE].bIgnoreCarCall_R = 0;
      uwStateTransitionTimeout_50ms = 0;
      ucAcceptingHC_DO_Timer_50ms = 0;
   }

   Auto_SetSpeed(GetFP_ContractSpeed());
   Auto_Set_Limits();

   switch( eSwingState )
   {
      case VIP_STATE__CAPTURING:
         Capturing();
         break;
      case VIP_STATE__ACCEPTING_HC:
         Accepting_VIP_HC();
         break;
      case VIP_STATE__ACCEPTING_CC:
         Accepting_VIP_CC();
         break;
      case VIP_STATE__WAIT_FOR_IDLE:
         Wait_For_Idle();
         break;
      default:
         eSwingState = VIP_STATE__CAPTURING;
         break;
   }

   Auto_Oper( 0 );
}
