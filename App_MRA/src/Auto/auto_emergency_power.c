/******************************************************************************
 *
 * @file     auto_emergency_power.c
 * @brief    .
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "GlobalData.h"
#include <stdint.h>
#include <string.h>
#include "sru_a.h"
#include "sys.h"
#include "sru.h"
#include "sru_a.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static enum enEmergencyPowerCommands enLastCommand = EPC__OFF;
static enum enEmergencyPowerCommands enCurrentCommand = EPC__OFF;
static uint8_t bCarRecalled;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
uint8_t EPower_GetCarRecalledFlag(void)
{
   return bCarRecalled;
}
void EPower_ResetCarRecalledFlag(void)
{
   bCarRecalled = 0;
}
/*-----------------------------------------------------------------------------
   Returns 1 if other modes of operation should be supressed
   because Epower operation is limiting the number of cars
   that can run
 -----------------------------------------------------------------------------*/
uint8_t GetEmergencyPowerLockout(void)
{
   uint8_t bLockout = 0;
   enum enEmergencyPowerCommands eCommand = GetEmergencyPowerCommand();
   if( ( eCommand )
    && ( eCommand != EPC__RUN_AUTO ) )
   {
      bLockout = 1;
   }
   return bLockout;
}
/*-----------------------------------------------------------------------------

   Called from Oper_Auto

 -----------------------------------------------------------------------------*/
void Auto_Emergency_Power ( uint8_t bInit )
{
   static uint8_t ucRecallFloor = INVALID_FLOOR;
   if (bInit) {
      //initialization setup
      enLastCommand = EPC__OFF;
      enCurrentCommand = EPC__OFF;
      ucRecallFloor = INVALID_FLOOR;
   }
   Auto_Set_Limits();

   enCurrentCommand = GetEmergencyPowerCommand();
   switch ( enCurrentCommand ) {

      case EPC__RECALL:
      {
         ClearLatchedCarCalls();
         ClearLatchedHallCalls();

         gstCurrentModeRules[ MODE_A__EPOWER ].bIgnoreCarCall_F = 1;
         gstCurrentModeRules[ MODE_A__EPOWER ].bIgnoreCarCall_R = 1;
         gstCurrentModeRules[ MODE_A__EPOWER ].bIgnoreHallCall = 1;

         ucRecallFloor = Param_ReadValue_8Bit( enPARAM8__FireRecallFloorM );
         en_recall_door_command eRecallDoorCommand = ( !Param_ReadValue_1Bit(enPARAM1__FireRecallDoorM) ) ? RECALL_DOOR_COMMAND__OPEN_FRONT_DOOR:RECALL_DOOR_COMMAND__OPEN_REAR_DOOR;
         //recall to designated recall opening
         if( Auto_Recall ( (enLastCommand != enCurrentCommand), ucRecallFloor, eRecallDoorCommand ) )
         {
            bCarRecalled = 1;
         }
         break;
      }

      case EPC__PRETRANSFER:
      {
         ClearLatchedCarCalls();
         ClearLatchedHallCalls();

         gstCurrentModeRules[ MODE_A__EPOWER ].bIgnoreCarCall_F = 1;
         gstCurrentModeRules[ MODE_A__EPOWER ].bIgnoreCarCall_R = 1;
         gstCurrentModeRules[ MODE_A__EPOWER ].bIgnoreHallCall = 1;

         if( ( enLastCommand != enCurrentCommand )
          || ( ucRecallFloor == INVALID_FLOOR ) )
         {
            ucRecallFloor = GetClosestValidOpening(GetMotion_Direction(), DOOR_ANY);
         }

         /* Prevent movement by asserting the OOS fault if configured to not allow car movement */
         if( Param_ReadValue_1Bit(enPARAM1__EPowerPretransferStall) )
         {
            SetFault(FLT__EPOWER);
         }

         Auto_Recall ( (enLastCommand != enCurrentCommand), ucRecallFloor, RECALL_DOOR_COMMAND__OPEN_EITHER_DOOR );
         break;
      }

      default:
      case EPC__RUN_AUTO: // Should not reach this state. This mode is now handled outside of Epower.
      case EPC__OOS:
         ucRecallFloor = INVALID_FLOOR;
         //TODO: check for opening doors when at a floor
//          Auto_OOS(bInit);
         //run car on automatic operation
         gstCurrentModeRules[ MODE_A__EPOWER ].bIgnoreCarCall_F = 1;
         gstCurrentModeRules[ MODE_A__EPOWER ].bIgnoreCarCall_R = 1;
         gstCurrentModeRules[ MODE_A__EPOWER ].bIgnoreHallCall = 1;
         SetFault(FLT__EPOWER);
         break;
   }
   enLastCommand = enCurrentCommand;

}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
