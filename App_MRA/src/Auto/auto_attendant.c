/******************************************************************************
 *
 * @file     mod_heartbeat.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_a.h"
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define HALL_CALL_BYPASS_HOLD_TIME_MS     (10000)
#define BUZZER_RESET_TIME_MS              (10000)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint16_t uwHallCallBypassTimer_ms = HALL_CALL_BYPASS_HOLD_TIME_MS+1;
static uint8_t bHallCallLamp_Above;
static uint8_t bHallCallLamp_Below;
static uint8_t bHallCallBypass;

static uint8_t bArmDirectionButtons;
static enum direction_enum eCommandedDir;
static uint8_t bBuzzer;

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t Attendant_GetHallCallBypass(void)
{
   return bHallCallBypass && ( GetOperation_AutoMode() == MODE_A__ATTENDANT );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t Attendant_GetHallCallLamp_Above(void)
{
   return bHallCallLamp_Above && ( GetOperation_AutoMode() == MODE_A__ATTENDANT );
}
uint8_t Attendant_GetHallCallLamp_Below(void)
{
   return bHallCallLamp_Below && ( GetOperation_AutoMode() == MODE_A__ATTENDANT );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
enum direction_enum Attendant_GetDirection()
{
   return eCommandedDir;
}
void Attendant_SetDirection(enum direction_enum eDir)
{
   eCommandedDir = eDir;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t Attendant_GetBuzzerFlag()
{
   return bBuzzer && ( GetOperation_AutoMode() == MODE_A__ATTENDANT );
}
/*-----------------------------------------------------------------------------
Input:
   0 = below
   1 = above
 -----------------------------------------------------------------------------*/
static uint8_t GetClosestHallCallFloor_Plus1( uint8_t bAbove )
{
   uint8_t ucFloor_Plus1 = 0;
   if( bAbove )
   {
      ucFloor_Plus1 = GetNearestHallCall_Up();
   }
   else
   {
      ucFloor_Plus1 = GetNearestHallCall_Down();
   }

   return ucFloor_Plus1;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateHallCallHandling(void)
{
   if( GetInputValue( enIN_ATTD_BYP ) )
   {
      uwHallCallBypassTimer_ms = 0;
   }

   /* Place a car call at least 2 landings out and a hall call in between the landings.
    * Turn the bypass input on and verify the car goes past the hall call to the car call. */
   if( uwHallCallBypassTimer_ms < HALL_CALL_BYPASS_HOLD_TIME_MS )
   {
      uwHallCallBypassTimer_ms += MOD_RUN_PERIOD_OPER_AUTO_1MS;
      bHallCallBypass = 1;
      ClearLatchedHallCalls();
   }
   else
   {
      bHallCallBypass = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateBuzzerFlag()
{
   static uint16_t uwBuzzerTimer_ms;
   uint32_t uiBuzzerHoldTime_ms = Param_ReadValue_8Bit( enPARAM8__ATTD_BuzzerTime_100ms ) * 100;
   uint8_t bSoundBuzzer = 0;
   uint8_t bAnyDoorOpen = ( ( GetDoorState(DOOR_FRONT) == DOOR__OPEN ) || ( GetDoorState(DOOR_REAR) == DOOR__OPEN ) );
   if( !bAnyDoorOpen )
   {
      uwBuzzerTimer_ms = 0;
      bBuzzer = 0;
   }
   else
   {
      if( bHallCallLamp_Above || bHallCallLamp_Below )
      {
         bSoundBuzzer = 1;
      }
      if( !bSoundBuzzer )
      {
         uwBuzzerTimer_ms = 0;
      }
      else if( uwBuzzerTimer_ms >= BUZZER_RESET_TIME_MS )
      {
         uwBuzzerTimer_ms = 1;
      }
      else
      {
         uwBuzzerTimer_ms += MOD_RUN_PERIOD_OPER_AUTO_1MS;
      }
      bBuzzer = uwBuzzerTimer_ms && ( uwBuzzerTimer_ms < uiBuzzerHoldTime_ms );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateCommandedDirection()
{
   //TODO add enPARAM1__ATTD_DirWithCCB support
   if( bArmDirectionButtons )
   {
      if( GetPosition_Velocity() < 5
       && GetPosition_Velocity() > -5  )
      {
         if( GetInputValue( enIN_ATTD_UP ) && !GetInputValue( enIN_ATTD_DN ) )
         {
            eCommandedDir = DIR__UP;
         }
         else if( !GetInputValue( enIN_ATTD_UP ) && GetInputValue( enIN_ATTD_DN ) )
         {
            eCommandedDir = DIR__DN;
         }
         else
         {
            eCommandedDir = DIR__NONE;
         }
      }
   }
   else if( !GetInputValue( enIN_ATTD_UP ) && !GetInputValue( enIN_ATTD_DN ) )
   {
      bArmDirectionButtons = 1;
   }

   /* Add automatic direction change at terminal floors */
   uint8_t ucCurrentFloorIndex = GetOperation_CurrentFloor();
   uint8_t ucTopFloorIndex = GetFP_NumFloors()-1;
   uint8_t ucBotFloorIndex = 0;
   if( ucCurrentFloorIndex == ucTopFloorIndex )
   {
      eCommandedDir = DIR__DN;
   }
   else if( ucCurrentFloorIndex == ucBotFloorIndex )
   {
      eCommandedDir = DIR__UP;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateLamps()
{
   /* Check if there is a hall call above */
   if( GetClosestHallCallFloor_Plus1(HC_DIR__UP)  != 0 )
   {
      bHallCallLamp_Above = 1;
   }
   else
   {
      bHallCallLamp_Above = 0;
   }
   /* Check if there is a hall call below */
   if( GetClosestHallCallFloor_Plus1(HC_DIR__DOWN)  != 0 )
   {
      bHallCallLamp_Below = 1;
   }
   else
   {
      bHallCallLamp_Below = 0;
   }
}
/*-----------------------------------------------------------------------------

   Called from Oper_Auto

 -----------------------------------------------------------------------------*/
void Auto_Attendant( uint8_t bInit )
{
   Auto_SetSpeed(GetFP_ContractSpeed());
   Auto_Set_Limits();

   if( bInit )
   {
      bArmDirectionButtons = 0;
      eCommandedDir = DIR__NONE;
      uwHallCallBypassTimer_ms = HALL_CALL_BYPASS_HOLD_TIME_MS + 1;
   }

   UpdateHallCallHandling();

   UpdateBuzzerFlag();

   UpdateLamps();

   /* Check for directions armed and valid */
   UpdateCommandedDirection();

   Auto_Oper( bInit );
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
