/******************************************************************************
 *
 * @file     auto_active_shooter.c
 * @brief    Logic that controls how active shooter operations work.
 * @version  V1.00
 * @date     15, December 2019
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include <string.h>
#include <stdint.h>

#include "sru_a.h"
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
void Auto_Active_Shooter_Mode( uint8_t bInit){

   ClearLatchedCarCalls();
   ClearLatchedHallCalls();

   Auto_SetSpeed(GetFP_ContractSpeed());
   Auto_Set_Limits();

   uint8_t ucRecallFloor = Param_ReadValue_8Bit(enPARAM8__FireRecallFloorA);

   if(ucRecallFloor < GetFP_NumFloors()){
      if(((GetOperation_CurrentFloor() != ucRecallFloor) || !InsideDeadZone()) || GetMotion_RunFlag())
      {
         SetInDestinationDoorzone(0);
         SetDoorCommand( DOOR_FRONT, DOOR_COMMAND__CLOSE);
         SetDoorCommand( DOOR_REAR, DOOR_COMMAND__CLOSE);
         SetDestination(ucRecallFloor);
      }
      else
      {
         SetInDestinationDoorzone(1);
         Auto_CheckDoorRequests();
      }
   }
   SetDoorCommand(DOOR_FRONT, DOOR_COMMAND__OPEN_HOLD_REQUEST);
}
