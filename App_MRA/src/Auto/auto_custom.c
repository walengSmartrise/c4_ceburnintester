/*
 * auto_custom.c
 *
 *  Created on: Jul 21, 2017
 *      Author: ChengS
 */



/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_a.h"
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

   Called from Oper_Auto

 -----------------------------------------------------------------------------*/
void Auto_InitCustomSettings(uint8_t bInit)
{
	gstCurrentModeRules[MODE_A__CUSTOM].bIgnoreCarCallSecurity = Param_ReadValue_1Bit(enPARAM1__CustomMode_IgnoreCarCallSecurity);
    gstCurrentModeRules[MODE_A__CUSTOM].bIgnoreHallCallSecurity = Param_ReadValue_1Bit(enPARAM1__CustomMode_IgnoreHallCallSecurity);
    gstCurrentModeRules[MODE_A__CUSTOM].bAllowedOutsideDoorZone = Param_ReadValue_1Bit(enPARAM1__CustomMode_AllowedOutsideDoorZone);
    gstCurrentModeRules[MODE_A__CUSTOM].bParkingEnabled = Param_ReadValue_1Bit(enPARAM1__CustomMode_ParkingEnabled);
    gstCurrentModeRules[MODE_A__CUSTOM].bIgnoreCarCall_F = Param_ReadValue_1Bit(enPARAM1__CustomMode_IgnoredCarCall_F);
    gstCurrentModeRules[MODE_A__CUSTOM].bIgnoreCarCall_R = Param_ReadValue_1Bit(enPARAM1__CustomMode_IgnoredCarCall_R);
    gstCurrentModeRules[MODE_A__CUSTOM].bIgnoreHallCall = Param_ReadValue_1Bit(enPARAM1__CustomMode_IgnoreHallCall);
    gstCurrentModeRules[MODE_A__CUSTOM].bAutoDoorOpen = Param_ReadValue_1Bit(enPARAM1__CustomMode_AutoDoorOpen);
    gstCurrentModeRules[MODE_A__CUSTOM].bDoorHold = Param_ReadValue_1Bit(enPARAM1__CustomMode_DoorHold);
    gstCurrentModeRules[MODE_A__CUSTOM].bIgnoreDCB = Param_ReadValue_1Bit(enPARAM1__CustomMode_IgnoreDCB);
    gstCurrentModeRules[MODE_A__CUSTOM].bForceDoorsOpenOrClosed = Param_ReadValue_1Bit(enPARAM1__CustomMode_ForceDoorsOpenOrClosed);
}


/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
