/******************************************************************************
 *
 * @file     auto_code_pink.c
 * @version  V1.00
 * @date     11 July 2016
 *
 * @note    When this mode is activated, car will be held at a floor with doors opened to prevent elevator usage.
 *          Used in cases where elevator use at floor such as the baby wing of a hospital needs to be locked down quickly.
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_a.h"
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

// indexIntoMask should be between 0 and 31
static uint8_t ValidFloorAndMask(uint32_t mask, uint8_t indexIntoMask)
{
    // This evaluates to 1 only if the floor has wander gaurd
    // otherwise it evaluates to 0
    return ((1<<indexIntoMask) & mask);
}

static uint8_t AtWanderGuardFloor( void )
{
    // Assume not at wander gaurd floor
    uint8_t result = 0;
    if( getDoorZone(DOOR_ANY) && !GetMotion_RunFlag() )
    {
       uint8_t currentFloor = GetOperation_CurrentFloor();
       // Between landings 0 to 31
       if( currentFloor <= 31 )
       {
           uint32_t mask = Param_ReadValue_32Bit(enPARAM32__WanderGuardMask0);

           // Check to see if the current floor has wandergaurd
           // To do so first subtract 0 from the current floor. The resulting
           // number is then used to check that the corresponding
           // bit is set in the mask. If the bit is set then this floor has wander guard.
           if(ValidFloorAndMask(mask, currentFloor))
           {
               result = 1;
           }
       }
       // Between landings 32 to 63
       else if( (currentFloor >= 32) && (currentFloor <= 63)  )
       {
           uint32_t mask = Param_ReadValue_32Bit(enPARAM32__WanderGuardMask1);
           if(ValidFloorAndMask(mask, currentFloor - 32))
           {
               result = 1;
           }
       }
       // Between landings 64 = 95
       else
       {
           uint32_t mask = Param_ReadValue_32Bit(enPARAM32__WanderGuardMask2);
           if(ValidFloorAndMask(mask, currentFloor - 64))
           {
               result = 1;
           }
       }
    }

    return result;
}

/*-----------------------------------------------------------------------------

   Called from Oper_Auto

 -----------------------------------------------------------------------------*/
void Auto_Code_Pink( uint8_t bInit )
{
   // Car is at a wander guard enabled floor and the wander guard input is active.
   // Idle with the doors open.
   if( AtWanderGuardFloor() )
   {
       Auto_OOS(bInit);
       gstCurrentModeRules[MODE_A__WANDERGUARD].bIgnoreHallCall = 1;
       gstCurrentModeRules[MODE_A__WANDERGUARD].bIgnoreCarCall_F = 1;
       gstCurrentModeRules[MODE_A__WANDERGUARD].bIgnoreCarCall_R = 1;
   }
   // Not at wander gaurd floor. system is in normal operation.
   else
   {
      Auto_Oper( bInit );
      gstCurrentModeRules[MODE_A__WANDERGUARD].bIgnoreHallCall = 0;
      gstCurrentModeRules[MODE_A__WANDERGUARD].bIgnoreCarCall_F = 0;
      gstCurrentModeRules[MODE_A__WANDERGUARD].bIgnoreCarCall_R = 0;
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
