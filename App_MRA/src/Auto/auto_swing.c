/******************************************************************************
 *
 * @file     mod_heartbeat.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_a.h"
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define IDLE_TIMER_MINIMUM_50MS     (200)
#define CAR_CAPTURE_DELAY_50MS      (200) // Matched with car capture timeout
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static en_swing_states eSwingState = SWING__UNKNOWN;
static uint8_t bInitiatedByKey = 0;//If mode initiated by key, don't wait for idle time to end swing
static uint16_t uwIdleTimer_50ms = 0xFFFF;

static uint16_t uwCaptureTimer_50ms;

static uint16_t uwIdleLimit_50ms;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Returns 1 if active
 -----------------------------------------------------------------------------*/
uint8_t GetSwingService()
{
   uint8_t bActive = 0;

   uwIdleLimit_50ms = Param_ReadValue_8Bit( enPARAM8__Swing_IdleTime_1s ) * 20;
   if( uwIdleLimit_50ms < IDLE_TIMER_MINIMUM_50MS )
   {
      uwIdleLimit_50ms = IDLE_TIMER_MINIMUM_50MS;
   }

   if( GetActiveSwingCallFlag_MRB()
    || GetInputValue( enIN_SWING_ENABLE_KEY ) )
   {
      bActive = 1;
   }
   else if( !bInitiatedByKey
       && ( uwIdleTimer_50ms < uwIdleLimit_50ms ) )
   {
      bActive = 1;
   }
   return bActive;
}
/*-----------------------------------------------------------------------------
   Check sub state of swing mode
 -----------------------------------------------------------------------------*/
en_swing_states GetAutoSwingState()
{
   return eSwingState;
}
/*-----------------------------------------------------------------------------
   Wait for calls to empty out and capture car
   When in this mode, car will stop accepting hall calls.
   //Note: Capture mode caused issues. duplicate here.
 -----------------------------------------------------------------------------*/
static void Swing_Capturing()
{
   /* If car has been idle for the capture timeout period, start taking swing calls */
   if( GetNextDestination_AnyDirection() == INVALID_FLOOR )
   {
      if( uwCaptureTimer_50ms >= CAR_CAPTURE_DELAY_50MS )
      {
         eSwingState = SWING__ON;
         uwIdleTimer_50ms = 0;
      }
      else
      {
         uwCaptureTimer_50ms++;
      }
   }
   else
   {
      uwCaptureTimer_50ms = 0;
   }
}
/*-----------------------------------------------------------------------------
      Stop taking group hall calls (if param bit is set)
   && Start take calls from swing riser
 -----------------------------------------------------------------------------*/
static void Swing_On()
{
   uint8_t ucDestination = GetNextDestination_AnyDirection();
   if( ( GetMotion_RunFlag() )
    || ( ucDestination != INVALID_FLOOR )
    || ( AnyDoorOpen() ) )
   {
      uwIdleTimer_50ms = 0;
   }
   else if( uwIdleTimer_50ms < uwIdleLimit_50ms )
   {
      uwIdleTimer_50ms++;
   }

   uwCaptureTimer_50ms = 0;
}
/*-----------------------------------------------------------------------------

   Called from Oper_Auto

 -----------------------------------------------------------------------------*/
void Auto_Swing( uint8_t bInit )
{
   Auto_SetSpeed(GetFP_ContractSpeed());
   Auto_Set_Limits();
   if(bInit)
   {
      uwCaptureTimer_50ms = 0;
      uwIdleTimer_50ms = 0xFFFF;
      eSwingState = SWING__CAPTURING;
   }

   bInitiatedByKey = GetInputValue( enIN_SWING_ENABLE_KEY );

   switch( eSwingState )
   {
      case SWING__CAPTURING:
         Swing_Capturing();
         break;
      case SWING__ON:
         Swing_On();
         break;
      default:
         eSwingState = SWING__CAPTURING;
         break;
   }

   Auto_Oper( 0 );
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
