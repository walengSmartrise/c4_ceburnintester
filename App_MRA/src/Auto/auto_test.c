/******************************************************************************
 *
 * @file     auto_test.c
 * @brief    Logic to control Test Mode Operation.
 * @version  V1.00
 * @date     25, October 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_a.h"
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/


uint8_t SetTestDestination ( uint8_t ucNewTestDestination )
{
   uint8_t bAccepted = 0;
   if (  GetOperation_AutoMode() == MODE_A__TEST
      && DoorsSafeAndReadyToRun()
      )
   {
      SetDestination(ucNewTestDestination);
      bAccepted = 1;
   }

   return bAccepted;
}

uint8_t SetTestCorrectionRunOn ( void )
{

   uint8_t bAccepted = 0;
   if ( GetOperation_AutoMode() == MODE_A__TEST )
   {
      bAccepted = 1;
      gstCurrentModeRules[ MODE_A__TEST ].bAllowedOutsideDoorZone = 0;
   }
   else
   {
      gstCurrentModeRules[ MODE_A__TEST ].bAllowedOutsideDoorZone = 1;
   }

   return bAccepted;
}

/*-----------------------------------------------------------------------------

   Called from Oper_Auto

 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
