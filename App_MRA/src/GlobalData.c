
#include "GlobalData.h"
#include <stdint.h>
#include <string.h>
#include "sru_a.h"
#include "sys.h"
#include "sru.h"
#include "sru_a.h"
#include "mod.h"


static struct st_position_b gstPostion_NTS;
static st_fault_data astFaultLog[NUM_FAULTLOG_ITEMS];

struct st_CarCallRequest
{
      uint8_t ucCounter;
      uint8_t ucFloorIndex;
      uint8_t ucDoorIndex;
};

#define MAX_CAR_CALL_REQUESTS (16)
static struct st_CarCallRequest stCarCallRequests[MAX_CAR_CALL_REQUESTS];

/*-----------------------------------------------------------------------------
   SRU
 -----------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------
   Parameters
 -----------------------------------------------------------------------------*/
static uint8_t gbFlashParamsRdy = 0;
static uint8_t gucFP_NumFloor = MIN_NUM_FLOORS;
static uint8_t gucFP_RearDoor = 0;
static en_door_type gaucFP_DoorType[NUM_OF_DOORS];
static uint8_t gucFP_FreightDoors = 0;
static uint16_t guwFP_ContractSpeed = MIN_CONTRACT_SPEED;

inline uint8_t GetFP_FlashParamsReady()
{
   return gbFlashParamsRdy;
}
// Only for mod_param_app
inline void SetFP_FlashParamsReady()
{
   gbFlashParamsRdy = 1;
}

inline uint8_t GetFP_NumFloors( void )
{
     return gucFP_NumFloor;
}

// Only for mod_param_app
inline void SetFP_NumFloors(uint8_t bFloor)
{
   if( bFloor >= MIN_NUM_FLOORS
    && bFloor <= MAX_NUM_FLOORS )
   {
      gucFP_NumFloor = bFloor;
   }
}
inline uint8_t GetFP_RearDoors( void )
{
     return gucFP_RearDoor;
}
// Only for mod_param_app
inline void SetFP_RearDoors(uint8_t bDoor)
{
   if(bDoor < 2)
   {
      gucFP_RearDoor = bDoor;
   }
}
inline en_door_type GetFP_DoorType( enum en_doors eDoor )
{
     return gaucFP_DoorType[eDoor];
}
inline void SetFP_DoorType( en_door_type enDoorType, enum en_doors eDoor )
{
   gaucFP_DoorType[eDoor] = enDoorType;
}
inline uint8_t GetFP_FreightDoors( void )
{
     return gucFP_FreightDoors;
}
inline void SetFP_FreightDoors( uint8_t bDoor )
{
     gucFP_FreightDoors = bDoor;
}

inline uint16_t GetFP_ContractSpeed( void )
{
     return guwFP_ContractSpeed;
}

// Only for mod_param_app
inline void SetFP_ContractSpeed( uint16_t uwSpeed )
{
   if( uwSpeed > MIN_CONTRACT_SPEED
    && uwSpeed <= MAX_CAR_SPEED)
   {
      guwFP_ContractSpeed = uwSpeed;
   }
}

/*-----------------------------------------------------------------------------
   I/O
 -----------------------------------------------------------------------------*/
static uint32_t gaulBitMapedInputs[ BITMAP32_SIZE ( NUM_INPUT_FUNCTIONS ) ];
static uint32_t gaulBitMapedOutputs[ BITMAP32_SIZE ( NUM_OUTPUT_FUNCTIONS ) ];
// ucIndex = index into bitmap
inline void SetInputBitMap(uint32_t ulBitmap, uint8_t ucIndex)
{
   if(ucIndex < BITMAP32_SIZE(NUM_INPUT_FUNCTIONS))
   {
      gaulBitMapedInputs[ucIndex] = ulBitmap;
   }
}
// ucIndex = index into bitmap
inline void SetOutputBitMap(uint32_t ulBitmap, uint8_t ucIndex)
{
   if(ucIndex < BITMAP32_SIZE(NUM_OUTPUT_FUNCTIONS))
   {
      gaulBitMapedOutputs[ucIndex] = ulBitmap;
   }
}
// ucIndex = index into bitmap
inline uint32_t GetInputBitMap(uint8_t ucIndex)
{
   uint32_t ulReturn = (ucIndex < BITMAP32_SIZE(NUM_INPUT_FUNCTIONS))
                     ? gaulBitMapedInputs[ucIndex]
                       : 0;
   return ulReturn;
}
// ucIndex = index into bitmap
inline uint32_t GetOutputBitMap(uint8_t ucIndex)
{
   uint32_t ulReturn = (ucIndex < BITMAP32_SIZE(NUM_OUTPUT_FUNCTIONS))
                     ? gaulBitMapedOutputs[ucIndex]
                       : 0;
   return ulReturn;
}


inline uint8_t GetOutputBitmapValue(enum en_output_functions enOutput)
{
   uint8_t ucReturn = (enOutput < NUM_OUTPUT_FUNCTIONS) ?
                           Sys_Bit_Get_Array32(gaulBitMapedOutputs,
                                                enOutput,
                                                BITMAP32_SIZE(NUM_OUTPUT_FUNCTIONS))
                           :0;
   return ucReturn;
}

/*-----------------------------------------------------------------------------
Position reference access
 -----------------------------------------------------------------------------*/
void SetNTSPosition(uint32_t ulPosition)
{
   gstPostion_NTS.ulPosCount = ulPosition;
}
uint32_t GetNTSPostion()
{
   return gstPostion_NTS.ulPosCount;
}
void SetNTSVelocity(int16_t wVelocity)
{
   gstPostion_NTS.wVelocity = wVelocity;
}
int16_t GetNTSVelocity()
{
   return gstPostion_NTS.wVelocity;
}

/*-----------------------------------------------------------------------------

   UI Requests

   bCT:
      = 1 for request from CT UI
      = 0 for  request from MR UI
 -----------------------------------------------------------------------------*/

void InitCarCallQue ( void )
{
   for (uint8_t i = 0; i < MAX_CAR_CALL_REQUESTS; i++)
   {
      stCarCallRequests[i].ucCounter = 0;
   }
}

uint8_t SetCarCall (uint8_t ucFloorIndex, uint8_t ucDoorIndex)
{
   uint8_t ucReturn = 0;

   if (ucFloorIndex < GetFP_NumFloors() )
   {
      if ( GetSRU_Deployment() == enSRU_DEPLOYMENT__MR )
      {
         LatchCarCall(ucFloorIndex, ucDoorIndex);
      }
      else
      {
         //Check if there is already this call latched
         for (uint8_t i = 0; i < MAX_CAR_CALL_REQUESTS; i++)
         {
            if (stCarCallRequests[i].ucCounter && stCarCallRequests[i].ucFloorIndex == ucFloorIndex)
            {
               ucReturn = 1;
               break;
            }
         }

         //if not, latch the call in.
         if (ucReturn == 0)
         {
            for (uint8_t i = 0; i < MAX_CAR_CALL_REQUESTS; i++)
            {
               if (stCarCallRequests[i].ucCounter == 0)
               {
                  stCarCallRequests[i].ucCounter = 10;
                  stCarCallRequests[i].ucFloorIndex = ucFloorIndex;
                  stCarCallRequests[i].ucDoorIndex = ucDoorIndex;
                  ucReturn = 1;
                  break;
               }
            }
         }
      }
   }
   return ucReturn;
}
uint8_t GetCarCallRequest ( enum en_doors eDoor )
{
   uint8_t ucReturn = INVALID_FLOOR;

   for (uint8_t i = 0; i < MAX_CAR_CALL_REQUESTS; i++)
   {
      if( ( ( eDoor == DOOR_FRONT ) && ( !stCarCallRequests[i].ucDoorIndex ) )
       || ( ( eDoor == DOOR_REAR  ) && (  stCarCallRequests[i].ucDoorIndex ) ) )
      {
         if (stCarCallRequests[i].ucCounter)
         {
            ucReturn = stCarCallRequests[i].ucFloorIndex+1;
            stCarCallRequests[i].ucCounter--;
            break;
         }
      }
   }

   return ucReturn;
}



/*-----------------------------------------------------------------------------
UI Request - fault log
   Priority for MR requests over CT
 -----------------------------------------------------------------------------*/
inline uint8_t GetUIRequest_FaultLog()
{
   uint8_t ucReq_MRB = GetUIRequest_FaultLog_MRB();
   uint8_t ucReq_CTB = GetUIRequest_FaultLog_CTB();
   uint8_t ucReq_COPB = GetUIRequest_FaultLog_COPB();

   uint8_t ucReturn = ucReq_MRB;
   if(!ucReq_MRB && !ucReq_CTB)
   {
      ucReturn = ucReq_COPB;
   }
   else if(!ucReq_MRB && !ucReq_COPB)
   {
      ucReturn = ucReq_CTB;
   }

   return ucReturn;
}
/*-----------------------------------------------------------------------------
Latched Calls
 -----------------------------------------------------------------------------*/
static uint32_t gaulBitMapedLatchedCCBs[NUM_OF_DOORS][BITMAP32_SIZE(MAX_NUM_FLOORS)];

void SetBitMap_LatchedCCB(uint32_t ulCarCalls, uint8_t ucIndex, uint8_t bRear)
{
   if(bRear < (gucFP_RearDoor+1))
   {
      if(ucIndex < BITMAP32_SIZE(MAX_NUM_FLOORS))
      {
         if(gaulBitMapedLatchedCCBs[bRear][ucIndex] != ulCarCalls)
         {
            gaulBitMapedLatchedCCBs[bRear][ucIndex] = ulCarCalls;
         }
      }
   }
}

uint32_t GetBitMap_LatchedCCB(uint8_t ucIndex, uint8_t bRear)
{
   uint32_t ulReturn = 0;
   if(bRear < (gucFP_RearDoor+1))
   {
      if(ucIndex < BITMAP32_SIZE(MAX_NUM_FLOORS))
      {
         ulReturn = gaulBitMapedLatchedCCBs[bRear][ucIndex];
      }
   }
   return ulReturn;
}


/*-----------------------------------------------------------------------------
Position Access
 -----------------------------------------------------------------------------*/
void SetStructPosition_NTS(struct st_position_b *pstPosition)
{
   // memcmp results inconsistent.
   uint8_t bDiff = gstPostion_NTS.wVelocity != pstPosition->wVelocity;
   bDiff |= gstPostion_NTS.ulPosCount != pstPosition->ulPosCount;
   bDiff |= gstPostion_NTS.uwStatus != pstPosition->uwStatus;
   bDiff |= gstPostion_NTS.ucError != pstPosition->ucError;
   if(bDiff)
   {
      memcpy(&gstPostion_NTS, pstPosition, sizeof(struct st_position_b));
   }
}
void GetStructPosition_NTS(struct st_position_b *pstPosition)
{
   memcpy(pstPosition, &gstPostion_NTS, sizeof(struct st_position_b));
}

/*-----------------------------------------------------------------------------
States
 ----------------------------------------------------------------------------*/
static uint8_t gucAto_State;

static uint8_t gucFlorLearn_State;
static uint8_t gucCW_Stat;
static uint8_t gucRecal_State;
static uint8_t gucFireSrvice_State;
static uint8_t gucFireSrvice2_State;


void SetModState_AutoState(uint8_t ucState )
{
   gucAto_State = ucState;
}
uint8_t GetModState_AutoState()
{
   return gucAto_State;
}
void SetModState_FloorLearnState(uint8_t ucState )
{
   gucFlorLearn_State = ucState;
}
uint8_t GetModState_FloorLearnState()
{
   return gucFlorLearn_State;
}

void SetModState_FireSrv(uint8_t ucState )
{
   gucFireSrvice_State = ucState;
}
uint8_t GetModState_FireSrv()
{
   return gucFireSrvice_State;
}
void SetModState_FireSrv2(uint8_t ucState )
{
   gucFireSrvice2_State = ucState;
}
uint8_t GetModState_FireSrv2()
{
   return gucFireSrvice2_State;
}
void SetModState_Counterweight(uint8_t ucState )
{
   gucCW_Stat = ucState;
}
uint8_t GetModState_Counterweight()
{
   return gucCW_Stat;
}
void SetModState_Recall(uint8_t ucState )
{
   gucRecal_State = ucState;
}
uint8_t GetModState_Recall()
{
   return gucRecal_State;
}

/*-----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
void SetFaultLogElement(st_fault_data *pstFLog, uint8_t ucIndex_Plus1)
{
   if(ucIndex_Plus1 && ucIndex_Plus1 <= NUM_FAULTLOG_ITEMS)
   {
      memcpy(&astFaultLog[ucIndex_Plus1-1], pstFLog, sizeof(st_fault_data));
   }
}
void GetFaultLogElement(st_fault_data *pstFLog, uint8_t ucIndex_Plus1)
{
   if(ucIndex_Plus1 && ucIndex_Plus1 <= NUM_FAULTLOG_ITEMS)
   {
      memcpy(pstFLog, &astFaultLog[ucIndex_Plus1-1], sizeof(st_fault_data));
   }
   else
   {
      memset(pstFLog, 0, sizeof(st_fault_data));
   }
}
/*-----------------------------------------------------------------------------
   Latching Emergency states bitmap
 ----------------------------------------------------------------------------*/
static uint32_t uiEmergencyBitmap;
inline void SetEmergencyBitmap(uint32_t uiBitmap)
{
   uiEmergencyBitmap = uiBitmap;
}
inline uint32_t GetEmergencyBitmap()
{
   return uiEmergencyBitmap;
}
inline uint8_t GetEmergencyBit( enum en_emergency_bitmap enEmergencyBF )
{
   uint8_t bReturn = 0;
   if( enEmergencyBF < Num_EmergenyBF)
   {
      bReturn = ( uiEmergencyBitmap >> enEmergencyBF ) & 1;
   }
   return bReturn;
}

/*-----------------------------------------------------------------------------
   Acceptance
 ----------------------------------------------------------------------------*/
static uint32_t uiAcceptanceTest_Plus1;

void SetAcceptanceTest_Plus1( uint32_t uiIndex_Plus1)
{
   uiAcceptanceTest_Plus1 = uiIndex_Plus1;
}
uint32_t GetAcceptanceTest_Plus1()
{
   return uiAcceptanceTest_Plus1;
}


/*-----------------------------------------------------------------------------
   Latching faults bitmap
   SetLatchedFaultBitmap only for use in sdata Unload functions & in fram_faultLog.c
 ----------------------------------------------------------------------------*/
static uint32_t uiLatchedFaultBitmap;

inline void SetLatchedFaultBitmap(uint32_t ulBitmap)
{
   uiLatchedFaultBitmap = ulBitmap;
}
inline uint32_t GetLatchedFaultBitmap()
{
   return uiLatchedFaultBitmap;
}
inline uint8_t GetLatchingSafetyFault( enum en_latching_safety_faults eFault )
{
   uint8_t bActive = 0;
   bActive = ( uiLatchedFaultBitmap >> eFault ) & 1;
   return bActive;
}
inline void ClearLatchingSafetyFault( enum en_latching_safety_faults eFault )
{
   Sys_Bit_Set(&uiLatchedFaultBitmap, eFault, 0);
}
/*-----------------------------------------------------------------------------
   Returns 1 if valid opening
 ----------------------------------------------------------------------------*/
uint8_t GetFloorOpening_Front( uint8_t ucFloor )
{
   uint8_t bValid = 0;
   if( ucFloor < GetFP_NumFloors() )
   {
      uint8_t ucMapIndex = ucFloor / 32;
      uint8_t ucBit = ucFloor % 32;
      uint32_t uiMap = Param_ReadValue_32Bit(enPARAM32__OpeningBitmapF_0+ucMapIndex);
      bValid = Sys_Bit_Get(&uiMap, ucBit);
   }
   return bValid;
}
uint8_t GetFloorOpening_Rear( uint8_t ucFloor )
{
   uint8_t bValid = 0;
   if( GetFP_RearDoors()
  && ( ucFloor < GetFP_NumFloors() ) )
   {
      uint8_t ucMapIndex = ucFloor / 32;
      uint8_t ucBit = ucFloor % 32;
      uint32_t uiMap = Param_ReadValue_32Bit(enPARAM32__OpeningBitmapR_0+ucMapIndex);
      bValid = Sys_Bit_Get(&uiMap, ucBit);
   }
   return bValid;
}

/*-----------------------------------------------------------------------------
   ADA requests
 ----------------------------------------------------------------------------*/
static uint8_t bADAFlag; // Flag signalling the next auto door open request should use the ADA timer
static uint32_t auiBF_ADARequests[NUM_ADA_TYPE][BITMAP32_SIZE(MAX_NUM_FLOORS)];
void Init_ADARequest()
{
   for(uint8_t i = 0; i < NUM_ADA_TYPE; i++)
   {
      for(uint8_t j = 0; j < BITMAP32_SIZE(MAX_NUM_FLOORS); j++)
      {
         auiBF_ADARequests[i][j] = 0;
      }
   }
}
uint8_t Get_ADARequest( uint8_t ucFloor, en_ada_type eType )
{
   uint8_t bActive = 0;
   if( ( ucFloor < GetFP_NumFloors() )
    && ( eType < NUM_ADA_TYPE ) )
   {
      bActive = Sys_Bit_Get(&auiBF_ADARequests[eType][0], ucFloor);
   }
   return bActive;
}
void Set_ADARequest( uint8_t ucFloor, en_ada_type eType )
{
   if( ( ucFloor < GetFP_NumFloors() )
    && ( eType < NUM_ADA_TYPE ) )
   {
      Sys_Bit_Set(&auiBF_ADARequests[eType][0], ucFloor, 1);
   }
}
void Clr_ADARequest( uint8_t ucFloor, en_ada_type eType )
{
   if( ( ucFloor < GetFP_NumFloors() )
    && ( eType < NUM_ADA_TYPE ) )
   {
      Sys_Bit_Set(&auiBF_ADARequests[eType][0], ucFloor, 0);
   }
}
uint8_t GetADAFlag()
{
   return bADAFlag;
}
void SetADAFlag(uint8_t bFlag)
{
   bADAFlag = bFlag;
}

/*-----------------------------------------------------------------------------
   View Debug Data Requests
 ----------------------------------------------------------------------------*/
en_view_debug_data GetUIRequest_ViewDebugDataCommand(void)
{
   en_view_debug_data eViewDebugData = VDD__NONE;
   if( GetUIRequest_ViewDebugData_MRB() )
   {
      eViewDebugData = GetUIRequest_ViewDebugData_MRB();
   }
   else if( GetUIRequest_ViewDebugData_CTB() )
   {
      eViewDebugData = GetUIRequest_ViewDebugData_CTB();
   }
   else if( GetUIRequest_ViewDebugData_COPB() )
   {
      eViewDebugData = GetUIRequest_ViewDebugData_COPB();
   }
   return eViewDebugData;
}
/*-----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
uint8_t GetOverLoadFlag(void)
{
   uint8_t bFlag = 0;
   if((GetOperation_AutoMode() == MODE_A__SABBATH) && Param_ReadValue_1Bit(enPARAM1__SabbathDisableLWD))
   {
	   bFlag = 0;
   }
   else
   {
   bFlag |= ( GetInputValue(enIN_OVER_LOAD) );
   bFlag |= ( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__MR ) && ( GetLW_GetLoadFlag_MRB() & (1 << LOAD_FLAG__OVER_LOAD) );
   bFlag |= ( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__CT ) && ( GetLW_GetLoadFlag_CTB() & (1 << LOAD_FLAG__OVER_LOAD) );
   }
   return bFlag;
}
uint8_t GetFullLoadFlag(void)
{
   uint8_t bFlag = 0;
   if((GetOperation_AutoMode() == MODE_A__SABBATH) && Param_ReadValue_1Bit(enPARAM1__SabbathDisableLWD))
   {
	   bFlag = 0;
   }
   else
   {
   bFlag |= ( GetInputValue(enIN_FULL_LOAD) );
   bFlag |= ( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__MR ) && ( GetLW_GetLoadFlag_MRB() & (1 << LOAD_FLAG__FULL_LOAD) );
   bFlag |= ( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__CT ) && ( GetLW_GetLoadFlag_CTB() & (1 << LOAD_FLAG__FULL_LOAD) );
   }
   return bFlag;
}
uint8_t GetLightLoadFlag(void)
{
   uint8_t bFlag = 0;
   if((GetOperation_AutoMode() == MODE_A__SABBATH) && Param_ReadValue_1Bit(enPARAM1__SabbathDisableLWD))
   {
	   bFlag = 0;
   }
   else
   {
   bFlag |= ( GetInputValue(enIN_LIGHT_LOAD) );
   bFlag |= ( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__MR ) && ( GetLW_GetLoadFlag_MRB() & (1 << LOAD_FLAG__LIGHT_LOAD) );
   bFlag |= ( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__CT ) && ( GetLW_GetLoadFlag_CTB() & (1 << LOAD_FLAG__LIGHT_LOAD) );
   }
   return bFlag;
}
/* Weight in car as reported by serial load weighing device */
uint16_t GetInCarWeight_lb(void)
{
   uint16_t uwWeight_lb = 0;
   if( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__CT )
   {
      uwWeight_lb = GetLW_GetInCarWeight_CTB() * 50;
   }
   else if( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__MR )
   {
      uwWeight_lb = GetLW_GetInCarWeight_MRB() * 50;
   }
   return uwWeight_lb;
}
/*-----------------------------------------------------------------------------
   Check if car is inside virtual dead zone
 ----------------------------------------------------------------------------*/
uint8_t InsideDeadZone( void ) {

   uint8_t bInsideDeadZone = 0;

   if ( Param_ReadValue_1Bit(enPARAM1__RelevelEnabled) )
   {
      uint32_t ucRelevelThreshold = Param_ReadValue_8Bit( enPARAM8__RelevelingDistance_05mm );
      if (ucRelevelThreshold < LOWER_RELEVELING_THRESHHOLD)
      {
         ucRelevelThreshold = LOWER_RELEVELING_THRESHHOLD;
      }
      else if (ucRelevelThreshold > UPPER_RELEVELING_THRESHHOLD)
      {
         ucRelevelThreshold = UPPER_RELEVELING_THRESHHOLD;
      }
      uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
      uint32_t uiPosition = GetPosition_PositionCount();

      bInsideDeadZone = ( ( uiPosition > ( Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0+ucCurrentFloor) - ucRelevelThreshold ) )
                       && ( uiPosition < ( Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0+ucCurrentFloor) + ucRelevelThreshold ) ) );
   }
   else
   {
      //if releveling is disabled, then inside the DZ is inside the dead zone
      bInsideDeadZone = GetInputValue(enIN_DZ_F) || ( GetFP_RearDoors() && GetInputValue(enIN_DZ_R) );
   }
   /*
    * If the re-level threshold is to low or too high, default it.
    * Motion doesn't allow movement for distances under 13 counts
    */
   return bInsideDeadZone;
}
/*-----------------------------------------------------------------------------
   Get DL20 flags
 ----------------------------------------------------------------------------*/
uint8_t DL20_GetPhoneFailureFlag(void)
{
   uint8_t bActive = 0;
   if( Param_ReadValue_1Bit(enPARAM1__EnableDL20_CT) )
   {
      bActive = GetDebugData_CTA_DL20_PhoneFailureFlag();
   }
   else if( Param_ReadValue_1Bit(enPARAM1__EnableDL20_COP) )
   {
      bActive = GetDebugData_COPA_DL20_PhoneFailureFlag();
   }
   return bActive;
}
uint8_t DL20_GetOOSFlag(void)
{
   uint8_t bActive = 0;
   if( Param_ReadValue_1Bit(enPARAM1__EnableDL20_CT) )
   {
      bActive = GetDebugData_CTA_DL20_OOSFlag();
   }
   else if( Param_ReadValue_1Bit(enPARAM1__EnableDL20_COP) )
   {
      bActive = GetDebugData_COPA_DL20_OOSFlag();
   }
   return bActive;
}
/*-----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static uint8_t abSwingClosedContactsProgrammed[NUM_OF_DOORS];
uint8_t Door_GetSwingClosedContactsProgrammedFlag( enum en_doors eDoor )
{
   if( eDoor == NUM_OF_DOORS )
   {
      return abSwingClosedContactsProgrammed[DOOR_FRONT] || abSwingClosedContactsProgrammed[DOOR_REAR];
   }
   else
   {
      return abSwingClosedContactsProgrammed[eDoor];
   }
}
void Door_SetSwingClosedContactsProgrammedFlag( enum en_doors eDoor )
{
   abSwingClosedContactsProgrammed[eDoor] = 1;
}
