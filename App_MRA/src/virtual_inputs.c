/******************************************************************************
 *
 * @file     remoteCommands.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     13 August 2019
 *
 * @note
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_a.h"
#include "sys.h"
#include "remoteCommands.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
en_dispatch_mode enCurrentDispatchMode = DISPATCH_MODE__NONE;

static uint32_t auiBF_Virtual_CarCallSecurity[ NUM_OF_DOORS ][BITMAP32_SIZE(MAX_NUM_FLOORS)];
static uint32_t auiBF_Virtual_HallCallSecurity[ NUM_DOOR_STATES ][BITMAP32_SIZE(MAX_NUM_FLOORS)];
static uint32_t uiBF_Virtual_SysInputs;
/*
    static uint32_t auiBF_Virtual_SysInputs[ BITMAP32_SIZE ( NUM_VIRTUAL_INPUTS ) ];
    Use if number of virtual system inputs becomes greater than 32
*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
void Unload_RemoteCommands( void )
{
   if(GetDatagramDirtyBit(DATAGRAM_ID_216))
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_216);
      en_remote_data enDataKey = GetRemoteData_Key();
      uint32_t uiValue = GetRemoteData_Value();
      switch(GetRemoteData_Key())
      {
         case REMOTE_DISPATCH_MODE:
            enCurrentDispatchMode = uiValue & 0xFF;
            break;
         case REMOTE_CARCALL_SECURITY_F0:
            auiBF_Virtual_CarCallSecurity[0][0] = uiValue;
            break;
         case REMOTE_CARCALL_SECURITY_F1:
            auiBF_Virtual_CarCallSecurity[0][1] = uiValue;
            break;
         case REMOTE_CARCALL_SECURITY_F2:
            auiBF_Virtual_CarCallSecurity[0][2] = uiValue;
            break;
         case REMOTE_CARCALL_SECURITY_R0:
            auiBF_Virtual_CarCallSecurity[1][0] = uiValue;
            break;
         case REMOTE_CARCALL_SECURITY_R1:
            auiBF_Virtual_CarCallSecurity[1][1] = uiValue;
            break;
         case REMOTE_CARCALL_SECURITY_R2:
            auiBF_Virtual_CarCallSecurity[1][2] = uiValue;
            break;
         case REMOTE_HALLCALL_SECURITY_F0:
            auiBF_Virtual_HallCallSecurity[0][0] = uiValue;
            break;
         case REMOTE_HALLCALL_SECURITY_F1:
            auiBF_Virtual_HallCallSecurity[0][1] = uiValue;
            break;
         case REMOTE_HALLCALL_SECURITY_F2:
            auiBF_Virtual_HallCallSecurity[0][2] = uiValue;
            break;
         case REMOTE_HALLCALL_SECURITY_R0:
            auiBF_Virtual_HallCallSecurity[1][0] = uiValue;
            break;
         case REMOTE_HALLCALL_SECURITY_R1:
            auiBF_Virtual_HallCallSecurity[1][1] = uiValue;
            break;
         case REMOTE_HALLCALL_SECURITY_R2:
            auiBF_Virtual_HallCallSecurity[1][2] = uiValue;
            break;
         case REMOTE_SYSTEM_INPUTS:
            uiBF_Virtual_SysInputs = uiValue;
            break;
         default:
            break;
      }
   }
}

en_dispatch_mode GetDispatchMode(void)
{
   return enCurrentDispatchMode;
}

uint8_t GetVirtual_CarCall_Enable(uint8_t ucLanding, uint8_t enDoorIndex)
{
   uint8_t ucMapIndex = ucLanding / 32;
   uint8_t ucBitIndex = ucLanding % 32;
   uint32_t uiValue = auiBF_Virtual_CarCallSecurity[enDoorIndex][ucMapIndex];
   uint8_t bValue = Sys_Bit_Get(&uiValue, ucBitIndex);

   return bValue;
}

uint32_t GetVirtual_CarCallSecurityBitmap(uint8_t ucMapIndex, uint8_t ucDoorIndex)
{
   return auiBF_Virtual_CarCallSecurity[ucDoorIndex][ucMapIndex];
}

uint8_t GetVirtual_SystemInput(en_virtual_system_inputs enVirtualInput)
{
   uint8_t bValue = 0;
   if (enVirtualInput < NUM_VIRTUAL_SYTEM_INPUTS)
   {
      bValue = Sys_Bit_Get(&uiBF_Virtual_SysInputs, enVirtualInput);
   }

   return bValue;
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

