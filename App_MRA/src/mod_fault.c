/******************************************************************************
 * @author   Keith Soneda
 * @version  V1.00
 * @date     1, August 2016
 *
 * @note   This file works with mod_fault_app.c for fault management between boards in enum en_fault_nodes
 *          ClrFault/SetFault(ucFaultNum)
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "sru_a.h"
#include "sru.h"
#include "sys.h"
#include <stdint.h>
#include <string.h>
#include "GlobalData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Fault =
{
        .pfnInit = Init,
        .pfnRun = Run,
};
st_fault gstFault;

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define LIMIT_CONSECUTIVE_FAULTS    (3)

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Checks reset source and asserts appropriate reset fault
 -----------------------------------------------------------------------------*/
static void UpdateResetFault()
{
   static uint32_t uiResetSrc;
   if(!uiResetSrc)
   {
      uiResetSrc = Chip_SYSCTL_GetSystemRSTStatus();
      Chip_SYSCTL_ClearSystemRSTStatus(uiResetSrc);
   }

   if( uiResetSrc & SYSCTL_RST_BOD )
   {
      en_faults eFaultNum = FLT__BOD_RESET_MRA + FaultApp_GetNode()-1;
      if(eFaultNum > FLT__BOD_RESET_COPB)
      {
         eFaultNum = FLT__BOD_RESET_COPB;
      }
      SetFault( eFaultNum );
      gstFault.bActiveFault = 1;
      gstFault.eFaultNumber = eFaultNum;
      gstFault.eActiveNode_Plus1 = FaultApp_GetNode() + 1;
      gstFault.aeActiveFaultNum[FaultApp_GetNode()] = eFaultNum;
   }
   else if( uiResetSrc & SYSCTL_RST_WDT )
   {
      en_faults eFaultNum = FLT__WDT_RESET_MRA + FaultApp_GetNode()-1;
      if(eFaultNum > FLT__WDT_RESET_COPB)
      {
         eFaultNum = FLT__WDT_RESET_COPB;
      }
      SetFault( eFaultNum );
      gstFault.bActiveFault = 1;
      gstFault.eFaultNumber = eFaultNum;
      gstFault.eActiveNode_Plus1 = FaultApp_GetNode() + 1;
      gstFault.aeActiveFaultNum[FaultApp_GetNode()] = eFaultNum;
   }
   else//assume SYSCTL_RST_POR
   {
      en_faults eFaultNum = FLT__BOARD_RESET_MRA + FaultApp_GetNode()-1;
      if(eFaultNum > FLT__BOARD_RESET_COPB)
      {
         eFaultNum = FLT__BOARD_RESET_COPB;
      }
      SetFault( eFaultNum );
      gstFault.bActiveFault = 1;
      gstFault.eFaultNumber = eFaultNum;
      gstFault.eActiveNode_Plus1 = FaultApp_GetNode() + 1;
      gstFault.aeActiveFaultNum[FaultApp_GetNode()] = eFaultNum;
   }
}
/*----------------------------------------------------------------------------
   Returns the corresponding enum en_latching_safety_faults + 1
   if the fault is latching
 *----------------------------------------------------------------------------*/
static uint8_t GetLatchedFaultIndex_Plus1( en_faults eFaultNum )
{
   uint8_t ucIndex_Plus1 = 0;
   if( GetFault_ClearType( eFaultNum ) == FT_CLR_LTCH )
   {
      switch( eFaultNum )
      {
         case FLT__GOV:
         case FLT__GOV_L:
            // See UpdateLatchingGOVFault()
//            ucIndex_Plus1 = LSF__GOV+1;
            break;

         case FLT__EB1_DROP:
         case FLT__EB1_DROP_L:
            // See UpdateLatchingFPGADropEBrakeFault()
//            ucIndex_Plus1 = LSF__EB1_DROPPED+1;
            break;

         case FLT__UNINTENDED_MOV:
         case FLT__UNINTENDED_MOV_L:
            ucIndex_Plus1 = LSF__UNINTENDED_MOVEMENT+1;
            break;

         case FLT__TRACTION_LOSS:
         case FLT__TRACTION_LOSS_L:
            ucIndex_Plus1 = LSF__TRACTION_LOSS+1;
            break;

         case FLT__OVERSPEED_GENERAL:
         case FLT__OVERSPEED_GENERAL_L:
            ucIndex_Plus1 = LSF__OVERSPEED+1;
            break;

         default:
            // TODO set fault
            break;
      }
   }
   return ucIndex_Plus1;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetFault( en_faults eFaultNum )
{
   if( ( GetOperation_ManualMode() != MODE_M__CONSTRUCTION )
    || ( GetFault_ConstructionType(eFaultNum) == FT_CON__ON ) )
    {
        if(eFaultNum < NUM_FAULTS)
        {
            st_fault_data stFaultData;
            stFaultData.eFaultNum = eFaultNum;
            SetLocalActiveFault(&stFaultData);
            //------------------------------------
            uint8_t ucLatchingFaultNum_Plus1 = GetLatchedFaultIndex_Plus1( eFaultNum );
            if( ucLatchingFaultNum_Plus1 )
            {
                SetLatchingSafetyFault( ucLatchingFaultNum_Plus1-1 );
            }
        }
    }
}


void SetCPLDFault( en_faults eFaultNum )
{
   if(eFaultNum < NUM_FAULTS)
   {
      st_fault_data stFaultData;
      stFaultData.eFaultNum  = eFaultNum;
      SetActiveFault_CPLD(&stFaultData);
   }
}

/*-----------------------------------------------------------------------------
   Update active fault and per node fault arrays
 -----------------------------------------------------------------------------*/
void UpdateActiveFaults( void )
{
    gstFault.bActiveFault = 0;
    gstFault.eFaultNumber = FLT__NONE;
    gstFault.eActiveNode_Plus1 = 0;
    for( uint8_t i = 0; i < NUM_FAULT_NODES; i++ )
    {
       /* The following will prioritize logging MRA faults over CPLD faults */
       enum en_fault_nodes eNode = ( enFAULT_NODE__MRA + i ) % NUM_FAULT_NODES;

        st_fault_data stActiveFault;
        GetBoardActiveFault( &stActiveFault, eNode );
        en_faults eFaultNum = stActiveFault.eFaultNum;

        gstFault.aeActiveFaultNum[ eNode ] = eFaultNum;

        if( !gstFault.bActiveFault
       && ( eFaultNum != FLT__NONE ) )
        {
            gstFault.bActiveFault = 1;
            gstFault.eFaultNumber = eFaultNum;
            gstFault.eActiveNode_Plus1 = eNode + 1;
        }
    }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_FaultLED()
{
    static uint8_t ucTimer_1ms;
    static uint8_t bLastLED;
    if ( gstFault.eFaultNumber )
    {
        if ( ucTimer_1ms > 100/MOD_RUN_PERIOD_FAULT_1MS )
        {
            SRU_Write_LED( enSRU_LED_Fault, bLastLED );

            ucTimer_1ms = 0;

            bLastLED = ( bLastLED )? 0 : 1;
        }
        else
        {
            ucTimer_1ms++;
        }
    }
    else
    {
        SRU_Write_LED( enSRU_LED_Fault, 0 );
    }
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
    pstThisModule->uwInitialDelay_1ms = 50;
    pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_FAULT_1MS;

    return 0;
}


/*-----------------------------------------------------------------------------

   The fault module asserts a reset fault for 3 seconds at startup before
   starting to process the rest of the faults.
   Most fault will automatically clear after 3 seconds.
   Some faults that don't fit in any other module are being processed here in
   the fault module such as DIP8 (CPU Stop Switch) or Fire Stop Switch.
   When ANY fault is active, the car cannot move and commands Emergency Stop.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
    static uint32_t uiStartupCounter_1ms = BOARD_RESET_FAULT_TIMER_MS/MOD_RUN_PERIOD_FAULT_1MS;

    if( uiStartupCounter_1ms )
    {
       UpdateResetFault();
       uiStartupCounter_1ms--;
    }
    else if( SRU_Read_DIP_Switch( enSRU_DIP_A1 ) )
    {
        en_faults eFaultNum = FLT__CPU_STOP_SW_MRA + FaultApp_GetNode()-1;
        if(eFaultNum > FLT__CPU_STOP_SW_COPB)
        {
           eFaultNum = FLT__CPU_STOP_SW_COPB;
        }
        SetFault( eFaultNum );
        gstFault.bActiveFault = 1;
        gstFault.eFaultNumber = eFaultNum;
        gstFault.eActiveNode_Plus1 = FaultApp_GetNode() + 1;
        gstFault.aeActiveFaultNum[FaultApp_GetNode()] = eFaultNum;
    }
    else
    {
        Run_ModFault_MRA();
    }
    Update_FaultLED();
    return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
