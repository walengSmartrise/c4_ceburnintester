/******************************************************************************
 *
 * @file     mod_flood_oper.c
 * @brief    Logic that controls how flood operation work.
 * @version  V1.00
 * @date     1, March 2018
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "GlobalData.h"
#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Sabbath = { .pfnInit = Init, .pfnRun = Run, };

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
// Prevents running past counterweight if no valid stop position found
#define DEBOUNCE_NEED_TO_ESTOP      (2)
#define UP                          (1)
#define DOWN                        (0)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
/* Add single cycle delay before floor selection, otherwise reachable floor assessment will be
 * calculated prior to speed limit change  */
static uint8_t bSelectDest = 1;
static uint8_t bDirSabbath = UP;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint32_t GetSabbathParamTime( uint8_t param )
{
   uint32_t timeInHours = Param_ReadValue_24Bit( param );

   uint32_t hourInSeconds = timeInHours / 100;

   uint32_t minuteInSeconds = timeInHours - (hourInSeconds * 100);

   uint32_t timeInSeconds = ( hourInSeconds * 3600 ) + ( minuteInSeconds * 60 );

   return timeInSeconds;
}

/*-----------------------------------------------------------------------------
   Returns 1 if valid opening
 ----------------------------------------------------------------------------*/
uint8_t GetSabbath_FloorOpening_Front( uint8_t ucFloor, enum direction_enum eDir)
{
   uint8_t bValid = 0;
   if( ucFloor < GetFP_NumFloors() )
   {
      uint8_t ucMapIndex = ucFloor / 32;
      uint8_t ucBit = ucFloor % 32;
      uint32_t uiMap = Param_ReadValue_32Bit(enPARAM32__SabbathOpeningF_0+ucMapIndex);
      uint32_t uiDest;
      if( eDir == DIR__UP)
      {
         uiDest = Param_ReadValue_32Bit(enPARAM32__SabbathDestinationUp_0+ucMapIndex);
      }
      else
      {
         uiDest = Param_ReadValue_32Bit(enPARAM32__SabbathDestinationDown_0+ucMapIndex);
      }
      bValid = Sys_Bit_Get(&uiMap, ucBit) & Sys_Bit_Get(&uiDest, ucBit);

      if( ucFloor == 1 )
      {
         int a = Sys_Bit_Get(&uiMap, ucBit);
         int b = Sys_Bit_Get(&uiDest, ucBit);
         a = 0;
      }
   }
   return bValid;
}
/*-----------------------------------------------------------------------------
   Returns 1 if valid opening
 ----------------------------------------------------------------------------*/
uint8_t GetSabbath_FloorOpening_Rear( uint8_t ucFloor, enum direction_enum eDir )
{
   uint8_t bValid = 0;
   if(   ( GetFP_RearDoors() ) &&
         ( ucFloor < GetFP_NumFloors() ) )
   {
      uint8_t ucMapIndex = ucFloor / 32;
      uint8_t ucBit = ucFloor % 32;
      uint32_t uiMap = Param_ReadValue_32Bit(enPARAM32__SabbathOpeningR_0+ucMapIndex);
      uint32_t uiDest;
      if( eDir == DIR__UP)
      {
         uiDest = Param_ReadValue_32Bit(enPARAM32__SabbathDestinationUp_0+ucMapIndex);
      }
      else
      {
         uiDest = Param_ReadValue_32Bit(enPARAM32__SabbathDestinationDown_0+ucMapIndex);
      }

      bValid = Sys_Bit_Get(&uiMap, ucBit) & Sys_Bit_Get(&uiDest, ucBit);
   }
   return bValid;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void Auto_Sabbath(uint8_t bInit)
{
   if(bInit)
   {
      ClearLatchedCarCalls();
      ClearLatchedHallCalls();
   }

   else
   {
      uint8_t bValidFrontDest;
      uint8_t bValidRearDest;

      if(!GetMotion_RunFlag() && bSelectDest)
      {
         if( ( bDirSabbath == UP )  )
         { 
            uint8_t ucNextDest = INVALID_FLOOR;
            int8_t ucNextFloor = GetOperation_CurrentFloor() + 1;

            /*Look for next valid up dest above*/
            for (int8_t i = ucNextFloor; i < GetFP_NumFloors(); i++)
            {
               bValidFrontDest = GetSabbath_FloorOpening_Front(i, bDirSabbath);
               bValidRearDest = GetSabbath_FloorOpening_Rear(i, bDirSabbath);

               if(bValidFrontDest || bValidRearDest)
               {
                  ucNextDest = i;
                  break;
               }
            }
            /*If no valid up dest above, look for highest valid down dest*/
            if(ucNextDest == INVALID_FLOOR)
            {
               bDirSabbath = DOWN;
               for(int8_t i =  GetFP_NumFloors() - 1; i >= 0; i--)
               {
                  bValidFrontDest = GetSabbath_FloorOpening_Front(i, bDirSabbath);
                  bValidRearDest = GetSabbath_FloorOpening_Rear(i, bDirSabbath);
                  
                  if((i != GetOperation_CurrentFloor()) &&
                     (bValidFrontDest || bValidRearDest)
                  )
                  {
                     ucNextDest = (uint8_t) i;
                     break;
                  }
               }
            }

            // /*If no down dest look for lowest valid up dest*/
            if(ucNextDest == INVALID_FLOOR)
            {
               bDirSabbath = UP;
               for(int8_t i = 0; i < GetFP_NumFloors(); i++)
               {
                  bValidFrontDest = GetSabbath_FloorOpening_Front(i, bDirSabbath);
                  bValidRearDest = GetSabbath_FloorOpening_Rear(i, bDirSabbath);
               
                  if((i != GetOperation_CurrentFloor()) &&
                     (bValidFrontDest || bValidRearDest)
                  )
                  {
                     ucNextDest = (uint8_t) i;
                     break;
                  }
               }
            }
            
            if( ( ucNextDest < GetFP_NumFloors() ) )
            {
               enum en_doors enDoorIndex = DOOR_ANY;
               uint8_t bDoorAny = (bValidFrontDest && bValidRearDest);
               if(!bDoorAny)
               {
                  enDoorIndex = (bValidFrontDest) ? DOOR_FRONT : DOOR_REAR;
               }
               LatchCarCall(ucNextDest, enDoorIndex);
               bSelectDest = 0;
            }
         }

         else if( ( bDirSabbath == DOWN ) )
         {
            uint8_t ucNextDest = INVALID_FLOOR;
            int8_t ucNextFloor = GetOperation_CurrentFloor() - 1;

            /*Look for next valid down dest below*/
            for( int8_t i = ucNextFloor; i >= 0; i--)
            {
                  bValidFrontDest = GetSabbath_FloorOpening_Front(i, bDirSabbath);
                  bValidRearDest = GetSabbath_FloorOpening_Rear(i, bDirSabbath);
                  
                  if(bValidFrontDest || bValidRearDest)
                  {
                     ucNextDest = (uint8_t) i;
                     break;
                  }
            }
            /*If no valid down dest above, look for lowest valid up dest*/
            if(ucNextDest == INVALID_FLOOR)
            {
               bDirSabbath = UP;
               for(int8_t i = 0; i < GetFP_NumFloors(); i++)
               {
                  bValidFrontDest = GetSabbath_FloorOpening_Front(i, bDirSabbath);
                  bValidRearDest = GetSabbath_FloorOpening_Rear(i, bDirSabbath);
                  
                  if((i != GetOperation_CurrentFloor()) &&
                     (bValidFrontDest || bValidRearDest)
                  )
                  {
                     ucNextDest = (uint8_t) i;
                     break;
                  }
               }
            }

            /*If no up dest above look for highest valid down dest above*/
            if(ucNextDest == INVALID_FLOOR)
            {
               bDirSabbath = DOWN;
               for(int8_t i = GetFP_NumFloors() - 1; i >= 0; i--)
               {
                  bValidFrontDest = GetSabbath_FloorOpening_Front(i, bDirSabbath);
                  bValidRearDest = GetSabbath_FloorOpening_Rear(i, bDirSabbath);
               
                  if((i != GetOperation_CurrentFloor()) &&
                     (bValidFrontDest || bValidRearDest)
                  )
                  {
                     ucNextDest =  (uint8_t) i;
                     break;
                  }
               }
            }

            if( ( ucNextDest < GetFP_NumFloors() ) )
            {
               enum en_doors enDoorIndex = DOOR_ANY;
               uint8_t bDoorAny = (bValidFrontDest && bValidRearDest);
               if(!bDoorAny)
               {
                  enDoorIndex = (bValidFrontDest) ? DOOR_FRONT : DOOR_REAR;
               }
               LatchCarCall(ucNextDest, enDoorIndex);
               bSelectDest = 0;
            }
         }
      }
      else
      {
         bSelectDest = 1;
      }

      Auto_Oper(bInit);
   }
}

/*-----------------------------------------------------------------------------

 Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 1500;
   pstThisModule->uwRunPeriod_1ms = 200;

   return 0;
}

/*-----------------------------------------------------------------------------
Earthquake Specifications from Functional Test Procedures Document for 2.41c

Bring the car down to the bottom landing and activate the flood sensor. Verify the car raises up the landings denoted in Parameter 13-148

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   SetOutputValue(enOUT_LMP_SABBATH, ( GetOperation_AutoMode() == MODE_A__SABBATH ));

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
