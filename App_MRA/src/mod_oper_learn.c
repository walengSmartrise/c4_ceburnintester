/******************************************************************************
 *
 * @file     mod_heartbeat.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_a.h"
#include <stdint.h>
#include <stdlib.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Oper_Learn =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

struct st_floors * gpastFloors;

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static enum en_mode_learn geCompletedLearnMode;
static enum en_learn_states geLearnState;
static enum en_learn_states eLastLearnState;
static uint16_t guwThreeSeconds;
static uint8_t gbNeedToLearn;
static uint8_t ucNumDZs = 1;

static uint16_t uwCounterUP, uwCounterDN;

static uint8_t bDZ_Old = 1;
static uint8_t ucLearnFloor;

static uint8_t bFirstHALearnCycle;
static uint8_t bLogFallingEdge; // Flag for logging the falling edge position of the starting learn floor
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
returns 1 if the argument floor is a short floor
Short floors do not trigger the minimum floor spacing requirement fault ( NEED_TO_LEARN)
When learning short floors, floors marked as short will be saved as an offset from the floor before it.
 -----------------------------------------------------------------------------*/
static uint8_t CheckIfShortFloor(uint8_t ucFloorIndex)
{
   uint8_t ucMapIndex = ucFloorIndex / NUM_BITS_PER_BITMAP16;
   uint8_t ucBitIndex = ucFloorIndex % NUM_BITS_PER_BITMAP16;
   uint16_t uwBitMap = Param_ReadValue_16Bit(enPARAM16__ShortFloorOpening_0+ucMapIndex);
   uint8_t bShortFloor = GET_BITFLAG_U16(uwBitMap, ucBitIndex);
   return bShortFloor;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t Learn_GetNeedToLearnFlag(void)
{
   return gbNeedToLearn;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
enum en_learn_states Learn_GetState( void )
{
   return geLearnState;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void ResetLearnState()
{
   geLearnState = LEARN__NOT_ON_LEARN;
   SetOperation_LearnMode( MODE_L__NONE);
   uwCounterUP = 0;
   uwCounterDN = 0;
   bDZ_Old = 1;
   ucNumDZs = 1;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t GetNextValidOpening( uint8_t ucStartingFloor, enum direction_enum eDir )
{
   uint8_t ucNextFloor = INVALID_FLOOR;
   if( ucStartingFloor < GetFP_NumFloors() )
   {
      if( eDir == DIR__UP )
      {
         for( uint8_t i = ucStartingFloor; i < GetFP_NumFloors(); i++ )
         {
            if( gpastFloors[i].bFrontOpening || gpastFloors[i].bRearOpening )
            {
               ucNextFloor = i;
               break;
            }
         }
      }
      else if( eDir == DIR__DN )
      {
         for( int8_t i = ucStartingFloor; i >= 0; i-- )
         {
            if( gpastFloors[i].bFrontOpening || gpastFloors[i].bRearOpening )
            {
               ucNextFloor = i;
               break;
            }
         }
      }
   }

   return ucNextFloor;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void StoreFloorPositionsInFlash( uint8_t bDir )
{
   uint32_t ulTimeout = 0;
   static uint8_t ucFloor;

   if(bDir) /* Up */
   {
      /* Don't offset starting floor */
      uint32_t ulWriteValue = (!ucFloor) ? ( gpastFloors[ ucFloor ].ulPosition )
                                         : ( gpastFloors[ ucFloor ].ulPosition + THREE_INCHES );
      uint8_t bSaved = Param_WriteValue_24Bit( enPARAM24__LearnedFloor_0 + ucFloor, ulWriteValue );
      if(bSaved || (ulTimeout++ >= 0xFFFFFFFF))
      {
         ucFloor = ( ucFloor + 1 ) % GetFP_NumFloors();
      }
   }
   else /* Down */
   {
      /* Don't offset starting floor */
      uint32_t ulWriteValue = ( ucFloor == (GetFP_NumFloors()-1) ) ? ( gpastFloors[ ucFloor ].ulPosition )
                                                                   : ( gpastFloors[ ucFloor ].ulPosition - THREE_INCHES );
      uint8_t bSaved = Param_WriteValue_24Bit( enPARAM24__LearnedFloor_0 + ucFloor, ulWriteValue );
      if(bSaved || (ulTimeout++ >= 0xFFFFFFFF))
      {
         ucFloor = ( ucFloor + 1 ) % GetFP_NumFloors();
      }
   }
}

/*-----------------------------------------------------------------------------
Checks that learned floors are in increasing order
and that each floor position is a minimum distance away from the one before it.

If either condition fails, sets fault.

 -----------------------------------------------------------------------------*/
static void CheckFloorPositions( void )
{
   gbNeedToLearn = 0;
   gpastFloors[ 0 ].ulPosition = Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 );

   for ( uint8_t ucFloor = 0; ucFloor < GetFP_NumFloors()-1; ucFloor++ )
   {
      uint32_t ulLo = Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0+ucFloor);
      uint32_t ulHi = Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0+ucFloor+1 );
      uint8_t bValidOpeningLo = gpastFloors[ucFloor].bFrontOpening || gpastFloors[ucFloor].bRearOpening;
      uint8_t bValidOpeningHi = gpastFloors[ucFloor+1].bFrontOpening || gpastFloors[ucFloor+1].bRearOpening;
      uint8_t bShortFloorLo = CheckIfShortFloor(ucFloor);
      uint8_t bShortFloorHi = CheckIfShortFloor(ucFloor+1);
      /* Only flag minimum floor spacing issues if neither floor is a short floor and both floors have openings */
      if( ( !bShortFloorLo )
       && ( !bShortFloorHi )
       && ( bValidOpeningLo )
       && ( bValidOpeningHi )
       && ( ulHi <= ( ulLo + MINIMUM_FLOOR_SPACING ) ) )
      {
         gbNeedToLearn = 1;
         break;
      }
      /* Otherwise flag just if a higher floor has a lower floor position than a floor that should be below it */
      else if( ulHi < ulLo )
      {
         gbNeedToLearn = 1;
         break;
      }
      gpastFloors[ ucFloor + 1 ].ulPosition = ulHi;
   }

   if( !gpastFloors[ 0 ].ulPosition )
   {
      gbNeedToLearn = 1;
   }

   if ( ( gbNeedToLearn )
     && ( GetOperation_ClassOfOp() == CLASSOP__AUTO ) )
   {
      SetFault(FLT__NEED_TO_LEARN);
   }
}

/*-----------------------------------------------------------------------------
True if dip5 is set
 -----------------------------------------------------------------------------*/
static uint8_t CheckIf_HoistwayLearn()
{
   uint8_t bReturn = (SRU_Read_DIP_Switch(enSRU_DIP_A5))
                   ? 1:0;
   return bReturn;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Process_NotOnLearn( void )
{
   geCompletedLearnMode = MODE_L__NONE;
   if ( GetOperation_ClassOfOp() == CLASSOP__SEMI_AUTO )
   {
      if ( CheckIf_HoistwayLearn() )
      {
          SetOperation_LearnMode( MODE_L__GOTO_TERM_HA);

         if ( ( GetPosition_Velocity() == 0 )
           && ( getDoorZone(DOOR_ANY) ) )
         {
        	Param_WriteValue_1Bit(enPARAM1__BypassTermLimits, 1);

        	geLearnState = LEARN__READY;

            SetOperation_LearnMode(MODE_L__READY_HA);
         }
      }
   }
   else
   {
      SetOperation_LearnMode( MODE_L__NONE);

      CheckFloorPositions();

      uwCounterUP = 0;
      uwCounterDN = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Process_Ready_HA()
{
   uwCounterDN = ( GetInputValue( enIN_MRDN ) ) ? (uwCounterDN+1):0;
   uwCounterUP = ( GetInputValue( enIN_MRUP ) ) ? (uwCounterUP+1):0;
   if ( uwCounterDN >= guwThreeSeconds )
   {
      gpastFloors[ GetFP_NumFloors() - 1 ].ulPosition = GetPosition_PositionCount();

      SetOperation_MotionCmd (MOCMD__RUN_DN);

      SetOperation_SpeedLimit( Param_ReadValue_16Bit( enPARAM16__LearnSpeed ));

      SetOperation_PositionLimit_DN( 0);

      geLearnState = LEARN__LEARNING;

      SetOperation_LearnMode( MODE_L__LEARNING_DHA);
      bLogFallingEdge = 1;
      bDZ_Old = 0;
      ucLearnFloor = GetFP_NumFloors()-1;
      bFirstHALearnCycle = 1;
   }
   else if ( uwCounterUP >= guwThreeSeconds )
   {
      gpastFloors[ 0 ].ulPosition = GetPosition_PositionCount();

      SetOperation_MotionCmd( MOCMD__RUN_UP);

      SetOperation_SpeedLimit( Param_ReadValue_16Bit( enPARAM16__LearnSpeed ));

      SetOperation_PositionLimit_UP( 0xFFFFFFFF);
      geLearnState = LEARN__LEARNING;

      SetOperation_LearnMode( MODE_L__LEARNING_UHA);
      bLogFallingEdge = 1;
      bDZ_Old = 0;
      ucLearnFloor = 0;
      bFirstHALearnCycle = 1;
   }
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Process_Ready( void )
{
   switch ( GetOperation_LearnMode() )
   {
      case MODE_L__READY_HA:
         Process_Ready_HA();
         break;

      case MODE_L__BYP_TERM_LIMIT_HA:
         //do nothing. state change will occur outside of switch
         break;

      default:
         geLearnState = LEARN__NOT_ON_LEARN;
         break;
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Process_Learning( void )
{
   if( GetOperation_LearnMode() == MODE_L__LEARNING_UHA )
   {
      // End learn if learn floor is invalid
      if ( ucLearnFloor >= GetFP_NumFloors() )
      {
         bDZ_Old = 1;
         SetOperation_MotionCmd(MOCMD__EMERG_STOP);
         geCompletedLearnMode = GetOperation_LearnMode();
         geLearnState = LEARN__COMPLETE;
      }
      else
      {
         // Assume a new DZ is detected on every transition from low to high
         if ( getDoorZone(DOOR_ANY) )
         {
            if ( !bDZ_Old )
            {
               bDZ_Old = 1;
               // Log position for falling edge of DZ signal for later adjustment
               if(bFirstHALearnCycle)
               {
                  bFirstHALearnCycle = 0;
               }
               else
               {
                  gpastFloors[ ucLearnFloor ].ulPosition = GetPosition_PositionCount();
               }
               // Get next valid learn floor
               uint8_t ucNewLearnFloor = GetNextValidOpening( ucLearnFloor+1, DIR__UP );
               if( ucNewLearnFloor < GetFP_NumFloors() )
               {
                  // For express floors, place filler positions to bypass need to learn fault
                  for( uint8_t i = ucLearnFloor+1; i < ucNewLearnFloor; i++ )
                  {
                     uint32_t uiOffset = (2*MINIMUM_FLOOR_SPACING)*(i-ucLearnFloor);
                     gpastFloors[i].ulPosition = gpastFloors[ucLearnFloor].ulPosition+uiOffset;
                  }
               }


               /* Update current learn floor */
               ucLearnFloor = ucNewLearnFloor;

               /* Check if short floor */
               if( CheckIfShortFloor(ucLearnFloor) )
               {
                  /* Log short floors as a fixed offset from the previous floor */
                  gpastFloors[ucLearnFloor].ulPosition = gpastFloors[ucLearnFloor-1].ulPosition + QUARTER_INCH;

                  // Get next valid learn floor
                  ucNewLearnFloor = GetNextValidOpening( ucNewLearnFloor+1, DIR__UP );
                  if( ucNewLearnFloor < GetFP_NumFloors() )
                  {
                     // For express floors, place filler positions to bypass need to learn fault
                     for( uint8_t i = ucLearnFloor+1; i < ucNewLearnFloor; i++ )
                     {
                        uint32_t uiOffset = (2*MINIMUM_FLOOR_SPACING)*(i-ucLearnFloor);
                        gpastFloors[i].ulPosition = gpastFloors[ucLearnFloor].ulPosition+uiOffset;
                     }
                  }

                  /* Update current learn floor */
                  ucLearnFloor = ucNewLearnFloor;
               }
            }
         }
         else
         {
            bDZ_Old = 0;
            if( bLogFallingEdge )
            {
               gpastFloors[0].ulPosition = GetPosition_PositionCount() - THREE_INCHES;
               bLogFallingEdge = 0;
            }
         }
      }
   }
   else if( GetOperation_LearnMode() == MODE_L__LEARNING_DHA )
   {
      // End learn if learn floor is invalid
      if ( ucLearnFloor >= GetFP_NumFloors() )
      {
         bDZ_Old = 1;
         SetOperation_MotionCmd(MOCMD__EMERG_STOP);
         geCompletedLearnMode = GetOperation_LearnMode();
         geLearnState = LEARN__COMPLETE;
      }
      else
      {
         // Assume a new DZ is detected on every transition from low to high
         if ( getDoorZone(DOOR_ANY) )
         {
            if ( !bDZ_Old )
            {
               bDZ_Old = 1;
               // Log position for falling edge of DZ signal for later adjustment
               if(bFirstHALearnCycle)
               {
                  bFirstHALearnCycle = 0;
               }
               else
               {
                  gpastFloors[ ucLearnFloor ].ulPosition = GetPosition_PositionCount();
               }
               // Get next valid learn floor
               uint8_t ucNewLearnFloor = GetNextValidOpening( ucLearnFloor-1, DIR__DN );
               if( ucNewLearnFloor < GetFP_NumFloors() )
               {
                  // For express floors, place filler positions to bypass need to learn fault
                  for( uint8_t i = ucNewLearnFloor+1; i < ucLearnFloor; i++ )
                  {
                     uint32_t uiOffset = (2*MINIMUM_FLOOR_SPACING)*(ucLearnFloor-i);
                     gpastFloors[i].ulPosition = gpastFloors[ucLearnFloor].ulPosition - uiOffset;
                  }
               }

               /* Update current learn floor */
               ucLearnFloor = ucNewLearnFloor;

               /* Check if short floor */
               if( CheckIfShortFloor(ucLearnFloor) )
               {
                  /* Log short floors as a fixed offset from the previous floor */
                  gpastFloors[ucLearnFloor].ulPosition = gpastFloors[ucLearnFloor+1].ulPosition - QUARTER_INCH;

                  // Get next valid learn floor
                  uint8_t ucNewLearnFloor = GetNextValidOpening( ucLearnFloor-1, DIR__DN );
                  if( ucNewLearnFloor < GetFP_NumFloors() )
                  {
                     // For express floors, place filler positions to bypass need to learn fault
                     for( uint8_t i = ucNewLearnFloor+1; i < ucLearnFloor; i++ )
                     {
                        uint32_t uiOffset = (2*MINIMUM_FLOOR_SPACING)*(ucLearnFloor-i);
                        gpastFloors[i].ulPosition = gpastFloors[ucLearnFloor].ulPosition - uiOffset;
                     }
                  }

                  /* Update current learn floor */
                  ucLearnFloor = ucNewLearnFloor;
               }
            }
         }
         else
         {
            bDZ_Old = 0;
            if( bLogFallingEdge )
            {
               gpastFloors[GetFP_NumFloors()-1].ulPosition = GetPosition_PositionCount() + THREE_INCHES;
               bLogFallingEdge = 0;
            }
         }
      }
   }
   else
   {
      geLearnState = LEARN__NOT_ON_LEARN;
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Process_Complete( void )
{
   if ( GetOperation_ClassOfOp() == CLASSOP__SEMI_AUTO )
   {
      if ( GetOperation_LearnMode() != MODE_L__COMPLETE )
      {
         SetOperation_LearnMode(MODE_L__COMPLETE);
      }
      //------------------------------------------------------
      if ( geCompletedLearnMode == MODE_L__LEARNING_DHA )
      {
         StoreFloorPositionsInFlash( 0 );
      }
      else if ( geCompletedLearnMode == MODE_L__LEARNING_UHA )
      {
         StoreFloorPositionsInFlash( 1 );
      }
   }
   else
   {
      geLearnState = LEARN__NOT_ON_LEARN;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_ReadyState( void )
{
   if ( GetOperation_ClassOfOp() == CLASSOP__SEMI_AUTO )
   {
       SetOperation_BypassTermLimits( 1);
      if ( CheckIf_HoistwayLearn() )
      {
         if ( ( GetPosition_Velocity() == 0 )
           && ( getDoorZone(DOOR_ANY) ) )
         {
            //if floor levels arent learned, user may need to bypass term limits
            if(gbNeedToLearn && !Param_ReadValue_1Bit(enPARAM1__BypassTermLimits))
            {
               geLearnState = LEARN__READY;
               SetOperation_LearnMode(MODE_L__BYP_TERM_LIMIT_HA);
            }
            else
            {
               geLearnState = LEARN__READY;
               SetOperation_LearnMode( MODE_L__READY_HA);
            }
         }
         else
         {
            geLearnState = LEARN__NOT_ON_LEARN;

            SetOperation_LearnMode(MODE_L__GOTO_TERM_HA);
         }
      }
      else
      {
          SetOperation_LearnMode(MODE_L__NONE);
      }
   }
   else
   {
      geLearnState = LEARN__NOT_ON_LEARN;
   }
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Init_FloorsAndOpenings()
{
   uint32_t auiOpeningBitmapF[BITMAP32_SIZE(MAX_NUM_FLOORS)];
   uint32_t auiOpeningBitmapR[BITMAP32_SIZE(MAX_NUM_FLOORS)];

   for( uint8_t i = 0; i < BITMAP32_SIZE(MAX_NUM_FLOORS); i++ )
   {
      auiOpeningBitmapF[i] = Param_ReadValue_32Bit( enPARAM32__OpeningBitmapF_0 + i );
      auiOpeningBitmapR[i] = Param_ReadValue_32Bit( enPARAM32__OpeningBitmapR_0 + i );
   }

   for ( uint8_t ucFloor = 0; ucFloor < GetFP_NumFloors(); ucFloor++ )
   {
      uint8_t ucOpeningIndex = ucFloor/32;
      uint8_t ucBit = ucFloor%32;

      gpastFloors[ ucFloor ].bFrontOpening = ( auiOpeningBitmapF[ucOpeningIndex] >> ucBit ) & 1;

       if ( GetFP_RearDoors() )
       {
          gpastFloors[ ucFloor ].bRearOpening = ( auiOpeningBitmapR[ucOpeningIndex] >> ucBit ) & 1;
       }
       else
       {
          gpastFloors[ ucFloor ].bRearOpening = 0;
       }

       gpastFloors[ ucFloor ].ulPosition = Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 + ucFloor );
   }
}
/*-----------------------------------------------------------------------------
Update door commands
 -----------------------------------------------------------------------------*/
static void UpdateDoors(void)
{
   SetDoorCommand(DOOR_FRONT, DOOR_COMMAND__CLOSE);
   SetDoorCommand(DOOR_REAR, DOOR_COMMAND__CLOSE);
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
    pstThisModule->uwInitialDelay_1ms = 100;
    pstThisModule->uwRunPeriod_1ms = 100;

    guwThreeSeconds = 3000 / pstThisModule->uwRunPeriod_1ms;

    geLearnState = LEARN__NOT_ON_LEARN;

    SetOperation_LearnMode( MODE_L__UNKNOWN);

    Init_FloorsAndOpenings();

    return 0;
}

/*-----------------------------------------------------------------------------
   DIPA5 puts the car is hoistway learn mode.
   When on Hoistway learn, the mechanic must place the car on one of the terminal
   doorzones to initiate the learn. Once in a DZ, the mechanic must hold MR Insp
   UP or DN for 3 seconds to learn the hoistway from bottom to top or from top
   to bottom. The controller will then run at the learn speed counting DZs
   until it reaches the number of floors. At the end of the learn, the floor
   positions are shifted by 3 inches to save the center of the magnet instead of
   the edge, and then saved to EPROM.

   When not on learn, this module verifies the floor levels.

   Eight NTS/ETS points are now auto generated instead of being learned.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{

   if((geLearnState != LEARN__COMPLETE) && gstFault.bActiveFault && getDoorZone(DOOR_ANY)) //
   {
      geLearnState = LEARN__READY;
      SetOperation_LearnMode( MODE_L__READY_HA);
   }
   else if ( (geLearnState != LEARN__COMPLETE) && gstFault.bActiveFault)
   {
      geLearnState = LEARN__NOT_ON_LEARN;
      SetOperation_LearnMode(MODE_L__GOTO_TERM_HA);
   }
   else{
      switch ( geLearnState )
      {
         default:
         case LEARN__NOT_ON_LEARN:
            Process_NotOnLearn();
            break;

         case LEARN__READY:
            Update_ReadyState();
            Process_Ready();
            break;

         case LEARN__LEARNING:
            if(gstFault.bActiveFault)
            {
               geLearnState = LEARN__READY;
               SetOperation_LearnMode( MODE_L__READY_HA);
            }
            else
            {
               Process_Learning();
            }
            break;

         case LEARN__COMPLETE:
            Process_Complete();
            break;
      }

      if ( GetOperation_ClassOfOp() == CLASSOP__SEMI_AUTO )
      {
          if ( gstFault.bActiveFault
          || ( !CarDoorsClosed() ) )
          {
              SetOperation_MotionCmd( MOCMD__EMERG_STOP);
          }
          UpdateDoors();
      }
      else
      {
         ResetLearnState();
      }
      eLastLearnState = geLearnState;
   }
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
