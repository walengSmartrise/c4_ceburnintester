/******************************************************************************
 *
 * @file
 * @brief
 * @version  V1.00
 * @date
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "sru_a.h"
#include "sys.h"

#include "ring_buffer.h"
#include <stdlib.h>
#include <string.h>
#include "GlobalData.h"
#include "position.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

static uint8_t ExpData_GetOnlineFlag( uint8_t ucMasterIndex, uint8_t ucBoardIndex );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_CAN =
{
   .pfnInit = Init,
   .pfnRun = Run,
};
extern struct st_sdata_local gstCtlNetSData_Config;
extern struct st_sdata_control * gastSData_Nodes[ NUM_CTRL_NET_NODES ];
extern uint8_t gaucPerNodeNumDatagrams[ NUM_CTRL_NET_NODES ];
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define LIMIT_UNLOAD_CAN_CYCLES       (16)

#define EXTRACT_NET_ID(x)      (( x >> 25 ) & 0x0F )
#define EXTRACT_SRC_ID(x)      (( x >> 20 ) & 0x1F )
#define EXTRACT_DEST_ID(x)     (( x >> 15 ) & 0x1F )
#define EXTRACT_DATAGRAM_ID(x) (( x >> 10 ) & 0x1F )

#define MASTER_IO_OFFLINE_LIMIT_MS        (10000)
#define MARSHAL_NUM_EXP_MASTER_BOARDS 7
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *-----------------------------------------------------------------------------*/
static uint32_t uiOfflineCounterCTA_ms;
static uint32_t uiOfflineCounterCOPA_ms;

static uint32_t uiOfflineDelay_ms;

static uint16_t uwBusErrorCounter_CAN1;
static uint16_t uwBusErrorCounter_CAN2;

static uint16_t auwMasterOfflineTimers_ms[MARSHAL_NUM_EXP_MASTER_BOARDS] =
{
      RISER_OFFLINE_COUNT_MS__UNUSED,
      RISER_OFFLINE_COUNT_MS__UNUSED,
      RISER_OFFLINE_COUNT_MS__UNUSED,
      RISER_OFFLINE_COUNT_MS__UNUSED,
      RISER_OFFLINE_COUNT_MS__UNUSED,
      RISER_OFFLINE_COUNT_MS__UNUSED,
      RISER_OFFLINE_COUNT_MS__UNUSED,
};

static uint8_t ucMarshal_NUM_EXP_MASTER_BOARDS;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Returns bus error counter
 -----------------------------------------------------------------------------*/
uint16_t GetDebugBusOfflineCounter_CAN1()
{
   return uwBusErrorCounter_CAN1;
}
uint16_t GetDebugBusOfflineCounter_CAN2()
{
   return uwBusErrorCounter_CAN2;
}
/*-----------------------------------------------------------------------------
Returns 1 if no communication detected
 -----------------------------------------------------------------------------*/
uint8_t CheckIfOffline_CTA()
{
   uint8_t bOffline = (uiOfflineCounterCTA_ms >= uiOfflineDelay_ms) ? 1:0;
   return bOffline;
}
uint8_t CheckIfOffline_COPA()
{
   uint8_t bOffline = (uiOfflineCounterCOPA_ms >= uiOfflineDelay_ms) ? 1:0;
   return bOffline;
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_CAN_1MS;

   if( Param_ReadValue_1Bit(enPARAM1__EnableBrakeV2) )
   {
      CAN2_Init_BrakeV2();
   }
   else
   {
      CAN2_Init_BrakeV1();
   }

   uiOfflineCounterCOPA_ms = 0;
   uiOfflineCounterCTA_ms = 0;

   uint8_t ucOfflineDelay_5ms = Param_ReadValue_8Bit(enPARAM8__OfflineCtrlTimer_5ms);
   if(ucOfflineDelay_5ms < MIN_OFFLINE_DELAY_5MS)
   {
      ucOfflineDelay_5ms = MIN_OFFLINE_DELAY_5MS;
   }
   uiOfflineDelay_ms = ucOfflineDelay_5ms * 5U;
   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Receive_Packet(CAN_MSG_T *pstRxMsg)
{
   static un_sdata_datagram unDatagram;
   uint8_t  ucNetworkDatagramID       = ExtractFromCAN_DatagramID(pstRxMsg->ID);
   uint8_t  ucSourceID                = ExtractFromCAN_SourceID  (pstRxMsg->ID);
   uint16_t uwDestinationBitmask      = ExtractFromCAN_DestinationBitmap(pstRxMsg->ID);
   uint16_t uwLocalMask               = 1 << GetSystemNodeID();
   uint16_t uwSourceMask              = 1 << ucSourceID;
   uint16_t uwOtherDestinationBitmask = uwDestinationBitmask & ~(uwLocalMask) & ~(uwSourceMask);



   memcpy(&unDatagram, &pstRxMsg->Data, 8 );
   InsertSharedDatagram_ByDatagramID( &unDatagram, ucNetworkDatagramID);

   if(CheckIf_SendOnABNet(uwDestinationBitmask))
   {
      UART_LoadCANMessage(pstRxMsg);
   }
 }
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t Valid_CtlNet_Datagram(uint32_t messageID, struct st_sdata_local* sdataConfig)
{
    uint8_t result       = 0; // Assume false
    uint8_t ulNetID      = EXTRACT_NET_ID(messageID)      ;
    uint8_t ulSourceID   = EXTRACT_SRC_ID(messageID)      ;
    uint8_t ulDestID     = EXTRACT_DEST_ID(messageID)     ;
    uint8_t ulDatagramID = EXTRACT_DATAGRAM_ID(messageID) ;

    if ( ulNetID == sdataConfig->ucNetworkID )
    {
       if ( (ulDestID == 0x1F) || (ulDestID == sdataConfig->ucLocalNodeID))
       {
          if ( (ulSourceID  < sdataConfig->ucNumNodes) && (ulSourceID != sdataConfig->ucLocalNodeID))
          {
             if ( ulDatagramID < gaucPerNodeNumDatagrams[ ulSourceID ] )
             {
                 result = 1;
             }
          }
       }
    }

    return result;
}
/*----------------------------------------------------------------------------
// Read data from ring buffers till it is empty
 *----------------------------------------------------------------------------*/
static void UnloadCAN_MRA( void )
{
    static CAN_MSG_T tmp;
    // Car Net
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN1_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
         // Unload or forward packet
         Receive_Packet(&tmp);

         uint8_t ucSourceID = ExtractFromCAN_SourceID(tmp.ID);
         if ( ( ucSourceID == SYS_NODE__CTA )
           || ( ucSourceID == SYS_NODE__CTB ) )
         {
            uiOfflineCounterCTA_ms = 0;
         }
         if ( ( ucSourceID == SYS_NODE__COPA )
           || ( ucSourceID == SYS_NODE__COPB ) )
         {
            uiOfflineCounterCOPA_ms = 0;
         }
       }
    }

    // Ctrl Net
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN2_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
          un_sdata_datagram unDatagram;
          // Unload or forward packet
         // Make sure that the data is valid. If it is then
         // save it in our arra.
         if ( Valid_CtlNet_Datagram(tmp.ID, &gstCtlNetSData_Config) )
         {
              // TODO: this should be placed in the sdata_car_net module. Need a FIFO to
              // transmite the data here to tthe other task.
              memcpy( unDatagram.auc8, tmp.Data, sizeof( tmp.Data) );

              SDATA_WriteDatagram( gastSData_Nodes[ EXTRACT_SRC_ID(tmp.ID) ], EXTRACT_DATAGRAM_ID(tmp.ID), &unDatagram );
         }
         if( EXTRACT_SRC_ID(tmp.ID) == CTRL_NET__BRK )
         {
            if( EXTRACT_DATAGRAM_ID(tmp.ID) == DG_CtrlNet_BRK1_Data )
            {
               RTTLog_TimeStampEvent("B1");
               gstBrake.uwOfflineCounter_1ms = 0;
            }
            else if( EXTRACT_DATAGRAM_ID(tmp.ID) == DG_CtrlNet_BRK2_Data )
            {
               RTTLog_TimeStampEvent("B2");
               gstBrake2.uwOfflineCounter_1ms = 0;
            }
            else
            {
               RTTLog_TimeStampEvent("N1");
            }
         }
         else
         {
            RTTLog_TimeStampEvent("N2");
         }
       }
    }
}
/*-----------------------------------------------------------------------------
   Check for CTA or COPA board offline
 -----------------------------------------------------------------------------*/
static void CheckFor_BoardOffline()
{
   if( GetOperation_ManualMode() != MODE_M__CONSTRUCTION )
   {
      if(uiOfflineCounterCTA_ms < uiOfflineDelay_ms)
      {
         uiOfflineCounterCTA_ms += MOD_RUN_PERIOD_CAN_1MS;
      }
      else
      {
         SetFault(FLT__OFFLINE_CTA_BY_MRA);
      }

      if(uiOfflineCounterCOPA_ms < uiOfflineDelay_ms)
      {
         uiOfflineCounterCOPA_ms += MOD_RUN_PERIOD_CAN_1MS;
      }
      else
      {
         SetFault(FLT__OFFLINE_COPA_BY_MRA);
      }
   }
}
/*-----------------------------------------------------------------------------
   Check for Master or Slave board duplicated or Comm loss
 -----------------------------------------------------------------------------*/
static void CheckFor_IOBoardFault()
{
   for( uint8_t i = 0; i < ucMarshal_NUM_EXP_MASTER_BOARDS; i++ )
   {
      for( uint8_t j = 0; j < NUM_EXP_BOARDS_PER_MASTER; j++ )
      {
         if( ( !ExpData_GetOnlineFlag( i, 0 ) && ( GetExpansionError( i, j ) == 0x1 ) ) ||
              ( ExpData_GetOnlineFlag( i, 0 ) && ( GetExpansionError( i, j ) == 0xD ) ) )
         {
            SetFault(FLT__EXP_COM_1 + i);
         }
         if( GetExpansionError( i, j ) == 0xA )
         {
            SetFault(FLT__EXP_DIP_1 + i);
         }
      }
   }
}
/*-----------------------------------------------------------------------------
   Check for CAN Bus offline and reset if offline
 -----------------------------------------------------------------------------*/
static void CheckFor_CANBusOffline()
{
   if( Chip_CAN_GetGlobalStatus( LPC_CAN1 ) & CAN_GSR_BS ) // Bus offline if 1
   {
      Chip_CAN_SetMode( LPC_CAN1, CAN_RESET_MODE, ENABLE );
      __NOP();
      __NOP();
      __NOP();
      Chip_CAN_SetMode( LPC_CAN1, CAN_RESET_MODE, DISABLE );
      SetAlarm(ALM__MR_CAN_RESET_1);
   }

   if( Chip_CAN_GetGlobalStatus( LPC_CAN2 ) & CAN_GSR_BS ) // Bus offline if 1
   {
      Chip_CAN_SetMode( LPC_CAN2, CAN_RESET_MODE, ENABLE );
      __NOP();
      __NOP();
      __NOP();
      Chip_CAN_SetMode( LPC_CAN2, CAN_RESET_MODE, DISABLE );
      SetAlarm(ALM__MR_CAN_RESET_2);
   }
}
/*-----------------------------------------------------------------------------
   Updates the CAN bus error counters viewed via View Debug Data menus
 -----------------------------------------------------------------------------*/
static void UpdateDebugBusErrorCounter()
{
   static uint8_t ucLastCountRx_CAN1;
   static uint8_t ucLastCountTx_CAN1;
   static uint8_t ucLastCountRx_CAN2;
   static uint8_t ucLastCountTx_CAN2;
   uint32_t uiStatus = Chip_CAN_GetGlobalStatus( LPC_CAN1 );
   uint8_t ucCountRx = (uiStatus >> 16) & 0xFF;
   uint8_t ucCountTx = (uiStatus >> 24) & 0xFF;
   if( ( ucCountRx > ucLastCountRx_CAN1 )
    || ( ucCountTx > ucLastCountTx_CAN1 ))
   {
       uwBusErrorCounter_CAN1++;
   }
   ucLastCountRx_CAN1 = ucCountRx;
   ucLastCountTx_CAN1 = ucCountTx;

   uiStatus = Chip_CAN_GetGlobalStatus( LPC_CAN2 );
   ucCountRx = (uiStatus >> 16) & 0xFF;
   ucCountTx = (uiStatus >> 24) & 0xFF;
   if( ( ucCountRx > ucLastCountRx_CAN2 )
    || ( ucCountTx > ucLastCountTx_CAN2 ))
   {
       uwBusErrorCounter_CAN2++;
   }
   ucLastCountRx_CAN2 = ucCountRx;
   ucLastCountTx_CAN2 = ucCountTx;
}

static void CheckMarshalToEnableMoreExp()
{
   if( GetInputValue( enIN_MARSHAL_MODE ) || bMarshalModeStatus() )
   {
      ucMarshal_NUM_EXP_MASTER_BOARDS = 7;
   }
   else
   {
      ucMarshal_NUM_EXP_MASTER_BOARDS = 5;
   }
}

static void ExpData_UpdateMasterOfflineTimers( uint16_t uwRunPeriod_1ms )
{

   // IO Master 1
   if(GetDatagramDirtyBit(DATAGRAM_ID_29))
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_29);
      SetLocalInputs(GetExpansionInputs1_LocalInputs1(), IO_BOARDS__EXP1);
      SetLocalInputs(GetExpansionInputs1_LocalInputs2(), IO_BOARDS__EXP2);
      SetLocalInputs(GetExpansionInputs1_LocalInputs3(), IO_BOARDS__EXP3);
      SetLocalInputs(GetExpansionInputs1_LocalInputs4(), IO_BOARDS__EXP4);
      SetLocalInputs(GetExpansionInputs1_LocalInputs5(), IO_BOARDS__EXP5);
      SetLocalInputs(GetExpansionInputs1_LocalInputs6(), IO_BOARDS__EXP6);
      SetLocalInputs(GetExpansionInputs1_LocalInputs7(), IO_BOARDS__EXP7);
      SetLocalInputs(GetExpansionInputs1_LocalInputs8(), IO_BOARDS__EXP8);
      auwMasterOfflineTimers_ms[0] = 0;
   }
   else if( GetDatagramDirtyBit(DATAGRAM_ID_198) )
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_198);
      auwMasterOfflineTimers_ms[0] = 0;
   }

   // IO Master 2
   if(GetDatagramDirtyBit(DATAGRAM_ID_30))
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_30);
      SetLocalInputs(GetExpansionInputs2_LocalInputs1(), IO_BOARDS__EXP9);
      SetLocalInputs(GetExpansionInputs2_LocalInputs2(), IO_BOARDS__EXP10);
      SetLocalInputs(GetExpansionInputs2_LocalInputs3(), IO_BOARDS__EXP11);
      SetLocalInputs(GetExpansionInputs2_LocalInputs4(), IO_BOARDS__EXP12);
      SetLocalInputs(GetExpansionInputs2_LocalInputs5(), IO_BOARDS__EXP13);
      SetLocalInputs(GetExpansionInputs2_LocalInputs6(), IO_BOARDS__EXP14);
      SetLocalInputs(GetExpansionInputs2_LocalInputs7(), IO_BOARDS__EXP15);
      SetLocalInputs(GetExpansionInputs2_LocalInputs8(), IO_BOARDS__EXP16);
      auwMasterOfflineTimers_ms[1] = 0;
   }
   else if( GetDatagramDirtyBit(DATAGRAM_ID_199) )
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_199);
      auwMasterOfflineTimers_ms[1] = 0;
   }

   // IO Master 3
   if(GetDatagramDirtyBit(DATAGRAM_ID_31))
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_31);
      SetLocalInputs(GetExpansionInputs3_LocalInputs1(), IO_BOARDS__EXP17);
      SetLocalInputs(GetExpansionInputs3_LocalInputs2(), IO_BOARDS__EXP18);
      SetLocalInputs(GetExpansionInputs3_LocalInputs3(), IO_BOARDS__EXP19);
      SetLocalInputs(GetExpansionInputs3_LocalInputs4(), IO_BOARDS__EXP20);
      SetLocalInputs(GetExpansionInputs3_LocalInputs5(), IO_BOARDS__EXP21);
      SetLocalInputs(GetExpansionInputs3_LocalInputs6(), IO_BOARDS__EXP22);
      SetLocalInputs(GetExpansionInputs3_LocalInputs7(), IO_BOARDS__EXP23);
      SetLocalInputs(GetExpansionInputs3_LocalInputs8(), IO_BOARDS__EXP24);
      auwMasterOfflineTimers_ms[2] = 0;
   }
   else if( GetDatagramDirtyBit(DATAGRAM_ID_200) )
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_200);
      auwMasterOfflineTimers_ms[2] = 0;
   }

   // IO Master 4
   if(GetDatagramDirtyBit(DATAGRAM_ID_32))
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_32);
      SetLocalInputs(GetExpansionInputs4_LocalInputs1(), IO_BOARDS__EXP25);
      SetLocalInputs(GetExpansionInputs4_LocalInputs2(), IO_BOARDS__EXP26);
      SetLocalInputs(GetExpansionInputs4_LocalInputs3(), IO_BOARDS__EXP27);
      SetLocalInputs(GetExpansionInputs4_LocalInputs4(), IO_BOARDS__EXP28);
      SetLocalInputs(GetExpansionInputs4_LocalInputs5(), IO_BOARDS__EXP29);
      SetLocalInputs(GetExpansionInputs4_LocalInputs6(), IO_BOARDS__EXP30);
      SetLocalInputs(GetExpansionInputs4_LocalInputs7(), IO_BOARDS__EXP31);
      SetLocalInputs(GetExpansionInputs4_LocalInputs8(), IO_BOARDS__EXP32);
      auwMasterOfflineTimers_ms[3] = 0;
   }
   else if( GetDatagramDirtyBit(DATAGRAM_ID_201) )
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_201);
      auwMasterOfflineTimers_ms[3] = 0;
   }

   // IO Master 5
   if(GetDatagramDirtyBit(DATAGRAM_ID_33))
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_33);
      SetLocalInputs(GetExpansionInputs5_LocalInputs1(), IO_BOARDS__EXP33);
      SetLocalInputs(GetExpansionInputs5_LocalInputs2(), IO_BOARDS__EXP34);
      SetLocalInputs(GetExpansionInputs5_LocalInputs3(), IO_BOARDS__EXP35);
      SetLocalInputs(GetExpansionInputs5_LocalInputs4(), IO_BOARDS__EXP36);
      SetLocalInputs(GetExpansionInputs5_LocalInputs5(), IO_BOARDS__EXP37);
      SetLocalInputs(GetExpansionInputs5_LocalInputs6(), IO_BOARDS__EXP38);
      SetLocalInputs(GetExpansionInputs5_LocalInputs7(), IO_BOARDS__EXP39);
      SetLocalInputs(GetExpansionInputs5_LocalInputs8(), IO_BOARDS__EXP40);
      auwMasterOfflineTimers_ms[4] = 0;
   }
   else if( GetDatagramDirtyBit(DATAGRAM_ID_202) )
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_202);
      auwMasterOfflineTimers_ms[4] = 0;
   }

   if( GetInputValue( enIN_MARSHAL_MODE ) || bMarshalModeStatus() )
   {
      // IO Master 6
      if( GetDatagramDirtyBit(DATAGRAM_ID_34) || GetDatagramDirtyBit(DATAGRAM_ID_203) )
      {
         ClrDatagramDirtyBit(DATAGRAM_ID_34);
         ClrDatagramDirtyBit(DATAGRAM_ID_203);
         auwMasterOfflineTimers_ms[5] = 0;
      }
      // IO Master 7
      if( GetDatagramDirtyBit(DATAGRAM_ID_35) || GetDatagramDirtyBit(DATAGRAM_ID_204) )
      {
         ClrDatagramDirtyBit(DATAGRAM_ID_35);
         ClrDatagramDirtyBit(DATAGRAM_ID_204);
         auwMasterOfflineTimers_ms[6] = 0;
      }
   }

   for( uint8_t i = 0; i < ucMarshal_NUM_EXP_MASTER_BOARDS; i++ )
   {
      if( auwMasterOfflineTimers_ms[i] < MASTER_IO_OFFLINE_LIMIT_MS )
      {
         auwMasterOfflineTimers_ms[i] += uwRunPeriod_1ms;
      }
   }
}

static uint8_t ExpData_GetOnlineFlag( uint8_t ucMasterIndex, uint8_t ucBoardIndex )
{
   uint8_t bOnline = 0;
   if( auwMasterOfflineTimers_ms[ucMasterIndex] < MASTER_IO_OFFLINE_LIMIT_MS )
   {
      bOnline = GetExpansionOnlineFlag(ucMasterIndex, ucBoardIndex);
   }
   return bOnline;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   UnloadCAN_MRA();
   CheckMarshalToEnableMoreExp();
   if(!GetMotion_RunFlag() && getDoorZone(DOOR_ANY))
   {
      CheckFor_IOBoardFault();
   }
   CheckFor_BoardOffline();
   CheckFor_CANBusOffline();
   UpdateDebugBusErrorCounter();
   CAN1_FillHardwareBuffer();

   ExpData_UpdateMasterOfflineTimers(pstThisModule->uwRunPeriod_1ms);

   return 0;
}

