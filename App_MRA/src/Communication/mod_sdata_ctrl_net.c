/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     26, March 2016
 *
 * @note    TODO changed node ID to MRA & change for brakes as well
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sru_a.h"
#include "sys.h"


/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_SData_CtrlNet =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/* Below are all the nodes in Ctrl Network */
struct st_sdata_control gstSData_CtrlNet_MRA;
struct st_sdata_control gstSData_CtrlNet_TRC;
struct st_sdata_control gstSData_CtrlNet_BRK;

struct st_sdata_control * gastSData_Nodes[ NUM_CTRL_NET_NODES ] =
{
   &gstSData_CtrlNet_MRA,
   &gstSData_CtrlNet_TRC,
   &gstSData_CtrlNet_BRK,
};

uint8_t gaucPerNodeNumDatagrams[ NUM_CTRL_NET_NODES ] =
{
   NUM_CtrlNet_MRA_DATAGRAMS,
   NUM_CtrlNet_TRC_DATAGRAMS,
   NUM_CtrlNet_BRK_DATAGRAMS,
};
struct st_sdata_local gstCtlNetSData_Config;

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define RING_BUFFER_SIZE 32

#define EXTRACT_NET_ID(x)      (( x >> 25 ) & 0x0F )
#define EXTRACT_SRC_ID(x)      (( x >> 20 ) & 0x1F )
#define EXTRACT_DEST_ID(x)     (( x >> 15 ) & 0x1F )
#define EXTRACT_DATAGRAM_ID(x) (( x >> 10 ) & 0x1F )
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static CAN_MSG_T gstCAN_MSG_Tx;

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/


void SharedData_CtrlNet_Init( void )
{
   int iError;
   int i;

   //--------------------------------------------------------
   gstCtlNetSData_Config.ucNetworkID = SDATA_NET__CONTROL;

   gstCtlNetSData_Config.ucNumNodes = NUM_CTRL_NET_NODES;

   gstCtlNetSData_Config.ucLocalNodeID = CTRL_NET__TRC;

   gstCtlNetSData_Config.ucDestNodeID = 0x1F; /* ALL */

   //--------------------------------------------------------
   for ( i = 0; i < gstCtlNetSData_Config.ucNumNodes; ++i )
   {
      iError = SDATA_CreateDatagrams( gastSData_Nodes[ i ],
                                      gaucPerNodeNumDatagrams[ i ]
                                    );

      if ( iError )
      {
         while ( 1 )
         {
            ;  // TODO: unable to allocate shared data
         }
      }
   }
   gstCAN_MSG_Tx.DLC = 8;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Receive( void )
{
   //move to mod_can
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
int ResendBrakeDatagram( void )
{
   un_sdata_datagram unDatagram;
   uint8_t ucNodeID;
   uint8_t ucDatagramID;

   ucNodeID = gstCtlNetSData_Config.ucLocalNodeID;
   ucDatagramID = DG_CtrlNet_TRC__Brake1;

   SDATA_ReadDatagram( gastSData_Nodes[ ucNodeID ],
         ucDatagramID,
                       &unDatagram
                     );

   memcpy( gstCAN_MSG_Tx.Data, unDatagram.auc8, sizeof( unDatagram.auc8 ) );

   gstCAN_MSG_Tx.ID = (1 << 30) | (ucDatagramID << 10)
                      | (CTRL_NET__BRK << 15)
                      | (gstCtlNetSData_Config.ucLocalNodeID << 20)
                      | (gstCtlNetSData_Config.ucNetworkID << 25);


   CAN_BUFFER_ID_T   TxBuf = Chip_CAN_GetFreeTxBuf(LPC_CAN2);

   int iError = !Sys_CAN_Send(LPC_CAN2, TxBuf, &gstCAN_MSG_Tx);

   return iError;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
int ResendEBrakeDatagram( void )
{
   un_sdata_datagram unDatagram;
   uint8_t ucNodeID;
   uint8_t ucDatagramID;

   ucNodeID = gstCtlNetSData_Config.ucLocalNodeID;
   ucDatagramID = DG_CtrlNet_TRC__Brake2;

   SDATA_ReadDatagram( gastSData_Nodes[ ucNodeID ],
         ucDatagramID,
                       &unDatagram
                     );

   memcpy( gstCAN_MSG_Tx.Data, unDatagram.auc8, sizeof( unDatagram.auc8 ) );

   gstCAN_MSG_Tx.ID = (1 << 30) | (ucDatagramID << 10)
                      | (CTRL_NET__BRK << 15)
                      | (gstCtlNetSData_Config.ucLocalNodeID << 20)
                      | (gstCtlNetSData_Config.ucNetworkID << 25);


   CAN_BUFFER_ID_T   TxBuf = Chip_CAN_GetFreeTxBuf(LPC_CAN2);

   int iError = !Sys_CAN_Send(LPC_CAN2, TxBuf, &gstCAN_MSG_Tx);

   return iError;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define DG_TX_CYCLE_LIMIT (2)
static void Transmit( void )
{
   un_sdata_datagram unDatagram;
   int iError = 0;
   uint8_t ucNodeID;
   uint8_t ucDatagramID;
   uint16_t uwDatagramIndex_Plus1 = 0;
   ucNodeID = gstCtlNetSData_Config.ucLocalNodeID;
   uint8_t ucTXCycle;
   for( ucTXCycle = 0; ucTXCycle < DG_TX_CYCLE_LIMIT; ucTXCycle++)
   {
      uwDatagramIndex_Plus1 = SDATA_GetNextDatagramToSendIndex_Plus1( gastSData_Nodes[ ucNodeID ] );
      if(uwDatagramIndex_Plus1)
      {
         ucDatagramID = uwDatagramIndex_Plus1 - 1;
         if ( ucDatagramID < gaucPerNodeNumDatagrams[ ucNodeID ] )
         {
            SDATA_ReadDatagram( gastSData_Nodes[ ucNodeID ],
                                ucDatagramID,
                                &unDatagram
                              );

            memcpy( gstCAN_MSG_Tx.Data, unDatagram.auc8, sizeof( unDatagram.auc8 ) );
            if(ucDatagramID == DG_CtrlNet_TRC__Brake1)
            {
               gstCAN_MSG_Tx.ID = (1 << 30) | (ucDatagramID << 10)
                                  | (CTRL_NET__BRK << 15)
                                  | (gstCtlNetSData_Config.ucLocalNodeID << 20)
                                  | (gstCtlNetSData_Config.ucNetworkID << 25);
            }
            else if(ucDatagramID == DG_CtrlNet_TRC__Brake2)
            {
               gstCAN_MSG_Tx.ID = (1 << 30) | (ucDatagramID << 10)
                                  | (CTRL_NET__BRK << 15)
                                  | (gstCtlNetSData_Config.ucLocalNodeID << 20)
                                  | (gstCtlNetSData_Config.ucNetworkID << 25);
            }
            else
            {
               gstCAN_MSG_Tx.ID = (1 << 30) | (ucDatagramID << 10)
                                  | (gstCtlNetSData_Config.ucDestNodeID << 15)
                                  | (gstCtlNetSData_Config.ucLocalNodeID << 20)
                                  | (gstCtlNetSData_Config.ucNetworkID << 25);
            }

            CAN_BUFFER_ID_T   TxBuf = Chip_CAN_GetFreeTxBuf(LPC_CAN2);

            iError = !Sys_CAN_Send(LPC_CAN2, TxBuf, &gstCAN_MSG_Tx);
            // Return of 0 means error. Return of not 0 means success. This is opposite of our logic.
            if ( iError )
            {
               SDATA_DirtyBit_Set( gastSData_Nodes[ ucNodeID ], ucDatagramID );

               if ( gastSData_Nodes[ ucNodeID ]->uwLastSentIndex )
               {
                  --gastSData_Nodes[ ucNodeID ]->uwLastSentIndex;
               }
               else
               {
                  gastSData_Nodes[ ucNodeID ]->uwLastSentIndex = gastSData_Nodes[ ucNodeID ]->uiNumDatagrams - 1;
               }
               ucTXCycle = DG_TX_CYCLE_LIMIT;
               break;
            }
            else
            {
               if(ucDatagramID == DG_CtrlNet_TRC__Brake1)
               {
                  RTTLog_TimeStampEvent("M1");
               }
               else if(ucDatagramID == DG_CtrlNet_TRC__Brake2)
               {
                  RTTLog_TimeStampEvent("M2");
               }
            }
         }
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadData( void )
{
   un_sdata_datagram unDatagram;

   //--------------------------------------
   SDATA_ReadDatagram( &gstSData_CtrlNet_BRK,
                       DG_CtrlNet_BRK1_Data,
                       &unDatagram
                     );

   gstBrake.eState = unDatagram.auc8[ 0 ];

   gstBrake.eError = unDatagram.auc8[ 1 ];

   gstBrake.ucVoltageFeedback = unDatagram.auc8[ 2 ];
   gstBrake.bBPS_Feedback = unDatagram.auc8[ 3 ];

   gstBrake.ucMeasuredPickVoltage = unDatagram.auc8[ 4 ];

   gstBrake.ucMeasuredHoldVoltage = unDatagram.auc8[ 5 ];

   gstBrake.bBrakeVoltage_240VAC = unDatagram.auc8[ 7 ];

   //--------------------------------------
   SDATA_ReadDatagram( &gstSData_CtrlNet_BRK,
                       DG_CtrlNet_BRK2_Data,
                       &unDatagram
                     );

   gstBrake2.eState = unDatagram.auc8[ 0 ];

   gstBrake2.eError = unDatagram.auc8[ 1 ];

   gstBrake2.ucVoltageFeedback = unDatagram.auc8[ 2 ];
   gstBrake2.bBPS_Feedback = unDatagram.auc8[ 3 ];

   gstBrake2.ucMeasuredPickVoltage = unDatagram.auc8[ 4 ];

   gstBrake2.ucMeasuredHoldVoltage = unDatagram.auc8[ 5 ];

   gstBrake2.bBrakeVoltage_240VAC = unDatagram.auc8[ 7 ];
}



/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData( void )
{
   un_sdata_datagram unDatagram;
   //--------------------------------------
   memset(&unDatagram, 0, sizeof(un_sdata_datagram));
   unDatagram.auc8[0] = gstBrake.ucCommandByte;
   unDatagram.auc8[1] = gstBrake.ucRampUpTime_10ms;
   unDatagram.auc8[2] = gstBrake.ucRampDownTime_10ms;
   unDatagram.auc8[3] = gstBrake.ucPickTime_10ms;
   unDatagram.auc8[4] = gstBrake.ucPickVoltageCommand;
   unDatagram.auc8[5] = gstBrake.ucHoldVoltageCommand;
   unDatagram.auc8[7] = 0;
   SDATA_WriteDatagram( &gstSData_CtrlNet_TRC,
                        DG_CtrlNet_TRC__Brake1,
                        &unDatagram
                      );
   //--------------------------------------
   memset(&unDatagram, 0, sizeof(un_sdata_datagram));
   unDatagram.auc8[0] = gstBrake2.ucCommandByte;
   unDatagram.auc8[1] = gstBrake2.ucRampUpTime_10ms;
   unDatagram.auc8[2] = gstBrake2.ucRampDownTime_10ms;
   unDatagram.auc8[3] = gstBrake2.ucPickTime_10ms;
   unDatagram.auc8[4] = gstBrake2.ucPickVoltageCommand;
   unDatagram.auc8[5] = gstBrake2.ucHoldVoltageCommand;
   unDatagram.auc8[7] = 0;
   SDATA_WriteDatagram( &gstSData_CtrlNet_TRC,
                        DG_CtrlNet_TRC__Brake2,
                        &unDatagram
                      );
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   //------------------------------------------------
   pstThisModule->uwInitialDelay_1ms = 5;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_CTRL_NET_1MS;

   uint16_t uwBrakeDelay_5ms = Param_ReadValue_8Bit(enPARAM8__ResendBrakeTimer_5ms);
   if( uwBrakeDelay_5ms < MIN_BRAKE_HEARTBEAT_TIMER_5MS )
   {
      uwBrakeDelay_5ms = MIN_BRAKE_HEARTBEAT_TIMER_5MS;
   }
   else if( uwBrakeDelay_5ms > MAX_BRAKE_HEARTBEAT_TIMER_5MS )
   {
      uwBrakeDelay_5ms = MAX_BRAKE_HEARTBEAT_TIMER_5MS;
   }
   gastSData_Nodes[gstCtlNetSData_Config.ucLocalNodeID]->paiResendRate_1ms[DG_CtrlNet_TRC__Brake1] = uwBrakeDelay_5ms*5;
   gastSData_Nodes[gstCtlNetSData_Config.ucLocalNodeID]->paiResendRate_1ms[DG_CtrlNet_TRC__Brake2] = uwBrakeDelay_5ms*5;
   gastSData_Nodes[gstCtlNetSData_Config.ucLocalNodeID]->uwHeartbeatInterval_1ms = uwBrakeDelay_5ms*5;

   return 0;
}

/*-----------------------------------------------------------------------------

                  MSB   x
                        x     4 bits for Network ID (global enum)
                        x
                        x
                      -----
                        x
                        x
                        x    5 bits for Source Node ID (static enum to use less bits)
  29-bit CAN ID:        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Destination Node ID (0x1F = Broadcast to all)
                        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Datagram ID (global enum)
                        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Version Number (major)
                        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Version Number (minor)
                        x
                  LSB   x

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   // Check for receive data every time.
   Receive();
   UnloadData();
   //------------------------------------------------
   LoadData();
   Transmit();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

