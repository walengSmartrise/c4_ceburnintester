/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     26, March 2016
 *
 * @note    Load local datagrams and selects datagrams for transmit
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sru_a.h"
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_SData =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

struct st_sdata_control gstSData_LocalData;

char *pasVersion;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define DG_TX_CYCLE_LIMIT  (3)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static Sys_Nodes eSysNodeID;// network id
static uint8_t ucNumberOfDatagrams;// number of datagrams generated locally
static CAN_MSG_T gstCAN_MSG_Tx;

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Gets the last segment of the software version string for transmit on the group net
 -----------------------------------------------------------------------------*/
static void UpdateVersionString(void)
{
   uint8_t ucSize = sizeof(SOFTWARE_RELEASE_VERSION);
   uint8_t ucFirstIndex = 0;
   for(int8_t i = ucSize-1; i >= 0; i--)
   {
      char *ps = SOFTWARE_RELEASE_VERSION+i;
      if(*ps == '.')
      {
         ucFirstIndex = i+1;
         break;
      }
   }

   pasVersion = SOFTWARE_RELEASE_VERSION+ucFirstIndex;
}
/*-----------------------------------------------------------------------------
   Creates datagrams produced by this node
 -----------------------------------------------------------------------------*/
void SharedData_Init()
{
   int iError = 1;
   eSysNodeID = GetSystemNodeID();

   switch(eSysNodeID)
   {

      case SYS_NODE__MRA:
        ucNumberOfDatagrams = NUM_MRA_DATAGRAMS;
        break;

      default:
         break;

   }
   if(ucNumberOfDatagrams)
   {
      iError = SDATA_CreateDatagrams( &gstSData_LocalData,
                                      ucNumberOfDatagrams
                                     );
   }


   if ( iError )
   {
      while ( 1 )
      {
         ;  // TODO: unable to allocate shared data
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

static void LoadData( void )
{
      LoadData_MR();
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t CheckIf_SendOnABNet(uint16_t uwDestinationBitmap)
{
   uint16_t uwForwardingBitmap = NODE__MRB;
   uint8_t bReturn = 0;
   if(uwForwardingBitmap & uwDestinationBitmap)
   {
      bReturn = 1;
   }
   return bReturn;
}
/*-----------------------------------------------------------------------------
   If the datagram has any destination other than the source ID or its own ID,
   then forward it to the Car net
 -----------------------------------------------------------------------------*/
uint8_t CheckIf_SendOnCarNet( uint16_t uwDestinationBitmap )
{
   uint16_t uwForwardingBitmap = NODE_ALL_CORE & ~(NODE__MRA|NODE__MRB);
   uint8_t bReturn = 0;
   if(uwForwardingBitmap & uwDestinationBitmap)
   {
      bReturn = 1;
   }
   return bReturn;
}
/*----------------------------------------------------------------------------
   Encode CAN message ID
 *----------------------------------------------------------------------------*/
uint32_t CarNet_EncodeCANMessageID(   Datagram_ID eDatagram,
                                      enum en_car_net_nodes eSource,
                                      uint16_t uwDest )
{
   uint32_t uiID = CAN_EXTEND_ID_USAGE
                 | ( (eDatagram & 0x00FF) << 21 )
                 | ( (SDATA_NET__CAR & 0x0007) << 18 )
                 | ( (eSource & 0x000F) << 14 )
                 | ( (uwDest & 0x3FFF) );
   return uiID;
}
/*-----------------------------------------------------------------------------
 Loads local datagrams for transmit over Car Net and/or AB Net
 -----------------------------------------------------------------------------*/
static void Transmit( void )
{
   un_sdata_datagram unDatagram;
   int iError_AB = 0;
   int iError_CAN = 0;
   uint8_t ucLocalDatagramID;
   uint8_t ucNetworkID = SDATA_NET__CAR;
   uint16_t uwDatagramToSend_Plus1;
   uint8_t ucTXCycle;
   for( ucTXCycle = 0; ucTXCycle < DG_TX_CYCLE_LIMIT; ucTXCycle++)
   {
      uwDatagramToSend_Plus1 = SDATA_GetNextDatagramToSendIndex_Plus1( &gstSData_LocalData );
      if(uwDatagramToSend_Plus1)
      {
         ucLocalDatagramID = uwDatagramToSend_Plus1 - 1;

         if ( ucLocalDatagramID < ucNumberOfDatagrams )
         {
            SDATA_ReadDatagram( &gstSData_LocalData,
                                 ucLocalDatagramID,
                                &unDatagram
                              );

            Sys_Nodes eLocalNodeID = GetSystemNodeID();
            Datagram_ID eDatagramID = GetNetworkDatagramID_AnyBoard(ucLocalDatagramID);
            uint16_t uwDestinationBitmap = GetDatagramDestinationBitmap(eDatagramID);

            memcpy( gstCAN_MSG_Tx.Data, unDatagram.auc8, sizeof( unDatagram.auc8 ) );
            gstCAN_MSG_Tx.DLC = 8;
            gstCAN_MSG_Tx.Type = 0;
            gstCAN_MSG_Tx.ID = 1 << 30;
            gstCAN_MSG_Tx.ID |= eDatagramID << 21;
            gstCAN_MSG_Tx.ID |= (ucNetworkID & 0x07) << 18;
            gstCAN_MSG_Tx.ID |= (eLocalNodeID & 0x0F) << 14;
            gstCAN_MSG_Tx.ID |= uwDestinationBitmap;

            if( gbBackupSync )
            {
               eDatagramID = GetNetworkDatagramID_CTA( ucLocalDatagramID );
               if(  eDatagramID == DATAGRAM_ID_151 )
               {
                  gstCAN_MSG_Tx.ID = CarNet_EncodeCANMessageID( eDatagramID, SYS_NODE__CTA, 0x3fff);
               }
            }

            if(CheckIf_SendOnCarNet(uwDestinationBitmap))
            {
               iError_CAN = !CAN1_LoadToRB(&gstCAN_MSG_Tx);
            }

            if(!iError_CAN && CheckIf_SendOnABNet(uwDestinationBitmap))
            {
               iError_AB = !UART_LoadCANMessage(&gstCAN_MSG_Tx);
            }

            if ( iError_AB | iError_CAN )
            {
               SDATA_DirtyBit_Set( &gstSData_LocalData, ucLocalDatagramID );

               if ( gstSData_LocalData.uwLastSentIndex )
               {
                  --gstSData_LocalData.uwLastSentIndex;
               }
               else
               {
                  gstSData_LocalData.uwLastSentIndex = gstSData_LocalData.uiNumDatagrams - 1;
               }
               ucTXCycle = DG_TX_CYCLE_LIMIT;
               break;
            }
         }
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadData( void )
{
   UnloadData_MR();
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#if 0
static uint8_t CheckIfDatagramToIgnore( Datagram_ID eID )
{
   uint8_t bIgnore = 0;
   // Don't check local datagrams
   for( uint8_t i = 0; i < ucNumberOfDatagrams; i++ )
   {
      Datagram_ID eNetworkID = GetNetworkDatagramID_AnyBoard( i );
      if( eID == eNetworkID )
      {
         bIgnore = 1;
      }
   }

   if( ( eID >= DATAGRAM_ID_96 ) // DG_MRA__ParamMaster_BlockCRCs
         && ( eID <= DATAGRAM_ID_149 ) )// DG_MRA__ParamMaster_BlockCRCs54
   {
      bIgnore = 1;
   }
   else if( ( eID >= DATAGRAM_ID_45 )
         && ( eID <= DATAGRAM_ID_57 ) )// DG_MRA__MappedInputs1
   {
      bIgnore = 1;
   }
   else if( ( eID >= DATAGRAM_ID_181 ) // DG_MRA__ParamMaster_ParamValues2
         && ( eID <= DATAGRAM_ID_190 ) )// DG_MRA__Run_Log
   {
      bIgnore = 1;
   }
   else if( ( eID >= DATAGRAM_ID_29 )
         && ( eID <= DATAGRAM_ID_44 ) )// DG_EXP__LocalInputs
   {
      bIgnore = 1;
   }
   // if local datagram then ignore
   else if( ( eID >= DATAGRAM_ID_3 ) //DG_MRA__CPLD_Fault1
         && ( eID <= DATAGRAM_ID_6 ) )// DG_MRA__Fault1
   {
      bIgnore = 1;
   }
   else if( ( eID >= DATAGRAM_ID_161 ) // DG_MRA__LoggedFaults1
         && ( eID <= DATAGRAM_ID_163 ) )// DG_MRA__DebugStates
   {
      bIgnore = 1;
   }
   else if( ( eID >= DATAGRAM_ID_17 )
         && ( eID <= DATAGRAM_ID_21 ) )// DG_MRA__Operation1
   {
      bIgnore = 1;
   }
   else if( ( eID >= DATAGRAM_ID_70 )
         && ( eID <= DATAGRAM_ID_73 ) )// DG_MRA__CarCalls1
   {
      bIgnore = 1;
   }
   else if( ( eID >= DATAGRAM_ID_158 ) // DG_MRA__ParamMaster_BlockCRCs55
         && ( eID <= DATAGRAM_ID_160 ) )// DG_MRA__ParamMaster_BlockCRCs57
   {
      bIgnore = 1;
   }
   else if( ( eID >= DATAGRAM_ID_84 )
         && ( eID <= DATAGRAM_ID_85 ) )// DG_MRA__Alarm1
   {
      bIgnore = 1;
   }
   else if( ( eID >= DATAGRAM_ID_193 )
         && ( eID <= DATAGRAM_ID_194 ) )// DG_MRA__BrakeData
   {
      bIgnore = 1;
   }
   else if( ( eID >= DATAGRAM_ID_196 ) // DG_MRA__ParamMaster_BlockCRCs59
         && ( eID <= DATAGRAM_ID_197 ) )// DG_MRA__ParamMaster_BlockCRCs60
   {
      bIgnore = 1;
   }
   else if( eID == DATAGRAM_ID_180 ) // DG_MRA__ParamMaster_BlockCRCs58
   {
      bIgnore = 1;
   }
   else if( eID == DATAGRAM_ID_25 )
   {
      bIgnore = 1;
   }
   return bIgnore;
}
/*-----------------------------------------------------------------------------
//FAULT__DATAGRAM_EXPIRED
   // If datagram should be received
   // If datagram is being monitored (&not local)
   // If lifetime has expired
   // Then fault
 -----------------------------------------------------------------------------*/
static void CheckFor_DatagramExpiredFault()
{
   for( Datagram_ID eID = DATAGRAM_ID_3; eID < INUSE_DATAGRAM_IDs; eID++ )
   {
      if( ( GetDatagramDestinationBitmap( eID ) & ( 1 << GetSystemNodeID() ) )
       && ( GetDatagramLifetime_ms( eID ) != IGNORED_DATAGRAM ) )
      {
         if( CheckDatagramLifeTimer_ms( eID, MOD_RUN_PERIOD_CAR_NET_1MS ) )
         {
//            if( !CheckIfDatagramToIgnore( eID ) )
//            {
               SetFault( FAULT__DATAGRAM_EXPIRED, eID );
//            }
         }
      }
   }
}
#endif
/*-----------------------------------------------------------------------------
   Set datagram resend rates
 -----------------------------------------------------------------------------*/
static void Initialize_DatagramSendRates()
{
   uint8_t ucOfflineDelay_5ms = Param_ReadValue_8Bit(enPARAM8__OfflineCtrlTimer_5ms);
   if(ucOfflineDelay_5ms < MIN_OFFLINE_DELAY_5MS)
   {
      ucOfflineDelay_5ms = MIN_OFFLINE_DELAY_5MS;
   }

   uint16_t uwHeartbeatInterval_1ms = (ucOfflineDelay_5ms*5)/3;
   gstSData_LocalData.uwHeartbeatInterval_1ms = uwHeartbeatInterval_1ms;

   for( uint8_t i = 0; i < ucNumberOfDatagrams; i++ )
   {
      Datagram_ID eID = GetNetworkDatagramID_AnyBoard( i );
      uint16_t uwLifetime_ms = GetDatagramLifetime_ms( eID );
      if( uwLifetime_ms != IGNORED_DATAGRAM )
      {
         gstSData_LocalData.paiResendRate_1ms[ i ] = uwLifetime_ms/2;
      }
   }

   gstSData_LocalData.paiResendRate_1ms[DG_MRA__LoggedFaults1] = DISABLED_DATAGRAM_RESEND_RATE_MS;
   gstSData_LocalData.paiResendRate_1ms[DG_MRA__LoggedFaults2] = DISABLED_DATAGRAM_RESEND_RATE_MS;
   gstSData_LocalData.paiResendRate_1ms[DG_MRA__LoggedFaults3] = DISABLED_DATAGRAM_RESEND_RATE_MS;
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   //------------------------------------------------
   pstThisModule->uwInitialDelay_1ms = 5;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_CAR_NET_1MS;

   Initialize_DatagramSendRates();

   UpdateVersionString();

   return 0;
}

/*-----------------------------------------------------------------------------

                  MSB   x
                        x     4 bits for Network ID (global enum)
                        x
                        x
                      -----
                        x
                        x
                        x    5 bits for Source Node ID (static enum to use less bits)
  29-bit CAN ID:        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Destination Node ID (0x1F = Broadcast to all)
                        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Datagram ID (global enum)
                        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Version Number (major)
                        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Version Number (minor)
                        x
                  LSB   x

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
#if 0
   CheckFor_DatagramExpiredFault();
#endif
   LoadData();
   Transmit();

   // Polls ring buffer for data and loads into CAN hardware buffer
   CAN1_FillHardwareBuffer();

   UnloadData();
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
