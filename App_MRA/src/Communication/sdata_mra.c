/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     26, March 2016
 *
 * @note    Load local datagrams and selects datagrams for transmit
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sru_a.h"
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
#include "motion.h"
#include "acceptance.h"
#include "config_system.h"
#include "fram.h"
#include "drive.h"
#include "remoteCommands.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
   Load brake data
 -----------------------------------------------------------------------------*/
static void Load_BrakeData()
{
   un_sdata_datagram unDatagram;
   //----------------------------------------------
   memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
   unDatagram.auc8[0] = gstBrake.eState;
   unDatagram.auc8[1] = gstBrake.eError;
   unDatagram.auc8[2] = gstBrake.ucVoltageFeedback;
   // Can be removed when input status update tool is made
   unDatagram.auc8[3] = ( CheckIfInputIsProgrammed(enIN_BRAKE1_BPS) )
                      ? ( GetInputValue(enIN_BRAKE1_BPS) )
                      : ( gstBrake.bBPS_Feedback );
   unDatagram.auc8[4] = gstBrake.ucMeasuredPickVoltage;
   unDatagram.auc8[5] = gstBrake.ucMeasuredHoldVoltage;
   unDatagram.auc8[7] = gstBrake.bBrakeVoltage_240VAC;

   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__BrakeData,
                        &unDatagram );

   //----------------------------------------------
   if(Param_ReadValue_1Bit(enPARAM1__EnableSecondaryBrake))
   {
      gstSData_LocalData.paiResendRate_1ms[DG_MRA__EBrakeData] = DEFAULT_DATAGRAM_RESEND_RATE_1MS;
      memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
      unDatagram.auc8[0] = gstBrake2.eState;
      unDatagram.auc8[1] = gstBrake2.eError;
      unDatagram.auc8[2] = gstBrake2.ucVoltageFeedback;
      // Can be removed when input status update tool is made
      unDatagram.auc8[3] = ( CheckIfInputIsProgrammed(enIN_BRAKE2_BPS) )
                         ? ( GetInputValue(enIN_BRAKE2_BPS) )
                         : ( gstBrake2.bBPS_Feedback );
      unDatagram.auc8[4] = gstBrake2.ucMeasuredPickVoltage;
      unDatagram.auc8[5] = gstBrake2.ucMeasuredHoldVoltage;
      unDatagram.auc8[7] = gstBrake2.bBrakeVoltage_240VAC;

      SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__EBrakeData,
                           &unDatagram );
   }
   else
   {
      memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
      SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__EBrakeData,
                           &unDatagram );
      gstSData_LocalData.paiResendRate_1ms[DG_MRA__EBrakeData] = DISABLED_DATAGRAM_RESEND_RATE_MS;
   }
}
/*-----------------------------------------------------------------------------
   On request, start sending run log packet
      State 0: Idle
      State 1: Send Transition points
      State 2: Send Accel points
      State 3: Send Decel points
      State 4: End of decel
 -----------------------------------------------------------------------------*/
//Delay between log transmissions to allow data to accumulate on MRB and then transmit to BBB
#define RUN_LOG_TRANSMISSION_DELAY_5MS          (2000)
static void LoadDatagram_RunLog()
{
   static uint32_t uiIndex;
   static uint32_t uiTransmitDelayCountdown_5ms;
   static uint8_t ucState;
   static uint8_t ucLastState;
   static uint8_t ucResendCounter;

   uint8_t bFirst = 0;
   static un_sdata_datagram unDatagram;
   Run_Log_Item stRunLogItem;

   if( ( Param_ReadValue_1Bit( enPARAM1__DEBUG_TransmitRunLog ) )
    && ( GetOperation_ClassOfOp() == CLASSOP__AUTO ) )
   {
      gstSData_LocalData.paiResendRate_1ms[DG_MRA__Run_Log] = DEFAULT_DATAGRAM_RESEND_RATE_1MS;
      if( !SDATA_DirtyBit_Get( &gstSData_LocalData, DG_MRA__Run_Log ) )
      {
         if(++ucResendCounter >= Param_ReadValue_8Bit(enPARAM8__NumResendRunLog))
         {
            ucResendCounter = 0;
            /* If state has changed since last cycle, run different code set */
            if( ucLastState != ucState )
            {
               ucLastState = ucState;
               uiIndex = 0;
               bFirst = 1;
            }

            memset( &unDatagram, 0, sizeof( un_sdata_datagram ) );
            switch( ucState )
            {
               case 0:
                  if( uiTransmitDelayCountdown_5ms )
                  {
                     uiTransmitDelayCountdown_5ms--;
                  }
                  /* If new log is available, then transmit to MRB */
                  if( ( GetRunLog_NewLogFlag() )
                   && ( !uiTransmitDelayCountdown_5ms ))
                  {
                     uiTransmitDelayCountdown_5ms = RUN_LOG_TRANSMISSION_DELAY_5MS;
                     ucState = 1;
                  }
                  break;

               case 1:
                  if( bFirst )
                  {
                     unDatagram.auc8[0] = 'S'; //Start of frame command
                     unDatagram.auc8[1] = 'O';
                     unDatagram.auc8[2] = 'T';
                     unDatagram.aui32[1] = NUM_TPs;
                     unDatagram.auc8[7] = Param_ReadValue_8Bit( enPARAM8__Debug_RunLogScaling );
                  }
                  else
                  {
                     unDatagram.auc8[0] = uiIndex & 0xFF;
                     unDatagram.aui32[1] = GetRunLog_TransitionPointTimestamp( uiIndex );
                     if( ++uiIndex >= NUM_TPs )
                     {
                        ucState = 2;
                     }
                  }
                  break;

               case 2:
                  if( bFirst )
                  {
                     unDatagram.auc8[0] = 'S'; //Start of Accel
                     unDatagram.auc8[1] = 'O';
                     unDatagram.auc8[2] = 'A';
                     unDatagram.aui32[1] = GetRunLog_AccelLogSize();
                  }
                  else
                  {
                     if( GetRunLog_AccelLogItem( &stRunLogItem, uiIndex ) )
                     {
                        unDatagram.auw16[0] = stRunLogItem.wCmdSpeed;
                        unDatagram.auw16[1] = stRunLogItem.wCarSpeed;
                        unDatagram.aui32[1] = stRunLogItem.uiCarPosition;
                        unDatagram.auc8[7] = uiIndex & 0xFF;
                     }

                     if( ++uiIndex >= GetRunLog_AccelLogSize() )
                     {
                        ucState = 3;
                     }
                  }
                  break;

               case 3:
                  if( bFirst )
                  {
                     unDatagram.auc8[0] = 'S'; //Start of Decel
                     unDatagram.auc8[1] = 'O';
                     unDatagram.auc8[2] = 'D';
                     unDatagram.aui32[1] = GetRunLog_DecelLogSize();
                  }
                  else
                  {
                     if( GetRunLog_DecelLogItem( &stRunLogItem, uiIndex ) )
                     {
                        unDatagram.auw16[0] = stRunLogItem.wCmdSpeed;
                        unDatagram.auw16[1] = stRunLogItem.wCarSpeed;
                        unDatagram.aui32[1] = stRunLogItem.uiCarPosition;
                        unDatagram.auc8[7] = uiIndex & 0xFF;
                     }

                     if( ++uiIndex >= GetRunLog_DecelLogSize() )
                     {
                        ucState = 4;
                     }
                  }
                  break;

               case 4:
                  if( bFirst )
                  {
                     unDatagram.auc8[0] = 'E'; //End of Decel
                     unDatagram.auc8[1] = 'O';
                     unDatagram.auc8[2] = 'D';
                     unDatagram.aui32[1] = 0;
                  }
                  else
                  {
                     if(!GetRunLog_NewLogFlag())
                     {
                        ucState = 0;
                     }
                     ClrRunLog_NewLogFlag();
                  }
                  break;

               default:
                  ucState = 0;
                  break;
            }
         }
         SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__Run_Log, &unDatagram );
         if( ucState )
         {
            SDATA_DirtyBit_Set( &gstSData_LocalData, DG_MRA__Run_Log );
         }
      }
   }
   else
   {
      gstSData_LocalData.paiResendRate_1ms[DG_MRA__Run_Log] = DISABLED_DATAGRAM_RESEND_RATE_MS;
      unDatagram.aui32[0] = 0;
      unDatagram.aui32[1] = 0;
      SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__Run_Log, &unDatagram );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Load_MappedIO()
{
   un_sdata_datagram unDatagram;
   //-------------------------------------------------
   unDatagram.aui32[0] = GetInputBitMap( 0 );
   unDatagram.aui32[1] = GetInputBitMap( 1 );
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__MappedInputs1, &unDatagram );
   //-------------------------------------------------
   unDatagram.aui32[0] = GetInputBitMap( 2 );
   unDatagram.aui32[1] = GetInputBitMap( 3 );
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__MappedInputs2, &unDatagram );
   //-------------------------------------------------
   unDatagram.aui32[0] = GetInputBitMap( 4 );
   unDatagram.aui32[1] = GetInputBitMap( 5 );
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__MappedInputs3, &unDatagram );
   //-------------------------------------------------
   unDatagram.aui32[0] = GetInputBitMap( 6 );
   unDatagram.aui32[1] = GetInputBitMap( 7 );
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__MappedInputs4, &unDatagram );
   //-------------------------------------------------
   unDatagram.aui32[0] = GetOutputBitMap( 0 );
   unDatagram.aui32[1] = GetOutputBitMap( 1 );
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__MappedOutputs1, &unDatagram );
   //-------------------------------------------------
   unDatagram.aui32[0] = GetOutputBitMap( 2 );
   unDatagram.aui32[1] = GetOutputBitMap( 3 );
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__MappedOutputs2, &unDatagram );

}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Load_LocalOutputs()
{
   un_sdata_datagram unDatagram;
   //----------------------------------------------
   memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
   unDatagram.auw16[0] = GetLocalOutputs( IO_BOARDS__CTA );
   unDatagram.auw16[1] = GetLocalOutputs( IO_BOARDS__COP );
   // Both riser and machine room have only 8 outputs
   unDatagram.auc8[6] = GetLocalOutputs( IO_BOARDS__MRA ) & 0xFF;
   unDatagram.auc8[7] = GetLocalOutputs( IO_BOARDS__RISER1 ) & 0xFF;

   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__LocalOutputs1, /* MRA + CTA + COP */
                        &unDatagram );

   //---------------------------------
   memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
   unDatagram.auc8[0] = GetLocalOutputs( IO_BOARDS__RISER1 );
   unDatagram.auc8[1] = GetLocalOutputs( IO_BOARDS__RISER2 );
   unDatagram.auc8[2] = GetLocalOutputs( IO_BOARDS__RISER3 );
   unDatagram.auc8[3] = GetLocalOutputs( IO_BOARDS__RISER4 );
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__RiserBoardOutputs, /* RIS */
                        &unDatagram );
   //---------------------------------
   memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
   unDatagram.auc8[0] = GetLocalOutputs( IO_BOARDS__EXP1 );
   unDatagram.auc8[1] = GetLocalOutputs( IO_BOARDS__EXP2 );
   unDatagram.auc8[2] = GetLocalOutputs( IO_BOARDS__EXP3 );
   unDatagram.auc8[3] = GetLocalOutputs( IO_BOARDS__EXP4 );
   unDatagram.auc8[4] = GetLocalOutputs( IO_BOARDS__EXP5 );
   unDatagram.auc8[5] = GetLocalOutputs( IO_BOARDS__EXP6 );
   unDatagram.auc8[6] = GetLocalOutputs( IO_BOARDS__EXP7);
   unDatagram.auc8[7] = GetLocalOutputs( IO_BOARDS__EXP8 );
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__LocalOutputs2, /* EXP1-8 */
                        &unDatagram );
   //---------------------------------
   memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
   unDatagram.auc8[0] = GetLocalOutputs( IO_BOARDS__EXP9 );
   unDatagram.auc8[1] = GetLocalOutputs( IO_BOARDS__EXP10 );
   unDatagram.auc8[2] = GetLocalOutputs( IO_BOARDS__EXP11 );
   unDatagram.auc8[3] = GetLocalOutputs( IO_BOARDS__EXP12 );
   unDatagram.auc8[4] = GetLocalOutputs( IO_BOARDS__EXP13 );
   unDatagram.auc8[5] = GetLocalOutputs( IO_BOARDS__EXP14 );
   unDatagram.auc8[6] = GetLocalOutputs( IO_BOARDS__EXP15 );
   unDatagram.auc8[7] = GetLocalOutputs( IO_BOARDS__EXP16 );
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__LocalOutputs3, /* EXP9-16 */
                        &unDatagram );
   //---------------------------------
   memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
   unDatagram.auc8[0] = GetLocalOutputs( IO_BOARDS__EXP17 );
   unDatagram.auc8[1] = GetLocalOutputs( IO_BOARDS__EXP18 );
   unDatagram.auc8[2] = GetLocalOutputs( IO_BOARDS__EXP19 );
   unDatagram.auc8[3] = GetLocalOutputs( IO_BOARDS__EXP20 );
   unDatagram.auc8[4] = GetLocalOutputs( IO_BOARDS__EXP21 );
   unDatagram.auc8[5] = GetLocalOutputs( IO_BOARDS__EXP22 );
   unDatagram.auc8[6] = GetLocalOutputs( IO_BOARDS__EXP23 );
   unDatagram.auc8[7] = GetLocalOutputs( IO_BOARDS__EXP24 );
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__LocalOutputs4, /* EXP17-24 */
                        &unDatagram );
   //---------------------------------
   memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
   unDatagram.auc8[0] = GetLocalOutputs( IO_BOARDS__EXP25 );
   unDatagram.auc8[1] = GetLocalOutputs( IO_BOARDS__EXP26 );
   unDatagram.auc8[2] = GetLocalOutputs( IO_BOARDS__EXP27 );
   unDatagram.auc8[3] = GetLocalOutputs( IO_BOARDS__EXP28 );
   unDatagram.auc8[4] = GetLocalOutputs( IO_BOARDS__EXP29 );
   unDatagram.auc8[5] = GetLocalOutputs( IO_BOARDS__EXP30 );
   unDatagram.auc8[6] = GetLocalOutputs( IO_BOARDS__EXP31 );
   unDatagram.auc8[7] = GetLocalOutputs( IO_BOARDS__EXP32 );
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__LocalOutputs5, /* EXP25-32 */
                        &unDatagram );
   //---------------------------------
   memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
   unDatagram.auc8[0] = GetLocalOutputs( IO_BOARDS__EXP33 );
   unDatagram.auc8[1] = GetLocalOutputs( IO_BOARDS__EXP34 );
   unDatagram.auc8[2] = GetLocalOutputs( IO_BOARDS__EXP35 );
   unDatagram.auc8[3] = GetLocalOutputs( IO_BOARDS__EXP36 );
   unDatagram.auc8[4] = GetLocalOutputs( IO_BOARDS__EXP37 );
   unDatagram.auc8[5] = GetLocalOutputs( IO_BOARDS__EXP38 );
   unDatagram.auc8[6] = GetLocalOutputs( IO_BOARDS__EXP39 );
   unDatagram.auc8[7] = GetLocalOutputs( IO_BOARDS__EXP40 );
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__LocalOutputs6, /* EXP33-40 */
                        &unDatagram );
   //---------------------------------
   if ( bMarshalModeStatus() || GetInputValue(enIN_MARSHAL_MODE) )
   {
   //Marshal 1-61 F
   memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
   unDatagram.auc8[0] = Get_Exp41CCLOutputs_F();
   unDatagram.auc8[1] = Get_Exp42CCLOutputs_F();
   unDatagram.auc8[2] = Get_Exp43CCLOutputs_F();
   unDatagram.auc8[3] = Get_Exp44CCLOutputs_F();
   unDatagram.auc8[4] = Get_Exp45CCLOutputs_F();
   unDatagram.auc8[5] = Get_Exp46CCLOutputs_F();
   unDatagram.auc8[6] = Get_Exp47CCLOutputs_F();
   unDatagram.auc8[7] = Get_Exp48CCLOutputs_F();
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__LocalOutputs7, /* EXP41-48 */
                        &unDatagram );

   //Marshal 1-61 R
   memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
   unDatagram.auc8[0] = Get_Exp49CCLOutputs_R();
   unDatagram.auc8[1] = Get_Exp50CCLOutputs_R();
   unDatagram.auc8[2] = Get_Exp51CCLOutputs_R();
   unDatagram.auc8[3] = Get_Exp52CCLOutputs_R();
   unDatagram.auc8[4] = Get_Exp53CCLOutputs_R();
   unDatagram.auc8[5] = Get_Exp54CCLOutputs_R();
   unDatagram.auc8[6] = Get_Exp55CCLOutputs_R();
   unDatagram.auc8[7] = Get_Exp56CCLOutputs_R();
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__LocalOutputs8, /* EXP49-56 */
                        &unDatagram );
   }
}

static void  Load_DefaultAll( void )
{
    un_sdata_datagram unDatagram;
    memset( &unDatagram, 0, sizeof(un_sdata_datagram) );

    unDatagram.aui32[0] = GetDefaultAllParameterIndex();
    unDatagram.aui32[1] = GetDefaultAllState();

    SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__DefaultAll, &unDatagram );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Load_Debug()
{
   un_sdata_datagram unDatagram;
   memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
   unDatagram.auc8[0] = Auto_GetState();
   unDatagram.auc8[1] = Learn_GetState();
   unDatagram.auc8[2] = GetRecallState();
   unDatagram.auc8[3] = Auto_CW_GetState();
   unDatagram.auc8[4] = Fire_GetFireService();
   unDatagram.auc8[5] = Fire_GetFireService_2_State();
   unDatagram.auc8[6] = GetAcceptanceTestStatus(GetUIRequest_AcceptanceTestStatus_MRB());
   unDatagram.auc8[7] = GetActiveAcceptanceTestState();
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__DebugStates, &unDatagram );
   //--------------------------------------------------
   memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
   unDatagram.auc8[0] = Motion_GetStartState();
   unDatagram.auc8[1] = Motion_GetStopState();
   unDatagram.auc8[2] = Motion_GetMotionState();
   unDatagram.auc8[3] = Motion_GetPatternState();

   switch( GetUIRequest_ViewDebugDataCommand() )
   {
      case VDD__MR_CAN1:
         unDatagram.auc8[5] = Debug_CAN_GetUtilization(0);
         unDatagram.auw16[3] = GetDebugBusOfflineCounter_CAN1();
         break;
      case VDD__MR_CAN2:
         unDatagram.auc8[5] = Debug_CAN_GetUtilization(1);
         unDatagram.auw16[3] = GetDebugBusOfflineCounter_CAN2();
         break;
      case VDD__MR_A_NET:
         unDatagram.auw16[3] = UART_GetRxErrorCount();
         break;
      case VDD__MR_RS485:
         unDatagram.auw16[3] = Drive_GetRxErrorCount();
         break;
      case VDD__RUN_SIGNALS:
         unDatagram.auw16[3]  = ( GetOperation_MotionCmd() == MOCMD__GOTO_DEST ) << RSF__GO_TO_DEST;
         unDatagram.auw16[3] |= ( CarDoorsClosed() & 1 ) << RSF__DOOR;
         unDatagram.auw16[3] |= ( AllHallDoorsClosed() & 1 ) << RSF__LOCK;
         unDatagram.auw16[3] |= ( GetMotion_RunFlag() & 1 ) << RSF__RUN;
         unDatagram.auw16[3] |= ( GetDriveHWEnableFlag() & 1 ) << RSF__HW_ENABLE;
         unDatagram.auw16[3] |= ( GetPickMFlag() & 1 ) << RSF__PICK_M;
         unDatagram.auw16[3] |= ( !GetInputValue(enIN_MMC) & 1 ) << RSF__MMC;
         unDatagram.auw16[3] |= ( GetPickDriveFlag() & 1 ) << RSF__PICK_DRIVE;
         unDatagram.auw16[3] |= ( GetInputValue(enIN_MBC) & 1 ) << RSF__MBC;
         unDatagram.auw16[3] |= ( gstDrive.bSpeedRegRelease & 1 ) << RSF__SERIAL_SPEED_REG;
         unDatagram.auw16[3] |= ( GetPickBrakeFlag() & 1 ) << RSF__BRAKE_PICK;
         unDatagram.auw16[3] |= ( Brake_GetBPS() & 1 ) << RSF__BPS;
         unDatagram.auw16[3] |= ( GetPickEBrakeFlag() & 1 ) << RSF__EBRAKE_PICK;
         unDatagram.auw16[3] |= ( Brake2_GetBPS() & 1 ) << RSF__EBPS;
         unDatagram.auw16[3] |= ( GetMotion_SpeedCommand() != 0 ) << RSF__COMMAND;
         unDatagram.auw16[3] |= ( GetPosition_Velocity() != 0 ) << RSF__FEEDBACK;
         break;
      case VDD__LAST_STOP_POS:
         unDatagram.aui32[1] = Motion_GetLastStopPosition();
         break;
      case VDD__MRA_VERSION:
         unDatagram.auc8[4] = *(pasVersion+0);
         unDatagram.auc8[5] = *(pasVersion+1);
         unDatagram.auc8[6] = *(pasVersion+2);
         unDatagram.auc8[7] = *(pasVersion+3);
         break;
      case VDD__DIR_CHANGE_COUNT:
         unDatagram.aui32[1] = FRAM_GetDirChangeCount();
         break;
      case VDD__DESTINATION_1:
         unDatagram.auw16[2] = Dispatch_GetDebugDestination_Current();
         unDatagram.auw16[3] = Dispatch_GetDebugDestination_Next_SameDir();
         break;
      case VDD__DESTINATION_2:
         unDatagram.auw16[2] = Dispatch_GetDebugDestination_Next_DiffDir();
         unDatagram.auc8[6] = InDestinationDoorzone();
         unDatagram.auc8[7] = 0;
         break;
      case VDD__IDLE_TIME:
         unDatagram.aui32[1] = ( IdleTime_GetParkingDoorCommand() != 0 ) << 31;
         unDatagram.aui32[1] |= ( IdleTime_GetParkingFloor_Plus1() << 24 );
         unDatagram.aui32[1] |= ( IdleTime_GetDirectionChangeTime_Seconds() & 0xFFF ) << 12;
         unDatagram.aui32[1] |= ( IdleTime_GetParkingTime_Seconds() & 0xFFF );
         break;
      case VDD__DRIVE_SPD_FB:
         unDatagram.auw16[3] = GetDriveSpeedFeedback();
         break;
      case VDD__DOOR_DATA_F:
         unDatagram.aui32[1] = Door_GetDebugDoorData(DOOR_FRONT);
         break;
      case VDD__DOOR_DATA_R:
         unDatagram.aui32[1] = Door_GetDebugDoorData(DOOR_REAR);
         break;

      default:
         unDatagram.auw16[3] = 0;
         break;
   }

   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__Debug, &unDatagram );

   //--------------------------------------------------
   memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
   unDatagram.aui32[0] = Motion_GetRampUpDistance();
   unDatagram.aui32[1] = Motion_GetSlowdownDistance();
   unDatagram.auc8[3] = GetActiveAcceptanceTest();
   unDatagram.auc8[7] = Motion_GetMaxFloorToFloorTime_sec();
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__Debug_PatternData, &unDatagram );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Load_GroupDatagrams()
{
   un_sdata_datagram unDatagram;

   memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
   GetDispatchData(&unDatagram);
   unDatagram.auc8[2] = GetEmergencyPowerCommand() & 0x7F;
   unDatagram.auc8[2] |= ( EPower_GetCarRecalledFlag() & 1 ) << 7;
   unDatagram.auc8[3] |= IdleTime_GetReadyToPark();
   unDatagram.auc8[4] |= VIP_GetCarReadyFlag() & 1;
   unDatagram.auc8[5] |= VIP_GetCarCaptureFlag() & 1;
   unDatagram.auc8[6]  = VIP_GetAutoSwingState() & 0x07;

   SDATA_WriteDatagram( &gstSData_LocalData,
                        DG_MRA__Group_Data,
                        &unDatagram
                      );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Load_CarCallSecurity(void)
{
   un_sdata_datagram unDatagram;
   memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
   unDatagram.aui32[0] = GetCarCallSecurityBitmap(0, DOOR_FRONT) | 
                         GetVirtual_CarCallSecurityBitmap(0, DOOR_FRONT);

   unDatagram.aui32[1] = GetCarCallSecurityBitmap(1, DOOR_FRONT) | 
                         GetVirtual_CarCallSecurityBitmap(1, DOOR_FRONT);

   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__CarCallSecurity_F0, &unDatagram );

   unDatagram.aui32[0] = GetCarCallSecurityBitmap(0, DOOR_REAR) | 
                         GetVirtual_CarCallSecurityBitmap(0, DOOR_REAR);
   unDatagram.aui32[1] = GetCarCallSecurityBitmap(1, DOOR_REAR) | 
                         GetVirtual_CarCallSecurityBitmap(1, DOOR_REAR);
   
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__CarCallSecurity_R0, &unDatagram );

   unDatagram.aui32[0] = GetCarCallSecurityBitmap(2, DOOR_FRONT)  |
                         GetVirtual_CarCallSecurityBitmap(2, DOOR_FRONT);
   unDatagram.aui32[1] = 0;
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__CarCallSecurity_F1, &unDatagram );

   unDatagram.aui32[0] = GetCarCallSecurityBitmap(2, DOOR_REAR) |
                         GetVirtual_CarCallSecurityBitmap(2, DOOR_REAR);
   unDatagram.aui32[1] = 0;
   SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__CarCallSecurity_R1, &unDatagram );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Load_CPLD_Data(void)
{
   if( GetUIRequest_ViewDebugDataCommand() == VDD__CPLD_MR )
   {
      FPGA_Packet stPacket;
      un_sdata_datagram unDatagram;

      gstSData_LocalData.paiResendRate_1ms[DG_MRA__CPLD_Data0] = UI_REQ_LIFETIME_MS/2;
      gstSData_LocalData.paiResendRate_1ms[DG_MRA__CPLD_Data1] = UI_REQ_LIFETIME_MS/2;

      FPGA_GetRecentPacket(&stPacket, FPGA_LOC__MRA);

      unDatagram.auc8[0] = stPacket.aucData[FPGA_BYTE__DATA0];
      unDatagram.auc8[1] = stPacket.aucData[FPGA_BYTE__DATA1];
      unDatagram.auc8[2] = stPacket.aucData[FPGA_BYTE__DATA2];
      unDatagram.auc8[3] = stPacket.aucData[FPGA_BYTE__DATA3];
      unDatagram.auc8[4] = stPacket.aucData[FPGA_BYTE__DATA4];
      unDatagram.auc8[5] = stPacket.aucData[FPGA_BYTE__DATA5];
      unDatagram.auc8[6] = stPacket.aucData[FPGA_BYTE__DATA6];
      unDatagram.auc8[7] = stPacket.aucData[FPGA_BYTE__DATA7];
      SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__CPLD_Data0, &unDatagram );

      unDatagram.auc8[0] = stPacket.aucData[FPGA_BYTE__DATA8];
      unDatagram.auc8[1] = stPacket.aucData[FPGA_BYTE__DATA9];
      unDatagram.auc8[2] = stPacket.aucData[FPGA_BYTE__DATA10];
      unDatagram.auc8[3] = stPacket.aucData[FPGA_BYTE__DATA11];
      unDatagram.auc8[4] = stPacket.aucData[FPGA_BYTE__DATA12];
      unDatagram.auc8[5] = 0;
      unDatagram.auc8[6] = 0;
      unDatagram.auc8[7] = 0;
      SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__CPLD_Data1, &unDatagram );
   }
   else
   {
      gstSData_LocalData.paiResendRate_1ms[DG_MRA__CPLD_Data0] = DISABLED_DATAGRAM_RESEND_RATE_MS;
      gstSData_LocalData.paiResendRate_1ms[DG_MRA__CPLD_Data1] = DISABLED_DATAGRAM_RESEND_RATE_MS;
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void LoadData_MR( void )
{
    un_sdata_datagram unDatagram;
    //--------------------------------------------------
    Load_Debug();
    Load_DefaultAll();
    //-------------------------------------------------
    Load_LocalOutputs();
    //-------------------------------------------------
    /* Send Latched CCs to system */
    memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
    unDatagram.aui32[0] = gaulCCB_Latched[0][0];
    unDatagram.aui32[1] = gaulCCB_Latched[1][0];
    SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__CarCalls1, &unDatagram );
    unDatagram.aui32[0] = gaulCCB_Latched[0][1];
    unDatagram.aui32[1] = gaulCCB_Latched[1][1];
    SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__CarCalls2, &unDatagram );
    unDatagram.aui32[0] = gaulCCB_Latched[0][2];
    unDatagram.aui32[1] = gaulCCB_Latched[1][2];
    SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__CarCalls3, &unDatagram );
    //--------------------------------------------------
    Load_MappedIO();
    //--------------------------------------------------

    memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
    unDatagram.auc8[0] = GetOperation_CurrentFloor();
    unDatagram.auc8[1] = GetOperation_MotionCmd();
    unDatagram.auc8[2] = InDestinationDoorzone();
    unDatagram.auc8[3] = GetPreflightFlag();
    unDatagram.aui32[1] = GetOperation_RequestedDestination();
    unDatagram.auc8[7] = GetOperation_DestinationFloor();

    SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__Operation1, &unDatagram );

    //--------------------------------------------------
    memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
    unDatagram.auc8[0] = GetOperation_ClassOfOp();
    unDatagram.auc8[1] = GetOperation_AutoMode();
    unDatagram.auc8[2] = GetOperation_ManualMode();
    unDatagram.auc8[3] = GetOperation_LearnMode();
    unDatagram.auw16[2] = GetOperation_SpeedLimit();
    unDatagram.auw16[3] = GetEmergencyBitmap();
    SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__Operation2, &unDatagram );
    //--------------------------------------------------
    memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
    unDatagram.aui32[0] = GetOperation_PositionLimit_UP();
    unDatagram.auc8[3] = GetOperation_BypassTermLimits();
    unDatagram.aui32[1] = GetOperation_PositionLimit_DN();
    unDatagram.auc8[7] = getCaptureMode() & 0x03;
    unDatagram.auc8[7] |= (CheckIfInputIsProgrammed( enIN_PHE_F2 ) & 1 ) << 2;
    unDatagram.auc8[7] |= (CheckIfInputIsProgrammed( enIN_PHE_R2 ) & 1 ) << 3;
    unDatagram.auc8[7] |= (CheckIfInputIsProgrammed( enIN_SafetyEdge_F ) & 1 ) << 4;
    unDatagram.auc8[7] |= (CheckIfInputIsProgrammed( enIN_SafetyEdge_R ) & 1 ) << 5;

    SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__Operation3, &unDatagram );

    //--------------------------------------------------
    memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
    unDatagram.auc8[0] = GetDoorState( DOOR_FRONT );
    unDatagram.auc8[1] = GetDoorState( DOOR_REAR );
    unDatagram.auc8[2] = SRU_Read_DIP_Switch(enSRU_DIP_A8); // Bootloader flag
    unDatagram.auc8[3] = gbBackupSync; // Param sync flag
    unDatagram.aui32[1] = FRAM_GetTotalRunCount();
    SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__Operation4, &unDatagram );

    //--------------------------------------------------

    memset( &unDatagram, 0, sizeof(un_sdata_datagram) );

    unDatagram.auc8[0] = GetMotion_RunFlag();
    unDatagram.auc8[1] = GetMotion_Direction();
    unDatagram.auw16[1] = GetMotion_SpeedCommand();
    unDatagram.aui32[1] = GetMotion_Destination();
    unDatagram.auc8[7] = GetMotion_Profile();

    SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__Motion, &unDatagram );

    //--------------------------------------
    LoadFaultLogDatagrams();

    //--------------------------------------
    Load_GroupDatagrams();
    //--------------------------------------
    LoadDatagram_RunLog();
    //--------------------------------------
    Load_BrakeData();

    //--------------------------------------
    memset( &unDatagram, 0, sizeof(un_sdata_datagram) );
    unDatagram.auc8[0] = DriveParam_GetConfirmCmd();
    unDatagram.auw16[1] = DriveParam_GetConfirmID();
    unDatagram.aui32[1] = DriveParam_GetConfirmValue();
    SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__DriveData, &unDatagram );

    Load_CarCallSecurity();

    Load_CPLD_Data();
}

/*-----------------------------------------------------------------------------
   Unload and latch hall calls
 -----------------------------------------------------------------------------*/
static void Unload_HallCalls()
{
   static uint8_t aucLastUpHCFloor_Plus1[NUM_OF_DOORS];
   static uint8_t aucLastDnHCFloor_Plus1[NUM_OF_DOORS];
   if(GetDatagramDirtyBit(DATAGRAM_ID_74))
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_74);

      EMS_UnloadMedicalCall();

      /* Clear the last received HC latch command in case the destination has changed */
      if(aucLastUpHCFloor_Plus1[DOOR_FRONT])
      {
         gpastHallCalls_F[aucLastUpHCFloor_Plus1[DOOR_FRONT]-1].bLatched_Up = 0;
      }
      if(aucLastDnHCFloor_Plus1[DOOR_FRONT])
      {
         gpastHallCalls_F[aucLastDnHCFloor_Plus1[DOOR_FRONT]-1].bLatched_Down = 0;
      }
      if( GetFP_RearDoors() )
      {
         if(aucLastUpHCFloor_Plus1[DOOR_REAR])
         {
            gpastHallCalls_F[aucLastUpHCFloor_Plus1[DOOR_REAR]-1].bLatched_Up = 0;
         }
         if(aucLastDnHCFloor_Plus1[DOOR_REAR])
         {
            gpastHallCalls_F[aucLastDnHCFloor_Plus1[DOOR_REAR]-1].bLatched_Down = 0;
         }
      }

      /* Record last latched HCs so they can be cleared if they change */
      aucLastUpHCFloor_Plus1[DOOR_FRONT] = GetHallCallFrontUp_FloorPlus1();
      aucLastUpHCFloor_Plus1[DOOR_REAR] = GetHallCallRearUp_FloorPlus1();
      aucLastDnHCFloor_Plus1[DOOR_FRONT] = GetHallCallFrontDn_FloorPlus1();
      aucLastDnHCFloor_Plus1[DOOR_REAR] = GetHallCallRearDn_FloorPlus1();

      /* Bound values in case of transmit error */
      for(uint8_t i = 0; i < NUM_OF_DOORS; i++)
      {
         if( aucLastUpHCFloor_Plus1[i] > GetFP_NumFloors() )
         {
            aucLastUpHCFloor_Plus1[i] = 0;
         }
         if( aucLastDnHCFloor_Plus1[i] > GetFP_NumFloors() )
         {
            aucLastDnHCFloor_Plus1[i] = 0;
         }
      }

      /* Latch the new destination's HCs */
      if( !HallCallsDisabled() && !gstFault.bActiveFault )
      {
         if(aucLastUpHCFloor_Plus1[DOOR_FRONT])
         {
            gpastHallCalls_F[aucLastUpHCFloor_Plus1[DOOR_FRONT]-1].bLatched_Up = 1;
         }
         if(aucLastDnHCFloor_Plus1[DOOR_FRONT])
         {
            gpastHallCalls_F[aucLastDnHCFloor_Plus1[DOOR_FRONT]-1].bLatched_Down = 1;
         }
         if( GetFP_RearDoors() )
         {
            if(aucLastUpHCFloor_Plus1[DOOR_REAR])
            {
               gpastHallCalls_R[aucLastUpHCFloor_Plus1[DOOR_REAR]-1].bLatched_Up = 1;
            }
            if(aucLastDnHCFloor_Plus1[DOOR_REAR])
            {
               gpastHallCalls_R[aucLastDnHCFloor_Plus1[DOOR_REAR]-1].bLatched_Down = 1;
            }
         }
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_MRB()
{
   if(GetDatagramDirtyBit(DATAGRAM_ID_28))
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_28);
      SetLocalInputs(GetGroupInputs_Riser1_MRB(), IO_BOARDS__RISER1);
      SetLocalInputs(GetGroupInputs_Riser2_MRB(), IO_BOARDS__RISER2);
      SetLocalInputs(GetGroupInputs_Riser3_MRB(), IO_BOARDS__RISER3);
      SetLocalInputs(GetGroupInputs_Riser4_MRB(), IO_BOARDS__RISER4);
   }

   RTC_SetSyncTime(GetSyncTime_Time_MRB());

   //---------------------------------------------------------
   Unload_HallCalls();

   Unload_RemoteCommands();

   //---------------------------------------------------------
   /* Unload UI Requests */
   if(GetDatagramDirtyBit(DATAGRAM_ID_155))
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_155);
      //---------------------------------------------------------
      if( GetUIRequest_CarCall_F_MRB() )
      {
         SetCarCall(GetUIRequest_CarCall_F_MRB()-1, DOOR_FRONT);
      }
      //---------------------------------------------------------
      static enum_default_cmd eLastCMD;
      if(GetUIRequest_DefaultCommand_MRB() && !eLastCMD)
      {
         SetDefaultParameterCommand(GetUIRequest_DefaultCommand_MRB());
      }
      eLastCMD = GetUIRequest_DefaultCommand_MRB();
   }

   if(GetDatagramDirtyBit(DATAGRAM_ID_222))
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_222);
      //---------------------------------------------------------
      if( GetUIRequest_CarCall_R_MRB() )
      {
         SetCarCall(GetUIRequest_CarCall_R_MRB()-1, DOOR_REAR);
      }

      //---------------------------------------------------------
      uint8_t ucFloor_Plus1 = GetUIRequest_ADA_Request_MRB() & 0xFF;
      if( ucFloor_Plus1 )
      {
         en_ada_type eType = ( GetUIRequest_ADA_Request_MRB() >> 8 ) & 0xF;
         Set_ADARequest( ucFloor_Plus1-1, eType );
      }
   }
};
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_CTA()
{
   if(GetDatagramDirtyBit(DATAGRAM_ID_26))
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_26);
      SetLocalInputs(GetLocalInputs_Inputs_CTA(), IO_BOARDS__CTA);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_COPA()
{
   if(GetDatagramDirtyBit(DATAGRAM_ID_27))
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_27);
      SetLocalInputs(GetLocalInputs_Inputs_COPA(), IO_BOARDS__COP);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_CTB()
{
   /* Unload UI Requests */
   if(GetDatagramDirtyBit(DATAGRAM_ID_156))
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_156);

      if( GetUIRequest_CarCall_F_CTB() )
      {
         SetCarCall(GetUIRequest_CarCall_F_CTB()-1, DOOR_FRONT);
      }

      static enum_default_cmd eLastCMD;
      if(GetUIRequest_DefaultCommand_CTB() && !eLastCMD)
      {
         SetDefaultParameterCommand(GetUIRequest_DefaultCommand_CTB());
      }
      eLastCMD = GetUIRequest_DefaultCommand_CTB();
   }

   if(GetDatagramDirtyBit(DATAGRAM_ID_223))
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_223);
      //---------------------------------------------------------
      if( GetUIRequest_CarCall_R_CTB() )
      {
         SetCarCall(GetUIRequest_CarCall_R_CTB()-1, DOOR_REAR);
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_COPB()
{
   /* Unload UI Requests */
   if(GetDatagramDirtyBit(DATAGRAM_ID_157))
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_157);

      if( GetUIRequest_CarCall_F_COPB() )
      {
         SetCarCall(GetUIRequest_CarCall_F_COPB()-1, DOOR_FRONT);
      }

      static enum_default_cmd eLastCMD;
      if(GetUIRequest_DefaultCommand_COPB() && !eLastCMD)
      {
         SetDefaultParameterCommand(GetUIRequest_DefaultCommand_COPB());
      }
      eLastCMD = GetUIRequest_DefaultCommand_COPB();
   }

   if(GetDatagramDirtyBit(DATAGRAM_ID_224))
   {
      ClrDatagramDirtyBit(DATAGRAM_ID_224);
      //---------------------------------------------------------
      if( GetUIRequest_CarCall_R_COPB() )
      {
         SetCarCall(GetUIRequest_CarCall_R_COPB()-1, DOOR_REAR);
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_EXP()
{
   // Moved to mod_can.c
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UnloadData_MR( void )
{
   Unload_MRB();
   Unload_CTA();
   Unload_COPA();
   Unload_CTB();
   Unload_COPB();
   Unload_EXP();
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
