/******************************************************************************
 * @file     mod_fault_app.c
 * @author   Keith Soneda
 * @version  V1.00
 * @date     1, August 2016
 *
 * @note   Each fault node (TRC, MRA, MRB, CTA, CTB, COPA, COPB) will assert their own fault.
 *         Nodes will forward their local active fault to eachother.
 *         Nodes will auto-clear their active fault after 2 seconds.
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "sru.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "sru_a.h"
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
enum en_faultapp_tx_state
{
  TX_STATE__GetNext,
  TX_STATE__Send1,
  TX_STATE__Confirm1,
  TX_STATE__Send2,
  TX_STATE__Confirm2,
  TX_STATE__Send3,
  TX_STATE__Confirm3,

  NUM_TX_STATE
};
static enum en_faultapp_tx_state eTXState = TX_STATE__GetNext;
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Unload from MRB
 -----------------------------------------------------------------------------*/
static void UnloadDatagram_MRB( st_fault_data *pastActiveFaults )
{
    static en_faults eLastFault;
    st_fault_data *pstFaultData = pastActiveFaults + enFAULT_NODE__MRB;

    if(    ( GetFault1_Fault_MRB()   < NUM_FAULTS )
        && ( GetFault1_Fault_MRB()   == GetFault2_Fault_MRB() )
        && ( GetFault1_Fault_MRB()   == GetFault3_Fault_MRB() ) )
    {
       pstFaultData->eFaultNum   = GetFault1_Fault_MRB();
       pstFaultData->ulTimestamp = GetFault1_Timestamp_MRB();
       pstFaultData->wSpeed      = GetFault2_Speed_MRB();
       pstFaultData->ulPosition  = GetFault2_Position_MRB();

       pstFaultData->ucCurrentFloor = GetFault3_CurrentFloor_MRB();
       pstFaultData->ucDestinationFloor = GetFault3_DestinationFloor_MRB();
       pstFaultData->wCommandSpeed = GetFault3_CommandSpeed_MRB();

       if( !pstFaultData->eFaultNum )
       {
          /* Store additional log data */
          pstFaultData->wEncoderSpeed = 0;
       }
       else if( pstFaultData->eFaultNum != eLastFault )
       {
          /* Store additional log data */
          pstFaultData->wEncoderSpeed = GetDriveSpeedFeedback();
       }
       eLastFault = pstFaultData->eFaultNum;
    }
}
/*-----------------------------------------------------------------------------
Unload from CTA
 -----------------------------------------------------------------------------*/
static void UnloadDatagram_CTA( st_fault_data *pastActiveFaults )
{
    static en_faults eLastFault;
    st_fault_data *pstFaultData = pastActiveFaults + enFAULT_NODE__CTA;

    if(    ( GetFault1_Fault_CTA()   < NUM_FAULTS )
        && ( GetFault1_Fault_CTA()   == GetFault2_Fault_CTA() )
        && ( GetFault1_Fault_CTA()   == GetFault3_Fault_CTA() ) )
    {
       pstFaultData->eFaultNum   = GetFault1_Fault_CTA();
       pstFaultData->ulTimestamp = GetFault1_Timestamp_CTA();
       pstFaultData->wSpeed      = GetFault2_Speed_CTA();
       pstFaultData->ulPosition  = GetFault2_Position_CTA();

       pstFaultData->ucCurrentFloor = GetFault3_CurrentFloor_CTA();
       pstFaultData->ucDestinationFloor = GetFault3_DestinationFloor_CTA();
       pstFaultData->wCommandSpeed = GetFault3_CommandSpeed_CTA();

       if( !pstFaultData->eFaultNum )
       {
          /* Store additional log data */
          pstFaultData->wEncoderSpeed = 0;
       }
       else if( pstFaultData->eFaultNum != eLastFault )
       {
          /* Store additional log data */
          pstFaultData->wEncoderSpeed = GetDriveSpeedFeedback();
       }
       eLastFault = pstFaultData->eFaultNum;
    }
}
/*-----------------------------------------------------------------------------
Unload from CTB
 -----------------------------------------------------------------------------*/
static void UnloadDatagram_CTB( st_fault_data *pastActiveFaults )
{
    static en_faults eLastFault;
    st_fault_data *pstFaultData = pastActiveFaults + enFAULT_NODE__CTB;

    if(    ( GetFault1_Fault_CTB()   < NUM_FAULTS )
        && ( GetFault1_Fault_CTB()   == GetFault2_Fault_CTB() )
        && ( GetFault1_Fault_CTB()   == GetFault3_Fault_CTB() ) )
    {
       pstFaultData->eFaultNum   = GetFault1_Fault_CTB();
       pstFaultData->ulTimestamp = GetFault1_Timestamp_CTB();
       pstFaultData->wSpeed      = GetFault2_Speed_CTB();
       pstFaultData->ulPosition  = GetFault2_Position_CTB();

       pstFaultData->ucCurrentFloor = GetFault3_CurrentFloor_CTB();
       pstFaultData->ucDestinationFloor = GetFault3_DestinationFloor_CTB();
       pstFaultData->wCommandSpeed = GetFault3_CommandSpeed_CTB();

       if( !pstFaultData->eFaultNum )
       {
          /* Store additional log data */
          pstFaultData->wEncoderSpeed = 0;
       }
       else if( pstFaultData->eFaultNum != eLastFault )
       {
          /* Store additional log data */
          pstFaultData->wEncoderSpeed = GetDriveSpeedFeedback();
       }
       eLastFault = pstFaultData->eFaultNum;
    }
}
/*-----------------------------------------------------------------------------
Unload from COPA
 -----------------------------------------------------------------------------*/
static void UnloadDatagram_COPA( st_fault_data *pastActiveFaults )
{
    static en_faults eLastFault;
    st_fault_data *pstFaultData = pastActiveFaults + enFAULT_NODE__COPA;

    if(    ( GetFault1_Fault_COPA()   < NUM_FAULTS )
        && ( GetFault1_Fault_COPA()   == GetFault2_Fault_COPA() )
        && ( GetFault1_Fault_COPA()   == GetFault3_Fault_COPA() ) )
    {
       pstFaultData->eFaultNum   = GetFault1_Fault_COPA();
       pstFaultData->ulTimestamp = GetFault1_Timestamp_COPA();
       pstFaultData->wSpeed      = GetFault2_Speed_COPA();
       pstFaultData->ulPosition  = GetFault2_Position_COPA();

       pstFaultData->ucCurrentFloor = GetFault3_CurrentFloor_COPA();
       pstFaultData->ucDestinationFloor = GetFault3_DestinationFloor_COPA();
       pstFaultData->wCommandSpeed = GetFault3_CommandSpeed_COPA();

       if( !pstFaultData->eFaultNum )
       {
          /* Store additional log data */
          pstFaultData->wEncoderSpeed = 0;
       }
       else if( pstFaultData->eFaultNum != eLastFault )
       {
          /* Store additional log data */
          pstFaultData->wEncoderSpeed = GetDriveSpeedFeedback();
       }
       eLastFault = pstFaultData->eFaultNum;
    }
}
/*-----------------------------------------------------------------------------
Unload from COPB
 -----------------------------------------------------------------------------*/
static void UnloadDatagram_COPB( st_fault_data *pastActiveFaults )
{
    static en_faults eLastFault;
    st_fault_data *pstFaultData = pastActiveFaults + enFAULT_NODE__COPB;

    if(    ( GetFault1_Fault_COPB()   < NUM_FAULTS )
        && ( GetFault1_Fault_COPB()   == GetFault2_Fault_COPB() )
        && ( GetFault1_Fault_COPB()   == GetFault3_Fault_COPB() ) )
    {
       pstFaultData->eFaultNum   = GetFault1_Fault_COPB();
       pstFaultData->ulTimestamp = GetFault1_Timestamp_COPB();
       pstFaultData->wSpeed      = GetFault2_Speed_COPB();
       pstFaultData->ulPosition  = GetFault2_Position_COPB();

       pstFaultData->ucCurrentFloor = GetFault3_CurrentFloor_COPB();
       pstFaultData->ucDestinationFloor = GetFault3_DestinationFloor_COPB();
       pstFaultData->wCommandSpeed = GetFault3_CommandSpeed_COPB();

       if( !pstFaultData->eFaultNum )
       {
          /* Store additional log data */
          pstFaultData->wEncoderSpeed = 0;
       }
       else if( pstFaultData->eFaultNum != eLastFault )
       {
          /* Store additional log data */
          pstFaultData->wEncoderSpeed = GetDriveSpeedFeedback();
       }
       eLastFault = pstFaultData->eFaultNum;
    }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UnloadDatagram_ActiveFaults_MR( st_fault_data *pastActiveFaults )
{
   //-----------------------------------------------
   UnloadDatagram_MRB(pastActiveFaults);
   //-----------------------------------------------
   UnloadDatagram_CTA(pastActiveFaults);
   //-----------------------------------------------
   UnloadDatagram_CTB(pastActiveFaults);
   //-----------------------------------------------
   UnloadDatagram_COPA(pastActiveFaults);
   //-----------------------------------------------
   UnloadDatagram_COPB(pastActiveFaults);
}


/*-----------------------------------------------------------------------------
Load active faults from local node for transmit to other fault nodes
 -----------------------------------------------------------------------------*/
static void LoadDatagram( st_fault_data *pastActiveFaults )
{
   static uint16_t uwResendCounter_ms;
   un_sdata_datagram unDatagram;
   uint8_t ucNode = FaultApp_GetNode();
   st_fault_data *pstFaultData = pastActiveFaults + ucNode;
   uint16_t uwResendLimit_ms = ( pstFaultData->eFaultNum ) ? FAULT_RESEND_RATE_ACTIVE_1MS : FAULT_RESEND_RATE_INACTIVE_1MS;

   switch(eTXState)
   {
      case TX_STATE__GetNext:
         uwResendCounter_ms += MOD_RUN_PERIOD_FAULT_APP_1MS;
         if(uwResendCounter_ms >= uwResendLimit_ms)
         {
            uwResendCounter_ms = 0;
            eTXState = TX_STATE__Send1;
         }
         break;

      case TX_STATE__Send1:
         unDatagram.auw16[0] = pstFaultData->eFaultNum;
         unDatagram.auw16[1] = 0;
         unDatagram.aui32[1] = pstFaultData->ulTimestamp;
         SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__Fault1, &unDatagram );
         SDATA_DirtyBit_Set( &gstSData_LocalData , DG_MRA__Fault1 );
         eTXState = TX_STATE__Confirm1;
         break;

      case TX_STATE__Confirm1:
         if(!SDATA_DirtyBit_Get( &gstSData_LocalData , DG_MRA__Fault1 ))
         {
            eTXState = TX_STATE__Send2;
         }
         break;

      case TX_STATE__Send2:
         unDatagram.auw16[0] = pstFaultData->eFaultNum;
         unDatagram.auw16[1] = pstFaultData->wSpeed;
         unDatagram.aui32[1] = pstFaultData->ulPosition;
         SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__Fault2, &unDatagram );
         SDATA_DirtyBit_Set( &gstSData_LocalData , DG_MRA__Fault2 );
         eTXState = TX_STATE__Confirm2;
         break;

      case TX_STATE__Confirm2:
         if(!SDATA_DirtyBit_Get( &gstSData_LocalData , DG_MRA__Fault2 ))
         {
            eTXState = TX_STATE__Send3;
         }
         break;

      case TX_STATE__Send3:
         unDatagram.auw16[0] = pstFaultData->eFaultNum;
         unDatagram.auc8[2] = pstFaultData->ucCurrentFloor;
         unDatagram.auc8[3] = pstFaultData->ucDestinationFloor;
         unDatagram.auw16[2] = pstFaultData->wCommandSpeed;
         unDatagram.auw16[3] = pstFaultData->wEncoderSpeed;
         SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__Fault3, &unDatagram );
         SDATA_DirtyBit_Set( &gstSData_LocalData , DG_MRA__Fault3 );
         eTXState = TX_STATE__Confirm3;
         break;

      case TX_STATE__Confirm3:
         if(!SDATA_DirtyBit_Get( &gstSData_LocalData , DG_MRA__Fault3 ))
         {
            eTXState = TX_STATE__GetNext;
         }
         break;

      default:
         eTXState = TX_STATE__GetNext;
         break;
   }
}
/*-----------------------------------------------------------------------------
Load active fault from CPLD for transmit to other fault nodes
 -----------------------------------------------------------------------------*/
static void LoadDatagram_CPLD( st_fault_data *pastActiveFaults )
{
   static uint16_t uwResendCounter_ms;
   un_sdata_datagram unDatagram;
   uint8_t ucNode = enFAULT_NODE__CPLD;
   st_fault_data *pstFaultData = pastActiveFaults + ucNode;
   uint16_t uwResendLimit_ms = ( pstFaultData->eFaultNum ) ? FAULT_RESEND_RATE_ACTIVE_1MS : FAULT_RESEND_RATE_INACTIVE_1MS;

   switch(eTXState)
   {
      case TX_STATE__GetNext:
         uwResendCounter_ms += MOD_RUN_PERIOD_FAULT_APP_1MS;
         if(uwResendCounter_ms >= uwResendLimit_ms)
         {
            uwResendCounter_ms = 0;
            eTXState = TX_STATE__Send1;
         }
         break;

      case TX_STATE__Send1:
         unDatagram.auw16[0] = pstFaultData->eFaultNum;
         unDatagram.auw16[1] = 0;
         unDatagram.aui32[1] = pstFaultData->ulTimestamp;
         SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__CPLD_Fault1, &unDatagram );
         SDATA_DirtyBit_Set( &gstSData_LocalData , DG_MRA__CPLD_Fault1 );
         eTXState = TX_STATE__Confirm1;
         break;

      case TX_STATE__Confirm1:
         if(!SDATA_DirtyBit_Get( &gstSData_LocalData , DG_MRA__CPLD_Fault1 ))
         {
            eTXState = TX_STATE__Send2;
         }
         break;

      case TX_STATE__Send2:
         unDatagram.auw16[0] = pstFaultData->eFaultNum;
         unDatagram.auw16[1] = pstFaultData->wSpeed;
         unDatagram.aui32[1] = pstFaultData->ulPosition;
         SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__CPLD_Fault2, &unDatagram );
         SDATA_DirtyBit_Set( &gstSData_LocalData , DG_MRA__CPLD_Fault2 );
         eTXState = TX_STATE__Confirm2;
         break;

      case TX_STATE__Confirm2:
         if(!SDATA_DirtyBit_Get( &gstSData_LocalData , DG_MRA__CPLD_Fault2 ))
         {
            eTXState = TX_STATE__Send3;
         }
         break;

      case TX_STATE__Send3:
         unDatagram.auw16[0] = pstFaultData->eFaultNum;
         unDatagram.auc8[2] = pstFaultData->ucCurrentFloor;
         unDatagram.auc8[3] = pstFaultData->ucDestinationFloor;
         unDatagram.auw16[2] = pstFaultData->wCommandSpeed;
         unDatagram.auw16[3] = pstFaultData->wEncoderSpeed;
         SDATA_WriteDatagram( &gstSData_LocalData, DG_MRA__CPLD_Fault3, &unDatagram );
         SDATA_DirtyBit_Set( &gstSData_LocalData , DG_MRA__CPLD_Fault3 );
         eTXState = TX_STATE__Confirm3;
         break;

      case TX_STATE__Confirm3:
         if(!SDATA_DirtyBit_Get( &gstSData_LocalData , DG_MRA__CPLD_Fault3 ))
         {
            eTXState = TX_STATE__GetNext;
         }
         break;

      default:
         eTXState = TX_STATE__GetNext;
         break;
   }
}
/*-----------------------------------------------------------------------------
Load active faults from local & other boards
 -----------------------------------------------------------------------------*/
void LoadDatagram_ActiveFaults_MR( st_fault_data *pastActiveFaults )
{
   LoadDatagram(pastActiveFaults);

   LoadDatagram_CPLD(pastActiveFaults);
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
