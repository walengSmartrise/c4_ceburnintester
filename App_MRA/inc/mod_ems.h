/******************************************************************************
 *
 * @file     mod_ems.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef MOD_EMS_H_
#define MOD_EMS_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_a.h"
#include "sys.h"
#include "motion.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_EMS_1MS        (50U)

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   EMS_SERVICE__INACTIVE,
   EMS_SERVICE__PHASE1,
   EMS_SERVICE__PHASE2,

   NUM_EMS_SERVICES
}en_ems_services;
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_module gstMod_EMS;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
uint8_t EMS_GetEMS1PrioritizedFlag(void);
uint8_t EMS_GetEMS2PrioritizedFlag(void);

en_ems_services EMS_GetEmergencyMedicalService(void);
uint8_t EMS_GetLampFlag(void);
uint8_t EMS_GetBuzzerFlag(void);

void EMS_Phase_1 ( uint8_t bInitMode );
void EMS_Phase_2 ( uint8_t bInitMode );

void EMS_UnloadMedicalCall(void);
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
