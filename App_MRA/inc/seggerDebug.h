/******************************************************************************
 *
 * @file     mod_run_log.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef MOD_SEGGER_DEBUG_H_
#define MOD_SEGGER_DEBUG_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_a.h"
#include "sys.h"
#include "mod_motion.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define ENABLE_SEGGER_DEBUG      (0)
#if ENABLE_SEGGER_DEBUG
   #include "SEGGER_SYSVIEW.h"
   #include "SEGGER_RTT.h"
   #define SEGGER_CHANNEL_IN_USE          (1)
   /* Module run time logging */
   #define RTT_ENABLE_MODULE_CALL_PERIOD_LOGGING   (0)

   /* Motion/Pattern logging */
   #define RTT_OUTPUT_RATE_5MS                         (5)
   #define RTT_ENABLE_OUTPUT_GENERATED_PATTERN         (0)
   #define RTT_ENABLE_MOTION_LOGGING                     (1)
   #define RTT_ENABLE_TEST_TRUNCATED_ACCEL_ONLY          (0)
   #define RTT_ENABLE_TEST_REDUCED_SPEED_LIMIT           (1)
   /* Brake COM logging */
   #define RTT_ENABLE_BRAKE_COM_LOGGING   (0)
#endif
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

void RTTLog_MotionStart( Motion_Control *pstMotionCtrl, Curve_Parameters *pstCurveParameters );
void RTTLog_InMotion( Motion_Control *pstMotionCtrl );
void RTTLog_TimeStampEvent( char *ps );
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
