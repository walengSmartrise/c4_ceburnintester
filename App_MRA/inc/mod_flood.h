/******************************************************************************
 *
 * @file     mod_flood.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef MOD_FLOOD_H_
#define MOD_FLOOD_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_a.h"
#include "sys.h"
#include "motion.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_FLOOD_1MS        (200U)

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_module gstMod_Flood_Op;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void Auto_Flood( uint8_t bInit);
uint8_t Flood_GetActiveFlag(void);
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
