/******************************************************************************
 *
 * @file     mod.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef MOD_H
#define MOD_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include <seggerDebug.h>
#include "sys.h"
#include "GlobalData.h"
#include "fram_data.h"
#include "fram.h"
#include "fault_app.h"
#include "alarm_app.h"
#include "shared_data.h"
#include "datagrams.h"
#include "sru_a.h"
#include "motion.h"
#include "mod_motion.h"
#include "operation.h"
#include "position.h"
#include "drive.h"
#include "mod_run_log.h"
#include "mod_ets.h"
#include "mod_ems.h"
#include "mod_input_mapping.h"
#include "mod_output_mapping.h"
#include "mod_brake.h"
#include "automatic_operation.h"
#include "mod_rescue.h"
#include "mod_doors.h"
#include "mod_phone_failure.h"
#include "mod_ems.h"
#include "mod_fire.h"
#include "mod_flood.h"
#include "mod_IdleTime.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_CTRL_NET_1MS          (5U)
#define MOD_RUN_PERIOD_CAR_NET_1MS           (5U)
#define MOD_RUN_PERIOD_AB_NET_1MS            (5U)
#define MOD_RUN_PERIOD_UART_1MS              (5U)
#define MOD_RUN_PERIOD_CAN_1MS               (5U)
#define MOD_RUN_PERIOD_FAULT_APP_1MS         (5U)
#define MOD_RUN_PERIOD_FAULT_1MS             (5U)
#define MOD_RUN_PERIOD_POSITION_1MS          (5U)
#define MOD_RUN_PERIOD_SAFETY_1MS            (10U)
#define MOD_RUN_PERIOD_CAN_1MS               (5U)
#define MOD_RUN_PERIOD_OPER_AUTO_1MS         (50U)
#define MOD_RUN_PERIOD_FPGA_1MS              (5U)
#define MOD_RUN_PERIOD_ETS_1MS               (10U)

#define MODE_SIGNALS_DEBOUNCE_50MS           (3)

#define MIN_EPOWER_SPEED_LIMIT_FPM      (10)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
enum enOOSSubFaults
{
   enOOS_NONE,
   enOOS_HourlyLimit,
   enOOS_ConsecutiveLimit,
   enOOS_DoorHourlyLimit,
   enOOS_MaxStartsPerMin,

   NUM_OOS_Subfaults
};

/* There will be an instance of this struct for every floor.
 * I am using bitfields to save RAM, keeping each struct's size to one byte */
struct st_carcalls
{
   uint8_t bButton_CurrentPress : 1;
   uint8_t bLamp : 1;
   uint8_t bLatched : 1;
   uint8_t bSecured : 1;
   uint8_t bValidOpening : 1;
   uint8_t bHallSecured : 1;
};

struct st_floors
{
   uint32_t ulPosition : 24;
   uint8_t bFrontOpening : 1;
   uint8_t bRearOpening : 1;
};

struct st_hallcalls
{
   uint8_t bLatched_Up: 1; //latched up will be used for the hall call lamp
   uint8_t bLatched_Down: 1; //latched down for the down hall call lamp
   uint8_t bSecured: 1;
};

typedef enum
{
  BRAKE_SEL__PRIMARY,
  BRAKE_SEL__EMERGENCY,

  MAX_BRAKE_SEL
} BrakeSelect;
typedef enum
{
  CONTACTOR_SEL__M,
  CONTACTOR_SEL__B2,

  MAX_CONTACTOR_SEL
} ContactorSelect;
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

extern struct st_module_control *gpstModuleControl_ThisDeployment;

extern struct st_sdata_control gstSData_CtrlNet_MRA;
extern struct st_sdata_control gstSData_CtrlNet_TRC;
extern struct st_sdata_control gstSData_CtrlNet_BRK;

extern struct st_sdata_control gstSData_LocalData;

extern struct st_module gstMod_CAN;

extern struct st_module gstMod_SData_CarNet;
extern struct st_module gstMod_UART;

extern struct st_module gstMod_CarCalls;
extern struct st_module gstMod_Dispatch;
extern struct st_module gstMod_Earthquake;
extern struct st_module gstMod_Capture;
extern struct st_module gstMod_CarToLobby;
extern struct st_module gstMod_MaxRuntime;
extern struct st_module gstMod_Releveling;
extern struct st_module gstMod_LocalInputs;
extern struct st_module gstMod_Fault;
extern struct st_module gstMod_Sabbath;
extern struct st_module gstMod_Heartbeat;
extern struct st_module gstMod_ModeOfOper;
extern struct st_module gstMod_Oper_Auto;
extern struct st_module gstMod_Oper_Learn;
extern struct st_module gstMod_Oper_Manual;
extern struct st_module gstMod_ParamMaster;
extern struct st_module gstMod_ParamSlave;
extern struct st_module gstMod_ParamApp;
extern struct st_module gstMod_Watchdog;
extern struct st_module gstMod_FaultApp;

extern struct st_module gstMod_Position;

extern struct st_module gstMod_SData;
extern struct st_module gstMod_Motion;
extern struct st_module gstMod_Drive;
extern struct st_module gstMod_SData_CtrlNet;
extern struct st_module gstMod_Safety;
extern struct st_module gstMod_FPGA;
extern struct st_module gstMod_AcceptanceTest;

extern struct st_module gstMod_Freight_Doors;

extern struct st_module gstMod_CarCallSecurity;

extern st_fault  gstFault;
extern struct st_drive gstDrive;

extern struct st_carcalls * gpastCarCalls_F;
extern struct st_carcalls * gpastCarCalls_R;
extern struct st_autoModeRules gstCurrentModeRules[ NUM_MODE_AUTO ];

extern struct st_hallcalls * gpastHallCalls_F;
extern struct st_hallcalls * gpastHallCalls_R;

extern struct st_floors   * gpastFloors;

extern struct st_module gstMod_TemporaryTest;//rm

extern uint8_t gbBackupSync;

extern uint32_t gaulCCB_Latched[2][BITMAP32_SIZE(MAX_NUM_FLOORS)];

extern char *pasVersion;

extern uint8_t ucbFlag_Auto_Stop;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

void Auto_Attendant( uint8_t bInit );
enum direction_enum Attendant_GetDirection();
void Attendant_SetDirection(enum direction_enum eDir);
uint8_t Attendant_GetBuzzerFlag();
uint8_t Attendant_GetHallCallBypass(void);
uint8_t Attendant_GetHallCallLamp_Above(void);
uint8_t Attendant_GetHallCallLamp_Below(void);

void Auto_CW_Derail( uint8_t bInit );
void Auto_Seismc( uint8_t bInit );
uint8_t OnEarthquake ( void );

void Auto_Active_Shooter_Mode ( uint8_t bInit);

void Auto_Marshal_Mode( uint8_t bInit );

void Auto_VIP_mode ( uint8_t bInit );

void Auto_EQ_Hoistway_Scan ( void );
void Update_CC_LatchRequest( void );

uint8_t bMarshalModeStatus( void );
uint8_t Get_Exp41CCLOutputs_F( void );
uint8_t Get_Exp42CCLOutputs_F( void );
uint8_t Get_Exp43CCLOutputs_F( void );
uint8_t Get_Exp44CCLOutputs_F( void );
uint8_t Get_Exp45CCLOutputs_F( void );
uint8_t Get_Exp46CCLOutputs_F( void );
uint8_t Get_Exp47CCLOutputs_F( void );
uint8_t Get_Exp48CCLOutputs_F( void );

uint8_t Get_Exp49CCLOutputs_R( void );
uint8_t Get_Exp50CCLOutputs_R( void );
uint8_t Get_Exp51CCLOutputs_R( void );
uint8_t Get_Exp52CCLOutputs_R( void );
uint8_t Get_Exp53CCLOutputs_R( void );
uint8_t Get_Exp54CCLOutputs_R( void );
uint8_t Get_Exp55CCLOutputs_R( void );
uint8_t Get_Exp56CCLOutputs_R( void );

void Auto_Indp_Srv( uint8_t bInit );
void Auto_Swing( uint8_t bInit );
void Auto_VIP_Mode( uint8_t bInit );
uint8_t GetSwingService();
uint8_t VIP_GetSwingService( void );
en_swing_states GetAutoSwingState();
en_vip_states VIP_GetAutoSwingState( void );
uint8_t VIP_GetCarReadyFlag( void );
uint8_t VIP_GetCarCaptureFlag( void );

void Auto_OOS( uint8_t bInit );
void Auto_InitCustomSettings(uint8_t bInit);
void Auto_Code_Pink(uint8_t bInit); // Also called wander guard.

uint32_t GetSabbathParamTime( uint8_t param );
void Auto_Sabbath( uint8_t bInit);

uint8_t InDestinationDoorzone( void );
void SetInDestinationDoorzone ( uint8_t bDestZone );
/* Emergency Power */
void Auto_Emergency_Power ( uint8_t bInit );
uint8_t GetEmergencyPowerLockout(void);
uint8_t EPower_GetCarRecalledFlag(void);
void EPower_ResetCarRecalledFlag(void);

/* Test Mode */
uint8_t SetTestDestination ( uint8_t ucNewTestDestination );
uint8_t SetTestCorrectionRunOn ( void );

void SharedData_CtrlNet_Init(  );

uint8_t GetFloorIndex( uint32_t ulPosCount );
uint8_t GetDestination_CurrentDirection( void );
uint8_t GetNextDestination_AnyDirection(void);
uint8_t HallCallsDisabled ( void );
uint8_t GetMidflightDestination( void );
uint8_t GetArrivalLanterns( void );

uint8_t LatchCarCall ( uint8_t ucFloorIndex, uint8_t ucDoorIndex) ;

uint8_t inReleveling ( void ) ;

enum en_Capture_Mode getCaptureMode( void );
void StartCaptureMode (uint8_t ucDestination);
void ResetCaptureMode(void);

//mod_dispatch function
void SetDestination ( uint8_t ucDestination );
void StopNextAvailableLanding ( void );
void StartReleveling( uint8_t  ucFloorIndex );
void GetDispatchData ( un_sdata_datagram * unDatagram );
void CheckDestination ( void );
uint8_t GetDistpatchDirectionPriority ( void );

uint8_t GetCurrentLandingCalls( void );
void ClearCurrentLandingCalls( void );
void ClearLatchedCarCalls( void );
void ClearLatchedCarCalls_Flood( void );
void ClearLatchedHallCalls( void );
void ClearLatchedHallCalls_Flood( void);

void ClearCurrentLandingCalls_ManualDoor( enum en_doors enDoorIndex );

//Auto_Recall
void ResetRecallState();
enum en_recall_states GetRecallState ( void );
uint8_t Auto_Recall ( uint8_t bInitMode, uint8_t ucRecallFloor, en_recall_door_command eDoorCommand );

/* Get module states */

enum en_cw_states Auto_CW_GetState( void );

enum en_learn_states Learn_GetState(void);
uint8_t Learn_GetNeedToLearnFlag(void);

uint8_t GetOutputValue( enum en_output_functions eOutput );
void SetOutputValue( enum en_output_functions eOutput, uint8_t bValue );

uint8_t GetCarToLobbyLamp ( void );
uint8_t GetCarToLobbyRecallFlag(void);

uint8_t GetLocalInputs_ThisNode(void);

uint16_t GetRequestToSetEmerBitmap();
uint16_t GetRequestToClrEmerBitmap();
void SetEmergencyBit(enum en_emergency_bitmap enEmergencyBF);
void ClrEmergencyBit(enum en_emergency_bitmap enEmergencyBF);

void Position_MRA(void);


Status UART_LoadCANMessage(CAN_MSG_T *pstTxMsg);
uint16_t UART_GetRxErrorCount(void);

uint8_t CheckIf_SendOnCarNet( uint16_t uwDestinationBitmap );
uint8_t CheckIf_SendOnABNet(uint16_t uwDestinationBitmap);

void SharedData_Init();
void LoadData_MR();

void UnloadData_MR();

void Run_ModFault_MRA( void );

/*----------------------------------------------------------------------------
   For manually picking contactors during acceptance test
   0 = CONTACTOR_SEL__M
   1 = CONTACTOR_SEL__B2
 *----------------------------------------------------------------------------*/
void SetAcceptanceContactorPickCommand( ContactorSelect eContactorSelect, uint8_t bPick );
uint8_t GetAcceptanceContactorPickCommand( ContactorSelect eContactorSelect );

/*----------------------------------------------------------------------------
   For manually picking brake during acceptance test
   0 = Primary brake
   1 = secondary brake
 *----------------------------------------------------------------------------*/
void SetAcceptanceBrakePickCommand( BrakeSelect eBrakeSelect, uint8_t bPick );
/*----------------------------------------------------------------------------
   For manually picking brake during acceptance test
   0 = Primary brake
   1 = secondary brake
 *----------------------------------------------------------------------------*/
uint8_t GetAcceptanceBrakePickCommand( BrakeSelect eBrakeSelect );

/* Bootloader reset checks */
uint8_t CheckIfOffline_MRB();
uint8_t CheckIfOffline_CTA();
uint8_t CheckIfOffline_COPA();

/* Hoistway access door checks */
uint8_t GetCarDoorSafeHoistwayAccess();
uint8_t GetHallDoorSafeHoistwayAccess ( void );

void SetDefaultParameterCommand( enum_default_cmd eCommand );

/* Auto OOS */
void SetOOSFault(uint8_t ucSubFault);
void UnsetOOSFault(uint8_t bInspection);
uint8_t GetOOSFault(void);
enum enOOSSubFaults GetOOSSubFault(void);

enum direction_enum GetArrivalLamp_Front();
enum direction_enum GetArrivalLamp_Rear();
uint8_t GetCurrentCarDirection();

uint8_t CheckIf_AllStartupSignalsReceived();

uint16_t GetDebugBusOfflineCounter_CAN1();
uint16_t GetDebugBusOfflineCounter_CAN2();

uint8_t GetClosestValidOpening( enum direction_enum eDir, enum en_doors eDoor );

void AbortCallsAtCurrentLanding();

uint8_t Safety_GetInCarStopBypass();

uint8_t Doors_GetDwellCompleteFlag( enum en_doors enDoorIndex );

uint8_t Overload_GetBuzzerFlag(void);

uint8_t GetLWD_LearnFloorRequest_Plus1(void);
uint8_t GetLWD_CaptureFlag(void);

uint8_t CheckInRequired(uint8_t ucFloorIndex, uint8_t bRearDoor);

uint32_t GetCarCallSecurityBitmap(uint8_t ucBitmapIndex, uint8_t bRear);

/*-----------------------------------------------------------------------------
   Returns projected destinations in the following encoding:
      Bits  0 to  7 :  ucFloorIndex
      Bits  8 to  9 :  the arrival direction, en_hc_dir_plus1
      Bits 10 to 11 :  the call type, en_hc_dir_plus1
      Bits 12       :  bDirectionChange_HC, 1 if the destination is a HC in the opposite direction
-----------------------------------------------------------------------------*/
uint16_t Dispatch_GetDebugDestination_Current(void);
uint16_t Dispatch_GetDebugDestination_Next_SameDir(void);
uint16_t Dispatch_GetDebugDestination_Next_DiffDir(void);

uint8_t AutoModeRules_GetAutoDoorOpenFlag(void);
uint8_t AutoModeRules_GetDoorHoldFlag(void);
uint8_t AutoModeRules_GetForceDoorsOpenOrClosedFlag(void);
uint8_t AutoModeRules_GetIgnoreDCBFlag(void);

/*----------------------------------------------------------------------------
The following handles control signals for the unintended movement test feature requested by NYC.
This feature allows testing of unintended movement in any mode of operation.
 *----------------------------------------------------------------------------*/
uint8_t Test_UM_GetTestActiveFlag(void);
void Test_UM_UpdateControlSignals(uint16_t uwRunPeriod__1ms);
uint8_t Test_UM_GetPickBrakeFlag(void);
uint8_t Test_UM_GetPickEBrakeFlag(void);
uint8_t Test_UM_GetPickB2Flag(void);
#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
