/******************************************************************************
 *
 * @file     mod.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef _MOD_MOTION_H
#define _MOD_MOTION_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_a.h"
#include "motion.h"
#include "sys.h"
#include <stdint.h>
#include "GlobalData.h"
#include "pattern.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_MOTION_1MS                           (5U)

#define DEBOUNCE_PREFLIGHT_COMPLETE_MS                      (100U)

#define ADDED_ACCEL_TIME_DELAY_SEC                          (1)

/* Timeouts */
#define TIMEOUT_PREPARE_TO_RUN_1MS                          (3000)
#define TIMEOUT_DRIVE_HW_ENABLE_1MS                         (3000)
#define TIMEOUT_PICK_M_1MS                                  (3000)
#define TIMEOUT_SPEED_REG_RLS_1MS                           (3000)
#define TIMEOUT_PICK_B2_1MS                                 (3000)
#define TIMEOUT_PICK_B_1MS                                  (3000)
#define TIMEOUT_PICK_BRAKES_1MS                             (5000)//Time to await BPS after picking brakes
#define TIMEOUT_RAMP_TO_ZERO_1MS                            (1500) //Timeout Zero Speed
#define TIMEOUT_HOLD_ZERO_1MS                               (1500) //Timeout Hold Zero
#define TIMEOUT_MOTOR_DEENERGIZE_1MS                        (8000)//(5000)
#define TIMEOUT_DROP_M_1MS                                  (3000) //Timeout drop M
#define TIMEOUT_DROP_BRAKE_1MS                              (3000) //Timeout Drop brake
#define TIMEOUT_PREFLIGHT_1MS                               (10000)
#define TIMEOUT_ACCEL_DELAY_1MS                             (3000)
#define DEFAULT_MANUAL_DOORS_PREPARE_TO_RUN_TIMEOUT_1MS     (4000)
#define MIN_TRUNCATED_ACCEL_TIME_OFFSET_SEC                 ( 0.0f )

#define START_SEQ_DSD_PRETORQUE_ASSERTION_TIME_MS           (200)

#define MIN_EBRAKE_DROP_TIME__MS                            (1000)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

extern struct st_module gstMod_Motion;

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   To set flag for acceptance test ETS/NTS runs. Bypasses decel
 *----------------------------------------------------------------------------*/
void SetAcceptanceTestBypassDecelFlag( uint8_t bBypass );
/*----------------------------------------------------------------------------
   To set flag to trigger overspeed for acceptance testing
 *----------------------------------------------------------------------------*/
void SetAcceptanceTestOverspeedFlag( uint8_t bTriggerOverspeed );

/*----------------------------------------------------------------------------
   Get flag indicating car has not moved for parameter determined time period
 *----------------------------------------------------------------------------*/
uint8_t GetCarStoppedFlag();

/*----------------------------------------------------------------------------
   Returns the car's floor at the start of a run
 *----------------------------------------------------------------------------*/
uint8_t Motion_GetStartOfRunFloor();
/*----------------------------------------------------------------------------
   Flag access functions
 *----------------------------------------------------------------------------*/
uint8_t GetPreflightFlag();
uint8_t GetDriveHWEnableFlag();
uint8_t GetPickB2Flag();
uint8_t GetPickMFlag();
uint8_t GetPickEBrakeFlag();
uint8_t GetPickDriveFlag();
uint8_t GetPickBrakeFlag();
uint8_t Motion_GetRelevelingFlag();
/*----------------------------------------------------------------------------
   Module state functions
 *----------------------------------------------------------------------------*/
enum en_motion_stop_sequence Motion_GetStopState(void);
enum en_motion_start_sequence Motion_GetStartState(void);
enum en_motion_state Motion_GetMotionState(void);
enum en_patterns Motion_GetPatternState(void);
uint8_t Motion_GetLevelingFlag();
/*----------------------------------------------------------------------------
   Debug functions
 *----------------------------------------------------------------------------*/
uint32_t Motion_GetStartPosition_Accel();
uint32_t Motion_GetStartPosition_Cruise();
uint32_t Motion_GetStartPosition_Decel();
uint32_t Motion_GetSlowdownDistance();
uint32_t Motion_GetRampUpDistance();
uint32_t Motion_GetRSLDistance();
uint8_t Motion_GetMaxFloorToFloorTime_sec(void);
uint32_t Motion_GetLastStopPosition(void);
/*----------------------------------------------------------------------------
   State Control functions
 *----------------------------------------------------------------------------*/
void MotionState_StartSequence( Motion_Control *pstMotionCtrl );
void MotionState_StopSequence( Motion_Control *pstMotionCtrl );
void ClearAllMotionRunFlags();
void ClearAllStartSeqTimers();
void ClearAllStopSeqTimers();
void EmergencyStop( Motion_Control *pstMotionCtrl );
uint8_t CheckIf_ValidManualRun();
uint8_t CheckIf_ValidDestinationRun();

/*----------------------------------------------------------------------------
   Secondary Brake Control Sequence
 *----------------------------------------------------------------------------*/
void StopSequence_DropEBrake( Motion_Control *pstMotionCtrl );
void StopSequence_DropB2( Motion_Control *pstMotionCtrl );

void StopSequence_Preflight( Motion_Control *pstMotionCtrl );
/*----------------------------------------------------------------------------
   Generate run functions
 *----------------------------------------------------------------------------*/
uint8_t GenerateRun_Auto();
uint8_t GenerateRun_Manual();

/*----------------------------------------------------------------------------
   Dispatching functions
 *----------------------------------------------------------------------------*/
uint8_t Motion_GetRemainingDecelTime();
uint8_t Motion_GetNextReachableFloor();

/*----------------------------------------------------------------------------
   Speed Limit Change Operation
 *----------------------------------------------------------------------------*/
uint8_t CheckIfReducedSpeedLimit( Motion_Control *pstMotionCtrl );
void SpeedLimitChangeDuringAccel( Motion_Control *pstMotionCtrl );
void SpeedLimitChangeDuringCruising( Motion_Control *pstMotionCtrl );
void MotionState_ReducedSpeedLimit( Motion_Control *pstMotionCtrl );


uint8_t CheckIfQuickStop();
#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
