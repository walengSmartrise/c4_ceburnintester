/******************************************************************************
 *
 * @file     mod_brake.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef MOD_BRAKE_H_
#define MOD_BRAKE_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_a.h"
#include "sys.h"
#include "motion.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_BRAKE_1MS             ( 10U )
#define MIN_BRAKE_HEARTBEAT_TIMER_5MS        ( 30U )
#define MAX_BRAKE_HEARTBEAT_TIMER_5MS        ( 150U )
#define BRAKE_OFFLINE_TIMEOUT_1MS            ( 2000U )
#define MAX_BRAKE_TIMEOUT_MS (3000)
#define MIN_BRAKE_TIMEOUT_MS (200)
#define MIN_BPS_ERROR_TIMEOUT_1MS            ( 3000U )

#define START_BRAKE_LEARN_VOLTAGE            ( 60 )
#define END_BRAKE_LEARN_VOLTAGE              ( 10 )
#define MAX_BRAKE_DC_VOLTAGE_120VAC          ( 160 )
#define BRAKE_LEARN_UPDATE_RATE_10MS         ( 200 )
#define BRAKE_LEARN_VOLT_CHANGE_PER_UPDATE   ( 5 )
/* TODO Brake learn needs to be formally integrated as safe learn mode with zero speed drive control */
#define ENABLE_BRAKE_LEARN                   (0)

/* MOSFET fault must occur 3 consecutive times before latching */
#define MOSFET_FAULT_CONSECUTIVE_LIMIT       ( 3 )
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
enum en_ramp_dowm_times
{
   EMERGENCY_STOP_RAMP_TIME,
   QUICK_STOP_RAMP_TIME,
   NORMAL_STOP_RAMP_TIME,

   NUM_RAMP_DOWN_TIMES
};
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_brake gstBrake;
extern struct st_brake gstBrake2;
extern struct st_module gstMod_Brake;
extern struct st_module gstMod_Brake2;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
int ResendBrakeDatagram( void );
int ResendEBrakeDatagram( void );
uint8_t Brake_GetBPS();
uint8_t Brake2_GetBPS();
uint8_t Brake_GetDropEBFlag(void);
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
