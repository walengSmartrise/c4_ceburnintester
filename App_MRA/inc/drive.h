/******************************************************************************
 *
 * @file     drive.h
 * @brief
 * @version  V1.00
 * @date     1, April 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef DRIVE_H
#define DRIVE_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include "sys.h"
#include "mod.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define MAX_MAGNETEK_SPEED    (8192.0f)
#define MIN_MAGNETEK_SPEED    (-8192.0f)

#define ASCII_EOT_CHAR   (0x04)
#define ASCII_ENQ_CHAR   0x05
#define ASCII_STX_CHAR   0x02
#define ASCII_ETX_CHAR   0x03
#define ASCII_ACK_CHAR   0x06
#define ASCII_NAK_CHAR   0x15

#define HPV_SYNC_BYTE               (0xFA)

#define HPV_SETUP_ID__READ_16BIT_PARAM     (0x0A)
#define HPV_SETUP_ID__WRITE_16BIT_PARAM    (0x06)

#define DRIVE_UART_RX_BYTE_LIMIT       (40)
#define ENABLE_NEW_HPV_UNLOAD (1)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

typedef struct
{
      uint32_t uiParameterID;
      uint32_t uiValue;
} Drive_Parameter_Object;
//----------------------------------------------------------
typedef enum
{
   DRIVE_MSG__RUNTIME,
   DRIVE_MSG__READ_PARAM,
   DRIVE_MSG__WRITE_PARAM,

   NUM_DRIVE_MSG
}Drive_Msg_Type;

//----------------------------------------------------------

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

uint8_t GetSpeedRegReleased();
uint8_t GetDriveFault();
int16_t GetDriveSpeedFeedback();

int16_t GetMagnetekSpeedCommand( void );
int16_t ConvertMagnetekSpeedToFPM( int16_t wMagnetekSpeed );

int16_t GetMagnetekPretorqueCommand();

void LoadInterface_HPV( struct st_drive * pstDrive );
void LoadInterface_KEB( struct st_drive * pstDrive );
void LoadInterface_DSD( struct st_drive * pstDrive );
void LoadInterface_DELTA( struct st_drive * pstDrive );

void Init_UART_RingBuffer(enum en_drive_type eDrive);
uint32_t UART_SendRB(const void *data, int bytes);
uint8_t UART_ReadByte(uint8_t *pucByte);

uint8_t Drive_GetMotorLearnFlag();

void DriveParam_ClrDriveEditConfirm();
void DriveParam_UnloadDriveResponse( uint16_t uwID, uint32_t uiValue );
en_drive_commands DriveParam_GetConfirmCmd();
uint16_t DriveParam_GetConfirmID();
uint32_t DriveParam_GetConfirmValue();

uint16_t GetDriveRxErrorCount_HPV(void);
uint16_t GetDriveRxErrorCount_KEB(void);
uint16_t GetDriveRxErrorCount_DSD(void);
uint16_t Drive_GetRxErrorCount(void);

uint8_t CheckDriveType( enum en_drive_type eType );
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
