/******************************************************************************
 *
 * @file     mod_fire.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note     Fire operation module header file
 *
 ******************************************************************************/

#ifndef MOD_FIRE_H_
#define MOD_FIRE_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_a.h"
#include "sys.h"

#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_FIRE_1MS        (50U)

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
enum en_fire_lamp
{
   FireLamp__OFF,
   FireLamp__ON,
   FireLamp__FLASHING,

   NUM_FIRE_LAMP_STATES
};

enum en_Smokes {
   SMOKE_NO_SMOKE_ACTIVE,
   SMOKE_FIRE_RECALL,
   SMOKE_MAIN,
   SMOKE_ALT,
   SMOKE_MR,
   SMOKE_HA,
   SMOKE_SAVED_SMOKE,
   SMOKE_PIT,
   SMOKE_MR_2, /* Smoke from external MR in same group */
   SMOKE_HA_2, /* Smoke from external Hoistway in same group */
   SMOKE__NUMBER_OF_SMOKES
};

enum en_FireKeyState {
   KEY_UNKNOWN,
   KEY_OFF,
   KEY_RESET,
   KEY_ON,
   KEY_NUM_OF
};
typedef enum
{
   FIRE_SERVICE__INACTIVE,
   FIRE_SERVICE__PHASE1,
   FIRE_SERVICE__PHASE2,
   FIRE_SERVICE__FLOOD_BYP,

   NUM_FIRE_SERVICES
}en_fire_services;
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_module gstMod_Fire;

extern uint8_t gbInCarFireLamp;
extern uint8_t gbLobbyFireLamp;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void Fire_Phase_1 ( uint8_t bInitMode );
void Fire_Phase_2 ( uint8_t bInitMode );

en_fire_services Fire_GetFireService( void );
enum en_fire_2_states Fire_GetFireService_2_State ( void );
uint8_t Fire_AtPhase2RecallFloor();
uint8_t Fire_GetBuzzer(void);
void Fire_SetBuzzer(uint8_t bOn);
uint8_t Fire_CheckIfFire2DoorsEnabled(void);
uint8_t Fire_CheckIfFire1Recall(void);
uint8_t Fire_CheckIfFireRecallDelayed( void );
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
