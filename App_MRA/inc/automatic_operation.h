/******************************************************************************
 *
 * @file     mod.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef AUTOMATIC_OPERATION_H
#define AUTOMATIC_OPERATION_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <seggerDebug.h>
#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/* Get module states */
enum en_automatic_state Auto_GetState(void);

uint8_t GetFF2_MomentaryCloseButton(enum en_doors enDoorIndex);

void Auto_Set_Limits( void );
void Auto_Set_Limits_Flood ( void);
void Auto_SetSpeed ( uint16_t uwSpeed );
void Auto_Oper( uint8_t bInitMode );


uint8_t GetParkingLamp(void);

uint8_t PreOpeningCheck(void);

uint8_t Auto_CheckDoorRequests(void);
uint8_t Auto_GetPreopeningFlag(void);

uint8_t Auto_CheckDoorOpenButtons ( void );
#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
