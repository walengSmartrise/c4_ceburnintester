
#ifndef GLOBAL_DATA_H
#define GLOBAL_DATA_H

#include <stdint.h>
#include "sys.h"
#include "fram_data.h"
#include "sru.h"

#define LOWER_RELEVELING_THRESHHOLD (13)
#define UPPER_RELEVELING_THRESHHOLD (100)
typedef enum
{
   ADA_TYPE__CC,
   ADA_TYPE__UP,
   ADA_TYPE__DN,

   NUM_ADA_TYPE
} en_ada_type;
typedef struct
{
      uint8_t ucCounter;
      uint8_t ucFloorIndex;
      en_ada_type eType;
} st_ADARequest;

/*-----------------------------------------------------------------------------
   SRU
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
   Parameters
 -----------------------------------------------------------------------------*/
uint8_t GetFP_FlashParamsReady();
void SetFP_FlashParamsReady();
uint8_t GetFP_NumFloors(void);
void SetFP_NumFloors(uint8_t ucFloors);
uint8_t GetFP_RearDoors(void);
void SetFP_RearDoors(uint8_t bDoor);
en_door_type GetFP_DoorType( enum en_doors eDoor );
void SetFP_DoorType( en_door_type enDoorType, enum en_doors eDoor );
uint8_t GetFP_FreightDoors(void);
void SetFP_FreightDoors(uint8_t bDoor);
uint16_t GetFP_ContractSpeed( void );
void SetFP_ContractSpeed( uint16_t uwSpeed );
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetInputBitMap(uint32_t ulBitmap, uint8_t ucIndex);
void SetOutputBitMap(uint32_t ulBitmap, uint8_t ucIndex);
uint32_t GetInputBitMap(uint8_t ucIndex);
uint32_t GetOutputBitMap(uint8_t ucIndex);
uint8_t GetOutputBitmapValue(enum en_output_functions enOutput);

void SetStructPosition_NTS(struct st_position_b *pstPosition);
void GetStructPosition_NTS(struct st_position_b *pstPosition);

void SetNTSPosition(uint32_t ulPosition);
uint32_t GetNTSPostion();
void SetNTSVelocity(int16_t wVelocity);
int16_t GetNTSVelocity();

/*-----------------------------------------------------------------------------
   UI Requests
 -----------------------------------------------------------------------------*/

void InitCarCallQue ( void );
uint8_t SetCarCall (uint8_t ucFloorIndex, uint8_t ucDoorIndex);
uint8_t GetCarCallRequest ( enum en_doors eDoor );

uint8_t GetUIRequest_FaultLog();

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetBitMap_LatchedCCB(uint32_t ulCarCalls, uint8_t ucIndex, uint8_t bRear);
uint32_t GetBitMap_LatchedCCB(uint8_t ucIndex, uint8_t bRear);

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/



void SetModState_AutoState(uint8_t ucState );
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetArrivalUpDn(uint8_t ucArrivalUpDn);
uint8_t GetArrivalUpDn();

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t GetModState_AutoState();
void SetModState_FloorLearnState(uint8_t ucState );
uint8_t GetModState_FloorLearnState();

//void SetModState_DoorState( enum en_door_states ucDoorState );
//uint8_t GetModState_DoorState(void);
void SetModState_FireSrv(uint8_t ucState );
uint8_t GetModState_FireSrv();
void SetModState_FireSrv2(uint8_t ucState );
uint8_t GetModState_FireSrv2();
void SetModState_Counterweight(uint8_t ucState );
uint8_t GetModState_Counterweight();
void SetModState_Recall(uint8_t ucState );
uint8_t GetModState_Recall();

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetFaultLogElement(st_fault_data *pstFLog, uint8_t ucIndex_Plus1);
void GetFaultLogElement(st_fault_data *pstFLog, uint8_t ucIndex_Plus1);

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetEmergencyBitmap(uint32_t uiBitmap);
uint32_t GetEmergencyBitmap();
uint8_t GetEmergencyBit( enum en_emergency_bitmap enEmergencyBF );
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetAcceptanceTest_Plus1( uint32_t uiIndex_Plus1 );
uint32_t GetAcceptanceTest_Plus1();



/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetLatchedFaultBitmap(uint32_t ulBitmap);
uint32_t GetLatchedFaultBitmap();
uint8_t GetLatchingSafetyFault( enum en_latching_safety_faults eFault );
void ClearLatchingSafetyFault( enum en_latching_safety_faults eFault );

/*-----------------------------------------------------------------------------
   Returns 1 if valid opening
 ----------------------------------------------------------------------------*/
uint8_t GetFloorOpening_Front( uint8_t ucFloor );
uint8_t GetFloorOpening_Rear( uint8_t ucFloor );

/*-----------------------------------------------------------------------------
   ADA requests
 ----------------------------------------------------------------------------*/
void Init_ADARequest();
uint8_t Get_ADARequest( uint8_t ucFloor, en_ada_type eType );
void Set_ADARequest( uint8_t ucFloor, en_ada_type eType );
void Clr_ADARequest( uint8_t ucFloor, en_ada_type eType );
uint8_t GetADAFlag();
void SetADAFlag(uint8_t bFlag);

/*-----------------------------------------------------------------------------
   View Debug Data Requests
 ----------------------------------------------------------------------------*/
en_view_debug_data GetUIRequest_ViewDebugDataCommand(void);
/*-----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
uint8_t GetOverLoadFlag(void);
uint8_t GetFullLoadFlag(void);
uint8_t GetLightLoadFlag(void);
uint16_t GetInCarWeight_lb(void);
/*-----------------------------------------------------------------------------
   Check if car is inside virtual dead zone
 ----------------------------------------------------------------------------*/
uint8_t InsideDeadZone( void );

/*-----------------------------------------------------------------------------
   Get DL20 flags
 ----------------------------------------------------------------------------*/
uint8_t DL20_GetPhoneFailureFlag(void);
uint8_t DL20_GetOOSFlag(void);

/*-----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
uint8_t Door_GetSwingClosedContactsProgrammedFlag( enum en_doors eDoor );
void Door_SetSwingClosedContactsProgrammedFlag( enum en_doors eDoor );

#endif // GLOBAL_DATA_H
