/******************************************************************************
 *
 * @file     mod_doors.h
 * @brief    Doors module header file
 * @version  V1.00
 * @date     29, Aug 2018
 *
 * @note
 *
 ******************************************************************************/

#ifndef MOD_DOORS_H_
#define MOD_DOORS_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_a.h"
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_DOORS_1MS        (100U)

// Scale factor needed to convert a time to a 1 second time base.
// Module runs every 5ms -. 1s/100ms = 10
#define MOD_DOORS_ONE_SECOND_SCALE_FACTOR ( 1000/MOD_RUN_PERIOD_DOORS_1MS )

#define DOOR_FAILED_TO_OPEN_TIMEOUT_100MS             (200)
#define DOOR_WARNING_BUZZER_START_TIMER_100MS         (5 * 1000/MOD_RUN_PERIOD_DOORS_1MS)
#define DEFAULT_MANUAL_DOOR_CALL_CLEAR_TIME_100MS     (50)
#define DOOR_FAILED_TO_CLOSE_TIMEOUT_100MS            (200)
#define DOOR_FAILED_TO_CLOSE_MANUAL_TIMEOUT_100MS     (10)
#define DEBOUNCE_NO_DEMAND_DOOR_OPEN_SIGNAL_100MS     (30)
#define PHE_TEST_TIMEOUT_100MS                        (150)
#define PHE_TEST_COUNTER_TIME_100MS                   (5)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   PHE_TEST_STATE__WAITING, // Waiting to start photoeye test
   PHE_TEST_STATE__IN_PROGRESS, // Testing in progress, test output asserted
   PHE_TEST_STATE__PASSED, // Test has passed, door may close
   NUM_PHE_TEST_STATES
} en_phe_test_states;

struct st_door_control {
      enum en_door_commands enLastDoorCommand;
      enum en_door_commands enDoorCommand;
      enum en_door_states enCurrentDoorState;
      enum en_door_states enLastDoorState;
      enum en_door_states enLastFullDoorState; // For bForceDoorsOpenOrClosed door option

      uint16_t uwCurrentTimer_100ms; /* This timer is reset any time a new door command is sent */
      uint16_t uwTimer_100ms; /* This timer is reset any time the door is fully closed or a new command is set */

      /* Timer used to estimate door fully open state during preflight in order to not increase set dwell time */
      uint8_t ucOpeningTime_100ms;

      /* Timer used to debounce loss of door fully opened signals when in door open state */
      uint16_t uwOpenLimitTimer_100ms;
      /* Timer used to debounce loss of door fully closed signals when in door closed state */
      uint16_t uwCloseLimitTimer_100ms;

      /* Timer used to debounce loss of door fully closed signals when in door closed state */
      uint16_t uwCallDropTimer_100ms;

      /* PHE test variables */
      en_phe_test_states eTestPHE_State;
      uint8_t ucTestPHE_Counter_100ms; // Time that both PHE signals must be low
      uint8_t ucTestPHE_Timeout_100ms; // Time before failing the PHE test triggers a fault
};
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_module gstMod_Doors;

extern en_door_type aenDoorType[NUM_OF_DOORS];
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

enum en_door_states GetDoorState( enum en_doors enDoorIndex );
uint8_t SetDoorCommand( enum en_doors enDoorIndex, enum en_door_commands enDoorCommand );
enum en_door_commands GetLastDoorCommand( enum en_doors enDoorIndex );
uint8_t GetNumberOfDoors ( void );
uint8_t getDoorZone (enum en_doors enDoorIndex);
uint8_t GetDoorsFieldEnableFlag(void);
uint8_t DoorsSafeAndReadyToRun ( void );
uint8_t AnyDoorOpen ( void );
uint8_t AnyDoorFullyOpen ( void );
uint8_t AllHallDoorsClosed ( void );
uint8_t AllHallDoorsClosed_Inspection( void );
uint8_t CarDoorsClosed ( void );
void CloseAnyOpenDoor ( void );
void ResetDoors (void );
uint8_t GetDoorOpenButton ( enum en_doors enDoorIndex );
uint8_t GetDoorCloseButton ( enum en_doors enDoorIndex );
uint8_t Doors_GetBypassHallDoors();
uint8_t Doors_GetBypassCarDoors();
uint8_t Doors_GetNoDemandDoorsOpen(void);
uint8_t Doors_GetBuzzerFlag();
void Doors_ClearDoorHoldCommand(enum en_doors enDoorIndex);
void Doors_ForceDoorsFullyOpen( enum en_doors enDoorIndex );
void Doors_HoldDoorOpen( enum en_doors enDoorIndex );

/* Door Input checks */
uint8_t GetDOL ( enum en_doors enDoorIndex );
uint8_t GetDCL ( enum en_doors enDoorIndex );
uint8_t GetDPM ( enum en_doors enDoorIndex );
uint8_t GetGSW ( enum en_doors enDoorIndex );
uint8_t GetPhotoEye ( enum en_doors enDoorIndex );
uint8_t GetDZ ( enum en_doors enDoorIndex );
uint8_t GetLocks ( enum en_doors enDoorIndex );
uint8_t GetLock_Top( enum en_doors enDoorIndex );
uint8_t GetLock_Mid( enum en_doors enDoorIndex );
uint8_t GetLock_Bot( enum en_doors enDoorIndex );
uint8_t GetCloseContacts ( enum en_doors enDoorIndex );
uint8_t GetCloseContact_Top( enum en_doors enDoorIndex );
uint8_t GetCloseContact_Mid( enum en_doors enDoorIndex );
uint8_t GetCloseContact_Bot( enum en_doors enDoorIndex );
uint8_t GetSafetyEdge( enum en_doors enDoorIndex );
uint8_t GetCAM( enum en_doors enDoorIndex );
uint8_t IsDoorFullyClosed( enum en_doors enDoorIndex );
uint8_t IsDoorFullyOpen( enum en_doors enDoorIndex );

/*----------------------------------------------------------------------------
   View Debug Data diagnostics
      Bits  0 to  4: Current Door Command (enum en_door_commands)
      Bits  5 to  9: Last Door Command (enum en_door_commands)
      Bits 10 to 20: Current Timer 200MS
      Bits 21 to 31: Timer Limit 200MS
 *----------------------------------------------------------------------------*/
uint32_t Door_GetDebugDoorData( enum en_doors enDoorIndex );

uint8_t Doors_CheckIfManualSwingDoor( enum en_doors enDoorIndex, uint8_t ucFloorIndex );
uint8_t Doors_CheckIfSwingLocksExpectedClosed( enum en_doors enDoorIndex );
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
