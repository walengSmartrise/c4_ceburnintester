/******************************************************************************
 *
 * @file     doors_private.h
 * @brief    Private Doors module header file
 * @version  V1.00
 * @date     29, Aug 2018
 *
 * @note
 *
 ******************************************************************************/

#ifndef _DOORS_PRIVATE_H_
#define _DOORS_PRIVATE_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_a.h"
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
enum en_door_faults
{
   DOOR_FAULT__NONE,
   DOOR_FAULT__JUMPER_ON_GSW,
   DOOR_FAULT__JUMPER_ON_LOCK,
   DOOR_FAULT__LOCKS_OPEN,// only checked when failing to close
   DOOR_FAULT__GSW_OPEN,// only checked when failing to close
   DOOR_FAULT__FAILED_TO_OPEN,
   DOOR_FAULT__FAILED_TO_CLOSE,
   DOOR_FAULT__UNUSED7,
   DOOR_FAULT__UNUSED8, // Unused
   DOOR_FAULT__LOST_DOOR_SIGNAL,//In motion and doors not fully closed

   NUM_DOOR_FAULTS
};
enum en_door_reports
{
   DOOR_REPORT_OPEN,
   DOOR_REPORT_OPENING,
   DOOR_REPORT_CLOSING,
   DOOR_REPORT_NUDGING,

   NUMBER_OF_DOOR_REPORT
};
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/* doorFault.c */
void DoorFaultHandler( enum en_doors enDoorIndex );
void SetDoorFault( enum en_doors enDoorIndex, enum en_door_faults eFault );
enum en_door_faults GetDoorFault( enum en_doors enDoorIndex );
enum en_door_faults GetLastDoorFault( enum en_doors enDoorIndex );


/* mod_doors.c */
void SetDoorState( enum en_doors enDoorIndex, enum en_door_states enDoorState );

void SetTimersForDoorFault(enum en_doors enDoorIndex, uint16_t uwCurrentTimer_100ms, uint16_t uwLimitTimer_100ms);
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
