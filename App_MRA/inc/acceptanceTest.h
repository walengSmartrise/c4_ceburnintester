#ifndef _ACCEPTANCE_TEST
#define _ACCEPTANCE_TEST

#include "mod.h"
#include "sys.h"
#include "operation.h"


// THIS FILE IS NOT RELATED TO ACCEPTANCE TEST AT ALL.
// The name was chosen by Ben. This file is really the commands for semi_automatic mode.
uint8_t GetManualMode( void );
void MoveUpManual( void );
void MoveDownManual( void );
void StopManual( void );
void MoveAutomatic( uint8_t ucDestination );



uint8_t TimerReducedExpired(void);
uint8_t TimerExpired(void);
uint8_t TimerExtExpired(void);
uint8_t TimeoutReached( void );
uint8_t TimeoutReached_AscDescOvsp( void );
void ResetTimer( void );
void IncrementTimer( void );
uint8_t AtTopFloor( void );


uint8_t AtBottomFloor( void );
uint8_t NoActiveFaultOrAlarm( void );
uint8_t OnAcceptanceTest( void );



uint8_t OnETS_AcceptanceTest( void );
void SetETS_AcceptanceTest( uint8_t );

void Acceptance_ClearAllPickCommands(void);

#endif
