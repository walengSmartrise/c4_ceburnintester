/******************************************************************************
 *
 * @file     gstMod_IdleTime.c
 * @brief    Maintains idle timers used by other auto operation modules
 * @version  V1.00
 * @date     19, March 2020
 *
 * @note     Maintains idle timers used by other auto operation features such as parking
 *
 ******************************************************************************/

#ifndef IDLE_TIMER_H
#define IDLE_TIMER_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <seggerDebug.h>
#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_IDLE_TIME_1MS               (100U)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_module gstMod_IdleTime;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
uint16_t IdleTime_GetParkingTime_100MS(void);
uint16_t IdleTime_GetParkingTime_Seconds(void);
uint16_t IdleTime_GetParkingTime_Minutes(void);

uint16_t IdleTime_GetDirectionChangeTime_100MS(void);
uint16_t IdleTime_GetDirectionChangeTime_Seconds(void);
uint16_t IdleTime_GetDirectionChangeTime_Minutes(void);

uint16_t IdleTime_GetFanAndLightTime_100MS(void);
uint16_t IdleTime_GetFanAndLightTime_Seconds(void);
uint16_t IdleTime_GetFanAndLightTime_Minutes(void);

uint16_t IdleTime_GetFaultedTime_100MS(void);
uint16_t IdleTime_GetFaultedTime_Seconds(void);
uint16_t IdleTime_GetFaultedTime_Minutes(void);

uint8_t IdleTime_GetParkingFloor(void);
en_recall_door_command IdleTime_GetParkingDoorCommand(void);
uint8_t IdleTime_GetReadyToPark(void);
uint8_t IdleTime_GetParkingFloor_Plus1(void);
#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
