/******************************************************************************
 *
 * @file     mod_input_mapping.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef MOD_INPUT_MAPPING_H_
#define MOD_INPUT_MAPPING_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_a.h"
#include "sys.h"

#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_INPUT_MAPPING_1MS        (5U)

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_module gstMod_InputMapping;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
uint8_t CheckIfInputIsProgrammed( enum en_input_functions eInput );
void SetMachineRoomExtInputs( uint32_t uiInputs, uint8_t ucIndex );
void SetLocalInputs( uint32_t uiInputs, enum en_io_boards eBoard );
uint8_t GetInputValue( enum en_input_functions eInput );
void SetInputValue( enum en_input_functions eInput, uint8_t bValue );
uint8_t GetCarCallEnable( uint8_t ucFloorIndex, uint8_t bRearDoor);
uint8_t CheckIfAllDoorInputsInitialized();

uint16_t CheckForClearingCarCallCommand(void);
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
