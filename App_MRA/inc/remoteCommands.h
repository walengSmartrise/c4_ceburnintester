/******************************************************************************
 *
 * @file     remoteCommands.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     13 August 2019
 *
 * @note
 *
 ******************************************************************************/

#ifndef _REMOTE_COMMANDS_H_
#define _REMOTE_COMMANDS_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_a.h"
#include "sys.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   VIRTUAL_CARCALL_SECURITY,
   VIRTUAL_HALLCALL_SECURITY,
   VIRTUAL_SYSTEM_INPUTS,

   NUM_VIRTUAL_INPUTS_TYPES,
} en_virtual_input_types;

typedef enum
{
   REMOTE_DISPATCH_MODE,
   REMOTE_CARCALL_SECURITY_F0,
   REMOTE_CARCALL_SECURITY_F1,
   REMOTE_CARCALL_SECURITY_F2,

   REMOTE_CARCALL_SECURITY_R0,
   REMOTE_CARCALL_SECURITY_R1,
   REMOTE_CARCALL_SECURITY_R2,

   REMOTE_HALLCALL_SECURITY_F0,
   REMOTE_HALLCALL_SECURITY_F1,
   REMOTE_HALLCALL_SECURITY_F2,

   REMOTE_HALLCALL_SECURITY_R0,
   REMOTE_HALLCALL_SECURITY_R1,
   REMOTE_HALLCALL_SECURITY_R2,

   REMOTE_SYSTEM_INPUTS,

   NUM_REMOTE_DATA
} en_remote_data;

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void Unload_RemoteCommands( void );

en_dispatch_mode GetDispatchMode( void );
uint8_t GetVirtual_CarCall_Enable(uint8_t ucLanding, uint8_t ucDoorIndex);
uint8_t GetVirtual_SystemInput(en_virtual_system_inputs enVirtualInput);

uint32_t GetVirtual_CarCallSecurityBitmap(uint8_t ucMapIndex, uint8_t ucDoorIndex);

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
