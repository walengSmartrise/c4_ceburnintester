/******************************************************************************
 *
 * @file     mod_run_log.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef MOD_RUN_LOG_H_
#define MOD_RUN_LOG_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_a.h"
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_RUN_LOG_1MS        (50U)


#define ENABLE_RUN_LOG_SEQUENTIAL_CAPTURE        (0U)
#define LOG_DRIVE_SPEED_FEEDBACK                 (1U)

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   TP__Start_Drive_HW_EN,
   TP__Start_PickB2,
   TP__Start_PickM,
   TP__Start_LiftEBrake,
   TP__Start_HoldZero,
   TP__Start_LiftBrake,
   TP__Start_BPS,
   TP__Start_EB_BPS,
   TP__EndOfStart,
   TP__EndOfAccel,
   TP__EndOfCruising,
   TP__EndOfDecel,
   TP__EndOfLeveling,
   TP__Stop_HoldZero,
   TP__Stop_DropBrake,
   TP__Stop_DropDrive,
   TP__Stop_DropEBrake,
   TP__Stop_DropM,
   TP__Stop_DropB2,
   TP__Stop_Preflight,
   TP__Stop_BPS,
   TP__Stop_EB_BPS,
   NUM_TPs,
} Transition_Points;
typedef enum
{
   RL_STATE__IDLE,
   RL_STATE__START_SEQ,
   RL_STATE__ACCEL,
   RL_STATE__CRUISING,
   RL_STATE__DECEL,
   RL_STATE__LEVELING,
   RL_STATE__STOP_SEQ,

   NUM_RL_STATES
} Run_Log_States;


typedef struct
{
      int16_t wCmdSpeed;
      int16_t wCarSpeed;
      uint32_t uiCarPosition;
} Run_Log_Item;

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_module gstMod_RunLog;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
uint8_t GetRunLog_NewLogFlag();
/*-----------------------------------------------------------------------------
   Clear new log flag after completing transmission to
   prevent loading of new log while transmitting
   New log will not be loadeduntil flag is cleared
 -----------------------------------------------------------------------------*/
void ClrRunLog_NewLogFlag();

Run_Log_States GetRunLog_State();
uint32_t GetRunLog_AccelLogSize();
uint32_t GetRunLog_DecelLogSize();
uint8_t GetRunLog_AccelLogItem( Run_Log_Item *pstRunLogItem, uint32_t uiIndex );
uint8_t GetRunLog_DecelLogItem( Run_Log_Item *pstRunLogItem, uint32_t uiIndex );
int32_t GetRunLog_TransitionPointTimestamp( Transition_Points eTransitionPoint );

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
