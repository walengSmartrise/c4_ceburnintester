/******************************************************************************
 *
 * @file     fram.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef FRAM_H
#define FRAM_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_a.h"
#include "sys.h"
#include <stdint.h>
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define DUPLICATES_OF_EACH_BYTE             (3U)
#define NUM_BYTES_PER_FAULTITEM             (17U)
#define NUM_BYTES_FAULTLOG                  (NUM_BYTES_PER_FAULTITEM * NUM_FAULTLOG_ITEMS)

#define NUM_BYTES_PER_ALARMITEM             (11U)
#define NUM_BYTES_ALARMLOG                  (NUM_BYTES_PER_ALARMITEM * NUM_FAULTLOG_ITEMS)

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/* FRAM CMD Codes */
#define CMD_WR_ENABLE   (0x06)
#define CMD_WR_DISABLE  (0x04)
#define CMD_WR_STATUS   (0x01)
#define CMD_WR_MEMORY   (0x02)
#define CMD_RD_STATUS   (0x05)
#define CMD_RD_ID       (0x9F)
#define CMD_RD_MEMORY   (0x03)
#define CMD_RD_FAST     (0x0B)
#define CMD_SLEEP       (0xB9)

/* FRAM STATUS REGISTER Codes */
#define STATUS_REG_CMD__WR_EN         ( 0x02 )
#define STATUS_REG_CMD__BLK1_PROT     ( 0x04 )
#define STATUS_REG_CMD__BLK2_PROT     ( 0x08 )
#define STATUS_REG_CMD__WP_EN         ( 0x80 )

#define FRAM_TOTAL_NUM_BYTES                 (512000U)
#define FRAM_MAX_TRIPLET_ADDR                (FRAM_TOTAL_NUM_BYTES/DUPLICATES_OF_EACH_BYTE - 1)

#define FRAM_TOTAL_NUM_BYTES_NEW                 (32000U)
#define FRAM_MAX_TRIPLET_ADDR_NEW                (FRAM_TOTAL_NUM_BYTES_NEW/DUPLICATES_OF_EACH_BYTE - 1)

// TADDR = triplet address
#define FRAM_TADDR_FIRSTWRITETAG_START          (0)
#define FRAM_TADDR_FIRSTWRITETAG_END            (FRAM_TADDR_FIRSTWRITETAG_START + 3)

#define FRAM_TADDR_FLOGINDEX_START              (FRAM_TADDR_FIRSTWRITETAG_END)
#define FRAM_TADDR_FLOGINDEX_END                (FRAM_TADDR_FLOGINDEX_START + 1)

#define FRAM_TADDR_FAULTLOG_START               (FRAM_TADDR_FLOGINDEX_END)
#define FRAM_TADDR_FAULTLOG_END                 (FRAM_TADDR_FAULTLOG_START + NUM_BYTES_FAULTLOG)

#define FRAM_TADDR_EMERGENCYBITMAP_START        (FRAM_TADDR_FAULTLOG_END)
#define FRAM_TADDR_EMERGENCYBITMAP_END          (FRAM_TADDR_EMERGENCYBITMAP_START + 4)

#define FRAM_TADDR_RUNCOUNTER_START             (FRAM_TADDR_EMERGENCYBITMAP_END )
#define FRAM_TADDR_RUNCOUNTER_END               (FRAM_TADDR_RUNCOUNTER_START + 4)

#define FRAM_TADDR_LATCHEDFAULTS_START          (FRAM_TADDR_RUNCOUNTER_END )
#define FRAM_TADDR_LATCHEDFAULTS_END            (FRAM_TADDR_LATCHEDFAULTS_START + 4)

#define FRAM_TADDR_RECALL_FLOOR_START           (FRAM_TADDR_LATCHEDFAULTS_END )
#define FRAM_TADDR_RECALL_FLOOR_END             (FRAM_TADDR_RECALL_FLOOR_START + 2 )

#define FRAM_TADDR_ALOGINDEX_START              (FRAM_TADDR_RECALL_FLOOR_END)
#define FRAM_TADDR_ALOGINDEX_END                (FRAM_TADDR_ALOGINDEX_START + 1)

#define FRAM_TADDR_ALARMLOG_START               (FRAM_TADDR_ALOGINDEX_END)
#define FRAM_TADDR_ALARMLOG_END                 (FRAM_TADDR_ALARMLOG_START + NUM_BYTES_ALARMLOG)

#define FRAM_TADDR_DIR_CHANGE_COUNTER_START     (FRAM_TADDR_ALARMLOG_END)
#define FRAM_TADDR_DIR_CHANGE_COUNTER_END       (FRAM_TADDR_DIR_CHANGE_COUNTER_START + 2)

enum en_framstates
{
   enFRAMSTATE__Unknown,
   enFRAMSTATE__Initializing,//init spi
   enFRAMSTATE__CheckingForFirstWrite,//if first write, default before validating
   enFRAMSTATE__FetchingStartupVariables,
   enFRAMSTATE__Running,//
   enFRAMSTATE__Defaulting,//Defaulting of FRAM is occuring
};
enum en_fram_fault
{
   FRAM_FAULT__NONE,
   FRAM_FAULT__Defaulting,//Fram was new and underwent defaulting
   FRAM_FAULT__REDUNDANCY,
   FRAM_FAULT__RESTORE,//restoration and redundancy failure
   FRAM_FAULT__OVERFLOW,
};


/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_module gstMod_FRAM;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void FRAM_Init(void);

uint8_t FRAM_ReadStatus(void);
uint8_t FRAM_WriteStatus(uint8_t ucDataTx);

uint8_t FRAM_WriteMemory(uint8_t *pucData, uint32_t ulNumBytes, uint32_t ulStartAddress);
uint8_t FRAM_ReadMemory(uint8_t *pucData, uint32_t ulNumBytes, uint32_t ulStartAddress);

uint8_t FRAM_WriteMemory_Triplets(const uint8_t *pucData, uint8_t ucNumBytes, uint32_t ulStartTAddress);
uint8_t FRAM_ReadMemory_Triplets(uint8_t *pucData, uint8_t ucNumBytes, uint32_t ulStartTAddress);

enum en_fram_fault FRAM_GetFault();
enum en_framstates FRAM_GetState();
void FRAM_SetFault(enum en_fram_fault eFault);

void FRAM_ReadArray_U8(uint8_t *pucData, uint8_t ucSize, uint32_t ulAddr );
void FRAM_ReadArray_U16(uint16_t *puwData, uint8_t ucSize, uint32_t ulAddr );
void FRAM_ReadArray_U24(uint32_t *pulData, uint8_t ucSize, uint32_t ulAddr );
void FRAM_ReadArray_U32(uint32_t *pulData, uint8_t ucSize, uint32_t ulAddr );

void FRAM_WriteArray_U8(uint8_t *pucData, uint8_t ucSize, uint32_t ulAddr );
void FRAM_WriteArray_U16(uint16_t *puwData, uint8_t ucSize, uint32_t ulAddr );
void FRAM_WriteArray_U24(uint32_t *pulData, uint8_t ucSize, uint32_t ulAddr );
void FRAM_WriteArray_U32(uint32_t *pulData, uint8_t ucSize, uint32_t ulAddr );

/* FRAM - Emergency bitmap */
/*----------------------------------------------------------------------------
   Returns 1 if emergency bitmap is ready
 *----------------------------------------------------------------------------*/
uint8_t FRAM_EmergencyBitmapReady();
void FRAM_WriteEmergencyBitmap(uint32_t uiBitmap);
uint32_t FRAM_ReadEmergencyBitmap();
void FRAM_UpdateEmergencyBitmap();

/* FRAM - Latching faults */
void FRAM_WriteLatchedFaultBitmap(uint32_t ulBitmap);
void FRAM_WriteLatchedFaultBitmap(uint32_t ulBitmap);
uint32_t FRAM_ReadLatchedFaultBitmap();
void FRAM_UpdateLatchedFaults();
void SetLatchingSafetyFault( enum en_latching_safety_faults eFault );
void ClrLatchingSafetyFault( enum en_latching_safety_faults eFault );

/* FRAM - Fault Log */
void FRAM_UpdateFaultLog(void);
void FRAM_UpdateAlarmLog(void);
void LoadFaultLogDatagrams(void);

/* FRAM - Run Counter */
uint32_t FRAM_GetTotalRunCount();
void FRAM_UpdateRunCounter();

/* FRAM - Recall floor */
uint8_t FRAM_RecallFloorReady();
void FRAM_SetRecallFloor_Plus1( uint8_t ucFloor_Plus1 );
void FRAM_SetRecallDoor( uint8_t bRear );
uint8_t FRAM_GetRecallFloor_Plus1();
uint8_t FRAM_GetRecallDoor();
void FRAM_UpdateRecallFloor();

/* FRAM - Direction Change Counter */
uint16_t FRAM_GetDirChangeCount(void);
void FRAM_UpdateDirChangeCounter(void);
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
