/******************************************************************************
 *
 * @file     mod_ets.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef MOD_ETS_H_
#define MOD_ETS_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_a.h"
#include "sys.h"
#include "motion.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *---------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_ETS_1MS        (10U)

#define MAX_DEBOUNCE_LIMIT_OVERSPEED_ETS        (100)
// Scale factor is set to 10%.
#define ETS_VELOCITY_SCALE_FACTOR ( 1.1 )
#define ETS_VELOCITY_SCALE_FACTOR_TEST (0.85)

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_module gstMod_ETS;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void SetETS_Speed( uint16_t uwSpeed, uint8_t ucIndex, Motion_Profile eProfile );
void SetETS_Position( uint32_t uiPos, uint8_t ucIndex, Motion_Profile eProfile );

uint16_t GetETS_Speed( uint8_t ucIndex, Motion_Profile eProfile );
uint32_t GetETS_Position( uint8_t ucIndex, Motion_Profile eProfile );


void SetETS_AcceptanceBypassFlag( uint8_t bBypass );//ONLY FOR ACCEPTANCE TEST
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
#endif
