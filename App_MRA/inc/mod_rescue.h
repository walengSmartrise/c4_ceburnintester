/******************************************************************************
 *
 * @file     mod.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef MOD_RESCUE_H
#define MOD_RESCUE_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <seggerDebug.h>
#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
#include "sru_a.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_RESCUE_1MS            (250U)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

extern struct st_module gstMod_Rescue;

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/* Auto Rescue */
uint8_t Rescue_GetActiveFlag();
uint8_t Rescue_GetManualTractionRescue();
uint8_t Rescue_GetAutoTractionRescue();
void Auto_Rescue( uint8_t bInitMode );
uint8_t Rescue_GetRecTrvDirCommand_HPV();
uint8_t Rescue_GetRecTrvDirCommand_KEB();

uint8_t Rescue_NeedToGetRecTrvDir_HPV(void);
uint8_t Rescue_NeedToGetRecTrvDir_KEB(void);

#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
