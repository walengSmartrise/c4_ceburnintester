/******************************************************************************
 *
 * @file     fault_app.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef _FAULT_APP_H_
#define _FAULT_APP_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
// FaultApp
void ManuallyClearLocalFault(void);
enum en_fault_nodes FaultApp_GetNode( void );
en_faultentry_oos GetFault_OOSType( en_faults eFault );
en_faultentry_con GetFault_ConstructionType( en_faults eFault );
en_faultentry_cleartype GetFault_ClearType( en_faults eFault );
en_faultentry_priority GetFault_Priority( en_faults eFault );

void SetLocalActiveFault( st_fault_data *pstActiveFault );
void GetBoardActiveFault(st_fault_data *pstActiveFault, enum en_fault_nodes eFaultNode );

uint8_t GetFault_ByNumber( en_faults eFaultNum );
uint8_t GetFault_ByRange( en_faults eFirstFaultNum, en_faults eLastFaultNum );

en_faults GetFault_ByNode(enum en_fault_nodes eFaultNode);

void UnloadDatagram_ActiveFaults_MR( st_fault_data *pastActiveFaults );
void LoadDatagram_ActiveFaults_MR( st_fault_data *pastActiveFaults );

// Mod_Fault
void SetFault( en_faults eFaultNum );

void SetCPLDFault( en_faults eFaultNum );

void UpdateActiveFaults( void );
void UpdateLatchingFaults( void );

void SetActiveFault_CPLD( st_fault_data *pstActiveFault );

#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
