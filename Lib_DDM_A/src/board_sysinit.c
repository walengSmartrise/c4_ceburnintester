#include "board.h"
/* The System initialization code is called prior to the application and
   initializes the board for run-time operation. Board initialization
   includes clock setup and default pin muxing configuration. */

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

#define IOCON_D_INPUT      (IOCON_FUNC0 | IOCON_HYS_EN)
#define IOCON_D_OUTPUT     (IOCON_FUNC0 | IOCON_HYS_EN)
#define IOCON_D_LED        (IOCON_FUNC0 | IOCON_MODE_PULLUP | IOCON_HYS_EN)
#define IOCON_D_MCU_ID_PIN (IOCON_FUNC0 | IOCON_MODE_PULLUP)
#define IOCON_D_STARTUP_INPUT (IOCON_FUNC0)
#define IOCON_D_SSP0          (IOCON_FUNC5)


#define IOCON_A_INPUT  (IOCON_FUNC0 | IOCON_FILT_DIS | IOCON_DIGMODE_EN)
#define IOCON_A_OUTPUT (IOCON_FUNC0 | IOCON_FILT_DIS | IOCON_DIGMODE_EN)
#define IOCON_A_LED    (IOCON_FUNC0 | IOCON_MODE_PULLUP | IOCON_FILT_DIS | IOCON_DIGMODE_EN)

#define IOCON_W_INPUT (IOCON_FUNC0 | IOCON_DIGMODE_EN | IOCON_HYS_EN)
#define IOCON_W_SSP1  (IOCON_FUNC2 | IOCON_DIGMODE_EN | IOCON_HYS_EN)

#define IOCON_U_INPUT (IOCON_FUNC0 )

#define IOCON_I_INPUT (IOCON_FUNC0 )

/* Pin muxing configuration */
static const PINMUX_GRP_T pinmuxing[] =
{
      //Inputs
    {2, 2 ,   (IOCON_D_STARTUP_INPUT)                                   },// PortAndPin_Input_WDEnable
    {2, 12,   (IOCON_D_STARTUP_INPUT)                                   },// PortAndPin_Input_MR_Select

    {0, 10,   (IOCON_D_MCU_ID_PIN)                                   },// PortAndPin_InputsID
    {0, 11,   (IOCON_D_MCU_ID_PIN)                                   },// PortAndPin_InputsID
    {0, 17,   (IOCON_D_MCU_ID_PIN)                                   },// PortAndPin_InputsID
    {0, 18,   (IOCON_D_MCU_ID_PIN)                                   },// PortAndPin_InputsID

    {1, 0,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {1, 1,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {1, 4,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {1, 8,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {1, 9,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {1,10,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {1,14,    (IOCON_W_INPUT)                                   },// PortAndPin_DIPs
    {1,15,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs

    {4, 13,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {2, 0,     (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {2, 1,     (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {4, 12,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {4, 11,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {2,7,      (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {4, 10,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
    {2,8,      (IOCON_D_INPUT)                                   },// PortAndPin_DIPs

    {4, 0,    (IOCON_D_INPUT)                                   },// enSRU_Input_501
    {1,22,    (IOCON_D_INPUT)                                   },// enSRU_Input_502
    {0,14,    (IOCON_D_INPUT)                                   },// enSRU_Input_503
    {1,19,    (IOCON_D_INPUT)                                   },// enSRU_Input_504
    {1,18,    (IOCON_D_INPUT)                                   },// enSRU_Input_505
    {3,23,    (IOCON_D_INPUT)                                   },// enSRU_Input_506
    {0,30,    (IOCON_U_INPUT)                                   },// enSRU_Input_507
    {0,29 ,   (IOCON_U_INPUT)                                   },// enSRU_Input_508

    {3,24,    (IOCON_D_INPUT)                                   },// enSRU_Input_509
    {3,25,    (IOCON_D_INPUT)                                   },// enSRU_Input_510
    {3,26,    (IOCON_D_INPUT)                                   },// enSRU_Input_511
    {0,31,    (IOCON_U_INPUT)                                   },// enSRU_Input_512
    {0,27,    (IOCON_I_INPUT)                                   },// enSRU_Input_513
    {0,28,    (IOCON_I_INPUT)                                   },// enSRU_Input_514
    {1,30,    (IOCON_A_INPUT)                                   },// enSRU_Input_515
    {2,11 ,   (IOCON_D_INPUT)                                   },// enSRU_Input_516

    {1,26 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_CRGB
    {4,2 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_CSFP
    {1,27 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_CRGP

    {4,4 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_SAF1
    {2,13 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_SAF2
    {4,3 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_SAF3
    {1,29 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_SAF4

    {1,25 ,   (IOCON_D_INPUT)                                   },// ePREFLIGHT_INPUT_1
    {4,1 ,   (IOCON_D_INPUT)                                   },// ePREFLIGHT_INPUT_2
    {5,4 ,   (IOCON_D_INPUT)                                   },// ePREFLIGHT_INPUT_3
    {0,3 ,   (IOCON_D_INPUT)                                   },// ePREFLIGHT_INPUT_4

    {2,6 ,   (IOCON_D_INPUT)                                   },// eEXT_Input_CPLD_AUX

    //Outputs
    {3,2 ,   (IOCON_D_LED)                                   },// enSRU_LED_Heartbeat
    {3,1 ,   (IOCON_D_LED)                                   },// enSRU_LED_Fault
    {3,0 ,   (IOCON_D_LED)                                   },// enSRU_LED_Alarm

    // COM
    {0,15 ,   ((IOCON_FUNC1 | IOCON_HYS_EN                     )) },// U1_TXD
    {0,16 ,   ((IOCON_FUNC1 | IOCON_HYS_EN                     )) },// U1_RXD
    {0,22 ,   ((IOCON_FUNC1 | IOCON_HYS_EN | IOCON_MODE_PULLUP )) },// U1_RTS

    {4,28 ,   (IOCON_FUNC2 | IOCON_HYS_EN                     ) },// U3_TXD
    {4,29 ,   (IOCON_FUNC2 | IOCON_HYS_EN                     ) },// U3_RXD

    {0,1 ,   ((IOCON_FUNC1 | IOCON_HYS_EN | IOCON_MODE_PULLUP )) },// CAN_TD1
    {0,0 ,   ((IOCON_FUNC1 | IOCON_HYS_EN | IOCON_MODE_PULLUP )) },// CAN_RD1

    {0,5 ,   ((IOCON_FUNC2 | IOCON_HYS_EN | IOCON_MODE_PULLUP )) },// CAN_TD2
    {0,4 ,   ((IOCON_FUNC2 | IOCON_HYS_EN | IOCON_MODE_PULLUP )) },// CAN_RD2

    {1,24 ,   (IOCON_FUNC5)                                   },// SSP0_MOSI
    {1,23 ,   (IOCON_FUNC5)                                   },// SSP0_MISO
    {1,20 ,   (IOCON_FUNC5)                                   },// SSP0_SCK
    {1,21 ,   (IOCON_FUNC3)                                   },// SSP0_SSEL

    {0,9 ,   (IOCON_FUNC2 | IOCON_DIGMODE_EN)                },// SSP1_MOSI cpld
    {0,8 ,   (IOCON_FUNC2 | IOCON_DIGMODE_EN)                },// SSP1_MISO
    {0,7 ,   (IOCON_FUNC2 | IOCON_DIGMODE_EN)                },// SSP1_SCK
    {0,6 ,   (IOCON_FUNC2)                                   },// SSP1_SSEL

#if 0
    {0, 0 ,  (IOCON_FUNC1 | IOCON_HYS_EN | IOCON_MODE_PULLUP ) }, // CAN1_RD
    {0, 1 ,  (IOCON_FUNC1 | IOCON_HYS_EN | IOCON_MODE_PULLUP ) }, // CAN1_TD
    {2, 7 ,  (IOCON_FUNC1 | IOCON_HYS_EN | IOCON_MODE_PULLUP ) }, // CAN2_RD
    {2, 8 ,  (IOCON_FUNC1 | IOCON_HYS_EN | IOCON_MODE_PULLUP ) }, // CAN2_TD
    {0, 15,  (IOCON_FUNC1 | IOCON_HYS_EN                     ) }, // U1_TXD
    {0, 16,  (IOCON_FUNC1 | IOCON_HYS_EN                     ) }, // U1_RXD
    {0, 6 ,  (IOCON_FUNC4 | IOCON_HYS_EN | IOCON_MODE_PULLUP ) }, // U1_RTS
    {4, 28,  (IOCON_FUNC2 | IOCON_HYS_EN                     ) }, // U3_TXD
    {4, 29,  (IOCON_FUNC2 | IOCON_HYS_EN                     ) }, // U3_RXD
    {2, 0 ,  (IOCON_D_INPUT                                  ) }, // Input 501
    {1, 18,  (IOCON_D_INPUT                                  ) }, // Input 502
    {1, 4 ,  (IOCON_D_INPUT                                  ) }, // Input 504
    {1, 8 ,  (IOCON_D_INPUT                                  ) }, // Input 507
    {2, 1 ,  (IOCON_D_INPUT                                  ) }, // Input 507
    {1, 26,  (IOCON_D_OUTPUT                                 ) }, // Output 602
    {1, 25,  (IOCON_D_OUTPUT                                 ) }, // Output 603
    {1, 24,  (IOCON_D_OUTPUT                                 ) }, // Output 604
    {1, 23,  (IOCON_D_OUTPUT                                 ) }, // Output 605
    {1, 22,  (IOCON_D_OUTPUT                                 ) }, // Output 606
    {1, 20,  (IOCON_D_OUTPUT                                 ) }, // Output 608
    {0, 22,  (IOCON_D_LED                                    ) }, // Output 601_617 LED
    {1, 19,  (IOCON_D_LED                                    ) }, // Heartbeat LED
    {1, 29,  (IOCON_D_LED                                    ) }, // Alarm LED
    {1, 28,  (IOCON_D_LED                                    ) }, // Fault LED
    {1, 15,  (IOCON_D_INPUT                                  ) }, // DIP A0
    {1, 10,  (IOCON_D_INPUT                                  ) }, // DIP A2
    {1, 9 ,  (IOCON_D_INPUT                                  ) }, // DIP A3
    {1, 1 ,  (IOCON_D_INPUT                                  ) }, // DIP A4
    {1, 0 ,  (IOCON_D_INPUT                                  ) }, // DIP A5
    {0, 2 ,  (IOCON_D_INPUT                                  ) }, // DIP A6
    {0, 3 ,  (IOCON_D_INPUT                                  ) }, // DIP A7
    {0, 10,  (IOCON_D_MCU_ID_PIN                             ) }, // MCU ID Pin 0
    {0, 11,  (IOCON_D_MCU_ID_PIN                             ) }, // MCU ID Pin 1
    {0, 17,  (IOCON_D_MCU_ID_PIN                             ) }, // MCU ID Pin 2
    {0, 18,  (IOCON_D_MCU_ID_PIN                             ) }, // MCU ID Pin 3
    {1, 30,  (IOCON_A_OUTPUT                                 ) }, // Output 601
    {1, 31,  (IOCON_A_OUTPUT                                 ) }, // Output 607
    {0, 26,  (IOCON_A_INPUT                                  ) }, // Input MCUB 617 LED EN
    {0, 9 ,  (IOCON_W_INPUT                                  ) }, // Input 503
    {0, 8 ,  (IOCON_W_INPUT                                  ) }, // Input 505
    {0, 7 ,  (IOCON_W_INPUT                                  ) }, // Input 506
    {1, 14,  (IOCON_W_INPUT                                  ) }  // DIP A1
#endif
};

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
/* Sets up system pin muxing */
void Board_SetupMuxing(void)
{
   // Reset the IOCON register
   Chip_SYSCTL_PeriphReset(SYSCTL_RESET_IOCON);

   /* Setup system level pin muxing */
   Chip_IOCON_SetPinMuxing(LPC_IOCON, pinmuxing, sizeof(pinmuxing) / sizeof(PINMUX_GRP_T));
}

/* Setup system clocking */
void Board_SetupClocking(void)
{
   // Use the internal system clock
   //Chip_SetupIrcClocking();
   Chip_SetupXtalClocking();
}


/* Set up and initialize hardware prior to call to main */
void Board_SystemInit(void)
{
   Board_SetupMuxing();
   Board_SetupClocking();
}

