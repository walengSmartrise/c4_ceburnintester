/******************************************************************************
 *
 * @file     mcu_a.c
 * @brief    Support for MCU A on SRU Base board
 * @version  V1.00
 * @date     21, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_a.h"

#include "sys.h"
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static en_sru_deployments eSRU_Deployment = enSRU_DEPLOYMENT__Invalid0;

static const PortAndPin PortAndPin_Input_WDEnable = {2,  2};

static const PortAndPin PortAndPin_Input_MR_Select = {2, 12};

static const PortAndPin PortAndPin_InputsID[SRU_NUM_ID_PINS] =
{
#if 0
      {0,  10 },
      {0,  11 },
      {0,  17 },
      {0,  18 },
#else
      {0,  18 },
      {0,  17 },
      {0,  11 },
      {0,  10 },
#endif
};

static const PortAndPin PortAndPin_DIPs[SRU_NUM_DIP_SWITCHES] =
{
   {1,  0 },
   {1,  1 },
   {1,  4 },
   {1,  8 },
   {1,  9 },
   {1, 10 },
   {1, 14 },
   {1, 15 },

   // B dips
   {4, 13 },
   {2, 0 },
   {2, 1 },
   {4, 12 },
   {4, 11 },
   {2, 7 },
   {4, 10 },
   {2, 8 },
};

static const PortAndPin PortAndPin_Inputs[ SRU_NUM_INPUTS ] =
{
#if 1
      {4,  0},  // enSRU_Input_501
      {1, 22}, // enSRU_Input_502
      {0, 14},  // enSRU_Input_503
      {1, 19},  // enSRU_Input_504
      {1, 18},  // enSRU_Input_505
      {3, 23},  // enSRU_Input_506
      {0, 30},  // enSRU_Input_507
      {0, 29},  // enSRU_Input_508

      {3, 24},  // enSRU_Input_509
      {3, 25}, // enSRU_Input_510
      {3, 26},  // enSRU_Input_511
      {0, 31},  // enSRU_Input_512
      {0, 27},  // enSRU_Input_513
      {0, 28},  // enSRU_Input_514
      {1, 30},  // enSRU_Input_515
      {2, 11},  // enSRU_Input_516
#else
   {2, 11},  // enSRU_Input_516
   {1, 30},  // enSRU_Input_515
   {0, 28},  // enSRU_Input_514
   {0, 27},  // enSRU_Input_513
   {0, 31},  // enSRU_Input_512
   {3, 26},  // enSRU_Input_511
   {3, 25}, // enSRU_Input_510
   {3, 24},  // enSRU_Input_509

{0, 29},  // enSRU_Input_508
{0, 30},  // enSRU_Input_507
{3, 23},  // enSRU_Input_506
{1, 18},  // enSRU_Input_505
{1, 19},  // enSRU_Input_504
{0, 14},  // enSRU_Input_503
{1, 22}, // enSRU_Input_502
{4,  0},  // enSRU_Input_501
#endif
};

static const PortAndPin_Inv PortAndPinInv_ExtInputs[ EXT_NUM_INPUTS ] =
{
      {4,4,0},//eEXT_Input_SAF1,
      {2,13,0},//eEXT_Input_SAF2,
      {4,3,0},//eEXT_Input_SAF3,
      {1,29,0},//eEXT_Input_SAF4,
};
static const PortAndPin PortAndPin_PreflightInputs[ NUM_PREFLIGHT_INPUTS ] =
{
      {1, 25},//ePREFLIGHT_INPUT_1,
      {4,  1},//ePREFLIGHT_INPUT_2,
      {5,  4},//ePREFLIGHT_INPUT_3,
      {0,  3},//ePREFLIGHT_INPUT_4,
};
//------------------------------------------------------------
static const PortAndPin PortAndPin_Preflight_Output = {2, 6};

static const PortAndPin PortAndPin_LEDs[ SRU_NUM_LEDS ] =
{
  {3, 2},  // enSRU_LED_Heartbeat
  {3, 1},  // enSRU_LED_Fault
  {3, 0}   // enSRU_LED_Alarm

};

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Returns 4 bit hardcoded MCU ID
-----------------------------------------------------------------------------*/
uint8_t SRU_Read_MCU_ID()
{
  uint8_t ucValue = 0;
  for(uint8_t i = 0; i < SRU_NUM_ID_PINS; i++)
  {
     uint8_t uiPort = PortAndPin_InputsID[i].port;
     uint8_t uiPin  = PortAndPin_InputsID[i].pin;
     uint8_t bValue = Chip_GPIO_GetPinState(LPC_GPIO, uiPort, uiPin);
     ucValue |= (bValue << i);
  }
  return ucValue;
}
/*-----------------------------------------------------------------------------
if pin is high, watchdog is enabled
-----------------------------------------------------------------------------*/
uint8_t SRU_Read_WatchDogEnable()
{
  uint8_t bValue = 0;

   uint8_t uiPort = PortAndPin_Input_WDEnable.port;
   uint8_t uiPin  = PortAndPin_Input_WDEnable.pin;

   bValue = Chip_GPIO_GetPinState(LPC_GPIO, uiPort, uiPin);

  return bValue;
}
/*-----------------------------------------------------------------------------
Returns 1 if MR, 0 if CT/COP
-----------------------------------------------------------------------------*/
uint8_t SRU_Read_BoardTypeIndicator()
{
  uint8_t bValue = 0;

   uint8_t uiPort = PortAndPin_Input_MR_Select.port;
   uint8_t uiPin  = PortAndPin_Input_MR_Select.pin;

   bValue = Chip_GPIO_GetPinState(LPC_GPIO, uiPort, uiPin);

  return bValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_DIP_Switch( enum en_sru_dip_switches enSRU_DIP_Switch )
{
   uint8_t bValue = 0;

   if( enSRU_DIP_Switch < SRU_NUM_DIP_SWITCHES )
   {
      uint8_t ucPin = PortAndPin_DIPs[enSRU_DIP_Switch].pin;
      uint8_t ucPort = PortAndPin_DIPs[enSRU_DIP_Switch].port;
      bValue = !Chip_GPIO_GetPinState(LPC_GPIO,ucPort,ucPin);
   }

   return bValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_DIP_Bank( enum en_sru_dip_banks enSRU_DIP_Bank )
{
   uint8_t ucValue = 0;

   if ( enSRU_DIP_Bank == enSRU_DIP_BANK_A )
   {
      for(uint8_t i = 0; i <= enSRU_DIP_A8; i++)
      {
         uint8_t ucPin = PortAndPin_DIPs[i].pin;
         uint8_t ucPort = PortAndPin_DIPs[i].port;
         uint8_t bValue = !Chip_GPIO_GetPinState(LPC_GPIO,ucPort,ucPin);
         ucValue |= (bValue << i);
      }
   }
   else if( enSRU_DIP_Bank == enSRU_DIP_BANK_B )
   {
      for(uint8_t i = enSRU_DIP_B1; i <= enSRU_DIP_B8; i++)
      {
         uint8_t ucPin = PortAndPin_DIPs[i].pin;
         uint8_t ucPort = PortAndPin_DIPs[i].port;
         uint8_t bValue = !Chip_GPIO_GetPinState(LPC_GPIO,ucPort,ucPin);
         ucValue |= (bValue << i);
      }
   }

   return ucValue;
}
/*-----------------------------------------------------------------------------
returns state of general gpio status
 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_Input( en_sru_inputs enSRU_Input )
{
   uint8_t bValue = 0;

   if ( enSRU_Input < SRU_NUM_INPUTS )
   {
      uint8_t uiPort = PortAndPin_Inputs[ enSRU_Input ].port;
      uint8_t uiPin  = PortAndPin_Inputs[ enSRU_Input ].pin;

      // Invert the value returned by Sys_MCU_Pin_In() because when an input is
      // powered, the port pin is low and vice versa.
      bValue = !Chip_GPIO_GetPinState(LPC_GPIO, uiPort, uiPin);
   }

   return bValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_ExtInput( en_ext_inputs enEXT_Input )
{
   uint8_t bValue = 0;

   if ( enEXT_Input < EXT_NUM_INPUTS )
   {
      uint8_t uiPort = PortAndPinInv_ExtInputs[ enEXT_Input ].port;
      uint8_t uiPin  = PortAndPinInv_ExtInputs[ enEXT_Input ].pin;
      uint8_t bInv = PortAndPinInv_ExtInputs[ enEXT_Input ].invert;

      // Invert the value returned by Sys_MCU_Pin_In() because when an input is
      // powered, the port pin is low and vice versa.
      bValue = !Chip_GPIO_GetPinState(LPC_GPIO, uiPort, uiPin) ^ bInv;
   }

   return bValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_PreflightInput( en_preflight_input ePreflightInput )
{
   uint8_t bValue = 0;

   if ( ePreflightInput < NUM_PREFLIGHT_INPUTS )
   {
      uint8_t uiPort = PortAndPin_PreflightInputs[ ePreflightInput ].port;
      uint8_t uiPin  = PortAndPin_PreflightInputs[ ePreflightInput ].pin;

      // Invert the value returned by Sys_MCU_Pin_In() because when an input is
      // powered, the port pin is low and vice versa.
      bValue = Chip_GPIO_GetPinState(LPC_GPIO, uiPort, uiPin);
   }

   return bValue;
}

/*-----------------------------------------------------------------------------
   LED write
 -----------------------------------------------------------------------------*/
void SRU_Write_LED( en_sru_leds enSRU_LED, uint8_t bValue )
{
   if ( enSRU_LED < SRU_NUM_LEDS )
   {
      uint8_t uiPort = PortAndPin_LEDs[ enSRU_LED ].port;
      uint8_t uiPin  = PortAndPin_LEDs[ enSRU_LED ].pin;

      // The port pins that control the LEDs must be pulled low to turn on the
      // LED so we need to invert bValue to get the desired behavior.
      Chip_GPIO_SetPinState(LPC_GPIO , uiPort, uiPin, !bValue);
   }
}
/*-----------------------------------------------------------------------------
   Write 1 to initiate NTS stop. Output will be read on the CT CPLD,
   the signal will be communicated to the MR CPLD and the output to the drive will set.
 -----------------------------------------------------------------------------*/
void SRU_Write_Preflight_Output( uint8_t bValue )
{
   uint8_t uiPort = PortAndPin_Preflight_Output.port;
   uint8_t uiPin  = PortAndPin_Preflight_Output.pin;
   Chip_GPIO_SetPinState(LPC_GPIO, uiPort, uiPin, bValue);
}

/*-----------------------------------------------------------------------------
   Set bit to indicate pin direction (0 = input, 1 = output).
 -----------------------------------------------------------------------------*/
static void ConfigureGPIO_DirectionOutputs( void )
{
    // Setup output pins
   Chip_GPIO_SetDir(LPC_GPIO, PortAndPin_Preflight_Output.port, PortAndPin_Preflight_Output.pin,true);

    // Setup led outputs
    for (uint32_t i = 0; i < SRU_NUM_LEDS; i++)
    {
        Chip_GPIO_SetDir(LPC_GPIO, PortAndPin_LEDs[i].port, PortAndPin_LEDs[i].pin,true);
    }

    // Turn off all outputs.
    SRU_Write_Preflight_Output(0);
    for ( uint32_t i = 0; i < SRU_NUM_LEDS; ++i )
    {
       SRU_Write_LED( i, 0 );
    }

}
static void ConfigureGPIO_Direction( void )
{
    ConfigureGPIO_DirectionOutputs();
}

/*-----------------------------------------------------------------------------
   Verify the 4 ID pins connected to the MCU identify this MCU as part of the
   SRU Base board (MCU A). If not, loop forever flashing all LEDs.
 -----------------------------------------------------------------------------*/
#define TOGGLE_LED_TIMEOUT (0xFFFFF)
void VerifyBoardID( void )
{
   uint8_t ucMCU_ID = SRU_Read_MCU_ID();
   if ( ( ucMCU_ID != enMCU_COP_Base )
     && ( ucMCU_ID != enMCU_CT_Base ) )
   {
      while ( 1 )
      {
         static uint32_t uiCounter, bState;

         if( uiCounter >= TOGGLE_LED_TIMEOUT )
         {
            uiCounter = 0;

            bState = ( bState )? 0: 1;

            SRU_Write_LED( enSRU_LED_Heartbeat, bState );
            SRU_Write_LED( enSRU_LED_Alarm,     bState );
            SRU_Write_LED( enSRU_LED_Fault,     bState );
         }
         else
         {
            uiCounter++;
         }
      }
   }
}

en_sru_deployments GetSRU_Deployment( void )
{
     return eSRU_Deployment;
}

void SetSRU_Deployment( void )
{
   uint8_t ucID = SRU_Read_MCU_ID();
   eSRU_Deployment = enSRU_DEPLOYMENT__Invalid0;
   if(ucID == enMCU_CT_Base)
   {
      eSRU_Deployment = enSRU_DEPLOYMENT__CT;
   }
   else if(ucID == enMCU_COP_Base)
   {
      eSRU_Deployment = enSRU_DEPLOYMENT__COP;
   }
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void MCU_A_Init( void )
{
   ConfigureGPIO_Direction();

   VerifyBoardID();

   // Set the global deployment variable. This is used to set up some
   // of the other board specific features.
   SetSRU_Deployment();
   CAN_Init();
   UART_AB_Init();
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
