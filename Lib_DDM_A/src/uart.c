#include "sru_a.h"


#define MCUA_UART      (LPC_UART3)
#define MCUA_UART_BAUD (115200)
#define MCUA_UART_IRQ  (UART3_IRQn)


/* Transmit and receive ring buffers */
static RINGBUFF_T txring;
static RINGBUFF_T rxring;

/* Ring buffer size */
#define UART_RB_SIZE (256)//(96)

/* Transmit and receive buffers */
static uint8_t rxbuff[UART_RB_SIZE];
static uint8_t txbuff[UART_RB_SIZE];

void UART_AB_Init( void )
{
/* Setup UART for 115.2K8N1 */
   Chip_UART_Init      (MCUA_UART);
//   Chip_UART_SetBaud   (MCUA_UART, MCUA_UART_BAUD);
   Chip_UART_SetBaudFDR(MCUA_UART, MCUA_UART_BAUD);
   Chip_UART_ConfigData(MCUA_UART, (UART_LCR_WLEN8   | UART_LCR_SBS_1BIT));
   Chip_UART_SetupFIFOS(MCUA_UART, (UART_FCR_FIFO_EN | UART_FCR_TRG_LEV3 ));
   Chip_UART_TXEnable  (MCUA_UART);

   RingBuffer_Init(&rxring, rxbuff, 1, UART_RB_SIZE);
   RingBuffer_Init(&txring, txbuff, 1, UART_RB_SIZE);

   /* Enable receive data and line status interrupt */
   Chip_UART_IntEnable(MCUA_UART, (UART_IER_THREINT| UART_IER_RBRINT));

   /* preemption = 1, sub-priority = 1 */
   NVIC_EnableIRQ  (MCUA_UART_IRQ);
}

// Only loads if there is room in buffer
uint32_t UART_AB_SendRB(const void *data, int bytes)
{
   uint32_t uiReturn = 0;
   if(RingBuffer_GetFree(&txring) >= bytes)
   {
      uiReturn = Chip_UART_SendRB(MCUA_UART, &txring, data, bytes);
   }

   return uiReturn;
}

uint8_t UART_AB_ReadByte(uint8_t *pucByte)
{
   if(!RingBuffer_IsEmpty(&rxring))
   {
      RingBuffer_Pop(&rxring, pucByte);
      return 1;
   }
   return 0;
}

void UART3_IRQHandler(void)
{
   // handles tx - same as default handler
   uint8_t bTxEnabled = ( Chip_UART_GetIntsEnabled(MCUA_UART) & UART_IER_THREINT ) != 0;
   uint8_t bTxBufferNotEmpty = ( Chip_UART_ReadLineStatus(MCUA_UART) & UART_LSR_THRE ) != 0;
   if ( bTxEnabled
     && bTxBufferNotEmpty )
   {
      Chip_UART_TXIntHandlerRB(MCUA_UART, &txring);
   }

   if (RingBuffer_IsEmpty(&txring)) {
      Chip_UART_IntDisable(MCUA_UART, UART_IIR_INTID_THRE);
   }

   Chip_UART_RXIntHandlerRB(MCUA_UART, &rxring);
}

