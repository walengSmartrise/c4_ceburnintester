/******************************************************************************
 *
 * @file     sru_a.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef SRU_A_H
#define SRU_A_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "board.h"
#include "sru.h"
#include <stdint.h>
#include "shared_data.h"
#include "fpga_api.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
enum en_sru_id_pins
{
    enSRU_ID_PIN1,
    enSRU_ID_PIN2,
    enSRU_ID_PIN3,
    enSRU_ID_PIN4,

    SRU_NUM_ID_PINS
};
typedef enum
{
    enSRU_Input_501,
    enSRU_Input_502,
    enSRU_Input_503,
    enSRU_Input_504,
    enSRU_Input_505,
    enSRU_Input_506,
    enSRU_Input_507,
    enSRU_Input_508,
    enSRU_Input_509,
    enSRU_Input_510,
    enSRU_Input_511,
    enSRU_Input_512,
    enSRU_Input_513,
    enSRU_Input_514,
    enSRU_Input_515,
    enSRU_Input_516,

    SRU_NUM_INPUTS
} en_sru_inputs;

typedef enum
{
   eEXT_Input_SAF1,
   eEXT_Input_SAF2,
   eEXT_Input_SAF3,
   eEXT_Input_SAF4,

   EXT_NUM_INPUTS
} en_ext_inputs;

typedef enum
{
    ePREFLIGHT_INPUT_1,
    ePREFLIGHT_INPUT_2,
    ePREFLIGHT_INPUT_3,
    ePREFLIGHT_INPUT_4,

    NUM_PREFLIGHT_INPUTS,
} en_preflight_input;

typedef enum
{
    eFRAM_Output_SSEL,

    NUM_FRAM_OUTPUTS
} en_fram_outputs;
typedef enum
{
    enSRU_LED_Heartbeat,
    enSRU_LED_Fault,
    enSRU_LED_Alarm,
    SRU_NUM_LEDS
} en_sru_leds;

enum en_sru_dip_switches
{
    enSRU_DIP_A1,
    enSRU_DIP_A2,
    enSRU_DIP_A3,
    enSRU_DIP_A4,
    enSRU_DIP_A5,
    enSRU_DIP_A6,
    enSRU_DIP_A7,
    enSRU_DIP_A8,

    enSRU_DIP_B1,
    enSRU_DIP_B2,
    enSRU_DIP_B3,
    enSRU_DIP_B4,
    enSRU_DIP_B5,
    enSRU_DIP_B6,
    enSRU_DIP_B7,
    enSRU_DIP_B8,
    SRU_NUM_DIP_SWITCHES
};

enum en_sru_dip_banks
{
    enSRU_DIP_BANK_A,
    enSRU_DIP_BANK_B,

    enSRU_NUM_DIP_BANKS,
};

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

void MCU_A_Init( void );
uint8_t SRU_Read_MCU_ID();

uint8_t SRU_Read_WatchDogEnable();

uint8_t SRU_Read_BoardTypeIndicator();
uint8_t SRU_Read_Input( en_sru_inputs enSRU_Input );
uint8_t SRU_Read_ExtInput( en_ext_inputs enEXT_Input );

void SRU_Write_Preflight_Output( uint8_t bValue );
void SRU_Write_LED( en_sru_leds enSRU_LED, uint8_t bValue );

uint8_t SRU_Read_DIP_Switch( enum en_sru_dip_switches enSRU_DIP_Switch );

uint8_t SRU_Read_DIP_Bank( enum en_sru_dip_banks enSRU_DIP_Bank );

uint8_t SRU_Read_PreflightInput( en_preflight_input ePreflightInput );

en_sru_deployments GetSRU_Deployment( void );
void SetSRU_Deployment( void );

void CAN_Init( void );
Status CAN1_UnloadFromRB(CAN_MSG_T *pstRxMsg);
Status CAN1_LoadToRB(CAN_MSG_T *pstRxMsg);
Status CAN2_UnloadFromRB(CAN_MSG_T *pstRxMsg);
Status CAN2_LoadToRB(CAN_MSG_T *pstRxMsg);
Status CAN1_FillHardwareBuffer();
Status CAN2_FillHardwareBuffer();

void UART_AB_Init( void );
uint8_t UART_AB_ReadByte(uint8_t *pucByte);
uint32_t UART_AB_SendRB(const void *data, int bytes);
#endif


/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
