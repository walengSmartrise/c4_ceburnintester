/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     26, March 2016
 *
 * @note    Load local datagrams and selects datagrams for transmit
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sru_a.h"
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_SData =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

struct st_sdata_control gstSData_LocalData;

char *pasVersion;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define DG_TX_CYCLE_LIMIT  (3)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static Sys_Nodes eSysNodeID;// network id
static uint8_t ucNumberOfDatagrams;// number of datagrams generated locally
static CAN_MSG_T gstCAN_MSG_Tx;

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Gets the last segment of the software version string for transmit on the group net
 -----------------------------------------------------------------------------*/
static void UpdateVersionString(void)
{
   uint8_t ucSize = sizeof(SOFTWARE_RELEASE_VERSION);
   uint8_t ucFirstIndex = 0;
   for(int8_t i = ucSize-1; i >= 0; i--)
   {
      char *ps = SOFTWARE_RELEASE_VERSION+i;
      if(*ps == '.')
      {
         ucFirstIndex = i+1;
         break;
      }
   }

   pasVersion = SOFTWARE_RELEASE_VERSION+ucFirstIndex;
}
/*-----------------------------------------------------------------------------
   Creates datagrams produced by this node
 -----------------------------------------------------------------------------*/
void SharedData_Init()
{
   int iError = 1;
   eSysNodeID = GetSystemNodeID();
   switch(eSysNodeID)
   {
      case SYS_NODE__CTA:
        //ucNumberOfDatagrams = NUM_CTA_DATAGRAMS;
        ucNumberOfDatagrams = NUM_MRA_DATAGRAMS; //Needed for param sync
        break;
      case SYS_NODE__COPA:
         ucNumberOfDatagrams = NUM_COPA_DATAGRAMS;
         break;

      default:
         break;

   }
   if(ucNumberOfDatagrams)
   {
      iError = SDATA_CreateDatagrams( &gstSData_LocalData,
                                      ucNumberOfDatagrams
                                     );
   }


   if ( iError )
   {
      while ( 1 )
      {
         ;  // TODO: unable to allocate shared data
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

static void LoadData( void )
{
   switch(GetSRU_Deployment())
   {
   case enSRU_DEPLOYMENT__CT:
      LoadData_CT();
      break;
   case enSRU_DEPLOYMENT__COP:
      LoadData_COP();
      break;
   default:
      break;
   }
}
/*-----------------------------------------------------------------------------
   If destination is B processor of the board, forward it
 -----------------------------------------------------------------------------*/
uint8_t CheckIf_SendOnABNet(uint16_t uwDestinationBitmap)
{
   uint8_t ucDestinationNodeID = GetSystemNodeID() + 1;
   uint16_t uwForwardingBitmap = 1 << ucDestinationNodeID;
   uint8_t bReturn = 0;
   if(uwForwardingBitmap & uwDestinationBitmap)
   {
      bReturn = 1;
   }
   return bReturn;
}
/*-----------------------------------------------------------------------------
   If the datagram has any destination other than the source ID or its own ID,
   then forward it to the Car net
 -----------------------------------------------------------------------------*/
uint8_t CheckIf_SendOnCarNet( uint16_t uwDestinationBitmap )
{
   uint8_t ucSysNodeID = GetSystemNodeID();
   uint16_t uwForwardingBitmap = NODE_ALL_CORE & ~(0x3 << ucSysNodeID);
   uint8_t bReturn = 0;
   if(uwForwardingBitmap & uwDestinationBitmap)
   {
      bReturn = 1;
   }
   return bReturn;
}

/*----------------------------------------------------------------------------
   Encode CAN message ID
 *----------------------------------------------------------------------------*/
uint32_t CarNet_EncodeCANMessageID(   Datagram_ID eDatagram,
                                      enum en_car_net_nodes eSource,
                                      uint16_t uwDest )
{
   uint32_t uiID = CAN_EXTEND_ID_USAGE
                 | ( (eDatagram & 0x00FF) << 21 )
                 | ( (SDATA_NET__CAR & 0x0007) << 18 )
                 | ( (eSource & 0x000F) << 14 )
                 | ( (uwDest & 0x3FFF) );
   return uiID;
}

/*-----------------------------------------------------------------------------
 Loads local datagrams for transmit over Car Net and/or AB Net
 -----------------------------------------------------------------------------*/
static void Transmit( void )
{
   un_sdata_datagram unDatagram;
   int iError_AB = 0;
   int iError_Car = 0;
   uint8_t ucLocalDatagramID;
   uint8_t ucNetworkID = SDATA_NET__CAR;
   uint16_t uwDatagramToSend_Plus1;
   uint8_t ucTXCycle;
   for( ucTXCycle = 0; ucTXCycle < DG_TX_CYCLE_LIMIT; ucTXCycle++)
   {
      uwDatagramToSend_Plus1 = SDATA_GetNextDatagramToSendIndex_Plus1( &gstSData_LocalData );
      if(uwDatagramToSend_Plus1)
      {
         ucLocalDatagramID = uwDatagramToSend_Plus1 - 1;

         if ( ucLocalDatagramID < ucNumberOfDatagrams )
         {
            SDATA_ReadDatagram( &gstSData_LocalData,
                                 ucLocalDatagramID,
                                &unDatagram
                              );

            Sys_Nodes eLocalNodeID = GetSystemNodeID();
            Datagram_ID eDatagramID = GetNetworkDatagramID_AnyBoard(ucLocalDatagramID);
            uint16_t uwDestinationBitmap = GetDatagramDestinationBitmap(eDatagramID);
            /* DO not transmit the CEDES position packet. position forwarding occurs in position_cta.c */
            if(eDatagramID == DATAGRAM_ID_2)
            {
               SDATA_DirtyBit_Clr(&gstSData_LocalData, ucLocalDatagramID);
            }
            else
            {
               memcpy( gstCAN_MSG_Tx.Data, unDatagram.auc8, sizeof( unDatagram.auc8 ) );
               gstCAN_MSG_Tx.DLC = 8;
               gstCAN_MSG_Tx.Type = 0;
               gstCAN_MSG_Tx.ID = 1 << 30;
               gstCAN_MSG_Tx.ID |= eDatagramID << 21;
               gstCAN_MSG_Tx.ID |= (ucNetworkID & 0x07) << 18;
               gstCAN_MSG_Tx.ID |= (eLocalNodeID & 0x0F) << 14;
               gstCAN_MSG_Tx.ID |= uwDestinationBitmap;

               if( GetOperation4_ParamSync())
               {
                  eDatagramID = GetNetworkDatagramID_MRA( ucLocalDatagramID );
                  if( ( eDatagramID >= DATAGRAM_ID_96 && eDatagramID <= DATAGRAM_ID_149)
                      || (eDatagramID >= DATAGRAM_ID_158 && eDatagramID <= DATAGRAM_ID_160)
                      || (eDatagramID >= DATAGRAM_ID_180 && eDatagramID <= DATAGRAM_ID_185)
                      || (eDatagramID >= DATAGRAM_ID_196 && eDatagramID <= DATAGRAM_ID_197))
                  {
                     gstCAN_MSG_Tx.ID = CarNet_EncodeCANMessageID( eDatagramID, SYS_NODE__MRA, 0x3fff);
                  }
               }

               if(CheckIf_SendOnCarNet(uwDestinationBitmap))
               {
                  iError_Car = !CAN1_LoadToRB(&gstCAN_MSG_Tx);
               }
               if(!iError_Car && CheckIf_SendOnABNet(uwDestinationBitmap))
               {
                     iError_AB = !UART_LoadCANMessage(&gstCAN_MSG_Tx);
               }

               if ( iError_AB | iError_Car )
               {
                  SDATA_DirtyBit_Set( &gstSData_LocalData, ucLocalDatagramID );

                  if ( gstSData_LocalData.uwLastSentIndex )
                  {
                     --gstSData_LocalData.uwLastSentIndex;
                  }
                  else
                  {
                     gstSData_LocalData.uwLastSentIndex = gstSData_LocalData.uiNumDatagrams - 1;
                  }
                  ucTXCycle = DG_TX_CYCLE_LIMIT;
                  break;
               }
            }
         }
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

static void UnloadData( void )
{
   switch(GetSRU_Deployment())
   {
   case enSRU_DEPLOYMENT__CT:
      UnloadData_CT();
      break;
   case enSRU_DEPLOYMENT__COP:
      UnloadData_COP();
      break;
   default:
      break;
   }
}

/*-----------------------------------------------------------------------------
   Set datagram resend rates
 -----------------------------------------------------------------------------*/
static void Initialize_DatagramSendRates()
{
   uint8_t ucOfflineDelay_5ms = Param_ReadValue_8Bit(enPARAM8__OfflineCtrlTimer_5ms);
   if(ucOfflineDelay_5ms < MIN_OFFLINE_DELAY_5MS)
   {
      ucOfflineDelay_5ms = MIN_OFFLINE_DELAY_5MS;
   }

   uint16_t uwHeartbeatInterval_1ms = (ucOfflineDelay_5ms*5)/3;
   gstSData_LocalData.uwHeartbeatInterval_1ms = uwHeartbeatInterval_1ms;

   for( uint8_t i = 0; i < ucNumberOfDatagrams; i++ )
   {
      Datagram_ID eID = GetNetworkDatagramID_AnyBoard( i );
      uint16_t uwLifetime_ms = GetDatagramLifetime_ms( eID );
      if( uwLifetime_ms != IGNORED_DATAGRAM )
      {
         gstSData_LocalData.paiResendRate_1ms[ i ] = uwLifetime_ms/2;
      }
   }

   if( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT )
   {
      // Resend for position handled in mod_position.c
      gstSData_LocalData.paiResendRate_1ms[ DG_CTA__SysPosition ] = DISABLED_DATAGRAM_RESEND_RATE_MS;
   }
   else if( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP )
   {
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   //------------------------------------------------
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_CAR_NET_1MS;

   Initialize_DatagramSendRates();

   UpdateVersionString();

   return 0;
}

/*-----------------------------------------------------------------------------

                  MSB   x
                        x     4 bits for Network ID (global enum)
                        x
                        x
                      -----
                        x
                        x
                        x    5 bits for Source Node ID (static enum to use less bits)
  29-bit CAN ID:        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Destination Node ID (0x1F = Broadcast to all)
                        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Datagram ID (global enum)
                        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Version Number (major)
                        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Version Number (minor)
                        x
                  LSB   x

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
    LoadData();

    Transmit();


    // Polls ring buffer for data and loads into CAN hardware buffer
    CAN1_FillHardwareBuffer();

    UnloadData();
    return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
