/******************************************************************************
 *
 * @brief    
 * @version  V1.00
 * @date     2, May 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "motion.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Safety = { .pfnInit = Init, .pfnRun = Run, };

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
    pstThisModule->uwInitialDelay_1ms = 1000;
    pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_SAFETY_1MS;

    return 0;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t CheckIfOnConstruction( void )
{
    uint8_t result = 0; // Assume false
    if ( GetOperation_ClassOfOp() == CLASSOP__MANUAL )
    {
        if( GetOperation_ManualMode() == MODE_M__CONSTRUCTION )
        {
            result = 1;
        }
    }

    return result;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   if ( CheckIfOnConstruction() )
   {
   }
   else
   {
      switch(GetSystemNodeID())
      {
         case SYS_NODE__CTA:
            Run_ModSafety_CT();
            break;
         case SYS_NODE__COPA:
            Run_ModSafety_COP();
            break;
         default:
            break;
      }

   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
