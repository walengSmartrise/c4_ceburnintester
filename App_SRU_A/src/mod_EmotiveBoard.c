/******************************************************************************
 *
 * @file
 * @brief
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note     Handles RS422 communication with Janus emotive fixtures
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_a.h"
#include "sru.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
#include "motion.h"
#include "contributors.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_EmotiveBoard =
{
   .pfnInit = Init,
   .pfnRun = Run,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define NUM_MESSAGE_BYTES        (11)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
struct st_data_buffer
{
   uint8_t bArrivalF_DN;
   uint8_t bArrivalF_UP;

   uint8_t bArrivalR_DN;
   uint8_t bArrivalR_UP;

   uint8_t bTravel_UP;
   uint8_t bTravel_DN;


   uint8_t bStrobe;
   uint8_t bChime;
   uint8_t bFire;
   uint8_t ucMessageNum;
   uint8_t ucSlot;

   uint8_t ucCharL;
   uint8_t ucCharM;
   uint8_t ucCharR;

   uint8_t ucFireFloor;
   uint8_t bFireDoor;

   uint8_t ucCurrentFloor;
};

enum en_ce_messages
{
   CE_MSG__NONE,
   CE_MSG__FIRE_MAIN,//Enabled by DIP5, MF
   CE_MSG__FIRE_ALT,//Enabled by DIP5, AF
   CE_MSG__NUDGE,//Enabled by DIP5, DN
   CE_MSG__INDP_SRV,//Enabled by DIP5, NI
   CE_MSG__OVERLOAD,//Enabled by DIP5, LO
   CE_MSG__EPOWER,//Enabled by DIP5, PE
   CE_MSG__INSP,//Enabled by DIP5, SI
   CE_MSG__SEISMIC,//Enabled by DIP5, SS
   CE_MSG__OOS,//Enabled by DIP5, OS
   CE_MSG__FF2,//Enabled by DIP5, F2

   NUM_CE_MSG
};
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static struct st_data_buffer stDataBuffer;
/*Message levels From CE documentation*/
#if 0
static uint8_t gaucMsgLevel[NUM_CE_MSG] =
{
      0,
      3,
      2,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
      1,
};

/* Message strings from CE documentation */
static const char * gpas_MsgStrings[NUM_CE_MSG] =
{
      "[[[",
      "FM",
      "FA",
      "ND",
      "IN",
      "OL",
      "EP",
      "IS",
      "SS",
      "OS",
      "F2",
};
#endif
static uint8_t aucTxBuffer[NUM_MESSAGE_BYTES];

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

   Egg: In car check for seeing if the elevator you're in is Smartrise C4.

   Press DOB & DCB together 3 times in succession, then hold both in for 3 seconds.
 -----------------------------------------------------------------------------*/
static uint8_t bSmartriseFlag;
static char aucSmartriseString[4];
static void UpdateSmartriseFlag(void)
{
   static char const * const pasStrings[ NUM_C4C ] =
   {
      C4C_TABLE(EXPAND_C4C_TABLE_AS_STRING_ARRAY)
   };
   static uint8_t bLastActive;
   static uint8_t ucToggleCounter;
   static uint32_t uiResetTimer_5ms;
   static uint16_t uwHoldTimer_5ms;
   static uint8_t ucState;
   if( GetOperation_AutoMode() == MODE_A__NORMAL )
   {
      if(bSmartriseFlag)
      {
         if(++uwHoldTimer_5ms >= FLAG_SEQUENCE_FLASH_RATE_5MS)
         {
            uwHoldTimer_5ms = 0;
            bLastActive ^= 1;
            ucToggleCounter = (bLastActive) ? ucToggleCounter+1:ucToggleCounter;
         }

         if(!ucState)
         {
            if(bLastActive)
            {
               aucSmartriseString[2] = 'R';
               aucSmartriseString[1] = 'S';
               aucSmartriseString[0] = ' ';
            }
            else
            {
               aucSmartriseString[2] = ' ';
               aucSmartriseString[1] = ' ';
               aucSmartriseString[0] = ' ';
            }

            if(ucToggleCounter >= FLAG_SEQUENCE_CYCLES_TO_FLASH)
            {
               ucToggleCounter = 0;
               ucState = 1;
               uiResetTimer_5ms = 0;
            }
         }
         else if(ucState < NUM_C4C+1)
         {
            if(bLastActive)
            {
               aucSmartriseString[2] = *(pasStrings[ucState-1]+2);
               aucSmartriseString[1] = *(pasStrings[ucState-1]+1);
               aucSmartriseString[0] = *(pasStrings[ucState-1]+0);
            }
            else
            {
               aucSmartriseString[2] = ' ';
               aucSmartriseString[1] = ' ';
               aucSmartriseString[0] = ' ';
            }

            if(ucToggleCounter >= FLAG_SEQUENCE_CYCLES_TO_FLASH/2)
            {
               ucToggleCounter = 0;
               ucState++;
            }
         }
         else
         {
            ucState = 0;
            bSmartriseFlag = 0;
         }
      }
      else
      {
         uint8_t bActive = GetInputValue(enIN_DOB_F) && GetInputValue(enIN_DCB_F);
         bActive |= GetInputValue(enIN_DOB_R) && GetInputValue(enIN_DCB_R);
         if( bActive )
         {
            uiResetTimer_5ms = 0;
            if(!bLastActive)
            {
               ucToggleCounter++;
            }
            else if( ucToggleCounter > FLAG_SEQUENCE_PRESS_LIMIT )
            {
               if( ++uwHoldTimer_5ms >= FLAG_SEQUENCE_HOLD_TIME_5MS )
               {
                  bSmartriseFlag = 1;
                  uiResetTimer_5ms = 0;
                  ucToggleCounter = 0;
                  uwHoldTimer_5ms = 0;
                  ucState = 0;
               }
            }
         }
         else
         {
            uwHoldTimer_5ms = 0;
         }
         bLastActive = bActive;

         if(++uiResetTimer_5ms >= FLAG_SEQUENCE_RESET_LIMIT_5MS)
         {
            uiResetTimer_5ms = 0;
            ucToggleCounter = 0;
            uwHoldTimer_5ms = 0;
         }
      }
   }
}
/*-----------------------------------------------------------------------------
(*ps) = PI Left Char
(*ps+1) = PI Right Char
 -----------------------------------------------------------------------------*/
char * Get_PI_Label( uint8_t ucFloor )
{
   static char aucChar[4];
   if(bSmartriseFlag)
   {
      return &aucSmartriseString[0];
   }
   else
   {
      uint32_t ulData = Param_ReadValue_24Bit(enPARAM24__PI_0 + ucFloor);
      aucChar[2] = ulData & 0xFF;
      aucChar[1] = ulData >> 8 & 0xFF;
      aucChar[0] = ulData >> 16 & 0xFF;
      aucChar[3] = 0;
      return &aucChar[0];
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define DOUBLE_CHIME_ARRIVAL_DOWN_ON_TIME_5MS         (100)
#define DOUBLE_CHIME_ARRIVAL_DOWN_OFF_TIME_5MS        (100)
static void Update_ArrivalArrows(void)
{
   static uint8_t aucDoubleChimeDownCounter_ON_5ms[NUM_OF_DOORS];
   static uint8_t aucDoubleChimeDownCounter_OFF_5ms[NUM_OF_DOORS];
   stDataBuffer.bArrivalF_UP = GetOutputValue(enOUT_ARV_UP_F);
   stDataBuffer.bArrivalR_UP = GetOutputValue(enOUT_ARV_UP_R);
   if( Param_ReadValue_1Bit(enPARAM1__DoubleChimeOnDown) )
   {
      uint8_t bArrivalF_DN = 0;
      uint8_t bArrivalR_DN = 0;
      if( GetOutputValue(enOUT_ARV_DN_F) )
      {
         if( aucDoubleChimeDownCounter_ON_5ms[DOOR_FRONT] >= DOUBLE_CHIME_ARRIVAL_DOWN_ON_TIME_5MS )
         {
            if( aucDoubleChimeDownCounter_OFF_5ms[DOOR_FRONT] >= DOUBLE_CHIME_ARRIVAL_DOWN_OFF_TIME_5MS )
            {
               bArrivalF_DN = 1;
            }
            else
            {
               aucDoubleChimeDownCounter_OFF_5ms[DOOR_FRONT]++;
            }
         }
         else
         {
            aucDoubleChimeDownCounter_ON_5ms[DOOR_FRONT]++;
            bArrivalF_DN = 1;
         }
      }
      else
      {
         aucDoubleChimeDownCounter_ON_5ms[DOOR_FRONT] = 0;
         aucDoubleChimeDownCounter_OFF_5ms[DOOR_FRONT] = 0;
      }

      if( GetOutputValue(enOUT_ARV_DN_R) )
      {
         if( aucDoubleChimeDownCounter_ON_5ms[DOOR_REAR] >= DOUBLE_CHIME_ARRIVAL_DOWN_ON_TIME_5MS )
         {
            if( aucDoubleChimeDownCounter_OFF_5ms[DOOR_REAR] >= DOUBLE_CHIME_ARRIVAL_DOWN_OFF_TIME_5MS )
            {
               bArrivalR_DN = 1;
            }
            else
            {
               aucDoubleChimeDownCounter_OFF_5ms[DOOR_REAR]++;
            }
         }
         else
         {
            aucDoubleChimeDownCounter_ON_5ms[DOOR_REAR]++;
            bArrivalR_DN = 1;
         }
      }
      else
      {
         aucDoubleChimeDownCounter_ON_5ms[DOOR_REAR] = 0;
         aucDoubleChimeDownCounter_OFF_5ms[DOOR_REAR] = 0;
      }
      stDataBuffer.bArrivalF_DN = bArrivalF_DN;
      stDataBuffer.bArrivalR_DN = bArrivalR_DN;
   }
   else
   {
      stDataBuffer.bArrivalF_DN = GetOutputValue(enOUT_ARV_DN_F);
      stDataBuffer.bArrivalR_DN = GetOutputValue(enOUT_ARV_DN_R);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_TravelArrows(void)
{
   if(GetMotion_Direction() > 0)
   {
     stDataBuffer.bTravel_UP = 1;
     stDataBuffer.bTravel_DN = 0;
   }
   else if(GetMotion_Direction() < 0)
   {
     stDataBuffer.bTravel_UP = 0;
     stDataBuffer.bTravel_DN = 1;
   }
   else if( !Param_ReadValue_1Bit(enPARAM1__DisableIdleTravelArrows) )
   {
      if( GetOutputValue(enOUT_ARV_UP_F) || GetOutputValue(enOUT_ARV_UP_R) || GetCarPriority() )
      {
        stDataBuffer.bTravel_UP = 1;
        stDataBuffer.bTravel_DN = 0;
      }
      else if( GetOutputValue(enOUT_ARV_DN_F) || GetOutputValue(enOUT_ARV_DN_R) || !GetCarPriority()  )
      {
        stDataBuffer.bTravel_UP = 0;
        stDataBuffer.bTravel_DN = 1;
      }
      else
      {
         stDataBuffer.bTravel_UP = 0;
         stDataBuffer.bTravel_DN = 0;
      }
   }
   else
   {
      stDataBuffer.bTravel_UP = 0;
      stDataBuffer.bTravel_DN = 0;
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_FireLamp(void)
{
   if(GetOperation_AutoMode() == MODE_A__FIRE1 || GetOperation_AutoMode() == MODE_A__FIRE2)
   {
      stDataBuffer.bFire = 1;
   }
   else
   {
      stDataBuffer.bFire = 0;
   }
}
/*-----------------------------------------------------------------------------
   For annunciator of floor levels
   Holds strobe high for 1 sec
 -----------------------------------------------------------------------------*/
static void Update_StrobeLamp(void)
{
   static uint16_t uwCountdown_5ms;
   static uint8_t bLastDZ;

   if( !Param_ReadValue_1Bit(enPARAM1__DisableCE_FloorPlus1) )
   {
      stDataBuffer.ucSlot = GetOperation_CurrentFloor()+1;
   }
   else
   {
      stDataBuffer.ucSlot = GetOperation_CurrentFloor();
   }

   if( ( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R) )
    && !bLastDZ )
   {
      uwCountdown_5ms = 300;
   }

   if( ( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R) )
    && uwCountdown_5ms
    && GetOperation_CurrentFloor() == GetOperation_DestinationFloor())
   {
      uwCountdown_5ms--;
      stDataBuffer.bStrobe = 1;
   }
   else
   {
      uwCountdown_5ms = 0;
      stDataBuffer.bStrobe = 0;
   }
   bLastDZ = ( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R) );
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_Message(void)
{
   stDataBuffer.ucFireFloor = 0;
   if(GetOperation_AutoMode() == MODE_A__FIRE1 || GetOperation_AutoMode() == MODE_A__FIRE2)
   {
      if( GetEmergencyBit( EmergencyBF_FirePhaseI_RecallToAltFloor ) )
      {
         stDataBuffer.ucMessageNum = CE_MSG__FIRE_ALT;
         stDataBuffer.ucFireFloor = Param_ReadValue_8Bit(enPARAM8__FireRecallFloorA)+1;
         stDataBuffer.bFireDoor = Param_ReadValue_1Bit(enPARAM1__FireRecallDoorA);
      }
      else
      {
         stDataBuffer.ucMessageNum = CE_MSG__FIRE_MAIN;
         stDataBuffer.ucFireFloor = Param_ReadValue_8Bit(enPARAM8__FireRecallFloorM)+1;
         stDataBuffer.bFireDoor = Param_ReadValue_1Bit(enPARAM1__FireRecallDoorM);
      }
   }
   else if( ( GetDoorState_Front() == DOOR__NUDGING )
         || ( GetDoorState_Rear() == DOOR__NUDGING ) )
   {
      stDataBuffer.ucMessageNum = CE_MSG__NUDGE;
   }
   else if(GetOperation_AutoMode() == MODE_A__INDP_SRV)
   {
      stDataBuffer.ucMessageNum = CE_MSG__INDP_SRV;
   }
   else if( GetOutputValue(enOUT_Overload) )
   {
      stDataBuffer.ucMessageNum = CE_MSG__OVERLOAD;
   }
   else if( ( GetOperation_AutoMode() == MODE_A__EPOWER )
         && ( !CheckIf_EPowerCarInNormal() ) )
   {
      stDataBuffer.ucMessageNum = CE_MSG__EPOWER;
   }
   else if(GetOperation_ManualMode() >= MODE_M__INSP_CT && GetOperation_ManualMode() < NUM_MODE_MANUAL)
   {
      stDataBuffer.ucMessageNum = CE_MSG__INSP;
   }
   else if (GetOperation_AutoMode() == MODE_A__SEISMC)
   {
      stDataBuffer.ucMessageNum = CE_MSG__SEISMIC;
   }
   else
   {
      stDataBuffer.ucMessageNum = CE_MSG__NONE;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define OOS_STATUS_TOGGLE_RATE_5MS     (200)
static void Update_PI(void)
{
   /* Simulate a flashing OOS display if in OOS mode, if in normal and car is captured or faulted */
   static uint8_t bOOS;
   static uint8_t bCR;
   static uint8_t bVIP;
   static uint8_t ucToggleCounter_5ms;
   if( ( GetOperation_AutoMode() == MODE_A__OOS )
    || ( ( GetOperation_AutoMode() == MODE_A__NORMAL )
      && ( gstFault.bActiveFault || ( HallCallsDisabled() && ( GetOperation3_CaptureMode_MR() == CAPTURE_IDLE ) ) ) ) )
   {
      if(++ucToggleCounter_5ms >= OOS_STATUS_TOGGLE_RATE_5MS)
      {
         bOOS = !bOOS;
         ucToggleCounter_5ms = 0;
      }
   }
   else if(GetOperation_AutoMode() == MODE_A__ACTIVE_SHOOTER_MODE)
   {
        if(++ucToggleCounter_5ms >= OOS_STATUS_TOGGLE_RATE_5MS)
        {
           bCR = !bCR;
           ucToggleCounter_5ms = 0;
        }
   }
   else if( !GetGroup_VIP_CarCapturing() && ( GetOperation_AutoMode() == MODE_A__VIP_MODE ) )
   {
        if(++ucToggleCounter_5ms >= OOS_STATUS_TOGGLE_RATE_5MS)
        {
           bVIP = !bVIP;
           ucToggleCounter_5ms = 0;
        }
   }
   else
   {
      ucToggleCounter_5ms = 0;
      bOOS = 0;
   }

   if(bOOS)
   {
      stDataBuffer.ucCharL = 'O';
      stDataBuffer.ucCharM = 'O';
      stDataBuffer.ucCharR = 'S';
   }
   else if(bCR)
   {
      stDataBuffer.ucCharL = ' ';
      stDataBuffer.ucCharM = 'C';
      stDataBuffer.ucCharR = 'R';
   }
   else if(bVIP)
   {
      stDataBuffer.ucCharL = ' ';
      stDataBuffer.ucCharM = 'V';
      stDataBuffer.ucCharR = 'P';
   }
   else
   {
      char * ps = Get_PI_Label(GetOperation_CurrentFloor());
      stDataBuffer.ucCharL = *(ps+0);
      stDataBuffer.ucCharM = *(ps+1);
      stDataBuffer.ucCharR = *(ps+2);

      if(stDataBuffer.ucCharL == 0x20)
      {
         stDataBuffer.ucCharL = 0x3B;
      }
      if(stDataBuffer.ucCharM == 0x20)
      {
         stDataBuffer.ucCharM = 0x3B;
      }
      if(stDataBuffer.ucCharR == 0x20)
      {
         stDataBuffer.ucCharR = 0x3B;
      }
   }
}

static void Update_Chime()
{
   stDataBuffer.bChime = GetLocalChimeSignal();
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

static void Update_DisplayData( void )
{
   stDataBuffer.ucCurrentFloor = GetOperation_CurrentFloor()+1;
   Update_TravelArrows();
   Update_ArrivalArrows();
   Update_FireLamp();
   Update_StrobeLamp();

   Update_PI();

   Update_Message();
   Update_Chime();
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData( void )
{
   /* Byte 1 Header + Arrival Dir + Traveling Dir */
   aucTxBuffer[0] = 0x80; // Indicate header byte
   if(stDataBuffer.bStrobe)
   {
      aucTxBuffer[0] |= 0x40;
   }
   if(stDataBuffer.bArrivalF_UP)
   {
      aucTxBuffer[0] |= 0x04;
   }
   else if(stDataBuffer.bArrivalF_DN)
   {
      aucTxBuffer[0] |= 0x08;
   }
   if(stDataBuffer.bArrivalR_UP)
   {
      aucTxBuffer[0] |= 0x10;
   }
   else if(stDataBuffer.bArrivalF_DN)
   {
      aucTxBuffer[0] |= 0x20;
   }
   if(stDataBuffer.bTravel_UP)
   {
      aucTxBuffer[0] |= 0x01;
   }
   else if(stDataBuffer.bTravel_DN)
   {
      aucTxBuffer[0] |= 0x02;
   }

   /* Byte 2 Current Floor*/
   aucTxBuffer[1] = stDataBuffer.ucCurrentFloor & 0x7F;

   /* Byte 3 Passing Chime + Fire Recall Door */
   aucTxBuffer[2] = stDataBuffer.bFireDoor;
   if(stDataBuffer.bChime)
   {
      aucTxBuffer[2] |= 0x02;
   }

   /* Byte 4 Fire floor */
   aucTxBuffer[3] = stDataBuffer.ucFireFloor & 0x7F;

   /* Byte 5 Arrival Floor*/
   aucTxBuffer[4] = stDataBuffer.ucCurrentFloor & 0x7F;

   /* Byte 6-8 PI Label */
   aucTxBuffer[5] = stDataBuffer.ucCharL & 0x7F;
   aucTxBuffer[6] = stDataBuffer.ucCharM & 0x7F;
   aucTxBuffer[7] = stDataBuffer.ucCharR & 0x7F;

   /* Byte 9 Message Number */
   aucTxBuffer[8] = stDataBuffer.ucMessageNum & 0x7F;

   /* Byte 10 Voice Announcement */
   aucTxBuffer[9] = 0;// From V2
   //aucTxBuffer[9] = stDataBuffer.ucCurrentFloor & 0x7F;

   aucTxBuffer[10] = 0;
   for(uint8_t i = 0; i < NUM_MESSAGE_BYTES-1; i++)
   {
      aucTxBuffer[10] += aucTxBuffer[i];
   }
   aucTxBuffer[10] &= 0x7F;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
#define TRANSMIT_DELAY_5MS    (20)
static void TransmitData(void)
{
   static uint8_t ucDelay_5ms;
   if(ucDelay_5ms >= TRANSMIT_DELAY_5MS)
   {
      if(UART_RS485_SendRB(&aucTxBuffer[0], NUM_MESSAGE_BYTES))
      {
         ucDelay_5ms = 0;
      }
   }
   else
   {
      ucDelay_5ms++;
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10000;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_EMOTIVE_BOARD_1MS;

   if( Param_ReadValue_1Bit(enPARAM1__Enable_Janus_RS_Fixture) )
   {
      UART_RS485_EMOTIVE_Init();
   }

   return 0;
}

/*-----------------------------------------------------------------------------
This module handles updates to the janus emotive
MX003DP0X-SM (Firmware QT 819C) serial encoder board
 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   if( Param_ReadValue_1Bit(enPARAM1__Enable_Janus_RS_Fixture) )
   {
      UpdateSmartriseFlag();
      UpdateLocalChimeSignal(pstThisModule->uwRunPeriod_1ms);
      Update_DisplayData();
      LoadData();

      TransmitData();
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
