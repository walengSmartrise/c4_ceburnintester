/******************************************************************************
 *
 * @file     mod_etsl.c
 * @brief
 * @version  V1.00
 * @date     29, June 2018
 * @author   Keith Soneda
 *
 * @note     Performs ETSL checks
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "sru_a.h"
#include "sys.h"
#include "operation.h"
#include "position.h"
#include "decel.h"
#include "acceptance.h"
#include "mod_etsl.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_module gstMod_ETSL =
{
   .pfnInit = Init,
   .pfnRun = Run,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
typedef struct
{
   uint16_t auwSpeed[NUM_ETS_TRIP_POINTS];
   uint32_t auiPos[NUM_ETS_TRIP_POINTS];
} ETS_Profile;
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static struct st_position stPosition;

static ETS_Profile astETSL_Profiles[NUM_MOTION_PROFILES];
static uint32_t auiMaxSlowdownDistance[NUM_MOTION_PROFILES];
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Access functions
 -----------------------------------------------------------------------------*/
int16_t ETSL_GetSpeed()
{
   return stPosition.wVelocity;
}
uint32_t ETSL_GetPosition()
{
   return stPosition.ulPosCount;
}
void ETSL_SetSpeed(int16_t wSpeed)
{
   stPosition.wVelocity = wSpeed;
}
void ETSL_SetPosition(uint32_t uiPosition)
{
   stPosition.ulPosCount = uiPosition;
}

/*-----------------------------------------------------------------------------
   Checks that ETS points are increasing, and that speeds are nonzero
 -----------------------------------------------------------------------------*/
static void CheckFor_InvalidETSLFault()
{
   /* If not on learn, and bottom floor has been learned */
   if( ( GetOperation_ClassOfOp() != CLASSOP__SEMI_AUTO )
    && ( Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0) ) )
   {
      for( Motion_Profile eProfile = MOTION_PROFILE__1; eProfile < NUM_MOTION_PROFILES; eProfile++ )
      {
         uint32_t uiTripSpeed = astETSL_Profiles[eProfile].auwSpeed[0];
         uint32_t uiTripPos = astETSL_Profiles[eProfile].auiPos[0];
         for( uint8_t i = 1; i < NUM_ETS_TRIP_POINTS; i++ )
         {
            if( !astETSL_Profiles[eProfile].auwSpeed[i] )
            {
               SetFault(FLT__INVALID_ETSL_1+eProfile);
               break;
            }
            else if( uiTripSpeed > astETSL_Profiles[eProfile].auwSpeed[i] )
            {
               SetFault(FLT__INVALID_ETSL_1+eProfile);
               break;
            }
            else if( uiTripPos > astETSL_Profiles[eProfile].auiPos[i] )
            {
               SetFault(FLT__INVALID_ETSL_1+eProfile);
               break;
            }
            else if( uiTripSpeed > Param_ReadValue_16Bit(enPARAM16__ContractSpeed) )
            {
               SetFault(FLT__INVALID_ETSL_1+eProfile);
               break;
            }
            uiTripSpeed = astETSL_Profiles[eProfile].auwSpeed[i];
            uiTripPos = astETSL_Profiles[eProfile].auiPos[i];
         }
      }
   }

}
static void CheckFor_ETSLOverspeedFault_OnePoint( void )
{
    uint8_t bClearCounter = 1;
    static uint8_t ucCounter;

    if( ( GetOperation_ManualMode() != MODE_M__CONSTRUCTION )
     && ( GetDebug_ActiveAcceptanceTest() != ACCEPTANCE_TEST_NTS )
     && ( GetDebug_ActiveAcceptanceTest() != ACCEPTANCE_TEST_ETS )
     )
    {
       uint32_t uiMiddlePos = Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 + (GetFP_NumFloors() / 2) - 1 );
       uint32_t uiBottomTerminalPos = Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0);

       uiMiddlePos -= uiBottomTerminalPos;

       Motion_Profile eProfile = GetMotion_Profile();
       int16_t wCarSpeed;

       uint8_t ucOverspeedDebounceLimit = Param_ReadValue_8Bit(enPARAM8__ETSL_ODL_10ms);
       ucOverspeedDebounceLimit = ( ucOverspeedDebounceLimit >= MAX_DEBOUNCE_LIMIT_OVERSPEED_ETSL )
             ? (MAX_DEBOUNCE_LIMIT_OVERSPEED_ETSL):(ucOverspeedDebounceLimit);
       wCarSpeed = ETSL_GetSpeed();
       if ( wCarSpeed > MIN_ETS_TRIP_SPEED )
       {
          for( uint8_t i = 0; i < NUM_ETS_TRIP_POINTS; i++ )
          {
             uint32_t uiTripPos = Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 + GetFP_NumFloors() - 1 )
                                - astETSL_Profiles[eProfile].auiPos[i] - uiMiddlePos;

             if( ( ETSL_GetPosition() - Param_ReadValue_16Bit(enPARAM16__ETSL_CameraOffset_05mm) ) > uiTripPos )
             {
                int16_t wSpeedLimit = astETSL_Profiles[eProfile].auwSpeed[i] * ETSL_VELOCITY_SCALE_FACTOR_TEST;
                if( wCarSpeed > wSpeedLimit )
                {
                   bClearCounter = 0;
                   if( ucCounter >= ucOverspeedDebounceLimit )
                   {
                	  if(i == Param_ReadValue_8Bit(enPARAM8__Acceptance_ETSL_Point))
                	  {
                          SetFault(FLT__OVERSPEED_UETSL_1+i);
                	  }

                   }
                   else
                   {
                      ucCounter++;
                   }
                   break;
                }
             }
          }
       }
       else if( wCarSpeed < ( -1 * MIN_ETS_TRIP_SPEED ) )
       {
          for( uint8_t i = 0; i < NUM_ETS_TRIP_POINTS; i++ )
          {
             uint32_t uiTripPos = Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 )
                                + astETSL_Profiles[eProfile].auiPos[i] + uiMiddlePos;
             if( ( ETSL_GetPosition() - Param_ReadValue_16Bit(enPARAM16__ETSL_CameraOffset_05mm) ) < uiTripPos )
             {
                int16_t wSpeedLimit = astETSL_Profiles[eProfile].auwSpeed[i] * ETSL_VELOCITY_SCALE_FACTOR_TEST * -1;
                if( wCarSpeed < wSpeedLimit )
                {
                   bClearCounter = 0;
                   if( ucCounter >= ucOverspeedDebounceLimit )
                   {
                 	  if(i == Param_ReadValue_8Bit(enPARAM8__Acceptance_ETSL_Point))
                 	  {
                           SetFault(FLT__OVERSPEED_DETSL_1+i);
                 	  }
                   }
                   else
                   {
                      ucCounter++;
                   }
                   break;
                }
             }
          }
       }
    }

    if( bClearCounter )
    {
       ucCounter = 0;
    }
}
/*-----------------------------------------------------------------------------

   Checks car speed/position against precalculated ETS points.

   If the car exceeds any of these points by more than 10% for
   a number of cycles determined by enPARAM8__ETSOverspeedDebounceLimit,
   then the car will perform an emergency stop.

   One of three sets of ETS points will be selected depending
   on the active profile, viewed via GetMotion_Profile(),

 -----------------------------------------------------------------------------*/
static void CheckFor_ETSLOverspeedFault( void )
{
    uint8_t bClearCounter = 1;
    static uint8_t ucCounter;

    if( ( !Param_ReadValue_1Bit( enPARAM1__BypassTermLimits ) )
     && ( GetOperation_ManualMode() != MODE_M__CONSTRUCTION )
     && ( GetDebug_ActiveAcceptanceTest() != ACCEPTANCE_TEST_NTS )
     && ( GetDebug_ActiveAcceptanceTest() != ACCEPTANCE_TEST_ETS )
     )
    {
       Motion_Profile eProfile = GetMotion_Profile();
       int16_t wCarSpeed;

       uint8_t ucOverspeedDebounceLimit = Param_ReadValue_8Bit(enPARAM8__ETSL_ODL_10ms);
       ucOverspeedDebounceLimit = ( ucOverspeedDebounceLimit >= MAX_DEBOUNCE_LIMIT_OVERSPEED_ETSL )
             ? (MAX_DEBOUNCE_LIMIT_OVERSPEED_ETSL):(ucOverspeedDebounceLimit);
       wCarSpeed = ETSL_GetSpeed();
       if ( wCarSpeed > MIN_ETS_TRIP_SPEED )
       {
          for( uint8_t i = 0; i < NUM_ETS_TRIP_POINTS; i++ )
          {
             uint32_t uiTripPos = Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 + GetFP_NumFloors() - 1 )
                                - astETSL_Profiles[eProfile].auiPos[i];
             if( ( ETSL_GetPosition() - Param_ReadValue_16Bit(enPARAM16__ETSL_CameraOffset_05mm) ) > uiTripPos )
             {
                int16_t wSpeedLimit = astETSL_Profiles[eProfile].auwSpeed[i] * ETSL_VELOCITY_SCALE_FACTOR;
                if( wCarSpeed > wSpeedLimit )
                {
                   bClearCounter = 0;
                   if( ucCounter >= ucOverspeedDebounceLimit )
                   {
                      SetFault(FLT__OVERSPEED_UETSL_1+i);
                   }
                   else
                   {
                      ucCounter++;
                   }
                   break;
                }
             }
          }
       }
       else if( wCarSpeed < ( -1 * MIN_ETS_TRIP_SPEED ) )
       {
          for( uint8_t i = 0; i < NUM_ETS_TRIP_POINTS; i++ )
          {
             uint32_t uiTripPos = Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 )
                                + astETSL_Profiles[eProfile].auiPos[i];
             if( ( ETSL_GetPosition() - Param_ReadValue_16Bit(enPARAM16__ETSL_CameraOffset_05mm) ) < uiTripPos )
             {
                int16_t wSpeedLimit = astETSL_Profiles[eProfile].auwSpeed[i] * ETSL_VELOCITY_SCALE_FACTOR * -1;
                if( wCarSpeed < wSpeedLimit )
                {
                   bClearCounter = 0;
                   if( ucCounter >= ucOverspeedDebounceLimit )
                   {
                      SetFault(FLT__OVERSPEED_DETSL_1+i);
                   }
                   else
                   {
                      ucCounter++;
                   }
                   break;
                }
             }
          }
       }
    }

    if( bClearCounter )
    {
       ucCounter = 0;
    }
}

/*--------------------------------------------------------------------------
   For both the up and down directions:
      Takes 10 evenly spaced time index points from the overall ramp down time.
      These points are offset by half of their difference.
      Finds the speed and absolute position at those points and updates system parameters.

--------------------------------------------------------------------------*/
static void UpdateETSLTripPoints( Motion_Profile eProfile )
{
   Motion_Profile eSavedProfile = GetMotion_Profile();
   SetMotion_Profile(eProfile);
   //Calculate the decel pattern for max speed to get the slowdown distance
   if( eProfile == MOTION_PROFILE__3 )
   {
      Decel_GenerateDecelRun(Param_ReadValue_16Bit(enPARAM16__EPowerSpeed_fpm)/60.0f);
   }
   else
   {
      Decel_GenerateDecelRun(gpstDecelMotionCtrl->flContractSpeed_FPS);
   }
   uint32_t uiTimeBetweenTripPoints = gpstDecelMotionCtrl->stRunParameters.uiPatternDecelTime/NUM_ETS_TRIP_POINTS;
   uint32_t uiTimeOffset =  uiTimeBetweenTripPoints/2;
   uint16_t uwOffsetFromNTS = Param_ReadValue_8Bit( enPARAM8__ETS_OffsetFromNTS_5mm ) * 10;

   for(uint8_t i = 0; i < NUM_ETS_TRIP_POINTS; i++)
   {
      uint32_t uiTimeIndex = (uiTimeBetweenTripPoints * i) + uiTimeOffset;
      uint32_t uiRelativePosition = Decel_GetDecelPosition(uiTimeIndex) + ( gpstDecelCurveParameters + eProfile )->uwLevelingDistance;
      if( uwOffsetFromNTS < uiRelativePosition )
      {
         uiRelativePosition -= uwOffsetFromNTS;
      }
      else
      {
         uiRelativePosition = 0;
      }

      uint16_t uwTripSpeed = Decel_GetDecelCommandSpeed(uiTimeIndex);

      astETSL_Profiles[eProfile].auwSpeed[i] = uwTripSpeed;
      astETSL_Profiles[eProfile].auiPos[i] = uiRelativePosition;
   }

   auiMaxSlowdownDistance[eProfile] = gpstDecelMotionCtrl->stRunParameters.uiSlowdownDistance;

   /* Restored previous state */
   SetMotion_Profile(eSavedProfile);
}
/*-----------------------------------------------------------------------------
   Update curve parameters & ETS/NTS points
 -----------------------------------------------------------------------------*/
static void UpdateMotionProfiles()
{
   for( Motion_Profile i = 0; i < NUM_MOTION_PROFILES; ++i )
   {
      if( Decel_UpdateParameters( i ) )
      {
         Decel_UpdateCurveLimits( i );
         UpdateETSLTripPoints( i );
      }
   }
}

/*-----------------------------------------------------------------------------
   Update ETSL Camera Offset
   (the position difference between the Primary CEDES camera
   and the secondary ETSL CEDES camera)
 -----------------------------------------------------------------------------*/
static void UpdateCameraOffset()
{
   static enum en_mode_learn eLastMode;

   if( ( GetOperation_LearnMode() == MODE_L__READY_HA )
    && ( eLastMode != GetOperation_LearnMode() ) )
   {
      if( GetPosition_PositionCount() < ETSL_GetPosition() )
      {
         uint32_t uiOffset = ETSL_GetPosition() - GetPosition_PositionCount();
         Param_WriteValue_16Bit(enPARAM16__ETSL_CameraOffset_05mm, uiOffset);
      }
      else
      {
         Param_WriteValue_16Bit(enPARAM16__ETSL_CameraOffset_05mm, 0);
      }
   }

   eLastMode = GetOperation_LearnMode();
}

/*-----------------------------------------------------------------------------
   Check for reduced stroke buffer fault
   For a reduced stroke buffer, ETSL points may need to be placed further
   from the terminal floor to prevent striking the buffer at a high speed.

   Check that no ETSL trip will result in sliding into the buffer (based on estimated slowdown distance) at speeds
   over its rating.
 -----------------------------------------------------------------------------*/
static void CheckFor_RSBufferFault(void)
{
   float fSlideDistance_05mm = Param_ReadValue_16Bit(enPARAM16__Acceptance_SlideDistance);
   float fBufferDistance_05mm = Param_ReadValue_16Bit(enPARAM16__BufferDistance_05mm);
   for( Motion_Profile i = 0; i < NUM_MOTION_PROFILES; ++i )
   {
      float fContractSpeed_fpm = Param_ReadValue_16Bit(enPARAM16__ContractSpeed);
      if( i == MOTION_PROFILE__3 )
      {
         fContractSpeed_fpm = Param_ReadValue_16Bit(enPARAM16__EPowerSpeed_fpm);
      }
      float fBufferSpeed_fpm = (Param_ReadValue_8Bit(enPARAM8__RatedBufferSpd_10fpm))
                             ? Param_ReadValue_8Bit(enPARAM8__RatedBufferSpd_10fpm)*10
                             : fContractSpeed_fpm;
      if( fContractSpeed_fpm >= fBufferSpeed_fpm )
      {
         float fBufferToContractRatio =  (fContractSpeed_fpm-fBufferSpeed_fpm)/fContractSpeed_fpm;
         float fRequriedSlowdown = 1.10f * ( fSlideDistance_05mm/HALF_MM_PER_FT ) * fBufferToContractRatio;
         float fRequiredSlowdown_05mm = fRequriedSlowdown*HALF_MM_PER_FT;
         if(i != MOTION_PROFILE__2) // Omit inspection profile
         {
            float fEstimatedSlowdown = 0.90f * ( auiMaxSlowdownDistance[i]/HALF_MM_PER_FT ) * fBufferToContractRatio;
            float fEstimatedSlowdown_05mm = fEstimatedSlowdown*HALF_MM_PER_FT;
            if(fRequiredSlowdown_05mm > ( fEstimatedSlowdown_05mm + fBufferDistance_05mm ) )
            {
               SetFault(FLT__RSBUFFER_P1+i);
               break;
            }
         }
      }
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   //------------------------
   pstThisModule->uwInitialDelay_1ms = 100;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_ETSL_1MS;

   UpdateMotionProfiles();

   return 0;
}

/*-----------------------------------------------------------------------------


 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   if(Param_ReadValue_1Bit(enPARAM1__Enable_ETSL))
   {
      UpdateCameraOffset();

      if(GetDebug_ActiveAcceptanceTest() == ACCEPTANCE_TEST_ETSL)
      {
         CheckFor_InvalidETSLFault();
         CheckFor_ETSLOverspeedFault_OnePoint();
      }
      else
      {
         UpdateMotionProfiles();
         CheckFor_InvalidETSLFault();
         CheckFor_ETSLOverspeedFault();
      }
      CheckFor_RSBufferFault();
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
