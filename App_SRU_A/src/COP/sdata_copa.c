/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     26, March 2016
 *
 * @note    Load local datagrams and selects datagrams for transmit
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "sru_a.h"
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
#include "motion.h"
#include "fpga_api.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Load_CPLD_Data(void)
{
   if( GetUIRequest_ViewDebugDataCommand() == VDD__CPLD_COP )
   {
      FPGA_Packet stPacket;
      un_sdata_datagram unDatagram;

      gstSData_LocalData.paiResendRate_1ms[DG_COPA__CPLD_Data0] = UI_REQ_LIFETIME_MS/2;
      gstSData_LocalData.paiResendRate_1ms[DG_COPA__CPLD_Data1] = UI_REQ_LIFETIME_MS/2;

      FPGA_GetRecentPacket(&stPacket, FPGA_LOC__COPA);

      unDatagram.auc8[0] = stPacket.aucData[FPGA_BYTE__DATA0];
      unDatagram.auc8[1] = stPacket.aucData[FPGA_BYTE__DATA1];
      unDatagram.auc8[2] = stPacket.aucData[FPGA_BYTE__DATA2];
      unDatagram.auc8[3] = stPacket.aucData[FPGA_BYTE__DATA3];
      unDatagram.auc8[4] = stPacket.aucData[FPGA_BYTE__DATA4];
      unDatagram.auc8[5] = stPacket.aucData[FPGA_BYTE__DATA5];
      unDatagram.auc8[6] = stPacket.aucData[FPGA_BYTE__DATA6];
      unDatagram.auc8[7] = stPacket.aucData[FPGA_BYTE__DATA7];
      SDATA_WriteDatagram( &gstSData_LocalData, DG_COPA__CPLD_Data0, &unDatagram );

      unDatagram.auc8[0] = stPacket.aucData[FPGA_BYTE__DATA8];
      unDatagram.auc8[1] = stPacket.aucData[FPGA_BYTE__DATA9];
      unDatagram.auc8[2] = stPacket.aucData[FPGA_BYTE__DATA10];
      unDatagram.auc8[3] = stPacket.aucData[FPGA_BYTE__DATA11];
      unDatagram.auc8[4] = stPacket.aucData[FPGA_BYTE__DATA12];
      unDatagram.auc8[5] = 0;
      unDatagram.auc8[6] = 0;
      unDatagram.auc8[7] = 0;
      SDATA_WriteDatagram( &gstSData_LocalData, DG_COPA__CPLD_Data1, &unDatagram );
   }
   else
   {
      gstSData_LocalData.paiResendRate_1ms[DG_COPA__CPLD_Data0] = DISABLED_DATAGRAM_RESEND_RATE_MS;
      gstSData_LocalData.paiResendRate_1ms[DG_COPA__CPLD_Data1] = DISABLED_DATAGRAM_RESEND_RATE_MS;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void LoadData_COP( void )
{
   un_sdata_datagram unDatagram;
   //--------------------------------------------------
   memset(&unDatagram, 0, sizeof(un_sdata_datagram));
   unDatagram.aui32[0] = GetLocalInputs_ThisNode();
   unDatagram.auc8[4] = FPGA_GetPreflightStatus(FPGA_LOC__COPA);//preflight
   SDATA_WriteDatagram( &gstSData_LocalData,
                        DG_COPA__LocalInputs,
                        &unDatagram
                      );

   //--------------------------------------------------
   memset(&unDatagram, 0, sizeof(un_sdata_datagram));
   unDatagram.auc8[0] = ( DL20_GetPhoneFailureFlag() )
                      | ( DL20_GetOOSFlag() << 1 );
   switch( GetUIRequest_ViewDebugDataCommand() )
   {
      case VDD__COP_CAN1:
         unDatagram.auc8[5] = Debug_CAN_GetUtilization(0);
         unDatagram.auw16[3] = GetDebugBusOfflineCounter_CAN1();
         break;
      case VDD__COP_CAN2:
         unDatagram.auc8[5] = Debug_CAN_GetUtilization(1);
         unDatagram.auw16[3] = GetDebugBusOfflineCounter_CAN2();
         break;
      case VDD__COP_A_NET:
         unDatagram.auw16[3] = UART_GetRxErrorCount();
         break;
      case VDD__COP_RS485:
         unDatagram.auw16[3] = 0;//todo
         break;
      case VDD__COPA_VERSION:
         unDatagram.auc8[4] = *(pasVersion+0);
         unDatagram.auc8[5] = *(pasVersion+1);
         unDatagram.auc8[6] = *(pasVersion+2);
         unDatagram.auc8[7] = *(pasVersion+3);
         break;
      default: break;
   }

   SDATA_WriteDatagram( &gstSData_LocalData,
                        DG_COPA__Debug,
                        &unDatagram
                       );

   Load_CPLD_Data();
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Unload_MRA()
{
   SetOperation_ClassOfOp(GetOperation2_ClassOfOperation_MRA());
   SetOperation_LearnMode(GetOperation2_LearnMode_MRA());
   SetOperation_AutoMode(GetOperation2_AutoMode_MRA());
   SetOperation_ManualMode(GetOperation2_ManualMode_MRA());
   SetOperation_CurrentFloor(GetOperation1_CurrentFloor_MRA());
   SetOperation_DestinationFloor(GetOperation1_DestinationFloor_MRA());
   SetOperation_RequestedDestination(GetOperation1_RequestedDestination_MRA());
   SetOperation_MotionCmd(GetOperation1_MotionCMD_MRA());

   SetOperation_SpeedLimit(GetOperation2_SpeedLimit_MRA());
   SetOperation_BypassTermLimits(GetOperation3_BypassTermLimits_MR());
   SetOperation_PositionLimit_UP(GetOperation3_PositionLimitUp_MR());
   SetOperation_PositionLimit_DN(GetOperation3_PositionLimitDown_MR());
   //-------------------------------------
   SetBitMap_LatchedCCB(GetCarCalls1_Front_MRA(), 0, 0);
   SetBitMap_LatchedCCB(GetCarCalls1_Rear_MRA(), 0, 1);
   //-------------------------------------
   /* Mapped Inputs */
   SetInputBitMap(GetMappedInputs1_InputBitmap1_MRA(), 0);
   SetInputBitMap(GetMappedInputs1_InputBitmap2_MRA(), 1);

   SetInputBitMap(GetMappedInputs2_InputBitmap3_MRA(), 2);
   SetInputBitMap(GetMappedInputs2_InputBitmap4_MRA(), 3);

   SetInputBitMap(GetMappedInputs3_InputBitmap5_MRA(), 4);
   SetInputBitMap(GetMappedInputs3_InputBitmap6_MRA(), 5);

   SetInputBitMap(GetMappedInputs4_InputBitmap7_MRA(), 6);
   SetInputBitMap(GetMappedInputs4_InputBitmap8_MRA(), 7);
   //-------------------------------------
   SetOutputBitMap(GetMappedOutputs1_OutputBitmap1_MRA(), 0);
   SetOutputBitMap(GetMappedOutputs1_OutputBitmap2_MRA(), 1);

   SetOutputBitMap(GetMappedOutputs2_OutputBitmap3_MRA(), 2);
   SetOutputBitMap(GetMappedOutputs2_OutputBitmap4_MRA(), 3);

   SetOutputBitMap(GetMappedOutputs3_OutputBitmap5_MRA(), 4);
   SetOutputBitMap(GetMappedOutputs3_OutputBitmap6_MRA(), 5);

   SetOutputBitMap(GetMappedOutputs4_OutputBitmap7_MRA(), 6);
   SetOutputBitMap(GetMappedOutputs4_OutputBitmap8_MRA(), 7);

   //-------------------------------------
   SetMotion_Destination(GetMotion_Destination_MRA());
   SetMotion_Direction(GetMotion_Direction_MRA());
   SetMotion_RunFlag(GetMotion_RunFlag_MRA());
   SetMotion_SpeedCommand(GetMotion_SpeedCMD_MRA());
   SetMotion_Profile(GetMotion_Profile_MRA());
   //-------------------------------------
   //Unload preflight flag and set output
   uint8_t bPreflight = GetOperation1_PreflightFlag_MRA();
   SRU_Write_Preflight_Output(bPreflight);
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void UnloadData_COP()
{
   Unload_MRA();
   //-------------------------------------
   RTC_SetSyncTime(GetSyncTime_Time_MRB());

}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
