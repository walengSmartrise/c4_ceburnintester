/******************************************************************************
 *
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sys.h"

#include "GlobalData.h"
#include <stdint.h>
#include <string.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Checks for invalid critical parameter fault
   Set when a critical parameter has been changed:
      Number of floors
      Rear Doors
      Contract speed
      Drive Select
 -----------------------------------------------------------------------------*/
static void CheckFor_NeedToReset()
{
   static uint8_t bFirst = 1;
   static uint8_t ucDriveSelect;
   static uint32_t auiOpeningF[BITMAP32_SIZE(MAX_NUM_FLOORS)];
   static uint32_t auiOpeningR[BITMAP32_SIZE(MAX_NUM_FLOORS)];
   static uint8_t bJanus;
   static uint8_t bDL20;
   if(bFirst)
   {
      bFirst = 0;
      ucDriveSelect = Param_ReadValue_8Bit(enPARAM8__DriveSelect);
      for(uint8_t i = 0; i < BITMAP32_SIZE(MAX_NUM_FLOORS); i++)
      {
         auiOpeningF[i] = Param_ReadValue_32Bit( enPARAM32__OpeningBitmapF_0 + i );
         auiOpeningR[i] = Param_ReadValue_32Bit( enPARAM32__OpeningBitmapR_0 + i );
      }
      bJanus = Param_ReadValue_1Bit(enPARAM1__Enable_Janus_RS_Fixture);
      bDL20 = Param_ReadValue_1Bit(enPARAM1__EnableDL20_CT) || Param_ReadValue_1Bit(enPARAM1__EnableDL20_COP);
   }
   else if( !GetMotion_RunFlag() )
   {
      if(GetFP_ContractSpeed() != Param_ReadValue_16Bit(enPARAM16__ContractSpeed))
      {
         /* Exception for Acceptance Test */
         if( GetOperation_AutoMode()  != MODE_A__TEST )
         {
            SetFault(FLT__NEED_TO_CYCLE_PWR_COP);
         }
      }
      else if(ucDriveSelect != Param_ReadValue_8Bit(enPARAM8__DriveSelect))
      {
         SetFault(FLT__NEED_TO_CYCLE_PWR_COP);
      }
      else if( GetOperation_ManualMode() != MODE_M__CONSTRUCTION )
      {
         if(GetFP_RearDoors() != Param_ReadValue_1Bit(enPARAM1__NumDoors))
         {
            SetFault(FLT__NEED_TO_CYCLE_PWR_COP);
         }
         else if(GetFP_FreightDoors() != Param_ReadValue_1Bit(enPARAM1__Enable_Freight_Doors))
         {
            SetFault(FLT__NEED_TO_CYCLE_PWR_COP);
         }
         else if(GetFP_NumFloors() != Param_ReadValue_8Bit(enPARAM8__NumFloors))
         {
            SetFault(FLT__NEED_TO_CYCLE_PWR_COP);
         }
         else if(bJanus != Param_ReadValue_1Bit(enPARAM1__Enable_Janus_RS_Fixture))
         {
            SetFault(FLT__NEED_TO_CYCLE_PWR_COP);
         }
         else if(bDL20 != ( Param_ReadValue_1Bit(enPARAM1__EnableDL20_CT) || Param_ReadValue_1Bit(enPARAM1__EnableDL20_COP) ))
         {
            SetFault(FLT__NEED_TO_CYCLE_PWR_COP);
         }
         else
         {
            for(uint8_t i = 0; i < BITMAP32_SIZE(GetFP_NumFloors()); i++)
            {
               if(auiOpeningF[i] != Param_ReadValue_32Bit( enPARAM32__OpeningBitmapF_0 + i ) )
               {
                  SetFault(FLT__NEED_TO_CYCLE_PWR_COP);
                  break;
               }
               else if( GetFP_RearDoors() && ( auiOpeningR[i] != Param_ReadValue_32Bit( enPARAM32__OpeningBitmapR_0 + i ) ) )
               {
                  SetFault(FLT__NEED_TO_CYCLE_PWR_COP);
                  break;
               }
            }
         }
      }
   }
}
/*----------------------------------------------------------------------------

    Called from mod_fault.c

 *----------------------------------------------------------------------------*/
void Run_ModFault_COPA( void )
{
   CheckFor_NeedToReset();

   UpdateActiveFaults();
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
