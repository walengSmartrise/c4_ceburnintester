/******************************************************************************
 *
 * @file     mod_can_test.c
 * @brief
 * @version  V1.00
 * @date     30, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "sru_a.h"
#include "sys.h"
#include "position.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
extern RINGBUFF_T RxPositionRingBuffer;

extern RINGBUFF_T RxCedesRingBuffer_0x21;//Packet 1 from cedes
extern RINGBUFF_T RxCedesRingBuffer_0x22;//Packet 2 from cedes

extern RINGBUFF_T RxCedesRingBuffer_0x2A;
extern RINGBUFF_T RxCedesRingBuffer_0x2B;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define DELAY_POS_FWD_TO_B_5MS   (5)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static CAN_MSG_T stRxMsg;
static CAN_MSG_T stTxMsg;

static uint16_t uwOfflineCounter_1ms;
static struct st_cedes_frame stCEDES;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
  Match CEDES 2 error to CEDES 1 protocol
 -----------------------------------------------------------------------------*/
static uint8_t GetCEDES2_GeneralError( void )
{
  uint8_t error;

  if( stCEDES.ucError & 0x3 )
  {
      /*Internal and comm errors are set in Byte 6 for new protocol*/
      switch( stCEDES.ucError & 0x3 )
      {
        case 0x01:
          error = 0x06;
          break;
        case 0x10:
          error = 0x07;
          break;
        case 0x11:
          error = 0x07;
          break;
        default:
          error = 0;
          break;
      }
  }
  /* Allignment errors are set in byte 7 for new protocol*/
  else if( stCEDES.ucClip & 0xC0 ) //allign far-near
  {
    switch( (stCEDES.ucClip & 0x20) >> 5 )
    {
      case 0x00:
        error = 0x02;
        break;
      case 0x01:
        error = 0x03;
        break;
    }
  }
  else if( stCEDES.ucClip & 0x1C ) //allign left-right
  {
    switch( (stCEDES.ucClip & 0x18) >> 3 )
    {
      case 0x00:
        error = 0x04;
        break;
      case 0x01:
        error = 0x05;
        break;
    }
  }
  return error;
}
/*-----------------------------------------------------------------------------
bit 7-6 Pos1 Err: Result of the internal position and velocity cross-check (channel 1 to channel 2)
   00 = Position1 and Velocity1 OK (position/velocity is safe)
   01 = Position1 Error (position comparison failed)
   10 = Velocity1 Error (velocity comparison failed)
   11 = Position1 and Velocity1 Error(position and velocity comparison failed)
bit 5-4 Pos2 Err: Result of the internal position and velocity cross-check (channel 2 to channel 1)
   00 = Position2 and Velocity2 OK (position and velocity is safe)
   01 = Position2 Error (position comparison failed)
   10 = Velocity2 Error (velocity comparison failed)
   11 = Position2 and Velocity2 Error (position and velocity comparison failed)
bit 3 Reserved: Transmitted as '0'
bit 2-0 General Error:
   000 = OK, no general error
   001 = Cannot Read Tape
   010 = Alignment error - too close
   011 = Alignment error - too far
   100 = Alignment error - too far left
   101 = Alignment error - too far right
   110 = Internal Fault
   111 = Communication Error
 -----------------------------------------------------------------------------*/
static void CheckFor_CEDESFault()
{
   static uint8_t counter_CEDESError;
   if( ( !Param_ReadValue_1Bit(enPARAM1__BypassTermLimits) )
    && ( GetOperation_ManualMode() != MODE_M__CONSTRUCTION ) )
   {
      if ( uwOfflineCounter_1ms >= CEDES_OFFLINE_LIMIT_1MS )
      {
         if( !Param_ReadValue_1Bit(enPARAM1__DisableCEDESOffline) )
         {
            SetFault(FLT__CEDES3_OFFLINE);
         }
      }
      else if ( stCEDES.ucError )
      {
         if(counter_CEDESError >= CEDES_ERROR_LIMIT)
         {
            enum en_cedes_error eError = SUBF_CEDES_FAULT__UNKNOWN;

            uint8_t ucPos1Error;
            uint8_t ucPos2Error;
            uint8_t ucGeneralError;

            if( Param_ReadValue_1Bit( enPARAM1__Enable_CEDES2 ) )
            {
              ucPos1Error = (stCEDES.ucError >> 4) & 0x1;
              ucPos2Error = (stCEDES.ucError >> 5) & 0x1;

              ucGeneralError = GetCEDES2_GeneralError();
            }
            else
            {
              ucPos1Error = (stCEDES.ucError >> 6) & 0x03;
              ucPos2Error = (stCEDES.ucError >> 4) & 0x03;
              ucGeneralError = stCEDES.ucError & 0x07;
            }

            if( ucGeneralError ) // General Error
            {
               eError = CEDES_ERROR__READ_FAIL + ucGeneralError - 1;
            }
            else if( ucPos1Error )
            {
               eError = CEDES_ERROR__CROSS1_POS + ucPos1Error - 1;
            }
            else if( ucPos2Error )
            {
               eError = CEDES_ERROR__CROSS2_POS + ucPos2Error - 1;
            }
            en_faults eFault = FLT__CEDES3_READ_FAIL + eError;
            if( eFault > FLT__CEDES3_CROSS2_BOTH )
            {
               eFault = FLT__CEDES3_CROSS2_BOTH; // TODO define unique
            }
            SetFault(eFault);
         }
         else
         {
            counter_CEDESError++;
         }
      }
      else
      {
         counter_CEDESError = 0;
      }
   }
}
/*-----------------------------------------------------------------------------
   Unload most recent element of the ring buffer.
   Discard the rest.
 -----------------------------------------------------------------------------*/
static uint8_t Receive_Packet1()
{
   Status bSuccess = ERROR;

   if( Param_ReadValue_1Bit( enPARAM1__Enable_CEDES2 ) )
   {
      while ( RingBuffer_GetCount( &RxCedesRingBuffer_0x2A ) > 0 )
      {
           RingBuffer_Pop( &RxCedesRingBuffer_0x2A, &stRxMsg );
           int iCount = RingBuffer_GetCount( &RxCedesRingBuffer_0x2A );
           if( iCount < 1 )
           {
             uint8_t aucData[8];
             /* TODO: Check CRC and Sequence Count */
             aucData[0] = stRxMsg.Data[0];
             aucData[1] = stRxMsg.Data[1] ;
             aucData[2] = stRxMsg.Data[2] ;
             aucData[3] = stRxMsg.Data[3] ;
             aucData[4] = stRxMsg.Data[4] ;
             aucData[5] = stRxMsg.Data[5] ;
             aucData[6] = stRxMsg.Data[6] ;
             aucData[7] = stRxMsg.Data[7] ;

             stCEDES.uiPosCount = aucData[3] | ( aucData[2] << 8 ) | ( aucData[1] << 16 );

             stCEDES.wVelocity = aucData[5] | ( aucData[4] << 8 );

             stCEDES.ucError = aucData[6];

             stCEDES.ucClip = aucData[7] & 0x3;

             bSuccess = SUCCESS;
             break;
           }
      }
   }

   else
   {
      while ( RingBuffer_GetCount( &RxCedesRingBuffer_0x21 ) > 0 )
      {
          RingBuffer_Pop( &RxCedesRingBuffer_0x21, &stRxMsg );
          int iCount = RingBuffer_GetCount( &RxCedesRingBuffer_0x21 );
          if( iCount < 1 )
          {
             uint8_t aucData[8];
             /* TODO: Check CRC and Sequence Count */
             aucData[0] = stRxMsg.Data[0];
             aucData[1] = stRxMsg.Data[1] ;
             aucData[2] = stRxMsg.Data[2] ;
             aucData[3] = stRxMsg.Data[3] ;
             aucData[4] = stRxMsg.Data[4] ;
             aucData[5] = stRxMsg.Data[5] ;
             aucData[6] = stRxMsg.Data[6] ;
             aucData[7] = stRxMsg.Data[7] ;

             stCEDES.uiPosCount = aucData[3] | ( aucData[2] << 8 ) | ( aucData[1] << 16 );

             stCEDES.wVelocity = aucData[5] | ( aucData[4] << 8 );

             bSuccess = SUCCESS;
             break;
          }
      }
   }

   return bSuccess;
}




/*-----------------------------------------------------------------------------
   Unload most recent element of the ring buffer.
   Discard the rest.
 -----------------------------------------------------------------------------*/
static uint8_t Unload_ForwardedPosition()
{
   Status bSuccess = ERROR;
   while ( RingBuffer_GetCount( &RxPositionRingBuffer ) > 0 )
   {
       RingBuffer_Pop( &RxPositionRingBuffer, &stRxMsg );
       int iCount = RingBuffer_GetCount( &RxPositionRingBuffer );
       if( iCount < 1 )
       {
          uint32_t uiPosition = stRxMsg.Data[0]
                              | (stRxMsg.Data[1] << 8)
                              | (stRxMsg.Data[2] << 16);
          int16_t wVelocity = (int16_t) stRxMsg.Data[4]
                                     | (stRxMsg.Data[5] << 8);
          SetPosition_PositionCount(uiPosition);
          SetPosition_Velocity(wVelocity);

          bSuccess = SUCCESS;
          break;
       }
   }
   return bSuccess;
}
/*-----------------------------------------------------------------------------
   Unload most recent element of the ring buffer.
   Discard the rest.
 -----------------------------------------------------------------------------*/
static uint8_t Receive_Packet2()
{
    Status bSuccess = ERROR;

   if( Param_ReadValue_1Bit( enPARAM1__Enable_CEDES2 ) )
   {
      while ( RingBuffer_GetCount( &RxCedesRingBuffer_0x2B ) > 0 )
      {
        RingBuffer_Pop( &RxCedesRingBuffer_0x2B, &stRxMsg );
        int iCount = RingBuffer_GetCount( &RxCedesRingBuffer_0x2B );
        if( iCount < 1 )
        {
          uint8_t aucData[8];
          /* TODO: Check CRC and Sequence Count */
          aucData[0] = stRxMsg.Data[0];
          aucData[1] = stRxMsg.Data[1] ;
          aucData[2] = stRxMsg.Data[2] ;
          aucData[3] = stRxMsg.Data[3] ;
          aucData[4] = stRxMsg.Data[4] ;
          aucData[5] = stRxMsg.Data[5] ;
          aucData[6] = stRxMsg.Data[6] ;
          aucData[7] = stRxMsg.Data[7] ;

          stCEDES.ucClipOffset = aucData[3];

          bSuccess = SUCCESS;
          break;
        }
      }
   }

   else
   {
      while ( RingBuffer_GetCount( &RxCedesRingBuffer_0x22 ) > 0 )
      {
         RingBuffer_Pop( &RxCedesRingBuffer_0x22, &stRxMsg );
         int iCount = RingBuffer_GetCount( &RxCedesRingBuffer_0x22 );
         if( iCount < 1 )
         {
            uint8_t aucData[8];
            /* TODO: Check CRC and Sequence Count */
            aucData[0] = stRxMsg.Data[0];
            aucData[1] = stRxMsg.Data[1] ;
            aucData[2] = stRxMsg.Data[2] ;
            aucData[3] = stRxMsg.Data[3] ;
            aucData[4] = stRxMsg.Data[4] ;
            aucData[5] = stRxMsg.Data[5] ;
            aucData[6] = stRxMsg.Data[6] ;
            aucData[7] = stRxMsg.Data[7] ;

            stCEDES.ucError = aucData[1];

            stCEDES.uwStatus = aucData[3] | ( aucData[2] << 8 );

            bSuccess = SUCCESS;
            break;
         }
      }
   }

   return bSuccess;
}
/*-----------------------------------------------------------------------------
   Unload cedes packet 1
 -----------------------------------------------------------------------------*/
static void Unload_CEDESPacket1()
{
   static int16_t lastGoodVelocity;
   if(stCEDES.wVelocity != CEDES_INVALID_VELOCITY)
   {
      lastGoodVelocity = stCEDES.wVelocity;
   }
   int16_t wVelocity = (int16_t)(  lastGoodVelocity / CEDES_SPEED_TO_FPM );
   ETSL_SetSpeed( wVelocity );

   static uint32_t lastGoodPosition;
   if( ( stCEDES.uiPosCount != CEDES_INVALID_POSITION ) )
   {
      lastGoodPosition = stCEDES.uiPosCount;
   }
   // This prevents lastGoodPosition from underflow. This was an issue when the system resets.
   uint32_t uiPositionCount = (lastGoodPosition <=CHANNEL_POS_COUNT_OFFSET ) ? lastGoodPosition : lastGoodPosition - CHANNEL_POS_COUNT_OFFSET;
   uint32_t uiLastPosition = GetPosition_PositionCount();
   if(uiLastPosition != uiPositionCount)
   {
      ETSL_SetPosition(uiPositionCount);
   }
}

/*-----------------------------------------------------------------------------
   Forward position to B processor
 -----------------------------------------------------------------------------*/
static void Load_ForwardedPosition()
{
   static uint16_t uwDelayCounter;
   /* Reduce transmission rate of position packets to B processor */
   if( ++uwDelayCounter >= DELAY_POS_FWD_TO_B_5MS )
   {
      uwDelayCounter = 0;
      uint32_t uiNewPosition = GetPosition_PositionCount();
      int16_t wNewVelocity = GetPosition_Velocity();
      stTxMsg.DLC = 8;
      stTxMsg.ID = POS_FWD_MSG_ID;
      stTxMsg.Data[0] = uiNewPosition & 0xFF;
      stTxMsg.Data[1] = (uiNewPosition >> 8) & 0xFF;
      stTxMsg.Data[2] = (uiNewPosition >> 16) & 0xFF;

      stTxMsg.Data[4] = wNewVelocity & 0xFF;
      stTxMsg.Data[5] = (wNewVelocity >> 8) & 0xFF;
      UART_LoadCANMessage(&stTxMsg);
   }
}
/*-----------------------------------------------------------------------------
   Receives data from the ETSL CEDES camera
 -----------------------------------------------------------------------------*/
static void Update_CEDES4()
{
   uint8_t bPacketReceived = 0;
   if(Receive_Packet1())
   {
      bPacketReceived = 1;
      Unload_CEDESPacket1();
   }
   if(Receive_Packet2())
   {
      bPacketReceived = 1;
   }

   if(bPacketReceived)
   {
      uwOfflineCounter_1ms = 0;
   }
   else if( uwOfflineCounter_1ms < CEDES_OFFLINE_LIMIT_1MS )
   {
      uwOfflineCounter_1ms += MOD_RUN_PERIOD_POSITION_1MS;
   }
   CheckFor_CEDESFault();
}
/*-----------------------------------------------------------------------------
   Unloads position an forwards to B processor
 -----------------------------------------------------------------------------*/
void Position_COPA()
{
   if(Param_ReadValue_1Bit(enPARAM1__Enable_ETSL))
   {
      Update_CEDES4();
   }

   if(Unload_ForwardedPosition())
   {
      Load_ForwardedPosition();
   }
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
