/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "operation.h"
#include "motion.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Preflight = { .pfnInit = Init, .pfnRun = Run, };

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
    Preflight_Unknown,
    Preflight_Idle,
    Preflight_Waiting,
    Preflight_Start,
    Preflight_WaitForFPGA_Awk,
    Preflight_TurnOff120VAC,
    Preflight_TurnOff3VDC,
    Preflight_CheckInputs,
    Preflight_TurnOn120VAC,
    Preflight_TurnOn3VDC,
    Preflight_Finished

} Preflight_States;
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static Preflight_States currentState = Preflight_Unknown;
static Preflight_States nextState = Preflight_Idle;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/

/*
 * Sets up function for this module.
 * This module has a initial delay of 10ms and a run time delay of 5ms.
 * The function is responsible for setting up the GPIO pins for "bit bang spi"
 */
static uint32_t Init( struct st_module *pstThisModule )
{
    pstThisModule->uwInitialDelay_1ms = 10;
    pstThisModule->uwRunPeriod_1ms = 50;

    return 0;
}

/*-----------------------------------------------------------------------------

 This module will be used to communicate with the FPGA via SPI or I2C and
 read all of its discrete inputs into gauiFPGA_RedundantInputs[]
 These inputs will be compared against the MCU D's discrete inputs in the
 safety module.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{

    return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
