/******************************************************************************
 *
 * @file     mod_param_master.c
 * @brief
 * @version  V1.00
 * @date     13, April 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "mod.h"
#include "sru_a.h"
#include "sys.h"
#include "motion.h"
#include "config_System.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_ParamMaster_CTA =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

enum en_mod_param_states
{
   enPSTATE__AWAITING_VALID_RAM_COPY,
   enPSTATE__RUNNING,
};

// These are all the networks that the master parameter module talks on
// directly.
enum enum_master_param_nets
{
   MASTER_PARAM_NET__MRA_MRB,
   MASTER_PARAM_NET__CAR_NET,

   NUM_MASTER_PARAM_NETS
};

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint8_t uiCounter;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
 If board reset fault detected, resend all CRC datagrams
 -----------------------------------------------------------------------------*/
static void CheckFor_ResendCRCDatagrams()
{
   static uint8_t bLastState = 0;
   uint8_t bResetFaultDetected = GetFault_ByRange(FLT__BOARD_RESET_MRA, FLT__BOARD_RESET_COPB)
                              || GetFault_ByRange(FLT__WDT_RESET_MRA, FLT__WDT_RESET_COPB)
                              || GetFault_ByRange(FLT__BOD_RESET_MRA, FLT__BOD_RESET_COPB);

   // On rising edge, resend all crcs
   if( !bLastState && bResetFaultDetected )
   {
      for( uint8_t i = 0; i < NUM_PARAM_BLOCKS; i++ )
      {
         SDATA_DirtyBit_Set( &gstSData_LocalData,
                             DG_MRA__ParamMaster_BlockCRCs1 + i );
      }
   }

   bLastState = bResetFaultDetected;
}
/*-----------------------------------------------------------------------------
 Returns 1 if parameter updates are allowed
 -----------------------------------------------------------------------------*/
static uint8_t CheckIf_ValidParameterUpdateState()
{
   uint8_t bValid = 0;
   if( !GetMotion_RunFlag()
    && !SRU_Read_DIP_Switch( enSRU_DIP_A1 ) )
   {
      bValid = 1;
   }
   return bValid;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadSlaveRequest_MRB( struct st_param_control *pstParamControl )
{
   struct st_param_and_value stParamAndValue;
   enum en_param_nodes eNode = enPARAM_NODE__MRB;
   enum en_param_datagram_message_types enParamDatagramType = GetParamSlaveRequest_MessageType_MRB();

   switch ( enParamDatagramType )
   {
      case enPARAM_DG__REQ_TO_SET_A_PARAM:  // request to set a parameter's value
         {
            uint8_t ucBlock_Plus1 = GetParamSlaveRequest_BlockIndex_MRB();
            uint16_t uwParam_Plus1 = GetParamSlaveRequest_ParameterIndex_MRB();
            if( ucBlock_Plus1
             && uwParam_Plus1 )
            {
               stParamAndValue.ucBlockIndex = ucBlock_Plus1 - 1;
               stParamAndValue.uwParamIndex = uwParam_Plus1 - 1;
               stParamAndValue.uiValue = GetParamSlaveRequest_ParamValue_MRB();
               Param_WriteValue( &stParamAndValue );
            }
         }
         break;

      case enPARAM_DG__REQ_FOR_PARAM_CHUNK:
         {
            uint8_t ucBlock_Plus1 = GetParamSlaveRequest_BlockIndex_MRB();
            uint8_t ucChunk_Plus1 = GetParamSlaveRequest_ChunkIndex_MRB();
            uint8_t ucNode = GetParamSlaveRequest_NodeID_MRB();
            if( (ucNode == eNode)
              && ucBlock_Plus1
              && ucChunk_Plus1  )
            {
               if( Sys_ParamChunk_IsValid(ucBlock_Plus1 - 1, ucChunk_Plus1 - 1) )
               {
                  struct st_param_chunk_req *pstSlaveRequestedChunk;
                  pstSlaveRequestedChunk = &(pstParamControl->astSlaveRequestedChunk[ eNode ]);
                  pstSlaveRequestedChunk->ucBlock_Plus1 = ucBlock_Plus1;
                  pstSlaveRequestedChunk->ucChunk_Plus1 = ucChunk_Plus1;
                  pstSlaveRequestedChunk->ucCountdown = PARAM_CHUNK_REQ_COUNTDOWN;
               }
            }
         }
         break;
      default:  // assume enPARAM_DG__NULL -- no action requested, no data being sent
         break;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadSlaveRequest_CTA( struct st_param_control *pstParamControl )
{
   struct st_param_and_value stParamAndValue;
   enum en_param_nodes eNode = enPARAM_NODE__CTA;
   enum en_param_datagram_message_types enParamDatagramType = GetParamSlaveRequest_MessageType_CTA();

   switch ( enParamDatagramType )
   {
      case enPARAM_DG__REQ_TO_SET_A_PARAM:  // request to set a parameter's value
         {
            uint8_t ucBlock_Plus1 = GetParamSlaveRequest_BlockIndex_CTA();
            uint16_t uwParam_Plus1 = GetParamSlaveRequest_ParameterIndex_CTA();
            if( ucBlock_Plus1
             && uwParam_Plus1 )
            {
               stParamAndValue.ucBlockIndex = ucBlock_Plus1 - 1;
               stParamAndValue.uwParamIndex = uwParam_Plus1 - 1;
               stParamAndValue.uiValue = GetParamSlaveRequest_ParamValue_CTA();
               Param_WriteValue( &stParamAndValue );
            }
         }
         break;

      case enPARAM_DG__REQ_FOR_PARAM_CHUNK:
         {
            uint8_t ucBlock_Plus1 = GetParamSlaveRequest_BlockIndex_CTA();
            uint8_t ucChunk_Plus1 = GetParamSlaveRequest_ChunkIndex_CTA();
            uint8_t ucNode = GetParamSlaveRequest_NodeID_CTA();
            if( (ucNode == eNode)
              && ucBlock_Plus1
              && ( ucBlock_Plus1 < ( NUM_PARAM_BLOCKS+1 ) )
              && ucChunk_Plus1
              && ( ucChunk_Plus1 < ( NUM_OF_CHUNKS_PER_BLOCK+1 ) ) )
            {
               struct st_param_chunk_req *pstSlaveRequestedChunk;
               pstSlaveRequestedChunk = &(pstParamControl->astSlaveRequestedChunk[ eNode ]);
               pstSlaveRequestedChunk->ucBlock_Plus1 = ucBlock_Plus1;
               pstSlaveRequestedChunk->ucChunk_Plus1 = ucChunk_Plus1;
               pstSlaveRequestedChunk->ucCountdown = PARAM_CHUNK_REQ_COUNTDOWN;
            }
         }
         break;
      default:  // assume enPARAM_DG__NULL -- no action requested, no data being sent
         break;
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadSlaveRequest_CTB( struct st_param_control *pstParamControl )
{
   struct st_param_and_value stParamAndValue;
   enum en_param_nodes eNode = enPARAM_NODE__CTB;
   enum en_param_datagram_message_types enParamDatagramType = GetParamSlaveRequest_MessageType_CTB();

   switch ( enParamDatagramType )
   {
      case enPARAM_DG__REQ_TO_SET_A_PARAM:  // request to set a parameter's value
         {
            uint8_t ucBlock_Plus1 = GetParamSlaveRequest_BlockIndex_CTB();
            uint16_t uwParam_Plus1 = GetParamSlaveRequest_ParameterIndex_CTB();
            if( ucBlock_Plus1
             && uwParam_Plus1 )
            {
               stParamAndValue.ucBlockIndex = ucBlock_Plus1 - 1;
               stParamAndValue.uwParamIndex = uwParam_Plus1 - 1;
               stParamAndValue.uiValue = GetParamSlaveRequest_ParamValue_CTB();
               Param_WriteValue( &stParamAndValue );
            }
         }
         break;

      case enPARAM_DG__REQ_FOR_PARAM_CHUNK:
         {
            uint8_t ucBlock_Plus1 = GetParamSlaveRequest_BlockIndex_CTB();
            uint8_t ucChunk_Plus1 = GetParamSlaveRequest_ChunkIndex_CTB();
            uint8_t ucNode = GetParamSlaveRequest_NodeID_CTB();
            if( (ucNode == eNode)
              && ucBlock_Plus1
              && ucChunk_Plus1  )
            {
               if( Sys_ParamChunk_IsValid(ucBlock_Plus1 - 1, ucChunk_Plus1 - 1) )
               {
                  struct st_param_chunk_req *pstSlaveRequestedChunk;
                  pstSlaveRequestedChunk = &(pstParamControl->astSlaveRequestedChunk[ eNode ]);
                  pstSlaveRequestedChunk->ucBlock_Plus1 = ucBlock_Plus1;
                  pstSlaveRequestedChunk->ucChunk_Plus1 = ucChunk_Plus1;
                  pstSlaveRequestedChunk->ucCountdown = PARAM_CHUNK_REQ_COUNTDOWN;
               }
            }
         }
         break;
      default:  // assume enPARAM_DG__NULL -- no action requested, no data being sent
         break;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadSlaveRequest_COPA( struct st_param_control *pstParamControl )
{
   struct st_param_and_value stParamAndValue;
   enum en_param_nodes eNode = enPARAM_NODE__COPA;
   enum en_param_datagram_message_types enParamDatagramType = GetParamSlaveRequest_MessageType_COPA();

   switch ( enParamDatagramType )
   {
      case enPARAM_DG__REQ_TO_SET_A_PARAM:  // request to set a parameter's value
         {
            uint8_t ucBlock_Plus1 = GetParamSlaveRequest_BlockIndex_COPA();
            uint16_t uwParam_Plus1 = GetParamSlaveRequest_ParameterIndex_COPA();
            if( ucBlock_Plus1
             && uwParam_Plus1 )
            {
               stParamAndValue.ucBlockIndex = ucBlock_Plus1 - 1;
               stParamAndValue.uwParamIndex = uwParam_Plus1 - 1;
               stParamAndValue.uiValue = GetParamSlaveRequest_ParamValue_COPA();
               Param_WriteValue( &stParamAndValue );
            }
         }
         break;

      case enPARAM_DG__REQ_FOR_PARAM_CHUNK:
         {
            uint8_t ucBlock_Plus1 = GetParamSlaveRequest_BlockIndex_COPA();
            uint8_t ucChunk_Plus1 = GetParamSlaveRequest_ChunkIndex_COPA();
            uint8_t ucNode = GetParamSlaveRequest_NodeID_COPA();
            if( (ucNode == eNode)
              && ucBlock_Plus1
              && ucChunk_Plus1  )
            {
               if( Sys_ParamChunk_IsValid(ucBlock_Plus1 - 1, ucChunk_Plus1 - 1) )
               {
                  struct st_param_chunk_req *pstSlaveRequestedChunk;
                  pstSlaveRequestedChunk = &(pstParamControl->astSlaveRequestedChunk[ eNode ]);
                  pstSlaveRequestedChunk->ucBlock_Plus1 = ucBlock_Plus1;
                  pstSlaveRequestedChunk->ucChunk_Plus1 = ucChunk_Plus1;
                  pstSlaveRequestedChunk->ucCountdown = PARAM_CHUNK_REQ_COUNTDOWN;
               }
            }
         }
         break;
      default:  // assume enPARAM_DG__NULL -- no action requested, no data being sent
         break;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadSlaveRequest_COPB( struct st_param_control *pstParamControl )
{
   struct st_param_and_value stParamAndValue;
   enum en_param_nodes eNode = enPARAM_NODE__COPB;
   enum en_param_datagram_message_types enParamDatagramType = GetParamSlaveRequest_MessageType_COPB();

   switch ( enParamDatagramType )
   {
      case enPARAM_DG__REQ_TO_SET_A_PARAM:  // request to set a parameter's value
         {
            uint8_t ucBlock_Plus1 = GetParamSlaveRequest_BlockIndex_COPB();
            uint16_t uwParam_Plus1 = GetParamSlaveRequest_ParameterIndex_COPB();
            if( ucBlock_Plus1
             && uwParam_Plus1 )
            {
               stParamAndValue.ucBlockIndex = ucBlock_Plus1 - 1;
               stParamAndValue.uwParamIndex = uwParam_Plus1 - 1;
               stParamAndValue.uiValue = GetParamSlaveRequest_ParamValue_COPB();
               Param_WriteValue( &stParamAndValue );
            }
         }
         break;

      case enPARAM_DG__REQ_FOR_PARAM_CHUNK:
         {
            uint8_t ucBlock_Plus1 = GetParamSlaveRequest_BlockIndex_COPB();
            uint8_t ucChunk_Plus1 = GetParamSlaveRequest_ChunkIndex_COPB();
            uint8_t ucNode = GetParamSlaveRequest_NodeID_COPB();
            if( (ucNode == eNode)
              && ucBlock_Plus1
              && ucChunk_Plus1  )
            {
               if( Sys_ParamChunk_IsValid(ucBlock_Plus1 - 1, ucChunk_Plus1 - 1) )
               {
                  struct st_param_chunk_req *pstSlaveRequestedChunk;
                  pstSlaveRequestedChunk = &(pstParamControl->astSlaveRequestedChunk[ eNode ]);
                  pstSlaveRequestedChunk->ucBlock_Plus1 = ucBlock_Plus1;
                  pstSlaveRequestedChunk->ucChunk_Plus1 = ucChunk_Plus1;
                  pstSlaveRequestedChunk->ucCountdown = PARAM_CHUNK_REQ_COUNTDOWN;
               }
            }
         }
         break;
      default:  // assume enPARAM_DG__NULL -- no action requested, no data being sent
         break;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadDatagram__ParamSlaveRequest( struct st_param_control *pstParamControl )
{
   UnloadSlaveRequest_MRB(pstParamControl);
   UnloadSlaveRequest_CTA(pstParamControl);
   UnloadSlaveRequest_CTB(pstParamControl);
   UnloadSlaveRequest_COPA(pstParamControl);
   UnloadSlaveRequest_COPB(pstParamControl);
}

/*-----------------------------------------------------------------------------
   Each Block's CRC will be sent 5 times.
   Then the next changed CRC will be sent.
 -----------------------------------------------------------------------------*/
static void LoadDatagram__ParamMaster_BlockCRCs( struct st_param_control *pstParamControl )
{
   static const uint32_t uiDatagramIndex = DG_MRA__ParamMaster_BlockCRCs1;
   static uint8_t aucResendCountdowns[ NUM_PARAM_BLOCKS ];
   struct st_sdata_control *pstSData_Control = &gstSData_LocalData;
   un_sdata_datagram unDatagram;


   if( pstSData_Control )
   {
      for( uint8_t i = 0; i < NUM_PARAM_BLOCKS; i++ )
      {
         int iOldDirtyBit = SDATA_DirtyBit_Get( pstSData_Control, uiDatagramIndex + i );
         uint32_t ulCRC = Param_GetBlockCRC_RAM(i);
         unDatagram.auc8[0] = enPARAM_DG__SENDING_BLOCK_CRC;
         unDatagram.auc8[1] = i + 1;
         unDatagram.auw16[1] = 0;
         unDatagram.aui32[1] = ulCRC;
         SDATA_WriteDatagram( pstSData_Control, uiDatagramIndex + i, &unDatagram );


         //If CRC has changed, send a few times
         if( !iOldDirtyBit && SDATA_DirtyBit_Get( pstSData_Control, uiDatagramIndex + i ) )
         {
            aucResendCountdowns[ i ] = NUM_TIMES_TO_RESEND_PARAM_CRC;
         }

         if( aucResendCountdowns[ i ] )
         {
            aucResendCountdowns[ i ]--;
            SDATA_DirtyBit_Set( pstSData_Control, uiDatagramIndex + i );
         }
      }
   }
}

/*-----------------------------------------------------------------------------
   Master will load values only after receiving a chunk request from a slave.
 -----------------------------------------------------------------------------*/
static void LoadDatagram__ParamValues_ByNode( struct st_param_control *pstParamControl, enum en_param_nodes eNode )
{
   struct st_sdata_control *pstSData_Control = &gstSData_LocalData;
   static const uint32_t auiDatagramIndex[NUM_PARAM_NODES] =
   {
         NUM_MRA_DATAGRAMS,//enPARAM_NODE__TRACTION
         NUM_MRA_DATAGRAMS,//enPARAM_NODE__MRA
         DG_MRA__ParamMaster_ParamValues2,//enPARAM_NODE__MRB
         DG_MRA__ParamMaster_ParamValues3,//enPARAM_NODE__CTA
         DG_MRA__ParamMaster_ParamValues4,//enPARAM_NODE__CTB
         DG_MRA__ParamMaster_ParamValues5,//enPARAM_NODE__COPA
         DG_MRA__ParamMaster_ParamValues6,//enPARAM_NODE__COPB
   };

   if( ( eNode >= enPARAM_NODE__MRB )
    && ( eNode <= enPARAM_NODE__COPB ) )
   {
      if( !SDATA_DirtyBit_Get(pstSData_Control, auiDatagramIndex[ eNode ] ) )
      {
         un_sdata_datagram unDatagram;
         struct st_param_chunk_req *pstRequestedChunk = &pstParamControl->astSlaveRequestedChunk[ eNode ];
         if( pstRequestedChunk->ucBlock_Plus1
          && pstRequestedChunk->ucChunk_Plus1
          && pstRequestedChunk->ucCountdown )
         {
            pstRequestedChunk->ucCountdown--;
            unDatagram.auc8[ 0 ] = enPARAM_DG__SENDING_PARAM_CHUNK;
            unDatagram.auc8[ 1 ] = pstRequestedChunk->ucBlock_Plus1;
            unDatagram.auc8[ 2 ] = pstRequestedChunk->ucChunk_Plus1;
            unDatagram.auc8[ 3 ] = eNode;
            unDatagram.aui32[ 1 ] = Param_ReadChunk( pstRequestedChunk->ucBlock_Plus1 - 1,
                                                     pstRequestedChunk->ucChunk_Plus1 - 1 );

            SDATA_WriteDatagram( pstSData_Control,
                                 auiDatagramIndex[ eNode ],
                                 &unDatagram
                               );
            SDATA_DirtyBit_Set( pstSData_Control,
                                auiDatagramIndex[ eNode ] );

         }
         else
         {
            pstRequestedChunk->ucBlock_Plus1 = 0;
            pstRequestedChunk->ucChunk_Plus1 = 0;
            unDatagram.aui32[0] = 0;
            unDatagram.aui32[1] = 0;
            SDATA_WriteDatagram( pstSData_Control,
                                 auiDatagramIndex[ eNode ],
                                 &unDatagram
                               );
         }
      }
   }
}

/*-----------------------------------------------------------------------------
   Master will load values only after receiving a chunk request from a slave.

 -----------------------------------------------------------------------------*/
static void LoadDatagram__ParamValues( struct st_param_control *pstParamControl )
{
   for( enum en_param_nodes eNode = enPARAM_NODE__MRB; eNode < NUM_PARAM_NODES ; eNode++ )
   {
      LoadDatagram__ParamValues_ByNode( pstParamControl, eNode );
   }
}

/*-----------------------------------------------------------------------------

   Clear send requests for parameter datagrams

 -----------------------------------------------------------------------------*/
static void ClearParamUpdateRequests()
{
   for( uint8_t i = 0; i < NUM_PARAM_BLOCKS; i++ )
   {
      SDATA_DirtyBit_Clr( &gstSData_LocalData,
                           DG_MRA__ParamMaster_BlockCRCs1 + i);
   }

   SDATA_DirtyBit_Clr( &gstSData_LocalData,
                        DG_MRA__ParamMaster_ParamValues2);
   SDATA_DirtyBit_Clr( &gstSData_LocalData,
                        DG_MRA__ParamMaster_ParamValues3);
   SDATA_DirtyBit_Clr( &gstSData_LocalData,
                        DG_MRA__ParamMaster_ParamValues4);
   SDATA_DirtyBit_Clr( &gstSData_LocalData,
                        DG_MRA__ParamMaster_ParamValues5);
   SDATA_DirtyBit_Clr( &gstSData_LocalData,
                        DG_MRA__ParamMaster_ParamValues6);
}

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   gstParamControl.ucParamNodeType = enPARAM_NODE__MRA;
   gstParamControl.pstParamBlockInfo = &gstSys_ParamBlockInfo;

   uiCounter = 100;
   
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = 20;
   return 0;
}

/*-----------------------------------------------------------------------------

   The EEPROMI can take up to 3 ms to write a page so don't call Run() more
   often than that and never do more than one write operation per call.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static uint8_t ucState = enPSTATE__AWAITING_VALID_RAM_COPY;
   struct st_param_control *pstParamControl = &gstParamControl;
   switch( ucState )
   {
      case enPSTATE__RUNNING:

         gstParamControl.bMaster = 1;

         if( !CheckIf_ValidParameterUpdateState() )
         {
            ClearParamUpdateRequests();
         }
         else
         {
            CheckFor_ResendCRCDatagrams();
            UnloadDatagram__ParamSlaveRequest( pstParamControl );

            LoadDatagram__ParamMaster_BlockCRCs( pstParamControl );
            LoadDatagram__ParamValues( pstParamControl );
         }
            

      break;

      default:  // enPSTATE__AWAITING_VALID_RAM_COPY
         if ( pstParamControl->bRAM_CopyValid )
         {

            if( GetOperation4_ParamSync() )
            {
               //gstParamControl.bMaster = 1;
               Param_SetMaster();
               gstParamControl.ucParamNodeType = enPARAM_NODE__MRA;
               ucState = enPSTATE__RUNNING;
            }
            else
            {
               gstParamControl.bMaster = 0;
               gstParamControl.ucParamNodeType = enPARAM_NODE__CTA;
            }
         }
         break;
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

