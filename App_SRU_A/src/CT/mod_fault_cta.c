/******************************************************************************
 *
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sys.h"

#include "GlobalData.h"
#include <stdint.h>
#include <string.h>
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Locally determine the nearest floor index
 -----------------------------------------------------------------------------*/
static uint8_t GetFloorIndex( uint32_t ulPosCount )
{
   uint8_t ucFloor = INVALID_FLOOR;

   if (ulPosCount)
   {
      for (uint8_t i = 0; i < GetFP_NumFloors(); i++ )
      {
         if ( ulPosCount < Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0+i) )
         {
            ucFloor = i;
            if ( ucFloor )
            {
               uint32_t uwHalfway = ((Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0+i) - Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0+i-1))/2) + Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0+i-1);
               if (ulPosCount < uwHalfway)
               {
                  ucFloor--;
               }
            }
            break;
         } else if ( (i+1) >= GetFP_NumFloors() ) {
            ucFloor = i;
         }
      }
   }

   return ucFloor;
}
/*-----------------------------------------------------------------------------
   Checks for invalid critical parameter fault
   Set when a critical parameter has been changed:
      Number of floors
      Rear Doors
      Contract speed
      Drive Select
 -----------------------------------------------------------------------------*/
static void CheckFor_NeedToReset()
{
   static uint8_t bFirst = 1;
   static uint8_t ucDriveSelect;
   static uint32_t auiOpeningF[BITMAP32_SIZE(MAX_NUM_FLOORS)];
   static uint32_t auiOpeningR[BITMAP32_SIZE(MAX_NUM_FLOORS)];
   static uint8_t bJanus;
   static uint8_t bDL20;
   if(bFirst)
   {
      bFirst = 0;
      ucDriveSelect = Param_ReadValue_8Bit(enPARAM8__DriveSelect);
      for(uint8_t i = 0; i < BITMAP32_SIZE(MAX_NUM_FLOORS); i++)
      {
         auiOpeningF[i] = Param_ReadValue_32Bit( enPARAM32__OpeningBitmapF_0 + i );
         auiOpeningR[i] = Param_ReadValue_32Bit( enPARAM32__OpeningBitmapR_0 + i );
      }
      bJanus = Param_ReadValue_1Bit(enPARAM1__Enable_Janus_RS_Fixture);
      bDL20 = Param_ReadValue_1Bit(enPARAM1__EnableDL20_CT) || Param_ReadValue_1Bit(enPARAM1__EnableDL20_COP);
   }
   else if( !GetMotion_RunFlag() )
   {
      if(GetFP_ContractSpeed() != Param_ReadValue_16Bit(enPARAM16__ContractSpeed))
      {
         /* Exception for Acceptance Test */
         if( GetOperation_AutoMode()  != MODE_A__TEST )
         {
            SetFault(FLT__NEED_TO_CYCLE_PWR_CT);
         }
      }
      else if(ucDriveSelect != Param_ReadValue_8Bit(enPARAM8__DriveSelect))
      {
         SetFault(FLT__NEED_TO_CYCLE_PWR_CT);
      }
      else if( GetOperation_ManualMode() != MODE_M__CONSTRUCTION )
      {
         if(GetFP_RearDoors() != Param_ReadValue_1Bit(enPARAM1__NumDoors))
         {
            SetFault(FLT__NEED_TO_CYCLE_PWR_CT);
         }
         else if(GetFP_FreightDoors() != Param_ReadValue_1Bit(enPARAM1__Enable_Freight_Doors))
         {
            SetFault(FLT__NEED_TO_CYCLE_PWR_CT);
         }
         else if(GetFP_NumFloors() != Param_ReadValue_8Bit(enPARAM8__NumFloors))
         {
            SetFault(FLT__NEED_TO_CYCLE_PWR_CT);
         }
         else if(bJanus != Param_ReadValue_1Bit(enPARAM1__Enable_Janus_RS_Fixture))
         {
            SetFault(FLT__NEED_TO_CYCLE_PWR_CT);
         }
         else if(bDL20 != ( Param_ReadValue_1Bit(enPARAM1__EnableDL20_CT) || Param_ReadValue_1Bit(enPARAM1__EnableDL20_COP) ))
         {
            SetFault(FLT__NEED_TO_CYCLE_PWR_CT);
         }
         else
         {
            for(uint8_t i = 0; i < BITMAP32_SIZE(GetFP_NumFloors()); i++)
            {
               if(auiOpeningF[i] != Param_ReadValue_32Bit( enPARAM32__OpeningBitmapF_0 + i ) )
               {
                  SetFault(FLT__NEED_TO_CYCLE_PWR_CT);
                  break;
               }
               else if( GetFP_RearDoors() && ( auiOpeningR[i] != Param_ReadValue_32Bit( enPARAM32__OpeningBitmapR_0 + i ) ) )
               {
                  SetFault(FLT__NEED_TO_CYCLE_PWR_CT);
                  break;
               }
            }
         }
      }
   }
}
/*-----------------------------------------------------------------------------
   Check for doorzone stuck high

   If for 3 sec...:
      - DZ is high &
      - In Auto Operation &
      - 6 inches outside learned floor

 -----------------------------------------------------------------------------*/
#define TIMEOUT_DZ_STUCK_HIGH_1MS     (3000)
#define TESTING_TIMEOUT_DZ_STUCK_HIGH_1MS     (1500)
static void CheckFor_DoorZoneStuckHigh()
{
   if( Param_ReadValue_1Bit( enPARAM1__DzStuckHighTesting ) )
   {
      static uint16_t uwCounter_1ms;
      uint8_t ucCurrentFloor = GetFloorIndex( GetPosition_PositionCount() );
      if( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
       && ( ucCurrentFloor < GetFP_NumFloors() )
       && ( SRU_Read_Input(enSRU_Input_503) ||  // enIN_DZ_F
          ( GetFP_RearDoors() && SRU_Read_Input(enSRU_Input_504) ) ) ) // enIN_DZ_R
      {
         uint8_t bInvalidDZ = 0;
         uint32_t uiFloorPos = Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 + ucCurrentFloor );
         uint32_t uiUpperLimit = uiFloorPos + SIX_INCHES;
         uint32_t uiLowerLimit = uiFloorPos - SIX_INCHES;
         uint32_t uiCurrentPos = GetPosition_PositionCount();

         if( ( uiCurrentPos > uiUpperLimit )
          || ( uiCurrentPos < uiLowerLimit ) )
         {
            bInvalidDZ = 1;
         }

         if(bInvalidDZ)
         {
            if( uwCounter_1ms >= TESTING_TIMEOUT_DZ_STUCK_HIGH_1MS)
            {
               SetFault(FLT__DZ_STUCK_HI);
            }
            else
            {
               uwCounter_1ms += MOD_RUN_PERIOD_FAULT_1MS;
            }
         }
         else
         {
            uwCounter_1ms = 0;
         }
      }
      else
      {
         uwCounter_1ms = 0;
      }
   }
   else
   {
      static uint16_t uwCounter_1ms;

         if( ( GetOperation_ClassOfOp() == CLASSOP__AUTO )
          && ( GetOperation_CurrentFloor() < GetFP_NumFloors() )
          && ( SRU_Read_Input(enSRU_Input_503) ||  // enIN_DZ_F
             ( GetFP_RearDoors() && SRU_Read_Input(enSRU_Input_504) ) ) ) // enIN_DZ_R
         {
            uint8_t bInvalidDZ = 0;
            uint32_t uiFloorPos = Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 + GetOperation_CurrentFloor() );
            uint32_t uiUpperLimit = uiFloorPos + SIX_INCHES;
            uint32_t uiLowerLimit = uiFloorPos - SIX_INCHES;
            uint32_t uiCurrentPos = GetPosition_PositionCount();

            if( ( uiCurrentPos > uiUpperLimit )
             || ( uiCurrentPos < uiLowerLimit ) )
            {
               bInvalidDZ = 1;
            }

            if(bInvalidDZ)
            {
               if( uwCounter_1ms >= TIMEOUT_DZ_STUCK_HIGH_1MS)
               {
                  SetFault(FLT__DZ_STUCK_HI);
               }
               else
               {
                  uwCounter_1ms += MOD_RUN_PERIOD_FAULT_1MS;
               }
            }
            else
            {
               uwCounter_1ms = 0;
            }
         }
         else
         {
            uwCounter_1ms = 0;
         }
   }
}
/*----------------------------------------------------------------------------

    Called from mod_fault.c

 *----------------------------------------------------------------------------*/
void Run_ModFault_CTA( void )
{
   CheckFor_DoorZoneStuckHigh();

   CheckFor_NeedToReset();

   UpdateActiveFaults();
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
