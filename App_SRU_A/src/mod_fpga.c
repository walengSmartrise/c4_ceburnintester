/******************************************************************************
 *
 * @file     mod_heartbeat.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include <chip.h>
#include <stdint.h>
#include "sys.h"
#include "fpga_api.h"

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_FPGA =
{
   .pfnInit = Init,
   .pfnRun  = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
#define PREFLIGHT_TIMEOUT_DELAY_1MS    (4000)
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static FPGA_Packet fpgaData;
static uint16_t uwOfflineCounter_ms;
static en_fpga_locations eLocation;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void CheckFor_PreflightFault()
{
   static uint32_t uiPreflightTimeout_ms;
   uint8_t bPreflightFlag = GetPreflightFlag();
   PreflightStatus eStatus = FPGA_GetPreflightStatus(eLocation);
   if(GetPreflightFlag())
   {
      if( eStatus == PREFLIGHT_STATUS__FAIL )
      {
         if( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT )
         {
            SetFault(FLT__PREFLIGHT_CT);
         }
         else if( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP )
         {
            SetFault(FLT__PREFLIGHT_COP);
         }
      }
      else if( eStatus == PREFLIGHT_STATUS__INACTIVE )
      {
         if( uiPreflightTimeout_ms >= PREFLIGHT_TIMEOUT_DELAY_1MS )
         {
            if( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT )
            {
               SetFault(FLT__PREFLIGHT_CT);
            }
            else if( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP )
            {
               SetFault(FLT__PREFLIGHT_COP);
            }
         }
         else
         {
            uiPreflightTimeout_ms += MOD_RUN_PERIOD_FPGA_1MS;
         }
      }
      else
      {
         uiPreflightTimeout_ms = 0;
      }
   }
   else
   {
      uiPreflightTimeout_ms = 0;
   }

}
/*----------------------------------------------------------------------------
   Checks for FPGA Preflight state = complete
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
/*
 * Sets up function for this module.
 * This module has a initial delay of 10ms and a run time delay of 5ms.
 * The function is responsible for setting up the GPIO pins for "bit bang spi"
 */
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_FPGA_1MS;

   if(GetSRU_Deployment() == enSRU_DEPLOYMENT__CT)
   {
      eLocation = FPGA_LOC__CTA;
   }
   else
   {
      eLocation = FPGA_LOC__COPA;
   }

   if( Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) )
   {
      FPGA_Init_V3();
   }
   else
   {
      FPGA_Init_V1_V2();
   }
   return 0;
}


// If In COP mode the following sogna;s are the ones that need to be checked.
// Fire Stop Switch
// In Car Stop Switch
// Inspection IC
// Inspection HA

static inline uint8_t CrossCheckFPGA( uint8_t input_MCU, uint8_t input_FPGA)
{
    // There are 4 possible combinations for the inputs. If the inputs
    // do not match return true. If the inputs match then return false
    // FPGA     MCU     Result
    //  0        0        0
    //  0        1        1
    //  1        0        1
    //  1        1        0
    return (input_FPGA ^ input_MCU);
}


/**************************************************************************************************
                             COP Safety Inputs
 *************************************************************************************************/

#define FPGA_FIRE_STOP_SWITCH   ((fpgaData.aucData[FPGA_BYTE__DATA0] & 0x20)>>5)
#define FPGA_IN_CAR_STOP_SWITCH ((fpgaData.aucData[FPGA_BYTE__DATA0] & 0x10)>>4)
#define FPGA_INSPECTION_IC      ((fpgaData.aucData[FPGA_BYTE__DATA0] & 0x08)>>3)
#define FPGA_INSPECTION_HA      ((fpgaData.aucData[FPGA_BYTE__DATA0] & 0x04)>>2)

// MUST READ THE RAW VALUE OF THESE FOM THE SRU
#define MCU_INSPECTION_HA       (SRU_Read_ExtInput(eEXT_Input_SAF1))
#define MCU_IN_CAR_STOP_SWITCH  (SRU_Read_ExtInput(eEXT_Input_SAF2))
#define MCU_FIRE_STOP_SWITCH    (SRU_Read_ExtInput(eEXT_Input_SAF3))
#define MCU_INSPECTION_IC       (SRU_Read_ExtInput(eEXT_Input_SAF4))

#define CROSS_CHECK_FIRE_STOP_SWITCH      (CrossCheckFPGA(MCU_FIRE_STOP_SWITCH  , FPGA_FIRE_STOP_SWITCH))
#define CROSS_CHECK_IN_CAR_STOP_SWITCH    (CrossCheckFPGA(MCU_IN_CAR_STOP_SWITCH, FPGA_IN_CAR_STOP_SWITCH))
#define CROSS_CHECK_INSPECTION_IC         (CrossCheckFPGA(MCU_INSPECTION_IC     , FPGA_INSPECTION_IC))
#define CROSS_CHECK_INSPECTION_HA         (CrossCheckFPGA(MCU_INSPECTION_HA     , FPGA_INSPECTION_HA))

#define CROSS_CHECK_FIRE_STOP_SWITCH_V2   (CrossCheckFPGA(MCU_FIRE_STOP_SWITCH  , FPGA_GetDataBit(FPGA_LOC__COPA, FPGA_BITS_COP__FSS) ))
#define CROSS_CHECK_IN_CAR_STOP_SWITCH_V2 (CrossCheckFPGA(MCU_IN_CAR_STOP_SWITCH, FPGA_GetDataBit(FPGA_LOC__COPA, FPGA_BITS_COP__IC_ST) ))
#define CROSS_CHECK_INSPECTION_IC_V2      (CrossCheckFPGA(MCU_INSPECTION_IC     , !FPGA_GetDataBit(FPGA_LOC__COPA, FPGA_BITS_COP__IC_INSP) ))
#define CROSS_CHECK_INSPECTION_HA_V2      (CrossCheckFPGA(MCU_INSPECTION_HA     , !FPGA_GetDataBit(FPGA_LOC__COPA, FPGA_BITS_COP__HA_INSP) ))

static uint8_t CheckRedundancyCOP( void )
{
    uint8_t result = 0; // Assume that there is no error
    uint8_t bStopSwitchBypass = GetBypassInCarStopSwitchFlag();
    if( Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V2) || Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) )
    {
       if(!bStopSwitchBypass && CROSS_CHECK_FIRE_STOP_SWITCH_V2)
       {
          uint8_t t = MCU_FIRE_STOP_SWITCH;
          uint8_t k = FPGA_GetDataBit(FPGA_LOC__COPA, FPGA_BITS_COP__FSS);
          result = 1;
       }
       else if(!bStopSwitchBypass && CROSS_CHECK_IN_CAR_STOP_SWITCH_V2)
       {
          uint8_t t = MCU_IN_CAR_STOP_SWITCH;
          uint8_t k = FPGA_GetDataBit(FPGA_LOC__COPA, FPGA_BITS_COP__IC_ST);
          result = 2;
       }
       else if (CROSS_CHECK_INSPECTION_IC_V2)
       {
          uint8_t t = MCU_INSPECTION_IC;
          uint8_t k = FPGA_GetDataBit(FPGA_LOC__COPA, FPGA_BITS_COP__IC_INSP);
          result = 3;
       }
       else if (CROSS_CHECK_INSPECTION_HA_V2)
       {
          uint8_t t = MCU_INSPECTION_HA;
          uint8_t k = FPGA_GetDataBit(FPGA_LOC__COPA, FPGA_BITS_COP__HA_INSP);
          result = 4;
       }
    }
    else
    {
       if(!bStopSwitchBypass && CROSS_CHECK_FIRE_STOP_SWITCH)
       {
          uint8_t t =MCU_FIRE_STOP_SWITCH;
          uint8_t k =FPGA_FIRE_STOP_SWITCH;
          result = 1;
       }
       else if(!bStopSwitchBypass && CROSS_CHECK_IN_CAR_STOP_SWITCH)
       {
          uint8_t t =MCU_IN_CAR_STOP_SWITCH;
          uint8_t k =FPGA_IN_CAR_STOP_SWITCH;
          result = 2;
       }
       else if (CROSS_CHECK_INSPECTION_IC)
       {
          uint8_t t = MCU_INSPECTION_IC;
          uint8_t k = FPGA_INSPECTION_IC;
          result = 3;
       }
       else if (CROSS_CHECK_INSPECTION_HA)
       {
          uint8_t t = MCU_INSPECTION_HA;
          uint8_t k = FPGA_INSPECTION_HA;
          result = 4;
       }
    }

    return result;
}

/**************************************************************************************************
                             CT Safety Inputs
 *************************************************************************************************/


#define FPGA_ESCAPE_HATCH        ((fpgaData.aucData[FPGA_BYTE__DATA1] & 0x20)>>5)
#define FPGA_CAR_SAFETIES        ((fpgaData.aucData[FPGA_BYTE__DATA1] & 0x10)>>4)

//#define FPGA_DOOR_ZONE_FRONT     ((fpgaData.data0 & 0x40)>>5)
//#define FPGA_DOOR_ZONE_REAR      ((fpgaData.data0 & 0x20)>>4)
#define FPGA_GATE_SWITCH_FRONT   ((fpgaData.aucData[FPGA_BYTE__DATA0] & 0x08)>>3)
#define FPGA_GATE_SWITCH_REAR    ((fpgaData.aucData[FPGA_BYTE__DATA0] & 0x04)>>2)
#define FPGA_INSPECTION_CAR_TOP  ((fpgaData.aucData[FPGA_BYTE__DATA0] & 0x02)>>1)
#define FPGA_CAR_TOP_STOP_SWITCH ((fpgaData.aucData[FPGA_BYTE__DATA0] & 0x01)>>0)


#define MCU_CAR_TOP_STOP_SWITCH (SRU_Read_ExtInput(eEXT_Input_SAF1))
#define MCU_ESCAPE_HATCH        (SRU_Read_ExtInput(eEXT_Input_SAF2))
#define MCU_CAR_SAFETIES        (SRU_Read_ExtInput(eEXT_Input_SAF3))
#define MCU_INSPECTION_CAR_TOP  (SRU_Read_ExtInput(eEXT_Input_SAF4))

#define MCU_GATE_SWITCH_FRONT   (SRU_Read_Input(enSRU_Input_501))
#define MCU_GATE_SWITCH_REAR    (SRU_Read_Input(enSRU_Input_502))
//#define MCU_DOOR_ZONE_FRONT     (SRU_Read_Input(enSRU_Input_503))
//#define MCU_DOOR_ZONE_REAR      (SRU_Read_Input(enSRU_Input_504))

// The signals that will be crosschecked
//#define CROSS_CHECK_DOOR_ZONE_FRONT        (CrossCheckFPGA(MCU_DOOR_ZONE_FRONT     ,FPGA_DOOR_ZONE_FRONT   ))
//#define CROSS_CHECK_DOOR_ZONE_REAR         (CrossCheckFPGA(MCU_DOOR_ZONE_REAR      ,FPGA_DOOR_ZONE_REAR    ))
#define CROSS_CHECK_GATE_SWITCH_FRONT       (CrossCheckFPGA(MCU_GATE_SWITCH_FRONT   ,FPGA_GATE_SWITCH_FRONT ))
#define CROSS_CHECK_GATE_SWITCH_REAR        (CrossCheckFPGA(MCU_GATE_SWITCH_REAR    ,FPGA_GATE_SWITCH_REAR  ))
#define CROSS_CHECK_INSPECTION_CAR_TOP      (CrossCheckFPGA(MCU_INSPECTION_CAR_TOP  ,FPGA_INSPECTION_CAR_TOP))
#define CROSS_CHECK_CAR_TOP_STOP_SWITCH     (CrossCheckFPGA(MCU_CAR_TOP_STOP_SWITCH ,FPGA_CAR_TOP_STOP_SWITCH))
#define CROSS_CHECK_ESCAPE_HATCH            (CrossCheckFPGA(MCU_ESCAPE_HATCH        ,FPGA_ESCAPE_HATCH       ))
#define CROSS_CHECK_CAR_SAFETIES            (CrossCheckFPGA(MCU_CAR_SAFETIES        ,FPGA_CAR_SAFETIES       ))

//#define CROSS_CHECK_DOOR_ZONE_FRONT_V2     (CrossCheckFPGA(MCU_DOOR_ZONE_FRONT     ,FPGA_GetDataBit(FPGA_LOC__CTA, FPGA_BITS_CT__DZF) ))
//#define CROSS_CHECK_DOOR_ZONE_REAR_V2      (CrossCheckFPGA(MCU_DOOR_ZONE_REAR      ,FPGA_GetDataBit(FPGA_LOC__CTA, FPGA_BITS_CT__DZR) ))
#define CROSS_CHECK_GATE_SWITCH_FRONT_V2    (CrossCheckFPGA(MCU_GATE_SWITCH_FRONT   ,FPGA_GetDataBit(FPGA_LOC__CTA, FPGA_BITS_CT__GSWF) ))
#define CROSS_CHECK_GATE_SWITCH_REAR_V2     (CrossCheckFPGA(MCU_GATE_SWITCH_REAR    ,FPGA_GetDataBit(FPGA_LOC__CTA, FPGA_BITS_CT__GSWR) ))
#define CROSS_CHECK_INSPECTION_CAR_TOP_V2   (CrossCheckFPGA(MCU_INSPECTION_CAR_TOP  ,!FPGA_GetDataBit(FPGA_LOC__CTA, FPGA_BITS_CT__CT_INSP) ))
#define CROSS_CHECK_CAR_TOP_STOP_SWITCH_V2  (CrossCheckFPGA(MCU_CAR_TOP_STOP_SWITCH ,FPGA_GetDataBit(FPGA_LOC__CTA, FPGA_BITS_CT__CT_SW) ))
#define CROSS_CHECK_ESCAPE_HATCH_V2         (CrossCheckFPGA(MCU_ESCAPE_HATCH        ,FPGA_GetDataBit(FPGA_LOC__CTA, FPGA_BITS_CT__ESC_HATCH) ))
#define CROSS_CHECK_CAR_SAFETIES_V2         (CrossCheckFPGA(MCU_CAR_SAFETIES        ,FPGA_GetDataBit(FPGA_LOC__CTA, FPGA_BITS_CT__CAR_SAFE) ))

static uint8_t CheckRedundancyCT( void )
{
    uint8_t result = 0; // Assume there is no problem
    if( Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V2) || Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) )
    {
       if( GetFP_RearDoors() && CROSS_CHECK_GATE_SWITCH_REAR_V2 )
       {
           uint8_t t = MCU_GATE_SWITCH_REAR;
           uint8_t k = FPGA_GetDataBit(FPGA_LOC__CTA, FPGA_BITS_CT__GSWR);
           result = 2;
       }
       else if(CROSS_CHECK_GATE_SWITCH_FRONT_V2)
       {
           uint8_t t = MCU_GATE_SWITCH_FRONT;
           uint8_t k = FPGA_GetDataBit(FPGA_LOC__CTA, FPGA_BITS_CT__GSWF);
           result = 3;
       }
       else if(CROSS_CHECK_INSPECTION_CAR_TOP_V2)
       {
           uint8_t t = MCU_INSPECTION_CAR_TOP;
           uint8_t k = FPGA_GetDataBit(FPGA_LOC__CTA, FPGA_BITS_CT__CT_INSP);
           result = 4;
       }
       else if (CROSS_CHECK_CAR_TOP_STOP_SWITCH_V2)
       {
           uint8_t t = MCU_CAR_TOP_STOP_SWITCH;
           uint8_t k = FPGA_GetDataBit(FPGA_LOC__CTA, FPGA_BITS_CT__CT_SW);
           result = 5;
       }
       else if (CROSS_CHECK_ESCAPE_HATCH_V2)
       {
           uint8_t t = MCU_ESCAPE_HATCH;
           uint8_t k = FPGA_GetDataBit(FPGA_LOC__CTA, FPGA_BITS_CT__ESC_HATCH);
           result = 6;
       }
       else if (CROSS_CHECK_CAR_SAFETIES_V2)
       {
           uint8_t t = MCU_CAR_SAFETIES;
           uint8_t k = FPGA_GetDataBit(FPGA_LOC__CTA, FPGA_BITS_CT__CAR_SAFE);
           result = 7;
       }
    }
    else
    {
       if( GetFP_RearDoors() && CROSS_CHECK_GATE_SWITCH_REAR )
       {
           uint8_t t = MCU_GATE_SWITCH_REAR;
           uint8_t k = FPGA_GATE_SWITCH_REAR;
           result = 2;
       }
       else if(CROSS_CHECK_GATE_SWITCH_FRONT)
       {
           uint8_t t = MCU_GATE_SWITCH_FRONT;
           uint8_t k = FPGA_GATE_SWITCH_FRONT;
           result = 3;
       }
       else if(CROSS_CHECK_INSPECTION_CAR_TOP)
       {
           uint8_t t = MCU_INSPECTION_CAR_TOP;
           uint8_t k = FPGA_INSPECTION_CAR_TOP;
           result = 4;
       }
       else if (CROSS_CHECK_CAR_TOP_STOP_SWITCH)
       {
           uint8_t t = MCU_CAR_TOP_STOP_SWITCH;
           uint8_t k = FPGA_CAR_TOP_STOP_SWITCH;
           result = 5;
       }
       else if (CROSS_CHECK_ESCAPE_HATCH)
       {
           uint8_t t = MCU_ESCAPE_HATCH;
           uint8_t k = FPGA_ESCAPE_HATCH;
           result = 6;
       }
       else if (CROSS_CHECK_CAR_SAFETIES)
       {
           uint8_t t = MCU_CAR_SAFETIES;
           uint8_t k = FPGA_CAR_SAFETIES;
           result = 7;
       }
    }

    return result;
}

/*-----------------------------------------------------------------------------

   This module will be used to communicate with the FPGA via SPI or I2C and
   read all of its discrete inputs into gauiFPGA_RedundantInputs[]
   These inputs will be compared against the MCU D's discrete inputs in the
   safety module.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
    // Assume no error
    uint8_t error = 0;
    static uint8_t errorCounter = 0;
    UpdateBypassInCarStopSwitchFlag();
    if( FPGA_UnloadData(eLocation) )
    {
        FPGA_GetRecentPacket(&fpgaData, eLocation);
        uwOfflineCounter_ms = 0;
    }

    if( FPGA_GetBufferOverflowFlag() )
    {
       FPGA_ClrBufferOverflowFlag();
       if( Param_ReadValue_1Bit(enPARAM1__DISA_CPLD_OVF_ALARM) )
       {
          if( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP )
          {
             SetAlarm(ALM__CPLD_OVF_COP);
          }
          else
          {
             SetAlarm(ALM__CPLD_OVF_CT);
          }
       }
    }

    CheckFor_PreflightFault();
    PreflightStatus eStatus = FPGA_GetPreflightStatus(eLocation);
    if( eStatus != PREFLIGHT_STATUS__ACTIVE )
    {
        if(GetSRU_Deployment() == enSRU_DEPLOYMENT__COP)
        {
            error = CheckRedundancyCOP();
        }
        else if(GetSRU_Deployment() == enSRU_DEPLOYMENT__CT)
        {
            error = CheckRedundancyCT();
        }

        if(error)
        {
            // Want 1 second
            // run period is 5ms
            if(errorCounter >= FPGA_REDUNDANCY_DEBOUNCE_5MS)
            {
                if(GetSRU_Deployment() == enSRU_DEPLOYMENT__CT)
                {
                   en_faults eFault = FLT__REDUNDANCY_GSWR + error - 2;
                   if(eFault > FLT__REDUNDANCY_CAR_SAFE)
                   {
                      eFault = FLT__REDUNDANCY_CAR_SAFE;
                   }
                   SetFault(eFault);
                }
                else if(GetSRU_Deployment() == enSRU_DEPLOYMENT__COP)
                {
                   en_faults eFault = FLT__REDUNDANCY_FSS + error - 1;
                   if(eFault > FLT__REDUNDANCY_HA_INSP)
                   {
                      eFault = FLT__REDUNDANCY_HA_INSP;
                   }
                   SetFault(eFault);
                }
            }
            else
            {
                errorCounter++;
            }
        }
        else
        {
            errorCounter = 0;
        }
    }
    else
    {
       errorCounter = 0;
    }

    /* Check if CPLD is reporting a fault */
    if( Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V2) || Param_ReadValue_1Bit(enPARAM1__EnableCPLD_V3) )
    {
       if( eLocation == FPGA_LOC__CTA )
       {
          FPGA_Fault_CT eError = FPGA_GetFault(eLocation);
          if( ( eError != FPGA_FAULT_CT__NONE )
           && ( eError != FPGA_FAULT_CT__STARTUP ) ) // Suppress startup fault. This should be asserted by MR
          {
             en_faults eFault = eError - FPGA_FAULT_CT__PF_CT_SW + FLT__PF_CT_SW;
             if( eFault > FLT__CPLD_CT_UNKNOWN )
             {
                eFault = FLT__CPLD_CT_UNKNOWN;
             }
             SetFault(eFault);
          }
       }
       else if( eLocation == FPGA_LOC__COPA )
       {
          FPGA_Fault_COP eError = FPGA_GetFault(eLocation);
          if( ( eError != FPGA_FAULT_COP__NONE )
           && ( eError != FPGA_FAULT_COP__STARTUP ) ) // Suppress startup fault. This should be asserted by MR
          {
             en_faults eFault = eError - FPGA_FAULT_COP__PF_HA_INSP + FLT__PF_HA_INSP;
             if( eFault > FLT__CPLD_COP_UNKNOWN )
             {
                eFault = FLT__CPLD_COP_UNKNOWN;
             }
             SetFault(eFault);
          }
       }
    }

    /* Detect communication loss */
    if( ( GetOperation_ManualMode() != MODE_M__CONSTRUCTION )
     && ( Param_ReadValue_1Bit(enPARAM1__EnableCPLDOffline) ) )
    {
       uint16_t uwLimit_ms = Param_ReadValue_8Bit(enPARAM8__CPLDOfflineTimeout_10ms)*10;
       if( uwLimit_ms < 50 )
       {
          uwLimit_ms = 50;
       }
       if( uwOfflineCounter_ms > uwLimit_ms )
       {
          // Suppress fault until CPLD communication issues worked out
//          en_faults eFault = (GetSRU_Deployment() == enSRU_DEPLOYMENT__COP)
//                           ? FLT__CPLD_OFFLINE_COP:FLT__CPLD_OFFLINE_CT;
//          SetFault(eFault);

          en_alarms eAlarm = (GetSRU_Deployment() == enSRU_DEPLOYMENT__COP)
                           ? ALM__CPLD_OFFLINE_COP:ALM__CPLD_OFFLINE_CT;
          SetAlarm(eAlarm);
       }
       else
       {
          uwOfflineCounter_ms += pstThisModule->uwRunPeriod_1ms;
       }
    }
    return 0;
}


