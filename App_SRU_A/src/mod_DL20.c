/******************************************************************************
 *
 * @file
 * @brief
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note     Handles RS422 communication with E.C.C DL-20 fixtures. Firmware 2.17 (1/30/18)
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"


#include "sru_a.h"
#include "sru.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
#include "motion.h"
#include "contributors.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_DL20Fixture =
{
   .pfnInit = Init,
   .pfnRun = Run,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_DL20_FIXTURE__MS   (10)
#define RESEND_RATE__MS                   (30)

#define HEADER_BYTE_VALUE                 (0xAA)

#define RX_TIMEOUT__MS                    (10000)
#define RX_BUFFER_SIZE                    (3) // also size of msg
#define RX_FUNCTION_BYTE2__OOS            (20)
#define RX_FUNCTION_BYTE2__PHONE          (21)

#define VOICE_MSG_MIN_SEND_CYCLES         (4) // Voice msg must be held a minimum number of cycles

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
   DL20_TX_BYTE__HEADER,
   DL20_TX_BYTE__FLOOR_PLUS1,
   DL20_TX_BYTE__DIR,
   DL20_TX_BYTE__STATUS_A,
   DL20_TX_BYTE__STATUS_B,
   DL20_TX_BYTE__VOICE,
   DL20_TX_BYTE__ALARM,
   DL20_TX_BYTE__RES7,
   DL20_TX_BYTE__RES8,
   DL20_TX_BYTE__RES9,
   DL20_TX_BYTE__FAULT,
   DL20_TX_BYTE__RES11,
   DL20_TX_BYTE__RES12,
   DL20_TX_BYTE__RES13,
   DL20_TX_BYTE__CLK_MONTH,
   DL20_TX_BYTE__CLK_DAY,
   DL20_TX_BYTE__CLK_YEAR,
   DL20_TX_BYTE__CLK_HOUR,
   DL20_TX_BYTE__CLK_MIN,
   DL20_TX_BYTE__RES19,
   DL20_TX_BYTE__RES20,
   DL20_TX_BYTE__RES21,
   DL20_TX_BYTE__RES22,
   DL20_TX_BYTE__RES23,
   DL20_TX_BYTE__RES24,
   DL20_TX_BYTE__CHECKSUM,

   NUM_DL20_TX_BYTES
} en_dl20_tx_bytes;

typedef enum
{
   DL20_DIR__UNK,
   DL20_DIR__UP,
   DL20_DIR__DOWN,
   DL20_DIR__NONE,
}en_dl20_dir;
typedef enum
{
   STATUS_A_BIT__IC_INSP,
   STATUS_A_BIT__CT_INSP,
   STATUS_A_BIT__MR_INSP,
   STATUS_A_BIT__HA_INSP,
   STATUS_A_BIT__EMS,
   STATUS_A_BIT__FIRE,
   STATUS_A_BIT__EQ,
   STATUS_A_BIT__ALWAYS_ZERO,

   NUM_STATUS_A_BITS
} en_status_a_bits;

typedef enum
{
   STATUS_B_BIT__BUZZER1,
   STATUS_B_BIT__BUZZER2,
   STATUS_B_BIT__DOOR_OPEN,
   STATUS_B_BIT__OVERLOAD,
   STATUS_B_BIT__FIRE_FLASH,
   STATUS_B_BIT__RES5,
   STATUS_B_BIT__RES6,
   STATUS_B_BIT__RES7,

   NUM_STATUS_B_BITS
} en_status_b_bits;

typedef enum
{
   VOICE_STATUS__NONE,
   VOICE_STATUS__FLOOR,
   VOICE_STATUS__GOING_UP,
   VOICE_STATUS__GOING_DN,
   VOICE_STATUS__CLEAR_DOORS,
   VOICE_STATUS__FIRE,
   VOICE_STATUS__EQ,
   VOICE_STATUS__EMS,
   VOICE_STATUS__OVERLOAD,
   VOICE_STATUS__EPWR,
   VOICE_STATUS__EPWR_EXIT,
   VOICE_STATUS__INSPECTION,
   VOICE_STATUS__INDEPENDENT,
   VOICE_STATUS__CHIME,

   NUM_VOICE_STATUS
}en_voice;
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint16_t uwRxErrorCounter;
static uint16_t uwTxErrorCounter;
static uint16_t uwResendTimer__ms;
static uint16_t uwOfflineCounter__ms;
static uint8_t aucTxBuffer[NUM_DL20_TX_BYTES];
static uint8_t ucVoiceMsgSendCountdown;

static uint8_t bDL20_PhoneFailure;
static uint8_t bDL20_OOS;

static uint8_t aucRxBuffer[RX_BUFFER_SIZE];
static uint8_t ucRxIndex;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t DL20_GetPhoneFailureFlag(void)
{
   return bDL20_PhoneFailure;
}
uint8_t DL20_GetOOSFlag(void)
{
   return bDL20_OOS;
}
uint16_t DL20_GetErrorCounter_Rx(void)
{
   return uwRxErrorCounter;
}
uint16_t DL20_GetErrorCounter_Tx(void)
{
   return uwTxErrorCounter;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadData(void)
{
   if( aucRxBuffer[1] == RX_FUNCTION_BYTE2__OOS )
   {
      bDL20_OOS = ( aucRxBuffer[2] ) ? 1:0;
      uwOfflineCounter__ms = 0;
   }
   else if( aucRxBuffer[1] == RX_FUNCTION_BYTE2__PHONE )
   {
      bDL20_PhoneFailure = ( aucRxBuffer[2] ) ? 1:0;
      uwOfflineCounter__ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void RecieveData(void)
{
   for(uint8_t i = 0; i < RX_BUFFER_SIZE*10; i++)
   {
      uint8_t ucByte;
      if( UART_RS485_ReadByte(&ucByte) )
      {
         if( ucByte == HEADER_BYTE_VALUE )
         {
            ucRxIndex = 0;
         }
         aucRxBuffer[ucRxIndex] = ucByte;
         ucRxIndex++;
         if( ucRxIndex == RX_BUFFER_SIZE )
         {
            ucRxIndex = 0;
            if( aucRxBuffer[0] == HEADER_BYTE_VALUE )
            {
               UnloadData();
            }
            else
            {
               uwRxErrorCounter++;
            }
         }
         ucRxIndex = ucRxIndex % RX_BUFFER_SIZE;
      }
      else
      {
         break;
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData_FloorAndDir(void)
{
   uint8_t ucFloor_Plus1 = 0;
   uint8_t ucFloor = GetOperation_CurrentFloor();
   if( ucFloor < GetFP_NumFloors() )
   {
      ucFloor_Plus1 = ucFloor+1;
   }
   aucTxBuffer[DL20_TX_BYTE__FLOOR_PLUS1] = ucFloor_Plus1;

   en_dl20_dir eDir = DL20_DIR__NONE;
   enum direction_enum eMotionDir = GetMotion_Direction();
   if( eMotionDir == DIR__UP )
   {
      eDir = DL20_DIR__UP;
   }
   else if( eMotionDir == DIR__DN )
   {
      eDir = DL20_DIR__DOWN;
   }
   aucTxBuffer[DL20_TX_BYTE__DIR] = eDir;

}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData_Status(void)
{
   uint8_t ucStatusA = 0;
   enum en_mode_manual eManualMode = GetOperation_ManualMode();
   enum en_mode_auto eAutoMode = GetOperation_AutoMode();
   if( eManualMode == MODE_M__INSP_IC )
   {
      ucStatusA |= 1 << STATUS_A_BIT__IC_INSP;
   }
   else if( eManualMode == MODE_M__INSP_CT )
   {
      ucStatusA |= 1 << STATUS_A_BIT__CT_INSP;
   }
   else if( eManualMode == MODE_M__INSP_MR )
   {
      ucStatusA |= 1 << STATUS_A_BIT__MR_INSP;
   }
   else if( eManualMode == MODE_M__INSP_HA )
   {
      ucStatusA |= 1 << STATUS_A_BIT__HA_INSP;
   }

   if( ( ( eAutoMode == MODE_A__EMS1 ) || ( eAutoMode == MODE_A__EMS2 ) )
    && ( GetOutputValue(enOUT_BUZZER) ) )
   {
      ucStatusA |= 1 << STATUS_A_BIT__EMS;
   }

   if( ( eAutoMode == MODE_A__FIRE1 ) || ( eAutoMode == MODE_A__FIRE2 ) || GetOutputValue(enOUT_LMP_FIRE) )
   {
      ucStatusA |= 1 << STATUS_A_BIT__FIRE;
   }

   if( ( eAutoMode == MODE_A__SEISMC ) || ( eAutoMode == MODE_A__CW_DRAIL ) || GetOutputValue(enOUT_LMP_EQ) )
   {
      ucStatusA |= 1 << STATUS_A_BIT__EQ;
   }
   aucTxBuffer[DL20_TX_BYTE__STATUS_A] = ucStatusA;

   //---------------------------------------
   uint8_t ucStatusB = 0;
   if( !Param_ReadValue_1Bit(enPARAM1__DisableDL20_Buzzer) )
   {
      /* From V2:
       *    DL20 can't buzz and play nudging message at the same time.
            Nudging message is not played on fire phase 1 since that would
            interrupt buzzer which is a violation of A17 code. */
      uint8_t bNudging = ( GetDoorState_Front() == DOOR__NUDGING ) || ( GetDoorState_Rear() == DOOR__NUDGING );
      if( ( GetOutputValue(enOUT_BUZZER) )
       && ( !bNudging || ( eAutoMode == MODE_A__FIRE1 ) ) )
      {
         ucStatusB |= 1 << STATUS_B_BIT__BUZZER1;
      }
   }

   uint8_t bAllGSW_Made = GetInputValue(enIN_GSWF) && ( GetInputValue(enIN_GSWR) || !GetFP_RearDoors() );
   if( !bAllGSW_Made )
   {
      ucStatusB |= 1 << STATUS_B_BIT__DOOR_OPEN;
   }

   if( GetOutputValue(enOUT_Overload) )
   {
      ucStatusB |= 1 << STATUS_B_BIT__OVERLOAD;
   }

   if( GetEmergencyBit(EmergencyBF_FirePhaseI_FlashFireHat) )
   {
      ucStatusB |= 1 << STATUS_B_BIT__FIRE_FLASH;
   }

   aucTxBuffer[DL20_TX_BYTE__STATUS_B] = ucStatusB;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData_Voice(void)
{
   static uint8_t bLastAllGSW_Made;
   static uint8_t bLastAnyDoorOpen;
   if( !ucVoiceMsgSendCountdown )
   {
      en_voice eVoice = VOICE_STATUS__NONE;
      enum en_classop eClassOp = GetOperation_ClassOfOp();
      enum en_mode_auto eAutoMode = GetOperation_AutoMode();
      uint8_t bAllGSW_Made = GetInputValue(enIN_GSWF) && ( GetInputValue(enIN_GSWR) || !GetFP_RearDoors() );
      if( eClassOp == CLASSOP__MANUAL )
      {
         eVoice = VOICE_STATUS__INSPECTION;
      }
      else if( GetOutputValue(enOUT_Overload) )
      {
         eVoice = VOICE_STATUS__OVERLOAD;
      }
      else if( ( eAutoMode == MODE_A__NORMAL )
            && ( ( GetDoorState_Front() == DOOR__NUDGING ) || ( GetDoorState_Rear() == DOOR__NUDGING ) ) )
      {
         eVoice = VOICE_STATUS__CLEAR_DOORS;
      }
      else if( eAutoMode == MODE_A__EPOWER )
      {
         eVoice = VOICE_STATUS__EPWR;
      }
      else if( ( eAutoMode == MODE_A__FIRE1 ) && !GetOutputValue(enOUT_BUZZER) )
      {
         eVoice = VOICE_STATUS__FIRE;
      }
      else if( ( eAutoMode == MODE_A__SEISMC ) || ( eAutoMode == MODE_A__CW_DRAIL ) )
      {
         eVoice = VOICE_STATUS__EQ;
      }
      else if( ( eAutoMode == MODE_A__EMS1 ) || ( eAutoMode == MODE_A__EMS2 ) )
      {
         eVoice = VOICE_STATUS__EMS;
      }
      else if( eAutoMode == MODE_A__INDP_SRV )
      {
         eVoice = VOICE_STATUS__INDEPENDENT;
      }
      else if( eAutoMode == MODE_A__NORMAL )
      {
         if( ( bLastAllGSW_Made != bAllGSW_Made ) && !bAllGSW_Made )// V2
         {
            eVoice = VOICE_STATUS__FLOOR;
         }
         else if( !bAllGSW_Made && ( GetOutputValue(enOUT_ARV_UP_F) || GetOutputValue(enOUT_ARV_UP_R) ) )
         {
            eVoice = VOICE_STATUS__GOING_UP;
         }
         else if( !bAllGSW_Made && ( GetOutputValue(enOUT_ARV_DN_F) || GetOutputValue(enOUT_ARV_DN_R) ) )
         {
            eVoice = VOICE_STATUS__GOING_DN;
         }
         else if( GetLocalChimeSignal() )
         {
            eVoice = VOICE_STATUS__CHIME;
         }
      }

      bLastAllGSW_Made = bAllGSW_Made;

      aucTxBuffer[DL20_TX_BYTE__VOICE] = eVoice;
      if( eVoice != VOICE_STATUS__NONE )
      {
         ucVoiceMsgSendCountdown = VOICE_MSG_MIN_SEND_CYCLES;
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData_Clock(void)
{
   aucTxBuffer[DL20_TX_BYTE__CLK_MONTH] = RTC_GetMonthOfTheYear()+1;
   aucTxBuffer[DL20_TX_BYTE__CLK_DAY] = RTC_GetDayOfTheMonth();
   aucTxBuffer[DL20_TX_BYTE__CLK_YEAR] = RTC_GetYear() % 100;
   aucTxBuffer[DL20_TX_BYTE__CLK_HOUR] = RTC_GetTimeOfDay_Hour();
   aucTxBuffer[DL20_TX_BYTE__CLK_MIN] = RTC_GetTimeOfDay_Min();
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData( void )
{
   aucTxBuffer[DL20_TX_BYTE__HEADER] = HEADER_BYTE_VALUE;

   LoadData_FloorAndDir();
   LoadData_Status();
   LoadData_Voice();
   LoadData_Clock();

   aucTxBuffer[DL20_TX_BYTE__CHECKSUM] = 0;
   for(uint8_t i = DL20_TX_BYTE__FLOOR_PLUS1; i < DL20_TX_BYTE__CHECKSUM; i++)
   {
      aucTxBuffer[DL20_TX_BYTE__CHECKSUM] += aucTxBuffer[i];
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void TransmitData(void)
{
   if(uwResendTimer__ms >= RESEND_RATE__MS)
   {
      if(UART_RS485_SendRB(&aucTxBuffer[0], NUM_DL20_TX_BYTES))
      {
         uwResendTimer__ms = 0;

         /* Mark the number of times a specific voice message has been sent */
         if( ucVoiceMsgSendCountdown )
         {
            ucVoiceMsgSendCountdown--;
         }
      }
      else
      {
         uwTxErrorCounter++;
      }
   }
   else
   {
      uwResendTimer__ms += gstMod_DL20Fixture.uwRunPeriod_1ms;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckFor_DL20Offline(void)
{
   if( uwOfflineCounter__ms >= RX_TIMEOUT__MS )
   {
      if( !GetMotion_RunFlag() && InsideDeadZone() )
      {
         if( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT )
         {
            SetAlarm(ALM__DL20_OFFLINE_CT);
         }
         else if( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP )
         {
            SetAlarm(ALM__DL20_OFFLINE_COP);
         }
      }
   }
   else
   {
      uwOfflineCounter__ms += gstMod_DL20Fixture.uwRunPeriod_1ms;
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10000;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_DL20_FIXTURE__MS;

   if( !Param_ReadValue_1Bit(enPARAM1__Enable_Janus_RS_Fixture) )
   {
      if( ( Param_ReadValue_1Bit(enPARAM1__EnableDL20_CT)  && ( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT  ) )
       || ( Param_ReadValue_1Bit(enPARAM1__EnableDL20_COP) && ( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP ) ) )
      {
         UART_RS485_DL20_Init();
      }
   }

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   if( ( Param_ReadValue_1Bit(enPARAM1__EnableDL20_CT)  && ( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT  ) )
    || ( Param_ReadValue_1Bit(enPARAM1__EnableDL20_COP) && ( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP ) ) )
   {
      CheckFor_DL20Offline();
      UpdateLocalChimeSignal(pstThisModule->uwRunPeriod_1ms);
      LoadData();

      TransmitData();

      RecieveData();
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
