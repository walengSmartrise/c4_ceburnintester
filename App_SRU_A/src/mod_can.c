/******************************************************************************
 *
 * @file
 * @brief
 * @version  V1.00
 * @date
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "sru_a.h"
#include "sys.h"

#include "ring_buffer.h"
#include <stdlib.h>
#include <string.h>
#include "GlobalData.h"
#include "position.h"


// SDA REVIEW: lots of repetition in this file. A lot of it can be refactored.


/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_CAN =
{
   .pfnInit = Init,
   .pfnRun = Run,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define LIMIT_UNLOAD_CAN_CYCLES       (16)

#define EXTRACT_NET_ID(x)      (( x >> 25 ) & 0x0F )
#define EXTRACT_SRC_ID(x)      (( x >> 20 ) & 0x1F )
#define EXTRACT_DEST_ID(x)     (( x >> 15 ) & 0x1F )
#define EXTRACT_DATAGRAM_ID(x) (( x >> 10 ) & 0x1F )

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *-----------------------------------------------------------------------------*/
static uint32_t uiOfflineCounterMRA_ms;
static uint32_t uiOfflineCounterCTA_ms;
static uint32_t uiOfflineCounterCOPA_ms;
static uint32_t uiOfflineDelay_ms;
static uint16_t uwBusErrorCounter_CAN1;
static uint16_t uwBusErrorCounter_CAN2;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Returns bus error counter
 -----------------------------------------------------------------------------*/
uint16_t GetDebugBusOfflineCounter_CAN1()
{
   return uwBusErrorCounter_CAN1;
}
uint16_t GetDebugBusOfflineCounter_CAN2()
{
   return uwBusErrorCounter_CAN2;
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = 5;

   uint8_t ucOfflineDelay_5ms = Param_ReadValue_8Bit(enPARAM8__OfflineCtrlTimer_5ms);
   if(ucOfflineDelay_5ms < MIN_OFFLINE_DELAY_5MS)
   {
      ucOfflineDelay_5ms = MIN_OFFLINE_DELAY_5MS;
   }
   uiOfflineDelay_ms = ucOfflineDelay_5ms * 5U;
   uiOfflineCounterMRA_ms = 0;
   uiOfflineCounterCOPA_ms = 0;
   uiOfflineCounterCTA_ms = 0;
   return 0;
}


/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Receive_Packet(CAN_MSG_T *pstRxMsg)
{
   static un_sdata_datagram unDatagram;
   uint8_t ucNetworkDatagramID = ExtractFromCAN_DatagramID(pstRxMsg->ID);
   uint8_t ucSourceID = ExtractFromCAN_SourceID(pstRxMsg->ID);
   uint16_t uwDestinationBitmask = ExtractFromCAN_DestinationBitmap(pstRxMsg->ID);
   uint16_t uwLocalMask = 1 << GetSystemNodeID();
   uint16_t uwSourceMask = 1 << ucSourceID;
   uint16_t uwOtherDestinationBitmask = uwDestinationBitmask & ~(uwLocalMask) & ~(uwSourceMask);

   memcpy(&unDatagram, &pstRxMsg->Data, 8 );
   InsertSharedDatagram_ByDatagramID( &unDatagram, ucNetworkDatagramID);

   if(CheckIf_SendOnABNet(uwDestinationBitmask))
   {
      UART_LoadCANMessage(pstRxMsg);
   }
 }
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static void UnloadCAN_CTA( void )
{
    static CAN_MSG_T tmp;
    // Read data from ring buffer till it is empty
    // Devices on CAN1: Car net
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN1_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
          // Unload or forward packet
          Receive_Packet(&tmp);

          uint8_t ucSourceID = ExtractFromCAN_SourceID(tmp.ID);
          if ( ( ucSourceID == SYS_NODE__MRA )
            || ( ucSourceID == SYS_NODE__MRB ) )
          {
             uiOfflineCounterMRA_ms = 0;
          }
          else if ( ( ucSourceID == SYS_NODE__COPA )
               || ( ucSourceID == SYS_NODE__COPB ) )
          {
             uiOfflineCounterCOPA_ms = 0;
          }
       }
    }

    // Read data from ring buffer till it is empty
    // This information is sent directly to traction in the ISR.
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN2_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
          switch ( tmp.ID )
          {
              default:
//                 SetAlarm( ALARM__SYS_DEBUG, SUBALARM_SYS_DEBUG__CAN_CTA_2 );
                  break;
          }
       }
    }
}

static void UnloadCAN_COPA( void )
{
    CAN_MSG_T tmp;
    // Read data from ring buffer till it is empty
    // Devices on CAN1: Car net
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN1_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
          // Unload or forward packet
          Receive_Packet(&tmp);

          uint8_t ucSourceID = ExtractFromCAN_SourceID(tmp.ID);
          if ( ( ucSourceID == SYS_NODE__MRA )
            || ( ucSourceID == SYS_NODE__MRB ) )
          {
             uiOfflineCounterMRA_ms = 0;
          }
          else if ( ( ucSourceID == SYS_NODE__CTA )
               || ( ucSourceID == SYS_NODE__CTB ) )
          {
             uiOfflineCounterCTA_ms = 0;
          }
       }
    }

    // Read data from ring buffer till it is empty
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN2_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
          switch ( tmp.ID )
          {
              default:
//                 SetAlarm( ALARM__SYS_DEBUG, SUBALARM_SYS_DEBUG__CAN_COPA_2 );
                  break;
          }
       }
    }
}
/*-----------------------------------------------------------------------------
   Check for car net board offline
 -----------------------------------------------------------------------------*/
static void CheckFor_BoardOffline()
{
   if(GetOperation_ManualMode() != MODE_M__CONSTRUCTION)
   {
      if( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT )
      {
         if(uiOfflineCounterMRA_ms < uiOfflineDelay_ms)
         {
            uiOfflineCounterMRA_ms += MOD_RUN_PERIOD_CAN_1MS;
         }
         else
         {
            SetFault(FLT__OFFLINE_MRA_BY_CTA);
         }
         if(uiOfflineCounterCOPA_ms < uiOfflineDelay_ms)
         {
            uiOfflineCounterCOPA_ms += MOD_RUN_PERIOD_CAN_1MS;
         }
         else
         {
            SetFault(FLT__OFFLINE_COPA_BY_CTA);
         }
      }
      else if( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP )
      {
         if(uiOfflineCounterMRA_ms < uiOfflineDelay_ms)
         {
            uiOfflineCounterMRA_ms += MOD_RUN_PERIOD_CAN_1MS;
         }
         else
         {
            SetFault(FLT__OFFLINE_MRA_BY_COPA);
         }
         if(uiOfflineCounterCTA_ms < uiOfflineDelay_ms)
         {
            uiOfflineCounterCTA_ms += MOD_RUN_PERIOD_CAN_1MS;
         }
         else
         {
            SetFault(FLT__OFFLINE_CTA_BY_COPA);
         }
      }
   }
}
/*-----------------------------------------------------------------------------
   Check for CAN Bus offline and reset if offline
 -----------------------------------------------------------------------------*/
static void CheckFor_CANBusOffline()
{
   if( Chip_CAN_GetGlobalStatus( LPC_CAN1 ) & CAN_GSR_BS ) // Bus offline if 1
   {
      Chip_CAN_SetMode( LPC_CAN1, CAN_RESET_MODE, ENABLE );
      __NOP();
      __NOP();
      __NOP();
      Chip_CAN_SetMode( LPC_CAN1, CAN_RESET_MODE, DISABLE );
      en_alarms eAlarm = (GetSRU_Deployment() == enSRU_DEPLOYMENT__CT)
                       ? ALM__CT_CAN_RESET_1:ALM__COP_CAN_RESET_1;
      SetAlarm(eAlarm);
   }

   if( Chip_CAN_GetGlobalStatus( LPC_CAN2 ) & CAN_GSR_BS ) // Bus offline if 1
   {
      Chip_CAN_SetMode( LPC_CAN2, CAN_RESET_MODE, ENABLE );
      __NOP();
      __NOP();
      __NOP();
      Chip_CAN_SetMode( LPC_CAN2, CAN_RESET_MODE, DISABLE );
      en_alarms eAlarm = (GetSRU_Deployment() == enSRU_DEPLOYMENT__CT)
                       ? ALM__CT_CAN_RESET_2:ALM__COP_CAN_RESET_2;
      SetAlarm(eAlarm);
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UpdateDebugBusErrorCounter()
{
   static uint8_t ucLastCountRx_CAN1;
   static uint8_t ucLastCountTx_CAN1;
   static uint8_t ucLastCountRx_CAN2;
   static uint8_t ucLastCountTx_CAN2;
   uint32_t uiStatus = Chip_CAN_GetGlobalStatus( LPC_CAN1 );
   uint8_t ucCountRx = (uiStatus >> 16) & 0xFF;
   uint8_t ucCountTx = (uiStatus >> 24) & 0xFF;
   if( ( ucCountRx > ucLastCountRx_CAN1 )
    || ( ucCountTx > ucLastCountTx_CAN1 ))
   {
       uwBusErrorCounter_CAN1++;
   }
   ucLastCountRx_CAN1 = ucCountRx;
   ucLastCountTx_CAN1 = ucCountTx;

   uiStatus = Chip_CAN_GetGlobalStatus( LPC_CAN2 );
   ucCountRx = (uiStatus >> 16) & 0xFF;
   ucCountTx = (uiStatus >> 24) & 0xFF;
   if( ( ucCountRx > ucLastCountRx_CAN2 )
    || ( ucCountTx > ucLastCountTx_CAN2 ))
   {
       uwBusErrorCounter_CAN2++;
   }
   ucLastCountRx_CAN2 = ucCountRx;
   ucLastCountTx_CAN2 = ucCountTx;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   // This is the only place the can periphs are init.
    switch(GetSRU_Deployment())
    {
        case enSRU_DEPLOYMENT__CT:
            UnloadCAN_CTA();
            break;
        case enSRU_DEPLOYMENT__COP:
            UnloadCAN_COPA();
            break;
        default:
            break;
    }
    CheckFor_BoardOffline();
    CheckFor_CANBusOffline();
    UpdateDebugBusErrorCounter();
    CAN1_FillHardwareBuffer();
    return 0;
}

