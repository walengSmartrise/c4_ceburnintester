
#include "GlobalData.h"
#include <stdint.h>
#include <string.h>
#include "sru_a.h"
#include "sys.h"
#include "sru.h"
#include "sru_a.h"
#include "mod.h"
#include "motion.h"


static struct st_position_b gstPostion_NTS;

struct st_CarCallRequest
{
      uint8_t ucCounter;
      uint8_t ucFloorIndex;
      uint8_t ucDoorIndex;
};

#define MAX_CAR_CALL_REQUESTS (10)
static struct st_CarCallRequest stCarCallRequests[MAX_CAR_CALL_REQUESTS];

/*-----------------------------------------------------------------------------
   SRU
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
   Parameters
 -----------------------------------------------------------------------------*/
static uint8_t gbFlashParamsRdy = 0;
static uint8_t gucFP_NumFloor = MIN_NUM_FLOORS;
static uint8_t gucFP_RearDoor = 0;
static en_door_type gaucFP_DoorType[NUM_OF_DOORS];
static uint8_t gucFP_FreightDoors = 0;
static uint16_t guwFP_ContractSpeed = MIN_CONTRACT_SPEED;

inline uint8_t GetFP_FlashParamsReady()
{
   return gbFlashParamsRdy;
}
// Only for mod_param_app
inline void SetFP_FlashParamsReady()
{
   gbFlashParamsRdy = 1;
}

inline uint8_t GetFP_NumFloors( void )
{
     return gucFP_NumFloor;
}

// Only for mod_param_app
inline void SetFP_NumFloors(uint8_t bFloor)
{
   if( bFloor >= MIN_NUM_FLOORS
    && bFloor <= MAX_NUM_FLOORS )
   {
      gucFP_NumFloor = bFloor;
   }
}
inline uint8_t GetFP_RearDoors( void )
{
     return gucFP_RearDoor;
}

// Only for mod_param_app
inline void SetFP_RearDoors(uint8_t bDoor)
{
   if(bDoor < 2)
   {
      gucFP_RearDoor = bDoor;
   }
}
inline en_door_type GetFP_DoorType( enum en_doors eDoor )
{
     return gaucFP_DoorType[eDoor];
}
inline void SetFP_DoorType( en_door_type enDoorType, enum en_doors eDoor )
{
   gaucFP_DoorType[eDoor] = enDoorType;
}
inline uint8_t GetFP_FreightDoors( void )
{
     return gucFP_FreightDoors;
}
inline void SetFP_FreightDoors( uint8_t bDoor )
{
     gucFP_FreightDoors = bDoor;
}
inline uint16_t GetFP_ContractSpeed( void )
{
     return guwFP_ContractSpeed;
}

// Only for mod_param_app
inline void SetFP_ContractSpeed( uint16_t uwSpeed )
{
   if( uwSpeed > MIN_CONTRACT_SPEED
    && uwSpeed <= MAX_CAR_SPEED)
   {
      guwFP_ContractSpeed = uwSpeed;
   }
}

/*-----------------------------------------------------------------------------
   I/O
 -----------------------------------------------------------------------------*/
static uint32_t gaulBitMapedInputs[ BITMAP32_SIZE ( NUM_INPUT_FUNCTIONS ) ];
static uint32_t gaulBitMapedOutputs[ BITMAP32_SIZE ( NUM_OUTPUT_FUNCTIONS ) ];
// ucIndex = index into bitmap
inline void SetInputBitMap(uint32_t ulBitmap, uint8_t ucIndex)
{
   if(ucIndex < BITMAP32_SIZE(NUM_INPUT_FUNCTIONS))
   {
      gaulBitMapedInputs[ucIndex] = ulBitmap;
   }
}
// ucIndex = index into bitmap
inline void SetOutputBitMap(uint32_t ulBitmap, uint8_t ucIndex)
{
   if(ucIndex < BITMAP32_SIZE(NUM_OUTPUT_FUNCTIONS))
   {
      gaulBitMapedOutputs[ucIndex] = ulBitmap;
   }
}
// ucIndex = index into bitmap
inline uint32_t GetInputBitMap(uint8_t ucIndex)
{
   uint32_t ulReturn = (ucIndex < BITMAP32_SIZE(NUM_INPUT_FUNCTIONS))
                     ? gaulBitMapedInputs[ucIndex]
                       : 0;
   return ulReturn;
}
// ucIndex = index into bitmap
inline uint32_t GetOutputBitMap(uint8_t ucIndex)
{
   uint32_t ulReturn = (ucIndex < BITMAP32_SIZE(NUM_OUTPUT_FUNCTIONS))
                     ? gaulBitMapedOutputs[ucIndex]
                       : 0;
   return ulReturn;
}

inline uint8_t GetInputValue(enum en_input_functions enInput)
{
   uint8_t ucReturn = (enInput < NUM_INPUT_FUNCTIONS) ?
                           Sys_Bit_Get_Array32(gaulBitMapedInputs,
                                                enInput,
                                                BITMAP32_SIZE(NUM_INPUT_FUNCTIONS))
                           :0;
   return ucReturn;
}
inline uint8_t GetOutputValue(enum en_output_functions enOutput)
{
   uint8_t ucReturn = (enOutput < NUM_OUTPUT_FUNCTIONS) ?
                           Sys_Bit_Get_Array32(gaulBitMapedOutputs,
                                                enOutput,
                                                BITMAP32_SIZE(NUM_OUTPUT_FUNCTIONS))
                           :0;
   return ucReturn;
}

/*-----------------------------------------------------------------------------
Position reference access
 -----------------------------------------------------------------------------*/

uint32_t GetPositionReference(void)
{
   return Param_ReadValue_24Bit( enPARAM24__LearnedFloor_0 );
}
void SetNTSPosition(uint32_t ulPosition)
{
   gstPostion_NTS.ulPosCount = ulPosition;
}
uint32_t GetNTSPostion()
{
   return gstPostion_NTS.ulPosCount;
}
void SetNTSVelocity(int16_t wVelocity)
{
   gstPostion_NTS.wVelocity = wVelocity;
}
int16_t GetNTSVelocity()
{
   return gstPostion_NTS.wVelocity;
}

/*-----------------------------------------------------------------------------

   UI Requests

   bCT:
      = 1 for request from CT UI
      = 0 for  request from MR UI
 -----------------------------------------------------------------------------*/

void InitCarCallQue ( void )
{
   for (uint8_t i = 0; i < MAX_CAR_CALL_REQUESTS; i++)
   {
      stCarCallRequests[i].ucCounter = 0;
   }
}

uint8_t SetCarCall (uint8_t ucFloorIndex, uint8_t ucDoorIndex)
{
   uint8_t ucReturn = 0;

   if (ucFloorIndex < Param_ReadValue_8Bit( enPARAM8__NumFloors ))
   {
      if ( GetSRU_Deployment() == 4 )
      {
         LatchCarCall(ucFloorIndex, ucDoorIndex);
      }
      else
      {
         //Check if there is already this call latched
         for (uint8_t i = 0; i < MAX_CAR_CALL_REQUESTS; i++)
         {
            if (stCarCallRequests[i].ucCounter && stCarCallRequests[i].ucFloorIndex == ucFloorIndex)
            {
               ucReturn = 1;
               break;
            }
         }

         //if not, latch the call in.
         if (ucReturn == 0)
         {
            for (uint8_t i = 0; i < MAX_CAR_CALL_REQUESTS; i++)
            {
               if (stCarCallRequests[i].ucCounter == 0)
               {
                  stCarCallRequests[i].ucCounter = 10;
                  stCarCallRequests[i].ucFloorIndex = ucFloorIndex;
                  stCarCallRequests[i].ucDoorIndex = ucDoorIndex;
                  ucReturn = 1;
                  break;
               }
            }
         }
      }
   }
   return ucReturn;
}

uint8_t GetCarCallRequest ( enum en_doors eDoor )
{
   uint8_t ucReturn = INVALID_FLOOR;

   for (uint8_t i = 0; i < MAX_CAR_CALL_REQUESTS; i++)
   {
      if( ( ( eDoor == DOOR_FRONT ) && ( !stCarCallRequests[i].ucDoorIndex ) )
       || ( ( eDoor == DOOR_REAR  ) && (  stCarCallRequests[i].ucDoorIndex ) ) )
      {
         if (stCarCallRequests[i].ucCounter)
         {
            ucReturn = stCarCallRequests[i].ucFloorIndex+1;
            stCarCallRequests[i].ucCounter--;
            break;
         }
      }
   }

   return ucReturn;
}

/*-----------------------------------------------------------------------------
Latched Calls
 -----------------------------------------------------------------------------*/
static uint32_t gaulBitMapedLatchedCCBs[NUM_OF_DOORS][BITMAP32_SIZE(MAX_NUM_FLOORS)];

void SetBitMap_LatchedCCB(uint32_t ulCarCalls, uint8_t ucIndex, uint8_t bRear)
{
   if(bRear < (gucFP_RearDoor+1))
   {
      if(ucIndex < BITMAP32_SIZE(MAX_NUM_FLOORS))
      {
         if(gaulBitMapedLatchedCCBs[bRear][ucIndex] != ulCarCalls)
         {
            gaulBitMapedLatchedCCBs[bRear][ucIndex] = ulCarCalls;
         }
      }
   }
}

uint32_t GetBitMap_LatchedCCB(uint8_t ucIndex, uint8_t bRear)
{
   uint32_t ulReturn = 0;
   if(bRear < (gucFP_RearDoor+1))
   {
      if(ucIndex < BITMAP32_SIZE(MAX_NUM_FLOORS))
      {
         ulReturn = gaulBitMapedLatchedCCBs[bRear][ucIndex];
      }
   }
   return ulReturn;
}

/*-----------------------------------------------------------------------------
Position Access
 -----------------------------------------------------------------------------*/
void SetStructPosition_NTS(struct st_position_b *pstPosition)
{
   // memcmp results inconsistent.
   uint8_t bDiff = gstPostion_NTS.wVelocity != pstPosition->wVelocity;
   bDiff |= gstPostion_NTS.ulPosCount != pstPosition->ulPosCount;
   bDiff |= gstPostion_NTS.uwStatus != pstPosition->uwStatus;
   bDiff |= gstPostion_NTS.ucError != pstPosition->ucError;
   if(bDiff)
   {
      memcpy(&gstPostion_NTS, pstPosition, sizeof(struct st_position_b));
   }
}
void GetStructPosition_NTS(struct st_position_b *pstPosition)
{
   memcpy(pstPosition, &gstPostion_NTS, sizeof(struct st_position_b));
}

/*-----------------------------------------------------------------------------
States
 ----------------------------------------------------------------------------*/
static uint8_t gucAto_State;

static uint8_t gucFlorLearn_State;

static uint8_t gucCW_Stat;
static uint8_t gucRecal_State;
static uint8_t gucFireSrvice_State;
static uint8_t gucFireSrvice2_State;

//static enum en_door_states geDorState;


void SetModState_AutoState(uint8_t ucState )
{
   gucAto_State = ucState;
}
uint8_t GetModState_AutoState()
{
   return gucAto_State;
}
void SetModState_FloorLearnState(uint8_t ucState )
{
   gucFlorLearn_State = ucState;
}
uint8_t GetModState_FloorLearnState()
{
   return gucFlorLearn_State;
}
void SetModState_FireSrv(uint8_t ucState )
{
   gucFireSrvice_State = ucState;
}
uint8_t GetModState_FireSrv()
{
   return gucFireSrvice_State;
}
void SetModState_FireSrv2(uint8_t ucState )
{
   gucFireSrvice2_State = ucState;
}
uint8_t GetModState_FireSrv2()
{
   return gucFireSrvice2_State;
}
void SetModState_Counterweight(uint8_t ucState )
{
   gucCW_Stat = ucState;
}
uint8_t GetModState_Counterweight()
{
   return gucCW_Stat;
}
void SetModState_Recall(uint8_t ucState )
{
   gucRecal_State = ucState;
}
uint8_t GetModState_Recall()
{
   return gucRecal_State;
}

/*-----------------------------------------------------------------------------
Doors
 -----------------------------------------------------------------------------*/
enum en_door_states GetDoorState_Front()
{
   return GetOperation4_FrontDoorState();
}
enum en_door_states GetDoorState_Rear()
{
   return GetOperation4_RearDoorState();
}
/*-----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/

uint8_t GetEmergencyBit( enum en_emergency_bitmap enEmergencyBF )
{
   uint8_t bReturn = 0;
   if( enEmergencyBF < Num_EmergenyBF)
   {
      bReturn = ( GetOperation2_EmergencyBitmap_MRA() >> enEmergencyBF ) & 1;
   }
   return bReturn;
}

/*-----------------------------------------------------------------------------
   Acceptance
 ----------------------------------------------------------------------------*/
static uint32_t uiAcceptanceTest_Plus1;

void SetAcceptanceTest_Plus1( uint32_t uiIndex_Plus1)
{
   uiAcceptanceTest_Plus1 = uiIndex_Plus1;
}
uint32_t GetAcceptanceTest_Plus1()
{
   return uiAcceptanceTest_Plus1;
}

/*----------------------------------------------------------------------------
   Motion Flag access functions
 *----------------------------------------------------------------------------*/
inline uint8_t GetPreflightFlag()
{
   return GetOperation1_PreflightFlag_MRA();
}
/*-----------------------------------------------------------------------------
   Returns 1 if valid opening
 ----------------------------------------------------------------------------*/
uint8_t GetFloorOpening_Front( uint8_t ucFloor )
{
   uint8_t bValid = 0;
   if( ucFloor < GetFP_NumFloors() )
   {
      uint8_t ucMapIndex = ucFloor / 32;
      uint8_t ucBit = ucFloor % 32;
      uint32_t uiMap = Param_ReadValue_32Bit(enPARAM32__OpeningBitmapF_0+ucMapIndex);
      bValid = Sys_Bit_Get(&uiMap, ucBit);
   }
   return bValid;
}
uint8_t GetFloorOpening_Rear( uint8_t ucFloor )
{
   uint8_t bValid = 0;
   if( GetFP_RearDoors()
  && ( ucFloor < GetFP_NumFloors() ) )
   {
      uint8_t ucMapIndex = ucFloor / 32;
      uint8_t ucBit = ucFloor % 32;
      uint32_t uiMap = Param_ReadValue_32Bit(enPARAM32__OpeningBitmapR_0+ucMapIndex);
      bValid = Sys_Bit_Get(&uiMap, ucBit);
   }
   return bValid;
}

/*----------------------------------------------------------------------------
For GetGroup_DispatchData()
   b0:    Any Door Open
   b1-b2: Priority_Plus1
   b3:    Hall Calls Disabled
   b4:    In Slowdown
   b5:    Suppress Reopen
   b6:    Car Out Of Group
   b7:    bIdleDirection
 *----------------------------------------------------------------------------*/
uint8_t HallCallsDisabled ( void )
{
   return ((GetGroup_DispatchData() >> 3) & 1);
}
uint8_t CheckIf_CarOutOfGroup ()
{
   uint8_t bOutOfGroup = (GetGroup_DispatchData() >> 6) & 1;
   return bOutOfGroup;
}
uint8_t GetCarPriority()
{
   // If 1, HC_DIR__UP, else HC_DIR__DOWN
    return ((GetGroup_DispatchData() >> 2) & 1);
}
uint8_t CheckIf_IdleDirection(void)
{
   return ((GetGroup_DispatchData() >> 7) & 1);
}
uint8_t CheckIf_InSlowdown(void)
{
   return ((GetGroup_DispatchData() >> 4) & 1);
}
uint8_t CheckIf_SuppressReopen(void)
{
   return ((GetGroup_DispatchData() >> 5) & 1);
}

/*-----------------------------------------------------------------------------
   Create a localized version of the chime output to avoid missing the passing
   chime when running at high speeds
 -----------------------------------------------------------------------------*/
static uint8_t bLocalChimeSignal;
#define CHIME_DURATION_MS      (500)
void UpdateLocalChimeSignal( uint16_t uwRunPeriod_ms )
{
   static uint8_t bLastDoorZone_F;
   static uint8_t bLastDoorZone_R;
   static uint16_t uwChimeTimer_ms = CHIME_DURATION_MS;
   uint8_t bChime_Out = 0;
   if(GetOperation_ClassOfOp() == CLASSOP__AUTO)
   {
      uint8_t bDoorZone_F = GetInputValue(enIN_DZ_F);
      uint8_t bDoorZone_R = GetInputValue(enIN_DZ_R);

      /* On rising edge of either DZ signal, assert the chime for the predefined period. */
      if( !bLastDoorZone_F && bDoorZone_F )
      {
         uwChimeTimer_ms = 0;
      }
      else if( !bLastDoorZone_R && bDoorZone_R )
      {
         uwChimeTimer_ms = 0;
      }

      if(uwChimeTimer_ms <= CHIME_DURATION_MS)
      {
         uwChimeTimer_ms += uwRunPeriod_ms;
         bChime_Out = 1;
      }

      bLastDoorZone_F = bDoorZone_F;
      bLastDoorZone_R = bDoorZone_R;
   }
   bLocalChimeSignal = bChime_Out;
}
uint8_t GetLocalChimeSignal(void)
{
   return bLocalChimeSignal;
}

/*-----------------------------------------------------------------------------
   For spoofing displayed car mode as normal operation when car is allowed to run on e-power
 -----------------------------------------------------------------------------*/
uint8_t CheckIf_EPowerCarInNormal(void)
{
   uint8_t bNormal = 0;
   if( ( GetOperation_AutoMode() == MODE_A__EPOWER )
    && ( !HallCallsDisabled() ) )
   {
      bNormal = 1;
   }
   return bNormal;
}

/*-----------------------------------------------------------------------------
   View Debug Data Requests
 ----------------------------------------------------------------------------*/
en_view_debug_data GetUIRequest_ViewDebugDataCommand(void)
{
   en_view_debug_data eViewDebugData = VDD__NONE;
   if( GetUIRequest_ViewDebugData_MRB() )
   {
      eViewDebugData = GetUIRequest_ViewDebugData_MRB();
   }
   else if( GetUIRequest_ViewDebugData_CTB() )
   {
      eViewDebugData = GetUIRequest_ViewDebugData_CTB();
   }
   else if( GetUIRequest_ViewDebugData_COPB() )
   {
      eViewDebugData = GetUIRequest_ViewDebugData_COPB();
   }
   return eViewDebugData;
}
/*-----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
uint8_t GetOverLoadFlag(void)
{
   uint8_t bFlag = 0;
   if((GetOperation_AutoMode() == MODE_A__SABBATH) && Param_ReadValue_1Bit(enPARAM1__SabbathDisableLWD))
   {
	   bFlag = 0;
   }
   else
   {
   bFlag |= ( GetInputValue(enIN_OVER_LOAD) );
   bFlag |= ( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__MR ) && ( GetLW_GetLoadFlag_MRB() & (1 << LOAD_FLAG__OVER_LOAD) );
   bFlag |= ( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__CT ) && ( GetLW_GetLoadFlag_CTB() & (1 << LOAD_FLAG__OVER_LOAD) );
   }
   return bFlag;
}
uint8_t GetFullLoadFlag(void)
{
   uint8_t bFlag = 0;
   if((GetOperation_AutoMode() == MODE_A__SABBATH) && Param_ReadValue_1Bit(enPARAM1__SabbathDisableLWD))
   {
	   bFlag = 0;
   }
   else
   {
   bFlag |= ( GetInputValue(enIN_FULL_LOAD) );
   bFlag |= ( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__MR ) && ( GetLW_GetLoadFlag_MRB() & (1 << LOAD_FLAG__FULL_LOAD) );
   bFlag |= ( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__CT ) && ( GetLW_GetLoadFlag_CTB() & (1 << LOAD_FLAG__FULL_LOAD) );
   }
   return bFlag;
}
uint8_t GetLightLoadFlag(void)
{
   uint8_t bFlag = 0;
   if((GetOperation_AutoMode() == MODE_A__SABBATH) && Param_ReadValue_1Bit(enPARAM1__SabbathDisableLWD))
   {
	   bFlag = 0;
   }
   else
   {
   bFlag |= ( GetInputValue(enIN_LIGHT_LOAD) );
   bFlag |= ( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__MR ) && ( GetLW_GetLoadFlag_MRB() & (1 << LOAD_FLAG__LIGHT_LOAD) );
   bFlag |= ( Param_ReadValue_8Bit(enPARAM8__LoadWeigherSelect) == LWS__CT ) && ( GetLW_GetLoadFlag_CTB() & (1 << LOAD_FLAG__LIGHT_LOAD) );
   }
   return bFlag;
}
/*-----------------------------------------------------------------------------
   Check if car is inside virtual dead zone
 ----------------------------------------------------------------------------*/
uint8_t InsideDeadZone( void ) {

   uint8_t bInsideDeadZone = 0;

   if ( Param_ReadValue_1Bit(enPARAM1__RelevelEnabled) )
   {
      uint32_t ucRelevelThreshold = Param_ReadValue_8Bit( enPARAM8__RelevelingDistance_05mm );
      if (ucRelevelThreshold < LOWER_RELEVELING_THRESHHOLD)
      {
         ucRelevelThreshold = LOWER_RELEVELING_THRESHHOLD;
      }
      else if (ucRelevelThreshold > UPPER_RELEVELING_THRESHHOLD)
      {
         ucRelevelThreshold = UPPER_RELEVELING_THRESHHOLD;
      }
      uint8_t ucCurrentFloor = GetOperation_CurrentFloor();
      uint32_t uiPosition = GetPosition_PositionCount();

      bInsideDeadZone = ( ( uiPosition > ( Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0+ucCurrentFloor) - ucRelevelThreshold ) )
                       && ( uiPosition < ( Param_ReadValue_24Bit(enPARAM24__LearnedFloor_0+ucCurrentFloor) + ucRelevelThreshold ) ) );
   }
   else
   {
      //if releveling is disabled, then inside the DZ is inside the dead zone
      bInsideDeadZone = GetInputValue(enIN_DZ_F) || ( GetFP_RearDoors() && GetInputValue(enIN_DZ_R) );
   }
   /*
    * If the re-level threshold is to low or too high, default it.
    * Motion doesn't allow movement for distances under 13 counts
    */
   return bInsideDeadZone;
}
/*-----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
static uint8_t bBypassInCarStopSwitch;
#define BYPASS_CPLD_IN_CAR_STOP_ANY_TIME_FIRE_ACTIVE     (1)  // always bypass in car stop switches when on fire until CPLD code fixed
#if !BYPASS_CPLD_IN_CAR_STOP_ANY_TIME_FIRE_ACTIVE
static uint8_t Fire_GetRecallFloor( void )
{
   uint8_t mainRecall = Param_ReadValue_8Bit( enPARAM8__FireRecallFloorM );
   uint8_t selectedRecallFloor  = mainRecall;

   if( GetEmergencyBit( EmergencyBF_FirePhaseI_RecallToAltFloor ) )
   {
      selectedRecallFloor = Param_ReadValue_8Bit( enPARAM8__FireRecallFloorA );
   }

   return selectedRecallFloor;
}
static uint8_t Fire_AtPhase2RecallFloor()
{
   uint8_t bAtRecallFloor = 0;
   if( ( GetOperation_CurrentFloor() == Fire_GetRecallFloor() )
    && ( GetInputValue(enIN_DZ_F) || GetInputValue(enIN_DZ_R) )
    && ( !GetMotion_RunFlag() ) )
   {
      bAtRecallFloor = 1;
   }
   return bAtRecallFloor;
}
#endif
void UpdateBypassInCarStopSwitchFlag(void)
{
   /* IC Stop should also be bypassed when recalling after Phase 2 is switched off
    * A17.1 2013 - 2.27.3.3.4*/
#if BYPASS_CPLD_IN_CAR_STOP_ANY_TIME_FIRE_ACTIVE
   if( Param_ReadValue_1Bit(enPARAM1__DisableBypassICStop) )
   {
      bBypassInCarStopSwitch = 0;
   }
   else if( ( GetOperation_AutoMode() == MODE_A__FIRE1 ) || ( GetOperation_AutoMode() == MODE_A__FIRE2 ) )
   {
      bBypassInCarStopSwitch = 1;
   }
   else
   {
      bBypassInCarStopSwitch = 0;
   }
#else
   uint8_t bPh2BypassMode = ( GetOperation_AutoMode() == MODE_A__FIRE2 )
                         && ( GetModState_FireSrv2() == FIRE_II__OFF );
   uint8_t bPh2Bypass = bPh2BypassMode && ( GetDoorState_Front() == DOOR__CLOSED ) && ( !GetFP_RearDoors() || ( GetDoorState_Rear() == DOOR__CLOSED ) );
   if( Param_ReadValue_1Bit(enPARAM1__DisableBypassICStop) )
   {
      bBypassInCarStopSwitch = 0;
   }
   else if ( ( GetMotion_SpeedCommand() || GetMotion_RunFlag() )
          && ( ( GetOperation_AutoMode() == MODE_A__FIRE1 ) ||
               ( GetOperation_AutoMode() == MODE_A__EMS1 ) ||
               ( bPh2Bypass ) ) )
   {
      bBypassInCarStopSwitch = 1;
   }
   else if( ( GetOperation_AutoMode() != MODE_A__FIRE1 )
         && ( GetOperation_AutoMode() != MODE_A__EMS1 )
         && ( !bPh2BypassMode || ( Fire_AtPhase2RecallFloor() && ( GetOperation_AutoMode() == MODE_A__FIRE2 ) ) ) )
   {
      bBypassInCarStopSwitch = 0;
   }
#endif
}
uint8_t GetBypassInCarStopSwitchFlag(void)
{
   return bBypassInCarStopSwitch;
}
