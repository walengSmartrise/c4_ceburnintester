/******************************************************************************
 * @author   Keith Soneda
 * @version  V1.00
 * @date     1, August 2016
 *
 * @note   This file works with mod_fault_app.c for fault management between boards in enum en_fault_nodes
 *          ClrFault/SetFault(ucFaultNum)
 *
 ******************************************************************************/
/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"

#include "sru.h"
#include "sys.h"
#include <stdint.h>
#include <string.h>
#include "GlobalData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_Fault =
{
   .pfnInit = Init,
   .pfnRun = Run,
};
st_fault gstFault;

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
   Checks reset source and asserts appropriate reset fault
 -----------------------------------------------------------------------------*/
static void UpdateResetFault()
{
   static uint32_t uiResetSrc;
   if(!uiResetSrc)
   {
      uiResetSrc = Chip_SYSCTL_GetSystemRSTStatus();
      Chip_SYSCTL_ClearSystemRSTStatus(uiResetSrc);
   }

   if( uiResetSrc & SYSCTL_RST_BOD )
   {
      en_faults eFaultNum = FLT__BOD_RESET_MRA + FaultApp_GetNode()-1;
      if(eFaultNum > FLT__BOD_RESET_COPB)
      {
         eFaultNum = FLT__BOD_RESET_COPB;
      }
      SetFault( eFaultNum );
      gstFault.bActiveFault = 1;
      gstFault.eFaultNumber = eFaultNum;
      gstFault.eActiveNode_Plus1 = FaultApp_GetNode() + 1;
      gstFault.aeActiveFaultNum[FaultApp_GetNode()] = eFaultNum;
   }
   else if( uiResetSrc & SYSCTL_RST_WDT )
   {
      en_faults eFaultNum = FLT__WDT_RESET_MRA + FaultApp_GetNode()-1;
      if(eFaultNum > FLT__WDT_RESET_COPB)
      {
         eFaultNum = FLT__WDT_RESET_COPB;
      }
      SetFault( eFaultNum );
      gstFault.bActiveFault = 1;
      gstFault.eFaultNumber = eFaultNum;
      gstFault.eActiveNode_Plus1 = FaultApp_GetNode() + 1;
      gstFault.aeActiveFaultNum[FaultApp_GetNode()] = eFaultNum;
   }
   else//assume SYSCTL_RST_POR
   {
      en_faults eFaultNum = FLT__BOARD_RESET_MRA + FaultApp_GetNode()-1;
      if(eFaultNum > FLT__BOARD_RESET_COPB)
      {
         eFaultNum = FLT__BOARD_RESET_COPB;
      }
      SetFault( eFaultNum );
      gstFault.bActiveFault = 1;
      gstFault.eFaultNumber = eFaultNum;
      gstFault.eActiveNode_Plus1 = FaultApp_GetNode() + 1;
      gstFault.aeActiveFaultNum[FaultApp_GetNode()] = eFaultNum;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetFault( en_faults eFaultNum )
{
   if( ( GetOperation_ManualMode() != MODE_M__CONSTRUCTION )
    || ( GetFault_ConstructionType(eFaultNum) == FT_CON__ON ) )
   {
      if(eFaultNum < NUM_FAULTS)
      {
         st_fault_data stFaultData;
         stFaultData.eFaultNum = eFaultNum;
         SetLocalActiveFault(&stFaultData);
      }
   }
}

/*-----------------------------------------------------------------------------
   Update active fault and per node fault arrays
 -----------------------------------------------------------------------------*/
void UpdateActiveFaults( void )
{
    gstFault.bActiveFault = 0;
    gstFault.eFaultNumber = FLT__NONE;
    gstFault.eActiveNode_Plus1 = 0;
    for( uint8_t i = 0; i < NUM_FAULT_NODES; i++ )
    {
        st_fault_data stActiveFault;
        GetBoardActiveFault( &stActiveFault, i );
        en_faults eFaultNum = stActiveFault.eFaultNum;

        gstFault.aeActiveFaultNum[ i ] = eFaultNum;

        if( !gstFault.bActiveFault
       && ( eFaultNum != FLT__NONE ) )
        {
            gstFault.bActiveFault = 1;
            gstFault.eFaultNumber = eFaultNum;
            gstFault.eActiveNode_Plus1 = i + 1;
        }
    }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Update_FaultLED()
{
   static uint8_t ucTimer_1ms;
   static uint8_t bLastLED;

   if ( gstFault.eFaultNumber )
   {
      if ( ucTimer_1ms > 100/MOD_RUN_PERIOD_FAULT_1MS )
      {
         SRU_Write_LED( enSRU_LED_Fault, bLastLED );

         ucTimer_1ms = 0;

         bLastLED = ( bLastLED )? 0 : 1;
      }
      else
      {
         ucTimer_1ms++;
      }
   }
   else
   {
      SRU_Write_LED( enSRU_LED_Fault, 0 );
   }
}


/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 50;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_FAULT_1MS;

   return 0;
}


/*-----------------------------------------------------------------------------

   The fault module asserts a reset fault for 3 seconds at startup before
   starting to process the rest of the faults.
   Most fault will automatically clear after 3 seconds.
   Some faults that don't fit in any other module are being processed here in
   the fault module such as DIP8 (CPU Stop Switch) or Fire Stop Switch.
   When ANY fault is active, the car cannot move and commands Emergency Stop.

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   static uint32_t uiStartupCounter_1ms = BOARD_RESET_FAULT_TIMER_MS/MOD_RUN_PERIOD_FAULT_1MS;

   if( uiStartupCounter_1ms )
   {
      UpdateResetFault();
      uiStartupCounter_1ms--;
   }
   else if( SRU_Read_DIP_Switch( enSRU_DIP_A1 ) )
   {
      en_faults eFaultNum = FLT__CPU_STOP_SW_MRA + FaultApp_GetNode()-1;
      if(eFaultNum > FLT__CPU_STOP_SW_COPB)
      {
         eFaultNum = FLT__CPU_STOP_SW_COPB;
      }
      SetFault( eFaultNum );
      gstFault.bActiveFault = 1;
      gstFault.eFaultNumber = eFaultNum;
      gstFault.eActiveNode_Plus1 = FaultApp_GetNode() + 1;
      gstFault.aeActiveFaultNum[FaultApp_GetNode()] = eFaultNum;
   }
   else
   {
      switch(GetSystemNodeID())
      {
         case SYS_NODE__CTA:
            Run_ModFault_CTA();
            break;
         case SYS_NODE__COPA:
            Run_ModFault_COPA();
            break;
         default:
            break;
      }
   }
   Update_FaultLED();
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
