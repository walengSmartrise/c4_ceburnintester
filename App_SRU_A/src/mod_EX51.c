/******************************************************************************
 *
 * @file
 * @brief
 * @version  V1.00
 * @date     11, November 2019
 *
 * @note     Handles RS422/RS485 communication with E.C.C EX-51 fixtures. Firmware 3.00 (11/11/19)
 *			 Uses same parameters as DL-20
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"


#include "sru_a.h"
#include "sru.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "sys.h"
#include "GlobalData.h"
#include "operation.h"
#include "motion.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
struct st_module gstMod_EX51Fixture =
{
   .pfnInit = Init,
   .pfnRun = Run,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_EX51_FIXTURE__MS   (10)
#define RESEND_RATE__MS                   (100)
#define HEADER_BYTE_VALUE                 (0xAA)

#define EX51_SETUP_FILE_INSTALLED_FLOOR_BYTE	(2)		//Dependent on Setup File in EX-51 SD Card
#define EX51_SETUP_FILE_FRONT_DOOR_BIT			(1)		//Dependent on Setup File in EX-51 SD Card
#define EX51_SETUP_FILE_BACK_DOOR_BIT			(0)		//Dependent on Setup File in EX-51 SD Card
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
typedef enum
{
	EX51_TX_BYTE__HEADER,
	EX51_TX_BYTE__FLOOR_PLUS1,
	EX51_TX_BYTE__STATUS_A,
	EX51_TX_BYTE__STATUS_B,
	EX51_TX_BYTE__CLK_DAY,
	EX51_TX_BYTE__CLK_HOUR,
	EX51_TX_BYTE__CLK_MIN,
	EX51_TX_BYTE__ARRIVAL_FLOOR,
	EX51_TX_BYTE__FUTURE,
	EX51_TX_BYTE__CHECKSUM,

	NUM_EX51_TX_BYTES
} en_ex51_tx_bytes;

typedef enum
{
	STATUS_A_BIT__UP_TRAVEL_DIR,
	STATUS_A_BIT__DOWN_TRAVEL_DIR,
	STATUS_A_BIT__UP_ARRIVAL_LANT,
	STATUS_A_BIT__DOWN_ARRIVAL_LANT,
	STATUS_A_BIT__GONG_ENA,
	STATUS_A_BIT__FUTURE_B1,
	STATUS_A_BIT__FUTURE_B2,
	STATUS_A_BIT__ALWAYS_ZERO,

	NUM_STATUS_A_BITS
} en_status_a_bits;

typedef enum
{
	STATUS_B_BIT__FRONT_DOOR,
	STATUS_B_BIT__BACK_DOOR,
	STATUS_B_BIT__FIRE_ICON,
	STATUS_B_BIT__OOS_ICON,
	STATUS_B_BIT__HOSPITAL_ICON,
	STATUS_B_BIT__INDEPENDENT_ICON,
	STATUS_B_BIT__FIRE_FLASH,
	STATUS_B_BIT__ALWAYS_ZERO,

	NUM_STATUS_B_BITS
} en_status_b_bits;
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static uint16_t uwResendTimer__ms;
static uint8_t aucTxBuffer[NUM_EX51_TX_BYTES];

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
static void LoadData_Floor(void)
{
	uint8_t ucFloor_Plus_1 = 0;
	uint8_t ucFloor = GetOperation_CurrentFloor();
	if( ucFloor < GetFP_NumFloors() )
	{
		ucFloor_Plus_1 = ucFloor + 1;
	}
	aucTxBuffer[EX51_TX_BYTE__FLOOR_PLUS1] = ucFloor_Plus_1;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData_Status(void)
{
	//Status A
	uint8_t ucStatusA = 0;
	enum direction_enum eMotionDir = GetMotion_Direction();

	if( eMotionDir == DIR__UP )
	{
		ucStatusA |= 1 << STATUS_A_BIT__UP_TRAVEL_DIR;			//Byte 3, Bit 0 set
	}
	else if( eMotionDir == DIR__DN )
	{
		ucStatusA |= 1 << STATUS_A_BIT__DOWN_TRAVEL_DIR;		//Byte 3, Bit 1 set
	}

	if( GetOutputValue(enOUT_ARV_DN_F) || GetOutputValue(enOUT_ARV_DN_R) )
	{
		ucStatusA |= 1 << STATUS_A_BIT__DOWN_ARRIVAL_LANT;	//Byte 3, Bit 3 set
		ucStatusA |= 1 << STATUS_A_BIT__GONG_ENA;			//Byte 3, Bit 4 set
	}

	else if( GetOutputValue(enOUT_ARV_UP_F) || GetOutputValue(enOUT_ARV_UP_R) )
	{
		ucStatusA |= 1 << STATUS_A_BIT__UP_ARRIVAL_LANT;	//Byte 3, Bit 2 set
		ucStatusA |= 1 << STATUS_A_BIT__GONG_ENA;			//Byte 3, Bit 4 set
	}


	aucTxBuffer[EX51_TX_BYTE__STATUS_A] = ucStatusA;	//Byte 3 set

	//Status B
	uint8_t ucStatusB = 0;
	enum en_mode_auto eAutoMode = GetOperation_AutoMode();
	enum en_mode_manual eManualMode = GetOperation_ManualMode();

	ucStatusB |= EX51_SETUP_FILE_FRONT_DOOR_BIT << STATUS_B_BIT__FRONT_DOOR;	//Byte 4, Bit 0 set
	ucStatusB |= EX51_SETUP_FILE_BACK_DOOR_BIT << STATUS_B_BIT__BACK_DOOR;		//Byte 4, Bit 1 set

	if( ( eAutoMode == MODE_A__FIRE1 ) || ( eAutoMode == MODE_A__FIRE2 ) || GetOutputValue(enOUT_LMP_FIRE) )
	{
		ucStatusB |= 1 << STATUS_B_BIT__FIRE_ICON;			//Byte 4, Bit 2 set
	}
	else if( GetFault_ByRange(FLT__GOV, NUM_FAULTS) || eManualMode != MODE_M__NONE )
	{
		ucStatusB |= 1 << STATUS_B_BIT__OOS_ICON;			//Byte 4, Bit 3 set
	}
	else if( ((eAutoMode == MODE_A__EMS1) || (eAutoMode == MODE_A__EMS2)) )
	{
		ucStatusB |= 1 << STATUS_B_BIT__HOSPITAL_ICON;		//Byte 4, Bit 4 set
	}
	else if( eAutoMode == MODE_A__INDP_SRV )
	{
		ucStatusB |= 1 << STATUS_B_BIT__INDEPENDENT_ICON;	//Byte 4, Bit 5 set
	}

	aucTxBuffer[EX51_TX_BYTE__STATUS_B] = ucStatusB;		//Byte 4 set
}
/*---------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData_Clock(void)
{
	aucTxBuffer[EX51_TX_BYTE__CLK_DAY] = RTC_GetDayOfWeek() + 1;	//Byte 5 set
	aucTxBuffer[EX51_TX_BYTE__CLK_HOUR] = RTC_GetTimeOfDay_Hour();	//Byte 6 set
	aucTxBuffer[EX51_TX_BYTE__CLK_MIN] = RTC_GetTimeOfDay_Min();	//Byte 7 Set
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData(void)
{
	aucTxBuffer[EX51_TX_BYTE__HEADER] = HEADER_BYTE_VALUE;		//Byte 1 set

	aucTxBuffer[EX51_TX_BYTE__ARRIVAL_FLOOR] = EX51_SETUP_FILE_INSTALLED_FLOOR_BYTE;	//Byte 8 set

	LoadData_Floor();
	LoadData_Status();
	LoadData_Clock();

	aucTxBuffer[EX51_TX_BYTE__FUTURE] = 0;		//Byte 9 set

	aucTxBuffer[EX51_TX_BYTE__CHECKSUM] = 0;
	for(uint8_t i = EX51_TX_BYTE__FLOOR_PLUS1; i < EX51_TX_BYTE__CHECKSUM; i++)
	{
		aucTxBuffer[EX51_TX_BYTE__CHECKSUM] += aucTxBuffer[i];	//Byte 10 set
	}
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void TransmitData(void)
{
	if( uwResendTimer__ms >= RESEND_RATE__MS )
	{
		if( UART_RS485_SendRB(&aucTxBuffer[0], NUM_EX51_TX_BYTES) )
		{
			uwResendTimer__ms = 0;
		}
	}
	else
	{
		uwResendTimer__ms += gstMod_EX51Fixture.uwRunPeriod_1ms;
	}
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10000;
   pstThisModule->uwRunPeriod_1ms = MOD_RUN_PERIOD_EX51_FIXTURE__MS;

   if( !Param_ReadValue_1Bit(enPARAM1__Enable_Janus_RS_Fixture) )
   {
      if( ( Param_ReadValue_1Bit(enPARAM1__EnableEX51_CT)  && ( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT  ) )
       || ( Param_ReadValue_1Bit(enPARAM1__EnableEX51_COP) && ( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP ) ) )
      {
    	  UART_RS485_DL20_Init();
      }
   }

   return 0;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   if( ( Param_ReadValue_1Bit(enPARAM1__EnableEX51_CT)  && ( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT  ) )
    || ( Param_ReadValue_1Bit(enPARAM1__EnableEX51_COP) && ( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP ) ) )
   {
      UpdateLocalChimeSignal(pstThisModule->uwRunPeriod_1ms);
      LoadData();

      TransmitData();
   }

   return 0;
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
