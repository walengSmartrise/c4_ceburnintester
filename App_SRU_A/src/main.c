/******************************************************************************
 *
 * @file     main.c
 * @brief    Program start point and main loop.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "sru_a.h"
#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
#include "board.h"

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static struct st_module * gastModules_CTA[] =
{
         &gstSys_Mod,  // must include the system module in addition to app modules
         &gstMod_Fault,
         &gstMod_FaultApp,
         &gstMod_Heartbeat,
         &gstMod_Watchdog,

         &gstMod_LocalInputs,
         &gstMod_CAN,
         &gstMod_UART,
         &gstMod_SData,
         &gstMod_ParamEEPROM,

         &gstMod_ParamApp,
         &gstMod_ParamCTA,
         &gstMod_ParamMaster_CTA,
         &gstMod_FPGA,
         &gstMod_RTC,

         &gstMod_AlarmApp,
         &gstMod_Position,
         &gstMod_Safety,
         &gstMod_EmotiveBoard,
         &gstMod_DL20Fixture,
		 &gstMod_EX51Fixture,
};

static struct st_module_control gstModuleControl_CTA =
{
   .uiNumModules = sizeof(gastModules_CTA) / sizeof(gastModules_CTA[ 0 ]),

   .pastModules = &gastModules_CTA,

   .enMCU_ID = enMCUA_SRU_Base,

   .enDeployment = enSRU_DEPLOYMENT__CT,
};
static struct st_module * gastModules_CTA_Pre[] =
{
         &gstSys_Mod,  // must include the system module in addition to app modules

         &gstMod_ParamEEPROM,
         &gstMod_ParamApp,
         &gstMod_ParamCTA,
         &gstMod_ParamMaster_CTA,
};

static struct st_module_control gstModuleControl_CTA_Pre =
{
   .uiNumModules = sizeof(gastModules_CTA_Pre) / sizeof(gastModules_CTA_Pre[ 0 ]),

   .pastModules = &gastModules_CTA_Pre,

   .enMCU_ID = enMCUA_SRU_Base,

   .enDeployment = enSRU_DEPLOYMENT__CT,
};

//------------------------------------------------------------------------------
static struct st_module * gastModules_COPA[] =
{
        &gstSys_Mod,  // must include the system module in addition to app modules
        &gstMod_Fault,
        &gstMod_FaultApp,
        &gstMod_Heartbeat,
        &gstMod_Watchdog,

        &gstMod_LocalInputs,
        &gstMod_CAN,
        &gstMod_UART,
        &gstMod_SData,
        &gstMod_ParamEEPROM,

        &gstMod_ParamApp,
        &gstMod_ParamCOPA,
        &gstMod_AlarmApp,
        &gstMod_RTC,
        &gstMod_FPGA,

        &gstMod_Safety,
        &gstMod_Position,
        &gstMod_ETSL,
        &gstMod_EmotiveBoard,
        &gstMod_DL20Fixture,
		&gstMod_EX51Fixture,
};

static struct st_module_control gstModuleControl_COPA =
{
   .uiNumModules = sizeof(gastModules_COPA) / sizeof(gastModules_COPA[ 0 ]),

   .pastModules = &gastModules_COPA,

   .enMCU_ID = enMCUA_SRU_Base,

   .enDeployment = enSRU_DEPLOYMENT__COP
};
static struct st_module * gastModules_COPA_Pre[] =
{
         &gstSys_Mod,  // must include the system module in addition to app modules

         &gstMod_ParamEEPROM,
         &gstMod_ParamApp,
         &gstMod_ParamCOPA,
};

static struct st_module_control gstModuleControl_COPA_Pre =
{
   .uiNumModules = sizeof(gastModules_COPA_Pre) / sizeof(gastModules_COPA_Pre[ 0 ]),

   .pastModules = &gastModules_COPA_Pre,

   .enMCU_ID = enMCUA_SRU_Base,

   .enDeployment = enSRU_DEPLOYMENT__COP,
};
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
struct st_module_control *gpstPreModuleControl_ThisDeployment;
struct st_module_control *gpstModuleControl_ThisDeployment;


/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void Deployment_Init( void )
{
   int iError = 1;  // assume error
   en_sru_deployments deplyment = GetSRU_Deployment();

   if ( deplyment == enSRU_DEPLOYMENT__CT )
   {
      gpstModuleControl_ThisDeployment = &gstModuleControl_CTA;

      gpstPreModuleControl_ThisDeployment = &gstModuleControl_CTA_Pre;
      // if( !GetOperation4_ParamSync() )
      // {
      //   gpstPreModuleControl_ThisDeployment = &gstModuleControl_CTA_Pre;
      // }

      // else
      // {
      //   gpstPreModuleControl_ThisDeployment = &gstModuleControl_CTA_Pre_Master;
      // }
      SetSystemNodeID(SYS_NODE__CTA);
      SharedData_Init();
      iError = 0;
   }
   else if ( deplyment == enSRU_DEPLOYMENT__COP )
   {
      gpstModuleControl_ThisDeployment = &gstModuleControl_COPA;
      gpstPreModuleControl_ThisDeployment = &gstModuleControl_COPA_Pre;
      SetSystemNodeID(SYS_NODE__COPA);
      SharedData_Init();
      iError = 0;
   }


   if ( iError )
   {
      while ( 1 )
      {
         static uint32_t uiCounter, bState;

         if( uiCounter >= 0xfffff )
         {
            uiCounter = 0;

            bState = ( bState )? 0: 1;

            SRU_Write_LED( enSRU_LED_Heartbeat, bState );
            SRU_Write_LED( enSRU_LED_Alarm,     bState );
            SRU_Write_LED( enSRU_LED_Fault,     bState );
         }
         else
         {
            uiCounter++;
         }
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
int main( void )
{
   SystemCoreClockUpdate();
   Sys_Init();

   MCU_A_Init();

   Deployment_Init();

   __enable_irq();

   // These are the modules that need to be started before the main application.
   // Right now this is just parameters. This is because most modules require
   // parameters and the parameters are not valid till they are loaded into RAM
   Mod_InitAllModules( gpstPreModuleControl_ThisDeployment );
   while(!GetFP_FlashParamsReady())
   {
       Mod_RunOneModule( gpstPreModuleControl_ThisDeployment );
   }

   Mod_InitAllModules( gpstModuleControl_ThisDeployment );

   // This is really a while(1) loop. Sys_Shutdown() always returns 0. The
   // function is being provided in anticipation of a future time when this
   // application might be running on a PC simulator and we would need a
   // mechanism to terminate this application.

   while ( !Sys_Shutdown() )
   {
      Mod_RunOneModule( gpstModuleControl_ThisDeployment );

      uint8_t ucCurrModIndex = gpstModuleControl_ThisDeployment->uiCurrModIndex;
      uint8_t ucNumOfMods = gpstModuleControl_ThisDeployment->uiNumModules;

      struct st_module * (* pastModules)[] = gpstModuleControl_ThisDeployment->pastModules;
      struct st_module * pstCurrentModule = (*pastModules)[ ucCurrModIndex ];

      if( ( pstCurrentModule->bTimeViolation )
       && ( ucCurrModIndex < ucNumOfMods )
       && ( ( Param_ReadValue_8Bit(enPARAM8__TimeViolationModule) == MRC__ALL )
         || ( ( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT  ) && ( ( Param_ReadValue_8Bit(enPARAM8__TimeViolationModule)-MRC__CTA_START  ) == ucCurrModIndex ) )
         || ( ( GetSRU_Deployment() == enSRU_DEPLOYMENT__COP ) && ( ( Param_ReadValue_8Bit(enPARAM8__TimeViolationModule)-MRC__COPA_START ) == ucCurrModIndex ) ) ) )
      {
         if( GetSRU_Deployment() == enSRU_DEPLOYMENT__CT )
         {
            SetAlarm( ALM__RUN_TIME_FAULT_CTA + ucCurrModIndex - 1);
         }
         else
         {
            SetAlarm( ALM__RUN_TIME_FAULT_COPA + ucCurrModIndex - 1);
         }
      }

      Debug_CAN_MonitorUtilization();
   }
   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
