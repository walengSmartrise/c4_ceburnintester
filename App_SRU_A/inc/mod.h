/******************************************************************************
 *
 * @file     mod.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef MOD_H
#define MOD_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
#include "fram_data.h"
#include "fault_app.h"
#include "alarm_app.h"
#include "shared_data.h"
#include "datagrams.h"
#include "sru_a.h"
#include "sru.h"
#include "motion.h"
#include "operation.h"
#include "position.h"
#include "mod_etsl.h"
#include "mod_DL20.h"
#include "mod_EX51.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_CTRL_NET_1MS        (5U)
#define MOD_RUN_PERIOD_CAR_NET_1MS         (5U)
#define MOD_RUN_PERIOD_AB_NET_1MS          (5U)
#define MOD_RUN_PERIOD_UART_1MS              (5U)
#define MOD_RUN_PERIOD_CAN_1MS              (5U)
#define MOD_RUN_PERIOD_FAULT_APP_1MS         (5U)
#define MOD_RUN_PERIOD_FAULT_1MS             (5U)
#define MOD_RUN_PERIOD_POSITION_1MS          (5U)
#define MOD_RUN_PERIOD_SAFETY_1MS             (10U)
#define MOD_RUN_PERIOD_FPGA_1MS             (5U)
#define MOD_RUN_PERIOD_EMOTIVE_BOARD_1MS             (5U)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

enum en_fire_lamp
{
   FireLamp__OFF,
   FireLamp__ON,
   FireLamp__FLASHING,

   NUM_FIRE_LAMP_STATES
};

/* There will be an instance of this struct for every floor.
 * I am using bitfields to save RAM, keeping each struct's size to one byte */
struct st_floors
{
   uint32_t ulPosition : 24;
   uint8_t bFrontOpening : 1;
   uint8_t bRearOpening : 1;
};

struct st_hallcalls
{
   uint8_t bRecentlyPressed_Up: 1;   //flag for recently pressed Up from the hall board
   uint8_t bRecentlyPressed_Down: 1; //flag for recently Pressed Down from the hall board
   uint8_t bNowPressed_Up: 1;   //flag for now pressed Up from the hall board
   uint8_t bNowPressed_Down: 1; //flag for now Pressed Down from the hall board
   uint8_t bNowLamp_Up: 1;   //flag for lamp now on from the hall board
   uint8_t bNowLamp_Down: 1; //flag for lamp now on Down from the hall board

   uint8_t bCommand_Down: 1; //command down for the down hall call lamp
   uint8_t bCommand_Up: 1; //command up will be used for the hall call lamp
   uint8_t bLatched_Up: 1; //latched up will be used for the hall call lamp
   uint8_t bLatched_Down: 1; //latched down for the down hall call lamp

   uint8_t bFlashLamp: 1;
   uint8_t bSecured: 1;
};

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

extern struct st_module_control *gpstModuleControl_ThisDeployment;

extern struct st_sdata_control gstSData_LocalData;

extern struct st_module gstMod_CAN;

extern struct st_module gstMod_UART;

extern struct st_module gstMod_LocalInputs;
extern struct st_module gstMod_Fault;
extern struct st_module gstMod_Heartbeat;

extern struct st_module gstMod_ParamCTA;
extern struct st_module gstMod_ParamCOPA;
extern struct st_module gstMod_ParamMaster_CTA;
extern struct st_module gstMod_ParamApp;
extern struct st_module gstMod_Watchdog;
extern struct st_module gstMod_FaultApp;
extern struct st_module gstMod_FPGA;

extern struct st_module gstMod_Position;

extern struct st_module gstMod_SData;

extern struct st_module gstMod_Safety;

extern struct st_module gstMod_EmotiveBoard;

extern st_fault  gstFault;

extern char *pasVersion;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
uint8_t LatchCarCall ( uint8_t ucFloorIndex, uint8_t ucDoorIndex) ;

uint32_t GetLocalInputs_ThisNode(void);

void Position_COPA(void);
void Position_CTA(void);

Status UART_LoadCANMessage(CAN_MSG_T *pstTxMsg);
uint16_t UART_GetRxErrorCount(void);

uint8_t CheckIf_SendOnABNet(uint16_t uwDestinationBitmap);
uint8_t CheckIf_SendOnCarNet( uint16_t uwDestinationBitmap );

void SharedData_Init();

void LoadData_CT();
void LoadData_COP();

void UnloadData_CT();
void UnloadData_COP();

void Run_ModSafety_CT();
void Run_ModSafety_COP();

uint8_t CheckIfOffline_SRUB();

uint32_t GetPosition( void );

/*-----------------------------------------------------------------------------
Returns bus error counter
 -----------------------------------------------------------------------------*/
uint16_t GetDebugBusOfflineCounter_CAN1();
uint16_t GetDebugBusOfflineCounter_CAN2();
#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
