
#ifndef GLOBAL_DATA_H
#define GLOBAL_DATA_H

#include <stdint.h>
#include "sys.h"
#include "fram_data.h"
#include "sru.h"
#include "shared_data.h"

#define LOWER_RELEVELING_THRESHHOLD (13)
#define UPPER_RELEVELING_THRESHHOLD (100)
/*-----------------------------------------------------------------------------
   SRU
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
   Parameters
 -----------------------------------------------------------------------------*/
uint8_t GetFP_FlashParamsReady();
void SetFP_FlashParamsReady();
uint8_t GetFP_NumFloors(void);
void SetFP_NumFloors(uint8_t ucFloors);
uint8_t GetFP_RearDoors(void);
void SetFP_RearDoors(uint8_t bDoor);
en_door_type GetFP_DoorType( enum en_doors eDoor );
void SetFP_DoorType( en_door_type enDoorType, enum en_doors eDoor );
uint8_t GetFP_FreightDoors(void);
void SetFP_FreightDoors(uint8_t bDoor);
uint16_t GetFP_ContractSpeed( void );
void SetFP_ContractSpeed( uint16_t uwSpeed );
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetInputBitMap(uint32_t ulBitmap, uint8_t ucIndex);
void SetOutputBitMap(uint32_t ulBitmap, uint8_t ucIndex);
uint32_t GetInputBitMap(uint8_t ucIndex);
uint32_t GetOutputBitMap(uint8_t ucIndex);
uint8_t GetInputValue(enum en_input_functions enInput);
uint8_t GetOutputValue(enum en_output_functions enOutput);

void SetStructPosition_NTS(struct st_position_b *pstPosition);
void GetStructPosition_NTS(struct st_position_b *pstPosition);

uint32_t GetPositionReference(void);

void SetNTSPosition(uint32_t ulPosition);
uint32_t GetNTSPostion();
void SetNTSVelocity(int16_t wVelocity);
int16_t GetNTSVelocity();

/*-----------------------------------------------------------------------------
   UI Requests
 -----------------------------------------------------------------------------*/
void InitCarCallQue ( void );
uint8_t SetCarCall (uint8_t ucFloorIndex, uint8_t ucDoorIndex);
uint8_t GetCarCallRequest ( enum en_doors eDoor );

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetBitMap_LatchedCCB(uint32_t ulCarCalls, uint8_t ucIndex, uint8_t bRear);
uint32_t GetBitMap_LatchedCCB(uint8_t ucIndex, uint8_t bRear);

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/



void SetModState_AutoState(uint8_t ucState );
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetArrivalUpDn(uint8_t ucArrivalUpDn);
uint8_t GetArrivalUpDn();

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t GetModState_MotionState();
uint8_t GetModState_MotionPattern();
uint8_t GetModState_MotionStop();
uint8_t GetModState_MotionStart();


uint8_t GetModState_AutoState();
void SetModState_FloorLearnState(uint8_t ucState );
uint8_t GetModState_FloorLearnState();

//void SetModState_DoorState( enum en_door_states ucDoorState );
//uint8_t GetModState_DoorState(void);
void SetModState_FireSrv(uint8_t ucState );
uint8_t GetModState_FireSrv();
void SetModState_FireSrv2(uint8_t ucState );
uint8_t GetModState_FireSrv2();
void SetModState_Counterweight(uint8_t ucState );
uint8_t GetModState_Counterweight();
void SetModState_Recall(uint8_t ucState );
uint8_t GetModState_Recall();
/*-----------------------------------------------------------------------------
Doors
 -----------------------------------------------------------------------------*/
enum en_door_states GetDoorState_Front();
enum en_door_states GetDoorState_Rear();

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint16_t GetEmergencyBitmap();
uint8_t GetEmergencyBit( enum en_emergency_bitmap enEmergencyBF );
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetAcceptanceTest_Plus1( uint32_t uiIndex_Plus1 );
uint32_t GetAcceptanceTest_Plus1();

/*----------------------------------------------------------------------------
   Motion Flag access functions
 *----------------------------------------------------------------------------*/
uint8_t GetPreflightFlag();
/*-----------------------------------------------------------------------------
   Returns 1 if valid opening
 ----------------------------------------------------------------------------*/
uint8_t GetFloorOpening_Front( uint8_t ucFloor );
uint8_t GetFloorOpening_Rear( uint8_t ucFloor );

/*----------------------------------------------------------------------------
For GetGroup_DispatchData()
   b0:    Any Door Open
   b1-b2: Priority_Plus1
   b3:    Hall Calls Disabled
   b4:    In Slowdown
   b5:    Suppress Reopen
   b6:    Car Out Of Group
   b7:    bIdleDirection
 *----------------------------------------------------------------------------*/
uint8_t HallCallsDisabled ( void );
uint8_t CheckIf_CarOutOfGroup ();
uint8_t GetCarPriority();
uint8_t CheckIf_IdleDirection(void);
uint8_t CheckIf_InSlowdown(void);
uint8_t CheckIf_SuppressReopen(void);

/*-----------------------------------------------------------------------------
   Create a localized version of the chime output to avoid missing the passing
   chime when running at high speeds
 -----------------------------------------------------------------------------*/
void UpdateLocalChimeSignal( uint16_t uwRunPeriod_ms );
uint8_t GetLocalChimeSignal(void);

/*-----------------------------------------------------------------------------
   For spoofing displayed car mode as normal operation when car is allowed to run on e-power
 -----------------------------------------------------------------------------*/
uint8_t CheckIf_EPowerCarInNormal(void);

/*-----------------------------------------------------------------------------
   View Debug Data Requests
 ----------------------------------------------------------------------------*/
en_view_debug_data GetUIRequest_ViewDebugDataCommand(void);
/*-----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
uint8_t GetOverLoadFlag(void);
uint8_t GetFullLoadFlag(void);
uint8_t GetLightLoadFlag(void);
/*-----------------------------------------------------------------------------
   Check if car is inside virtual dead zone
 ----------------------------------------------------------------------------*/
uint8_t InsideDeadZone( void );
/*-----------------------------------------------------------------------------

 ----------------------------------------------------------------------------*/
void UpdateBypassInCarStopSwitchFlag(void);
uint8_t GetBypassInCarStopSwitchFlag(void);
#endif // GLOBAL_DATA_H
