/******************************************************************************
 *
 * @file     mod_DL20.h
 * @brief
 * @version  V1.00
 * @date     29, June 2018
 * @author   Keith Soneda
 *
 * @note     Performs ETSL checks
 *
 ******************************************************************************/

#ifndef _MOD_DL20_H_
#define _MOD_DL20_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_module gstMod_DL20Fixture;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
uint8_t DL20_GetPhoneFailureFlag(void);
uint8_t DL20_GetOOSFlag(void);

uint16_t DL20_GetErrorCounter_Rx(void);
uint16_t DL20_GetErrorCounter_Tx(void);

#endif
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
