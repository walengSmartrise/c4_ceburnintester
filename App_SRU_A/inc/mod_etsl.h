/******************************************************************************
 *
 * @file     mod_etsl.h
 * @brief
 * @version  V1.00
 * @date     29, June 2018
 * @author   Keith Soneda
 *
 * @note     Performs ETSL checks
 *
 ******************************************************************************/

#ifndef _MOD_ETSL_H_
#define _MOD_ETSL_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_ETSL_1MS                    ( 10U )

#define MAX_DEBOUNCE_LIMIT_OVERSPEED_ETSL          ( 100 )

// Scale factor is set to 10%.
#define ETSL_VELOCITY_SCALE_FACTOR                 ( 1.1 )
#define ETSL_VELOCITY_SCALE_FACTOR_TEST                 ( 0.9 )
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_module gstMod_ETSL;
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
int16_t ETSL_GetSpeed();
uint32_t ETSL_GetPosition();
void ETSL_SetSpeed(int16_t wSpeed);
void ETSL_SetPosition(uint32_t uiPosition);

#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
