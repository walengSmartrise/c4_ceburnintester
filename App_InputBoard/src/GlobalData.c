
#include "mcu_inputBoard.h"
#include "sru.h"
#include "GlobalData.h"
#include <stdint.h>
#include "sys.h"

static uint8_t gucIOExt_Master;
/*----------------------------------------------------------------------------
   If no slave DIPS are set DIP[1,3], then this is a slave EXP board. Returns 0.
   Else, this is a master IO board with ID set by DIP[4,7] + 1
 *----------------------------------------------------------------------------*/
inline void SetSRU_ExpansionMasterID( void )
{
   uint8_t ucDIP_Switches = SRU_Read_DIP_Bank( enSRU_DIP_BANK_B );
   uint8_t ucSlaveBits = ucDIP_Switches & 0x7;
   uint8_t ucMasterBits = ( ucDIP_Switches >> 3 ) & 0xF;
   if( !ucSlaveBits || SRU_Read_DIP_Bank( enSRU_DIP_BANK_B )== 0xFF )
   {
      gucIOExt_Master = ucMasterBits+1;
   }
   else
   {
      gucIOExt_Master = 0;
   }
}
/*----------------------------------------------------------------------------
   Returns IO board Master ID
 *----------------------------------------------------------------------------*/
inline uint8_t GetSRU_ExpansionMasterID( void )
{
     return gucIOExt_Master;
}
/*----------------------------------------------------------------------------
   Returns IO board slave ID - DIP numbers [1,3]
 *----------------------------------------------------------------------------*/
inline uint8_t GetSRU_ExpansionSlaveID()
{
   uint8_t ucDIP_Switches = SRU_Read_DIP_Bank( enSRU_DIP_BANK_B ) & 0x7;
   if(ucDIP_Switches)
   {
      ucDIP_Switches -=1;
   }
   else if(gucIOExt_Master)
   {
      ucDIP_Switches = 0;
   }

   return ucDIP_Switches;
}

/*----------------------------------------------------------------------------
UNUSED for input board
 *----------------------------------------------------------------------------*/
static enum en_group_net_nodes eRiserID = GROUP_NET__RIS_1;
inline void SetSRU_RiserBoardID()
{
   eRiserID = ( SRU_Read_DIP_Bank( enSRU_DIP_BANK_B ) & 0x03 ) + GROUP_NET__RIS_1;
}
inline enum en_group_net_nodes GetSRU_RiserBoardID() //TODO add fault for change
{
   return eRiserID;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
static en_io_board_type eIOBoardType;
inline en_io_board_type GetIOBoardType()
{
   return eIOBoardType;
}
inline void SetIOBoardType(en_io_board_type eBoardType)
{
   eIOBoardType = eBoardType;
}
