/******************************************************************************
 *
 * @file     mod_shared_data.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     26, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "mod.h"
#include "sru.h"
#include "mcu_inputBoard.h"
#include "sys.h"
#include "GlobalData.h"
#include "sdata.h"
#include "expData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_SData_CarNet =
{
   .pfnInit = Init,
   .pfnRun = Run,
};


struct st_sdata_control gstSData_CarNetData;

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static Sys_Nodes eSysNodeID;// network id
static uint8_t ucNumberOfDatagrams;// number of datagrams generated locally
static CAN_MSG_T gstCAN_MSG_Tx;

static Datagram_ID eTxNetworkDatagramID;

static uint8_t aucSlaveOutputs[NUM_IONet_Slave_DATAGRAMS];
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t GetSlaveOutputs( uint8_t ucIndex )
{
   uint8_t ucOuputs = 0;
   if( ucIndex < NUM_IONet_Slave_DATAGRAMS )
   {
      ucOuputs = aucSlaveOutputs[ucIndex];
   }
   return ucOuputs;
}
/*-----------------------------------------------------------------------------

   Initialize shared data used by MCU_A.

 -----------------------------------------------------------------------------*/
void SharedData_CarNet_Init( int iSRU_Deployment )
{
   int iError = 1;

   //--------------------------------------------------------
   eSysNodeID = GetSystemNodeID();

   if ( eSysNodeID == SYS_NODE__EXP )
   {
      switch(GetSRU_ExpansionMasterID())
      {
         case 1:
         case 2:
         case 3:
         case 4:
         case 5:
         case 6:
         case 7:
         case 8:
         case 9:
         case 10:
         case 11:
         case 12:
         case 13:
         case 14:
         case 15:
         case 16:
            ucNumberOfDatagrams = 2;// Start with 7 slave IO boards per master
            break;
         case 0:
         default:
            ucNumberOfDatagrams = 0;
            break;
      }
   }

   //--------------------------------------------------------
   if(ucNumberOfDatagrams)
   {
      iError = SDATA_CreateDatagrams( &gstSData_CarNetData,
                                       ucNumberOfDatagrams
                                    );
   }

   if ( iError )
   {
      while ( 1 )
      {
         // TODO: unable to allocate shared data
         SRU_Write_LED( enSRU_LED_Fault, 1 );
      }
   }
}
/*-----------------------------------------------------------------------------
   Determines the network datagram id by the exp master id
 -----------------------------------------------------------------------------*/
Datagram_ID GetExpNetworkDatagramID()
{
   Datagram_ID eDatagramID = NUM_DATAGRAM_IDs;//invalid
   switch(GetSRU_ExpansionMasterID())
   {
      case 1:
         eDatagramID = DATAGRAM_ID_29;
         break;
      case 2:
         eDatagramID = DATAGRAM_ID_30;
         break;
      case 3:
         eDatagramID = DATAGRAM_ID_31;
         break;
      case 4:
         eDatagramID = DATAGRAM_ID_32;
         break;
      case 5:
		   eDatagramID = DATAGRAM_ID_33;
		   break;
      case 6:
		   eDatagramID = DATAGRAM_ID_34;
		   break;
      case 7:
		   eDatagramID = DATAGRAM_ID_35;
		   break;
      case 8:
		   eDatagramID = DATAGRAM_ID_36;
		   break;
      case 9:
		   eDatagramID = DATAGRAM_ID_37;
		   break;
      case 10:
		   eDatagramID = DATAGRAM_ID_38;
		   break;
      case 11:
		   eDatagramID = DATAGRAM_ID_39;
		   break;
      case 12:
		   eDatagramID = DATAGRAM_ID_40;
		   break;
      case 13:
		   eDatagramID = DATAGRAM_ID_41;
		   break;
      case 14:
		   eDatagramID = DATAGRAM_ID_42;
		   break;
      case 15:
		   eDatagramID = DATAGRAM_ID_43;
		   break;
      case 16:
         eDatagramID = DATAGRAM_ID_44;
         break;

      case 0:
      default:
         break;
   }
   return eDatagramID;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
Datagram_ID GetExpDipComNetworkDatagramID()
{
   Datagram_ID eDatagramID = NUM_DATAGRAM_IDs;//invalid
   switch(GetSRU_ExpansionMasterID())
   {
      case 1:
         eDatagramID = DATAGRAM_ID_198;
         break;
      case 2:
         eDatagramID = DATAGRAM_ID_199;
         break;
      case 3:
         eDatagramID = DATAGRAM_ID_200;
         break;
      case 4:
         eDatagramID = DATAGRAM_ID_201;
         break;
      case 5:
		   eDatagramID = DATAGRAM_ID_202;
		   break;
      case 6:
		   eDatagramID = DATAGRAM_ID_203;
		   break;
      case 7:
		   eDatagramID = DATAGRAM_ID_204;
		   break;
      case 8:
		   eDatagramID = DATAGRAM_ID_205;
		   break;
      case 9:
		   eDatagramID = DATAGRAM_ID_206;
		   break;
      case 10:
		   eDatagramID = DATAGRAM_ID_207;
		   break;
      case 11:
		   eDatagramID = DATAGRAM_ID_208;
		   break;
      case 12:
		   eDatagramID = DATAGRAM_ID_209;
		   break;
      case 13:
		   eDatagramID = DATAGRAM_ID_210;
		   break;
      case 14:
		   eDatagramID = DATAGRAM_ID_211;
		   break;
      case 15:
		   eDatagramID = DATAGRAM_ID_212;
		   break;
      case 0:
      default:
         break;
   }
   return eDatagramID;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Transmit( void )
{

#define DG_TX_CYCLE_LIMIT  (2)
   un_sdata_datagram unDatagram;

   int iError_CAN = 0;
   uint8_t ucLocalDatagramID;
   uint8_t ucNetworkID = SDATA_NET__CAR;
   uint16_t uwDatagramToSend_Plus1;
   uint8_t ucTXCycle;

   for( ucTXCycle = 0; ucTXCycle < DG_TX_CYCLE_LIMIT; ucTXCycle++)
   {
      uwDatagramToSend_Plus1 = SDATA_GetNextDatagramToSendIndex_Plus1( &gstSData_CarNetData );

      if(uwDatagramToSend_Plus1)
      {
         ucLocalDatagramID = uwDatagramToSend_Plus1 - 1;

         if ( ucLocalDatagramID < ucNumberOfDatagrams )
         {
               SDATA_ReadDatagram( &gstSData_CarNetData,
                                    ucLocalDatagramID,
                                   &unDatagram
                                 );

               Sys_Nodes eLocalNodeID = GetSystemNodeID();

//               Datagram_ID eDatagramID = GetNetworkDatagramID_AnyBoard(ucLocalDatagramID);
               if(ucLocalDatagramID)
               {
            	   eTxNetworkDatagramID = GetExpDipComNetworkDatagramID();
               }else
               {
            	   eTxNetworkDatagramID = GetExpNetworkDatagramID();
               }

               uint16_t uwDestinationBitmap = GetDatagramDestinationBitmap(eTxNetworkDatagramID);

               memcpy( gstCAN_MSG_Tx.Data, unDatagram.auc8, sizeof( unDatagram.auc8 ) );
               gstCAN_MSG_Tx.DLC = 8;
               gstCAN_MSG_Tx.Type = 0;
               gstCAN_MSG_Tx.ID = 1 << 30;
               gstCAN_MSG_Tx.ID |= eTxNetworkDatagramID << 21;
               gstCAN_MSG_Tx.ID |= (ucNetworkID & 0x07) << 18;
               gstCAN_MSG_Tx.ID |= (eLocalNodeID & 0x0F) << 14;
               gstCAN_MSG_Tx.ID |= uwDestinationBitmap;

               iError_CAN = !CAN1_LoadToRB(&gstCAN_MSG_Tx);

               if ( iError_CAN )
               {
                  SDATA_DirtyBit_Set( &gstSData_CarNetData, ucLocalDatagramID );

                  if ( gstSData_CarNetData.uwLastSentIndex )
                  {
                     --gstSData_CarNetData.uwLastSentIndex;
                  }
                  else
                  {
                     gstSData_CarNetData.uwLastSentIndex = gstSData_CarNetData.uiNumDatagrams - 1;
                  }
                  ucTXCycle = DG_TX_CYCLE_LIMIT;
                  break;
               }
            }
            else
            {
               SDATA_DirtyBit_Clr( &gstSData_CarNetData, ucLocalDatagramID );
            }
         }
      }

   // Polls ring buffer for data and loads into CAN hardware buffer
   CAN1_FillHardwareBuffer();
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void LoadData( void )
{
   un_sdata_datagram unDatagram;
   uint8_t ucDatagramID = 0;// Always zero, mapped to network ID on transmit
   memset( &unDatagram, 0, sizeof( un_sdata_datagram ) );
   ExpData_LoadInputStatusMessage(&unDatagram);
   SDATA_WriteDatagram( &gstSData_CarNetData,
                        ucDatagramID,
                        &unDatagram
                      );


   /* Load the EXP status datagram */
   ucDatagramID = 1;
   memset( &unDatagram, 0, sizeof( un_sdata_datagram ) );
   ExpData_LoadMasterStatusMessage(&unDatagram);
   SDATA_WriteDatagram( &gstSData_CarNetData,
                        ucDatagramID,
                        &unDatagram
                      );
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadData_MasterIO1()
{
   uint32_t uiLocalInputs_ThisNode = 0;

   uiLocalInputs_ThisNode = (GetLocalOutputs2_Output1_MRA()) |
                            (GetLocalOutputs2_Output2_MRA() << 8) |
                            (GetLocalOutputs2_Output3_MRA() << 16);

   SetLocalOutputs_ThisNode(uiLocalInputs_ThisNode);
   aucSlaveOutputs[0] = GetLocalOutputs2_Output2_MRA();
   aucSlaveOutputs[1] = GetLocalOutputs2_Output3_MRA();
   aucSlaveOutputs[2] = GetLocalOutputs2_Output4_MRA();
   aucSlaveOutputs[3] = GetLocalOutputs2_Output5_MRA();
   aucSlaveOutputs[4] = GetLocalOutputs2_Output6_MRA();
   aucSlaveOutputs[5] = GetLocalOutputs2_Output7_MRA();
   aucSlaveOutputs[6] = GetLocalOutputs2_Output8_MRA();
}
static void UnloadData_MasterIO2()
{
   uint32_t uiLocalInputs_ThisNode = 0;

   uiLocalInputs_ThisNode = (GetLocalOutputs3_Output1_MRA()) |
                            (GetLocalOutputs3_Output2_MRA() << 8) |
                            (GetLocalOutputs3_Output3_MRA() << 16);

   SetLocalOutputs_ThisNode(uiLocalInputs_ThisNode);
   aucSlaveOutputs[0] = GetLocalOutputs3_Output2_MRA();
   aucSlaveOutputs[1] = GetLocalOutputs3_Output3_MRA();
   aucSlaveOutputs[2] = GetLocalOutputs3_Output4_MRA();
   aucSlaveOutputs[3] = GetLocalOutputs3_Output5_MRA();
   aucSlaveOutputs[4] = GetLocalOutputs3_Output6_MRA();
   aucSlaveOutputs[5] = GetLocalOutputs3_Output7_MRA();
   aucSlaveOutputs[6] = GetLocalOutputs3_Output8_MRA();
}
static void UnloadData_MasterIO3()
{
   uint32_t uiLocalInputs_ThisNode = 0;

   uiLocalInputs_ThisNode = (GetLocalOutputs4_Output1_MRA()) |
                            (GetLocalOutputs4_Output2_MRA() << 8) |
                            (GetLocalOutputs4_Output3_MRA() << 16);

   SetLocalOutputs_ThisNode(uiLocalInputs_ThisNode);
   aucSlaveOutputs[0] = GetLocalOutputs4_Output2_MRA();
   aucSlaveOutputs[1] = GetLocalOutputs4_Output3_MRA();
   aucSlaveOutputs[2] = GetLocalOutputs4_Output4_MRA();
   aucSlaveOutputs[3] = GetLocalOutputs4_Output5_MRA();
   aucSlaveOutputs[4] = GetLocalOutputs4_Output6_MRA();
   aucSlaveOutputs[5] = GetLocalOutputs4_Output7_MRA();
   aucSlaveOutputs[6] = GetLocalOutputs4_Output8_MRA();
}

static void UnloadData_MasterIO4()
{
   uint32_t uiLocalInputs_ThisNode = 0;

   uiLocalInputs_ThisNode = (GetLocalOutputs5_Output1_MRA()) |
                            (GetLocalOutputs5_Output2_MRA() << 8) |
                            (GetLocalOutputs5_Output3_MRA() << 16);

   SetLocalOutputs_ThisNode(uiLocalInputs_ThisNode);
   aucSlaveOutputs[0] = GetLocalOutputs5_Output2_MRA();
   aucSlaveOutputs[1] = GetLocalOutputs5_Output3_MRA();
   aucSlaveOutputs[2] = GetLocalOutputs5_Output4_MRA();
   aucSlaveOutputs[3] = GetLocalOutputs5_Output5_MRA();
   aucSlaveOutputs[4] = GetLocalOutputs5_Output6_MRA();
   aucSlaveOutputs[5] = GetLocalOutputs5_Output7_MRA();
   aucSlaveOutputs[6] = GetLocalOutputs5_Output8_MRA();
}
static void UnloadData_MasterIO5()
{
   uint32_t uiLocalInputs_ThisNode = 0;

   uiLocalInputs_ThisNode = (GetLocalOutputs6_Output1_MRA()) |
                            (GetLocalOutputs6_Output2_MRA() << 8) |
                            (GetLocalOutputs6_Output3_MRA() << 16 );

   SetLocalOutputs_ThisNode(uiLocalInputs_ThisNode);
   aucSlaveOutputs[0] = GetLocalOutputs6_Output2_MRA();
   aucSlaveOutputs[1] = GetLocalOutputs6_Output3_MRA();
   aucSlaveOutputs[2] = GetLocalOutputs6_Output4_MRA();
   aucSlaveOutputs[3] = GetLocalOutputs6_Output5_MRA();
   aucSlaveOutputs[4] = GetLocalOutputs6_Output6_MRA();
   aucSlaveOutputs[5] = GetLocalOutputs6_Output7_MRA();
   aucSlaveOutputs[6] = GetLocalOutputs6_Output8_MRA();
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadData( void )
{
   uint8_t ucMasterID = GetSRU_ExpansionMasterID();

   switch(ucMasterID)
   {
      case 1:
         UnloadData_MasterIO1();
         break;
      case 2:
         UnloadData_MasterIO2();
         break;
      case 3:
         UnloadData_MasterIO3();
         break;
      case 4:
         UnloadData_MasterIO4();
         break;
      case 5:
         UnloadData_MasterIO5();
         break;

      default:
         break;
   }
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   //------------------------------------------------
   pstThisModule->uwInitialDelay_1ms = 5;
   pstThisModule->uwRunPeriod_1ms = 5;

   return 0;
}

/*-----------------------------------------------------------------------------

                  MSB   x
                        x     4 bits for Network ID (global enum)
                        x
                        x
                      -----
                        x
                        x
                        x    5 bits for Source Node ID (static enum to use less bits)
  29-bit CAN ID:        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Destination Node ID (0x1F = Broadcast to all)
                        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Datagram ID (global enum)
                        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Version Number (major)
                        x
                        x
                      -----
                        x
                        x
                        x     5 bits for Version Number (minor)
                        x
                  LSB   x

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   UnloadData();

   LoadData();
   Transmit();

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
