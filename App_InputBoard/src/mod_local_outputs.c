/******************************************************************************
 *
 * @file     mod_local_outputs.c
 * @brief    Logic that scans the local inputs connected to his processor.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"

#include <stdint.h>
#include "sru.h"
#include "mcu_inputBoard.h"
#include "sys.h"
#include "riser.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_LocalOutputs =
{
   .pfnInit = Init,
   .pfnRun = Run,
};

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/

static uint32_t uiLocalOutputs_ThisNode;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SetLocalOutputs_ThisNode( uint32_t uiOutputs )
{
   uiLocalOutputs_ThisNode = uiOutputs;
}
uint32_t GetLocalOutputs_ThisNode( void )
{
   return uiLocalOutputs_ThisNode;
}
/*-----------------------------------------------------------------------------
   Bit Reversal
 -----------------------------------------------------------------------------*/
static uint32_t BitReversal( uint32_t uiLocalInputs_ThisNode )
{
   uint32_t uiReversedBits = 0;

   for(enum en_sru_inputs enInput = enSRU_Input_501; enInput <= enSRU_Input_524; enInput++)
   {
      uint8_t bValue = Sys_Bit_Get(&uiLocalInputs_ThisNode, enInput); 
      Sys_Bit_Set(&uiReversedBits, enSRU_Output_624 - enInput, bValue);
   }

   return uiReversedBits;
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{

   //------------------------
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = 10;

   return 0;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   enum en_sru_outputs enOutput;

   /* Mirror inputs if in test mode */
   if( GetSRU_TestMode() )
   {
      uiLocalOutputs_ThisNode = BitReversal( GetLocalInputs_ThisNode() );
   }

   /* Turn off outputs if overcurrent input is high */
   if(GetLocalInputs_ThisNode() & 0x1000000) //Overcurrent
   {
      SetFault(ERROR_RIS_EXP__DRIVER_FLT);
	   SRU_Write_Output(enSRU_Output_OE,0);
   }
   else
   {
	   SRU_Write_Output(enSRU_Output_OE,1);
   }

   for ( enOutput = enSRU_Output_601; enOutput <= enSRU_Output_624; ++enOutput )
   {
      uint_fast8_t bValue = Sys_Bit_Get( &uiLocalOutputs_ThisNode, enOutput );
      SRU_Write_Output( enOutput, bValue );
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
