/******************************************************************************
 *
 * @file     main.c
 * @brief    Program start point and main loop.
 * @version  V1.00
 * @date     19, March 2016
 *
 * @note    Main loop for SR-3041 24-Input board. This board acts as 3 expansion boards.
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mcu_inputBoard.h"
#include "mod.h"
#include "sru.h"
#include <stdint.h>
#include "sys.h"
#include "GlobalData.h"
/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
static struct st_module * gastModules_CNA_Master[] =
{
   &gstSys_Mod,  // must include the system module in addition to app modules

   &gstMod_CAN,
   &gstMod_LocalInputs,
   &gstMod_LocalOutputs,
   &gstMod_Heartbeat,
   &gstMod_SData_CarNet,
   &gstMod_SData_IONet,
   &gstMod_Watchdog,
   &gstMod_Fault,
};

static struct st_module_control gstModuleControl_CNA_Master =
{
   .uiNumModules = sizeof(gastModules_CNA_Master) / sizeof(gastModules_CNA_Master[ 0 ]),

   .pastModules = &gastModules_CNA_Master,

   .enMCU_ID = enMCUE_SRU_IO,

   .enDeployment = enSRU_DEPLOYMENT__CN,
};
/*----------------------------------------------------------------------------
 *----------------------------------------------------------------------------*/
static struct st_module * gastModules_CNA_Slave[] =
{
   &gstSys_Mod,  // must include the system module in addition to app modules

   &gstMod_CAN,
   &gstMod_LocalInputs,
   &gstMod_LocalOutputs,
   &gstMod_Heartbeat,
   &gstMod_SData_IONet,
   &gstMod_Watchdog,
   &gstMod_Fault,
};

static struct st_module_control gstModuleControl_CNA_Slave =
{
   .uiNumModules = sizeof(gastModules_CNA_Slave) / sizeof(gastModules_CNA_Slave[ 0 ]),

   .pastModules = &gastModules_CNA_Slave,

   .enMCU_ID = enMCUE_SRU_IO,

   .enDeployment = enSRU_DEPLOYMENT__CN,
};
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
struct st_module_control *gpstModuleControl_ThisDeployment;

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void Deployment_Init( void )
{
   int iError = 1;  // assume error

   enum_sys_mcu_ids enMCU_ID = enMCU_Unknown;

   if(SRU_Read_MCU_ID() == enMCU_Input)
   {
      enMCU_ID = enMCUF_Input;
   }

   if ( enMCU_ID == enMCUF_Input )
   {
      if ( GetSRU_Deployment() == enSRU_DEPLOYMENT__CN )
      {
         SetIOBoardType(IO_BOARD_TYPE__Input);
         SetSRU_ExpansionMasterID();
         if(GetSRU_ExpansionMasterID())
         {
            SetSystemNodeID(SYS_NODE__EXP);
            gpstModuleControl_ThisDeployment = &gstModuleControl_CNA_Master;
            SharedData_CarNet_Init( GetSRU_Deployment() );
            SharedData_IONet_Init( );
         }
         else
         {
            SetSystemNodeID(SYS_NODE__EXP_S);
            gpstModuleControl_ThisDeployment = &gstModuleControl_CNA_Slave;
            SharedData_IONet_Init( );
         }
         iError = 0;
      }
   }

   if ( iError )
   {
      while ( 1 )
      {
         static uint32_t uiCounter, bState;

         if( uiCounter >= 0xfffff )
         {
            uiCounter = 0;

            bState = ( bState )? 0: 1;

            SRU_Write_LED( enSRU_LED_Heartbeat, bState );
            SRU_Write_LED( enSRU_LED_Fault,     bState );
         }
         else
         {
            uiCounter++;
         }
      }
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
int main( void )
{

   SystemCoreClockUpdate();

   Sys_Init();

   MCU_InputBoard_Init();

   Deployment_Init();

   Mod_InitAllModules( gpstModuleControl_ThisDeployment );

   __enable_irq();

   // This is really a while(1) loop. Sys_Shutdown() always returns 0. The
   // function is being provided in anticipation of a future time when this
   // application might be running on a PC simulator and we would need a
   // mechanism to terminate this application.
   while ( !Sys_Shutdown() )
   {
      Mod_RunOneModule( gpstModuleControl_ThisDeployment );
   }

   return 0;
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
