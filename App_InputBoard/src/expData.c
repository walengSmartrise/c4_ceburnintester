/******************************************************************************
 * @author   Keith Soneda
 * @version  V1.00
 * @date     6, Sept 2018
 *
 * @note    Access functions for master expansion board's data
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/
#include "mod.h"
#include "mcu_inputBoard.h"
#include "sru.h"
#include "sys.h"

#include <stdint.h>
#include <string.h>
#include "riser.h"
#include "expData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static st_exp_data stExpData;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void ExpData_LoadInputStatusMessage( un_sdata_datagram *punDatagram )
{
   for(uint8_t i = 0; i < NUM_EXP_BOARDS_PER_MASTER; i++)
   {
      punDatagram->auc8[i] = ExpData_GetInputs(i);
   }
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void ExpData_LoadMasterStatusMessage( un_sdata_datagram *punDatagram )
{
   punDatagram->auc8[0] = SRU_Read_DIP_Bank(enSRU_DIP_BANK_B);
   punDatagram->auc8[1] = 0; // Unused to preserve compatibility with v1.02.54
   punDatagram->auc8[2] = 0; // Unused to preserve compatibility with v1.02.54

   punDatagram->auc8[3] = ExpData_GetOnlineBitmap();

   punDatagram->auc8[4] = ExpData_GetError(0) & 0xF;
   punDatagram->auc8[4] |= ( ExpData_GetError(1) & 0xF ) << 4;
   punDatagram->auc8[5] = ExpData_GetError(2) & 0xF;
   punDatagram->auc8[5] |= ( ExpData_GetError(3) & 0xF ) << 4;
   punDatagram->auc8[6] = ExpData_GetError(4) & 0xF;
   punDatagram->auc8[6] |= ( ExpData_GetError(5) & 0xF ) << 4;
   punDatagram->auc8[7] = ExpData_GetError(6) & 0xF;
   punDatagram->auc8[7] |= ( ExpData_GetError(7) & 0xF ) << 4;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void ExpData_UnloadSlaveMessage( un_sdata_datagram *punDatagram, uint8_t ucSlaveIndex )
{
   if( ucSlaveIndex < NUM_EXP_BOARDS_PER_MASTER-1 )
   {
      en_ris_exp_error eError = punDatagram->auc8[7];
      en_io_board_type eType = punDatagram->auc8[4];

      ExpData_SetError(ucSlaveIndex+1, eError);

      /* To maintain backwards compatibility with v1.02.54 and prior */
      /* Byte2 was previously an invalid DIP flag */
      if( punDatagram->auc8[2] && !eError )
      {
         ExpData_SetError(ucSlaveIndex+1, ERROR_RIS_EXP__ADDRESS);
      }

      if( eType == IO_BOARD_TYPE__Input )
      {
         ExpData_SetInputs(ucSlaveIndex+1, punDatagram->auc8[0]);
         ExpData_SetInputs(ucSlaveIndex+2, punDatagram->auc8[5]);
         ExpData_SetInputs(ucSlaveIndex+3, punDatagram->auc8[6]);
      }
      else
      {
         ExpData_SetInputs(ucSlaveIndex+1, punDatagram->auc8[0]);
      }

      ExpData_ClrOfflineTimer(ucSlaveIndex+1);
   }
}
/*----------------------------------------------------------------------------
   Clear the timeout counter, update the inputs, and the fault for this board
 *----------------------------------------------------------------------------*/
void ExpData_UpdateLocalData(void)
{
   uint32_t uiInputs = GetLocalInputs_ThisNode();
   if(GetSRU_ExpansionMasterID())
   {
      ExpData_ClrOfflineTimer(0);
      ExpData_SetError( 0, GetFault() );
      if( GetIOBoardType() == IO_BOARD_TYPE__Input )
      {
         ExpData_SetInputs( 0, GET_BYTE_U32(uiInputs, 0) );
         ExpData_SetInputs( 1, GET_BYTE_U32(uiInputs, 1) );
         ExpData_SetInputs( 2, GET_BYTE_U32(uiInputs, 2) );
      }
      else
      {
         ExpData_SetInputs( 0, GET_BYTE_U32(uiInputs, 0) );
      }
   }
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void ExpData_Initialize(void)
{
   for(uint8_t i = 0; i < NUM_EXP_BOARDS_PER_MASTER; i++)
   {
      stExpData.auwOfflineCounter_ms[i] = RISER_OFFLINE_COUNT_MS__UNUSED;
      stExpData.aucInputs[i] = 0;
      stExpData.aucOutputs[i] = 0;
      stExpData.aeError[i] = ERROR_RIS_EXP__UNKNOWN;
   }
   stExpData.ucBF_OnlineFlags = 0;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
uint8_t ExpData_GetOnlineBitmap(void)
{
   return stExpData.ucBF_OnlineFlags;
}
void ExpData_ClrOfflineTimer( uint8_t ucBoardIndex )
{
   if( ucBoardIndex < NUM_EXP_BOARDS_PER_MASTER )
   {
      stExpData.auwOfflineCounter_ms[ucBoardIndex] = 0;
   }
}
void ExpData_UpdateOfflineTimers( uint16_t uwRunPeriod_ms )
{
   for(uint8_t i = 0; i < NUM_EXP_BOARDS_PER_MASTER; i++)
   {
      if( stExpData.auwOfflineCounter_ms[i] < RISER_OFFLINE_COUNT_MS__FAULTED )
      {
         stExpData.auwOfflineCounter_ms[i] += uwRunPeriod_ms;
         Sys_Bit_Set(&stExpData.ucBF_OnlineFlags, i, 1);
      }
      else
      {
         Sys_Bit_Set(&stExpData.ucBF_OnlineFlags, i, 0);
      }
   }
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void ExpData_SetInputs( uint8_t ucBoardIndex, uint8_t ucInputs )
{
   if( ucBoardIndex < NUM_EXP_BOARDS_PER_MASTER )
   {
      stExpData.aucInputs[ucBoardIndex] = ucInputs;
   }
}
uint8_t ExpData_GetInputs( uint8_t ucBoardIndex )
{
   uint8_t ucInputs = 0;
   if( ucBoardIndex < NUM_EXP_BOARDS_PER_MASTER )
   {
      ucInputs = stExpData.aucInputs[ucBoardIndex];
   }
   return ucInputs;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void ExpData_SetOutputs( uint8_t ucBoardIndex, uint8_t ucOutputs )
{
   if( ucBoardIndex < NUM_EXP_BOARDS_PER_MASTER )
   {
      stExpData.aucOutputs[ucBoardIndex] = ucOutputs;
   }
}
uint8_t ExpData_GetOutputs( uint8_t ucBoardIndex )
{
   uint8_t ucOutputs = 0;
   if( ucBoardIndex < NUM_EXP_BOARDS_PER_MASTER )
   {
      ucOutputs = stExpData.aucOutputs[ucBoardIndex];
   }
   return ucOutputs;
}
/*----------------------------------------------------------------------------

 *----------------------------------------------------------------------------*/
void ExpData_SetError( uint8_t ucBoardIndex, en_ris_exp_error eError )
{
   if( ucBoardIndex < NUM_EXP_BOARDS_PER_MASTER )
   {
      stExpData.aeError[ucBoardIndex] = eError;
   }
}
en_ris_exp_error ExpData_GetError( uint8_t ucBoardIndex )
{
   en_ris_exp_error eError = 0;
   if( ucBoardIndex < NUM_EXP_BOARDS_PER_MASTER )
   {
      eError = stExpData.aeError[ucBoardIndex];
   }
   return eError;
}
/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
