/******************************************************************************
 *
 * @file
 * @brief
 * @version  V1.00
 * @date
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "mod.h"
#include "sru.h"
#include "mcu_inputBoard.h"
#include "sys.h"
#include <string.h>
#include "ring_buffer.h"

#include "GlobalData.h"
#include "sdata.h"
#include "expData.h"
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

static uint32_t Init( struct st_module *pstThisModule );
static uint32_t Run( struct st_module *pstThisModule );

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

struct st_module gstMod_CAN =
{
   .pfnInit = Init,
   .pfnRun = Run,
};
RINGBUFF_T RxPositionRingBuffer;
/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

#define MAX_RING_BUFFER_READ           (4)
#define LIMIT_UNLOAD_CAN_CYCLES       (16)
#define EXTRACT_NET_ID(x)      (( x >> 25 ) & 0x0F )
#define EXTRACT_SRC_ID(x)      (( x >> 20 ) & 0x1F )
#define EXTRACT_DEST_ID(x)     (( x >> 15 ) & 0x1F )
#define EXTRACT_DATAGRAM_ID(x) (( x >> 10 ) & 0x1F )

#define OFFLINE_TIMEOUT_5MS         (1000U)

/* Since there won't always be a device communicating on CAN2,
 * initialize the offline timer with a starter value and suppress
 * communication errors for the bus if not communication was ever detected */
#define OFFLINE_TIMER_COM_NOT_DETECTED          (0xFFFF)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *-----------------------------------------------------------------------------*/
// Cleared if valid packet received on CAN1 for an IO Board
static uint16_t uwOfflineTimeout_CAN1_5ms;
static uint16_t uwOfflineTimeout_CAN2_5ms = OFFLINE_TIMER_COM_NOT_DETECTED;
/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static uint8_t bNewPacket_CAN1;
static uint8_t bNewPacket_CAN2;
/*-----------------------------------------------------------------------------
   Check for CAN Bus offline and reset if offline
 -----------------------------------------------------------------------------*/
#define BUS_ERROR_COUNT_LIMIT_5MS    (0)
static void CheckFor_CANBusOffline()
{
   static uint16_t uwCounter_CAN1_5ms;
   static uint16_t uwCounter_CAN2_5ms;
   if( Chip_CAN_GetGlobalStatus( LPC_CAN1 ) & CAN_GSR_BS ) // Bus offline if 1
   {
      if( ++uwCounter_CAN1_5ms >= BUS_ERROR_COUNT_LIMIT_5MS )
      {
         uwCounter_CAN1_5ms = 0;
         Chip_CAN_SetMode( LPC_CAN1, CAN_RESET_MODE, ENABLE );
         __NOP();
         __NOP();
         __NOP();
         Chip_CAN_SetMode( LPC_CAN1, CAN_RESET_MODE, DISABLE );
         SetFault( ERROR_RIS_EXP__BUS_RESET_CAN1 );
      }
   }
   else
   {
      uwCounter_CAN1_5ms = 0;
   }

   if( Chip_CAN_GetGlobalStatus( LPC_CAN2 ) & CAN_GSR_BS ) // Bus offline if 1
   {
      if( ++uwCounter_CAN2_5ms >= BUS_ERROR_COUNT_LIMIT_5MS )
      {
         uwCounter_CAN2_5ms = 0;
         Chip_CAN_SetMode( LPC_CAN2, CAN_RESET_MODE, ENABLE );
         __NOP();
         __NOP();
         __NOP();
         Chip_CAN_SetMode( LPC_CAN2, CAN_RESET_MODE, DISABLE );
         SetFault( ERROR_RIS_EXP__BUS_RESET_CAN2 );
      }
   }
   else
   {
      uwCounter_CAN2_5ms = 0;
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void CheckFor_CommunicationLoss_EXP()
{
   if( GetSRU_ExpansionMasterID() )
   {
      if( uwOfflineTimeout_CAN1_5ms > ( FAULT_TIMEOUT_CAR_COM_MS/gstMod_CAN.uwRunPeriod_1ms ) )
      {
         SetFault( ERROR_RIS_EXP__COM_CAR );
      }
      else
      {
         uwOfflineTimeout_CAN1_5ms++;
      }

      if( uwOfflineTimeout_CAN2_5ms > ( FAULT_TIMEOUT_SLAVE_COM_MS/gstMod_CAN.uwRunPeriod_1ms ) )
      {
         /* Suppress the offline error for CAN2 if no communication has ever been detected on the bus.
          * There is not always a device on this bus. */
         if( uwOfflineTimeout_CAN2_5ms != OFFLINE_TIMER_COM_NOT_DETECTED )
         {
            SetFault( ERROR_RIS_EXP__COM_SLAVE );
         }
      }
      else
      {
         uwOfflineTimeout_CAN2_5ms++;
      }
   }
   else
   {
      if( uwOfflineTimeout_CAN1_5ms > ( FAULT_TIMEOUT_MASTER_COM_MS/gstMod_CAN.uwRunPeriod_1ms ) )
      {
         SetFault( ERROR_RIS_EXP__COM_MASTER );
      }
      else
      {
         uwOfflineTimeout_CAN1_5ms++;
      }
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void Receive_Packet(CAN_MSG_T *pstRxMsg)
{
   un_sdata_datagram unDatagram;
   uint8_t ucNetworkDatagramID = ExtractFromCAN_DatagramID(pstRxMsg->ID);

   memcpy(&unDatagram, &pstRxMsg->Data, sizeof(un_sdata_datagram) );
   InsertSharedDatagram_ByDatagramID( &unDatagram, ucNetworkDatagramID);
}
/*-----------------------------------------------------------------------------

   Initialize anything required prior to running for the first time

 -----------------------------------------------------------------------------*/
static uint32_t Init( struct st_module *pstThisModule )
{
   pstThisModule->uwInitialDelay_1ms = 10;
   pstThisModule->uwRunPeriod_1ms = 5;

   ExpData_Initialize();

   return 0;
}
/* Check for expansion boards with overlapping input address space */
static uint8_t CheckForDuplicateSlaveAddress(un_sdata_datagram *punDatagram)
{
   uint8_t bDuplicate = 0;
   uint8_t ucLocalSlaveID = SRU_Read_DIP_Bank(enSRU_DIP_BANK_B) & 0x07;
   uint8_t ucRxSlaveID = punDatagram->auc8[1] & 0x07;

   /* This is a 24-input board meaning it will take
    *  the space of 3 standard exp IO boards */
   for(uint8_t i = 0; i < 3; i++)
   {
      uint8_t ucOccupiedID = ucLocalSlaveID+i;
      if(ucRxSlaveID == ucOccupiedID)
      {
         bDuplicate = 1;
         break;
      }
   }

   return bDuplicate;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
static void UnloadCAN_CNA_Master( void )
{
    CAN_MSG_T tmp;
    // Car Net
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN1_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
          uwOfflineTimeout_CAN1_5ms = 0;
          un_sdata_datagram unDatagram;
          memcpy( unDatagram.auc8, tmp.Data, sizeof ( tmp.Data ) );

          // Unload or forward packet
          Receive_Packet(&tmp);
          bNewPacket_CAN1 = 1;

          uint8_t ucDatagramID = ExtractFromCAN_DatagramID(tmp.ID) ;
          CheckFor_AddressFault_Master( &unDatagram, ucDatagramID );
       }
    }

    // IO Net
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN2_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
          uwOfflineTimeout_CAN2_5ms = 0;
          un_sdata_datagram unDatagram;
          uint8_t ucDatagramID = (tmp.ID >> 21) & 0xFF;
          uint8_t ucNetworkID = (tmp.ID >> 18) & 0x7;
          uint8_t ucNodeID = (tmp.ID >> 14) & 0xF;
          uint16_t uwDestinationBitmap = tmp.ID & 0xFFFF;
          if( ( ucNetworkID == SDATA_NET__IO )
           && ( ucNodeID == SYS_NODE__EXP_S )
           && ( ucDatagramID < NUM_IONet_Slave_DATAGRAMS )
           && ( ( uwDestinationBitmap & NODE__EXP ) == NODE__EXP ) )
          {
              memcpy( unDatagram.auc8, tmp.Data, sizeof ( tmp.Data ) );

              SDATA_WriteDatagram( gastIONetSData_Nodes[IO_NET__SLAVE],
                                   ucDatagramID,
                                   &unDatagram );
              ExpData_UnloadSlaveMessage(&unDatagram, ucDatagramID);
              if(CheckForDuplicateSlaveAddress(&unDatagram))
              {
                 SetFault(ERROR_RIS_EXP__ADDRESS);
              }
          }
          bNewPacket_CAN2 = 1;
       }
    }
}

static void UnloadCAN_CNA_Slave( void )
{
    CAN_MSG_T tmp;
    // parent Net
    for(uint8_t i = 0; i < LIMIT_UNLOAD_CAN_CYCLES; i++)
    {
       // Read a CAN message from the buffer
       if(!CAN1_UnloadFromRB(&tmp)) //if empty
       {
          i = LIMIT_UNLOAD_CAN_CYCLES;
          break;
       }
       else
       {
          uwOfflineTimeout_CAN1_5ms = 0;
          un_sdata_datagram unDatagram;
          uint8_t ucDatagramID = (tmp.ID >> 21) & 0xFF;
          uint8_t ucNetworkID = (tmp.ID >> 18) & 0x7;
          uint8_t ucNodeID = (tmp.ID >> 14) & 0xF;
          uint16_t uwDestinationBitmap = tmp.ID & 0xFFFF;
          if( ( ucNetworkID == SDATA_NET__IO )
           && ( ucNodeID == SYS_NODE__EXP )
           && ( ucDatagramID < NUM_IONet_Master_DATAGRAMS )
           && ( ( uwDestinationBitmap & NODE__EXP_S ) == NODE__EXP_S ) )
          {
              memcpy( unDatagram.auc8, tmp.Data, sizeof ( tmp.Data ) );

              SDATA_WriteDatagram( gastIONetSData_Nodes[IO_NET__MASTER],
                                   ucDatagramID,
                                   &unDatagram );
          }
          else if( ( ucNetworkID == SDATA_NET__IO )
                && ( ucNodeID == SYS_NODE__EXP_S )
                && ( ucDatagramID < NUM_IONet_Slave_DATAGRAMS )
                && ( ( uwDestinationBitmap & NODE__EXP ) == NODE__EXP ) )
          {
             memcpy( unDatagram.auc8, tmp.Data, sizeof ( tmp.Data ) );

             if(CheckForDuplicateSlaveAddress(&unDatagram))
             {
                SetFault(ERROR_RIS_EXP__ADDRESS);
             }
          }
       }
    }
}
static void UnloadCAN_CNA( void )
{
   if(GetSRU_ExpansionMasterID())
   {
      UnloadCAN_CNA_Master();
   }
   else
   {
      UnloadCAN_CNA_Slave();
   }
}

/*-----------------------------------------------------------------------------


 -----------------------------------------------------------------------------*/
static uint32_t Run( struct st_module *pstThisModule )
{
   // This is the only place the can periphs are init.
   switch(GetSRU_Deployment())
   {
      case enSRU_DEPLOYMENT__CN:
         CheckFor_CommunicationLoss_EXP();
         UnloadCAN_CNA();
         ExpData_UpdateLocalData();
         ExpData_UpdateOfflineTimers(pstThisModule->uwRunPeriod_1ms);
         break;
      default:
         break;
   }

    CheckFor_CANBusOffline();

    /* Toggle LEDs when new packet received if in test mode */
    if( GetSRU_TestMode() )
    {
       static uint8_t bFaultLED;
       static uint8_t bHeartbeatLED;
       if(bNewPacket_CAN1)
       {
          bNewPacket_CAN1 = 0;
          bHeartbeatLED ^= 1;
       }
       if(bNewPacket_CAN2)
       {
          bNewPacket_CAN2 = 0;
          bFaultLED ^= 1;
       }
       SRU_Write_LED( enSRU_LED_Extra, bFaultLED ); // LED B
       SRU_Write_LED( enSRU_LED_Heartbeat, bHeartbeatLED ); // LED A
    }

    return 0;
}
