/******************************************************************************
 *
 * @file     mod.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef _SDATA_H
#define _SDATA_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/* CNA - IO Expansion */
extern struct st_sdata_control gstSData_CarNetData;

extern struct st_sdata_control * gastIONetSData_Nodes[ NUM_IO_NET_NODES ];
extern uint8_t gaucIONetPerNodeNumDatagrams[ NUM_IO_NET_NODES ];
extern struct st_sdata_local gstIONetSData_Config;

extern struct st_sdata_control * gastIOChildNetSData_Nodes[ NUM_IO_NET_NODES ];
extern uint8_t gaucIOChildNetPerNodeNumDatagrams[ NUM_IO_NET_NODES ];
extern struct st_sdata_local gstIOChildNetSData_Config;

/* RIS - Riser Board */
extern struct st_sdata_local gstGroupNetSData_Config;

extern struct st_sdata_control * gastHallNetSData_Nodes[ NUM_HALL_NET_NODES ];
extern uint8_t gaucHallNetPerNodeNumDatagrams[ NUM_HALL_NET_NODES ];
extern struct st_sdata_local gstHallNetSData_Config;

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void SharedData_CarNet_Init( int iSRU_Deployment );
void SharedData_IONet_Init();
void SharedData_GroupNet_Init( );
void SharedData_HallNet_Init();


void SetSlaveInputs( uint8_t ucInputs, uint8_t ucIndex );
uint8_t GetSlaveOutputs( uint8_t ucIndex );

#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
