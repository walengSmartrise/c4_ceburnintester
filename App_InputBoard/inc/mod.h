/******************************************************************************
 *
 * @file     mod.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef MOD_H
#define MOD_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/

#include <stdint.h>
#include "sys.h"
#include "sdata.h"
#include "shared_data.h"
#include "mcu_inputBoard.h"
#include "datagrams.h"
#include "GlobalData.h"
#include "mod_fault.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define CAR_OFFLINE_TIMEOUT (100)
#define HALLBOARD_OFFLINE_TIMEOUT (250)
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

extern struct st_module_control *gpstModuleControl_ThisDeployment;

extern struct st_module gstMod_SData_CarNet;
extern struct st_module gstMod_SData_IONet;
extern struct st_module gstMod_SData_IOChildNet;

extern struct st_module gstMod_LocalInputs;
extern struct st_module gstMod_LocalOutputs;
extern struct st_module gstMod_Heartbeat;

extern struct st_module gstMod_Watchdog;
extern struct st_module gstMod_CAN;

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

void GetChildInputs(un_sdata_datagram *punChildNodeInputs);
void SetChildOutputs(un_sdata_datagram *punChildNodeOutputs);

void SetInputsToSend_C2P( un_sdata_datagram *punInputsReceived );
void SetOutputsToSend_P2C( un_sdata_datagram *punOutputsReceived );
void SetInputsToSend_CarNet( un_sdata_datagram *punInputsReceived );

Datagram_ID GetExpNetworkDatagramID();
Datagram_ID GetExpDipComNetworkDatagramID();

void SetLocalInput_ThisNode( enum en_sru_inputs enInput, uint8_t bValue);
uint32_t GetLocalInputs_ThisNode();

void SetLocalOutputs_ThisNode( uint32_t uiOutputs );
uint32_t GetLocalOutputs_ThisNode( void );
#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
