
#ifndef GLOBAL_DATA_H
#define GLOBAL_DATA_H

#include <stdint.h>

typedef enum
{
   IO_BOARD_TYPE__InOut, // Standard IO boards SR3031
   IO_BOARD_TYPE__Input, // 24-input boards

   NUM_IO_BOARD_TYPES
}en_io_board_type;

uint8_t GetSRU_ExpansionMasterID( void );
void SetSRU_ExpansionMasterID( void );
uint8_t GetSRU_ExpansionSlaveID(void);
void SetSRU_RiserBoardID();
enum en_group_net_nodes GetSRU_RiserBoardID();

en_io_board_type GetIOBoardType();
void SetIOBoardType(en_io_board_type eBoardType);
#endif // GLOBAL_DATA_H
