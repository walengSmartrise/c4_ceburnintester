/******************************************************************************
 *
 * @file     mod.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     20, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef _MOD_FAULT_H_
#define _MOD_FAULT_H_

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include <stdint.h>
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/
#define MOD_RUN_PERIOD_FAULT_1MS       ( 5 )
#define FAULT_TIMEOUT_GROUP_COM_MS     ( 10000 )
#define FAULT_TIMEOUT_HALL_COM_MS      ( 20000 )
#define FAULT_TIMEOUT_CAR_COM_MS       ( 10000 )
#define FAULT_TIMEOUT_MASTER_COM_MS    ( 10000 )
#define FAULT_TIMEOUT_SLAVE_COM_MS     ( 10000 )
/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
extern struct st_module gstMod_Fault;
 /*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/
void SetFault( en_ris_exp_error eFault );
en_ris_exp_error GetFault();

void CheckFor_AddressFault_Master( un_sdata_datagram *punDatagram, uint8_t ucDatagramID );
#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/

