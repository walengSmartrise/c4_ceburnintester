/******************************************************************************
 *
 * @file     sru_b.h
 * @brief    Systen Header File
 * @version  V1.00
 * @date     22, March 2016
 *
 * @note
 *
 ******************************************************************************/

#ifndef SRU_B_H
#define SRU_B_H

/*----------------------------------------------------------------------------
 *
 * Include any additional header files that are needed to compile this
 * header file here.
 *
 *----------------------------------------------------------------------------*/
#include "sys.h"
#include "sru.h"
#include <stdint.h>

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that are required for this header
 * file or that need to be seen by more than one source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that need to be seen by
 * multiple source files here.
 *
 *----------------------------------------------------------------------------*/
enum en_sru_id_pins
{
   enSRU_ID_PIN1,
   enSRU_ID_PIN2,
   enSRU_ID_PIN3,
   enSRU_ID_PIN4,

   SRU_NUM_ID_PINS
};

enum en_sru_outputs
{
   enSRU_Output_601,
   enSRU_Output_602,
   enSRU_Output_603,
   enSRU_Output_604,
   enSRU_Output_605,
   enSRU_Output_606,
   enSRU_Output_607,
   enSRU_Output_608,

   SRU_NUM_OUTPUTS
};

enum en_sru_leds
{
   enSRU_LED_Heartbeat,
   enSRU_LED_Fault,
   enSRU_LED_Alarm,

   SRU_NUM_LEDS
};

enum en_sru_dip_switches
{
   enSRU_DIP_A1,
   enSRU_DIP_A2,
   enSRU_DIP_A3,
   enSRU_DIP_A4,
   enSRU_DIP_A5,
   enSRU_DIP_A6,
   enSRU_DIP_A7,
   enSRU_DIP_A8,

   enSRU_DIP_B1,
   enSRU_DIP_B2,
   enSRU_DIP_B3,
   enSRU_DIP_B4,
   enSRU_DIP_B5,
   enSRU_DIP_B6,
   enSRU_DIP_B7,
   enSRU_DIP_B8,

   SRU_NUM_DIP_SWITCHES
};

enum en_sru_dip_banks
{
   enSRU_DIP_BANK_A,
   enSRU_DIP_BANK_B,

   enSRU_NUM_DIP_BANKS,
};

enum en_sru_buttons
{
   enSRU_BUTTON_UP,
   enSRU_BUTTON_DOWN,
   enSRU_BUTTON_LEFT,
   enSRU_BUTTON_RIGHT,
   enSRU_BUTTON_ENTER,

   SRU_NUM_BUTTONS
};
enum en_sru_lcd_outputs
{
   enSRU_LCD_OUTPUT__EN,
   enSRU_LCD_OUTPUT__RW,
   enSRU_LCD_OUTPUT__RS,

   enSRU_LCD_OUTPUT__DATA0,
   enSRU_LCD_OUTPUT__DATA1,
   enSRU_LCD_OUTPUT__DATA2,
   enSRU_LCD_OUTPUT__DATA3,
   enSRU_LCD_OUTPUT__DATA4,
   enSRU_LCD_OUTPUT__DATA5,
   enSRU_LCD_OUTPUT__DATA6,
   enSRU_LCD_OUTPUT__DATA7,

   SRU_NUM_LCD_OUTPUTS
};
enum en_sru_cpld_outputs
{
   enSRU_CPLD_OUTPUTS_X1,
   enSRU_CPLD_OUTPUTS_X2,
   enSRU_CPLD_OUTPUTS_X3,
   enSRU_CPLD_OUTPUTS_X4,
   enSRU_CPLD_OUTPUTS_X5,
   enSRU_CPLD_OUTPUTS_X6,
   enSRU_CPLD_OUTPUTS_X7,
   enSRU_CPLD_OUTPUTS_X8,

   SRU_NUM_CPLD_OUTPUTS,
};
/*----------------------------------------------------------------------------
 *
 * Place global variables that are used by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

//extern const struct st_port_pin_pair gastPortPinPairs_Inputs[ SRU_NUM_INPUTS ];

/*----------------------------------------------------------------------------
 *
 * Place function prototypes that need to be seen by multiple source files here.
 *
 *----------------------------------------------------------------------------*/

void MCU_B_Init( void );

uint8_t MCU_Pin_In( uint32_t uiPort, uint32_t uiPin );

void MCU_Pin_Out( uint32_t uiPort, uint32_t uiPin, uint32_t bValue );

uint8_t SRU_Read_MCU_ID();
uint8_t SRU_Read_WatchDogEnable();

uint8_t SRU_Read_DIP_Switch( enum en_sru_dip_switches enSRU_DIP_Switch );

uint8_t SRU_Read_DIP_Bank( enum en_sru_dip_banks enSRU_DIP_Bank );

uint8_t SRU_Read_Button( enum en_sru_buttons enSRU_Button );

void SRU_Write_Output( enum en_sru_outputs enSRU_Output, uint8_t bValue );
void SRU_Write_LED( enum en_sru_leds enSRU_LED, uint8_t bValue );
void SRU_Write_LCD( enum en_sru_lcd_outputs enSRU_LCD, uint8_t bValue );

void SRU_Write_CPLD_Output( enum en_sru_cpld_outputs enSRU_CPLD_Output, uint8_t bValue );

en_sru_deployments GetSRU_Deployment( void );
void SetSRU_Deployment( void );

void CAN_Init( void );
Status CAN1_UnloadFromRB(CAN_MSG_T *pstRxMsg);
Status CAN1_LoadToRB(CAN_MSG_T *pstRxMsg);
Status CAN2_UnloadFromRB(CAN_MSG_T *pstRxMsg);
Status CAN2_LoadToRB(CAN_MSG_T *pstRxMsg);
Status CAN1_FillHardwareBuffer();
Status CAN2_FillHardwareBuffer();

void UART_AB_Init( void );
uint8_t UART_AB_ReadByte(uint8_t *pucByte);
uint32_t UART_AB_SendRB(const void *data, int bytes);

#endif

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
