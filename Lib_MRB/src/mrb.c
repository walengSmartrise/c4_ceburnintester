/******************************************************************************
 *

 * @file     mcu_b.c
 * @brief    Support for MCU B on SRU Base board
 * @version  V1.00
 * @date     22, March 2016
 *
 * @note
 *
 ******************************************************************************/

/*----------------------------------------------------------------------------
 *
 * Place #include files required to compile this source file here.
 *
 *----------------------------------------------------------------------------*/

#include "sru_b.h"
#include "sys.h"

/*----------------------------------------------------------------------------
 *
 * Define global variables that will be used by both this source file
 * and by other source files here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place #define constants and macros that will be used only by this
 * source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place typedefs, structs, unions, and enums that will be used only
 * by this source file here.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place static variables that will be used only by this file here.
 *
 *----------------------------------------------------------------------------*/
static en_sru_deployments eSRU_Deployment = enSRU_DEPLOYMENT__Invalid0;

static const PortAndPin PortAndPin_Input_WDEnable = {2,  11};

static const PortAndPin PortAndPin_InputsID[SRU_NUM_ID_PINS] =
{
#if 0
      {0,  10 },
      {0,  11 },
      {0,  17 },
      {0,  18 },
#else
      {0,  18 },
      {0,  17 },
      {0,  11 },
      {0,  10 },
#endif
};

static const PortAndPin PortAndPin_DIPs[SRU_NUM_DIP_SWITCHES] =
{
   /* Bank A */
   {3,  5 },
   {0,  23 },
   {3,  6 },
   {3,  7 },
   {5,  1 },
   {1, 31 },
   {0, 12 },
   {0, 13 },
   /* Bank B */
   {2,  13 },
   {4,  3 },
   {1, 29 },
   {1, 28 },
   {1, 27 },
   {4, 2 },
   {1, 26 },
   {1, 25 },
};

static const PortAndPin PortAndPin_Buttons[SRU_NUM_BUTTONS] =
{
   {0, 24 },//enSRU_BUTTON_UP,
   {0, 25 },//enSRU_BUTTON_DOWN,
   {3,  3 },//enSRU_BUTTON_LEFT,
   {3,  4 },//enSRU_BUTTON_RIGHT,
   {5,  0 },//enSRU_BUTTON_ENTER,
};
//------------------------------------------------------------
static const PortAndPin PortAndPin_Outputs[ SRU_NUM_OUTPUTS ] =
{
#if 0
      {0,19},//enSRU_Output_608,
      {4, 7},//enSRU_Output_607,
      {0,20},//enSRU_Output_606,
      {0,21},//enSRU_Output_605,
      {4, 6},//enSRU_Output_604,
      {4, 5},//enSRU_Output_603,
      {2, 12},//enSRU_Output_602,
      {4, 4},//enSRU_Output_601,
#else
      {4, 4},//enSRU_Output_601,
      {2, 12},//enSRU_Output_602
      {4, 5},//enSRU_Output_603,
      {4, 6},//enSRU_Output_604,
      {0,21},//enSRU_Output_605,
      {0,20},//enSRU_Output_606,
      {4, 7},//enSRU_Output_607,
      {0,19},//enSRU_Output_608,
#endif

};

static const PortAndPin gastPortAndPin_LEDs[ SRU_NUM_LEDS ] =
{
      {3, 2},  // enSRU_LED_Heartbeat
      {3, 1},  // enSRU_LED_Fault
      {3, 0}   // enSRU_LED_Alarm
};

static const PortAndPin PortAndPin_LCDOutputs[ SRU_NUM_LCD_OUTPUTS ] =
{
   {0, 30}, //enSRU_LCD_OUTPUT__EN,
   {0, 29}, //enSRU_LCD_OUTPUT__RW,
   {0, 26}, //enSRU_LCD_OUTPUT__RS,

   {2,  0}, //enSRU_LCD_OUTPUT__DATA0,
   {2,  1}, //enSRU_LCD_OUTPUT__DATA1,
   {2,  2}, //enSRU_LCD_OUTPUT__DATA2,
   {2,  3}, //enSRU_LCD_OUTPUT__DATA3,

   {2,  4}, //enSRU_LCD_OUTPUT__DATA4,
   {2,  5}, //enSRU_LCD_OUTPUT__DATA5,
   {2,  6}, //enSRU_LCD_OUTPUT__DATA6,
   {2,  7}  //enSRU_LCD_OUTPUT__DATA7,
};


static const PortAndPin PortAndPin_CPLD_Outputs[SRU_NUM_CPLD_OUTPUTS] =
{
      {1, 15},//enSRU_CPLD_OUTPUTS_X1,
      {1, 14},//enSRU_CPLD_OUTPUTS_X2,
      {1, 10},//enSRU_CPLD_OUTPUTS_X3,
      {1, 9},//enSRU_CPLD_OUTPUTS_X4,
      {1, 8},//enSRU_CPLD_OUTPUTS_X5,
      {1, 4},//enSRU_CPLD_OUTPUTS_X6,
      {1, 1},//enSRU_CPLD_OUTPUTS_X7,
      {1, 0},//enSRU_CPLD_OUTPUTS_X8,
};
/*----------------------------------------------------------------------------
 *
 * Place function prototypes that will be used only by this file here.
 * This section need only include "forward referenced" functions.
 *
 *----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *
 * Place function bodies here.
 *
 *----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
Returns 4 bit hardcoded MCU ID
-----------------------------------------------------------------------------*/
uint8_t SRU_Read_MCU_ID()
{
  uint8_t ucValue = 0;
  for(uint8_t i = 0; i < SRU_NUM_ID_PINS; i++)
  {
     uint8_t uiPort = PortAndPin_InputsID[i].port;
     uint8_t uiPin  = PortAndPin_InputsID[i].pin;
     uint8_t bValue = Chip_GPIO_GetPinState(LPC_GPIO, uiPort, uiPin);
     ucValue |= (bValue << i);
  }
  return ucValue;
}
/*-----------------------------------------------------------------------------
if pin is high, watchdog is enabled
-----------------------------------------------------------------------------*/
uint8_t SRU_Read_WatchDogEnable()
{
  uint8_t bValue = 0;

   uint8_t uiPort = PortAndPin_Input_WDEnable.port;
   uint8_t uiPin  = PortAndPin_Input_WDEnable.pin;

   bValue = Chip_GPIO_GetPinState(LPC_GPIO, uiPort, uiPin);

  return bValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_DIP_Switch( enum en_sru_dip_switches enSRU_DIP_Switch )
{
   uint8_t bValue = 0;

   if( enSRU_DIP_Switch < SRU_NUM_DIP_SWITCHES )
   {
      uint8_t ucPin = PortAndPin_DIPs[enSRU_DIP_Switch].pin;
      uint8_t ucPort = PortAndPin_DIPs[enSRU_DIP_Switch].port;
      bValue = !Chip_GPIO_GetPinState(LPC_GPIO,ucPort,ucPin);
   }

   return bValue;
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_DIP_Bank( enum en_sru_dip_banks enSRU_DIP_Bank )
{
   uint8_t ucValue = 0;

   if ( enSRU_DIP_Bank == enSRU_DIP_BANK_A )
   {
      for(uint8_t i = 0; i <= enSRU_DIP_A8; i++)
      {
         uint8_t ucPin = PortAndPin_DIPs[i].pin;
         uint8_t ucPort = PortAndPin_DIPs[i].port;
         uint8_t bValue = !Chip_GPIO_GetPinState(LPC_GPIO,ucPort,ucPin);
         ucValue |= (bValue << i);
      }
   }
   else if( enSRU_DIP_Bank == enSRU_DIP_BANK_B )
   {
      for(uint8_t i = enSRU_DIP_B1; i <= enSRU_DIP_B8; i++)
      {
         uint8_t ucPin = PortAndPin_DIPs[i].pin;
         uint8_t ucPort = PortAndPin_DIPs[i].port;
         uint8_t bValue = !Chip_GPIO_GetPinState(LPC_GPIO,ucPort,ucPin);
         ucValue |= (bValue << i);
      }
   }

   return ucValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
uint8_t SRU_Read_Button( enum en_sru_buttons enSRU_Button )
{
   uint8_t bValue = 0;

   if ( enSRU_Button < SRU_NUM_BUTTONS )
   {
      uint32_t uiPort = PortAndPin_Buttons[ enSRU_Button ].port;
      uint32_t uiPin = PortAndPin_Buttons[ enSRU_Button ].pin;

      // Invert the value returned by Sys_MCU_Pin_In() because when a button is
      // held down, the port pin is low and vice versa.

      bValue = !Chip_GPIO_GetPinState(LPC_GPIO,uiPort, uiPin);
   }

   return bValue;
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SRU_Write_Output( enum en_sru_outputs enSRU_Output, uint8_t bValue )
{
   if ( enSRU_Output < SRU_NUM_OUTPUTS )
   {
      uint8_t uiPort = PortAndPin_Outputs[ enSRU_Output ].port;
      uint8_t uiPin = PortAndPin_Outputs[ enSRU_Output ].pin;

      Chip_GPIO_SetPinState(LPC_GPIO, uiPort, uiPin, bValue );
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/

void SRU_Write_LED( enum en_sru_leds enSRU_LED, uint8_t bValue )
{
   if ( enSRU_LED < SRU_NUM_LEDS )
   {
      uint32_t uiPort = gastPortAndPin_LEDs[ enSRU_LED ].port;
      uint32_t uiPin = gastPortAndPin_LEDs[ enSRU_LED ].pin;

      // The port pins that control the LEDs must be pulled low to turn on the
      // LED so we need to invert bValue to get the desired behavior.
      Chip_GPIO_SetPinState(LPC_GPIO, uiPort, uiPin, !bValue );
   }
}
/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SRU_Write_LCD( enum en_sru_lcd_outputs enSRU_LCD, uint8_t bValue )
{
   if ( enSRU_LCD < SRU_NUM_LCD_OUTPUTS )
   {
      uint32_t uiPort = PortAndPin_LCDOutputs[ enSRU_LCD ].port;
      uint32_t uiPin = PortAndPin_LCDOutputs[ enSRU_LCD ].pin;

      Chip_GPIO_SetPinState(LPC_GPIO, uiPort, uiPin, bValue );
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void SRU_Write_CPLD_Output( enum en_sru_cpld_outputs enSRU_CPLD_Output, uint8_t bValue )
{
   if ( enSRU_CPLD_Output < SRU_NUM_CPLD_OUTPUTS )
   {
      uint8_t uiPort = PortAndPin_CPLD_Outputs[ enSRU_CPLD_Output ].port;
      uint8_t uiPin = PortAndPin_CPLD_Outputs[ enSRU_CPLD_Output ].pin;

      Chip_GPIO_SetPinState(LPC_GPIO, uiPort, uiPin, bValue );
   }
}

/*-----------------------------------------------------------------------------
   Verify the 4 ID pins connected to the MCU identify this MCU as part of the
   SRU Base board (MCU A). If not, loop forever flashing all LEDs.
 -----------------------------------------------------------------------------*/
void VerifyBoardID( void )
{
   uint8_t ucMCU_ID = SRU_Read_MCU_ID() & 0xE;// Ignore least significant bit
   if(ucMCU_ID != enMCU_MR_UI)
   {
      while ( 1 )
      {
         static uint32_t uiCounter, bState;

         if( uiCounter >= 0xfffff )
         {
            uiCounter = 0;

            bState = ( bState )? 0: 1;

            SRU_Write_LED( enSRU_LED_Heartbeat, bState );
            SRU_Write_LED( enSRU_LED_Alarm,     bState );
            SRU_Write_LED( enSRU_LED_Fault,     bState );
         }
         else
         {
            uiCounter++;
         }
      }
   }
}

static void ConfigureGPIO_DirectionOutputs( void )
{
    for( uint8_t i = 0; i < SRU_NUM_LEDS; i++ )
    {
        uint32_t uiPort = gastPortAndPin_LEDs[ i ].port;
        uint32_t uiPin  = gastPortAndPin_LEDs[ i ].pin;

        Chip_GPIO_SetDir(LPC_GPIO, uiPort, uiPin, true);
    }

    for( uint8_t i = 0; i < SRU_NUM_OUTPUTS; i++ )
    {
        uint32_t uiPort = PortAndPin_Outputs[ i ].port;
        uint32_t uiPin  = PortAndPin_Outputs[ i ].pin;

        Chip_GPIO_SetDir(LPC_GPIO, uiPort, uiPin, true);
    }

    for( uint8_t i = 0; i < SRU_NUM_LCD_OUTPUTS; i++ )
    {
       uint32_t uiPort = PortAndPin_LCDOutputs[ i ].port;
       uint32_t uiPin = PortAndPin_LCDOutputs[ i ].pin;

       Chip_GPIO_SetDir(LPC_GPIO, uiPort, uiPin, true);
    }

    for( uint8_t i = 0; i < SRU_NUM_CPLD_OUTPUTS; i++ )
    {
       uint32_t uiPort = PortAndPin_CPLD_Outputs[ i ].port;
       uint32_t uiPin = PortAndPin_CPLD_Outputs[ i ].pin;

       Chip_GPIO_SetDir(LPC_GPIO, uiPort, uiPin, true);
    }

    // Turn off all outputs.
    for ( uint32_t i = 0; i < SRU_NUM_OUTPUTS; ++i )
    {
       SRU_Write_Output( i, 0 );
    }

    // Turn off all LEDs.
    for ( uint32_t i = 0; i < SRU_NUM_LEDS; ++i )
    {
       SRU_Write_LED( i, 0 );
    }
    // Turn off all LCD pins.
    for ( uint32_t i = 0; i < SRU_NUM_LCD_OUTPUTS; ++i )
    {
       SRU_Write_LCD( i, 0 );
    }

    // Turn off all CPLD pins.
    for ( uint32_t i = 0; i < SRU_NUM_CPLD_OUTPUTS; ++i )
    {
       SRU_Write_CPLD_Output( i, 0 );
    }
}

/*-----------------------------------------------------------------------------
   Set bit to indicate pin direction (0 = input, 1 = output).
 -----------------------------------------------------------------------------*/
static void ConfigureGPIO_Direction( void )
{
    ConfigureGPIO_DirectionOutputs();
}

en_sru_deployments GetSRU_Deployment( void )
{
     return eSRU_Deployment;
}

void SetSRU_Deployment( void )
{
   uint8_t ucID = SRU_Read_MCU_ID() & 0xE;// Ignore least significant bit
   eSRU_Deployment = enSRU_DEPLOYMENT__Invalid0;
   if( ucID == enMCU_MR_UI )
   {
      eSRU_Deployment = enSRU_DEPLOYMENT__MR;
   }
}

/*-----------------------------------------------------------------------------

 -----------------------------------------------------------------------------*/
void MCU_B_Init( void )
{
   // The following is init in the reset isr. See board_sysinit
   ConfigureGPIO_Direction();

   VerifyBoardID();

   // Set the global deployment variable. This is used to set up some
   // of the other board specific features.
   SetSRU_Deployment();

   CAN_Init();
   UART_AB_Init();
   RTC_InitPeripheral();
}

/*----------------------------------------------------------------------------
 *
 * End of file.
 *
 *----------------------------------------------------------------------------*/
