#include "board.h"
/* The System initialization code is called prior to the application and
   initializes the board for run-time operation. Board initialization
   includes clock setup and default pin muxing configuration. */

#define IOCON_D_INPUT      (IOCON_FUNC0 | IOCON_HYS_EN)
#define IOCON_D_OUTPUT     (IOCON_FUNC0 | IOCON_HYS_EN)

#define IOCON_D_LED        (IOCON_FUNC0 | IOCON_MODE_PULLUP | IOCON_HYS_EN)
#define IOCON_D_MCU_ID_PIN (IOCON_FUNC0 | IOCON_MODE_PULLUP)
#define IOCON_D_STARTUP_INPUT (IOCON_FUNC0)

#define IOCON_A_INPUT  (IOCON_FUNC0 | IOCON_HYS_EN | IOCON_FILT_DIS | IOCON_DIGMODE_EN)
#define IOCON_A_OUTPUT (IOCON_FUNC0 | IOCON_HYS_EN | IOCON_FILT_DIS | IOCON_DIGMODE_EN)

#define IOCON_W_INPUT  (IOCON_FUNC0 | IOCON_HYS_EN | IOCON_DIGMODE_EN | IOCON_FILT_DIS)
#define IOCON_W_OUTPUT (IOCON_FUNC0 | IOCON_HYS_EN | IOCON_DIGMODE_EN                 )

#define IOCON_U_INPUT (IOCON_FUNC0 )
#define IOCON_U_OUTPUT (IOCON_FUNC0 )
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/* Pin muxing configuration */
static const PINMUX_GRP_T pinmuxing[] =
{
      //Inputs
      {2, 11,   (IOCON_D_STARTUP_INPUT)                                   },// PortAndPin_Input_WDEnable

      {0, 10,   (IOCON_D_MCU_ID_PIN)                                   },// PortAndPin_InputsID
      {0, 11,   (IOCON_D_MCU_ID_PIN)                                   },// PortAndPin_InputsID
      {0, 17,   (IOCON_D_MCU_ID_PIN)                                   },// PortAndPin_InputsID
      {0, 18,   (IOCON_D_MCU_ID_PIN)                                   },// PortAndPin_InputsID

      {3, 5,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
      {0, 23,    (IOCON_A_INPUT)                                   },// PortAndPin_DIPs
      {3, 6,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
      {3, 7,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
      {5, 1,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
      {1,31,    (IOCON_A_INPUT)                                   },// PortAndPin_DIPs
      {0,12,    (IOCON_A_INPUT)                                   },// PortAndPin_DIPs
      {0,13,    (IOCON_A_INPUT)                                   },// PortAndPin_DIPs

      {2, 13,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
      {4, 3,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
      {1, 29,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
      {1, 28,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
      {1, 27,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
      {4, 2,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
      {1,26,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs
      {1,25,    (IOCON_D_INPUT)                                   },// PortAndPin_DIPs

      {1, 15,    (IOCON_D_OUTPUT)                                   },// enSRU_CPLD_OUTPUTS_X1
      {1, 14,    (IOCON_W_OUTPUT)                                   },// enSRU_CPLD_OUTPUTS_X2
      {1, 10,    (IOCON_D_OUTPUT)                                   },// enSRU_CPLD_OUTPUTS_X3
      {1, 9,    (IOCON_D_OUTPUT)                                   },// enSRU_CPLD_OUTPUTS_X4
      {1, 8,    (IOCON_D_OUTPUT)                                   },// enSRU_CPLD_OUTPUTS_X5
      {1, 4,    (IOCON_D_OUTPUT)                                   },// enSRU_CPLD_OUTPUTS_X6
      {1, 1,    (IOCON_D_OUTPUT)                                   },// enSRU_CPLD_OUTPUTS_X7
      {1, 0,    (IOCON_D_OUTPUT)                                   },// enSRU_CPLD_OUTPUTS_X8

      {0, 24,    (IOCON_A_INPUT)                                   },// enSRU_BUTTON_UP
      {0, 25,    (IOCON_A_INPUT)                                   },// enSRU_BUTTON_DOWN
      {3, 3,    (IOCON_D_INPUT)                                   },// enSRU_BUTTON_LEFT
      {3, 4,    (IOCON_D_INPUT)                                   },// enSRU_BUTTON_RIGHT
      {5, 0,    (IOCON_D_INPUT)                                   },// enSRU_BUTTON_ENTER
      // Outputs
      {4, 4,    (IOCON_D_OUTPUT)                                   },// enSRU_Output_601
      {2,12,    (IOCON_D_OUTPUT)                                   },// enSRU_Output_602
      {4,5,    (IOCON_D_OUTPUT)                                   },// enSRU_Output_603
      {4,6,    (IOCON_D_OUTPUT)                                   },// enSRU_Output_604
      {0,21,    (IOCON_D_OUTPUT)                                   },// enSRU_Output_605
      {0,20,    (IOCON_D_OUTPUT)                                   },// enSRU_Output_606
      {4,7,    (IOCON_D_OUTPUT)                                   },// enSRU_Output_607
      {0,19 ,   (IOCON_D_OUTPUT)                                   },// enSRU_Output_608

      {3,2,    (IOCON_D_LED)                                   },// enSRU_LED_Heartbeat
      {3,1,    (IOCON_D_LED)                                   },// enSRU_LED_Fault
      {3,0 ,   (IOCON_D_LED)                                   },// enSRU_LED_Alarm

      {0,30,    (IOCON_U_OUTPUT)                                   },// enSRU_LCD_OUTPUT__EN
      {0,29,    (IOCON_U_OUTPUT)                                   },// enSRU_LCD_OUTPUT__RW
      {0,26 ,   (IOCON_A_OUTPUT)                                   },// enSRU_LCD_OUTPUT__RS
      {2,0,    (IOCON_D_LED)                                   },// enSRU_LCD_OUTPUT__DATA0
      {2,1,    (IOCON_D_LED)                                   },// enSRU_LCD_OUTPUT__DATA1
      {2,2 ,   (IOCON_D_LED)                                   },// enSRU_LCD_OUTPUT__DATA2
      {2,3,    (IOCON_D_LED)                                   },// enSRU_LCD_OUTPUT__DATA3
      {2,4,    (IOCON_D_LED)                                   },// enSRU_LCD_OUTPUT__DATA4
      {2,5 ,   (IOCON_D_LED)                                   },// enSRU_LCD_OUTPUT__DATA5
      {2,6,    (IOCON_D_LED)                                   },// enSRU_LCD_OUTPUT__DATA6
      {2,7,    (IOCON_D_LED)                                   },// enSRU_LCD_OUTPUT__DATA7

      //COM
      {0,15 ,   (IOCON_FUNC1 | IOCON_HYS_EN                     ) },// U1_TXD
      {0,16 ,   (IOCON_FUNC1 | IOCON_HYS_EN                     ) },// U1_RXD

      {0,1 ,   ((IOCON_FUNC1 | IOCON_HYS_EN | IOCON_MODE_PULLUP )) },// CAN_TD1
      {0,0 ,   ((IOCON_FUNC1 | IOCON_HYS_EN | IOCON_MODE_PULLUP )) },// CAN_RD1

      {0,5 ,   ((IOCON_FUNC2 | IOCON_HYS_EN | IOCON_MODE_PULLUP )) },// CAN_TD2
      {0,4 ,   ((IOCON_FUNC2 | IOCON_HYS_EN | IOCON_MODE_PULLUP )) },// CAN_RD2

};

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/* Sets up system pin muxing */
void Board_SetupMuxing(void)
{
   // Reset the IOCON register
   Chip_SYSCTL_PeriphReset(SYSCTL_RESET_IOCON);

   /* Setup system level pin muxing */
   Chip_IOCON_SetPinMuxing(LPC_IOCON, pinmuxing, sizeof(pinmuxing) / sizeof(PINMUX_GRP_T));
}

/* Setup system clocking */
void Board_SetupClocking(void)
{
   // Use the internal system clock
   //Chip_SetupIrcClocking();
   Chip_SetupXtalClocking();
}


/* Set up and initialize hardware prior to call to main */
void Board_SystemInit(void)
{
   Board_SetupMuxing();
   Board_SetupClocking();
}

